.class public LX/I6T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Oi;


# instance fields
.field private final a:LX/0SI;

.field private final b:LX/I6X;

.field private final c:LX/0tX;

.field private final d:LX/0Sh;

.field public final e:LX/03V;

.field public final f:LX/2hU;

.field private final g:LX/Bkv;

.field public final h:Lcom/facebook/events/permalink/EventPermalinkFragment;

.field public final i:LX/I6W;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;LX/0SI;LX/I6X;LX/0tX;LX/0Sh;LX/03V;LX/2hU;LX/Bkv;LX/I6W;)V
    .locals 0
    .param p1    # Lcom/facebook/events/permalink/EventPermalinkFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2537579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2537580
    iput-object p2, p0, LX/I6T;->a:LX/0SI;

    .line 2537581
    iput-object p3, p0, LX/I6T;->b:LX/I6X;

    .line 2537582
    iput-object p4, p0, LX/I6T;->c:LX/0tX;

    .line 2537583
    iput-object p5, p0, LX/I6T;->d:LX/0Sh;

    .line 2537584
    iput-object p6, p0, LX/I6T;->e:LX/03V;

    .line 2537585
    iput-object p7, p0, LX/I6T;->f:LX/2hU;

    .line 2537586
    iput-object p8, p0, LX/I6T;->g:LX/Bkv;

    .line 2537587
    iput-object p1, p0, LX/I6T;->h:Lcom/facebook/events/permalink/EventPermalinkFragment;

    .line 2537588
    iput-object p9, p0, LX/I6T;->i:LX/I6W;

    .line 2537589
    return-void
.end method

.method public static a$redex0(LX/I6T;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Lcom/facebook/graphql/calls/EventStoryPinStatus;
        .end annotation
    .end param

    .prologue
    .line 2537590
    iget-object v0, p0, LX/I6T;->a:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2537591
    new-instance v1, LX/4EK;

    invoke-direct {v1}, LX/4EK;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    .line 2537592
    const-string v3, "event_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2537593
    move-object v1, v1

    .line 2537594
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 2537595
    const-string v3, "story_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2537596
    move-object v1, v1

    .line 2537597
    const-string v2, "pinned_status"

    invoke-virtual {v1, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2537598
    move-object v1, v1

    .line 2537599
    new-instance v2, LX/4EL;

    invoke-direct {v2}, LX/4EL;-><init>()V

    new-instance v3, LX/4EG;

    invoke-direct {v3}, LX/4EG;-><init>()V

    const-string v4, "event_permalink"

    invoke-virtual {v3, v4}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    move-result-object v2

    .line 2537600
    const-string v3, "context"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2537601
    move-object v1, v1

    .line 2537602
    iget-boolean v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v2, v2

    .line 2537603
    if-eqz v2, :cond_0

    .line 2537604
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2537605
    const-string v2, "actor_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2537606
    :cond_0
    new-instance v0, LX/I67;

    invoke-direct {v0}, LX/I67;-><init>()V

    move-object v0, v0

    .line 2537607
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2537608
    iget-object v1, p0, LX/I6T;->b:LX/I6X;

    invoke-virtual {v1, v0}, LX/I6X;->a(LX/0gW;)V

    .line 2537609
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2537610
    iget-object v1, p0, LX/I6T;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2537611
    iget-object v1, p0, LX/I6T;->d:LX/0Sh;

    new-instance v2, LX/I6R;

    invoke-direct {v2, p0, p1, p3, p2}, LX/I6R;-><init>(LX/I6T;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2537612
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2537572
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x403827a

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2537573
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    .line 2537574
    iget-object v2, p0, LX/I6T;->g:LX/Bkv;

    invoke-virtual {v2, v0}, LX/Bkv;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2537575
    new-instance v0, LX/31Y;

    invoke-direct {v0, p2}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v2, 0x7f083802

    invoke-virtual {v0, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v2, 0x7f083803

    invoke-virtual {v0, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v2, 0x7f080019

    new-instance v3, LX/I6S;

    invoke-direct {v3, p0, p1, p2}, LX/I6S;-><init>(LX/I6T;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080017

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2537576
    :goto_1
    return-void

    .line 2537577
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2537578
    :cond_1
    const-string v0, "PINNED"

    invoke-static {p0, p1, v0, p2}, LX/I6T;->a$redex0(LX/I6T;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2537568
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x403827a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2537569
    const-string v0, "NONE"

    invoke-static {p0, p1, v0, p2}, LX/I6T;->a$redex0(LX/I6T;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Landroid/content/Context;)V

    .line 2537570
    return-void

    .line 2537571
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
