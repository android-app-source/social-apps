.class public final LX/HNx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459095
    iput-object p1, p0, LX/HNx;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x5809e0ea

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2459096
    iget-object v0, p0, LX/HNx;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->m:LX/2Pb;

    sget-object v1, LX/8wW;->ENTER_FLOW:LX/8wW;

    iget-object v2, p0, LX/HNx;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    const-string v3, "edit_cta"

    invoke-virtual {v0, v1, v2, v3}, LX/2Pb;->a(LX/8wW;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459097
    iget-object v0, p0, LX/HNx;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->l:LX/GF2;

    iget-object v1, p0, LX/HNx;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    sget-object v2, LX/21D;->PAGE_FEED:LX/21D;

    const-string v3, "share_cta_from_edit"

    iget-object v4, p0, LX/HNx;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    const/16 v5, 0xd6

    .line 2459098
    new-instance v8, LX/4At;

    iget-object v9, v0, LX/GF2;->d:Landroid/content/Context;

    const v10, 0x7f0836aa

    invoke-direct {v8, v9, v10}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2459099
    invoke-virtual {v8}, LX/4At;->beginShowingProgress()V

    .line 2459100
    iget-object v9, v0, LX/GF2;->a:LX/1nC;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v9, v2, v3, v10, v11}, LX/1nC;->a(LX/21D;Ljava/lang/String;J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 2459101
    new-instance v10, LX/GF1;

    invoke-direct {v10, v0, v8, v5, v4}, LX/GF1;-><init>(LX/GF2;LX/4At;ILandroid/support/v4/app/Fragment;)V

    iget-object v8, v0, LX/GF2;->e:Ljava/util/concurrent/Executor;

    invoke-static {v9, v10, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2459102
    const v0, -0x6a8d2399

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
