.class public LX/IzL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/IzK;

.field public static final b:LX/IzK;

.field public static final c:LX/IzK;

.field public static final d:LX/IzK;

.field public static final e:LX/IzK;

.field public static final f:LX/IzK;

.field public static final g:LX/IzK;

.field public static final h:LX/IzK;

.field public static final i:LX/IzK;

.field public static final j:LX/IzK;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2634531
    new-instance v0, LX/IzK;

    const-string v1, "recent_all_includes_oldest_transaction"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->a:LX/IzK;

    .line 2634532
    new-instance v0, LX/IzK;

    const-string v1, "recent_incoming_includes_oldest_transaction"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->b:LX/IzK;

    .line 2634533
    new-instance v0, LX/IzK;

    const-string v1, "recent_outgoing_includes_oldest_transaction"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->c:LX/IzK;

    .line 2634534
    new-instance v0, LX/IzK;

    const-string v1, "incoming_requests_present"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->d:LX/IzK;

    .line 2634535
    new-instance v0, LX/IzK;

    const-string v1, "sync_token"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->e:LX/IzK;

    .line 2634536
    new-instance v0, LX/IzK;

    const-string v1, "last_sequence_id"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->f:LX/IzK;

    .line 2634537
    new-instance v0, LX/IzK;

    const-string v1, "sync_needs_full_refresh"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->g:LX/IzK;

    .line 2634538
    new-instance v0, LX/IzK;

    const-string v1, "full_refresh_reason"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->h:LX/IzK;

    .line 2634539
    new-instance v0, LX/IzK;

    const-string v1, "last_sync_full_refresh_ms"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->i:LX/IzK;

    .line 2634540
    new-instance v0, LX/IzK;

    const-string v1, "payment_account_enabled_status"

    invoke-direct {v0, v1}, LX/IzK;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IzL;->j:LX/IzK;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2634541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
