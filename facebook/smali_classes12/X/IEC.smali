.class public LX/IEC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2551641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2551642
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IEC;->b:Ljava/lang/String;

    .line 2551643
    iput-object p1, p0, LX/IEC;->a:LX/0Zb;

    .line 2551644
    return-void
.end method

.method public static a(LX/DHs;)LX/IEB;
    .locals 2

    .prologue
    .line 2551645
    sget-object v0, LX/IEA;->a:[I

    invoke-virtual {p0}, LX/DHs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2551646
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected value for FriendListType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2551647
    :pswitch_0
    sget-object v0, LX/IEB;->FRIEND_LIST_ALL_FRIENDS_SEEN:LX/IEB;

    .line 2551648
    :goto_0
    return-object v0

    .line 2551649
    :pswitch_1
    sget-object v0, LX/IEB;->FRIEND_LIST_MUTUAL_FRIENDS_SEEN:LX/IEB;

    goto :goto_0

    .line 2551650
    :pswitch_2
    sget-object v0, LX/IEB;->FRIEND_LIST_PYMK_SEEN:LX/IEB;

    goto :goto_0

    .line 2551651
    :pswitch_3
    sget-object v0, LX/IEB;->FRIEND_LIST_RECENTLY_ADDED_FRIENDS_SEEN:LX/IEB;

    goto :goto_0

    .line 2551652
    :pswitch_4
    sget-object v0, LX/IEB;->FRIEND_LIST_SUGGESTIONS_SEEN:LX/IEB;

    goto :goto_0

    .line 2551653
    :pswitch_5
    sget-object v0, LX/IEB;->FRIEND_LIST_WITH_NEW_POSTS_SEEN:LX/IEB;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/IEC;
    .locals 4

    .prologue
    .line 2551654
    const-class v1, LX/IEC;

    monitor-enter v1

    .line 2551655
    :try_start_0
    sget-object v0, LX/IEC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2551656
    sput-object v2, LX/IEC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2551657
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2551658
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2551659
    new-instance p0, LX/IEC;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/IEC;-><init>(LX/0Zb;)V

    .line 2551660
    move-object v0, p0

    .line 2551661
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2551662
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IEC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2551663
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2551664
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/IEC;LX/IEB;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2551665
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p1, LX/IEB;->analyticsName:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "profile_friends_page"

    .line 2551666
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2551667
    move-object v0, v0

    .line 2551668
    iget-object v1, p0, LX/IEC;->b:Ljava/lang/String;

    .line 2551669
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2551670
    move-object v0, v0

    .line 2551671
    const-string v1, "target_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method
