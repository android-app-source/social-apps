.class public LX/Iid;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2604987
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "omni_m/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2604988
    sput-object v0, LX/Iid;->a:LX/0Tn;

    const-string v1, "suggestions_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Iid;->b:LX/0Tn;

    .line 2604989
    sget-object v0, LX/Iid;->a:LX/0Tn;

    const-string v1, "ride_preference"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Iid;->c:LX/0Tn;

    .line 2604990
    sget-object v0, LX/Iid;->a:LX/0Tn;

    const-string v1, "nux_times_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Iid;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2604985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2604986
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2604984
    sget-object v0, LX/Iid;->b:LX/0Tn;

    sget-object v1, LX/Iid;->c:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
