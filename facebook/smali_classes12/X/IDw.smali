.class public final LX/IDw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/ID4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IDv;

.field public final synthetic b:LX/IDy;


# direct methods
.method public constructor <init>(LX/IDy;LX/IDv;)V
    .locals 0

    .prologue
    .line 2551305
    iput-object p1, p0, LX/IDw;->b:LX/IDy;

    iput-object p2, p0, LX/IDw;->a:LX/IDv;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2551306
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 2551307
    iget-object v0, p0, LX/IDw;->b:LX/IDy;

    iget-object v0, v0, LX/IDy;->e:LX/03V;

    const-string v1, "friendlist_discovery_entry_point_load_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2551308
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2551309
    check-cast p1, LX/ID4;

    .line 2551310
    iget-object v0, p0, LX/IDw;->b:LX/IDy;

    iget-object v0, v0, LX/IDy;->g:LX/IE7;

    iget-object v1, p0, LX/IDw;->a:LX/IDv;

    iget-object v2, p0, LX/IDw;->b:LX/IDy;

    iget-object v2, v2, LX/IDy;->b:Ljava/lang/String;

    iget-object v3, p0, LX/IDw;->b:LX/IDy;

    iget-object v3, v3, LX/IDy;->c:Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 2551311
    iget-object v4, v0, LX/IE7;->b:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, LX/IE7;->a:Landroid/content/res/Resources;

    const v6, 0x7f08383c

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-virtual {v1, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2551312
    iget-object v4, p1, LX/ID4;->a:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;

    move-object v4, v4

    .line 2551313
    if-eqz v4, :cond_1

    .line 2551314
    iget-object v4, p1, LX/ID4;->a:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;

    move-object v4, v4

    .line 2551315
    invoke-virtual {v4}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->a()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2551316
    iget-object v4, p1, LX/ID4;->a:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;

    move-object v4, v4

    .line 2551317
    invoke-virtual {v4}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->a()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;->a()I

    move-result v4

    .line 2551318
    :goto_1
    iget-object v6, v0, LX/IE7;->a:Landroid/content/res/Resources;

    const v7, 0x7f0f017b

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {v6, v7, v4, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2551319
    iget-object v4, p1, LX/ID4;->b:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointBucketFieldsModel;

    move-object v4, v4

    .line 2551320
    if-eqz v4, :cond_2

    .line 2551321
    iget-object v4, p1, LX/ID4;->b:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointBucketFieldsModel;

    move-object v4, v4

    .line 2551322
    invoke-virtual {v4}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointBucketFieldsModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2551323
    :goto_2
    new-instance v5, LX/IE3;

    invoke-direct {v5, v0, v2, v4}, LX/IE3;-><init>(LX/IE7;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, LX/IDv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2551324
    iget-object v0, p0, LX/IDw;->a:LX/IDv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/IDv;->setVisibility(I)V

    .line 2551325
    return-void

    .line 2551326
    :cond_0
    iget-object v4, v0, LX/IE7;->a:Landroid/content/res/Resources;

    const v6, 0x7f08383d

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v3, v7, v5

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_1
    move v4, v5

    .line 2551327
    goto :goto_1

    .line 2551328
    :cond_2
    const/4 v4, 0x0

    goto :goto_2
.end method
