.class public LX/IeD;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/2UR;

.field private final d:LX/3LP;

.field public final e:LX/3Lo;

.field public final f:LX/2Iv;

.field private final g:LX/3Mp;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/2RQ;


# direct methods
.method public constructor <init>(LX/0Zr;LX/2UR;LX/3LP;LX/3Lo;LX/2Iv;LX/3Mp;LX/0Or;LX/2RQ;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAMessengerOnlyUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zr;",
            "LX/2UR;",
            "LX/3LP;",
            "LX/3Lo;",
            "LX/2Iv;",
            "LX/3Mp;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2RQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598483
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 2598484
    iput-object p2, p0, LX/IeD;->c:LX/2UR;

    .line 2598485
    iput-object p3, p0, LX/IeD;->d:LX/3LP;

    .line 2598486
    iput-object p4, p0, LX/IeD;->e:LX/3Lo;

    .line 2598487
    iput-object p5, p0, LX/IeD;->f:LX/2Iv;

    .line 2598488
    iput-object p6, p0, LX/IeD;->g:LX/3Mp;

    .line 2598489
    iput-object p7, p0, LX/IeD;->h:LX/0Or;

    .line 2598490
    iput-object p8, p0, LX/IeD;->i:LX/2RQ;

    .line 2598491
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2598399
    iget-object v0, p0, LX/IeD;->c:LX/2UR;

    const/16 v1, 0x1e

    invoke-virtual {v0, p1, v1}, LX/2UR;->a(Ljava/lang/String;I)LX/6N9;

    move-result-object v3

    .line 2598400
    :cond_0
    :try_start_0
    invoke-virtual {v3}, LX/0Rr;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2598401
    invoke-virtual {v3}, LX/0Rr;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2598402
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2598403
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserPhoneNumber;

    .line 2598404
    new-instance v6, LX/0XI;

    invoke-direct {v6}, LX/0XI;-><init>()V

    invoke-virtual {v6, v0}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v6

    .line 2598405
    iget-object v7, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v7, v7

    .line 2598406
    iget-object v8, v1, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v8, v8

    .line 2598407
    invoke-virtual {v6, v7, v8}, LX/0XI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;

    move-result-object v6

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2598408
    iput-object v1, v6, LX/0XI;->d:Ljava/util/List;

    .line 2598409
    move-object v1, v6

    .line 2598410
    iget-object v6, v0, Lcom/facebook/user/model/User;->l:Ljava/lang/String;

    move-object v6, v6

    .line 2598411
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2598412
    iget-object v6, v0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v6, v6

    .line 2598413
    invoke-virtual {v6}, Lcom/facebook/user/model/Name;->b()Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v6}, Lcom/facebook/user/model/Name;->d()Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v6}, Lcom/facebook/user/model/Name;->h()Z

    move-result v6

    if-nez v6, :cond_4

    .line 2598414
    const/4 v6, 0x0

    .line 2598415
    :goto_1
    move-object v6, v6

    .line 2598416
    iput-object v6, v1, LX/0XI;->s:Ljava/lang/String;

    .line 2598417
    :cond_1
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 2598418
    iget-object v6, p0, LX/IeD;->g:LX/3Mp;

    invoke-virtual {v6, v0}, LX/3Mp;->a(Lcom/facebook/user/model/User;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2598419
    iget-object v6, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v6, v6

    .line 2598420
    invoke-interface {p2, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2598421
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2598422
    :cond_3
    invoke-virtual {v3}, LX/6N9;->c()V

    .line 2598423
    return-void

    .line 2598424
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, LX/6N9;->c()V

    throw v0

    .line 2598425
    :cond_4
    new-instance v6, LX/3hD;

    invoke-direct {v6}, LX/3hD;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v7

    .line 2598426
    iput-object v7, v6, LX/3hD;->b:Ljava/lang/String;

    .line 2598427
    move-object v6, v6

    .line 2598428
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v7

    .line 2598429
    iput-object v7, v6, LX/3hD;->a:Ljava/lang/String;

    .line 2598430
    move-object v6, v6

    .line 2598431
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->h()Ljava/lang/String;

    move-result-object v7

    .line 2598432
    iput-object v7, v6, LX/3hD;->c:Ljava/lang/String;

    .line 2598433
    move-object v6, v6

    .line 2598434
    invoke-virtual {v6}, LX/3hD;->a()LX/3hE;

    move-result-object v6

    .line 2598435
    iget-object v7, p0, LX/IeD;->e:LX/3Lo;

    iget-object v8, p0, LX/IeD;->f:LX/2Iv;

    invoke-virtual {v8}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, LX/3Lo;->a(Landroid/database/sqlite/SQLiteDatabase;LX/3hE;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method private b(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2598462
    iget-object v0, p0, LX/IeD;->i:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    .line 2598463
    iput-object p1, v0, LX/2RR;->e:Ljava/lang/String;

    .line 2598464
    move-object v0, v0

    .line 2598465
    sget-object v1, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 2598466
    iput-object v1, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 2598467
    move-object v0, v0

    .line 2598468
    iput-boolean v2, v0, LX/2RR;->f:Z

    .line 2598469
    move-object v0, v0

    .line 2598470
    iput-boolean v2, v0, LX/2RR;->h:Z

    .line 2598471
    move-object v0, v0

    .line 2598472
    invoke-virtual {v0}, LX/2RR;->i()LX/2RR;

    move-result-object v0

    const/16 v1, 0x1e

    .line 2598473
    iput v1, v0, LX/2RR;->p:I

    .line 2598474
    move-object v0, v0

    .line 2598475
    iget-object v1, p0, LX/IeD;->d:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 2598476
    if-eqz v1, :cond_1

    .line 2598477
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598478
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2598479
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2598480
    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2598481
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, LX/3On;->close()V

    .line 2598482
    :cond_1
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 6

    .prologue
    .line 2598436
    new-instance v2, LX/39y;

    invoke-direct {v2}, LX/39y;-><init>()V

    .line 2598437
    if-eqz p1, :cond_3

    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2598438
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2598439
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2598440
    invoke-direct {p0, v1, v3}, LX/IeD;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2598441
    iget-object v0, p0, LX/IeD;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2598442
    invoke-direct {p0, v1, v3}, LX/IeD;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 2598443
    :cond_0
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2598444
    new-instance v1, LX/IeC;

    invoke-direct {v1, p0}, LX/IeC;-><init>(LX/IeD;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2598445
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2598446
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 2598447
    iget-object v5, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v5, v3}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v3

    .line 2598448
    if-eqz v3, :cond_1

    .line 2598449
    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2598450
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2598451
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    .line 2598452
    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 2598453
    iget v1, v0, LX/3Og;->d:I

    move v0, v1

    .line 2598454
    iput v0, v2, LX/39y;->b:I

    .line 2598455
    :goto_2
    return-object v2

    .line 2598456
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto :goto_0

    .line 2598457
    :cond_4
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 2598458
    const/4 v0, -0x1

    iput v0, v2, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2598459
    :catch_0
    move-exception v0

    .line 2598460
    const-string v1, "orca:ContactPickerCombinedContactsFilter"

    const-string v2, "exception while filtering"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2598461
    throw v0
.end method
