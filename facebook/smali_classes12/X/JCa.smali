.class public LX/JCa;
.super Landroid/text/method/LinkMovementMethod;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/JCa;


# instance fields
.field private a:Landroid/text/style/BackgroundColorSpan;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2662546
    invoke-direct {p0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    .line 2662547
    return-void
.end method

.method public static a(LX/0QB;)LX/JCa;
    .locals 3

    .prologue
    .line 2662548
    sget-object v0, LX/JCa;->b:LX/JCa;

    if-nez v0, :cond_1

    .line 2662549
    const-class v1, LX/JCa;

    monitor-enter v1

    .line 2662550
    :try_start_0
    sget-object v0, LX/JCa;->b:LX/JCa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2662551
    if-eqz v2, :cond_0

    .line 2662552
    :try_start_1
    new-instance v0, LX/JCa;

    invoke-direct {v0}, LX/JCa;-><init>()V

    .line 2662553
    move-object v0, v0

    .line 2662554
    sput-object v0, LX/JCa;->b:LX/JCa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2662555
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2662556
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2662557
    :cond_1
    sget-object v0, LX/JCa;->b:LX/JCa;

    return-object v0

    .line 2662558
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2662559
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;)Landroid/text/style/BackgroundColorSpan;
    .locals 3

    .prologue
    .line 2662560
    iget-object v0, p0, LX/JCa;->a:Landroid/text/style/BackgroundColorSpan;

    if-nez v0, :cond_0

    .line 2662561
    new-instance v0, Landroid/text/style/BackgroundColorSpan;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a055b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    iput-object v0, p0, LX/JCa;->a:Landroid/text/style/BackgroundColorSpan;

    .line 2662562
    :cond_0
    iget-object v0, p0, LX/JCa;->a:Landroid/text/style/BackgroundColorSpan;

    return-object v0
.end method

.method private a(Landroid/text/Spannable;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2662563
    invoke-direct {p0, p2}, LX/JCa;->a(Landroid/content/Context;)Landroid/text/style/BackgroundColorSpan;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2662564
    return-void
.end method


# virtual methods
.method public final onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 2662565
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    .line 2662566
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 2662567
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 2662568
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v4

    sub-int/2addr v1, v4

    .line 2662569
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    .line 2662570
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v4

    add-int/2addr v1, v4

    .line 2662571
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v4

    add-int/2addr v3, v4

    .line 2662572
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    .line 2662573
    invoke-virtual {v4, v3}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v3

    .line 2662574
    invoke-virtual {v4, v3}, Landroid/text/Layout;->getLineRight(I)F

    move-result v5

    int-to-float p2, v1

    cmpg-float v5, v5, p2

    if-gez v5, :cond_5

    .line 2662575
    const/4 v1, -0x1

    .line 2662576
    :goto_0
    move v3, v1

    .line 2662577
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    const-class v4, Landroid/text/style/ClickableSpan;

    invoke-interface {v1, v3, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    .line 2662578
    array-length v3, v1

    if-lez v3, :cond_4

    const/4 v3, 0x0

    aget-object v1, v1, v3

    :goto_1
    move-object v3, v1

    .line 2662579
    if-eqz v3, :cond_2

    move v1, v2

    .line 2662580
    :goto_2
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 2662581
    const/4 v5, 0x1

    .line 2662582
    const/4 p2, 0x3

    if-ne v4, p2, :cond_6

    move v1, v5

    .line 2662583
    :cond_0
    :goto_3
    move v1, v1

    .line 2662584
    if-eqz v1, :cond_3

    .line 2662585
    packed-switch v4, :pswitch_data_0

    .line 2662586
    :cond_1
    :goto_4
    return v2

    .line 2662587
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 2662588
    :pswitch_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/JCa;->a(Landroid/text/Spannable;Landroid/content/Context;)V

    .line 2662589
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, LX/JCa;->a(Landroid/content/Context;)Landroid/text/style/BackgroundColorSpan;

    move-result-object v1

    invoke-interface {v0, v3}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0, v3}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v3

    const/16 v5, 0x21

    invoke-interface {v0, v1, v4, v3, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_4

    .line 2662590
    :pswitch_1
    invoke-virtual {v3, p1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    .line 2662591
    :pswitch_2
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/JCa;->a(Landroid/text/Spannable;Landroid/content/Context;)V

    goto :goto_4

    .line 2662592
    :cond_3
    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2662593
    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p3}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    int-to-float v1, v1

    invoke-virtual {v4, v3, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v1

    goto :goto_0

    .line 2662594
    :cond_6
    const/4 p2, 0x2

    if-ne v4, p2, :cond_0

    .line 2662595
    if-nez v1, :cond_7

    move v1, v5

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
