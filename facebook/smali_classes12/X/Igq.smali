.class public final LX/Igq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/PhotoItemController;)V
    .locals 0

    .prologue
    .line 2601761
    iput-object p1, p0, LX/Igq;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x4389db8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2601762
    iget-object v1, p0, LX/Igq;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->w:LX/Igl;

    if-eqz v1, :cond_0

    .line 2601763
    iget-object v1, p0, LX/Igq;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->x:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2601764
    iget-object v1, p0, LX/Igq;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->w:LX/Igl;

    iget-object v2, p0, LX/Igq;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->x:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601765
    iget-object p0, v1, LX/Igl;->a:LX/Ign;

    iget-object p0, p0, LX/Ign;->i:LX/Igc;

    if-eqz p0, :cond_0

    .line 2601766
    iget-object p0, v1, LX/Igl;->a:LX/Ign;

    iget-object p0, p0, LX/Ign;->i:LX/Igc;

    invoke-virtual {p0, v2}, LX/Igc;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2601767
    :cond_0
    const v1, 0x271158e3

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
