.class public final enum LX/IVo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IVo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IVo;

.field public static final enum ACTION_BAR:LX/IVo;

.field public static final enum ARCHIVED:LX/IVo;

.field public static final enum BROWSE_CATEGORIES_BAR:LX/IVo;

.field public static final enum CHILD_GROUPS:LX/IVo;

.field public static final enum CLIENT_HEADER_COMPONENT:LX/IVo;

.field public static final enum COMMUNITY_DESCRIPTION:LX/IVo;

.field public static final enum COMMUNITY_EMAIL_VERIFICATION:LX/IVo;

.field public static final enum COMPOSER_BAR:LX/IVo;

.field public static final enum COVER_HEADER:LX/IVo;

.field public static final enum DESCRIPTION:LX/IVo;

.field public static final enum DISCUSSION_TOPICS_BAR:LX/IVo;

.field public static final enum EMPTY_COMMUNITY:LX/IVo;

.field public static final enum FOR_SALE_POSTS_BAR:LX/IVo;

.field public static final enum JOIN:LX/IVo;

.field public static final enum LEARNING_TAB_BAR:LX/IVo;

.field public static final enum LOADING_BAR:LX/IVo;

.field public static final enum MULIT_COMPANY_INVITE_REDEMPTION:LX/IVo;

.field public static final enum QUESTIONS:LX/IVo;

.field public static final enum QUICK_PROMOTION:LX/IVo;

.field public static final enum RELIABLE_EXPECTATIONS:LX/IVo;

.field public static final enum SUGGESTIONS_CARD:LX/IVo;

.field public static final enum SUGGESTIONS_CHAIN:LX/IVo;

.field public static final enum THIN_BORDER:LX/IVo;

.field public static final enum WEATHER:LX/IVo;

.field public static final enum YOUR_SALE_POST:LX/IVo;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2582598
    new-instance v0, LX/IVo;

    const-string v1, "COVER_HEADER"

    invoke-direct {v0, v1, v3}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->COVER_HEADER:LX/IVo;

    .line 2582599
    new-instance v0, LX/IVo;

    const-string v1, "ACTION_BAR"

    invoke-direct {v0, v1, v4}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->ACTION_BAR:LX/IVo;

    .line 2582600
    new-instance v0, LX/IVo;

    const-string v1, "MULIT_COMPANY_INVITE_REDEMPTION"

    invoke-direct {v0, v1, v5}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->MULIT_COMPANY_INVITE_REDEMPTION:LX/IVo;

    .line 2582601
    new-instance v0, LX/IVo;

    const-string v1, "JOIN"

    invoke-direct {v0, v1, v6}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->JOIN:LX/IVo;

    .line 2582602
    new-instance v0, LX/IVo;

    const-string v1, "SUGGESTIONS_CHAIN"

    invoke-direct {v0, v1, v7}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->SUGGESTIONS_CHAIN:LX/IVo;

    .line 2582603
    new-instance v0, LX/IVo;

    const-string v1, "SUGGESTIONS_CARD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->SUGGESTIONS_CARD:LX/IVo;

    .line 2582604
    new-instance v0, LX/IVo;

    const-string v1, "DESCRIPTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->DESCRIPTION:LX/IVo;

    .line 2582605
    new-instance v0, LX/IVo;

    const-string v1, "COMMUNITY_DESCRIPTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->COMMUNITY_DESCRIPTION:LX/IVo;

    .line 2582606
    new-instance v0, LX/IVo;

    const-string v1, "COMMUNITY_EMAIL_VERIFICATION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->COMMUNITY_EMAIL_VERIFICATION:LX/IVo;

    .line 2582607
    new-instance v0, LX/IVo;

    const-string v1, "LEARNING_TAB_BAR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->LEARNING_TAB_BAR:LX/IVo;

    .line 2582608
    new-instance v0, LX/IVo;

    const-string v1, "DISCUSSION_TOPICS_BAR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->DISCUSSION_TOPICS_BAR:LX/IVo;

    .line 2582609
    new-instance v0, LX/IVo;

    const-string v1, "COMPOSER_BAR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->COMPOSER_BAR:LX/IVo;

    .line 2582610
    new-instance v0, LX/IVo;

    const-string v1, "YOUR_SALE_POST"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->YOUR_SALE_POST:LX/IVo;

    .line 2582611
    new-instance v0, LX/IVo;

    const-string v1, "FOR_SALE_POSTS_BAR"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->FOR_SALE_POSTS_BAR:LX/IVo;

    .line 2582612
    new-instance v0, LX/IVo;

    const-string v1, "BROWSE_CATEGORIES_BAR"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->BROWSE_CATEGORIES_BAR:LX/IVo;

    .line 2582613
    new-instance v0, LX/IVo;

    const-string v1, "LOADING_BAR"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->LOADING_BAR:LX/IVo;

    .line 2582614
    new-instance v0, LX/IVo;

    const-string v1, "ARCHIVED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->ARCHIVED:LX/IVo;

    .line 2582615
    new-instance v0, LX/IVo;

    const-string v1, "RELIABLE_EXPECTATIONS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->RELIABLE_EXPECTATIONS:LX/IVo;

    .line 2582616
    new-instance v0, LX/IVo;

    const-string v1, "CLIENT_HEADER_COMPONENT"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->CLIENT_HEADER_COMPONENT:LX/IVo;

    .line 2582617
    new-instance v0, LX/IVo;

    const-string v1, "EMPTY_COMMUNITY"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->EMPTY_COMMUNITY:LX/IVo;

    .line 2582618
    new-instance v0, LX/IVo;

    const-string v1, "THIN_BORDER"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->THIN_BORDER:LX/IVo;

    .line 2582619
    new-instance v0, LX/IVo;

    const-string v1, "QUICK_PROMOTION"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->QUICK_PROMOTION:LX/IVo;

    .line 2582620
    new-instance v0, LX/IVo;

    const-string v1, "WEATHER"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->WEATHER:LX/IVo;

    .line 2582621
    new-instance v0, LX/IVo;

    const-string v1, "QUESTIONS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->QUESTIONS:LX/IVo;

    .line 2582622
    new-instance v0, LX/IVo;

    const-string v1, "CHILD_GROUPS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/IVo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVo;->CHILD_GROUPS:LX/IVo;

    .line 2582623
    const/16 v0, 0x19

    new-array v0, v0, [LX/IVo;

    sget-object v1, LX/IVo;->COVER_HEADER:LX/IVo;

    aput-object v1, v0, v3

    sget-object v1, LX/IVo;->ACTION_BAR:LX/IVo;

    aput-object v1, v0, v4

    sget-object v1, LX/IVo;->MULIT_COMPANY_INVITE_REDEMPTION:LX/IVo;

    aput-object v1, v0, v5

    sget-object v1, LX/IVo;->JOIN:LX/IVo;

    aput-object v1, v0, v6

    sget-object v1, LX/IVo;->SUGGESTIONS_CHAIN:LX/IVo;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/IVo;->SUGGESTIONS_CARD:LX/IVo;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IVo;->DESCRIPTION:LX/IVo;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IVo;->COMMUNITY_DESCRIPTION:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/IVo;->COMMUNITY_EMAIL_VERIFICATION:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/IVo;->LEARNING_TAB_BAR:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/IVo;->DISCUSSION_TOPICS_BAR:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/IVo;->COMPOSER_BAR:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/IVo;->YOUR_SALE_POST:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/IVo;->FOR_SALE_POSTS_BAR:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/IVo;->BROWSE_CATEGORIES_BAR:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/IVo;->LOADING_BAR:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/IVo;->ARCHIVED:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/IVo;->RELIABLE_EXPECTATIONS:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/IVo;->CLIENT_HEADER_COMPONENT:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/IVo;->EMPTY_COMMUNITY:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/IVo;->THIN_BORDER:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/IVo;->QUICK_PROMOTION:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/IVo;->WEATHER:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/IVo;->QUESTIONS:LX/IVo;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/IVo;->CHILD_GROUPS:LX/IVo;

    aput-object v2, v0, v1

    sput-object v0, LX/IVo;->$VALUES:[LX/IVo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2582624
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IVo;
    .locals 1

    .prologue
    .line 2582625
    const-class v0, LX/IVo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IVo;

    return-object v0
.end method

.method public static values()[LX/IVo;
    .locals 1

    .prologue
    .line 2582626
    sget-object v0, LX/IVo;->$VALUES:[LX/IVo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IVo;

    return-object v0
.end method
