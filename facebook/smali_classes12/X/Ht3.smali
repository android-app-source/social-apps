.class public LX/Ht3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2515358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2515359
    iput-object p1, p0, LX/Ht3;->a:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    .line 2515360
    return-void
.end method

.method public static a(LX/0QB;)LX/Ht3;
    .locals 4

    .prologue
    .line 2515361
    const-class v1, LX/Ht3;

    monitor-enter v1

    .line 2515362
    :try_start_0
    sget-object v0, LX/Ht3;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2515363
    sput-object v2, LX/Ht3;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2515364
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2515365
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2515366
    new-instance p0, LX/Ht3;

    invoke-static {v0}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->b(LX/0QB;)Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-direct {p0, v3}, LX/Ht3;-><init>(Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;)V

    .line 2515367
    move-object v0, p0

    .line 2515368
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2515369
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ht3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2515370
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2515371
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
