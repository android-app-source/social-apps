.class public LX/JKg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5pc;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/JKg;


# instance fields
.field public final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/fbreact/interfaces/ListenableReactBridgeStartup$ReactBridgeStartupListener;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0So;

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(LX/0So;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2680500
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/JKg;->a:Ljava/util/Collection;

    .line 2680501
    iput-object p1, p0, LX/JKg;->b:LX/0So;

    .line 2680502
    iput-object p2, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2680503
    return-void
.end method

.method public static a(LX/0QB;)LX/JKg;
    .locals 5

    .prologue
    .line 2680455
    sget-object v0, LX/JKg;->d:LX/JKg;

    if-nez v0, :cond_1

    .line 2680456
    const-class v1, LX/JKg;

    monitor-enter v1

    .line 2680457
    :try_start_0
    sget-object v0, LX/JKg;->d:LX/JKg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2680458
    if-eqz v2, :cond_0

    .line 2680459
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2680460
    new-instance p0, LX/JKg;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3, v4}, LX/JKg;-><init>(LX/0So;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 2680461
    move-object v0, p0

    .line 2680462
    sput-object v0, LX/JKg;->d:LX/JKg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2680463
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2680464
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2680465
    :cond_1
    sget-object v0, LX/JKg;->d:LX/JKg;

    return-object v0

    .line 2680466
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2680467
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const v5, 0x770004

    const v4, 0x770003

    const v8, 0x770002

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 2680468
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2680469
    :cond_1
    :goto_1
    return-void

    .line 2680470
    :sswitch_0
    const-string v3, "CREATE_REACT_CONTEXT_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v3, "CREATE_REACT_CONTEXT_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "PROCESS_PACKAGES_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_3
    const-string v3, "PROCESS_PACKAGES_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "BUILD_NATIVE_MODULE_REGISTRY_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "BUILD_NATIVE_MODULE_REGISTRY_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "BUILD_JS_MODULE_CONFIG_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "BUILD_JS_MODULE_CONFIG_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v3, "CREATE_CATALYST_INSTANCE_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v3, "CREATE_CATALYST_INSTANCE_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v3, "RUN_JS_BUNDLE_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v3, "RUN_JS_BUNDLE_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v3, "NativeModule_start"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v3, "NativeModule_end"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v3, "SETUP_REACT_CONTEXT_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v3, "SETUP_REACT_CONTEXT_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v3, "CREATE_UI_MANAGER_MODULE_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v3, "CREATE_UI_MANAGER_MODULE_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v3, "CREATE_VIEW_MANAGERS_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v3, "CREATE_VIEW_MANAGERS_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v3, "CREATE_UI_MANAGER_MODULE_CONSTANTS_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v3, "CREATE_UI_MANAGER_MODULE_CONSTANTS_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v3, "CREATE_MODULE_START"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v3, "CREATE_MODULE_END"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x17

    goto/16 :goto_0

    .line 2680471
    :pswitch_0
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, p0, LX/JKg;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-interface {v0, v8, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    goto/16 :goto_1

    .line 2680472
    :pswitch_1
    iget-object v0, p0, LX/JKg;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 2680473
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v8, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(II)J

    move-result-wide v6

    .line 2680474
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v8, v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ISJ)V

    .line 2680475
    iget-object v0, p0, LX/JKg;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JK6;

    .line 2680476
    invoke-virtual {v0, v6, v7, v4, v5}, LX/JK6;->a(JJ)V

    goto :goto_2

    .line 2680477
    :pswitch_2
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680478
    :pswitch_3
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680479
    :pswitch_4
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680480
    :pswitch_5
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680481
    :pswitch_6
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770005

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680482
    :pswitch_7
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770005

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680483
    :pswitch_8
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770006

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680484
    :pswitch_9
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770006

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680485
    :pswitch_a
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770007

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680486
    :pswitch_b
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770007

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680487
    :pswitch_c
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770008

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680488
    :pswitch_d
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770008

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680489
    :pswitch_e
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770009

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680490
    :pswitch_f
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770009

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680491
    :pswitch_10
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x77000a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680492
    :pswitch_11
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x77000a

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680493
    :pswitch_12
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x77000b

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680494
    :pswitch_13
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x77000b

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680495
    :pswitch_14
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x77000c

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680496
    :pswitch_15
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x77000c

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    .line 2680497
    :pswitch_16
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770010

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto/16 :goto_1

    .line 2680498
    :pswitch_17
    iget-object v0, p0, LX/JKg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x770010

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x730211e4 -> :sswitch_4
        -0x5b0fd127 -> :sswitch_14
        -0x47fd8158 -> :sswitch_1
        -0x47b3b5a0 -> :sswitch_2
        -0x44e60a37 -> :sswitch_f
        -0x3dda8911 -> :sswitch_0
        -0x386417a5 -> :sswitch_8
        -0x317ce8ee -> :sswitch_16
        -0x300685a7 -> :sswitch_3
        0xcfe5b14 -> :sswitch_9
        0xe6b1d60 -> :sswitch_b
        0x12a7bd13 -> :sswitch_6
        0x20e147a7 -> :sswitch_a
        0x25de42a6 -> :sswitch_c
        0x2f0e2912 -> :sswitch_15
        0x4c8f9239 -> :sswitch_13
        0x51114ce2 -> :sswitch_11
        0x52b99ea9 -> :sswitch_10
        0x5d3ba9d0 -> :sswitch_e
        0x66f8dd15 -> :sswitch_5
        0x67bbea40 -> :sswitch_12
        0x6be7fdcc -> :sswitch_7
        0x6e7fcb8b -> :sswitch_17
        0x7fe7c81f -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method
