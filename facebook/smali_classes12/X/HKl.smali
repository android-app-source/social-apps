.class public LX/HKl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HKj;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HKm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2455043
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/HKl;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HKm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455040
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2455041
    iput-object p1, p0, LX/HKl;->b:LX/0Ot;

    .line 2455042
    return-void
.end method

.method public static a(LX/1De;Ljava/lang/String;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/String;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2455044
    const v0, -0x42a1681c

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/HKl;
    .locals 4

    .prologue
    .line 2455029
    const-class v1, LX/HKl;

    monitor-enter v1

    .line 2455030
    :try_start_0
    sget-object v0, LX/HKl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2455031
    sput-object v2, LX/HKl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2455032
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455033
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2455034
    new-instance v3, LX/HKl;

    const/16 p0, 0x2bd6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HKl;-><init>(LX/0Ot;)V

    .line 2455035
    move-object v0, v3

    .line 2455036
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2455037
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2455038
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2455039
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2455026
    check-cast p2, LX/HKk;

    .line 2455027
    iget-object v0, p0, LX/HKl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HKm;

    iget-object v2, p2, LX/HKk;->a:Ljava/lang/String;

    iget-object v3, p2, LX/HKk;->b:Ljava/lang/String;

    iget-object v4, p2, LX/HKk;->c:Ljava/lang/String;

    iget-object v5, p2, LX/HKk;->d:Ljava/lang/String;

    iget-boolean v6, p2, LX/HKk;->e:Z

    iget-boolean v7, p2, LX/HKk;->f:Z

    iget-object v8, p2, LX/HKk;->g:LX/0Px;

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, LX/HKm;->a(LX/1De;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLX/0Px;)LX/1Dg;

    move-result-object v0

    .line 2455028
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2455014
    invoke-static {}, LX/1dS;->b()V

    .line 2455015
    iget v0, p1, LX/1dQ;->b:I

    .line 2455016
    packed-switch v0, :pswitch_data_0

    .line 2455017
    :goto_0
    return-object v3

    .line 2455018
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2455019
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/String;

    .line 2455020
    iget-object p1, p0, LX/HKl;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2455021
    new-instance p1, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const/4 v2, 0x1

    invoke-direct {p1, p0, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2455022
    invoke-virtual {p1, v0}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2455023
    sget-object p0, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {p1, p0}, LX/0ht;->a(LX/3AV;)V

    .line 2455024
    invoke-virtual {p1, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2455025
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x42a1681c
        :pswitch_0
    .end packed-switch
.end method
