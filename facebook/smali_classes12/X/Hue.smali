.class public LX/Hue;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/AQ7;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/Hub;


# direct methods
.method public constructor <init>(LX/0Ot;LX/Hub;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/AQ7;",
            ">;>;",
            "LX/Hub;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517801
    iput-object p1, p0, LX/Hue;->a:LX/0Ot;

    .line 2517802
    iput-object p2, p0, LX/Hue;->b:LX/Hub;

    .line 2517803
    return-void
.end method

.method private a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)LX/AQ7;
    .locals 4
    .param p1    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2517804
    if-nez p1, :cond_0

    .line 2517805
    iget-object v0, p0, LX/Hue;->b:LX/Hub;

    .line 2517806
    :goto_0
    return-object v0

    .line 2517807
    :cond_0
    iget-object v0, p0, LX/Hue;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ7;

    .line 2517808
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, LX/88e;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 2517809
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ComposerPluginRegistry: factory not found for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - did you forget to multi-bind it?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V
    .locals 5
    .param p1    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2517810
    if-nez p1, :cond_1

    .line 2517811
    :cond_0
    return-void

    .line 2517812
    :cond_1
    const/4 v1, 0x0

    .line 2517813
    iget-object v0, p0, LX/Hue;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ7;

    .line 2517814
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/88e;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2517815
    if-nez v1, :cond_3

    move-object v1, v0

    .line 2517816
    goto :goto_0

    .line 2517817
    :cond_3
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ComposerPluginRegistry.verifyNoDuplicates: duplicate ComposerPlugin.Factory for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " and "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 1
    .param p1    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/B5f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 2517818
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2517819
    if-eqz v0, :cond_0

    .line 2517820
    invoke-direct {p0, p1}, LX/Hue;->b(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V

    .line 2517821
    :cond_0
    invoke-direct {p0, p1}, LX/Hue;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)LX/AQ7;

    move-result-object v0

    .line 2517822
    invoke-interface {v0, p1, p2, p3}, LX/AQ7;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    return-object v0
.end method
