.class public LX/Iv5;
.super LX/Iur;
.source ""


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2627887
    invoke-direct {p0, p1}, LX/Iur;-><init>(Landroid/content/Context;)V

    .line 2627888
    invoke-virtual {p0}, LX/Iv5;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2627889
    const v1, 0x7f0300eb

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    .line 2627890
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/Iv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0687

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/Iv5;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2627891
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Iv5;->setOrientation(I)V

    .line 2627892
    iget-object v0, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0d054e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Iv5;->b:Landroid/widget/ImageView;

    .line 2627893
    iget-object v0, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0343

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Iv5;->c:Landroid/widget/ImageView;

    .line 2627894
    iget-object v0, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0d02c4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Iv5;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2627895
    iget-object v0, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0d02c3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Iv5;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2627896
    iget-object v0, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0d054f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Iv5;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2627897
    iget-object v0, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0550

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Iv5;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2627898
    iget-object v0, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0551

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Iv5;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2627899
    return-void
.end method


# virtual methods
.method public final a(LX/HjF;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2627858
    iget-object v0, p0, LX/Iv5;->b:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, LX/Iur;->a(Landroid/view/View;)V

    .line 2627859
    invoke-static {p1}, LX/HjF;->m(LX/HjF;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    .line 2627860
    iget-object v2, p0, LX/Iv5;->c:Landroid/widget/ImageView;

    invoke-static {v0, v2}, LX/HjF;->a(LX/Hj9;Landroid/widget/ImageView;)V

    .line 2627861
    invoke-virtual {p1}, LX/HjF;->b()LX/Hj9;

    move-result-object v0

    .line 2627862
    iget-object v2, p0, LX/Iv5;->b:Landroid/widget/ImageView;

    invoke-static {v0, v2}, LX/HjF;->a(LX/Hj9;Landroid/widget/ImageView;)V

    .line 2627863
    iget-object v0, p0, LX/Iv5;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, LX/HjF;->m(LX/HjF;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    .line 2627864
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2627865
    invoke-static {p1}, LX/HjF;->m(LX/HjF;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_2
    move-object v3, v0

    .line 2627866
    invoke-static {p1}, LX/HjF;->m(LX/HjF;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x0

    :goto_3
    move-object v0, v0

    .line 2627867
    if-nez v0, :cond_1

    move v0, v1

    .line 2627868
    :goto_4
    invoke-virtual {p0}, LX/Iv5;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0838c0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2627869
    if-nez v0, :cond_3

    .line 2627870
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2627871
    const-string v0, "%s %s %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v1

    invoke-virtual {p0}, LX/Iv5;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0838b2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v6

    const/4 v2, 0x2

    aput-object v3, v4, v2

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2627872
    :goto_5
    iget-object v2, p0, LX/Iv5;->e:Lcom/facebook/resources/ui/FbTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2627873
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2627874
    iget-object v2, p0, LX/Iv5;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2627875
    iget-object v0, p0, LX/Iv5;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2627876
    :cond_0
    iget-object v0, p0, LX/Iv5;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, LX/HjF;->m(LX/HjF;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x0

    :goto_6
    move-object v2, v2

    .line 2627877
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2627878
    iget-object v0, p0, LX/Iv5;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, LX/HjF;->m(LX/HjF;)Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x0

    :goto_7
    move-object v2, v2

    .line 2627879
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2627880
    iget-object v0, p0, LX/Iv5;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/Iv5;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0838c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2627881
    new-array v0, v6, [Landroid/view/View;

    iget-object v2, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    aput-object v2, v0, v1

    .line 2627882
    iget-object v1, p0, LX/Iv5;->a:Landroid/widget/LinearLayout;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/HjF;->a(Landroid/view/View;Ljava/util/List;)V

    .line 2627883
    return-void

    .line 2627884
    :cond_1
    iget-boolean v2, v0, LX/Hk3;->a:Z

    move v0, v2

    .line 2627885
    goto/16 :goto_4

    :cond_2
    move-object v0, v2

    .line 2627886
    goto :goto_5

    :cond_3
    move-object v0, v3

    goto :goto_5

    :cond_4
    iget-object v0, p1, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v0}, LX/Hjj;->h()LX/Hj9;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    iget-object v2, p1, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v2}, LX/Hjj;->j()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_6
    iget-object v0, p1, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v0}, LX/Hjj;->k()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_7
    iget-object v0, p1, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v0}, LX/Hjj;->r()LX/Hk3;

    move-result-object v0

    goto/16 :goto_3

    :cond_8
    iget-object v2, p1, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v2}, LX/Hjj;->m()Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    :cond_9
    iget-object v2, p1, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v2}, LX/Hjj;->l()Ljava/lang/String;

    move-result-object v2

    goto :goto_7
.end method
