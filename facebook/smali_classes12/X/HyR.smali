.class public final LX/HyR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hx6;

.field public final synthetic b:LX/HyT;


# direct methods
.method public constructor <init>(LX/HyT;LX/Hx6;)V
    .locals 0

    .prologue
    .line 2524207
    iput-object p1, p0, LX/HyR;->b:LX/HyT;

    iput-object p2, p0, LX/HyR;->a:LX/Hx6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2524208
    const/4 v2, 0x0

    .line 2524209
    iget-object v0, p0, LX/HyR;->b:LX/HyT;

    iget-object v1, p0, LX/HyR;->a:LX/Hx6;

    .line 2524210
    sget-object v3, LX/HyS;->a:[I

    invoke-virtual {v1}, LX/Hx6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2524211
    iget-object v3, v0, LX/HyT;->b:LX/Bky;

    iget-object v3, v3, LX/Bky;->d:Landroid/net/Uri;

    .line 2524212
    :goto_0
    move-object v1, v3

    .line 2524213
    iget-object v0, p0, LX/HyR;->a:LX/Hx6;

    .line 2524214
    sget-object v3, LX/Hx6;->PAST:LX/Hx6;

    if-ne v0, v3, :cond_1

    .line 2524215
    sget-object v3, LX/Bkw;->I:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v3

    .line 2524216
    :goto_1
    move-object v5, v3

    .line 2524217
    iget-object v0, p0, LX/HyR;->b:LX/HyT;

    iget-object v0, v0, LX/HyT;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2524218
    if-eqz v0, :cond_0

    .line 2524219
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 2524220
    :cond_0
    return-object v0

    .line 2524221
    :pswitch_0
    iget-object v3, v0, LX/HyT;->b:LX/Bky;

    iget-object v3, v3, LX/Bky;->e:Landroid/net/Uri;

    goto :goto_0

    .line 2524222
    :pswitch_1
    iget-object v3, v0, LX/HyT;->b:LX/Bky;

    iget-object v3, v3, LX/Bky;->f:Landroid/net/Uri;

    goto :goto_0

    .line 2524223
    :pswitch_2
    iget-object v3, v0, LX/HyT;->b:LX/Bky;

    iget-object v3, v3, LX/Bky;->g:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
