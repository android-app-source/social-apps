.class public final LX/Irp;
.super LX/Irb;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V
    .locals 0

    .prologue
    .line 2618732
    iput-object p1, p0, LX/Irp;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-direct {p0}, LX/Irb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Irc;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 2618733
    iget-object v0, p0, LX/Irp;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618734
    iget-object v2, v0, LX/Ird;->c:LX/Iqg;

    move-object v2, v2

    .line 2618735
    if-nez v2, :cond_0

    .line 2618736
    const/4 v0, 0x0

    .line 2618737
    :goto_0
    return v0

    .line 2618738
    :cond_0
    iget-object v0, p0, LX/Irp;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    .line 2618739
    iput-boolean v1, v0, LX/Iqk;->k:Z

    .line 2618740
    iget v0, v2, LX/Iqg;->d:F

    move v0, v0

    .line 2618741
    const/4 p0, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 2618742
    invoke-static {p1}, LX/Irc;->f(LX/Irc;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2618743
    iget-boolean v3, p1, LX/Irc;->F:Z

    if-eqz v3, :cond_1

    iget v3, p1, LX/Irc;->h:F

    iget v5, p1, LX/Irc;->i:F

    cmpg-float v3, v3, v5

    if-ltz v3, :cond_2

    :cond_1
    iget-boolean v3, p1, LX/Irc;->F:Z

    if-nez v3, :cond_3

    iget v3, p1, LX/Irc;->h:F

    iget v5, p1, LX/Irc;->i:F

    cmpl-float v3, v3, v5

    if-lez v3, :cond_3

    :cond_2
    const/4 v3, 0x1

    .line 2618744
    :goto_1
    iget v5, p1, LX/Irc;->h:F

    iget v6, p1, LX/Irc;->i:F

    div-float/2addr v5, v6

    sub-float v5, v4, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v5, v6

    .line 2618745
    iget v6, p1, LX/Irc;->i:F

    cmpg-float v6, v6, p0

    if-gtz v6, :cond_4

    move v3, v4

    .line 2618746
    :goto_2
    move v3, v3

    .line 2618747
    mul-float/2addr v0, v3

    .line 2618748
    const v3, 0x3dcccccd    # 0.1f

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2618749
    const/high16 v3, 0x40f00000    # 7.5f

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2618750
    invoke-virtual {v2, v0}, LX/Iqg;->a(F)V

    move v0, v1

    .line 2618751
    goto :goto_0

    .line 2618752
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 2618753
    :cond_4
    if-eqz v3, :cond_5

    add-float v3, v4, v5

    goto :goto_2

    :cond_5
    sub-float v3, v4, v5

    goto :goto_2

    .line 2618754
    :cond_6
    iget v3, p1, LX/Irc;->i:F

    cmpl-float v3, v3, p0

    if-lez v3, :cond_7

    iget v3, p1, LX/Irc;->h:F

    iget v4, p1, LX/Irc;->i:F

    div-float/2addr v3, v4

    goto :goto_2

    :cond_7
    move v3, v4

    goto :goto_2
.end method

.method public final b(LX/Irc;)Z
    .locals 3

    .prologue
    .line 2618755
    iget v0, p1, LX/Irc;->d:F

    move v0, v0

    .line 2618756
    float-to-int v0, v0

    .line 2618757
    iget v1, p1, LX/Irc;->e:F

    move v1, v1

    .line 2618758
    float-to-int v1, v1

    .line 2618759
    iget-object v2, p0, LX/Irp;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {v2, v0, v1}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;II)LX/Iqg;

    move-result-object v0

    .line 2618760
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
