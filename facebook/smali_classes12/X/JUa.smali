.class public final LX/JUa;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JUb;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/JUb;


# direct methods
.method public constructor <init>(LX/JUb;)V
    .locals 1

    .prologue
    .line 2699300
    iput-object p1, p0, LX/JUa;->e:LX/JUb;

    .line 2699301
    move-object v0, p1

    .line 2699302
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2699303
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2699304
    const-string v0, "PageContextualRecommendationsImageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2699305
    if-ne p0, p1, :cond_1

    .line 2699306
    :cond_0
    :goto_0
    return v0

    .line 2699307
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2699308
    goto :goto_0

    .line 2699309
    :cond_3
    check-cast p1, LX/JUa;

    .line 2699310
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2699311
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2699312
    if-eq v2, v3, :cond_0

    .line 2699313
    iget-object v2, p0, LX/JUa;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JUa;->a:Ljava/lang/String;

    iget-object v3, p1, LX/JUa;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2699314
    goto :goto_0

    .line 2699315
    :cond_5
    iget-object v2, p1, LX/JUa;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2699316
    :cond_6
    iget-object v2, p0, LX/JUa;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JUa;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JUa;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2699317
    goto :goto_0

    .line 2699318
    :cond_8
    iget-object v2, p1, LX/JUa;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2699319
    :cond_9
    iget-object v2, p0, LX/JUa;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JUa;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object v3, p1, LX/JUa;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2699320
    goto :goto_0

    .line 2699321
    :cond_b
    iget-object v2, p1, LX/JUa;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-nez v2, :cond_a

    .line 2699322
    :cond_c
    iget-object v2, p0, LX/JUa;->d:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JUa;->d:Ljava/lang/String;

    iget-object v3, p1, LX/JUa;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2699323
    goto :goto_0

    .line 2699324
    :cond_d
    iget-object v2, p1, LX/JUa;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
