.class public final enum LX/Htp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Htp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Htp;

.field public static final enum FROM_BOTTOM:LX/Htp;

.field public static final enum FROM_TOP:LX/Htp;

.field public static final enum NO_ANIMATION:LX/Htp;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2516752
    new-instance v0, LX/Htp;

    const-string v1, "NO_ANIMATION"

    invoke-direct {v0, v1, v2}, LX/Htp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Htp;->NO_ANIMATION:LX/Htp;

    .line 2516753
    new-instance v0, LX/Htp;

    const-string v1, "FROM_TOP"

    invoke-direct {v0, v1, v3}, LX/Htp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Htp;->FROM_TOP:LX/Htp;

    .line 2516754
    new-instance v0, LX/Htp;

    const-string v1, "FROM_BOTTOM"

    invoke-direct {v0, v1, v4}, LX/Htp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Htp;->FROM_BOTTOM:LX/Htp;

    .line 2516755
    const/4 v0, 0x3

    new-array v0, v0, [LX/Htp;

    sget-object v1, LX/Htp;->NO_ANIMATION:LX/Htp;

    aput-object v1, v0, v2

    sget-object v1, LX/Htp;->FROM_TOP:LX/Htp;

    aput-object v1, v0, v3

    sget-object v1, LX/Htp;->FROM_BOTTOM:LX/Htp;

    aput-object v1, v0, v4

    sput-object v0, LX/Htp;->$VALUES:[LX/Htp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2516756
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Htp;
    .locals 1

    .prologue
    .line 2516757
    const-class v0, LX/Htp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Htp;

    return-object v0
.end method

.method public static values()[LX/Htp;
    .locals 1

    .prologue
    .line 2516758
    sget-object v0, LX/Htp;->$VALUES:[LX/Htp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Htp;

    return-object v0
.end method
