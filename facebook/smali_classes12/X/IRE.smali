.class public LX/IRE;
.super LX/1Cv;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2576298
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2576299
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2576300
    iput-object v0, p0, LX/IRE;->d:LX/0Px;

    .line 2576301
    iput-object p1, p0, LX/IRE;->a:Ljava/lang/String;

    .line 2576302
    iput-object p2, p0, LX/IRE;->b:Ljava/lang/String;

    .line 2576303
    iput-object p3, p0, LX/IRE;->c:Ljava/lang/String;

    .line 2576304
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2576305
    new-instance p1, LX/IRD;

    sget-object p2, LX/IUI;->h:LX/DML;

    invoke-direct {p1, p0, p2}, LX/IRD;-><init>(LX/IRE;LX/DML;)V

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2576306
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IRE;->d:LX/0Px;

    .line 2576307
    const v0, -0x639182d9

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2576308
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2576297
    sget-object v0, LX/IUI;->I:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2576294
    check-cast p2, LX/DMB;

    .line 2576295
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2576296
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2576291
    iput-object p1, p0, LX/IRE;->a:Ljava/lang/String;

    .line 2576292
    const v0, 0x2e30fd40

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2576293
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2576290
    iget-object v0, p0, LX/IRE;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2576289
    iget-object v0, p0, LX/IRE;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2576286
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2576288
    sget-object v1, LX/IUI;->I:LX/0Px;

    iget-object v0, p0, LX/IRE;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2576287
    sget-object v0, LX/IUI;->I:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
