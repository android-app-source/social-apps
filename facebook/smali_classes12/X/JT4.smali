.class public final LX/JT4;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;)V
    .locals 0

    .prologue
    .line 2696340
    iput-object p1, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2696341
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2696342
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2696343
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 2696344
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2696345
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2696346
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 2696347
    const-string v0, "https://m.facebook.com/spotify_callback"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2696348
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2696349
    invoke-virtual {v0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v1

    const-string v2, "code"

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2696350
    const-string v1, "code"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JT4;->a:Ljava/lang/String;

    .line 2696351
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->o:LX/JTE;

    if-eqz v0, :cond_0

    .line 2696352
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->o:LX/JTE;

    iget-object v1, p0, LX/JT4;->a:Ljava/lang/String;

    .line 2696353
    iget-object v2, v0, LX/JTE;->a:LX/JTY;

    if-eqz v2, :cond_0

    .line 2696354
    iget-object v2, v0, LX/JTE;->a:LX/JTY;

    sget-object v3, LX/JTV;->auth_success:LX/JTV;

    invoke-virtual {v2, v3}, LX/JTY;->a(LX/JTV;)V

    .line 2696355
    iget-object v2, v0, LX/JTE;->b:LX/JTO;

    iget-object v3, v0, LX/JTE;->c:Landroid/net/Uri;

    iget-object p1, v0, LX/JTE;->d:LX/JTK;

    iget-object p2, v0, LX/JTE;->e:LX/JTI;

    .line 2696356
    new-instance v0, LX/JTH;

    invoke-direct {v0, p2}, LX/JTH;-><init>(LX/JTI;)V

    move-object p2, v0

    .line 2696357
    invoke-virtual {v2, v1, v3, p1, p2}, LX/JTO;->a(Ljava/lang/String;Landroid/net/Uri;LX/JTK;LX/JTF;)V

    .line 2696358
    :cond_0
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2696359
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 2696360
    :goto_1
    return v0

    .line 2696361
    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    const-string v1, "error"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2696362
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->o:LX/JTE;

    if-eqz v0, :cond_3

    .line 2696363
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->o:LX/JTE;

    .line 2696364
    iget-object v1, v0, LX/JTE;->a:LX/JTY;

    if-eqz v1, :cond_3

    .line 2696365
    iget-object v1, v0, LX/JTE;->a:LX/JTY;

    sget-object v2, LX/JTV;->auth_fail:LX/JTV;

    invoke-virtual {v1, v2}, LX/JTY;->a(LX/JTV;)V

    .line 2696366
    :cond_3
    iget-object v0, p0, LX/JT4;->b:Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0

    .line 2696367
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method
