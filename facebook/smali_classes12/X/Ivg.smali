.class public final LX/Ivg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2628483
    iput-object p1, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2628484
    invoke-static {p2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 2628485
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, LX/Ivg;->b:I

    .line 2628486
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, LX/Ivg;->c:I

    .line 2628487
    return-void
.end method

.method private e(F)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2628488
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 2628489
    iget v2, p0, LX/Ivg;->c:I

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-gez v2, :cond_1

    :goto_0
    move p1, v0

    .line 2628490
    :cond_0
    return p1

    .line 2628491
    :cond_1
    iget v2, p0, LX/Ivg;->b:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 2628492
    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    iget v0, p0, LX/Ivg;->b:I

    int-to-float v0, v0

    goto :goto_0

    :cond_2
    iget v0, p0, LX/Ivg;->b:I

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 2628493
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    const/4 v1, 0x1

    .line 2628494
    iput-boolean v1, v0, LX/3ID;->i:Z

    .line 2628495
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628496
    invoke-static {v0, p1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;F)V

    .line 2628497
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    if-eqz v0, :cond_0

    .line 2628498
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628499
    invoke-static {v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628500
    :cond_0
    return-void
.end method

.method public final b(F)V
    .locals 2

    .prologue
    .line 2628501
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    const/4 v1, 0x1

    .line 2628502
    iput-boolean v1, v0, LX/3ID;->j:Z

    .line 2628503
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628504
    invoke-static {v0, p1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;F)V

    .line 2628505
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    if-eqz v0, :cond_0

    .line 2628506
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628507
    invoke-static {v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628508
    :cond_0
    return-void
.end method

.method public final c(F)V
    .locals 2

    .prologue
    .line 2628509
    invoke-direct {p0, p1}, LX/Ivg;->e(F)F

    move-result v0

    .line 2628510
    iget-object v1, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628511
    invoke-static {v1, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;F)V

    .line 2628512
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    const/4 v1, 0x0

    .line 2628513
    iput-boolean v1, v0, LX/3ID;->i:Z

    .line 2628514
    return-void
.end method

.method public final d(F)V
    .locals 2

    .prologue
    .line 2628515
    invoke-direct {p0, p1}, LX/Ivg;->e(F)F

    move-result v0

    .line 2628516
    iget-object v1, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628517
    invoke-static {v1, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->c$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;F)V

    .line 2628518
    iget-object v0, p0, LX/Ivg;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    const/4 v1, 0x0

    .line 2628519
    iput-boolean v1, v0, LX/3ID;->j:Z

    .line 2628520
    return-void
.end method
