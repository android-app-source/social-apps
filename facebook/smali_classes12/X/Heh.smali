.class public LX/Heh;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/LinearLayout;

.field private b:Landroid/widget/LinearLayout;

.field private c:Landroid/view/View$OnClickListener;

.field public d:Landroid/view/View;

.field public e:LX/Heg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2490597
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2490598
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/Heh;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490599
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2490600
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2490601
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Heh;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490602
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2490603
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490604
    invoke-direct {p0, p1, p2, p3}, LX/Heh;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490605
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2490606
    const v0, 0x7f0312ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2490607
    const v0, 0x7f0d2c0e

    invoke-virtual {p0, v0}, LX/Heh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Heh;->a:Landroid/widget/LinearLayout;

    .line 2490608
    const v0, 0x7f0d2c0f

    invoke-virtual {p0, v0}, LX/Heh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Heh;->b:Landroid/widget/LinearLayout;

    .line 2490609
    new-instance v0, LX/Hef;

    invoke-direct {v0, p0}, LX/Hef;-><init>(LX/Heh;)V

    iput-object v0, p0, LX/Heh;->c:Landroid/view/View$OnClickListener;

    .line 2490610
    sget-object v0, LX/03r;->SegmentedTabBar2:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2490611
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2490612
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2490613
    if-lez v1, :cond_0

    .line 2490614
    invoke-virtual {p0}, LX/Heh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 2490615
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 2490616
    array-length v5, v3

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v3, v1

    .line 2490617
    const v0, 0x7f031462

    iget-object v7, p0, LX/Heh;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0, v7, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2490618
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490619
    invoke-direct {p0, v0}, LX/Heh;->a(Landroid/view/View;)V

    .line 2490620
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2490621
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2490622
    iget-object v0, p0, LX/Heh;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2490623
    iget-object v0, p0, LX/Heh;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2490624
    return-void
.end method


# virtual methods
.method public getTabContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 2490625
    iget-object v0, p0, LX/Heh;->a:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public setBorderColor(I)V
    .locals 5

    .prologue
    .line 2490626
    if-nez p1, :cond_0

    .line 2490627
    :goto_0
    return-void

    .line 2490628
    :cond_0
    iget-object v0, p0, LX/Heh;->a:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2490629
    iget-object v0, p0, LX/Heh;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2490630
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 2490631
    const v1, 0x7f0d31f2

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2490632
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0}, LX/Heh;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b022a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v3, p1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 2490633
    const v1, 0x7f0d31f2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 2490634
    iget-object v1, p0, LX/Heh;->b:Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setListener(LX/Heg;)V
    .locals 0

    .prologue
    .line 2490635
    iput-object p1, p0, LX/Heh;->e:LX/Heg;

    .line 2490636
    return-void
.end method
