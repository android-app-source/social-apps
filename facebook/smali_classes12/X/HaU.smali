.class public final LX/HaU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:D

.field public b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public c:D

.field public d:Z

.field public e:Z

.field public f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2483878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/0Px;DI)LX/0P1;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;DI)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2483886
    const/4 v0, 0x1

    .line 2483887
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2483888
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    move v4, v0

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move v1, v2

    .line 2483889
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 2483890
    add-int v7, v1, p3

    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    int-to-double v8, v4

    mul-double/2addr v8, p1

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2483891
    add-int/2addr v1, p3

    goto :goto_1

    .line 2483892
    :cond_0
    add-int/lit8 v1, v4, 0x1

    .line 2483893
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v1

    goto :goto_0

    .line 2483894
    :cond_1
    invoke-static {v5}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0P1;)LX/HaU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Double;",
            ">;)",
            "LX/HaU;"
        }
    .end annotation

    .prologue
    .line 2483884
    invoke-static {p1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/HaU;->b:LX/0P1;

    .line 2483885
    return-object p0
.end method

.method public final a(LX/0Px;D)LX/HaU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;D)",
            "LX/HaU;"
        }
    .end annotation

    .prologue
    .line 2483882
    const/4 v0, 0x2

    invoke-static {p1, p2, p3, v0}, LX/HaU;->a(LX/0Px;DI)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/HaU;->g:LX/0P1;

    .line 2483883
    return-object p0
.end method

.method public final b(LX/0Px;D)LX/HaU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;D)",
            "LX/HaU;"
        }
    .end annotation

    .prologue
    .line 2483880
    const/4 v0, 0x3

    invoke-static {p1, p2, p3, v0}, LX/HaU;->a(LX/0Px;DI)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/HaU;->h:LX/0P1;

    .line 2483881
    return-object p0
.end method

.method public final i()LX/HaV;
    .locals 1

    .prologue
    .line 2483879
    new-instance v0, LX/HaV;

    invoke-direct {v0, p0}, LX/HaV;-><init>(LX/HaU;)V

    return-object v0
.end method
