.class public final LX/HFX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2443692
    iput-object p1, p0, LX/HFX;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iput-object p2, p0, LX/HFX;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2443693
    iget-object v0, p0, LX/HFX;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const/4 v1, 0x0

    .line 2443694
    iput-boolean v1, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->B:Z

    .line 2443695
    iget-object v0, p0, LX/HFX;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->a:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    const-string v2, "location fetch failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2443696
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2443697
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2443698
    if-eqz p1, :cond_0

    .line 2443699
    iget-object v0, p0, LX/HFX;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v1

    .line 2443700
    iput-object v1, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->z:Landroid/location/Location;

    .line 2443701
    iget-object v0, p0, LX/HFX;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->z:Landroid/location/Location;

    if-eqz v0, :cond_1

    .line 2443702
    iget-object v0, p0, LX/HFX;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, p0, LX/HFX;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->e(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;Ljava/lang/String;)V

    .line 2443703
    :cond_0
    :goto_0
    return-void

    .line 2443704
    :cond_1
    iget-object v0, p0, LX/HFX;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const/4 v1, 0x0

    .line 2443705
    iput-boolean v1, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->B:Z

    .line 2443706
    goto :goto_0
.end method
