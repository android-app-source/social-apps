.class public LX/HKI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HKK;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HKI",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HKK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454194
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2454195
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HKI;->b:LX/0Zi;

    .line 2454196
    iput-object p1, p0, LX/HKI;->a:LX/0Ot;

    .line 2454197
    return-void
.end method

.method public static a(LX/0QB;)LX/HKI;
    .locals 4

    .prologue
    .line 2454198
    const-class v1, LX/HKI;

    monitor-enter v1

    .line 2454199
    :try_start_0
    sget-object v0, LX/HKI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454200
    sput-object v2, LX/HKI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454201
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454202
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454203
    new-instance v3, LX/HKI;

    const/16 p0, 0x2bc7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HKI;-><init>(LX/0Ot;)V

    .line 2454204
    move-object v0, v3

    .line 2454205
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454206
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454207
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2454209
    check-cast p2, LX/HKH;

    .line 2454210
    iget-object v0, p0, LX/HKI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HKK;

    iget-object v1, p2, LX/HKH;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p2, LX/HKH;->b:LX/2km;

    const/4 v6, 0x1

    const/4 p2, 0x3

    const/4 v7, 0x0

    const/4 p0, 0x2

    .line 2454211
    invoke-static {v1, p2, v2}, LX/HKK;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ILX/2km;)Ljava/util/List;

    move-result-object v8

    .line 2454212
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HKJ;

    .line 2454213
    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/HKJ;

    .line 2454214
    invoke-interface {v8, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/HKJ;

    .line 2454215
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ne v8, p2, :cond_0

    :goto_0
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 2454216
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    const v8, 0x7f0a00d5

    invoke-interface {v6, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v6

    const v8, 0x7f0b0e40

    invoke-interface {v6, p2, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    invoke-static {v0, p1, v3, v1, v2}, LX/HKK;->a(LX/HKK;LX/1De;LX/HKJ;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;

    move-result-object v3

    const v8, 0x7f0b0e3b

    invoke-interface {v3, v7, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v8, 0x7f0b0e3a

    invoke-interface {v3, p0, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {v0, p1, v4, v1, v2}, LX/HKK;->a(LX/HKK;LX/1De;LX/HKJ;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;

    move-result-object v4

    const/4 v6, 0x6

    const v8, 0x7f0b0e3a

    invoke-interface {v4, v6, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {v0, p1, v5, v1, v2}, LX/HKK;->a(LX/HKK;LX/1De;LX/HKJ;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0e3a

    invoke-interface {v4, v7, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0e3b

    invoke-interface {v4, p0, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2454217
    return-object v0

    :cond_0
    move v6, v7

    .line 2454218
    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2454219
    invoke-static {}, LX/1dS;->b()V

    .line 2454220
    const/4 v0, 0x0

    return-object v0
.end method
