.class public final LX/IQy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V
    .locals 0

    .prologue
    .line 2575973
    iput-object p1, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2575972
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575948
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2575949
    if-eqz p1, :cond_0

    .line 2575950
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575951
    if-nez v0, :cond_1

    .line 2575952
    :cond_0
    :goto_0
    return-void

    .line 2575953
    :cond_1
    const/4 v3, 0x0

    .line 2575954
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575955
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;

    .line 2575956
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    const v8, -0x2a04dead

    if-ne v7, v8, :cond_3

    move-object v3, v0

    .line 2575957
    :cond_2
    if-nez v3, :cond_4

    move v0, v2

    :goto_2
    if-eqz v0, :cond_6

    move v0, v2

    :goto_3
    if-nez v0, :cond_0

    .line 2575958
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$GroupStoryTopicsModel$NodesModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    iget-object v2, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    if-eqz v0, :cond_8

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2575959
    :goto_4
    iput-object v0, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->l:LX/0Px;

    .line 2575960
    iget-object v0, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_5
    if-ge v1, v2, :cond_9

    iget-object v0, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->l:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$GroupStoryTopicsModel$NodesModel;

    .line 2575961
    iget-object v3, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->p:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$GroupStoryTopicsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2575962
    iget-object v3, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$GroupStoryTopicsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2575963
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 2575964
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 2575965
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2575966
    if-nez v0, :cond_5

    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2

    .line 2575967
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2575968
    invoke-virtual {v4, v0, v1}, LX/15i;->j(II)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_3

    .line 2575969
    :cond_8
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2575970
    goto :goto_4

    .line 2575971
    :cond_9
    iget-object v0, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->g:LX/IQQ;

    iget-object v1, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    iget-object v2, p0, LX/IQy;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, LX/IQQ;->a(LX/0Px;Ljava/util/Set;)V

    goto/16 :goto_0
.end method
