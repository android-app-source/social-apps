.class public final LX/HNz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459128
    iput-object p1, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2459129
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459130
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Ljava/lang/String;)V

    .line 2459131
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2459132
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->bd:LX/0ih;

    const-string v2, "setup_info_fetch_failed"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2459133
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->bd:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2459134
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2459135
    check-cast p1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    .line 2459136
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459137
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    .line 2459138
    iput-object p1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    .line 2459139
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459140
    iget-object v0, p0, LX/HNz;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    .line 2459141
    iget-object v2, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object p0, LX/0ig;->bd:LX/0ih;

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "has_services"

    :goto_0
    invoke-virtual {v2, p0, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2459142
    iget-object v2, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object p0, LX/0ig;->bd:LX/0ih;

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "message_enabled"

    :goto_1
    invoke-virtual {v2, p0, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2459143
    iget-object v2, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object p0, LX/0ig;->bd:LX/0ih;

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "is_published"

    :goto_2
    invoke-virtual {v2, p0, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2459144
    return-void

    .line 2459145
    :cond_0
    const-string v1, "has_no_services"

    goto :goto_0

    .line 2459146
    :cond_1
    const-string v1, "message_disabled"

    goto :goto_1

    .line 2459147
    :cond_2
    const-string v1, "is_not_published"

    goto :goto_2
.end method
