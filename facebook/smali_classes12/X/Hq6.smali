.class public LX/Hq6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Hq6;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0wM;

.field public c:Z

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2510249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2510250
    iput-object p1, p0, LX/Hq6;->a:Landroid/content/res/Resources;

    .line 2510251
    iput-object p2, p0, LX/Hq6;->b:LX/0wM;

    .line 2510252
    return-void
.end method

.method public static a(LX/0QB;)LX/Hq6;
    .locals 5

    .prologue
    .line 2510236
    sget-object v0, LX/Hq6;->f:LX/Hq6;

    if-nez v0, :cond_1

    .line 2510237
    const-class v1, LX/Hq6;

    monitor-enter v1

    .line 2510238
    :try_start_0
    sget-object v0, LX/Hq6;->f:LX/Hq6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2510239
    if-eqz v2, :cond_0

    .line 2510240
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2510241
    new-instance p0, LX/Hq6;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-direct {p0, v3, v4}, LX/Hq6;-><init>(Landroid/content/res/Resources;LX/0wM;)V

    .line 2510242
    move-object v0, p0

    .line 2510243
    sput-object v0, LX/Hq6;->f:LX/Hq6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2510244
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2510245
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2510246
    :cond_1
    sget-object v0, LX/Hq6;->f:LX/Hq6;

    return-object v0

    .line 2510247
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2510248
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
