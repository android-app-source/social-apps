.class public final LX/HF9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;)V
    .locals 0

    .prologue
    .line 2443107
    iput-object p1, p0, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2443108
    iget-object v0, p0, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->b:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fetch suggestion failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443109
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2443110
    check-cast p1, LX/0Px;

    const/4 v4, 0x0

    .line 2443111
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2443112
    iget-object v0, p0, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 2443113
    iget-object v0, p0, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    const v1, 0x7f0d22aa

    .line 2443114
    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    .line 2443115
    check-cast v0, Lcom/facebook/widget/FlowLayout;

    .line 2443116
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_0

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    .line 2443117
    const v2, 0x7f030e3b

    invoke-virtual {v5, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fig/button/FigButton;

    .line 2443118
    invoke-virtual {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2443119
    new-instance v7, LX/HF8;

    invoke-direct {v7, p0, v1}, LX/HF8;-><init>(LX/HF9;Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;)V

    invoke-virtual {v2, v7}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443120
    invoke-virtual {v0, v2}, Lcom/facebook/widget/FlowLayout;->addView(Landroid/view/View;)V

    .line 2443121
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 2443122
    :cond_0
    invoke-virtual {v0, v4}, Lcom/facebook/widget/FlowLayout;->setVisibility(I)V

    .line 2443123
    :cond_1
    return-void
.end method
