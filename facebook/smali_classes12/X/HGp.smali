.class public final enum LX/HGp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HGp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HGp;

.field public static final enum ALBUM:LX/HGp;

.field public static final enum CREATE_ALBUM:LX/HGp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2446640
    new-instance v0, LX/HGp;

    const-string v1, "CREATE_ALBUM"

    invoke-direct {v0, v1, v2}, LX/HGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HGp;->CREATE_ALBUM:LX/HGp;

    .line 2446641
    new-instance v0, LX/HGp;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v3}, LX/HGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HGp;->ALBUM:LX/HGp;

    .line 2446642
    const/4 v0, 0x2

    new-array v0, v0, [LX/HGp;

    sget-object v1, LX/HGp;->CREATE_ALBUM:LX/HGp;

    aput-object v1, v0, v2

    sget-object v1, LX/HGp;->ALBUM:LX/HGp;

    aput-object v1, v0, v3

    sput-object v0, LX/HGp;->$VALUES:[LX/HGp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2446643
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HGp;
    .locals 1

    .prologue
    .line 2446644
    const-class v0, LX/HGp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HGp;

    return-object v0
.end method

.method public static values()[LX/HGp;
    .locals 1

    .prologue
    .line 2446645
    sget-object v0, LX/HGp;->$VALUES:[LX/HGp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HGp;

    return-object v0
.end method
