.class public LX/JEO;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/fbui/glyph/GlyphView;

.field public d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2665951
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2665952
    const v0, 0x7f0315a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2665953
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JEO;->setOrientation(I)V

    .line 2665954
    iput-object p1, p0, LX/JEO;->d:Landroid/content/Context;

    .line 2665955
    const v0, 0x7f0d30cc

    invoke-virtual {p0, v0}, LX/JEO;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JEO;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2665956
    const v0, 0x7f0d30cd

    invoke-virtual {p0, v0}, LX/JEO;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JEO;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2665957
    const v0, 0x7f0d30cb

    invoke-virtual {p0, v0}, LX/JEO;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/JEO;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2665958
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    iget-object v2, p0, LX/JEO;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b24a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2665959
    invoke-virtual {p0, v0}, LX/JEO;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2665960
    return-void
.end method
