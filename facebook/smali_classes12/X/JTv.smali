.class public final LX/JTv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zu;


# instance fields
.field public final synthetic a:LX/JTw;


# direct methods
.method public constructor <init>(LX/JTw;)V
    .locals 0

    .prologue
    .line 2697460
    iput-object p1, p0, LX/JTv;->a:LX/JTw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2697461
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2697462
    sget-object v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2697463
    iget-object v1, p0, LX/JTv;->a:LX/JTw;

    iget-object v1, v1, LX/JTw;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/JTv;->a:LX/JTw;

    iget-object v2, v2, LX/JTw;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2697464
    iget-object v0, p0, LX/JTv;->a:LX/JTw;

    iget-object v0, v0, LX/JTw;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->f:LX/JUO;

    iget-object v1, p0, LX/JTv;->a:LX/JTw;

    iget-object v1, v1, LX/JTw;->a:LX/JUH;

    iget-object v1, v1, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v2, p0, LX/JTv;->a:LX/JTw;

    iget-object v2, v2, LX/JTw;->a:LX/JUH;

    iget-object v2, v2, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v0, v1, v2}, LX/JUO;->c(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2697465
    return-void
.end method
