.class public final LX/IIS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/location/ImmutableLocation;

.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Lcom/facebook/location/ImmutableLocation;)V
    .locals 0

    .prologue
    .line 2561854
    iput-object p1, p0, LX/IIS;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iput-object p2, p0, LX/IIS;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2561855
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2561856
    const-string v0, "BackgroundLocationReportingUpdateParams"

    new-instance v1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;

    new-instance v3, LX/2TT;

    invoke-direct {v3}, LX/2TT;-><init>()V

    iget-object v4, p0, LX/IIS;->a:Lcom/facebook/location/ImmutableLocation;

    .line 2561857
    iput-object v4, v3, LX/2TT;->a:Lcom/facebook/location/ImmutableLocation;

    .line 2561858
    move-object v3, v3

    .line 2561859
    iget-object v4, p0, LX/IIS;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v4, v4, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->H:LX/0Uo;

    invoke-virtual {v4}, LX/0Uo;->l()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2561860
    iput-object v4, v3, LX/2TT;->b:Ljava/lang/Boolean;

    .line 2561861
    move-object v3, v3

    .line 2561862
    invoke-virtual {v3}, LX/2TT;->a()Lcom/facebook/location/LocationSignalDataPackage;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;-><init>(LX/0Px;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2561863
    iget-object v0, p0, LX/IIS;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->h:LX/0aG;

    const-string v1, "background_location_update"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x5c2820ea

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method
