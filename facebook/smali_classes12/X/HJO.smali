.class public LX/HJO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HJO",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452615
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2452616
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HJO;->b:LX/0Zi;

    .line 2452617
    iput-object p1, p0, LX/HJO;->a:LX/0Ot;

    .line 2452618
    return-void
.end method

.method public static a(LX/0QB;)LX/HJO;
    .locals 4

    .prologue
    .line 2452619
    const-class v1, LX/HJO;

    monitor-enter v1

    .line 2452620
    :try_start_0
    sget-object v0, LX/HJO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452621
    sput-object v2, LX/HJO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452622
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452623
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452624
    new-instance v3, LX/HJO;

    const/16 p0, 0x2bab

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJO;-><init>(LX/0Ot;)V

    .line 2452625
    move-object v0, v3

    .line 2452626
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452627
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452628
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2452630
    const v0, -0x4dac46c5

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2452631
    check-cast p2, LX/HJN;

    .line 2452632
    iget-object v0, p0, LX/HJO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;

    iget-object v2, p2, LX/HJN;->a:LX/HL6;

    iget-object v3, p2, LX/HJN;->b:LX/2km;

    iget-object v4, p2, LX/HJN;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v5, p2, LX/HJN;->d:LX/HLF;

    move-object v1, p1

    const/4 p2, 0x0

    .line 2452633
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2452634
    iget-object v7, v2, LX/HL6;->b:Landroid/net/Uri;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2452635
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const p0, 0x7f0a0097

    invoke-interface {v7, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v7

    .line 2452636
    const p0, -0x4dac46c5

    const/4 p1, 0x0

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2452637
    invoke-interface {v7, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    .line 2452638
    iget-object p0, v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->c:LX/8yV;

    invoke-virtual {p0, v1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object p0

    const p1, 0x7f0b2323

    .line 2452639
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->i:I

    .line 2452640
    move-object p0, p0

    .line 2452641
    sget-object p1, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v6

    const p0, 0x7f0b2327

    invoke-virtual {v6, p0}, LX/8yU;->h(I)LX/8yU;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/8yU;->a(Z)LX/8yU;

    move-result-object v6

    .line 2452642
    iget p0, v2, LX/HL6;->c:I

    check-cast v3, LX/1Pr;

    invoke-static {v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->a(LX/1Pr;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;)I

    move-result p1

    if-ne p0, p1, :cond_0

    .line 2452643
    const p0, 0x7f0a08e4

    invoke-virtual {v6, p0}, LX/8yU;->i(I)LX/8yU;

    move-result-object p0

    const p1, 0x7f0b2324

    invoke-virtual {p0, p1}, LX/8yU;->k(I)LX/8yU;

    .line 2452644
    :goto_0
    invoke-virtual {v6}, LX/1X5;->d()LX/1X1;

    move-result-object v6

    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2452645
    return-object v0

    .line 2452646
    :cond_0
    const/4 p0, 0x0

    invoke-virtual {v6, p0}, LX/8yU;->c(F)LX/8yU;

    move-result-object p0

    const p1, 0x7f0a08e5

    .line 2452647
    iget-object p2, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->j:I

    .line 2452648
    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2452649
    invoke-static {}, LX/1dS;->b()V

    .line 2452650
    iget v0, p1, LX/1dQ;->b:I

    .line 2452651
    packed-switch v0, :pswitch_data_0

    .line 2452652
    :goto_0
    return-object v1

    .line 2452653
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2452654
    check-cast v0, LX/HJN;

    .line 2452655
    iget-object v2, p0, LX/HJO;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;

    iget-object v3, v0, LX/HJN;->b:LX/2km;

    iget-object v4, v0, LX/HJN;->a:LX/HL6;

    iget-object p1, v0, LX/HJN;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object p2, v0, LX/HJN;->d:LX/HLF;

    invoke-virtual {v2, v3, v4, p1, p2}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->onClick(LX/2km;LX/HL6;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;)V

    .line 2452656
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x4dac46c5
        :pswitch_0
    .end packed-switch
.end method
