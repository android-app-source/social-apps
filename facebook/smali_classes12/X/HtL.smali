.class public final LX/HtL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/feedattachment/graphql/FetchEventComposerPreviewModels$FetchEventComposerPreviewModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;)V
    .locals 0

    .prologue
    .line 2515801
    iput-object p1, p0, LX/HtL;->a:Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2515802
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2515803
    if-nez p1, :cond_0

    .line 2515804
    const/4 v0, 0x0

    .line 2515805
    :goto_0
    return-object v0

    .line 2515806
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2515807
    check-cast v0, Lcom/facebook/composer/feedattachment/graphql/FetchEventComposerPreviewModels$FetchEventComposerPreviewModel;

    invoke-virtual {v0}, Lcom/facebook/composer/feedattachment/graphql/FetchEventComposerPreviewModels$FetchEventComposerPreviewModel;->a()Lcom/facebook/composer/feedattachment/graphql/FetchEventComposerPreviewModels$EventStoryAttachmentModel;

    move-result-object v0

    .line 2515808
    if-nez v0, :cond_1

    .line 2515809
    const/4 v1, 0x0

    .line 2515810
    :goto_1
    move-object v0, v1

    .line 2515811
    goto :goto_0

    .line 2515812
    :cond_1
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    .line 2515813
    invoke-virtual {v0}, Lcom/facebook/composer/feedattachment/graphql/FetchEventComposerPreviewModels$EventStoryAttachmentModel;->a()LX/0Px;

    move-result-object v2

    .line 2515814
    iput-object v2, v1, LX/39x;->p:LX/0Px;

    .line 2515815
    invoke-virtual {v0}, Lcom/facebook/composer/feedattachment/graphql/FetchEventComposerPreviewModels$EventStoryAttachmentModel;->b()LX/55t;

    move-result-object v2

    .line 2515816
    if-nez v2, :cond_2

    .line 2515817
    const/4 v3, 0x0

    .line 2515818
    :goto_2
    move-object v2, v3

    .line 2515819
    iput-object v2, v1, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2515820
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    goto :goto_1

    .line 2515821
    :cond_2
    new-instance v3, LX/4XR;

    invoke-direct {v3}, LX/4XR;-><init>()V

    .line 2515822
    new-instance v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v5, 0x403827a

    invoke-direct {v4, v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2515823
    iput-object v4, v3, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2515824
    invoke-interface {v2}, LX/55t;->b()Z

    move-result v4

    .line 2515825
    iput-boolean v4, v3, LX/4XR;->bg:Z

    .line 2515826
    invoke-interface {v2}, LX/55t;->c()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    .line 2515827
    iput-object v4, v3, LX/4XR;->co:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 2515828
    invoke-interface {v2}, LX/55t;->d()J

    move-result-wide v5

    .line 2515829
    iput-wide v5, v3, LX/4XR;->dJ:J

    .line 2515830
    invoke-interface {v2}, LX/55t;->e()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    move-result-object v4

    .line 2515831
    if-nez v4, :cond_3

    .line 2515832
    const/4 v5, 0x0

    .line 2515833
    :goto_3
    move-object v4, v5

    .line 2515834
    iput-object v4, v3, LX/4XR;->dW:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 2515835
    invoke-interface {v2}, LX/55t;->aH_()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    move-result-object v4

    .line 2515836
    if-nez v4, :cond_7

    .line 2515837
    const/4 v5, 0x0

    .line 2515838
    :goto_4
    move-object v4, v5

    .line 2515839
    iput-object v4, v3, LX/4XR;->ed:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 2515840
    invoke-interface {v2}, LX/55t;->aG_()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    move-result-object v4

    .line 2515841
    if-nez v4, :cond_8

    .line 2515842
    const/4 v7, 0x0

    .line 2515843
    :goto_5
    move-object v4, v7

    .line 2515844
    iput-object v4, v3, LX/4XR;->ee:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 2515845
    invoke-interface {v2}, LX/55t;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    move-result-object v4

    .line 2515846
    if-nez v4, :cond_b

    .line 2515847
    const/4 v5, 0x0

    .line 2515848
    :goto_6
    move-object v4, v5

    .line 2515849
    iput-object v4, v3, LX/4XR;->el:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 2515850
    invoke-interface {v2}, LX/55t;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    move-result-object v4

    .line 2515851
    if-nez v4, :cond_c

    .line 2515852
    const/4 v5, 0x0

    .line 2515853
    :goto_7
    move-object v4, v5

    .line 2515854
    iput-object v4, v3, LX/4XR;->eV:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 2515855
    invoke-interface {v2}, LX/55t;->l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    move-result-object v4

    .line 2515856
    if-nez v4, :cond_11

    .line 2515857
    const/4 v5, 0x0

    .line 2515858
    :goto_8
    move-object v4, v5

    .line 2515859
    iput-object v4, v3, LX/4XR;->eX:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 2515860
    invoke-interface {v2}, LX/55t;->m()Ljava/lang/String;

    move-result-object v4

    .line 2515861
    iput-object v4, v3, LX/4XR;->fO:Ljava/lang/String;

    .line 2515862
    invoke-interface {v2}, LX/55t;->n()Z

    move-result v4

    .line 2515863
    iput-boolean v4, v3, LX/4XR;->gn:Z

    .line 2515864
    invoke-interface {v2}, LX/55t;->o()Z

    move-result v4

    .line 2515865
    iput-boolean v4, v3, LX/4XR;->gr:Z

    .line 2515866
    invoke-interface {v2}, LX/55t;->p()Ljava/lang/String;

    move-result-object v4

    .line 2515867
    iput-object v4, v3, LX/4XR;->iB:Ljava/lang/String;

    .line 2515868
    invoke-interface {v2}, LX/55t;->q()LX/586;

    move-result-object v4

    .line 2515869
    if-nez v4, :cond_16

    .line 2515870
    const/4 v5, 0x0

    .line 2515871
    :goto_9
    move-object v4, v5

    .line 2515872
    iput-object v4, v3, LX/4XR;->mL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2515873
    invoke-interface {v2}, LX/55t;->r()J

    move-result-wide v5

    .line 2515874
    iput-wide v5, v3, LX/4XR;->ng:J

    .line 2515875
    invoke-interface {v2}, LX/55t;->s()Ljava/lang/String;

    move-result-object v4

    .line 2515876
    iput-object v4, v3, LX/4XR;->oh:Ljava/lang/String;

    .line 2515877
    invoke-interface {v2}, LX/55t;->t()Ljava/lang/String;

    move-result-object v4

    .line 2515878
    iput-object v4, v3, LX/4XR;->oU:Ljava/lang/String;

    .line 2515879
    invoke-interface {v2}, LX/55t;->u()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    .line 2515880
    iput-object v4, v3, LX/4XR;->py:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2515881
    invoke-interface {v2}, LX/55t;->v()Z

    move-result v4

    .line 2515882
    iput-boolean v4, v3, LX/4XR;->pz:Z

    .line 2515883
    invoke-interface {v2}, LX/55t;->w()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    .line 2515884
    iput-object v4, v3, LX/4XR;->pL:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2515885
    invoke-virtual {v3}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    goto/16 :goto_2

    .line 2515886
    :cond_3
    new-instance v5, LX/4WQ;

    invoke-direct {v5}, LX/4WQ;-><init>()V

    .line 2515887
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel$PhotoModel;

    move-result-object v6

    .line 2515888
    if-nez v6, :cond_4

    .line 2515889
    const/4 v7, 0x0

    .line 2515890
    :goto_a
    move-object v6, v7

    .line 2515891
    iput-object v6, v5, LX/4WQ;->c:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2515892
    invoke-virtual {v5}, LX/4WQ;->a()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v5

    goto/16 :goto_3

    .line 2515893
    :cond_4
    new-instance v7, LX/4Xy;

    invoke-direct {v7}, LX/4Xy;-><init>()V

    .line 2515894
    invoke-virtual {v6}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel$PhotoModel;->b()Ljava/lang/String;

    move-result-object v8

    .line 2515895
    iput-object v8, v7, LX/4Xy;->I:Ljava/lang/String;

    .line 2515896
    invoke-virtual {v6}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel$PhotoModel;->c()LX/1Fb;

    move-result-object v8

    .line 2515897
    if-nez v8, :cond_5

    .line 2515898
    const/4 v9, 0x0

    .line 2515899
    :goto_b
    move-object v8, v9

    .line 2515900
    iput-object v8, v7, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2515901
    invoke-virtual {v6}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel$PhotoModel;->d()LX/174;

    move-result-object v8

    .line 2515902
    if-nez v8, :cond_6

    .line 2515903
    const/4 v9, 0x0

    .line 2515904
    :goto_c
    move-object v8, v9

    .line 2515905
    iput-object v8, v7, LX/4Xy;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2515906
    invoke-virtual {v6}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel$PhotoModel;->e()Ljava/lang/String;

    move-result-object v8

    .line 2515907
    iput-object v8, v7, LX/4Xy;->ba:Ljava/lang/String;

    .line 2515908
    invoke-virtual {v7}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v7

    goto :goto_a

    .line 2515909
    :cond_5
    new-instance v9, LX/2dc;

    invoke-direct {v9}, LX/2dc;-><init>()V

    .line 2515910
    invoke-interface {v8}, LX/1Fb;->a()I

    move-result v4

    .line 2515911
    iput v4, v9, LX/2dc;->c:I

    .line 2515912
    invoke-interface {v8}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    .line 2515913
    iput-object v4, v9, LX/2dc;->h:Ljava/lang/String;

    .line 2515914
    invoke-interface {v8}, LX/1Fb;->c()I

    move-result v4

    .line 2515915
    iput v4, v9, LX/2dc;->i:I

    .line 2515916
    invoke-virtual {v9}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    goto :goto_b

    .line 2515917
    :cond_6
    new-instance v9, LX/173;

    invoke-direct {v9}, LX/173;-><init>()V

    .line 2515918
    invoke-interface {v8}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 2515919
    iput-object v4, v9, LX/173;->f:Ljava/lang/String;

    .line 2515920
    invoke-virtual {v9}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    goto :goto_c

    .line 2515921
    :cond_7
    new-instance v5, LX/4WC;

    invoke-direct {v5}, LX/4WC;-><init>()V

    .line 2515922
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;->a()I

    move-result v6

    .line 2515923
    iput v6, v5, LX/4WC;->b:I

    .line 2515924
    invoke-virtual {v5}, LX/4WC;->a()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v5

    goto/16 :goto_4

    .line 2515925
    :cond_8
    new-instance v7, LX/4Y6;

    invoke-direct {v7}, LX/4Y6;-><init>()V

    .line 2515926
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 2515927
    iput-object v8, v7, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2515928
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;->c()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel$CityModel;

    move-result-object v8

    .line 2515929
    if-nez v8, :cond_9

    .line 2515930
    const/4 v9, 0x0

    .line 2515931
    :goto_d
    move-object v8, v9

    .line 2515932
    iput-object v8, v7, LX/4Y6;->h:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2515933
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;->d()Ljava/lang/String;

    move-result-object v8

    .line 2515934
    iput-object v8, v7, LX/4Y6;->i:Ljava/lang/String;

    .line 2515935
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;->e()Ljava/lang/String;

    move-result-object v8

    .line 2515936
    iput-object v8, v7, LX/4Y6;->n:Ljava/lang/String;

    .line 2515937
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;->aJ_()LX/1k1;

    move-result-object v8

    .line 2515938
    if-nez v8, :cond_a

    .line 2515939
    const/4 v9, 0x0

    .line 2515940
    :goto_e
    move-object v8, v9

    .line 2515941
    iput-object v8, v7, LX/4Y6;->p:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 2515942
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;->aI_()Ljava/lang/String;

    move-result-object v8

    .line 2515943
    iput-object v8, v7, LX/4Y6;->r:Ljava/lang/String;

    .line 2515944
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;->j()Ljava/lang/String;

    move-result-object v8

    .line 2515945
    iput-object v8, v7, LX/4Y6;->P:Ljava/lang/String;

    .line 2515946
    invoke-virtual {v7}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v7

    goto/16 :goto_5

    .line 2515947
    :cond_9
    new-instance v9, LX/4XY;

    invoke-direct {v9}, LX/4XY;-><init>()V

    .line 2515948
    invoke-virtual {v8}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel$CityModel;->a()Ljava/lang/String;

    move-result-object v10

    .line 2515949
    iput-object v10, v9, LX/4XY;->N:Ljava/lang/String;

    .line 2515950
    invoke-virtual {v9}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    goto :goto_d

    .line 2515951
    :cond_a
    new-instance v9, LX/4X8;

    invoke-direct {v9}, LX/4X8;-><init>()V

    .line 2515952
    invoke-interface {v8}, LX/1k1;->a()D

    move-result-wide v11

    .line 2515953
    iput-wide v11, v9, LX/4X8;->b:D

    .line 2515954
    invoke-interface {v8}, LX/1k1;->b()D

    move-result-wide v11

    .line 2515955
    iput-wide v11, v9, LX/4X8;->c:D

    .line 2515956
    invoke-virtual {v9}, LX/4X8;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v9

    goto :goto_e

    .line 2515957
    :cond_b
    new-instance v5, LX/4WD;

    invoke-direct {v5}, LX/4WD;-><init>()V

    .line 2515958
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;->a()I

    move-result v6

    .line 2515959
    iput v6, v5, LX/4WD;->b:I

    .line 2515960
    invoke-virtual {v5}, LX/4WD;->a()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v5

    goto/16 :goto_6

    .line 2515961
    :cond_c
    new-instance v7, LX/4WC;

    invoke-direct {v7}, LX/4WC;-><init>()V

    .line 2515962
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_e

    .line 2515963
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2515964
    const/4 v5, 0x0

    move v6, v5

    :goto_f
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_d

    .line 2515965
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;

    .line 2515966
    if-nez v5, :cond_f

    .line 2515967
    const/4 v9, 0x0

    .line 2515968
    :goto_10
    move-object v5, v9

    .line 2515969
    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2515970
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_f

    .line 2515971
    :cond_d
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2515972
    iput-object v5, v7, LX/4WC;->d:LX/0Px;

    .line 2515973
    :cond_e
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;->b()I

    move-result v5

    .line 2515974
    iput v5, v7, LX/4WC;->f:I

    .line 2515975
    invoke-virtual {v7}, LX/4WC;->a()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v5

    goto/16 :goto_7

    .line 2515976
    :cond_f
    new-instance v9, LX/3dL;

    invoke-direct {v9}, LX/3dL;-><init>()V

    .line 2515977
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    .line 2515978
    iput-object v10, v9, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2515979
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->c()Ljava/lang/String;

    move-result-object v10

    .line 2515980
    iput-object v10, v9, LX/3dL;->E:Ljava/lang/String;

    .line 2515981
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->d()Ljava/lang/String;

    move-result-object v10

    .line 2515982
    iput-object v10, v9, LX/3dL;->ag:Ljava/lang/String;

    .line 2515983
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel;->e()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;

    move-result-object v10

    .line 2515984
    if-nez v10, :cond_10

    .line 2515985
    const/4 v11, 0x0

    .line 2515986
    :goto_11
    move-object v10, v11

    .line 2515987
    iput-object v10, v9, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2515988
    invoke-virtual {v9}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    goto :goto_10

    .line 2515989
    :cond_10
    new-instance v11, LX/2dc;

    invoke-direct {v11}, LX/2dc;-><init>()V

    .line 2515990
    invoke-virtual {v10}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model$NodesModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2515991
    iput-object v5, v11, LX/2dc;->h:Ljava/lang/String;

    .line 2515992
    invoke-virtual {v11}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    goto :goto_11

    .line 2515993
    :cond_11
    new-instance v7, LX/4WD;

    invoke-direct {v7}, LX/4WD;-><init>()V

    .line 2515994
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_13

    .line 2515995
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2515996
    const/4 v5, 0x0

    move v6, v5

    :goto_12
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_12

    .line 2515997
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model$NodesModel;

    .line 2515998
    if-nez v5, :cond_14

    .line 2515999
    const/4 v9, 0x0

    .line 2516000
    :goto_13
    move-object v5, v9

    .line 2516001
    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2516002
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_12

    .line 2516003
    :cond_12
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2516004
    iput-object v5, v7, LX/4WD;->d:LX/0Px;

    .line 2516005
    :cond_13
    invoke-virtual {v4}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;->b()I

    move-result v5

    .line 2516006
    iput v5, v7, LX/4WD;->f:I

    .line 2516007
    invoke-virtual {v7}, LX/4WD;->a()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v5

    goto/16 :goto_8

    .line 2516008
    :cond_14
    new-instance v9, LX/3dL;

    invoke-direct {v9}, LX/3dL;-><init>()V

    .line 2516009
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    .line 2516010
    iput-object v10, v9, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2516011
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model$NodesModel;->c()Ljava/lang/String;

    move-result-object v10

    .line 2516012
    iput-object v10, v9, LX/3dL;->E:Ljava/lang/String;

    .line 2516013
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model$NodesModel;->d()Ljava/lang/String;

    move-result-object v10

    .line 2516014
    iput-object v10, v9, LX/3dL;->ag:Ljava/lang/String;

    .line 2516015
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model$NodesModel;->e()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model$NodesModel$ProfilePictureModel;

    move-result-object v10

    .line 2516016
    if-nez v10, :cond_15

    .line 2516017
    const/4 v11, 0x0

    .line 2516018
    :goto_14
    move-object v10, v11

    .line 2516019
    iput-object v10, v9, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2516020
    invoke-virtual {v9}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    goto :goto_13

    .line 2516021
    :cond_15
    new-instance v11, LX/2dc;

    invoke-direct {v11}, LX/2dc;-><init>()V

    .line 2516022
    invoke-virtual {v10}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model$NodesModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2516023
    iput-object v5, v11, LX/2dc;->h:Ljava/lang/String;

    .line 2516024
    invoke-virtual {v11}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    goto :goto_14

    .line 2516025
    :cond_16
    new-instance v5, LX/173;

    invoke-direct {v5}, LX/173;-><init>()V

    .line 2516026
    invoke-interface {v4}, LX/586;->a()Ljava/lang/String;

    move-result-object v6

    .line 2516027
    iput-object v6, v5, LX/173;->f:Ljava/lang/String;

    .line 2516028
    invoke-virtual {v5}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    goto/16 :goto_9
.end method
