.class public final enum LX/IGd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IGd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IGd;

.field public static final enum DELETE_PING:LX/IGd;

.field public static final enum GET_LOCATION:LX/IGd;

.field public static final enum OBTAIN_EXISTING_PING:LX/IGd;

.field public static final enum SEND_PING:LX/IGd;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2556160
    new-instance v0, LX/IGd;

    const-string v1, "OBTAIN_EXISTING_PING"

    invoke-direct {v0, v1, v2}, LX/IGd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IGd;->OBTAIN_EXISTING_PING:LX/IGd;

    .line 2556161
    new-instance v0, LX/IGd;

    const-string v1, "GET_LOCATION"

    invoke-direct {v0, v1, v3}, LX/IGd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IGd;->GET_LOCATION:LX/IGd;

    .line 2556162
    new-instance v0, LX/IGd;

    const-string v1, "SEND_PING"

    invoke-direct {v0, v1, v4}, LX/IGd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IGd;->SEND_PING:LX/IGd;

    .line 2556163
    new-instance v0, LX/IGd;

    const-string v1, "DELETE_PING"

    invoke-direct {v0, v1, v5}, LX/IGd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IGd;->DELETE_PING:LX/IGd;

    .line 2556164
    const/4 v0, 0x4

    new-array v0, v0, [LX/IGd;

    sget-object v1, LX/IGd;->OBTAIN_EXISTING_PING:LX/IGd;

    aput-object v1, v0, v2

    sget-object v1, LX/IGd;->GET_LOCATION:LX/IGd;

    aput-object v1, v0, v3

    sget-object v1, LX/IGd;->SEND_PING:LX/IGd;

    aput-object v1, v0, v4

    sget-object v1, LX/IGd;->DELETE_PING:LX/IGd;

    aput-object v1, v0, v5

    sput-object v0, LX/IGd;->$VALUES:[LX/IGd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2556159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IGd;
    .locals 1

    .prologue
    .line 2556158
    const-class v0, LX/IGd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IGd;

    return-object v0
.end method

.method public static values()[LX/IGd;
    .locals 1

    .prologue
    .line 2556157
    sget-object v0, LX/IGd;->$VALUES:[LX/IGd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IGd;

    return-object v0
.end method
