.class public LX/I81;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/events/model/Event;

.field private final b:Landroid/content/Context;

.field private final c:LX/1nQ;

.field private final d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/events/model/Event;Landroid/content/Context;LX/1nQ;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # Lcom/facebook/events/model/Event;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/model/Event;",
            "Landroid/content/Context;",
            "LX/1nQ;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2540539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540540
    iput-object p2, p0, LX/I81;->b:Landroid/content/Context;

    .line 2540541
    iput-object p1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540542
    iput-object p3, p0, LX/I81;->c:LX/1nQ;

    .line 2540543
    iput-object p4, p0, LX/I81;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2540544
    iput-object p5, p0, LX/I81;->e:LX/0Or;

    .line 2540545
    iput-object p6, p0, LX/I81;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2540546
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2540532
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/I81;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2540533
    const-string v1, "event_id"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540534
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2540535
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2540536
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_NOTIFICATION_SETTINGS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2540537
    iget-object v1, p0, LX/I81;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/I81;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2540538
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2540547
    iget-object v0, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_1

    .line 2540548
    :cond_0
    :goto_0
    return-void

    .line 2540549
    :cond_1
    iget-object v0, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540550
    iget-object v1, v0, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    move-object v0, v1

    .line 2540551
    if-eqz v0, :cond_0

    .line 2540552
    iget-object v0, p0, LX/I81;->b:Landroid/content/Context;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2540553
    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540554
    iget-object v2, v1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2540555
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540556
    iget-object v3, v1, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    move-object v1, v3

    .line 2540557
    invoke-static {v1, p1, p2}, LX/1H1;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-static {v2, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 2540558
    iget-object v0, p0, LX/I81;->b:Landroid/content/Context;

    iget-object v1, p0, LX/I81;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081cf5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2540559
    :cond_2
    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540560
    iget-object v3, v1, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    move-object v1, v3

    .line 2540561
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2540525
    iget-object v0, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_0

    .line 2540526
    :goto_0
    return-void

    .line 2540527
    :cond_0
    const-string v0, "/report/id/?fbid=%s"

    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540528
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2540529
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2540530
    sget-object v1, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2540531
    iget-object v1, p0, LX/I81;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/I81;->b:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2540477
    iget-object v0, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_0

    .line 2540478
    :goto_0
    return-void

    .line 2540479
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "allDay"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540480
    iget-boolean v3, v2, Lcom/facebook/events/model/Event;->N:Z

    move v2, v3

    .line 2540481
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "beginTime"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540482
    iget-object v3, v2, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2540483
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "availability"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2540484
    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540485
    iget-object v2, v1, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v1, v2

    .line 2540486
    if-eqz v1, :cond_6

    .line 2540487
    const-string v1, "eventLocation"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540488
    iget-object v3, v2, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v2, v3

    .line 2540489
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2540490
    :cond_1
    :goto_1
    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540491
    iget-object v2, v1, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v1, v2

    .line 2540492
    if-eqz v1, :cond_2

    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540493
    iget-object v2, v1, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v1, v2

    .line 2540494
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2540495
    const-string v1, "description"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540496
    iget-object v3, v2, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v2, v3

    .line 2540497
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2540498
    :cond_2
    const-string v1, "beginTime"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2540499
    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540500
    iget-object v2, v1, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v1, v2

    .line 2540501
    if-eqz v1, :cond_3

    .line 2540502
    const-string v1, "eventTimezone"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540503
    iget-object v3, v2, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v2, v3

    .line 2540504
    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2540505
    :cond_3
    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2540506
    const-string v1, "endTime"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->O()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2540507
    :cond_4
    const-string v1, "guestsCanInviteOthers"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540508
    iget-boolean v3, v2, Lcom/facebook/events/model/Event;->h:Z

    move v2, v3

    .line 2540509
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2540510
    const-string v1, "eventColor"

    iget-object v2, p0, LX/I81;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2540511
    iget-object v1, p0, LX/I81;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/I81;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2540512
    iget-object v0, p0, LX/I81;->c:LX/1nQ;

    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540513
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2540514
    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    .line 2540515
    iget-object v3, v0, LX/1nQ;->i:LX/0Zb;

    const-string v4, "event_export_to_calendar_click"

    const/4 p0, 0x0

    invoke-interface {v3, v4, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2540516
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2540517
    const-string v4, "mechanism"

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v3

    const-string v4, "event_permalink"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    iget-object v4, v0, LX/1nQ;->j:LX/0kv;

    iget-object p0, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v4, p0}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "Event"

    invoke-virtual {v3, v4}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2540518
    :cond_5
    goto/16 :goto_0

    .line 2540519
    :cond_6
    iget-object v1, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540520
    iget-object v2, v1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v1, v2

    .line 2540521
    if-eqz v1, :cond_1

    .line 2540522
    const-string v1, "eventLocation"

    iget-object v2, p0, LX/I81;->a:Lcom/facebook/events/model/Event;

    .line 2540523
    iget-object v3, v2, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v2, v3

    .line 2540524
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1
.end method
