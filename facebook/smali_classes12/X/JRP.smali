.class public LX/JRP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:LX/1nA;

.field public final b:LX/17V;

.field private final c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/17V;LX/1nA;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;I)V
    .locals 8
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17V;",
            "LX/1nA;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693280
    iput-object p1, p0, LX/JRP;->b:LX/17V;

    .line 2693281
    iput-object p2, p0, LX/JRP;->a:LX/1nA;

    .line 2693282
    const/4 v7, 0x0

    .line 2693283
    if-nez p3, :cond_0

    move-object v1, v7

    .line 2693284
    :goto_0
    move-object v0, v1

    .line 2693285
    iput-object v0, p0, LX/JRP;->c:Landroid/view/View$OnClickListener;

    .line 2693286
    return-void

    .line 2693287
    :cond_0
    iget-object v1, p0, LX/JRP;->b:LX/17V;

    .line 2693288
    iget-object v2, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2693289
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v3

    invoke-static {p3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    const-string v5, "native_newsfeed"

    move-object v2, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2693290
    if-eqz v1, :cond_1

    .line 2693291
    const-string v2, "cta_click"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2693292
    :cond_1
    iget-object v2, p0, LX/JRP;->a:LX/1nA;

    invoke-virtual {v2, p4, v1, v7}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/Map;)Landroid/view/View$OnClickListener;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6ae7281b

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2693293
    iget-object v1, p0, LX/JRP;->c:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_0

    .line 2693294
    iget-object v1, p0, LX/JRP;->c:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2693295
    :cond_0
    const v1, 0x3b9331ce

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
