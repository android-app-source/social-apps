.class public final enum LX/I9a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I9a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I9a;

.field public static final enum EDIT:LX/I9a;

.field public static final enum MESSAGE:LX/I9a;


# instance fields
.field private final mGuestButtonColorResId:I

.field private final mGuestButtonDescriptionResId:I

.field private final mGuestButtonDrawableResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    const v4, -0x423e37

    .line 2543304
    new-instance v0, LX/I9a;

    const-string v1, "EDIT"

    const v3, 0x7f020952

    const v5, 0x7f081f42

    invoke-direct/range {v0 .. v5}, LX/I9a;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, LX/I9a;->EDIT:LX/I9a;

    .line 2543305
    new-instance v5, LX/I9a;

    const-string v6, "MESSAGE"

    const v8, 0x7f020740

    const v10, 0x7f081f40

    move v9, v4

    invoke-direct/range {v5 .. v10}, LX/I9a;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, LX/I9a;->MESSAGE:LX/I9a;

    .line 2543306
    const/4 v0, 0x2

    new-array v0, v0, [LX/I9a;

    sget-object v1, LX/I9a;->EDIT:LX/I9a;

    aput-object v1, v0, v2

    sget-object v1, LX/I9a;->MESSAGE:LX/I9a;

    aput-object v1, v0, v7

    sput-object v0, LX/I9a;->$VALUES:[LX/I9a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 2543299
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2543300
    iput p3, p0, LX/I9a;->mGuestButtonDrawableResId:I

    .line 2543301
    iput p4, p0, LX/I9a;->mGuestButtonColorResId:I

    .line 2543302
    iput p5, p0, LX/I9a;->mGuestButtonDescriptionResId:I

    .line 2543303
    return-void
.end method

.method public static getEventGuestListButton(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)LX/I9a;
    .locals 2

    .prologue
    .line 2543293
    if-eqz p1, :cond_0

    .line 2543294
    sget-object v0, LX/I9a;->EDIT:LX/I9a;

    .line 2543295
    :goto_0
    return-object v0

    .line 2543296
    :cond_0
    sget-object v0, LX/I9Z;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2543297
    const/4 v0, 0x0

    goto :goto_0

    .line 2543298
    :pswitch_0
    sget-object v0, LX/I9a;->MESSAGE:LX/I9a;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/I9a;
    .locals 1

    .prologue
    .line 2543292
    const-class v0, LX/I9a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I9a;

    return-object v0
.end method

.method public static values()[LX/I9a;
    .locals 1

    .prologue
    .line 2543291
    sget-object v0, LX/I9a;->$VALUES:[LX/I9a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I9a;

    return-object v0
.end method


# virtual methods
.method public final getGuestButtonColorResId()I
    .locals 1

    .prologue
    .line 2543290
    iget v0, p0, LX/I9a;->mGuestButtonColorResId:I

    return v0
.end method

.method public final getGuestButtonDescriptionResId()I
    .locals 1

    .prologue
    .line 2543289
    iget v0, p0, LX/I9a;->mGuestButtonDescriptionResId:I

    return v0
.end method

.method public final getGuestButtonDrawableResId()I
    .locals 1

    .prologue
    .line 2543288
    iget v0, p0, LX/I9a;->mGuestButtonDrawableResId:I

    return v0
.end method
