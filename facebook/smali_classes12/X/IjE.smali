.class public LX/IjE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6yS;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/03V;

.field private c:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605493
    iput-object p1, p0, LX/IjE;->a:Landroid/content/Context;

    .line 2605494
    iput-object p2, p0, LX/IjE;->b:LX/03V;

    .line 2605495
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6E6;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2605478
    check-cast p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    .line 2605479
    new-instance v1, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    iget-object v0, p0, LX/IjE;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;-><init>(Landroid/content/Context;)V

    .line 2605480
    iget-boolean v0, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    if-eqz v0, :cond_0

    .line 2605481
    const v0, 0x7f082c09

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheader(I)V

    .line 2605482
    invoke-virtual {v1, v3}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheaderVisibility(I)V

    .line 2605483
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setHeaderVisibility(I)V

    .line 2605484
    :goto_0
    iget-object v0, p0, LX/IjE;->c:LX/6qh;

    invoke-virtual {v1, v0}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 2605485
    return-object v1

    .line 2605486
    :cond_0
    iget-object v0, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setHeader(Ljava/lang/CharSequence;)V

    .line 2605487
    iget-object v0, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->d:Ljava/lang/String;

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheader(Ljava/lang/CharSequence;)V

    .line 2605488
    invoke-virtual {v1, v3}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setHeaderVisibility(I)V

    .line 2605489
    invoke-virtual {v1, v3}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheaderVisibility(I)V

    goto :goto_0

    .line 2605490
    :cond_1
    iget-object v0, p0, LX/IjE;->a:Landroid/content/Context;

    const v2, 0x7f082c07

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2605491
    :cond_2
    iget-object v0, p0, LX/IjE;->a:Landroid/content/Context;

    const v2, 0x7f082c08

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 2605496
    iput-object p1, p0, LX/IjE;->c:LX/6qh;

    .line 2605497
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6E6;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2605464
    check-cast p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    .line 2605465
    sget-object v0, LX/5g0;->NUX:LX/5g0;

    iget-object v0, v0, LX/5g0;->analyticsModule:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2605466
    new-instance v0, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;

    iget-object v1, p0, LX/IjE;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;-><init>(Landroid/content/Context;)V

    .line 2605467
    iget-object v1, p0, LX/IjE;->c:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 2605468
    :goto_0
    return-object v0

    .line 2605469
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    iget-object v2, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2605470
    iget-object v0, p0, LX/IjE;->b:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Null senderName or transactionId received when in a receive nux flow."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2605471
    const/4 v0, 0x0

    goto :goto_0

    .line 2605472
    :cond_1
    new-instance v0, LX/IjZ;

    iget-object v1, p0, LX/IjE;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/IjZ;-><init>(Landroid/content/Context;)V

    .line 2605473
    iget-object v1, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->a:Ljava/lang/String;

    iget-object v2, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->b:Ljava/lang/String;

    .line 2605474
    iget-object p1, v0, LX/IjZ;->a:Lcom/facebook/resources/ui/FbTextView;

    new-instance p2, LX/IjY;

    invoke-direct {p2, v0, v1, v2}, LX/IjY;-><init>(LX/IjZ;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2605475
    iget-object v1, p0, LX/IjE;->c:LX/6qh;

    .line 2605476
    iput-object v1, v0, LX/73V;->a:LX/6qh;

    .line 2605477
    goto :goto_0
.end method
