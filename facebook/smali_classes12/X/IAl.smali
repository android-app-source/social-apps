.class public final LX/IAl;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;)V
    .locals 0

    .prologue
    .line 2546087
    iput-object p1, p0, LX/IAl;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 5

    .prologue
    .line 2546088
    iget-object v0, p0, LX/IAl;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->l:LX/IAh;

    .line 2546089
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v3

    .line 2546090
    iget-object v1, v0, LX/IAh;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p1, :cond_0

    iget-object v1, v0, LX/IAh;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IAg;

    .line 2546091
    iget-object p2, v1, LX/IAg;->d:Ljava/util/HashSet;

    invoke-static {p2}, LX/0Ph;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object p2

    move-object v1, p2

    .line 2546092
    invoke-virtual {v3, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 2546093
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2546094
    :cond_0
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    move-object v1, v1

    .line 2546095
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2546096
    iget-object v0, p0, LX/IAl;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    iget-boolean v0, v0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->f:Z

    if-eqz v0, :cond_2

    .line 2546097
    iget-object v0, p0, LX/IAl;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IAd;

    iget-object v2, p0, LX/IAl;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, LX/IAl;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const/4 p1, 0x0

    .line 2546098
    iget-object v4, v0, LX/IAd;->f:LX/1Ck;

    const-string p0, "create_group_thread"

    invoke-virtual {v4, p0}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2546099
    :cond_1
    :goto_1
    return-void

    .line 2546100
    :cond_2
    iget-object v0, p0, LX/IAl;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->a(Ljava/util/Set;)V

    goto :goto_1

    .line 2546101
    :cond_3
    iget-object v4, v0, LX/IAd;->i:Landroid/support/v4/app/DialogFragment;

    if-nez v4, :cond_4

    .line 2546102
    const v4, 0x7f081f6e

    const/4 p0, 0x1

    invoke-static {v4, p0, p1, p1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v4

    iput-object v4, v0, LX/IAd;->i:Landroid/support/v4/app/DialogFragment;

    .line 2546103
    :cond_4
    iget-object v4, v0, LX/IAd;->i:Landroid/support/v4/app/DialogFragment;

    sget-object p0, LX/IAd;->a:Ljava/lang/String;

    invoke-virtual {v4, v3, p0}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2546104
    iget-object v4, v0, LX/IAd;->f:LX/1Ck;

    const-string p0, "create_group_thread"

    new-instance p1, LX/IAb;

    invoke-direct {p1, v0, v1}, LX/IAb;-><init>(LX/IAd;Ljava/util/Set;)V

    new-instance p2, LX/IAc;

    invoke-direct {p2, v0, v2}, LX/IAc;-><init>(LX/IAd;Landroid/app/Activity;)V

    invoke-virtual {v4, p0, p1, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1
.end method
