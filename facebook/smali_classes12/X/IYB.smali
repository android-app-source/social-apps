.class public final LX/IYB;
.super LX/ChT;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/instantarticles/view/ShareBar;


# direct methods
.method public constructor <init>(Lcom/facebook/instantarticles/view/ShareBar;)V
    .locals 0

    .prologue
    .line 2587400
    iput-object p1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-direct {p0}, LX/ChT;-><init>()V

    return-void
.end method

.method private b()V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x0

    .line 2587401
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308c8

    iget-object v2, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2587402
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    const v2, 0x7f0d16d2

    invoke-virtual {v0, v2}, Lcom/facebook/instantarticles/view/ShareBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2587403
    iput-object v0, v1, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    .line 2587404
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021665

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2587405
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0626

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v0, v1}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2587406
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021665

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2587407
    iget-object v2, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v2}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0625

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v1, v2}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2587408
    iget-object v2, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2587409
    iget-object v2, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    new-instance v4, LX/IY2;

    invoke-direct {v4, p0, v1, v0}, LX/IY2;-><init>(LX/IYB;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2587410
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    new-instance v1, LX/IY3;

    invoke-direct {v1, p0}, LX/IY3;-><init>(LX/IYB;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2587411
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    const v2, 0x7f0d16d3

    invoke-virtual {v0, v2}, Lcom/facebook/instantarticles/view/ShareBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2587412
    iput-object v0, v1, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    .line 2587413
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/ShareBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    if-eqz v0, :cond_4

    .line 2587414
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ck0;

    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    const v2, 0x7f0d0154

    const v4, 0x7f0d0154

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 2587415
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ck0;

    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, v1, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    const v2, 0x7f0d0157

    const v4, 0x7f0d0158

    invoke-virtual {v0, v1, v2, v4}, LX/Ck0;->a(Landroid/view/View;II)V

    .line 2587416
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, v0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v2, 0x7f0d0158

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v3, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(IF)V

    .line 2587417
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v2, 0x7f0d0156

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    iput v0, v1, Lcom/facebook/instantarticles/view/ShareBar;->p:I

    .line 2587418
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v2, 0x7f0d0155

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    iput v0, v1, Lcom/facebook/instantarticles/view/ShareBar;->s:I

    .line 2587419
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2587420
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget v1, v1, Lcom/facebook/instantarticles/view/ShareBar;->D:I

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2587421
    iget-object v2, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/instantarticles/view/ShareBar;->measure(II)V

    .line 2587422
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, v1, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getTextSize()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Lcom/facebook/instantarticles/view/ShareBar;->o:I

    .line 2587423
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, v1, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    iput v1, v0, Lcom/facebook/instantarticles/view/ShareBar;->q:I

    .line 2587424
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    .line 2587425
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v2, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget v2, v2, Lcom/facebook/instantarticles/view/ShareBar;->q:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    iput v0, v1, Lcom/facebook/instantarticles/view/ShareBar;->r:F

    .line 2587426
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v2, 0x7f0d0153

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    iput v0, v1, Lcom/facebook/instantarticles/view/ShareBar;->u:I

    .line 2587427
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v2, 0x7f0d0154

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    iput v0, v1, Lcom/facebook/instantarticles/view/ShareBar;->t:I

    .line 2587428
    :goto_0
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/IY4;

    invoke-direct {v1, p0}, LX/IY4;-><init>(LX/IYB;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2587429
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587430
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/instantarticles/view/ShareBar;->setLayoutDirection(I)V

    .line 2587431
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0, v7}, Lcom/facebook/instantarticles/view/ShareBar;->setTextDirection(I)V

    .line 2587432
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/widget/text/BetterTextView;->setTextDirection(I)V

    .line 2587433
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 2587434
    :cond_0
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b128c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2587435
    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b128d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2587436
    iget-object v2, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2587437
    iget-object v2, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v1, v0, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2587438
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14x;

    invoke-virtual {v0}, LX/14x;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14x;

    invoke-virtual {v0}, LX/14x;->d()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-boolean v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->E:Z

    if-nez v0, :cond_3

    .line 2587439
    :cond_2
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2587440
    :cond_3
    return-void

    .line 2587441
    :cond_4
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ck0;

    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    const v2, 0x7f0d0153

    const v4, 0x7f0d0153

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 2587442
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ck0;

    iget-object v1, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, v1, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    const v2, 0x7f0d0155

    const v4, 0x7f0d0156

    invoke-virtual {v0, v1, v2, v4}, LX/Ck0;->a(Landroid/view/View;II)V

    .line 2587443
    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, v0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, p0, LX/IYB;->a:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v2, 0x7f0d0156

    invoke-interface {v0, v2}, LX/Cju;->c(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v3, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(IF)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2587444
    invoke-direct {p0}, LX/IYB;->b()V

    return-void
.end method
