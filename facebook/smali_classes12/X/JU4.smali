.class public final LX/JU4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DHx;


# instance fields
.field public final synthetic a:LX/JUH;

.field public final synthetic b:LX/JTs;

.field public final synthetic c:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;LX/JUH;LX/JTs;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2697581
    iput-object p1, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iput-object p2, p0, LX/JU4;->a:LX/JUH;

    iput-object p3, p0, LX/JU4;->b:LX/JTs;

    iput-object p4, p0, LX/JU4;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    iput-object p5, p0, LX/JU4;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V
    .locals 5

    .prologue
    .line 2697582
    iget-object v0, p0, LX/JU4;->a:LX/JUH;

    iget-object v0, v0, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-static {v0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    .line 2697583
    sget-object v1, LX/JU5;->a:[I

    iget-object v2, p0, LX/JU4;->b:LX/JTs;

    .line 2697584
    iget-object v3, v2, LX/JTs;->a:LX/3OL;

    move-object v2, v3

    .line 2697585
    invoke-virtual {v2}, LX/3OL;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2697586
    iget-object v0, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->d:LX/JUO;

    iget-object v1, p0, LX/JU4;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2697587
    iget-object v2, v0, LX/JUO;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2697588
    iget-object v0, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    invoke-virtual {p1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/JU4;->d:Ljava/lang/String;

    .line 2697589
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2697590
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v3, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2697591
    const/high16 p0, 0x10000000

    invoke-virtual {v3, p0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2697592
    iget-object p0, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->g:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2697593
    :goto_0
    return-void

    .line 2697594
    :pswitch_0
    iget-object v1, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->d:LX/JUO;

    invoke-virtual {v1, v0}, LX/JUO;->a(Ljava/lang/String;)V

    .line 2697595
    iget-object v1, p0, LX/JU4;->b:LX/JTs;

    sget-object v2, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    .line 2697596
    iput-object v2, v1, LX/JTs;->a:LX/3OL;

    .line 2697597
    iget-object v1, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->e:LX/3LX;

    new-instance v2, LX/JU1;

    invoke-direct {v2, p0, p1}, LX/JU1;-><init>(LX/JU4;Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    invoke-virtual {v1, v0, v2}, LX/3LX;->a(Ljava/lang/String;LX/DI1;)V

    goto :goto_0

    .line 2697598
    :pswitch_1
    iget-object v1, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->d:LX/JUO;

    invoke-virtual {v1, v0}, LX/JUO;->a(Ljava/lang/String;)V

    .line 2697599
    iget-object v1, p0, LX/JU4;->b:LX/JTs;

    sget-object v2, LX/3OL;->INTERACTED:LX/3OL;

    .line 2697600
    iput-object v2, v1, LX/JTs;->a:LX/3OL;

    .line 2697601
    iget-object v1, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->e:LX/3LX;

    new-instance v2, LX/JU2;

    invoke-direct {v2, p0, p1}, LX/JU2;-><init>(LX/JU4;Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    invoke-virtual {v1, v0, v2}, LX/3LX;->a(Ljava/lang/String;LX/DI1;)V

    goto :goto_0

    .line 2697602
    :pswitch_2
    iget-object v1, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->d:LX/JUO;

    .line 2697603
    iget-object v2, v1, LX/JUO;->a:LX/0Zb;

    const-string v3, "friends_nearby_feedunit_wave"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2697604
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2697605
    const-string v3, "native_newsfeed"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "target_id"

    invoke-virtual {v2, v3, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "action"

    const-string v4, "wave_canceled"

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2697606
    :cond_0
    iget-object v1, p0, LX/JU4;->b:LX/JTs;

    sget-object v2, LX/3OL;->NOT_SENT:LX/3OL;

    .line 2697607
    iput-object v2, v1, LX/JTs;->a:LX/3OL;

    .line 2697608
    iget-object v1, p0, LX/JU4;->e:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->e:LX/3LX;

    new-instance v2, LX/JU3;

    invoke-direct {v2, p0, p1}, LX/JU3;-><init>(LX/JU4;Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    invoke-virtual {v1, v0, v2}, LX/3LX;->b(Ljava/lang/String;LX/DI1;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
