.class public final LX/I1J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;)V
    .locals 0

    .prologue
    .line 2528855
    iput-object p1, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v0, 0x1

    const v1, 0x5cb80db8

    invoke-static {v11, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2528852
    iget-object v0, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v7, v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->w:LX/I53;

    iget-object v0, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v8, v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->o:Ljava/lang/String;

    iget-object v0, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v9, v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->q:LX/0m9;

    iget-object v0, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v10, v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->p:Ljava/lang/String;

    new-instance v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v1, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v1, v1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->v:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v2, v2, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->v:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    iget-object v3, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v3, v3, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->v:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_CATEGORY_ROW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v4}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v7, v8, v9, v10, v0}, LX/I53;->a(Ljava/lang/String;LX/0m9;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2528853
    iget-object v0, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->x:LX/1nQ;

    iget-object v1, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v1, v1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->v:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_CATEGORIES:Lcom/facebook/events/common/ActionMechanism;

    iget-object v3, p0, LX/I1J;->a:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    iget-object v3, v3, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/1nQ;->a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)V

    .line 2528854
    const v0, -0x4fa9cdeb

    invoke-static {v11, v11, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
