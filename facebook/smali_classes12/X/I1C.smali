.class public final LX/I1C;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I18;

.field public final synthetic b:LX/I1G;


# direct methods
.method public constructor <init>(LX/I1G;LX/I18;)V
    .locals 0

    .prologue
    .line 2528748
    iput-object p1, p0, LX/I1C;->b:LX/I1G;

    iput-object p2, p0, LX/I1C;->a:LX/I18;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2528749
    iget-object v0, p0, LX/I1C;->a:LX/I18;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/I18;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 2528750
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2528751
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2528752
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528753
    check-cast v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;

    .line 2528754
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->a()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->a()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->j()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2528755
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->a()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->j()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    move-result-object v2

    .line 2528756
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2528757
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2528758
    const/4 v4, 0x3

    invoke-virtual {v3, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    .line 2528759
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2528760
    const/4 v3, 0x2

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2528761
    :goto_1
    iget-object v3, p0, LX/I1C;->a:LX/I18;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;->a()LX/0Px;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, v1, v0}, LX/I18;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 2528762
    :goto_3
    return-void

    .line 2528763
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 2528764
    :cond_2
    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_2

    .line 2528765
    :cond_3
    iget-object v0, p0, LX/I1C;->a:LX/I18;

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v2, v1}, LX/I18;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_3
.end method
