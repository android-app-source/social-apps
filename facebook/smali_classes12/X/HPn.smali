.class public LX/HPn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final b:LX/3fB;

.field public final c:LX/9XE;

.field public final d:LX/1Ck;

.field public final e:LX/HPf;

.field public final f:LX/HPd;

.field public g:LX/145;

.field private h:Ljava/lang/String;

.field public i:J

.field public j:LX/89z;

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:LX/HPa;

.field public o:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/HPl;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3fB;LX/9XE;LX/1Ck;LX/HPf;LX/HPd;LX/145;Ljava/lang/Long;LX/89z;Ljava/lang/String;)V
    .locals 4
    .param p7    # LX/145;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/89z;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2462478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2462479
    iput-boolean v0, p0, LX/HPn;->k:Z

    .line 2462480
    iput-boolean v0, p0, LX/HPn;->l:Z

    .line 2462481
    iput-boolean v0, p0, LX/HPn;->m:Z

    .line 2462482
    sget-object v0, LX/HPl;->INSTEAD_OF_CACHE_HIT:LX/HPl;

    iput-object v0, p0, LX/HPn;->s:LX/HPl;

    .line 2462483
    iput-object p1, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2462484
    iput-object p2, p0, LX/HPn;->b:LX/3fB;

    .line 2462485
    iput-object p3, p0, LX/HPn;->c:LX/9XE;

    .line 2462486
    iput-object p4, p0, LX/HPn;->d:LX/1Ck;

    .line 2462487
    iput-object p5, p0, LX/HPn;->e:LX/HPf;

    .line 2462488
    iput-object p6, p0, LX/HPn;->f:LX/HPd;

    .line 2462489
    iput-object p7, p0, LX/HPn;->g:LX/145;

    .line 2462490
    invoke-virtual {p8}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/HPn;->i:J

    .line 2462491
    iput-object p9, p0, LX/HPn;->j:LX/89z;

    .line 2462492
    iput-object p10, p0, LX/HPn;->h:Ljava/lang/String;

    .line 2462493
    new-instance v0, LX/HPa;

    iget-wide v2, p0, LX/HPn;->i:J

    invoke-direct {v0, v2, v3}, LX/HPa;-><init>(J)V

    iput-object v0, p0, LX/HPn;->n:LX/HPa;

    .line 2462494
    return-void
.end method

.method private a(LX/0ta;)V
    .locals 8

    .prologue
    const v7, 0x130080

    const v6, 0x13007a

    .line 2462495
    const-string v0, "HeaderFromEarlyFetcher"

    iget-boolean v1, p0, LX/HPn;->k:Z

    if-eqz v1, :cond_3

    const-string v1, "1"

    :goto_0
    const-string v2, "SurfaceFirstCardFromEarlyFetcher"

    iget-boolean v3, p0, LX/HPn;->l:Z

    if-eqz v3, :cond_4

    const-string v3, "1"

    :goto_1
    const-string v4, "SurfaceFirstCardCachedWithEarlyFetcher"

    iget-boolean v5, p0, LX/HPn;->m:Z

    if-eqz v5, :cond_5

    const-string v5, "1"

    :goto_2
    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    .line 2462496
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {p1, v0}, LX/0ta;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2462497
    iget-object v0, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "from_network"

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2462498
    iget-object v0, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HPn;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x18

    invoke-interface {v0, v7, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2462499
    iget-object v0, p0, LX/HPn;->b:LX/3fB;

    const-string v1, "FetchPageHeaderDataFromServer"

    invoke-virtual {v0, v1}, LX/3fB;->b(Ljava/lang/String;)LX/3fB;

    .line 2462500
    iget-object v0, p0, LX/HPn;->b:LX/3fB;

    const-string v1, "FetchPageHeaderDataFromCache"

    invoke-virtual {v0, v1}, LX/3fB;->e(Ljava/lang/String;)LX/3fB;

    .line 2462501
    :goto_3
    iget-boolean v0, p0, LX/HPn;->k:Z

    if-eqz v0, :cond_0

    .line 2462502
    iget-object v0, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "early_fetch"

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2462503
    :cond_0
    iget-boolean v0, p0, LX/HPn;->l:Z

    if-eqz v0, :cond_1

    .line 2462504
    iget-object v0, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "surface_first_card_early_fetch"

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2462505
    :cond_1
    iget-boolean v0, p0, LX/HPn;->m:Z

    if-eqz v0, :cond_2

    .line 2462506
    iget-object v0, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "surface_first_card_cached_early_fetch"

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2462507
    :cond_2
    iget-object v0, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2462508
    return-void

    .line 2462509
    :cond_3
    const-string v1, "0"

    goto :goto_0

    :cond_4
    const-string v3, "0"

    goto :goto_1

    :cond_5
    const-string v5, "0"

    goto :goto_2

    .line 2462510
    :cond_6
    iget-object v0, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "from_cache"

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2462511
    iget-object v0, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HPn;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x22

    invoke-interface {v0, v7, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2462512
    iget-object v0, p0, LX/HPn;->b:LX/3fB;

    const-string v1, "FetchPageHeaderDataFromServer"

    invoke-virtual {v0, v1}, LX/3fB;->e(Ljava/lang/String;)LX/3fB;

    .line 2462513
    iget-object v0, p0, LX/HPn;->b:LX/3fB;

    const-string v1, "FetchPageHeaderDataFromCache"

    invoke-virtual {v0, v1}, LX/3fB;->b(Ljava/lang/String;)LX/3fB;

    goto :goto_3
.end method

.method public static a(LX/HPn;Z)V
    .locals 4

    .prologue
    .line 2462476
    iget-object v0, p0, LX/HPn;->d:LX/1Ck;

    sget-object v1, LX/8Dp;->FETCH_PAGES_HEADER_NOCACHE:LX/8Dp;

    new-instance v2, LX/HPi;

    invoke-direct {v2, p0}, LX/HPi;-><init>(LX/HPn;)V

    new-instance v3, LX/HPj;

    invoke-direct {v3, p0, p1}, LX/HPj;-><init>(LX/HPn;Z)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2462477
    return-void
.end method

.method public static a$redex0(LX/HPn;Lcom/facebook/graphql/executor/GraphQLResult;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2462514
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2462515
    invoke-direct {p0, v0}, LX/HPn;->a(LX/0ta;)V

    .line 2462516
    const/4 v0, 0x0

    iput-object v0, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462517
    iget-object v0, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2462518
    iget-object v0, p0, LX/HPn;->c:LX/9XE;

    iget-wide v2, p0, LX/HPn;->i:J

    iget-object v1, p0, LX/HPn;->j:LX/89z;

    .line 2462519
    iget-object v4, v0, LX/9XE;->a:LX/0Zb;

    sget-object v5, LX/9XB;->EVENT_NETWORK_LOADED_BEFORE_CACHE:LX/9XB;

    invoke-static {v5, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "ref"

    iget-object v7, v1, LX/89z;->loggingName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2462520
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462521
    iget-object v0, p0, LX/HPn;->d:LX/1Ck;

    sget-object v1, LX/8Dp;->FETCH_PAGES_HEADER:LX/8Dp;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2462522
    if-eqz p2, :cond_1

    .line 2462523
    iget-object v0, p0, LX/HPn;->c:LX/9XE;

    iget-wide v2, p0, LX/HPn;->i:J

    sget-object v1, LX/89y;->HEADER:LX/89y;

    iget-object v4, p0, LX/HPn;->j:LX/89z;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/9XE;->a(JLX/89y;LX/89z;)V

    .line 2462524
    :cond_1
    iget-object v0, p0, LX/HPn;->g:LX/145;

    .line 2462525
    sget-object v1, LX/HPk;->b:[I

    iget-object v2, p0, LX/HPn;->s:LX/HPl;

    invoke-virtual {v2}, LX/HPl;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2462526
    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 2462527
    invoke-interface {v0, p1, v1}, LX/145;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/HPm;)V

    .line 2462528
    return-void

    .line 2462529
    :pswitch_0
    sget-object v1, LX/HPm;->DEFAULT:LX/HPm;

    goto :goto_0

    .line 2462530
    :pswitch_1
    sget-object v1, LX/HPm;->FORCED_SERVER_AFTER_CACHE_HIT:LX/HPm;

    goto :goto_0

    .line 2462531
    :pswitch_2
    sget-object v1, LX/HPm;->FORCED_BY_USER:LX/HPm;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(LX/HPn;Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2462464
    const/4 v0, 0x0

    iput-object v0, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462465
    if-eqz p1, :cond_0

    .line 2462466
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2462467
    sget-object v1, LX/0ta;->NO_DATA:LX/0ta;

    if-ne v0, v1, :cond_1

    .line 2462468
    :cond_0
    const/4 v0, 0x0

    .line 2462469
    :goto_0
    return v0

    .line 2462470
    :cond_1
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2462471
    invoke-direct {p0, v0}, LX/HPn;->a(LX/0ta;)V

    .line 2462472
    sget-object v0, LX/HPl;->AFTER_CACHE_HIT:LX/HPl;

    iput-object v0, p0, LX/HPn;->s:LX/HPl;

    .line 2462473
    iget-object v0, p0, LX/HPn;->c:LX/9XE;

    iget-wide v2, p0, LX/HPn;->i:J

    sget-object v1, LX/89y;->HEADER:LX/89y;

    iget-object v4, p0, LX/HPn;->j:LX/89z;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/9XE;->a(JLX/89y;LX/89z;)V

    .line 2462474
    iget-object v0, p0, LX/HPn;->g:LX/145;

    sget-object v1, LX/HPm;->DEFAULT:LX/HPm;

    invoke-interface {v0, p1, v1}, LX/145;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/HPm;)V

    .line 2462475
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2462462
    iget-object v0, p0, LX/HPn;->d:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2462463
    return-void
.end method

.method public final a(LX/HPm;)V
    .locals 13

    .prologue
    .line 2462412
    sget-object v0, LX/HPk;->a:[I

    invoke-virtual {p1}, LX/HPm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2462413
    :goto_0
    return-void

    .line 2462414
    :pswitch_0
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2462415
    iget-object v2, p0, LX/HPn;->b:LX/3fB;

    const-string v3, "FetchPageHeaderDataFromServer"

    invoke-virtual {v2, v3}, LX/3fB;->a(Ljava/lang/String;)LX/3fB;

    .line 2462416
    iget-object v2, p0, LX/HPn;->b:LX/3fB;

    const-string v3, "FetchPageHeaderDataFromCache"

    invoke-virtual {v2, v3}, LX/3fB;->a(Ljava/lang/String;)LX/3fB;

    .line 2462417
    iget-object v2, p0, LX/HPn;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x13007a

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2462418
    iget-object v2, p0, LX/HPn;->c:LX/9XE;

    iget-wide v6, p0, LX/HPn;->i:J

    iget-object v3, p0, LX/HPn;->j:LX/89z;

    .line 2462419
    iget-object v8, v2, LX/9XE;->a:LX/0Zb;

    sget-object v9, LX/9X9;->EVENT_PAGE_DETAILS_PARALLEL_FETCH_STARTED:LX/9X9;

    invoke-static {v9, v6, v7}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "ref"

    iget-object v11, v3, LX/89z;->loggingName:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-interface {v8, v9}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2462420
    iget-object v2, p0, LX/HPn;->f:LX/HPd;

    iget-object v3, p0, LX/HPn;->n:LX/HPa;

    invoke-virtual {v2, v3}, LX/98h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HPZ;

    .line 2462421
    if-eqz v2, :cond_2

    iget-object v3, v2, LX/HPZ;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/HPZ;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v3}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2462422
    iget-object v3, v2, LX/HPZ;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v3, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462423
    :goto_1
    if-eqz v2, :cond_3

    iget-object v3, v2, LX/HPZ;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v3, :cond_3

    iget-object v3, v2, LX/HPZ;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v3}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2462424
    iget-object v3, v2, LX/HPZ;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v3, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462425
    iput-boolean v4, p0, LX/HPn;->k:Z

    .line 2462426
    :goto_2
    if-eqz v2, :cond_0

    .line 2462427
    iget-object v3, v2, LX/HPZ;->d:Ljava/lang/String;

    iput-object v3, p0, LX/HPn;->q:Ljava/lang/String;

    .line 2462428
    iget-object v3, p0, LX/HPn;->q:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v3, v4

    :goto_3
    iput-boolean v3, p0, LX/HPn;->m:Z

    .line 2462429
    iget-object v2, v2, LX/HPZ;->c:Ljava/lang/String;

    iput-object v2, p0, LX/HPn;->r:Ljava/lang/String;

    .line 2462430
    iget-object v2, p0, LX/HPn;->r:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_4
    iput-boolean v4, p0, LX/HPn;->l:Z

    .line 2462431
    :cond_0
    goto :goto_0

    .line 2462432
    :pswitch_1
    iget-object v2, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_6

    .line 2462433
    :cond_1
    :goto_5
    goto :goto_0

    .line 2462434
    :cond_2
    iget-object v3, p0, LX/HPn;->e:LX/HPf;

    iget-wide v6, p0, LX/HPn;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    sget-object v7, LX/0zS;->b:LX/0zS;

    invoke-virtual {v3, v6, v7}, LX/HPf;->a(Ljava/lang/Long;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 2462435
    :cond_3
    iget-object v3, p0, LX/HPn;->e:LX/HPf;

    iget-wide v6, p0, LX/HPn;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    sget-object v7, LX/0zS;->d:LX/0zS;

    invoke-virtual {v3, v6, v7}, LX/HPf;->a(Ljava/lang/Long;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462436
    iput-boolean v5, p0, LX/HPn;->k:Z

    goto :goto_2

    :cond_4
    move v3, v5

    .line 2462437
    goto :goto_3

    :cond_5
    move v4, v5

    .line 2462438
    goto :goto_4

    .line 2462439
    :cond_6
    sget-object v2, LX/HPl;->FORCED_BY_USER:LX/HPl;

    iput-object v2, p0, LX/HPn;->s:LX/HPl;

    .line 2462440
    iget-object v2, p0, LX/HPn;->e:LX/HPf;

    iget-wide v4, p0, LX/HPn;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v2, v3, v4}, LX/HPf;->a(Ljava/lang/Long;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462441
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/HPn;->k:Z

    .line 2462442
    iget-object v2, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_1

    .line 2462443
    const/4 v2, 0x1

    invoke-static {p0, v2}, LX/HPn;->a(LX/HPn;Z)V

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2462444
    iget-object v0, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2462445
    iget-object v0, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2462446
    if-eqz v0, :cond_1

    .line 2462447
    invoke-static {p0, v0, v1}, LX/HPn;->a$redex0(LX/HPn;Lcom/facebook/graphql/executor/GraphQLResult;Z)V

    .line 2462448
    :cond_0
    :goto_0
    return-void

    .line 2462449
    :cond_1
    iget-object v0, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2462450
    iget-object v0, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2462451
    invoke-static {p0, v0}, LX/HPn;->a$redex0(LX/HPn;Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2462452
    const/4 v0, 0x0

    .line 2462453
    :goto_1
    iget-object v1, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2462454
    iget-object v1, p0, LX/HPn;->e:LX/HPf;

    iget-wide v2, p0, LX/HPn;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, LX/0zS;->d:LX/0zS;

    invoke-virtual {v1, v2, v3}, LX/HPf;->a(Ljava/lang/Long;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462455
    :cond_2
    iget-object v1, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2462456
    iget-object v1, p0, LX/HPn;->e:LX/HPf;

    iget-wide v2, p0, LX/HPn;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, LX/0zS;->b:LX/0zS;

    invoke-virtual {v1, v2, v3}, LX/HPf;->a(Ljava/lang/Long;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2462457
    :cond_3
    iget-object v1, p0, LX/HPn;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_4

    .line 2462458
    invoke-static {p0, v0}, LX/HPn;->a(LX/HPn;Z)V

    .line 2462459
    :cond_4
    iget-object v0, p0, LX/HPn;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2462460
    iget-object v0, p0, LX/HPn;->d:LX/1Ck;

    sget-object v1, LX/8Dp;->FETCH_PAGES_HEADER:LX/8Dp;

    new-instance v2, LX/HPg;

    invoke-direct {v2, p0}, LX/HPg;-><init>(LX/HPn;)V

    new-instance v3, LX/HPh;

    invoke-direct {v3, p0}, LX/HPh;-><init>(LX/HPn;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2462461
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method
