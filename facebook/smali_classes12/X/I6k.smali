.class public LX/I6k;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/I6h;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel$PossibleNotificationSubscriptionLevelsModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

.field private c:LX/I6i;

.field public d:Ljava/lang/String;

.field public e:LX/0tX;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0tX;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2537901
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2537902
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/I6k;->a:Ljava/util/List;

    .line 2537903
    iput-object p1, p0, LX/I6k;->d:Ljava/lang/String;

    .line 2537904
    iput-object p2, p0, LX/I6k;->e:LX/0tX;

    .line 2537905
    return-void
.end method

.method public static c(Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/facebook/graphql/calls/EventNotificationSubscriptionLevelInput;
    .end annotation

    .prologue
    .line 2537889
    sget-object v0, LX/I6j;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2537890
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported notification subscription level: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2537891
    :pswitch_0
    const-string v0, "ALL"

    .line 2537892
    :goto_0
    return-object v0

    .line 2537893
    :pswitch_1
    const-string v0, "MOST_IMPORTANT"

    goto :goto_0

    .line 2537894
    :pswitch_2
    const-string v0, "HOST_ONLY"

    goto :goto_0

    .line 2537895
    :pswitch_3
    const-string v0, "UNSUBSCRIBED"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2537896
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2537897
    const v1, 0x7f030585

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2537898
    new-instance v1, LX/I6i;

    invoke-direct {v1, p0}, LX/I6i;-><init>(LX/I6k;)V

    move-object v1, v1

    .line 2537899
    iput-object v1, p0, LX/I6k;->c:LX/I6i;

    .line 2537900
    new-instance v1, LX/I6h;

    invoke-direct {v1, v0}, LX/I6h;-><init>(Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2537882
    check-cast p1, LX/I6h;

    .line 2537883
    iget-object v0, p0, LX/I6k;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel$PossibleNotificationSubscriptionLevelsModel$EdgesModel;

    .line 2537884
    invoke-virtual {v0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel$PossibleNotificationSubscriptionLevelsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2537885
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2537886
    iget-object v2, p0, LX/I6k;->b:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    iget-object v3, p0, LX/I6k;->c:LX/I6i;

    invoke-virtual {p1, v1, v0, v2, v3}, LX/I6h;->a(LX/15i;ILcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;LX/I6i;)V

    .line 2537887
    return-void

    .line 2537888
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2537881
    iget-object v0, p0, LX/I6k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
