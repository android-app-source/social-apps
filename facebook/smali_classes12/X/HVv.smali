.class public LX/HVv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475472
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 2475473
    const-string v0, "owner_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2475474
    const-string v1, "extra_pages_admin_permissions"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2475475
    const-string v2, "page_profile_pic_url_extra"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2475476
    const-string v3, "profile_name"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2475477
    new-instance v4, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;

    invoke-direct {v4}, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;-><init>()V

    .line 2475478
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2475479
    const-string p1, "com.facebook.katana.profile.id"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475480
    if-eqz v1, :cond_0

    .line 2475481
    const-string p1, "extra_pages_admin_permissions"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2475482
    :cond_0
    if-eqz v2, :cond_1

    .line 2475483
    const-string p1, "page_profile_pic_url_extra"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475484
    :cond_1
    if-eqz v3, :cond_2

    .line 2475485
    const-string p1, "profile_name"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475486
    :cond_2
    invoke-virtual {v4, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475487
    move-object v0, v4

    .line 2475488
    return-object v0
.end method
