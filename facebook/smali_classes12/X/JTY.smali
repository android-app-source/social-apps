.class public LX/JTY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/JTc;


# direct methods
.method public constructor <init>(LX/JTc;LX/0Zb;)V
    .locals 0
    .param p1    # LX/JTc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2696808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696809
    iput-object p1, p0, LX/JTY;->b:LX/JTc;

    .line 2696810
    iput-object p2, p0, LX/JTY;->a:LX/0Zb;

    .line 2696811
    return-void
.end method

.method public static a(LX/JTY;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 5

    .prologue
    .line 2696825
    iget-object v0, p0, LX/JTY;->b:LX/JTc;

    .line 2696826
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, v0, LX/JTc;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object v1, v1

    .line 2696827
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JTa;

    .line 2696828
    invoke-virtual {v1}, LX/JTa;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2696829
    iget-object v4, v0, LX/JTc;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 2696830
    iget-object v4, v0, LX/JTc;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v1, v4

    .line 2696831
    invoke-virtual {p1, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2696832
    :cond_0
    move-object v0, p1

    .line 2696833
    iget-object v1, p0, LX/JTY;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2696834
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 2696818
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/JTW;->PAUSE:LX/JTW;

    invoke-virtual {v1}, LX/JTW;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "newsfeed_music_story_view"

    .line 2696819
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2696820
    move-object v0, v0

    .line 2696821
    sget-object v1, LX/JTX;->PLAYBACK_DURATION:LX/JTX;

    invoke-virtual {v1}, LX/JTX;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2696822
    sget-object v1, LX/JTX;->SONG_DURATION:LX/JTX;

    invoke-virtual {v1}, LX/JTX;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2696823
    invoke-static {p0, v0}, LX/JTY;->a(LX/JTY;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2696824
    return-void
.end method

.method public final a(LX/JTV;)V
    .locals 3

    .prologue
    .line 2696812
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/JTW;->TAP_ON_CTA:LX/JTW;

    invoke-virtual {v1}, LX/JTW;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "newsfeed_music_story_view"

    .line 2696813
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2696814
    move-object v0, v0

    .line 2696815
    sget-object v1, LX/JTX;->CTA_ACTION:LX/JTX;

    invoke-virtual {v1}, LX/JTX;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/JTV;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2696816
    invoke-static {p0, v0}, LX/JTY;->a(LX/JTY;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2696817
    return-void
.end method
