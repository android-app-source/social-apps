.class public final LX/Iob;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Iog;


# direct methods
.method public constructor <init>(LX/Iog;)V
    .locals 0

    .prologue
    .line 2612017
    iput-object p1, p0, LX/Iob;->a:LX/Iog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2612018
    iget-object v0, p0, LX/Iob;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->c:LX/03V;

    const-string v1, "OrionMessengerPayLoader"

    const-string v2, "Failed to fetch PaymentPin to confirm the sending of money"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612019
    iget-object v0, p0, LX/Iob;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->h:LX/Io3;

    invoke-virtual {v0}, LX/Io3;->a()V

    .line 2612020
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2612021
    check-cast p1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2612022
    iget-object v0, p0, LX/Iob;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612023
    iget-object p0, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    if-eqz p0, :cond_0

    iget-object p0, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {p0, p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2612024
    :goto_0
    return-void

    .line 2612025
    :cond_0
    iput-object p1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2612026
    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method
