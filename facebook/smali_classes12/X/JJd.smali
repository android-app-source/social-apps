.class public LX/JJd;
.super LX/5pb;
.source ""

# interfaces
.implements LX/0fl;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AppState"
.end annotation


# instance fields
.field private final a:LX/0Uo;

.field private final b:LX/1LT;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/5pY;LX/0Uo;LX/1LT;)V
    .locals 1
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2679669
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2679670
    iput-object p2, p0, LX/JJd;->a:LX/0Uo;

    .line 2679671
    iput-object p3, p0, LX/JJd;->b:LX/1LT;

    .line 2679672
    iget-object v0, p0, LX/JJd;->a:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "background"

    :goto_0
    iput-object v0, p0, LX/JJd;->c:Ljava/lang/String;

    .line 2679673
    return-void

    .line 2679674
    :cond_0
    const-string v0, "active"

    goto :goto_0
.end method

.method private u()LX/5pH;
    .locals 3

    .prologue
    .line 2679666
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2679667
    const-string v1, "app_state"

    iget-object v2, p0, LX/JJd;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679668
    return-object v0
.end method

.method private v()V
    .locals 3

    .prologue
    .line 2679663
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2679664
    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "appStateDidChange"

    invoke-direct {p0}, LX/JJd;->u()LX/5pH;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2679665
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 2679659
    iget-object v0, p0, LX/JJd;->b:LX/1LT;

    .line 2679660
    iput-object p0, v0, LX/1LT;->c:LX/0fl;

    .line 2679661
    iget-object v0, p0, LX/JJd;->b:LX/1LT;

    invoke-virtual {v0}, LX/1LT;->a()V

    .line 2679662
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2679657
    iget-object v0, p0, LX/JJd;->b:LX/1LT;

    invoke-virtual {v0}, LX/1LT;->b()V

    .line 2679658
    return-void
.end method

.method public getCurrentAppState(Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679655
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-direct {p0}, LX/JJd;->u()LX/5pH;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2679656
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679648
    const-string v0, "AppState"

    return-object v0
.end method

.method public final ku_()V
    .locals 1

    .prologue
    .line 2679652
    const-string v0, "background"

    iput-object v0, p0, LX/JJd;->c:Ljava/lang/String;

    .line 2679653
    invoke-direct {p0}, LX/JJd;->v()V

    .line 2679654
    return-void
.end method

.method public final kv_()V
    .locals 1

    .prologue
    .line 2679649
    const-string v0, "active"

    iput-object v0, p0, LX/JJd;->c:Ljava/lang/String;

    .line 2679650
    invoke-direct {p0}, LX/JJd;->v()V

    .line 2679651
    return-void
.end method
