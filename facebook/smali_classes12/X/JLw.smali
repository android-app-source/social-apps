.class public LX/JLw;
.super LX/JLv;
.source ""

# interfaces
.implements LX/5r7;


# instance fields
.field private final a:LX/FUn;

.field public b:Z

.field public c:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field private e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field private i:LX/F6L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I


# direct methods
.method public constructor <init>(LX/5pX;LX/F6L;)V
    .locals 1

    .prologue
    .line 2682733
    invoke-direct {p0, p1}, LX/JLv;-><init>(Landroid/content/Context;)V

    .line 2682734
    new-instance v0, LX/FUn;

    invoke-direct {v0}, LX/FUn;-><init>()V

    iput-object v0, p0, LX/JLw;->a:LX/FUn;

    .line 2682735
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JLw;->h:Z

    .line 2682736
    const/4 v0, 0x0

    iput-object v0, p0, LX/JLw;->i:LX/F6L;

    .line 2682737
    const/4 v0, 0x0

    iput v0, p0, LX/JLw;->l:I

    .line 2682738
    iput-object p2, p0, LX/JLw;->i:LX/F6L;

    .line 2682739
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2682730
    invoke-direct {p0}, LX/JLw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2682731
    iget-object v0, p0, LX/JLw;->i:LX/F6L;

    iget-object v1, p0, LX/JLw;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/F6L;->a(Ljava/lang/String;)V

    .line 2682732
    :cond_0
    return-void
.end method

.method public static c(LX/JLw;)V
    .locals 2

    .prologue
    .line 2682727
    invoke-direct {p0}, LX/JLw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2682728
    iget-object v0, p0, LX/JLw;->i:LX/F6L;

    iget-object v1, p0, LX/JLw;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/F6L;->b(Ljava/lang/String;)V

    .line 2682729
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 2682726
    iget-object v0, p0, LX/JLw;->i:LX/F6L;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JLw;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JLw;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2682719
    iget-boolean v0, p0, LX/JLw;->b:Z

    if-nez v0, :cond_1

    .line 2682720
    :cond_0
    :goto_0
    return-void

    .line 2682721
    :cond_1
    iget-object v0, p0, LX/JLw;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2682722
    iget-object v0, p0, LX/JLw;->c:Landroid/graphics/Rect;

    invoke-static {p0, v0}, LX/5r8;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2682723
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JLw;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2682724
    instance-of v1, v0, LX/5r7;

    if-eqz v1, :cond_0

    .line 2682725
    check-cast v0, LX/5r7;

    invoke-interface {v0}, LX/5r7;->a()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 2682711
    invoke-super {p0, p1}, LX/JLv;->a(I)V

    .line 2682712
    iget-boolean v0, p0, LX/JLw;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/JLw;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2682713
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JLw;->f:Z

    .line 2682714
    invoke-direct {p0}, LX/JLw;->b()V

    .line 2682715
    invoke-static {p0}, LX/FUt;->d(Landroid/view/ViewGroup;)V

    .line 2682716
    new-instance v0, Lcom/facebook/fbreact/views/fbscroll/FbReactScrollView$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbreact/views/fbscroll/FbReactScrollView$1;-><init>(LX/JLw;)V

    .line 2682717
    const-wide/16 v2, 0x14

    invoke-virtual {p0, v0, v2, v3}, LX/JLw;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    .line 2682718
    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 2682709
    iget-object v0, p0, LX/JLw;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2682710
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2682667
    iget v0, p0, LX/JLw;->l:I

    if-eqz v0, :cond_0

    .line 2682668
    invoke-virtual {p0, v4}, LX/JLw;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2682669
    iget-object v1, p0, LX/JLw;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p0}, LX/JLw;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2682670
    iget-object v1, p0, LX/JLw;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, LX/JLw;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/JLw;->getHeight()I

    move-result v3

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2682671
    iget-object v0, p0, LX/JLw;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2682672
    :cond_0
    invoke-super {p0, p1}, LX/JLv;->draw(Landroid/graphics/Canvas;)V

    .line 2682673
    return-void
.end method

.method public getRemoveClippedSubviews()Z
    .locals 1

    .prologue
    .line 2682708
    iget-boolean v0, p0, LX/JLw;->b:Z

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2682700
    iget-boolean v2, p0, LX/JLw;->h:Z

    if-nez v2, :cond_1

    .line 2682701
    :cond_0
    :goto_0
    return v0

    .line 2682702
    :cond_1
    invoke-super {p0, p1}, LX/JLv;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2682703
    invoke-static {p0, p1}, LX/5sB;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2682704
    invoke-static {p0}, LX/FUt;->b(Landroid/view/ViewGroup;)V

    .line 2682705
    iput-boolean v1, p0, LX/JLw;->e:Z

    .line 2682706
    invoke-direct {p0}, LX/JLw;->b()V

    move v0, v1

    .line 2682707
    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 2682698
    invoke-virtual {p0}, LX/JLw;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, LX/JLw;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/JLw;->scrollTo(II)V

    .line 2682699
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2682695
    invoke-static {p1, p2}, LX/5qs;->a(II)V

    .line 2682696
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/JLw;->setMeasuredDimension(II)V

    .line 2682697
    return-void
.end method

.method public onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 2682687
    invoke-super {p0, p1, p2, p3, p4}, LX/JLv;->onScrollChanged(IIII)V

    .line 2682688
    iget-object v0, p0, LX/JLw;->a:LX/FUn;

    invoke-virtual {v0, p1, p2}, LX/FUn;->a(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2682689
    iget-boolean v0, p0, LX/JLw;->b:Z

    if-eqz v0, :cond_0

    .line 2682690
    invoke-virtual {p0}, LX/JLw;->a()V

    .line 2682691
    :cond_0
    iget-boolean v0, p0, LX/JLw;->f:Z

    if-eqz v0, :cond_1

    .line 2682692
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JLw;->g:Z

    .line 2682693
    :cond_1
    invoke-static {p0}, LX/FUt;->a(Landroid/view/ViewGroup;)V

    .line 2682694
    :cond_2
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x37369abf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2682683
    invoke-super {p0, p1, p2, p3, p4}, LX/JLv;->onSizeChanged(IIII)V

    .line 2682684
    iget-boolean v1, p0, LX/JLw;->b:Z

    if-eqz v1, :cond_0

    .line 2682685
    invoke-virtual {p0}, LX/JLw;->a()V

    .line 2682686
    :cond_0
    const/16 v1, 0x2d

    const v2, -0xcc70ca3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x2

    const v1, -0x79711354

    invoke-static {v3, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2682674
    iget-boolean v2, p0, LX/JLw;->h:Z

    if-nez v2, :cond_0

    .line 2682675
    const v2, 0x3d03962b

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2682676
    :goto_0
    return v0

    .line 2682677
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    .line 2682678
    if-ne v2, v4, :cond_1

    iget-boolean v2, p0, LX/JLw;->e:Z

    if-eqz v2, :cond_1

    .line 2682679
    invoke-static {p0}, LX/FUt;->c(Landroid/view/ViewGroup;)V

    .line 2682680
    iput-boolean v0, p0, LX/JLw;->e:Z

    .line 2682681
    invoke-static {p0}, LX/JLw;->c(LX/JLw;)V

    .line 2682682
    :cond_1
    invoke-super {p0, p1}, LX/JLv;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x1d47652b

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
