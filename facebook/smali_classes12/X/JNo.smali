.class public final LX/JNo;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JNp;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

.field public b:I

.field public c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

.field public final synthetic d:LX/JNp;


# direct methods
.method public constructor <init>(LX/JNp;)V
    .locals 1

    .prologue
    .line 2686491
    iput-object p1, p0, LX/JNo;->d:LX/JNp;

    .line 2686492
    move-object v0, p1

    .line 2686493
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2686494
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2686495
    const-string v0, "ConnectWithFacebookCoverPhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2686496
    if-ne p0, p1, :cond_1

    .line 2686497
    :cond_0
    :goto_0
    return v0

    .line 2686498
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2686499
    goto :goto_0

    .line 2686500
    :cond_3
    check-cast p1, LX/JNo;

    .line 2686501
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2686502
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2686503
    if-eq v2, v3, :cond_0

    .line 2686504
    iget-object v2, p0, LX/JNo;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JNo;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget-object v3, p1, LX/JNo;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2686505
    goto :goto_0

    .line 2686506
    :cond_5
    iget-object v2, p1, LX/JNo;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    if-nez v2, :cond_4

    .line 2686507
    :cond_6
    iget v2, p0, LX/JNo;->b:I

    iget v3, p1, LX/JNo;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2686508
    goto :goto_0

    .line 2686509
    :cond_7
    iget-object v2, p0, LX/JNo;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JNo;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object v3, p1, LX/JNo;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2686510
    goto :goto_0

    .line 2686511
    :cond_8
    iget-object v2, p1, LX/JNo;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
