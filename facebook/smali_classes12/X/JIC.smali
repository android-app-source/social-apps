.class public LX/JIC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JIA;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2677540
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JIC;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2677537
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2677538
    iput-object p1, p0, LX/JIC;->b:LX/0Ot;

    .line 2677539
    return-void
.end method

.method public static a(LX/0QB;)LX/JIC;
    .locals 4

    .prologue
    .line 2677526
    const-class v1, LX/JIC;

    monitor-enter v1

    .line 2677527
    :try_start_0
    sget-object v0, LX/JIC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2677528
    sput-object v2, LX/JIC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2677529
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2677530
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2677531
    new-instance v3, LX/JIC;

    const/16 p0, 0x1aca

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JIC;-><init>(LX/0Ot;)V

    .line 2677532
    move-object v0, v3

    .line 2677533
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2677534
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JIC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677535
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2677536
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2677475
    const v0, -0x63d8e1e4

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2677496
    check-cast p2, LX/JIB;

    .line 2677497
    iget-object v0, p0, LX/JIC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;

    iget-object v2, p2, LX/JIB;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    iget-boolean v3, p2, LX/JIB;->b:Z

    iget-boolean v4, p2, LX/JIB;->c:Z

    iget-boolean v5, p2, LX/JIB;->d:Z

    iget-boolean v6, p2, LX/JIB;->e:Z

    move-object v1, p1

    .line 2677498
    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;->kZ_()LX/1Fb;

    move-result-object v7

    invoke-interface {v7}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    .line 2677499
    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v10

    .line 2677500
    iget-object v7, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->c:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1Ad;

    invoke-virtual {v7, v8}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v7

    sget-object v8, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v7

    invoke-virtual {v7}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v11

    .line 2677501
    if-eqz v3, :cond_3

    const v7, 0x7f0203e4

    .line 2677502
    :goto_0
    iget-object v9, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->b:Landroid/content/res/Resources;

    if-eqz v3, :cond_4

    const v8, 0x7f0a008b

    :goto_1
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    .line 2677503
    if-eqz v3, :cond_5

    .line 2677504
    iget-object v8, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->g:Landroid/graphics/PorterDuffColorFilter;

    if-nez v8, :cond_0

    .line 2677505
    const v8, 0x7f0a008b

    invoke-static {v0, v8}, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->a(Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;I)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v8

    iput-object v8, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->g:Landroid/graphics/PorterDuffColorFilter;

    .line 2677506
    :cond_0
    iget-object v8, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->g:Landroid/graphics/PorterDuffColorFilter;

    move-object v8, v8

    .line 2677507
    :goto_2
    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v9

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->DISCOVERY_BUCKET:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    invoke-static {v9, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;->la_()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel$DiscoveryBucketModel;

    move-result-object v9

    if-eqz v9, :cond_7

    const/4 v9, 0x1

    .line 2677508
    :goto_3
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    const/4 p2, 0x2

    invoke-interface {p1, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p1

    .line 2677509
    const p2, -0x63d8e1e4

    const/4 v2, 0x0

    invoke-static {v1, p2, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2677510
    invoke-interface {p1, p2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object p1

    const/4 p2, 0x0

    invoke-interface {p1, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v7

    const/4 p1, 0x6

    const p2, 0x7f0b253d

    invoke-interface {v7, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p1

    const/4 p2, 0x1

    if-eqz v5, :cond_8

    const v7, 0x7f0b2541

    :goto_4
    invoke-interface {p1, p2, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p1

    const/4 p2, 0x3

    if-nez v4, :cond_1

    if-eqz v6, :cond_9

    :cond_1
    const v7, 0x7f0b2540

    :goto_5
    invoke-interface {p1, p2, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    const/4 p1, 0x1

    invoke-interface {v7, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p1

    invoke-virtual {p1, v11}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v11

    invoke-virtual {v11, v8}, LX/1up;->a(Landroid/graphics/ColorFilter;)LX/1up;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v11, 0x7f0b253b

    invoke-interface {v8, v11}, LX/1Di;->i(I)LX/1Di;

    move-result-object v8

    const v11, 0x7f0b253b

    invoke-interface {v8, v11}, LX/1Di;->q(I)LX/1Di;

    move-result-object v8

    const/4 v11, 0x1

    const p1, 0x7f0b253c

    invoke-interface {v8, v11, p1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v10, 0x7f0b0050

    invoke-virtual {v8, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v8

    const/4 v10, 0x0

    invoke-virtual {v8, v10}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v8

    const/4 v10, 0x2

    invoke-virtual {v8, v10}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/4 v10, 0x4

    const v11, 0x7f0b253d

    invoke-interface {v8, v10, v11}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-interface {v8, v10}, LX/1Di;->a(F)LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    .line 2677511
    if-eqz v9, :cond_2

    .line 2677512
    iget-object v9, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->b:Landroid/content/res/Resources;

    if-eqz v3, :cond_a

    const v7, 0x7f0a00cf

    :goto_6
    invoke-virtual {v9, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 2677513
    iget-object v9, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->e:LX/1vg;

    invoke-virtual {v9, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/2xv;->i(I)LX/2xv;

    move-result-object v7

    const v9, 0x7f0207e9

    invoke-virtual {v7, v9}, LX/2xv;->h(I)LX/2xv;

    move-result-object v7

    invoke-virtual {v7}, LX/1n6;->b()LX/1dc;

    move-result-object v7

    .line 2677514
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const/4 v10, 0x1

    invoke-interface {v9, v10}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v10

    invoke-virtual {v10, v7}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/4 v10, 0x4

    const v11, 0x7f0b253d

    invoke-interface {v7, v10, v11}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    invoke-interface {v9, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2677515
    :cond_2
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 2677516
    return-object v0

    .line 2677517
    :cond_3
    const v7, 0x7f0203e5

    goto/16 :goto_0

    .line 2677518
    :cond_4
    const v8, 0x7f0a00a8

    goto/16 :goto_1

    .line 2677519
    :cond_5
    iget-object v8, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->f:Landroid/graphics/PorterDuffColorFilter;

    if-nez v8, :cond_6

    .line 2677520
    const v8, 0x7f0a00a8

    invoke-static {v0, v8}, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->a(Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;I)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v8

    iput-object v8, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->f:Landroid/graphics/PorterDuffColorFilter;

    .line 2677521
    :cond_6
    iget-object v8, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->f:Landroid/graphics/PorterDuffColorFilter;

    move-object v8, v8

    .line 2677522
    goto/16 :goto_2

    .line 2677523
    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 2677524
    :cond_8
    const v7, 0x7f0b253e

    goto/16 :goto_4

    :cond_9
    const v7, 0x7f0b253e

    goto/16 :goto_5

    .line 2677525
    :cond_a
    const v7, 0x7f0a00a2

    goto :goto_6
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2677476
    invoke-static {}, LX/1dS;->b()V

    .line 2677477
    iget v0, p1, LX/1dQ;->b:I

    .line 2677478
    packed-switch v0, :pswitch_data_0

    .line 2677479
    :goto_0
    return-object v2

    .line 2677480
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2677481
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2677482
    check-cast v1, LX/JIB;

    .line 2677483
    iget-object v3, p0, LX/JIC;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;

    iget-object v4, v1, LX/JIB;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    iget-object v5, v1, LX/JIB;->f:LX/Eoo;

    .line 2677484
    iget-object p1, v3, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->d:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/Epm;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 2677485
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object p0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->DISCOVERY_BUCKET:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    invoke-static {p0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;->la_()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel$DiscoveryBucketModel;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 2677486
    sget-object p0, LX/0ax;->bR:Ljava/lang/String;

    const-string v1, "CONTEXT_ITEM"

    .line 2677487
    invoke-interface {v5}, LX/Eoo;->c()Ljava/lang/String;

    move-result-object v3

    .line 2677488
    invoke-interface {v4}, LX/5vW;->c()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2677489
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v0, " "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v4}, LX/5vW;->c()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2677490
    :cond_0
    move-object v3, v3

    .line 2677491
    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;->la_()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel$DiscoveryBucketModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel$DiscoveryBucketModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2677492
    iget-object p0, p1, LX/Epm;->a:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/17W;

    .line 2677493
    invoke-virtual {p0, p2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2677494
    :goto_1
    goto :goto_0

    .line 2677495
    :cond_1
    invoke-virtual {p1, p2, v4, v5}, LX/Epm;->a(Landroid/content/Context;LX/5vW;LX/Eoo;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x63d8e1e4
        :pswitch_0
    .end packed-switch
.end method
