.class public final LX/IIx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 0

    .prologue
    .line 2562290
    iput-object p1, p0, LX/IIx;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x26

    const v1, 0x33700962

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2562291
    const-string v0, "user_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2562292
    const-string v1, "user_nearby_context"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2562293
    const-string v1, "user_meta_context"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2562294
    const-string v1, "user_location"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/location/ImmutableLocation;

    .line 2562295
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    .line 2562296
    iget-object v5, p0, LX/IIx;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v5, v5, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v3, v4, v1}, LX/IFX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V

    .line 2562297
    :cond_0
    const/16 v0, 0x27

    const v1, 0x39909726

    invoke-static {v6, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
