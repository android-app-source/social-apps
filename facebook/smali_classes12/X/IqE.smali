.class public LX/IqE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0W9;


# direct methods
.method private constructor <init>(LX/0W9;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2616761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2616762
    iput-object p1, p0, LX/IqE;->a:LX/0W9;

    .line 2616763
    return-void
.end method

.method public static a(LX/0QB;)LX/IqE;
    .locals 1

    .prologue
    .line 2616764
    invoke-static {p0}, LX/IqE;->b(LX/0QB;)LX/IqE;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/IqE;
    .locals 2

    .prologue
    .line 2616765
    new-instance v1, LX/IqE;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-direct {v1, v0}, LX/IqE;-><init>(LX/0W9;)V

    .line 2616766
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/montage/model/art/ArtAsset;Lcom/facebook/messaging/montage/model/art/ArtItem;LX/IqD;)LX/Iqg;
    .locals 17
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2616767
    const/4 v1, 0x0

    .line 2616768
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/facebook/messaging/montage/model/art/TextAsset;

    if-eqz v2, :cond_4

    move-object/from16 v2, p1

    .line 2616769
    check-cast v2, Lcom/facebook/messaging/montage/model/art/TextAsset;

    .line 2616770
    sget-object v1, LX/IqD;->EDITOR:LX/IqD;

    move-object/from16 v0, p3

    if-eq v0, v1, :cond_1

    const/4 v13, 0x1

    .line 2616771
    :goto_0
    sget-object v1, LX/IqD;->NUX:LX/IqD;

    move-object/from16 v0, p3

    if-ne v0, v1, :cond_2

    const/4 v14, 0x1

    .line 2616772
    :goto_1
    sget-object v1, LX/IqD;->EDITOR:LX/IqD;

    move-object/from16 v0, p3

    if-ne v0, v1, :cond_3

    const/4 v1, 0x1

    .line 2616773
    :goto_2
    iget-object v8, v2, Lcom/facebook/messaging/montage/model/art/TextAsset;->f:Ljava/lang/String;

    .line 2616774
    iget-object v3, v2, Lcom/facebook/messaging/montage/model/art/TextAsset;->k:LX/Dhi;

    sget-object v4, LX/Dhi;->TIME:LX/Dhi;

    if-ne v3, v4, :cond_6

    .line 2616775
    const/4 v1, 0x3

    move-object/from16 v0, p0

    iget-object v3, v0, LX/IqE;->a:LX/0W9;

    invoke-virtual {v3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v1, v3}, Ljava/text/DateFormat;->getTimeInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 2616776
    const/4 v15, 0x0

    .line 2616777
    :goto_3
    new-instance v1, LX/IsC;

    iget v3, v2, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    const/4 v4, 0x0

    iget v5, v2, Lcom/facebook/messaging/montage/model/art/TextAsset;->h:F

    invoke-static/range {p1 .. p1}, LX/Iqo;->a(Lcom/facebook/messaging/montage/model/art/ArtAsset;)LX/Iqo;

    move-result-object v6

    invoke-static/range {p1 .. p1}, LX/Iqo;->b(Lcom/facebook/messaging/montage/model/art/ArtAsset;)LX/Iqo;

    move-result-object v7

    iget v9, v2, Lcom/facebook/messaging/montage/model/art/TextAsset;->g:I

    const/4 v10, 0x0

    iget-object v11, v2, Lcom/facebook/messaging/montage/model/art/TextAsset;->i:LX/Dhh;

    iget-object v12, v2, Lcom/facebook/messaging/montage/model/art/TextAsset;->j:Lcom/facebook/messaging/font/FontAsset;

    iget-object v0, v2, Lcom/facebook/messaging/montage/model/art/TextAsset;->k:LX/Dhi;

    move-object/from16 v16, v0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v16}, LX/IsC;-><init>(Lcom/facebook/messaging/montage/model/art/ArtItem;FZFLX/Iqo;LX/Iqo;Ljava/lang/CharSequence;IILX/Dhh;Lcom/facebook/messaging/font/FontAsset;ZZZLX/Dhi;)V

    .line 2616778
    :cond_0
    :goto_4
    return-object v1

    .line 2616779
    :cond_1
    const/4 v13, 0x0

    goto :goto_0

    .line 2616780
    :cond_2
    const/4 v14, 0x0

    goto :goto_1

    .line 2616781
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 2616782
    :cond_4
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/facebook/messaging/montage/model/art/StickerAsset;

    if-eqz v2, :cond_5

    move-object/from16 v2, p1

    .line 2616783
    check-cast v2, Lcom/facebook/messaging/montage/model/art/StickerAsset;

    .line 2616784
    new-instance v1, LX/Irx;

    iget v3, v2, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    const/4 v4, 0x0

    invoke-static/range {p1 .. p1}, LX/Iqo;->a(Lcom/facebook/messaging/montage/model/art/ArtAsset;)LX/Iqo;

    move-result-object v5

    invoke-static/range {p1 .. p1}, LX/Iqo;->b(Lcom/facebook/messaging/montage/model/art/ArtAsset;)LX/Iqo;

    move-result-object v6

    iget-object v7, v2, Lcom/facebook/messaging/montage/model/art/StickerAsset;->f:Lcom/facebook/stickers/model/Sticker;

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, LX/Irx;-><init>(Lcom/facebook/messaging/montage/model/art/ArtItem;FZLX/Iqo;LX/Iqo;Lcom/facebook/stickers/model/Sticker;)V

    goto :goto_4

    .line 2616785
    :cond_5
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/facebook/messaging/montage/model/art/ImageAsset;

    if-eqz v2, :cond_0

    move-object/from16 v2, p1

    .line 2616786
    check-cast v2, Lcom/facebook/messaging/montage/model/art/ImageAsset;

    .line 2616787
    new-instance v1, LX/Iqh;

    iget v3, v2, Lcom/facebook/messaging/montage/model/art/ArtAsset;->b:F

    const/4 v4, 0x0

    iget v5, v2, Lcom/facebook/messaging/montage/model/art/ImageAsset;->g:F

    invoke-static/range {p1 .. p1}, LX/Iqo;->a(Lcom/facebook/messaging/montage/model/art/ArtAsset;)LX/Iqo;

    move-result-object v6

    invoke-static/range {p1 .. p1}, LX/Iqo;->b(Lcom/facebook/messaging/montage/model/art/ArtAsset;)LX/Iqo;

    move-result-object v7

    iget-object v8, v2, Lcom/facebook/messaging/montage/model/art/ImageAsset;->f:Ljava/lang/String;

    iget-object v9, v2, Lcom/facebook/messaging/montage/model/art/ArtAsset;->c:Ljava/lang/String;

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v9}, LX/Iqh;-><init>(Lcom/facebook/messaging/montage/model/art/ArtItem;FZFLX/Iqo;LX/Iqo;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    move v15, v1

    goto :goto_3
.end method
