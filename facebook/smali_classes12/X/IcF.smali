.class public LX/IcF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Og;

.field private final b:LX/FDq;

.field private final c:Ljava/lang/String;

.field public final d:LX/03V;

.field public final e:LX/0tX;

.field public final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/IcE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Og;LX/FDq;LX/0Or;LX/03V;LX/0tX;LX/1Ck;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Og;",
            "LX/FDq;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tX;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2595337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2595338
    iput-object p1, p0, LX/IcF;->a:LX/2Og;

    .line 2595339
    iput-object p2, p0, LX/IcF;->b:LX/FDq;

    .line 2595340
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/IcF;->c:Ljava/lang/String;

    .line 2595341
    iput-object p4, p0, LX/IcF;->d:LX/03V;

    .line 2595342
    iput-object p5, p0, LX/IcF;->e:LX/0tX;

    .line 2595343
    iput-object p6, p0, LX/IcF;->f:LX/1Ck;

    .line 2595344
    return-void
.end method
