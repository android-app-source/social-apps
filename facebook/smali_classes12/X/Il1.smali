.class public final enum LX/Il1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Il1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Il1;

.field public static final enum ACTION_BUTTON:LX/Il1;

.field public static final enum ORDER_ID:LX/Il1;

.field public static final enum PAYMENT_STATUS_WITH_ATTACHMENT:LX/Il1;

.field public static final enum PRICE_BREAKDOWN:LX/Il1;

.field public static final enum PRODUCT_ITEM:LX/Il1;

.field public static final enum QUANTITY:LX/Il1;

.field public static final enum SHIPPING_ADDRESS:LX/Il1;

.field public static final enum SHIPPING_FULFILLMENT:LX/Il1;

.field public static final enum SHIPPING_METHOD:LX/Il1;


# instance fields
.field private mViewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2607358
    new-instance v0, LX/Il1;

    const-string v1, "PRODUCT_ITEM"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->PRODUCT_ITEM:LX/Il1;

    .line 2607359
    new-instance v0, LX/Il1;

    const-string v1, "PRICE_BREAKDOWN"

    invoke-direct {v0, v1, v4, v5}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->PRICE_BREAKDOWN:LX/Il1;

    .line 2607360
    new-instance v0, LX/Il1;

    const-string v1, "SHIPPING_ADDRESS"

    invoke-direct {v0, v1, v5, v6}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->SHIPPING_ADDRESS:LX/Il1;

    .line 2607361
    new-instance v0, LX/Il1;

    const-string v1, "SHIPPING_METHOD"

    invoke-direct {v0, v1, v6, v7}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->SHIPPING_METHOD:LX/Il1;

    .line 2607362
    new-instance v0, LX/Il1;

    const-string v1, "SHIPPING_FULFILLMENT"

    invoke-direct {v0, v1, v7, v8}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->SHIPPING_FULFILLMENT:LX/Il1;

    .line 2607363
    new-instance v0, LX/Il1;

    const-string v1, "QUANTITY"

    const/4 v2, 0x5

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->QUANTITY:LX/Il1;

    .line 2607364
    new-instance v0, LX/Il1;

    const-string v1, "PAYMENT_STATUS_WITH_ATTACHMENT"

    const/4 v2, 0x6

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->PAYMENT_STATUS_WITH_ATTACHMENT:LX/Il1;

    .line 2607365
    new-instance v0, LX/Il1;

    const-string v1, "ORDER_ID"

    const/4 v2, 0x7

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->ORDER_ID:LX/Il1;

    .line 2607366
    new-instance v0, LX/Il1;

    const-string v1, "ACTION_BUTTON"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v8, v2}, LX/Il1;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Il1;->ACTION_BUTTON:LX/Il1;

    .line 2607367
    const/16 v0, 0x9

    new-array v0, v0, [LX/Il1;

    const/4 v1, 0x0

    sget-object v2, LX/Il1;->PRODUCT_ITEM:LX/Il1;

    aput-object v2, v0, v1

    sget-object v1, LX/Il1;->PRICE_BREAKDOWN:LX/Il1;

    aput-object v1, v0, v4

    sget-object v1, LX/Il1;->SHIPPING_ADDRESS:LX/Il1;

    aput-object v1, v0, v5

    sget-object v1, LX/Il1;->SHIPPING_METHOD:LX/Il1;

    aput-object v1, v0, v6

    sget-object v1, LX/Il1;->SHIPPING_FULFILLMENT:LX/Il1;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Il1;->QUANTITY:LX/Il1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Il1;->PAYMENT_STATUS_WITH_ATTACHMENT:LX/Il1;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Il1;->ORDER_ID:LX/Il1;

    aput-object v2, v0, v1

    sget-object v1, LX/Il1;->ACTION_BUTTON:LX/Il1;

    aput-object v1, v0, v8

    sput-object v0, LX/Il1;->$VALUES:[LX/Il1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2607355
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2607356
    iput p3, p0, LX/Il1;->mViewType:I

    .line 2607357
    return-void
.end method

.method public static getItemFromViewType(I)LX/Il1;
    .locals 5

    .prologue
    .line 2607350
    invoke-static {}, LX/Il1;->values()[LX/Il1;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2607351
    invoke-virtual {v3}, LX/Il1;->getItemViewType()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 2607352
    return-object v3

    .line 2607353
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2607354
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No Item found for view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Il1;
    .locals 1

    .prologue
    .line 2607347
    const-class v0, LX/Il1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Il1;

    return-object v0
.end method

.method public static values()[LX/Il1;
    .locals 1

    .prologue
    .line 2607349
    sget-object v0, LX/Il1;->$VALUES:[LX/Il1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Il1;

    return-object v0
.end method


# virtual methods
.method public final getItemViewType()I
    .locals 1

    .prologue
    .line 2607348
    iget v0, p0, LX/Il1;->mViewType:I

    return v0
.end method
