.class public final LX/HFg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;)V
    .locals 0

    .prologue
    .line 2444021
    iput-object p1, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x4618679f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2444022
    iget-object v1, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2444023
    iget-object v2, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->h:LX/HFJ;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2444024
    iget-object v2, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-static {v2, v3}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2444025
    iget-object v2, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->h:LX/HFJ;

    .line 2444026
    iget-object v3, v2, LX/HFJ;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2444027
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2444028
    iget-object v1, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    invoke-static {v1}, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->l(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;)V

    .line 2444029
    :goto_0
    const v1, -0x541bf992

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2444030
    :cond_0
    iget-object v2, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->h:LX/HFJ;

    .line 2444031
    iget-object v3, v2, LX/HFJ;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2444032
    if-nez v2, :cond_1

    .line 2444033
    iget-object v2, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    .line 2444034
    iget-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->d:LX/1Ck;

    const-string v4, "name_check_gql_task_key"

    iget-object v5, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->b:LX/HFc;

    .line 2444035
    new-instance p0, LX/HGX;

    invoke-direct {p0}, LX/HGX;-><init>()V

    move-object p0, p0

    .line 2444036
    const-string p1, "input"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p0

    check-cast p0, LX/HGX;

    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    .line 2444037
    iget-object p1, v5, LX/HFc;->a:LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    invoke-static {p0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v5, p0

    .line 2444038
    new-instance p0, LX/HFi;

    invoke-direct {p0, v2, v1}, LX/HFi;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2444039
    goto :goto_0

    .line 2444040
    :cond_1
    iget-object v2, p0, LX/HFg;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    .line 2444041
    iget-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->d:LX/1Ck;

    const-string v4, "name_update_gql_task_key"

    iget-object v5, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->b:LX/HFc;

    iget-object v6, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->h:LX/HFJ;

    .line 2444042
    iget-object v7, v6, LX/HFJ;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2444043
    new-instance v7, LX/4Hr;

    invoke-direct {v7}, LX/4Hr;-><init>()V

    .line 2444044
    const-string p0, "pageid"

    invoke-virtual {v7, p0, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444045
    move-object v7, v7

    .line 2444046
    const-string p0, "new_name"

    invoke-virtual {v7, p0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444047
    move-object v7, v7

    .line 2444048
    new-instance p0, LX/HGF;

    invoke-direct {p0}, LX/HGF;-><init>()V

    move-object p0, p0

    .line 2444049
    const-string p1, "input"

    invoke-virtual {p0, p1, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/HGF;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2444050
    iget-object p0, v5, LX/HFc;->a:LX/0tX;

    invoke-virtual {p0, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v7}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v5, v7

    .line 2444051
    new-instance v6, LX/HFj;

    invoke-direct {v6, v2, v1}, LX/HFj;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2444052
    goto/16 :goto_0
.end method
