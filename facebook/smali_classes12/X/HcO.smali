.class public LX/HcO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<STATE:",
        "Ljava/lang/Object;",
        "EVENT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TSTATE;",
            "Ljava/util/Map",
            "<TEVENT;",
            "Lcom/facebook/statemachine/StateTransition",
            "<TSTATE;>;>;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSTATE;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSTATE;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSTATE;"
        }
    .end annotation
.end field

.field public e:LX/GQX;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSTATE;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2487016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487017
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487018
    iput-object p1, p0, LX/HcO;->b:Ljava/lang/Object;

    .line 2487019
    iput-object v1, p0, LX/HcO;->d:Ljava/lang/Object;

    .line 2487020
    iput-object p1, p0, LX/HcO;->c:Ljava/lang/Object;

    .line 2487021
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HcO;->a:Ljava/util/Map;

    .line 2487022
    iput-object v1, p0, LX/HcO;->e:LX/GQX;

    .line 2487023
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEVENT;TSTATE;TSTATE;)V"
        }
    .end annotation

    .prologue
    .line 2487024
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487025
    new-instance v0, LX/HcN;

    invoke-direct {v0, p0, p3}, LX/HcN;-><init>(LX/HcO;Ljava/lang/Object;)V

    .line 2487026
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487027
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487028
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487029
    iget-object v1, p0, LX/HcO;->a:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 2487030
    if-nez v1, :cond_0

    .line 2487031
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2487032
    iget-object p3, p0, LX/HcO;->a:Ljava/util/Map;

    invoke-interface {p3, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487033
    :cond_0
    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_1

    const/4 p3, 0x1

    :goto_0
    invoke-static {p3}, LX/0PB;->checkState(Z)V

    .line 2487034
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487035
    return-void

    .line 2487036
    :cond_1
    const/4 p3, 0x0

    goto :goto_0
.end method
