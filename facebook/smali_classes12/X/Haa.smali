.class public final LX/Haa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;)V
    .locals 0

    .prologue
    .line 2484031
    iput-object p1, p0, LX/Haa;->a:Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2484035
    if-nez p1, :cond_1

    .line 2484036
    :cond_0
    :goto_0
    return-void

    .line 2484037
    :cond_1
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2484038
    check-cast v0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel;

    .line 2484039
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel;->a()Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2484040
    iget-object v1, p0, LX/Haa;->a:Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel;->a()Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    move-result-object v0

    .line 2484041
    invoke-static {v1, v0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->a$redex0(Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2484042
    goto :goto_0

    .line 2484043
    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2484033
    sget-object v0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->p:Ljava/lang/String;

    const-string v1, "Failure loading security checkup."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2484034
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2484032
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Haa;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
