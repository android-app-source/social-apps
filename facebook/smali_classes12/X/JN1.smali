.class public final LX/JN1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLEvent;

.field public final synthetic b:LX/AnC;

.field public final synthetic c:LX/JN3;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic e:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

.field public final synthetic f:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;Lcom/facebook/graphql/model/GraphQLEvent;LX/AnC;LX/JN3;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 2684915
    iput-object p1, p0, LX/JN1;->f:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iput-object p2, p0, LX/JN1;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object p3, p0, LX/JN1;->b:LX/AnC;

    iput-object p4, p0, LX/JN1;->c:LX/JN3;

    iput-object p5, p0, LX/JN1;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p6, p0, LX/JN1;->e:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 7

    .prologue
    .line 2684916
    iget-object v0, p0, LX/JN1;->f:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iget-object v0, v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->o:LX/7vW;

    iget-object v1, p0, LX/JN1;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v1

    const-string v3, "unknown"

    const-string v4, "native_newsfeed"

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->EVENT_CHAINING:Lcom/facebook/events/common/ActionMechanism;

    const/4 v6, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2684917
    iget-object v1, p0, LX/JN1;->f:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iget-object v1, v1, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->m:LX/1Ck;

    new-instance v2, LX/JMz;

    invoke-direct {v2, p0}, LX/JMz;-><init>(LX/JN1;)V

    invoke-virtual {v1, p0, v0, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2684918
    iget-object v0, p0, LX/JN1;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    iget-object v1, p0, LX/JN1;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0, v1, p2}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2684919
    iget-object v1, p0, LX/JN1;->b:LX/AnC;

    iget-object v2, p0, LX/JN1;->c:LX/JN3;

    iget-object v3, p0, LX/JN1;->e:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v4, p0, LX/JN1;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, LX/AnC;->a(Landroid/view/View;Ljava/lang/Object;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 2684920
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 6

    .prologue
    .line 2684921
    iget-object v0, p0, LX/JN1;->f:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iget-object v0, v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->p:LX/7vZ;

    iget-object v1, p0, LX/JN1;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v1

    const-string v3, "unknown"

    const-string v4, "native_newsfeed"

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->EVENT_CHAINING:Lcom/facebook/events/common/ActionMechanism;

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2684922
    iget-object v1, p0, LX/JN1;->f:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iget-object v1, v1, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->m:LX/1Ck;

    new-instance v2, LX/JN0;

    invoke-direct {v2, p0}, LX/JN0;-><init>(LX/JN1;)V

    invoke-virtual {v1, p0, v0, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2684923
    iget-object v0, p0, LX/JN1;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    iget-object v1, p0, LX/JN1;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0, v1, p2}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2684924
    iget-object v1, p0, LX/JN1;->b:LX/AnC;

    iget-object v2, p0, LX/JN1;->c:LX/JN3;

    iget-object v3, p0, LX/JN1;->e:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v4, p0, LX/JN1;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, LX/AnC;->a(Landroid/view/View;Ljava/lang/Object;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 2684925
    return-void
.end method
