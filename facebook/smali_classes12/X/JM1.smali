.class public LX/JM1;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "PrivacyCheckupReactModule"
.end annotation


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/1Kf;

.field public final c:LX/J3t;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final f:LX/14x;

.field private final g:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/viewercontext/ViewerContext;LX/J3t;LX/0SG;LX/1Kf;LX/0Ot;LX/14x;Lcom/facebook/content/SecureContextHelper;LX/5pY;)V
    .locals 0
    .param p8    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/J3t;",
            "LX/0SG;",
            "LX/1Kf;",
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;",
            "LX/14x;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/5pY;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2682830
    invoke-direct {p0, p8}, LX/5pb;-><init>(LX/5pY;)V

    .line 2682831
    iput-object p1, p0, LX/JM1;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2682832
    iput-object p2, p0, LX/JM1;->c:LX/J3t;

    .line 2682833
    iput-object p3, p0, LX/JM1;->a:LX/0SG;

    .line 2682834
    iput-object p4, p0, LX/JM1;->b:LX/1Kf;

    .line 2682835
    iput-object p5, p0, LX/JM1;->d:LX/0Ot;

    .line 2682836
    iput-object p6, p0, LX/JM1;->f:LX/14x;

    .line 2682837
    iput-object p7, p0, LX/JM1;->g:Lcom/facebook/content/SecureContextHelper;

    .line 2682838
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2682818
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2682819
    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2682829
    const-string v0, "PrivacyCheckupReactModule"

    return-object v0
.end method

.method public sendPrivacyEdits(LX/5pC;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 8
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2682839
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2682840
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 2682841
    invoke-interface {p1, v6}, LX/5pC;->a(I)LX/5pG;

    move-result-object v0

    .line 2682842
    const-string v1, "legacy_graph_api_privacy_json"

    invoke-interface {v0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2682843
    const-string v1, "fbid"

    invoke-interface {v0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2682844
    const-string v2, "fbid_type"

    invoke-interface {v0, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2682845
    new-instance v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;

    iget-object v2, p0, LX/JM1;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;-><init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;Ljava/lang/String;)V

    .line 2682846
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2682847
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2682848
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2682849
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2682850
    new-instance v2, Lcom/facebook/fbreact/privacy/PrivacyCheckupReactModule$1;

    invoke-direct {v2, p0, v0, p2, p3}, Lcom/facebook/fbreact/privacy/PrivacyCheckupReactModule$1;-><init>(LX/JM1;LX/0Px;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    invoke-virtual {v1, v2}, LX/5pX;->a(Ljava/lang/Runnable;)V

    .line 2682851
    return-void
.end method

.method public share(Ljava/lang/String;)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2682825
    sget-object v0, LX/21D;->PRIVACY_CHECKUP:LX/21D;

    const-string v1, "privacyCheckupShareReact"

    invoke-static {p1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v2

    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2682826
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2682827
    iget-object v1, p0, LX/JM1;->b:LX/1Kf;

    const/4 v2, 0x0

    const/16 v3, 0x6dc

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v4

    invoke-interface {v1, v2, v0, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2682828
    return-void
.end method

.method public shareWithMessenger(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2682820
    iget-object v0, p0, LX/JM1;->f:LX/14x;

    invoke-virtual {v0}, LX/14x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2682821
    iget-object v0, p0, LX/JM1;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9y;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    const-string v5, "share_link_url"

    move-object v2, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, LX/B9y;->a(Landroid/content/Context;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 2682822
    :goto_0
    return-void

    .line 2682823
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/facebook/katana/orca/DiodeMessengerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2682824
    iget-object v1, p0, LX/JM1;->g:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
