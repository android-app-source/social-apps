.class public final enum LX/IEz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IEz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IEz;

.field public static final enum LAUNCH_SIMPLE_STICKER_PICKER:LX/IEz;

.field public static final enum STICKER_UPSELL_BUTTON_IMPRESSION:LX/IEz;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2553520
    new-instance v0, LX/IEz;

    const-string v1, "STICKER_UPSELL_BUTTON_IMPRESSION"

    invoke-direct {v0, v1, v2}, LX/IEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IEz;->STICKER_UPSELL_BUTTON_IMPRESSION:LX/IEz;

    .line 2553521
    new-instance v0, LX/IEz;

    const-string v1, "LAUNCH_SIMPLE_STICKER_PICKER"

    invoke-direct {v0, v1, v3}, LX/IEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IEz;->LAUNCH_SIMPLE_STICKER_PICKER:LX/IEz;

    .line 2553522
    const/4 v0, 0x2

    new-array v0, v0, [LX/IEz;

    sget-object v1, LX/IEz;->STICKER_UPSELL_BUTTON_IMPRESSION:LX/IEz;

    aput-object v1, v0, v2

    sget-object v1, LX/IEz;->LAUNCH_SIMPLE_STICKER_PICKER:LX/IEz;

    aput-object v1, v0, v3

    sput-object v0, LX/IEz;->$VALUES:[LX/IEz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2553518
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IEz;
    .locals 1

    .prologue
    .line 2553519
    const-class v0, LX/IEz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IEz;

    return-object v0
.end method

.method public static values()[LX/IEz;
    .locals 1

    .prologue
    .line 2553517
    sget-object v0, LX/IEz;->$VALUES:[LX/IEz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IEz;

    return-object v0
.end method
