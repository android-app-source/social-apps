.class public LX/Ifk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field private static final e:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2600106
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "invite_friends/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600107
    sput-object v0, LX/Ifk;->e:LX/0Tn;

    const-string v1, "last_upsell_decline_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ifk;->a:LX/0Tn;

    .line 2600108
    sget-object v0, LX/Ifk;->e:LX/0Tn;

    const-string v1, "upsell_decline_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ifk;->b:LX/0Tn;

    .line 2600109
    sget-object v0, LX/Ifk;->e:LX/0Tn;

    const-string v1, "sent_invites"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ifk;->c:LX/0Tn;

    .line 2600110
    sget-object v0, LX/Ifk;->e:LX/0Tn;

    const-string v1, "has_seen_invites_interstitial"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ifk;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2600111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600112
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2600113
    sget-object v0, LX/Ifk;->e:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
