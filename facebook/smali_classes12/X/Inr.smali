.class public final LX/Inr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ijx;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 0

    .prologue
    .line 2611101
    iput-object p1, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2611091
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->i:LX/Duk;

    .line 2611092
    invoke-static {v0}, LX/Duk;->d(LX/Duk;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2611093
    iget-object v1, v0, LX/Duk;->e:LX/3RX;

    const v2, 0x7f070082

    iget-object v3, v0, LX/Duk;->f:LX/3RZ;

    invoke-virtual {v3}, LX/3RZ;->a()I

    move-result v3

    const v4, 0x3e70a3d7    # 0.235f

    invoke-virtual {v1, v2, v3, v4}, LX/3RX;->a(IIF)LX/7Cb;

    .line 2611094
    :cond_0
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611095
    return-void
.end method

.method public final a(Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/payments/auth/model/NuxFollowUpAction;)V
    .locals 2

    .prologue
    .line 2611096
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611097
    iput-object p2, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 2611098
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(LX/0am;)V

    .line 2611099
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611100
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2611083
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611084
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2611085
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->p:LX/Inc;

    iget-object v1, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611086
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v2

    .line 2611087
    iget-object v2, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v2, v2, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/Inc;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611088
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2611089
    iget-object v0, p0, LX/Inr;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611090
    return-void
.end method
