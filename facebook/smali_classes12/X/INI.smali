.class public LX/INI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/INI;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2570731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2570732
    return-void
.end method

.method public static a(LX/0QB;)LX/INI;
    .locals 3

    .prologue
    .line 2570719
    sget-object v0, LX/INI;->b:LX/INI;

    if-nez v0, :cond_1

    .line 2570720
    const-class v1, LX/INI;

    monitor-enter v1

    .line 2570721
    :try_start_0
    sget-object v0, LX/INI;->b:LX/INI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2570722
    if-eqz v2, :cond_0

    .line 2570723
    :try_start_1
    new-instance v0, LX/INI;

    invoke-direct {v0}, LX/INI;-><init>()V

    .line 2570724
    move-object v0, v0

    .line 2570725
    sput-object v0, LX/INI;->b:LX/INI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2570726
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2570727
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2570728
    :cond_1
    sget-object v0, LX/INI;->b:LX/INI;

    return-object v0

    .line 2570729
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2570730
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
