.class public final LX/Icc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:LX/Icd;


# direct methods
.method public constructor <init>(LX/Icd;)V
    .locals 0

    .prologue
    .line 2595661
    iput-object p1, p0, LX/Icc;->a:LX/Icd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 6

    .prologue
    .line 2595662
    invoke-virtual {p1}, LX/6al;->d()Landroid/location/Location;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2595663
    :goto_0
    return-void

    .line 2595664
    :cond_0
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p1}, LX/6al;->d()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, LX/6al;->d()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const/high16 v1, 0x41700000    # 15.0f

    invoke-static {v0, v1}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v0

    const/16 v1, 0x1f4

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/6al;->a(LX/6aM;ILX/6aj;)V

    goto :goto_0
.end method
