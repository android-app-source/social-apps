.class public LX/JQP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLJobOpening;)V
    .locals 1

    .prologue
    .line 2691750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2691751
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/JQP;->b:Ljava/lang/String;

    .line 2691752
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, LX/JQP;->c:Ljava/lang/String;

    .line 2691753
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JQP;->e:Ljava/lang/String;

    .line 2691754
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, p0, LX/JQP;->d:Ljava/lang/String;

    .line 2691755
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->k()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->k()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->k()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, p0, LX/JQP;->a:Ljava/lang/String;

    .line 2691756
    return-void

    .line 2691757
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 2691758
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 2691759
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 2691760
    :cond_3
    const-string v0, ""

    goto :goto_3
.end method
