.class public LX/ImI;
.super LX/ImH;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final k:Ljava/lang/Object;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/J0S;

.field private final d:LX/J0B;

.field private final e:LX/18V;

.field private final f:LX/2JS;

.field public final g:LX/0Zb;

.field private final h:LX/2JT;

.field private final i:LX/Izp;

.field private final j:LX/Izk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609223
    const-class v0, LX/ImI;

    sput-object v0, LX/ImI;->a:Ljava/lang/Class;

    .line 2609224
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImI;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/J0S;LX/J0B;LX/18V;LX/2JS;LX/0Zb;LX/2JT;LX/Izp;LX/Izk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609225
    invoke-direct {p0}, LX/ImH;-><init>()V

    .line 2609226
    iput-object p1, p0, LX/ImI;->b:LX/03V;

    .line 2609227
    iput-object p2, p0, LX/ImI;->c:LX/J0S;

    .line 2609228
    iput-object p3, p0, LX/ImI;->d:LX/J0B;

    .line 2609229
    iput-object p4, p0, LX/ImI;->e:LX/18V;

    .line 2609230
    iput-object p5, p0, LX/ImI;->f:LX/2JS;

    .line 2609231
    iput-object p6, p0, LX/ImI;->g:LX/0Zb;

    .line 2609232
    iput-object p7, p0, LX/ImI;->h:LX/2JT;

    .line 2609233
    iput-object p8, p0, LX/ImI;->i:LX/Izp;

    .line 2609234
    iput-object p9, p0, LX/ImI;->j:LX/Izk;

    .line 2609235
    return-void
.end method

.method public static a(LX/0QB;)LX/ImI;
    .locals 7

    .prologue
    .line 2609236
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609237
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609238
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609239
    if-nez v1, :cond_0

    .line 2609240
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609241
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609242
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609243
    sget-object v1, LX/ImI;->k:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609244
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609245
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609246
    :cond_1
    if-nez v1, :cond_4

    .line 2609247
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609248
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609249
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/ImI;->b(LX/0QB;)LX/ImI;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2609250
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609251
    if-nez v1, :cond_2

    .line 2609252
    sget-object v0, LX/ImI;->k:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImI;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609253
    :goto_1
    if-eqz v0, :cond_3

    .line 2609254
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609255
    :goto_3
    check-cast v0, LX/ImI;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609256
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609257
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609258
    :catchall_1
    move-exception v0

    .line 2609259
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609260
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609261
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609262
    :cond_2
    :try_start_8
    sget-object v0, LX/ImI;->k:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImI;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/ImI;
    .locals 10

    .prologue
    .line 2609263
    new-instance v0, LX/ImI;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/J0S;->a(LX/0QB;)LX/J0S;

    move-result-object v2

    check-cast v2, LX/J0S;

    invoke-static {p0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v3

    check-cast v3, LX/J0B;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {p0}, LX/2JS;->a(LX/0QB;)LX/2JS;

    move-result-object v5

    check-cast v5, LX/2JS;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {p0}, LX/2JT;->a(LX/0QB;)LX/2JT;

    move-result-object v7

    check-cast v7, LX/2JT;

    invoke-static {p0}, LX/Izp;->a(LX/0QB;)LX/Izp;

    move-result-object v8

    check-cast v8, LX/Izp;

    invoke-static {p0}, LX/Izk;->a(LX/0QB;)LX/Izk;

    move-result-object v9

    check-cast v9, LX/Izk;

    invoke-direct/range {v0 .. v9}, LX/ImI;-><init>(LX/03V;LX/J0S;LX/J0B;LX/18V;LX/2JS;LX/0Zb;LX/2JT;LX/Izp;LX/Izk;)V

    .line 2609264
    return-object v0
.end method


# virtual methods
.method public final a(LX/7GJ;)Landroid/os/Bundle;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2609265
    iget-object v0, p1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v0}, LX/Ipt;->h()LX/Ipk;

    move-result-object v3

    .line 2609266
    const/4 v1, 0x0

    .line 2609267
    iget-object v0, v3, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v3, LX/Ipk;->themeId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2609268
    if-eqz v0, :cond_2

    .line 2609269
    :try_start_0
    iget-object v0, p0, LX/ImI;->e:LX/18V;

    iget-object v2, p0, LX/ImI;->c:LX/J0S;

    new-instance v4, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;

    iget-object v5, v3, LX/Ipk;->requestFbId:Ljava/lang/Long;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v4}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2609270
    :goto_1
    iget-object v1, p0, LX/ImI;->h:LX/2JT;

    invoke-virtual {v1, v0}, LX/2JT;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2609271
    iget-object v1, p0, LX/ImI;->j:LX/Izk;

    invoke-virtual {v1, v0}, LX/Izk;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2609272
    :cond_1
    :goto_2
    iget-object v1, p0, LX/ImI;->g:LX/0Zb;

    const-string v2, "p2p_sync_delta"

    const-string v4, "p2p_request"

    invoke-static {v2, v4}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v2

    const-string v4, "DeltaNewPaymentRequest"

    invoke-virtual {v2, v4}, LX/5fz;->j(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    iget-object v4, v3, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v2, v4}, LX/5fz;->a(Ljava/lang/Long;)LX/5fz;

    move-result-object v2

    .line 2609273
    iget-object v4, v2, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v2, v4

    .line 2609274
    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2609275
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2609276
    const-string v2, "payment_request"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2609277
    return-object v1

    .line 2609278
    :catch_0
    iget-object v0, p0, LX/ImI;->b:LX/03V;

    sget-object v2, LX/ImI;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to fetch payment request with id "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v3, LX/Ipk;->requestFbId:Ljava/lang/Long;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2609279
    goto :goto_1

    .line 2609280
    :cond_2
    iget-object v0, p0, LX/ImI;->i:LX/Izp;

    iget-object v1, v3, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Izp;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    .line 2609281
    iget-object v0, p0, LX/ImI;->i:LX/Izp;

    iget-object v2, v3, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/Izp;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    .line 2609282
    sget-object v0, LX/Ipz;->b:Ljava/util/Map;

    iget-object v4, v3, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    move-result-object v0

    .line 2609283
    new-instance v4, LX/Dtl;

    invoke-direct {v4}, LX/Dtl;-><init>()V

    iget-object v5, v3, LX/Ipk;->currency:Ljava/lang/String;

    .line 2609284
    iput-object v5, v4, LX/Dtl;->b:Ljava/lang/String;

    .line 2609285
    move-object v4, v4

    .line 2609286
    iget-object v5, v3, LX/Ipk;->amount:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->intValue()I

    move-result v5

    .line 2609287
    iput v5, v4, LX/Dtl;->a:I

    .line 2609288
    move-object v4, v4

    .line 2609289
    iget-object v5, v3, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2609290
    iput v5, v4, LX/Dtl;->c:I

    .line 2609291
    move-object v4, v4

    .line 2609292
    invoke-virtual {v4}, LX/Dtl;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v4

    .line 2609293
    new-instance v5, LX/Dtm;

    invoke-direct {v5}, LX/Dtm;-><init>()V

    iget-object v6, v3, LX/Ipk;->requestFbId:Ljava/lang/Long;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2609294
    iput-object v6, v5, LX/Dtm;->d:Ljava/lang/String;

    .line 2609295
    move-object v5, v5

    .line 2609296
    iput-object v0, v5, LX/Dtm;->f:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 2609297
    move-object v0, v5

    .line 2609298
    iput-object v4, v0, LX/Dtm;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2609299
    move-object v0, v0

    .line 2609300
    iget-object v4, v3, LX/Ipk;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 2609301
    iput-wide v4, v0, LX/Dtm;->b:J

    .line 2609302
    move-object v0, v0

    .line 2609303
    iget-object v4, v3, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2609304
    iput-object v4, v0, LX/Dtm;->c:Ljava/lang/String;

    .line 2609305
    move-object v0, v0

    .line 2609306
    iget-object v4, v3, LX/Ipk;->memoText:Ljava/lang/String;

    .line 2609307
    iput-object v4, v0, LX/Dtm;->e:Ljava/lang/String;

    .line 2609308
    move-object v4, v0

    .line 2609309
    move-object v0, v1

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2609310
    iput-object v0, v4, LX/Dtm;->h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2609311
    move-object v1, v4

    .line 2609312
    move-object v0, v2

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2609313
    iput-object v0, v1, LX/Dtm;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2609314
    move-object v0, v1

    .line 2609315
    invoke-virtual {v0}, LX/Dtm;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    move-result-object v0

    goto/16 :goto_1

    .line 2609316
    :cond_3
    iget-object v1, p0, LX/ImI;->h:LX/2JT;

    invoke-virtual {v1, v0}, LX/2JT;->c(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2609317
    iget-object v1, p0, LX/ImI;->j:LX/Izk;

    invoke-virtual {v1, v0}, LX/Izk;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    goto/16 :goto_2

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2609318
    const-string v0, "payment_request"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2609319
    iget-object v1, p0, LX/ImI;->f:LX/2JS;

    invoke-virtual {v1, v0}, LX/2JS;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2609320
    iget-object v1, p0, LX/ImI;->d:LX/J0B;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/J0B;->a(Ljava/lang/String;)V

    .line 2609321
    return-void
.end method
