.class public LX/Hlk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field private static volatile z:LX/Hlk;


# instance fields
.field private final n:[F

.field private final o:Landroid/content/Context;

.field private final p:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final q:Landroid/app/PendingIntent;

.field private final r:LX/0SG;

.field private final s:LX/0So;

.field private final t:LX/03V;

.field private final u:LX/Hlj;

.field public final v:LX/2Hc;

.field public final w:LX/Hli;

.field public final x:LX/0W3;

.field private final y:LX/0Uo;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2499140
    const-class v0, LX/Hlk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hlk;->a:Ljava/lang/String;

    .line 2499141
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "geofence/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2499142
    sput-object v0, LX/Hlk;->b:LX/0Tn;

    const-string v1, "last_geofence/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2499143
    sput-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->d:LX/0Tn;

    .line 2499144
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "longitude"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->e:LX/0Tn;

    .line 2499145
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "accuracy_meters"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->f:LX/0Tn;

    .line 2499146
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "altitude_meters"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->g:LX/0Tn;

    .line 2499147
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "speed_meters_per_sec"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->h:LX/0Tn;

    .line 2499148
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "bearing_degrees"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->i:LX/0Tn;

    .line 2499149
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "elapsed_time_since_boot_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->j:LX/0Tn;

    .line 2499150
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "timestamp_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->k:LX/0Tn;

    .line 2499151
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "age_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->l:LX/0Tn;

    .line 2499152
    sget-object v0, LX/Hlk;->c:LX/0Tn;

    const-string v1, "radius_meters"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hlk;->m:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/0So;LX/2Hc;LX/Hli;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/Hlj;LX/0W3;LX/0Uo;)V
    .locals 4
    .param p3    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2499126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2499127
    const/4 v0, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, LX/Hlk;->n:[F

    .line 2499128
    iput-object p1, p0, LX/Hlk;->o:Landroid/content/Context;

    .line 2499129
    iput-object p2, p0, LX/Hlk;->r:LX/0SG;

    .line 2499130
    iput-object p3, p0, LX/Hlk;->s:LX/0So;

    .line 2499131
    iput-object p4, p0, LX/Hlk;->v:LX/2Hc;

    .line 2499132
    iput-object p5, p0, LX/Hlk;->w:LX/Hli;

    .line 2499133
    iput-object p6, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2499134
    iget-object v0, p0, LX/Hlk;->o:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/Hlk;->q:Landroid/app/PendingIntent;

    .line 2499135
    iput-object p7, p0, LX/Hlk;->t:LX/03V;

    .line 2499136
    iput-object p8, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499137
    iput-object p9, p0, LX/Hlk;->x:LX/0W3;

    .line 2499138
    iput-object p10, p0, LX/Hlk;->y:LX/0Uo;

    .line 2499139
    return-void
.end method

.method public static a(LX/0QB;)LX/Hlk;
    .locals 14

    .prologue
    .line 2499113
    sget-object v0, LX/Hlk;->z:LX/Hlk;

    if-nez v0, :cond_1

    .line 2499114
    const-class v1, LX/Hlk;

    monitor-enter v1

    .line 2499115
    :try_start_0
    sget-object v0, LX/Hlk;->z:LX/Hlk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2499116
    if-eqz v2, :cond_0

    .line 2499117
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2499118
    new-instance v3, LX/Hlk;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static {v0}, LX/2Hc;->a(LX/0QB;)LX/2Hc;

    move-result-object v7

    check-cast v7, LX/2Hc;

    invoke-static {v0}, LX/Hli;->a(LX/0QB;)LX/Hli;

    move-result-object v8

    check-cast v8, LX/Hli;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/Hlj;->a(LX/0QB;)LX/Hlj;

    move-result-object v11

    check-cast v11, LX/Hlj;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v12

    check-cast v12, LX/0W3;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v13

    check-cast v13, LX/0Uo;

    invoke-direct/range {v3 .. v13}, LX/Hlk;-><init>(Landroid/content/Context;LX/0SG;LX/0So;LX/2Hc;LX/Hli;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/Hlj;LX/0W3;LX/0Uo;)V

    .line 2499119
    move-object v0, v3

    .line 2499120
    sput-object v0, LX/Hlk;->z:LX/Hlk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2499121
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2499122
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2499123
    :cond_1
    sget-object v0, LX/Hlk;->z:LX/Hlk;

    return-object v0

    .line 2499124
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2499125
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized c(LX/Hlk;Lcom/facebook/location/ImmutableLocation;)V
    .locals 13

    .prologue
    .line 2499070
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499071
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2499072
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2499073
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "geofence_requested_start"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2499074
    :cond_0
    invoke-virtual {p0}, LX/Hlk;->a()V

    .line 2499075
    new-instance v0, LX/2vz;

    iget-object v1, p0, LX/Hlk;->o:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2vz;-><init>(Landroid/content/Context;)V

    sget-object v1, LX/2vm;->a:LX/2vs;

    invoke-virtual {v0, v1}, LX/2vz;->a(LX/2vs;)LX/2vz;

    move-result-object v0

    invoke-virtual {v0}, LX/2vz;->b()LX/2wX;

    move-result-object v7

    .line 2499076
    const-wide/16 v0, 0xa

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v0, v1, v2}, LX/2wX;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    .line 2499077
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2499078
    iget-object v1, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499079
    invoke-static {v1}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v2

    .line 2499080
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2499081
    invoke-static {v1, v2, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v2

    const-string v3, "action"

    const-string v4, "geofence_requested_failure"

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "sub_action"

    const-string v4, "google_api_client_failed"

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "api_error_code"

    iget v4, v0, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v4, v4

    .line 2499082
    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v2

    invoke-virtual {v2}, LX/0oG;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2499083
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 2499084
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v2

    .line 2499085
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    .line 2499086
    iget-object v0, p0, LX/Hlk;->x:LX/0W3;

    sget-wide v8, LX/0X5;->an:J

    invoke-interface {v0, v8, v9}, LX/0W4;->g(J)D

    move-result-wide v0

    double-to-float v6, v0

    .line 2499087
    iget-object v0, p0, LX/Hlk;->x:LX/0W3;

    sget-wide v8, LX/0X5;->ao:J

    const v1, 0xea60

    invoke-interface {v0, v8, v9, v1}, LX/0W4;->a(JI)I

    move-result v8

    .line 2499088
    new-instance v0, LX/7Zw;

    invoke-direct {v0}, LX/7Zw;-><init>()V

    const-string v1, "geofence"

    iput-object v1, v0, LX/7Zw;->a:Ljava/lang/String;

    move-object v1, v0

    .line 2499089
    const/4 v12, 0x1

    iput-short v12, v1, LX/7Zw;->d:S

    iput-wide v2, v1, LX/7Zw;->e:D

    iput-wide v4, v1, LX/7Zw;->f:D

    iput v6, v1, LX/7Zw;->g:F

    move-object v0, v1

    .line 2499090
    const-wide/16 v10, -0x1

    invoke-virtual {v0, v10, v11}, LX/7Zw;->a(J)LX/7Zw;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, LX/7Zw;->b:I

    move-object v0, v0

    .line 2499091
    iput v8, v0, LX/7Zw;->h:I

    move-object v0, v0

    .line 2499092
    invoke-virtual {v0}, LX/7Zw;->a()LX/7Zx;

    move-result-object v0

    .line 2499093
    new-instance v1, LX/7Zz;

    invoke-direct {v1}, LX/7Zz;-><init>()V

    const-string v9, "geofence can\'t be null."

    invoke-static {v0, v9}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v9, v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    const-string v10, "Geofence must be created using Geofence.Builder."

    invoke-static {v9, v10}, LX/1ol;->b(ZLjava/lang/Object;)V

    iget-object v9, v1, LX/7Zz;->a:Ljava/util/List;

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 2499094
    const/4 v9, 0x0

    iget-object v1, v0, LX/7Zz;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    :goto_1
    const-string v10, "No geofence has been added to this request."

    invoke-static {v1, v10}, LX/1ol;->b(ZLjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/location/GeofencingRequest;

    iget-object v10, v0, LX/7Zz;->a:Ljava/util/List;

    iget v11, v0, LX/7Zz;->b:I

    invoke-direct {v1, v10, v11}, Lcom/google/android/gms/location/GeofencingRequest;-><init>(Ljava/util/List;I)V

    move-object v0, v1

    .line 2499095
    sget-object v1, LX/2vm;->c:LX/2vw;

    iget-object v9, p0, LX/Hlk;->q:Landroid/app/PendingIntent;

    invoke-interface {v1, v7, v0, v9}, LX/2vw;->a(LX/2wX;Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;)LX/2wg;

    move-result-object v0

    const-wide/16 v10, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v10, v11, v1}, LX/2wg;->a(JLjava/util/concurrent/TimeUnit;)LX/2NW;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/common/api/Status;

    .line 2499096
    invoke-virtual {v9}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2499097
    invoke-virtual {v7}, LX/2wX;->g()V

    .line 2499098
    iget-object v4, p0, LX/Hlk;->u:LX/Hlj;

    int-to-long v7, v8

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, LX/Hlj;->a(Lcom/facebook/location/ImmutableLocation;FJLcom/google/android/gms/common/api/Status;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 2499099
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2499100
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hlk;->d:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;D)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hlk;->e:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, LX/0hN;->a(LX/0Tn;D)LX/0hN;

    move-result-object v1

    sget-object v2, LX/Hlk;->f:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;F)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hlk;->j:LX/0Tn;

    iget-object v2, p0, LX/Hlk;->s:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hlk;->k:LX/0Tn;

    iget-object v2, p0, LX/Hlk;->r:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hlk;->m:LX/0Tn;

    invoke-interface {v0, v1, v6}, LX/0hN;->a(LX/0Tn;F)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hlk;->l:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    .line 2499101
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->d()LX/0am;

    move-result-object v0

    .line 2499102
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2499103
    sget-object v2, LX/Hlk;->g:LX/0Tn;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;D)LX/0hN;

    .line 2499104
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->f()LX/0am;

    move-result-object v0

    .line 2499105
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2499106
    sget-object v2, LX/Hlk;->h:LX/0Tn;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;F)LX/0hN;

    .line 2499107
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->e()LX/0am;

    move-result-object v0

    .line 2499108
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2499109
    sget-object v2, LX/Hlk;->i:LX/0Tn;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;F)LX/0hN;

    .line 2499110
    :cond_6
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2499111
    invoke-virtual {v7}, LX/2wX;->g()V

    .line 2499112
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    invoke-virtual {v0, p1, v6, v8}, LX/Hlj;->a(Lcom/facebook/location/ImmutableLocation;FI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_7
    move v1, v9

    goto/16 :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 14

    .prologue
    .line 2498916
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2498917
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2498918
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2498919
    const-string v2, "action"

    const-string v3, "last_geofence_flushed_start"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2498920
    :cond_0
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hlk;->l:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2498921
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2498922
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2498923
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2498924
    const-string v2, "action"

    const-string v3, "last_geofence_flushed_cancel"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2498925
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 2498926
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hlk;->d:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v0

    .line 2498927
    iget-object v2, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Hlk;->e:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v2

    .line 2498928
    iget-object v4, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Hlk;->l:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 2498929
    iget-object v6, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/Hlk;->k:LX/0Tn;

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 2498930
    add-long/2addr v4, v6

    .line 2498931
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0z7;->c(J)LX/0z7;

    move-result-object v0

    .line 2498932
    iput-wide v6, v0, LX/0z7;->b:J

    .line 2498933
    move-object v0, v0

    .line 2498934
    iput-wide v4, v0, LX/0z7;->c:J

    .line 2498935
    move-object v0, v0

    .line 2498936
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->m:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;F)F

    move-result v1

    .line 2498937
    iput v1, v0, LX/0z7;->d:F

    .line 2498938
    move-object v0, v0

    .line 2498939
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->f:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;F)F

    move-result v1

    invoke-virtual {v0, v1}, LX/0z7;->b(F)LX/0z7;

    move-result-object v0

    .line 2498940
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->i:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2498941
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->i:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;F)F

    move-result v1

    .line 2498942
    iget-object v2, v0, LX/0z7;->a:Landroid/location/Location;

    invoke-virtual {v2, v1}, Landroid/location/Location;->setBearing(F)V

    .line 2498943
    :cond_3
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->h:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2498944
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->h:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;F)F

    move-result v1

    .line 2498945
    iget-object v2, v0, LX/0z7;->a:Landroid/location/Location;

    invoke-virtual {v2, v1}, Landroid/location/Location;->setSpeed(F)V

    .line 2498946
    :cond_4
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->g:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2498947
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->g:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v2

    .line 2498948
    iget-object v1, v0, LX/0z7;->a:Landroid/location/Location;

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setAltitude(D)V

    .line 2498949
    :cond_5
    invoke-virtual {v0}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 2498950
    new-instance v1, LX/2TT;

    invoke-direct {v1}, LX/2TT;-><init>()V

    .line 2498951
    iput-object v0, v1, LX/2TT;->a:Lcom/facebook/location/ImmutableLocation;

    .line 2498952
    move-object v1, v1

    .line 2498953
    iget-object v2, p0, LX/Hlk;->y:LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->l()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2498954
    iput-object v2, v1, LX/2TT;->b:Ljava/lang/Boolean;

    .line 2498955
    move-object v1, v1

    .line 2498956
    invoke-virtual {v1}, LX/2TT;->a()Lcom/facebook/location/LocationSignalDataPackage;

    move-result-object v1

    .line 2498957
    iget-object v10, p0, LX/Hlk;->x:LX/0W3;

    sget-wide v12, LX/0X5;->ai:J

    invoke-interface {v10, v12, v13}, LX/0W4;->a(J)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2498958
    iget-object v10, p0, LX/Hlk;->w:LX/Hli;

    .line 2498959
    const/4 v11, 0x0

    invoke-virtual {v10, v1, v11}, LX/Hli;->a(Lcom/facebook/location/LocationSignalDataPackage;Z)V

    .line 2498960
    :goto_1
    iget-object v1, p0, LX/Hlk;->u:LX/Hlj;

    .line 2498961
    invoke-static {v1}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v2

    .line 2498962
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2498963
    invoke-static {v1, v2, v0}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v2

    const-string v3, "action"

    const-string v4, "last_geofence_flushed_server_request_sent"

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    .line 2498964
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->k()LX/0am;

    move-result-object v3

    .line 2498965
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2498966
    const-string v4, "geofence_radius_meters"

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2498967
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->i()LX/0am;

    move-result-object v3

    .line 2498968
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2498969
    const-string v4, "geofence_start_time_ms"

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2498970
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->j()LX/0am;

    move-result-object v3

    .line 2498971
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2498972
    const-string v4, "geofence_end_time_ms"

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2498973
    :cond_8
    invoke-virtual {v2}, LX/0oG;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2498974
    :cond_9
    :try_start_2
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hlk;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 2498975
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2498976
    :catchall_1
    move-exception v0

    :try_start_3
    iget-object v1, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Hlk;->c:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2498977
    :cond_a
    iget-object v10, p0, LX/Hlk;->v:LX/2Hc;

    invoke-virtual {v10, v1}, LX/2Hc;->a(Lcom/facebook/location/LocationSignalDataPackage;)V

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/facebook/location/ImmutableLocation;)V
    .locals 12

    .prologue
    .line 2499012
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499013
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2499014
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2499015
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "location_update_received_start"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2499016
    :cond_0
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2499017
    iget-object v0, p0, LX/Hlk;->t:LX/03V;

    sget-object v1, LX/Hlk;->a:Ljava/lang/String;

    const-string v2, "FbSharedPreferences is not initialized"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2499018
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499019
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2499020
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2499021
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "location_update_received"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "sub_action"

    const-string v3, "shared_prefs_not_initialized"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2499022
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 2499023
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hlk;->j:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2499024
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499025
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2499026
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2499027
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "location_update_received"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "sub_action"

    const-string v3, "geofence_was_not_set"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2499028
    :cond_3
    invoke-static {p0, p1}, LX/Hlk;->c(LX/Hlk;Lcom/facebook/location/ImmutableLocation;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2499029
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2499030
    :cond_4
    :try_start_2
    iget-object v0, p0, LX/Hlk;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2499031
    iget-object v2, p0, LX/Hlk;->r:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2499032
    iget-object v4, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Hlk;->j:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 2499033
    iget-object v6, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/Hlk;->k:LX/0Tn;

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 2499034
    sub-long/2addr v0, v4

    .line 2499035
    sub-long/2addr v2, v6

    .line 2499036
    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_6

    .line 2499037
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499038
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2499039
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2499040
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "location_update_received"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "sub_action"

    const-string v3, "location_has_inconsistent_timestamp"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2499041
    :cond_5
    invoke-static {p0, p1}, LX/Hlk;->c(LX/Hlk;Lcom/facebook/location/ImmutableLocation;)V

    goto/16 :goto_0

    .line 2499042
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2499043
    sub-long v10, v0, v6

    .line 2499044
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-gez v0, :cond_8

    .line 2499045
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499046
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2499047
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2499048
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "location_update_received"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "sub_action"

    const-string v3, "location_older_than_last_geofence"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "delta_wall_time_to_last_geofence_ms"

    invoke-virtual {v1, v2, v10, v11}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2499049
    :cond_7
    goto/16 :goto_0

    .line 2499050
    :cond_8
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hlk;->d:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v4

    .line 2499051
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hlk;->e:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v6

    .line 2499052
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v2

    iget-object v8, p0, LX/Hlk;->n:[F

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 2499053
    iget-object v0, p0, LX/Hlk;->n:[F

    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2499054
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Hlk;->m:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;F)F

    move-result v0

    float-to-double v2, v0

    .line 2499055
    float-to-double v4, v1

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v6, v0

    add-double/2addr v6, v2

    cmpl-double v0, v4, v6

    if-lez v0, :cond_9

    .line 2499056
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/Hlj;->a(Lcom/facebook/location/ImmutableLocation;FD)V

    .line 2499057
    invoke-static {p0, p1}, LX/Hlk;->c(LX/Hlk;Lcom/facebook/location/ImmutableLocation;)V

    goto/16 :goto_0

    .line 2499058
    :cond_9
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hlk;->l:LX/0Tn;

    invoke-interface {v0, v1, v10, v11}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2499059
    const-wide/32 v0, 0x112a880

    cmp-long v0, v10, v0

    if-lez v0, :cond_b

    .line 2499060
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    const-wide/32 v2, 0x112a880

    .line 2499061
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2499062
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2499063
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v4, "action"

    const-string v5, "location_update_received"

    invoke-virtual {v1, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v4, "sub_action"

    const-string v5, "time_threshold_to_restart_geofence_exceeded"

    invoke-virtual {v1, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v4, "time_threshold_to_restart_geofence_ms"

    invoke-virtual {v1, v4, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2499064
    :cond_a
    invoke-static {p0, p1}, LX/Hlk;->c(LX/Hlk;Lcom/facebook/location/ImmutableLocation;)V

    goto/16 :goto_0

    .line 2499065
    :cond_b
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499066
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2499067
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2499068
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "location_update_received"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "sub_action"

    const-string v3, "no_need_to_request_geofence"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2499069
    :cond_c
    goto/16 :goto_0
.end method

.method public final b()LX/0Rf;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2499011
    sget-object v0, LX/Hlk;->f:LX/0Tn;

    sget-object v1, LX/Hlk;->l:LX/0Tn;

    sget-object v2, LX/Hlk;->g:LX/0Tn;

    sget-object v3, LX/Hlk;->i:LX/0Tn;

    sget-object v4, LX/Hlk;->j:LX/0Tn;

    sget-object v5, LX/Hlk;->d:LX/0Tn;

    const/4 v6, 0x4

    new-array v6, v6, [LX/0Tn;

    const/4 v7, 0x0

    sget-object v8, LX/Hlk;->e:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, LX/Hlk;->m:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, LX/Hlk;->h:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, LX/Hlk;->k:LX/0Tn;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized b(Lcom/facebook/location/ImmutableLocation;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2498978
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2498979
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2498980
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2498981
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "geofence_exited_start"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2498982
    :cond_0
    iget-object v0, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hlk;->k:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2498983
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2498984
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2498985
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2498986
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "geofence_exited"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "sub_action"

    const-string v3, "geofence_was_not_set"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2498987
    :cond_1
    invoke-static {p0, p1}, LX/Hlk;->c(LX/Hlk;Lcom/facebook/location/ImmutableLocation;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2498988
    :goto_0
    monitor-exit p0

    return-void

    .line 2498989
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    .line 2498990
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2498991
    iget-object v0, p0, LX/Hlk;->u:LX/Hlj;

    .line 2498992
    invoke-static {v0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v1

    .line 2498993
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2498994
    invoke-static {v0, v1, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "geofence_exited"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "sub_action"

    const-string v3, "location_lacking_timestamp"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2498995
    :cond_3
    invoke-static {p0, p1}, LX/Hlk;->c(LX/Hlk;Lcom/facebook/location/ImmutableLocation;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2498996
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2498997
    :cond_4
    :try_start_2
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Hlk;->k:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 2498998
    cmp-long v2, v0, v6

    if-gez v2, :cond_6

    .line 2498999
    iget-object v2, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499000
    invoke-static {v2}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v3

    .line 2499001
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2499002
    invoke-static {v2, v3, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v3

    const-string v4, "action"

    const-string v5, "geofence_exited"

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "sub_action"

    const-string v5, "location_older_than_last_geofence"

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "delta_wall_time_to_last_geofence_ms"

    invoke-virtual {v3, v4, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2499003
    :cond_5
    goto :goto_0

    .line 2499004
    :cond_6
    iget-object v2, p0, LX/Hlk;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/Hlk;->l:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2499005
    invoke-static {p0, p1}, LX/Hlk;->c(LX/Hlk;Lcom/facebook/location/ImmutableLocation;)V

    .line 2499006
    iget-object v2, p0, LX/Hlk;->u:LX/Hlj;

    .line 2499007
    invoke-static {v2}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v3

    .line 2499008
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2499009
    invoke-static {v2, v3, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v3

    const-string v4, "action"

    const-string v5, "geofence_exited"

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "sub_action"

    const-string v5, "last_geofence_flushed"

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "delta_wall_time_to_last_geofence_ms"

    invoke-virtual {v3, v4, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2499010
    :cond_7
    goto/16 :goto_0
.end method
