.class public final LX/HF5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HEy;

.field public final synthetic b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;LX/HEy;)V
    .locals 0

    .prologue
    .line 2443001
    iput-object p1, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iput-object p2, p0, LX/HF5;->a:LX/HEy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x6474f2db

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2443002
    iget-object v0, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443003
    iget-object v2, v0, LX/HFJ;->c:LX/HEu;

    move-object v0, v2

    .line 2443004
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    iget-object v2, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443005
    iget-object v3, v2, LX/HFJ;->c:LX/HEu;

    move-object v2, v3

    .line 2443006
    iget-object v3, v2, LX/HEu;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2443007
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2443008
    iget-object v0, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->g:LX/HFf;

    iget-object v2, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443009
    iget-object v3, v2, LX/HFJ;->c:LX/HEu;

    move-object v2, v3

    .line 2443010
    iget-object v3, v2, LX/HEu;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2443011
    iget-object v3, v0, LX/HFf;->a:LX/0if;

    sget-object v4, LX/0ig;->ao:LX/0ih;

    const-string v5, "change_super_category"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p1, "category: "

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2443012
    :cond_0
    iget-object v2, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443013
    iget-object v3, v0, LX/HFJ;->c:LX/HEu;

    move-object v0, v3

    .line 2443014
    iget-object v3, v0, LX/HEu;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2443015
    :goto_0
    iput-object v0, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    .line 2443016
    new-instance v0, LX/HEz;

    iget-object v2, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, LX/HEz;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2443017
    iget-object v2, p0, LX/HF5;->a:LX/HEy;

    invoke-virtual {v0, v2}, LX/HEz;->a(LX/HEy;)V

    .line 2443018
    iget-object v2, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, LX/0ht;->a(Landroid/view/View;)V

    .line 2443019
    const v0, -0x7e52d6f6

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2443020
    :cond_1
    iget-object v0, p0, LX/HF5;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    goto :goto_0
.end method
