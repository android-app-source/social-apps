.class public LX/IBk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2547567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2547568
    iput v0, p0, LX/IBk;->a:I

    .line 2547569
    iput v0, p0, LX/IBk;->b:I

    .line 2547570
    return-void
.end method

.method public static a(ZLcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2547571
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p1, v2, :cond_3

    .line 2547572
    if-nez p0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v1

    .line 2547573
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    const/4 v1, 0x2

    :cond_2
    return v1

    .line 2547574
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p1, v2, :cond_1

    .line 2547575
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p3, v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/IBk;
    .locals 1

    .prologue
    .line 2547576
    new-instance v0, LX/IBk;

    invoke-direct {v0}, LX/IBk;-><init>()V

    .line 2547577
    move-object v0, v0

    .line 2547578
    return-object v0
.end method
