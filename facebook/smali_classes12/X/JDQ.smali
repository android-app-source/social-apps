.class public final LX/JDQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

.field public final synthetic b:Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 0

    .prologue
    .line 2663947
    iput-object p1, p0, LX/JDQ;->b:Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;

    iput-object p2, p0, LX/JDQ;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x47280040    # 43008.25f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2663948
    iget-object v0, p0, LX/JDQ;->b:Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->b:LX/1Uk;

    invoke-virtual {v0}, LX/1Uk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0ax;->bC:Ljava/lang/String;

    .line 2663949
    :goto_0
    iget-object v2, p0, LX/JDQ;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->mS_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2663950
    iget-object v2, p0, LX/JDQ;->b:Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;

    iget-object v2, v2, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/JDQ;->b:Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;

    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2663951
    const v0, 0x537f65f8

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2663952
    :cond_0
    sget-object v0, LX/0ax;->bB:Ljava/lang/String;

    goto :goto_0
.end method
