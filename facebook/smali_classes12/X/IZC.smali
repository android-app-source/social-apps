.class public LX/IZC;
.super LX/6LI;
.source ""


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public final b:Landroid/content/res/Resources;

.field public final c:Landroid/content/Context;

.field private final d:LX/6LR;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Landroid/content/Context;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2588577
    const-string v0, "BusinessRequestErrorBannerNotification"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 2588578
    iput-object p1, p0, LX/IZC;->a:Landroid/view/LayoutInflater;

    .line 2588579
    iput-object p2, p0, LX/IZC;->b:Landroid/content/res/Resources;

    .line 2588580
    iput-object p3, p0, LX/IZC;->c:Landroid/content/Context;

    .line 2588581
    new-instance v0, LX/2M6;

    invoke-direct {v0}, LX/2M6;-><init>()V

    .line 2588582
    iget-object v1, p0, LX/IZC;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    const/4 p1, 0x0

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 2588583
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2588584
    iget-object v1, p0, LX/IZC;->b:Landroid/content/res/Resources;

    const v2, 0x7f081c9e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2588585
    :goto_0
    move-object v1, v1

    .line 2588586
    iput-object v1, v0, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 2588587
    move-object v0, v0

    .line 2588588
    iget-object v1, p0, LX/IZC;->b:Landroid/content/res/Resources;

    const v2, 0x7f0a063f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2588589
    iput-object v1, v0, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 2588590
    move-object v0, v0

    .line 2588591
    sget-object v1, LX/2M7;->ALWAYS:LX/2M7;

    .line 2588592
    iput-object v1, v0, LX/2M6;->h:LX/2M7;

    .line 2588593
    move-object v0, v0

    .line 2588594
    invoke-virtual {v0}, LX/2M6;->a()LX/6LR;

    move-result-object v0

    iput-object v0, p0, LX/IZC;->d:LX/6LR;

    .line 2588595
    return-void

    :cond_0
    iget-object v1, p0, LX/IZC;->b:Landroid/content/res/Resources;

    const v2, 0x7f081c9d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2588574
    iget-object v0, p0, LX/IZC;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f03018f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/banner/BasicBannerNotificationView;

    .line 2588575
    iget-object v1, p0, LX/IZC;->d:LX/6LR;

    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 2588576
    return-object v0
.end method
