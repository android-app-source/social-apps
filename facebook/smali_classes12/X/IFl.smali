.class public LX/IFl;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/IFy;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0tX;

.field private final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public g:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/IFk;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "LX/IFR;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/IFa;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2554858
    const-class v0, LX/IFl;

    sput-object v0, LX/IFl;->a:Ljava/lang/Class;

    .line 2554859
    const-string v0, "mutual_importance"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IFl;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/1Ck;Landroid/content/res/Resources;LX/0tX;Landroid/content/Context;LX/IFa;)V
    .locals 1
    .param p1    # LX/1Ck;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2554860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554861
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2554862
    iput-object v0, p0, LX/IFl;->g:LX/0Rf;

    .line 2554863
    sget-object v0, LX/IFk;->INITIAL:LX/IFk;

    iput-object v0, p0, LX/IFl;->h:LX/IFk;

    .line 2554864
    const/4 v0, 0x0

    iput-object v0, p0, LX/IFl;->i:Ljava/lang/String;

    .line 2554865
    const v0, 0x7f0b23da

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IFl;->f:Ljava/lang/String;

    .line 2554866
    iput-object p1, p0, LX/IFl;->c:LX/1Ck;

    .line 2554867
    const v0, 0x7f083859

    invoke-virtual {p4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IFl;->e:Ljava/lang/String;

    .line 2554868
    iput-object p3, p0, LX/IFl;->d:LX/0tX;

    .line 2554869
    iput-object p5, p0, LX/IFl;->l:LX/IFa;

    .line 2554870
    return-void
.end method

.method public static b(LX/IFl;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;)LX/IFd;
    .locals 13

    .prologue
    .line 2554813
    if-nez p1, :cond_0

    .line 2554814
    new-instance v0, LX/IFn;

    iget-object v1, p0, LX/IFl;->e:Ljava/lang/String;

    .line 2554815
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2554816
    invoke-direct {v0, v1, v2}, LX/IFn;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2554817
    :goto_0
    return-object v0

    .line 2554818
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    move-result-object v0

    .line 2554819
    if-nez v0, :cond_1

    .line 2554820
    new-instance v0, LX/IFn;

    iget-object v1, p0, LX/IFl;->e:Ljava/lang/String;

    .line 2554821
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2554822
    invoke-direct {v0, v1, v2}, LX/IFn;-><init>(Ljava/lang/String;LX/0Px;)V

    goto :goto_0

    .line 2554823
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel;

    .line 2554824
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2554825
    if-eqz v0, :cond_3

    .line 2554826
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_4

    .line 2554827
    iget-object v4, p0, LX/IFl;->e:Ljava/lang/String;

    .line 2554828
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_7

    .line 2554829
    :cond_2
    const/4 v5, 0x0

    .line 2554830
    :goto_2
    move v5, v5

    .line 2554831
    iget-object v6, p0, LX/IFl;->l:LX/IFa;

    invoke-static {v0, v4, v5, v6}, LX/IFZ;->a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;Ljava/lang/String;ZLX/IFa;)LX/IFZ;

    move-result-object v0

    .line 2554832
    if-eqz v0, :cond_3

    .line 2554833
    iget-object v4, p0, LX/IFl;->k:LX/0Pz;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554834
    :cond_3
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2554835
    :cond_4
    const-string v4, "friends_nearby_search_section"

    const/4 v9, 0x0

    .line 2554836
    if-nez v0, :cond_8

    .line 2554837
    :cond_5
    :goto_4
    move-object v0, v9

    .line 2554838
    if-eqz v0, :cond_3

    .line 2554839
    iget-object v4, p0, LX/IFl;->k:LX/0Pz;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2554840
    :cond_6
    iget-object v0, p0, LX/IFl;->k:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2554841
    new-instance v0, LX/IFn;

    iget-object v2, p0, LX/IFl;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v1}, LX/IFn;-><init>(Ljava/lang/String;LX/0Px;)V

    goto :goto_0

    :cond_7
    iget-object v5, p0, LX/IFl;->g:LX/0Rf;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto :goto_2

    .line 2554842
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->n()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel;

    move-result-object v7

    .line 2554843
    if-eqz v7, :cond_5

    .line 2554844
    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel;->a()LX/0Px;

    move-result-object v7

    .line 2554845
    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    .line 2554846
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel;

    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;

    move-result-object v7

    .line 2554847
    if-eqz v7, :cond_5

    .line 2554848
    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v7

    .line 2554849
    if-eqz v7, :cond_5

    .line 2554850
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->REQUESTABLE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    if-ne v7, v8, :cond_9

    .line 2554851
    sget-object v11, LX/IFQ;->NOT_INVITED:LX/IFQ;

    .line 2554852
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 2554853
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    if-nez v7, :cond_a

    .line 2554854
    :goto_6
    new-instance v7, LX/IFS;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v10

    move-object v12, v4

    invoke-direct/range {v7 .. v12}, LX/IFS;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/IFQ;Ljava/lang/String;)V

    move-object v9, v7

    goto :goto_4

    .line 2554855
    :cond_9
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    if-ne v7, v8, :cond_5

    .line 2554856
    sget-object v11, LX/IFQ;->INVITED:LX/IFQ;

    goto :goto_5

    .line 2554857
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v9

    goto :goto_6
.end method

.method public static synthetic b(LX/IFl;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554804
    const/4 v0, 0x0

    .line 2554805
    if-nez p1, :cond_1

    .line 2554806
    :cond_0
    :goto_0
    move-object v0, v0

    .line 2554807
    return-object v0

    .line 2554808
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    move-result-object p0

    .line 2554809
    if-eqz p0, :cond_0

    .line 2554810
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object p0

    .line 2554811
    if-eqz p0, :cond_0

    .line 2554812
    invoke-interface {p0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
