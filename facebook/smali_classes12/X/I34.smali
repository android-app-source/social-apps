.class public LX/I34;
.super LX/Gco;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2531035
    invoke-direct {p0}, LX/Gco;-><init>()V

    .line 2531036
    iput-object p1, p0, LX/I34;->a:LX/0tX;

    .line 2531037
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/events/model/Event;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2531038
    const v0, 0x7f0821ce

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 2531039
    iget-object v3, p2, Lcom/facebook/events/model/Event;->ao:Ljava/lang/String;

    move-object v3, v3

    .line 2531040
    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 3

    .prologue
    .line 2531023
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    const-string v1, "MOBILE_SUBSCRIPTIONS_DASHBOARD"

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    .line 2531024
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    move-result-object v0

    .line 2531025
    new-instance v1, LX/4Il;

    invoke-direct {v1}, LX/4Il;-><init>()V

    .line 2531026
    iget-object v2, p1, Lcom/facebook/events/model/Event;->an:Ljava/lang/String;

    move-object v2, v2

    .line 2531027
    invoke-virtual {v1, v2}, LX/4Il;->a(Ljava/lang/String;)LX/4Il;

    move-result-object v1

    .line 2531028
    iget-object v2, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2531029
    const-string p1, "event_id"

    invoke-virtual {v1, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531030
    move-object v1, v1

    .line 2531031
    const-string v2, "NONE"

    invoke-virtual {v1, v2}, LX/4Il;->b(Ljava/lang/String;)LX/4Il;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/4Il;->a(LX/4EL;)LX/4Il;

    move-result-object v0

    .line 2531032
    invoke-static {}, LX/7uR;->c()LX/7uB;

    move-result-object v1

    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7uB;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2531033
    iget-object v1, p0, LX/I34;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2531034
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2531022
    const/4 v0, 0x1

    return v0
.end method
