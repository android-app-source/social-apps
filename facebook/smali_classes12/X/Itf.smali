.class public LX/Itf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:LX/FDt;

.field public final b:LX/2OQ;

.field public final c:LX/2Ow;

.field private final d:LX/2Mk;

.field public final e:LX/03V;

.field public final f:LX/FKF;

.field private final g:LX/0Sh;

.field private final h:LX/2Lw;

.field private final i:LX/2Mv;


# direct methods
.method public constructor <init>(LX/FDt;LX/2OQ;LX/2Ow;LX/2Mk;LX/03V;LX/FKF;LX/0Sh;LX/2Lw;LX/2Mv;)V
    .locals 0
    .param p2    # LX/2OQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2623745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2623746
    iput-object p1, p0, LX/Itf;->a:LX/FDt;

    .line 2623747
    iput-object p2, p0, LX/Itf;->b:LX/2OQ;

    .line 2623748
    iput-object p3, p0, LX/Itf;->c:LX/2Ow;

    .line 2623749
    iput-object p4, p0, LX/Itf;->d:LX/2Mk;

    .line 2623750
    iput-object p5, p0, LX/Itf;->e:LX/03V;

    .line 2623751
    iput-object p6, p0, LX/Itf;->f:LX/FKF;

    .line 2623752
    iput-object p7, p0, LX/Itf;->g:LX/0Sh;

    .line 2623753
    iput-object p8, p0, LX/Itf;->h:LX/2Lw;

    .line 2623754
    iput-object p9, p0, LX/Itf;->i:LX/2Mv;

    .line 2623755
    return-void
.end method

.method public static a(LX/Itf;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/service/model/NewMessageResult;)V
    .locals 7

    .prologue
    .line 2623756
    iget-object v0, p2, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2623757
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623758
    const/4 v1, 0x0

    .line 2623759
    :try_start_0
    iget-object v0, p0, LX/Itf;->a:LX/FDt;

    invoke-virtual {v0, p2}, LX/FDt;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2623760
    if-eqz v0, :cond_0

    .line 2623761
    iget-object v3, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2623762
    :try_start_1
    iget-object v3, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v3, v3

    .line 2623763
    iget-object v4, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v4

    .line 2623764
    invoke-static {p0, v2, v1, v3, v0}, LX/Itf;->a(LX/Itf;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2623765
    :cond_0
    :goto_0
    return-void

    .line 2623766
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 2623767
    :goto_1
    const-string v3, "Unable to save message send to local db. message id: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    :goto_2
    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2623768
    iget-object v3, p0, LX/Itf;->e:LX/03V;

    const-string v4, "save_send_failed"

    invoke-virtual {v3, v4, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2623769
    iget-object v0, p0, LX/Itf;->c:LX/2Ow;

    invoke-virtual {v0, v2}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_0

    .line 2623770
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    goto :goto_2

    .line 2623771
    :catch_1
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_1
.end method

.method private static a(LX/Itf;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 9
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2623732
    iget-object v0, p0, LX/Itf;->b:LX/2OQ;

    .line 2623733
    const-wide/16 v6, -0x1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-object v3, v0

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v3 .. v8}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLjava/lang/Boolean;)V

    .line 2623734
    if-eqz p4, :cond_0

    .line 2623735
    iget-object v0, p0, LX/Itf;->b:LX/2OQ;

    invoke-virtual {v0, p4}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2623736
    :cond_0
    if-eqz p2, :cond_2

    .line 2623737
    sget-object v0, LX/DdH;->MESSAGE_SENT:LX/DdH;

    .line 2623738
    invoke-static {p2}, LX/2Mk;->t(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2623739
    sget-object v0, LX/DdH;->STICKER_SENT:LX/DdH;

    .line 2623740
    :cond_1
    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2Ow;->a(LX/DdH;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2623741
    iget-object v1, p0, LX/Itf;->c:LX/2Ow;

    invoke-virtual {v1, p1, v0}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/os/Bundle;)V

    .line 2623742
    iget-object v0, p0, LX/Itf;->c:LX/2Ow;

    invoke-virtual {v0, p2}, LX/2Ow;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2623743
    :goto_0
    return-void

    .line 2623744
    :cond_2
    iget-object v0, p0, LX/Itf;->c:LX/2Ow;

    invoke-virtual {v0, p1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_0
.end method

.method public static a(LX/Itf;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 2623772
    iget-object v0, p0, LX/Itf;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/messaging/send/service/PostSendMessageManager$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/messaging/send/service/PostSendMessageManager$1;-><init>(LX/Itf;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2623773
    return-void
.end method


# virtual methods
.method public final a(LX/FKG;Z)V
    .locals 6

    .prologue
    .line 2623718
    iget-object v1, p1, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    .line 2623719
    if-eqz p2, :cond_0

    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v0, v0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-boolean v0, v0, LX/6fP;->shouldNotBeRetried:Z

    if-nez v0, :cond_0

    .line 2623720
    :goto_0
    return-void

    .line 2623721
    :cond_0
    iget-object v0, p0, LX/Itf;->h:LX/2Lw;

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v3, v1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    invoke-virtual {v0, v2, v3}, LX/2Lw;->a(Ljava/lang/String;Lcom/facebook/messaging/model/send/SendError;)V

    .line 2623722
    iget-object v0, p0, LX/Itf;->i:LX/2Mv;

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2623723
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v0, v0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-boolean v0, v0, LX/6fP;->shouldNotBeRetried:Z

    if-eqz v0, :cond_2

    .line 2623724
    iget-object v0, p0, LX/Itf;->c:LX/2Ow;

    invoke-virtual {v0, v1}, LX/2Ow;->c(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2623725
    :cond_1
    :goto_1
    :try_start_0
    iget-object v0, p0, LX/Itf;->a:LX/FDt;

    invoke-virtual {v0, v1}, LX/FDt;->c(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2623726
    iget-object v0, p0, LX/Itf;->b:LX/2OQ;

    iget-object v2, p1, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v2}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2623727
    :goto_2
    iget-object v0, p0, LX/Itf;->c:LX/2Ow;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_0

    .line 2623728
    :cond_2
    iget-object v0, p0, LX/Itf;->c:LX/2Ow;

    invoke-virtual {v0, v1}, LX/2Ow;->b(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_1

    .line 2623729
    :catch_0
    move-exception v0

    .line 2623730
    const-string v2, "Unable to save message send failure to local db. message id: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2623731
    iget-object v3, p0, LX/Itf;->e:LX/03V;

    const-string v4, "save_send_failed"

    invoke-virtual {v3, v4, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 2
    .param p2    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2623710
    if-nez p2, :cond_0

    .line 2623711
    invoke-static {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object p2

    .line 2623712
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0, p1, p2, p3}, LX/Itf;->a(LX/Itf;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2623713
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2623714
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v1, 0xcb

    invoke-static {p0, v0, v1}, LX/Itf;->a(LX/Itf;Ljava/lang/String;I)V

    .line 2623715
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/messaging/service/model/NewMessageResult;)V
    .locals 1

    .prologue
    .line 2623716
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {p0, v0, p2}, LX/Itf;->a(LX/Itf;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2623717
    return-void
.end method
