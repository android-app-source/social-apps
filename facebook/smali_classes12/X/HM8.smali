.class public LX/HM8;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2456842
    const-class v0, LX/HM8;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HM8;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2456843
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456844
    const-class v0, LX/HM8;

    invoke-static {v0, p0}, LX/HM8;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2456845
    invoke-virtual {p0, v4}, LX/HM8;->setOrientation(I)V

    .line 2456846
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, LX/HM8;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0069

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2456847
    invoke-virtual {p0}, LX/HM8;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b163c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, LX/HM8;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b163c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2456848
    invoke-virtual {p0, v0}, LX/HM8;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2456849
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/HM8;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    iput-object p0, p1, LX/HM8;->a:LX/03V;

    return-void
.end method
