.class public final enum LX/Hmh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hmh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hmh;

.field public static final enum COMPATIBLE_DPI:LX/Hmh;

.field public static final enum INCOMPATIBLE_CPU:LX/Hmh;

.field public static final enum INSUFFICIENT_STORAGE:LX/Hmh;

.field public static final enum MIN_SDK_VERSION:LX/Hmh;

.field public static final enum OLDER_MAJOR_VERSION:LX/Hmh;

.field public static final enum OLDER_VERSION_CODE:LX/Hmh;


# instance fields
.field private final errorName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2500985
    new-instance v0, LX/Hmh;

    const-string v1, "INSUFFICIENT_STORAGE"

    const-string v2, "insufficient_storage"

    invoke-direct {v0, v1, v4, v2}, LX/Hmh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmh;->INSUFFICIENT_STORAGE:LX/Hmh;

    .line 2500986
    new-instance v0, LX/Hmh;

    const-string v1, "OLDER_VERSION_CODE"

    const-string v2, "older_version_code"

    invoke-direct {v0, v1, v5, v2}, LX/Hmh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmh;->OLDER_VERSION_CODE:LX/Hmh;

    .line 2500987
    new-instance v0, LX/Hmh;

    const-string v1, "OLDER_MAJOR_VERSION"

    const-string v2, "older_major_version"

    invoke-direct {v0, v1, v6, v2}, LX/Hmh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmh;->OLDER_MAJOR_VERSION:LX/Hmh;

    .line 2500988
    new-instance v0, LX/Hmh;

    const-string v1, "INCOMPATIBLE_CPU"

    const-string v2, "incompatible_cpu"

    invoke-direct {v0, v1, v7, v2}, LX/Hmh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmh;->INCOMPATIBLE_CPU:LX/Hmh;

    .line 2500989
    new-instance v0, LX/Hmh;

    const-string v1, "MIN_SDK_VERSION"

    const-string v2, "min_sdk_version"

    invoke-direct {v0, v1, v8, v2}, LX/Hmh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmh;->MIN_SDK_VERSION:LX/Hmh;

    .line 2500990
    new-instance v0, LX/Hmh;

    const-string v1, "COMPATIBLE_DPI"

    const/4 v2, 0x5

    const-string v3, "compatible_dpi"

    invoke-direct {v0, v1, v2, v3}, LX/Hmh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmh;->COMPATIBLE_DPI:LX/Hmh;

    .line 2500991
    const/4 v0, 0x6

    new-array v0, v0, [LX/Hmh;

    sget-object v1, LX/Hmh;->INSUFFICIENT_STORAGE:LX/Hmh;

    aput-object v1, v0, v4

    sget-object v1, LX/Hmh;->OLDER_VERSION_CODE:LX/Hmh;

    aput-object v1, v0, v5

    sget-object v1, LX/Hmh;->OLDER_MAJOR_VERSION:LX/Hmh;

    aput-object v1, v0, v6

    sget-object v1, LX/Hmh;->INCOMPATIBLE_CPU:LX/Hmh;

    aput-object v1, v0, v7

    sget-object v1, LX/Hmh;->MIN_SDK_VERSION:LX/Hmh;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Hmh;->COMPATIBLE_DPI:LX/Hmh;

    aput-object v2, v0, v1

    sput-object v0, LX/Hmh;->$VALUES:[LX/Hmh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2500992
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2500993
    iput-object p3, p0, LX/Hmh;->errorName:Ljava/lang/String;

    .line 2500994
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hmh;
    .locals 1

    .prologue
    .line 2500995
    const-class v0, LX/Hmh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hmh;

    return-object v0
.end method

.method public static values()[LX/Hmh;
    .locals 1

    .prologue
    .line 2500996
    sget-object v0, LX/Hmh;->$VALUES:[LX/Hmh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hmh;

    return-object v0
.end method
