.class public LX/Hv7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesHasUserCancelledAlbumCreationFlow;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsTargetAlbumNew;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0jD;",
        "PluginData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginSurveyConstraintGetter;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0in",
        "<TPluginData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7l0;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final d:Z

.field public final e:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0il;ZZ)V
    .locals 2
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7l0;",
            ">;TServices;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2518338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518339
    iput-object p1, p0, LX/Hv7;->b:LX/0Ot;

    .line 2518340
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hv7;->c:Ljava/lang/ref/WeakReference;

    .line 2518341
    iput-boolean p3, p0, LX/Hv7;->d:Z

    .line 2518342
    iput-boolean p4, p0, LX/Hv7;->e:Z

    .line 2518343
    return-void
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2518291
    iget-object v0, p0, LX/Hv7;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/0il;

    .line 2518292
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    move-object v0, v6

    .line 2518293
    check-cast v0, LX/0in;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    .line 2518294
    iget-object v1, v0, LX/AQ9;->h:LX/AQ4;

    move-object v0, v1

    .line 2518295
    if-eqz v0, :cond_0

    move-object v0, v6

    check-cast v0, LX/0in;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    .line 2518296
    iget-object v1, v0, LX/AQ9;->h:LX/AQ4;

    move-object v0, v1

    .line 2518297
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, v6

    .line 2518298
    check-cast v0, LX/0in;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    .line 2518299
    iget-object v1, v0, LX/AQ9;->h:LX/AQ4;

    move-object v0, v1

    .line 2518300
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v8, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2518301
    :cond_0
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2518302
    const-string v0, "photo_added"

    const-string v1, "1"

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518303
    :cond_1
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v7

    move v1, v7

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2518304
    invoke-static {v0}, LX/7kq;->b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2518305
    add-int/lit8 v0, v1, 0x1

    .line 2518306
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2518307
    :cond_2
    const-string v0, "photo_count"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518308
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2518309
    const-string v0, "video_added"

    const-string v1, "1"

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518310
    :cond_3
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v7

    move v1, v7

    :goto_2
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2518311
    invoke-static {v0}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2518312
    add-int/lit8 v0, v1, 0x1

    .line 2518313
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 2518314
    :cond_4
    const-string v0, "video_count"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518315
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v9, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2518316
    iget-object v0, p0, LX/Hv7;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2518317
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2518318
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, LX/Hv7;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7l0;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0jD;

    invoke-interface {v3}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v3

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j2;

    invoke-interface {v4}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    invoke-interface {v5}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/7ky;->a(JLX/7l0;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    .line 2518319
    :goto_4
    if-eqz v0, :cond_5

    .line 2518320
    const-string v1, "has_tagged_users"

    const-string v2, "1"

    invoke-interface {v8, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518321
    :cond_5
    if-eqz v9, :cond_6

    .line 2518322
    iget-object v1, v9, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v1

    .line 2518323
    if-eqz v1, :cond_6

    .line 2518324
    iget-object v1, v9, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v1

    .line 2518325
    invoke-static {v1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v1, v2, :cond_6

    if-eqz v0, :cond_6

    .line 2518326
    const-string v0, "privacy_friends_and_tagged"

    const-string v1, "1"

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518327
    :cond_6
    const-string v1, "session_id"

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518328
    iget-boolean v0, p0, LX/Hv7;->d:Z

    if-eqz v0, :cond_7

    .line 2518329
    const-string v0, "is_saving_draft"

    const-string v1, "1"

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518330
    iget-boolean v0, p0, LX/Hv7;->e:Z

    if-eqz v0, :cond_7

    .line 2518331
    const-string v0, "is_draft_resave"

    const-string v1, "1"

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518332
    :cond_7
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2518333
    const-string v0, "album_creation_cancelled"

    const-string v1, "1"

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518334
    :cond_8
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isTargetAlbumNew()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2518335
    const-string v0, "album_creation_new"

    const-string v1, "1"

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518336
    :cond_9
    invoke-static {v8}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0

    :cond_a
    move v0, v7

    .line 2518337
    goto :goto_4

    :cond_b
    move v0, v1

    goto/16 :goto_3

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method
