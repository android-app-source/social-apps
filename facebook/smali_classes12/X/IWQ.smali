.class public LX/IWQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Integer;

.field public static final b:Ljava/lang/Integer;


# instance fields
.field public final c:LX/0tX;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/1Ck;

.field public final f:Landroid/content/res/Resources;

.field public final g:Ljava/lang/String;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

.field public j:LX/IWG;

.field public k:Ljava/lang/String;

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2583849
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/IWQ;->a:Ljava/lang/Integer;

    .line 2583850
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/IWQ;->b:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1Ck;Landroid/content/res/Resources;Ljava/lang/String;LX/IWG;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/IWG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2583852
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, LX/IWQ;->i:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 2583853
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IWQ;->l:Z

    .line 2583854
    iput-object p1, p0, LX/IWQ;->c:LX/0tX;

    .line 2583855
    iput-object p2, p0, LX/IWQ;->d:Ljava/util/concurrent/ExecutorService;

    .line 2583856
    iput-object p3, p0, LX/IWQ;->e:LX/1Ck;

    .line 2583857
    iput-object p4, p0, LX/IWQ;->f:Landroid/content/res/Resources;

    .line 2583858
    iput-object p5, p0, LX/IWQ;->g:Ljava/lang/String;

    .line 2583859
    iput-object p6, p0, LX/IWQ;->j:LX/IWG;

    .line 2583860
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2583861
    iget-boolean v0, p0, LX/IWQ;->l:Z

    if-nez v0, :cond_0

    .line 2583862
    iget-object v0, p0, LX/IWQ;->e:LX/1Ck;

    sget-object v1, LX/IWP;->FETCH_GROUP_ALBUMS:LX/IWP;

    new-instance v2, LX/IWN;

    invoke-direct {v2, p0}, LX/IWN;-><init>(LX/IWQ;)V

    new-instance v3, LX/IWO;

    invoke-direct {v3, p0}, LX/IWO;-><init>(LX/IWQ;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2583863
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2583864
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IWQ;->l:Z

    .line 2583865
    iput-object v1, p0, LX/IWQ;->k:Ljava/lang/String;

    .line 2583866
    iput-object v1, p0, LX/IWQ;->h:LX/0Px;

    .line 2583867
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, LX/IWQ;->i:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 2583868
    return-void
.end method
