.class public LX/Hdg;
.super LX/Hcr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Hcr",
        "<",
        "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field public c:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2488908
    invoke-direct {p0}, LX/Hcr;-><init>()V

    .line 2488909
    iput-object p1, p0, LX/Hdg;->a:LX/0Ot;

    .line 2488910
    iput-object p2, p0, LX/Hdg;->b:Landroid/content/Context;

    .line 2488911
    return-void
.end method

.method public static synthetic a(LX/Hdg;)V
    .locals 0

    .prologue
    .line 2488903
    invoke-virtual {p0}, LX/Hdg;->setChanged()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ")",
            "Landroid/util/Pair",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;",
            ">;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 2488895
    new-instance v0, LX/HdP;

    invoke-direct {v0}, LX/HdP;-><init>()V

    move-object v0, v0

    .line 2488896
    const-string v1, "topic"

    invoke-virtual {p1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2488897
    iget-object v1, p0, LX/Hdg;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 2488898
    const-string v2, "scale"

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2488899
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2488900
    new-instance v1, LX/Hdf;

    invoke-direct {v1, p0}, LX/Hdf;-><init>(LX/Hdg;)V

    .line 2488901
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2488904
    iget-object v1, p0, LX/Hdg;->c:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    if-nez v1, :cond_1

    .line 2488905
    :cond_0
    :goto_0
    return v0

    .line 2488906
    :cond_1
    iget-object v1, p0, LX/Hdg;->c:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    invoke-virtual {v1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;->a()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;

    move-result-object v1

    .line 2488907
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->a()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1

    .prologue
    .line 2488902
    iget-object v0, p0, LX/Hdg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    return-object v0
.end method

.method public final b(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)V
    .locals 0

    .prologue
    .line 2488894
    return-void
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2488893
    iget-object v0, p0, LX/Hdg;->c:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    return-object v0
.end method
