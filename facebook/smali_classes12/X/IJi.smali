.class public LX/IJi;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:LX/0wJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2564153
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2564154
    const v0, 0x7f030727

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2564155
    const v0, 0x7f0a0121

    invoke-virtual {p0, v0}, LX/IJi;->setBackgroundResource(I)V

    .line 2564156
    const v0, 0x7f0d131d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/IJi;->a:Landroid/widget/TextView;

    .line 2564157
    new-instance v0, LX/0wJ;

    invoke-virtual {p0}, LX/IJi;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-direct {v0, p1}, LX/0wJ;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/IJi;->b:LX/0wJ;

    .line 2564158
    return-void
.end method


# virtual methods
.method public setTitleText(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2564159
    iget-object v0, p0, LX/IJi;->a:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2564160
    iget-object v0, p0, LX/IJi;->a:Landroid/widget/TextView;

    iget-object v1, p0, LX/IJi;->b:LX/0wJ;

    invoke-virtual {v1, p1, v2}, LX/0wJ;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2564161
    return-void
.end method
