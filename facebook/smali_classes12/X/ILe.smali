.class public final LX/ILe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;)V
    .locals 0

    .prologue
    .line 2568212
    iput-object p1, p0, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x1aecd61d

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2568213
    iget-object v1, p0, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->c:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2568214
    new-instance v2, LX/4At;

    iget-object v3, p0, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f083024

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2568215
    invoke-virtual {v2}, LX/4At;->beginShowingProgress()V

    .line 2568216
    iget-object v3, p0, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    iget-object v3, v3, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->a:LX/IMk;

    iget-object v4, p0, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    iget-object v4, v4, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->c:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/ILd;

    invoke-direct {v5, p0, v2, v1}, LX/ILd;-><init>(LX/ILe;LX/4At;Ljava/lang/String;)V

    .line 2568217
    new-instance v1, LX/4Fl;

    invoke-direct {v1}, LX/4Fl;-><init>()V

    .line 2568218
    const-string v2, "confirmation_code"

    invoke-virtual {v1, v2, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2568219
    move-object v1, v1

    .line 2568220
    new-instance v2, LX/ILr;

    invoke-direct {v2}, LX/ILr;-><init>()V

    move-object v2, v2

    .line 2568221
    const-string p0, "input"

    invoke-virtual {v2, p0, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/ILr;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2568222
    iget-object v2, v3, LX/IMk;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {v1}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2568223
    iget-object v2, v3, LX/IMk;->a:LX/1Ck;

    sget-object p0, LX/IMj;->TASK_JOIN_GROUP_WITH_EMAIL:LX/IMj;

    new-instance p1, LX/IMi;

    invoke-direct {p1, v3, v5}, LX/IMi;-><init>(LX/IMk;LX/ILc;)V

    invoke-virtual {v2, p0, v1, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2568224
    const v1, 0x2c8af8b0

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
