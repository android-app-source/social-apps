.class public LX/JDK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/175;

.field public final c:LX/174;

.field public final d:LX/1Fb;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public final h:LX/JA0;

.field public i:Z

.field public j:Z

.field public k:LX/5vL;

.field public l:LX/JDJ;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/175;LX/174;LX/1Fb;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/JA0;ZZ)V
    .locals 1

    .prologue
    .line 2663693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2663694
    sget-object v0, LX/JDJ;->REQUEST_NONE:LX/JDJ;

    iput-object v0, p0, LX/JDK;->l:LX/JDJ;

    .line 2663695
    iput-object p1, p0, LX/JDK;->a:Ljava/lang/String;

    .line 2663696
    iput-object p2, p0, LX/JDK;->b:LX/175;

    .line 2663697
    iput-object p3, p0, LX/JDK;->c:LX/174;

    .line 2663698
    iput-object p4, p0, LX/JDK;->d:LX/1Fb;

    .line 2663699
    iput-object p5, p0, LX/JDK;->e:Ljava/lang/String;

    .line 2663700
    iput-object p6, p0, LX/JDK;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2663701
    iput-object p7, p0, LX/JDK;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2663702
    iput-object p8, p0, LX/JDK;->h:LX/JA0;

    .line 2663703
    iput-boolean p9, p0, LX/JDK;->i:Z

    .line 2663704
    iput-boolean p10, p0, LX/JDK;->j:Z

    .line 2663705
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/175;
    .locals 1

    .prologue
    .line 2663669
    new-instance v0, LX/4af;

    invoke-direct {v0}, LX/4af;-><init>()V

    .line 2663670
    iput-object p0, v0, LX/4af;->b:Ljava/lang/String;

    .line 2663671
    move-object v0, v0

    .line 2663672
    invoke-virtual {v0}, LX/4af;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/JDK;
    .locals 11
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "newCollectionItemInstance"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 2663673
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2663674
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->mS_()Ljava/lang/String;

    move-result-object v1

    .line 2663675
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 2663676
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v8, v0

    .line 2663677
    :goto_1
    new-instance v0, LX/JDK;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->e()LX/1Fb;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->l()Ljava/lang/String;

    move-result-object v5

    move-object v6, p1

    move v10, v9

    invoke-direct/range {v0 .. v10}, LX/JDK;-><init>(Ljava/lang/String;LX/175;LX/174;LX/1Fb;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/JA0;ZZ)V

    return-object v0

    .line 2663678
    :cond_0
    new-instance v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v7, v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    move-object v1, v0

    goto :goto_0

    .line 2663679
    :cond_1
    new-instance v8, LX/JA0;

    invoke-direct {v8, p0}, LX/JA0;-><init>(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/JDK;
    .locals 14

    .prologue
    .line 2663680
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2663681
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2663682
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2663683
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->mR_()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/JDK;->a(Ljava/lang/String;)LX/175;

    move-result-object v5

    .line 2663684
    const/4 v6, 0x0

    .line 2663685
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x25d6af

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->e()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2663686
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->e()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, LX/JDK;->a(Ljava/lang/String;)LX/175;

    move-result-object v6

    .line 2663687
    :cond_0
    :goto_0
    new-instance v3, LX/JDK;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->j()LX/1Fb;

    move-result-object v7

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->k()Ljava/lang/String;

    move-result-object v8

    new-instance v11, LX/JA0;

    invoke-direct {v11, p0}, LX/JA0;-><init>(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;)V

    move-object v9, p1

    move-object v10, v0

    move v12, v1

    move v13, v2

    invoke-direct/range {v3 .. v13}, LX/JDK;-><init>(Ljava/lang/String;LX/175;LX/174;LX/1Fb;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/JA0;ZZ)V

    .line 2663688
    move-object v0, v3

    .line 2663689
    return-object v0

    .line 2663690
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, -0x4dba1a9d

    if-ne v3, v4, :cond_0

    .line 2663691
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->mQ_()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/JDK;->a(Ljava/lang/String;)LX/175;

    move-result-object v6

    goto :goto_0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2663692
    const-class v0, LX/JDK;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, LX/JDK;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, LX/JDK;->b:LX/175;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
