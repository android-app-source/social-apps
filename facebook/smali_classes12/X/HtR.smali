.class public LX/HtR;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/HtP;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516183
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2516184
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/HqO;)LX/HtP;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0ip;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(TServices;",
            "Lcom/facebook/composer/feedattachment/minutiae/MinutiaeAttachment$Callback;",
            ")",
            "LX/HtP",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2516185
    new-instance v0, LX/HtP;

    move-object v1, p1

    check-cast v1, LX/0il;

    .line 2516186
    new-instance v2, LX/HtQ;

    const/16 v3, 0x1985

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1983

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/HtQ;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2516187
    move-object v3, v2

    .line 2516188
    check-cast v3, LX/HtQ;

    invoke-static {p0}, LX/IC3;->a(LX/0QB;)LX/IC3;

    move-result-object v4

    check-cast v4, LX/IC3;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, LX/HtP;-><init>(LX/0il;LX/HqO;LX/HtQ;LX/IC3;LX/0Uh;Ljava/util/concurrent/Executor;)V

    .line 2516189
    return-object v0
.end method
