.class public final LX/JSr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:LX/JSs;


# direct methods
.method public constructor <init>(LX/JSs;)V
    .locals 0

    .prologue
    .line 2696099
    iput-object p1, p0, LX/JSr;->a:LX/JSs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2696100
    iget-object v0, p0, LX/JSr;->a:LX/JSs;

    iget-object v0, v0, LX/JSs;->c:LX/JSP;

    invoke-interface {v0}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->a()V

    .line 2696101
    iget-object v0, p0, LX/JSr;->a:LX/JSs;

    iget-object v0, v0, LX/JSs;->c:LX/JSP;

    invoke-interface {v0}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->setVisibility(I)V

    .line 2696102
    iget-object v0, p0, LX/JSr;->a:LX/JSs;

    iget-object v0, v0, LX/JSs;->c:LX/JSP;

    invoke-interface {v0}, LX/JSP;->getCoverArt()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2696103
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2696104
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2696105
    return-void
.end method
