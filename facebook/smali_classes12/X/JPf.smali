.class public final LX/JPf;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JPg;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/JPJ;

.field public final synthetic c:LX/JPg;


# direct methods
.method public constructor <init>(LX/JPg;)V
    .locals 1

    .prologue
    .line 2690219
    iput-object p1, p0, LX/JPf;->c:LX/JPg;

    .line 2690220
    move-object v0, p1

    .line 2690221
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2690222
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2690223
    const-string v0, "UnitFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2690205
    if-ne p0, p1, :cond_1

    .line 2690206
    :cond_0
    :goto_0
    return v0

    .line 2690207
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2690208
    goto :goto_0

    .line 2690209
    :cond_3
    check-cast p1, LX/JPf;

    .line 2690210
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2690211
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2690212
    if-eq v2, v3, :cond_0

    .line 2690213
    iget-object v2, p0, LX/JPf;->a:LX/1Po;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JPf;->a:LX/1Po;

    iget-object v3, p1, LX/JPf;->a:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2690214
    goto :goto_0

    .line 2690215
    :cond_5
    iget-object v2, p1, LX/JPf;->a:LX/1Po;

    if-nez v2, :cond_4

    .line 2690216
    :cond_6
    iget-object v2, p0, LX/JPf;->b:LX/JPJ;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JPf;->b:LX/JPJ;

    iget-object v3, p1, LX/JPf;->b:LX/JPJ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2690217
    goto :goto_0

    .line 2690218
    :cond_7
    iget-object v2, p1, LX/JPf;->b:LX/JPJ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
