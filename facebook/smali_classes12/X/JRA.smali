.class public LX/JRA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/JQo;


# direct methods
.method public constructor <init>(LX/JQo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692854
    iput-object p1, p0, LX/JRA;->a:LX/JQo;

    .line 2692855
    return-void
.end method

.method public static a(LX/0QB;)LX/JRA;
    .locals 4

    .prologue
    .line 2692856
    const-class v1, LX/JRA;

    monitor-enter v1

    .line 2692857
    :try_start_0
    sget-object v0, LX/JRA;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692858
    sput-object v2, LX/JRA;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692859
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692860
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692861
    new-instance p0, LX/JRA;

    invoke-static {v0}, LX/JQo;->a(LX/0QB;)LX/JQo;

    move-result-object v3

    check-cast v3, LX/JQo;

    invoke-direct {p0, v3}, LX/JRA;-><init>(LX/JQo;)V

    .line 2692862
    move-object v0, p0

    .line 2692863
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692864
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692865
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692866
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
