.class public LX/I7i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/support/v4/app/Fragment;

.field public final b:LX/IBH;

.field public final c:LX/1Kf;

.field public final d:LX/1ay;

.field public final e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field public final f:LX/IBL;

.field public final g:LX/3RX;

.field public final h:LX/1Ck;

.field public final i:LX/Bl6;

.field public final j:Lcom/facebook/content/SecureContextHelper;

.field public k:Lcom/facebook/events/model/Event;


# direct methods
.method public constructor <init>(LX/IBH;LX/1Kf;LX/1ay;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/IBL;LX/3RX;LX/1Ck;LX/Bl6;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2540083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540084
    iput-object p1, p0, LX/I7i;->b:LX/IBH;

    .line 2540085
    iput-object p2, p0, LX/I7i;->c:LX/1Kf;

    .line 2540086
    iput-object p3, p0, LX/I7i;->d:LX/1ay;

    .line 2540087
    iput-object p4, p0, LX/I7i;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2540088
    iput-object p5, p0, LX/I7i;->f:LX/IBL;

    .line 2540089
    iput-object p6, p0, LX/I7i;->g:LX/3RX;

    .line 2540090
    iput-object p7, p0, LX/I7i;->h:LX/1Ck;

    .line 2540091
    iput-object p8, p0, LX/I7i;->i:LX/Bl6;

    .line 2540092
    iput-object p9, p0, LX/I7i;->j:Lcom/facebook/content/SecureContextHelper;

    .line 2540093
    return-void
.end method

.method public static a(LX/0QB;)LX/I7i;
    .locals 11

    .prologue
    .line 2540094
    new-instance v1, LX/I7i;

    invoke-static {p0}, LX/IBH;->b(LX/0QB;)LX/IBH;

    move-result-object v2

    check-cast v2, LX/IBH;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v3

    check-cast v3, LX/1Kf;

    invoke-static {p0}, LX/1ay;->b(LX/0QB;)LX/1ay;

    move-result-object v4

    check-cast v4, LX/1ay;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p0}, LX/IBL;->a(LX/0QB;)LX/IBL;

    move-result-object v6

    check-cast v6, LX/IBL;

    invoke-static {p0}, LX/8iw;->b(LX/0QB;)LX/8iw;

    move-result-object v7

    check-cast v7, LX/3RX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {p0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v9

    check-cast v9, LX/Bl6;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v1 .. v10}, LX/I7i;-><init>(LX/IBH;LX/1Kf;LX/1ay;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/IBL;LX/3RX;LX/1Ck;LX/Bl6;Lcom/facebook/content/SecureContextHelper;)V

    .line 2540095
    move-object v0, v1

    .line 2540096
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2540097
    iget-object v0, p0, LX/I7i;->k:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_1

    .line 2540098
    :cond_0
    :goto_0
    return-void

    .line 2540099
    :cond_1
    iget-object v0, p0, LX/I7i;->f:LX/IBL;

    iget-object v1, p0, LX/I7i;->k:Lcom/facebook/events/model/Event;

    new-instance v2, LX/I7e;

    invoke-direct {v2, p0}, LX/I7e;-><init>(LX/I7i;)V

    invoke-virtual {v0, v1, v2}, LX/IBL;->a(Lcom/facebook/events/model/Event;LX/0QK;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2540100
    iget-object v0, p0, LX/I7i;->k:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_1

    .line 2540101
    :cond_0
    :goto_0
    return-void

    .line 2540102
    :cond_1
    iget-object v0, p0, LX/I7i;->f:LX/IBL;

    iget-object v1, p0, LX/I7i;->k:Lcom/facebook/events/model/Event;

    new-instance v2, LX/I7g;

    invoke-direct {v2, p0}, LX/I7g;-><init>(LX/I7i;)V

    invoke-virtual {v0, v1, v2}, LX/IBL;->a(Lcom/facebook/events/model/Event;LX/0QK;)V

    goto :goto_0
.end method
