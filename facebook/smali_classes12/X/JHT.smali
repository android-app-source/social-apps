.class public LX/JHT;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/JHT;",
        ">;"
    }
.end annotation


# static fields
.field private static a:J


# instance fields
.field private final b:D

.field private final c:D

.field private final d:D

.field private final e:D

.field private final f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2672936
    const-wide/high16 v0, -0x8000000000000000L

    sput-wide v0, LX/JHT;->a:J

    return-void
.end method

.method public constructor <init>(IDDDD)V
    .locals 4

    .prologue
    .line 2672929
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2672930
    iput-wide p2, p0, LX/JHT;->b:D

    .line 2672931
    iput-wide p4, p0, LX/JHT;->c:D

    .line 2672932
    iput-wide p6, p0, LX/JHT;->d:D

    .line 2672933
    iput-wide p8, p0, LX/JHT;->e:D

    .line 2672934
    sget-wide v0, LX/JHT;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    sput-wide v2, LX/JHT;->a:J

    iput-wide v0, p0, LX/JHT;->f:J

    .line 2672935
    return-void
.end method

.method private a(LX/JHT;)LX/JHT;
    .locals 6

    .prologue
    .line 2672920
    iget-wide v4, p0, LX/5r0;->d:J

    move-wide v0, v4

    .line 2672921
    iget-wide v4, p1, LX/5r0;->d:J

    move-wide v2, v4

    .line 2672922
    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 2672923
    iget-wide v0, p0, LX/JHT;->f:J

    iget-wide v2, p1, LX/JHT;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2672924
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    move-object p0, p1

    .line 2672925
    goto :goto_0

    .line 2672926
    :cond_2
    iget-wide v4, p0, LX/5r0;->d:J

    move-wide v0, v4

    .line 2672927
    iget-wide v4, p1, LX/5r0;->d:J

    move-wide v2, v4

    .line 2672928
    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    move-object p0, p1

    goto :goto_0
.end method

.method private j()LX/5pH;
    .locals 4

    .prologue
    .line 2672937
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2672938
    const-string v1, "latitude"

    iget-wide v2, p0, LX/JHT;->b:D

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2672939
    const-string v1, "longitude"

    iget-wide v2, p0, LX/JHT;->c:D

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2672940
    const-string v1, "latitudeDelta"

    iget-wide v2, p0, LX/JHT;->d:D

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2672941
    const-string v1, "longitudeDelta"

    iget-wide v2, p0, LX/JHT;->e:D

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2672942
    return-object v0
.end method

.method private k()LX/5pH;
    .locals 3

    .prologue
    .line 2672914
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2672915
    const-string v1, "region"

    invoke-direct {p0}, LX/JHT;->j()LX/5pH;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2672916
    const-string v1, "target"

    .line 2672917
    iget v2, p0, LX/5r0;->c:I

    move v2, v2

    .line 2672918
    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2672919
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/5r0;)LX/5r0;
    .locals 1

    .prologue
    .line 2672913
    check-cast p1, LX/JHT;

    invoke-direct {p0, p1}, LX/JHT;->a(LX/JHT;)LX/JHT;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2672910
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2672911
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/JHT;->k()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2672912
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2672909
    const-string v0, "topChange"

    return-object v0
.end method
