.class public LX/InT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/6lN;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6lN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610688
    iput-object p1, p0, LX/InT;->a:Landroid/content/Context;

    .line 2610689
    iput-object p2, p0, LX/InT;->b:LX/6lN;

    .line 2610690
    return-void
.end method

.method public static b(LX/0QB;)LX/InT;
    .locals 3

    .prologue
    .line 2610685
    new-instance v2, LX/InT;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/6lN;->a(LX/0QB;)LX/6lN;

    move-result-object v1

    check-cast v1, LX/6lN;

    invoke-direct {v2, v0, v1}, LX/InT;-><init>(Landroid/content/Context;LX/6lN;)V

    .line 2610686
    return-object v2
.end method
