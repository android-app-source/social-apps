.class public final LX/J7d;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/J7f;


# direct methods
.method public constructor <init>(LX/J7f;LX/0TF;)V
    .locals 0

    .prologue
    .line 2651084
    iput-object p1, p0, LX/J7d;->b:LX/J7f;

    iput-object p2, p0, LX/J7d;->a:LX/0TF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2651085
    iget-object v0, p0, LX/J7d;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2651086
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2651087
    check-cast p1, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel;

    .line 2651088
    if-nez p1, :cond_0

    .line 2651089
    :goto_0
    return-void

    .line 2651090
    :cond_0
    iget-object v0, p0, LX/J7d;->a:LX/0TF;

    invoke-virtual {p1}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
