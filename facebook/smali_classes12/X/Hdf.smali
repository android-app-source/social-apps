.class public final LX/Hdf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hdg;


# direct methods
.method public constructor <init>(LX/Hdg;)V
    .locals 0

    .prologue
    .line 2488880
    iput-object p1, p0, LX/Hdf;->a:LX/Hdg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2488881
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2488882
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2488883
    if-eqz p1, :cond_0

    .line 2488884
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2488885
    if-eqz v0, :cond_0

    .line 2488886
    iget-object v1, p0, LX/Hdf;->a:LX/Hdg;

    .line 2488887
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2488888
    check-cast v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    .line 2488889
    iput-object v0, v1, LX/Hdg;->c:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    .line 2488890
    iget-object v0, p0, LX/Hdf;->a:LX/Hdg;

    invoke-static {v0}, LX/Hdg;->a(LX/Hdg;)V

    .line 2488891
    iget-object v0, p0, LX/Hdf;->a:LX/Hdg;

    invoke-virtual {v0}, LX/Hdg;->notifyObservers()V

    .line 2488892
    :cond_0
    return-void
.end method
