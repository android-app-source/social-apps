.class public final LX/I1r;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V
    .locals 0

    .prologue
    .line 2529417
    iput-object p1, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2529418
    iget-object v0, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->m:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2529419
    iget-object v0, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2529420
    :cond_0
    iget-object v0, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->a$redex0(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529421
    iget-object v0, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->k:LX/I1o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I1o;->b(Z)V

    .line 2529422
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2529423
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529424
    iget-object v0, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->m:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2529425
    iget-object v0, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2529426
    :cond_0
    iget-object v0, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    invoke-static {v0, p1}, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->a$redex0(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529427
    iget-object v0, p0, LX/I1r;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->k:LX/I1o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I1o;->b(Z)V

    .line 2529428
    return-void
.end method
