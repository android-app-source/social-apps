.class public final LX/Hae;
.super LX/1OX;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public final synthetic c:LX/Hag;

.field public d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Hag;)V
    .locals 1

    .prologue
    .line 2484174
    iput-object p1, p0, LX/Hae;->c:LX/Hag;

    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 2484175
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Hae;->d:LX/0am;

    return-void
.end method

.method private c()V
    .locals 12

    .prologue
    const/16 v11, 0xc8

    const/4 v10, -0x1

    const/16 v9, 0x64

    const/4 v8, 0x1

    .line 2484176
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    iget-object v0, v0, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    .line 2484177
    iget-object v1, p0, LX/Hae;->c:LX/Hag;

    iget-object v1, v1, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v1, v0}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v1

    .line 2484178
    if-nez v1, :cond_1

    .line 2484179
    :cond_0
    :goto_0
    return-void

    .line 2484180
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 2484181
    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    iget-object v2, v2, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v2}, LX/1OM;->ij_()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_6

    .line 2484182
    iget v2, p0, LX/Hae;->b:I

    if-lt v2, v0, :cond_2

    iget v2, p0, LX/Hae;->b:I

    if-ne v2, v0, :cond_4

    iget v2, p0, LX/Hae;->a:I

    if-le v2, v1, :cond_4

    .line 2484183
    :cond_2
    int-to-double v2, v1

    iget-object v4, p0, LX/Hae;->c:LX/Hag;

    iget-object v4, v4, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v4, v4

    const-wide v6, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v4, v6

    cmpg-double v2, v2, v4

    if-gez v2, :cond_8

    .line 2484184
    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    iget-object v2, v2, LX/Hag;->e:LX/Hac;

    sget-object v3, LX/Hac;->SCROLLING:LX/Hac;

    if-eq v2, v3, :cond_3

    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    iget v2, v2, LX/Hag;->i:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_7

    .line 2484185
    :cond_3
    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    .line 2484186
    iget-object v3, v2, LX/Hag;->c:LX/Hbd;

    .line 2484187
    iput v10, v3, LX/Hbd;->t:I

    .line 2484188
    iget-object v3, v2, LX/Hag;->c:LX/Hbd;

    .line 2484189
    iput v9, v3, LX/Hbd;->u:I

    .line 2484190
    iget-object v3, v2, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2484191
    :cond_4
    :goto_1
    iget v2, p0, LX/Hae;->b:I

    if-gt v2, v0, :cond_5

    iget v2, p0, LX/Hae;->b:I

    if-ne v2, v0, :cond_6

    iget v2, p0, LX/Hae;->a:I

    if-ge v2, v1, :cond_6

    .line 2484192
    :cond_5
    int-to-double v2, v1

    iget-object v4, p0, LX/Hae;->c:LX/Hag;

    iget-object v4, v4, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v4, v4

    const-wide v6, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v4, v6

    cmpl-double v2, v2, v4

    if-lez v2, :cond_9

    .line 2484193
    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    add-int/lit8 v3, v0, -0x2

    .line 2484194
    iget-object v4, v2, LX/Hag;->c:LX/Hbd;

    .line 2484195
    iput v8, v4, LX/Hbd;->t:I

    .line 2484196
    iget-object v4, v2, LX/Hag;->c:LX/Hbd;

    .line 2484197
    iput v9, v4, LX/Hbd;->u:I

    .line 2484198
    iget-object v4, v2, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2484199
    :cond_6
    :goto_2
    iput v0, p0, LX/Hae;->b:I

    .line 2484200
    iput v1, p0, LX/Hae;->a:I

    .line 2484201
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    iget-object v0, v0, LX/Hag;->e:LX/Hac;

    sget-object v1, LX/Hac;->COLLAPSE_COMPLETED:LX/Hac;

    if-ne v0, v1, :cond_0

    .line 2484202
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    sget-object v1, LX/Hac;->SCROLLING:LX/Hac;

    .line 2484203
    iput-object v1, v0, LX/Hag;->e:LX/Hac;

    .line 2484204
    goto/16 :goto_0

    .line 2484205
    :cond_7
    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    add-int/lit8 v3, v0, -0x1

    .line 2484206
    iget-object v4, v2, LX/Hag;->c:LX/Hbd;

    .line 2484207
    iput v8, v4, LX/Hbd;->t:I

    .line 2484208
    iget-object v4, v2, LX/Hag;->c:LX/Hbd;

    .line 2484209
    iput v9, v4, LX/Hbd;->u:I

    .line 2484210
    iget-object v4, v2, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2484211
    goto :goto_1

    .line 2484212
    :cond_8
    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    add-int/lit8 v3, v0, -0x1

    .line 2484213
    iget-object v4, v2, LX/Hag;->c:LX/Hbd;

    .line 2484214
    iput v8, v4, LX/Hbd;->t:I

    .line 2484215
    iget-object v4, v2, LX/Hag;->c:LX/Hbd;

    .line 2484216
    iput v11, v4, LX/Hbd;->u:I

    .line 2484217
    iget-object v4, v2, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2484218
    goto :goto_1

    .line 2484219
    :cond_9
    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    iget-object v3, p0, LX/Hae;->c:LX/Hag;

    iget-object v3, v3, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 2484220
    iget-object v4, v2, LX/Hag;->c:LX/Hbd;

    .line 2484221
    iput v10, v4, LX/Hbd;->t:I

    .line 2484222
    iget-object v4, v2, LX/Hag;->c:LX/Hbd;

    .line 2484223
    iput v11, v4, LX/Hbd;->u:I

    .line 2484224
    iget-object v4, v2, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2484225
    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 3

    .prologue
    .line 2484226
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    iget-object v0, v0, LX/Hag;->e:LX/Hac;

    sget-object v1, LX/Hac;->EXPAND_STARTED:LX/Hac;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    iget-object v0, v0, LX/Hag;->e:LX/Hac;

    sget-object v1, LX/Hac;->COLLAPSE_STARTED:LX/Hac;

    if-ne v0, v1, :cond_1

    .line 2484227
    :cond_0
    :goto_0
    return-void

    .line 2484228
    :cond_1
    if-nez p2, :cond_2

    .line 2484229
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    iget-object v0, v0, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v0}, LX/1P1;->m()I

    move-result v1

    .line 2484230
    if-gez v1, :cond_4

    .line 2484231
    :cond_2
    :goto_1
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    iget-object v0, v0, LX/Hag;->e:LX/Hac;

    sget-object v1, LX/Hac;->EXPAND_COMPLETED:LX/Hac;

    if-ne v0, v1, :cond_3

    if-nez p2, :cond_3

    .line 2484232
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    const/4 v1, -0x1

    const/16 v2, 0x64

    iget-object p1, p0, LX/Hae;->c:LX/Hag;

    iget p1, p1, LX/Hag;->h:I

    .line 2484233
    iget-object p0, v0, LX/Hag;->c:LX/Hbd;

    .line 2484234
    iput v1, p0, LX/Hbd;->t:I

    .line 2484235
    iget-object p0, v0, LX/Hag;->c:LX/Hbd;

    .line 2484236
    iput v2, p0, LX/Hbd;->u:I

    .line 2484237
    iget-object p0, v0, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2484238
    goto :goto_0

    .line 2484239
    :cond_3
    invoke-direct {p0}, LX/Hae;->c()V

    goto :goto_0

    .line 2484240
    :cond_4
    iget-object v0, p0, LX/Hae;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/Hae;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2484241
    if-eqz v0, :cond_5

    .line 2484242
    iget-object v2, p0, LX/Hae;->c:LX/Hag;

    iget-object v0, p0, LX/Hae;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v2, v0}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v0

    .line 2484243
    if-eqz v0, :cond_5

    .line 2484244
    check-cast v0, LX/HbY;

    .line 2484245
    iget-object v2, v0, LX/HbY;->C:LX/HaY;

    .line 2484246
    iget-object p1, v2, LX/HaY;->d:LX/HaX;

    sget-object v0, LX/HaX;->ANIMATED:LX/HaX;

    if-eq p1, v0, :cond_8

    .line 2484247
    :goto_3
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Hae;->d:LX/0am;

    .line 2484248
    :cond_5
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    iget-object v0, v0, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq v1, v0, :cond_2

    .line 2484249
    iget-object v0, p0, LX/Hae;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/Hae;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v1, :cond_9

    :cond_6
    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2484250
    if-eqz v0, :cond_2

    .line 2484251
    iget-object v0, p0, LX/Hae;->c:LX/Hag;

    invoke-static {v0, v1}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v0

    .line 2484252
    if-eqz v0, :cond_2

    .line 2484253
    check-cast v0, LX/HbY;

    .line 2484254
    iget-object v2, v0, LX/HbY;->C:LX/HaY;

    invoke-virtual {v2}, LX/HaY;->a()V

    .line 2484255
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Hae;->d:LX/0am;

    goto/16 :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 2484256
    :cond_8
    const/16 p1, 0x1f4

    const/4 v0, 0x0

    invoke-static {v2, p1, v0}, LX/HaY;->a(LX/HaY;IZ)V

    .line 2484257
    sget-object p1, LX/HaX;->INITIAL:LX/HaX;

    iput-object p1, v2, LX/HaY;->d:LX/HaX;

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_4
.end method
