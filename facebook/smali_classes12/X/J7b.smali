.class public LX/J7b;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/J7f;

.field public d:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:LX/J7a;

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2651066
    const-class v0, LX/J7b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/J7b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/J7f;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2651067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2651068
    const/4 v0, 0x2

    iput v0, p0, LX/J7b;->h:I

    .line 2651069
    iput-object p1, p0, LX/J7b;->b:LX/03V;

    .line 2651070
    iput-object p2, p0, LX/J7b;->c:LX/J7f;

    .line 2651071
    new-instance v0, LX/J7Z;

    invoke-direct {v0, p0}, LX/J7Z;-><init>(LX/J7b;)V

    move-object v0, v0

    .line 2651072
    iput-object v0, p0, LX/J7b;->d:LX/0TF;

    .line 2651073
    return-void
.end method


# virtual methods
.method public final a(LX/J81;)V
    .locals 3

    .prologue
    .line 2651074
    iget-object v1, p0, LX/J7b;->c:LX/J7f;

    iget-object v2, p0, LX/J7b;->d:LX/0TF;

    sget-object v0, LX/J81;->BAKEOFF:LX/J81;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/J7f;->a(LX/0TF;I)V

    .line 2651075
    return-void

    .line 2651076
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2651062
    iget v0, p0, LX/J7b;->g:I

    iget-object v1, p0, LX/J7b;->f:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    .line 2651063
    iget-object v0, p0, LX/J7b;->e:LX/J7a;

    invoke-interface {v0}, LX/J7a;->d()V

    .line 2651064
    :goto_0
    return-void

    .line 2651065
    :cond_0
    iget-object v0, p0, LX/J7b;->e:LX/J7a;

    invoke-interface {v0}, LX/J7a;->c()V

    goto :goto_0
.end method

.method public final c()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2651050
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2651051
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, LX/J7b;->h:I

    if-ge v0, v2, :cond_1

    .line 2651052
    iget v2, p0, LX/J7b;->g:I

    iget-object v3, p0, LX/J7b;->f:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 2651053
    iget v2, p0, LX/J7b;->g:I

    iget-object v3, p0, LX/J7b;->f:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 2651054
    iget-object v2, p0, LX/J7b;->f:LX/0Px;

    iget v3, p0, LX/J7b;->g:I

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;

    .line 2651055
    :goto_1
    move-object v2, v2

    .line 2651056
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2651057
    iget v2, p0, LX/J7b;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/J7b;->g:I

    .line 2651058
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2651059
    :cond_1
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 2651060
    :cond_2
    iget-object v2, p0, LX/J7b;->b:LX/03V;

    sget-object v3, LX/J7b;->a:Ljava/lang/String;

    const-string v4, "survey unit index out of bound"

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2651061
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2651046
    iget-object v0, p0, LX/J7b;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    move v0, v0

    .line 2651047
    iget v1, p0, LX/J7b;->h:I

    div-int/2addr v0, v1

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 2651048
    iget v0, p0, LX/J7b;->g:I

    move v0, v0

    .line 2651049
    iget v1, p0, LX/J7b;->h:I

    div-int/2addr v0, v1

    return v0
.end method
