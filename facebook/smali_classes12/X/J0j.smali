.class public LX/J0j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;",
        "Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0lC;

.field private final b:LX/0dC;


# direct methods
.method public constructor <init>(LX/0lC;LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637697
    iput-object p1, p0, LX/J0j;->a:LX/0lC;

    .line 2637698
    iput-object p2, p0, LX/J0j;->b:LX/0dC;

    .line 2637699
    return-void
.end method

.method public static a(LX/0QB;)LX/J0j;
    .locals 3

    .prologue
    .line 2637700
    new-instance v2, LX/J0j;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v1

    check-cast v1, LX/0dC;

    invoke-direct {v2, v0, v1}, LX/J0j;-><init>(LX/0lC;LX/0dC;)V

    .line 2637701
    move-object v0, v2

    .line 2637702
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2637703
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;

    .line 2637704
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2637705
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "offline_threading_id"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->i:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637706
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "amount"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2637707
    iget-object v4, v3, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v3, v4

    .line 2637708
    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637709
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "currency"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2637710
    iget-object v4, v3, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2637711
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637712
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "sender_credential"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637713
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "memo_text"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637714
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pin"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->f:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637715
    iget-object v1, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->g:Ljava/lang/String;

    .line 2637716
    if-eqz v1, :cond_0

    .line 2637717
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "touchid_nonce"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637718
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/J0j;->b:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637719
    :cond_0
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "theme_id"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->o:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637720
    const-string v1, "%s_%s"

    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->i:Ljava/lang/String;

    const-string v3, "messenger_payments"

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2637721
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "idempotence_token"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637722
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "group_thread_id"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637723
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "platform_context_id"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->j:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637724
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "shipping_address_id"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->l:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637725
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "shipping_option_id"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->m:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637726
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "request_id"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->n:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637727
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637728
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "messenger_payments"

    .line 2637729
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2637730
    move-object v1, v1

    .line 2637731
    const-string v2, "POST"

    .line 2637732
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2637733
    move-object v1, v1

    .line 2637734
    const-string v2, "/%d/%s"

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "messenger_payments"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2637735
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2637736
    move-object v1, v1

    .line 2637737
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2637738
    move-object v0, v1

    .line 2637739
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2637740
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637741
    move-object v0, v0

    .line 2637742
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2637743
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637744
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2637745
    iget-object v1, p0, LX/J0j;->a:LX/0lC;

    invoke-virtual {v0}, LX/0lF;->c()LX/15w;

    move-result-object v0

    iget-object v2, p0, LX/J0j;->a:LX/0lC;

    .line 2637746
    iget-object v3, v2, LX/0lC;->_typeFactory:LX/0li;

    move-object v2, v3

    .line 2637747
    const-class v3, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;

    invoke-virtual {v2, v3}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(LX/15w;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;

    .line 2637748
    return-object v0
.end method
