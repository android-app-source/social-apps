.class public final LX/IDU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/IDG;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/friendlist/fragment/FriendListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendlist/fragment/FriendListFragment;Z)V
    .locals 0

    .prologue
    .line 2550882
    iput-object p1, p0, LX/IDU;->b:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iput-boolean p2, p0, LX/IDU;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2550883
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2550884
    check-cast p1, LX/IDG;

    .line 2550885
    if-eqz p1, :cond_0

    .line 2550886
    iget-object v0, p1, LX/IDG;->a:LX/0Px;

    move-object v0, v0

    .line 2550887
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2550888
    :cond_0
    :goto_0
    return-void

    .line 2550889
    :cond_1
    iget-object v0, p0, LX/IDU;->b:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    .line 2550890
    iget-object v1, p1, LX/IDG;->a:LX/0Px;

    move-object v1, v1

    .line 2550891
    iget-boolean v2, p0, LX/IDU;->a:Z

    .line 2550892
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IDH;

    .line 2550893
    iget-object v4, v0, LX/IE2;->a:Ljava/util/Map;

    invoke-virtual {v3}, LX/IDH;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/IDH;

    .line 2550894
    if-eqz v4, :cond_2

    .line 2550895
    if-eqz v2, :cond_4

    .line 2550896
    iget-boolean v6, v3, LX/IDH;->i:Z

    move v6, v6

    .line 2550897
    if-eqz v6, :cond_4

    .line 2550898
    invoke-virtual {v0}, LX/IE2;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2550899
    iget-object v3, v0, LX/IE2;->f:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2550900
    :cond_3
    iget-object v3, v0, LX/IE2;->a:Ljava/util/Map;

    invoke-virtual {v4}, LX/IDH;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2550901
    :cond_4
    invoke-virtual {v4}, LX/IDH;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    invoke-virtual {v3}, LX/IDH;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    if-eq v6, v7, :cond_5

    .line 2550902
    invoke-virtual {v3}, LX/IDH;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/IDH;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2550903
    :cond_5
    iget-object v6, v3, LX/IDH;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v6, v6

    .line 2550904
    iput-object v6, v4, LX/IDH;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2550905
    goto :goto_1

    .line 2550906
    :cond_6
    invoke-static {v0}, LX/IE2;->e(LX/IE2;)V

    .line 2550907
    iget-object v0, p0, LX/IDU;->b:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    const v1, 0x5f450b57

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
