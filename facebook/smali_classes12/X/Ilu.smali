.class public final LX/Ilu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wv;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;)V
    .locals 0

    .prologue
    .line 2608500
    iput-object p1, p0, LX/Ilu;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2608501
    iget-object v0, p0, LX/Ilu;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2608502
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2608503
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2608504
    const-string v1, "https://m.facebook.com/help/contact/370238886476028"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2608505
    iget-object v1, p0, LX/Ilu;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;

    iget-object v1, v1, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Ilu;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2608506
    iget-object v0, p0, LX/Ilu;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2608507
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2608508
    iget-object v0, p0, LX/Ilu;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2608509
    return-void
.end method
