.class public LX/IFe;
.super LX/IFd;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/IFR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/IFR;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2554664
    invoke-direct {p0, p1}, LX/IFd;-><init>(Ljava/lang/String;)V

    .line 2554665
    iput-object p2, p0, LX/IFe;->a:Ljava/lang/String;

    .line 2554666
    iput-object p3, p0, LX/IFe;->d:LX/0Px;

    .line 2554667
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/IFR;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2554668
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2554669
    iget-object v1, p0, LX/IFe;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2554670
    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2554671
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IFe;->d:LX/0Px;

    .line 2554672
    return-void
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/IFR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2554673
    iget-object v0, p0, LX/IFe;->d:LX/0Px;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/IFR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2554674
    iget-object v0, p0, LX/IFe;->d:LX/0Px;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554675
    iget-object v0, p0, LX/IFe;->a:Ljava/lang/String;

    return-object v0
.end method
