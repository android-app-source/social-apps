.class public LX/Iwr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Iwr;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2631018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2631019
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Iwr;->a:Ljava/util/Set;

    .line 2631020
    return-void
.end method

.method public static a(LX/0QB;)LX/Iwr;
    .locals 3

    .prologue
    .line 2631021
    sget-object v0, LX/Iwr;->b:LX/Iwr;

    if-nez v0, :cond_1

    .line 2631022
    const-class v1, LX/Iwr;

    monitor-enter v1

    .line 2631023
    :try_start_0
    sget-object v0, LX/Iwr;->b:LX/Iwr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2631024
    if-eqz v2, :cond_0

    .line 2631025
    :try_start_1
    new-instance v0, LX/Iwr;

    invoke-direct {v0}, LX/Iwr;-><init>()V

    .line 2631026
    move-object v0, v0

    .line 2631027
    sput-object v0, LX/Iwr;->b:LX/Iwr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2631028
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2631029
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2631030
    :cond_1
    sget-object v0, LX/Iwr;->b:LX/Iwr;

    return-object v0

    .line 2631031
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2631032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2631033
    iget-object v0, p0, LX/Iwr;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2631034
    return-void
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2631035
    iget-object v0, p0, LX/Iwr;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
