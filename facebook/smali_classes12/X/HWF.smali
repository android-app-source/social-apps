.class public LX/HWF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/HXC;


# direct methods
.method public constructor <init>(LX/HXC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475752
    iput-object p1, p0, LX/HWF;->a:LX/HXC;

    .line 2475753
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 2475754
    const-string v0, "extra_page_presence_tab_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2475755
    if-eqz v0, :cond_1

    .line 2475756
    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    .line 2475757
    :goto_0
    const-string v0, "extra_page_presence_tab_content_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2475758
    if-eqz v0, :cond_0

    .line 2475759
    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v0

    move-object v9, v0

    .line 2475760
    :goto_1
    const-string v0, "extra_page_presence_tab_reaction_surface"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2475761
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2475762
    const-string v2, "profile_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2475763
    const-string v3, "page_profile_pic_url_extra"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2475764
    const-string v5, "extra_launched_from_deeplink"

    invoke-virtual {p1, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 2475765
    const-string v6, "extra_is_admin"

    invoke-virtual {p1, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 2475766
    invoke-static/range {v0 .. v7}, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;ZZLjava/lang/String;)Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    move-result-object v0

    .line 2475767
    iget-object v3, p0, LX/HWF;->a:LX/HXC;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    move-object v5, v9

    move-object v6, v10

    invoke-virtual/range {v3 .. v8}, LX/HXC;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;Ljava/lang/String;Landroid/os/Bundle;Z)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v1

    .line 2475768
    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->a(Lcom/facebook/base/fragment/FbFragment;)V

    .line 2475769
    return-object v0

    :cond_0
    move-object v9, v7

    goto :goto_1

    :cond_1
    move-object v4, v7

    goto :goto_0
.end method
