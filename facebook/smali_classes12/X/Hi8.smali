.class public final LX/Hi8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:LX/Hi9;

.field private b:I

.field private c:F

.field private final d:LX/0hc;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Hi9;LX/0hc;)V
    .locals 1
    .param p2    # LX/0hc;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 2496850
    iput-object p1, p0, LX/Hi8;->a:LX/Hi9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2496851
    const/4 v0, -0x1

    iput v0, p0, LX/Hi8;->b:I

    .line 2496852
    iput-object p2, p0, LX/Hi8;->d:LX/0hc;

    .line 2496853
    return-void
.end method

.method private c(I)I
    .locals 1

    .prologue
    .line 2496830
    iget-object v0, p0, LX/Hi8;->a:LX/Hi9;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 2496831
    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 2496847
    iget-object v0, p0, LX/Hi8;->a:LX/Hi9;

    iget-boolean v0, v0, LX/Hi9;->c:Z

    if-nez v0, :cond_0

    .line 2496848
    iget-object v0, p0, LX/Hi8;->d:LX/0hc;

    invoke-direct {p0, p1}, LX/Hi8;->c(I)I

    move-result v1

    invoke-interface {v0, v1}, LX/0hc;->B_(I)V

    .line 2496849
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2496835
    iget-object v0, p0, LX/Hi8;->a:LX/Hi9;

    iget-boolean v0, v0, LX/Hi9;->c:Z

    if-nez v0, :cond_0

    .line 2496836
    cmpl-float v0, p2, v3

    if-nez v0, :cond_1

    if-nez p3, :cond_1

    .line 2496837
    invoke-direct {p0, p1}, LX/Hi8;->c(I)I

    move-result v0

    .line 2496838
    :goto_0
    cmpg-float v1, p2, v3

    if-gez v1, :cond_2

    iget v1, p0, LX/Hi8;->c:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    iget v1, p0, LX/Hi8;->b:I

    if-ne v1, v0, :cond_2

    .line 2496839
    iget v0, p0, LX/Hi8;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Hi8;->b:I

    .line 2496840
    iput v3, p0, LX/Hi8;->c:F

    .line 2496841
    :goto_1
    iget-object v1, p0, LX/Hi8;->d:LX/0hc;

    iget v2, p0, LX/Hi8;->b:I

    iget v0, p0, LX/Hi8;->c:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    iget v3, p0, LX/Hi8;->c:F

    sub-float/2addr v0, v3

    :goto_2
    invoke-interface {v1, v2, v0, p3}, LX/0hc;->a(IFI)V

    .line 2496842
    :cond_0
    return-void

    .line 2496843
    :cond_1
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, LX/Hi8;->c(I)I

    move-result v0

    goto :goto_0

    .line 2496844
    :cond_2
    iput v0, p0, LX/Hi8;->b:I

    .line 2496845
    iput p2, p0, LX/Hi8;->c:F

    goto :goto_1

    .line 2496846
    :cond_3
    iget v0, p0, LX/Hi8;->c:F

    goto :goto_2
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2496832
    iget-object v0, p0, LX/Hi8;->a:LX/Hi9;

    iget-boolean v0, v0, LX/Hi9;->c:Z

    if-nez v0, :cond_0

    .line 2496833
    iget-object v0, p0, LX/Hi8;->d:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    .line 2496834
    :cond_0
    return-void
.end method
