.class public LX/JNV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JNW;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNV",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JNW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685999
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2686000
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JNV;->b:LX/0Zi;

    .line 2686001
    iput-object p1, p0, LX/JNV;->a:LX/0Ot;

    .line 2686002
    return-void
.end method

.method public static a(LX/0QB;)LX/JNV;
    .locals 4

    .prologue
    .line 2685988
    const-class v1, LX/JNV;

    monitor-enter v1

    .line 2685989
    :try_start_0
    sget-object v0, LX/JNV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685990
    sput-object v2, LX/JNV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685991
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685992
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685993
    new-instance v3, LX/JNV;

    const/16 p0, 0x1eec

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNV;-><init>(LX/0Ot;)V

    .line 2685994
    move-object v0, v3

    .line 2685995
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685996
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685997
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685998
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 2686003
    check-cast p2, LX/JNU;

    .line 2686004
    iget-object v0, p0, LX/JNV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JNW;

    iget-object v1, p2, LX/JNU;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2686005
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2686006
    check-cast v2, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->r()Ljava/lang/String;

    move-result-object v8

    .line 2686007
    iget-object v9, v0, LX/JNW;->b:LX/I53;

    new-instance v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    const-string v3, "unknown"

    const-string v4, "unknown"

    iget-object v5, v0, LX/JNW;->c:LX/0kx;

    const-string v6, "native_newsfeed"

    invoke-virtual {v5, v6}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2686008
    iget-object v6, v0, LX/JNW;->c:LX/0kx;

    invoke-virtual {v6}, LX/0kx;->b()Ljava/lang/String;

    move-result-object v6

    const-string v7, "event_dashboard"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2686009
    const-string v6, "home_tab_"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2686010
    :goto_0
    move-object v6, v6

    .line 2686011
    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v9, v8, v2}, LX/I53;->a(Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2686012
    return-void

    :cond_0
    sget-object v6, Lcom/facebook/events/common/ActionMechanism;->EVENTS_SUGGESTION_UNIT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v6}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2685983
    iget-object v0, p0, LX/JNV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JNW;

    .line 2685984
    const/4 v1, 0x0

    const v2, 0x7f0e0123

    invoke-static {p1, v1, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    iget-object v2, v0, LX/JNW;->a:LX/23P;

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f08274d

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 p2, 0x0

    invoke-virtual {v2, p0, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    .line 2685985
    const v2, 0x74d6b75e

    const/4 p0, 0x0

    invoke-static {p1, v2, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2685986
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2685987
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2685977
    invoke-static {}, LX/1dS;->b()V

    .line 2685978
    iget v0, p1, LX/1dQ;->b:I

    .line 2685979
    packed-switch v0, :pswitch_data_0

    .line 2685980
    :goto_0
    return-object v2

    .line 2685981
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2685982
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/JNV;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x74d6b75e
        :pswitch_0
    .end packed-switch
.end method
