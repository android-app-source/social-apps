.class public final enum LX/IVk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IVk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IVk;

.field public static final enum ABOUT:LX/IVk;

.field public static final enum FRIENDS_IN_GROUP:LX/IVk;

.field public static final enum MEMBER_REQUESTS:LX/IVk;

.field public static final enum PENDING_POST:LX/IVk;

.field public static final enum PINNED_POST:LX/IVk;

.field public static final enum PRIVACY:LX/IVk;

.field public static final enum REPORTED_POST:LX/IVk;

.field public static final enum UPCOMING_EVENT:LX/IVk;

.field public static final enum YOUR_SALE_POSTS:LX/IVk;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2582232
    new-instance v0, LX/IVk;

    const-string v1, "FRIENDS_IN_GROUP"

    invoke-direct {v0, v1, v3}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->FRIENDS_IN_GROUP:LX/IVk;

    .line 2582233
    new-instance v0, LX/IVk;

    const-string v1, "ABOUT"

    invoke-direct {v0, v1, v4}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->ABOUT:LX/IVk;

    .line 2582234
    new-instance v0, LX/IVk;

    const-string v1, "PRIVACY"

    invoke-direct {v0, v1, v5}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->PRIVACY:LX/IVk;

    .line 2582235
    new-instance v0, LX/IVk;

    const-string v1, "UPCOMING_EVENT"

    invoke-direct {v0, v1, v6}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->UPCOMING_EVENT:LX/IVk;

    .line 2582236
    new-instance v0, LX/IVk;

    const-string v1, "YOUR_SALE_POSTS"

    invoke-direct {v0, v1, v7}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->YOUR_SALE_POSTS:LX/IVk;

    .line 2582237
    new-instance v0, LX/IVk;

    const-string v1, "MEMBER_REQUESTS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->MEMBER_REQUESTS:LX/IVk;

    .line 2582238
    new-instance v0, LX/IVk;

    const-string v1, "PENDING_POST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->PENDING_POST:LX/IVk;

    .line 2582239
    new-instance v0, LX/IVk;

    const-string v1, "REPORTED_POST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->REPORTED_POST:LX/IVk;

    .line 2582240
    new-instance v0, LX/IVk;

    const-string v1, "PINNED_POST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/IVk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IVk;->PINNED_POST:LX/IVk;

    .line 2582241
    const/16 v0, 0x9

    new-array v0, v0, [LX/IVk;

    sget-object v1, LX/IVk;->FRIENDS_IN_GROUP:LX/IVk;

    aput-object v1, v0, v3

    sget-object v1, LX/IVk;->ABOUT:LX/IVk;

    aput-object v1, v0, v4

    sget-object v1, LX/IVk;->PRIVACY:LX/IVk;

    aput-object v1, v0, v5

    sget-object v1, LX/IVk;->UPCOMING_EVENT:LX/IVk;

    aput-object v1, v0, v6

    sget-object v1, LX/IVk;->YOUR_SALE_POSTS:LX/IVk;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/IVk;->MEMBER_REQUESTS:LX/IVk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IVk;->PENDING_POST:LX/IVk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IVk;->REPORTED_POST:LX/IVk;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/IVk;->PINNED_POST:LX/IVk;

    aput-object v2, v0, v1

    sput-object v0, LX/IVk;->$VALUES:[LX/IVk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2582242
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IVk;
    .locals 1

    .prologue
    .line 2582231
    const-class v0, LX/IVk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IVk;

    return-object v0
.end method

.method public static values()[LX/IVk;
    .locals 1

    .prologue
    .line 2582230
    sget-object v0, LX/IVk;->$VALUES:[LX/IVk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IVk;

    return-object v0
.end method
