.class public LX/I52;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2534983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2534984
    iput-object p1, p0, LX/I52;->a:LX/0Uh;

    .line 2534985
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2534986
    iget-object v0, p0, LX/I52;->a:LX/0Uh;

    const/16 v1, 0x3a3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2534987
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2534988
    new-instance v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-direct {v1}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;-><init>()V

    .line 2534989
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2534990
    move-object v0, v1

    .line 2534991
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2534992
    new-instance v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    invoke-direct {v1}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;-><init>()V

    .line 2534993
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2534994
    move-object v0, v1

    .line 2534995
    goto :goto_0
.end method
