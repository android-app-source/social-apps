.class public LX/JLI;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKAnalyticsFunnelLogger"
.end annotation


# instance fields
.field private final a:LX/0if;


# direct methods
.method public constructor <init>(LX/5pY;LX/0if;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680795
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2680796
    iput-object p2, p0, LX/JLI;->a:LX/0if;

    .line 2680797
    return-void
.end method

.method private a(LX/0ih;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2680836
    if-nez p2, :cond_0

    .line 2680837
    iget-object v0, p0, LX/JLI;->a:LX/0if;

    invoke-virtual {v0, p1, p3, p4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2680838
    :goto_0
    return-void

    .line 2680839
    :cond_0
    iget-object v0, p0, LX/JLI;->a:LX/0if;

    int-to-long v2, p2

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addActionToFunnel(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680840
    invoke-static {p1}, LX/0ih;->b(Ljava/lang/String;)LX/0ih;

    move-result-object v0

    .line 2680841
    if-eqz v0, :cond_0

    .line 2680842
    if-eqz p4, :cond_1

    .line 2680843
    invoke-direct {p0, v0, p2, p3, p4}, LX/JLI;->a(LX/0ih;ILjava/lang/String;Ljava/lang/String;)V

    .line 2680844
    :cond_0
    :goto_0
    return-void

    .line 2680845
    :cond_1
    if-nez p2, :cond_2

    .line 2680846
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    invoke-virtual {v1, v0, p3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 2680847
    :cond_2
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3, p3}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    goto :goto_0
.end method

.method public addFunnelTag(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680811
    invoke-static {p1}, LX/0ih;->b(Ljava/lang/String;)LX/0ih;

    move-result-object v0

    .line 2680812
    if-eqz v0, :cond_0

    .line 2680813
    if-nez p2, :cond_1

    .line 2680814
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    invoke-virtual {v1, v0, p3}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2680815
    :cond_0
    :goto_0
    return-void

    .line 2680816
    :cond_1
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3, p3}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    goto :goto_0
.end method

.method public cancelFunnel(Ljava/lang/String;I)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680817
    invoke-static {p1}, LX/0ih;->b(Ljava/lang/String;)LX/0ih;

    move-result-object v0

    .line 2680818
    if-eqz v0, :cond_0

    .line 2680819
    if-nez p2, :cond_1

    .line 2680820
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    .line 2680821
    invoke-static {v0}, LX/0if;->e(LX/0ih;)V

    .line 2680822
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v2

    .line 2680823
    iput-object v0, v2, LX/15p;->a:LX/0ih;

    .line 2680824
    move-object v2, v2

    .line 2680825
    invoke-virtual {v2}, LX/15p;->a()LX/15o;

    move-result-object v2

    .line 2680826
    iget-object v3, v1, LX/0if;->g:LX/11Y;

    iget-object p0, v1, LX/0if;->g:LX/11Y;

    const/4 p1, 0x5

    invoke-virtual {p0, p1, v2}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 2680827
    :cond_0
    :goto_0
    return-void

    .line 2680828
    :cond_1
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    int-to-long v2, p2

    .line 2680829
    invoke-static {v0}, LX/0if;->e(LX/0ih;)V

    .line 2680830
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v4

    .line 2680831
    iput-object v0, v4, LX/15p;->a:LX/0ih;

    .line 2680832
    move-object v4, v4

    .line 2680833
    invoke-virtual {v4, v2, v3}, LX/15p;->a(J)LX/15p;

    move-result-object v4

    invoke-virtual {v4}, LX/15p;->a()LX/15o;

    move-result-object v4

    .line 2680834
    iget-object p0, v1, LX/0if;->g:LX/11Y;

    iget-object p1, v1, LX/0if;->g:LX/11Y;

    const/4 p2, 0x5

    invoke-virtual {p1, p2, v4}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 2680835
    goto :goto_0
.end method

.method public endFunnel(Ljava/lang/String;I)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680805
    invoke-static {p1}, LX/0ih;->b(Ljava/lang/String;)LX/0ih;

    move-result-object v0

    .line 2680806
    if-eqz v0, :cond_0

    .line 2680807
    if-nez p2, :cond_1

    .line 2680808
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    invoke-virtual {v1, v0}, LX/0if;->c(LX/0ih;)V

    .line 2680809
    :cond_0
    :goto_0
    return-void

    .line 2680810
    :cond_1
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3}, LX/0if;->c(LX/0ih;J)V

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2680804
    const-string v0, "RKAnalyticsFunnelLogger"

    return-object v0
.end method

.method public startFunnel(Ljava/lang/String;I)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680798
    invoke-static {p1}, LX/0ih;->b(Ljava/lang/String;)LX/0ih;

    move-result-object v0

    .line 2680799
    if-eqz v0, :cond_0

    .line 2680800
    if-nez p2, :cond_1

    .line 2680801
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    invoke-virtual {v1, v0}, LX/0if;->a(LX/0ih;)V

    .line 2680802
    :cond_0
    :goto_0
    return-void

    .line 2680803
    :cond_1
    iget-object v1, p0, LX/JLI;->a:LX/0if;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3}, LX/0if;->a(LX/0ih;J)V

    goto :goto_0
.end method
