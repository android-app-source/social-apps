.class public LX/HuU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517650
    iput-object p1, p0, LX/HuU;->a:Landroid/content/res/Resources;

    .line 2517651
    return-void
.end method

.method public static b(LX/0QB;)LX/HuU;
    .locals 2

    .prologue
    .line 2517647
    new-instance v1, LX/HuU;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/HuU;-><init>(Landroid/content/res/Resources;)V

    .line 2517648
    return-object v1
.end method


# virtual methods
.method public final a(LX/Hr2;)LX/Hu0;
    .locals 3

    .prologue
    .line 2517632
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v0

    const v1, 0x7f020964

    .line 2517633
    iput v1, v0, LX/Htz;->a:I

    .line 2517634
    move-object v0, v0

    .line 2517635
    const v1, 0x7f0a0979

    .line 2517636
    iput v1, v0, LX/Htz;->f:I

    .line 2517637
    move-object v0, v0

    .line 2517638
    iget-object v1, p0, LX/HuU;->a:Landroid/content/res/Resources;

    const v2, 0x7f083981

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2517639
    iput-object v1, v0, LX/Htz;->b:Ljava/lang/String;

    .line 2517640
    move-object v0, v0

    .line 2517641
    sget-object v1, LX/Hty;->LOCATION:LX/Hty;

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2517642
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2517643
    move-object v0, v0

    .line 2517644
    iput-object p1, v0, LX/Htz;->e:LX/Hr2;

    .line 2517645
    move-object v0, v0

    .line 2517646
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    return-object v0
.end method
