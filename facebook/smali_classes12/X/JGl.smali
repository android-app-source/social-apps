.class public final LX/JGl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5rT;


# instance fields
.field public final synthetic a:LX/JGq;

.field private final b:I

.field private final c:[LX/JGM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Landroid/util/SparseIntArray;

.field private final e:[F

.field private final f:[F

.field private final g:[LX/JGQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:[LX/JGu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:[F

.field private final j:[F

.field private final k:Z


# direct methods
.method private constructor <init>(LX/JGq;I[LX/JGM;Landroid/util/SparseIntArray;[F[F[LX/JGQ;[LX/JGu;[F[FZ)V
    .locals 0
    .param p3    # [LX/JGM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # [LX/JGQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # [LX/JGu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2669018
    iput-object p1, p0, LX/JGl;->a:LX/JGq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2669019
    iput p2, p0, LX/JGl;->b:I

    .line 2669020
    iput-object p3, p0, LX/JGl;->c:[LX/JGM;

    .line 2669021
    iput-object p4, p0, LX/JGl;->d:Landroid/util/SparseIntArray;

    .line 2669022
    iput-object p5, p0, LX/JGl;->e:[F

    .line 2669023
    iput-object p6, p0, LX/JGl;->f:[F

    .line 2669024
    iput-object p7, p0, LX/JGl;->g:[LX/JGQ;

    .line 2669025
    iput-object p8, p0, LX/JGl;->h:[LX/JGu;

    .line 2669026
    iput-object p9, p0, LX/JGl;->i:[F

    .line 2669027
    iput-object p10, p0, LX/JGl;->j:[F

    .line 2669028
    iput-boolean p11, p0, LX/JGl;->k:Z

    .line 2669029
    return-void
.end method

.method public synthetic constructor <init>(LX/JGq;I[LX/JGM;Landroid/util/SparseIntArray;[F[F[LX/JGQ;[LX/JGu;[F[FZB)V
    .locals 0

    .prologue
    .line 2669030
    invoke-direct/range {p0 .. p11}, LX/JGl;-><init>(LX/JGq;I[LX/JGM;Landroid/util/SparseIntArray;[F[F[LX/JGQ;[LX/JGu;[F[FZ)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 2669031
    iget-object v0, p0, LX/JGl;->a:LX/JGq;

    iget-object v0, v0, LX/JGq;->b:LX/JGd;

    iget v1, p0, LX/JGl;->b:I

    iget-object v2, p0, LX/JGl;->c:[LX/JGM;

    iget-object v3, p0, LX/JGl;->d:Landroid/util/SparseIntArray;

    iget-object v4, p0, LX/JGl;->e:[F

    iget-object v5, p0, LX/JGl;->f:[F

    iget-object v6, p0, LX/JGl;->g:[LX/JGQ;

    iget-object v7, p0, LX/JGl;->h:[LX/JGu;

    iget-object v8, p0, LX/JGl;->i:[F

    iget-object v9, p0, LX/JGl;->j:[F

    iget-boolean v10, p0, LX/JGl;->k:Z

    invoke-virtual/range {v0 .. v10}, LX/JGd;->a(I[LX/JGM;Landroid/util/SparseIntArray;[F[F[LX/JGQ;[LX/JGu;[F[FZ)V

    .line 2669032
    return-void
.end method
