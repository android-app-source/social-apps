.class public LX/HjF;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/Hjs;

.field private static final b:Ljava/lang/String;

.field private static c:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/HjF;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Ljava/lang/String;

.field public f:LX/Hj1;

.field public g:LX/Hj4;

.field public h:LX/HkC;

.field private volatile i:Z

.field public j:LX/Hjj;

.field public k:LX/Hjy;

.field public l:Landroid/view/View;

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public n:Landroid/view/View$OnTouchListener;

.field private o:LX/HjZ;

.field public p:LX/Hjp;

.field public q:LX/HjC;

.field private r:LX/Hl5;

.field public s:LX/HjG;

.field public t:Z

.field public u:Z

.field public v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, LX/Hjs;->a:LX/Hjs;

    sput-object v0, LX/HjF;->a:LX/Hjs;

    const-class v0, LX/HjF;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HjF;->b:Ljava/lang/String;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, LX/HjF;->c:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>(LX/HjF;)V
    .locals 2

    iget-object v0, p1, LX/HjF;->d:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/HjF;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p1, LX/HjF;->k:LX/Hjy;

    iput-object v0, p0, LX/HjF;->k:LX/Hjy;

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HjF;->i:Z

    iget-object v0, p1, LX/HjF;->j:LX/Hjj;

    iput-object v0, p0, LX/HjF;->j:LX/Hjj;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Hjj;LX/Hjy;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/HjF;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p3, p0, LX/HjF;->k:LX/Hjy;

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HjF;->i:Z

    iput-object p2, p0, LX/HjF;->j:LX/Hjj;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HjF;->m:Ljava/util/List;

    iput-object p1, p0, LX/HjF;->d:Landroid/content/Context;

    iput-object p2, p0, LX/HjF;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(LX/Hj9;Landroid/widget/ImageView;)V
    .locals 4

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, LX/Hkr;

    invoke-direct {v0, p1}, LX/Hkr;-><init>(Landroid/widget/ImageView;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Hj9;->a:Ljava/lang/String;

    move-object v3, v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/Hkr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public static m(LX/HjF;)Z
    .locals 1

    iget-object v0, p0, LX/HjF;->j:LX/Hjj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()I
    .locals 1

    iget-object v0, p0, LX/HjF;->k:LX/Hjy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HjF;->k:LX/Hjy;

    iget p0, v0, LX/Hjy;->b:I

    move v0, p0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/HjF;->j:LX/Hjj;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v0}, LX/Hjj;->f()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/HjF;->h:LX/HkC;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HjF;->h:LX/HkC;

    invoke-virtual {v0}, LX/HkC;->a()LX/Hjy;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HjF;->h:LX/HkC;

    invoke-virtual {v0}, LX/HkC;->a()LX/Hjy;

    move-result-object v0

    iget p0, v0, LX/Hjy;->f:I

    move v0, p0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()I
    .locals 1

    iget-object v0, p0, LX/HjF;->k:LX/Hjy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HjF;->k:LX/Hjy;

    iget p0, v0, LX/Hjy;->g:I

    move v0, p0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/HjF;->j:LX/Hjj;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v0}, LX/Hjj;->g()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/HjF;->h:LX/HkC;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HjF;->h:LX/HkC;

    invoke-virtual {v0}, LX/HkC;->a()LX/Hjy;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HjF;->h:LX/HkC;

    invoke-virtual {v0}, LX/HkC;->a()LX/Hjy;

    move-result-object v0

    iget p0, v0, LX/Hjy;->g:I

    move v0, p0

    goto :goto_0

    :cond_2
    const/16 v0, 0x3e8

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must provide a View"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid set of clickable views"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {p0}, LX/HjF;->m(LX/HjF;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, LX/HjF;->b:Ljava/lang/String;

    const-string v1, "Ad not loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, LX/HjF;->l:Landroid/view/View;

    if-eqz v0, :cond_4

    sget-object v0, LX/HjF;->b:Ljava/lang/String;

    const-string v1, "Native Ad was already registered with a View. Auto unregistering and proceeding."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, LX/HjF;->l()V

    :cond_4
    sget-object v0, LX/HjF;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, LX/HjF;->b:Ljava/lang/String;

    const-string v1, "View already registered with a NativeAd. Auto unregistering and proceeding."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, LX/HjF;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HjF;

    invoke-virtual {v0}, LX/HjF;->l()V

    :cond_5
    new-instance v0, LX/HjC;

    invoke-direct {v0, p0}, LX/HjC;-><init>(LX/HjF;)V

    iput-object v0, p0, LX/HjF;->q:LX/HjC;

    iput-object p1, p0, LX/HjF;->l:Landroid/view/View;

    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    new-instance v0, LX/Hl5;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/Hj6;

    invoke-direct {v2, p0}, LX/Hj6;-><init>(LX/HjF;)V

    invoke-direct {v0, v1, v2}, LX/Hl5;-><init>(Landroid/content/Context;LX/Hj6;)V

    iput-object v0, p0, LX/HjF;->r:LX/Hl5;

    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/HjF;->r:LX/Hl5;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_6
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, LX/HjF;->m:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, LX/HjF;->q:LX/HjC;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, LX/HjF;->q:LX/HjC;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    :cond_7
    new-instance v0, LX/Hjp;

    iget-object v1, p0, LX/HjF;->d:Landroid/content/Context;

    new-instance v2, LX/HjE;

    invoke-direct {v2, p0}, LX/HjE;-><init>(LX/HjF;)V

    iget-object v3, p0, LX/HjF;->j:LX/Hjj;

    invoke-direct {v0, v1, v2, v3}, LX/Hjp;-><init>(Landroid/content/Context;LX/HjD;LX/Hjj;)V

    iput-object v0, p0, LX/HjF;->p:LX/Hjp;

    iget-object v0, p0, LX/HjF;->p:LX/Hjp;

    iput-object p2, v0, LX/Hjp;->h:Ljava/util/List;

    const/4 v0, 0x1

    iget-object v1, p0, LX/HjF;->k:LX/Hjy;

    if-eqz v1, :cond_9

    iget-object v0, p0, LX/HjF;->k:LX/Hjy;

    iget v1, v0, LX/Hjy;->b:I

    move v0, v1

    :cond_8
    :goto_2
    move v0, v0

    new-instance v1, LX/HjZ;

    iget-object v2, p0, LX/HjF;->d:Landroid/content/Context;

    iget-object v3, p0, LX/HjF;->l:Landroid/view/View;

    new-instance v4, LX/Hj8;

    invoke-direct {v4, p0}, LX/Hj8;-><init>(LX/HjF;)V

    invoke-direct {v1, v2, v3, v0, v4}, LX/HjZ;-><init>(Landroid/content/Context;Landroid/view/View;ILX/Hj7;)V

    iput-object v1, p0, LX/HjF;->o:LX/HjZ;

    iget-object v0, p0, LX/HjF;->o:LX/HjZ;

    invoke-direct {p0}, LX/HjF;->o()I

    move-result v1

    iput v1, v0, LX/HjZ;->a:I

    iget-object v0, p0, LX/HjF;->o:LX/HjZ;

    invoke-direct {p0}, LX/HjF;->p()I

    move-result v1

    iput v1, v0, LX/HjZ;->b:I

    iget-object v0, p0, LX/HjF;->o:LX/HjZ;

    invoke-virtual {v0}, LX/HjZ;->a()V

    sget-object v0, LX/HjF;->c:Ljava/util/WeakHashMap;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_9
    iget-object v1, p0, LX/HjF;->h:LX/HkC;

    if-eqz v1, :cond_8

    iget-object v1, p0, LX/HjF;->h:LX/HkC;

    invoke-virtual {v1}, LX/HkC;->a()LX/Hjy;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v0, p0, LX/HjF;->h:LX/HkC;

    invoke-virtual {v0}, LX/HkC;->a()LX/Hjy;

    move-result-object v0

    iget v1, v0, LX/Hjy;->b:I

    move v0, v1

    goto :goto_2
.end method

.method public final b()LX/Hj9;
    .locals 1

    invoke-static {p0}, LX/HjF;->m(LX/HjF;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v0}, LX/Hjj;->i()LX/Hj9;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LX/HjF;->m(LX/HjF;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v0}, LX/Hjj;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, LX/HjF;->l:Landroid/view/View;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, LX/HjF;->c:Ljava/util/WeakHashMap;

    iget-object v1, p0, LX/HjF;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/HjF;->c:Ljava/util/WeakHashMap;

    iget-object v1, p0, LX/HjF;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "View not registered with this NativeAd"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, LX/HjF;->l:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HjF;->r:LX/Hl5;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HjF;->l:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/HjF;->r:LX/Hl5;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, LX/HjF;->r:LX/Hl5;

    :cond_3
    sget-object v0, LX/HjF;->c:Ljava/util/WeakHashMap;

    iget-object v1, p0, LX/HjF;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p0, LX/HjF;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, LX/HjF;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v2, p0, LX/HjF;->l:Landroid/view/View;

    iget-object v0, p0, LX/HjF;->o:LX/HjZ;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/HjF;->o:LX/HjZ;

    invoke-virtual {v0}, LX/HjZ;->b()V

    iput-object v2, p0, LX/HjF;->o:LX/HjZ;

    :cond_5
    iput-object v2, p0, LX/HjF;->p:LX/Hjp;

    goto :goto_0
.end method
