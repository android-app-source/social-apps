.class public LX/Irc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private A:I

.field public B:Landroid/view/MotionEvent;

.field public C:I

.field private final D:Landroid/os/Handler;

.field private E:Landroid/view/GestureDetector;

.field public F:Z

.field private final a:Landroid/content/Context;

.field private final b:LX/Irb;

.field private c:LX/Iri;

.field public d:F

.field public e:F

.field private f:Z

.field private g:Z

.field public h:F

.field public i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:J

.field private p:J

.field public q:Z

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:F

.field private w:F

.field private x:F

.field private y:I

.field private z:J


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Irb;)V
    .locals 1

    .prologue
    .line 2618278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Irc;-><init>(Landroid/content/Context;LX/Irb;Landroid/os/Handler;)V

    .line 2618279
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/Irb;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 2618280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2618281
    const/4 v0, 0x0

    iput v0, p0, LX/Irc;->C:I

    .line 2618282
    iput-object p1, p0, LX/Irc;->a:Landroid/content/Context;

    .line 2618283
    iput-object p2, p0, LX/Irc;->b:LX/Irb;

    .line 2618284
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Irc;->r:I

    .line 2618285
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2618286
    const v1, 0x7f0b0365

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/Irc;->A:I

    .line 2618287
    const v1, 0x7f0b0366

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/Irc;->s:I

    .line 2618288
    const v1, 0x7f0b036e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/Irc;->t:I

    .line 2618289
    const v1, 0x7f0b036f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/Irc;->u:I

    .line 2618290
    iput-object p3, p0, LX/Irc;->D:Landroid/os/Handler;

    .line 2618291
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    .line 2618292
    const/4 v0, 0x1

    .line 2618293
    const/4 v1, 0x0

    const/4 p1, 0x0

    invoke-virtual {p0, v0, v1, p1}, LX/Irc;->a(ZZLX/Iri;)V

    .line 2618294
    :cond_0
    return-void
.end method

.method private static b(LX/Irc;Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/high16 v13, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 2618295
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 2618296
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    .line 2618297
    iget-wide v6, p0, LX/Irc;->z:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x80

    cmp-long v0, v4, v6

    if-ltz v0, :cond_6

    move v0, v1

    .line 2618298
    :goto_0
    const/4 v3, 0x0

    move v8, v2

    move v9, v2

    move v4, v0

    .line 2618299
    :goto_1
    if-ge v8, v10, :cond_b

    .line 2618300
    iget v0, p0, LX/Irc;->x:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 2618301
    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v11

    .line 2618302
    add-int/lit8 v12, v11, 0x1

    move v6, v2

    move v5, v3

    .line 2618303
    :goto_3
    if-ge v6, v12, :cond_a

    .line 2618304
    if-ge v6, v11, :cond_8

    .line 2618305
    invoke-virtual {p1, v8, v6}, Landroid/view/MotionEvent;->getHistoricalTouchMajor(II)F

    move-result v3

    .line 2618306
    :goto_4
    iget v7, p0, LX/Irc;->A:I

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-gez v7, :cond_0

    iget v3, p0, LX/Irc;->A:I

    int-to-float v3, v3

    .line 2618307
    :cond_0
    add-float v7, v5, v3

    .line 2618308
    iget v5, p0, LX/Irc;->v:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_1

    iget v5, p0, LX/Irc;->v:F

    cmpl-float v5, v3, v5

    if-lez v5, :cond_2

    .line 2618309
    :cond_1
    iput v3, p0, LX/Irc;->v:F

    .line 2618310
    :cond_2
    iget v5, p0, LX/Irc;->w:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_3

    iget v5, p0, LX/Irc;->w:F

    cmpg-float v5, v3, v5

    if-gez v5, :cond_4

    .line 2618311
    :cond_3
    iput v3, p0, LX/Irc;->w:F

    .line 2618312
    :cond_4
    if-eqz v0, :cond_d

    .line 2618313
    iget v5, p0, LX/Irc;->x:F

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    float-to-int v3, v3

    .line 2618314
    iget v5, p0, LX/Irc;->y:I

    if-ne v3, v5, :cond_5

    if-nez v3, :cond_d

    iget v5, p0, LX/Irc;->y:I

    if-nez v5, :cond_d

    .line 2618315
    :cond_5
    iput v3, p0, LX/Irc;->y:I

    .line 2618316
    if-ge v6, v11, :cond_9

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v4

    .line 2618317
    :goto_5
    iput-wide v4, p0, LX/Irc;->z:J

    move v3, v2

    .line 2618318
    :goto_6
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v5, v7

    move v4, v3

    goto :goto_3

    :cond_6
    move v0, v2

    .line 2618319
    goto :goto_0

    :cond_7
    move v0, v2

    .line 2618320
    goto :goto_2

    .line 2618321
    :cond_8
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getTouchMajor(I)F

    move-result v3

    goto :goto_4

    .line 2618322
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    goto :goto_5

    .line 2618323
    :cond_a
    add-int v3, v9, v12

    .line 2618324
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v9, v3

    move v3, v5

    goto/16 :goto_1

    .line 2618325
    :cond_b
    int-to-float v0, v9

    div-float v0, v3, v0

    .line 2618326
    if-eqz v4, :cond_c

    .line 2618327
    iget v1, p0, LX/Irc;->v:F

    iget v3, p0, LX/Irc;->w:F

    add-float/2addr v1, v3

    add-float/2addr v0, v1

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    .line 2618328
    iget v1, p0, LX/Irc;->v:F

    add-float/2addr v1, v0

    div-float/2addr v1, v13

    iput v1, p0, LX/Irc;->v:F

    .line 2618329
    iget v1, p0, LX/Irc;->w:F

    add-float/2addr v1, v0

    div-float/2addr v1, v13

    iput v1, p0, LX/Irc;->w:F

    .line 2618330
    iput v0, p0, LX/Irc;->x:F

    .line 2618331
    iput v2, p0, LX/Irc;->y:I

    .line 2618332
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/Irc;->z:J

    .line 2618333
    :cond_c
    return-void

    :cond_d
    move v3, v4

    goto :goto_6
.end method

.method private static e(LX/Irc;)V
    .locals 2

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    .line 2618334
    iput v0, p0, LX/Irc;->v:F

    .line 2618335
    iput v0, p0, LX/Irc;->w:F

    .line 2618336
    iput v0, p0, LX/Irc;->x:F

    .line 2618337
    const/4 v0, 0x0

    iput v0, p0, LX/Irc;->y:I

    .line 2618338
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Irc;->z:J

    .line 2618339
    return-void
.end method

.method public static f(LX/Irc;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2618340
    iget v1, p0, LX/Irc;->C:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ZZLX/Iri;)V
    .locals 4

    .prologue
    .line 2618341
    iput-boolean p1, p0, LX/Irc;->f:Z

    .line 2618342
    iput-object p3, p0, LX/Irc;->c:LX/Iri;

    .line 2618343
    iput-boolean p2, p0, LX/Irc;->g:Z

    .line 2618344
    iget-boolean v0, p0, LX/Irc;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Irc;->E:Landroid/view/GestureDetector;

    if-nez v0, :cond_0

    .line 2618345
    new-instance v0, LX/Ira;

    invoke-direct {v0, p0}, LX/Ira;-><init>(LX/Irc;)V

    .line 2618346
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, LX/Irc;->a:Landroid/content/Context;

    iget-object v3, p0, LX/Irc;->D:Landroid/os/Handler;

    invoke-direct {v1, v2, v0, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v1, p0, LX/Irc;->E:Landroid/view/GestureDetector;

    .line 2618347
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    .line 2618348
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/Irc;->o:J

    .line 2618349
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    .line 2618350
    iget-boolean v0, p0, LX/Irc;->f:Z

    if-eqz v0, :cond_0

    .line 2618351
    iget-object v0, p0, LX/Irc;->E:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2618352
    :cond_0
    const/4 v0, 0x1

    if-eq v9, v0, :cond_1

    const/4 v0, 0x3

    if-ne v9, v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    .line 2618353
    :goto_0
    if-eqz v9, :cond_2

    if-eqz v0, :cond_6

    .line 2618354
    :cond_2
    iget-boolean v1, p0, LX/Irc;->q:Z

    if-eqz v1, :cond_5

    .line 2618355
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Irc;->q:Z

    .line 2618356
    const/4 v1, 0x0

    iput v1, p0, LX/Irc;->j:F

    .line 2618357
    const/4 v1, 0x0

    iput v1, p0, LX/Irc;->C:I

    .line 2618358
    :cond_3
    :goto_1
    if-eqz v0, :cond_6

    .line 2618359
    invoke-static {p0}, LX/Irc;->e(LX/Irc;)V

    .line 2618360
    const/4 v0, 0x1

    .line 2618361
    :goto_2
    return v0

    .line 2618362
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2618363
    :cond_5
    iget v1, p0, LX/Irc;->C:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    if-eqz v0, :cond_3

    .line 2618364
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Irc;->q:Z

    .line 2618365
    const/4 v1, 0x0

    iput v1, p0, LX/Irc;->j:F

    .line 2618366
    const/4 v1, 0x0

    iput v1, p0, LX/Irc;->C:I

    goto :goto_1

    .line 2618367
    :cond_6
    if-eqz v9, :cond_7

    const/4 v0, 0x6

    if-eq v9, v0, :cond_7

    const/4 v0, 0x5

    if-ne v9, v0, :cond_9

    :cond_7
    const/4 v0, 0x1

    move v8, v0

    .line 2618368
    :goto_3
    const/4 v0, 0x6

    if-ne v9, v0, :cond_a

    const/4 v0, 0x1

    move v1, v0

    .line 2618369
    :goto_4
    if-eqz v1, :cond_b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 2618370
    :goto_5
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2618371
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    .line 2618372
    if-eqz v1, :cond_c

    add-int/lit8 v1, v2, -0x1

    .line 2618373
    :goto_6
    iget v3, p0, LX/Irc;->C:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_f

    .line 2618374
    iget-object v3, p0, LX/Irc;->c:LX/Iri;

    if-eqz v3, :cond_d

    .line 2618375
    iget-object v3, p0, LX/Irc;->c:LX/Iri;

    .line 2618376
    iget-object v4, v3, LX/Iri;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {v4}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->e(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)F

    move-result v4

    move v4, v4

    .line 2618377
    iget-object v3, p0, LX/Irc;->c:LX/Iri;

    .line 2618378
    iget-object v5, v3, LX/Iri;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {v5}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)F

    move-result v5

    move v3, v5

    .line 2618379
    :goto_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    cmpg-float v5, v5, v3

    if-gez v5, :cond_e

    .line 2618380
    const/4 v5, 0x1

    iput-boolean v5, p0, LX/Irc;->F:Z

    .line 2618381
    :goto_8
    invoke-static {p0, p1}, LX/Irc;->b(LX/Irc;Landroid/view/MotionEvent;)V

    .line 2618382
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2618383
    const/4 v5, 0x0

    move v12, v5

    move v5, v6

    move v6, v7

    move v7, v12

    :goto_9
    if-ge v7, v2, :cond_12

    .line 2618384
    if-eq v0, v7, :cond_8

    .line 2618385
    iget v10, p0, LX/Irc;->x:F

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    .line 2618386
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v11

    sub-float/2addr v11, v4

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v11, v10

    add-float/2addr v6, v11

    .line 2618387
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v11

    sub-float/2addr v11, v3

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v10, v11

    add-float/2addr v5, v10

    .line 2618388
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    .line 2618389
    :cond_9
    const/4 v0, 0x0

    move v8, v0

    goto :goto_3

    .line 2618390
    :cond_a
    const/4 v0, 0x0

    move v1, v0

    goto :goto_4

    .line 2618391
    :cond_b
    const/4 v0, -0x1

    goto :goto_5

    :cond_c
    move v1, v2

    .line 2618392
    goto :goto_6

    .line 2618393
    :cond_d
    iget-object v3, p0, LX/Irc;->B:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 2618394
    iget-object v3, p0, LX/Irc;->B:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    goto :goto_7

    .line 2618395
    :cond_e
    const/4 v5, 0x0

    iput-boolean v5, p0, LX/Irc;->F:Z

    goto :goto_8

    .line 2618396
    :cond_f
    const/4 v3, 0x0

    move v12, v3

    move v3, v4

    move v4, v5

    move v5, v12

    :goto_a
    if-ge v5, v2, :cond_11

    .line 2618397
    if-eq v0, v5, :cond_10

    .line 2618398
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    add-float/2addr v4, v6

    .line 2618399
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    add-float/2addr v3, v6

    .line 2618400
    :cond_10
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 2618401
    :cond_11
    int-to-float v5, v1

    div-float/2addr v4, v5

    .line 2618402
    int-to-float v5, v1

    div-float/2addr v3, v5

    goto :goto_8

    .line 2618403
    :cond_12
    int-to-float v0, v1

    div-float v0, v6, v0

    .line 2618404
    int-to-float v1, v1

    div-float v1, v5, v1

    .line 2618405
    const/high16 v2, 0x40000000    # 2.0f

    mul-float v5, v0, v2

    .line 2618406
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v1, v0

    .line 2618407
    invoke-static {p0}, LX/Irc;->f(LX/Irc;)Z

    move-result v0

    if-eqz v0, :cond_1b

    move v0, v1

    .line 2618408
    :goto_b
    iget-boolean v6, p0, LX/Irc;->q:Z

    .line 2618409
    iput v4, p0, LX/Irc;->d:F

    .line 2618410
    iput v3, p0, LX/Irc;->e:F

    .line 2618411
    invoke-static {p0}, LX/Irc;->f(LX/Irc;)Z

    move-result v2

    if-nez v2, :cond_14

    iget-boolean v2, p0, LX/Irc;->q:Z

    if-eqz v2, :cond_14

    iget v2, p0, LX/Irc;->s:I

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_13

    if-eqz v8, :cond_14

    .line 2618412
    :cond_13
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/Irc;->q:Z

    .line 2618413
    iput v0, p0, LX/Irc;->j:F

    .line 2618414
    const/4 v2, 0x0

    iput v2, p0, LX/Irc;->C:I

    .line 2618415
    :cond_14
    if-eqz v8, :cond_15

    .line 2618416
    iput v5, p0, LX/Irc;->k:F

    iput v5, p0, LX/Irc;->m:F

    .line 2618417
    iput v1, p0, LX/Irc;->l:F

    iput v1, p0, LX/Irc;->n:F

    .line 2618418
    iput v0, p0, LX/Irc;->h:F

    iput v0, p0, LX/Irc;->i:F

    iput v0, p0, LX/Irc;->j:F

    .line 2618419
    :cond_15
    invoke-static {p0}, LX/Irc;->f(LX/Irc;)Z

    move-result v2

    if-eqz v2, :cond_1c

    iget v2, p0, LX/Irc;->r:I

    .line 2618420
    :goto_c
    iget-boolean v3, p0, LX/Irc;->q:Z

    if-nez v3, :cond_17

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_17

    if-nez v6, :cond_16

    iget v2, p0, LX/Irc;->j:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, LX/Irc;->r:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_17

    .line 2618421
    :cond_16
    iput v5, p0, LX/Irc;->k:F

    iput v5, p0, LX/Irc;->m:F

    .line 2618422
    iput v1, p0, LX/Irc;->l:F

    iput v1, p0, LX/Irc;->n:F

    .line 2618423
    iput v0, p0, LX/Irc;->h:F

    iput v0, p0, LX/Irc;->i:F

    .line 2618424
    iget-wide v2, p0, LX/Irc;->o:J

    iput-wide v2, p0, LX/Irc;->p:J

    .line 2618425
    iget-object v2, p0, LX/Irc;->b:LX/Irb;

    invoke-virtual {v2, p0}, LX/Irb;->b(LX/Irc;)Z

    move-result v2

    iput-boolean v2, p0, LX/Irc;->q:Z

    .line 2618426
    :cond_17
    const/4 v2, 0x2

    if-ne v9, v2, :cond_1a

    .line 2618427
    iput v5, p0, LX/Irc;->k:F

    .line 2618428
    iput v1, p0, LX/Irc;->l:F

    .line 2618429
    iput v0, p0, LX/Irc;->h:F

    .line 2618430
    const/4 v0, 0x1

    .line 2618431
    invoke-static {p0}, LX/Irc;->f(LX/Irc;)Z

    move-result v1

    if-eqz v1, :cond_18

    iget-boolean v1, p0, LX/Irc;->g:Z

    if-eqz v1, :cond_18

    .line 2618432
    iget v1, p0, LX/Irc;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 2618433
    iget v2, p0, LX/Irc;->u:I

    int-to-float v2, v2

    iget v3, p0, LX/Irc;->t:I

    int-to-float v3, v3

    invoke-static {v2, v3, v1}, LX/Iqu;->b(FFF)F

    move-result v1

    .line 2618434
    iget v2, p0, LX/Irc;->k:F

    iget v3, p0, LX/Irc;->m:F

    invoke-static {v2, v3, v1}, LX/Iqu;->a(FFF)F

    move-result v2

    iput v2, p0, LX/Irc;->m:F

    .line 2618435
    iget v2, p0, LX/Irc;->l:F

    iget v3, p0, LX/Irc;->n:F

    invoke-static {v2, v3, v1}, LX/Iqu;->a(FFF)F

    move-result v2

    iput v2, p0, LX/Irc;->n:F

    .line 2618436
    iget v2, p0, LX/Irc;->h:F

    iget v3, p0, LX/Irc;->i:F

    invoke-static {v2, v3, v1}, LX/Iqu;->a(FFF)F

    move-result v1

    iput v1, p0, LX/Irc;->i:F

    .line 2618437
    :cond_18
    iget-boolean v1, p0, LX/Irc;->q:Z

    if-eqz v1, :cond_19

    .line 2618438
    iget-object v0, p0, LX/Irc;->b:LX/Irb;

    invoke-virtual {v0, p0}, LX/Irb;->a(LX/Irc;)Z

    move-result v0

    .line 2618439
    :cond_19
    if-eqz v0, :cond_1a

    .line 2618440
    iget v0, p0, LX/Irc;->k:F

    iput v0, p0, LX/Irc;->m:F

    .line 2618441
    iget v0, p0, LX/Irc;->l:F

    iput v0, p0, LX/Irc;->n:F

    .line 2618442
    iget v0, p0, LX/Irc;->h:F

    iput v0, p0, LX/Irc;->i:F

    .line 2618443
    iget-wide v0, p0, LX/Irc;->o:J

    iput-wide v0, p0, LX/Irc;->p:J

    .line 2618444
    :cond_1a
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 2618445
    :cond_1b
    mul-float v0, v5, v5

    mul-float v2, v1, v1

    add-float/2addr v0, v2

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v0, v6

    goto/16 :goto_b

    .line 2618446
    :cond_1c
    iget v2, p0, LX/Irc;->s:I

    goto/16 :goto_c
.end method
