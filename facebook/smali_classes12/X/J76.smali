.class public final LX/J76;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Landroid/widget/Button;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/facebook/profile/inforequest/InfoRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/profile/inforequest/InfoRequestFragment;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;Ljava/lang/String;Landroid/widget/Button;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2650509
    iput-object p1, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iput-object p2, p0, LX/J76;->a:Ljava/lang/String;

    iput-object p3, p0, LX/J76;->b:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    iput-object p4, p0, LX/J76;->c:Ljava/lang/String;

    iput-object p5, p0, LX/J76;->d:Landroid/widget/Button;

    iput-object p6, p0, LX/J76;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v0, 0x1

    const v1, -0x4e84e90c

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2650510
    iget-object v0, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->e:LX/J7R;

    iget-object v1, p0, LX/J76;->a:Ljava/lang/String;

    iget-object v2, p0, LX/J76;->b:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    iget-object v3, p0, LX/J76;->c:Ljava/lang/String;

    iget-object v4, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v4, v4, Lcom/facebook/profile/inforequest/InfoRequestFragment;->i:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v5, v5, Lcom/facebook/profile/inforequest/InfoRequestFragment;->a:LX/0aG;

    iget-object v6, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v6, v6, Lcom/facebook/profile/inforequest/InfoRequestFragment;->m:LX/J73;

    iget-object v7, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v7, v7, Lcom/facebook/profile/inforequest/InfoRequestFragment;->d:Ljava/util/concurrent/Executor;

    .line 2650511
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 2650512
    const-string v12, "operationParams"

    new-instance v13, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;

    invoke-static {v2}, LX/J7R;->a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v13, v3, p1, v4}, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v11, v12, v13}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2650513
    const-string v12, "timeline_send_info_request"

    const v13, -0x52306864

    invoke-static {v5, v12, v11, v13}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v11

    invoke-interface {v11}, LX/1MF;->start()LX/1ML;

    move-result-object v11

    .line 2650514
    new-instance v12, LX/J7K;

    invoke-direct {v12}, LX/J7K;-><init>()V

    .line 2650515
    iput-object v1, v12, LX/J7K;->a:Ljava/lang/String;

    .line 2650516
    move-object v12, v12

    .line 2650517
    sget-object v13, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2650518
    iput-object v13, v12, LX/J7K;->b:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2650519
    move-object v12, v12

    .line 2650520
    invoke-virtual {v12}, LX/J7K;->a()Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;

    move-result-object v12

    .line 2650521
    invoke-static {v0, v11, v12, v6, v7}, LX/J7R;->a(LX/J7R;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;LX/J73;Ljava/util/concurrent/Executor;)V

    .line 2650522
    iget-object v0, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->i:Landroid/widget/EditText;

    invoke-virtual {v0, v9}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2650523
    iget-object v0, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->i:Landroid/widget/EditText;

    invoke-virtual {v0, v9}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 2650524
    iget-object v0, p0, LX/J76;->d:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2650525
    iget-object v0, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2650526
    iget-object v0, p0, LX/J76;->f:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->f:Landroid/widget/TextView;

    iget-object v1, p0, LX/J76;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650527
    const v0, -0x9759949

    invoke-static {v10, v10, v0, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
