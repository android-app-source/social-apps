.class public final LX/HNn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HNb;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459038
    iput-object p1, p0, LX/HNn;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2459039
    iget-object v0, p0, LX/HNn;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->aj:LX/0ih;

    const-string v2, "save_success"

    iget-object v3, p0, LX/HNn;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459040
    iget-object v0, p0, LX/HNn;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, p0, LX/HNn;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081688

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/HNn;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/String;)V

    .line 2459041
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2459042
    iget-object v0, p0, LX/HNn;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->aj:LX/0ih;

    const-string v2, "error_message_shown"

    const-string v3, "server_error"

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459043
    iget-object v0, p0, LX/HNn;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-static {v0, p1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/Throwable;)V

    .line 2459044
    return-void
.end method
