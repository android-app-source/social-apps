.class public LX/Hz2;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Landroid/view/ViewGroup$LayoutParams;


# instance fields
.field private final b:LX/1Db;

.field public c:LX/Hx6;

.field private d:LX/Hx6;

.field private e:Landroid/content/Context;

.field private f:Ljava/lang/String;

.field public g:LX/I2o;

.field private h:LX/I2p;

.field private i:Lcom/facebook/events/common/EventAnalyticsParams;

.field public j:LX/I2Z;

.field private k:LX/HyA;

.field private l:LX/HyO;

.field private m:LX/1nQ;

.field public n:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public o:LX/1My;

.field public p:LX/1Qq;

.field public q:LX/1DS;

.field private r:LX/1DL;

.field private s:LX/1Jg;

.field private t:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

.field private u:LX/2jY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:LX/2ja;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2525289
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    sput-object v0, LX/Hz2;->a:Landroid/view/ViewGroup$LayoutParams;

    return-void
.end method

.method public constructor <init>(LX/Hx6;Ljava/lang/Boolean;Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;LX/2jY;Ljava/lang/String;LX/2ja;LX/I2p;LX/I2a;LX/HyA;LX/HyO;LX/1nQ;LX/1My;LX/1DL;LX/1DS;LX/1Db;LX/0Ot;)V
    .locals 2
    .param p1    # LX/Hx6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/2ja;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Hx6;",
            "Ljava/lang/Boolean;",
            "Landroid/content/Context;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;",
            "LX/2jY;",
            "Ljava/lang/String;",
            "LX/2ja;",
            "LX/I2p;",
            "LX/I2a;",
            "LX/HyA;",
            "LX/HyO;",
            "LX/1nQ;",
            "LX/1My;",
            "LX/1DL;",
            "LX/1DS;",
            "LX/1Db;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2525237
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2525238
    iput-object p1, p0, LX/Hz2;->c:LX/Hx6;

    .line 2525239
    iput-object p3, p0, LX/Hz2;->e:Landroid/content/Context;

    .line 2525240
    iput-object p4, p0, LX/Hz2;->i:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2525241
    iput-object p5, p0, LX/Hz2;->t:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    .line 2525242
    iput-object p6, p0, LX/Hz2;->u:LX/2jY;

    .line 2525243
    iput-object p7, p0, LX/Hz2;->f:Ljava/lang/String;

    .line 2525244
    iput-object p9, p0, LX/Hz2;->h:LX/I2p;

    .line 2525245
    iget-object v1, p0, LX/Hz2;->c:LX/Hx6;

    invoke-virtual {p10, p2, v1}, LX/I2a;->a(Ljava/lang/Boolean;LX/Hx6;)LX/I2Z;

    move-result-object v1

    iput-object v1, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525246
    iput-object p11, p0, LX/Hz2;->k:LX/HyA;

    .line 2525247
    iput-object p12, p0, LX/Hz2;->l:LX/HyO;

    .line 2525248
    iput-object p13, p0, LX/Hz2;->m:LX/1nQ;

    .line 2525249
    invoke-direct {p0}, LX/Hz2;->l()LX/0TF;

    move-result-object v1

    iput-object v1, p0, LX/Hz2;->n:LX/0TF;

    .line 2525250
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Hz2;->o:LX/1My;

    .line 2525251
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Hz2;->r:LX/1DL;

    .line 2525252
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Hz2;->q:LX/1DS;

    .line 2525253
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Hz2;->b:LX/1Db;

    .line 2525254
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Hz2;->w:LX/0Ot;

    .line 2525255
    iput-object p8, p0, LX/Hz2;->v:LX/2ja;

    .line 2525256
    invoke-direct {p0}, LX/Hz2;->g()V

    .line 2525257
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2525258
    iget-object v0, p0, LX/Hz2;->r:LX/1DL;

    invoke-virtual {v0}, LX/1DL;->a()LX/1Jg;

    move-result-object v0

    iput-object v0, p0, LX/Hz2;->s:LX/1Jg;

    .line 2525259
    invoke-static {p0}, LX/Hz2;->h(LX/Hz2;)V

    .line 2525260
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    if-nez v0, :cond_0

    .line 2525261
    iget-object v0, p0, LX/Hz2;->q:LX/1DS;

    iget-object v1, p0, LX/Hz2;->w:LX/0Ot;

    iget-object v2, p0, LX/Hz2;->j:LX/I2Z;

    invoke-virtual {v0, v1, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    iget-object v1, p0, LX/Hz2;->g:LX/I2o;

    .line 2525262
    iput-object v1, v0, LX/1Ql;->f:LX/1PW;

    .line 2525263
    move-object v0, v0

    .line 2525264
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/Hz2;->p:LX/1Qq;

    .line 2525265
    :cond_0
    return-void
.end method

.method private static h(LX/Hz2;)V
    .locals 21

    .prologue
    .line 2525266
    move-object/from16 v0, p0

    iget-object v15, v0, LX/Hz2;->h:LX/I2p;

    const-string v17, "event_dashboard"

    move-object/from16 v0, p0

    iget-object v0, v0, LX/Hz2;->e:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static {}, LX/I6Y;->b()LX/I6Y;

    move-result-object v19

    new-instance v20, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardViewAdapter$1;

    invoke-direct/range {v20 .. v21}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardViewAdapter$1;-><init>(LX/Hz2;)V

    move-object/from16 v0, p0

    iget-object v7, v0, LX/Hz2;->s:LX/1Jg;

    invoke-static/range {p0 .. p0}, LX/Hz2;->j(LX/Hz2;)LX/1PY;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/Hz2;->e:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/Hz2;->t:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/Hz2;->t:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    if-nez v1, :cond_0

    const/4 v11, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, LX/Hz2;->u:LX/2jY;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/Hz2;->i:Lcom/facebook/events/common/EventAnalyticsParams;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/Hz2;->t:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    new-instance v1, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Hz2;->i:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/Hz2;->i:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Hz2;->u:LX/2jY;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Hz2;->u:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v4

    :goto_1
    const-string v5, "unknown"

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v0, v0, LX/Hz2;->j:LX/I2Z;

    move-object/from16 v16, v0

    move-object v2, v15

    move-object/from16 v3, v17

    move-object/from16 v4, v18

    move-object/from16 v5, v19

    move-object/from16 v6, v20

    move-object v15, v1

    invoke-virtual/range {v2 .. v16}, LX/I2p;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1Jg;LX/1PY;Landroid/content/Context;LX/0o8;LX/2ja;LX/2jY;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/I2Y;)LX/I2o;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/Hz2;->g:LX/I2o;

    .line 2525267
    return-void

    .line 2525268
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Hz2;->t:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-virtual {v1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->m()LX/2ja;

    move-result-object v11

    goto :goto_0

    :cond_1
    const-string v4, "unknown"

    goto :goto_1
.end method

.method private static j(LX/Hz2;)LX/1PY;
    .locals 1

    .prologue
    .line 2525269
    new-instance v0, LX/Hz0;

    invoke-direct {v0, p0}, LX/Hz0;-><init>(LX/Hz2;)V

    return-object v0
.end method

.method public static k(LX/Hz2;)V
    .locals 6

    .prologue
    .line 2525270
    invoke-static {p0}, LX/Hz2;->m(LX/Hz2;)V

    .line 2525271
    iget-object v0, p0, LX/Hz2;->m:LX/1nQ;

    iget-object v1, p0, LX/Hz2;->c:LX/Hx6;

    invoke-virtual {v1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525272
    iget-object v3, v2, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v2, v3

    .line 2525273
    iget-object v3, p0, LX/Hz2;->f:Ljava/lang/String;

    iget-object v4, p0, LX/Hz2;->i:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2525274
    iget-object v5, v4, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v4, v5

    .line 2525275
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    iget-object v5, p0, LX/Hz2;->i:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/1nQ;->a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 2525276
    iget-object v0, p0, LX/Hz2;->d:LX/Hx6;

    iget-object v1, p0, LX/Hz2;->c:LX/Hx6;

    if-eq v0, v1, :cond_0

    .line 2525277
    iget-object v0, p0, LX/Hz2;->k:LX/HyA;

    iget-object v1, p0, LX/Hz2;->c:LX/Hx6;

    invoke-virtual {v1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HyA;->c(Ljava/lang/String;)V

    .line 2525278
    iget-object v0, p0, LX/Hz2;->c:LX/Hx6;

    iput-object v0, p0, LX/Hz2;->d:LX/Hx6;

    .line 2525279
    :cond_0
    return-void
.end method

.method private l()LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2525283
    new-instance v0, LX/Hz1;

    invoke-direct {v0, p0}, LX/Hz1;-><init>(LX/Hz2;)V

    return-object v0
.end method

.method public static m(LX/Hz2;)V
    .locals 1

    .prologue
    .line 2525280
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2525281
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2525282
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2525138
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    invoke-interface {v0, p2, p1}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2525139
    sget-object v1, LX/Hz2;->a:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2525140
    new-instance v1, LX/Hyz;

    invoke-direct {v1, v0, p1, p2}, LX/Hyz;-><init>(Landroid/view/View;Landroid/view/ViewGroup;I)V

    return-object v1
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2525290
    iget-object v0, p0, LX/Hz2;->g:LX/I2o;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2525291
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525292
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, LX/I2Z;->d:Ljava/util/List;

    .line 2525293
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/I2Z;->p:Z

    .line 2525294
    invoke-static {v0}, LX/I2Z;->b(LX/I2Z;)V

    .line 2525295
    iget-object v0, p0, LX/Hz2;->c:LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v1, :cond_0

    .line 2525296
    iget-object v0, p0, LX/Hz2;->l:LX/HyO;

    sget-object v1, LX/HyN;->RENDERING:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->a(LX/HyN;)V

    .line 2525297
    :cond_0
    invoke-static {p0}, LX/Hz2;->k(LX/Hz2;)V

    .line 2525298
    return-void
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 2525284
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 2525285
    instance-of v0, p1, LX/Hyz;

    if-eqz v0, :cond_0

    .line 2525286
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    .line 2525287
    :goto_0
    return-void

    .line 2525288
    :cond_0
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2525214
    check-cast p1, LX/Hyz;

    .line 2525215
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    invoke-interface {v0, p2}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    .line 2525216
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    iget-object v1, p0, LX/Hz2;->p:LX/1Qq;

    invoke-interface {v1, p2}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    .line 2525217
    iget v1, p1, LX/Hyz;->m:I

    move v4, v1

    .line 2525218
    iget-object v1, p1, LX/Hyz;->l:Landroid/view/ViewGroup;

    move-object v5, v1

    .line 2525219
    move v1, p2

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 2525220
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    invoke-interface {v0, p2}, LX/1Qr;->h_(I)I

    move-result v1

    .line 2525221
    if-ltz v1, :cond_0

    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    invoke-virtual {v0}, LX/I2Z;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2525222
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    invoke-virtual {v0, v1}, LX/I2Z;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2V;

    .line 2525223
    if-eqz v0, :cond_0

    .line 2525224
    iget-object v2, v0, LX/I2V;->b:LX/I2b;

    move-object v2, v2

    .line 2525225
    sget-object v3, LX/I2b;->i:LX/I2b;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, LX/Hz2;->v:LX/2ja;

    if-eqz v2, :cond_0

    .line 2525226
    iget-object v2, p0, LX/Hz2;->v:LX/2ja;

    .line 2525227
    iget-object v3, v0, LX/I2V;->a:Ljava/lang/Object;

    move-object v0, v3

    .line 2525228
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v2, v0, v1, p2}, LX/2ja;->a(LX/9qX;II)V

    .line 2525229
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2525230
    if-nez p1, :cond_1

    .line 2525231
    :cond_0
    :goto_0
    return-void

    .line 2525232
    :cond_1
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    if-eqz v0, :cond_0

    .line 2525233
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525234
    iget-object v1, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2525235
    invoke-virtual {v0, v1, p1}, LX/I2Z;->a(Ljava/lang/String;Lcom/facebook/events/model/Event;)V

    .line 2525236
    invoke-static {p0}, LX/Hz2;->m(LX/Hz2;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2525205
    iget-object v0, p0, LX/Hz2;->g:LX/I2o;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2525206
    if-nez p1, :cond_0

    .line 2525207
    :goto_0
    return-void

    .line 2525208
    :cond_0
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525209
    iget-object v1, v0, LX/I2Z;->i:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2525210
    :goto_1
    invoke-static {p0}, LX/Hz2;->m(LX/Hz2;)V

    goto :goto_0

    .line 2525211
    :cond_1
    iget-object v1, v0, LX/I2Z;->i:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2525212
    iget-object v2, v0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2525213
    invoke-static {v0}, LX/I2Z;->b(LX/I2Z;)V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2525193
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525194
    iput-object p1, v0, LX/I2Z;->k:Ljava/util/List;

    .line 2525195
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, v0, LX/I2Z;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 2525196
    iget-object v3, v0, LX/I2Z;->l:Ljava/util/HashMap;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2525197
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2525198
    :cond_0
    invoke-static {v0}, LX/I2Z;->b(LX/I2Z;)V

    .line 2525199
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2525200
    invoke-static {v3}, LX/CfY;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Ljava/util/Set;

    move-result-object v7

    .line 2525201
    new-instance v2, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v4, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v5, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2525202
    iget-object v4, p0, LX/Hz2;->o:LX/1My;

    iget-object v5, p0, LX/Hz2;->n:LX/0TF;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v2}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_1

    .line 2525203
    :cond_1
    invoke-static {p0}, LX/Hz2;->m(LX/Hz2;)V

    .line 2525204
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2525186
    iget-object v0, p0, LX/Hz2;->g:LX/I2o;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2525187
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525188
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/I2Z;->d:Ljava/util/List;

    .line 2525189
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v0, LX/I2Z;->f:Ljava/util/List;

    .line 2525190
    invoke-virtual {v0, p1, p2}, LX/I2Z;->b(Ljava/util/List;Ljava/lang/String;)V

    .line 2525191
    invoke-static {p0}, LX/Hz2;->k(LX/Hz2;)V

    .line 2525192
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 2525177
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525178
    iget-boolean v1, v0, LX/I2Z;->o:Z

    if-eq p1, v1, :cond_1

    .line 2525179
    iput-boolean p1, v0, LX/I2Z;->o:Z

    .line 2525180
    invoke-static {v0}, LX/I2Z;->b(LX/I2Z;)V

    .line 2525181
    const/4 v1, 0x1

    .line 2525182
    :goto_0
    move v0, v1

    .line 2525183
    if-eqz v0, :cond_0

    .line 2525184
    invoke-static {p0}, LX/Hz2;->m(LX/Hz2;)V

    .line 2525185
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(LX/Hx6;)Z
    .locals 1

    .prologue
    .line 2525173
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    if-eqz v0, :cond_0

    .line 2525174
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525175
    iget-object p0, v0, LX/I2Z;->m:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    move v0, p0

    .line 2525176
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2525166
    iget-object v0, p0, LX/Hz2;->g:LX/I2o;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2525167
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525168
    iget-object v1, v0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2525169
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/I2Z;->p:Z

    .line 2525170
    invoke-static {v0}, LX/I2Z;->b(LX/I2Z;)V

    .line 2525171
    invoke-static {p0}, LX/Hz2;->k(LX/Hz2;)V

    .line 2525172
    return-void
.end method

.method public final c(LX/Hx6;)V
    .locals 3

    .prologue
    .line 2525157
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    if-eqz v0, :cond_1

    .line 2525158
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525159
    iget-object v1, v0, LX/I2Z;->m:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2525160
    iget-object v1, v0, LX/I2Z;->n:LX/Hx6;

    sget-object v2, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v1, v2, :cond_2

    iget-object v1, v0, LX/I2Z;->e:Ljava/util/List;

    :goto_0
    iput-object v1, v0, LX/I2Z;->d:Ljava/util/List;

    .line 2525161
    iget-object v1, v0, LX/I2Z;->m:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iput-object v1, v0, LX/I2Z;->h:Ljava/util/List;

    .line 2525162
    :cond_0
    invoke-static {p0}, LX/Hz2;->k(LX/Hz2;)V

    .line 2525163
    iget-object v0, p0, LX/Hz2;->k:LX/HyA;

    invoke-virtual {p1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HyA;->c(Ljava/lang/String;)V

    .line 2525164
    :cond_1
    return-void

    .line 2525165
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 2525148
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525149
    iget-boolean v1, v0, LX/I2Z;->p:Z

    if-eq p1, v1, :cond_1

    iget-object v1, v0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2525150
    iput-boolean p1, v0, LX/I2Z;->p:Z

    .line 2525151
    invoke-static {v0}, LX/I2Z;->b(LX/I2Z;)V

    .line 2525152
    const/4 v1, 0x1

    .line 2525153
    :goto_0
    move v0, v1

    .line 2525154
    if-eqz v0, :cond_0

    .line 2525155
    invoke-static {p0}, LX/Hz2;->m(LX/Hz2;)V

    .line 2525156
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final d(LX/Hx6;)Z
    .locals 1

    .prologue
    .line 2525145
    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hz2;->j:LX/I2Z;

    .line 2525146
    iget-object p0, v0, LX/I2Z;->g:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    move v0, p0

    .line 2525147
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2525143
    iget-object v0, p0, LX/Hz2;->o:LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 2525144
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2525142
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2525141
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Hz2;->p:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    goto :goto_0
.end method
