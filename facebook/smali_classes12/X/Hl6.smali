.class public LX/Hl6;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/Hl8;

.field private b:LX/HjZ;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field private g:Z

.field private h:I

.field private i:F


# direct methods
.method private c()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, LX/Hl6;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Hl6;->e:Ljava/lang/String;

    move-object v0, v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "time"

    iget v2, p0, LX/Hl6;->h:I

    div-int/lit16 v2, v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "inline"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, LX/Hkv;

    invoke-direct {v1, v0}, LX/Hkv;-><init>(Ljava/util/Map;)V

    new-array v0, v4, [Ljava/lang/String;

    iget-object v2, p0, LX/Hl6;->e:Ljava/lang/String;

    move-object v2, v2

    aput-object v2, v0, v3

    invoke-virtual {v1, v0}, LX/Hkv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput-boolean v4, p0, LX/Hl6;->g:Z

    iput v3, p0, LX/Hl6;->h:I

    :cond_0
    return-void
.end method

.method private setOnClickListenerInternal(Landroid/view/View$OnClickListener;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setOnTouchListenerInternal(Landroid/view/View$OnTouchListener;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget v0, p0, LX/Hl6;->h:I

    if-lez v0, :cond_0

    invoke-direct {p0}, LX/Hl6;->c()V

    const/4 v0, 0x0

    iput v0, p0, LX/Hl6;->h:I

    :cond_0
    return-void
.end method

.method public getAutoplay()Z
    .locals 1

    iget-boolean v0, p0, LX/Hl6;->f:Z

    return v0
.end method

.method public getVideoPlayReportURI()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LX/Hl6;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoURI()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LX/Hl6;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getVolume()F
    .locals 1

    iget v0, p0, LX/Hl6;->i:F

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x92391e4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    iget-object v1, p0, LX/Hl6;->b:LX/HjZ;

    invoke-virtual {v1}, LX/HjZ;->a()V

    const/16 v1, 0x2d

    const v2, -0x78ad2b57

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x74b8c03d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    invoke-direct {p0}, LX/Hl6;->c()V

    iget-object v1, p0, LX/Hl6;->b:LX/HjZ;

    invoke-virtual {v1}, LX/HjZ;->b()V

    const/16 v1, 0x2d

    const v2, 0x6a0fd6f8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    return-void
.end method

.method public setVolume(F)V
    .locals 0

    iput p1, p0, LX/Hl6;->i:F

    return-void
.end method
