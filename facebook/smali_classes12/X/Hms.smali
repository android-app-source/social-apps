.class public final LX/Hms;
.super LX/Hmq;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Hmq",
        "<",
        "Lcom/facebook/beam/protocol/BeamPreflightInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/protocol/BeamPreflightInfo;

.field public final synthetic b:LX/Hmz;


# direct methods
.method public constructor <init>(LX/Hmz;Lcom/facebook/beam/protocol/BeamPreflightInfo;)V
    .locals 0

    .prologue
    .line 2501128
    iput-object p1, p0, LX/Hms;->b:LX/Hmz;

    iput-object p2, p0, LX/Hms;->a:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-direct {p0, p1}, LX/Hmq;-><init>(LX/Hmz;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2501129
    iget-object v0, p0, LX/Hms;->b:LX/Hmz;

    iget-object v0, v0, LX/Hmz;->b:LX/Hm4;

    iget-object v1, p0, LX/Hms;->a:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-virtual {v0, v1}, LX/Hm4;->a(Lcom/facebook/beam/protocol/BeamPreflightInfo;)V

    .line 2501130
    iget-object v0, p0, LX/Hms;->b:LX/Hmz;

    iget-object v0, v0, LX/Hmz;->b:LX/Hm4;

    invoke-virtual {v0}, LX/Hm4;->d()LX/Hm3;

    move-result-object v0

    .line 2501131
    iget-object v1, p0, LX/Hms;->b:LX/Hmz;

    const/4 v2, 0x1

    new-array v2, v2, [LX/Hm3;

    const/4 v3, 0x0

    sget-object v4, LX/Hm3;->PREFLIGHT_INFO:LX/Hm3;

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/Hmz;->a(Ljava/util/List;LX/Hm3;)V

    .line 2501132
    iget-object v0, p0, LX/Hms;->b:LX/Hmz;

    iget-object v0, v0, LX/Hmz;->b:LX/Hm4;

    invoke-virtual {v0}, LX/Hm4;->e()Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-result-object v0

    return-object v0
.end method
