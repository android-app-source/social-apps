.class public LX/IuH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iu3;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/3Ec;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Or;LX/3Ec;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Iu3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625908
    iput-object p1, p0, LX/IuH;->a:LX/0Ot;

    .line 2625909
    iput-object p2, p0, LX/IuH;->b:LX/0Ot;

    .line 2625910
    iput-object p3, p0, LX/IuH;->c:LX/0Or;

    .line 2625911
    iput-object p4, p0, LX/IuH;->d:LX/3Ec;

    .line 2625912
    return-void
.end method

.method public static a(LX/0QB;)LX/IuH;
    .locals 1

    .prologue
    .line 2625913
    invoke-static {p0}, LX/IuH;->b(LX/0QB;)LX/IuH;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/IuH;
    .locals 5

    .prologue
    .line 2625914
    new-instance v1, LX/IuH;

    const/16 v0, 0x542

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v0, 0x2a0b

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v0, 0x15e7

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/3Ec;->b(LX/0QB;)LX/3Ec;

    move-result-object v0

    check-cast v0, LX/3Ec;

    invoke-direct {v1, v2, v3, v4, v0}, LX/IuH;-><init>(LX/0Ot;LX/0Ot;LX/0Or;LX/3Ec;)V

    .line 2625915
    return-object v1
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2625916
    const-string v0, "_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2625917
    array-length v0, v2

    const/4 v3, 0x2

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625918
    aget-object v0, v2, v1

    return-object v0

    .line 2625919
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2625920
    iget-object v0, p0, LX/IuH;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2625921
    if-eqz v0, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2625922
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2625923
    const-string v4, "_"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 2625924
    aget-object v3, v4, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2625925
    array-length v3, v4

    if-le v3, v2, :cond_1

    .line 2625926
    invoke-static {v6, v7, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2625927
    :goto_1
    return-object v0

    :cond_0
    move v1, v3

    .line 2625928
    goto :goto_0

    .line 2625929
    :cond_1
    iget-object v0, p0, LX/IuH;->d:LX/3Ec;

    invoke-virtual {v0, v6, v7}, LX/3Ec;->c(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_1
.end method
