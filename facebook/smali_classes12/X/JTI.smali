.class public LX/JTI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public c:LX/0gc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Lcom/facebook/content/SecureContextHelper;

.field public e:LX/JTO;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/JTO;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2696548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696549
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/JTI;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2696550
    iput-object p1, p0, LX/JTI;->a:Landroid/content/Context;

    .line 2696551
    iput-object p2, p0, LX/JTI;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2696552
    iget-object v0, p0, LX/JTI;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 2696553
    iget-object v0, p0, LX/JTI;->a:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, LX/JTI;->c:LX/0gc;

    .line 2696554
    :cond_0
    iput-object p3, p0, LX/JTI;->e:LX/JTO;

    .line 2696555
    return-void
.end method

.method public static b(LX/0QB;)LX/JTI;
    .locals 7

    .prologue
    .line 2696556
    new-instance v3, LX/JTI;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    .line 2696557
    new-instance v6, LX/JTO;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-direct {v6, v2, v4, v5}, LX/JTO;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0tX;)V

    .line 2696558
    move-object v2, v6

    .line 2696559
    check-cast v2, LX/JTO;

    invoke-direct {v3, v0, v1, v2}, LX/JTI;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/JTO;)V

    .line 2696560
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 2696561
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2696562
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2696563
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2696564
    iget-object v1, p0, LX/JTI;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/JTI;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2696565
    return-void
.end method
