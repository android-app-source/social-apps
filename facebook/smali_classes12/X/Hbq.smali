.class public LX/Hbq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2486348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2486349
    iput-object p2, p0, LX/Hbq;->a:LX/0Ot;

    .line 2486350
    iput-object p1, p0, LX/Hbq;->b:Landroid/content/Context;

    .line 2486351
    return-void
.end method

.method public static b(LX/0QB;)LX/Hbq;
    .locals 3

    .prologue
    .line 2486352
    new-instance v1, LX/Hbq;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v2, 0x2eb

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/Hbq;-><init>(Landroid/content/Context;LX/0Ot;)V

    .line 2486353
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2486354
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2486355
    sget-object v1, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2486356
    iget-object v0, p0, LX/Hbq;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/Hbq;->b:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2486357
    return-void
.end method
