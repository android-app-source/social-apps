.class public LX/HIj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HIh;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2451593
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/HIj;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451594
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2451595
    iput-object p1, p0, LX/HIj;->b:LX/0Ot;

    .line 2451596
    return-void
.end method

.method public static a(LX/0QB;)LX/HIj;
    .locals 4

    .prologue
    .line 2451597
    const-class v1, LX/HIj;

    monitor-enter v1

    .line 2451598
    :try_start_0
    sget-object v0, LX/HIj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451599
    sput-object v2, LX/HIj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451600
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451601
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2451602
    new-instance v3, LX/HIj;

    const/16 p0, 0x2b9a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HIj;-><init>(LX/0Ot;)V

    .line 2451603
    move-object v0, v3

    .line 2451604
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451605
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HIj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451606
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451607
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2451608
    check-cast p2, LX/HIi;

    .line 2451609
    iget-object v0, p0, LX/HIj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentSpec;

    iget-object v1, p2, LX/HIi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 p2, 0x7

    const/4 p0, 0x6

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v10, 0x2

    .line 2451610
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v2

    .line 2451611
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0b0d61

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2451612
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v7

    iget-object v2, v0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentSpec;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-interface {v5}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v8

    invoke-interface {v8}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    sget-object v8, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 2451613
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0836a5

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a010c

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0052

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0060

    invoke-interface {v6, v4, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0060

    invoke-interface {v6, v10, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    .line 2451614
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0213ab

    invoke-interface {v7, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v3}, LX/1Dh;->P(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b0d5f

    invoke-interface {v7, p2, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b0d5e

    invoke-interface {v7, p0, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v6

    .line 2451615
    invoke-interface {v5}, LX/9uc;->bM()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_0
    if-ge v5, v8, :cond_5

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 2451616
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->VISA:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    if-ne v2, v9, :cond_1

    .line 2451617
    const v2, 0x7f0213af

    .line 2451618
    :goto_1
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v9, 0x7f0b0062

    invoke-interface {v2, v10, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    :cond_0
    move v2, v4

    .line 2451619
    :goto_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v2

    goto :goto_0

    .line 2451620
    :cond_1
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->AMEX:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    if-ne v2, v9, :cond_2

    .line 2451621
    const v2, 0x7f0213ac

    goto :goto_1

    .line 2451622
    :cond_2
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->MASTERCARD:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    if-ne v2, v9, :cond_3

    .line 2451623
    const v2, 0x7f0213ae

    goto :goto_1

    .line 2451624
    :cond_3
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->DISCOVER:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    if-ne v2, v9, :cond_4

    .line 2451625
    const v2, 0x7f0213ad

    goto :goto_1

    .line 2451626
    :cond_4
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->CASH_ONLY:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    if-ne v2, v9, :cond_0

    move v2, v3

    .line 2451627
    goto :goto_2

    .line 2451628
    :cond_5
    if-eqz v4, :cond_6

    .line 2451629
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0836a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00a4

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f02137b

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0061

    invoke-interface {v2, p0, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0063

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0062

    invoke-interface {v2, v10, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2451630
    :cond_6
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2451631
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2451632
    invoke-static {}, LX/1dS;->b()V

    .line 2451633
    const/4 v0, 0x0

    return-object v0
.end method
