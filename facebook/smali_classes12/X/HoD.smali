.class public final LX/HoD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V
    .locals 0

    .prologue
    .line 2505513
    iput-object p1, p0, LX/HoD;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 2

    .prologue
    .line 2505514
    if-eqz p2, :cond_0

    .line 2505515
    iget-object v0, p0, LX/HoD;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2505516
    :cond_0
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 2

    .prologue
    .line 2505505
    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    iget-object v0, p0, LX/HoD;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2505506
    iget-boolean v1, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    move v0, v1

    .line 2505507
    if-nez v0, :cond_0

    iget-object v0, p0, LX/HoD;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2505508
    iget-object v0, p0, LX/HoD;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;

    sget-object v1, LX/HoH;->TAIL_LOAD:LX/HoH;

    invoke-virtual {v0, v1}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a(LX/HoH;)V

    .line 2505509
    iget-object v0, p0, LX/HoD;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->n:LX/Hnj;

    .line 2505510
    const-string v1, "election_hub_tail_feed_fetch"

    invoke-static {v0, v1}, LX/Hnj;->b(LX/Hnj;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2505511
    iget-object p0, v0, LX/Hnj;->a:LX/0Zb;

    invoke-interface {p0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2505512
    :cond_0
    return-void
.end method
