.class public LX/J8l;
.super LX/J8N;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2652138
    const-class v0, LX/J8l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/J8l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2652133
    invoke-direct {p0}, LX/J8N;-><init>()V

    .line 2652134
    :try_start_0
    invoke-static {}, Landroid/os/StrictMode;->getVmPolicy()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    iput-object v0, p0, LX/J8l;->b:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2652135
    :goto_0
    return-void

    .line 2652136
    :catch_0
    move-exception v0

    .line 2652137
    sget-object v1, LX/J8l;->a:Ljava/lang/String;

    const-string v2, "Unable to retrieve current vm policy."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/StrictMode$VmPolicy$Builder;)Landroid/os/StrictMode$VmPolicy;
    .locals 1

    .prologue
    .line 2652129
    iget-object v0, p0, LX/J8l;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 2652130
    iget-object v0, p0, LX/J8l;->b:Ljava/lang/Object;

    check-cast v0, Landroid/os/StrictMode$VmPolicy;

    .line 2652131
    invoke-static {v0}, LX/J8N;->a(Landroid/os/StrictMode$VmPolicy;)Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    .line 2652132
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2652128
    iget-object v0, p0, LX/J8l;->b:Ljava/lang/Object;

    check-cast v0, Landroid/os/StrictMode$VmPolicy;

    invoke-virtual {p0, v0}, LX/J8N;->b(Landroid/os/StrictMode$VmPolicy;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
