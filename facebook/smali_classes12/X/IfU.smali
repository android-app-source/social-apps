.class public final LX/IfU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$AddCYMKSuggestionModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/IfW;


# direct methods
.method public constructor <init>(LX/IfW;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2599758
    iput-object p1, p0, LX/IfU;->c:LX/IfW;

    iput-object p2, p0, LX/IfU;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/IfU;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2599759
    iget-object v0, p0, LX/IfU;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2599760
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2599761
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2599762
    iget-object v0, p0, LX/IfU;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0x4eca8947

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2599763
    iget-object v0, p0, LX/IfU;->c:LX/IfW;

    iget-object v0, v0, LX/IfW;->c:LX/1mR;

    iget-object v1, p0, LX/IfU;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/Iey;->b(Ljava/lang/String;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2599764
    iget-object v1, p0, LX/IfU;->c:LX/IfW;

    .line 2599765
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2599766
    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$AddCYMKSuggestionModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$AddCYMKSuggestionModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    move-result-object v0

    .line 2599767
    :try_start_0
    iget-object v2, v1, LX/IfW;->f:LX/3fw;

    invoke-virtual {v2, v0}, LX/3fw;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v2

    .line 2599768
    new-instance p0, LX/3hB;

    invoke-direct {p0, v2}, LX/3hB;-><init>(Lcom/facebook/contacts/graphql/Contact;)V

    const/4 v2, 0x1

    .line 2599769
    iput-boolean v2, p0, LX/3hB;->z:Z

    .line 2599770
    move-object v2, p0

    .line 2599771
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 2599772
    iput-object p0, v2, LX/3hB;->P:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 2599773
    move-object v2, v2

    .line 2599774
    invoke-virtual {v2}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v2

    .line 2599775
    iget-object p0, v1, LX/IfW;->e:LX/3fv;

    invoke-virtual {p0, v2}, LX/3fv;->a(Lcom/facebook/contacts/graphql/Contact;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2599776
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
