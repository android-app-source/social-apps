.class public LX/HfJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HfH;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2491528
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/HfJ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2491529
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2491530
    iput-object p1, p0, LX/HfJ;->b:LX/0Ot;

    .line 2491531
    return-void
.end method

.method public static a(LX/0QB;)LX/HfJ;
    .locals 4

    .prologue
    .line 2491532
    const-class v1, LX/HfJ;

    monitor-enter v1

    .line 2491533
    :try_start_0
    sget-object v0, LX/HfJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2491534
    sput-object v2, LX/HfJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2491535
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2491536
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2491537
    new-instance v3, LX/HfJ;

    const/16 p0, 0x38c8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HfJ;-><init>(LX/0Ot;)V

    .line 2491538
    move-object v0, v3

    .line 2491539
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2491540
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HfJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2491541
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2491542
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2491543
    const v0, -0x3124dd89

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2491544
    check-cast p2, LX/HfI;

    .line 2491545
    iget-object v0, p0, LX/HfJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;

    iget v1, p2, LX/HfI;->a:I

    iget-object v2, p2, LX/HfI;->b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    iget-object v3, p2, LX/HfI;->c:LX/2kW;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->a(LX/1De;ILcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;LX/2kW;)LX/1Dg;

    move-result-object v0

    .line 2491546
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2491547
    invoke-static {}, LX/1dS;->b()V

    .line 2491548
    iget v0, p1, LX/1dQ;->b:I

    .line 2491549
    sparse-switch v0, :sswitch_data_0

    .line 2491550
    :goto_0
    return-object v2

    .line 2491551
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2491552
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2491553
    check-cast v1, LX/HfI;

    .line 2491554
    iget-object v3, p0, LX/HfJ;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;

    iget v4, v1, LX/HfI;->a:I

    iget-object v5, v1, LX/HfI;->b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    .line 2491555
    invoke-virtual {v5}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object p1

    .line 2491556
    invoke-virtual {v5}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->l()Ljava/lang/String;

    move-result-object p2

    .line 2491557
    iget-object p0, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->d:LX/HfW;

    invoke-virtual {p0, p1, p2}, LX/HfW;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    .line 2491558
    const/4 p2, 0x0

    invoke-static {p2}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object p2

    iget-object p0, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->c:Ljava/util/concurrent/Executor;

    invoke-static {p1, p2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2491559
    iget-object p1, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->i:LX/HgN;

    .line 2491560
    iget-object p2, p1, LX/HgN;->a:LX/0if;

    sget-object p0, LX/0ig;->af:LX/0ih;

    const-string v1, "suggestion_join_unjoin_button"

    const/4 v3, 0x0

    invoke-static {v4, v5}, LX/HgN;->c(ILcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;)LX/1rQ;

    move-result-object v0

    invoke-virtual {p2, p0, v1, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2491561
    goto :goto_0

    .line 2491562
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2491563
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2491564
    check-cast v1, LX/HfI;

    .line 2491565
    iget-object v3, p0, LX/HfJ;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;

    iget v4, v1, LX/HfI;->a:I

    iget-object v5, v1, LX/HfI;->b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    .line 2491566
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 2491567
    invoke-virtual {v5}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->n()Ljava/lang/String;

    move-result-object p1

    .line 2491568
    :goto_1
    move-object p1, p1

    .line 2491569
    if-nez p1, :cond_0

    .line 2491570
    :goto_2
    goto :goto_0

    .line 2491571
    :cond_0
    iget-object p2, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->g:LX/17Y;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {p2, p0, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 2491572
    iget-object p2, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->h:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {p2, p1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2491573
    iget-object p1, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->i:LX/HgN;

    .line 2491574
    iget-object p2, p1, LX/HgN;->a:LX/0if;

    sget-object p0, LX/0ig;->af:LX/0ih;

    const-string v1, "suggestion_go_to_group"

    const/4 v3, 0x0

    invoke-static {v4, v5}, LX/HgN;->c(ILcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;)LX/1rQ;

    move-result-object v0

    invoke-virtual {p2, p0, v1, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2491575
    goto :goto_2

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3125134d -> :sswitch_1
        -0x3124dd89 -> :sswitch_0
    .end sparse-switch
.end method
