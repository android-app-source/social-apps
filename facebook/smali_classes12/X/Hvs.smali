.class public LX/Hvs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData:",
        "Ljava/lang/Object;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsTransliterationSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/view/View$OnClickListener;

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/transliteration/ComposerTransliterationController$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fbui/glyph/GlyphButton;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/HqV;LX/0il;Landroid/view/ViewStub;)V
    .locals 2
    .param p1    # LX/HqV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/transliteration/ComposerTransliterationController$Listener;",
            "TServices;",
            "Landroid/view/ViewStub;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2519984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2519985
    new-instance v0, LX/Hvq;

    invoke-direct {v0, p0}, LX/Hvq;-><init>(LX/Hvs;)V

    iput-object v0, p0, LX/Hvs;->a:Landroid/view/View$OnClickListener;

    .line 2519986
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hvs;->b:Ljava/lang/ref/WeakReference;

    .line 2519987
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hvs;->c:Ljava/lang/ref/WeakReference;

    .line 2519988
    new-instance v0, LX/0zw;

    new-instance v1, LX/Hvr;

    invoke-direct {v1, p0}, LX/Hvr;-><init>(LX/Hvs;)V

    invoke-direct {v0, p3, v1}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v0, p0, LX/Hvs;->d:LX/0zw;

    .line 2519989
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2519990
    iget-object v0, p0, LX/Hvs;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    .line 2519991
    iget-object v1, v0, LX/2zG;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/APP;

    invoke-static {v0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 2519992
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2519993
    sget-object v3, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/2rw;->GROUP:LX/2rw;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/2rw;->USER:LX/2rw;

    if-ne v2, v3, :cond_2

    .line 2519994
    :cond_0
    iget-object v3, v1, LX/APP;->a:LX/01T;

    sget-object v0, LX/01T;->FB4A:LX/01T;

    if-ne v3, v0, :cond_2

    iget-object v3, v1, LX/APP;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/A6h;

    invoke-virtual {v3}, LX/A6h;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2519995
    const/4 v3, 0x1

    .line 2519996
    :goto_0
    move v1, v3

    .line 2519997
    move v0, v1

    .line 2519998
    if-eqz v0, :cond_1

    .line 2519999
    iget-object v0, p0, LX/Hvs;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2520000
    :goto_1
    return-void

    .line 2520001
    :cond_1
    iget-object v0, p0, LX/Hvs;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 1

    .prologue
    .line 2520002
    sget-object v0, LX/5L2;->ON_FIRST_DRAW:LX/5L2;

    if-ne p1, v0, :cond_0

    .line 2520003
    invoke-direct {p0}, LX/Hvs;->b()V

    .line 2520004
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2520005
    invoke-direct {p0}, LX/Hvs;->b()V

    .line 2520006
    return-void
.end method
