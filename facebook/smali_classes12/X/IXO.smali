.class public final enum LX/IXO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IXO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IXO;

.field public static final enum JS:LX/IXO;

.field public static final enum URL:LX/IXO;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2585956
    new-instance v0, LX/IXO;

    const-string v1, "URL"

    invoke-direct {v0, v1, v2}, LX/IXO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IXO;->URL:LX/IXO;

    .line 2585957
    new-instance v0, LX/IXO;

    const-string v1, "JS"

    invoke-direct {v0, v1, v3}, LX/IXO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IXO;->JS:LX/IXO;

    .line 2585958
    const/4 v0, 0x2

    new-array v0, v0, [LX/IXO;

    sget-object v1, LX/IXO;->URL:LX/IXO;

    aput-object v1, v0, v2

    sget-object v1, LX/IXO;->JS:LX/IXO;

    aput-object v1, v0, v3

    sput-object v0, LX/IXO;->$VALUES:[LX/IXO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2585959
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IXO;
    .locals 1

    .prologue
    .line 2585960
    const-class v0, LX/IXO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IXO;

    return-object v0
.end method

.method public static values()[LX/IXO;
    .locals 1

    .prologue
    .line 2585961
    sget-object v0, LX/IXO;->$VALUES:[LX/IXO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IXO;

    return-object v0
.end method
