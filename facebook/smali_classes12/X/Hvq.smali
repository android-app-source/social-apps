.class public final LX/Hvq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Hvs;


# direct methods
.method public constructor <init>(LX/Hvs;)V
    .locals 0

    .prologue
    .line 2519971
    iput-object p1, p0, LX/Hvq;->a:LX/Hvs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x3d24e19f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2519972
    iget-object v0, p0, LX/Hvq;->a:LX/Hvs;

    iget-object v0, v0, LX/Hvs;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqV;

    .line 2519973
    iget-object v3, v0, LX/HqV;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2519974
    iget-object p1, v3, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v0, LX/0ge;->COMPOSER_TRANSLITERATE_CLICK:LX/0ge;

    iget-object p0, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {p0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v0, p0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2519975
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    const-class v0, Lcom/facebook/transliteration/TransliterationActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2519976
    const-string v0, "composer_text"

    iget-object p0, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {p0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2519977
    const-string p0, "entry_point"

    const-string v0, "composer"

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2519978
    iget-object p0, v3, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    const/16 v0, 0xb

    invoke-interface {p0, p1, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2519979
    const v0, -0x2ef021b8

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
