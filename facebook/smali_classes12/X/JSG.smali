.class public LX/JSG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/JSR;


# direct methods
.method public constructor <init>(LX/JSR;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695008
    iput-object p2, p0, LX/JSG;->a:LX/0SG;

    .line 2695009
    iput-object p1, p0, LX/JSG;->b:LX/JSR;

    .line 2695010
    return-void
.end method

.method public static a(LX/0QB;)LX/JSG;
    .locals 5

    .prologue
    .line 2694996
    const-class v1, LX/JSG;

    monitor-enter v1

    .line 2694997
    :try_start_0
    sget-object v0, LX/JSG;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2694998
    sput-object v2, LX/JSG;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2694999
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695000
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695001
    new-instance p0, LX/JSG;

    invoke-static {v0}, LX/JSR;->a(LX/0QB;)LX/JSR;

    move-result-object v3

    check-cast v3, LX/JSR;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/JSG;-><init>(LX/JSR;LX/0SG;)V

    .line 2695002
    move-object v0, p0

    .line 2695003
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695004
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JSG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695005
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695006
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1Pr;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JSO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pr;",
            "LX/JSe;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/JSO;"
        }
    .end annotation

    .prologue
    .line 2694995
    new-instance v0, LX/JSN;

    invoke-direct {v0, p1}, LX/JSN;-><init>(LX/JSe;)V

    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-interface {p0, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSO;

    return-object v0
.end method


# virtual methods
.method public final a(LX/JSe;LX/JTY;LX/JSO;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2694994
    new-instance v0, LX/JSF;

    invoke-direct {v0, p0, p3, p2, p1}, LX/JSF;-><init>(LX/JSG;LX/JSO;LX/JTY;LX/JSe;)V

    return-object v0
.end method

.method public final a(LX/1Pr;LX/JSe;LX/JSI;LX/JTY;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSl;ILcom/facebook/feedplugins/musicstory/MusicPlaybackView;)V
    .locals 6
    .param p6    # LX/JSl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pr;",
            "LX/JSe;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlayer$Callback;",
            "LX/JTY;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/JSl;",
            "I",
            "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2694950
    iget-object v0, p0, LX/JSG;->b:LX/JSR;

    invoke-virtual {v0, p2, p3}, LX/JSR;->b(LX/JSe;LX/JSI;)V

    .line 2694951
    iget-object v0, p0, LX/JSG;->b:LX/JSR;

    invoke-virtual {v0, p2}, LX/JSR;->c(LX/JSe;)Z

    move-result v1

    .line 2694952
    if-eqz p6, :cond_3

    iget v0, p6, LX/JSl;->a:I

    if-eq p7, v0, :cond_3

    const/4 v0, 0x1

    .line 2694953
    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2694954
    invoke-static {p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2694955
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 2694956
    invoke-virtual {p8, v5}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 2694957
    invoke-virtual {p8}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->getTop()I

    move-result p6

    .line 2694958
    const/16 v2, -0x96

    if-ge p6, v2, :cond_6

    move v2, v3

    .line 2694959
    :goto_1
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int v5, p6, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    const/16 p6, 0x96

    if-ge v5, p6, :cond_7

    move v5, v3

    .line 2694960
    :goto_2
    if-nez v2, :cond_8

    if-nez v5, :cond_8

    :goto_3
    move v2, v3

    .line 2694961
    if-eqz v2, :cond_0

    if-eqz v0, :cond_5

    .line 2694962
    :cond_0
    invoke-static {p1, p2, p5}, LX/JSG;->a(LX/1Pr;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JSO;

    move-result-object v0

    .line 2694963
    iget-object v1, v0, LX/JSO;->a:LX/JSK;

    .line 2694964
    sget-object v2, LX/JSK;->STOPPED:LX/JSK;

    iput-object v2, v0, LX/JSO;->a:LX/JSK;

    .line 2694965
    invoke-virtual {p8}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b()V

    .line 2694966
    iget-object v2, p8, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object v2, v2

    .line 2694967
    invoke-virtual {v2}, LX/JSs;->b()V

    .line 2694968
    if-eqz v1, :cond_4

    sget-object v2, LX/JSK;->PAUSED:LX/JSK;

    if-eq v1, v2, :cond_4

    sget-object v2, LX/JSK;->STOPPED:LX/JSK;

    if-eq v1, v2, :cond_4

    .line 2694969
    iget-object v1, p0, LX/JSG;->b:LX/JSR;

    .line 2694970
    invoke-virtual {v1, p2}, LX/JSR;->c(LX/JSe;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, LX/JSR;->c:LX/JTe;

    if-nez v2, :cond_9

    .line 2694971
    :cond_1
    const/4 v2, 0x0

    .line 2694972
    :goto_4
    move v1, v2

    .line 2694973
    iget-object v2, p0, LX/JSG;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, v0, LX/JSO;->c:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    invoke-virtual {p4, v1, v0}, LX/JTY;->a(II)V

    .line 2694974
    iget-object v0, p0, LX/JSG;->b:LX/JSR;

    invoke-virtual {v0, p2, p3}, LX/JSR;->c(LX/JSe;LX/JSI;)V

    .line 2694975
    :cond_2
    :goto_5
    return-void

    .line 2694976
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 2694977
    :cond_4
    iget-object v0, p0, LX/JSG;->b:LX/JSR;

    invoke-virtual {v0, p2, p3}, LX/JSR;->b(LX/JSe;LX/JSI;)V

    goto :goto_5

    .line 2694978
    :cond_5
    if-nez v1, :cond_2

    .line 2694979
    iget-object v0, p0, LX/JSG;->b:LX/JSR;

    invoke-virtual {v0, p2, p3}, LX/JSR;->b(LX/JSe;LX/JSI;)V

    goto :goto_5

    :cond_6
    move v2, v4

    .line 2694980
    goto :goto_1

    :cond_7
    move v5, v4

    .line 2694981
    goto :goto_2

    :cond_8
    move v3, v4

    .line 2694982
    goto :goto_3

    :cond_9
    iget-object v2, v1, LX/JSR;->c:LX/JTe;

    invoke-virtual {v2}, LX/JTe;->b()I

    move-result v2

    goto :goto_4
.end method

.method public final a(LX/JSe;LX/JSI;)V
    .locals 2

    .prologue
    .line 2694983
    iget-object v0, p0, LX/JSG;->b:LX/JSR;

    .line 2694984
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 2694985
    :cond_0
    :goto_0
    return-void

    .line 2694986
    :cond_1
    iget-object v1, v0, LX/JSR;->a:LX/0Xu;

    .line 2694987
    iget-object p0, p1, LX/JSe;->h:Landroid/net/Uri;

    move-object p0, p0

    .line 2694988
    invoke-interface {v1, p0, p2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2694989
    iget-object v1, v0, LX/JSR;->c:LX/JTe;

    if-eqz v1, :cond_0

    .line 2694990
    iget-object v1, v0, LX/JSR;->c:LX/JTe;

    .line 2694991
    iget-object p0, v1, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 2694992
    iget-object p0, v1, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2694993
    :cond_2
    goto :goto_0
.end method
