.class public final LX/Iby;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideSignupMessageMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ic4;


# direct methods
.method public constructor <init>(LX/Ic4;)V
    .locals 0

    .prologue
    .line 2595009
    iput-object p1, p0, LX/Iby;->a:LX/Ic4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2595010
    iget-object v0, p0, LX/Iby;->a:LX/Ic4;

    const/4 v1, 0x0

    .line 2595011
    iput-object v1, v0, LX/Ic4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2595012
    iget-object v0, p0, LX/Iby;->a:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->c:LX/03V;

    sget-object v1, LX/Ic4;->a:Ljava/lang/String;

    const-string v2, "Fail to send signup welcome message"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595013
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595014
    iget-object v0, p0, LX/Iby;->a:LX/Ic4;

    const/4 v1, 0x0

    .line 2595015
    iput-object v1, v0, LX/Ic4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2595016
    return-void
.end method
