.class public LX/Is1;
.super LX/Iqk;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Sh;

.field public final c:Lcom/facebook/common/callercontext/CallerContext;

.field public final d:LX/Irx;

.field public final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final f:LX/1Ad;

.field public final g:LX/IiT;

.field public final h:LX/1HI;

.field public i:LX/Irl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2619187
    const-class v0, LX/Is1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Is1;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Irx;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontext/CallerContext;LX/0Sh;LX/1Ad;LX/IiT;LX/0wW;LX/1HI;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # LX/Irx;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/drawee/fbpipeline/FbDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2619150
    invoke-direct {p0, p1, p2, p7}, LX/Iqk;-><init>(LX/Iqg;Landroid/view/View;LX/0wW;)V

    .line 2619151
    iput-object p1, p0, LX/Is1;->d:LX/Irx;

    .line 2619152
    iput-object p2, p0, LX/Is1;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2619153
    iput-object p3, p0, LX/Is1;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2619154
    iput-object p4, p0, LX/Is1;->b:LX/0Sh;

    .line 2619155
    iput-object p5, p0, LX/Is1;->f:LX/1Ad;

    .line 2619156
    iput-object p6, p0, LX/Is1;->g:LX/IiT;

    .line 2619157
    iput-object p8, p0, LX/Is1;->h:LX/1HI;

    .line 2619158
    iput-object p9, p0, LX/Is1;->j:Ljava/util/concurrent/ExecutorService;

    .line 2619159
    return-void
.end method

.method private t()V
    .locals 4

    .prologue
    .line 2619160
    iget-object v0, p0, LX/Is1;->d:LX/Irx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Iqg;->a(Z)V

    .line 2619161
    iget-object v0, p0, LX/Is1;->d:LX/Irx;

    .line 2619162
    iget-object v1, v0, LX/Irx;->a:Lcom/facebook/stickers/model/Sticker;

    move-object v0, v1

    .line 2619163
    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    .line 2619164
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-static {}, LX/1bd;->c()LX/1bd;

    move-result-object v1

    .line 2619165
    iput-object v1, v0, LX/1bX;->d:LX/1bd;

    .line 2619166
    move-object v0, v0

    .line 2619167
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 2619168
    iget-object v0, p0, LX/Is1;->f:LX/1Ad;

    iget-object v2, p0, LX/Is1;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, LX/Is1;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    new-instance v2, LX/Iry;

    invoke-direct {v2, p0}, LX/Iry;-><init>(LX/Is1;)V

    invoke-virtual {v0, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    .line 2619169
    iget-object v0, p0, LX/Is1;->h:LX/1HI;

    iget-object v2, p0, LX/Is1;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 2619170
    new-instance v2, LX/Irz;

    invoke-direct {v2, p0, v1}, LX/Irz;-><init>(LX/Is1;LX/1bf;)V

    iget-object v3, p0, LX/Is1;->j:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v2, v3}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2619171
    iget-object v0, p0, LX/Is1;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/Is1;->f:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2619172
    iget-object v0, p0, LX/Is1;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->invalidate()V

    .line 2619173
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2619174
    invoke-super {p0}, LX/Iqk;->a()V

    .line 2619175
    invoke-direct {p0}, LX/Is1;->t()V

    .line 2619176
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2619177
    invoke-super {p0, p1}, LX/Iqk;->a(Ljava/lang/Object;)V

    .line 2619178
    instance-of v0, p1, LX/Iqn;

    if-eqz v0, :cond_2

    .line 2619179
    sget-object v0, LX/Is0;->a:[I

    check-cast p1, LX/Iqn;

    invoke-virtual {p1}, LX/Iqn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2619180
    :cond_0
    :goto_0
    return-void

    .line 2619181
    :pswitch_0
    iget-object v1, p0, LX/Is1;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, LX/Is1;->d:LX/Irx;

    .line 2619182
    iget-boolean p0, v0, LX/Iqg;->l:Z

    move v0, p0

    .line 2619183
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_1

    .line 2619184
    :cond_2
    instance-of v0, p1, LX/Irw;

    if-eqz v0, :cond_0

    .line 2619185
    sget-object v0, LX/Is0;->b:[I

    check-cast p1, LX/Irw;

    invoke-virtual {p1}, LX/Irw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 2619186
    :pswitch_1
    invoke-direct {p0}, LX/Is1;->t()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method
