.class public LX/ImP;
.super LX/ImH;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private final a:LX/IzG;

.field private final b:LX/J0b;

.field private final c:LX/03V;

.field private final d:LX/J0B;

.field private final e:LX/18V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609740
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImP;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/IzG;LX/J0b;LX/03V;LX/J0B;LX/18V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609704
    invoke-direct {p0}, LX/ImH;-><init>()V

    .line 2609705
    iput-object p1, p0, LX/ImP;->a:LX/IzG;

    .line 2609706
    iput-object p2, p0, LX/ImP;->b:LX/J0b;

    .line 2609707
    iput-object p3, p0, LX/ImP;->c:LX/03V;

    .line 2609708
    iput-object p4, p0, LX/ImP;->d:LX/J0B;

    .line 2609709
    iput-object p5, p0, LX/ImP;->e:LX/18V;

    .line 2609710
    return-void
.end method

.method public static a(LX/0QB;)LX/ImP;
    .locals 13

    .prologue
    .line 2609711
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609712
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609713
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609714
    if-nez v1, :cond_0

    .line 2609715
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609716
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609717
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609718
    sget-object v1, LX/ImP;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609719
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609720
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609721
    :cond_1
    if-nez v1, :cond_4

    .line 2609722
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609723
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609724
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609725
    new-instance v7, LX/ImP;

    invoke-static {v0}, LX/IzG;->a(LX/0QB;)LX/IzG;

    move-result-object v8

    check-cast v8, LX/IzG;

    invoke-static {v0}, LX/J0b;->a(LX/0QB;)LX/J0b;

    move-result-object v9

    check-cast v9, LX/J0b;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v11

    check-cast v11, LX/J0B;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v12

    check-cast v12, LX/18V;

    invoke-direct/range {v7 .. v12}, LX/ImP;-><init>(LX/IzG;LX/J0b;LX/03V;LX/J0B;LX/18V;)V

    .line 2609726
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609727
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609728
    if-nez v1, :cond_2

    .line 2609729
    sget-object v0, LX/ImP;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImP;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609730
    :goto_1
    if-eqz v0, :cond_3

    .line 2609731
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609732
    :goto_3
    check-cast v0, LX/ImP;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609733
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609734
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609735
    :catchall_1
    move-exception v0

    .line 2609736
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609737
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609738
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609739
    :cond_2
    :try_start_8
    sget-object v0, LX/ImP;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImP;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/7GJ;)Landroid/os/Bundle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2609703
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2609698
    :try_start_0
    iget-object v0, p0, LX/ImP;->e:LX/18V;

    iget-object v1, p0, LX/ImP;->b:LX/J0b;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2609699
    iget-object v1, p0, LX/ImP;->a:LX/IzG;

    invoke-virtual {v1, v0}, LX/IzG;->a(Ljava/util/ArrayList;)V

    .line 2609700
    iget-object v0, p0, LX/ImP;->d:LX/J0B;

    invoke-virtual {v0}, LX/J0B;->f()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2609701
    :goto_0
    return-void

    .line 2609702
    :catch_0
    iget-object v0, p0, LX/ImP;->c:LX/03V;

    const-string v1, "DeltaPlatformItemInterestHandler"

    const-string v2, "Failed to fetch payment platform contexts for the user"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
