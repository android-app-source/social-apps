.class public LX/IAD;
.super LX/IA6;
.source ""


# instance fields
.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/IA5;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2545129
    invoke-direct {p0, p1, p2}, LX/IA6;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 2545130
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/IAD;->b:Ljava/util/Map;

    .line 2545131
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/EventUser;)V
    .locals 3

    .prologue
    .line 2545132
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2545133
    iget-object v1, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2545134
    iget-object v1, p0, LX/IAD;->b:Ljava/util/Map;

    .line 2545135
    iget-object v2, p1, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2545136
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2545137
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 2545138
    iget-object v0, p0, LX/IAD;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2545139
    iget-object v0, p0, LX/IAD;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2545140
    new-instance v2, LX/7vI;

    iget-object v1, p0, LX/IA6;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/model/EventUser;

    invoke-direct {v2, v1}, LX/7vI;-><init>(Lcom/facebook/events/model/EventUser;)V

    .line 2545141
    iput-object p2, v2, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2545142
    move-object v1, v2

    .line 2545143
    invoke-virtual {v1}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v1

    .line 2545144
    iget-object v2, p0, LX/IA6;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2545145
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2545120
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2545121
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2545122
    iget-object v3, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2545123
    iget-object v3, p0, LX/IAD;->b:Ljava/util/Map;

    .line 2545124
    iget-object v4, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2545125
    add-int/lit8 v0, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .line 2545126
    goto :goto_0

    .line 2545127
    :cond_0
    return-void
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2545128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method
