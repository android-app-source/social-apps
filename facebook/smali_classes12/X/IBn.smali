.class public LX/IBn;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements LX/IBX;


# instance fields
.field public a:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I8s;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/7xH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/IBl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1nP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field public j:Lcom/facebook/events/common/EventAnalyticsParams;

.field public k:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 2547618
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2547619
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/IBn;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v3

    check-cast v3, LX/1nQ;

    invoke-static {v0}, LX/I8s;->b(LX/0QB;)LX/I8s;

    move-result-object v4

    check-cast v4, LX/I8s;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v6

    check-cast v6, LX/11S;

    invoke-static {v0}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object v7

    check-cast v7, LX/7xH;

    invoke-static {v0}, LX/IBl;->b(LX/0QB;)LX/IBl;

    move-result-object v8

    check-cast v8, LX/IBl;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-static {v0}, LX/1nP;->a(LX/0QB;)LX/1nP;

    move-result-object v0

    check-cast v0, LX/1nP;

    iput-object v3, v2, LX/IBn;->a:LX/1nQ;

    iput-object v4, v2, LX/IBn;->b:LX/I8s;

    iput-object v5, v2, LX/IBn;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, LX/IBn;->d:LX/11S;

    iput-object v7, v2, LX/IBn;->e:LX/7xH;

    iput-object v8, v2, LX/IBn;->f:LX/IBl;

    iput-object p1, v2, LX/IBn;->g:LX/0ad;

    iput-object v0, v2, LX/IBn;->h:LX/1nP;

    .line 2547620
    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Z)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2547621
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;

    .line 2547622
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 2547623
    const/4 v1, 0x0

    .line 2547624
    invoke-static {p2}, LX/7xC;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2547625
    invoke-virtual {p0}, LX/IBn;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f081ee0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, LX/IBn;->d:LX/11S;

    sget-object v7, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    invoke-interface {v6, v7, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    iget-object v7, p0, LX/IBn;->d:LX/11S;

    sget-object v8, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    invoke-interface {v7, v8, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2547626
    iget-object v2, p0, LX/IBn;->e:LX/7xH;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2547627
    iget-object v3, v2, LX/7xH;->a:Landroid/content/res/Resources;

    const v4, 0x7f081fd7

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 2547628
    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    .line 2547629
    :goto_0
    iget-object v2, p0, LX/IBn;->f:LX/IBl;

    invoke-virtual {v2, p0, v0, v1, p3}, LX/IBl;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 2547630
    iget-object v0, p0, LX/IBn;->f:LX/IBl;

    const v1, 0x7f02033b

    invoke-virtual {v0, p0, v1, p3}, LX/IBl;->a(Landroid/widget/TextView;IZ)V

    .line 2547631
    return-void

    .line 2547632
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2547633
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v2

    .line 2547634
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2547635
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;->c()I

    move-result v3

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, LX/7xE;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    .line 2547636
    invoke-virtual {v2}, Lcom/facebook/payments/currency/CurrencyAmount;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, LX/IBn;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081ea9

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v0, v2

    .line 2547637
    iget-object v2, p0, LX/IBn;->h:LX/1nP;

    .line 2547638
    iget-object v3, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2547639
    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_CONTEXT_ROW:Lcom/facebook/events/common/ActionMechanism;

    iget-object v5, p0, LX/IBn;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v2, v3, v4, v5}, LX/1nP;->a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/common/EventAnalyticsParams;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/IBn;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LX/IBn;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081ea8

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private static b(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 1

    .prologue
    .line 2547640
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 1

    .prologue
    .line 2547641
    iget-object v0, p0, LX/IBn;->b:LX/I8s;

    invoke-virtual {v0, p1, p2}, LX/I8s;->b(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2547642
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ab()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, LX/7xC;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2547643
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 3
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547644
    if-nez p2, :cond_0

    .line 2547645
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/IBn;->setVisibility(I)V

    .line 2547646
    :goto_0
    return-void

    .line 2547647
    :cond_0
    iput-object p2, p0, LX/IBn;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2547648
    iput-object p3, p0, LX/IBn;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2547649
    invoke-direct {p0, p1, p2}, LX/IBn;->b(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, LX/IBn;->b(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2547650
    iget-object v0, p0, LX/IBn;->g:LX/0ad;

    sget-short v1, LX/347;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2547651
    if-eqz v0, :cond_2

    .line 2547652
    :cond_1
    iget-object v0, p0, LX/IBn;->b:LX/I8s;

    .line 2547653
    iget-object v1, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2547654
    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_CONTEXT_ROW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2}, LX/I8s;->a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2547655
    invoke-direct {p0, p1, p2, p4}, LX/IBn;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Z)V

    goto :goto_0

    .line 2547656
    :cond_2
    iget-object v0, p0, LX/IBn;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2547657
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/IBn;->setVisibility(I)V

    .line 2547658
    :goto_1
    goto :goto_0

    .line 2547659
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/IBn;->setVisibility(I)V

    .line 2547660
    iget-object v0, p0, LX/IBn;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/IBn;->k:Landroid/net/Uri;

    .line 2547661
    iget-object v0, p0, LX/IBn;->k:Landroid/net/Uri;

    if-nez v0, :cond_5

    .line 2547662
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/IBn;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2547663
    :goto_2
    iget-object v0, p0, LX/IBn;->b:LX/I8s;

    invoke-virtual {v0, p1, p2}, LX/I8s;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2547664
    invoke-virtual {p0}, LX/IBn;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f86

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2547665
    iget-object v0, p0, LX/IBn;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2547666
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ad()Ljava/lang/String;

    move-result-object v2

    .line 2547667
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 2547668
    :goto_3
    move-object v0, v2

    .line 2547669
    :goto_4
    iget-object v2, p0, LX/IBn;->f:LX/IBl;

    invoke-virtual {v2, p0, v1, v0, p4}, LX/IBl;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 2547670
    iget-object v0, p0, LX/IBn;->f:LX/IBl;

    const v1, 0x7f02033b

    invoke-virtual {v0, p0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;IZ)V

    goto :goto_1

    .line 2547671
    :cond_4
    invoke-virtual {p0}, LX/IBn;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081eec

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2547672
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 2547673
    :cond_5
    new-instance v0, LX/IBm;

    invoke-direct {v0, p0}, LX/IBm;-><init>(LX/IBn;)V

    invoke-virtual {p0, v0}, LX/IBn;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2547674
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v2

    .line 2547675
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_7

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 2
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2547676
    if-nez p2, :cond_1

    .line 2547677
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1, p2}, LX/IBn;->b(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p2}, LX/IBn;->b(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
