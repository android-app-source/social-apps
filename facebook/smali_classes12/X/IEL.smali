.class public final LX/IEL;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2551821
    const-class v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;

    const v0, 0xdd8ca4d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchWithNewPostsFriendListQuery"

    const-string v6, "780d0a3c07c9ec44606eddaab5ec8e4a"

    const-string v7, "user"

    const-string v8, "10155192631701729"

    const-string v9, "10155259088536729"

    .line 2551822
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2551823
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2551824
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2551831
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2551832
    sparse-switch v0, :sswitch_data_0

    .line 2551833
    :goto_0
    return-object p1

    .line 2551834
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2551835
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2551836
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2551837
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2551838
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2551839
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x76d1794f -> :sswitch_5
        -0x41b8e48f -> :sswitch_0
        -0x295975c2 -> :sswitch_2
        0xa174e6b -> :sswitch_3
        0x21beac6a -> :sswitch_1
        0x6c6f579a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2551825
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 2551826
    :goto_1
    return v0

    .line 2551827
    :pswitch_1
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 2551828
    :pswitch_4
    const/16 v0, 0x14

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2551829
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2551830
    :pswitch_6
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
