.class public final LX/Iru;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8CF;


# instance fields
.field public final synthetic a:LX/Irv;


# direct methods
.method public constructor <init>(LX/Irv;)V
    .locals 0

    .prologue
    .line 2619080
    iput-object p1, p0, LX/Iru;->a:LX/Irv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 2619075
    iget-object v0, p0, LX/Iru;->a:LX/Irv;

    iget-object v0, v0, LX/Irv;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setColour(I)V

    .line 2619076
    iget-object v0, p0, LX/Iru;->a:LX/Irv;

    iget-object v0, v0, LX/Irv;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->a()V

    .line 2619077
    iget-object v0, p0, LX/Iru;->a:LX/Irv;

    iget-object v0, v0, LX/IqZ;->a:LX/Iqa;

    if-eqz v0, :cond_0

    .line 2619078
    iget-object v0, p0, LX/Iru;->a:LX/Irv;

    iget-object v0, v0, LX/IqZ;->a:LX/Iqa;

    invoke-virtual {v0, p1}, LX/Iqa;->a(I)V

    .line 2619079
    :cond_0
    return-void
.end method

.method public final a(IFFF)V
    .locals 1

    .prologue
    .line 2619067
    iget-object v0, p0, LX/Iru;->a:LX/Irv;

    iget-object v0, v0, LX/Irv;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(IFFF)V

    .line 2619068
    iget-object v0, p0, LX/Iru;->a:LX/Irv;

    iget-object v0, v0, LX/IqZ;->a:LX/Iqa;

    if-eqz v0, :cond_0

    .line 2619069
    iget-object v0, p0, LX/Iru;->a:LX/Irv;

    iget-object v0, v0, LX/IqZ;->a:LX/Iqa;

    invoke-virtual {v0, p1}, LX/Iqa;->a(I)V

    .line 2619070
    iget-object v0, p0, LX/Iru;->a:LX/Irv;

    iget-object v0, v0, LX/IqZ;->a:LX/Iqa;

    .line 2619071
    iget-object p0, v0, LX/Iqa;->a:LX/Iqd;

    .line 2619072
    invoke-static {p0}, LX/Iqd;->i(LX/Iqd;)V

    .line 2619073
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p4}, Lcom/facebook/drawingview/DrawingView;->setStrokeWidth(F)V

    .line 2619074
    :cond_0
    return-void
.end method
