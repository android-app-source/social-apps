.class public LX/Ik8;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomLinearLayout;"
    }
.end annotation


# instance fields
.field public a:LX/IkC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IkF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/73s;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

.field private f:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

.field private g:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field private h:Lcom/facebook/widget/text/BetterTextView;

.field private i:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2606446
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2606447
    invoke-direct {p0}, LX/Ik8;->a()V

    .line 2606448
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606449
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2606450
    invoke-direct {p0}, LX/Ik8;->a()V

    .line 2606451
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606435
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606436
    invoke-direct {p0}, LX/Ik8;->a()V

    .line 2606437
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2606438
    const-class v0, LX/Ik8;

    invoke-static {v0, p0}, LX/Ik8;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2606439
    const v0, 0x7f030df0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2606440
    const v0, 0x7f0d221c

    invoke-virtual {p0, v0}, LX/Ik8;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    iput-object v0, p0, LX/Ik8;->e:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    .line 2606441
    const v0, 0x7f0d221d

    invoke-virtual {p0, v0}, LX/Ik8;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    iput-object v0, p0, LX/Ik8;->f:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    .line 2606442
    const v0, 0x7f0d1c3b

    invoke-virtual {p0, v0}, LX/Ik8;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, LX/Ik8;->g:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2606443
    const v0, 0x7f0d221e

    invoke-virtual {p0, v0}, LX/Ik8;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ik8;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2606444
    const v0, 0x7f0d221f

    invoke-virtual {p0, v0}, LX/Ik8;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ik8;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2606445
    return-void
.end method

.method private static a(LX/Ik8;LX/IkC;LX/IkF;Landroid/content/res/Resources;LX/73s;)V
    .locals 0

    .prologue
    .line 2606434
    iput-object p1, p0, LX/Ik8;->a:LX/IkC;

    iput-object p2, p0, LX/Ik8;->b:LX/IkF;

    iput-object p3, p0, LX/Ik8;->c:Landroid/content/res/Resources;

    iput-object p4, p0, LX/Ik8;->d:LX/73s;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Ik8;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, LX/Ik8;

    invoke-static {v3}, LX/IkC;->a(LX/0QB;)LX/IkC;

    move-result-object v0

    check-cast v0, LX/IkC;

    invoke-static {v3}, LX/IkF;->a(LX/0QB;)LX/IkF;

    move-result-object v1

    check-cast v1, LX/IkF;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {v3}, LX/73s;->b(LX/0QB;)LX/73s;

    move-result-object v3

    check-cast v3, LX/73s;

    invoke-static {p0, v0, v1, v2, v3}, LX/Ik8;->a(LX/Ik8;LX/IkC;LX/IkF;Landroid/content/res/Resources;LX/73s;)V

    return-void
.end method
