.class public final LX/Hqe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hqd;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;ZZ)V
    .locals 0

    .prologue
    .line 2510670
    iput-object p1, p0, LX/Hqe;->c:Lcom/facebook/composer/activity/ComposerFragment;

    iput-boolean p2, p0, LX/Hqe;->a:Z

    iput-boolean p3, p0, LX/Hqe;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 2510671
    iget-object v0, p0, LX/Hqe;->c:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    iget-object v0, p0, LX/Hqe;->c:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/Hqe;->c:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2510672
    iget-object v3, v1, LX/1Kj;->d:LX/11i;

    sget-object v4, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v3, v4}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v11

    .line 2510673
    const-string v3, "composer_uuid"

    const-string v5, "composer_type"

    .line 2510674
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPicker()LX/5RI;

    move-result-object v4

    sget-object v6, LX/5RI;->NONE:LX/5RI;

    if-eq v4, v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPicker()LX/5RI;

    move-result-object v4

    invoke-virtual {v4}, LX/5RI;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    :goto_0
    move-object v6, v4

    .line 2510675
    const-string v7, "year_class"

    iget-object v4, v1, LX/1Kj;->f:Landroid/content/Context;

    invoke-static {v4}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "ref"

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v4

    invoke-virtual {v4}, LX/21D;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v4, v2

    invoke-static/range {v3 .. v10}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    .line 2510676
    if-eqz v11, :cond_0

    .line 2510677
    const-string v4, "ComposerRenderPhase"

    const v5, -0x7e5213a5

    invoke-static {v11, v4, v5}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v4

    const-string v5, "ComposerDrawPhase"

    const v6, 0x3032b92c

    invoke-static {v4, v5, v6}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v4

    const-string v5, "ComposerLaunchPhase"

    const v6, -0x4984cf35

    invoke-static {v4, v5, v6}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2510678
    iget-object v4, v1, LX/1Kj;->d:LX/11i;

    sget-object v5, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v4, v5, v3}, LX/11i;->b(LX/0Pq;LX/0P1;)V

    .line 2510679
    const/4 v4, 0x0

    iput-boolean v4, v1, LX/1Kj;->g:Z

    .line 2510680
    :cond_0
    iget-object v4, v1, LX/1Kj;->e:LX/0id;

    const-string v5, "OpenComposer"

    invoke-virtual {v4, v5, v3}, LX/0id;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2510681
    iget-object v3, v1, LX/1Kj;->d:LX/11i;

    sget-object v4, LX/1Kj;->b:LX/0Pq;

    invoke-interface {v3, v4}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 2510682
    iget-object v0, p0, LX/Hqe;->c:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    new-instance v1, Lcom/facebook/composer/activity/ComposerFragment$31$1;

    invoke-direct {v1, p0}, Lcom/facebook/composer/activity/ComposerFragment$31$1;-><init>(LX/Hqe;)V

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->post(Ljava/lang/Runnable;)Z

    .line 2510683
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v4

    invoke-virtual {v4}, LX/2rt;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
