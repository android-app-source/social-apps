.class public LX/JCP;
.super LX/3tK;
.source ""

# interfaces
.implements LX/J9E;


# static fields
.field public static final A:[LX/JCO;

.field public static final B:I

.field public static final x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public static final y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

.field public static final z:[LX/JCG;


# instance fields
.field private final j:LX/JCI;

.field private final k:LX/JDA;

.field private final l:LX/J94;

.field private final m:LX/9lP;

.field private final n:LX/J8p;

.field public final o:LX/JCx;

.field private final p:LX/JCc;

.field public q:Z

.field private final r:LX/JDT;

.field private final s:LX/J8z;

.field private final t:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/J9F;

.field public w:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2662129
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->values()[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v0

    sput-object v0, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 2662130
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->values()[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    move-result-object v0

    sput-object v0, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 2662131
    invoke-static {}, LX/JCG;->values()[LX/JCG;

    move-result-object v0

    sput-object v0, LX/JCP;->z:[LX/JCG;

    .line 2662132
    invoke-static {}, LX/JCO;->values()[LX/JCO;

    move-result-object v0

    sput-object v0, LX/JCP;->A:[LX/JCO;

    .line 2662133
    sget-object v0, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v0, v0

    sget-object v1, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v1, v1

    add-int/2addr v0, v1

    sget-object v1, LX/JCG;->LOADING:LX/JCG;

    invoke-virtual {v1}, LX/JCG;->ordinal()I

    move-result v1

    add-int/2addr v0, v1

    sput v0, LX/JCP;->B:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/9lP;LX/J8p;LX/JCI;LX/JDA;LX/J94;LX/JCx;LX/JCc;LX/JDT;LX/J8z;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9lP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/J8p;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2662134
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, LX/3tK;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 2662135
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/JCP;->t:LX/0YU;

    .line 2662136
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/JCP;->u:LX/0YU;

    .line 2662137
    new-instance v0, LX/J9F;

    invoke-direct {v0}, LX/J9F;-><init>()V

    iput-object v0, p0, LX/JCP;->v:LX/J9F;

    .line 2662138
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9lP;

    iput-object v0, p0, LX/JCP;->m:LX/9lP;

    .line 2662139
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J8p;

    iput-object v0, p0, LX/JCP;->n:LX/J8p;

    .line 2662140
    iput-object p4, p0, LX/JCP;->j:LX/JCI;

    .line 2662141
    iput-object p5, p0, LX/JCP;->k:LX/JDA;

    .line 2662142
    iput-object p6, p0, LX/JCP;->l:LX/J94;

    .line 2662143
    iput-object p7, p0, LX/JCP;->o:LX/JCx;

    .line 2662144
    iput-object p8, p0, LX/JCP;->p:LX/JCc;

    .line 2662145
    iput-boolean v2, p0, LX/JCP;->q:Z

    .line 2662146
    iput-object p9, p0, LX/JCP;->r:LX/JDT;

    .line 2662147
    iput-object p10, p0, LX/JCP;->s:LX/J8z;

    .line 2662148
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/View;LX/JCO;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2662124
    sget-object v0, LX/JCN;->a:[I

    invoke-virtual {p3}, LX/JCO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2662125
    invoke-static {p2, p1}, LX/J94;->a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2662126
    :pswitch_0
    invoke-static {p2, p1}, LX/J94;->b(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662127
    :pswitch_1
    invoke-static {p2, p1}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662128
    :pswitch_2
    invoke-static {p2, p1}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2662123
    sget-object v0, LX/JCG;->LOADING:LX/JCG;

    invoke-virtual {v0}, LX/JCG;->getItemLayoutResId()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;Z)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2662105
    invoke-virtual {p0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, LX/2nf;

    .line 2662106
    invoke-static {p0, v0}, LX/JCP;->a(LX/JCP;LX/2nf;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2662107
    sget-object v1, LX/JCN;->c:[I

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2662108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2662109
    :pswitch_0
    invoke-direct {p0, p1, p2, p4}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2662110
    :goto_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    .line 2662111
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2662112
    const v0, 0x7f0302a4

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2662113
    if-eqz p4, :cond_0

    .line 2662114
    const v1, 0x7f0d0963

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2662115
    invoke-static {v0, p1}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662116
    :cond_0
    const v1, 0x7f0d0963

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2662117
    invoke-static {v0, p1}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662118
    :cond_1
    invoke-direct {p0, p1, p2, p4}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662119
    :pswitch_2
    const v0, 0x7f031054

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2662120
    if-eqz p4, :cond_2

    .line 2662121
    invoke-static {v0, p1}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662122
    :cond_2
    invoke-static {v0, p1}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2662096
    const v0, 0x7f03104b

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2662097
    new-instance v1, LX/JCH;

    invoke-direct {v1, v0}, LX/JCH;-><init>(Landroid/view/View;)V

    .line 2662098
    if-eqz p3, :cond_0

    .line 2662099
    invoke-virtual {v1}, LX/JCH;->a()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2662100
    invoke-static {v0, p1}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 2662101
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2662102
    return-object v0

    .line 2662103
    :cond_0
    invoke-virtual {v1}, LX/JCH;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2662104
    invoke-static {v0, p1}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/JCP;LX/2nf;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">(",
            "LX/2nf;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2662086
    goto :goto_1

    .line 2662087
    :goto_0
    return-object v0

    .line 2662088
    :goto_1
    invoke-interface {p1}, LX/2nf;->getPosition()I

    move-result v1

    .line 2662089
    invoke-interface {p1}, LX/2nf;->b()J

    move-result-wide v2

    .line 2662090
    iget-object v0, p0, LX/JCP;->u:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2662091
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_0

    .line 2662092
    iget-object v0, p0, LX/JCP;->t:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    goto :goto_0

    .line 2662093
    :cond_0
    invoke-interface {p1}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 2662094
    iget-object v4, p0, LX/JCP;->t:LX/0YU;

    invoke-virtual {v4, v1, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 2662095
    iget-object v4, p0, LX/JCP;->u:LX/0YU;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, LX/0YU;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2662149
    sget-object v1, LX/JCN;->c:[I

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2662150
    :cond_0
    :goto_0
    return-void

    .line 2662151
    :pswitch_0
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2662152
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    if-eqz v0, :cond_1

    .line 2662153
    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    .line 2662154
    const v1, 0x7f0d0963

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2662155
    iget-object v1, p0, LX/JCP;->j:LX/JCI;

    invoke-virtual {v1, p2, v0}, LX/JCI;->a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;)V

    goto :goto_0

    .line 2662156
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2662157
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JCH;

    invoke-virtual {v0}, LX/JCH;->b()Lcom/facebook/resources/ui/FbTextView;

    move-result-object v1

    .line 2662158
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JCH;

    .line 2662159
    iget-object v2, v0, LX/JCH;->a:Landroid/view/View;

    move-object v0, v2

    .line 2662160
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662161
    iget-object v0, p0, LX/JCP;->r:LX/JDT;

    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/JDT;->a(Lcom/facebook/resources/ui/FbTextView;LX/0Px;)V

    goto :goto_0

    .line 2662162
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2662163
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JCH;

    invoke-virtual {v0}, LX/JCH;->b()Lcom/facebook/resources/ui/FbTextView;

    move-result-object v1

    .line 2662164
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JCH;

    .line 2662165
    iget-object v2, v0, LX/JCH;->a:Landroid/view/View;

    move-object v0, v2

    .line 2662166
    iget-object v2, p0, LX/JCP;->r:LX/JDT;

    const/4 v4, 0x1

    const/4 p3, 0x0

    const/4 p0, 0x0

    .line 2662167
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2662168
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2662169
    if-eqz v3, :cond_5

    move v3, v4

    :goto_1
    if-eqz v3, :cond_8

    .line 2662170
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v3

    iget-object p1, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2662171
    invoke-virtual {p1, v3, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    move v3, v4

    :goto_2
    if-eqz v3, :cond_9

    .line 2662172
    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2662173
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v4, v3, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2662174
    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-static {v3, v4}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 2662175
    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00e1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2662176
    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2662177
    invoke-virtual {v1, p3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662178
    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662179
    const v3, 0x800003

    invoke-static {v1, v3}, LX/JDT;->a(Lcom/facebook/resources/ui/FbTextView;I)V

    .line 2662180
    :goto_3
    goto/16 :goto_0

    .line 2662181
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2662182
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JCH;

    invoke-virtual {v0}, LX/JCH;->b()Lcom/facebook/resources/ui/FbTextView;

    move-result-object v1

    .line 2662183
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JCH;

    .line 2662184
    iget-object v2, v0, LX/JCH;->a:Landroid/view/View;

    move-object v0, v2

    .line 2662185
    iget-object v2, p0, LX/JCP;->r:LX/JDT;

    invoke-virtual {v2, v1, v0, p2}, LX/JDT;->b(Lcom/facebook/resources/ui/FbTextView;Landroid/view/View;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)V

    goto/16 :goto_0

    .line 2662186
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2662187
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JCH;

    invoke-virtual {v0}, LX/JCH;->b()Lcom/facebook/resources/ui/FbTextView;

    move-result-object v1

    .line 2662188
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JCH;

    .line 2662189
    iget-object v2, v0, LX/JCH;->a:Landroid/view/View;

    move-object v0, v2

    .line 2662190
    iget-object v2, p0, LX/JCP;->r:LX/JDT;

    invoke-virtual {v2, v1, v0, p2}, LX/JDT;->b(Lcom/facebook/resources/ui/FbTextView;Landroid/view/View;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)V

    goto/16 :goto_0

    .line 2662191
    :pswitch_2
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    const/4 v0, 0x1

    :cond_4
    if-eqz v0, :cond_0

    .line 2662192
    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;

    .line 2662193
    invoke-virtual {v0, p2}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)V

    goto/16 :goto_0

    :cond_5
    move v3, p0

    .line 2662194
    goto/16 :goto_1

    :cond_6
    move v3, p0

    goto/16 :goto_1

    :cond_7
    move v3, p0

    goto/16 :goto_2

    :cond_8
    move v3, p0

    goto/16 :goto_2

    .line 2662195
    :cond_9
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_a

    :goto_4
    if-eqz v4, :cond_c

    .line 2662196
    invoke-virtual {v2, v1, v0, p2}, LX/JDT;->b(Lcom/facebook/resources/ui/FbTextView;Landroid/view/View;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)V

    goto/16 :goto_3

    :cond_a
    move v4, p0

    .line 2662197
    goto :goto_4

    :cond_b
    move v4, p0

    goto :goto_4

    .line 2662198
    :cond_c
    const-string v3, ""

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2662199
    invoke-virtual {v1, p3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662200
    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2662064
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2662065
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {p0, v2}, LX/JCP;->getItemViewType(I)I

    move-result v2

    .line 2662066
    invoke-static {v2}, LX/JEz;->b(I)I

    move-result v3

    .line 2662067
    invoke-static {v2}, LX/JEz;->a(I)LX/JCO;

    move-result-object v2

    .line 2662068
    sget-object v4, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v4, v4

    sget-object v5, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v5, v5

    add-int/2addr v4, v5

    if-lt v3, v4, :cond_1

    .line 2662069
    invoke-static {}, LX/JCG;->values()[LX/JCG;

    move-result-object v4

    sget-object v5, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v5, v5

    sub-int/2addr v3, v5

    sget-object v5, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v5, v5

    sub-int/2addr v3, v5

    aget-object v3, v4, v3

    .line 2662070
    sget-object v4, LX/JCN;->b:[I

    invoke-virtual {v3}, LX/JCG;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2662071
    iget-object v0, p0, LX/JCP;->j:LX/JCI;

    iget-object v1, p0, LX/3tK;->d:Landroid/content/Context;

    invoke-virtual {v3}, LX/JCG;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2, p3}, LX/JCI;->a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2662072
    :goto_0
    return-object v0

    .line 2662073
    :pswitch_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2662074
    :pswitch_1
    invoke-static {v1, p3}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2662075
    invoke-direct {p0, v1, v0, v2}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/View;LX/JCO;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662076
    :pswitch_2
    new-instance v0, LX/JDe;

    invoke-direct {v0, p1}, LX/JDe;-><init>(Landroid/content/Context;)V

    .line 2662077
    invoke-direct {p0, v1, v0, v2}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/View;LX/JCO;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662078
    :pswitch_3
    invoke-virtual {v3}, LX/JCG;->getItemLayoutResId()I

    move-result v3

    invoke-virtual {v1, v3, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2662079
    const v4, 0x7f0d0963

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    sget-object v5, LX/JCO;->LAST:LX/JCO;

    if-ne v2, v5, :cond_0

    const/4 v0, 0x4

    :cond_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2662080
    invoke-direct {p0, v1, v3, v2}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/View;LX/JCO;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662081
    :cond_1
    sget-object v4, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v4, v4

    if-lt v3, v4, :cond_3

    .line 2662082
    sget-object v4, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v5, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v5, v5

    sub-int/2addr v3, v5

    aget-object v3, v4, v3

    .line 2662083
    sget-object v4, LX/JCO;->LAST:LX/JCO;

    if-ne v2, v4, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-direct {p0, v1, p3, v3, v0}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2662084
    :cond_3
    iget-object v0, p0, LX/JCP;->k:LX/JDA;

    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->values()[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v2

    aget-object v2, v2, v3

    invoke-virtual {v0, v2, v1, p1}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 2662085
    invoke-static {v0, v1}, LX/J94;->a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 2661995
    move-object v0, p2

    check-cast v0, LX/2nf;

    invoke-static {p0, v0}, LX/JCP;->a(LX/JCP;LX/2nf;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v1

    .line 2661996
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, LX/JCP;->getItemViewType(I)I

    move-result v0

    .line 2661997
    invoke-static {v0}, LX/JEz;->b(I)I

    move-result v0

    .line 2661998
    sget-object v3, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v3, v3

    sget-object v4, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v4, v4

    add-int/2addr v3, v4

    if-lt v0, v3, :cond_5

    .line 2661999
    invoke-static {}, LX/JCG;->values()[LX/JCG;

    move-result-object v3

    sget-object v4, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v4, v4

    sub-int/2addr v0, v4

    sget-object v4, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v4, v4

    sub-int/2addr v0, v4

    aget-object v4, v3, v0

    .line 2662000
    sget-object v0, LX/JCN;->b:[I

    invoke-virtual {v4}, LX/JCG;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :goto_0
    move-object v0, v2

    .line 2662001
    :goto_1
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 2662002
    iget-object v1, p0, LX/JCP;->n:LX/J8p;

    iget-object v3, p0, LX/JCP;->m:LX/9lP;

    .line 2662003
    iget-object v4, v3, LX/9lP;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2662004
    iget-object v4, p0, LX/JCP;->m:LX/9lP;

    invoke-static {v4}, LX/J8p;->a(LX/9lP;)LX/9lQ;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v0, v2}, LX/J8p;->a(Ljava/lang/String;LX/9lQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 2662005
    :cond_0
    return-void

    .line 2662006
    :pswitch_0
    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/JDe;

    .line 2662007
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;

    const/4 v3, 0x0

    .line 2662008
    iget-object v4, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2662009
    iput-boolean v3, v4, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->b:Z

    .line 2662010
    iget-object v4, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2662011
    iput-boolean v3, v4, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->d:Z

    .line 2662012
    iget-object v4, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    const/4 v3, 0x1

    .line 2662013
    :cond_1
    iput-boolean v3, v4, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->c:Z

    .line 2662014
    iget-object v3, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-virtual {v3, v1}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;)V

    .line 2662015
    move-object v0, v2

    .line 2662016
    goto :goto_1

    .line 2662017
    :pswitch_1
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;

    .line 2662018
    invoke-interface {v1}, LX/JAb;->mV_()Ljava/lang/String;

    move-result-object v3

    .line 2662019
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;

    .line 2662020
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2662021
    sget-object v0, LX/JCG;->SECTION_COLLECTION_HEADER:LX/JCG;

    if-ne v4, v0, :cond_2

    .line 2662022
    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/JDe;

    .line 2662023
    const/4 v5, 0x0

    .line 2662024
    iget-object v4, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2662025
    iput-boolean v5, v4, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->b:Z

    .line 2662026
    iget-object v6, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-interface {v1}, LX/JAb;->mU_()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    .line 2662027
    :goto_2
    iput-boolean v4, v6, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->d:Z

    .line 2662028
    iget-object v4, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2662029
    iput-boolean v5, v4, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->c:Z

    .line 2662030
    iget-object v4, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-virtual {v4, v1}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;)V

    .line 2662031
    move-object v0, v3

    .line 2662032
    goto :goto_1

    .line 2662033
    :cond_2
    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/JDe;

    .line 2662034
    iget-object v4, p0, LX/JCP;->m:LX/9lP;

    const/4 v6, 0x0

    .line 2662035
    iget-object v5, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-virtual {v4}, LX/9lP;->f()Z

    move-result v7

    .line 2662036
    iput-boolean v7, v5, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->b:Z

    .line 2662037
    iget-object v7, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-interface {v1}, LX/JAb;->j()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_9

    const/4 v5, 0x1

    .line 2662038
    :goto_3
    iput-boolean v5, v7, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->d:Z

    .line 2662039
    iget-object v5, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2662040
    iput-boolean v6, v5, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->c:Z

    .line 2662041
    iget-object v5, v0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-virtual {v5, v1}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->b(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;)V

    .line 2662042
    move-object v0, v3

    .line 2662043
    goto/16 :goto_1

    .line 2662044
    :pswitch_2
    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    .line 2662045
    instance-of v3, v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    if-eqz v3, :cond_3

    .line 2662046
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    move-object v0, v2

    goto/16 :goto_1

    .line 2662047
    :cond_3
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    iget-object v3, p0, LX/JCP;->m:LX/9lP;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v0, v1, v3, v4}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V

    move-object v0, v2

    .line 2662048
    goto/16 :goto_1

    .line 2662049
    :pswitch_3
    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    .line 2662050
    instance-of v3, v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    if-eqz v3, :cond_4

    .line 2662051
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    move-object v0, v2

    goto/16 :goto_1

    .line 2662052
    :cond_4
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    iget-object v3, p0, LX/JCP;->m:LX/9lP;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->CONTACT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v0, v1, v3, v4}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V

    goto/16 :goto_0

    .line 2662053
    :cond_5
    sget-object v3, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v3, v3

    if-lt v0, v3, :cond_6

    .line 2662054
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2662055
    sget-object v3, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v4, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v4, v4

    sub-int/2addr v0, v4

    aget-object v0, v3, v0

    invoke-direct {p0, p1, v1, v0}, LX/JCP;->a(Landroid/view/View;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;)V

    move-object v0, v2

    .line 2662056
    goto/16 :goto_1

    .line 2662057
    :cond_6
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;

    .line 2662058
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->mV_()Ljava/lang/String;

    move-result-object v8

    .line 2662059
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->p()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->p()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2662060
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->p()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->j()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 2662061
    :goto_4
    iget-object v0, p0, LX/JCP;->k:LX/JDA;

    iget-object v3, p0, LX/JCP;->m:LX/9lP;

    iget-object v4, p0, LX/JCP;->v:LX/J9F;

    iget-object v5, p0, LX/JCP;->p:LX/JCc;

    iget-object v6, p0, LX/JCP;->w:Ljava/lang/String;

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, LX/JDA;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;Landroid/view/View;LX/9lP;LX/J9F;LX/JCc;Ljava/lang/String;)V

    move-object v2, v7

    move-object v0, v8

    goto/16 :goto_1

    :cond_7
    move-object v7, v2

    goto :goto_4

    :cond_8
    move v4, v5

    .line 2662062
    goto/16 :goto_2

    :cond_9
    move v5, v6

    .line 2662063
    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2661991
    iget-boolean v0, p0, LX/JCP;->q:Z

    if-eq v0, p1, :cond_0

    .line 2661992
    iput-boolean p1, p0, LX/JCP;->q:Z

    .line 2661993
    const v0, 0x4420bf6b

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2661994
    :cond_0
    return-void
.end method

.method public final b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 2661973
    if-nez p1, :cond_0

    .line 2661974
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/3tK;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 2661975
    :goto_0
    return-object v0

    .line 2661976
    :cond_0
    iget-object v0, p0, LX/JCP;->w:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    move-object v0, p1

    .line 2661977
    check-cast v0, LX/2nf;

    .line 2661978
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/2nf;->moveToPosition(I)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2661979
    invoke-static {p0, v0}, LX/JCP;->a(LX/JCP;LX/2nf;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    .line 2661980
    instance-of v1, v0, LX/JA3;

    if-eqz v1, :cond_2

    .line 2661981
    check-cast v0, LX/JA3;

    .line 2661982
    invoke-interface {v0}, LX/JA3;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/JCP;->w:Ljava/lang/String;

    .line 2661983
    invoke-interface {v0}, LX/JA3;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2661984
    iget-object v1, p0, LX/JCP;->v:LX/J9F;

    invoke-interface {v0}, LX/JA3;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;->a()I

    move-result v2

    .line 2661985
    iput v2, v1, LX/J9F;->a:I

    .line 2661986
    iget-object v1, p0, LX/JCP;->v:LX/J9F;

    invoke-interface {v0}, LX/JA3;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;->b()LX/0Px;

    move-result-object v0

    .line 2661987
    iput-object v0, v1, LX/J9F;->b:Ljava/util/List;

    .line 2661988
    :cond_1
    :goto_1
    invoke-super {p0, p1}, LX/3tK;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 2661989
    :cond_2
    const-string v0, ""

    iput-object v0, p0, LX/JCP;->w:Ljava/lang/String;

    .line 2661990
    const-class v0, LX/JCP;

    const-string v1, "Got unexpected first row."

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2661967
    iget-object v0, p0, LX/JCP;->w:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2661968
    const/4 v0, 0x0

    .line 2661969
    :cond_0
    :goto_0
    return v0

    .line 2661970
    :cond_1
    invoke-super {p0}, LX/3tK;->getCount()I

    move-result v0

    .line 2661971
    iget-boolean v1, p0, LX/JCP;->q:Z

    if-eqz v1, :cond_0

    .line 2661972
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 5

    .prologue
    .line 2661920
    invoke-virtual {p0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, LX/2nf;

    .line 2661921
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2nf;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 2661922
    :cond_0
    sget-object v0, LX/JCO;->FULL_CARD:LX/JCO;

    sget v1, LX/JCP;->B:I

    invoke-static {v0, v1}, LX/JEz;->a(LX/JCO;I)I

    move-result v0

    .line 2661923
    :goto_0
    return v0

    .line 2661924
    :cond_1
    invoke-interface {v0, p1}, LX/2nf;->moveToPosition(I)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2661925
    invoke-interface {v0}, LX/2nf;->d()I

    move-result v0

    .line 2661926
    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 2661927
    sget-object v1, LX/JCO;->FIRST:LX/JCO;

    .line 2661928
    :goto_1
    move-object v0, v1

    .line 2661929
    const/4 v3, 0x0

    .line 2661930
    if-nez p1, :cond_5

    .line 2661931
    sget-object v1, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v1, v1

    sget-object v2, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, LX/JCG;->INVISIBLE_PROFILE_DATA:LX/JCG;

    invoke-virtual {v2}, LX/JCG;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    .line 2661932
    :goto_2
    move v1, v1

    .line 2661933
    invoke-static {v0, v1}, LX/JEz;->a(LX/JCO;I)I

    move-result v0

    goto :goto_0

    .line 2661934
    :cond_2
    and-int/lit8 v1, v0, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 2661935
    sget-object v1, LX/JCO;->MIDDLE:LX/JCO;

    goto :goto_1

    .line 2661936
    :cond_3
    and-int/lit8 v1, v0, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2661937
    sget-object v1, LX/JCO;->LAST:LX/JCO;

    goto :goto_1

    .line 2661938
    :cond_4
    sget-object v1, LX/JCO;->FULL_CARD:LX/JCO;

    goto :goto_1

    .line 2661939
    :cond_5
    invoke-virtual {p0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v1

    check-cast v1, LX/2nf;

    .line 2661940
    iget-boolean v2, p0, LX/JCP;->q:Z

    if-eqz v2, :cond_6

    invoke-interface {v1}, LX/2nf;->getCount()I

    move-result v2

    if-lt p1, v2, :cond_6

    .line 2661941
    sget v1, LX/JCP;->B:I

    goto :goto_2

    .line 2661942
    :cond_6
    invoke-interface {v1, p1}, LX/2nf;->moveToPosition(I)Z

    .line 2661943
    invoke-static {p0, v1}, LX/JCP;->a(LX/JCP;LX/2nf;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v2

    .line 2661944
    instance-of v4, v2, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    if-eqz v4, :cond_7

    move-object v1, v2

    .line 2661945
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2661946
    sget-object v2, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v2, v2

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->ordinal()I

    move-result v1

    add-int/2addr v1, v2

    goto :goto_2

    .line 2661947
    :cond_7
    instance-of v4, v2, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    if-nez v4, :cond_8

    instance-of v4, v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    if-eqz v4, :cond_b

    .line 2661948
    :cond_8
    invoke-interface {v1}, LX/2nf;->d()I

    move-result v1

    .line 2661949
    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_9

    const/4 v1, 0x1

    .line 2661950
    :goto_3
    if-eqz v1, :cond_a

    .line 2661951
    sget-object v1, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v1, v1

    sget-object v2, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, LX/JCG;->ABOUT_LIST_ITEM_MIDDLE:LX/JCG;

    invoke-virtual {v2}, LX/JCG;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_2

    :cond_9
    move v1, v3

    .line 2661952
    goto :goto_3

    .line 2661953
    :cond_a
    sget-object v1, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v1, v1

    sget-object v2, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, LX/JCG;->LIST_ITEM_MIDDLE:LX/JCG;

    invoke-virtual {v2}, LX/JCG;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    goto/16 :goto_2

    .line 2661954
    :cond_b
    instance-of v1, v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;

    if-eqz v1, :cond_c

    .line 2661955
    sget-object v1, LX/JCN;->a:[I

    invoke-virtual {v0}, LX/JCO;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 2661956
    :pswitch_0
    check-cast v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;

    .line 2661957
    const/4 v4, 0x0

    .line 2661958
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 2661959
    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->d()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v1

    .line 2661960
    :goto_4
    move-object v1, v1

    .line 2661961
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ordinal()I

    move-result v1

    goto/16 :goto_2

    .line 2661962
    :pswitch_1
    sget-object v1, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v1, v1

    sget-object v2, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, LX/JCG;->SECTION_COLLECTION_HEADER:LX/JCG;

    invoke-virtual {v2}, LX/JCG;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    goto/16 :goto_2

    .line 2661963
    :pswitch_2
    sget-object v1, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v1, v1

    sget-object v2, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, LX/JCG;->ABOUT_MORE_ABOUT:LX/JCG;

    invoke-virtual {v2}, LX/JCG;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    goto/16 :goto_2

    .line 2661964
    :cond_c
    instance-of v1, v2, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;

    if-eqz v1, :cond_d

    .line 2661965
    sget-object v1, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v1, v1

    sget-object v2, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, LX/JCG;->PROFILE_FIELD_SECTION_HEADER:LX/JCG;

    invoke-virtual {v2}, LX/JCG;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    goto/16 :goto_2

    .line 2661966
    :cond_d
    sget-object v1, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v1, v1

    sget-object v2, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, LX/JCG;->UNKNOWN_TYPE:LX/JCG;

    invoke-virtual {v2}, LX/JCG;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    goto/16 :goto_2

    :cond_e
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2661906
    invoke-virtual {p0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, LX/2nf;

    .line 2661907
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2nf;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 2661908
    :cond_0
    invoke-virtual {p0, p1}, LX/JCP;->getItemViewType(I)I

    move-result v0

    .line 2661909
    invoke-static {v0}, LX/JEz;->b(I)I

    move-result v1

    .line 2661910
    invoke-static {v0}, LX/JEz;->a(I)LX/JCO;

    move-result-object v2

    .line 2661911
    sget v0, LX/JCP;->B:I

    if-ne v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2661912
    iget-object v0, p0, LX/3tK;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2661913
    invoke-static {v0, p3}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2661914
    invoke-direct {p0, v0, v1, v2}, LX/JCP;->a(Landroid/view/LayoutInflater;Landroid/view/View;LX/JCO;)Landroid/view/View;

    move-result-object v0

    .line 2661915
    :goto_1
    return-object v0

    .line 2661916
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2661917
    :cond_2
    invoke-super {p0, p1, p2, p3}, LX/3tK;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2661918
    sget-object v0, LX/JCP;->x:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    array-length v0, v0

    sget-object p0, LX/JCP;->y:[Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    array-length p0, p0

    add-int/2addr v0, p0

    sget-object p0, LX/JCP;->z:[LX/JCG;

    array-length p0, p0

    add-int/2addr v0, p0

    sget-object p0, LX/JCP;->A:[LX/JCO;

    array-length p0, p0

    mul-int/2addr v0, p0

    move v0, v0

    .line 2661919
    return v0
.end method
