.class public LX/HMS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HMI;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0kL;

.field private final c:Landroid/content/res/Resources;

.field public final d:LX/9XE;

.field private final e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/HDT;

.field private final h:LX/BNL;

.field public final i:LX/BNE;

.field public final j:LX/Ch5;

.field private final k:LX/0SI;

.field private final l:LX/01T;

.field public m:J

.field private n:Ljava/lang/String;

.field private o:I

.field public p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0kL;Landroid/content/res/Resources;LX/9XE;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0Or;LX/HDT;LX/BNL;LX/BNE;LX/Ch5;LX/0SI;LX/01T;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0kL;",
            "Landroid/content/res/Resources;",
            "LX/9XE;",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/HDT;",
            "LX/BNL;",
            "LX/BNE;",
            "LX/Ch5;",
            "LX/0SI;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2457145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457146
    iput-object p1, p0, LX/HMS;->a:Landroid/content/Context;

    .line 2457147
    iput-object p2, p0, LX/HMS;->b:LX/0kL;

    .line 2457148
    iput-object p3, p0, LX/HMS;->c:Landroid/content/res/Resources;

    .line 2457149
    iput-object p4, p0, LX/HMS;->d:LX/9XE;

    .line 2457150
    iput-object p5, p0, LX/HMS;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2457151
    iput-object p6, p0, LX/HMS;->f:LX/0Or;

    .line 2457152
    iput-object p7, p0, LX/HMS;->g:LX/HDT;

    .line 2457153
    iput-object p8, p0, LX/HMS;->h:LX/BNL;

    .line 2457154
    iput-object p9, p0, LX/HMS;->i:LX/BNE;

    .line 2457155
    iput-object p10, p0, LX/HMS;->j:LX/Ch5;

    .line 2457156
    iput-object p11, p0, LX/HMS;->k:LX/0SI;

    .line 2457157
    iput-object p12, p0, LX/HMS;->l:LX/01T;

    .line 2457158
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2457142
    iget-object v0, p0, LX/HMS;->d:LX/9XE;

    iget-wide v2, p0, LX/HMS;->m:J

    invoke-virtual {v0, v1, v2, v3, v1}, LX/9XE;->a(ZJZ)V

    .line 2457143
    iget-object v0, p0, LX/HMS;->b:LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2457144
    return-void
.end method

.method public static b(LX/0QB;)LX/HMS;
    .locals 13

    .prologue
    .line 2457140
    new-instance v0, LX/HMS;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {p0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v4

    check-cast v4, LX/9XE;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    const/16 v6, 0x12cb

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v7

    check-cast v7, LX/HDT;

    invoke-static {p0}, LX/BNL;->a(LX/0QB;)LX/BNL;

    move-result-object v8

    check-cast v8, LX/BNL;

    invoke-static {p0}, LX/BNE;->a(LX/0QB;)LX/BNE;

    move-result-object v9

    check-cast v9, LX/BNE;

    invoke-static {p0}, LX/Ch5;->a(LX/0QB;)LX/Ch5;

    move-result-object v10

    check-cast v10, LX/Ch5;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v11

    check-cast v11, LX/0SI;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v12

    check-cast v12, LX/01T;

    invoke-direct/range {v0 .. v12}, LX/HMS;-><init>(Landroid/content/Context;LX/0kL;Landroid/content/res/Resources;LX/9XE;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0Or;LX/HDT;LX/BNL;LX/BNE;LX/Ch5;LX/0SI;LX/01T;)V

    .line 2457141
    return-object v0
.end method


# virtual methods
.method public final a()LX/4At;
    .locals 4

    .prologue
    .line 2457109
    new-instance v0, LX/4At;

    iget-object v1, p0, LX/HMS;->a:Landroid/content/Context;

    iget-object v2, p0, LX/HMS;->c:Landroid/content/res/Resources;

    const v3, 0x7f0814e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p3    # LX/8A4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/8A4;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "Landroid/content/Intent;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457132
    iput-wide p1, p0, LX/HMS;->m:J

    .line 2457133
    const-string v0, "publishReviewParams"

    invoke-virtual {p5, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/protocol/PostReviewParams;

    .line 2457134
    iget v1, v0, Lcom/facebook/composer/protocol/PostReviewParams;->e:I

    iput v1, p0, LX/HMS;->o:I

    .line 2457135
    iget-object v1, v0, Lcom/facebook/composer/protocol/PostReviewParams;->c:Ljava/lang/String;

    iput-object v1, p0, LX/HMS;->n:Ljava/lang/String;

    .line 2457136
    const/16 v1, 0x277c

    if-ne p6, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, LX/HMS;->p:Z

    .line 2457137
    iget-object v2, p0, LX/HMS;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, p0, LX/HMS;->l:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-eq v1, v3, :cond_1

    iget-object v1, p0, LX/HMS;->k:LX/0SI;

    invoke-interface {v1}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v0, v1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/composer/protocol/PostReviewParams;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2457138
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 2457139
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 9

    .prologue
    .line 2457118
    :try_start_0
    iget-boolean v1, p0, LX/HMS;->p:Z

    if-eqz v1, :cond_0

    .line 2457119
    iget-object v1, p0, LX/HMS;->d:LX/9XE;

    sget-object v2, LX/9XB;->EVENT_PLACE_EDIT_REVIEW_SUCCESS:LX/9XB;

    iget-wide v3, p0, LX/HMS;->m:J

    invoke-virtual {v1, v2, v3, v4}, LX/9XE;->a(LX/9X2;J)V

    .line 2457120
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5tj;

    .line 2457121
    iget-object v2, p0, LX/HMS;->g:LX/HDT;

    new-instance v3, LX/HDk;

    invoke-direct {v3}, LX/HDk;-><init>()V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2457122
    iget-object v2, p0, LX/HMS;->g:LX/HDT;

    .line 2457123
    new-instance v3, LX/HDo;

    new-instance v4, LX/HDm;

    invoke-direct {v4}, LX/HDm;-><init>()V

    invoke-direct {v3, v4}, LX/HDo;-><init>(LX/HDY;)V

    move-object v3, v3

    .line 2457124
    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2457125
    iget-object v2, p0, LX/HMS;->j:LX/Ch5;

    iget-wide v3, p0, LX/HMS;->m:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, LX/ChH;->a(Ljava/lang/String;LX/5tj;)LX/ChF;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2457126
    iget-object v1, p0, LX/HMS;->b:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f0814e5

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2457127
    iget-wide v6, p0, LX/HMS;->m:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 2457128
    iget-object v7, p0, LX/HMS;->i:LX/BNE;

    new-instance v8, LX/HMR;

    invoke-direct {v8, p0, v6}, LX/HMR;-><init>(LX/HMS;Ljava/lang/String;)V

    invoke-virtual {v7, v6, v8}, LX/BNE;->a(Ljava/lang/String;LX/BNC;)V
    :try_end_0
    .catch LX/4BK; {:try_start_0 .. :try_end_0} :catch_0

    .line 2457129
    :goto_1
    return-void

    .line 2457130
    :catch_0
    iget-object v0, p0, LX/HMS;->h:LX/BNL;

    invoke-virtual {v0}, LX/BNL;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/HMS;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 2457131
    :cond_0
    iget-object v1, p0, LX/HMS;->d:LX/9XE;

    const/4 v2, 0x1

    iget-wide v3, p0, LX/HMS;->m:J

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, LX/9XE;->a(ZJZ)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2457113
    iget-object v0, p0, LX/HMS;->h:LX/BNL;

    invoke-virtual {v0, p1}, LX/BNL;->a(Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/HMS;->a(Ljava/lang/String;)V

    .line 2457114
    iget-object v0, p0, LX/HMS;->g:LX/HDT;

    .line 2457115
    new-instance v1, LX/HDo;

    new-instance p0, LX/HDn;

    invoke-direct {p0}, LX/HDn;-><init>()V

    invoke-direct {v1, p0}, LX/HDo;-><init>(LX/HDY;)V

    move-object v1, v1

    .line 2457116
    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2457117
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2457112
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2457111
    const/4 v0, 0x1

    return v0
.end method

.method public final d()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457110
    const/16 v0, 0x6df

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x277c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x277b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, LX/Cfc;->WRITE_REVIEW_TAP:LX/Cfc;

    invoke-virtual {v3}, LX/Cfc;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
