.class public LX/HNP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field private final f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2458421
    const v0, 0x7f0209c5

    sput v0, LX/HNP;->a:I

    .line 2458422
    const v0, 0x7f08367a

    sput v0, LX/HNP;->b:I

    .line 2458423
    const v0, 0x7f083678

    sput v0, LX/HNP;->c:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458417
    iput-object p1, p0, LX/HNP;->d:LX/0Ot;

    .line 2458418
    iput-object p2, p0, LX/HNP;->e:Landroid/content/Context;

    .line 2458419
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/HNP;->f:Z

    .line 2458420
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2458424
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->SHARE_TAB:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    iget-boolean v2, p0, LX/HNP;->f:Z

    if-eqz v2, :cond_0

    sget v2, LX/HNP;->c:I

    :goto_0
    sget v3, LX/HNP;->a:I

    iget-object v5, p0, LX/HNP;->g:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    move v5, v4

    :goto_1
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    sget v2, LX/HNP;->b:I

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2458415
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->SHARE_TAB:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    iget-boolean v2, p0, LX/HNP;->f:Z

    if-eqz v2, :cond_0

    sget v2, LX/HNP;->c:I

    :goto_0
    sget v3, LX/HNP;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    sget v2, LX/HNP;->b:I

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2458413
    iget-object v0, p0, LX/HNP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v1, 0x0

    sget-object v2, LX/21D;->PAGE_FEED:LX/21D;

    const-string v3, "SharePageTabLink"

    iget-object v4, p0, LX/HNP;->g:Ljava/lang/String;

    invoke-static {v4}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v4

    invoke-virtual {v4}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v3, p0, LX/HNP;->e:Landroid/content/Context;

    invoke-interface {v0, v1, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2458414
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2458412
    const/4 v0, 0x0

    return-object v0
.end method
