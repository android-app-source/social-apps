.class public LX/IDu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BWd",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/17W;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/17W;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17W;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2551291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2551292
    iput-object p1, p0, LX/IDu;->a:LX/17W;

    .line 2551293
    iput-object p2, p0, LX/IDu;->b:LX/0Ot;

    .line 2551294
    return-void
.end method

.method public static b(LX/0QB;)LX/IDu;
    .locals 3

    .prologue
    .line 2551289
    new-instance v1, LX/IDu;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v0

    check-cast v0, LX/17W;

    const/16 v2, 0x97

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/IDu;-><init>(LX/17W;LX/0Ot;)V

    .line 2551290
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2551274
    instance-of v0, p2, LX/IDH;

    if-nez v0, :cond_0

    .line 2551275
    :goto_0
    return-void

    .line 2551276
    :cond_0
    check-cast p2, LX/IDH;

    .line 2551277
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {p2}, LX/IDH;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2551278
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2551279
    invoke-virtual {p2}, LX/IDH;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, LX/IDH;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, LX/IDH;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v3, v4}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2551280
    const-string v0, "timeline_friend_request_ref"

    sget-object v3, LX/5P2;->FRIENDS_TAB:LX/5P2;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2551281
    const-string v3, "timeline_has_unseen_section"

    .line 2551282
    iget v0, p2, LX/IDH;->j:I

    move v0, v0

    .line 2551283
    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2551284
    iget-object v0, p0, LX/IDu;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v3, "tap_friendlist_item"

    invoke-virtual {v0, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2551285
    iget-object v0, p0, LX/IDu;->a:LX/17W;

    invoke-virtual {v0, p1, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 2551286
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2551288
    const/4 v0, 0x0

    return v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 2551287
    const/4 v0, 0x1

    return v0
.end method
