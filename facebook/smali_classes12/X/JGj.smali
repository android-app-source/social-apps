.class public final LX/JGj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5rT;


# instance fields
.field public final synthetic a:LX/JGq;


# direct methods
.method public constructor <init>(LX/JGq;)V
    .locals 0

    .prologue
    .line 2668994
    iput-object p1, p0, LX/JGj;->a:LX/JGq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 2668995
    const/4 v0, 0x0

    sget-object v1, LX/JGs;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-eq v1, v2, :cond_2

    .line 2668996
    sget-object v0, LX/JGs;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGs;

    .line 2668997
    const/high16 p0, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 2668998
    iput-boolean v3, v0, LX/JGs;->o:Z

    .line 2668999
    invoke-virtual {v0}, LX/JGs;->getChildCount()I

    move-result v4

    :goto_1
    if-eq v3, v4, :cond_1

    .line 2669000
    invoke-virtual {v0, v3}, LX/JGs;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2669001
    invoke-virtual {v5}, Landroid/view/View;->isLayoutRequested()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2669002
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-static {v6, p0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v7

    invoke-static {v7, p0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/view/View;->measure(II)V

    .line 2669003
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v8

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 2669004
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2669005
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2669006
    :cond_2
    sget-object v0, LX/JGs;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2669007
    return-void
.end method
