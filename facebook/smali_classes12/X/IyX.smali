.class public LX/IyX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ixz;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/1Ck;

.field private final b:LX/IyZ;

.field private final c:LX/Iyd;

.field public final d:LX/IyH;

.field public e:Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;


# direct methods
.method public constructor <init>(LX/1Ck;LX/IyZ;LX/Iyd;LX/IyH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2633541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633542
    iput-object p1, p0, LX/IyX;->a:LX/1Ck;

    .line 2633543
    iput-object p2, p0, LX/IyX;->b:LX/IyZ;

    .line 2633544
    iput-object p3, p0, LX/IyX;->c:LX/Iyd;

    .line 2633545
    iput-object p4, p0, LX/IyX;->d:LX/IyH;

    .line 2633546
    return-void
.end method

.method public static a(LX/0QB;)LX/IyX;
    .locals 7

    .prologue
    .line 2633521
    const-class v1, LX/IyX;

    monitor-enter v1

    .line 2633522
    :try_start_0
    sget-object v0, LX/IyX;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2633523
    sput-object v2, LX/IyX;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2633524
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2633525
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2633526
    new-instance p0, LX/IyX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/IyZ;->b(LX/0QB;)LX/IyZ;

    move-result-object v4

    check-cast v4, LX/IyZ;

    invoke-static {v0}, LX/Iyd;->b(LX/0QB;)LX/Iyd;

    move-result-object v5

    check-cast v5, LX/Iyd;

    invoke-static {v0}, LX/IyH;->a(LX/0QB;)LX/IyH;

    move-result-object v6

    check-cast v6, LX/IyH;

    invoke-direct {p0, v3, v4, v5, v6}, LX/IyX;-><init>(LX/1Ck;LX/IyZ;LX/Iyd;LX/IyH;)V

    .line 2633527
    move-object v0, p0

    .line 2633528
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2633529
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IyX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2633530
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2633531
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/IyX;)V
    .locals 4

    .prologue
    .line 2633547
    iget-object v0, p0, LX/IyX;->e:Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    iget-object v0, v0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->a:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    if-eqz v0, :cond_1

    .line 2633548
    iget-object v0, p0, LX/IyX;->d:LX/IyH;

    iget-object v1, p0, LX/IyX;->e:Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    iget-object v1, v1, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->a:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2633549
    iget-object v2, v0, LX/IyH;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Iy3;

    .line 2633550
    invoke-virtual {v2, v1}, LX/Iy3;->a(Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;)V

    goto :goto_0

    .line 2633551
    :cond_0
    :goto_1
    return-void

    .line 2633552
    :cond_1
    iget-object v0, p0, LX/IyX;->e:Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    iget-object v0, v0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2633553
    iget-object v0, p0, LX/IyX;->d:LX/IyH;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/IyX;->e:Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    iget-object v2, v2, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 2633554
    iget-object v2, v0, LX/IyH;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Iy3;

    .line 2633555
    iget-object p0, v2, LX/Iy3;->a:Lcom/facebook/payments/cart/PaymentsCartFragment;

    .line 2633556
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->n:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2633557
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2633558
    if-eqz v0, :cond_2

    .line 2633559
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2633560
    :cond_2
    goto :goto_2

    .line 2633561
    :cond_3
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/Iy3;)V
    .locals 1

    .prologue
    .line 2633532
    iget-object v0, p0, LX/IyX;->d:LX/IyH;

    invoke-virtual {v0, p1}, LX/IyH;->a(LX/Iy3;)V

    .line 2633533
    return-void
.end method

.method public final a(Lcom/facebook/payments/cart/model/PaymentsCartParams;)V
    .locals 6

    .prologue
    .line 2633534
    iget-object v0, p0, LX/IyX;->e:Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    if-eqz v0, :cond_1

    .line 2633535
    invoke-static {p0}, LX/IyX;->b(LX/IyX;)V

    .line 2633536
    :cond_0
    :goto_0
    return-void

    .line 2633537
    :cond_1
    iget-object v0, p0, LX/IyX;->a:LX/1Ck;

    const-string v1, "fetch_config_task_key"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2633538
    new-instance v1, LX/IyV;

    invoke-direct {v1, p0}, LX/IyV;-><init>(LX/IyX;)V

    .line 2633539
    new-instance v2, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;

    iget-object v0, p1, Lcom/facebook/payments/cart/model/PaymentsCartParams;->c:Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

    check-cast v0, Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;

    iget-wide v4, v0, Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;->a:J

    iget-object v0, p1, Lcom/facebook/payments/cart/model/PaymentsCartParams;->a:LX/6xg;

    invoke-virtual {v0}, LX/6xg;->toPaymentModulesClient()LX/6xh;

    move-result-object v0

    invoke-direct {v2, v4, v5, v0}, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;-><init>(JLX/6xh;)V

    .line 2633540
    iget-object v0, p0, LX/IyX;->a:LX/1Ck;

    const-string v3, "fetch_config_task_key"

    iget-object v4, p0, LX/IyX;->b:LX/IyZ;

    invoke-virtual {v4, v2}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-virtual {v0, v3, v2, v1}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/cart/model/PaymentsCartParams;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2633515
    new-instance v0, LX/IyW;

    invoke-direct {v0, p0}, LX/IyW;-><init>(LX/IyX;)V

    .line 2633516
    new-instance v1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;

    iget-object v2, p1, Lcom/facebook/payments/cart/model/PaymentsCartParams;->a:LX/6xg;

    invoke-virtual {v2}, LX/6xg;->toPaymentModulesClient()LX/6xh;

    move-result-object v2

    invoke-direct {v1, p2, v2}, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;-><init>(Ljava/lang/String;LX/6xh;)V

    .line 2633517
    iget-object v2, p0, LX/IyX;->a:LX/1Ck;

    iget-object v3, p0, LX/IyX;->c:LX/Iyd;

    invoke-virtual {v3, v1}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-virtual {v2, p2, v1, v0}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2633518
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2633514
    iget-object v0, p0, LX/IyX;->a:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    return v0
.end method

.method public final b(LX/Iy3;)V
    .locals 1

    .prologue
    .line 2633519
    iget-object v0, p0, LX/IyX;->d:LX/IyH;

    invoke-virtual {v0, p1}, LX/IyH;->b(LX/Iy3;)V

    .line 2633520
    return-void
.end method
