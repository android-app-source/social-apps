.class public final LX/IIG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 0

    .prologue
    .line 2561749
    iput-object p1, p0, LX/IIG;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 2561742
    iget-object v0, p0, LX/IIG;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2561743
    iget-boolean v1, v0, LX/IID;->e:Z

    if-eqz v1, :cond_0

    .line 2561744
    :goto_0
    iget-object v0, p0, LX/IIG;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0, p1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/CharSequence;)V

    .line 2561745
    return-void

    .line 2561746
    :cond_0
    const-string v1, "friends_nearby_dashboard_search_type_during_session"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2561747
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561748
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/IID;->e:Z

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2561750
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2561741
    return-void
.end method
