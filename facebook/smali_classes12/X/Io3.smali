.class public final LX/Io3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 0

    .prologue
    .line 2611243
    iput-object p1, p0, LX/Io3;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2611244
    iget-object v0, p0, LX/Io3;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->b:LX/03V;

    const-string v1, "EnterPaymentValueFragment"

    const-string v2, "Failed to fetch sender\'s info needed for sending money."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2611245
    iget-object v0, p0, LX/Io3;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611246
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2611247
    if-eqz v0, :cond_0

    .line 2611248
    iget-object v0, p0, LX/Io3;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->m$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611249
    :cond_0
    iget-object v0, p0, LX/Io3;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->h:LX/2MA;

    sget-object v1, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v0, v1}, LX/2MA;->b(LX/8Bk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2611250
    iget-object v0, p0, LX/Io3;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F:LX/Ini;

    invoke-virtual {v0}, LX/Ini;->a()V

    .line 2611251
    :cond_1
    return-void
.end method
