.class public final LX/Iu6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;)V
    .locals 0

    .prologue
    .line 2625279
    iput-object p1, p0, LX/Iu6;->a:Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x502d4719

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2625280
    const-string v0, "EncryptedPhotoUploadStatusKey"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;

    .line 2625281
    iget-object v2, p0, LX/Iu6;->a:Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;

    .line 2625282
    iget-object p0, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->c:LX/FGR;

    sget-object p1, LX/FGR;->Success:LX/FGR;

    if-ne p0, p1, :cond_1

    .line 2625283
    iget-object p0, v2, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->c:LX/Iu9;

    iget-object p1, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a:Ljava/lang/String;

    iget-object p2, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->b:Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, LX/Iu9;->a(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2625284
    invoke-static {v2, v0}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->b(Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;)V

    .line 2625285
    :cond_0
    :goto_0
    const/16 v0, 0x27

    const v2, 0x38ae3de3

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2625286
    :cond_1
    iget-object p0, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->c:LX/FGR;

    sget-object p1, LX/FGR;->Failure:LX/FGR;

    if-ne p0, p1, :cond_2

    const/4 p0, 0x1

    :goto_1
    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 2625287
    iget-object p0, v2, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->c:LX/Iu9;

    iget-object p1, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a:Ljava/lang/String;

    iget-object p2, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->b:Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, LX/Iu9;->b(Ljava/lang/String;Landroid/net/Uri;)Z

    move-result p0

    .line 2625288
    if-nez p0, :cond_0

    .line 2625289
    invoke-static {v2, v0}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->b(Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;)V

    goto :goto_0

    .line 2625290
    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method
