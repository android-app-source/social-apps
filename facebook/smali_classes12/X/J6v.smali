.class public LX/J6v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J6v;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2650389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650390
    iput-object p1, p0, LX/J6v;->a:LX/0Ot;

    .line 2650391
    return-void
.end method

.method public static a(LX/0QB;)LX/J6v;
    .locals 4

    .prologue
    .line 2650392
    sget-object v0, LX/J6v;->b:LX/J6v;

    if-nez v0, :cond_1

    .line 2650393
    const-class v1, LX/J6v;

    monitor-enter v1

    .line 2650394
    :try_start_0
    sget-object v0, LX/J6v;->b:LX/J6v;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2650395
    if-eqz v2, :cond_0

    .line 2650396
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2650397
    new-instance v3, LX/J6v;

    const/16 p0, 0x1b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/J6v;-><init>(LX/0Ot;)V

    .line 2650398
    move-object v0, v3

    .line 2650399
    sput-object v0, LX/J6v;->b:LX/J6v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2650400
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2650401
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2650402
    :cond_1
    sget-object v0, LX/J6v;->b:LX/J6v;

    return-object v0

    .line 2650403
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2650404
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
