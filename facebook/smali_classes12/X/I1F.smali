.class public final LX/I1F;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I16;

.field public final synthetic b:LX/I1G;


# direct methods
.method public constructor <init>(LX/I1G;LX/I16;)V
    .locals 0

    .prologue
    .line 2528795
    iput-object p1, p0, LX/I1F;->b:LX/I1G;

    iput-object p2, p0, LX/I1F;->a:LX/I16;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2528784
    iget-object v0, p0, LX/I1F;->a:LX/I16;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I16;->a(Ljava/util/List;)V

    .line 2528785
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2528786
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2528787
    if-eqz p1, :cond_0

    .line 2528788
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528789
    if-eqz v0, :cond_0

    .line 2528790
    iget-object v1, p0, LX/I1F;->a:LX/I16;

    .line 2528791
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528792
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/I16;->a(Ljava/util/List;)V

    .line 2528793
    :goto_0
    return-void

    .line 2528794
    :cond_0
    iget-object v0, p0, LX/I1F;->a:LX/I16;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I16;->a(Ljava/util/List;)V

    goto :goto_0
.end method
