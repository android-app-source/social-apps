.class public final LX/IRC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V
    .locals 0

    .prologue
    .line 2576202
    iput-object p1, p0, LX/IRC;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2576203
    iget-object v0, p0, LX/IRC;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/IRC;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081bc1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2576204
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2576205
    iget-object v0, p0, LX/IRC;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->y(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V

    .line 2576206
    iget-object v0, p0, LX/IRC;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2576207
    return-void
.end method
