.class public LX/HMW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HMI;


# instance fields
.field private final a:LX/0kL;

.field private final b:LX/9XE;


# direct methods
.method public constructor <init>(LX/0kL;LX/9XE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2457230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457231
    iput-object p1, p0, LX/HMW;->a:LX/0kL;

    .line 2457232
    iput-object p2, p0, LX/HMW;->b:LX/9XE;

    .line 2457233
    return-void
.end method


# virtual methods
.method public final a()LX/4At;
    .locals 1

    .prologue
    .line 2457229
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p3    # LX/8A4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/8A4;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "Landroid/content/Intent;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457226
    iget-object v0, p0, LX/HMW;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817d1

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2457227
    iget-object v0, p0, LX/HMW;->b:LX/9XE;

    sget-object v1, LX/9XB;->EVENT_SUGGEST_EDIT_SUCCESS:LX/9XB;

    invoke-virtual {v0, v1, p1, p2}, LX/9XE;->a(LX/9X2;J)V

    .line 2457228
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 0

    .prologue
    .line 2457225
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2457221
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2457224
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2457223
    const/4 v0, 0x1

    return v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457222
    const/16 v0, 0x2776

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
