.class public LX/I9f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field private b:LX/Blc;

.field public c:Lcom/facebook/events/common/EventActionContext;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/events/common/ActionMechanism;

.field public f:LX/1Ck;

.field public g:LX/0tX;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;Ljava/lang/String;LX/Blc;LX/0Px;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Blc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/events/common/EventActionContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/events/common/ActionMechanism;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/0tX;",
            "Ljava/lang/String;",
            "LX/Blc;",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;",
            "Lcom/facebook/events/common/EventActionContext;",
            "Lcom/facebook/events/common/ActionMechanism;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2543369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2543370
    iput-object p1, p0, LX/I9f;->f:LX/1Ck;

    .line 2543371
    iput-object p2, p0, LX/I9f;->g:LX/0tX;

    .line 2543372
    iput-object p3, p0, LX/I9f;->a:Ljava/lang/String;

    .line 2543373
    iput-object p4, p0, LX/I9f;->b:LX/Blc;

    .line 2543374
    iput-object p5, p0, LX/I9f;->d:LX/0Px;

    .line 2543375
    iput-object p6, p0, LX/I9f;->c:Lcom/facebook/events/common/EventActionContext;

    .line 2543376
    iput-object p7, p0, LX/I9f;->e:Lcom/facebook/events/common/ActionMechanism;

    .line 2543377
    return-void
.end method

.method public static a(LX/Blc;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/facebook/graphql/calls/EventGuestStatusMutationValue;
    .end annotation

    .prologue
    .line 2543363
    sget-object v0, LX/I9e;->a:[I

    invoke-virtual {p0}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2543364
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported guest status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2543365
    :pswitch_0
    const-string v0, "going"

    .line 2543366
    :goto_0
    return-object v0

    .line 2543367
    :pswitch_1
    const-string v0, "maybe"

    goto :goto_0

    .line 2543368
    :pswitch_2
    const-string v0, "not_going"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/I9f;LX/Blc;)Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;
    .locals 10
    .param p0    # LX/I9f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2543317
    iget-object v1, p0, LX/I9f;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    move v6, v0

    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    :goto_0
    if-ge v6, v7, :cond_4

    iget-object v0, p0, LX/I9f;->d:LX/0Px;

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    .line 2543318
    iget-object v5, v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    move-object v5, v5

    .line 2543319
    if-eqz v5, :cond_5

    .line 2543320
    iget-object v5, v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    move-object v5, v5

    .line 2543321
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2543322
    iget-object v8, v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    move-object v8, v8

    .line 2543323
    iget-object v0, p0, LX/I9f;->b:LX/Blc;

    if-ne v0, v8, :cond_6

    .line 2543324
    add-int/lit8 v0, v5, -0x1

    .line 2543325
    :goto_1
    if-ne p1, v8, :cond_0

    .line 2543326
    add-int/lit8 v0, v0, 0x1

    .line 2543327
    :cond_0
    sget-object v5, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    if-ne v8, v5, :cond_1

    move v9, v1

    move v1, v2

    move v2, v3

    move v3, v0

    move v0, v9

    .line 2543328
    :goto_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2543329
    :cond_1
    sget-object v5, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    if-ne v8, v5, :cond_2

    move v3, v4

    move v9, v2

    move v2, v0

    move v0, v1

    move v1, v9

    .line 2543330
    goto :goto_2

    .line 2543331
    :cond_2
    sget-object v5, LX/Blc;->PRIVATE_GOING:LX/Blc;

    if-ne v8, v5, :cond_3

    move v2, v3

    move v3, v4

    move v9, v1

    move v1, v0

    move v0, v9

    .line 2543332
    goto :goto_2

    .line 2543333
    :cond_3
    sget-object v5, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    if-ne v8, v5, :cond_5

    move v1, v2

    move v2, v3

    move v3, v4

    .line 2543334
    goto :goto_2

    .line 2543335
    :cond_4
    new-instance v0, LX/7uV;

    invoke-direct {v0}, LX/7uV;-><init>()V

    .line 2543336
    iput v4, v0, LX/7uV;->a:I

    .line 2543337
    move-object v0, v0

    .line 2543338
    invoke-virtual {v0}, LX/7uV;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    move-result-object v0

    .line 2543339
    new-instance v4, LX/7uW;

    invoke-direct {v4}, LX/7uW;-><init>()V

    .line 2543340
    iput v3, v4, LX/7uW;->a:I

    .line 2543341
    move-object v3, v4

    .line 2543342
    invoke-virtual {v3}, LX/7uW;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    move-result-object v3

    .line 2543343
    new-instance v4, LX/7uX;

    invoke-direct {v4}, LX/7uX;-><init>()V

    .line 2543344
    iput v2, v4, LX/7uX;->a:I

    .line 2543345
    move-object v2, v4

    .line 2543346
    invoke-virtual {v2}, LX/7uX;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    move-result-object v2

    .line 2543347
    new-instance v4, LX/7uU;

    invoke-direct {v4}, LX/7uU;-><init>()V

    .line 2543348
    iput v1, v4, LX/7uU;->a:I

    .line 2543349
    move-object v1, v4

    .line 2543350
    invoke-virtual {v1}, LX/7uU;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    move-result-object v1

    .line 2543351
    new-instance v4, LX/7uT;

    invoke-direct {v4}, LX/7uT;-><init>()V

    iget-object v5, p0, LX/I9f;->a:Ljava/lang/String;

    .line 2543352
    iput-object v5, v4, LX/7uT;->e:Ljava/lang/String;

    .line 2543353
    move-object v4, v4

    .line 2543354
    iput-object v0, v4, LX/7uT;->b:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    .line 2543355
    move-object v0, v4

    .line 2543356
    iput-object v3, v0, LX/7uT;->c:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    .line 2543357
    move-object v0, v0

    .line 2543358
    iput-object v2, v0, LX/7uT;->d:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    .line 2543359
    move-object v0, v0

    .line 2543360
    iput-object v1, v0, LX/7uT;->a:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    .line 2543361
    move-object v0, v0

    .line 2543362
    invoke-virtual {v0}, LX/7uT;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    move-result-object v0

    return-object v0

    :cond_5
    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    goto :goto_2

    :cond_6
    move v0, v5

    goto/16 :goto_1
.end method
