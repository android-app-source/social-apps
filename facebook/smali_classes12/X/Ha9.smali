.class public LX/Ha9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/registration/model/ContactPointSuggestions;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0dC;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0dC;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/registration/annotations/RegInstance;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2483409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483410
    iput-object p1, p0, LX/Ha9;->a:Ljava/lang/String;

    .line 2483411
    iput-object p2, p0, LX/Ha9;->b:LX/0dC;

    .line 2483412
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2483413
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2483414
    iget-object v0, p0, LX/Ha9;->b:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v0

    .line 2483415
    if-eqz v0, :cond_0

    .line 2483416
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2483417
    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "reg_instance"

    iget-object v4, p0, LX/Ha9;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483418
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "phone_id"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483419
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483420
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v2, LX/11I;->CONTACT_POINT_SUGGESTIONS:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 2483421
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2483422
    move-object v0, v0

    .line 2483423
    const-string v2, "GET"

    .line 2483424
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2483425
    move-object v0, v0

    .line 2483426
    const-string v2, "method/user.prefillorautocompletecontactpoint"

    .line 2483427
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 2483428
    move-object v0, v0

    .line 2483429
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    .line 2483430
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2483431
    move-object v0, v0

    .line 2483432
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2483433
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2483434
    move-object v0, v0

    .line 2483435
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2483436
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2483437
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/registration/model/ContactPointSuggestions;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/ContactPointSuggestions;

    return-object v0
.end method
