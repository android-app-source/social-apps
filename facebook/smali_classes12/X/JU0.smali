.class public final LX/JU0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JUH;

.field public final synthetic b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;LX/JUH;)V
    .locals 0

    .prologue
    .line 2697557
    iput-object p1, p0, LX/JU0;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iput-object p2, p0, LX/JU0;->a:LX/JUH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x2f07206d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697558
    iget-object v1, p0, LX/JU0;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v2, p0, LX/JU0;->a:LX/JUH;

    invoke-static {v1, p1, v2}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->a$redex0(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;Landroid/view/View;LX/JUH;)V

    .line 2697559
    iget-object v1, p0, LX/JU0;->a:LX/JUH;

    iget-object v1, v1, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2697560
    iget-object v1, p0, LX/JU0;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->d:LX/JUO;

    iget-object v2, p0, LX/JU0;->a:LX/JUH;

    iget-object v2, v2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v3, p0, LX/JU0;->a:LX/JUH;

    iget-object v3, v3, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v1, v2, v3}, LX/JUO;->c(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2697561
    :goto_0
    const v1, -0x7c0197ba

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2697562
    :cond_0
    iget-object v1, p0, LX/JU0;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->d:LX/JUO;

    iget-object v2, p0, LX/JU0;->a:LX/JUH;

    iget-object v2, v2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v3, p0, LX/JU0;->a:LX/JUH;

    iget-object v3, v3, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v1, v2, v3}, LX/JUO;->d(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    goto :goto_0
.end method
