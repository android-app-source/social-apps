.class public final LX/IT0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V
    .locals 0

    .prologue
    .line 2578716
    iput-object p1, p0, LX/IT0;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x5d1bdd72

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2578717
    iget-object v0, p0, LX/IT0;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 2578718
    :goto_0
    iget-object v2, p0, LX/IT0;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    .line 2578719
    new-instance v3, LX/5OM;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2578720
    invoke-virtual {v3}, LX/5OM;->c()LX/5OG;

    move-result-object v4

    .line 2578721
    new-instance v5, LX/IT1;

    invoke-direct {v5, v2}, LX/IT1;-><init>(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V

    .line 2578722
    new-instance v6, LX/IT2;

    invoke-direct {v6, v2}, LX/IT2;-><init>(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V

    .line 2578723
    iget-object p0, v2, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2578724
    const p0, 0x7f081b88

    invoke-virtual {v4, p0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578725
    :cond_0
    iget-object v6, v2, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    .line 2578726
    iget-object p0, v2, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object p0

    .line 2578727
    if-eqz v0, :cond_3

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq p0, p1, :cond_1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v6, p0, :cond_3

    .line 2578728
    :cond_1
    const v6, 0x7f081b89

    invoke-virtual {v4, v6}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578729
    invoke-virtual {v4}, LX/5OG;->getCount()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 2578730
    invoke-static {v2}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->j(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V

    .line 2578731
    :goto_1
    const v0, 0x486992a9

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2578732
    :cond_2
    iget-object v0, p0, LX/IT0;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2578733
    const/4 v3, 0x0

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v0, v3, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    goto :goto_0

    .line 2578734
    :cond_3
    iget-object v4, v2, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v3, v4}, LX/0ht;->a(Landroid/view/View;)V

    goto :goto_1
.end method
