.class public final LX/I3d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLInterfaces$EventCollectionSectionEdge;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I3e;


# direct methods
.method public constructor <init>(LX/I3e;)V
    .locals 0

    .prologue
    .line 2531563
    iput-object p1, p0, LX/I3d;->a:LX/I3e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2531564
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2531565
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2531566
    iget-object v0, p0, LX/I3d;->a:LX/I3e;

    iget-object v0, v0, LX/I3e;->ae:LX/Clo;

    if-eqz v0, :cond_0

    .line 2531567
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531568
    if-eqz v0, :cond_0

    .line 2531569
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531570
    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;->a()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2531571
    :cond_0
    :goto_0
    return-void

    .line 2531572
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531573
    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;->a()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->d()LX/7oa;

    move-result-object v0

    .line 2531574
    if-eqz v0, :cond_0

    .line 2531575
    iget-object v1, p0, LX/I3d;->a:LX/I3e;

    invoke-virtual {v1}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/I3d;->a:LX/I3e;

    iget-object v2, v2, LX/I3e;->ad:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-static {v1, v0, v2}, LX/I4F;->a(Landroid/content/Context;LX/7oa;Lcom/facebook/events/common/EventAnalyticsParams;)LX/Clr;

    move-result-object v0

    .line 2531576
    iget-object v1, p0, LX/I3d;->a:LX/I3e;

    iget-object v1, v1, LX/I3e;->ae:LX/Clo;

    .line 2531577
    iget-object v2, v1, LX/Clo;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    move-object v1, v2

    .line 2531578
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 2531579
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Clr;

    invoke-interface {v2}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Clr;

    invoke-interface {v2}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, LX/Clr;->n()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2531580
    :goto_2
    move v1, v3

    .line 2531581
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2531582
    iget-object v2, p0, LX/I3d;->a:LX/I3e;

    iget-object v2, v2, LX/I3e;->X:LX/Chv;

    new-instance v3, LX/CiO;

    invoke-direct {v3, v1, v0}, LX/CiO;-><init>(ILX/Clr;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 2531583
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2531584
    :cond_3
    const/4 v3, -0x1

    goto :goto_2
.end method
