.class public final enum LX/IN8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IN8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IN8;

.field public static final enum CreateGroupButton:LX/IN8;

.field public static final enum GroupRow:LX/IN8;

.field public static final enum Header:LX/IN8;

.field private static final values:[LX/IN8;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2570561
    new-instance v0, LX/IN8;

    const-string v1, "GroupRow"

    invoke-direct {v0, v1, v2}, LX/IN8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IN8;->GroupRow:LX/IN8;

    .line 2570562
    new-instance v0, LX/IN8;

    const-string v1, "Header"

    invoke-direct {v0, v1, v3}, LX/IN8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IN8;->Header:LX/IN8;

    .line 2570563
    new-instance v0, LX/IN8;

    const-string v1, "CreateGroupButton"

    invoke-direct {v0, v1, v4}, LX/IN8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IN8;->CreateGroupButton:LX/IN8;

    .line 2570564
    const/4 v0, 0x3

    new-array v0, v0, [LX/IN8;

    sget-object v1, LX/IN8;->GroupRow:LX/IN8;

    aput-object v1, v0, v2

    sget-object v1, LX/IN8;->Header:LX/IN8;

    aput-object v1, v0, v3

    sget-object v1, LX/IN8;->CreateGroupButton:LX/IN8;

    aput-object v1, v0, v4

    sput-object v0, LX/IN8;->$VALUES:[LX/IN8;

    .line 2570565
    invoke-static {}, LX/IN8;->values()[LX/IN8;

    move-result-object v0

    sput-object v0, LX/IN8;->values:[LX/IN8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2570557
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromOrdinal(I)LX/IN8;
    .locals 1

    .prologue
    .line 2570560
    sget-object v0, LX/IN8;->values:[LX/IN8;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/IN8;
    .locals 1

    .prologue
    .line 2570559
    const-class v0, LX/IN8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IN8;

    return-object v0
.end method

.method public static values()[LX/IN8;
    .locals 1

    .prologue
    .line 2570558
    sget-object v0, LX/IN8;->$VALUES:[LX/IN8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IN8;

    return-object v0
.end method
