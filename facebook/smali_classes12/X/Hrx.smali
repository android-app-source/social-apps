.class public final LX/Hrx;
.super LX/9bf;
.source ""


# instance fields
.field public final synthetic a:LX/Hry;


# direct methods
.method public constructor <init>(LX/Hry;)V
    .locals 0

    .prologue
    .line 2513738
    iput-object p1, p0, LX/Hrx;->a:LX/Hry;

    invoke-direct {p0}, LX/9bf;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2513739
    check-cast p1, LX/9be;

    const/4 v3, 0x0

    .line 2513740
    iget-object v0, p0, LX/Hrx;->a:LX/Hry;

    iget-object v0, v0, LX/Hry;->k:LX/Hro;

    invoke-virtual {v0}, LX/Hro;->b()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Hrx;->a:LX/Hry;

    iget-object v0, v0, LX/Hry;->k:LX/Hro;

    invoke-virtual {v0}, LX/Hro;->b()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/9be;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2513741
    iget-object v0, p0, LX/Hrx;->a:LX/Hry;

    iget-object v0, v0, LX/Hry;->h:LX/Hrp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, LX/Hrp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Z)V

    .line 2513742
    :goto_0
    iget-object v0, p0, LX/Hrx;->a:LX/Hry;

    iget-object v0, v0, LX/Hry;->r:LX/0ht;

    if-eqz v0, :cond_0

    .line 2513743
    iget-object v0, p0, LX/Hrx;->a:LX/Hry;

    const/4 v1, 0x1

    .line 2513744
    iput-boolean v1, v0, LX/Hry;->s:Z

    .line 2513745
    iget-object v0, p0, LX/Hrx;->a:LX/Hry;

    iget-object v0, v0, LX/Hry;->r:LX/0ht;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2513746
    :cond_0
    return-void

    .line 2513747
    :cond_1
    iget-object v0, p0, LX/Hrx;->a:LX/Hry;

    iget-object v0, v0, LX/Hry;->b:LX/0gd;

    sget-object v1, LX/0ge;->COMPOSER_SELECT_ALBUM_CHOOSE:LX/0ge;

    iget-object v2, p0, LX/Hrx;->a:LX/Hry;

    iget-object v2, v2, LX/Hry;->k:LX/Hro;

    invoke-virtual {v2}, LX/Hro;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2513748
    iget-object v0, p0, LX/Hrx;->a:LX/Hry;

    iget-object v0, v0, LX/Hry;->h:LX/Hrp;

    iget-object v1, p1, LX/9be;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0, v1, v3}, LX/Hrp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Z)V

    goto :goto_0
.end method
