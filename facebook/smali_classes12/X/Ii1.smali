.class public LX/Ii1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0gh;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 2603210
    const-string v0, "avi"

    const-string v1, "flv"

    const-string v2, "mkv"

    const-string v3, "mov"

    const-string v4, "mpg"

    const-string v5, "webm"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "wmv"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Ii1;->c:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0gh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2603194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2603195
    iput-object p1, p0, LX/Ii1;->a:Landroid/content/Context;

    .line 2603196
    iput-object p2, p0, LX/Ii1;->b:LX/0gh;

    .line 2603197
    return-void
.end method

.method public static b(LX/0QB;)LX/Ii1;
    .locals 3

    .prologue
    .line 2603208
    new-instance v2, LX/Ii1;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v1

    check-cast v1, LX/0gh;

    invoke-direct {v2, v0, v1}, LX/Ii1;-><init>(Landroid/content/Context;LX/0gh;)V

    .line 2603209
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 2603198
    new-instance v0, LX/Ii0;

    invoke-direct {v0, p0}, LX/Ii0;-><init>(LX/Ii1;)V

    .line 2603199
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2603200
    sget-object v1, LX/Ii1;->c:LX/0Rf;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1t3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2603201
    if-nez v1, :cond_0

    .line 2603202
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2603203
    new-instance v1, LX/31Y;

    iget-object v2, p0, LX/Ii1;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v2, 0x7f083a0e

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f083a0f

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2603204
    iget-object v1, p0, LX/Ii1;->b:LX/0gh;

    const-string v2, "messenger_video_format_not_supported_dialog"

    const/4 v3, 0x1

    const-string p1, "fromModule"

    invoke-static {p1, p2}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v1, v2, v3, p1}, LX/0gh;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 2603205
    const/4 v1, 0x0

    .line 2603206
    :goto_1
    move v0, v1

    .line 2603207
    return v0

    :cond_0
    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
