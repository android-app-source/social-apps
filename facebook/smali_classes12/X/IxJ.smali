.class public LX/IxJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final b:[I

.field private static f:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/154;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1vg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/961;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2631445
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a1

    aput v2, v0, v1

    sput-object v0, LX/IxJ;->b:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2631459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2631460
    return-void
.end method

.method public static a(LX/0QB;)LX/IxJ;
    .locals 7

    .prologue
    .line 2631446
    const-class v1, LX/IxJ;

    monitor-enter v1

    .line 2631447
    :try_start_0
    sget-object v0, LX/IxJ;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2631448
    sput-object v2, LX/IxJ;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2631449
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2631450
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2631451
    new-instance v6, LX/IxJ;

    invoke-direct {v6}, LX/IxJ;-><init>()V

    .line 2631452
    const/16 v3, 0x2d0

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-static {v0}, LX/961;->b(LX/0QB;)LX/961;

    move-result-object v4

    check-cast v4, LX/961;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v5

    check-cast v5, LX/0if;

    .line 2631453
    iput-object p0, v6, LX/IxJ;->a:LX/0Or;

    iput-object v3, v6, LX/IxJ;->c:LX/1vg;

    iput-object v4, v6, LX/IxJ;->d:LX/961;

    iput-object v5, v6, LX/IxJ;->e:LX/0if;

    .line 2631454
    move-object v0, v6

    .line 2631455
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2631456
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IxJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2631457
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2631458
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
