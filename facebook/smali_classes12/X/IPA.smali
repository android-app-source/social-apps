.class public LX/IPA;
.super LX/1Cv;
.source ""


# instance fields
.field public a:LX/1DI;

.field public b:Z

.field private c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/IPB;

.field private g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2573860
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2573861
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IPA;->d:Ljava/util/List;

    .line 2573862
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/IPA;->e:Ljava/util/Set;

    .line 2573863
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2573853
    invoke-static {}, LX/IP9;->values()[LX/IP9;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2573854
    sget-object v1, LX/IP8;->a:[I

    invoke-virtual {v0}, LX/IP9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2573855
    new-instance v0, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    .line 2573856
    :pswitch_0
    new-instance v0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2573857
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2573858
    const v1, 0x7f030a3f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/IPA;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2573859
    iget-object v0, p0, LX/IPA;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 2573836
    invoke-static {}, LX/IP9;->values()[LX/IP9;

    move-result-object v0

    aget-object v0, v0, p4

    .line 2573837
    sget-object v1, LX/IP8;->a:[I

    invoke-virtual {v0}, LX/IP9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2573838
    :goto_0
    return-void

    .line 2573839
    :pswitch_0
    check-cast p2, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    .line 2573840
    check-cast p3, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    .line 2573841
    iget-object v0, p0, LX/IPA;->f:LX/IPB;

    .line 2573842
    iput-object v0, p3, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->t:LX/IPB;

    .line 2573843
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->k()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->k()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 2573844
    invoke-virtual {p0, p2}, LX/IPA;->a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;)V

    .line 2573845
    const-string v0, "FB4AGroupsDiscoverAdapter"

    const-string v1, "Members count for suggested group is less than 3"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573846
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IPA;->e:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2573847
    invoke-virtual {p3, p2, v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;Z)V

    goto :goto_0

    .line 2573848
    :pswitch_1
    iget-boolean v0, p0, LX/IPA;->b:Z

    if-nez v0, :cond_2

    .line 2573849
    iget-object v0, p0, LX/IPA;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/IPA;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/IPA;->a:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0

    .line 2573850
    :cond_2
    iget-boolean v0, p0, LX/IPA;->c:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/IPA;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2573851
    iget-object v0, p0, LX/IPA;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2573852
    :cond_3
    iget-object v1, p0, LX/IPA;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-boolean v0, p0, LX/IPA;->c:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    const/16 v0, 0x8

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;)V
    .locals 2

    .prologue
    .line 2573834
    iget-object v0, p0, LX/IPA;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2573835
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2573830
    iget-boolean v0, p0, LX/IPA;->c:Z

    if-eq p1, v0, :cond_0

    .line 2573831
    iput-boolean p1, p0, LX/IPA;->c:Z

    .line 2573832
    const v0, 0x5d2ef3ec

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2573833
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2573829
    iget-object v0, p0, LX/IPA;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/IPA;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget-object v1, p0, LX/IPA;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2573824
    iget-object v0, p0, LX/IPA;->d:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2573825
    :goto_0
    iget-boolean v1, p0, LX/IPA;->c:Z

    if-eqz v1, :cond_0

    .line 2573826
    add-int/lit8 v0, v0, 0x1

    .line 2573827
    :cond_0
    return v0

    .line 2573828
    :cond_1
    iget-object v0, p0, LX/IPA;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2573823
    iget-object v0, p0, LX/IPA;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IPA;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/IPA;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2573822
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2573814
    iget-object v0, p0, LX/IPA;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, LX/IPA;->c:Z

    if-eqz v0, :cond_0

    .line 2573815
    sget-object v0, LX/IP9;->LOADING_BAR:LX/IP9;

    invoke-virtual {v0}, LX/IP9;->ordinal()I

    move-result v0

    .line 2573816
    :goto_0
    return v0

    .line 2573817
    :cond_0
    iget-object v0, p0, LX/IPA;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2573818
    sget-object v0, LX/IP9;->DISCOVER_ROW:LX/IP9;

    invoke-virtual {v0}, LX/IP9;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2573819
    :cond_1
    const-string v0, "FB4AGroupsDiscoverAdapter"

    const-string v1, "Position is greater than the number of item exist"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573820
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2573821
    invoke-static {}, LX/IP9;->values()[LX/IP9;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
