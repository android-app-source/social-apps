.class public final LX/ISj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V
    .locals 0

    .prologue
    .line 2578132
    iput-object p1, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x2

    const/4 v0, 0x1

    const v1, 0x4ba95e3b    # 2.2199414E7f

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v9

    .line 2578133
    iget-object v0, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v1}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v1

    iget-object v2, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->l:LX/0ad;

    sget-short v3, LX/1EB;->ai:S

    invoke-interface {v2, v3, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    iget-object v3, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->l:LX/0ad;

    sget-short v4, LX/1EB;->an:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    iget-object v4, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v4, v4, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->o:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7Dh;

    invoke-virtual {v4}, LX/7Dh;->b()Z

    move-result v4

    iget-object v5, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v5, v5, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->o:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7Dh;

    invoke-virtual {v5}, LX/7Dh;->d()Z

    move-result v5

    iget-object v6, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v6, v6, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->s:LX/ISp;

    invoke-virtual {v6}, LX/ISp;->c()Z

    move-result v6

    iget-object v7, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v7, v7, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static/range {v0 .. v7}, LX/DJw;->a(Landroid/content/Context;LX/DZC;ZZZZZLcom/facebook/ipc/composer/intent/ComposerPageData;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x6dc

    iget-object v0, p0, LX/ISj;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v8, v1, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2578134
    const v0, -0x311c170f

    invoke-static {v10, v10, v0, v9}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
