.class public final LX/I3L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I3M;


# direct methods
.method public constructor <init>(LX/I3M;)V
    .locals 0

    .prologue
    .line 2531286
    iput-object p1, p0, LX/I3L;->a:LX/I3M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x5055273b

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2531287
    iget-object v1, p0, LX/I3L;->a:LX/I3M;

    iget-object v1, v1, LX/I3M;->c:LX/HxA;

    iget-object v2, p0, LX/I3L;->a:LX/I3M;

    invoke-virtual {v2}, LX/I3M;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/I3L;->a:LX/I3M;

    iget-object v3, v3, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    iget-object v4, p0, LX/I3L;->a:LX/I3M;

    iget-object v4, v4, LX/I3M;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    iget-object v5, p0, LX/I3L;->a:LX/I3M;

    invoke-virtual {v5}, LX/I3M;->getModule()Ljava/lang/String;

    move-result-object v5

    .line 2531288
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    iget-object v7, v1, LX/HxA;->b:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/ComponentName;

    invoke-virtual {p0, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v7

    .line 2531289
    const-string p0, "target_fragment"

    sget-object p1, LX/0cQ;->EVENTS_SUGGESTIONS_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v7, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2531290
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->a()Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v4, v5}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p0

    move-object p0, p0

    .line 2531291
    invoke-virtual {v7, p0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2531292
    iget-object p0, v1, LX/HxA;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v7, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2531293
    const v1, 0x6c102fcf

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->a()Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->name()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
