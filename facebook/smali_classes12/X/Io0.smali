.class public final LX/Io0;
.super LX/6nc;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 0

    .prologue
    .line 2611194
    iput-object p1, p0, LX/Io0;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {p0}, LX/6nc;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2611195
    iget-object v0, p0, LX/Io0;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F:LX/Ini;

    invoke-virtual {v0}, LX/Ini;->a()V

    .line 2611196
    return-void
.end method

.method public final a(LX/6qH;)V
    .locals 3

    .prologue
    .line 2611197
    sget-object v0, LX/Inw;->a:[I

    .line 2611198
    iget-object v1, p1, LX/6qH;->a:LX/6qJ;

    move-object v1, v1

    .line 2611199
    invoke-virtual {v1}, LX/6qJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2611200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected authResult "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2611201
    :pswitch_0
    iget-object v0, p0, LX/Io0;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-virtual {p1}, LX/6qH;->a()Ljava/lang/String;

    move-result-object v1

    .line 2611202
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611203
    iput-object v1, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->v:Ljava/lang/String;

    .line 2611204
    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611205
    :goto_0
    return-void

    .line 2611206
    :pswitch_1
    iget-object v0, p0, LX/Io0;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-virtual {p1}, LX/6qH;->a()Ljava/lang/String;

    move-result-object v1

    .line 2611207
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611208
    iput-object v1, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->u:Ljava/lang/String;

    .line 2611209
    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611210
    goto :goto_0

    .line 2611211
    :pswitch_2
    iget-object v0, p0, LX/Io0;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2611212
    iget-object v0, p0, LX/Io0;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611213
    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611214
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->p:LX/Inc;

    .line 2611215
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2611216
    iget-object p0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v1, v2, p0}, LX/Inc;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611217
    return-void
.end method
