.class public final LX/HGz;
.super LX/HGy;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2446761
    iput-object p1, p0, LX/HGz;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-direct {p0, p2}, LX/HGy;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 2446762
    invoke-virtual {p2, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 2446763
    instance-of v0, v0, LX/HGm;

    return v0
.end method

.method public final b(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)LX/HMF;
    .locals 3

    .prologue
    .line 2446764
    const v0, 0x7f0d2289

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2446765
    invoke-static {p1}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    move v2, v1

    .line 2446766
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getLeft()I

    move-result v1

    .line 2446767
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 2446768
    :goto_2
    new-instance v2, LX/HMF;

    invoke-direct {v2, v1, v0}, LX/HMF;-><init>(II)V

    return-object v2

    .line 2446769
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0

    .line 2446770
    :cond_1
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v1

    goto :goto_1

    .line 2446771
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getRight()I

    move-result v0

    goto :goto_2
.end method
