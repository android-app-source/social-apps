.class public final LX/IrN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/IrP;


# direct methods
.method public constructor <init>(LX/IrP;)V
    .locals 0

    .prologue
    .line 2617813
    iput-object p1, p0, LX/IrN;->a:LX/IrP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 2617814
    iget-object v0, p0, LX/IrN;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    if-eqz v0, :cond_0

    .line 2617815
    iget-object v0, p0, LX/IrN;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->setAddDoodleLayerButtonBrushTipColour(I)V

    .line 2617816
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2617817
    iget-object v0, p0, LX/IrN;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    if-eqz v0, :cond_0

    .line 2617818
    iget-object v0, p0, LX/IrN;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->setUndoodleButtonVisibility(Z)V

    .line 2617819
    :cond_0
    iget-object v0, p0, LX/IrN;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->v:LX/Ir4;

    if-eqz v0, :cond_1

    .line 2617820
    iget-object v0, p0, LX/IrN;->a:LX/IrP;

    invoke-virtual {v0}, LX/IrP;->b()Z

    .line 2617821
    :cond_1
    return-void
.end method
