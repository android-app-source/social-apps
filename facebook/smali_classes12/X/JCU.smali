.class public LX/JCU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 2662367
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PAGE_TAGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->TEXT_LISTS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->UPSELL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->DATE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->TEXT_LINK:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PHONE:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->EMAIL:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->STRING:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->LIST_OF_STRINGS:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    invoke-static/range {v0 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/JCU;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2662347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2662348
    return-void
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)Z
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "isProfileFieldValid"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2662349
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2662350
    :cond_0
    :goto_0
    return v2

    .line 2662351
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    .line 2662352
    sget-object v3, LX/JCU;->a:LX/0Px;

    invoke-virtual {v3, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2662353
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    if-ne v0, v3, :cond_2

    .line 2662354
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2662355
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2662356
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 2662357
    :goto_1
    if-nez v0, :cond_2

    .line 2662358
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2662359
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2662360
    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 2662361
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2662362
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 2662363
    :goto_3
    if-eqz v0, :cond_0

    :cond_2
    move v2, v1

    .line 2662364
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2662365
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    .line 2662366
    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_3
.end method
