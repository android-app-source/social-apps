.class public LX/JCB;
.super LX/1Cd;
.source ""


# instance fields
.field public final a:LX/JCJ;

.field public final b:LX/9lP;


# direct methods
.method public constructor <init>(LX/JCJ;LX/9lP;)V
    .locals 0
    .param p1    # LX/JCJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9lP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2661654
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 2661655
    iput-object p1, p0, LX/JCB;->a:LX/JCJ;

    .line 2661656
    iput-object p2, p0, LX/JCB;->b:LX/9lP;

    .line 2661657
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2661658
    instance-of v0, p1, LX/JCT;

    if-nez v0, :cond_0

    .line 2661659
    :goto_0
    return-void

    .line 2661660
    :cond_0
    check-cast p1, LX/JCT;

    .line 2661661
    iget-object v0, p1, LX/JCT;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    .line 2661662
    if-nez v0, :cond_2

    .line 2661663
    :cond_1
    :goto_1
    goto :goto_0

    .line 2661664
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    move-result-object v0

    .line 2661665
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    if-eq v0, v1, :cond_1

    .line 2661666
    iget-object v1, p0, LX/JCB;->a:LX/JCJ;

    iget-object v2, p0, LX/JCB;->b:LX/9lP;

    .line 2661667
    iget-object v3, v2, LX/9lP;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2661668
    iget-object v3, p0, LX/JCB;->b:LX/9lP;

    invoke-static {v3}, LX/J8p;->a(LX/9lP;)LX/9lQ;

    move-result-object v3

    .line 2661669
    iget-object v4, v1, LX/JCJ;->b:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2661670
    :goto_2
    goto :goto_1

    .line 2661671
    :cond_3
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "profile_about_item_vpv"

    invoke-direct {v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/JCJ;->b()Ljava/lang/String;

    move-result-object p0

    .line 2661672
    iput-object p0, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2661673
    move-object v4, v4

    .line 2661674
    const-string p0, "profile_id"

    invoke-virtual {v4, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2661675
    if-eqz v3, :cond_4

    sget-object p0, LX/9lQ;->UNDEFINED:LX/9lQ;

    if-eq v3, p0, :cond_4

    .line 2661676
    const-string p0, "relationship_type"

    invoke-virtual {v3}, LX/9lQ;->getValue()I

    move-result p1

    invoke-virtual {v4, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2661677
    :cond_4
    const-string p0, "item_type"

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2661678
    iget-object p0, v1, LX/J8p;->a:LX/0Zb;

    invoke-interface {p0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2661679
    iget-object v4, v1, LX/JCJ;->b:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method
