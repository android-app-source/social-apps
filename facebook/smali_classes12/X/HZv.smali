.class public LX/HZv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HZv;


# instance fields
.field public final a:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2482895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2482896
    iput-object p1, p0, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2482897
    return-void
.end method

.method public static a(LX/0QB;)LX/HZv;
    .locals 4

    .prologue
    .line 2482898
    sget-object v0, LX/HZv;->b:LX/HZv;

    if-nez v0, :cond_1

    .line 2482899
    const-class v1, LX/HZv;

    monitor-enter v1

    .line 2482900
    :try_start_0
    sget-object v0, LX/HZv;->b:LX/HZv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2482901
    if-eqz v2, :cond_0

    .line 2482902
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2482903
    new-instance p0, LX/HZv;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/HZv;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 2482904
    move-object v0, p0

    .line 2482905
    sput-object v0, LX/HZv;->b:LX/HZv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2482906
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2482907
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2482908
    :cond_1
    sget-object v0, LX/HZv;->b:LX/HZv;

    return-object v0

    .line 2482909
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2482910
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2482911
    iget-object v0, p0, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x400002

    const-string v2, "RegistrationStepValidationTime"

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2482912
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 2482913
    iget-object v0, p0, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x400003

    const-string v2, "RegistrationLoginTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2482914
    return-void
.end method
