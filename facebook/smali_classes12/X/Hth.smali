.class public LX/Hth;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516572
    iput-object p1, p0, LX/Hth;->a:LX/0Zb;

    .line 2516573
    return-void
.end method

.method public static a(LX/0QB;)LX/Hth;
    .locals 4

    .prologue
    .line 2516574
    const-class v1, LX/Hth;

    monitor-enter v1

    .line 2516575
    :try_start_0
    sget-object v0, LX/Hth;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2516576
    sput-object v2, LX/Hth;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2516577
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2516578
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2516579
    new-instance p0, LX/Hth;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/Hth;-><init>(LX/0Zb;)V

    .line 2516580
    move-object v0, p0

    .line 2516581
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2516582
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hth;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2516583
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2516584
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2516585
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "inline_sprouts"

    .line 2516586
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2516587
    move-object v0, v0

    .line 2516588
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2516589
    move-object v0, v0

    .line 2516590
    return-object v0
.end method
