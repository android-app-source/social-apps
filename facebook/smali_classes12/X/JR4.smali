.class public LX/JR4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ":",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/JRM;

.field public final b:LX/JQr;

.field public final c:LX/JRI;

.field public final d:LX/JR5;

.field public final e:LX/JR8;

.field public final f:LX/JQo;


# direct methods
.method public constructor <init>(LX/JRM;LX/JQr;LX/JRI;LX/JR5;LX/JR8;LX/JQo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692721
    iput-object p1, p0, LX/JR4;->a:LX/JRM;

    .line 2692722
    iput-object p2, p0, LX/JR4;->b:LX/JQr;

    .line 2692723
    iput-object p3, p0, LX/JR4;->c:LX/JRI;

    .line 2692724
    iput-object p4, p0, LX/JR4;->d:LX/JR5;

    .line 2692725
    iput-object p5, p0, LX/JR4;->e:LX/JR8;

    .line 2692726
    iput-object p6, p0, LX/JR4;->f:LX/JQo;

    .line 2692727
    return-void
.end method

.method public static a(LX/0QB;)LX/JR4;
    .locals 10

    .prologue
    .line 2692728
    const-class v1, LX/JR4;

    monitor-enter v1

    .line 2692729
    :try_start_0
    sget-object v0, LX/JR4;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692730
    sput-object v2, LX/JR4;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692731
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692732
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692733
    new-instance v3, LX/JR4;

    invoke-static {v0}, LX/JRM;->a(LX/0QB;)LX/JRM;

    move-result-object v4

    check-cast v4, LX/JRM;

    invoke-static {v0}, LX/JQr;->a(LX/0QB;)LX/JQr;

    move-result-object v5

    check-cast v5, LX/JQr;

    invoke-static {v0}, LX/JRI;->a(LX/0QB;)LX/JRI;

    move-result-object v6

    check-cast v6, LX/JRI;

    invoke-static {v0}, LX/JR5;->a(LX/0QB;)LX/JR5;

    move-result-object v7

    check-cast v7, LX/JR5;

    invoke-static {v0}, LX/JR8;->a(LX/0QB;)LX/JR8;

    move-result-object v8

    check-cast v8, LX/JR8;

    invoke-static {v0}, LX/JQo;->a(LX/0QB;)LX/JQo;

    move-result-object v9

    check-cast v9, LX/JQo;

    invoke-direct/range {v3 .. v9}, LX/JR4;-><init>(LX/JRM;LX/JQr;LX/JRI;LX/JR5;LX/JR8;LX/JQo;)V

    .line 2692734
    move-object v0, v3

    .line 2692735
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692736
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JR4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692737
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
