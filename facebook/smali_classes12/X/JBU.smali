.class public final LX/JBU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 2659782
    const/4 v13, 0x0

    .line 2659783
    const/4 v12, 0x0

    .line 2659784
    const/4 v11, 0x0

    .line 2659785
    const/4 v10, 0x0

    .line 2659786
    const/4 v9, 0x0

    .line 2659787
    const/4 v8, 0x0

    .line 2659788
    const/4 v7, 0x0

    .line 2659789
    const/4 v6, 0x0

    .line 2659790
    const/4 v5, 0x0

    .line 2659791
    const/4 v4, 0x0

    .line 2659792
    const/4 v3, 0x0

    .line 2659793
    const/4 v2, 0x0

    .line 2659794
    const/4 v1, 0x0

    .line 2659795
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 2659796
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2659797
    const/4 v1, 0x0

    .line 2659798
    :goto_0
    return v1

    .line 2659799
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2659800
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 2659801
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 2659802
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2659803
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 2659804
    const-string v15, "curation_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 2659805
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 2659806
    :cond_2
    const-string v15, "eligibleItemsSuggestions"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 2659807
    invoke-static/range {p0 .. p1}, LX/JBB;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 2659808
    :cond_3
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 2659809
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 2659810
    :cond_4
    const-string v15, "items"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 2659811
    invoke-static/range {p0 .. p1}, LX/JB6;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2659812
    :cond_5
    const-string v15, "mediaset"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 2659813
    invoke-static/range {p0 .. p1}, LX/JBV;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2659814
    :cond_6
    const-string v15, "name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 2659815
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2659816
    :cond_7
    const-string v15, "rating_title"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 2659817
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 2659818
    :cond_8
    const-string v15, "requestable_fields"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 2659819
    invoke-static/range {p0 .. p1}, LX/JBX;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2659820
    :cond_9
    const-string v15, "style_list"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 2659821
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 2659822
    :cond_a
    const-string v15, "supports_suggestions"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 2659823
    const/4 v1, 0x1

    .line 2659824
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v4

    goto/16 :goto_1

    .line 2659825
    :cond_b
    const-string v15, "tracking"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 2659826
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 2659827
    :cond_c
    const-string v15, "url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 2659828
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 2659829
    :cond_d
    const/16 v14, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 2659830
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 2659831
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 2659832
    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 2659833
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 2659834
    const/4 v10, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 2659835
    const/4 v9, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 2659836
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2659837
    const/4 v7, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 2659838
    const/16 v6, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 2659839
    if-eqz v1, :cond_e

    .line 2659840
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 2659841
    :cond_e
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2659842
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2659843
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2659844
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2659845
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2659846
    if-eqz v0, :cond_0

    .line 2659847
    const-string v1, "curation_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659848
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2659849
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2659850
    if-eqz v0, :cond_1

    .line 2659851
    const-string v1, "eligibleItemsSuggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659852
    invoke-static {p0, v0, p2, p3}, LX/JBB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2659853
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2659854
    if-eqz v0, :cond_2

    .line 2659855
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659856
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2659857
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2659858
    if-eqz v0, :cond_3

    .line 2659859
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659860
    invoke-static {p0, v0, p2, p3}, LX/JB6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2659861
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2659862
    if-eqz v0, :cond_4

    .line 2659863
    const-string v1, "mediaset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659864
    invoke-static {p0, v0, p2}, LX/JBV;->a(LX/15i;ILX/0nX;)V

    .line 2659865
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2659866
    if-eqz v0, :cond_5

    .line 2659867
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659868
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2659869
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2659870
    if-eqz v0, :cond_6

    .line 2659871
    const-string v1, "rating_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659872
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2659873
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2659874
    if-eqz v0, :cond_7

    .line 2659875
    const-string v1, "requestable_fields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659876
    invoke-static {p0, v0, p2, p3}, LX/JBX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2659877
    :cond_7
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2659878
    if-eqz v0, :cond_8

    .line 2659879
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659880
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2659881
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2659882
    if-eqz v0, :cond_9

    .line 2659883
    const-string v1, "supports_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659884
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2659885
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2659886
    if-eqz v0, :cond_a

    .line 2659887
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659888
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2659889
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2659890
    if-eqz v0, :cond_b

    .line 2659891
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2659892
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2659893
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2659894
    return-void
.end method
