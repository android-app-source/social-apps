.class public final LX/HjK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HjJ;


# instance fields
.field public final synthetic a:[LX/HjF;

.field public final synthetic b:I

.field public final synthetic c:Ljava/util/List;

.field public final synthetic d:[I

.field public final synthetic e:LX/HjL;


# direct methods
.method public constructor <init>(LX/HjL;[LX/HjF;ILjava/util/List;[I)V
    .locals 0

    iput-object p1, p0, LX/HjK;->e:LX/HjL;

    iput-object p2, p0, LX/HjK;->a:[LX/HjF;

    iput p3, p0, LX/HjK;->b:I

    iput-object p4, p0, LX/HjK;->c:Ljava/util/List;

    iput-object p5, p0, LX/HjK;->d:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    const/4 v1, 0x0

    iget-object v2, p0, LX/HjK;->a:[LX/HjF;

    iget v3, p0, LX/HjK;->b:I

    new-instance v4, LX/HjF;

    iget-object v0, p0, LX/HjK;->e:LX/HjL;

    iget-object v0, v0, LX/HjL;->b:LX/HjM;

    iget-object v5, v0, LX/HjM;->b:Landroid/content/Context;

    iget-object v0, p0, LX/HjK;->c:Ljava/util/List;

    iget v6, p0, LX/HjK;->b:I

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hjj;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v0, v6}, LX/HjF;-><init>(Landroid/content/Context;LX/Hjj;LX/Hjy;)V

    aput-object v4, v2, v3

    iget-object v0, p0, LX/HjK;->d:[I

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    iget-object v0, p0, LX/HjK;->d:[I

    aget v0, v0, v1

    iget-object v2, p0, LX/HjK;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_2

    iget-object v0, p0, LX/HjK;->e:LX/HjL;

    iget-object v0, v0, LX/HjL;->b:LX/HjM;

    const/4 v2, 0x1

    iput-boolean v2, v0, LX/HjM;->j:Z

    iget-object v0, p0, LX/HjK;->e:LX/HjL;

    iget-object v0, v0, LX/HjL;->b:LX/HjM;

    iget-object v0, v0, LX/HjM;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, LX/HjK;->e:LX/HjL;

    iget-object v0, v0, LX/HjL;->b:LX/HjM;

    iput v1, v0, LX/HjM;->f:I

    iget-object v2, p0, LX/HjK;->a:[LX/HjF;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v1, v2, v0

    if-eqz v1, :cond_0

    iget-object v4, p0, LX/HjK;->e:LX/HjL;

    iget-object v4, v4, LX/HjL;->b:LX/HjM;

    iget-object v4, v4, LX/HjM;->e:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/HjK;->e:LX/HjL;

    iget-object v0, v0, LX/HjL;->b:LX/HjM;

    iget-object v0, v0, LX/HjM;->g:Lcom/facebook/neko/util/MainActivityFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HjK;->e:LX/HjL;

    iget-object v0, v0, LX/HjL;->b:LX/HjM;

    iget-object v0, v0, LX/HjM;->g:Lcom/facebook/neko/util/MainActivityFragment;

    invoke-virtual {v0}, Lcom/facebook/neko/util/MainActivityFragment;->a()V

    :cond_2
    return-void
.end method
