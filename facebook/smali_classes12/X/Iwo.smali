.class public LX/Iwo;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Iwo;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2630996
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2630997
    const-string v0, "pages/?category={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "category"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PAGES_BROWSER_LIST_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2630998
    sget-object v0, LX/0ax;->bq:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PAGES_BROWSER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2630999
    return-void
.end method

.method public static a(LX/0QB;)LX/Iwo;
    .locals 3

    .prologue
    .line 2631000
    sget-object v0, LX/Iwo;->a:LX/Iwo;

    if-nez v0, :cond_1

    .line 2631001
    const-class v1, LX/Iwo;

    monitor-enter v1

    .line 2631002
    :try_start_0
    sget-object v0, LX/Iwo;->a:LX/Iwo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2631003
    if-eqz v2, :cond_0

    .line 2631004
    :try_start_1
    new-instance v0, LX/Iwo;

    invoke-direct {v0}, LX/Iwo;-><init>()V

    .line 2631005
    move-object v0, v0

    .line 2631006
    sput-object v0, LX/Iwo;->a:LX/Iwo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2631007
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2631008
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2631009
    :cond_1
    sget-object v0, LX/Iwo;->a:LX/Iwo;

    return-object v0

    .line 2631010
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2631011
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
