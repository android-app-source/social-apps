.class public LX/HXx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/HY4;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0lC;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/2QQ;


# direct methods
.method public constructor <init>(LX/HY4;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0lC;Lcom/facebook/content/SecureContextHelper;LX/2QQ;LX/0Or;LX/0Or;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/AuthTokenString;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HY4;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0lC;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/2QQ;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2479743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2479744
    iput-object p1, p0, LX/HXx;->a:LX/HY4;

    .line 2479745
    iput-object p2, p0, LX/HXx;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2479746
    iput-object p3, p0, LX/HXx;->c:LX/0Or;

    .line 2479747
    iput-object p4, p0, LX/HXx;->d:LX/0lC;

    .line 2479748
    iput-object p5, p0, LX/HXx;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2479749
    iput-object p6, p0, LX/HXx;->h:LX/2QQ;

    .line 2479750
    iput-object p7, p0, LX/HXx;->f:LX/0Or;

    .line 2479751
    iput-object p8, p0, LX/HXx;->g:LX/0Or;

    .line 2479752
    return-void
.end method

.method public static b(LX/0QB;)LX/HXx;
    .locals 9

    .prologue
    .line 2479753
    new-instance v0, LX/HXx;

    invoke-static {p0}, LX/HY4;->a(LX/0QB;)LX/HY4;

    move-result-object v1

    check-cast v1, LX/HY4;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v3, 0x19e

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/2QQ;->a(LX/0QB;)LX/2QQ;

    move-result-object v6

    check-cast v6, LX/2QQ;

    const/16 v7, 0x15e7

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x15e6

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/HXx;-><init>(LX/HY4;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0lC;Lcom/facebook/content/SecureContextHelper;LX/2QQ;LX/0Or;LX/0Or;)V

    .line 2479754
    return-object v0
.end method

.method public static c(LX/HXx;Landroid/app/Activity;Landroid/content/Intent;Lcom/facebook/platform/common/action/PlatformAppCall;)LX/HXw;
    .locals 10

    .prologue
    .line 2479742
    new-instance v0, LX/HXw;

    iget-object v1, p0, LX/HXx;->a:LX/HY4;

    iget-object v2, p0, LX/HXx;->c:LX/0Or;

    iget-object v3, p0, LX/HXx;->d:LX/0lC;

    iget-object v4, p0, LX/HXx;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v8, p0, LX/HXx;->f:LX/0Or;

    iget-object v9, p0, LX/HXx;->g:LX/0Or;

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v9}, LX/HXw;-><init>(LX/HY4;LX/0Or;LX/0lC;Lcom/facebook/content/SecureContextHelper;Landroid/app/Activity;Landroid/content/Intent;Lcom/facebook/platform/common/action/PlatformAppCall;LX/0Or;LX/0Or;)V

    return-object v0
.end method
