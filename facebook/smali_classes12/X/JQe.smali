.class public LX/JQe;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQc;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2692047
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JQe;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JQf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692044
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2692045
    iput-object p1, p0, LX/JQe;->b:LX/0Ot;

    .line 2692046
    return-void
.end method

.method public static a(LX/0QB;)LX/JQe;
    .locals 4

    .prologue
    .line 2692033
    const-class v1, LX/JQe;

    monitor-enter v1

    .line 2692034
    :try_start_0
    sget-object v0, LX/JQe;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692035
    sput-object v2, LX/JQe;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692036
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692037
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692038
    new-instance v3, LX/JQe;

    const/16 p0, 0x2008

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQe;-><init>(LX/0Ot;)V

    .line 2692039
    move-object v0, v3

    .line 2692040
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692041
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692042
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2692027
    check-cast p2, LX/JQd;

    .line 2692028
    iget-object v0, p0, LX/JQe;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/JQd;->a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 2692029
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2692030
    :goto_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const p0, 0x7f0b0050

    invoke-virtual {v1, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v1, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    const/16 p0, 0x8

    const/4 p2, 0x5

    invoke-interface {v1, p0, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2692031
    return-object v0

    .line 2692032
    :cond_0
    const v1, 0x7f082992

    invoke-virtual {p1, v1}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2692025
    invoke-static {}, LX/1dS;->b()V

    .line 2692026
    const/4 v0, 0x0

    return-object v0
.end method
