.class public LX/IVp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Pz;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Enum",
            "<*>;>;"
        }
    .end annotation
.end field

.field public d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2582627
    const-class v0, LX/IVp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IVp;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Pz;)V
    .locals 1

    .prologue
    .line 2582628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2582629
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/IVp;->c:Ljava/util/Set;

    .line 2582630
    iput-object p1, p0, LX/IVp;->b:LX/0Pz;

    .line 2582631
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 2582632
    iget-object v0, p0, LX/IVp;->b:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/lang/Enum",
            "<*>;)",
            "LX/IVp",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 2582633
    iget-object v0, p0, LX/IVp;->c:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582634
    sget-object v0, LX/IVp;->a:Ljava/lang/String;

    const-string v1, "Component already added to header, skipping: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2582635
    :goto_0
    return-object p0

    .line 2582636
    :cond_0
    iget-object v0, p0, LX/IVp;->c:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2582637
    iget-object v0, p0, LX/IVp;->b:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2582638
    iget v0, p0, LX/IVp;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/IVp;->d:I

    goto :goto_0
.end method
