.class public final LX/Hvm;
.super LX/62a;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/textstyle/RichTextStylePickerView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/textstyle/RichTextStylePickerView;I)V
    .locals 0

    .prologue
    .line 2519853
    iput-object p1, p0, LX/Hvm;->a:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    .line 2519854
    invoke-direct {p0, p2}, LX/62a;-><init>(I)V

    .line 2519855
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 2

    .prologue
    .line 2519856
    invoke-super {p0, p1, p2, p3, p4}, LX/62a;->a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 2519857
    iget-object v0, p0, LX/Hvm;->a:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    iget-object v0, v0, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 2519858
    iget-object v0, p0, LX/Hvm;->a:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0ce4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 2519859
    :goto_0
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    sub-int v0, v1, v0

    .line 2519860
    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 2519861
    return-void

    .line 2519862
    :cond_0
    iget-object v0, p0, LX/Hvm;->a:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0cd5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto :goto_0
.end method
