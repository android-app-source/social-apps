.class public final LX/HN0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9Zu;

.field public final synthetic b:LX/HN4;


# direct methods
.method public constructor <init>(LX/HN4;LX/9Zu;)V
    .locals 0

    .prologue
    .line 2457988
    iput-object p1, p0, LX/HN0;->b:LX/HN4;

    iput-object p2, p0, LX/HN0;->a:LX/9Zu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x47d35573

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457989
    iget-object v1, p0, LX/HN0;->b:LX/HN4;

    iget-object v1, v1, LX/HN4;->e:LX/HMw;

    if-eqz v1, :cond_1

    .line 2457990
    iget-object v1, p0, LX/HN0;->b:LX/HN4;

    iget-object v1, v1, LX/HN4;->e:LX/HMw;

    iget-object v2, p0, LX/HN0;->a:LX/9Zu;

    .line 2457991
    iget-object v4, v1, LX/HMw;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    .line 2457992
    iget-object v5, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    sget-object v6, LX/0ig;->ah:LX/0ih;

    const-string v7, "tap_add_edit_button"

    invoke-virtual {v5, v6, v7}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2457993
    iget-object v5, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->i:LX/0Uh;

    const/16 v6, 0x5ba

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2457994
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2457995
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2457996
    const-string v7, "page_id"

    iget-object v8, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v9, v8, LX/HN5;->a:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457997
    const-string v7, "service_id"

    invoke-interface {v2}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457998
    const-string v7, "init_props"

    invoke-virtual {v5, v7, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2457999
    const/4 v6, 0x1

    iput-boolean v6, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->w:Z

    .line 2458000
    iget-object v6, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->h:LX/17W;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    sget-object v8, LX/0ax;->fy:Ljava/lang/String;

    invoke-virtual {v6, v7, v8, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2458001
    :cond_0
    :goto_0
    const v1, 0x1f9643e2

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2458002
    :cond_1
    iget-object v1, p0, LX/HN0;->b:LX/HN4;

    iget-object v1, v1, LX/HN4;->f:LX/HMi;

    if-eqz v1, :cond_0

    .line 2458003
    iget-object v1, p0, LX/HN0;->b:LX/HN4;

    iget-object v1, v1, LX/HN4;->b:LX/0if;

    sget-object v2, LX/0ig;->ah:LX/0ih;

    const-string v3, "tap_list_service_item"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2458004
    iget-object v1, p0, LX/HN0;->b:LX/HN4;

    iget-object v1, v1, LX/HN4;->f:LX/HMi;

    iget-object v2, p0, LX/HN0;->a:LX/9Zu;

    .line 2458005
    if-nez v2, :cond_3

    .line 2458006
    :goto_1
    goto :goto_0

    .line 2458007
    :cond_2
    sget-object v5, LX/8Dq;->J:Ljava/lang/String;

    iget-object v6, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v7, v6, LX/HN5;->a:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v2}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2458008
    iget-object v5, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v5, v7, v6}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2458009
    iget-object v5, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2458010
    :cond_3
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2458011
    const-string v5, "com.facebook.katana.profile.id"

    iget-object v6, v1, LX/HMi;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v6, v6, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v6, v6, LX/HN5;->a:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2458012
    const-string v5, "page_service_id_extra"

    invoke-interface {v2}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458013
    iget-object v5, v1, LX/HMi;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-object v5, v5, LX/HN5;->b:Ljava/lang/String;

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2458014
    const-string v5, "profile_name"

    iget-object v6, v1, LX/HMi;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v6, v6, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-object v6, v6, LX/HN5;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458015
    :cond_4
    const-string v5, "extra_service_launched_from_page"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2458016
    iget-object v5, v1, LX/HMi;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/services/PagesServicesFragment;->h:LX/17W;

    iget-object v6, v1, LX/HMi;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, LX/0ax;->aH:Ljava/lang/String;

    iget-object v8, v1, LX/HMi;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v8, v8, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v8, v8, LX/HN5;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v2}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto/16 :goto_1
.end method
