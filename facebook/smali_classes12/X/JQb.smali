.class public LX/JQb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/JQS;

.field public final b:LX/3mL;

.field public final c:LX/JQe;

.field public final d:LX/JQV;

.field public final e:LX/0Zb;


# direct methods
.method public constructor <init>(LX/JQS;LX/3mL;LX/JQe;LX/JQV;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2691970
    iput-object p1, p0, LX/JQb;->a:LX/JQS;

    .line 2691971
    iput-object p2, p0, LX/JQb;->b:LX/3mL;

    .line 2691972
    iput-object p3, p0, LX/JQb;->c:LX/JQe;

    .line 2691973
    iput-object p4, p0, LX/JQb;->d:LX/JQV;

    .line 2691974
    iput-object p5, p0, LX/JQb;->e:LX/0Zb;

    .line 2691975
    return-void
.end method

.method public static a(LX/0QB;)LX/JQb;
    .locals 9

    .prologue
    .line 2691976
    const-class v1, LX/JQb;

    monitor-enter v1

    .line 2691977
    :try_start_0
    sget-object v0, LX/JQb;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691978
    sput-object v2, LX/JQb;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691979
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691980
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691981
    new-instance v3, LX/JQb;

    const-class v4, LX/JQS;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/JQS;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v5

    check-cast v5, LX/3mL;

    invoke-static {v0}, LX/JQe;->a(LX/0QB;)LX/JQe;

    move-result-object v6

    check-cast v6, LX/JQe;

    invoke-static {v0}, LX/JQV;->a(LX/0QB;)LX/JQV;

    move-result-object v7

    check-cast v7, LX/JQV;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-direct/range {v3 .. v8}, LX/JQb;-><init>(LX/JQS;LX/3mL;LX/JQe;LX/JQV;LX/0Zb;)V

    .line 2691982
    move-object v0, v3

    .line 2691983
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691984
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691985
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691986
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
