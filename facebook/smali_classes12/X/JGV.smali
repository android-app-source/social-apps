.class public final LX/JGV;
.super LX/JGO;
.source ""


# static fields
.field public static final c:Landroid/graphics/Paint;

.field private static final d:[F


# instance fields
.field public e:F

.field public f:F

.field public g:F

.field public h:F

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field private o:Landroid/graphics/DashPathEffect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Landroid/graphics/Path;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2667507
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, LX/JGV;->c:Landroid/graphics/Paint;

    .line 2667508
    const/4 v0, 0x4

    new-array v0, v0, [F

    sput-object v0, LX/JGV;->d:[F

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2667399
    invoke-direct {p0}, LX/JGO;-><init>()V

    .line 2667400
    const/4 v0, 0x0

    iput v0, p0, LX/JGV;->m:I

    return-void
.end method

.method private static a(FF)F
    .locals 1

    .prologue
    .line 2667506
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    move p1, p0

    goto :goto_0
.end method

.method private static a(LX/JGV;III)I
    .locals 1

    .prologue
    .line 2667505
    invoke-virtual {p0, p1}, LX/JGO;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p2

    :cond_0
    move p2, p3

    goto :goto_0
.end method

.method private static a(LX/JGV;FFFFFF)V
    .locals 1

    .prologue
    .line 2667496
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    if-nez v0, :cond_0

    .line 2667497
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    .line 2667498
    :cond_0
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 2667499
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p3, p1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2667500
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p4, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667501
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p6, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667502
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p5, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667503
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p3, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667504
    return-void
.end method

.method private static b(LX/JGV;FFFFFF)V
    .locals 1

    .prologue
    .line 2667487
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    if-nez v0, :cond_0

    .line 2667488
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    .line 2667489
    :cond_0
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 2667490
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p3, p1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2667491
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p5, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667492
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p6, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667493
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p4, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667494
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p3, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667495
    return-void
.end method

.method private static c(F)Landroid/graphics/DashPathEffect;
    .locals 3

    .prologue
    .line 2667483
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 2667484
    sget-object v1, LX/JGV;->d:[F

    aput p0, v1, v0

    .line 2667485
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2667486
    :cond_0
    new-instance v0, Landroid/graphics/DashPathEffect;

    sget-object v1, LX/JGV;->d:[F

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    return-object v0
.end method

.method private static c(LX/JGV;FFFFFF)V
    .locals 1

    .prologue
    .line 2667509
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    if-nez v0, :cond_0

    .line 2667510
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    .line 2667511
    :cond_0
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 2667512
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p5, p1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2667513
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p6, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667514
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p6, p4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667515
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p5, p3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667516
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p5, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667517
    return-void
.end method

.method private static d(LX/JGV;FFFFFF)V
    .locals 1

    .prologue
    .line 2667474
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    if-nez v0, :cond_0

    .line 2667475
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    .line 2667476
    :cond_0
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 2667477
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p5, p1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2667478
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p5, p3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667479
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p6, p4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667480
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p6, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667481
    iget-object v0, p0, LX/JGV;->p:Landroid/graphics/Path;

    invoke-virtual {v0, p5, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2667482
    return-void
.end method

.method private e(Landroid/graphics/Canvas;)V
    .locals 23

    .prologue
    .line 2667425
    invoke-virtual/range {p0 .. p0}, LX/JGO;->c()I

    move-result v1

    .line 2667426
    invoke-virtual/range {p0 .. p0}, LX/JGO;->a()F

    move-result v7

    .line 2667427
    invoke-virtual/range {p0 .. p0}, LX/JGN;->k()F

    move-result v2

    .line 2667428
    move-object/from16 v0, p0

    iget v3, v0, LX/JGV;->f:F

    invoke-static {v3, v7}, LX/JGV;->a(FF)F

    move-result v8

    .line 2667429
    add-float v3, v2, v8

    .line 2667430
    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget v5, v0, LX/JGV;->j:I

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5, v1}, LX/JGV;->a(LX/JGV;III)I

    move-result v9

    .line 2667431
    invoke-virtual/range {p0 .. p0}, LX/JGN;->m()F

    move-result v15

    .line 2667432
    move-object/from16 v0, p0

    iget v4, v0, LX/JGV;->h:F

    invoke-static {v4, v7}, LX/JGV;->a(FF)F

    move-result v14

    .line 2667433
    sub-float v16, v15, v14

    .line 2667434
    const/16 v4, 0x10

    move-object/from16 v0, p0

    iget v5, v0, LX/JGV;->l:I

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5, v1}, LX/JGV;->a(LX/JGV;III)I

    move-result v17

    .line 2667435
    invoke-virtual/range {p0 .. p0}, LX/JGN;->j()F

    move-result v4

    .line 2667436
    move-object/from16 v0, p0

    iget v5, v0, LX/JGV;->e:F

    invoke-static {v5, v7}, LX/JGV;->a(FF)F

    move-result v18

    .line 2667437
    add-float v5, v4, v18

    .line 2667438
    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget v10, v0, LX/JGV;->i:I

    move-object/from16 v0, p0

    invoke-static {v0, v6, v10, v1}, LX/JGV;->a(LX/JGV;III)I

    move-result v19

    .line 2667439
    invoke-virtual/range {p0 .. p0}, LX/JGN;->l()F

    move-result v6

    .line 2667440
    move-object/from16 v0, p0

    iget v10, v0, LX/JGV;->g:F

    invoke-static {v10, v7}, LX/JGV;->a(FF)F

    move-result v20

    .line 2667441
    sub-float v7, v6, v20

    .line 2667442
    const/16 v10, 0x8

    move-object/from16 v0, p0

    iget v11, v0, LX/JGV;->k:I

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11, v1}, LX/JGV;->a(LX/JGV;III)I

    move-result v21

    .line 2667443
    invoke-static/range {p0 .. p0}, LX/JGV;->o(LX/JGV;)Z

    move-result v22

    .line 2667444
    if-eqz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/JGV;->p:Landroid/graphics/Path;

    if-nez v1, :cond_0

    .line 2667445
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/JGV;->p:Landroid/graphics/Path;

    .line 2667446
    :cond_0
    invoke-static {v9}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    cmpl-float v1, v8, v1

    if-eqz v1, :cond_1

    .line 2667447
    sget-object v1, LX/JGV;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 2667448
    if-eqz v22, :cond_6

    move-object/from16 v1, p0

    .line 2667449
    invoke-static/range {v1 .. v7}, LX/JGV;->a(LX/JGV;FFFFFF)V

    .line 2667450
    move-object/from16 v0, p0

    iget-object v1, v0, LX/JGV;->p:Landroid/graphics/Path;

    sget-object v8, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2667451
    :cond_1
    :goto_0
    invoke-static/range {v17 .. v17}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    cmpl-float v1, v14, v1

    if-eqz v1, :cond_2

    .line 2667452
    sget-object v1, LX/JGV;->c:Landroid/graphics/Paint;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2667453
    if-eqz v22, :cond_7

    move-object/from16 v8, p0

    move v9, v15

    move/from16 v10, v16

    move v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    .line 2667454
    invoke-static/range {v8 .. v14}, LX/JGV;->b(LX/JGV;FFFFFF)V

    .line 2667455
    move-object/from16 v0, p0

    iget-object v1, v0, LX/JGV;->p:Landroid/graphics/Path;

    sget-object v8, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2667456
    :cond_2
    :goto_1
    invoke-static/range {v19 .. v19}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    cmpl-float v1, v18, v1

    if-eqz v1, :cond_3

    .line 2667457
    sget-object v1, LX/JGV;->c:Landroid/graphics/Paint;

    move/from16 v0, v19

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2667458
    if-eqz v22, :cond_8

    move-object/from16 v8, p0

    move v9, v2

    move v10, v3

    move v11, v15

    move/from16 v12, v16

    move v13, v4

    move v14, v5

    .line 2667459
    invoke-static/range {v8 .. v14}, LX/JGV;->c(LX/JGV;FFFFFF)V

    .line 2667460
    move-object/from16 v0, p0

    iget-object v1, v0, LX/JGV;->p:Landroid/graphics/Path;

    sget-object v4, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2667461
    :cond_3
    :goto_2
    invoke-static/range {v21 .. v21}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    cmpl-float v1, v20, v1

    if-eqz v1, :cond_4

    .line 2667462
    sget-object v1, LX/JGV;->c:Landroid/graphics/Paint;

    move/from16 v0, v21

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2667463
    if-eqz v22, :cond_9

    move-object/from16 v8, p0

    move v9, v2

    move v10, v3

    move v11, v15

    move/from16 v12, v16

    move v13, v6

    move v14, v7

    .line 2667464
    invoke-static/range {v8 .. v14}, LX/JGV;->d(LX/JGV;FFFFFF)V

    .line 2667465
    move-object/from16 v0, p0

    iget-object v1, v0, LX/JGV;->p:Landroid/graphics/Path;

    sget-object v2, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2667466
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget v1, v0, LX/JGV;->n:I

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    if-eqz v1, :cond_5

    .line 2667467
    sget-object v1, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, LX/JGV;->n:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2667468
    sget-object v9, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    move v6, v3

    move/from16 v8, v16

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2667469
    :cond_5
    return-void

    .line 2667470
    :cond_6
    sget-object v13, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v8, p1

    move v9, v4

    move v10, v2

    move v11, v6

    move v12, v3

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 2667471
    :cond_7
    sget-object v13, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v8, p1

    move v9, v4

    move/from16 v10, v16

    move v11, v6

    move v12, v15

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 2667472
    :cond_8
    sget-object v13, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v8, p1

    move v9, v4

    move v10, v3

    move v11, v5

    move/from16 v12, v16

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 2667473
    :cond_9
    sget-object v13, LX/JGV;->c:Landroid/graphics/Paint;

    move-object/from16 v8, p1

    move v9, v7

    move v10, v3

    move v11, v6

    move/from16 v12, v16

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_3
.end method

.method private n()Landroid/graphics/DashPathEffect;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v2, 0x20

    .line 2667416
    invoke-virtual {p0, v2}, LX/JGO;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2667417
    iget v0, p0, LX/JGV;->m:I

    packed-switch v0, :pswitch_data_0

    .line 2667418
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGV;->o:Landroid/graphics/DashPathEffect;

    .line 2667419
    :goto_0
    invoke-virtual {p0, v2}, LX/JGO;->d(I)V

    .line 2667420
    :cond_0
    iget-object v0, p0, LX/JGV;->o:Landroid/graphics/DashPathEffect;

    return-object v0

    .line 2667421
    :pswitch_0
    iget v0, p0, LX/JGO;->g:F

    move v0, v0

    .line 2667422
    invoke-static {v0}, LX/JGV;->c(F)Landroid/graphics/DashPathEffect;

    move-result-object v0

    iput-object v0, p0, LX/JGV;->o:Landroid/graphics/DashPathEffect;

    goto :goto_0

    .line 2667423
    :pswitch_1
    iget v0, p0, LX/JGO;->g:F

    move v0, v0

    .line 2667424
    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, LX/JGV;->c(F)Landroid/graphics/DashPathEffect;

    move-result-object v0

    iput-object v0, p0, LX/JGV;->o:Landroid/graphics/DashPathEffect;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static o(LX/JGV;)Z
    .locals 1

    .prologue
    .line 2667415
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/JGO;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/JGO;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/JGO;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/JGO;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2667402
    iget v0, p0, LX/JGO;->h:F

    move v0, v0

    .line 2667403
    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    invoke-direct {p0}, LX/JGV;->n()Landroid/graphics/DashPathEffect;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2667404
    :cond_0
    iget v0, p0, LX/JGV;->n:I

    if-eqz v0, :cond_1

    .line 2667405
    sget-object v0, LX/JGV;->c:Landroid/graphics/Paint;

    iget v1, p0, LX/JGV;->n:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2667406
    invoke-virtual {p0}, LX/JGO;->f()Landroid/graphics/Path;

    move-result-object v0

    sget-object v1, LX/JGV;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2667407
    :cond_1
    iget v0, p0, LX/JGO;->g:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 2667408
    :cond_2
    :goto_0
    return-void

    .line 2667409
    :cond_3
    invoke-direct {p0, p1}, LX/JGV;->e(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 2667410
    :cond_4
    iget v0, p0, LX/JGO;->f:I

    if-eqz v0, :cond_2

    .line 2667411
    sget-object v0, LX/JGO;->c:Landroid/graphics/Paint;

    iget v1, p0, LX/JGO;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2667412
    sget-object v0, LX/JGO;->c:Landroid/graphics/Paint;

    iget v1, p0, LX/JGO;->g:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2667413
    sget-object v0, LX/JGO;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/JGO;->e()Landroid/graphics/PathEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 2667414
    invoke-virtual {p0}, LX/JGO;->f()Landroid/graphics/Path;

    move-result-object v0

    sget-object v1, LX/JGO;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final synthetic e()Landroid/graphics/PathEffect;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2667401
    invoke-direct {p0}, LX/JGV;->n()Landroid/graphics/DashPathEffect;

    move-result-object v0

    return-object v0
.end method
