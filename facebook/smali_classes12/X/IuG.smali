.class public final enum LX/IuG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IuG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IuG;

.field public static final enum COMPLETED:LX/IuG;

.field public static final enum FAILED:LX/IuG;

.field public static final enum NOT_STARTED:LX/IuG;

.field public static final enum STARTED:LX/IuG;


# instance fields
.field private mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2625889
    new-instance v0, LX/IuG;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2, v2}, LX/IuG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuG;->NOT_STARTED:LX/IuG;

    .line 2625890
    new-instance v0, LX/IuG;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v3, v3}, LX/IuG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuG;->STARTED:LX/IuG;

    .line 2625891
    new-instance v0, LX/IuG;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v4, v4}, LX/IuG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuG;->COMPLETED:LX/IuG;

    .line 2625892
    new-instance v0, LX/IuG;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5, v5}, LX/IuG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuG;->FAILED:LX/IuG;

    .line 2625893
    const/4 v0, 0x4

    new-array v0, v0, [LX/IuG;

    sget-object v1, LX/IuG;->NOT_STARTED:LX/IuG;

    aput-object v1, v0, v2

    sget-object v1, LX/IuG;->STARTED:LX/IuG;

    aput-object v1, v0, v3

    sget-object v1, LX/IuG;->COMPLETED:LX/IuG;

    aput-object v1, v0, v4

    sget-object v1, LX/IuG;->FAILED:LX/IuG;

    aput-object v1, v0, v5

    sput-object v0, LX/IuG;->$VALUES:[LX/IuG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2625886
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2625887
    iput p3, p0, LX/IuG;->mValue:I

    .line 2625888
    return-void
.end method

.method public static from(I)LX/IuG;
    .locals 5

    .prologue
    .line 2625894
    invoke-static {}, LX/IuG;->values()[LX/IuG;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2625895
    invoke-virtual {v0}, LX/IuG;->getValue()I

    move-result v4

    if-ne p0, v4, :cond_0

    .line 2625896
    :goto_1
    return-object v0

    .line 2625897
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2625898
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/IuG;
    .locals 1

    .prologue
    .line 2625885
    const-class v0, LX/IuG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IuG;

    return-object v0
.end method

.method public static values()[LX/IuG;
    .locals 1

    .prologue
    .line 2625884
    sget-object v0, LX/IuG;->$VALUES:[LX/IuG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IuG;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2625883
    iget v0, p0, LX/IuG;->mValue:I

    return v0
.end method
