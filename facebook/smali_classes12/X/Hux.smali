.class public LX/Hux;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Huw;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2518148
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2518149
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/HqJ;LX/0zw;LX/ASX;)LX/Huw;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j2;",
            ":",
            "LX/0iq;",
            ":",
            "LX/0jD;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(TServices;",
            "Lcom/facebook/composer/privacy/controller/TagExpansionPillViewController$TagExpansionPillClickedListener;",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;",
            "LX/ASX;",
            ")",
            "LX/Huw",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2518150
    new-instance v0, LX/Huw;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    const/16 v2, 0x1962

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v3

    check-cast v3, LX/0gd;

    invoke-static {p0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    move-object v5, p1

    check-cast v5, LX/0il;

    invoke-static {p0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v9

    check-cast v9, LX/3kp;

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v10}, LX/Huw;-><init>(LX/0wM;LX/0Ot;LX/0gd;Lcom/facebook/user/model/User;LX/0il;LX/HqJ;LX/0zw;LX/ASX;LX/3kp;Landroid/content/Context;)V

    .line 2518151
    return-object v0
.end method
