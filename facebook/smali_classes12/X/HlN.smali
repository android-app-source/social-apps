.class public final enum LX/HlN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HlN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HlN;

.field public static final enum FIRST:LX/HlN;

.field public static final enum HINT:LX/HlN;

.field public static final enum TIME:LX/HlN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2498492
    new-instance v0, LX/HlN;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v2}, LX/HlN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HlN;->FIRST:LX/HlN;

    .line 2498493
    new-instance v0, LX/HlN;

    const-string v1, "TIME"

    invoke-direct {v0, v1, v3}, LX/HlN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HlN;->TIME:LX/HlN;

    .line 2498494
    new-instance v0, LX/HlN;

    const-string v1, "HINT"

    invoke-direct {v0, v1, v4}, LX/HlN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HlN;->HINT:LX/HlN;

    .line 2498495
    const/4 v0, 0x3

    new-array v0, v0, [LX/HlN;

    sget-object v1, LX/HlN;->FIRST:LX/HlN;

    aput-object v1, v0, v2

    sget-object v1, LX/HlN;->TIME:LX/HlN;

    aput-object v1, v0, v3

    sget-object v1, LX/HlN;->HINT:LX/HlN;

    aput-object v1, v0, v4

    sput-object v0, LX/HlN;->$VALUES:[LX/HlN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2498496
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HlN;
    .locals 1

    .prologue
    .line 2498497
    const-class v0, LX/HlN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HlN;

    return-object v0
.end method

.method public static values()[LX/HlN;
    .locals 1

    .prologue
    .line 2498498
    sget-object v0, LX/HlN;->$VALUES:[LX/HlN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HlN;

    return-object v0
.end method
