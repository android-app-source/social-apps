.class public final enum LX/J81;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J81;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J81;

.field public static final enum BAKEOFF:LX/J81;

.field public static final enum DEFAULT:LX/J81;


# instance fields
.field private final mSurveyType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2651403
    new-instance v0, LX/J81;

    const-string v1, "DEFAULT"

    const-string v2, "default"

    invoke-direct {v0, v1, v3, v2}, LX/J81;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J81;->DEFAULT:LX/J81;

    .line 2651404
    new-instance v0, LX/J81;

    const-string v1, "BAKEOFF"

    const-string v2, "bakeoff"

    invoke-direct {v0, v1, v4, v2}, LX/J81;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J81;->BAKEOFF:LX/J81;

    .line 2651405
    const/4 v0, 0x2

    new-array v0, v0, [LX/J81;

    sget-object v1, LX/J81;->DEFAULT:LX/J81;

    aput-object v1, v0, v3

    sget-object v1, LX/J81;->BAKEOFF:LX/J81;

    aput-object v1, v0, v4

    sput-object v0, LX/J81;->$VALUES:[LX/J81;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2651396
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2651397
    iput-object p3, p0, LX/J81;->mSurveyType:Ljava/lang/String;

    .line 2651398
    return-void
.end method

.method public static fromStringToSurveyType(Ljava/lang/String;)LX/J81;
    .locals 1

    .prologue
    .line 2651399
    sget-object v0, LX/J81;->BAKEOFF:LX/J81;

    invoke-virtual {v0}, LX/J81;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2651400
    sget-object v0, LX/J81;->BAKEOFF:LX/J81;

    .line 2651401
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/J81;->DEFAULT:LX/J81;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/J81;
    .locals 1

    .prologue
    .line 2651402
    const-class v0, LX/J81;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J81;

    return-object v0
.end method

.method public static values()[LX/J81;
    .locals 1

    .prologue
    .line 2651394
    sget-object v0, LX/J81;->$VALUES:[LX/J81;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J81;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2651395
    iget-object v0, p0, LX/J81;->mSurveyType:Ljava/lang/String;

    return-object v0
.end method
