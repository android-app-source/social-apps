.class public final LX/J1a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IoE;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;)V
    .locals 0

    .prologue
    .line 2639037
    iput-object p1, p0, LX/J1a;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2639031
    iget-object v0, p0, LX/J1a;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v0, v0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->f:LX/Iox;

    if-eqz v0, :cond_0

    .line 2639032
    iget-object v0, p0, LX/J1a;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v0, v0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->f:LX/Iox;

    iget-object v1, p0, LX/J1a;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v1, v1, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Iox;->a(Ljava/lang/String;)V

    .line 2639033
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2639034
    iget-object v0, p0, LX/J1a;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v0, v0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J1c;

    iget-object v1, p0, LX/J1a;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v1, v1, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, LX/J1c;->a(Landroid/view/View;)V

    .line 2639035
    iget-object v0, p0, LX/J1a;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v0, v0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 2639036
    return-void
.end method
