.class public final LX/IaE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V
    .locals 0

    .prologue
    .line 2590730
    iput-object p1, p0, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xe2a5c56

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2590716
    iget-object v1, p0, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->e:LX/IZw;

    const-string v2, "click_confirm_code_button"

    invoke-virtual {v1, v2}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590717
    iget-object v1, p0, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    .line 2590718
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->i:LX/4At;

    if-nez v2, :cond_0

    .line 2590719
    new-instance v2, LX/4At;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const p1, 0x7f083a42

    invoke-direct {v2, v3, p1}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->i:LX/4At;

    .line 2590720
    :cond_0
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->i:LX/4At;

    invoke-virtual {v2}, LX/4At;->beginShowingProgress()V

    .line 2590721
    iget-object v1, p0, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->a:LX/Ia0;

    iget-object v2, p0, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->g:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/IaD;

    invoke-direct {v3, p0}, LX/IaD;-><init>(LX/IaE;)V

    .line 2590722
    new-instance v5, LX/IZY;

    invoke-direct {v5}, LX/IZY;-><init>()V

    move-object v5, v5

    .line 2590723
    new-instance v6, LX/4Dd;

    invoke-direct {v6}, LX/4Dd;-><init>()V

    .line 2590724
    const-string p0, "confirmation_code"

    invoke-virtual {v6, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590725
    const-string p0, "input"

    invoke-virtual {v5, p0, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2590726
    iget-object v6, v1, LX/Ia0;->c:LX/1Ck;

    const-string p0, "ConfirmPhoneCodeKey"

    iget-object p1, v1, LX/Ia0;->b:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2590727
    new-instance p1, LX/IZz;

    invoke-direct {p1, v1, v3}, LX/IZz;-><init>(LX/Ia0;LX/IaD;)V

    move-object p1, p1

    .line 2590728
    invoke-virtual {v6, p0, v5, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2590729
    const v1, 0x40f95f72

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
