.class public final LX/JQd;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JQe;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

.field public final synthetic b:LX/JQe;


# direct methods
.method public constructor <init>(LX/JQe;)V
    .locals 1

    .prologue
    .line 2692009
    iput-object p1, p0, LX/JQd;->b:LX/JQe;

    .line 2692010
    move-object v0, p1

    .line 2692011
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2692012
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2692013
    const-string v0, "JobSearchHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2692014
    if-ne p0, p1, :cond_1

    .line 2692015
    :cond_0
    :goto_0
    return v0

    .line 2692016
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2692017
    goto :goto_0

    .line 2692018
    :cond_3
    check-cast p1, LX/JQd;

    .line 2692019
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2692020
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2692021
    if-eq v2, v3, :cond_0

    .line 2692022
    iget-object v2, p0, LX/JQd;->a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JQd;->a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    iget-object v3, p1, LX/JQd;->a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2692023
    goto :goto_0

    .line 2692024
    :cond_4
    iget-object v2, p1, LX/JQd;->a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
