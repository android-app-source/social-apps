.class public final LX/IhT;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;LX/0gc;)V
    .locals 0

    .prologue
    .line 2602666
    iput-object p1, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    .line 2602667
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2602668
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2602649
    instance-of v0, p1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    if-eqz v0, :cond_1

    .line 2602650
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    check-cast p1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    .line 2602651
    iput-object p1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->d:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    .line 2602652
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->d:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object v1, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->h:LX/IhS;

    .line 2602653
    iput-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->e:LX/IhK;

    .line 2602654
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2602655
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->d:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object v1, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a(Ljava/util/ArrayList;)V

    .line 2602656
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    const/4 v1, 0x0

    .line 2602657
    iput-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->i:Ljava/util/ArrayList;

    .line 2602658
    :cond_0
    :goto_0
    return-void

    .line 2602659
    :cond_1
    instance-of v0, p1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;

    if-eqz v0, :cond_2

    .line 2602660
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    check-cast p1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;

    .line 2602661
    iput-object p1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->c:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;

    .line 2602662
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->c:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;

    iget-object v1, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->g:LX/IhR;

    .line 2602663
    iput-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->d:LX/IhR;

    .line 2602664
    goto :goto_0

    .line 2602665
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attaching an unexpected fragment type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2602669
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, LX/IhU;->values()[LX/IhU;

    move-result-object v1

    aget-object v1, v1, p1

    iget v1, v1, LX/IhU;->titleResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2602634
    sget-object v0, LX/IhQ;->a:[I

    invoke-static {}, LX/IhU;->values()[LX/IhU;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, LX/IhU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2602635
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2602636
    :pswitch_0
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->j:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602637
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2602638
    const-string p1, "environment"

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2602639
    new-instance v1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;-><init>()V

    .line 2602640
    invoke-virtual {v1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2602641
    move-object v0, v1

    .line 2602642
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/IhT;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->j:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602643
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2602644
    const-string p1, "extra_environment"

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2602645
    new-instance v1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;-><init>()V

    .line 2602646
    invoke-virtual {v1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2602647
    move-object v0, v1

    .line 2602648
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2602631
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2602632
    invoke-direct {p0, v0}, LX/IhT;->a(Landroid/support/v4/app/Fragment;)V

    .line 2602633
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2602630
    invoke-static {}, LX/IhU;->values()[LX/IhU;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
