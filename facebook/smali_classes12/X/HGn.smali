.class public final LX/HGn;
.super LX/HGm;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public l:I

.field public m:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/HH2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2446597
    invoke-direct {p0, p1}, LX/HGm;-><init>(Landroid/view/View;)V

    .line 2446598
    const/4 v0, -0x1

    iput v0, p0, LX/HGn;->l:I

    .line 2446599
    iput-object v1, p0, LX/HGn;->m:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2446600
    iput-object v1, p0, LX/HGn;->n:LX/HH2;

    .line 2446601
    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/HH2;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2446589
    invoke-direct {p0, p1}, LX/HGm;-><init>(Landroid/view/View;)V

    .line 2446590
    const/4 v0, -0x1

    iput v0, p0, LX/HGn;->l:I

    .line 2446591
    iput-object v1, p0, LX/HGn;->m:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2446592
    iput-object v1, p0, LX/HGn;->n:LX/HH2;

    .line 2446593
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2446594
    iput-object p2, p0, LX/HGn;->n:LX/HH2;

    .line 2446595
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2446596
    return-void
.end method

.method private static a(ILandroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2446602
    if-nez p0, :cond_0

    .line 2446603
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0811d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2446604
    :goto_0
    return-object v0

    .line 2446605
    :cond_0
    if-lez p0, :cond_1

    .line 2446606
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0085

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2446607
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid album size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(JLX/11R;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2446586
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 2446587
    const-string v0, ""

    .line 2446588
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p0

    invoke-virtual {p2, v0, v2, v3}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;ILandroid/content/Context;LX/11R;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2446534
    iput p2, p0, LX/HGn;->l:I

    .line 2446535
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    .line 2446536
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    .line 2446537
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 2446538
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v4

    .line 2446539
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->z()J

    move-result-wide v8

    .line 2446540
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    .line 2446541
    :goto_4
    if-eqz v0, :cond_5

    .line 2446542
    iput-object p1, p0, LX/HGn;->m:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2446543
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 2446544
    :goto_5
    if-eqz v2, :cond_6

    .line 2446545
    iget-object v0, p0, LX/HGm;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2446546
    :goto_6
    iget-object v0, p0, LX/HGm;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2446547
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4, p3}, LX/HGn;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \u00b7 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v8, v9, p4}, LX/HGn;->a(JLX/11R;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2446548
    iget-object v1, p0, LX/HGm;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2446549
    iget-object v0, p0, LX/HGm;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2446550
    return-void

    :cond_0
    move-object v0, v1

    .line 2446551
    goto/16 :goto_0

    :cond_1
    move-object v2, v1

    .line 2446552
    goto/16 :goto_1

    .line 2446553
    :cond_2
    const-string v3, ""

    goto :goto_2

    :cond_3
    move v4, v5

    .line 2446554
    goto :goto_3

    .line 2446555
    :cond_4
    const-string v6, ""

    goto :goto_4

    .line 2446556
    :cond_5
    iput-object v1, p0, LX/HGn;->m:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2446557
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    goto :goto_5

    .line 2446558
    :cond_6
    iget-object v0, p0, LX/HGm;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x7f020626

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    goto :goto_6
.end method

.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x7c7ca59f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2446559
    iget-object v1, p0, LX/HGn;->n:LX/HH2;

    if-eqz v1, :cond_2

    .line 2446560
    iget-object v1, p0, LX/HGn;->n:LX/HH2;

    iget-object v2, p0, LX/HGn;->m:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget v3, p0, LX/HGn;->l:I

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 2446561
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    move v4, v13

    .line 2446562
    :goto_0
    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    .line 2446563
    :goto_1
    iget-object v4, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->q:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9XE;

    iget-object v4, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v8

    const-string v11, "redesign_albums_list"

    move v10, v3

    invoke-virtual/range {v5 .. v12}, LX/9XE;->a(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    .line 2446564
    iget-object v4, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v5

    .line 2446565
    iget-object v4, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->p:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8I0;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/8I0;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2446566
    const-string v4, "extra_album_selected"

    invoke-static {v6, v4, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2446567
    const-string v4, "extra_photo_tab_mode_params"

    sget-object v7, LX/5SD;->VIEWING_MODE:LX/5SD;

    iget-object v8, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v8, v8, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2446568
    iget-object v9, v8, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v8, v9

    .line 2446569
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2446570
    const-string v4, "is_page"

    invoke-virtual {v6, v4, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2446571
    const-string v4, "owner_id"

    iget-object v7, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v7, v7, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2446572
    const-string v4, "pick_hc_pic"

    invoke-virtual {v6, v4, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2446573
    const-string v4, "pick_pic_lite"

    invoke-virtual {v6, v4, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2446574
    const-string v4, "disable_adding_photos_to_albums"

    invoke-virtual {v6, v4, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2446575
    iget-object v4, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-static {v4}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->l(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    .line 2446576
    if-eqz v4, :cond_1

    .line 2446577
    const-string v7, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2446578
    iget-object v4, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->F:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->F:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2446579
    const-string v4, "extra_pages_admin_permissions"

    iget-object v7, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v7, v7, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->F:Ljava/util/ArrayList;

    invoke-virtual {v6, v4, v7}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2446580
    :cond_0
    const-string v4, "extra_composer_target_data"

    iget-object v7, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v7, v7, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->G:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v6, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2446581
    :cond_1
    iget-object v4, v1, LX/HH2;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->o:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v6, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2446582
    const v1, -0x10c455f8

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2446583
    :cond_2
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Set AlbumViewHolder with null onClickAlbumListener as View.OnClickListener"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    const v2, -0x2f124b79

    invoke-static {v2, v0}, LX/02F;->a(II)V

    throw v1

    :cond_3
    move v4, v12

    .line 2446584
    goto/16 :goto_0

    .line 2446585
    :cond_4
    const-string v9, ""

    goto/16 :goto_1
.end method
