.class public final LX/Hzl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525970
    iput-object p1, p0, LX/Hzl;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2525966
    const/4 v5, 0x0

    .line 2525967
    iget-object v0, p0, LX/Hzl;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    iget-object v1, p0, LX/Hzl;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0f74

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "PUBLISHED"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "CANCELED"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 2525968
    sget-object v7, LX/Hx6;->UPCOMING:LX/Hx6;

    const/16 v9, 0xa

    move-object v6, v0

    move v8, v5

    move v10, v1

    move-object v11, v2

    invoke-virtual/range {v6 .. v11}, LX/Hyx;->a(LX/Hx6;ZIILjava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2525969
    return-object v0
.end method
