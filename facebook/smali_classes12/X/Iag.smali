.class public final LX/Iag;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;)V
    .locals 0

    .prologue
    .line 2591217
    iput-object p1, p0, LX/Iag;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2591213
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2591216
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2591214
    iget-object v0, p0, LX/Iag;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->d:Lcom/facebook/widget/text/BetterButton;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->b(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2591215
    return-void
.end method
