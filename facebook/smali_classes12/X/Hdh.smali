.class public LX/Hdh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/3GL;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3GL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2488912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2488913
    iput-object p1, p0, LX/Hdh;->a:Landroid/content/Context;

    .line 2488914
    iput-object p2, p0, LX/Hdh;->b:LX/3GL;

    .line 2488915
    return-void
.end method

.method public static a(LX/0QB;)LX/Hdh;
    .locals 5

    .prologue
    .line 2488930
    const-class v1, LX/Hdh;

    monitor-enter v1

    .line 2488931
    :try_start_0
    sget-object v0, LX/Hdh;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2488932
    sput-object v2, LX/Hdh;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2488933
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2488934
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2488935
    new-instance p0, LX/Hdh;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/3GL;->b(LX/0QB;)LX/3GL;

    move-result-object v4

    check-cast v4, LX/3GL;

    invoke-direct {p0, v3, v4}, LX/Hdh;-><init>(Landroid/content/Context;LX/3GL;)V

    .line 2488936
    move-object v0, p0

    .line 2488937
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2488938
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hdh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2488939
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2488940
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2488916
    invoke-virtual {p1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "One source is needed to form the sentence"

    invoke-static {v0, v3}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 2488917
    iget-object v0, p0, LX/Hdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2488918
    invoke-virtual {p1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    .line 2488919
    invoke-virtual {p1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->a()I

    move-result v6

    .line 2488920
    const/4 v0, 0x3

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 2488921
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    .line 2488922
    :goto_1
    if-ge v3, v7, :cond_1

    .line 2488923
    invoke-virtual {p1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;->a()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2488924
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 2488925
    goto :goto_0

    .line 2488926
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    .line 2488927
    if-ge v0, v5, :cond_2

    .line 2488928
    const v3, 0x7f0f016f

    sub-int v5, v6, v0

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v9, p0, LX/Hdh;->b:LX/3GL;

    invoke-virtual {v9, v8}, LX/3GL;->d(Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    sub-int v0, v6, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-virtual {v4, v3, v5, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2488929
    :goto_2
    return-object v0

    :cond_2
    const v0, 0x7f083672

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, LX/Hdh;->b:LX/3GL;

    invoke-virtual {v3, v8}, LX/3GL;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v4, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
