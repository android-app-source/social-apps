.class public final LX/ILi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V
    .locals 0

    .prologue
    .line 2568265
    iput-object p1, p0, LX/ILi;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2568266
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2568267
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2568268
    iget-object v0, p0, LX/ILi;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v2, v0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->l:Lcom/facebook/fig/button/FigButton;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2568269
    iget-object v0, p0, LX/ILi;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->m:Landroid/view/View;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2568270
    return-void

    :cond_0
    move v0, v1

    .line 2568271
    goto :goto_0

    .line 2568272
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method
