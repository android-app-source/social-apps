.class public LX/Hs4;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hs3;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Hs4;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2513978
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hs4;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2513950
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2513951
    iput-object p1, p0, LX/Hs4;->b:LX/0Ot;

    .line 2513952
    return-void
.end method

.method public static a(LX/0QB;)LX/Hs4;
    .locals 4

    .prologue
    .line 2513965
    sget-object v0, LX/Hs4;->c:LX/Hs4;

    if-nez v0, :cond_1

    .line 2513966
    const-class v1, LX/Hs4;

    monitor-enter v1

    .line 2513967
    :try_start_0
    sget-object v0, LX/Hs4;->c:LX/Hs4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2513968
    if-eqz v2, :cond_0

    .line 2513969
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2513970
    new-instance v3, LX/Hs4;

    const/16 p0, 0x195d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hs4;-><init>(LX/0Ot;)V

    .line 2513971
    move-object v0, v3

    .line 2513972
    sput-object v0, LX/Hs4;->c:LX/Hs4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2513973
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2513974
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2513975
    :cond_1
    sget-object v0, LX/Hs4;->c:LX/Hs4;

    return-object v0

    .line 2513976
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2513977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2513955
    check-cast p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    .line 2513956
    iget-object v0, p0, LX/Hs4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

    iget-object v2, p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->b:Ljava/lang/String;

    iget-object v4, p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->c:Ljava/lang/String;

    iget-object v5, p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->d:Ljava/lang/String;

    iget-object v6, p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->e:Ljava/lang/String;

    iget-object v7, p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->f:Ljava/lang/Boolean;

    iget-object v8, p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->g:LX/0QK;

    iget-object v9, p2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->h:Lcom/facebook/java2js/JSContext;

    move-object v1, p1

    .line 2513957
    new-instance v10, LX/5KK;

    invoke-direct {v10, v9}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string v11, "defaultAlbumType"

    const-string v12, ""

    invoke-virtual {v10, v11, v12}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string v11, "publisherTargetType"

    invoke-virtual {v10, v11, v4}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string v11, "shouldHideSharedAlbums"

    invoke-virtual {v10, v11, v7}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    invoke-virtual {v10}, LX/5KK;->a()LX/0P1;

    move-result-object v10

    .line 2513958
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    invoke-static {v1}, LX/5KZ;->c(LX/1De;)LX/5KX;

    move-result-object v12

    invoke-virtual {v12, v9}, LX/5KX;->a(Lcom/facebook/java2js/JSContext;)LX/5KX;

    move-result-object v12

    iget-object p0, v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->b:LX/5KP;

    invoke-virtual {p0, v1, v9}, LX/5KP;->a(LX/1De;Lcom/facebook/java2js/JSContext;)LX/5KO;

    move-result-object p0

    invoke-virtual {v12, p0}, LX/5KX;->a(Ljava/lang/Object;)LX/5KX;

    move-result-object v12

    const-string p0, "CSComposerAlbumPicker"

    invoke-virtual {v12, p0}, LX/5KX;->b(Ljava/lang/String;)LX/5KX;

    move-result-object v12

    new-instance p0, LX/5KK;

    invoke-direct {p0, v9}, LX/5KK;-><init>(Lcom/facebook/java2js/JSContext;)V

    const-string p1, "filterConfiguration"

    invoke-virtual {p0, p1, v10}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string p0, "ownerFBID"

    invoke-virtual {v10, p0, v3}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string p0, "targetType"

    invoke-virtual {v10, p0, v4}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string p0, "albumMediaOwnerType"

    const-string p1, "unknown"

    invoke-virtual {v10, p0, p1}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string p0, "selectedAlbumFBID"

    invoke-virtual {v10, p0, v5}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string p0, "selectionHandler"

    new-instance p1, LX/Hs5;

    iget-object p2, v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->c:LX/0lB;

    invoke-direct {p1, v0, p2, v8}, LX/Hs5;-><init>(Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;LX/0lB;LX/0QK;)V

    invoke-virtual {v10, p0, p1}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string p0, "coverPhotoPlaceholderImage"

    const p1, 0x7f020626

    invoke-static {p1}, LX/1be;->a(I)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v10, p0, p1}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string p0, "style"

    const-string p1, "material"

    invoke-virtual {v10, p0, p1}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    const-string p0, "toolbox"

    iget-object p1, v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->d:LX/Hpc;

    .line 2513959
    new-instance v1, LX/Hpb;

    invoke-direct {v1, v2, v6}, LX/Hpb;-><init>(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)V

    .line 2513960
    invoke-static {p1}, LX/1QO;->b(LX/0QB;)LX/1QO;

    move-result-object p2

    check-cast p2, LX/1QO;

    invoke-static {p1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 2513961
    iput-object p2, v1, LX/Hpb;->c:LX/1QO;

    iput-object v0, v1, LX/Hpb;->d:LX/1Ad;

    .line 2513962
    move-object p1, v1

    .line 2513963
    invoke-virtual {v10, p0, p1}, LX/5KK;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;

    move-result-object v10

    invoke-virtual {v10}, LX/5KK;->a()LX/0P1;

    move-result-object v10

    invoke-virtual {v12, v10}, LX/5KX;->b(Ljava/lang/Object;)LX/5KX;

    move-result-object v10

    invoke-interface {v11, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v10

    invoke-interface {v10}, LX/1Di;->k()LX/1Dg;

    move-result-object v10

    move-object v0, v10

    .line 2513964
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2513953
    invoke-static {}, LX/1dS;->b()V

    .line 2513954
    const/4 v0, 0x0

    return-object v0
.end method
