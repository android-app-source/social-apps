.class public LX/ImJ;
.super LX/ImH;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/Izl;

.field private final b:LX/IzJ;

.field private final c:LX/J0B;

.field private final d:LX/Duh;

.field private final e:LX/ImB;

.field private final f:LX/Izp;

.field public final g:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609437
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImJ;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Izl;LX/IzJ;LX/J0B;LX/Duh;LX/ImB;LX/Izp;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609428
    invoke-direct {p0}, LX/ImH;-><init>()V

    .line 2609429
    iput-object p1, p0, LX/ImJ;->a:LX/Izl;

    .line 2609430
    iput-object p2, p0, LX/ImJ;->b:LX/IzJ;

    .line 2609431
    iput-object p3, p0, LX/ImJ;->c:LX/J0B;

    .line 2609432
    iput-object p4, p0, LX/ImJ;->d:LX/Duh;

    .line 2609433
    iput-object p5, p0, LX/ImJ;->e:LX/ImB;

    .line 2609434
    iput-object p6, p0, LX/ImJ;->f:LX/Izp;

    .line 2609435
    iput-object p7, p0, LX/ImJ;->g:LX/0Zb;

    .line 2609436
    return-void
.end method

.method public static a(LX/0QB;)LX/ImJ;
    .locals 15

    .prologue
    .line 2609399
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609400
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609401
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609402
    if-nez v1, :cond_0

    .line 2609403
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609404
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609405
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609406
    sget-object v1, LX/ImJ;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609407
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609408
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609409
    :cond_1
    if-nez v1, :cond_4

    .line 2609410
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609411
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609412
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609413
    new-instance v7, LX/ImJ;

    invoke-static {v0}, LX/Izl;->a(LX/0QB;)LX/Izl;

    move-result-object v8

    check-cast v8, LX/Izl;

    invoke-static {v0}, LX/IzJ;->a(LX/0QB;)LX/IzJ;

    move-result-object v9

    check-cast v9, LX/IzJ;

    invoke-static {v0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v10

    check-cast v10, LX/J0B;

    invoke-static {v0}, LX/Duh;->a(LX/0QB;)LX/Duh;

    move-result-object v11

    check-cast v11, LX/Duh;

    invoke-static {v0}, LX/ImB;->a(LX/0QB;)LX/ImB;

    move-result-object v12

    check-cast v12, LX/ImB;

    invoke-static {v0}, LX/Izp;->a(LX/0QB;)LX/Izp;

    move-result-object v13

    check-cast v13, LX/Izp;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v14

    check-cast v14, LX/0Zb;

    invoke-direct/range {v7 .. v14}, LX/ImJ;-><init>(LX/Izl;LX/IzJ;LX/J0B;LX/Duh;LX/ImB;LX/Izp;LX/0Zb;)V

    .line 2609414
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609415
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609416
    if-nez v1, :cond_2

    .line 2609417
    sget-object v0, LX/ImJ;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImJ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609418
    :goto_1
    if-eqz v0, :cond_3

    .line 2609419
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609420
    :goto_3
    check-cast v0, LX/ImJ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609421
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609422
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609423
    :catchall_1
    move-exception v0

    .line 2609424
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609425
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609426
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609427
    :cond_2
    :try_start_8
    sget-object v0, LX/ImJ;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImJ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(Ljava/lang/Long;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2609322
    if-nez p0, :cond_0

    .line 2609323
    const/4 v0, 0x0

    .line 2609324
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/ImJ;LX/Ipl;)Z
    .locals 2

    .prologue
    .line 2609398
    iget-object v0, p0, LX/ImJ;->d:LX/Duh;

    iget-object v1, p1, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Duh;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/7GJ;)Landroid/os/Bundle;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2609336
    iget-object v0, p1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v0}, LX/Ipt;->c()LX/Ipl;

    move-result-object v1

    .line 2609337
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2609338
    iget-object v2, v1, LX/Ipl;->themeId:Ljava/lang/Long;

    if-nez v2, :cond_0

    iget-object v2, v1, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, v1, LX/Ipl;->platformItemId:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 2609339
    :cond_0
    :try_start_0
    iget-object v2, p0, LX/ImJ;->e:LX/ImB;

    iget-object v1, v1, LX/Ipl;->transferFbId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/ImB;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2609340
    :goto_0
    return-object v0

    .line 2609341
    :cond_1
    iget-object v2, p0, LX/ImJ;->f:LX/Izp;

    iget-object v3, v1, LX/Ipl;->senderFbId:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Izp;->b(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v2

    .line 2609342
    iget-object v3, p0, LX/ImJ;->f:LX/Izp;

    iget-object v4, v1, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Izp;->b(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v3

    .line 2609343
    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    .line 2609344
    :cond_2
    :try_start_1
    iget-object v2, p0, LX/ImJ;->e:LX/ImB;

    iget-object v1, v1, LX/Ipl;->transferFbId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/ImB;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0

    .line 2609345
    :cond_3
    new-instance v5, Lcom/facebook/payments/p2p/model/Sender;

    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v7

    invoke-direct {v5, v4, v6, v7}, Lcom/facebook/payments/p2p/model/Sender;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2609346
    new-instance v6, Lcom/facebook/payments/p2p/model/Receiver;

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v8

    invoke-direct {v6, v4, v7, v8}, Lcom/facebook/payments/p2p/model/Receiver;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2609347
    invoke-static {p0, v1}, LX/ImJ;->b(LX/ImJ;LX/Ipl;)Z

    move-result v4

    .line 2609348
    if-eqz v4, :cond_7

    .line 2609349
    iget-object v7, v1, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    .line 2609350
    sget-object v8, LX/Iq6;->b:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, LX/DtQ;->fromString(Ljava/lang/String;)LX/DtQ;

    move-result-object v7

    .line 2609351
    :goto_1
    move-object v7, v7

    .line 2609352
    sget-object v4, LX/DtQ;->R_COMPLETED:LX/DtQ;

    if-eq v7, v4, :cond_4

    sget-object v4, LX/DtQ;->S_COMPLETED:LX/DtQ;

    if-ne v7, v4, :cond_6

    :cond_4
    iget-object v4, v1, LX/Ipl;->timestampMs:Ljava/lang/Long;

    invoke-static {v4}, LX/ImJ;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v4

    .line 2609353
    :goto_2
    new-instance v8, Lcom/facebook/payments/p2p/model/Amount;

    iget-object v9, v1, LX/Ipl;->currency:Ljava/lang/String;

    iget-object v10, v1, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iget-object p1, v1, LX/Ipl;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    move-result p1

    invoke-direct {v8, v9, v10, p1}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    .line 2609354
    new-instance v9, LX/Dtx;

    invoke-direct {v9}, LX/Dtx;-><init>()V

    iget-object v10, v1, LX/Ipl;->memoText:Ljava/lang/String;

    .line 2609355
    iput-object v10, v9, LX/Dtx;->a:Ljava/lang/String;

    .line 2609356
    move-object v9, v9

    .line 2609357
    invoke-virtual {v9}, LX/Dtx;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v9

    .line 2609358
    invoke-static {}, Lcom/facebook/payments/p2p/model/PaymentTransaction;->newBuilder()LX/DtJ;

    move-result-object v10

    iget-object p1, v1, LX/Ipl;->transferFbId:Ljava/lang/Long;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2609359
    iput-object p1, v10, LX/DtJ;->a:Ljava/lang/String;

    .line 2609360
    move-object v10, v10

    .line 2609361
    iput-object v5, v10, LX/DtJ;->c:Lcom/facebook/payments/p2p/model/Sender;

    .line 2609362
    move-object v5, v10

    .line 2609363
    iput-object v6, v5, LX/DtJ;->d:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2609364
    move-object v5, v5

    .line 2609365
    iget-object v6, v1, LX/Ipl;->timestampMs:Ljava/lang/Long;

    invoke-static {v6}, LX/ImJ;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v6

    .line 2609366
    iput-object v6, v5, LX/DtJ;->e:Ljava/lang/String;

    .line 2609367
    move-object v5, v5

    .line 2609368
    iget-object v6, v1, LX/Ipl;->timestampMs:Ljava/lang/Long;

    invoke-static {v6}, LX/ImJ;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v6

    .line 2609369
    iput-object v6, v5, LX/DtJ;->h:Ljava/lang/String;

    .line 2609370
    move-object v5, v5

    .line 2609371
    iput-object v4, v5, LX/DtJ;->g:Ljava/lang/String;

    .line 2609372
    move-object v4, v5

    .line 2609373
    iput-object v7, v4, LX/DtJ;->f:LX/DtQ;

    .line 2609374
    move-object v4, v4

    .line 2609375
    iput-object v8, v4, LX/DtJ;->i:Lcom/facebook/payments/p2p/model/Amount;

    .line 2609376
    move-object v4, v4

    .line 2609377
    iput-object v9, v4, LX/DtJ;->k:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2609378
    move-object v4, v4

    .line 2609379
    iget-object v5, v1, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    if-eqz v5, :cond_5

    .line 2609380
    new-instance v5, Lcom/facebook/payments/p2p/model/Amount;

    iget-object v6, v1, LX/Ipl;->currency:Ljava/lang/String;

    iget-object v7, v1, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, v1, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->intValue()I

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    .line 2609381
    iput-object v5, v4, LX/DtJ;->j:Lcom/facebook/payments/p2p/model/Amount;

    .line 2609382
    :cond_5
    invoke-virtual {v4}, LX/DtJ;->o()Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v4

    move-object v2, v4

    .line 2609383
    iget-object v3, p0, LX/ImJ;->a:LX/Izl;

    invoke-virtual {v3, v2}, LX/Izl;->b(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2609384
    iget-object v3, p0, LX/ImJ;->a:LX/Izl;

    invoke-virtual {v3, v2}, LX/Izl;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2609385
    iget-object v3, p0, LX/ImJ;->c:LX/J0B;

    .line 2609386
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 2609387
    const-string v5, "com.facebook.messaging.payment.ACTION_NEW_TRANSFER"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2609388
    invoke-static {v3, v4}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2609389
    invoke-static {p0, v1}, LX/ImJ;->b(LX/ImJ;LX/Ipl;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "p2p_receive"

    .line 2609390
    :goto_3
    iget-object v4, p0, LX/ImJ;->g:LX/0Zb;

    const-string v5, "p2p_sync_delta"

    invoke-static {v5, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v3

    const-string v5, "DeltaNewTransfer"

    invoke-virtual {v3, v5}, LX/5fz;->j(Ljava/lang/String;)LX/5fz;

    move-result-object v3

    iget-object v5, v1, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v3, v5}, LX/5fz;->a(Ljava/lang/Long;)LX/5fz;

    move-result-object v3

    .line 2609391
    iget-object v5, v3, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v3, v5

    .line 2609392
    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2609393
    const-string v1, "newPaymentTransaction"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_0

    :catch_1
    goto/16 :goto_0

    .line 2609394
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 2609395
    :cond_7
    iget-object v7, v1, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    .line 2609396
    sget-object v8, LX/Iq8;->b:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, LX/DtQ;->fromString(Ljava/lang/String;)LX/DtQ;

    move-result-object v7

    goto/16 :goto_1

    .line 2609397
    :cond_8
    const-string v3, "p2p_send"

    goto :goto_3
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2609325
    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v0}, LX/Ipt;->c()LX/Ipl;

    move-result-object v1

    .line 2609326
    const-string v0, "newPaymentTransaction"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2609327
    if-eqz v0, :cond_1

    .line 2609328
    iget-object v2, p0, LX/ImJ;->b:LX/IzJ;

    invoke-virtual {v2, v0}, LX/IzJ;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2609329
    iget-object v2, v1, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 2609330
    iget-object v2, p0, LX/ImJ;->b:LX/IzJ;

    iget-object v1, v1, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5, v0}, LX/IzJ;->a(JLcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2609331
    :cond_0
    iget-object v1, p0, LX/ImJ;->c:LX/J0B;

    .line 2609332
    iget-object v2, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v2, v2

    .line 2609333
    iget-object v3, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2609334
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/J0B;->a(LX/DtQ;J)V

    .line 2609335
    :cond_1
    return-void
.end method
