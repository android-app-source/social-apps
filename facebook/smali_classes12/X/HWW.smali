.class public LX/HWW;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/1Uf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/TextView;

.field public c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public e:Landroid/widget/TextView;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public h:Landroid/view/View;

.field public i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2476419
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2476420
    const-class v0, LX/HWW;

    invoke-static {v0, p0}, LX/HWW;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2476421
    const v0, 0x7f030e89

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2476422
    const v0, 0x7f0d239e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HWW;->b:Landroid/widget/TextView;

    .line 2476423
    const v0, 0x7f0d239f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2476424
    const v0, 0x7f0d23a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/HWW;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2476425
    const v0, 0x7f0d239d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/HWW;->h:Landroid/view/View;

    .line 2476426
    invoke-virtual {p0}, LX/HWW;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, LX/HWW;->j:I

    .line 2476427
    new-instance v0, Landroid/util/SparseArray;

    const/4 p1, 0x2

    invoke-direct {v0, p1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, LX/HWW;->i:Landroid/util/SparseArray;

    .line 2476428
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/HWW;

    invoke-static {p0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object p0

    check-cast p0, LX/1Uf;

    iput-object p0, p1, LX/HWW;->a:LX/1Uf;

    return-void
.end method


# virtual methods
.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2476429
    iget v0, p0, LX/HWW;->j:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 2476430
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, LX/HWW;->j:I

    .line 2476431
    :cond_0
    return-void
.end method
