.class public LX/Iii;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iis;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iiq;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iir;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iip;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/Iis;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Iiq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Iir;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Iip;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605011
    iput-object p1, p0, LX/Iii;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2605012
    iput-object p2, p0, LX/Iii;->b:LX/0Ot;

    .line 2605013
    iput-object p3, p0, LX/Iii;->c:LX/0Ot;

    .line 2605014
    iput-object p4, p0, LX/Iii;->d:LX/0Ot;

    .line 2605015
    iput-object p5, p0, LX/Iii;->e:LX/0Ot;

    .line 2605016
    return-void
.end method

.method public static b(LX/0QB;)LX/Iii;
    .locals 6

    .prologue
    .line 2605017
    new-instance v0, LX/Iii;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v2, 0x2855

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2853

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2854

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2852

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Iii;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2605018
    return-object v0
.end method

.method public static c(LX/Iii;LX/Iiv;)LX/Iio;
    .locals 2

    .prologue
    .line 2605019
    sget-object v0, LX/Iih;->a:[I

    invoke-virtual {p1}, LX/Iiv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2605020
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid payment awareness nux type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2605021
    :pswitch_0
    iget-object v0, p0, LX/Iii;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iio;

    .line 2605022
    :goto_0
    return-object v0

    .line 2605023
    :pswitch_1
    iget-object v0, p0, LX/Iii;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iio;

    goto :goto_0

    .line 2605024
    :pswitch_2
    iget-object v0, p0, LX/Iii;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iio;

    goto :goto_0

    .line 2605025
    :pswitch_3
    iget-object v0, p0, LX/Iii;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iio;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
