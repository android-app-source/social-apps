.class public final LX/Hsv;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/feedattachment/GifAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/feedattachment/GifAttachmentView;)V
    .locals 0

    .prologue
    .line 2515183
    iput-object p1, p0, LX/Hsv;->a:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2515176
    check-cast p2, LX/1ln;

    .line 2515177
    if-nez p2, :cond_1

    .line 2515178
    :cond_0
    :goto_0
    return-void

    .line 2515179
    :cond_1
    if-eqz p3, :cond_2

    .line 2515180
    invoke-interface {p3}, Landroid/graphics/drawable/Animatable;->start()V

    .line 2515181
    :cond_2
    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2515182
    iget-object v0, p0, LX/Hsv;->a:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    goto :goto_0
.end method
