.class public LX/Hv5;
.super LX/Hs6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsStorylineSlideshowSupported;",
        ":",
        "LX/5RE;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/Hs6;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:LX/Hr2;

.field private final d:LX/Hu0;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0il;LX/Hr2;)V
    .locals 2
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "TServices;",
            "LX/Hr2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2518255
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2518256
    iput-object p1, p0, LX/Hv5;->a:Landroid/content/res/Resources;

    .line 2518257
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hv5;->b:Ljava/lang/ref/WeakReference;

    .line 2518258
    iput-object p3, p0, LX/Hv5;->c:LX/Hr2;

    .line 2518259
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v0

    const v1, 0x7f020874

    .line 2518260
    iput v1, v0, LX/Htz;->a:I

    .line 2518261
    move-object v0, v0

    .line 2518262
    const v1, 0x7f0a07aa

    .line 2518263
    iput v1, v0, LX/Htz;->f:I

    .line 2518264
    move-object v0, v0

    .line 2518265
    const v1, 0x7f08289a

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2518266
    iput-object v1, v0, LX/Htz;->b:Ljava/lang/String;

    .line 2518267
    move-object v0, v0

    .line 2518268
    invoke-virtual {p0}, LX/Hv5;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2518269
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2518270
    move-object v0, v0

    .line 2518271
    iget-object v1, p0, LX/Hv5;->c:LX/Hr2;

    .line 2518272
    iput-object v1, v0, LX/Htz;->e:LX/Hr2;

    .line 2518273
    move-object v0, v0

    .line 2518274
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/Hv5;->d:LX/Hu0;

    .line 2518275
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2518288
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2518287
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2518286
    iget-object v0, p0, LX/Hv5;->a:Landroid/content/res/Resources;

    const v1, 0x7f083985

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2518285
    iget-object v0, p0, LX/Hv5;->d:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 2518278
    iget-object v0, p0, LX/Hv5;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 2518279
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2zG;

    check-cast v1, LX/5RE;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    .line 2518280
    sget-object v2, LX/5RF;->STORYLINE:LX/5RF;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/5RF;->CHECKIN:LX/5RF;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/5RF;->MINUTIAE:LX/5RF;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    if-ne v1, v2, :cond_1

    :cond_0
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    .line 2518281
    iget-object v1, v0, LX/2zG;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/APD;

    invoke-virtual {v0}, LX/2zG;->H()Z

    move-result v2

    .line 2518282
    if-eqz v2, :cond_2

    iget-object v0, v1, LX/APD;->f:LX/1kZ;

    invoke-virtual {v0}, LX/1kZ;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 2518283
    move v0, v1

    .line 2518284
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2518277
    iget-object v0, p0, LX/Hv5;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v1, LX/5RF;->STORYLINE:LX/5RF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2518276
    sget-object v0, LX/Hty;->STORYLINE:LX/Hty;

    return-object v0
.end method
