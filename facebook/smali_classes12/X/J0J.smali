.class public LX/J0J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/70U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/70U",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636830
    return-void
.end method


# virtual methods
.method public final a()LX/6zQ;
    .locals 1

    .prologue
    .line 2636831
    sget-object v0, LX/6zQ;->NEW_MANUAL_TRANSFER:LX/6zQ;

    return-object v0
.end method

.method public final a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
    .locals 5

    .prologue
    .line 2636832
    const/4 v1, 0x0

    .line 2636833
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2636834
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2636835
    invoke-static {v0}, LX/6zQ;->forValue(Ljava/lang/String;)LX/6zQ;

    move-result-object v0

    sget-object v2, LX/6zQ;->NEW_MANUAL_TRANSFER:LX/6zQ;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2636836
    const-string v0, "title"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 2636837
    const-string v0, "manual_transfer_info"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2636838
    const-string v0, "manual_transfer_info"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2636839
    const-string v1, "description"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 2636840
    const-string v1, "transfer_option_id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 2636841
    const-string v4, "invoice_id"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2636842
    :goto_1
    new-instance v4, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 2636843
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v0, v1

    move-object v2, v1

    goto :goto_1
.end method
