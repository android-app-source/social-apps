.class public LX/J3X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qa;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field private final b:LX/6r5;

.field public c:LX/6qb;

.field private d:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/6r5;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2643001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643002
    iput-object p1, p0, LX/J3X;->a:Ljava/util/concurrent/Executor;

    .line 2643003
    iput-object p2, p0, LX/J3X;->b:LX/6r5;

    .line 2643004
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/shipping/model/SimpleShippingOption;
    .locals 1

    .prologue
    .line 2643005
    new-instance v0, LX/72l;

    invoke-direct {v0}, LX/72l;-><init>()V

    move-object v0, v0

    .line 2643006
    iput-object p0, v0, LX/72l;->a:Ljava/lang/String;

    .line 2643007
    move-object v0, v0

    .line 2643008
    iput-object p1, v0, LX/72l;->b:Ljava/lang/String;

    .line 2643009
    move-object v0, v0

    .line 2643010
    new-instance p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;

    invoke-direct {p0, v0}, Lcom/facebook/payments/shipping/model/SimpleShippingOption;-><init>(LX/72l;)V

    move-object v0, p0

    .line 2643011
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 2643012
    iget-object v0, p0, LX/J3X;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2643013
    iget-object v0, p0, LX/J3X;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2643014
    :goto_0
    return-object v0

    .line 2643015
    :cond_0
    iget-object v0, p0, LX/J3X;->b:LX/6r5;

    invoke-virtual {v0, p1}, LX/6r5;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2643016
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2643017
    const-string v2, "1"

    const-string p1, "FedEx - teleport items"

    invoke-static {v2, p1}, LX/J3X;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/shipping/model/SimpleShippingOption;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2643018
    const-string v2, "2"

    const-string p1, "USPS - 2 day delivery"

    invoke-static {v2, p1}, LX/J3X;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/shipping/model/SimpleShippingOption;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2643019
    const-string v2, "3"

    const-string p1, "By road"

    invoke-static {v2, p1}, LX/J3X;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/shipping/model/SimpleShippingOption;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2643020
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2643021
    new-instance p1, LX/J3W;

    invoke-direct {p1, p0, v1}, LX/J3W;-><init>(LX/J3X;LX/0Pz;)V

    iget-object v1, p0, LX/J3X;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, p1, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2643022
    move-object v1, v2

    .line 2643023
    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/J3X;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2643024
    iget-object v0, p0, LX/J3X;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final a(LX/6qb;)V
    .locals 2

    .prologue
    .line 2643025
    iput-object p1, p0, LX/J3X;->c:LX/6qb;

    .line 2643026
    iget-object v0, p0, LX/J3X;->b:LX/6r5;

    iget-object v1, p0, LX/J3X;->c:LX/6qb;

    invoke-virtual {v0, v1}, LX/6r5;->a(LX/6qb;)V

    .line 2643027
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2643028
    iget-object v0, p0, LX/J3X;->b:LX/6r5;

    invoke-virtual {v0}, LX/6r5;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/J3X;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
