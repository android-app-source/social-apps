.class public final LX/Hlu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private a:I

.field private b:F

.field private c:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2499538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2499539
    const/4 v0, 0x0

    iput v0, p0, LX/Hlu;->a:I

    .line 2499540
    iput v1, p0, LX/Hlu;->b:F

    .line 2499541
    iput v1, p0, LX/Hlu;->c:F

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()F
    .locals 3

    .prologue
    .line 2499542
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/Hlu;->b:F

    iget v1, p0, LX/Hlu;->a:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 2499543
    iget v1, p0, LX/Hlu;->c:F

    iget v2, p0, LX/Hlu;->a:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v0

    sub-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    double-to-float v0, v0

    monitor-exit p0

    return v0

    .line 2499544
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 2499545
    return-void
.end method

.method public final declared-synchronized onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4

    .prologue
    .line 2499546
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/Hlu;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Hlu;->a:I

    .line 2499547
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 2499548
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 2499549
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    .line 2499550
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    .line 2499551
    iget v1, p0, LX/Hlu;->c:F

    add-float/2addr v1, v0

    iput v1, p0, LX/Hlu;->c:F

    .line 2499552
    iget v1, p0, LX/Hlu;->b:F

    float-to-double v2, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, LX/Hlu;->b:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2499553
    monitor-exit p0

    return-void

    .line 2499554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
