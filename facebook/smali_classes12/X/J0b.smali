.class public LX/J0b;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentPlatformContext;",
        ">;>;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J0b;


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637375
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2637376
    return-void
.end method

.method public static a(LX/0QB;)LX/J0b;
    .locals 4

    .prologue
    .line 2637377
    sget-object v0, LX/J0b;->b:LX/J0b;

    if-nez v0, :cond_1

    .line 2637378
    const-class v1, LX/J0b;

    monitor-enter v1

    .line 2637379
    :try_start_0
    sget-object v0, LX/J0b;->b:LX/J0b;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637380
    if-eqz v2, :cond_0

    .line 2637381
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2637382
    new-instance p0, LX/J0b;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-direct {p0, v3}, LX/J0b;-><init>(LX/0sO;)V

    .line 2637383
    move-object v0, p0

    .line 2637384
    sput-object v0, LX/J0b;->b:LX/J0b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637385
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637386
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637387
    :cond_1
    sget-object v0, LX/J0b;->b:LX/J0b;

    return-object v0

    .line 2637388
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637389
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2637390
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentPlatformContextsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentPlatformContextsQueryModel;

    .line 2637391
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentPlatformContextsQueryModel;->a()LX/0Px;

    move-result-object v2

    .line 2637392
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2637393
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2637394
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2637395
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2637396
    :cond_0
    return-object v3
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2637397
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 2637398
    new-instance v0, LX/Dte;

    invoke-direct {v0}, LX/Dte;-><init>()V

    move-object v0, v0

    .line 2637399
    return-object v0
.end method
