.class public final LX/IS1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic b:LX/ISI;


# direct methods
.method public constructor <init>(LX/ISI;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 0

    .prologue
    .line 2577399
    iput-object p1, p0, LX/IS1;->b:LX/ISI;

    iput-object p2, p0, LX/IS1;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x48f389ec    # 498767.38f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2577400
    iget-object v0, p0, LX/IS1;->b:LX/ISI;

    iget-object v0, v0, LX/ISI;->q:LX/DOL;

    iget-object v2, p0, LX/IS1;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2577401
    invoke-static {v0}, LX/DOL;->a(LX/DOL;)Landroid/content/Intent;

    move-result-object v3

    .line 2577402
    const-string v5, "group_feed_id"

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2577403
    const-string v5, "target_fragment"

    sget-object v6, LX/0cQ;->COMMUNITY_TRENDING_STORIES_FRAGMENT:LX/0cQ;

    invoke-virtual {v6}, LX/0cQ;->ordinal()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2577404
    move-object v2, v3

    .line 2577405
    iget-object v0, p0, LX/IS1;->b:LX/ISI;

    iget-object v0, v0, LX/ISI;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2577406
    const v0, 0x56d97865

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
