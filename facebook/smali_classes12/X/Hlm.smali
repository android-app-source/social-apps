.class public final LX/Hlm;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 2499155
    iput-object p1, p0, LX/Hlm;->a:Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 2499156
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2499157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "message code is not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2499158
    :pswitch_0
    iget-object v1, p0, LX/Hlm;->a:Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 2499159
    invoke-static {v1, v0, v2}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->a$redex0(Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;Landroid/content/Intent;I)V

    .line 2499160
    :goto_0
    return-void

    .line 2499161
    :pswitch_1
    iget-object v0, p0, LX/Hlm;->a:Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;

    .line 2499162
    invoke-static {v0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->a$redex0(Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;)V

    .line 2499163
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
