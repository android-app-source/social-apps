.class public final LX/JDR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;)V
    .locals 0

    .prologue
    .line 2663982
    iput-object p1, p0, LX/JDR;->a:Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/4 v2, 0x1

    const v3, -0x5646d048

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2663983
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    .line 2663984
    iget-object v3, p0, LX/JDR;->a:Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    iget-object v3, v3, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->a:LX/1nG;

    invoke-static {v0, v3}, LX/JDA;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;LX/1nG;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2663985
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->mS_()Ljava/lang/String;

    move-result-object v4

    .line 2663986
    iget-object v0, p0, LX/JDR;->a:Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2663987
    if-eqz v0, :cond_1

    .line 2663988
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, LX/JDa;

    .line 2663989
    if-eqz v0, :cond_1

    .line 2663990
    const v5, 0x7f0d0221

    invoke-virtual {v0, v5}, LX/JDa;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 2663991
    if-eqz v0, :cond_1

    .line 2663992
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2663993
    :goto_0
    if-eqz v0, :cond_0

    .line 2663994
    invoke-static {v0}, LX/9hF;->a(Ljava/lang/String;)LX/9hE;

    move-result-object v0

    .line 2663995
    :goto_1
    sget-object v5, LX/74S;->TIMELINE_PHOTOS_ABOUT_TAB:LX/74S;

    invoke-virtual {v0, v5}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    invoke-static {v3}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v3

    .line 2663996
    iget-object v0, p0, LX/JDR;->a:Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    iget-object v4, p0, LX/JDR;->a:Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v0, v4, v3, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2663997
    const v0, -0x4e7e1e8b

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 2663998
    :cond_0
    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
