.class public final LX/IBG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/events/model/Event;

.field public final synthetic c:LX/IBH;


# direct methods
.method public constructor <init>(LX/IBH;Landroid/content/Context;Lcom/facebook/events/model/Event;)V
    .locals 0

    .prologue
    .line 2546736
    iput-object p1, p0, LX/IBG;->c:LX/IBH;

    iput-object p2, p0, LX/IBG;->a:Landroid/content/Context;

    iput-object p3, p0, LX/IBG;->b:Lcom/facebook/events/model/Event;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2546733
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2546734
    iget-object v0, p0, LX/IBG;->c:LX/IBH;

    iget-object v1, v0, LX/IBH;->a:LX/1ay;

    iget-object v0, p0, LX/IBG;->c:LX/IBH;

    iget-object v2, p0, LX/IBG;->a:Landroid/content/Context;

    iget-object v3, p0, LX/IBG;->b:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, p1, v2, v3}, LX/IBH;->a(Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/content/Context;Lcom/facebook/events/model/Event;)Landroid/content/Intent;

    move-result-object v2

    const/16 v3, 0x1f6

    iget-object v0, p0, LX/IBG;->a:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v1, v2, v3, v0}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2546735
    const/4 v0, 0x0

    return-object v0
.end method
