.class public LX/HXC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/GZj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2478584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2478585
    iput-object p1, p0, LX/HXC;->c:LX/0ad;

    .line 2478586
    return-void
.end method

.method public static a(LX/0QB;)LX/HXC;
    .locals 1

    .prologue
    .line 2478579
    invoke-static {p0}, LX/HXC;->b(LX/0QB;)LX/HXC;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/HXC;
    .locals 3

    .prologue
    .line 2478580
    new-instance v2, LX/HXC;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v2, v0}, LX/HXC;-><init>(LX/0ad;)V

    .line 2478581
    invoke-static {p0}, LX/GZj;->b(LX/0QB;)LX/GZj;

    move-result-object v0

    check-cast v0, LX/GZj;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v1

    check-cast v1, LX/01T;

    .line 2478582
    iput-object v0, v2, LX/HXC;->a:LX/GZj;

    iput-object v1, v2, LX/HXC;->b:LX/01T;

    .line 2478583
    return-object v2
.end method


# virtual methods
.method public final a(LX/9Y7;Landroid/os/Bundle;Z)Lcom/facebook/base/fragment/FbFragment;
    .locals 15

    .prologue
    .line 2478544
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478545
    invoke-interface/range {p1 .. p1}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478546
    const-string v2, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2478547
    const-string v4, "profile_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2478548
    invoke-interface/range {p1 .. p1}, LX/9Y7;->e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->REACTION_SURFACE:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    if-ne v5, v6, :cond_0

    .line 2478549
    const-string v4, "page_fragment_uuid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/os/ParcelUuid;

    .line 2478550
    if-nez v4, :cond_6

    .line 2478551
    new-instance v9, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-direct {v9, v4}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    .line 2478552
    :goto_0
    const-string v4, "extra_should_enable_related_pages_like_chaining"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "arg_should_support_cache"

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "arg_pages_surface_reaction_surface"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "arg_precreated_reaction_session_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "arg_precreated_cached_reaction_session_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v10, "extra_is_landing_fragment"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    const-string v11, "source_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "extra_page_view_referrer"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v12, "empty_view"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v14

    check-cast v14, LX/8YB;

    move/from16 v12, p3

    invoke-static/range {v2 .. v14}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->a(JZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelUuid;ZLjava/lang/String;ZLjava/lang/String;LX/8YB;)Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    move-result-object v2

    .line 2478553
    :goto_1
    return-object v2

    .line 2478554
    :cond_0
    sget-object v5, LX/HXB;->a:[I

    invoke-interface/range {p1 .. p1}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2478555
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid Pages Surface Tab Type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2478556
    :pswitch_0
    move/from16 v0, p3

    invoke-static {v2, v3, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->a(JZ)Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    move-result-object v2

    goto :goto_1

    .line 2478557
    :pswitch_1
    const-string v5, "event_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    const-string v6, "extra_ref_module"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "event_ref_mechanism"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move/from16 v8, p3

    invoke-static/range {v2 .. v8}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(JLjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    move-result-object v2

    goto :goto_1

    .line 2478558
    :pswitch_2
    const-string v5, "extra_child_locations"

    move-object/from16 v0, p2

    invoke-static {v0, v5}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    const-string v5, "extra_child_locations"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/location/Location;

    move-wide v8, v2

    move-object v10, v4

    move/from16 v11, p3

    invoke-static/range {v6 .. v11}, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->a(Ljava/util/ArrayList;Landroid/location/Location;JLjava/lang/String;Z)Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    move-result-object v2

    goto :goto_1

    .line 2478559
    :pswitch_3
    iget-object v4, p0, LX/HXC;->c:LX/0ad;

    sget-object v5, LX/0c0;->Live:LX/0c0;

    sget-short v6, LX/17g;->d:S

    const/4 v7, 0x0

    invoke-interface {v4, v5, v6, v7}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2478560
    const-string v4, "page_fragment_uuid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/os/ParcelUuid;

    const-string v5, "source_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move/from16 v0, p3

    invoke-static {v2, v3, v4, v5, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(JLandroid/os/ParcelUuid;Ljava/lang/String;Z)Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478561
    :cond_1
    move/from16 v0, p3

    invoke-static {v2, v3, v0}, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->a(JZ)Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478562
    :pswitch_4
    if-eqz p3, :cond_2

    .line 2478563
    move/from16 v0, p3

    invoke-static {v2, v3, v4, v4, v0}, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->a(JLjava/lang/String;Ljava/lang/String;Z)Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478564
    :cond_2
    invoke-static {v2, v3, v4, v4}, Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;->a(JLjava/lang/String;Ljava/lang/String;)Lcom/facebook/reviews/ui/PageReviewsFeedFullscreenFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478565
    :pswitch_5
    const-string v5, "page_clicked_item_id_extra"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "extra_page_tab_entry_point"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move/from16 v7, p3

    invoke-static/range {v2 .. v7}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/pages/common/services/PagesServicesFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478566
    :pswitch_6
    iget-object v4, p0, LX/HXC;->a:LX/GZj;

    invoke-virtual {v4}, LX/GZj;->c()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, LX/HXC;->b:LX/01T;

    sget-object v5, LX/01T;->FB4A:LX/01T;

    if-ne v4, v5, :cond_3

    .line 2478567
    const-string v4, "product_ref_type"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, LX/7iP;

    .line 2478568
    if-nez v4, :cond_5

    .line 2478569
    sget-object v4, LX/7iP;->PAGE:LX/7iP;

    move-object v6, v4

    .line 2478570
    :goto_2
    const-string v4, "product_ref_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2478571
    if-nez v4, :cond_4

    .line 2478572
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 2478573
    :goto_3
    const-string v4, "arg_init_product_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "extra_finish_on_launch_edit_shop"

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iget-object v7, v6, LX/7iP;->value:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    move/from16 v6, p3

    move/from16 v9, p3

    invoke-static/range {v2 .. v9}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->a(JLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Z)Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478574
    :cond_3
    const-string v4, "arg_init_product_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "extra_finish_on_launch_edit_shop"

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "product_ref_type"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, LX/7iP;

    const-string v6, "product_ref_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move/from16 v6, p3

    invoke-static/range {v2 .. v8}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(JLjava/lang/String;ZZLX/7iP;Ljava/lang/Long;)Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478575
    :pswitch_7
    const-string v4, "extra_force_all_videos"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    move/from16 v0, p3

    invoke-static {v2, v3, v4, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a(JZZ)Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478576
    :pswitch_8
    invoke-static {v2, v3}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a(J)Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478577
    :pswitch_9
    invoke-static {v2, v3, v4}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a(JLjava/lang/String;)Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    move-result-object v2

    goto/16 :goto_1

    .line 2478578
    :pswitch_a
    invoke-static {v2, v3}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a(J)Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    move-result-object v2

    goto/16 :goto_1

    :cond_4
    move-object v8, v4

    goto :goto_3

    :cond_5
    move-object v6, v4

    goto/16 :goto_2

    :cond_6
    move-object v9, v4

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;Ljava/lang/String;Landroid/os/Bundle;Z)Lcom/facebook/base/fragment/FbFragment;
    .locals 1

    .prologue
    .line 2478540
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478541
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478542
    new-instance v0, LX/HXA;

    invoke-direct {v0, p0, p2, p3, p1}, LX/HXA;-><init>(LX/HXC;Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    move-object v0, v0

    .line 2478543
    invoke-virtual {p0, v0, p4, p5}, LX/HXC;->a(LX/9Y7;Landroid/os/Bundle;Z)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    return-object v0
.end method
