.class public final LX/IzY;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field private static final c:LX/0sv;

.field private static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2634666
    new-instance v0, LX/0U1;

    const-string v1, "transaction_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzY;->a:LX/0U1;

    .line 2634667
    new-instance v0, LX/0U1;

    const-string v1, "credential_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzY;->b:LX/0U1;

    .line 2634668
    new-instance v0, LX/0su;

    sget-object v1, LX/IzY;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/IzY;->c:LX/0sv;

    .line 2634669
    sget-object v0, LX/IzY;->a:LX/0U1;

    sget-object v1, LX/IzY;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IzY;->d:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2634670
    const-string v0, "transaction_payment_card_id"

    sget-object v1, LX/IzY;->d:LX/0Px;

    sget-object v2, LX/IzY;->c:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 2634671
    return-void
.end method
