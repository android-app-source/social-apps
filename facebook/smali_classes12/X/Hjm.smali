.class public LX/Hjm;
.super LX/HjX;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public final c:LX/Hl2;

.field public d:LX/Hjl;

.field private e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, LX/Hjm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hjm;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Hl2;LX/HjD;)V
    .locals 0

    invoke-direct {p0, p1, p3}, LX/HjX;-><init>(Landroid/content/Context;LX/HjD;)V

    iput-object p2, p0, LX/Hjm;->c:LX/Hl2;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 5

    iget-object v0, p0, LX/Hjm;->d:LX/Hjl;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/Hjm;->c:LX/Hl2;

    if-eqz v0, :cond_1

    const-string v0, "facebookAd.sendImpression();"

    move-object v0, v0

    invoke-static {v0}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Hjm;->c:LX/Hl2;

    iget-boolean v1, v0, LX/Hl2;->a:Z

    move v0, v1

    if-eqz v0, :cond_3

    sget-object v0, LX/Hjm;->b:Ljava/lang/String;

    const-string v1, "Webview already destroyed, cannot send impression"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    const-string v0, "evt"

    const-string v1, "native_imp"

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, LX/Hjm;->d:LX/Hjl;

    if-nez v1, :cond_4

    :cond_2
    :goto_2
    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/Hjm;->c:LX/Hl2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "facebookAd.sendImpression();"

    move-object v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Hl2;->loadUrl(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, LX/Hjm;->d:LX/Hjl;

    iget-object v2, v1, LX/Hjl;->c:Ljava/lang/String;

    move-object v1, v2

    invoke-static {v1}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, LX/Hkv;

    invoke-direct {v2, v0}, LX/Hkv;-><init>(Ljava/util/Map;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, LX/Hkv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2
.end method

.method public final declared-synchronized c()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/Hjm;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Hjm;->d:LX/Hjl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/Hjm;->e:Z

    iget-object v0, p0, LX/Hjm;->c:LX/Hl2;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hjm;->d:LX/Hjl;

    iget-object v1, v0, LX/Hjl;->b:Ljava/lang/String;

    move-object v0, v1

    invoke-static {v0}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Hjm;->c:LX/Hl2;

    new-instance v1, Lcom/facebook/ads/internal/adapters/m$1;

    invoke-direct {v1, p0}, Lcom/facebook/ads/internal/adapters/m$1;-><init>(LX/Hjm;)V

    invoke-virtual {v0, v1}, LX/Hl2;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
