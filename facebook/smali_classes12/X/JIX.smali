.class public final LX/JIX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JIZ;


# direct methods
.method public constructor <init>(LX/JIZ;)V
    .locals 0

    .prologue
    .line 2678035
    iput-object p1, p0, LX/JIX;->a:LX/JIZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2678039
    iget-object v0, p0, LX/JIX;->a:LX/JIZ;

    iget-object v0, v0, LX/JIZ;->a:LX/JIe;

    iget-object v0, v0, LX/JIe;->d:LX/03V;

    const-string v1, "discovery_curation"

    const-string v2, "Could not fetch tags to refresh"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2678040
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2678036
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678037
    iget-object v0, p0, LX/JIX;->a:LX/JIZ;

    iget-object v0, v0, LX/JIZ;->a:LX/JIe;

    iget-object v0, v0, LX/JIe;->f:LX/Eny;

    iget-object v1, p0, LX/JIX;->a:LX/JIZ;

    iget-object v1, v1, LX/JIZ;->a:LX/JIe;

    iget-object v1, v1, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/Eny;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2678038
    return-void
.end method
