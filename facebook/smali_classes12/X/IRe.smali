.class public final LX/IRe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 0

    .prologue
    .line 2576611
    iput-object p1, p0, LX/IRe;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x2aac917b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2576612
    iget-object v0, p0, LX/IRe;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ILp;

    iget-object v2, p0, LX/IRe;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/IRe;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/IRe;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v4, v4, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LX/ILp;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Landroid/content/Intent;

    move-result-object v2

    .line 2576613
    iget-object v0, p0, LX/IRe;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/IRe;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2576614
    const v0, 0x7445d024

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
