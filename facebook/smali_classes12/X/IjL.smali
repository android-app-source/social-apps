.class public LX/IjL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6y0;


# instance fields
.field private final a:LX/6yY;


# direct methods
.method public constructor <init>(LX/6yY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605664
    iput-object p1, p0, LX/IjL;->a:LX/6yY;

    .line 2605665
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605666
    iget-object v0, p0, LX/IjL;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605668
    iget-object v0, p0, LX/IjL;->a:LX/6yY;

    invoke-virtual {v0, p1, p2}, LX/6yY;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Lcom/facebook/messaging/dialog/ConfirmActionParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2605667
    iget-object v0, p0, LX/IjL;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2605660
    iget-object v0, p0, LX/IjL;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 2

    .prologue
    .line 2605661
    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    .line 2605662
    iget-boolean v1, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/IjL;->a:LX/6yY;

    invoke-virtual {v1, p1}, LX/6yY;->d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605649
    check-cast p1, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    .line 2605650
    iget-boolean v0, p1, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 3

    .prologue
    .line 2605657
    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    .line 2605658
    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    check-cast v1, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2605659
    iget-boolean v2, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    if-nez v2, :cond_0

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/PaymentCard;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605651
    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    .line 2605652
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IjL;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    goto :goto_0
.end method

.method public final h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605653
    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    .line 2605654
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IjL;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    goto :goto_0
.end method

.method public final i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605655
    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    .line 2605656
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IjL;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    goto :goto_0
.end method
