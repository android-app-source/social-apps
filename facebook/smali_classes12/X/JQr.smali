.class public LX/JQr;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQq;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2692305
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JQr;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JQu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692322
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2692323
    iput-object p1, p0, LX/JQr;->b:LX/0Ot;

    .line 2692324
    return-void
.end method

.method public static a(LX/0QB;)LX/JQr;
    .locals 4

    .prologue
    .line 2692311
    const-class v1, LX/JQr;

    monitor-enter v1

    .line 2692312
    :try_start_0
    sget-object v0, LX/JQr;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692313
    sput-object v2, LX/JQr;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692314
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692315
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692316
    new-instance v3, LX/JQr;

    const/16 p0, 0x2016

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQr;-><init>(LX/0Ot;)V

    .line 2692317
    move-object v0, v3

    .line 2692318
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692319
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692320
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692321
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2692308
    check-cast p2, LX/JQp;

    .line 2692309
    iget-object v0, p0, LX/JQr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JQu;

    iget-object v2, p2, LX/JQp;->a:LX/1Pm;

    iget-object v3, p2, LX/JQp;->b:LX/0Px;

    iget-object v4, p2, LX/JQp;->c:Ljava/lang/String;

    iget-boolean v5, p2, LX/JQp;->d:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/JQu;->a(LX/1De;LX/1Pm;LX/0Px;Ljava/lang/String;Z)LX/1Dg;

    move-result-object v0

    .line 2692310
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2692306
    invoke-static {}, LX/1dS;->b()V

    .line 2692307
    const/4 v0, 0x0

    return-object v0
.end method
