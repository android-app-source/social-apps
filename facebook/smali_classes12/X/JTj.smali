.class public LX/JTj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JTh;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JTk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2697162
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JTj;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JTk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697159
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2697160
    iput-object p1, p0, LX/JTj;->b:LX/0Ot;

    .line 2697161
    return-void
.end method

.method public static a(LX/0QB;)LX/JTj;
    .locals 4

    .prologue
    .line 2697148
    const-class v1, LX/JTj;

    monitor-enter v1

    .line 2697149
    :try_start_0
    sget-object v0, LX/JTj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697150
    sput-object v2, LX/JTj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697151
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697152
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697153
    new-instance v3, LX/JTj;

    const/16 p0, 0x2050

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JTj;-><init>(LX/0Ot;)V

    .line 2697154
    move-object v0, v3

    .line 2697155
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697156
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JTj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697157
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697158
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2697147
    const v0, 0x6c13d80d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2697110
    check-cast p2, LX/JTi;

    .line 2697111
    iget-object v0, p0, LX/JTj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JTk;

    iget-object v1, p2, LX/JTi;->a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 2697112
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x2

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x6

    const p2, 0x7f0b0f13

    invoke-interface {v2, p0, p2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x7

    const/16 p2, 0x8

    invoke-interface {v2, p0, p2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    invoke-static {v0, p1, v1}, LX/JTk;->b(LX/JTk;LX/1De;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;)LX/1Di;

    move-result-object p0

    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2697113
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object p0

    const p2, 0x7f0a0046

    invoke-virtual {p0, p2}, LX/25Q;->i(I)LX/25Q;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/4 p2, 0x4

    invoke-interface {p0, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object p0

    const/4 p2, 0x1

    invoke-interface {p0, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object p0

    move-object p0, p0

    .line 2697114
    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2697115
    iget-object p0, v0, LX/JTk;->a:LX/1vg;

    invoke-virtual {p0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p0

    invoke-static {}, LX/10A;->a()I

    move-result p2

    invoke-virtual {p0, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object p0

    const p2, 0x7f0a00e6

    invoke-virtual {p0, p2}, LX/2xv;->j(I)LX/2xv;

    move-result-object p0

    invoke-virtual {p0}, LX/1n6;->b()LX/1dc;

    move-result-object p0

    .line 2697116
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2, p0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object p0

    const/16 p2, 0x18

    invoke-interface {p0, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object p0

    const/4 p2, 0x6

    const/16 v1, 0xc

    invoke-interface {p0, p2, v1}, LX/1Di;->d(II)LX/1Di;

    move-result-object p0

    .line 2697117
    const p2, 0x6c13de1b

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2697118
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    move-object p0, p0

    .line 2697119
    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2697120
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2697121
    invoke-static {}, LX/1dS;->b()V

    .line 2697122
    iget v0, p1, LX/1dQ;->b:I

    .line 2697123
    sparse-switch v0, :sswitch_data_0

    .line 2697124
    :goto_0
    return-object v2

    .line 2697125
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2697126
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2697127
    check-cast v1, LX/JTi;

    .line 2697128
    iget-object v3, p0, LX/JTj;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/JTi;->b:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    iget-object p1, v1, LX/JTi;->a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object p2, v1, LX/JTi;->c:LX/JTg;

    .line 2697129
    if-eqz p2, :cond_0

    .line 2697130
    invoke-static {p2, v0, p1}, LX/JTg;->a(LX/JTg;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;)V

    .line 2697131
    iget-object p0, p2, LX/JTg;->c:LX/JUO;

    invoke-virtual {p0, p1, v3}, LX/JUO;->a(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2697132
    :cond_0
    goto :goto_0

    .line 2697133
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2697134
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2697135
    check-cast v1, LX/JTi;

    .line 2697136
    iget-object v3, p0, LX/JTj;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/JTi;->b:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    iget-object v4, v1, LX/JTi;->a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v5, v1, LX/JTi;->c:LX/JTg;

    .line 2697137
    if-eqz v5, :cond_2

    .line 2697138
    invoke-static {v4}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p1

    .line 2697139
    sget-object p2, LX/0ax;->ai:Ljava/lang/String;

    if-nez p1, :cond_3

    const-string p1, ""

    :goto_1
    invoke-static {p2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2697140
    sget-object p2, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_MESSAGE:LX/6VK;

    invoke-static {v4, v3}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object p0

    invoke-static {p2, p0}, LX/1g5;->a(LX/6VK;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    .line 2697141
    if-eqz p2, :cond_1

    .line 2697142
    const-string p0, "feed_type"

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v1

    invoke-virtual {p2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2697143
    const-string p0, "location_category"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v1

    invoke-virtual {p2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2697144
    :cond_1
    iget-object p0, v5, LX/JTg;->a:LX/1nA;

    invoke-virtual {p0, p1, p2}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/view/View$OnClickListener;

    move-result-object p1

    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2697145
    :cond_2
    goto :goto_0

    .line 2697146
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x6c13d80d -> :sswitch_0
        0x6c13de1b -> :sswitch_1
    .end sparse-switch
.end method
