.class public LX/I0v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/I0r;

.field private b:LX/I0g;

.field private c:LX/11R;

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:J

.field public g:J

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;>;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/44w",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/I0r;LX/I0g;LX/11R;LX/0Or;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/I0r;",
            "LX/I0g;",
            "LX/11R;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x6

    .line 2528140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2528141
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2528142
    iput-object v0, p0, LX/I0v;->l:LX/0Px;

    .line 2528143
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/I0v;->m:Ljava/util/HashMap;

    .line 2528144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/I0v;->n:Ljava/util/HashMap;

    .line 2528145
    iput-object p1, p0, LX/I0v;->a:LX/I0r;

    .line 2528146
    iput-object p2, p0, LX/I0v;->b:LX/I0g;

    .line 2528147
    iput-object p3, p0, LX/I0v;->c:LX/11R;

    .line 2528148
    iput-object p4, p0, LX/I0v;->d:LX/0Or;

    .line 2528149
    return-void
.end method

.method public static d(LX/I0v;JLX/0Px;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2528150
    const/4 v7, 0x0

    .line 2528151
    const/4 v6, 0x0

    .line 2528152
    const-wide/16 v4, 0x0

    .line 2528153
    move-object/from16 v0, p0

    iget-object v2, v0, LX/I0v;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/TimeZone;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v10

    .line 2528154
    invoke-virtual/range {p3 .. p3}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2528155
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 2528156
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, LX/I0v;->c:LX/11R;

    move-wide/from16 v0, p1

    invoke-virtual {v8, v0, v1, v4, v5}, LX/11R;->a(JJ)J

    move-result-wide v8

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 2528157
    move-wide/from16 v0, p1

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-virtual {v10, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-wide v4, v2

    .line 2528158
    :goto_0
    invoke-static {v10}, LX/I0r;->a(Ljava/util/Calendar;)V

    .line 2528159
    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/I0v;->f:J

    .line 2528160
    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    move-wide v8, v2

    move v2, v6

    move v6, v7

    .line 2528161
    :goto_1
    int-to-long v12, v6

    sget v3, LX/I0h;->a:I

    int-to-long v14, v3

    sub-long/2addr v14, v4

    cmp-long v3, v12, v14

    if-gez v3, :cond_0

    .line 2528162
    move-object/from16 v0, p0

    iget-object v3, v0, LX/I0v;->a:LX/I0r;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/I0v;->m:Ljava/util/HashMap;

    invoke-virtual {v3, v7, v8, v9}, LX/I0r;->a(Ljava/util/HashMap;J)Ljava/util/List;

    move v3, v2

    .line 2528163
    :goto_2
    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, LX/I0v;->b:LX/I0g;

    sget-object v11, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->b()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v12

    invoke-virtual {v7, v8, v9, v12, v13}, LX/I0g;->a(JJ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2528164
    move-object/from16 v0, p0

    iget-object v7, v0, LX/I0v;->n:Ljava/util/HashMap;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2528165
    :cond_0
    return-void

    .line 2528166
    :cond_1
    move-wide/from16 v0, p1

    invoke-virtual {v10, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0

    .line 2528167
    :cond_2
    move-object/from16 v0, p0

    iget-object v7, v0, LX/I0v;->a:LX/I0r;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/I0v;->m:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/I0v;->n:Ljava/util/HashMap;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    const/4 v13, 0x0

    invoke-virtual {v7, v11, v12, v2, v13}, LX/I0r;->a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;Z)V

    .line 2528168
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2528169
    :cond_3
    const/4 v2, 0x6

    const/4 v7, 0x1

    invoke-virtual {v10, v2, v7}, Ljava/util/Calendar;->add(II)V

    .line 2528170
    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 2528171
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v3

    goto :goto_1
.end method

.method public static e(LX/I0v;)V
    .locals 14

    .prologue
    const/4 v13, 0x6

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 2528172
    iput-boolean v2, p0, LX/I0v;->h:Z

    .line 2528173
    iget-object v0, p0, LX/I0v;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v3

    .line 2528174
    iget-object v0, p0, LX/I0v;->a:LX/I0r;

    invoke-virtual {v0}, LX/I0r;->a()Ljava/util/Calendar;

    move-result-object v4

    .line 2528175
    iget-wide v0, p0, LX/I0v;->g:J

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move v1, v2

    .line 2528176
    :goto_0
    if-ge v1, v13, :cond_1

    .line 2528177
    iget-object v0, p0, LX/I0v;->a:LX/I0r;

    iget-object v5, p0, LX/I0v;->m:Ljava/util/HashMap;

    iget-wide v6, p0, LX/I0v;->g:J

    invoke-virtual {v0, v5, v6, v7}, LX/I0r;->a(Ljava/util/HashMap;J)Ljava/util/List;

    move-result-object v5

    .line 2528178
    :goto_1
    iget v0, p0, LX/I0v;->e:I

    iget-object v6, p0, LX/I0v;->l:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    iget-object v6, p0, LX/I0v;->b:LX/I0g;

    iget-wide v8, p0, LX/I0v;->g:J

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, p0, LX/I0v;->l:LX/0Px;

    iget v10, p0, LX/I0v;->e:I

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->b()J

    move-result-wide v10

    invoke-virtual {v7, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    invoke-virtual {v6, v8, v9, v10, v11}, LX/I0g;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2528179
    iget-object v6, p0, LX/I0v;->a:LX/I0r;

    iget-object v7, p0, LX/I0v;->m:Ljava/util/HashMap;

    iget-object v8, p0, LX/I0v;->n:Ljava/util/HashMap;

    iget-object v0, p0, LX/I0v;->l:LX/0Px;

    iget v9, p0, LX/I0v;->e:I

    invoke-virtual {v0, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v6, v7, v8, v0, v2}, LX/I0r;->a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;Z)V

    .line 2528180
    iget v0, p0, LX/I0v;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/I0v;->e:I

    goto :goto_1

    .line 2528181
    :cond_0
    iget-wide v6, p0, LX/I0v;->g:J

    invoke-static {v5, v6, v7}, LX/I0r;->a(Ljava/util/List;J)V

    .line 2528182
    iget-object v0, p0, LX/I0v;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, LX/I0v;->e:I

    iget-object v5, p0, LX/I0v;->l:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-lt v0, v5, :cond_2

    .line 2528183
    iput-boolean v12, p0, LX/I0v;->h:Z

    .line 2528184
    :cond_1
    return-void

    .line 2528185
    :cond_2
    iget-wide v6, p0, LX/I0v;->g:J

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    .line 2528186
    add-int/lit8 v0, v1, 0x1

    .line 2528187
    :goto_2
    invoke-virtual {v3, v13, v12}, Ljava/util/Calendar;->add(II)V

    .line 2528188
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    iput-wide v6, p0, LX/I0v;->g:J

    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(JLX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2528189
    iget-boolean v0, p0, LX/I0v;->i:Z

    if-eqz v0, :cond_0

    .line 2528190
    :goto_0
    return-void

    .line 2528191
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I0v;->i:Z

    .line 2528192
    iget-object v1, p0, LX/I0v;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/TimeZone;

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v2

    .line 2528193
    invoke-virtual {v2, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528194
    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2528195
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const/4 v1, 0x0

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->b()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    .line 2528196
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528197
    :cond_1
    invoke-static {v2}, LX/I0r;->a(Ljava/util/Calendar;)V

    .line 2528198
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iput-wide v1, p0, LX/I0v;->g:J

    .line 2528199
    iget-wide v1, p0, LX/I0v;->g:J

    iput-wide v1, p0, LX/I0v;->f:J

    .line 2528200
    invoke-virtual {p0, p3}, LX/I0v;->b(LX/0Px;)V

    goto :goto_0
.end method

.method public final a(LX/7oa;)V
    .locals 5

    .prologue
    .line 2528201
    if-nez p1, :cond_0

    .line 2528202
    :goto_0
    return-void

    .line 2528203
    :cond_0
    invoke-interface {p1}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/I0v;->a(Ljava/lang/String;)V

    .line 2528204
    new-instance v0, LX/7n4;

    invoke-direct {v0}, LX/7n4;-><init>()V

    .line 2528205
    invoke-interface {p1}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7n4;->f:Ljava/lang/String;

    .line 2528206
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    iput-object p1, v0, LX/7n4;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2528207
    const-string v1, "EVENT"

    iput-object v1, v0, LX/7n4;->b:Ljava/lang/String;

    .line 2528208
    iget-object v1, p0, LX/I0v;->a:LX/I0r;

    iget-object v2, p0, LX/I0v;->m:Ljava/util/HashMap;

    iget-object v3, p0, LX/I0v;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, LX/7n4;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v0, v4}, LX/I0r;->a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x1

    .line 2528209
    iget-object v0, p0, LX/I0v;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    .line 2528210
    if-nez v0, :cond_1

    .line 2528211
    :cond_0
    return-void

    .line 2528212
    :cond_1
    iget-object v1, p0, LX/I0v;->a:LX/I0r;

    invoke-virtual {v1}, LX/I0r;->a()Ljava/util/Calendar;

    move-result-object v2

    .line 2528213
    iget-object v1, p0, LX/I0v;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/TimeZone;

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v3

    .line 2528214
    iget-object v1, v0, LX/44w;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528215
    :goto_0
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iget-object v1, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gtz v1, :cond_5

    .line 2528216
    iget-object v1, p0, LX/I0v;->m:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2528217
    invoke-interface {v1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v4

    .line 2528218
    :cond_2
    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2528219
    invoke-interface {v4}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/I0X;

    .line 2528220
    iget-object v5, v1, LX/I0X;->a:LX/I0Y;

    sget-object v6, LX/I0Y;->EVENT_ROW:LX/I0Y;

    if-eq v5, v6, :cond_3

    iget-object v5, v1, LX/I0X;->a:LX/I0Y;

    sget-object v6, LX/I0Y;->EVENT_CANT_ATTEND_ROW:LX/I0Y;

    if-ne v5, v6, :cond_2

    .line 2528221
    :cond_3
    iget-object v1, v1, LX/I0X;->b:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2528222
    iget-object v5, v1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-object v1, v5

    .line 2528223
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2528224
    invoke-interface {v4}, Ljava/util/ListIterator;->remove()V

    .line 2528225
    :cond_4
    invoke-virtual {v3, v9, v8}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 2528226
    :cond_5
    iget-object v1, v0, LX/44w;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528227
    :goto_1
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iget-object v1, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gtz v1, :cond_0

    .line 2528228
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_6

    .line 2528229
    invoke-virtual {v3, v9, v8}, Ljava/util/Calendar;->add(II)V

    goto :goto_1

    .line 2528230
    :cond_6
    iget-object v1, p0, LX/I0v;->m:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2528231
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, LX/I0r;->a(Ljava/util/List;J)V

    .line 2528232
    invoke-virtual {v3, v9, v8}, Ljava/util/Calendar;->add(II)V

    goto :goto_1
.end method

.method public final b(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2528233
    iput-object p1, p0, LX/I0v;->l:LX/0Px;

    .line 2528234
    iput v0, p0, LX/I0v;->e:I

    .line 2528235
    invoke-static {p0}, LX/I0v;->e(LX/I0v;)V

    .line 2528236
    iput-boolean v0, p0, LX/I0v;->j:Z

    .line 2528237
    return-void
.end method
