.class public final enum LX/HOL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HOL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HOL;

.field public static final enum ACTION_ITEM:LX/HOL;

.field public static final enum CATEGORY_HEADER:LX/HOL;

.field public static final enum DESCRIPTION_HEADER:LX/HOL;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2459970
    new-instance v0, LX/HOL;

    const-string v1, "DESCRIPTION_HEADER"

    invoke-direct {v0, v1, v2}, LX/HOL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HOL;->DESCRIPTION_HEADER:LX/HOL;

    .line 2459971
    new-instance v0, LX/HOL;

    const-string v1, "CATEGORY_HEADER"

    invoke-direct {v0, v1, v3}, LX/HOL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HOL;->CATEGORY_HEADER:LX/HOL;

    .line 2459972
    new-instance v0, LX/HOL;

    const-string v1, "ACTION_ITEM"

    invoke-direct {v0, v1, v4}, LX/HOL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HOL;->ACTION_ITEM:LX/HOL;

    .line 2459973
    const/4 v0, 0x3

    new-array v0, v0, [LX/HOL;

    sget-object v1, LX/HOL;->DESCRIPTION_HEADER:LX/HOL;

    aput-object v1, v0, v2

    sget-object v1, LX/HOL;->CATEGORY_HEADER:LX/HOL;

    aput-object v1, v0, v3

    sget-object v1, LX/HOL;->ACTION_ITEM:LX/HOL;

    aput-object v1, v0, v4

    sput-object v0, LX/HOL;->$VALUES:[LX/HOL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2459969
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HOL;
    .locals 1

    .prologue
    .line 2459967
    const-class v0, LX/HOL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HOL;

    return-object v0
.end method

.method public static values()[LX/HOL;
    .locals 1

    .prologue
    .line 2459968
    sget-object v0, LX/HOL;->$VALUES:[LX/HOL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HOL;

    return-object v0
.end method
