.class public LX/HIt;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HIu;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HIt",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HIu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451843
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2451844
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HIt;->b:LX/0Zi;

    .line 2451845
    iput-object p1, p0, LX/HIt;->a:LX/0Ot;

    .line 2451846
    return-void
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2451869
    check-cast p1, LX/HIs;

    .line 2451870
    iget-object v0, p0, LX/HIt;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HIu;

    iget-object v1, p1, LX/HIs;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p1, LX/HIs;->b:LX/2km;

    .line 2451871
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2451872
    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2451873
    :goto_0
    return-void

    .line 2451874
    :cond_0
    iget-object v3, v0, LX/HIu;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/E1f;

    .line 2451875
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2451876
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    move-object v5, v2

    check-cast v5, LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    move-object v7, v2

    check-cast v7, LX/2kp;

    invoke-interface {v7}, LX/2kp;->t()LX/2jY;

    move-result-object v7

    .line 2451877
    iget-object v8, v7, LX/2jY;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2451878
    move-object v8, v2

    check-cast v8, LX/2kp;

    invoke-interface {v8}, LX/2kp;->t()LX/2jY;

    move-result-object v8

    .line 2451879
    iget-object v9, v8, LX/2jY;->b:Ljava/lang/String;

    move-object v8, v9

    .line 2451880
    iget-object v9, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 2451881
    invoke-virtual/range {v3 .. v9}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    .line 2451882
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2451883
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2451884
    invoke-interface {v2, v4, v5, v3}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2451885
    const v0, 0x29a0d597

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2451852
    check-cast p2, LX/HIs;

    .line 2451853
    iget-object v0, p0, LX/HIt;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HIu;

    iget-object v1, p2, LX/HIs;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2451854
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2451855
    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2451856
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2451857
    invoke-interface {v2}, LX/9uc;->bm()Z

    move-result v5

    .line 2451858
    if-eqz v5, :cond_0

    const v2, 0x7f0a0092

    move v3, v2

    .line 2451859
    :goto_0
    if-eqz v5, :cond_1

    const v2, 0x7f0207da

    .line 2451860
    :goto_1
    iget-object v5, v0, LX/HIu;->b:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2451861
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const p0, 0x7f0a0097

    invoke-interface {v5, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    .line 2451862
    const p0, 0x29a0d597

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2451863
    invoke-interface {v5, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x2

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x6

    const p2, 0x7f0b0d5e

    invoke-interface {v5, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x7

    const p2, 0x7f0b0d5f

    invoke-interface {v5, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    const v5, 0x7f0b0050

    .line 2451864
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object p0

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/4 p2, 0x6

    const v0, 0x7f0b0d5e

    invoke-interface {p0, p2, v0}, LX/1Di;->g(II)LX/1Di;

    move-result-object p0

    move-object v3, p0

    .line 2451865
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2451866
    return-object v0

    .line 2451867
    :cond_0
    const v2, 0x7f0a008a

    move v3, v2

    goto/16 :goto_0

    .line 2451868
    :cond_1
    const v2, 0x7f020972

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2451847
    invoke-static {}, LX/1dS;->b()V

    .line 2451848
    iget v0, p1, LX/1dQ;->b:I

    .line 2451849
    packed-switch v0, :pswitch_data_0

    .line 2451850
    :goto_0
    return-object v1

    .line 2451851
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HIt;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x29a0d597
        :pswitch_0
    .end packed-switch
.end method
