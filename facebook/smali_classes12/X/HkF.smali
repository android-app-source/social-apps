.class public final enum LX/HkF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HkF;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BANNER:LX/HkF;

.field public static final enum INTERSTITIAL:LX/HkF;

.field public static final enum NATIVE:LX/HkF;

.field public static final enum UNKNOWN:LX/HkF;

.field private static final synthetic b:[LX/HkF;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, LX/HkF;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v3, v2}, LX/HkF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HkF;->UNKNOWN:LX/HkF;

    new-instance v0, LX/HkF;

    const-string v1, "BANNER"

    const-string v2, "banner"

    invoke-direct {v0, v1, v4, v2}, LX/HkF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HkF;->BANNER:LX/HkF;

    new-instance v0, LX/HkF;

    const-string v1, "INTERSTITIAL"

    const-string v2, "interstitial"

    invoke-direct {v0, v1, v5, v2}, LX/HkF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HkF;->INTERSTITIAL:LX/HkF;

    new-instance v0, LX/HkF;

    const-string v1, "NATIVE"

    const-string v2, "native"

    invoke-direct {v0, v1, v6, v2}, LX/HkF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HkF;->NATIVE:LX/HkF;

    const/4 v0, 0x4

    new-array v0, v0, [LX/HkF;

    sget-object v1, LX/HkF;->UNKNOWN:LX/HkF;

    aput-object v1, v0, v3

    sget-object v1, LX/HkF;->BANNER:LX/HkF;

    aput-object v1, v0, v4

    sget-object v1, LX/HkF;->INTERSTITIAL:LX/HkF;

    aput-object v1, v0, v5

    sget-object v1, LX/HkF;->NATIVE:LX/HkF;

    aput-object v1, v0, v6

    sput-object v0, LX/HkF;->b:[LX/HkF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, LX/HkF;->a:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/HkF;
    .locals 1

    invoke-static {p0}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/HkF;->UNKNOWN:LX/HkF;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/HkF;->valueOf(Ljava/lang/String;)LX/HkF;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    sget-object v0, LX/HkF;->UNKNOWN:LX/HkF;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/HkF;
    .locals 1

    const-class v0, LX/HkF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HkF;

    return-object v0
.end method

.method public static values()[LX/HkF;
    .locals 1

    sget-object v0, LX/HkF;->b:[LX/HkF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HkF;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LX/HkF;->a:Ljava/lang/String;

    return-object v0
.end method
