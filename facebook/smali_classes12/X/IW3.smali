.class public LX/IW3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static p:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/2lS;

.field private final d:LX/88n;

.field public final e:LX/DO3;

.field public final f:LX/IW5;

.field public final g:LX/1My;

.field private final h:LX/DNX;

.field public final i:LX/03V;

.field public final j:LX/0tX;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z

.field public final o:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2583395
    const-class v0, LX/IW3;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IW3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2lS;LX/88n;LX/DO3;LX/IW5;LX/1My;LX/DNX;LX/03V;LX/0tX;LX/0Or;)V
    .locals 2
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2lS;",
            "LX/88n;",
            "LX/DO3;",
            "LX/IW5;",
            "LX/1My;",
            "LX/DNX;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2583345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2583346
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IW3;->l:Z

    .line 2583347
    iput-boolean v1, p0, LX/IW3;->m:Z

    .line 2583348
    iput-boolean v1, p0, LX/IW3;->n:Z

    .line 2583349
    new-instance v0, LX/IVx;

    invoke-direct {v0, p0}, LX/IVx;-><init>(LX/IW3;)V

    iput-object v0, p0, LX/IW3;->o:LX/0TF;

    .line 2583350
    iput-object p1, p0, LX/IW3;->b:Landroid/content/Context;

    .line 2583351
    iput-object p2, p0, LX/IW3;->c:LX/2lS;

    .line 2583352
    iput-object p3, p0, LX/IW3;->d:LX/88n;

    .line 2583353
    iput-object p4, p0, LX/IW3;->e:LX/DO3;

    .line 2583354
    iput-object p5, p0, LX/IW3;->f:LX/IW5;

    .line 2583355
    iput-object p6, p0, LX/IW3;->g:LX/1My;

    .line 2583356
    iput-object p7, p0, LX/IW3;->h:LX/DNX;

    .line 2583357
    iput-object p8, p0, LX/IW3;->i:LX/03V;

    .line 2583358
    iput-object p9, p0, LX/IW3;->j:LX/0tX;

    .line 2583359
    iput-object p10, p0, LX/IW3;->k:LX/0Or;

    .line 2583360
    return-void
.end method

.method public static a(LX/0QB;)LX/IW3;
    .locals 14

    .prologue
    .line 2583384
    const-class v1, LX/IW3;

    monitor-enter v1

    .line 2583385
    :try_start_0
    sget-object v0, LX/IW3;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2583386
    sput-object v2, LX/IW3;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2583387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2583389
    new-instance v3, LX/IW3;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2lS;->a(LX/0QB;)LX/2lS;

    move-result-object v5

    check-cast v5, LX/2lS;

    invoke-static {v0}, LX/88n;->b(LX/0QB;)LX/88n;

    move-result-object v6

    check-cast v6, LX/88n;

    invoke-static {v0}, LX/DO3;->b(LX/0QB;)LX/DO3;

    move-result-object v7

    check-cast v7, LX/DO3;

    invoke-static {v0}, LX/IW5;->a(LX/0QB;)LX/IW5;

    move-result-object v8

    check-cast v8, LX/IW5;

    invoke-static {v0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v9

    check-cast v9, LX/1My;

    invoke-static {v0}, LX/DNX;->b(LX/0QB;)LX/DNX;

    move-result-object v10

    check-cast v10, LX/DNX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    const/16 v13, 0x15e7

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/IW3;-><init>(Landroid/content/Context;LX/2lS;LX/88n;LX/DO3;LX/IW5;LX/1My;LX/DNX;LX/03V;LX/0tX;LX/0Or;)V

    .line 2583390
    move-object v0, v3

    .line 2583391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2583392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IW3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2583394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/IW3;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/util/concurrent/atomic/AtomicInteger;)V
    .locals 4

    .prologue
    .line 2583381
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2583382
    :cond_0
    :goto_0
    return-void

    .line 2583383
    :cond_1
    iget-object v0, p0, LX/IW3;->d:LX/88n;

    sget-object v1, LX/88p;->CONFIRM_MUTATION:LX/88p;

    new-instance v2, LX/IW0;

    invoke-direct {v2, p0, p1}, LX/IW0;-><init>(LX/IW3;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    new-instance v3, LX/IW1;

    invoke-direct {v3, p0, p2, p1}, LX/IW1;-><init>(LX/IW3;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v0, v1, v2, v3}, LX/88n;->a(LX/88p;Ljava/util/concurrent/Callable;LX/0Ve;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0zS;)V
    .locals 5

    .prologue
    .line 2583368
    iget-object v0, p0, LX/IW3;->b:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 2583369
    iget-boolean v1, p0, LX/IW3;->l:Z

    .line 2583370
    iget-boolean v0, p0, LX/IW3;->l:Z

    if-eqz v0, :cond_0

    .line 2583371
    iget-object v0, p0, LX/IW3;->c:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->c()V

    .line 2583372
    iget-object v0, p0, LX/IW3;->c:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->b()V

    .line 2583373
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IW3;->l:Z

    .line 2583374
    :cond_0
    sget-object v0, LX/DO3;->a:LX/0zS;

    if-ne p1, v0, :cond_2

    sget-object v0, LX/88p;->COMMUNITY:LX/88p;

    .line 2583375
    :goto_0
    iget-object v2, p0, LX/IW3;->d:LX/88n;

    new-instance v3, LX/IVy;

    invoke-direct {v3, p0, p1}, LX/IVy;-><init>(LX/IW3;LX/0zS;)V

    new-instance v4, LX/IVz;

    invoke-direct {v4, p0, v1}, LX/IVz;-><init>(LX/IW3;Z)V

    invoke-virtual {v2, v0, v3, v4}, LX/88n;->a(LX/88p;Ljava/util/concurrent/Callable;LX/0Ve;)V

    .line 2583376
    iget-object v0, p0, LX/IW3;->f:LX/IW5;

    sget-object v1, LX/IW2;->STARTED:LX/IW2;

    .line 2583377
    iput-object v1, v0, LX/IW5;->i:LX/IW2;

    .line 2583378
    invoke-virtual {v0}, LX/IW5;->i()V

    .line 2583379
    :cond_1
    return-void

    .line 2583380
    :cond_2
    sget-object v0, LX/88p;->BASIC_INFO:LX/88p;

    goto :goto_0
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2583361
    instance-of v0, p2, LX/IW4;

    if-eqz v0, :cond_0

    .line 2583362
    check-cast p2, LX/IW4;

    .line 2583363
    sget-object v0, LX/DO3;->a:LX/0zS;

    invoke-virtual {p0, v0}, LX/IW3;->a(LX/0zS;)V

    .line 2583364
    iget-boolean v0, p2, LX/IW4;->a:Z

    move v0, v0

    .line 2583365
    if-nez v0, :cond_0

    .line 2583366
    iget-object v0, p0, LX/IW3;->h:LX/DNX;

    invoke-virtual {v0}, LX/DNA;->k()V

    .line 2583367
    :cond_0
    return-void
.end method
