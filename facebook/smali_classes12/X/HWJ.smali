.class public LX/HWJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475809
    return-void
.end method

.method public static a(LX/0QB;)LX/HWJ;
    .locals 1

    .prologue
    .line 2475805
    new-instance v0, LX/HWJ;

    invoke-direct {v0}, LX/HWJ;-><init>()V

    .line 2475806
    move-object v0, v0

    .line 2475807
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 2475797
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "extra_page_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2475798
    new-instance v3, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-direct {v3}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;-><init>()V

    .line 2475799
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2475800
    const-string p0, "com.facebook.katana.profile.id"

    invoke-virtual {v4, p0, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475801
    const-string p0, "extra_page_name"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475802
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475803
    move-object v0, v3

    .line 2475804
    return-object v0
.end method
