.class public final LX/JPu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

.field private b:J

.field private c:J

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field private i:Ljava/lang/String;

.field private j:LX/0lF;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;JJLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;Ljava/lang/String;LX/0lF;)V
    .locals 0

    .prologue
    .line 2690562
    iput-object p1, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2690563
    iput-wide p2, p0, LX/JPu;->b:J

    .line 2690564
    iput-wide p4, p0, LX/JPu;->c:J

    .line 2690565
    iput-object p6, p0, LX/JPu;->d:Ljava/lang/String;

    .line 2690566
    iput-object p7, p0, LX/JPu;->e:Ljava/lang/String;

    .line 2690567
    iput-boolean p8, p0, LX/JPu;->f:Z

    .line 2690568
    iput-object p9, p0, LX/JPu;->g:Ljava/lang/String;

    .line 2690569
    iput-object p10, p0, LX/JPu;->h:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 2690570
    iput-object p11, p0, LX/JPu;->i:Ljava/lang/String;

    .line 2690571
    iput-object p12, p0, LX/JPu;->j:LX/0lF;

    .line 2690572
    return-void
.end method

.method private a(Lcom/facebook/auth/viewercontext/ViewerContext;JLjava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 8

    .prologue
    .line 2690573
    iget-object v0, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->h:LX/1Kf;

    const/4 v7, 0x0

    iget-object v1, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->i:LX/CSL;

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, LX/CSL;->a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    const/16 v2, 0x6dc

    invoke-interface {v0, v7, v1, v2, p6}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2690574
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x53aa6764

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2690575
    iget-object v0, p0, LX/JPu;->h:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/JPu;->h:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->name()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/JPu;->h:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2690576
    sget-object v0, LX/JPs;->b:[I

    iget-object v1, p0, LX/JPu;->h:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2690577
    iget-object v0, p0, LX/JPu;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2690578
    iget-object v0, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/JPu;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2690579
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->d:LX/0Zb;

    .line 2690580
    iget-object v1, p0, LX/JPu;->j:LX/0lF;

    iget-wide v2, p0, LX/JPu;->b:J

    iget-wide v4, p0, LX/JPu;->c:J

    iget-boolean v6, p0, LX/JPu;->f:Z

    iget-object v7, p0, LX/JPu;->g:Ljava/lang/String;

    .line 2690581
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "page_admin_panel_aymt_tap"

    invoke-direct {v9, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "tracking"

    invoke-virtual {v9, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "admin_id"

    invoke-virtual {v9, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "event_name"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v9, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "page_id"

    invoke-virtual {v9, p0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "is_ego_show_single_page_item"

    invoke-virtual {v9, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "source"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->AYMT_TIP:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v9, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "action"

    invoke-virtual {v9, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "page_admin_panel"

    .line 2690582
    iput-object p0, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2690583
    move-object v9, v9

    .line 2690584
    move-object v1, v9

    .line 2690585
    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2690586
    const v0, 0x16b70b26

    invoke-static {v0, v8}, LX/02F;->a(II)V

    return-void

    .line 2690587
    :pswitch_1
    iget-object v0, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->j:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2690588
    iget-wide v2, p0, LX/JPu;->c:J

    iget-object v4, p0, LX/JPu;->d:Ljava/lang/String;

    iget-object v5, p0, LX/JPu;->e:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v6, Landroid/app/Activity;

    invoke-static {v0, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/JPu;->a(Lcom/facebook/auth/viewercontext/ViewerContext;JLjava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 2690589
    :pswitch_2
    iget-object v0, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->l:LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    sget-object v2, LX/0ax;->bb:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2690590
    if-eqz v1, :cond_0

    .line 2690591
    iget-object v0, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v2, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->k:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2690592
    :cond_1
    iget-object v0, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/JPu;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 2690593
    :cond_2
    iget-object v0, p0, LX/JPu;->a:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/JPu;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
