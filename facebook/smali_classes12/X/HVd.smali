.class public LX/HVd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1ay;

.field public final b:LX/1Kf;

.field public final c:LX/CSL;


# direct methods
.method public constructor <init>(LX/1ay;LX/1Kf;LX/CSL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475254
    iput-object p1, p0, LX/HVd;->a:LX/1ay;

    .line 2475255
    iput-object p2, p0, LX/HVd;->b:LX/1Kf;

    .line 2475256
    iput-object p3, p0, LX/HVd;->c:LX/CSL;

    .line 2475257
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 11

    .prologue
    .line 2475258
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v1

    .line 2475259
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2475260
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ADMIN_TIP_CREATE_POST:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    if-ne v1, v2, :cond_1

    .line 2475261
    iget-object v3, p0, LX/HVd;->b:LX/1Kf;

    const/4 v10, 0x0

    iget-object v4, p0, LX/HVd;->c:LX/CSL;

    iget-wide v5, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    iget-object v7, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    iget-object v8, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->f:Ljava/lang/String;

    iget-object v9, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual/range {v4 .. v9}, LX/CSL;->a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    const/16 v5, 0x6dc

    invoke-interface {v3, v10, v4, v5, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2475262
    :cond_0
    :goto_0
    return-void

    .line 2475263
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ADMIN_TIP_CREATE_PHOTO_POST:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    if-ne v1, v2, :cond_0

    .line 2475264
    iget-object v3, p0, LX/HVd;->a:LX/1ay;

    iget-object v4, p0, LX/HVd;->c:LX/CSL;

    iget-wide v5, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    iget-object v7, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    iget-object v8, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->f:Ljava/lang/String;

    iget-object v9, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual/range {v4 .. v9}, LX/CSL;->b(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;

    move-result-object v4

    const/16 v5, 0x6dc

    invoke-virtual {v3, v4, v5, v0}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2475265
    goto :goto_0
.end method
