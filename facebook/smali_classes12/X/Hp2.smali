.class public LX/Hp2;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/1Er;

.field private final d:LX/03V;

.field private e:LX/Hp1;

.field private f:LX/0kL;

.field private g:LX/Hos;

.field private h:Z

.field private i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private j:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2507149
    const-class v0, LX/Hp2;

    sput-object v0, LX/Hp2;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/1Er;LX/03V;LX/Hp1;LX/0kL;LX/Hos;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2507150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2507151
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Hp2;->h:Z

    .line 2507152
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/Hp2;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2507153
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/Hp2;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2507154
    iput-object p1, p0, LX/Hp2;->b:Ljava/util/concurrent/ExecutorService;

    .line 2507155
    iput-object p2, p0, LX/Hp2;->c:LX/1Er;

    .line 2507156
    iput-object p3, p0, LX/Hp2;->d:LX/03V;

    .line 2507157
    iput-object p4, p0, LX/Hp2;->e:LX/Hp1;

    .line 2507158
    iput-object p5, p0, LX/Hp2;->f:LX/0kL;

    .line 2507159
    iput-object p6, p0, LX/Hp2;->g:LX/Hos;

    .line 2507160
    return-void
.end method
