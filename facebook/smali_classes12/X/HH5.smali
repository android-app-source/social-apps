.class public final LX/HH5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Fd;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V
    .locals 0

    .prologue
    .line 2446985
    iput-object p1, p0, LX/HH5;->a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2446986
    iget-object v0, p0, LX/HH5;->a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail to fetch viewer context for page "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HH5;->a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    iget-object v3, v3, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->z:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2446987
    iget-object v0, p0, LX/HH5;->a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08003d

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2446988
    iget-object v0, p0, LX/HH5;->a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    invoke-static {v0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->l(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V

    .line 2446989
    return-void
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1
    .param p1    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2446990
    iget-object v0, p0, LX/HH5;->a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    iget-boolean v0, v0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->x:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HH5;->a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    iget-boolean v0, v0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->y:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2446991
    iget-object v0, p0, LX/HH5;->a:Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    invoke-static {v0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->a(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V

    .line 2446992
    :cond_0
    return-void
.end method
