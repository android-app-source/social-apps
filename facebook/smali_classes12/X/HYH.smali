.class public LX/HYH;
.super LX/HYA;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HYH;


# instance fields
.field private a:LX/4hz;


# direct methods
.method public constructor <init>(LX/4hz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480394
    invoke-direct {p0}, LX/HYA;-><init>()V

    .line 2480395
    iput-object p1, p0, LX/HYH;->a:LX/4hz;

    .line 2480396
    return-void
.end method

.method public static a(LX/0QB;)LX/HYH;
    .locals 4

    .prologue
    .line 2480397
    sget-object v0, LX/HYH;->b:LX/HYH;

    if-nez v0, :cond_1

    .line 2480398
    const-class v1, LX/HYH;

    monitor-enter v1

    .line 2480399
    :try_start_0
    sget-object v0, LX/HYH;->b:LX/HYH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480400
    if-eqz v2, :cond_0

    .line 2480401
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480402
    new-instance p0, LX/HYH;

    invoke-static {v0}, LX/4hz;->a(LX/0QB;)LX/4hz;

    move-result-object v3

    check-cast v3, LX/4hz;

    invoke-direct {p0, v3}, LX/HYH;-><init>(LX/4hz;)V

    .line 2480403
    move-object v0, p0

    .line 2480404
    sput-object v0, LX/HYH;->b:LX/HYH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480405
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480406
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480407
    :cond_1
    sget-object v0, LX/HYH;->b:LX/HYH;

    return-object v0

    .line 2480408
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)LX/HYB;
    .locals 3

    .prologue
    .line 2480410
    new-instance v0, LX/HYG;

    iget-object v1, p0, LX/HYH;->a:LX/4hz;

    invoke-direct {v0, v1, p1}, LX/HYG;-><init>(LX/4hz;Lorg/json/JSONObject;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2480411
    const-string v0, "dialogready"

    return-object v0
.end method
