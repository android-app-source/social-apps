.class public abstract LX/Idl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Idk;


# static fields
.field private static final i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/content/ContentResolver;

.field public b:Landroid/net/Uri;

.field public c:J

.field public d:Ljava/lang/String;

.field public e:J

.field public final f:I

.field public g:Ljava/lang/String;

.field public h:LX/Idm;

.field private final j:J

.field private k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private m:I

.field private n:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2597861
    const-class v0, LX/Idl;

    sput-object v0, LX/Idl;->i:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Idm;Landroid/content/ContentResolver;JILandroid/net/Uri;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2597846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2597847
    iput v0, p0, LX/Idl;->m:I

    .line 2597848
    iput v0, p0, LX/Idl;->n:I

    .line 2597849
    iput-object p1, p0, LX/Idl;->h:LX/Idm;

    .line 2597850
    iput-object p2, p0, LX/Idl;->a:Landroid/content/ContentResolver;

    .line 2597851
    iput-wide p3, p0, LX/Idl;->c:J

    .line 2597852
    iput p5, p0, LX/Idl;->f:I

    .line 2597853
    iput-object p6, p0, LX/Idl;->b:Landroid/net/Uri;

    .line 2597854
    iput-object p7, p0, LX/Idl;->d:Ljava/lang/String;

    .line 2597855
    iput-wide p8, p0, LX/Idl;->e:J

    .line 2597856
    iput-object p10, p0, LX/Idl;->g:Ljava/lang/String;

    .line 2597857
    iput-wide p11, p0, LX/Idl;->j:J

    .line 2597858
    iput-object p13, p0, LX/Idl;->k:Ljava/lang/String;

    .line 2597859
    iput-object p14, p0, LX/Idl;->l:Ljava/lang/String;

    .line 2597860
    return-void
.end method


# virtual methods
.method public final a(II)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 2597838
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2597839
    iget-object v2, p0, LX/Idl;->h:LX/Idm;

    iget-wide v4, p0, LX/Idl;->c:J

    invoke-virtual {v2, v4, v5}, LX/Idm;->a(J)Landroid/net/Uri;

    move-result-object v2

    .line 2597840
    if-nez v2, :cond_1

    const/4 v2, 0x0

    .line 2597841
    :cond_0
    :goto_0
    move-object v0, v2

    .line 2597842
    return-object v0

    .line 2597843
    :cond_1
    iget-object v3, p0, LX/Idl;->a:Landroid/content/ContentResolver;

    invoke-static {p1, p2, v2, v3, v1}, LX/IdZ;->a(IILandroid/net/Uri;Landroid/content/ContentResolver;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2597844
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 2597845
    invoke-virtual {p0}, LX/Idl;->c()I

    move-result v3

    invoke-static {v2, v3}, LX/IdZ;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2597837
    iget-object v0, p0, LX/Idl;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 2597836
    iget-wide v0, p0, LX/Idl;->j:J

    return-wide v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 2597830
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2597835
    iget-object v0, p0, LX/Idl;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2597833
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/Idn;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 2597834
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/Idl;->b:Landroid/net/Uri;

    check-cast p1, LX/Idn;

    iget-object v1, p1, LX/Idl;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2597832
    iget-object v0, p0, LX/Idl;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2597831
    iget-object v0, p0, LX/Idl;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
