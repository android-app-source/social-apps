.class public LX/Hcy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g0;
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g0",
        "<",
        "LX/Hcr;",
        ">;",
        "Ljava/util/Observer;"
    }
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/Hcr;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0qs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qs",
            "<",
            "LX/Hcr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/Hcr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2487703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487704
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Hcy;->b:Ljava/util/List;

    .line 2487705
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hcy;->c:LX/0qs;

    .line 2487706
    iput-object p1, p0, LX/Hcy;->a:LX/0Px;

    .line 2487707
    iget-object v0, p0, LX/Hcy;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    .line 2487708
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2487709
    iget-object v0, p0, LX/Hcy;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hcr;

    .line 2487710
    invoke-virtual {v0, p0}, LX/Hcr;->addObserver(Ljava/util/Observer;)V

    .line 2487711
    invoke-virtual {v0}, LX/Hcr;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487712
    iget-object v0, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2487713
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2487714
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2487715
    iget-object v1, p0, LX/Hcy;->a:LX/0Px;

    iget-object v0, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hcr;

    return-object v0
.end method

.method public final a(LX/0qs;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<",
            "LX/Hcr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2487716
    iput-object p1, p0, LX/Hcy;->c:LX/0qs;

    .line 2487717
    return-void
.end method

.method public final b(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<",
            "LX/Hcr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2487718
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hcy;->c:LX/0qs;

    .line 2487719
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2487720
    iget-object v0, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2487721
    check-cast p1, LX/Hcr;

    .line 2487722
    iget-object v0, p0, LX/Hcy;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 2487723
    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    .line 2487724
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The section is not part of the collection"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2487725
    :cond_0
    iget-object v0, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2487726
    iget-object v3, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    .line 2487727
    iget-object v3, p0, LX/Hcy;->a:LX/0Px;

    invoke-virtual {v3, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result p2

    .line 2487728
    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_6

    .line 2487729
    iget-object v3, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2487730
    if-lt v3, p2, :cond_5

    move v3, v4

    .line 2487731
    :goto_1
    move v3, v3

    .line 2487732
    if-ge v3, v0, :cond_2

    iget-object v0, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    .line 2487733
    :goto_2
    invoke-virtual {p1}, LX/Hcr;->a()Z

    move-result v4

    .line 2487734
    if-eqz v0, :cond_3

    if-eqz v4, :cond_3

    .line 2487735
    iget-object v0, p0, LX/Hcy;->c:LX/0qs;

    if-eqz v0, :cond_1

    .line 2487736
    iget-object v0, p0, LX/Hcy;->c:LX/0qs;

    invoke-interface {v0, v3, p1, p1, v1}, LX/0qs;->a(ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 2487737
    :cond_1
    :goto_3
    return-void

    :cond_2
    move v0, v1

    .line 2487738
    goto :goto_2

    .line 2487739
    :cond_3
    if-nez v0, :cond_4

    if-eqz v4, :cond_4

    .line 2487740
    iget-object v0, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2487741
    iget-object v0, p0, LX/Hcy;->c:LX/0qs;

    if-eqz v0, :cond_1

    .line 2487742
    iget-object v0, p0, LX/Hcy;->c:LX/0qs;

    invoke-interface {v0, v3, p1, v1}, LX/0qs;->a(ILjava/lang/Object;Z)V

    goto :goto_3

    .line 2487743
    :cond_4
    if-eqz v0, :cond_1

    .line 2487744
    iget-object v0, p0, LX/Hcy;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2487745
    iget-object v0, p0, LX/Hcy;->c:LX/0qs;

    if-eqz v0, :cond_1

    .line 2487746
    iget-object v0, p0, LX/Hcy;->c:LX/0qs;

    invoke-interface {v0, v3, p1, v1}, LX/0qs;->b(ILjava/lang/Object;Z)V

    goto :goto_3

    .line 2487747
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_6
    move v3, v5

    .line 2487748
    goto :goto_1
.end method
