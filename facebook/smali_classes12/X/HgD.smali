.class public LX/HgD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1vg;

.field public final b:LX/HgN;


# direct methods
.method public constructor <init>(LX/1vg;LX/HgN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2492786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2492787
    iput-object p1, p0, LX/HgD;->a:LX/1vg;

    .line 2492788
    iput-object p2, p0, LX/HgD;->b:LX/HgN;

    .line 2492789
    return-void
.end method

.method public static a(LX/0QB;)LX/HgD;
    .locals 5

    .prologue
    .line 2492790
    const-class v1, LX/HgD;

    monitor-enter v1

    .line 2492791
    :try_start_0
    sget-object v0, LX/HgD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2492792
    sput-object v2, LX/HgD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2492793
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2492794
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2492795
    new-instance p0, LX/HgD;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-static {v0}, LX/HgN;->a(LX/0QB;)LX/HgN;

    move-result-object v4

    check-cast v4, LX/HgN;

    invoke-direct {p0, v3, v4}, LX/HgD;-><init>(LX/1vg;LX/HgN;)V

    .line 2492796
    move-object v0, p0

    .line 2492797
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2492798
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HgD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2492799
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2492800
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
