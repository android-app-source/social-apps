.class public LX/JCx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JCx;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2662949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2662950
    return-void
.end method

.method public static a(LX/0QB;)LX/JCx;
    .locals 3

    .prologue
    .line 2662951
    sget-object v0, LX/JCx;->a:LX/JCx;

    if-nez v0, :cond_1

    .line 2662952
    const-class v1, LX/JCx;

    monitor-enter v1

    .line 2662953
    :try_start_0
    sget-object v0, LX/JCx;->a:LX/JCx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2662954
    if-eqz v2, :cond_0

    .line 2662955
    :try_start_1
    new-instance v0, LX/JCx;

    invoke-direct {v0}, LX/JCx;-><init>()V

    .line 2662956
    move-object v0, v0

    .line 2662957
    sput-object v0, LX/JCx;->a:LX/JCx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2662958
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2662959
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2662960
    :cond_1
    sget-object v0, LX/JCx;->a:LX/JCx;

    return-object v0

    .line 2662961
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2662962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;"
        }
    .end annotation

    .prologue
    .line 2662963
    if-eqz p0, :cond_2

    .line 2662964
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 2662965
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->CONTACT_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->NOTES:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->REVIEW_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->GRID:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2662966
    if-eqz v2, :cond_0

    .line 2662967
    :goto_1
    return-object v0

    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
