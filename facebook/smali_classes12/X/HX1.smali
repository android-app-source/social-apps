.class public final LX/HX1;
.super LX/HDr;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2477188
    iput-object p1, p0, LX/HX1;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {p0}, LX/HDr;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2477189
    check-cast p1, LX/HDs;

    .line 2477190
    iget-object v0, p0, LX/HX1;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    sget-object v1, LX/HPm;->FORCED_BY_USER:LX/HPm;

    invoke-virtual {v0, v1}, LX/HPn;->a(LX/HPm;)V

    .line 2477191
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {v1, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2477192
    move-object v2, v0

    .line 2477193
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2477194
    iget-object v4, p0, LX/HX1;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v4, v4, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v4, v0}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2477195
    iget-object v4, p0, LX/HX1;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v4, v4, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v4, v0}, LX/HQB;->b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I

    move-result v0

    .line 2477196
    if-ltz v0, :cond_0

    .line 2477197
    iget-object v4, p0, LX/HX1;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-static {v4, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2477198
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2477199
    :cond_1
    return-void
.end method
