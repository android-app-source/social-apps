.class public final LX/HyZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524324
    iput-object p1, p0, LX/HyZ;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2524325
    iget-object v0, p0, LX/HyZ;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    const/4 v1, 0x0

    .line 2524326
    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2524327
    iget-object v0, p0, LX/HyZ;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->G:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2524328
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2524329
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2524330
    iget-object v0, p0, LX/HyZ;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->G:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2524331
    iget-object v0, p0, LX/HyZ;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    .line 2524332
    invoke-static {v0, p1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2524333
    iget-object v0, p0, LX/HyZ;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->A(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    .line 2524334
    return-void
.end method
