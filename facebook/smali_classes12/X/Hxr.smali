.class public final enum LX/Hxr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hxr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hxr;

.field public static final enum ON_BIRTHDAYS:LX/Hxr;

.field public static final enum ON_EVENTS_FROM_GRAPHQL:LX/Hxr;

.field public static final enum ON_SUBSCRIPTIONS:LX/Hxr;

.field public static final enum ON_SUGGESTIONS:LX/Hxr;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2522887
    new-instance v0, LX/Hxr;

    const-string v1, "ON_EVENTS_FROM_GRAPHQL"

    invoke-direct {v0, v1, v2}, LX/Hxr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hxr;->ON_EVENTS_FROM_GRAPHQL:LX/Hxr;

    .line 2522888
    new-instance v0, LX/Hxr;

    const-string v1, "ON_SUBSCRIPTIONS"

    invoke-direct {v0, v1, v3}, LX/Hxr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hxr;->ON_SUBSCRIPTIONS:LX/Hxr;

    .line 2522889
    new-instance v0, LX/Hxr;

    const-string v1, "ON_SUGGESTIONS"

    invoke-direct {v0, v1, v4}, LX/Hxr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hxr;->ON_SUGGESTIONS:LX/Hxr;

    .line 2522890
    new-instance v0, LX/Hxr;

    const-string v1, "ON_BIRTHDAYS"

    invoke-direct {v0, v1, v5}, LX/Hxr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hxr;->ON_BIRTHDAYS:LX/Hxr;

    .line 2522891
    const/4 v0, 0x4

    new-array v0, v0, [LX/Hxr;

    sget-object v1, LX/Hxr;->ON_EVENTS_FROM_GRAPHQL:LX/Hxr;

    aput-object v1, v0, v2

    sget-object v1, LX/Hxr;->ON_SUBSCRIPTIONS:LX/Hxr;

    aput-object v1, v0, v3

    sget-object v1, LX/Hxr;->ON_SUGGESTIONS:LX/Hxr;

    aput-object v1, v0, v4

    sget-object v1, LX/Hxr;->ON_BIRTHDAYS:LX/Hxr;

    aput-object v1, v0, v5

    sput-object v0, LX/Hxr;->$VALUES:[LX/Hxr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2522884
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hxr;
    .locals 1

    .prologue
    .line 2522885
    const-class v0, LX/Hxr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hxr;

    return-object v0
.end method

.method public static values()[LX/Hxr;
    .locals 1

    .prologue
    .line 2522886
    sget-object v0, LX/Hxr;->$VALUES:[LX/Hxr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hxr;

    return-object v0
.end method
