.class public final LX/Ic7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Id4;

.field public final synthetic b:LX/Ic9;


# direct methods
.method public constructor <init>(LX/Ic9;LX/Id4;)V
    .locals 0

    .prologue
    .line 2595203
    iput-object p1, p0, LX/Ic7;->b:LX/Ic9;

    iput-object p2, p0, LX/Ic7;->a:LX/Id4;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2595204
    iget-object v0, p0, LX/Ic7;->b:LX/Ic9;

    iget-object v0, v0, LX/Ic9;->a:LX/03V;

    const-string v1, "RidePaymentHelper"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595205
    iget-object v0, p0, LX/Ic7;->a:LX/Id4;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Id4;->a(Lcom/facebook/payments/paymentmethods/model/CreditCard;)V

    .line 2595206
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2595207
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 2595208
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2595209
    :goto_0
    iget-object v1, p0, LX/Ic7;->a:LX/Id4;

    invoke-virtual {v1, v0}, LX/Id4;->a(Lcom/facebook/payments/paymentmethods/model/CreditCard;)V

    .line 2595210
    return-void

    .line 2595211
    :cond_0
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v0, v0

    .line 2595212
    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    const-class v1, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-virtual {v0, v1}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    goto :goto_0
.end method
