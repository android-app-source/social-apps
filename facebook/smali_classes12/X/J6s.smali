.class public final LX/J6s;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;)V
    .locals 0

    .prologue
    .line 2650327
    iput-object p1, p0, LX/J6s;->a:Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2650328
    iget-object v0, p0, LX/J6s;->a:Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;

    .line 2650329
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2650330
    sget-object p0, LX/J6t;->a:[I

    iget-object p1, v0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->b:LX/2by;

    invoke-virtual {p1}, LX/2by;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 2650331
    const-string p0, ""

    :goto_0
    move-object p0, p0

    .line 2650332
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2650333
    iget-object p0, v0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {p0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2650334
    return-void

    .line 2650335
    :pswitch_0
    sget-object p0, LX/0ax;->do:Ljava/lang/String;

    const-string p1, "https://m.facebook.com/help/android-app/120939471321735"

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2650336
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2650337
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2650338
    iget-object v0, p0, LX/J6s;->a:Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2650339
    return-void
.end method
