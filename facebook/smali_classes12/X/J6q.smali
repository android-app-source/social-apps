.class public final enum LX/J6q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J6q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J6q;

.field public static final enum CHOSE_FRIENDS:LX/J6q;

.field public static final enum CHOSE_MORE_OPTIONS:LX/J6q;

.field public static final enum CHOSE_ONLY_ME:LX/J6q;

.field public static final enum CHOSE_PUBLIC:LX/J6q;

.field public static final enum CHOSE_SKIP:LX/J6q;

.field public static final enum EXPOSED:LX/J6q;

.field public static final enum LEARN_MORE:LX/J6q;

.field public static final enum NAVIGATED_BACK:LX/J6q;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2650191
    new-instance v0, LX/J6q;

    const-string v1, "EXPOSED"

    invoke-direct {v0, v1, v3}, LX/J6q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6q;->EXPOSED:LX/J6q;

    .line 2650192
    new-instance v0, LX/J6q;

    const-string v1, "NAVIGATED_BACK"

    invoke-direct {v0, v1, v4}, LX/J6q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6q;->NAVIGATED_BACK:LX/J6q;

    .line 2650193
    new-instance v0, LX/J6q;

    const-string v1, "CHOSE_PUBLIC"

    invoke-direct {v0, v1, v5}, LX/J6q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6q;->CHOSE_PUBLIC:LX/J6q;

    .line 2650194
    new-instance v0, LX/J6q;

    const-string v1, "CHOSE_FRIENDS"

    invoke-direct {v0, v1, v6}, LX/J6q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6q;->CHOSE_FRIENDS:LX/J6q;

    .line 2650195
    new-instance v0, LX/J6q;

    const-string v1, "CHOSE_ONLY_ME"

    invoke-direct {v0, v1, v7}, LX/J6q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6q;->CHOSE_ONLY_ME:LX/J6q;

    .line 2650196
    new-instance v0, LX/J6q;

    const-string v1, "CHOSE_MORE_OPTIONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/J6q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6q;->CHOSE_MORE_OPTIONS:LX/J6q;

    .line 2650197
    new-instance v0, LX/J6q;

    const-string v1, "CHOSE_SKIP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/J6q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6q;->CHOSE_SKIP:LX/J6q;

    .line 2650198
    new-instance v0, LX/J6q;

    const-string v1, "LEARN_MORE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/J6q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6q;->LEARN_MORE:LX/J6q;

    .line 2650199
    const/16 v0, 0x8

    new-array v0, v0, [LX/J6q;

    sget-object v1, LX/J6q;->EXPOSED:LX/J6q;

    aput-object v1, v0, v3

    sget-object v1, LX/J6q;->NAVIGATED_BACK:LX/J6q;

    aput-object v1, v0, v4

    sget-object v1, LX/J6q;->CHOSE_PUBLIC:LX/J6q;

    aput-object v1, v0, v5

    sget-object v1, LX/J6q;->CHOSE_FRIENDS:LX/J6q;

    aput-object v1, v0, v6

    sget-object v1, LX/J6q;->CHOSE_ONLY_ME:LX/J6q;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/J6q;->CHOSE_MORE_OPTIONS:LX/J6q;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/J6q;->CHOSE_SKIP:LX/J6q;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/J6q;->LEARN_MORE:LX/J6q;

    aput-object v2, v0, v1

    sput-object v0, LX/J6q;->$VALUES:[LX/J6q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2650200
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J6q;
    .locals 1

    .prologue
    .line 2650201
    const-class v0, LX/J6q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J6q;

    return-object v0
.end method

.method public static values()[LX/J6q;
    .locals 1

    .prologue
    .line 2650202
    sget-object v0, LX/J6q;->$VALUES:[LX/J6q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J6q;

    return-object v0
.end method
