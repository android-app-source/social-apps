.class public final enum LX/Hx7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hx7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hx7;

.field public static final enum CALENDAR:LX/Hx7;

.field public static final enum DISCOVER:LX/Hx7;

.field public static final enum HOSTING:LX/Hx7;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2522018
    new-instance v0, LX/Hx7;

    const-string v1, "DISCOVER"

    invoke-direct {v0, v1, v2}, LX/Hx7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx7;->DISCOVER:LX/Hx7;

    new-instance v0, LX/Hx7;

    const-string v1, "CALENDAR"

    invoke-direct {v0, v1, v3}, LX/Hx7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx7;->CALENDAR:LX/Hx7;

    new-instance v0, LX/Hx7;

    const-string v1, "HOSTING"

    invoke-direct {v0, v1, v4}, LX/Hx7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx7;->HOSTING:LX/Hx7;

    .line 2522019
    const/4 v0, 0x3

    new-array v0, v0, [LX/Hx7;

    sget-object v1, LX/Hx7;->DISCOVER:LX/Hx7;

    aput-object v1, v0, v2

    sget-object v1, LX/Hx7;->CALENDAR:LX/Hx7;

    aput-object v1, v0, v3

    sget-object v1, LX/Hx7;->HOSTING:LX/Hx7;

    aput-object v1, v0, v4

    sput-object v0, LX/Hx7;->$VALUES:[LX/Hx7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2522020
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hx7;
    .locals 1

    .prologue
    .line 2522016
    const-class v0, LX/Hx7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hx7;

    return-object v0
.end method

.method public static values()[LX/Hx7;
    .locals 1

    .prologue
    .line 2522017
    sget-object v0, LX/Hx7;->$VALUES:[LX/Hx7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hx7;

    return-object v0
.end method
