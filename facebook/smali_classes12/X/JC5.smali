.class public final LX/JC5;
.super LX/J9u;
.source ""


# instance fields
.field public final synthetic a:LX/JC6;


# direct methods
.method public constructor <init>(LX/JC6;)V
    .locals 0

    .prologue
    .line 2661466
    iput-object p1, p0, LX/JC5;->a:LX/JC6;

    invoke-direct {p0}, LX/J9u;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2661467
    check-cast p1, LX/J9t;

    .line 2661468
    iget-object v0, p1, LX/J9t;->b:Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2661469
    :goto_0
    iget-object v1, p1, LX/J9t;->c:Ljava/lang/String;

    iget-object v2, p1, LX/J9t;->d:LX/JBL;

    iget-object v3, p1, LX/J9t;->e:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iget-object v4, p1, LX/J9t;->f:LX/1Fb;

    iget-object v5, p0, LX/JC5;->a:LX/JC6;

    iget-object v5, v5, LX/JC6;->d:LX/9lP;

    .line 2661470
    iget-object v6, v5, LX/9lP;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v5, v6

    .line 2661471
    iget-object v6, p0, LX/JC5;->a:LX/JC6;

    iget-object v6, v6, LX/JC6;->d:LX/9lP;

    .line 2661472
    iget-object v7, v6, LX/9lP;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v6, v7

    .line 2661473
    iget-object v7, p1, LX/J9t;->g:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, LX/J8v;->a(Landroid/os/Bundle;Ljava/lang/String;LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Ljava/lang/String;)V

    .line 2661474
    iget-object v1, p0, LX/JC5;->a:LX/JC6;

    iget-object v1, v1, LX/JC6;->a:LX/17W;

    iget-object v2, p0, LX/JC5;->a:LX/JC6;

    iget-object v2, v2, LX/JC6;->c:Landroid/content/Context;

    iget-object v3, p1, LX/J9t;->a:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2661475
    return-void

    .line 2661476
    :cond_0
    iget-object v0, p1, LX/J9t;->b:Landroid/os/Bundle;

    goto :goto_0
.end method
