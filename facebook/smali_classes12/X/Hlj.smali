.class public LX/Hlj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Hlj;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0yD;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0yD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2498887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498888
    iput-object p1, p0, LX/Hlj;->a:LX/0Zb;

    .line 2498889
    iput-object p2, p0, LX/Hlj;->b:LX/0yD;

    .line 2498890
    return-void
.end method

.method public static a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;
    .locals 8

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    .line 2498875
    iget-object v0, p0, LX/Hlj;->b:LX/0yD;

    .line 2498876
    iget-object v6, v0, LX/0yD;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {p2, v6, v7}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;J)J

    move-result-wide v6

    move-wide v0, v6

    .line 2498877
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 2498878
    const-string v2, "wall_clock_age_ms"

    invoke-virtual {p1, v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2498879
    :cond_0
    iget-object v0, p0, LX/Hlj;->b:LX/0yD;

    .line 2498880
    iget-object v6, v0, LX/0yD;->b:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    invoke-static {p2, v6, v7}, LX/0yD;->b(Lcom/facebook/location/ImmutableLocation;J)J

    move-result-wide v6

    move-wide v0, v6

    .line 2498881
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 2498882
    const-string v2, "since_boot_clock_age_ms"

    invoke-virtual {p1, v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2498883
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    .line 2498884
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2498885
    const-string v1, "accuracy_meters"

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2498886
    :cond_2
    return-object p1
.end method

.method public static a(LX/0QB;)LX/Hlj;
    .locals 5

    .prologue
    .line 2498862
    sget-object v0, LX/Hlj;->c:LX/Hlj;

    if-nez v0, :cond_1

    .line 2498863
    const-class v1, LX/Hlj;

    monitor-enter v1

    .line 2498864
    :try_start_0
    sget-object v0, LX/Hlj;->c:LX/Hlj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2498865
    if-eqz v2, :cond_0

    .line 2498866
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2498867
    new-instance p0, LX/Hlj;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v4

    check-cast v4, LX/0yD;

    invoke-direct {p0, v3, v4}, LX/Hlj;-><init>(LX/0Zb;LX/0yD;)V

    .line 2498868
    move-object v0, p0

    .line 2498869
    sput-object v0, LX/Hlj;->c:LX/Hlj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2498870
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2498871
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2498872
    :cond_1
    sget-object v0, LX/Hlj;->c:LX/Hlj;

    return-object v0

    .line 2498873
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2498874
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/Hlj;)LX/0oG;
    .locals 3

    .prologue
    .line 2498891
    iget-object v0, p0, LX/Hlj;->a:LX/0Zb;

    const-string v1, "geofence_events"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/location/ImmutableLocation;FD)V
    .locals 5

    .prologue
    .line 2498858
    invoke-static {p0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v0

    .line 2498859
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2498860
    invoke-static {p0, v0, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v0

    const-string v1, "action"

    const-string v2, "location_update_received"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "sub_action"

    const-string v2, "distance_to_last_geofence_threshold_exceeded"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "distance_to_last_geofence_meters"

    float-to-double v2, p2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    move-result-object v0

    const-string v1, "geofence_radius_meters"

    invoke-virtual {v0, v1, p3, p4}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2498861
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/location/ImmutableLocation;FI)V
    .locals 4

    .prologue
    .line 2498854
    invoke-static {p0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v0

    .line 2498855
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2498856
    invoke-static {p0, v0, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v0

    const-string v1, "action"

    const-string v2, "geofence_requested_success"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "geofence_radius_meters"

    float-to-double v2, p2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    move-result-object v0

    const-string v1, "geofence_notification_responsiveness_ms"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2498857
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/location/ImmutableLocation;FJLcom/google/android/gms/common/api/Status;)V
    .locals 5

    .prologue
    .line 2498849
    invoke-static {p0}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v0

    .line 2498850
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2498851
    invoke-static {p0, v0, p1}, LX/Hlj;->a(LX/Hlj;LX/0oG;Lcom/facebook/location/ImmutableLocation;)LX/0oG;

    move-result-object v0

    const-string v1, "action"

    const-string v2, "geofence_requested_failure"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "sub_action"

    const-string v2, "geofence_api_request_failed"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "geofence_radius_meters"

    float-to-double v2, p2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    move-result-object v0

    const-string v1, "geofence_notification_responsiveness_ms"

    invoke-virtual {v0, v1, p3, p4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v0

    const-string v1, "api_error_code"

    iget v2, p5, Lcom/google/android/gms/common/api/Status;->i:I

    move v2, v2

    .line 2498852
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2498853
    :cond_0
    return-void
.end method
