.class public LX/IpL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/InV;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612823
    iput-object p1, p0, LX/IpL;->a:Landroid/content/res/Resources;

    .line 2612824
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2612825
    iget-object v0, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2612826
    if-eqz v0, :cond_0

    .line 2612827
    iget-object v0, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2612828
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2612829
    :cond_0
    iget-object v0, p0, LX/IpL;->a:Landroid/content/res/Resources;

    const v1, 0x7f082cdc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2612830
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/IpL;->a:Landroid/content/res/Resources;

    const v1, 0x7f082cdd

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2612831
    iget-object v4, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v4, v4

    .line 2612832
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
