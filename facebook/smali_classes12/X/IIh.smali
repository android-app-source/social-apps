.class public final LX/IIh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2561928
    iput-object p1, p0, LX/IIh;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iput-object p2, p0, LX/IIh;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 2561929
    iget-object v0, p0, LX/IIh;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-eqz v0, :cond_2

    .line 2561930
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 2561931
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, LX/IIh;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 2561932
    :goto_0
    iget-object v1, p0, LX/IIh;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    if-eqz v1, :cond_0

    .line 2561933
    iget-object v1, p0, LX/IIh;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    .line 2561934
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 2561935
    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2561936
    :cond_0
    if-eq v0, p2, :cond_1

    .line 2561937
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 2561938
    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    move-object v0, p2

    goto :goto_0
.end method
