.class public final LX/IkV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceMutationModels$InvoiceEditModel;",
        "LX/Il0;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IkW;


# direct methods
.method public constructor <init>(LX/IkW;)V
    .locals 0

    .prologue
    .line 2606770
    iput-object p1, p0, LX/IkV;->a:LX/IkW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606771
    check-cast p1, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceMutationModels$InvoiceEditModel;

    .line 2606772
    if-nez p1, :cond_0

    .line 2606773
    iget-object v0, p0, LX/IkV;->a:LX/IkW;

    iget-object v0, v0, LX/IkW;->f:LX/03V;

    const-string v1, "InvoiceMutator"

    const-string v2, "Failed to mutate the invoice status"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2606774
    const/4 v0, 0x0

    .line 2606775
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IkV;->a:LX/IkW;

    iget-object v0, v0, LX/IkW;->g:LX/Il6;

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceMutationModels$InvoiceEditModel;->a()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Il6;->a(Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;)LX/Il0;

    move-result-object v0

    goto :goto_0
.end method
