.class public final LX/HmQ;
.super LX/HmN;
.source ""


# instance fields
.field public final synthetic a:LX/HmV;


# direct methods
.method public constructor <init>(LX/HmV;)V
    .locals 1

    .prologue
    .line 2500652
    iput-object p1, p0, LX/HmQ;->a:LX/HmV;

    invoke-direct {p0, p1}, LX/HmN;-><init>(LX/HmV;)V

    return-void
.end method


# virtual methods
.method public final a()LX/HmU;
    .locals 3

    .prologue
    .line 2500653
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v0}, LX/Hm4;->d()LX/Hm3;

    move-result-object v0

    .line 2500654
    if-eqz v0, :cond_0

    .line 2500655
    sget-object v1, LX/HmT;->a:[I

    invoke-virtual {v0}, LX/Hm3;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2500656
    :cond_0
    :pswitch_0
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    invoke-static {v0}, LX/HmV;->j(LX/HmV;)V

    .line 2500657
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->k:LX/HmU;

    :goto_0
    return-object v0

    .line 2500658
    :pswitch_1
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    invoke-static {v0}, LX/HmV;->k(LX/HmV;)V

    .line 2500659
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->k:LX/HmU;

    goto :goto_0

    .line 2500660
    :pswitch_2
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    sget-object v1, LX/Hm3;->INCOMPATIBLE_VERSION:LX/Hm3;

    .line 2500661
    iput-object v1, v0, LX/HmV;->n:LX/Hm3;

    .line 2500662
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    sget-object v1, LX/HmU;->SENDER_ERROR:LX/HmU;

    .line 2500663
    iput-object v1, v0, LX/HmV;->k:LX/HmU;

    move-object v0, v1

    .line 2500664
    goto :goto_0

    .line 2500665
    :pswitch_3
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    iget-object v1, p0, LX/HmQ;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v1}, LX/Hm4;->f()Ljava/lang/String;

    move-result-object v1

    .line 2500666
    iput-object v1, v0, LX/HmV;->m:Ljava/lang/String;

    .line 2500667
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    sget-object v1, LX/Hm3;->NOTHING_TO_SEND:LX/Hm3;

    .line 2500668
    iput-object v1, v0, LX/HmV;->n:LX/Hm3;

    .line 2500669
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    sget-object v1, LX/HmU;->SENDER_ERROR:LX/HmU;

    .line 2500670
    iput-object v1, v0, LX/HmV;->k:LX/HmU;

    move-object v0, v1

    .line 2500671
    goto :goto_0

    .line 2500672
    :pswitch_4
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    iget-object v1, p0, LX/HmQ;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->j:LX/Hm4;

    .line 2500673
    iget-object v2, v1, LX/Hm4;->c:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2500674
    iput-object v1, v0, LX/HmV;->p:Ljava/lang/String;

    .line 2500675
    iget-object v0, p0, LX/HmQ;->a:LX/HmV;

    sget-object v1, LX/HmU;->WAIT_RECEIVE_APK:LX/HmU;

    .line 2500676
    iput-object v1, v0, LX/HmV;->k:LX/HmU;

    move-object v0, v1

    .line 2500677
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
