.class public final LX/Hcg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:Ljava/lang/CharSequence;

.field public final c:Lcom/facebook/graphql/model/GraphQLComment;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLComment;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2487230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487231
    iput-object p1, p0, LX/Hcg;->a:Ljava/lang/CharSequence;

    .line 2487232
    iput-object p2, p0, LX/Hcg;->b:Ljava/lang/CharSequence;

    .line 2487233
    iput-object p3, p0, LX/Hcg;->c:Lcom/facebook/graphql/model/GraphQLComment;

    .line 2487234
    iput-object p4, p0, LX/Hcg;->d:Ljava/util/List;

    .line 2487235
    iput-object p5, p0, LX/Hcg;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2487236
    return-void
.end method
