.class public LX/HvT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j2;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j9;",
        "DerivedData::",
        "LX/5RE;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:Landroid/graphics/Typeface;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/1WB;

.field private final d:LX/1WC;

.field private final e:LX/0tO;

.field private final f:LX/0ad;

.field public final g:F

.field public h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Be1;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Landroid/animation/ValueAnimator;

.field public l:Landroid/animation/ValueAnimator;

.field public m:Z

.field public n:I

.field public o:LX/1z6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2519166
    const-string v0, "sans-serif-light"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, LX/HvT;->a:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/Be1;ZFLandroid/graphics/Typeface;Landroid/content/Context;LX/1WB;LX/1WC;LX/0tO;LX/0ad;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Be1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/graphics/Typeface;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Be1;",
            "ZF",
            "Landroid/graphics/Typeface;",
            "Landroid/content/Context;",
            "LX/1WB;",
            "LX/1WC;",
            "LX/0tO;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2519146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2519147
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HvT;->j:Z

    .line 2519148
    iput v2, p0, LX/HvT;->n:I

    .line 2519149
    iput-object p6, p0, LX/HvT;->b:Landroid/content/Context;

    .line 2519150
    iput-object p7, p0, LX/HvT;->c:LX/1WB;

    .line 2519151
    iget-object v0, p0, LX/HvT;->c:LX/1WB;

    .line 2519152
    iput p4, v0, LX/1WB;->f:F

    .line 2519153
    iput-object p8, p0, LX/HvT;->d:LX/1WC;

    .line 2519154
    iput-object p9, p0, LX/HvT;->e:LX/0tO;

    .line 2519155
    iput-object p10, p0, LX/HvT;->f:LX/0ad;

    .line 2519156
    iput p4, p0, LX/HvT;->g:F

    .line 2519157
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/HvT;->d:LX/1WC;

    invoke-virtual {v0}, LX/1WC;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HvT;->f:LX/0ad;

    sget-short v1, LX/0wk;->i:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2519158
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2519159
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j2;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowDynamicTextStyle()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2519160
    :cond_0
    :goto_0
    return-void

    .line 2519161
    :cond_1
    iput-object p1, p0, LX/HvT;->i:LX/0il;

    .line 2519162
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HvT;->h:Ljava/lang/ref/WeakReference;

    .line 2519163
    invoke-direct {p0}, LX/HvT;->b()V

    .line 2519164
    if-eqz p3, :cond_0

    const-string v0, "grey"

    iget-object v1, p0, LX/HvT;->f:LX/0ad;

    sget-char v2, LX/0wk;->d:C

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2519165
    iget-object v0, p0, LX/HvT;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Be1;

    iget-object v1, p0, LX/HvT;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-interface {v0, v1}, LX/Be1;->setTextColor(I)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2519133
    iput-boolean v3, p0, LX/HvT;->m:Z

    .line 2519134
    iget-object v0, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2519135
    iget-object v0, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2519136
    :cond_0
    iget-object v0, p0, LX/HvT;->o:LX/1z6;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HvT;->o:LX/1z6;

    iget v0, v0, LX/1z6;->b:I

    int-to-float v0, v0

    .line 2519137
    :goto_0
    const/4 v2, 0x0

    iput-object v2, p0, LX/HvT;->o:LX/1z6;

    .line 2519138
    iget-object v2, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2519139
    if-eqz p1, :cond_3

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_3

    .line 2519140
    iget-object v1, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    const/4 v2, 0x2

    new-array v2, v2, [F

    aput v0, v2, v3

    const/4 v0, 0x1

    iget v3, p0, LX/HvT;->g:F

    aput v3, v2, v0

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 2519141
    iget-object v0, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2519142
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 2519143
    goto :goto_0

    .line 2519144
    :cond_3
    iget-object v0, p0, LX/HvT;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Be1;

    iget v1, p0, LX/HvT;->g:F

    invoke-interface {v0, v1}, LX/Be1;->setTextSize(F)V

    .line 2519145
    sget-object v0, LX/1WB;->a:Landroid/graphics/Typeface;

    invoke-static {p0, v0}, LX/HvT;->a$redex0(LX/HvT;Landroid/graphics/Typeface;)V

    goto :goto_1
.end method

.method public static a$redex0(LX/HvT;Landroid/graphics/Typeface;)V
    .locals 2

    .prologue
    .line 2519129
    iget-object v0, p0, LX/HvT;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2519130
    iget-object v0, p0, LX/HvT;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Be1;

    iget-object v1, p0, LX/HvT;->i:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j2;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0, p1, v1}, LX/Be1;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2519131
    :cond_0
    return-void

    .line 2519132
    :cond_1
    iget-object v1, p0, LX/HvT;->i:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j2;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v1

    invoke-static {v1}, LX/87X;->a(LX/5RY;)I

    move-result v1

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2519167
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iget-object v1, p0, LX/HvT;->c:LX/1WB;

    invoke-virtual {v1}, LX/1WB;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    .line 2519168
    iget-object v0, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/HvT;->c:LX/1WB;

    invoke-virtual {v1}, LX/1WB;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 2519169
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iget-object v1, p0, LX/HvT;->c:LX/1WB;

    invoke-virtual {v1}, LX/1WB;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    .line 2519170
    iget-object v0, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/HvT;->c:LX/1WB;

    invoke-virtual {v1}, LX/1WB;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 2519171
    new-instance v0, LX/HvQ;

    invoke-direct {v0, p0}, LX/HvQ;-><init>(LX/HvT;)V

    .line 2519172
    iget-object v1, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2519173
    iget-object v1, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2519174
    iget-object v0, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    new-instance v1, LX/HvR;

    invoke-direct {v1, p0}, LX/HvR;-><init>(LX/HvT;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2519175
    iget-object v0, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    new-instance v1, LX/HvS;

    invoke-direct {v1, p0}, LX/HvS;-><init>(LX/HvT;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2519176
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2519097
    iget-object v0, p0, LX/HvT;->i:LX/0il;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HvT;->h:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 2519098
    :cond_0
    :goto_0
    return-void

    .line 2519099
    :cond_1
    iget-object v0, p0, LX/HvT;->i:LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v3, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    if-eq v0, v3, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2519100
    if-eqz v0, :cond_2

    .line 2519101
    iget-object v0, p0, LX/HvT;->i:LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v3, LX/5RF;->STICKER:LX/5RF;

    if-ne v0, v3, :cond_7

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2519102
    if-nez v0, :cond_2

    .line 2519103
    invoke-direct {p0, v2}, LX/HvT;->a(Z)V

    goto :goto_0

    .line 2519104
    :cond_2
    iget-object v3, p0, LX/HvT;->c:LX/1WB;

    iget-object v0, p0, LX/HvT;->i:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j2;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 2519105
    iget-object v0, p0, LX/HvT;->i:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j2;

    check-cast v0, LX/0j9;

    invoke-interface {v0}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    .line 2519106
    if-eqz v0, :cond_8

    sget-object v5, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v0, v5}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2519107
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HvT;->e:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->d()I

    move-result v0

    if-le v0, v1, :cond_3

    move v0, v1

    :goto_4
    invoke-virtual {v3, v4, v0}, LX/1WB;->a(Ljava/lang/String;Z)LX/1z6;

    move-result-object v0

    .line 2519108
    if-nez v0, :cond_4

    .line 2519109
    invoke-direct {p0, v1}, LX/HvT;->a(Z)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2519110
    goto :goto_4

    .line 2519111
    :cond_4
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2519112
    iput-boolean v5, p0, LX/HvT;->m:Z

    .line 2519113
    iget-object v1, p0, LX/HvT;->i:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j2;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    .line 2519114
    iget-object v3, p0, LX/HvT;->o:LX/1z6;

    if-ne v3, v0, :cond_9

    if-eqz v1, :cond_5

    iget v3, p0, LX/HvT;->n:I

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v4

    invoke-static {v4}, LX/87X;->a(LX/5RY;)I

    move-result v4

    if-ne v3, v4, :cond_9

    .line 2519115
    :cond_5
    :goto_5
    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 2519116
    :cond_9
    iput-object v0, p0, LX/HvT;->o:LX/1z6;

    .line 2519117
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v1

    invoke-static {v1}, LX/87X;->a(LX/5RY;)I

    move-result v1

    :goto_6
    iput v1, p0, LX/HvT;->n:I

    .line 2519118
    iget-boolean v1, p0, LX/HvT;->j:Z

    if-eqz v1, :cond_b

    .line 2519119
    iget-object v1, p0, LX/HvT;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Be1;

    iget v3, v0, LX/1z6;->b:I

    int-to-float v3, v3

    invoke-interface {v1, v3}, LX/Be1;->setTextSize(F)V

    .line 2519120
    invoke-static {p0}, LX/HvT;->g(LX/HvT;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-static {p0, v1}, LX/HvT;->a$redex0(LX/HvT;Landroid/graphics/Typeface;)V

    .line 2519121
    iput-boolean v2, p0, LX/HvT;->j:Z

    goto :goto_5

    :cond_a
    move v1, v2

    .line 2519122
    goto :goto_6

    .line 2519123
    :cond_b
    iget-object v1, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2519124
    iget-object v1, p0, LX/HvT;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2519125
    :cond_c
    iget-object v1, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2519126
    iget-object v1, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2519127
    :cond_d
    iget-object v1, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    iget v4, p0, LX/HvT;->g:F

    aput v4, v3, v2

    iget v2, v0, LX/1z6;->b:I

    int-to-float v2, v2

    aput v2, v3, v5

    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 2519128
    iget-object v1, p0, LX/HvT;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_5
.end method

.method public static g(LX/HvT;)Landroid/graphics/Typeface;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2519093
    iget-object v0, p0, LX/HvT;->i:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j2;

    check-cast v0, LX/0j9;

    invoke-interface {v0}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HvT;->i:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j2;

    check-cast v0, LX/0j9;

    invoke-interface {v0}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v0

    sget-object v2, LX/5RY;->NORMAL:LX/5RY;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, LX/HvT;->e:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->d()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 2519094
    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "sans-serif"

    iget-object v0, p0, LX/HvT;->i:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j2;

    check-cast v0, LX/0j9;

    invoke-interface {v0}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v0

    invoke-static {v0}, LX/87X;->a(LX/5RY;)I

    move-result v0

    invoke-static {v1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2519095
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2519096
    :cond_1
    iget-object v0, p0, LX/HvT;->o:LX/1z6;

    iget-object v0, v0, LX/1z6;->f:Ljava/lang/String;

    iget-object v1, p0, LX/HvT;->o:LX/1z6;

    iget v1, v1, LX/1z6;->g:I

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 1

    .prologue
    .line 2519090
    sget-object v0, LX/5L2;->ON_FIRST_DRAW:LX/5L2;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/5L2;->ON_STATUS_TEXT_CHANGED:LX/5L2;

    if-ne p1, v0, :cond_1

    .line 2519091
    :cond_0
    invoke-direct {p0}, LX/HvT;->c()V

    .line 2519092
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2519088
    invoke-direct {p0}, LX/HvT;->c()V

    .line 2519089
    return-void
.end method
