.class public LX/JFc;
.super LX/JF9;
.source ""

# interfaces
.implements LX/JEu;


# instance fields
.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 2

    invoke-direct {p0, p1, p2}, LX/JF9;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    const-string v0, "place_id"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JFc;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 9

    new-instance v1, LX/JF7;

    invoke-direct {v1}, LX/JF7;-><init>()V

    const-string v2, "place_address"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/JF7;->m:Ljava/lang/String;

    move-object v1, v1

    const-string v2, "place_attributions"

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v2, v4}, LX/JF9;->a(LX/JF9;Ljava/lang/String;[B)[B

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    move-object v2, v3

    move-object v2, v2

    iput-object v2, v1, LX/JF7;->o:Ljava/util/List;

    move-object v1, v1

    iget-object v2, p0, LX/JFc;->c:Ljava/lang/String;

    move-object v2, v2

    iput-object v2, v1, LX/JF7;->b:Ljava/lang/String;

    move-object v1, v1

    const-string v2, "place_is_permanently_closed"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;Z)Z

    move-result v2

    move v2, v2

    iput-boolean v2, v1, LX/JF7;->h:Z

    move-object v1, v1

    invoke-virtual {p0}, LX/JFc;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iput-object v2, v1, LX/JF7;->d:Lcom/google/android/gms/maps/model/LatLng;

    move-object v1, v1

    const-string v2, "place_level_number"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;F)F

    move-result v2

    move v2, v2

    iput v2, v1, LX/JF7;->e:F

    move-object v1, v1

    const-string v2, "place_name"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/JF7;->c:Ljava/lang/String;

    move-object v1, v1

    const-string v2, "place_phone_number"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/JF7;->n:Ljava/lang/String;

    move-object v1, v1

    const-string v2, "place_price_level"

    const/4 v3, -0x1

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;I)I

    move-result v2

    move v2, v2

    iput v2, v1, LX/JF7;->j:I

    move-object v1, v1

    const-string v2, "place_rating"

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;F)F

    move-result v2

    move v2, v2

    iput v2, v1, LX/JF7;->i:F

    move-object v1, v1

    const-string v2, "place_types"

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    move-object v2, v2

    iput-object v2, v1, LX/JF7;->l:Ljava/util/List;

    move-object v1, v1

    const-string v2, "place_viewport"

    sget-object v3, Lcom/google/android/gms/maps/model/LatLngBounds;->CREATOR:LX/7cg;

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;Landroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/model/LatLngBounds;

    move-object v2, v2

    iput-object v2, v1, LX/JF7;->f:Lcom/google/android/gms/maps/model/LatLngBounds;

    move-object v1, v1

    const/4 v2, 0x0

    const-string v3, "place_website_uri"

    invoke-virtual {p0, v3, v2}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    move-object v2, v2

    iput-object v2, v1, LX/JF7;->g:Landroid/net/Uri;

    move-object v1, v1

    const-string v5, "place_timestamp_secs"

    const-wide/16 v7, 0x0

    invoke-virtual {p0, v5, v7, v8}, LX/JF9;->a(Ljava/lang/String;J)J

    move-result-wide v5

    move-wide v3, v5

    iput-wide v3, v1, LX/JF7;->k:J

    move-object v1, v1

    invoke-virtual {v1}, LX/JF7;->a()Lcom/google/android/gms/location/places/internal/PlaceEntity;

    move-result-object v1

    const-string v2, "place_locale_language"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "place_locale_country"

    const-string v4, ""

    invoke-virtual {p0, v2, v4}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v2, Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move-object v2, v2

    iput-object v2, v1, Lcom/google/android/gms/location/places/internal/PlaceEntity;->w:Ljava/util/Locale;

    move-object v0, v1

    return-object v0

    :cond_1
    :try_start_0
    invoke-static {v4}, LX/JEp;->a([B)LX/JEp;

    move-result-object v4

    iget-object v5, v4, LX/JEp;->c:[Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v4, v4, LX/JEp;->c:[Ljava/lang/String;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;
    :try_end_0
    .catch LX/4uB; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto/16 :goto_0

    :catch_0
    move-exception v4

    const-string v5, "SafeDataBufferRef"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "SafeDataBufferRef"

    const-string v6, "Cannot parse byte[]"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :cond_2
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    :cond_3
    const-string v2, "place_locale"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    goto :goto_2
.end method

.method public final b()Lcom/google/android/gms/maps/model/LatLng;
    .locals 2

    const-string v0, "place_lat_lng"

    sget-object v1, Lcom/google/android/gms/maps/model/LatLng;->CREATOR:LX/7ch;

    invoke-virtual {p0, v0, v1}, LX/JF9;->a(Ljava/lang/String;Landroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method
