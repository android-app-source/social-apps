.class public final LX/J4I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public f:LX/8SR;

.field public g:Ljava/lang/String;

.field public h:LX/1Fd;

.field public i:Z


# direct methods
.method public constructor <init>(LX/J4J;)V
    .locals 1

    .prologue
    .line 2643901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643902
    iget-object v0, p1, LX/J4J;->a:Ljava/lang/String;

    iput-object v0, p0, LX/J4I;->a:Ljava/lang/String;

    .line 2643903
    iget-object v0, p1, LX/J4J;->b:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    iput-object v0, p0, LX/J4I;->b:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 2643904
    iget-object v0, p1, LX/J4J;->c:Ljava/lang/String;

    iput-object v0, p0, LX/J4I;->c:Ljava/lang/String;

    .line 2643905
    iget-object v0, p1, LX/J4J;->d:Ljava/lang/String;

    iput-object v0, p0, LX/J4I;->d:Ljava/lang/String;

    .line 2643906
    iget-object v0, p1, LX/J4J;->e:Ljava/lang/String;

    iput-object v0, p0, LX/J4I;->e:Ljava/lang/String;

    .line 2643907
    iget-object v0, p1, LX/J4J;->f:LX/8SR;

    iput-object v0, p0, LX/J4I;->f:LX/8SR;

    .line 2643908
    iget-object v0, p1, LX/J4J;->g:Ljava/lang/String;

    iput-object v0, p0, LX/J4I;->g:Ljava/lang/String;

    .line 2643909
    iget-object v0, p1, LX/J4J;->h:LX/1Fd;

    iput-object v0, p0, LX/J4I;->h:LX/1Fd;

    .line 2643910
    iget-boolean v0, p1, LX/J4J;->i:Z

    iput-boolean v0, p0, LX/J4I;->i:Z

    .line 2643911
    return-void
.end method


# virtual methods
.method public final a()LX/J4J;
    .locals 10

    .prologue
    .line 2643912
    new-instance v0, LX/J4J;

    iget-object v1, p0, LX/J4I;->a:Ljava/lang/String;

    iget-object v2, p0, LX/J4I;->b:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    iget-object v3, p0, LX/J4I;->c:Ljava/lang/String;

    iget-object v4, p0, LX/J4I;->d:Ljava/lang/String;

    iget-object v5, p0, LX/J4I;->e:Ljava/lang/String;

    iget-object v6, p0, LX/J4I;->f:LX/8SR;

    iget-object v7, p0, LX/J4I;->g:Ljava/lang/String;

    iget-object v8, p0, LX/J4I;->h:LX/1Fd;

    iget-boolean v9, p0, LX/J4I;->i:Z

    invoke-direct/range {v0 .. v9}, LX/J4J;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8SR;Ljava/lang/String;LX/1Fd;Z)V

    return-object v0
.end method
