.class public LX/HWD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private a:LX/HXC;


# direct methods
.method public constructor <init>(LX/HXC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475732
    iput-object p1, p0, LX/HWD;->a:LX/HXC;

    .line 2475733
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 2475734
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v6, -0x1

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "profile_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v6, "extra_page_tab_entry_point"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move v6, v5

    invoke-static/range {v0 .. v7}, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;ZZLjava/lang/String;)Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    move-result-object v6

    .line 2475735
    iget-object v0, p0, LX/HWD;->a:LX/HXC;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, LX/HXC;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;Ljava/lang/String;Landroid/os/Bundle;Z)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    .line 2475736
    invoke-virtual {v6, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->a(Lcom/facebook/base/fragment/FbFragment;)V

    .line 2475737
    return-object v6
.end method
