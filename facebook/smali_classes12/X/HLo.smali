.class public LX/HLo;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Landroid/text/style/TextAppearanceSpan;

.field public d:Landroid/text/style/TextAppearanceSpan;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2456369
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456370
    const v0, 0x7f030e1f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456371
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, LX/HLo;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b230f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, LX/HLo;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2456372
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/HLo;->setOrientation(I)V

    .line 2456373
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/HLo;->setGravity(I)V

    .line 2456374
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, LX/HLo;->setBackgroundResource(I)V

    .line 2456375
    const v0, 0x7f0d228d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/HLo;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2456376
    const v0, 0x7f0d228c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HLo;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2456377
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e0baf

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/HLo;->c:Landroid/text/style/TextAppearanceSpan;

    .line 2456378
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e0bb0

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/HLo;->d:Landroid/text/style/TextAppearanceSpan;

    .line 2456379
    return-void
.end method
