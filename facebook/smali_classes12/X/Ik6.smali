.class public LX/Ik6;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomLinearLayout;"
    }
.end annotation


# instance fields
.field public a:LX/IlN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IkR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IkD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IkL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

.field private f:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private j:Lcom/facebook/widget/text/BetterTextView;

.field private k:Lcom/facebook/widget/text/BetterTextView;

.field private l:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private m:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private n:Lcom/facebook/resources/ui/FbTextView;

.field private o:Landroid/widget/LinearLayout;

.field private p:Lcom/facebook/resources/ui/FbTextView;

.field private q:Lcom/facebook/widget/text/BetterButton;

.field private r:Lcom/facebook/resources/ui/FbTextView;

.field private s:Lcom/facebook/resources/ui/FbTextView;

.field private t:Lcom/facebook/resources/ui/FbTextView;

.field private u:Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

.field private v:Lcom/facebook/payments/ui/SingleItemInfoView;

.field private w:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private x:Lcom/facebook/widget/text/BetterButton;

.field private y:Lcom/facebook/widget/text/BetterButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2606382
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Ik6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2606383
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2606384
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ik6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606385
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2606386
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606387
    const-class v0, LX/Ik6;

    invoke-static {v0, p0}, LX/Ik6;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2606388
    const v0, 0x7f030de7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2606389
    const v0, 0x7f0d1c3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    iput-object v0, p0, LX/Ik6;->e:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    .line 2606390
    const v0, 0x7f0d1c3b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, LX/Ik6;->f:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2606391
    const v0, 0x7f0d221e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik6;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2606392
    const v0, 0x7f0d2965

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik6;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2606393
    const v0, 0x7f0d294c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik6;->i:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606394
    const v0, 0x7f0d294d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ik6;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 2606395
    const v0, 0x7f0d294e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ik6;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2606396
    const v0, 0x7f0d294a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik6;->l:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606397
    const v0, 0x7f0d294b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik6;->m:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606398
    const v0, 0x7f0d295b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik6;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2606399
    const v0, 0x7f0d295c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Ik6;->o:Landroid/widget/LinearLayout;

    .line 2606400
    const v0, 0x7f0d295a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik6;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2606401
    const v0, 0x7f0d295d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, LX/Ik6;->q:Lcom/facebook/widget/text/BetterButton;

    .line 2606402
    const v0, 0x7f0d295e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik6;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 2606403
    const v0, 0x7f0d2969

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik6;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 2606404
    const v0, 0x7f0d2968

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik6;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 2606405
    const v0, 0x7f0d2966

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, LX/Ik6;->x:Lcom/facebook/widget/text/BetterButton;

    .line 2606406
    const v0, 0x7f0d2967

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, LX/Ik6;->y:Lcom/facebook/widget/text/BetterButton;

    .line 2606407
    const v0, 0x7f0d1aac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/SingleItemInfoView;

    iput-object v0, p0, LX/Ik6;->v:Lcom/facebook/payments/ui/SingleItemInfoView;

    .line 2606408
    const v0, 0x7f0d2217

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik6;->w:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606409
    const v0, 0x7f0d1c3a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

    iput-object v0, p0, LX/Ik6;->u:Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

    .line 2606410
    return-void
.end method

.method private static a(LX/Ik6;LX/IlN;LX/IkR;LX/IkD;LX/IkL;)V
    .locals 0

    .prologue
    .line 2606411
    iput-object p1, p0, LX/Ik6;->a:LX/IlN;

    iput-object p2, p0, LX/Ik6;->b:LX/IkR;

    iput-object p3, p0, LX/Ik6;->c:LX/IkD;

    iput-object p4, p0, LX/Ik6;->d:LX/IkL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Ik6;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, LX/Ik6;

    invoke-static {v3}, LX/IlN;->a(LX/0QB;)LX/IlN;

    move-result-object v0

    check-cast v0, LX/IlN;

    invoke-static {v3}, LX/IkR;->a(LX/0QB;)LX/IkR;

    move-result-object v1

    check-cast v1, LX/IkR;

    invoke-static {v3}, LX/IkD;->a(LX/0QB;)LX/IkD;

    move-result-object v2

    check-cast v2, LX/IkD;

    invoke-static {v3}, LX/IkL;->a(LX/0QB;)LX/IkL;

    move-result-object v3

    check-cast v3, LX/IkL;

    invoke-static {p0, v0, v1, v2, v3}, LX/Ik6;->a(LX/Ik6;LX/IlN;LX/IkR;LX/IkD;LX/IkL;)V

    return-void
.end method
