.class public LX/IQr;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/components/ComponentView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2575822
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/IQr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2575823
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575830
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/IQr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2575831
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575832
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2575833
    new-instance p1, Lcom/facebook/components/ComponentView;

    invoke-virtual {p0}, LX/IQr;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, LX/IQr;->a:Lcom/facebook/components/ComponentView;

    .line 2575834
    iget-object p1, p0, LX/IQr;->a:Lcom/facebook/components/ComponentView;

    invoke-virtual {p0, p1}, LX/IQr;->addView(Landroid/view/View;)V

    .line 2575835
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2575824
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2575825
    invoke-static {p1}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    invoke-static {v0}, LX/DJw;->c(LX/DZC;)Ljava/lang/String;

    move-result-object v0

    .line 2575826
    new-instance v1, LX/1De;

    invoke-virtual {p0}, LX/IQr;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2575827
    invoke-static {v1}, LX/DOy;->c(LX/1De;)LX/DOw;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/DOw;->b(Ljava/lang/String;)LX/DOw;

    move-result-object v0

    invoke-static {v1, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 2575828
    iget-object v1, p0, LX/IQr;->a:Lcom/facebook/components/ComponentView;

    invoke-virtual {v1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2575829
    return-void
.end method
