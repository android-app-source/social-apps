.class public final LX/IR9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V
    .locals 0

    .prologue
    .line 2576172
    iput-object p1, p0, LX/IR9;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 2576173
    iget-object v0, p0, LX/IR9;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    .line 2576174
    const-string v1, "%1$s %2$s %3$s"

    const/4 p0, 0x3

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object p2, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    aput-object p2, p0, p1

    const/4 p1, 0x1

    iget-object p2, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->a:LX/0Or;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p2

    aput-object p2, p0, p1

    const/4 p1, 0x2

    iget-object p2, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->n:Ljava/lang/String;

    aput-object p2, p0, p1

    invoke-static {v1, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2576175
    new-instance p1, LX/4Fu;

    invoke-direct {p1}, LX/4Fu;-><init>()V

    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2576176
    const-string p2, "actor_id"

    invoke-virtual {p1, p2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576177
    move-object v1, p1

    .line 2576178
    const-string p1, "client_mutation_id"

    invoke-virtual {v1, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576179
    move-object v1, v1

    .line 2576180
    iget-object p0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->s:Ljava/lang/String;

    .line 2576181
    const-string p1, "topic_id"

    invoke-virtual {v1, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576182
    move-object v1, v1

    .line 2576183
    new-instance p0, LX/9TM;

    invoke-direct {p0}, LX/9TM;-><init>()V

    move-object p0, p0

    .line 2576184
    const-string p1, "input"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2576185
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2576186
    iget-object p0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->b:LX/0tX;

    invoke-virtual {p0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2576187
    new-instance p0, LX/IRC;

    invoke-direct {p0, v0}, LX/IRC;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V

    iget-object p1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->c:LX/0Tf;

    invoke-static {v1, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2576188
    return-void
.end method
