.class public final LX/HJs;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/HJu;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/HJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HJu",
            "<TE;>.PageOpenHoursComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/HJu;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/HJu;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2453395
    iput-object p1, p0, LX/HJs;->b:LX/HJu;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2453396
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "isOutOfBound"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/HJs;->c:[Ljava/lang/String;

    .line 2453397
    iput v3, p0, LX/HJs;->d:I

    .line 2453398
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/HJs;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/HJs;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/HJs;LX/1De;IILX/HJt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/HJu",
            "<TE;>.PageOpenHoursComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2453391
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2453392
    iput-object p4, p0, LX/HJs;->a:LX/HJt;

    .line 2453393
    iget-object v0, p0, LX/HJs;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2453394
    return-void
.end method


# virtual methods
.method public final a(LX/2km;)LX/HJs;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/HJu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2453388
    iget-object v0, p0, LX/HJs;->a:LX/HJt;

    iput-object p1, v0, LX/HJt;->c:LX/2km;

    .line 2453389
    iget-object v0, p0, LX/HJs;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2453390
    return-object p0
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/HJs;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/HJu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2453399
    iget-object v0, p0, LX/HJs;->a:LX/HJt;

    iput-object p1, v0, LX/HJt;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2453400
    iget-object v0, p0, LX/HJs;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2453401
    return-object p0
.end method

.method public final a(Z)LX/HJs;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/HJu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2453385
    iget-object v0, p0, LX/HJs;->a:LX/HJt;

    iput-boolean p1, v0, LX/HJt;->a:Z

    .line 2453386
    iget-object v0, p0, LX/HJs;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2453387
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2453381
    invoke-super {p0}, LX/1X5;->a()V

    .line 2453382
    const/4 v0, 0x0

    iput-object v0, p0, LX/HJs;->a:LX/HJt;

    .line 2453383
    iget-object v0, p0, LX/HJs;->b:LX/HJu;

    iget-object v0, v0, LX/HJu;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2453384
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/HJu;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2453371
    iget-object v1, p0, LX/HJs;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HJs;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/HJs;->d:I

    if-ge v1, v2, :cond_2

    .line 2453372
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2453373
    :goto_0
    iget v2, p0, LX/HJs;->d:I

    if-ge v0, v2, :cond_1

    .line 2453374
    iget-object v2, p0, LX/HJs;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2453375
    iget-object v2, p0, LX/HJs;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2453376
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2453377
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2453378
    :cond_2
    iget-object v0, p0, LX/HJs;->a:LX/HJt;

    .line 2453379
    invoke-virtual {p0}, LX/HJs;->a()V

    .line 2453380
    return-object v0
.end method
