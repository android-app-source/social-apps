.class public LX/Itp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/18V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/FKz;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Mw;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3QL;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Lw;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itm;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6f6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2624626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2624627
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624628
    iput-object v0, p0, LX/Itp;->d:LX/0Ot;

    .line 2624629
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624630
    iput-object v0, p0, LX/Itp;->e:LX/0Ot;

    .line 2624631
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624632
    iput-object v0, p0, LX/Itp;->f:LX/0Ot;

    .line 2624633
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624634
    iput-object v0, p0, LX/Itp;->g:LX/0Ot;

    .line 2624635
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624636
    iput-object v0, p0, LX/Itp;->h:LX/0Ot;

    .line 2624637
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624638
    iput-object v0, p0, LX/Itp;->i:LX/0Ot;

    .line 2624639
    return-void
.end method

.method private static a(LX/Itp;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2624640
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 2624641
    iput-object p2, v0, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    .line 2624642
    iget-object v1, p0, LX/Itp;->b:LX/18V;

    iget-object v2, p0, LX/Itp;->c:LX/FKz;

    invoke-virtual {v1, v2, p1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2624643
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v1

    const-string v2, "Montage thread key must be a group"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2624644
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v1

    .line 2624645
    iput-object v0, v1, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2624646
    move-object v1, v1

    .line 2624647
    sget-object v2, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v1, v2}, LX/6f7;->a(LX/6f3;)LX/6f7;

    move-result-object v1

    new-instance v2, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v3, v3, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    invoke-direct {v2, v0, v3}, Lcom/facebook/messaging/model/send/PendingSendQueueKey;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/6fM;)V

    .line 2624648
    iput-object v2, v1, LX/6f7;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 2624649
    move-object v0, v1

    .line 2624650
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2624651
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    iget-object v0, p0, LX/Itp;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 19

    .prologue
    .line 2624652
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itp;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6f6;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v2, v3}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v6

    .line 2624653
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624654
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itp;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v16

    .line 2624655
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itp;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3QL;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/Itp;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result v3

    sget-object v4, LX/FCY;->GRAPH:LX/FCY;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v6, v3, v4}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;ZLX/FCY;)V

    .line 2624656
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itp;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Lw;

    sget-object v3, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual {v2, v3, v15}, LX/2Lw;->a(LX/FCY;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624657
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, LX/Itp;->a(LX/Itp;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v18

    .line 2624658
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itp;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3QL;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/Itp;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v4, v4, v16

    const-string v7, "via_graph"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/Itp;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result v12

    sget-object v13, LX/FCY;->GRAPH:LX/FCY;

    const/4 v14, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v14}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZLX/FCY;Z)V

    .line 2624659
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itp;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Lw;

    sget-object v3, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/messaging/service/model/NewMessageResult;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/2Lw;->a(LX/FCY;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2624660
    return-object v18

    .line 2624661
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 2624662
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Itm;

    sget-object v4, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v2, v3, v15, v4}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v7

    .line 2624663
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itp;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3QL;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/Itp;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v4, v4, v16

    move-object/from16 v0, p0

    iget-object v3, v0, LX/Itp;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result v8

    const-string v9, "via_graph"

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v9}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;LX/FKG;ZLjava/lang/String;)V

    .line 2624664
    throw v7
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 4

    .prologue
    .line 2624665
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-static {p0, v0, v1}, LX/Itp;->a(LX/Itp;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v1

    .line 2624666
    iget-object v0, p0, LX/Itp;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Mw;

    .line 2624667
    iget-object v2, v1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v2

    .line 2624668
    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2Mw;->a(J)V

    .line 2624669
    return-object v1
.end method
