.class public final enum LX/Ie7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ie7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ie7;

.field public static final enum NOTICE_ACCEPTED:LX/Ie7;

.field public static final enum NOTICE_DECLINED:LX/Ie7;

.field public static final enum NOTICE_SKIPPED:LX/Ie7;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2598317
    new-instance v0, LX/Ie7;

    const-string v1, "NOTICE_SKIPPED"

    invoke-direct {v0, v1, v2}, LX/Ie7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ie7;->NOTICE_SKIPPED:LX/Ie7;

    .line 2598318
    new-instance v0, LX/Ie7;

    const-string v1, "NOTICE_DECLINED"

    invoke-direct {v0, v1, v3}, LX/Ie7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ie7;->NOTICE_DECLINED:LX/Ie7;

    .line 2598319
    new-instance v0, LX/Ie7;

    const-string v1, "NOTICE_ACCEPTED"

    invoke-direct {v0, v1, v4}, LX/Ie7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ie7;->NOTICE_ACCEPTED:LX/Ie7;

    .line 2598320
    const/4 v0, 0x3

    new-array v0, v0, [LX/Ie7;

    sget-object v1, LX/Ie7;->NOTICE_SKIPPED:LX/Ie7;

    aput-object v1, v0, v2

    sget-object v1, LX/Ie7;->NOTICE_DECLINED:LX/Ie7;

    aput-object v1, v0, v3

    sget-object v1, LX/Ie7;->NOTICE_ACCEPTED:LX/Ie7;

    aput-object v1, v0, v4

    sput-object v0, LX/Ie7;->$VALUES:[LX/Ie7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2598321
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ie7;
    .locals 1

    .prologue
    .line 2598322
    const-class v0, LX/Ie7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ie7;

    return-object v0
.end method

.method public static values()[LX/Ie7;
    .locals 1

    .prologue
    .line 2598323
    sget-object v0, LX/Ie7;->$VALUES:[LX/Ie7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ie7;

    return-object v0
.end method
