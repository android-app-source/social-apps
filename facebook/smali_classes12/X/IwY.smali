.class public LX/IwY;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/pages/browser/data/methods/FetchRecommendedPagesInCategory$Params;",
        "Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2630686
    invoke-direct {p0, p2}, LX/0ro;-><init>(LX/0sO;)V

    .line 2630687
    iput-object p1, p0, LX/IwY;->b:Landroid/content/res/Resources;

    .line 2630688
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2630689
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    .line 2630690
    if-nez v0, :cond_0

    .line 2630691
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid JSON result"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2630692
    :cond_0
    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2630685
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2630679
    check-cast p1, Lcom/facebook/pages/browser/data/methods/FetchRecommendedPagesInCategory$Params;

    .line 2630680
    iget-object v0, p0, LX/IwY;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b2448

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2630681
    new-instance v1, LX/IwF;

    invoke-direct {v1}, LX/IwF;-><init>()V

    move-object v1, v1

    .line 2630682
    const-string v2, "num_recommended_pages_in_list"

    const-string v3, "50"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_pic_image_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "category"

    .line 2630683
    iget-object v2, p1, Lcom/facebook/pages/browser/data/methods/FetchRecommendedPagesInCategory$Params;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2630684
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
