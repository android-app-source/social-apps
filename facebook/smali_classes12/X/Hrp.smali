.class public final LX/Hrp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/album/activity/AlbumSelectorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/album/activity/AlbumSelectorActivity;)V
    .locals 0

    .prologue
    .line 2513596
    iput-object p1, p0, LX/Hrp;->a:Lcom/facebook/composer/album/activity/AlbumSelectorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2513597
    iget-object v0, p0, LX/Hrp;->a:Lcom/facebook/composer/album/activity/AlbumSelectorActivity;

    invoke-virtual {v0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->onBackPressed()V

    .line 2513598
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;Z)V
    .locals 3

    .prologue
    .line 2513599
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2513600
    const-string v1, "extra_selected_album"

    invoke-static {v0, v1, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2513601
    const-string v1, "extra_selected_album_is_new"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2513602
    const-string v1, "extra_canceled_creation"

    iget-object v2, p0, LX/Hrp;->a:Lcom/facebook/composer/album/activity/AlbumSelectorActivity;

    iget-boolean v2, v2, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->v:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2513603
    iget-object v1, p0, LX/Hrp;->a:Lcom/facebook/composer/album/activity/AlbumSelectorActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 2513604
    iget-object v0, p0, LX/Hrp;->a:Lcom/facebook/composer/album/activity/AlbumSelectorActivity;

    invoke-virtual {v0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->finish()V

    .line 2513605
    return-void
.end method
