.class public final LX/IzZ;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field private static final p:LX/0sv;

.field private static final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 2634672
    new-instance v0, LX/0U1;

    const-string v1, "transaction_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->a:LX/0U1;

    .line 2634673
    new-instance v0, LX/0U1;

    const-string v1, "sender_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->b:LX/0U1;

    .line 2634674
    new-instance v0, LX/0U1;

    const-string v1, "receiver_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->c:LX/0U1;

    .line 2634675
    new-instance v0, LX/0U1;

    const-string v1, "transfer_status"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->d:LX/0U1;

    .line 2634676
    new-instance v0, LX/0U1;

    const-string v1, "creation_time"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->e:LX/0U1;

    .line 2634677
    new-instance v0, LX/0U1;

    const-string v1, "updated_time"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->f:LX/0U1;

    .line 2634678
    new-instance v0, LX/0U1;

    const-string v1, "completed_time"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->g:LX/0U1;

    .line 2634679
    new-instance v0, LX/0U1;

    const-string v1, "raw_amount"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->h:LX/0U1;

    .line 2634680
    new-instance v0, LX/0U1;

    const-string v1, "amount_offset"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->i:LX/0U1;

    .line 2634681
    new-instance v0, LX/0U1;

    const-string v1, "currency"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->j:LX/0U1;

    .line 2634682
    new-instance v0, LX/0U1;

    const-string v1, "raw_amount_fb_discount"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->k:LX/0U1;

    .line 2634683
    new-instance v0, LX/0U1;

    const-string v1, "memo_text"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->l:LX/0U1;

    .line 2634684
    new-instance v0, LX/0U1;

    const-string v1, "theme"

    const-string v2, "THEME"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->m:LX/0U1;

    .line 2634685
    new-instance v0, LX/0U1;

    const-string v1, "platform_item"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->n:LX/0U1;

    .line 2634686
    new-instance v0, LX/0U1;

    const-string v1, "commerce_order"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzZ;->o:LX/0U1;

    .line 2634687
    new-instance v0, LX/0su;

    sget-object v1, LX/IzZ;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/IzZ;->p:LX/0sv;

    .line 2634688
    sget-object v0, LX/IzZ;->a:LX/0U1;

    sget-object v1, LX/IzZ;->b:LX/0U1;

    sget-object v2, LX/IzZ;->c:LX/0U1;

    sget-object v3, LX/IzZ;->d:LX/0U1;

    sget-object v4, LX/IzZ;->e:LX/0U1;

    sget-object v5, LX/IzZ;->f:LX/0U1;

    sget-object v6, LX/IzZ;->g:LX/0U1;

    sget-object v7, LX/IzZ;->h:LX/0U1;

    sget-object v8, LX/IzZ;->i:LX/0U1;

    sget-object v9, LX/IzZ;->j:LX/0U1;

    sget-object v10, LX/IzZ;->k:LX/0U1;

    sget-object v11, LX/IzZ;->l:LX/0U1;

    const/4 v12, 0x3

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/IzZ;->m:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, LX/IzZ;->n:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, LX/IzZ;->o:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IzZ;->q:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2634689
    const-string v0, "transactions"

    sget-object v1, LX/IzZ;->q:LX/0Px;

    sget-object v2, LX/IzZ;->p:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 2634690
    return-void
.end method
