.class public final LX/IY6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/instantarticles/view/ShareBar;


# direct methods
.method public constructor <init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2587360
    iput-object p1, p0, LX/IY6;->c:Lcom/facebook/instantarticles/view/ShareBar;

    iput-object p2, p0, LX/IY6;->a:Ljava/lang/String;

    iput-object p3, p0, LX/IY6;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2587361
    iget-object v0, p0, LX/IY6;->c:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, p0, LX/IY6;->a:Ljava/lang/String;

    .line 2587362
    invoke-static {v0, v1}, Lcom/facebook/instantarticles/view/ShareBar;->a$redex0(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V

    .line 2587363
    iget-object v0, p0, LX/IY6;->a:Ljava/lang/String;

    const-string v1, "twitter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2587364
    iget-object v0, p0, LX/IY6;->c:Lcom/facebook/instantarticles/view/ShareBar;

    const-string v1, "share_to_twitter"

    iget-object v2, p0, LX/IY6;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/instantarticles/view/ShareBar;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2587365
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2587366
    :cond_1
    iget-object v0, p0, LX/IY6;->a:Ljava/lang/String;

    const-string v1, "pinterest"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587367
    iget-object v0, p0, LX/IY6;->c:Lcom/facebook/instantarticles/view/ShareBar;

    const-string v1, "share_to_pinterest"

    iget-object v2, p0, LX/IY6;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/instantarticles/view/ShareBar;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
