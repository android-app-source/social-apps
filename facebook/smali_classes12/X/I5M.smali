.class public LX/I5M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2iz;

.field public final b:Lcom/facebook/reaction/ReactionUtil;


# direct methods
.method public constructor <init>(LX/2iz;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2535407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2535408
    iput-object p1, p0, LX/I5M;->a:LX/2iz;

    .line 2535409
    iput-object p2, p0, LX/I5M;->b:Lcom/facebook/reaction/ReactionUtil;

    .line 2535410
    return-void
.end method

.method public static a(LX/0QB;)LX/I5M;
    .locals 1

    .prologue
    .line 2535411
    invoke-static {p0}, LX/I5M;->b(LX/0QB;)LX/I5M;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/I5M;
    .locals 3

    .prologue
    .line 2535412
    new-instance v2, LX/I5M;

    invoke-static {p0}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/ReactionUtil;

    invoke-direct {v2, v0, v1}, LX/I5M;-><init>(LX/2iz;Lcom/facebook/reaction/ReactionUtil;)V

    .line 2535413
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/2jY;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2535414
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2535415
    iget-object v1, p0, LX/I5M;->a:LX/2iz;

    invoke-virtual {v1, v0, p1}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v1

    .line 2535416
    iget-object v2, p0, LX/I5M;->a:LX/2iz;

    invoke-virtual {v2, v0}, LX/2iz;->c(Ljava/lang/String;)V

    .line 2535417
    return-object v1
.end method

.method public final a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;",
            ">;>;",
            "LX/0m9;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2535418
    iget-object v0, p0, LX/I5M;->b:Lcom/facebook/reaction/ReactionUtil;

    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object v1, p2

    move-object v4, p1

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0Ve;ILjava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;)V

    .line 2535419
    return-void
.end method
