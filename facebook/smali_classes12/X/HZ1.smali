.class public LX/HZ1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HYm;


# instance fields
.field private a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/registration/fragment/RegistrationFragment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/registration/fragment/RegistrationFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2481364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2481365
    const v0, 0x7f0400af

    iput v0, p0, LX/HZ1;->d:I

    .line 2481366
    const v0, 0x7f0400b0

    iput v0, p0, LX/HZ1;->e:I

    .line 2481367
    const v0, 0x7f0400af

    iput v0, p0, LX/HZ1;->f:I

    .line 2481368
    const v0, 0x7f0400b0

    iput v0, p0, LX/HZ1;->g:I

    .line 2481369
    iput-boolean v1, p0, LX/HZ1;->b:Z

    .line 2481370
    iput-boolean v1, p0, LX/HZ1;->c:Z

    .line 2481371
    iput-object p1, p0, LX/HZ1;->a:Ljava/lang/Class;

    .line 2481372
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2481356
    new-instance v0, LX/42p;

    iget-object v1, p0, LX/HZ1;->a:Ljava/lang/Class;

    invoke-direct {v0, v1}, LX/42p;-><init>(Ljava/lang/Class;)V

    .line 2481357
    iget-boolean v1, p0, LX/HZ1;->b:Z

    if-eqz v1, :cond_0

    .line 2481358
    invoke-virtual {v0}, LX/42p;->a()LX/42p;

    .line 2481359
    :cond_0
    iget-boolean v1, p0, LX/HZ1;->c:Z

    if-eqz v1, :cond_1

    .line 2481360
    invoke-virtual {v0}, LX/42p;->b()LX/42p;

    .line 2481361
    :cond_1
    iget v1, p0, LX/HZ1;->d:I

    iget v2, p0, LX/HZ1;->e:I

    iget v3, p0, LX/HZ1;->f:I

    iget v4, p0, LX/HZ1;->g:I

    invoke-virtual {v0, v1, v2, v3, v4}, LX/42p;->a(IIII)LX/42p;

    .line 2481362
    iget-object v1, v0, LX/42p;->a:Landroid/content/Intent;

    move-object v0, v1

    .line 2481363
    return-object v0
.end method

.method public final b()LX/HZ1;
    .locals 1

    .prologue
    .line 2481352
    const/4 v0, 0x1

    .line 2481353
    iput-boolean v0, p0, LX/HZ1;->b:Z

    .line 2481354
    move-object v0, p0

    .line 2481355
    return-object v0
.end method

.method public final c()LX/HZ1;
    .locals 1

    .prologue
    .line 2481348
    const/4 v0, 0x1

    .line 2481349
    iput-boolean v0, p0, LX/HZ1;->c:Z

    .line 2481350
    move-object v0, p0

    .line 2481351
    return-object v0
.end method
