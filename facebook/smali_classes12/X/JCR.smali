.class public final LX/JCR;
.super LX/J7F;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2662205
    iput-object p1, p0, LX/JCR;->b:Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;

    invoke-direct {p0, p2}, LX/J7F;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2662206
    check-cast p1, LX/J7E;

    .line 2662207
    iget-object v0, p1, LX/J7E;->c:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    move-object v0, v0

    .line 2662208
    if-nez v0, :cond_0

    .line 2662209
    :goto_0
    return-void

    .line 2662210
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2662211
    const-string v2, "profile_name"

    iget-object v0, p0, LX/JCR;->b:Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->B:LX/JCP;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/JCR;->b:Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->B:LX/JCP;

    .line 2662212
    iget-object v3, v0, LX/JCP;->w:Ljava/lang/String;

    move-object v0, v3

    .line 2662213
    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662214
    const-string v0, "request_field_cache_id"

    .line 2662215
    iget-object v2, p1, LX/J7E;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2662216
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662217
    const-string v0, "fragment_id"

    iget-object v2, p0, LX/JCR;->b:Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;

    iget-object v2, v2, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->A:Landroid/os/ParcelUuid;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2662218
    sget-object v0, LX/0ax;->bP:Ljava/lang/String;

    iget-object v2, p0, LX/JCR;->b:Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;

    iget-object v2, v2, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2662219
    iget-object v3, v2, LX/9lP;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2662220
    iget-object v3, p1, LX/J7E;->c:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    move-object v3, v3

    .line 2662221
    invoke-static {v0, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2662222
    iget-object v0, p0, LX/JCR;->b:Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v3, p0, LX/JCR;->b:Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_0

    .line 2662223
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
