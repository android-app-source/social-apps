.class public final LX/IJm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IJn;


# direct methods
.method public constructor <init>(LX/IJn;)V
    .locals 0

    .prologue
    .line 2564201
    iput-object p1, p0, LX/IJm;->a:LX/IJn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564202
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v5, 0x6c4e2f62

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2564203
    if-eqz p1, :cond_0

    .line 2564204
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564205
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2564206
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2564207
    :goto_2
    return-object v0

    .line 2564208
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564209
    check-cast v0, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2564210
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2564211
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564212
    check-cast v0, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2564213
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2564214
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2564215
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564216
    check-cast v0, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2564217
    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2564218
    :goto_4
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :cond_6
    :goto_5
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2564219
    invoke-virtual {v5, v0, v2}, LX/15i;->g(II)I

    move-result v0

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2564220
    if-eqz v0, :cond_6

    .line 2564221
    new-instance v6, Landroid/location/Address;

    iget-object v7, p0, LX/IJm;->a:LX/IJn;

    iget-object v7, v7, LX/IJn;->d:Ljava/util/Locale;

    invoke-direct {v6, v7}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 2564222
    invoke-virtual {v5, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    .line 2564223
    invoke-virtual {v5, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    .line 2564224
    const/4 v7, 0x2

    :try_start_1
    invoke-virtual {v5, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/location/Address;->setLatitude(D)V

    .line 2564225
    const/4 v7, 0x3

    invoke-virtual {v5, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/location/Address;->setLongitude(D)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2564226
    :goto_6
    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 2564227
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    .line 2564228
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2564229
    :catch_0
    move-exception v0

    .line 2564230
    iget-object v5, p0, LX/IJm;->a:LX/IJn;

    iget-object v5, v5, LX/IJn;->c:LX/03V;

    sget-object v7, LX/IJn;->a:Ljava/lang/String;

    const-string v8, "Failed to parseDouble from result"

    invoke-virtual {v5, v7, v8, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 2564231
    :cond_8
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_2
.end method
