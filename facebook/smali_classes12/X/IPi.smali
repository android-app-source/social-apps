.class public final LX/IPi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IPj;


# direct methods
.method public constructor <init>(LX/IPj;)V
    .locals 0

    .prologue
    .line 2574459
    iput-object p1, p0, LX/IPi;->a:LX/IPj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2574460
    new-instance v0, LX/9Qw;

    invoke-direct {v0}, LX/9Qw;-><init>()V

    move-object v0, v0

    .line 2574461
    iget-object v1, p0, LX/IPi;->a:LX/IPj;

    invoke-static {v1}, LX/IPj;->f(LX/IPj;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2574462
    const-string v1, "has_unseen"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2574463
    :cond_0
    const-string v1, "before"

    .line 2574464
    iget-object v2, p1, LX/3DR;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2574465
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2574466
    const-string v1, "after"

    .line 2574467
    iget-object v2, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2574468
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2574469
    const-string v1, "first"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2574470
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2574471
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2574472
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;

    move-result-object v0

    .line 2574473
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    new-instance v3, LX/5Mb;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-direct {v3, v0, v2, v1}, LX/5Mb;-><init>(LX/0Px;LX/15i;I)V

    return-object v3
.end method
