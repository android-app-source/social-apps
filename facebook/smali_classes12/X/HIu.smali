.class public LX/HIu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E1f;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1vg;


# direct methods
.method public constructor <init>(LX/0Ot;LX/1vg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E1f;",
            ">;",
            "LX/1vg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2451914
    iput-object p1, p0, LX/HIu;->a:LX/0Ot;

    .line 2451915
    iput-object p2, p0, LX/HIu;->b:LX/1vg;

    .line 2451916
    return-void
.end method

.method public static a(LX/0QB;)LX/HIu;
    .locals 5

    .prologue
    .line 2451917
    const-class v1, LX/HIu;

    monitor-enter v1

    .line 2451918
    :try_start_0
    sget-object v0, LX/HIu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451919
    sput-object v2, LX/HIu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451920
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451921
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2451922
    new-instance v4, LX/HIu;

    const/16 v3, 0x3098

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-direct {v4, p0, v3}, LX/HIu;-><init>(LX/0Ot;LX/1vg;)V

    .line 2451923
    move-object v0, v4

    .line 2451924
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451925
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HIu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451926
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
