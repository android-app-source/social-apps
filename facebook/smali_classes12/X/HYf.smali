.class public final enum LX/HYf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HYf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HYf;

.field public static final enum AUTH_COMPLETE:LX/HYf;

.field public static final enum AUTH_FAILED:LX/HYf;

.field public static final enum LOGGING_IN:LX/HYf;

.field public static final enum LOGIN_COMPLETE:LX/HYf;

.field public static final enum TIMEOUT:LX/HYf;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2480853
    new-instance v0, LX/HYf;

    const-string v1, "LOGGING_IN"

    invoke-direct {v0, v1, v2}, LX/HYf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYf;->LOGGING_IN:LX/HYf;

    .line 2480854
    new-instance v0, LX/HYf;

    const-string v1, "AUTH_COMPLETE"

    invoke-direct {v0, v1, v3}, LX/HYf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYf;->AUTH_COMPLETE:LX/HYf;

    .line 2480855
    new-instance v0, LX/HYf;

    const-string v1, "LOGIN_COMPLETE"

    invoke-direct {v0, v1, v4}, LX/HYf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYf;->LOGIN_COMPLETE:LX/HYf;

    .line 2480856
    new-instance v0, LX/HYf;

    const-string v1, "AUTH_FAILED"

    invoke-direct {v0, v1, v5}, LX/HYf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYf;->AUTH_FAILED:LX/HYf;

    .line 2480857
    new-instance v0, LX/HYf;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v6}, LX/HYf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYf;->TIMEOUT:LX/HYf;

    .line 2480858
    const/4 v0, 0x5

    new-array v0, v0, [LX/HYf;

    sget-object v1, LX/HYf;->LOGGING_IN:LX/HYf;

    aput-object v1, v0, v2

    sget-object v1, LX/HYf;->AUTH_COMPLETE:LX/HYf;

    aput-object v1, v0, v3

    sget-object v1, LX/HYf;->LOGIN_COMPLETE:LX/HYf;

    aput-object v1, v0, v4

    sget-object v1, LX/HYf;->AUTH_FAILED:LX/HYf;

    aput-object v1, v0, v5

    sget-object v1, LX/HYf;->TIMEOUT:LX/HYf;

    aput-object v1, v0, v6

    sput-object v0, LX/HYf;->$VALUES:[LX/HYf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2480859
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HYf;
    .locals 1

    .prologue
    .line 2480860
    const-class v0, LX/HYf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HYf;

    return-object v0
.end method

.method public static values()[LX/HYf;
    .locals 1

    .prologue
    .line 2480861
    sget-object v0, LX/HYf;->$VALUES:[LX/HYf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HYf;

    return-object v0
.end method
