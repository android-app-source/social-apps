.class public final LX/IYr;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 2588186
    const-class v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    const-string v5, "LookNowStoryQuery"

    const-string v6, "08362602736bef221df39bdc9411330c"

    const-string v7, "look_now_story"

    const-string v8, "10155258467671729"

    const/4 v9, 0x0

    .line 2588187
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2588188
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2588189
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2588190
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2588191
    sparse-switch v0, :sswitch_data_0

    .line 2588192
    :goto_0
    return-object p1

    .line 2588193
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2588194
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2588195
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2588196
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2588197
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2588198
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2588199
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2588200
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2588201
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2588202
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2588203
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2588204
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2588205
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2588206
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2588207
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2588208
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2588209
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2588210
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2588211
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 2588212
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 2588213
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 2588214
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 2588215
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 2588216
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 2588217
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 2588218
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 2588219
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 2588220
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 2588221
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 2588222
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 2588223
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 2588224
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    .line 2588225
    :sswitch_20
    const-string p1, "32"

    goto :goto_0

    .line 2588226
    :sswitch_21
    const-string p1, "33"

    goto :goto_0

    .line 2588227
    :sswitch_22
    const-string p1, "34"

    goto :goto_0

    .line 2588228
    :sswitch_23
    const-string p1, "35"

    goto :goto_0

    .line 2588229
    :sswitch_24
    const-string p1, "36"

    goto :goto_0

    .line 2588230
    :sswitch_25
    const-string p1, "37"

    goto :goto_0

    .line 2588231
    :sswitch_26
    const-string p1, "38"

    goto :goto_0

    .line 2588232
    :sswitch_27
    const-string p1, "39"

    goto :goto_0

    .line 2588233
    :sswitch_28
    const-string p1, "40"

    goto :goto_0

    .line 2588234
    :sswitch_29
    const-string p1, "41"

    goto :goto_0

    .line 2588235
    :sswitch_2a
    const-string p1, "42"

    goto/16 :goto_0

    .line 2588236
    :sswitch_2b
    const-string p1, "43"

    goto/16 :goto_0

    .line 2588237
    :sswitch_2c
    const-string p1, "44"

    goto/16 :goto_0

    .line 2588238
    :sswitch_2d
    const-string p1, "45"

    goto/16 :goto_0

    .line 2588239
    :sswitch_2e
    const-string p1, "46"

    goto/16 :goto_0

    .line 2588240
    :sswitch_2f
    const-string p1, "47"

    goto/16 :goto_0

    .line 2588241
    :sswitch_30
    const-string p1, "48"

    goto/16 :goto_0

    .line 2588242
    :sswitch_31
    const-string p1, "49"

    goto/16 :goto_0

    .line 2588243
    :sswitch_32
    const-string p1, "50"

    goto/16 :goto_0

    .line 2588244
    :sswitch_33
    const-string p1, "51"

    goto/16 :goto_0

    .line 2588245
    :sswitch_34
    const-string p1, "52"

    goto/16 :goto_0

    .line 2588246
    :sswitch_35
    const-string p1, "53"

    goto/16 :goto_0

    .line 2588247
    :sswitch_36
    const-string p1, "54"

    goto/16 :goto_0

    .line 2588248
    :sswitch_37
    const-string p1, "55"

    goto/16 :goto_0

    .line 2588249
    :sswitch_38
    const-string p1, "56"

    goto/16 :goto_0

    .line 2588250
    :sswitch_39
    const-string p1, "57"

    goto/16 :goto_0

    .line 2588251
    :sswitch_3a
    const-string p1, "58"

    goto/16 :goto_0

    .line 2588252
    :sswitch_3b
    const-string p1, "59"

    goto/16 :goto_0

    .line 2588253
    :sswitch_3c
    const-string p1, "60"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7f566515 -> :sswitch_25
        -0x7b752021 -> :sswitch_3
        -0x7531a756 -> :sswitch_1a
        -0x6e3ba572 -> :sswitch_11
        -0x6a24640d -> :sswitch_36
        -0x6a02a4f4 -> :sswitch_28
        -0x69f19a9a -> :sswitch_1
        -0x680de62a -> :sswitch_20
        -0x6326fdb3 -> :sswitch_1d
        -0x5e743804 -> :sswitch_c
        -0x5709d77d -> :sswitch_3a
        -0x55ff6f9b -> :sswitch_2
        -0x5349037c -> :sswitch_26
        -0x51484e72 -> :sswitch_e
        -0x513764de -> :sswitch_37
        -0x50cab1c8 -> :sswitch_8
        -0x4eea3afb -> :sswitch_b
        -0x4ae70342 -> :sswitch_9
        -0x4496acc9 -> :sswitch_21
        -0x41a91745 -> :sswitch_2e
        -0x3c54de38 -> :sswitch_24
        -0x3b85b241 -> :sswitch_39
        -0x39e54905 -> :sswitch_2d
        -0x30b65c8f -> :sswitch_18
        -0x2f1c601a -> :sswitch_1b
        -0x25a646c8 -> :sswitch_17
        -0x24e1906f -> :sswitch_4
        -0x2177e47b -> :sswitch_19
        -0x201d08e7 -> :sswitch_2b
        -0x1b87b280 -> :sswitch_1c
        -0x17e48248 -> :sswitch_5
        -0x14283bca -> :sswitch_2a
        -0x12efdeb3 -> :sswitch_22
        -0x8ca6426 -> :sswitch_7
        -0x587d3fa -> :sswitch_1e
        0x180aba4 -> :sswitch_15
        0xa1fa812 -> :sswitch_10
        0xc168ff8 -> :sswitch_a
        0x11850e88 -> :sswitch_31
        0x18ce3dbb -> :sswitch_f
        0x214100e0 -> :sswitch_23
        0x2292beef -> :sswitch_35
        0x244e76e6 -> :sswitch_34
        0x26d0c0ff -> :sswitch_30
        0x27208b4a -> :sswitch_32
        0x291d8de0 -> :sswitch_2f
        0x2f8b060e -> :sswitch_3c
        0x3052e0ff -> :sswitch_13
        0x34e16755 -> :sswitch_0
        0x410878b1 -> :sswitch_2c
        0x420eb51c -> :sswitch_12
        0x43ee5105 -> :sswitch_3b
        0x54ace343 -> :sswitch_33
        0x54df6484 -> :sswitch_d
        0x5e7957c4 -> :sswitch_16
        0x5f424068 -> :sswitch_14
        0x63c03b07 -> :sswitch_1f
        0x6771e9f5 -> :sswitch_27
        0x73a026b5 -> :sswitch_29
        0x7506f93c -> :sswitch_38
        0x7c6b80b3 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 2588254
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2588255
    :goto_1
    return v0

    .line 2588256
    :sswitch_0
    const-string v5, "3"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v5, "19"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_2
    const-string v5, "14"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v3

    goto :goto_0

    :sswitch_3
    const-string v5, "22"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v5, "24"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v4

    goto :goto_0

    :sswitch_5
    const-string v5, "15"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v5, "10"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v5, "1"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v5, "2"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_9
    const-string v5, "37"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v5, "0"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_b
    const-string v5, "39"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xb

    goto :goto_0

    :sswitch_c
    const-string v5, "13"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v5, "11"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v5, "5"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v5, "12"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v5, "4"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v5, "55"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v5, "9"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v5, "8"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v5, "7"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v5, "6"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v5, "57"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x16

    goto/16 :goto_0

    .line 2588257
    :pswitch_0
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588258
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588259
    :pswitch_2
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588260
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588261
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588262
    :pswitch_5
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588263
    :pswitch_6
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588264
    :pswitch_7
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588265
    :pswitch_8
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588266
    :pswitch_9
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588267
    :pswitch_a
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588268
    :pswitch_b
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588269
    :pswitch_c
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588270
    :pswitch_d
    const-string v0, "FUSE_BIG"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588271
    :pswitch_e
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588272
    :pswitch_f
    const-string v0, "%s"

    invoke-static {p2, v4, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588273
    :pswitch_10
    const-string v0, "%s"

    invoke-static {p2, v3, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588274
    :pswitch_11
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588275
    :pswitch_12
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588276
    :pswitch_13
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588277
    :pswitch_14
    const/16 v0, 0x78

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588278
    :pswitch_15
    const/16 v0, 0x5a

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2588279
    :pswitch_16
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_a
        0x31 -> :sswitch_7
        0x32 -> :sswitch_8
        0x33 -> :sswitch_0
        0x34 -> :sswitch_10
        0x35 -> :sswitch_e
        0x36 -> :sswitch_15
        0x37 -> :sswitch_14
        0x38 -> :sswitch_13
        0x39 -> :sswitch_12
        0x61f -> :sswitch_6
        0x620 -> :sswitch_d
        0x621 -> :sswitch_f
        0x622 -> :sswitch_c
        0x623 -> :sswitch_2
        0x624 -> :sswitch_5
        0x628 -> :sswitch_1
        0x640 -> :sswitch_3
        0x642 -> :sswitch_4
        0x664 -> :sswitch_9
        0x666 -> :sswitch_b
        0x6a0 -> :sswitch_11
        0x6a2 -> :sswitch_16
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method
