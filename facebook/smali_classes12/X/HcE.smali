.class public final LX/HcE;
.super LX/HcD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/HcD",
        "<",
        "Lcom/facebook/share/model/ShareOpenGraphAction;",
        "LX/HcE;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486859
    invoke-direct {p0}, LX/HcD;-><init>()V

    return-void
.end method

.method public static a(LX/HcE;Lcom/facebook/share/model/ShareOpenGraphAction;)LX/HcE;
    .locals 2

    .prologue
    .line 2486860
    if-nez p1, :cond_0

    .line 2486861
    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, LX/HcD;->a(Lcom/facebook/share/model/ShareOpenGraphValueContainer;)LX/HcD;

    move-result-object v0

    check-cast v0, LX/HcE;

    .line 2486862
    const-string v1, "og:type"

    .line 2486863
    iget-object p0, p1, Lcom/facebook/share/model/ShareOpenGraphValueContainer;->a:Landroid/os/Bundle;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    move-object v1, p0

    .line 2486864
    move-object v1, v1

    .line 2486865
    const-string p0, "og:type"

    .line 2486866
    iget-object p1, v0, LX/HcD;->a:Landroid/os/Bundle;

    invoke-virtual {p1, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2486867
    move-object p0, v0

    .line 2486868
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/share/model/ShareOpenGraphValueContainer;)LX/HcD;
    .locals 1

    .prologue
    .line 2486869
    check-cast p1, Lcom/facebook/share/model/ShareOpenGraphAction;

    invoke-static {p0, p1}, LX/HcE;->a(LX/HcE;Lcom/facebook/share/model/ShareOpenGraphAction;)LX/HcE;

    move-result-object v0

    return-object v0
.end method
