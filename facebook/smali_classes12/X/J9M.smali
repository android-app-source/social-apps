.class public LX/J9M;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2652755
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2652756
    return-void
.end method

.method public static a(LX/JCA;LX/Amh;)LX/1qM;
    .locals 3
    .param p1    # LX/Amh;
        .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/ForTimelineCollections;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/inject/ContextScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/timeline/aboutpage/service/TimelineCollectionsQueue;
    .end annotation

    .prologue
    .line 2652754
    new-instance v0, LX/2m1;

    new-instance v1, LX/2m1;

    new-instance v2, LX/4BM;

    invoke-direct {v2}, LX/4BM;-><init>()V

    invoke-direct {v1, p1, v2}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v0, p0, v1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method

.method public static a(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;)LX/Amh;
    .locals 6
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/ForTimelineCollections;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Or",
            "<",
            "LX/826;",
            ">;)",
            "LX/Amh;"
        }
    .end annotation

    .prologue
    .line 2652753
    new-instance v0, LX/Amh;

    const-string v5, "update_timeline_app_collection_in_timeline"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/Amh;-><init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a()Ljava/lang/Boolean;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/IsNativeCollectionsSubscribeEnabled;
    .end annotation

    .prologue
    .line 2652752
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/lang/Boolean;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/IsTheWhoEnabled;
    .end annotation

    .prologue
    .line 2652750
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2652751
    return-void
.end method
