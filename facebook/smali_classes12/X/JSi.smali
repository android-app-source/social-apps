.class public LX/JSi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2695842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/JSe;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2695843
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2695844
    iget-object v1, p0, LX/JSe;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2695845
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2695846
    iget-object v1, p0, LX/JSe;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2695847
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2695848
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2695849
    const-string v1, " . "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2695850
    :cond_0
    iget-object v1, p0, LX/JSe;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2695851
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2695852
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/JSe;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2695853
    const-string v0, ""

    .line 2695854
    iget-object v1, p1, LX/JSe;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2695855
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2695856
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083a86

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2695857
    iget-object v4, p1, LX/JSe;->e:Ljava/lang/String;

    move-object v4, v4

    .line 2695858
    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2695859
    :cond_0
    return-object v0
.end method
