.class public LX/JJ4;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2678883
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2678884
    iput-object p1, p0, LX/JJ4;->b:Landroid/view/View$OnClickListener;

    .line 2678885
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2678886
    return-void
.end method

.method private e(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2678899
    iget-object v0, p0, LX/JJ4;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    if-nez v0, :cond_0

    .line 2678900
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getCount() should have returned 0, hence getItem() should not have been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2678901
    :cond_0
    iget-object v0, p0, LX/JJ4;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2678896
    iget-object v0, p0, LX/JJ4;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    if-nez v0, :cond_0

    .line 2678897
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getCount() should have returned 0, hence getItemId() should not have been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2678898
    :cond_0
    iget-object v0, p0, LX/JJ4;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2678902
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2678903
    const v1, 0x7f0303c2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 2678904
    new-instance v1, LX/JJ3;

    invoke-direct {v1, v0}, LX/JJ3;-><init>(Lcom/facebook/widget/CustomLinearLayout;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2678890
    check-cast p1, LX/JJ3;

    .line 2678891
    iget-object v0, p1, LX/JJ3;->l:Lcom/facebook/widget/CustomLinearLayout;

    .line 2678892
    iget-object v1, p1, LX/JJ3;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, p2}, LX/JJ4;->e(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2678893
    iget-object v1, p0, LX/JJ4;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2678894
    const v1, 0x7f0d0224

    new-instance v2, LX/JJD;

    invoke-direct {p0, p2}, LX/JJ4;->e(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p2}, LX/1OM;->C_(I)J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, LX/JJD;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->setTag(ILjava/lang/Object;)V

    .line 2678895
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2678887
    iget-object v0, p0, LX/JJ4;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    if-nez v0, :cond_0

    .line 2678888
    const/4 v0, 0x0

    .line 2678889
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/JJ4;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
