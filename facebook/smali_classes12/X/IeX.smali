.class public LX/IeX;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/3Lv;


# direct methods
.method public constructor <init>(LX/0Zr;LX/3LP;LX/3Lv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598785
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 2598786
    iput-object p3, p0, LX/IeX;->c:LX/3Lv;

    .line 2598787
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 9
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2598752
    const-string v0, "ContactPickerSmsContactFilter.Filtering"

    const v1, -0x227d43

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2598753
    :try_start_0
    new-instance v7, LX/39y;

    invoke-direct {v7}, LX/39y;-><init>()V

    .line 2598754
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2598755
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    .line 2598756
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2598757
    invoke-static {v1}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v4

    .line 2598758
    iget-object v0, p0, LX/IeX;->c:LX/3Lv;

    const/16 v2, 0x1e

    const/4 v3, 0x1

    invoke-static {v1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v5

    sget-object v6, LX/3Oo;->SEARCH:LX/3Oo;

    invoke-virtual/range {v0 .. v6}, LX/3Lv;->a(Ljava/lang/String;IZZZLX/3Oo;)LX/0Px;

    move-result-object v0

    .line 2598759
    invoke-interface {v8, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2598760
    new-instance v0, LX/DAp;

    invoke-direct {v0, v8}, LX/DAp;-><init>(Ljava/util/Collection;)V

    invoke-static {v8, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2598761
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2598762
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2598763
    iget-object v8, v1, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    move-object v8, v8

    .line 2598764
    if-nez v8, :cond_4

    .line 2598765
    :cond_0
    :goto_2
    goto :goto_1

    .line 2598766
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2598767
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    .line 2598768
    iput-object v0, v7, LX/39y;->a:Ljava/lang/Object;

    .line 2598769
    iget v1, v0, LX/3Og;->d:I

    move v0, v1

    .line 2598770
    iput v0, v7, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2598771
    :goto_3
    const v0, 0x20a26fd4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2598772
    const-string v0, "orca:ContactPickerSmsContactFilter"

    invoke-static {v0}, LX/0PR;->c(Ljava/lang/String;)V

    return-object v7

    .line 2598773
    :cond_2
    :try_start_1
    const-string v1, ""

    goto :goto_0

    .line 2598774
    :cond_3
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v7, LX/39y;->a:Ljava/lang/Object;

    .line 2598775
    const/4 v0, -0x1

    iput v0, v7, LX/39y;->b:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 2598776
    :catch_0
    move-exception v0

    .line 2598777
    :try_start_2
    const-string v1, "orca:ContactPickerSmsContactFilter"

    const-string v2, "exception while filtering"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2598778
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2598779
    :catchall_0
    move-exception v0

    const v1, -0x487f2438

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2598780
    const-string v1, "orca:ContactPickerSmsContactFilter"

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    throw v0

    .line 2598781
    :cond_4
    invoke-virtual {p0, v8}, LX/3Ml;->a(Lcom/facebook/user/model/UserIdentifier;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2598782
    iget-object v8, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v8, v1}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v8

    .line 2598783
    if-eqz v8, :cond_0

    .line 2598784
    invoke-virtual {v0, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2
.end method
