.class public LX/HKK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/HKB;


# direct methods
.method public constructor <init>(LX/HKB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2454288
    iput-object p1, p0, LX/HKK;->a:LX/HKB;

    .line 2454289
    return-void
.end method

.method public static a(LX/HKK;LX/1De;LX/HKJ;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/HKJ;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 2454257
    iget-object v0, p0, LX/HKK;->a:LX/HKB;

    const/4 v1, 0x0

    .line 2454258
    new-instance v2, LX/HKA;

    invoke-direct {v2, v0}, LX/HKA;-><init>(LX/HKB;)V

    .line 2454259
    iget-object p0, v0, LX/HKB;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HK9;

    .line 2454260
    if-nez p0, :cond_0

    .line 2454261
    new-instance p0, LX/HK9;

    invoke-direct {p0, v0}, LX/HK9;-><init>(LX/HKB;)V

    .line 2454262
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HK9;->a$redex0(LX/HK9;LX/1De;IILX/HKA;)V

    .line 2454263
    move-object v2, p0

    .line 2454264
    move-object v1, v2

    .line 2454265
    move-object v0, v1

    .line 2454266
    iget-object v1, v0, LX/HK9;->a:LX/HKA;

    iput-object p3, v1, LX/HKA;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2454267
    iget-object v1, v0, LX/HK9;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454268
    move-object v0, v0

    .line 2454269
    iget-object v1, v0, LX/HK9;->a:LX/HKA;

    iput-object p2, v1, LX/HKA;->a:LX/HKJ;

    .line 2454270
    iget-object v1, v0, LX/HK9;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454271
    move-object v0, v0

    .line 2454272
    iget-object v1, v0, LX/HK9;->a:LX/HKA;

    iput-object p4, v1, LX/HKA;->c:LX/2km;

    .line 2454273
    iget-object v1, v0, LX/HK9;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454274
    move-object v0, v0

    .line 2454275
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/HKK;
    .locals 4

    .prologue
    .line 2454276
    const-class v1, LX/HKK;

    monitor-enter v1

    .line 2454277
    :try_start_0
    sget-object v0, LX/HKK;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454278
    sput-object v2, LX/HKK;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454279
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454280
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454281
    new-instance p0, LX/HKK;

    invoke-static {v0}, LX/HKB;->a(LX/0QB;)LX/HKB;

    move-result-object v3

    check-cast v3, LX/HKB;

    invoke-direct {p0, v3}, LX/HKK;-><init>(LX/HKB;)V

    .line 2454282
    move-object v0, p0

    .line 2454283
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454284
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454285
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454286
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(ILandroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2454251
    if-nez p0, :cond_0

    .line 2454252
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0811d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2454253
    :goto_0
    return-object v0

    .line 2454254
    :cond_0
    if-lez p0, :cond_1

    .line 2454255
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0085

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2454256
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid album size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ILX/2km;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "ITE;)",
            "Ljava/util/List",
            "<",
            "LX/HKJ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2454228
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2454229
    invoke-interface {v0}, LX/9uc;->bL()LX/0Px;

    move-result-object v9

    .line 2454230
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move v8, v6

    .line 2454231
    :goto_0
    if-ge v8, p1, :cond_6

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v0

    if-ge v8, v0, :cond_6

    .line 2454232
    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$PageModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2454233
    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;

    move-result-object v11

    .line 2454234
    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 2454235
    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v7

    .line 2454236
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 2454237
    :goto_2
    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$PhotoItemsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v7

    .line 2454238
    :goto_3
    if-eqz v0, :cond_3

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$PhotoItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$PhotoItemsModel;->a()I

    move-result v0

    move v2, v0

    :goto_4
    move-object v0, p2

    .line 2454239
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v2, v0}, LX/HKK;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 2454240
    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v7

    .line 2454241
    :goto_5
    if-eqz v0, :cond_5

    invoke-virtual {v11}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitOpenAlbumActionsFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2454242
    :goto_6
    new-instance v0, LX/HKJ;

    invoke-direct/range {v0 .. v5}, LX/HKJ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2454243
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_0

    :cond_0
    move v0, v6

    .line 2454244
    goto :goto_1

    .line 2454245
    :cond_1
    const-string v3, ""

    goto :goto_2

    :cond_2
    move v0, v6

    .line 2454246
    goto :goto_3

    :cond_3
    move v2, v6

    .line 2454247
    goto :goto_4

    :cond_4
    move v0, v6

    .line 2454248
    goto :goto_5

    .line 2454249
    :cond_5
    const/4 v2, 0x0

    goto :goto_6

    .line 2454250
    :cond_6
    return-object v10
.end method
