.class public final LX/JHn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;",
        "LX/3OS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3PA;


# direct methods
.method public constructor <init>(LX/3PA;)V
    .locals 0

    .prologue
    .line 2677101
    iput-object p1, p0, LX/JHn;->a:LX/3PA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2677102
    check-cast p1, LX/0Px;

    .line 2677103
    if-nez p1, :cond_0

    .line 2677104
    const/4 v0, 0x0

    .line 2677105
    :goto_0
    return-object v0

    .line 2677106
    :cond_0
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2677107
    sget-object v1, LX/7Hz;->a:LX/7Hz;

    move-object v1, v1

    .line 2677108
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2677109
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/3OS;->a(LX/0Px;)LX/3OS;

    move-result-object v0

    goto :goto_0
.end method
