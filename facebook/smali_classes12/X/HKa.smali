.class public final LX/HKa;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HKb;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public b:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/HKb;


# direct methods
.method public constructor <init>(LX/HKb;)V
    .locals 1

    .prologue
    .line 2454640
    iput-object p1, p0, LX/HKa;->c:LX/HKb;

    .line 2454641
    move-object v0, p1

    .line 2454642
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2454643
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2454658
    const-string v0, "PageSocialContextImageBlockUnitComponentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2454644
    if-ne p0, p1, :cond_1

    .line 2454645
    :cond_0
    :goto_0
    return v0

    .line 2454646
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2454647
    goto :goto_0

    .line 2454648
    :cond_3
    check-cast p1, LX/HKa;

    .line 2454649
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2454650
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2454651
    if-eq v2, v3, :cond_0

    .line 2454652
    iget-object v2, p0, LX/HKa;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HKa;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HKa;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2454653
    goto :goto_0

    .line 2454654
    :cond_5
    iget-object v2, p1, LX/HKa;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_4

    .line 2454655
    :cond_6
    iget-object v2, p0, LX/HKa;->b:LX/2km;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/HKa;->b:LX/2km;

    iget-object v3, p1, LX/HKa;->b:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2454656
    goto :goto_0

    .line 2454657
    :cond_7
    iget-object v2, p1, LX/HKa;->b:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
