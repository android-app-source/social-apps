.class public final LX/IR6;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V
    .locals 0

    .prologue
    .line 2576125
    iput-object p1, p0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 2576126
    iget-object v0, p0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Af;

    .line 2576127
    new-instance v2, LX/7TY;

    invoke-virtual {v0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2576128
    iget-object v1, p0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iget-boolean v1, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->y:Z

    if-eqz v1, :cond_0

    .line 2576129
    iget-object v1, p0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081bba

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 2576130
    const v3, 0x7f020952

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2576131
    new-instance v3, LX/IR3;

    invoke-direct {v3, p0}, LX/IR3;-><init>(LX/IR6;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2576132
    iget-object v1, p0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081bbb

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 2576133
    const v3, 0x7f020a0b

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2576134
    new-instance v3, LX/IR4;

    invoke-direct {v3, p0}, LX/IR4;-><init>(LX/IR6;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2576135
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->v:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2576136
    iget-object v1, p0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081bb9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 2576137
    :goto_0
    const v3, 0x7f0208b2

    move v3, v3

    .line 2576138
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2576139
    new-instance v3, LX/IR5;

    invoke-direct {v3, p0}, LX/IR5;-><init>(LX/IR6;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2576140
    invoke-virtual {v0, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2576141
    invoke-virtual {v0}, LX/3Af;->show()V

    .line 2576142
    return-void

    .line 2576143
    :cond_1
    iget-object v1, p0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081bb8

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    goto :goto_0
.end method
