.class public final LX/I7T;
.super LX/BlO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538946
    iput-object p1, p0, LX/I7T;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, LX/BlO;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 2538947
    check-cast p1, LX/BlN;

    .line 2538948
    iget-object v0, p0, LX/I7T;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->I(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2538949
    :cond_0
    :goto_0
    return-void

    .line 2538950
    :cond_1
    iget-object v0, p1, LX/BlN;->a:LX/BlI;

    sget-object v1, LX/BlI;->SENDING:LX/BlI;

    if-ne v0, v1, :cond_2

    .line 2538951
    iget-object v0, p0, LX/I7T;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    .line 2538952
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/I8k;->e(LX/I8k;Z)V

    .line 2538953
    goto :goto_0

    .line 2538954
    :cond_2
    iget-object v0, p1, LX/BlN;->a:LX/BlI;

    sget-object v1, LX/BlI;->SUCCESS:LX/BlI;

    if-ne v0, v1, :cond_3

    .line 2538955
    iget-object v0, p0, LX/I7T;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->p()V

    goto :goto_0

    .line 2538956
    :cond_3
    iget-object v0, p1, LX/BlN;->a:LX/BlI;

    sget-object v1, LX/BlI;->FAILURE:LX/BlI;

    if-ne v0, v1, :cond_0

    .line 2538957
    iget-object v0, p0, LX/I7T;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v0}, LX/I8k;->m()V

    .line 2538958
    iget-object v0, p0, LX/I7T;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    sget-object v1, LX/DBa;->COMMENT_POST_FAILED:LX/DBa;

    invoke-static {v0, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a$redex0(Lcom/facebook/events/permalink/EventPermalinkFragment;LX/DBa;)V

    goto :goto_0
.end method
