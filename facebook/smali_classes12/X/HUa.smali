.class public final LX/HUa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V
    .locals 0

    .prologue
    .line 2473285
    iput-object p1, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2473287
    iget-object v0, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    move v1, v0

    .line 2473288
    :goto_0
    iget-object v0, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->K:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->L:I

    if-eq v0, v1, :cond_1

    .line 2473289
    :cond_0
    iget-object v0, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->D:Landroid/view/View;

    iget-object v3, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    sget-object v4, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->J:[I

    iget-object v5, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v5, v5, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->e:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->d()I

    move-result v5

    invoke-static {v0, v3, p2, v4, v5}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2473290
    iget-object v4, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2473291
    iput-boolean v0, v4, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->K:Z

    .line 2473292
    iget-object v0, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->K:Z

    if-eqz v0, :cond_1

    .line 2473293
    iget-object v4, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->E_(I)V

    .line 2473294
    iget-object v0, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473295
    iput v1, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->L:I

    .line 2473296
    :cond_1
    iget-object v0, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2473297
    iget-object v1, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->E:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->G:I

    if-eq v1, v0, :cond_2

    iget-object v1, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2473298
    iget-object v1, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->E:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v1, v2, p2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2473299
    iget-object v1, p0, LX/HUa;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473300
    iput v0, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->G:I

    .line 2473301
    :cond_2
    return-void

    :cond_3
    move v1, v2

    .line 2473302
    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2473286
    return-void
.end method
