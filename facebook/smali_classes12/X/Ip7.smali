.class public LX/Ip7;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/AL6;
.implements LX/IoU;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/J1j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AL7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/widget/ScrollView;

.field public e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field public f:Landroid/widget/ProgressBar;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/resources/ui/FbButton;

.field public i:Landroid/widget/LinearLayout;

.field public j:Landroid/widget/LinearLayout;

.field public k:Lcom/facebook/resources/ui/FbTextView;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

.field public n:Landroid/support/v4/view/ViewPager;

.field public o:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

.field public p:LX/Io1;

.field public q:LX/J1i;

.field public r:LX/Ios;

.field public s:LX/5g0;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2612481
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2612482
    const-class v0, LX/Ip7;

    invoke-static {v0, p0}, LX/Ip7;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2612483
    const v0, 0x7f030de4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2612484
    iget-object v0, p0, LX/Ip7;->c:LX/AL7;

    .line 2612485
    iput-object p0, v0, LX/AL7;->g:LX/AL6;

    .line 2612486
    const v0, 0x7f0d220c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, LX/Ip7;->d:Landroid/widget/ScrollView;

    .line 2612487
    const v0, 0x7f0d220d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, LX/Ip7;->n:Landroid/support/v4/view/ViewPager;

    .line 2612488
    const v0, 0x7f0d2202

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, LX/Ip7;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2612489
    const v0, 0x7f0d2204

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Ip7;->f:Landroid/widget/ProgressBar;

    .line 2612490
    const v0, 0x7f0d2209

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ip7;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2612491
    const v0, 0x7f0d220a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Ip7;->h:Lcom/facebook/resources/ui/FbButton;

    .line 2612492
    const v0, 0x7f0d2208

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Ip7;->i:Landroid/widget/LinearLayout;

    .line 2612493
    const v0, 0x7f0d2205

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Ip7;->j:Landroid/widget/LinearLayout;

    .line 2612494
    const v0, 0x7f0d2206

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ip7;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2612495
    const v0, 0x7f0d2207

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ip7;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2612496
    const v0, 0x7f0d220e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iput-object v0, p0, LX/Ip7;->m:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    .line 2612497
    const v0, 0x7f0d220f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    iput-object v0, p0, LX/Ip7;->o:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    .line 2612498
    iget-object v0, p0, LX/Ip7;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b()V

    .line 2612499
    iget-object v0, p0, LX/Ip7;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setLongClickable(Z)V

    .line 2612500
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ip7;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const-class v2, LX/J1j;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/J1j;

    invoke-static {p0}, LX/AL7;->a(LX/0QB;)LX/AL7;

    move-result-object p0

    check-cast p0, LX/AL7;

    iput-object v1, p1, LX/Ip7;->a:Landroid/content/res/Resources;

    iput-object v2, p1, LX/Ip7;->b:LX/J1j;

    iput-object p0, p1, LX/Ip7;->c:LX/AL7;

    return-void
.end method

.method public static setPaymentCardInfoVisibility(LX/Ip7;I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2612471
    iget-object v0, p0, LX/Ip7;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612472
    if-nez p1, :cond_0

    .line 2612473
    iget-object v0, p0, LX/Ip7;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612474
    iget-object v0, p0, LX/Ip7;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612475
    :goto_0
    return-void

    .line 2612476
    :cond_0
    iget-object v0, p0, LX/Ip7;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612477
    iget-object v0, p0, LX/Ip7;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612478
    iget-object v0, p0, LX/Ip7;->k:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082c85

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2612479
    iget-object v0, p0, LX/Ip7;->k:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f020e62

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2612480
    iget-object v0, p0, LX/Ip7;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final I_(I)V
    .locals 2

    .prologue
    .line 2612468
    iget-object v0, p0, LX/Ip7;->d:Landroid/widget/ScrollView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2612469
    iget-object v0, p0, LX/Ip7;->d:Landroid/widget/ScrollView;

    new-instance v1, Lcom/facebook/messaging/payment/value/input/OrionMessengerRedesignPayView$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/payment/value/input/OrionMessengerRedesignPayView$1;-><init>(LX/Ip7;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 2612470
    :cond_0
    return-void
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2612466
    iget-object v0, p0, LX/Ip7;->q:LX/J1i;

    invoke-virtual {v0}, LX/J1i;->a()V

    .line 2612467
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 0
    .param p1    # Landroid/view/MenuItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2612465
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2612501
    iget-object v0, p0, LX/Ip7;->o:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    invoke-virtual {v0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a()V

    .line 2612502
    return-void
.end method

.method public getImmediateFocusView()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2612464
    iget-object v0, p0, LX/Ip7;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x61796d3c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2612461
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onAttachedToWindow()V

    .line 2612462
    iget-object v2, p0, LX/Ip7;->c:LX/AL7;

    invoke-virtual {p0}, LX/Ip7;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v2, v0}, LX/AL7;->a(Landroid/app/Activity;)V

    .line 2612463
    const/16 v0, 0x2d

    const v2, -0x671050e1

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6bbc2f2f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2612458
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 2612459
    iget-object v1, p0, LX/Ip7;->c:LX/AL7;

    invoke-virtual {v1}, LX/AL7;->a()V

    .line 2612460
    const/16 v1, 0x2d

    const v2, -0x7ddd96f4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setListener(LX/Io1;)V
    .locals 2

    .prologue
    .line 2612447
    iput-object p1, p0, LX/Ip7;->p:LX/Io1;

    .line 2612448
    iget-object v0, p0, LX/Ip7;->m:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    new-instance v1, LX/Ioy;

    invoke-direct {v1, p0}, LX/Ioy;-><init>(LX/Ip7;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->setListener(LX/Iox;)V

    .line 2612449
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/Ip7;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance p1, LX/Ioz;

    invoke-direct {p1, p0}, LX/Ioz;-><init>(LX/Ip7;)V

    invoke-direct {v0, v1, p1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 2612450
    iget-object v1, p0, LX/Ip7;->n:Landroid/support/v4/view/ViewPager;

    new-instance p1, LX/Ip0;

    invoke-direct {p1, p0, v0}, LX/Ip0;-><init>(LX/Ip7;Landroid/view/GestureDetector;)V

    invoke-virtual {v1, p1}, Landroid/support/v4/view/ViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2612451
    iget-object v0, p0, LX/Ip7;->n:Landroid/support/v4/view/ViewPager;

    new-instance v1, LX/Ip1;

    invoke-direct {v1, p0}, LX/Ip1;-><init>(LX/Ip7;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2612452
    iget-object v0, p0, LX/Ip7;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    new-instance v1, LX/Ip2;

    invoke-direct {v1, p0}, LX/Ip2;-><init>(LX/Ip7;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2612453
    iget-object v0, p0, LX/Ip7;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Ip3;

    invoke-direct {v1, p0}, LX/Ip3;-><init>(LX/Ip7;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612454
    iget-object v0, p0, LX/Ip7;->l:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/Ip4;

    invoke-direct {v1, p0}, LX/Ip4;-><init>(LX/Ip7;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612455
    iget-object v0, p0, LX/Ip7;->o:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    new-instance v1, LX/Ip6;

    invoke-direct {v1, p0}, LX/Ip6;-><init>(LX/Ip7;)V

    .line 2612456
    iput-object v1, v0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->e:LX/Ip5;

    .line 2612457
    return-void
.end method

.method public setMessengerPayViewParams(LX/Ios;)V
    .locals 4

    .prologue
    .line 2612417
    check-cast p1, LX/Ios;

    iput-object p1, p0, LX/Ip7;->r:LX/Ios;

    .line 2612418
    iget-object v0, p0, LX/Ip7;->q:LX/J1i;

    if-eqz v0, :cond_1

    .line 2612419
    :goto_0
    iget-object v0, p0, LX/Ip7;->q:LX/J1i;

    iget-object v1, p0, LX/Ip7;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612420
    iget-object v2, v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2612421
    invoke-virtual {v0, v1}, LX/J1i;->a(Ljava/lang/String;)V

    .line 2612422
    sget-object v0, LX/Iow;->b:[I

    iget-object v1, p0, LX/Ip7;->r:LX/Ios;

    .line 2612423
    iget-object v2, v1, LX/Ios;->a:LX/IoT;

    move-object v1, v2

    .line 2612424
    invoke-virtual {v1}, LX/IoT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2612425
    iget-object v0, p0, LX/Ip7;->q:LX/J1i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/J1i;->a(Z)V

    .line 2612426
    :goto_1
    iget-object v0, p0, LX/Ip7;->m:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v1, p0, LX/Ip7;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->setMemoText(Ljava/lang/String;)V

    .line 2612427
    iget-object v0, p0, LX/Ip7;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->h:Ljava/util/List;

    if-nez v0, :cond_2

    .line 2612428
    :goto_2
    iget-object v0, p0, LX/Ip7;->s:LX/5g0;

    sget-object v1, LX/5g0;->GROUP_COMMERCE_REQUEST:LX/5g0;

    if-eq v0, v1, :cond_0

    .line 2612429
    const/4 v3, 0x0

    const/16 v1, 0x8

    .line 2612430
    iget-object v0, p0, LX/Ip7;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    if-nez v0, :cond_4

    .line 2612431
    iget-object v0, p0, LX/Ip7;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612432
    iget-object v0, p0, LX/Ip7;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612433
    iget-object v0, p0, LX/Ip7;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612434
    :cond_0
    :goto_3
    return-void

    .line 2612435
    :cond_1
    iget-object v0, p0, LX/Ip7;->b:LX/J1j;

    new-instance v1, LX/Iov;

    invoke-direct {v1, p0}, LX/Iov;-><init>(LX/Ip7;)V

    const/4 v2, 0x1

    iget-object v3, p0, LX/Ip7;->r:LX/Ios;

    iget-object v3, v3, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612436
    iget-object p1, v3, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v3, p1

    .line 2612437
    invoke-virtual {v0, v1, v2, v3}, LX/J1j;->a(LX/Ioo;ZLjava/lang/String;)LX/J1i;

    move-result-object v0

    iput-object v0, p0, LX/Ip7;->q:LX/J1i;

    .line 2612438
    iget-object v0, p0, LX/Ip7;->q:LX/J1i;

    iget-object v1, p0, LX/Ip7;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0, v1}, LX/J1i;->a(Lcom/facebook/payments/p2p/ui/DollarIconEditText;)V

    goto :goto_0

    .line 2612439
    :pswitch_0
    iget-object v0, p0, LX/Ip7;->q:LX/J1i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/J1i;->a(Z)V

    goto :goto_1

    .line 2612440
    :cond_2
    iget-object v0, p0, LX/Ip7;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_3

    .line 2612441
    iget-object v0, p0, LX/Ip7;->n:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;

    iget-object v2, p0, LX/Ip7;->r:LX/Ios;

    iget-object v2, v2, LX/Ios;->h:Ljava/util/List;

    invoke-virtual {p0}, LX/Ip7;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;-><init>(Ljava/util/List;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    goto :goto_2

    .line 2612442
    :cond_3
    iget-object v0, p0, LX/Ip7;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, LX/Ip7;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->h:Ljava/util/List;

    iget-object v2, p0, LX/Ip7;->r:LX/Ios;

    iget-object v2, v2, LX/Ios;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-static {v1, v2}, LX/Ipi;->a(Ljava/util/List;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_2

    .line 2612443
    :cond_4
    iget-object v0, p0, LX/Ip7;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2612444
    invoke-static {p0, v1}, LX/Ip7;->setPaymentCardInfoVisibility(LX/Ip7;I)V

    goto :goto_3

    .line 2612445
    :cond_5
    iget-object v1, p0, LX/Ip7;->g:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, LX/Ip7;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    iget-object v2, p0, LX/Ip7;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/p2p/model/PaymentCard;->c(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2612446
    invoke-static {p0, v3}, LX/Ip7;->setPaymentCardInfoVisibility(LX/Ip7;I)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setPaymentFlowType(LX/5g0;)V
    .locals 2

    .prologue
    .line 2612411
    iput-object p1, p0, LX/Ip7;->s:LX/5g0;

    .line 2612412
    sget-object v0, LX/Iow;->a:[I

    iget-object v1, p0, LX/Ip7;->s:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2612413
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid paymentFlowType for OrionMessengerRedesignPayView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2612414
    :pswitch_0
    iget-object v0, p0, LX/Ip7;->o:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    sget-object v1, LX/Ipg;->BOTH:LX/Ipg;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setButtonMode(LX/Ipg;)V

    .line 2612415
    :goto_0
    return-void

    .line 2612416
    :pswitch_1
    iget-object v0, p0, LX/Ip7;->o:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    sget-object v1, LX/Ipg;->REQUEST_ONLY:LX/Ipg;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setButtonMode(LX/Ipg;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
