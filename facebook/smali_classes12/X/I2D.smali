.class public final LX/I2D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I2F;


# direct methods
.method public constructor <init>(LX/I2F;)V
    .locals 0

    .prologue
    .line 2529999
    iput-object p1, p0, LX/I2D;->a:LX/I2F;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x71629c1d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2530000
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/I2D;->a:LX/I2F;

    iget-object v0, v0, LX/I2F;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2530001
    const-string v2, "target_fragment"

    sget-object v3, LX/0cQ;->EVENTS_DASHBOARD_HOSTING_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2530002
    const-string v2, "extra_events_hosting_dashboard_section_type"

    iget-object v3, p0, LX/I2D;->a:LX/I2F;

    iget-object v3, v3, LX/I2F;->l:LX/I2H;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2530003
    const-string v2, "extra_ref_module"

    iget-object v3, p0, LX/I2D;->a:LX/I2F;

    iget-object v3, v3, LX/I2F;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2530004
    iget-object v2, p0, LX/I2D;->a:LX/I2F;

    iget-object v2, v2, LX/I2F;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/I2D;->a:LX/I2F;

    iget-object v3, v3, LX/I2F;->c:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2530005
    const v0, 0x33863888

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
