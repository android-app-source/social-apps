.class public LX/HoL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2505624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2505625
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HoL;->a:Ljava/util/List;

    .line 2505626
    const/4 v0, 0x0

    iput v0, p0, LX/HoL;->b:I

    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2505627
    iget-object v0, p0, LX/HoL;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/HoL;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 2505628
    iget-object v0, p0, LX/HoL;->a:Ljava/util/List;

    iget v1, p0, LX/HoL;->b:I

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2505629
    iget v0, p0, LX/HoL;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/HoL;->b:I

    .line 2505630
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2505631
    iget-object v0, p0, LX/HoL;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
