.class public LX/JSR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JS8;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Xu;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Landroid/net/Uri;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlayer$Callback;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JTe;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/JTe;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/JTe;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695343
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/JSR;->a:LX/0Xu;

    .line 2695344
    iput-object p1, p0, LX/JSR;->b:LX/0Or;

    .line 2695345
    return-void
.end method

.method public static a(LX/0QB;)LX/JSR;
    .locals 4

    .prologue
    .line 2695331
    const-class v1, LX/JSR;

    monitor-enter v1

    .line 2695332
    :try_start_0
    sget-object v0, LX/JSR;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695333
    sput-object v2, LX/JSR;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695334
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695335
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695336
    new-instance v3, LX/JSR;

    const/16 p0, 0x204c

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JSR;-><init>(LX/0Or;)V

    .line 2695337
    move-object v0, v3

    .line 2695338
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695339
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JSR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695340
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695341
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/JSe;)I
    .locals 1

    .prologue
    .line 2695277
    invoke-virtual {p0, p1}, LX/JSR;->c(LX/JSe;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JSR;->c:LX/JTe;

    if-nez v0, :cond_1

    .line 2695278
    :cond_0
    const/4 v0, 0x0

    .line 2695279
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/JSR;->c:LX/JTe;

    invoke-virtual {v0}, LX/JTe;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 2695330
    return-void
.end method

.method public final a(Landroid/net/Uri;II)V
    .locals 3

    .prologue
    .line 2695324
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPlayerError"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2695325
    const-string v1, " (what="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2695326
    const-string v1, " (extra="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2695327
    iget-object v0, p0, LX/JSR;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSI;

    .line 2695328
    invoke-virtual {v0}, LX/JSI;->b()V

    goto :goto_0

    .line 2695329
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;LX/JTd;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2695304
    iget-object v0, p0, LX/JSR;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v3

    .line 2695305
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2695306
    sget-object v0, LX/JTd;->PLAYING:LX/JTd;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/JTd;->BUFFERING:LX/JTd;

    if-ne p2, v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/JSR;->c:LX/JTe;

    if-eqz v0, :cond_1

    .line 2695307
    iget-object v0, p0, LX/JSR;->c:LX/JTe;

    invoke-virtual {v0}, LX/JTe;->c()V

    .line 2695308
    :cond_1
    return-void

    .line 2695309
    :cond_2
    iget-object v0, p0, LX/JSR;->c:LX/JTe;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/JSR;->c:LX/JTe;

    invoke-virtual {v0}, LX/JTe;->a()I

    move-result v0

    move v1, v0

    .line 2695310
    :goto_0
    iget-object v0, p0, LX/JSR;->c:LX/JTe;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/JSR;->c:LX/JTe;

    invoke-virtual {v0}, LX/JTe;->b()I

    move-result v2

    .line 2695311
    :cond_3
    sget-object v0, LX/JSQ;->a:[I

    invoke-virtual {p2}, LX/JTd;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 2695312
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSI;

    .line 2695313
    invoke-virtual {v0, v2, v1}, LX/JSI;->b(II)V

    goto :goto_1

    :cond_4
    move v1, v2

    .line 2695314
    goto :goto_0

    .line 2695315
    :pswitch_0
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSI;

    .line 2695316
    invoke-virtual {v0, v2, v1}, LX/JSI;->a(II)V

    goto :goto_2

    .line 2695317
    :pswitch_1
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSI;

    .line 2695318
    sget-object v2, LX/JSK;->LOADING:LX/JSK;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/JSI;->a(LX/JSI;LX/JSK;I)Z

    .line 2695319
    goto :goto_3

    .line 2695320
    :pswitch_2
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSI;

    .line 2695321
    invoke-static {v0, v2, v1}, LX/JSI;->d(LX/JSI;II)V

    .line 2695322
    sget-object v4, LX/JSK;->PAUSED:LX/JSK;

    invoke-static {v0, v4, v1}, LX/JSI;->a(LX/JSI;LX/JSK;I)Z

    .line 2695323
    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2695301
    iget-object v0, p0, LX/JSR;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSI;

    .line 2695302
    invoke-virtual {v0}, LX/JSI;->b()V

    goto :goto_0

    .line 2695303
    :cond_0
    return-void
.end method

.method public final b(LX/JSe;LX/JSI;)V
    .locals 2

    .prologue
    .line 2695296
    if-eqz p1, :cond_0

    .line 2695297
    iget-object v0, p0, LX/JSR;->a:LX/0Xu;

    .line 2695298
    iget-object v1, p1, LX/JSe;->h:Landroid/net/Uri;

    move-object v1, v1

    .line 2695299
    invoke-interface {v0, v1, p2}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2695300
    :cond_0
    return-void
.end method

.method public final c(LX/JSe;LX/JSI;)V
    .locals 4

    .prologue
    .line 2695288
    iget-object v0, p1, LX/JSe;->h:Landroid/net/Uri;

    move-object v0, v0

    .line 2695289
    iget-object v1, p0, LX/JSR;->c:LX/JTe;

    if-eqz v1, :cond_0

    .line 2695290
    iget-object v1, p0, LX/JSR;->c:LX/JTe;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, LX/JTe;->a(Landroid/net/Uri;LX/JS8;Z)V

    .line 2695291
    iget-object v1, p0, LX/JSR;->a:LX/0Xu;

    invoke-interface {v1, v0}, LX/0Xu;->f(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2695292
    iget-object v1, p0, LX/JSR;->a:LX/0Xu;

    invoke-interface {v1, v0}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSI;

    .line 2695293
    iget-object v2, p0, LX/JSR;->c:LX/JTe;

    invoke-virtual {v2}, LX/JTe;->b()I

    move-result v2

    iget-object v3, p0, LX/JSR;->c:LX/JTe;

    invoke-virtual {v3}, LX/JTe;->a()I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/JSI;->b(II)V

    goto :goto_0

    .line 2695294
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/JSR;->b(LX/JSe;LX/JSI;)V

    .line 2695295
    return-void
.end method

.method public final c(LX/JSe;)Z
    .locals 3

    .prologue
    .line 2695280
    iget-object v0, p1, LX/JSe;->h:Landroid/net/Uri;

    move-object v0, v0

    .line 2695281
    iget-object v1, p0, LX/JSR;->c:LX/JTe;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JSR;->c:LX/JTe;

    const/4 p1, 0x0

    .line 2695282
    iget-boolean v2, v1, LX/JTe;->i:Z

    if-nez v2, :cond_3

    move v2, p1

    .line 2695283
    :goto_0
    move v1, v2

    .line 2695284
    if-nez v1, :cond_1

    iget-object v1, p0, LX/JSR;->c:LX/JTe;

    const/4 v2, 0x0

    .line 2695285
    iget-boolean p0, v1, LX/JTe;->i:Z

    if-nez p0, :cond_5

    .line 2695286
    :cond_0
    :goto_1
    move v0, v2

    .line 2695287
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    iget-object v2, v1, LX/JTe;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v1, LX/JTe;->e:Landroid/net/Uri;

    if-eqz v2, :cond_4

    iget-object v2, v1, LX/JTe;->e:Landroid/net/Uri;

    invoke-virtual {v2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    goto :goto_0

    :cond_4
    move v2, p1

    goto :goto_0

    :cond_5
    iget-object p0, v1, LX/JTe;->j:LX/JTd;

    sget-object p1, LX/JTd;->BUFFERING:LX/JTd;

    if-ne p0, p1, :cond_0

    iget-object p0, v1, LX/JTe;->e:Landroid/net/Uri;

    if-eqz p0, :cond_0

    iget-object p0, v1, LX/JTe;->e:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v2, 0x1

    goto :goto_1
.end method
