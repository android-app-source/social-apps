.class public LX/HkK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/HkK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/HkK;

    invoke-direct {v0}, LX/HkK;-><init>()V

    sput-object v0, LX/HkK;->a:LX/HkK;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()LX/HkK;
    .locals 2

    const-class v0, LX/HkK;

    monitor-enter v0

    :try_start_0
    sget-object v1, LX/HkK;->a:LX/HkK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static c(Lorg/json/JSONObject;)LX/HkO;
    .locals 4

    new-instance v0, LX/HkO;

    const-string v1, "message"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "code"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/HkO;-><init>(Ljava/lang/String;ILX/Hjx;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/HkM;
    .locals 7

    invoke-static {p1}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "type"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    const-string v0, "error"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/HkK;->c(Lorg/json/JSONObject;)LX/HkO;

    move-result-object v0

    :goto_1
    return-object v0

    :sswitch_0
    const-string v3, "ads"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x0

    const-string v2, "placements"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "definition"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {v3}, LX/Hjy;->a(Lorg/json/JSONObject;)LX/Hjy;

    move-result-object v3

    new-instance v4, LX/Hjx;

    invoke-direct {v4, v3}, LX/Hjx;-><init>(LX/Hjy;)V

    iget-object v5, v3, LX/Hjy;->a:LX/HkF;

    move-object v3, v5

    const-string v5, "ads"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "ads"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    new-instance v5, LX/Hju;

    invoke-direct {v5, v3}, LX/Hju;-><init>(LX/HkF;)V

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string p0, "adapter"

    invoke-virtual {v6, p0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v5, LX/Hju;->b:Ljava/lang/String;

    const-string p0, "data"

    invoke-virtual {v6, p0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    const-string p1, "trackers"

    invoke-virtual {v6, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    if-eqz v6, :cond_1

    const-string p1, "trackers"

    invoke-virtual {p0, p1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    iput-object p0, v5, LX/Hju;->c:Lorg/json/JSONObject;

    iget-object v6, v4, LX/Hjx;->a:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    new-instance v0, LX/HkN;

    invoke-direct {v0, v4}, LX/HkN;-><init>(LX/Hjx;)V

    move-object v0, v0

    goto :goto_1

    :pswitch_1
    :try_start_0
    const-string v0, "placements"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "definition"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, LX/Hjy;->a(Lorg/json/JSONObject;)LX/Hjy;

    move-result-object v2

    new-instance v0, LX/HkO;

    const-string v3, "message"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "code"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    new-instance v5, LX/Hjx;

    invoke-direct {v5, v2}, LX/Hjx;-><init>(LX/Hjy;)V

    invoke-direct {v0, v3, v4, v5}, LX/HkO;-><init>(Ljava/lang/String;ILX/Hjx;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    move-object v0, v0

    goto/16 :goto_1

    :cond_3
    new-instance v0, LX/HkM;

    sget-object v1, LX/HkL;->a:LX/HkL;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/HkM;-><init>(LX/HkL;LX/Hjx;)V

    goto/16 :goto_1

    :catch_0
    invoke-static {v1}, LX/HkK;->c(Lorg/json/JSONObject;)LX/HkO;

    move-result-object v0

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x178b0 -> :sswitch_0
        0x5c4d208 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
