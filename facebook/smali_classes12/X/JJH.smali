.class public LX/JJH;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:J

.field public final e:Landroid/accounts/Account;

.field private final f:Landroid/content/ContentProviderClient;

.field private final g:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2679228
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    sput-object v0, LX/JJH;->a:[Ljava/lang/String;

    .line 2679229
    const-string v0, "true"

    sput-object v0, LX/JJH;->b:Ljava/lang/String;

    .line 2679230
    const-class v0, LX/JJH;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/JJH;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JLandroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2679231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2679232
    iput-wide p1, p0, LX/JJH;->d:J

    .line 2679233
    iput-object p3, p0, LX/JJH;->e:Landroid/accounts/Account;

    .line 2679234
    iput-object p4, p0, LX/JJH;->f:Landroid/content/ContentProviderClient;

    .line 2679235
    iput-object p5, p0, LX/JJH;->g:Landroid/content/Context;

    .line 2679236
    return-void
.end method

.method public static a(LX/JJH;LX/JJI;Landroid/content/ContentProviderOperation$Builder;)Landroid/content/ContentProviderOperation;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2679180
    invoke-virtual {p1}, LX/JJI;->b()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/JJI;->d()Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2679181
    :cond_0
    const/4 v0, 0x0

    .line 2679182
    :goto_0
    return-object v0

    .line 2679183
    :cond_1
    invoke-virtual {p1}, LX/JJI;->b()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 2679184
    invoke-virtual {p1}, LX/JJI;->d()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 2679185
    const-string v0, "calendar_id"

    iget-wide v8, p0, LX/JJH;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p2, v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679186
    const-string v0, "dtstart"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p2, v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679187
    const-string v0, "dtend"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p2, v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679188
    const-string v3, "allDay"

    .line 2679189
    iget-boolean v0, p1, LX/JJI;->i:Z

    move v0, v0

    .line 2679190
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679191
    const-string v0, "title"

    .line 2679192
    iget-object v3, p1, LX/JJI;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2679193
    invoke-virtual {p2, v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679194
    const-string v0, "eventLocation"

    .line 2679195
    iget-object v3, p1, LX/JJI;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2679196
    invoke-virtual {p2, v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679197
    const-string v0, "%s%n%n%s%n%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    .line 2679198
    iget-object v4, p1, LX/JJI;->f:Ljava/lang/String;

    move-object v4, v4

    .line 2679199
    aput-object v4, v3, v2

    iget-object v2, p0, LX/JJH;->g:Landroid/content/Context;

    const v4, 0x7f083a61

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/JJH;->g:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x2

    .line 2679200
    iget-object v2, p1, LX/JJI;->h:Ljava/lang/String;

    move-object v2, v2

    .line 2679201
    aput-object v2, v3, v1

    invoke-static {v0, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2679202
    const-string v1, "description"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679203
    iget v0, p1, LX/JJI;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2679204
    if-eqz v0, :cond_2

    .line 2679205
    const-string v0, "eventStatus"

    .line 2679206
    iget v1, p1, LX/JJI;->g:I

    move v1, v1

    .line 2679207
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679208
    :cond_2
    const-string v0, "_sync_id"

    .line 2679209
    iget-object v1, p1, LX/JJI;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2679210
    invoke-virtual {p2, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679211
    invoke-virtual {p2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 2679212
    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/JJG;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2679213
    :try_start_0
    iget-object v0, p0, LX/JJH;->f:Landroid/content/ContentProviderClient;

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    iget-object v3, p0, LX/JJH;->e:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    iget-object v3, p0, LX/JJH;->e:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/JJH;->a:[Ljava/lang/String;

    const-string v3, "calendar_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v8, p0, LX/JJH;->d:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2679214
    if-nez v1, :cond_2

    .line 2679215
    if-eqz v1, :cond_0

    .line 2679216
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v6

    :cond_1
    :goto_0
    return-object v0

    .line 2679217
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2679218
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2679219
    new-instance v2, LX/JJG;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-direct {v2, v4, v5, v8, v9}, LX/JJG;-><init>(JJ)V

    .line 2679220
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 2679221
    :catch_0
    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_3

    .line 2679222
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v6

    goto :goto_0

    .line 2679223
    :cond_4
    if-eqz v1, :cond_1

    .line 2679224
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2679225
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_5

    .line 2679226
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 2679227
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    :catch_1
    move-object v0, v6

    goto :goto_2
.end method

.method public final a(J)V
    .locals 7

    .prologue
    .line 2679171
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    sget-object v2, LX/JJH;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, LX/JJH;->e:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    iget-object v2, p0, LX/JJH;->e:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2679172
    :try_start_0
    iget-object v0, p0, LX/JJH;->f:Landroid/content/ContentProviderClient;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2679173
    :goto_0
    return-void

    .line 2679174
    :catch_0
    move-exception v0

    .line 2679175
    sget-object v2, LX/JJH;->c:Ljava/lang/String;

    const-string v3, "Error while trying to delete the event with URI=%s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2679176
    :try_start_0
    iget-object v1, p0, LX/JJH;->f:Landroid/content/ContentProviderClient;

    invoke-virtual {v1, p1}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2679177
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2679178
    :catch_0
    goto :goto_0

    .line 2679179
    :catch_1
    goto :goto_0
.end method
