.class public final LX/INm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/INs;


# direct methods
.method public constructor <init>(LX/INs;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2571119
    iput-object p1, p0, LX/INm;->c:LX/INs;

    iput-object p2, p0, LX/INm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iput-object p3, p0, LX/INm;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x3a606137

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2571120
    iget-object v1, p0, LX/INm;->c:LX/INs;

    const-string v2, "group_email_verification_resend"

    iget-object v3, p0, LX/INm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v3}, LX/INs;->d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/INs;->a$redex0(LX/INs;Ljava/lang/String;Ljava/lang/String;)V

    .line 2571121
    iget-object v1, p0, LX/INm;->c:LX/INs;

    iget-object v1, v1, LX/INs;->n:Lcom/facebook/fig/button/FigButton;

    const v2, 0x7f083019

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2571122
    iget-object v1, p0, LX/INm;->c:LX/INs;

    iget-object v1, v1, LX/INs;->n:Lcom/facebook/fig/button/FigButton;

    const v2, 0x7f0207da

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2571123
    iget-object v1, p0, LX/INm;->c:LX/INs;

    iget-object v1, v1, LX/INs;->n:Lcom/facebook/fig/button/FigButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2571124
    iget-object v1, p0, LX/INm;->c:LX/INs;

    iget-object v1, v1, LX/INs;->c:LX/IMk;

    iget-object v2, p0, LX/INm;->b:Ljava/lang/String;

    iget-object v3, p0, LX/INm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v3}, LX/INs;->d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/INm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v4}, LX/INs;->g(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Ljava/util/ArrayList;

    move-result-object v4

    new-instance v5, LX/INl;

    invoke-direct {v5, p0}, LX/INl;-><init>(LX/INm;)V

    invoke-virtual {v1, v2, v3, v4, v5}, LX/IMk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;LX/ILc;)V

    .line 2571125
    const v1, -0x77bf88e5

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
