.class public final LX/HP6;
.super LX/CgI;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V
    .locals 0

    .prologue
    .line 2461272
    iput-object p1, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-direct {p0}, LX/CgI;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2461273
    check-cast p1, LX/CgH;

    .line 2461274
    iget-object v0, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    const-class v1, LX/HQy;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HQy;

    .line 2461275
    iget-object v1, p1, LX/CgH;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2461276
    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    .line 2461277
    sget-object v1, LX/HQu;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v3, v1

    .line 2461278
    if-eqz v0, :cond_2

    invoke-interface {v0, v3}, LX/HQy;->c(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2461279
    iget-object v1, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v4, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->F:LX/CSN;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const/4 v5, 0x1

    invoke-virtual {v4, v1, v5}, LX/CSN;->a(Ljava/lang/String;Z)V

    .line 2461280
    invoke-interface {v0, v3}, LX/HQy;->d(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2461281
    iget-object v0, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->q:LX/9XE;

    iget-object v1, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-wide v4, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    .line 2461282
    invoke-static {v0, v4, v5, v3}, LX/9XE;->c(LX/9XE;JLcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2461283
    iget-object v1, v0, LX/9XE;->a:LX/0Zb;

    sget-object v6, LX/9XH;->EVENT_PRESENCE_TAB_SWITCH:LX/9XH;

    invoke-static {v6, v4, v5}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source_card_style"

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v1, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2461284
    :cond_0
    :goto_1
    return-void

    .line 2461285
    :cond_1
    const-string v1, "NULL"

    goto :goto_0

    .line 2461286
    :cond_2
    iget-object v0, p1, LX/CgH;->d:LX/Cfl;

    move-object v0, v0

    .line 2461287
    if-eqz v0, :cond_0

    .line 2461288
    iget-object v0, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v1, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->F:LX/CSN;

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/CSN;->a(Ljava/lang/String;Z)V

    .line 2461289
    iget-object v0, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tx;

    .line 2461290
    iget-object v1, p1, LX/CgH;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2461291
    iget-object v2, p1, LX/CgH;->d:LX/Cfl;

    move-object v2, v2

    .line 2461292
    iget-object v3, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v4, p0, LX/HP6;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/3Tx;->a(Ljava/lang/String;LX/Cfl;LX/0o8;Landroid/content/Context;)V

    goto :goto_1

    .line 2461293
    :cond_3
    const-string v0, "NULL"

    goto :goto_2
.end method
