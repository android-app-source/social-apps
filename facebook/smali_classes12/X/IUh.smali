.class public final LX/IUh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IUk;


# direct methods
.method public constructor <init>(LX/IUk;)V
    .locals 0

    .prologue
    .line 2580946
    iput-object p1, p0, LX/IUh;->a:LX/IUk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x5b6af4c9

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580947
    iget-object v1, p0, LX/IUh;->a:LX/IUk;

    invoke-static {v1}, LX/IUk;->g(LX/IUk;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2580948
    const v1, 0x27d5b011

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2580949
    :goto_0
    return-void

    .line 2580950
    :cond_0
    iget-object v1, p0, LX/IUh;->a:LX/IUk;

    iget-object v1, v1, LX/IUk;->d:LX/3mF;

    iget-object v2, p0, LX/IUh;->a:LX/IUk;

    iget-object v2, v2, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mobile_group_join"

    .line 2580951
    new-instance v4, LX/4GH;

    invoke-direct {v4}, LX/4GH;-><init>()V

    .line 2580952
    const-string v5, "invite_id"

    invoke-virtual {v4, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580953
    move-object v4, v4

    .line 2580954
    const-string v5, "source"

    invoke-virtual {v4, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580955
    move-object v4, v4

    .line 2580956
    new-instance v5, LX/B24;

    invoke-direct {v5}, LX/B24;-><init>()V

    move-object v5, v5

    .line 2580957
    const-string p1, "input"

    invoke-virtual {v5, p1, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2580958
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2580959
    iget-object v5, v1, LX/3mF;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 2580960
    iget-object v2, p0, LX/IUh;->a:LX/IUk;

    iget-object v2, v2, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2580961
    invoke-static {v2}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v2

    .line 2580962
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v3, v2, LX/9OQ;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2580963
    const/4 v3, 0x0

    iput-object v3, v2, LX/9OQ;->E:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    .line 2580964
    invoke-virtual {v2}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2580965
    iget-object v3, p0, LX/IUh;->a:LX/IUk;

    iget-object v4, p0, LX/IUh;->a:LX/IUk;

    iget-object v4, v4, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v3, v4, v2, v1}, LX/IUk;->a$redex0(LX/IUk;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2580966
    const v1, -0x76fa3fd0

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
