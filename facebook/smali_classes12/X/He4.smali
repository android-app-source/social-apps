.class public LX/He4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:LX/He2;

.field private b:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(LX/He2;Landroid/widget/ImageButton;)V
    .locals 0

    .prologue
    .line 2489630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2489631
    iput-object p1, p0, LX/He4;->a:LX/He2;

    .line 2489632
    iput-object p2, p0, LX/He4;->b:Landroid/widget/ImageButton;

    .line 2489633
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 2489608
    iget-object v0, p0, LX/He4;->a:LX/He2;

    .line 2489609
    iget-boolean v1, v0, LX/He1;->c:Z

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2489610
    if-nez p1, :cond_0

    .line 2489611
    invoke-static {v0}, LX/He1;->a(LX/He1;)V

    .line 2489612
    :goto_0
    return-void

    .line 2489613
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\""

    const-string p0, ""

    invoke-virtual {v1, v2, p0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2489614
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 2489615
    invoke-virtual {v0, v1}, LX/He1;->a(Ljava/lang/String;)V

    .line 2489616
    iget-object v1, v0, LX/He1;->e:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2489617
    goto :goto_0

    .line 2489618
    :cond_1
    invoke-static {v0}, LX/He1;->a(LX/He1;)V

    .line 2489619
    invoke-virtual {v0}, LX/He1;->d()V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 9

    .prologue
    .line 2489620
    if-nez p2, :cond_0

    const/4 v0, 0x1

    if-le p3, v0, :cond_0

    if-nez p4, :cond_0

    .line 2489621
    iget-object v0, p0, LX/He4;->a:LX/He2;

    invoke-virtual {v0}, LX/He2;->a()LX/G5R;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 2489622
    iput v3, v0, LX/G5R;->c:I

    .line 2489623
    iput v3, v0, LX/G5R;->d:I

    .line 2489624
    const-string v4, "clear"

    const/4 v7, -0x1

    move-object v3, v0

    move-object v5, v1

    move-object v8, v6

    invoke-static/range {v3 .. v8}, LX/G5R;->b(LX/G5R;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/util/List;)V

    .line 2489625
    :cond_0
    if-nez p2, :cond_1

    if-nez p4, :cond_1

    .line 2489626
    iget-object v0, p0, LX/He4;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/He4;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082a84

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2489627
    :cond_1
    if-nez p2, :cond_2

    if-nez p3, :cond_2

    if-lez p4, :cond_2

    .line 2489628
    iget-object v0, p0, LX/He4;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/He4;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082a85

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2489629
    :cond_2
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2489604
    iget-object v0, p0, LX/He4;->a:LX/He2;

    invoke-virtual {v0}, LX/He2;->a()LX/G5R;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 2489605
    iget p0, v0, LX/G5R;->c:I

    iget p1, v0, LX/G5R;->d:I

    sub-int/2addr p1, v1

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    add-int/2addr p0, p1

    iput p0, v0, LX/G5R;->c:I

    .line 2489606
    iput v1, v0, LX/G5R;->d:I

    .line 2489607
    return-void
.end method
