.class public final LX/IKI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2565755
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 2565756
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2565757
    :goto_0
    return v1

    .line 2565758
    :cond_0
    const-string v12, "is_viewer_subscribed"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2565759
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    .line 2565760
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_9

    .line 2565761
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2565762
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2565763
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2565764
    const-string v12, "customization_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2565765
    invoke-static {p0, p1}, LX/IKE;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2565766
    :cond_2
    const-string v12, "description"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2565767
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2565768
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2565769
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2565770
    :cond_4
    const-string v12, "image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2565771
    invoke-static {p0, p1}, LX/IKF;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2565772
    :cond_5
    const-string v12, "joinable_mode"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2565773
    invoke-static {p0, p1}, LX/IKJ;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2565774
    :cond_6
    const-string v12, "thread_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2565775
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2565776
    :cond_7
    const-string v12, "thread_queue_participants"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 2565777
    invoke-static {p0, p1}, LX/IKH;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2565778
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2565779
    :cond_9
    const/16 v11, 0x8

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2565780
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 2565781
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 2565782
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2565783
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2565784
    if-eqz v0, :cond_a

    .line 2565785
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 2565786
    :cond_a
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2565787
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2565788
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2565789
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2565790
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2565791
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2565792
    if-eqz v0, :cond_0

    .line 2565793
    const-string v1, "customization_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565794
    invoke-static {p0, v0, p2}, LX/IKE;->a(LX/15i;ILX/0nX;)V

    .line 2565795
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2565796
    if-eqz v0, :cond_1

    .line 2565797
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565798
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2565799
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2565800
    if-eqz v0, :cond_2

    .line 2565801
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565802
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2565803
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2565804
    if-eqz v0, :cond_3

    .line 2565805
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565806
    invoke-static {p0, v0, p2}, LX/IKF;->a(LX/15i;ILX/0nX;)V

    .line 2565807
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2565808
    if-eqz v0, :cond_4

    .line 2565809
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565810
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2565811
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2565812
    if-eqz v0, :cond_5

    .line 2565813
    const-string v1, "joinable_mode"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565814
    invoke-static {p0, v0, p2}, LX/IKJ;->a(LX/15i;ILX/0nX;)V

    .line 2565815
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2565816
    if-eqz v0, :cond_6

    .line 2565817
    const-string v1, "thread_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565818
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2565819
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2565820
    if-eqz v0, :cond_b

    .line 2565821
    const-string v1, "thread_queue_participants"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565822
    const/4 v1, 0x0

    .line 2565823
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2565824
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2565825
    if-eqz v1, :cond_7

    .line 2565826
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565827
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2565828
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2565829
    if-eqz v1, :cond_a

    .line 2565830
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565831
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2565832
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 2565833
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 2565834
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2565835
    const/4 p1, 0x0

    invoke-virtual {p0, v3, p1}, LX/15i;->g(II)I

    move-result p1

    .line 2565836
    if-eqz p1, :cond_8

    .line 2565837
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565838
    invoke-static {p0, p1, p2}, LX/IKG;->a(LX/15i;ILX/0nX;)V

    .line 2565839
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2565840
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2565841
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2565842
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2565843
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2565844
    return-void
.end method
