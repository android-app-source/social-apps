.class public final LX/Il7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public final synthetic b:LX/Il9;


# direct methods
.method public constructor <init>(LX/Il9;Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;)V
    .locals 0

    .prologue
    .line 2607488
    iput-object p1, p0, LX/Il7;->b:LX/Il9;

    iput-object p2, p0, LX/Il7;->a:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0xe945953

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2607489
    iget-object v1, p0, LX/Il7;->b:LX/Il9;

    iget-object v1, v1, LX/Il9;->e:LX/Iku;

    iget-object v2, p0, LX/Il7;->a:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2607490
    iget-object v5, v1, LX/Iku;->a:LX/Ikv;

    iget-object v5, v5, LX/Ikv;->e:LX/Ikx;

    invoke-virtual {v5}, LX/Ikx;->a()V

    .line 2607491
    iget-object v5, v1, LX/Iku;->a:LX/Ikv;

    iget-object v5, v5, LX/Ikv;->c:LX/IkW;

    iget-object v6, v1, LX/Iku;->a:LX/Ikv;

    iget-object v6, v6, LX/Ikv;->f:Ljava/lang/String;

    .line 2607492
    new-instance v9, LX/4Gd;

    invoke-direct {v9}, LX/4Gd;-><init>()V

    .line 2607493
    iget-object v8, v5, LX/IkW;->e:LX/3Ed;

    invoke-virtual {v8}, LX/3Ed;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 2607494
    const-string v10, "client_mutation_id"

    invoke-virtual {v9, v10, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2607495
    iget-object v8, v5, LX/IkW;->d:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2607496
    iget-object v10, v8, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v8, v10

    .line 2607497
    const-string v10, "actor_id"

    invoke-virtual {v9, v10, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2607498
    const-string v8, "invoice_id"

    invoke-virtual {v9, v8, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2607499
    const-string v8, "transaction_status"

    invoke-virtual {v9, v8, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2607500
    const-string v8, "receipt_reject_reason"

    invoke-virtual {v9, v8, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2607501
    const-string v8, "shipment_tracking_no"

    invoke-virtual {v9, v8, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2607502
    new-instance v8, LX/HoT;

    invoke-direct {v8}, LX/HoT;-><init>()V

    move-object v8, v8

    .line 2607503
    const-string v10, "input"

    invoke-virtual {v8, v10, v9}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v8

    check-cast v8, LX/HoT;

    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v8

    .line 2607504
    iget-object v9, v5, LX/IkW;->a:LX/0tX;

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    invoke-static {v8}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 2607505
    new-instance v9, LX/IkV;

    invoke-direct {v9, v5}, LX/IkV;-><init>(LX/IkW;)V

    .line 2607506
    sget-object v10, LX/131;->INSTANCE:LX/131;

    move-object v10, v10

    .line 2607507
    invoke-static {v8, v9, v10}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v5, v8

    .line 2607508
    iget-object v6, v1, LX/Iku;->a:LX/Ikv;

    iget-object v6, v6, LX/Ikv;->h:LX/0Vd;

    iget-object v7, v1, LX/Iku;->a:LX/Ikv;

    iget-object v7, v7, LX/Ikv;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2607509
    const v1, -0x71b88374

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
