.class public final LX/Hxj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 0

    .prologue
    .line 2522806
    iput-object p1, p0, LX/Hxj;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2522807
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2522808
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2522809
    if-eqz p1, :cond_0

    .line 2522810
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2522811
    if-eqz v0, :cond_0

    .line 2522812
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2522813
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2522814
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2522815
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

    move-result-object v0

    .line 2522816
    iget-object v1, p0, LX/Hxj;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    .line 2522817
    iput-object v0, v1, LX/Hx5;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

    .line 2522818
    iget-object v1, p0, LX/Hxj;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    .line 2522819
    iget-object p0, v1, LX/Hxz;->c:LX/HxV;

    if-eqz p0, :cond_0

    .line 2522820
    iget-object p0, v1, LX/Hxz;->c:LX/HxV;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->a()I

    move-result p1

    invoke-interface {p0, p1}, LX/HxV;->a(I)V

    .line 2522821
    :cond_0
    return-void
.end method
