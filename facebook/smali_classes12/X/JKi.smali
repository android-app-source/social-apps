.class public final LX/JKi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5qR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2680508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/5qQ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2680509
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2680510
    const-class v1, LX/JJa;

    new-instance v2, LX/5qQ;

    const-string v3, "SetResultAndroid"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680511
    const-class v1, LX/JJc;

    new-instance v2, LX/5qQ;

    const-string v3, "RKAnalytics"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680512
    const-class v1, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;

    new-instance v2, LX/5qQ;

    const-string v3, "AdInterfacesModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680513
    const-class v1, LX/GNp;

    new-instance v2, LX/5qQ;

    const-string v3, "AdsPaymentsModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680514
    const-class v1, LX/JLI;

    new-instance v2, LX/5qQ;

    const-string v3, "RKAnalyticsFunnelLogger"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680515
    const-class v1, LX/JJr;

    new-instance v2, LX/5qQ;

    const-string v3, "FBProfileCurationTagsEditViewEventHandler"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680516
    const-class v1, LX/JJu;

    new-instance v2, LX/5qQ;

    const-string v3, "FBProfileNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680517
    const-class v1, LX/JJn;

    new-instance v2, LX/5qQ;

    const-string v3, "DeviceRequestAndroid"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680518
    const-class v1, LX/F5M;

    new-instance v2, LX/5qQ;

    const-string v3, "RKContactsManager"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680519
    const-class v1, LX/F5T;

    new-instance v2, LX/5qQ;

    const-string v3, "RKTreehouseManager"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680520
    const-class v1, LX/9DL;

    new-instance v2, LX/5qQ;

    const-string v3, "FBCommentGroupCommercePredictiveComments"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680521
    const-class v1, LX/JJk;

    new-instance v2, LX/5qQ;

    const-string v3, "FBCommunityCommerceComposerModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680522
    const-class v1, LX/JK3;

    new-instance v2, LX/5qQ;

    const-string v3, "FBEventsNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680523
    const-class v1, LX/JLS;

    new-instance v2, LX/5qQ;

    const-string v3, "FBLoyaltyNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680524
    const-class v1, LX/JLW;

    new-instance v2, LX/5qQ;

    const-string v3, "FBMarketplaceComposerBridgeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680525
    const-class v1, LX/JLY;

    new-instance v2, LX/5qQ;

    const-string v3, "FBMarketplaceImagePickerModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680526
    const-class v1, LX/JLc;

    new-instance v2, LX/5qQ;

    const-string v3, "FBMarketplaceNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680527
    const-class v1, LX/JLe;

    new-instance v2, LX/5qQ;

    const-string v3, "FBReactSearchInputNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680528
    const-class v1, LX/JJg;

    new-instance v2, LX/5qQ;

    const-string v3, "FBShopNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680529
    const-class v1, LX/JLD;

    new-instance v2, LX/5qQ;

    const-string v3, "FigBottomSheetReactModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680530
    const-class v1, LX/JLK;

    new-instance v2, LX/5qQ;

    const-string v3, "GoodwillVideoNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680531
    const-class v1, LX/JLj;

    new-instance v2, LX/5qQ;

    const-string v3, "FBMarketplaceQuickPerformanceLoggerModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680532
    const-class v1, LX/JLo;

    new-instance v2, LX/5qQ;

    const-string v3, "PageServiceAddEditPickerNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680533
    const-class v1, LX/JM1;

    new-instance v2, LX/5qQ;

    const-string v3, "PrivacyCheckupReactModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680534
    const-class v1, LX/JM5;

    new-instance v2, LX/5qQ;

    const-string v3, "FBProfileEditViewEventHandler"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680535
    const-class v1, LX/K4a;

    new-instance v2, LX/5qQ;

    const-string v3, "ReactNarrativeEngineNativeModule"

    invoke-direct {v2, v3, v4, v4, v4}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680536
    return-object v0
.end method
