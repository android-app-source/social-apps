.class public final LX/J6A;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 2649258
    iput-object p1, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iput-object p2, p0, LX/J6A;->a:Landroid/support/v4/app/DialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2649259
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->A:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 2649260
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->A:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 2649261
    :cond_0
    iget-object v0, p0, LX/J6A;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2649262
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    sget-object v1, LX/J5h;->PRIVACY_REVIEW_WRITE_SUCCESS:LX/J5h;

    const/4 v2, 0x1

    .line 2649263
    invoke-static {v0, v1, v2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;LX/J5h;Z)V

    .line 2649264
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->finish()V

    .line 2649265
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2649266
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->A:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 2649267
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->A:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 2649268
    :cond_0
    iget-object v0, p0, LX/J6A;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2649269
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-static {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->l(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)I

    .line 2649270
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    sget-object v1, LX/J5h;->PRIVACY_REVIEW_WRITE_FAILURE:LX/J5h;

    const/4 v2, 0x1

    .line 2649271
    invoke-static {v0, v1, v2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;LX/J5h;Z)V

    .line 2649272
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-static {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->t(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)Z

    move-result v0

    .line 2649273
    if-nez v0, :cond_1

    .line 2649274
    iget-object v0, p0, LX/J6A;->b:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->y:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0838f3

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2649275
    :cond_1
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2649276
    invoke-direct {p0}, LX/J6A;->a()V

    return-void
.end method
