.class public final LX/IWN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/1Zp",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IWQ;


# direct methods
.method public constructor <init>(LX/IWQ;)V
    .locals 0

    .prologue
    .line 2583793
    iput-object p1, p0, LX/IWN;->a:LX/IWQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2583794
    iget-object v0, p0, LX/IWN;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/groups/photos/loader/GroupAlbumsPagedListLoader$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/groups/photos/loader/GroupAlbumsPagedListLoader$1$1;-><init>(LX/IWN;)V

    const v2, -0x7d29db89

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2583795
    iget-object v0, p0, LX/IWN;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->c:LX/0tX;

    iget-object v1, p0, LX/IWN;->a:LX/IWQ;

    iget-object v1, v1, LX/IWQ;->f:Landroid/content/res/Resources;

    iget-object v2, p0, LX/IWN;->a:LX/IWQ;

    iget-object v2, v2, LX/IWQ;->g:Ljava/lang/String;

    iget-object v3, p0, LX/IWN;->a:LX/IWQ;

    iget-object v3, v3, LX/IWQ;->k:Ljava/lang/String;

    .line 2583796
    new-instance v4, LX/IWW;

    invoke-direct {v4}, LX/IWW;-><init>()V

    move-object v4, v4

    .line 2583797
    const-string v5, "albums_to_fetch"

    sget-object v6, LX/IWQ;->a:Ljava/lang/Integer;

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v6, "preview_image_thumb_height"

    const p0, 0x7f0b23f2

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "preview_image_thumb_width"

    const p0, 0x7f0b23f2

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "cover_photo_height"

    const p0, 0x7f0b23fa

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "cover_photo_width"

    const p0, 0x7f0b23fa

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "group"

    invoke-virtual {v5, v6, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "preview_images_count"

    sget-object p0, LX/IWQ;->b:Ljava/lang/Integer;

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2583798
    if-eqz v3, :cond_0

    .line 2583799
    const-string v5, "end_cursor"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2583800
    :cond_0
    move-object v1, v4

    .line 2583801
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
