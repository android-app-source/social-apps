.class public final LX/JPO;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JPQ;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JPP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JPQ",
            "<TE;>.MultiPagesCardComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JPQ;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JPQ;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 2689662
    iput-object p1, p0, LX/JPO;->b:LX/JPQ;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2689663
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "feedProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "feedUnitItem"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "pageSwitcher"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JPO;->c:[Ljava/lang/String;

    .line 2689664
    iput v3, p0, LX/JPO;->d:I

    .line 2689665
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JPO;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JPO;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JPO;LX/1De;IILX/JPP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JPQ",
            "<TE;>.MultiPagesCardComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2689666
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2689667
    iput-object p4, p0, LX/JPO;->a:LX/JPP;

    .line 2689668
    iget-object v0, p0, LX/JPO;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2689669
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2689670
    invoke-super {p0}, LX/1X5;->a()V

    .line 2689671
    const/4 v0, 0x0

    iput-object v0, p0, LX/JPO;->a:LX/JPP;

    .line 2689672
    iget-object v0, p0, LX/JPO;->b:LX/JPQ;

    iget-object v0, v0, LX/JPQ;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2689673
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JPQ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2689674
    iget-object v1, p0, LX/JPO;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JPO;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JPO;->d:I

    if-ge v1, v2, :cond_2

    .line 2689675
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2689676
    :goto_0
    iget v2, p0, LX/JPO;->d:I

    if-ge v0, v2, :cond_1

    .line 2689677
    iget-object v2, p0, LX/JPO;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2689678
    iget-object v2, p0, LX/JPO;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2689679
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2689680
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2689681
    :cond_2
    iget-object v0, p0, LX/JPO;->a:LX/JPP;

    .line 2689682
    invoke-virtual {p0}, LX/JPO;->a()V

    .line 2689683
    return-object v0
.end method
