.class public LX/JE2;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JE2;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2664919
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2664920
    sget-object v0, LX/0ax;->jc:Ljava/lang/String;

    const-string v1, "{video_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/JE1;

    invoke-direct {v1, p0}, LX/JE1;-><init>(LX/JE2;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2664921
    return-void
.end method

.method public static a(LX/0QB;)LX/JE2;
    .locals 3

    .prologue
    .line 2664923
    sget-object v0, LX/JE2;->a:LX/JE2;

    if-nez v0, :cond_1

    .line 2664924
    const-class v1, LX/JE2;

    monitor-enter v1

    .line 2664925
    :try_start_0
    sget-object v0, LX/JE2;->a:LX/JE2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2664926
    if-eqz v2, :cond_0

    .line 2664927
    :try_start_1
    new-instance v0, LX/JE2;

    invoke-direct {v0}, LX/JE2;-><init>()V

    .line 2664928
    move-object v0, v0

    .line 2664929
    sput-object v0, LX/JE2;->a:LX/JE2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2664930
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2664931
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2664932
    :cond_1
    sget-object v0, LX/JE2;->a:LX/JE2;

    return-object v0

    .line 2664933
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2664934
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2664922
    const/4 v0, 0x1

    return v0
.end method
