.class public final LX/JGW;
.super LX/JGN;
.source ""

# interfaces
.implements LX/JGQ;
.implements LX/1Ai;


# instance fields
.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9nR;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/JGZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Landroid/graphics/PorterDuffColorFilter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/1Up;

.field public g:F

.field public h:F

.field public i:I

.field public j:I

.field public k:Z

.field public l:I

.field private m:LX/JGr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2667599
    invoke-direct {p0}, LX/JGN;-><init>()V

    .line 2667600
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/JGW;->c:Ljava/util/List;

    .line 2667601
    sget-object v0, LX/1Up;->g:LX/1Up;

    move-object v0, v0

    .line 2667602
    iput-object v0, p0, LX/JGW;->f:LX/1Up;

    .line 2667603
    const/16 v0, 0x12c

    iput v0, p0, LX/JGW;->l:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2667593
    iget-object v0, p0, LX/JGW;->d:LX/JGZ;

    if-eqz v0, :cond_0

    .line 2667594
    iget-object v0, p0, LX/JGW;->d:LX/JGZ;

    .line 2667595
    iget p0, v0, LX/JGZ;->d:I

    add-int/lit8 p0, p0, -0x1

    iput p0, v0, LX/JGZ;->d:I

    .line 2667596
    iget p0, v0, LX/JGZ;->d:I

    if-nez p0, :cond_0

    .line 2667597
    iget-object p0, v0, LX/JGZ;->c:LX/1aZ;

    invoke-interface {p0}, LX/1aZ;->e()V

    .line 2667598
    :cond_0
    return-void
.end method

.method public final a(LX/JGr;)V
    .locals 5

    .prologue
    .line 2667569
    iput-object p1, p0, LX/JGW;->m:LX/JGr;

    .line 2667570
    iget-object v0, p0, LX/JGW;->d:LX/JGZ;

    if-nez v0, :cond_0

    .line 2667571
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No DraweeRequestHelper - width: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/JGN;->l()F

    move-result v2

    invoke-virtual {p0}, LX/JGN;->j()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/JGN;->m()F

    move-result v2

    invoke-virtual {p0}, LX/JGN;->k()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - number of sources: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/JGW;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2667572
    :cond_0
    iget-object v0, p0, LX/JGW;->d:LX/JGZ;

    invoke-virtual {v0}, LX/JGZ;->b()LX/1af;

    move-result-object v1

    .line 2667573
    iget-object v0, v1, LX/1af;->c:LX/4Ab;

    move-object v0, v0

    .line 2667574
    iget v2, p0, LX/JGW;->i:I

    if-nez v2, :cond_1

    iget v2, p0, LX/JGW;->h:F

    const/high16 v3, 0x3f000000    # 0.5f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_6

    :cond_1
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2667575
    if-eqz v2, :cond_5

    .line 2667576
    if-nez v0, :cond_2

    .line 2667577
    new-instance v0, LX/4Ab;

    invoke-direct {v0}, LX/4Ab;-><init>()V

    .line 2667578
    :cond_2
    iget v2, p0, LX/JGW;->i:I

    iget v3, p0, LX/JGW;->g:F

    invoke-virtual {v0, v2, v3}, LX/4Ab;->a(IF)LX/4Ab;

    .line 2667579
    iget v2, p0, LX/JGW;->h:F

    invoke-virtual {v0, v2}, LX/4Ab;->a(F)LX/4Ab;

    .line 2667580
    invoke-virtual {v1, v0}, LX/1af;->a(LX/4Ab;)V

    .line 2667581
    :cond_3
    :goto_1
    iget-object v0, p0, LX/JGW;->f:LX/1Up;

    invoke-virtual {v1, v0}, LX/1af;->a(LX/1Up;)V

    .line 2667582
    iget-object v0, p0, LX/JGW;->e:Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {v1, v0}, LX/1af;->a(Landroid/graphics/ColorFilter;)V

    .line 2667583
    iget v0, p0, LX/JGW;->l:I

    invoke-virtual {v1, v0}, LX/1af;->a(I)V

    .line 2667584
    invoke-virtual {v1}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, LX/JGN;->j()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p0}, LX/JGN;->k()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {p0}, LX/JGN;->l()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p0}, LX/JGN;->m()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2667585
    iget-object v0, p0, LX/JGW;->d:LX/JGZ;

    .line 2667586
    iget v1, v0, LX/JGZ;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/JGZ;->d:I

    .line 2667587
    iget v1, v0, LX/JGZ;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 2667588
    invoke-virtual {v0}, LX/JGZ;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1}, LX/JGr;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable$Callback;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2667589
    iget-object v1, v0, LX/JGZ;->c:LX/1aZ;

    invoke-interface {v1}, LX/1aZ;->d()V

    .line 2667590
    :cond_4
    return-void

    .line 2667591
    :cond_5
    if-eqz v0, :cond_3

    .line 2667592
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/1af;->a(LX/4Ab;)V

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2667568
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2667518
    iget-object v0, p0, LX/JGW;->m:LX/JGr;

    if-eqz v0, :cond_0

    iget v0, p0, LX/JGW;->j:I

    if-eqz v0, :cond_0

    .line 2667519
    iget-object v0, p0, LX/JGW;->m:LX/JGr;

    iget v1, p0, LX/JGW;->j:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, LX/JGr;->a(II)V

    .line 2667520
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2667564
    iget-object v0, p0, LX/JGW;->m:LX/JGr;

    if-eqz v0, :cond_0

    iget v0, p0, LX/JGW;->j:I

    if-eqz v0, :cond_0

    .line 2667565
    iget-object v0, p0, LX/JGW;->m:LX/JGr;

    iget v1, p0, LX/JGW;->j:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/JGr;->a(II)V

    .line 2667566
    iget-object v0, p0, LX/JGW;->m:LX/JGr;

    iget v1, p0, LX/JGW;->j:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/JGr;->a(II)V

    .line 2667567
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2667563
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2667562
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2667558
    iget-object v0, p0, LX/JGW;->m:LX/JGr;

    if-eqz v0, :cond_0

    iget v0, p0, LX/JGW;->j:I

    if-eqz v0, :cond_0

    .line 2667559
    iget-object v0, p0, LX/JGW;->m:LX/JGr;

    iget v1, p0, LX/JGW;->j:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/JGr;->a(II)V

    .line 2667560
    iget-object v0, p0, LX/JGW;->m:LX/JGr;

    iget v1, p0, LX/JGW;->j:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/JGr;->a(II)V

    .line 2667561
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2667557
    iget-object v0, p0, LX/JGW;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2667554
    iget-object v0, p0, LX/JGW;->d:LX/JGZ;

    if-eqz v0, :cond_0

    .line 2667555
    iget-object v0, p0, LX/JGW;->d:LX/JGZ;

    invoke-virtual {v0}, LX/JGZ;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2667556
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 2667521
    invoke-super {p0}, LX/JGN;->d()V

    .line 2667522
    const/4 v1, 0x0

    .line 2667523
    invoke-virtual {p0}, LX/JGN;->l()F

    move-result v0

    invoke-virtual {p0}, LX/JGN;->j()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p0}, LX/JGN;->m()F

    move-result v2

    invoke-virtual {p0}, LX/JGN;->k()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, LX/JGW;->c:Ljava/util/List;

    invoke-static {v0, v2, v3}, LX/K0R;->a(IILjava/util/List;)LX/K0Q;

    move-result-object v0

    .line 2667524
    iget-object v2, v0, LX/K0Q;->a:LX/9nR;

    move-object v2, v2

    .line 2667525
    iget-object v3, v0, LX/K0Q;->b:LX/9nR;

    move-object v3, v3

    .line 2667526
    if-nez v2, :cond_0

    .line 2667527
    iput-object v1, p0, LX/JGW;->d:LX/JGZ;

    .line 2667528
    :goto_0
    return-void

    .line 2667529
    :cond_0
    invoke-virtual {v2}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v0

    .line 2667530
    if-nez v0, :cond_4

    const/4 v0, 0x0

    .line 2667531
    :goto_1
    const-string v4, "file"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "content"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2667532
    if-eqz v0, :cond_3

    .line 2667533
    invoke-virtual {p0}, LX/JGN;->l()F

    move-result v0

    invoke-virtual {p0}, LX/JGN;->j()F

    move-result v4

    sub-float/2addr v0, v4

    float-to-int v4, v0

    .line 2667534
    invoke-virtual {p0}, LX/JGN;->m()F

    move-result v0

    invoke-virtual {p0}, LX/JGN;->k()F

    move-result v5

    sub-float/2addr v0, v5

    float-to-int v5, v0

    .line 2667535
    new-instance v0, LX/1o9;

    invoke-direct {v0, v4, v5}, LX/1o9;-><init>(II)V

    .line 2667536
    :goto_3
    invoke-virtual {v2}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    .line 2667537
    iput-object v0, v2, LX/1bX;->c:LX/1o9;

    .line 2667538
    move-object v2, v2

    .line 2667539
    iget-boolean v4, p0, LX/JGW;->k:Z

    .line 2667540
    iput-boolean v4, v2, LX/1bX;->g:Z

    .line 2667541
    move-object v2, v2

    .line 2667542
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 2667543
    if-eqz v3, :cond_2

    .line 2667544
    invoke-virtual {v3}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    .line 2667545
    iput-object v0, v1, LX/1bX;->c:LX/1o9;

    .line 2667546
    move-object v0, v1

    .line 2667547
    iget-boolean v1, p0, LX/JGW;->k:Z

    .line 2667548
    iput-boolean v1, v0, LX/1bX;->g:Z

    .line 2667549
    move-object v0, v0

    .line 2667550
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 2667551
    :cond_2
    new-instance v3, LX/JGZ;

    invoke-static {v2}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bf;

    invoke-direct {v3, v0, v1, p0}, LX/JGZ;-><init>(LX/1bf;LX/1bf;LX/1Ai;)V

    iput-object v3, p0, LX/JGW;->d:LX/JGZ;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_3

    .line 2667552
    :cond_4
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2667553
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method
