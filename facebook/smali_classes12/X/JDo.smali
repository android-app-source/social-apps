.class public final LX/JDo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AQ4",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JDu;


# direct methods
.method public constructor <init>(LX/JDu;)V
    .locals 0

    .prologue
    .line 2664624
    iput-object p1, p0, LX/JDo;->a:LX/JDu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2664625
    iget-object v0, p0, LX/JDo;->a:LX/JDu;

    .line 2664626
    iget-object v1, v0, LX/JDu;->e:LX/AR9;

    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    invoke-virtual {v3}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v3

    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v4

    invoke-virtual {v4}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v4

    .line 2664627
    new-instance v5, LX/AR8;

    invoke-static {v1}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v9

    check-cast v9, LX/8LV;

    invoke-static {v1}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v10

    check-cast v10, LX/1EZ;

    move-object v6, v2

    move-object v7, v3

    move-object v8, v4

    invoke-direct/range {v5 .. v10}, LX/AR8;-><init>(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;Lcom/facebook/ipc/composer/model/ComposerDateInfo;LX/8LV;LX/1EZ;)V

    .line 2664628
    const/16 v6, 0x12cb

    invoke-static {v1, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    .line 2664629
    iput-object v6, v5, LX/AR8;->a:LX/0Or;

    .line 2664630
    move-object v1, v5

    .line 2664631
    const/4 v4, 0x0

    .line 2664632
    iget-object v2, v1, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2664633
    iget-object v2, v1, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2664634
    :goto_0
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2664635
    iget-object v2, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664636
    iget-object v5, v2, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->f:Ljava/lang/String;

    move-object v2, v5

    .line 2664637
    if-eqz v2, :cond_2

    .line 2664638
    iget-object v2, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664639
    iget-object v5, v2, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->f:Ljava/lang/String;

    move-object v2, v5

    .line 2664640
    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2664641
    :cond_0
    iget-object v2, v1, LX/AR8;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2664642
    new-instance v5, LX/7l5;

    invoke-direct {v5}, LX/7l5;-><init>()V

    iget-object v7, v1, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v7}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v7

    .line 2664643
    iput-object v7, v5, LX/7l5;->a:Ljava/lang/String;

    .line 2664644
    move-object v5, v5

    .line 2664645
    iget-object v7, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v7

    .line 2664646
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2664647
    iput-object v2, v5, LX/7l5;->b:Ljava/lang/String;

    .line 2664648
    move-object v2, v5

    .line 2664649
    iget-object v5, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664650
    iget-object v7, v5, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->d:Ljava/lang/String;

    move-object v5, v7

    .line 2664651
    iput-object v5, v2, LX/7l5;->c:Ljava/lang/String;

    .line 2664652
    move-object v2, v2

    .line 2664653
    iget-object v5, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664654
    iget-object v7, v5, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->a:Ljava/lang/String;

    move-object v5, v7

    .line 2664655
    iput-object v5, v2, LX/7l5;->d:Ljava/lang/String;

    .line 2664656
    move-object v2, v2

    .line 2664657
    iget-object v5, v1, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {v5}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v5

    .line 2664658
    iput-object v5, v2, LX/7l5;->e:Ljava/lang/String;

    .line 2664659
    move-object v2, v2

    .line 2664660
    iget-object v5, v1, LX/AR8;->f:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v5

    .line 2664661
    invoke-static {v5}, LX/7l5;->c(Lcom/facebook/uicontrib/datepicker/Date;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, LX/7l5;->f:Ljava/lang/String;

    .line 2664662
    move-object v2, v2

    .line 2664663
    iget-object v5, v1, LX/AR8;->f:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->b()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v5

    .line 2664664
    invoke-static {v5}, LX/7l5;->c(Lcom/facebook/uicontrib/datepicker/Date;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, LX/7l5;->g:Ljava/lang/String;

    .line 2664665
    move-object v2, v2

    .line 2664666
    const-string v5, "fb4a_composer"

    .line 2664667
    iput-object v5, v2, LX/7l5;->h:Ljava/lang/String;

    .line 2664668
    move-object v2, v2

    .line 2664669
    iput-object v3, v2, LX/7l5;->i:Ljava/lang/String;

    .line 2664670
    move-object v2, v2

    .line 2664671
    iget-object v3, v1, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v3

    .line 2664672
    iput-object v3, v2, LX/7l5;->j:Ljava/lang/String;

    .line 2664673
    move-object v2, v2

    .line 2664674
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2664675
    iput-object v3, v2, LX/7l5;->k:LX/0Px;

    .line 2664676
    move-object v2, v2

    .line 2664677
    iget-object v3, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664678
    iget-object v5, v3, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-object v3, v5

    .line 2664679
    iput-object v3, v2, LX/7l5;->m:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 2664680
    move-object v2, v2

    .line 2664681
    iget-object v3, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664682
    iget-boolean v5, v3, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->e:Z

    move v3, v5

    .line 2664683
    iput-boolean v3, v2, LX/7l5;->n:Z

    .line 2664684
    move-object v2, v2

    .line 2664685
    iget-object v3, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664686
    iget-boolean v5, v3, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->h:Z

    move v3, v5

    .line 2664687
    iput-boolean v3, v2, LX/7l5;->o:Z

    .line 2664688
    move-object v2, v2

    .line 2664689
    iget-object v3, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664690
    iget-object v5, v3, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    move-object v3, v5

    .line 2664691
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    if-ne v3, v5, :cond_3

    .line 2664692
    :goto_1
    iput-object v4, v2, LX/7l5;->p:Ljava/lang/String;

    .line 2664693
    move-object v2, v2

    .line 2664694
    iget-object v3, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664695
    iget-object v4, v3, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->j:Ljava/lang/String;

    move-object v3, v4

    .line 2664696
    iput-object v3, v2, LX/7l5;->q:Ljava/lang/String;

    .line 2664697
    move-object v2, v2

    .line 2664698
    iget-object v3, v1, LX/AR8;->f:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->c()Z

    move-result v3

    .line 2664699
    iput-boolean v3, v2, LX/7l5;->r:Z

    .line 2664700
    move-object v2, v2

    .line 2664701
    iget-object v3, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664702
    iget-object v4, v3, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->k:Ljava/lang/String;

    move-object v3, v4

    .line 2664703
    iput-object v3, v2, LX/7l5;->s:Ljava/lang/String;

    .line 2664704
    move-object v2, v2

    .line 2664705
    invoke-virtual {v2}, LX/7l5;->a()Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    move-result-object v2

    move-object v2, v2

    .line 2664706
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "publishLifeEventParams"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    .line 2664707
    invoke-virtual {v1, v2}, LX/AR8;->a(Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2664708
    const-string v1, "is_uploading_media"

    const/4 v2, 0x1

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2664709
    :cond_1
    move-object v0, v3

    .line 2664710
    return-object v0

    .line 2664711
    :cond_2
    iget-object v2, v1, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2664712
    iget-object v2, v1, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v5, v2

    :goto_2
    if-ge v5, v8, :cond_0

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 2664713
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2664714
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 2664715
    :cond_3
    iget-object v3, v1, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2664716
    iget-object v4, v3, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    move-object v3, v4

    .line 2664717
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->name()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_4
    move-object v3, v4

    goto/16 :goto_0
.end method
