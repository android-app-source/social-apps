.class public final enum LX/IOE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IOE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IOE;

.field public static final enum INVITED:LX/IOE;

.field public static final enum NOT_INVITED:LX/IOE;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2571621
    new-instance v0, LX/IOE;

    const-string v1, "NOT_INVITED"

    invoke-direct {v0, v1, v2}, LX/IOE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IOE;->NOT_INVITED:LX/IOE;

    .line 2571622
    new-instance v0, LX/IOE;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v3}, LX/IOE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IOE;->INVITED:LX/IOE;

    .line 2571623
    const/4 v0, 0x2

    new-array v0, v0, [LX/IOE;

    sget-object v1, LX/IOE;->NOT_INVITED:LX/IOE;

    aput-object v1, v0, v2

    sget-object v1, LX/IOE;->INVITED:LX/IOE;

    aput-object v1, v0, v3

    sput-object v0, LX/IOE;->$VALUES:[LX/IOE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2571624
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IOE;
    .locals 1

    .prologue
    .line 2571625
    const-class v0, LX/IOE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IOE;

    return-object v0
.end method

.method public static values()[LX/IOE;
    .locals 1

    .prologue
    .line 2571626
    sget-object v0, LX/IOE;->$VALUES:[LX/IOE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IOE;

    return-object v0
.end method
