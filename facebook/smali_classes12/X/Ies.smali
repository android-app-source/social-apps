.class public LX/Ies;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/IfL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public b:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/IfB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/Ieq;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2599028
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2599029
    new-instance v0, LX/Ier;

    invoke-direct {v0, p0}, LX/Ier;-><init>(LX/Ies;)V

    iput-object v0, p0, LX/Ies;->g:LX/Ieq;

    .line 2599030
    iput-object p1, p0, LX/Ies;->a:Landroid/view/LayoutInflater;

    .line 2599031
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2599032
    iget-object v0, p0, LX/Ies;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030370

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2599033
    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;

    iget-object v2, p0, LX/Ies;->g:LX/Ieq;

    .line 2599034
    iput-object v2, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->b:LX/Ieq;

    .line 2599035
    new-instance v0, LX/IfL;

    invoke-direct {v0, v1}, LX/IfL;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2599036
    check-cast p1, LX/IfL;

    .line 2599037
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;

    .line 2599038
    iget-object v1, p0, LX/Ies;->c:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599039
    iget-object v2, p0, LX/Ies;->e:Ljava/util/Set;

    iget-object v3, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599040
    iget-object v4, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2599041
    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    iget-object v3, p0, LX/Ies;->f:Ljava/util/Set;

    iget-object v4, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599042
    iget-object p0, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, p0

    .line 2599043
    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;ZZ)V

    .line 2599044
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->setTag(Ljava/lang/Object;)V

    .line 2599045
    return-void
.end method

.method public final a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2599046
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2599047
    iget-object v1, p0, LX/Ies;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v0, p0, LX/Ies;->c:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599048
    iget-object v5, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599049
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2599050
    iget-object v6, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599051
    iget-object v7, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2599052
    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2599053
    invoke-virtual {p0, v1}, LX/1OM;->d(I)V

    move v0, v1

    .line 2599054
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2599055
    :cond_0
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2599056
    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    .line 2599057
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Ies;->c:LX/0Px;

    .line 2599058
    return-void
.end method

.method public final b(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2599059
    iget-object v1, p0, LX/Ies;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/Ies;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599060
    iget-object v0, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599061
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2599062
    iget-object v4, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599063
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2599064
    invoke-static {v0, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2599065
    iget-object v0, p0, LX/Ies;->e:Ljava/util/Set;

    iget-object v1, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599066
    iget-object v3, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2599067
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2599068
    iget-object v0, p0, LX/Ies;->f:Ljava/util/Set;

    iget-object v1, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599069
    iget-object v3, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2599070
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2599071
    invoke-virtual {p0, v2}, LX/1OM;->i_(I)V

    .line 2599072
    :cond_0
    return-void

    .line 2599073
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 2599074
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2599075
    iget-object v1, p0, LX/Ies;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/Ies;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599076
    iget-object v0, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599077
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2599078
    iget-object v4, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599079
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2599080
    invoke-static {v0, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2599081
    iget-object v0, p0, LX/Ies;->f:Ljava/util/Set;

    iget-object v1, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599082
    iget-object v3, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2599083
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2599084
    invoke-virtual {p0, v2}, LX/1OM;->i_(I)V

    .line 2599085
    :cond_0
    return-void

    .line 2599086
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 2599087
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2599088
    iget-object v0, p0, LX/Ies;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
