.class public LX/HV4;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field public final b:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

.field public final c:Lcom/facebook/widget/listview/BetterListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 2474698
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/HV4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2474699
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 2474700
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2474701
    const v0, 0x7f0315ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2474702
    const v0, 0x7f0d30e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, LX/HV4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2474703
    const v0, 0x7f0d30e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    iput-object v0, p0, LX/HV4;->b:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    .line 2474704
    const v0, 0x7f0d30e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, LX/HV4;->c:Lcom/facebook/widget/listview/BetterListView;

    .line 2474705
    iget-object v0, p0, LX/HV4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->MEDIUM:LX/0xr;

    iget-object v3, p0, LX/HV4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v3}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2474706
    iget-object v0, p0, LX/HV4;->b:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->LIGHT:LX/0xr;

    iget-object v3, p0, LX/HV4;->b:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v3}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2474707
    return-void
.end method
