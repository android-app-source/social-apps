.class public final LX/IXJ;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticlesTrackerModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/IXQ;


# direct methods
.method public constructor <init>(LX/IXQ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2585862
    iput-object p1, p0, LX/IXJ;->b:LX/IXQ;

    iput-object p2, p0, LX/IXJ;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2585863
    iget-object v0, p0, LX/IXJ;->b:LX/IXQ;

    iget-object v0, v0, LX/IXQ;->j:LX/03V;

    const-string v1, "ThirdPartyTrackerHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error fetching async tracker info: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/IXJ;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2585864
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2585865
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2585866
    if-eqz p1, :cond_1

    .line 2585867
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2585868
    if-eqz v0, :cond_1

    .line 2585869
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2585870
    check-cast v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticlesTrackerModel;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticlesTrackerModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2585871
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2585872
    check-cast v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticlesTrackerModel;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticlesTrackerModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2585873
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 2585874
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2585875
    check-cast v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticlesTrackerModel;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticlesTrackerModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2585876
    iget-object v2, p0, LX/IXJ;->b:LX/IXQ;

    iget-object v3, p0, LX/IXJ;->a:Ljava/lang/String;

    invoke-static {v2, v0, v3, v1}, LX/IXQ;->b(LX/IXQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2585877
    :cond_1
    return-void
.end method
