.class public final LX/HQC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V
    .locals 0

    .prologue
    .line 2463112
    iput-object p1, p0, LX/HQC;->a:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x5f1ab9fe

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2463113
    iget-object v1, p0, LX/HQC;->a:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->q:LX/9XE;

    iget-object v2, p0, LX/HQC;->a:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-wide v2, v2, LX/HQa;->f:J

    iget-object v4, p0, LX/HQC;->a:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    invoke-static {v4}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getProfilePhotoId(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)Ljava/lang/String;

    move-result-object v4

    .line 2463114
    iget-object v6, v1, LX/9XE;->a:LX/0Zb;

    sget-object v7, LX/9XI;->EVENT_TAPPED_PROFILE_PHOTO:LX/9XI;

    invoke-static {v7, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "photo_id"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2463115
    iget-object v1, p0, LX/HQC;->a:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    .line 2463116
    invoke-static {v1, p1}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->b$redex0(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Landroid/view/View;)V

    .line 2463117
    const v1, 0x6eff0ba3

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
