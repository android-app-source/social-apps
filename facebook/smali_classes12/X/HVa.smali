.class public LX/HVa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/17Y;

.field public final c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17Y;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475230
    iput-object p1, p0, LX/HVa;->a:Landroid/content/Context;

    .line 2475231
    iput-object p2, p0, LX/HVa;->b:LX/17Y;

    .line 2475232
    iput-object p3, p0, LX/HVa;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2475233
    return-void
.end method

.method public static a(LX/HVa;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2475214
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2475215
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2475216
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2475217
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2475218
    iget-object v6, p0, LX/HVa;->a:Landroid/content/Context;

    invoke-static {v6, v0}, LX/2A3;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2475219
    iget-object v2, p0, LX/HVa;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    move v0, v1

    .line 2475220
    :goto_1
    return v0

    .line 2475221
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2475222
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2475223
    iget-object v0, p0, LX/HVa;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2475224
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2475225
    if-eqz v0, :cond_2

    .line 2475226
    iget-object v2, p0, LX/HVa;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    move v0, v1

    .line 2475227
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2475228
    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/HVa;
    .locals 4

    .prologue
    .line 2475212
    new-instance v3, LX/HVa;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v3, v0, v1, v2}, LX/HVa;-><init>(Landroid/content/Context;LX/17Y;Lcom/facebook/content/SecureContextHelper;)V

    .line 2475213
    return-object v3
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 4

    .prologue
    .line 2475197
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2475198
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;

    .line 2475199
    invoke-static {p0, v0}, LX/HVa;->a(LX/HVa;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2475200
    :cond_0
    :goto_1
    return-void

    .line 2475201
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2475202
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2475203
    if-eqz v0, :cond_0

    .line 2475204
    iget-object v1, p0, LX/HVa;->b:LX/17Y;

    iget-object v2, p0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2475205
    if-eqz v1, :cond_4

    .line 2475206
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/2yp;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2475207
    iget-object v0, p0, LX/HVa;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 2475208
    :cond_3
    iget-object v0, p0, LX/HVa;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 2475209
    :cond_4
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2475210
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2475211
    iget-object v0, p0, LX/HVa;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method
