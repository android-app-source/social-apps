.class public final enum LX/HzB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HzB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HzB;

.field public static final enum DB_FETCH:LX/HzB;

.field public static final enum FETCH_EVENT_COUNTS:LX/HzB;

.field public static final enum FETCH_SINGLE_EVENT:LX/HzB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2525356
    new-instance v0, LX/HzB;

    const-string v1, "DB_FETCH"

    invoke-direct {v0, v1, v2}, LX/HzB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HzB;->DB_FETCH:LX/HzB;

    .line 2525357
    new-instance v0, LX/HzB;

    const-string v1, "FETCH_SINGLE_EVENT"

    invoke-direct {v0, v1, v3}, LX/HzB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HzB;->FETCH_SINGLE_EVENT:LX/HzB;

    .line 2525358
    new-instance v0, LX/HzB;

    const-string v1, "FETCH_EVENT_COUNTS"

    invoke-direct {v0, v1, v4}, LX/HzB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HzB;->FETCH_EVENT_COUNTS:LX/HzB;

    .line 2525359
    const/4 v0, 0x3

    new-array v0, v0, [LX/HzB;

    sget-object v1, LX/HzB;->DB_FETCH:LX/HzB;

    aput-object v1, v0, v2

    sget-object v1, LX/HzB;->FETCH_SINGLE_EVENT:LX/HzB;

    aput-object v1, v0, v3

    sget-object v1, LX/HzB;->FETCH_EVENT_COUNTS:LX/HzB;

    aput-object v1, v0, v4

    sput-object v0, LX/HzB;->$VALUES:[LX/HzB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2525360
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HzB;
    .locals 1

    .prologue
    .line 2525361
    const-class v0, LX/HzB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HzB;

    return-object v0
.end method

.method public static values()[LX/HzB;
    .locals 1

    .prologue
    .line 2525362
    sget-object v0, LX/HzB;->$VALUES:[LX/HzB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HzB;

    return-object v0
.end method
