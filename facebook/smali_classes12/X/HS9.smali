.class public final enum LX/HS9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HS9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HS9;

.field public static final enum PRIMARY:LX/HS9;

.field public static final enum SECONDARY:LX/HS9;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2466623
    new-instance v0, LX/HS9;

    const-string v1, "PRIMARY"

    invoke-direct {v0, v1, v2}, LX/HS9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HS9;->PRIMARY:LX/HS9;

    .line 2466624
    new-instance v0, LX/HS9;

    const-string v1, "SECONDARY"

    invoke-direct {v0, v1, v3}, LX/HS9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HS9;->SECONDARY:LX/HS9;

    .line 2466625
    const/4 v0, 0x2

    new-array v0, v0, [LX/HS9;

    sget-object v1, LX/HS9;->PRIMARY:LX/HS9;

    aput-object v1, v0, v2

    sget-object v1, LX/HS9;->SECONDARY:LX/HS9;

    aput-object v1, v0, v3

    sput-object v0, LX/HS9;->$VALUES:[LX/HS9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2466626
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HS9;
    .locals 1

    .prologue
    .line 2466627
    const-class v0, LX/HS9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HS9;

    return-object v0
.end method

.method public static values()[LX/HS9;
    .locals 1

    .prologue
    .line 2466628
    sget-object v0, LX/HS9;->$VALUES:[LX/HS9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HS9;

    return-object v0
.end method
