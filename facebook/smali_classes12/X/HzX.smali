.class public LX/HzX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/0hB;


# direct methods
.method public constructor <init>(LX/0tX;LX/0hB;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2525699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2525700
    iput-object p1, p0, LX/HzX;->a:LX/0tX;

    .line 2525701
    iput-object p2, p0, LX/HzX;->b:LX/0hB;

    .line 2525702
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2525703
    new-instance v0, LX/7oI;

    invoke-direct {v0}, LX/7oI;-><init>()V

    .line 2525704
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2525705
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0f74

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2525706
    const-string v2, "profile_image_size"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2525707
    const-string v1, "cover_image_portrait_size"

    iget-object v2, p0, LX/HzX;->b:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2525708
    const-string v1, "cover_image_landscape_size"

    iget-object v2, p0, LX/HzX;->b:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2525709
    invoke-static {}, LX/7oV;->e()LX/7oI;

    move-result-object v1

    .line 2525710
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2525711
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 2525712
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 2525713
    iget-object v1, p0, LX/HzX;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
