.class public final LX/HrX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/93q;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 0

    .prologue
    .line 2511181
    iput-object p1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v5, 0x8

    const/4 v3, 0x0

    .line 2511182
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2511183
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-boolean v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->bL:Z

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_2

    .line 2511184
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->L(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2511185
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511186
    iput-boolean v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aP:Z

    .line 2511187
    :cond_0
    :goto_0
    return-void

    .line 2511188
    :cond_1
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->Z(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2511189
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0, p1}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto :goto_0

    .line 2511190
    :cond_2
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 2511191
    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v4, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    const/4 v6, 0x0

    .line 2511192
    if-eqz v1, :cond_3

    if-nez v4, :cond_11

    .line 2511193
    :cond_3
    :goto_1
    move v1, v6

    .line 2511194
    if-eqz v1, :cond_4

    .line 2511195
    invoke-virtual {v0, v2}, LX/0jL;->g(Z)Ljava/lang/Object;

    .line 2511196
    :cond_4
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511197
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-eqz v0, :cond_5

    .line 2511198
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_PRIVACY_READY:LX/0ge;

    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511199
    :cond_5
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_6

    .line 2511200
    :goto_2
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->v()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2511201
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->ar:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 2511202
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->c:Z

    if-nez v0, :cond_7

    .line 2511203
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->ar:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    move v2, v3

    .line 2511204
    goto :goto_2

    .line 2511205
    :cond_7
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->ar:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2511206
    if-eqz v2, :cond_c

    .line 2511207
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    invoke-virtual {v0, v3}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->setVisibility(I)V

    .line 2511208
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2511209
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-virtual {v0, v5}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->setVisibility(I)V

    .line 2511210
    :cond_8
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v1}, Lcom/facebook/composer/activity/ComposerFragment;->aJ(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v4

    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a(ZLcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 2511211
    :cond_9
    :goto_3
    if-eqz v2, :cond_b

    .line 2511212
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-boolean v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->aP:Z

    if-eqz v0, :cond_a

    .line 2511213
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511214
    iput-boolean v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->aP:Z

    .line 2511215
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->N$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511216
    :cond_a
    if-eqz p2, :cond_b

    .line 2511217
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511218
    :cond_b
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->aL(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511219
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v1, LX/5L2;->ON_PRIVACY_FETCHED:LX/5L2;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/5L2;)V

    goto/16 :goto_0

    .line 2511220
    :cond_c
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-eqz v0, :cond_e

    .line 2511221
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    invoke-virtual {v0, v3}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->setVisibility(I)V

    .line 2511222
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2511223
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-virtual {v0, v5}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->setVisibility(I)V

    .line 2511224
    :cond_d
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v1}, Lcom/facebook/composer/activity/ComposerFragment;->aJ(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v4

    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a(ZLcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto/16 :goto_3

    .line 2511225
    :cond_e
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eqz v0, :cond_9

    .line 2511226
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2511227
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    invoke-virtual {v0, v5}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->setVisibility(I)V

    .line 2511228
    :cond_f
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-virtual {v0, v3}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->setVisibility(I)V

    .line 2511229
    const/4 v1, 0x0

    .line 2511230
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->E()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-static {v0}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2511231
    new-instance v0, LX/HrW;

    invoke-direct {v0, p0}, LX/HrW;-><init>(LX/HrX;)V

    move-object v4, v0

    .line 2511232
    :goto_4
    iget-object v0, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v1}, Lcom/facebook/composer/activity/ComposerFragment;->aJ(Lcom/facebook/composer/activity/ComposerFragment;)Z

    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-object v5, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v1, p0, LX/HrX;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    invoke-virtual {v0, v5, v1, v4}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    :cond_10
    move-object v4, v1

    goto :goto_4

    .line 2511233
    :cond_11
    iget-object v7, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v7, :cond_12

    iget-object v7, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v7, v7, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v7, :cond_12

    iget-object v7, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v7, :cond_12

    iget-object v7, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v7, v7, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v7, :cond_12

    iget-object v7, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v7, v7, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v8, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v8, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2511234
    :cond_12
    iget-object v7, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v7, v7

    .line 2511235
    if-eqz v7, :cond_3

    .line 2511236
    iget-object v7, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v7, v7

    .line 2511237
    if-eqz v7, :cond_3

    .line 2511238
    iget-object v7, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v7, v7

    .line 2511239
    iget-object v8, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v8, v8

    .line 2511240
    invoke-static {v7, v8}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 2511241
    iget-boolean v7, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v7, v7

    .line 2511242
    iget-boolean v8, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v8, v8

    .line 2511243
    if-eq v7, v8, :cond_3

    :cond_13
    const/4 v6, 0x1

    goto/16 :goto_1
.end method
