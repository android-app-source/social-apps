.class public LX/Ipr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final irisSeqId:Ljava/lang/Long;

.field public final numNoOps:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2614789
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPaymentNoOp"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipr;->b:LX/1sv;

    .line 2614790
    new-instance v0, LX/1sw;

    const-string v1, "numNoOps"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipr;->c:LX/1sw;

    .line 2614791
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0xa

    const/16 v3, 0x3e8

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipr;->d:LX/1sw;

    .line 2614792
    sput-boolean v4, LX/Ipr;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2614785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2614786
    iput-object p1, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    .line 2614787
    iput-object p2, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    .line 2614788
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2614753
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2614754
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 2614755
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 2614756
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaPaymentNoOp"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2614757
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614758
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614759
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614760
    const/4 v1, 0x1

    .line 2614761
    iget-object v5, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 2614762
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614763
    const-string v1, "numNoOps"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614764
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614765
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614766
    iget-object v1, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 2614767
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614768
    :goto_3
    const/4 v1, 0x0

    .line 2614769
    :cond_0
    iget-object v5, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    if-eqz v5, :cond_2

    .line 2614770
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614771
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614772
    const-string v1, "irisSeqId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614773
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614774
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614775
    iget-object v0, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 2614776
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614777
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614778
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614779
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2614780
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2614781
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2614782
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 2614783
    :cond_6
    iget-object v1, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2614784
    :cond_7
    iget-object v0, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2614741
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2614742
    iget-object v0, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2614743
    iget-object v0, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2614744
    sget-object v0, LX/Ipr;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614745
    iget-object v0, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2614746
    :cond_0
    iget-object v0, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2614747
    iget-object v0, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2614748
    sget-object v0, LX/Ipr;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614749
    iget-object v0, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614750
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2614751
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2614752
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2614715
    if-nez p1, :cond_1

    .line 2614716
    :cond_0
    :goto_0
    return v0

    .line 2614717
    :cond_1
    instance-of v1, p1, LX/Ipr;

    if-eqz v1, :cond_0

    .line 2614718
    check-cast p1, LX/Ipr;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2614719
    if-nez p1, :cond_3

    .line 2614720
    :cond_2
    :goto_1
    move v0, v2

    .line 2614721
    goto :goto_0

    .line 2614722
    :cond_3
    iget-object v0, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2614723
    :goto_2
    iget-object v3, p1, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2614724
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2614725
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614726
    iget-object v0, p0, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipr;->numNoOps:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614727
    :cond_5
    iget-object v0, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2614728
    :goto_4
    iget-object v3, p1, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2614729
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2614730
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614731
    iget-object v0, p0, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipr;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 2614732
    goto :goto_1

    :cond_8
    move v0, v2

    .line 2614733
    goto :goto_2

    :cond_9
    move v3, v2

    .line 2614734
    goto :goto_3

    :cond_a
    move v0, v2

    .line 2614735
    goto :goto_4

    :cond_b
    move v3, v2

    .line 2614736
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2614740
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2614737
    sget-boolean v0, LX/Ipr;->a:Z

    .line 2614738
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipr;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2614739
    return-object v0
.end method
