.class public LX/IBy;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IBy;


# direct methods
.method public constructor <init>(LX/01T;LX/0ad;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2547925
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2547926
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2547927
    const-string v0, "target_tab_name"

    sget-object v2, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2547928
    sget-object v2, LX/0ax;->cl:Ljava/lang/String;

    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/347;->C:S

    invoke-interface {p2, v0, v3, v4}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/facebook/base/activity/ReactFragmentActivity;

    :goto_0
    sget-object v3, LX/0cQ;->EVENTS_DASHBOARD_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v2, v0, v3, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2547929
    sget-object v0, LX/0ax;->cn:Ljava/lang/String;

    const-string v2, "section_name"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->EVENTS_DASHBOARD_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v0, v2, v3, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2547930
    sget-object v0, LX/0ax;->cr:Ljava/lang/String;

    const-string v2, "extra_privacy_string"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-virtual {p0, v0, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2547931
    sget-object v0, LX/0ax;->ct:Ljava/lang/String;

    const-string v2, "{events_creation_prefill_extras}"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, LX/IBx;

    invoke-direct {v2}, LX/IBx;-><init>()V

    invoke-virtual {p0, v0, v2}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2547932
    sget-object v0, LX/0ax;->cs:Ljava/lang/String;

    const-string v2, "{events_creation_story_cache_id}"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-virtual {p0, v0, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2547933
    sget-object v0, LX/0ax;->cq:Ljava/lang/String;

    const-class v2, Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-virtual {p0, v0, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2547934
    sget-object v0, LX/0ax;->cv:Ljava/lang/String;

    const-string v2, "extra_page_event_host_id"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-virtual {p0, v0, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2547935
    sget-object v0, LX/0ax;->cx:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->EVENTS_DISCOVERY_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v0, v2, v3}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2547936
    const-string v0, "extras_event_action_context"

    sget-object v2, Lcom/facebook/events/common/EventActionContext;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2547937
    sget-object v0, LX/0ax;->cy:Ljava/lang/String;

    const-string v2, "events_suggestions_cut_type"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->EVENTS_SUGGESTIONS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v0, v2, v3, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2547938
    sget-object v0, LX/0ax;->cw:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->EVENT_CREATE_CATEGORY_SELECTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2547939
    return-void

    .line 2547940
    :cond_0
    const-class v0, Lcom/facebook/base/activity/FragmentChromeActivity;

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/IBy;
    .locals 5

    .prologue
    .line 2547941
    sget-object v0, LX/IBy;->a:LX/IBy;

    if-nez v0, :cond_1

    .line 2547942
    const-class v1, LX/IBy;

    monitor-enter v1

    .line 2547943
    :try_start_0
    sget-object v0, LX/IBy;->a:LX/IBy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2547944
    if-eqz v2, :cond_0

    .line 2547945
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2547946
    new-instance p0, LX/IBy;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/IBy;-><init>(LX/01T;LX/0ad;)V

    .line 2547947
    move-object v0, p0

    .line 2547948
    sput-object v0, LX/IBy;->a:LX/IBy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2547949
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2547950
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2547951
    :cond_1
    sget-object v0, LX/IBy;->a:LX/IBy;

    return-object v0

    .line 2547952
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2547953
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
