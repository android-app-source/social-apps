.class public LX/JFb;
.super LX/JF9;
.source ""

# interfaces
.implements LX/JF1;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, LX/JF9;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    const-string v0, "photo_fife_url"

    invoke-virtual {p0, v0}, LX/4sg;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JFb;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 6

    new-instance v0, LX/JFa;

    iget-object v1, p0, LX/JFb;->c:Ljava/lang/String;

    const-string v2, "photo_max_width"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/JF9;->a(Ljava/lang/String;I)I

    move-result v2

    move v2, v2

    const-string v3, "photo_max_height"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/JF9;->a(Ljava/lang/String;I)I

    move-result v3

    move v3, v3

    const-string v4, "photo_attributions"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, LX/JF9;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    iget v5, p0, LX/4sg;->b:I

    invoke-direct/range {v0 .. v5}, LX/JFa;-><init>(Ljava/lang/String;IILjava/lang/CharSequence;I)V

    return-object v0
.end method
