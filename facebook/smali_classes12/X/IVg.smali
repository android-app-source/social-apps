.class public LX/IVg;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IVh;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IVg",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IVh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582174
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2582175
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/IVg;->b:LX/0Zi;

    .line 2582176
    iput-object p1, p0, LX/IVg;->a:LX/0Ot;

    .line 2582177
    return-void
.end method

.method public static a(LX/0QB;)LX/IVg;
    .locals 4

    .prologue
    .line 2582178
    const-class v1, LX/IVg;

    monitor-enter v1

    .line 2582179
    :try_start_0
    sget-object v0, LX/IVg;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582180
    sput-object v2, LX/IVg;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582181
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582182
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582183
    new-instance v3, LX/IVg;

    const/16 p0, 0x242d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IVg;-><init>(LX/0Ot;)V

    .line 2582184
    move-object v0, v3

    .line 2582185
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582186
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582187
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582188
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2582189
    check-cast p2, LX/IVf;

    .line 2582190
    iget-object v0, p0, LX/IVg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IVh;

    iget-object v1, p2, LX/IVf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/IVf;->b:LX/1Pn;

    .line 2582191
    iget-object v3, v0, LX/IVh;->a:LX/C87;

    invoke-virtual {v3, p1}, LX/C87;->c(LX/1De;)LX/C85;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/C85;->a(LX/1Pn;)LX/C85;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/C85;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C85;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f020a43

    invoke-interface {v3, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    const p0, 0x7f0b124b

    invoke-interface {v3, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const p0, 0x7f0b124a

    invoke-interface {v3, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/16 p0, 0x8

    const/4 p2, 0x1

    invoke-interface {v3, p0, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    .line 2582192
    const p0, 0xebef3cd

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2582193
    invoke-interface {v3, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2582194
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2582195
    invoke-static {}, LX/1dS;->b()V

    .line 2582196
    iget v0, p1, LX/1dQ;->b:I

    .line 2582197
    packed-switch v0, :pswitch_data_0

    .line 2582198
    :goto_0
    return-object v2

    .line 2582199
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2582200
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2582201
    check-cast v1, LX/IVf;

    .line 2582202
    iget-object v3, p0, LX/IVg;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IVh;

    iget-object p1, v1, LX/IVf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582203
    iget-object p2, v3, LX/IVh;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/content/SecureContextHelper;

    iget-object p0, v3, LX/IVh;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1nB;

    .line 2582204
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2582205
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, v1}, LX/1nB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object p0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2582206
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xebef3cd
        :pswitch_0
    .end packed-switch
.end method
