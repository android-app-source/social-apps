.class public LX/HM4;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2456645
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456646
    invoke-virtual {p0}, LX/HM4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2456647
    const v0, 0x7f0b0d5f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/HM4;->a:I

    .line 2456648
    const v0, 0x7f0b0d5e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/HM4;->b:I

    .line 2456649
    const v0, 0x7f0213ab

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456650
    const v0, 0x7f030c73

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456651
    iget v0, p0, LX/HM4;->b:I

    iget v2, p0, LX/HM4;->a:I

    iget v3, p0, LX/HM4;->b:I

    iget v4, p0, LX/HM4;->a:I

    invoke-virtual {p0, v0, v2, v3, v4}, LX/HM4;->setPadding(IIII)V

    .line 2456652
    const v0, 0x7f0d1e92

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HM4;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2456653
    const v0, 0x7f0d1e93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HM4;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2456654
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a00fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2456655
    const v0, 0x7f0b0034

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2456656
    iget v0, p0, LX/HM4;->a:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerPadding(I)V

    .line 2456657
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2456658
    invoke-virtual {p0}, LX/HM4;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2456659
    iget-object v1, p0, LX/HM4;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456660
    sget-object v1, LX/HM3;->a:[I

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2456661
    const-string v0, "This shouldn\'t be hit as the calling part definition ensures it"

    invoke-static {v3, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2456662
    :goto_0
    if-eqz p2, :cond_0

    .line 2456663
    iget-object v0, p0, LX/HM4;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456664
    iget-object v0, p0, LX/HM4;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456665
    :goto_1
    return-void

    .line 2456666
    :pswitch_0
    iget-object v1, p0, LX/HM4;->c:Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f0a053d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    .line 2456667
    :pswitch_1
    iget-object v1, p0, LX/HM4;->c:Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f0a053e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    .line 2456668
    :pswitch_2
    iget-object v1, p0, LX/HM4;->c:Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f0a053f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    .line 2456669
    :cond_0
    iget-object v0, p0, LX/HM4;->d:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2456670
    invoke-virtual {p0, v4}, LX/HM4;->setOrientation(I)V

    .line 2456671
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2456672
    iget-object v0, p0, LX/HM4;->c:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, LX/HM4;->a:I

    iget v2, p0, LX/HM4;->b:I

    iget v3, p0, LX/HM4;->a:I

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2456673
    iget-object v0, p0, LX/HM4;->d:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, LX/HM4;->b:I

    iget v2, p0, LX/HM4;->a:I

    iget v3, p0, LX/HM4;->a:I

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2456674
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onMeasure(II)V

    .line 2456675
    iget-object v0, p0, LX/HM4;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2456676
    :cond_0
    :goto_0
    return-void

    .line 2456677
    :cond_1
    iget-object v0, p0, LX/HM4;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getLineCount()I

    move-result v0

    if-gt v0, v5, :cond_2

    iget-object v0, p0, LX/HM4;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getLineCount()I

    move-result v0

    if-le v0, v5, :cond_0

    .line 2456678
    :cond_2
    invoke-virtual {p0, v5}, LX/HM4;->setOrientation(I)V

    .line 2456679
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2456680
    iget-object v0, p0, LX/HM4;->c:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, LX/HM4;->a:I

    invoke-virtual {v0, v4, v1, v4, v4}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2456681
    iget-object v0, p0, LX/HM4;->d:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, LX/HM4;->a:I

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2456682
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onMeasure(II)V

    goto :goto_0
.end method
