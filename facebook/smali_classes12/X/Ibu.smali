.class public LX/Ibu;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/03V;

.field private final d:LX/IJn;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:LX/IdH;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2594912
    const-class v0, LX/Ibu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ibu;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;LX/IJn;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2594884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2594885
    iput-object p1, p0, LX/Ibu;->b:Landroid/content/Context;

    .line 2594886
    iput-object p2, p0, LX/Ibu;->c:LX/03V;

    .line 2594887
    iput-object p3, p0, LX/Ibu;->d:LX/IJn;

    .line 2594888
    iput-object p4, p0, LX/Ibu;->e:Ljava/util/concurrent/ExecutorService;

    .line 2594889
    return-void
.end method

.method public static a(LX/0QB;)LX/Ibu;
    .locals 1

    .prologue
    .line 2594911
    invoke-static {p0}, LX/Ibu;->b(LX/0QB;)LX/Ibu;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Ibu;
    .locals 5

    .prologue
    .line 2594909
    new-instance v4, LX/Ibu;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/IJn;->a(LX/0QB;)LX/IJn;

    move-result-object v2

    check-cast v2, LX/IJn;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Ibu;-><init>(Landroid/content/Context;LX/03V;LX/IJn;Ljava/util/concurrent/ExecutorService;)V

    .line 2594910
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/location/Address;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2594897
    invoke-virtual {p1}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2594898
    :goto_0
    invoke-virtual {p1}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    .line 2594899
    :goto_1
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2594900
    iget-object v0, p0, LX/Ibu;->b:Landroid/content/Context;

    const v3, 0x7f082d7c

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1, v2}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p1}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2594901
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 2594902
    goto :goto_0

    :cond_1
    move v3, v2

    .line 2594903
    goto :goto_1

    .line 2594904
    :cond_2
    if-eqz v0, :cond_3

    .line 2594905
    invoke-virtual {p1, v2}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2594906
    :cond_3
    if-eqz v3, :cond_4

    .line 2594907
    invoke-virtual {p1}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2594908
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2594894
    iget-object v0, p0, LX/Ibu;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ibu;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Ibu;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2594895
    :cond_0
    :goto_0
    return-void

    .line 2594896
    :cond_1
    iget-object v0, p0, LX/Ibu;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0
.end method

.method public final a(DD)V
    .locals 7

    .prologue
    .line 2594890
    invoke-virtual {p0}, LX/Ibu;->a()V

    .line 2594891
    iget-object v1, p0, LX/Ibu;->d:LX/IJn;

    const/4 v6, 0x1

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, LX/IJn;->a(DDI)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Ibu;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2594892
    iget-object v0, p0, LX/Ibu;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Ibt;

    invoke-direct {v1, p0}, LX/Ibt;-><init>(LX/Ibu;)V

    iget-object v2, p0, LX/Ibu;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2594893
    return-void
.end method
