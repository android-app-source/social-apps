.class public final enum LX/INV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/INV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/INV;

.field public static final enum REGULAR_NEWS_CARD_TYPE:LX/INV;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2570890
    new-instance v0, LX/INV;

    const-string v1, "REGULAR_NEWS_CARD_TYPE"

    invoke-direct {v0, v1, v2}, LX/INV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/INV;->REGULAR_NEWS_CARD_TYPE:LX/INV;

    .line 2570891
    const/4 v0, 0x1

    new-array v0, v0, [LX/INV;

    sget-object v1, LX/INV;->REGULAR_NEWS_CARD_TYPE:LX/INV;

    aput-object v1, v0, v2

    sput-object v0, LX/INV;->$VALUES:[LX/INV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2570892
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/INV;
    .locals 1

    .prologue
    .line 2570893
    const-class v0, LX/INV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/INV;

    return-object v0
.end method

.method public static values()[LX/INV;
    .locals 1

    .prologue
    .line 2570894
    sget-object v0, LX/INV;->$VALUES:[LX/INV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/INV;

    return-object v0
.end method
