.class public LX/I8u;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/I8t;

.field public final b:LX/I8s;

.field public final c:LX/I99;


# direct methods
.method public constructor <init>(LX/I8s;LX/I8t;LX/I99;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2542123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2542124
    iput-object p1, p0, LX/I8u;->b:LX/I8s;

    .line 2542125
    iput-object p2, p0, LX/I8u;->a:LX/I8t;

    .line 2542126
    iput-object p3, p0, LX/I8u;->c:LX/I99;

    .line 2542127
    return-void
.end method

.method public static a(LX/0QB;)LX/I8u;
    .locals 6

    .prologue
    .line 2542128
    new-instance v3, LX/I8u;

    invoke-static {p0}, LX/I8s;->b(LX/0QB;)LX/I8s;

    move-result-object v0

    check-cast v0, LX/I8s;

    .line 2542129
    new-instance v2, LX/I8t;

    invoke-static {p0}, LX/I7W;->a(LX/0QB;)LX/I7W;

    move-result-object v1

    check-cast v1, LX/I7W;

    invoke-direct {v2, v1}, LX/I8t;-><init>(LX/I7W;)V

    .line 2542130
    move-object v1, v2

    .line 2542131
    check-cast v1, LX/I8t;

    .line 2542132
    new-instance v5, LX/I99;

    invoke-static {p0}, LX/I7W;->a(LX/0QB;)LX/I7W;

    move-result-object v2

    check-cast v2, LX/I7W;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v2, v4}, LX/I99;-><init>(LX/I7W;LX/0ad;)V

    .line 2542133
    move-object v2, v5

    .line 2542134
    check-cast v2, LX/I99;

    invoke-direct {v3, v0, v1, v2}, LX/I8u;-><init>(LX/I8s;LX/I8t;LX/I99;)V

    .line 2542135
    move-object v0, v3

    .line 2542136
    return-object v0
.end method
