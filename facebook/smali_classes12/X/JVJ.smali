.class public LX/JVJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JVH;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JVK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2700497
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JVJ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JVK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700484
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2700485
    iput-object p1, p0, LX/JVJ;->b:LX/0Ot;

    .line 2700486
    return-void
.end method

.method public static a(LX/0QB;)LX/JVJ;
    .locals 4

    .prologue
    .line 2700498
    const-class v1, LX/JVJ;

    monitor-enter v1

    .line 2700499
    :try_start_0
    sget-object v0, LX/JVJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700500
    sput-object v2, LX/JVJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700501
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700502
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700503
    new-instance v3, LX/JVJ;

    const/16 p0, 0x2092

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JVJ;-><init>(LX/0Ot;)V

    .line 2700504
    move-object v0, v3

    .line 2700505
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700506
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700507
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2700487
    check-cast p2, LX/JVI;

    .line 2700488
    iget-object v0, p0, LX/JVJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/JVI;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700489
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v1, 0x0

    const p0, 0x7f0e0120

    invoke-static {p1, v1, p0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p0

    .line 2700490
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2700491
    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 p0, 0x6

    const p2, 0x7f0b0917

    invoke-interface {v1, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 p0, 0x7

    const p2, 0x7f0b08fe

    invoke-interface {v1, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v1, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2700492
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const p0, 0x7f020aea

    invoke-virtual {v2, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const p0, 0x7f0b00e3

    invoke-interface {v2, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const p0, 0x7f0b00e9

    invoke-interface {v2, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    const/4 p0, 0x1

    const/16 p2, 0x9

    invoke-interface {v2, p0, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x3

    const/4 p2, 0x7

    invoke-interface {v2, p0, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x0

    const p2, 0x7f0b00eb

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x2

    const p2, 0x7f0b00e5

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    .line 2700493
    const p0, -0x75576001

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2700494
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 2700495
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2700496
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2700475
    invoke-static {}, LX/1dS;->b()V

    .line 2700476
    iget v0, p1, LX/1dQ;->b:I

    .line 2700477
    packed-switch v0, :pswitch_data_0

    .line 2700478
    :goto_0
    return-object v2

    .line 2700479
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2700480
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2700481
    check-cast v1, LX/JVI;

    .line 2700482
    iget-object p1, p0, LX/JVJ;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/JVI;->b:LX/1Pf;

    iget-object p2, v1, LX/JVI;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0, p1, p2}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(Landroid/view/View;LX/1Pk;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2700483
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x75576001
        :pswitch_0
    .end packed-switch
.end method
