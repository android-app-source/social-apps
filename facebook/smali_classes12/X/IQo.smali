.class public LX/IQo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2575723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/IQj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2575724
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2575725
    if-nez p0, :cond_0

    move-object v0, v7

    .line 2575726
    :goto_0
    return-object v0

    .line 2575727
    :cond_0
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 2575728
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v0, 0x0

    move v8, v0

    :goto_1
    if-ge v8, v11, :cond_c

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel$NodesModel;

    .line 2575729
    invoke-virtual {v2}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel$NodesModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2575730
    invoke-virtual {v2}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel$NodesModel;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel$NodesModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel$NodesModel$AttachmentsModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;

    move-result-object v4

    .line 2575731
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_4

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$OriginGroupModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2575732
    const/4 v6, 0x1

    .line 2575733
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->k()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel;

    move-result-object v0

    .line 2575734
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2575735
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel;->a()I

    move-result v6

    .line 2575736
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2575737
    if-eqz v1, :cond_4

    invoke-virtual {v9, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2575738
    invoke-virtual {v9, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2575739
    const/4 v1, 0x0

    .line 2575740
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v12

    const/4 v0, 0x0

    move v3, v0

    :goto_4
    if-ge v3, v12, :cond_d

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;

    .line 2575741
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v13

    if-eqz v13, :cond_9

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;->b()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_9

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel;->j()Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel$SyncObjectModel$SyncedItemsModel$NodesModel$OriginGroupModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2575742
    const/4 v0, 0x1

    .line 2575743
    :goto_5
    if-nez v0, :cond_4

    .line 2575744
    :cond_2
    const/4 v5, 0x0

    .line 2575745
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->hB_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_b

    .line 2575746
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->hB_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2575747
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_3

    .line 2575748
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->hB_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2575749
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->hA_()LX/1vs;

    move-result-object v0

    iget-object v12, v0, LX/1vs;->a:LX/15i;

    iget v13, v0, LX/1vs;->b:I

    .line 2575750
    new-instance v0, LX/IQj;

    invoke-virtual {v2}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v12, v13, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v6}, LX/IQj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2575751
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_c

    .line 2575752
    :cond_4
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_1

    .line 2575753
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->hA_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2575754
    if-nez v0, :cond_6

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2575755
    :cond_7
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/SuggestedProductItemForCrossPostGraphQLModels$SuggestedProductItemForCrossPostModel;->hA_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2575756
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto/16 :goto_3

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 2575757
    :cond_9
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_4

    .line 2575758
    :cond_a
    const/4 v0, 0x0

    goto :goto_6

    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    :cond_c
    move-object v0, v7

    .line 2575759
    goto/16 :goto_0

    :cond_d
    move v0, v1

    goto/16 :goto_5
.end method
