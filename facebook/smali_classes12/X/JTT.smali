.class public LX/JTT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final b:Lcom/facebook/graphql/model/GraphQLNode;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 2

    .prologue
    .line 2696729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696730
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696731
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696732
    iput-object p1, p0, LX/JTT;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2696733
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    iput-object v0, p0, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2696734
    iget-object v0, p0, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x78df566b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2696735
    return-void

    .line 2696736
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
