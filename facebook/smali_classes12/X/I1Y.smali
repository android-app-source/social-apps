.class public LX/I1Y;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/I1b;

.field private final d:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0m9;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2529081
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const v3, 0x7f0d01a7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0d01a3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0d01a5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/I1Y;->a:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/I1b;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529059
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2529060
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I1Y;->e:Ljava/util/List;

    .line 2529061
    iput-object p2, p0, LX/I1Y;->b:Landroid/content/Context;

    .line 2529062
    iput-object p1, p0, LX/I1Y;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529063
    iput-object p3, p0, LX/I1Y;->c:LX/I1b;

    .line 2529064
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 12

    .prologue
    .line 2529065
    const v0, 0x7f0d01a3

    if-ne p2, v0, :cond_0

    .line 2529066
    new-instance v0, LX/I1P;

    new-instance v1, Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/fig/header/FigHeader;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/I1P;-><init>(Lcom/facebook/fig/header/FigHeader;)V

    .line 2529067
    :goto_0
    return-object v0

    .line 2529068
    :cond_0
    const v0, 0x7f0d01a7

    if-ne p2, v0, :cond_1

    .line 2529069
    new-instance v1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2529070
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0168

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2529071
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b15db

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2529072
    new-instance v0, LX/I1X;

    invoke-direct {v0, v1}, LX/I1X;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2529073
    :cond_1
    const v0, 0x7f0d01a5

    if-ne p2, v0, :cond_2

    .line 2529074
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2529075
    const v1, 0x7f030583

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;

    .line 2529076
    iget-object v1, p0, LX/I1Y;->c:LX/I1b;

    iget-object v2, p0, LX/I1Y;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529077
    new-instance v5, LX/I1a;

    const-class v6, Landroid/content/Context;

    invoke-interface {v1, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v1}, LX/I53;->b(LX/0QB;)LX/I53;

    move-result-object v9

    check-cast v9, LX/I53;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v10

    check-cast v10, LX/0wM;

    invoke-static {v1}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v11

    check-cast v11, LX/1nQ;

    move-object v6, v2

    move-object v7, v0

    invoke-direct/range {v5 .. v11}, LX/I1a;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;Landroid/content/Context;LX/I53;LX/0wM;LX/1nQ;)V

    .line 2529078
    move-object v0, v5

    .line 2529079
    goto/16 :goto_0

    .line 2529080
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 2529038
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2529039
    const v1, 0x7f0d01a3

    if-ne v0, v1, :cond_1

    .line 2529040
    check-cast p1, LX/I1P;

    .line 2529041
    iget-object v0, p0, LX/I1Y;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/I1P;->a(Ljava/lang/String;)V

    .line 2529042
    :cond_0
    :goto_0
    return-void

    .line 2529043
    :cond_1
    const v1, 0x7f0d01a5

    if-ne v0, v1, :cond_0

    .line 2529044
    check-cast p1, LX/I1a;

    .line 2529045
    iget-object v0, p0, LX/I1Y;->e:Ljava/util/List;

    iget-object v1, p0, LX/I1Y;->f:LX/0m9;

    const/4 p2, -0x1

    const/4 p0, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2529046
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2529047
    :cond_2
    :goto_1
    goto :goto_0

    .line 2529048
    :cond_3
    iput-object v1, p1, LX/I1a;->t:LX/0m9;

    .line 2529049
    iput-object v0, p1, LX/I1a;->u:Ljava/util/List;

    .line 2529050
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2529051
    iget-object v2, p1, LX/I1a;->l:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    invoke-virtual {v2, v7}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->setVisibility(I)V

    .line 2529052
    iget-object v3, p1, LX/I1a;->l:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    iget-object v2, p1, LX/I1a;->p:LX/0wM;

    const v4, 0x7f0209e9

    invoke-virtual {v2, v4, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v5, v2, p1}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2529053
    :cond_4
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2529054
    iget-object v2, p1, LX/I1a;->m:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    invoke-virtual {v2, v7}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->setVisibility(I)V

    .line 2529055
    iget-object v3, p1, LX/I1a;->m:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    iget-object v2, p1, LX/I1a;->p:LX/0wM;

    const v4, 0x7f0209ec

    iget-object v5, p1, LX/I1a;->o:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a06d2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v5, v2, p1}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2529056
    :cond_5
    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2529057
    iget-object v2, p1, LX/I1a;->n:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    invoke-virtual {v2, v7}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->setVisibility(I)V

    .line 2529058
    iget-object v3, p1, LX/I1a;->n:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    iget-object v2, p1, LX/I1a;->p:LX/0wM;

    const v4, 0x7f0207ac

    invoke-virtual {v2, v4, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v5, v2, p1}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2529032
    if-nez p1, :cond_0

    .line 2529033
    const v0, 0x7f0d01a7

    .line 2529034
    :goto_0
    return v0

    .line 2529035
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 2529036
    const v0, 0x7f0d01a3

    goto :goto_0

    .line 2529037
    :cond_1
    const v0, 0x7f0d01a5

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2529031
    iget-object v0, p0, LX/I1Y;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method
