.class public LX/HQ9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2463064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2463065
    iput-object p1, p0, LX/HQ9;->a:LX/0tX;

    .line 2463066
    iput-object p2, p0, LX/HQ9;->b:LX/1Ck;

    .line 2463067
    return-void
.end method

.method private a(LX/399;JLcom/facebook/graphql/enums/GraphQLPageActionType;LX/HQ4;)V
    .locals 4

    .prologue
    .line 2463068
    iget-object v0, p0, LX/HQ9;->b:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "page_add_tab_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HQ9;->a:LX/0tX;

    invoke-virtual {v2, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/HQ7;

    invoke-direct {v3, p0, p5, p4}, LX/HQ7;-><init>(LX/HQ9;LX/HQ4;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2463069
    return-void
.end method


# virtual methods
.method public final a(JLcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;LX/HBy;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2463056
    new-instance v0, LX/4Hc;

    invoke-direct {v0}, LX/4Hc;-><init>()V

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Hc;->a(Ljava/lang/String;)LX/4Hc;

    move-result-object v0

    .line 2463057
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2463058
    invoke-virtual {v0, p4}, LX/4Hc;->b(Ljava/lang/String;)LX/4Hc;

    .line 2463059
    :cond_0
    new-instance v1, LX/4HX;

    invoke-direct {v1}, LX/4HX;-><init>()V

    const-string v2, "PROFILE_TAB_NAVIGATION"

    invoke-virtual {v1, v2}, LX/4HX;->b(Ljava/lang/String;)LX/4HX;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/4HX;->a(LX/4Hc;)LX/4HX;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4HX;->a(Ljava/lang/String;)LX/4HX;

    move-result-object v0

    .line 2463060
    invoke-static {}, LX/9XZ;->a()LX/9XY;

    move-result-object v1

    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9XY;

    .line 2463061
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    move-object v1, v0

    .line 2463062
    new-instance v5, LX/HQ5;

    invoke-direct {v5, p0, p5}, LX/HQ5;-><init>(LX/HQ9;LX/HBy;)V

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/HQ9;->a(LX/399;JLcom/facebook/graphql/enums/GraphQLPageActionType;LX/HQ4;)V

    .line 2463063
    return-void
.end method

.method public final a(JLcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;LX/HMt;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2463048
    new-instance v0, LX/4Hc;

    invoke-direct {v0}, LX/4Hc;-><init>()V

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Hc;->a(Ljava/lang/String;)LX/4Hc;

    move-result-object v0

    .line 2463049
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2463050
    invoke-virtual {v0, p4}, LX/4Hc;->b(Ljava/lang/String;)LX/4Hc;

    .line 2463051
    :cond_0
    new-instance v1, LX/4HZ;

    invoke-direct {v1}, LX/4HZ;-><init>()V

    const-string v2, "PROFILE_TAB_NAVIGATION"

    invoke-virtual {v1, v2}, LX/4HZ;->b(Ljava/lang/String;)LX/4HZ;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/4HZ;->a(LX/4Hc;)LX/4HZ;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4HZ;->a(Ljava/lang/String;)LX/4HZ;

    move-result-object v0

    .line 2463052
    invoke-static {}, LX/9Xl;->a()LX/9Xk;

    move-result-object v1

    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9Xk;

    .line 2463053
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    move-object v1, v0

    .line 2463054
    new-instance v5, LX/HQ6;

    invoke-direct {v5, p0, p5}, LX/HQ6;-><init>(LX/HQ9;LX/HMt;)V

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/HQ9;->a(LX/399;JLcom/facebook/graphql/enums/GraphQLPageActionType;LX/HQ4;)V

    .line 2463055
    return-void
.end method
