.class public LX/IAt;
.super LX/I9K;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Blc;)V
    .locals 0
    .param p2    # LX/Blc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2546255
    invoke-direct {p0, p1}, LX/I9K;-><init>(Landroid/content/Context;)V

    .line 2546256
    iput-object p2, p0, LX/I9K;->g:LX/Blc;

    .line 2546257
    return-void
.end method


# virtual methods
.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2546258
    invoke-virtual {p0, p1}, LX/I9K;->f(I)I

    move-result v0

    if-ge v0, p2, :cond_0

    .line 2546259
    invoke-virtual {p0, p1, p2}, LX/I9K;->d(II)V

    .line 2546260
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/I9K;->c(II)I

    move-result v0

    .line 2546261
    invoke-static {}, LX/IAr;->values()[LX/IAr;

    move-result-object v1

    aget-object v0, v1, v0

    .line 2546262
    if-nez p4, :cond_3

    .line 2546263
    sget-object v1, LX/IAs;->a:[I

    invoke-virtual {v0}, LX/IAr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2546264
    const/4 v1, 0x0

    :goto_0
    move-object v2, v1

    .line 2546265
    :goto_1
    sget-object v1, LX/IAr;->CHILD:LX/IAr;

    if-ne v0, v1, :cond_2

    .line 2546266
    invoke-virtual {p0, p1, p2}, LX/I9K;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    move-object v1, v2

    .line 2546267
    check-cast v1, LX/IB6;

    .line 2546268
    iget-object v3, p0, LX/I9K;->g:LX/Blc;

    move-object v3, v3

    .line 2546269
    iget-object p0, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object p0, p0

    .line 2546270
    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2546271
    sget-object p0, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    if-ne v3, p0, :cond_1

    .line 2546272
    iget-object p0, v0, Lcom/facebook/events/model/EventUser;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-object p0, p0

    .line 2546273
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->SEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-eq p0, p1, :cond_4

    .line 2546274
    :cond_1
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/events/model/EventUser;->i()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2546275
    iget-boolean p0, v0, Lcom/facebook/events/model/EventUser;->k:Z

    move p0, p0

    .line 2546276
    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2546277
    :cond_2
    return-object v2

    :cond_3
    move-object v2, p4

    goto :goto_1

    .line 2546278
    :pswitch_0
    new-instance v1, LX/IB6;

    .line 2546279
    iget-object v2, p0, LX/I9K;->c:Landroid/content/Context;

    move-object v2, v2

    .line 2546280
    invoke-direct {v1, v2}, LX/IB6;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2546281
    :pswitch_1
    new-instance v1, LX/I9b;

    .line 2546282
    iget-object v2, p0, LX/I9K;->c:Landroid/content/Context;

    move-object v2, v2

    .line 2546283
    invoke-direct {v1, v2}, LX/I9b;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2546284
    :cond_4
    new-instance p0, Landroid/text/SpannableStringBuilder;

    invoke-direct {p0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2546285
    const-string p1, "[checkmark_placeholder]"

    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2546286
    sget-object p1, LX/IB6;->l:LX/IAI;

    const/4 p2, 0x0

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p3

    const/16 p4, 0x11

    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2546287
    iget-object p1, v1, LX/IB6;->k:Landroid/content/res/Resources;

    const p2, 0x7f081f20

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2546288
    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
