.class public LX/IN9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWf;
.implements LX/DSX;


# instance fields
.field public a:LX/EWL;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/INA;",
            "LX/0Pz",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/INA;",
            "Ljava/util/ArrayList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    .line 2570566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2570567
    sget-object v0, LX/EWL;->a:LX/EWL;

    iput-object v0, p0, LX/IN9;->a:LX/EWL;

    .line 2570568
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/IN9;->b:Ljava/util/Map;

    .line 2570569
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/IN9;->c:Ljava/util/Map;

    .line 2570570
    invoke-static {}, LX/INA;->values()[LX/INA;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2570571
    iget-object v4, p0, LX/IN9;->b:Ljava/util/Map;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570572
    iget-object v4, p0, LX/IN9;->c:Ljava/util/Map;

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570573
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2570574
    :cond_0
    return-void
.end method

.method public static a(LX/IN9;LX/0Px;LX/0Px;ZLjava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/INA;",
            ">;",
            "LX/0Px",
            "<",
            "LX/IN5;",
            ">;Z",
            "Ljava/lang/String;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2570575
    if-nez p3, :cond_1

    .line 2570576
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/INA;

    .line 2570577
    iget-object v1, p0, LX/IN9;->b:Ljava/util/Map;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570578
    iget-object v1, p0, LX/IN9;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pz;

    new-instance v4, LX/IN7;

    .line 2570579
    const v5, 0x7f083001

    move v5, v5

    .line 2570580
    invoke-direct {v4, v5}, LX/IN7;-><init>(I)V

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2570581
    iget-object v1, p0, LX/IN9;->c:Ljava/util/Map;

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570582
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2570583
    :cond_0
    iget-object v0, p0, LX/IN9;->b:Ljava/util/Map;

    sget-object v1, LX/INA;->ALL_GROUPS:LX/INA;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    new-instance v1, LX/IN6;

    invoke-direct {v1, p4, p5}, LX/IN6;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2570584
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v5, :cond_4

    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IN5;

    move-object v1, v0

    .line 2570585
    check-cast v1, LX/INB;

    .line 2570586
    invoke-interface {v0}, LX/IN5;->c()LX/IN8;

    move-result-object v0

    sget-object v2, LX/IN8;->GroupRow:LX/IN8;

    if-ne v0, v2, :cond_3

    .line 2570587
    const/4 v2, 0x0

    .line 2570588
    sget-object v0, LX/INA;->ALL_GROUPS:LX/INA;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    move-object v6, v0

    .line 2570589
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v7, :cond_2

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/INA;

    .line 2570590
    iget-object v8, p0, LX/IN9;->c:Ljava/util/Map;

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1}, LX/INB;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2570591
    const/4 v0, 0x1

    .line 2570592
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_2

    .line 2570593
    :cond_2
    if-nez v2, :cond_3

    .line 2570594
    iget-object v0, p0, LX/IN9;->b:Ljava/util/Map;

    .line 2570595
    sget-object v2, LX/INA;->ALL_GROUPS:LX/INA;

    move-object v2, v2

    .line 2570596
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2570597
    iget-object v0, p0, LX/IN9;->c:Ljava/util/Map;

    .line 2570598
    sget-object v2, LX/INA;->ALL_GROUPS:LX/INA;

    move-object v2, v2

    .line 2570599
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1}, LX/INB;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2570600
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 2570601
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2570602
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_6

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/INA;

    .line 2570603
    iget-object v4, p0, LX/IN9;->b:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2570604
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_5

    .line 2570605
    new-instance v4, LX/EWI;

    const/4 v5, 0x1

    invoke-direct {v4, v0, v5}, LX/EWI;-><init>(LX/0Px;Z)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2570606
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2570607
    :cond_6
    new-instance v0, LX/EWL;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EWL;-><init>(LX/0Px;)V

    iput-object v0, p0, LX/IN9;->a:LX/EWL;

    .line 2570608
    return-void

    :cond_7
    move v0, v2

    goto :goto_3
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2570609
    iget-object v0, p0, LX/IN9;->a:LX/EWL;

    .line 2570610
    iget p0, v0, LX/EWL;->d:I

    move v0, p0

    .line 2570611
    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2570612
    iget-object v0, p0, LX/IN9;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2570613
    invoke-static {}, LX/IN8;->values()[LX/IN8;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 2570614
    invoke-virtual {p0, p1}, LX/IN9;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IN5;

    invoke-interface {v0}, LX/IN5;->c()LX/IN8;

    move-result-object v0

    invoke-virtual {v0}, LX/IN8;->ordinal()I

    move-result v0

    return v0
.end method

.method public final c(I)Z
    .locals 1

    .prologue
    .line 2570615
    iget-object v0, p0, LX/IN9;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->c(I)Z

    move-result v0

    return v0
.end method

.method public final getPositionForSection(I)I
    .locals 1

    .prologue
    .line 2570616
    iget-object v0, p0, LX/IN9;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public final getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 2570617
    iget-object v0, p0, LX/IN9;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2570618
    iget-object v0, p0, LX/IN9;->a:LX/EWL;

    invoke-virtual {v0}, LX/EWL;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final v_(I)Z
    .locals 1

    .prologue
    .line 2570619
    iget-object v0, p0, LX/IN9;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->v_(I)Z

    move-result v0

    return v0
.end method
