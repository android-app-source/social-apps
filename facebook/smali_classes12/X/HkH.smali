.class public final LX/HkH;
.super LX/HkG;
.source ""


# instance fields
.field public final synthetic a:LX/HkJ;


# direct methods
.method public constructor <init>(LX/HkJ;)V
    .locals 0

    iput-object p1, p0, LX/HkH;->a:LX/HkJ;

    invoke-direct {p0}, LX/HkG;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Hkb;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/Hkb;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HkH;->a:LX/HkJ;

    iget-object v1, v1, LX/HkJ;->d:LX/Hk0;

    invoke-static {v1}, LX/Hkj;->b(LX/Hk0;)V

    iget-object v1, p0, LX/HkH;->a:LX/HkJ;

    const/4 v2, 0x0

    iput-object v2, v1, LX/HkJ;->e:LX/HkR;

    iget-object v1, p0, LX/HkH;->a:LX/HkJ;

    invoke-static {v1, v0}, LX/HkJ;->a$redex0(LX/HkJ;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 5

    const-class v0, LX/Hka;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, LX/Hka;

    iget-object v0, p0, LX/HkH;->a:LX/HkJ;

    iget-object v0, v0, LX/HkJ;->d:LX/Hk0;

    invoke-static {v0}, LX/Hkj;->b(LX/Hk0;)V

    iget-object v0, p0, LX/HkH;->a:LX/HkJ;

    const/4 v1, 0x0

    iput-object v1, v0, LX/HkJ;->e:LX/HkR;

    :try_start_0
    iget-object v0, p1, LX/Hka;->a:LX/Hkb;

    move-object v0, v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/Hkb;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/HkH;->a:LX/HkJ;

    iget-object v0, v0, LX/HkJ;->b:LX/HkK;

    invoke-virtual {v0, v1}, LX/HkK;->a(Ljava/lang/String;)LX/HkM;

    move-result-object v0

    iget-object v2, v0, LX/HkM;->b:LX/HkL;

    move-object v2, v2

    sget-object v3, LX/HkL;->b:LX/HkL;

    if-ne v2, v3, :cond_2

    check-cast v0, LX/HkO;

    iget-object v2, v0, LX/HkO;->a:Ljava/lang/String;

    move-object v2, v2

    iget v3, v0, LX/HkO;->b:I

    move v0, v3

    sget-object v3, LX/HjN;->ERROR_MESSAGE:LX/HjN;

    invoke-static {v0, v3}, LX/HjN;->adErrorTypeFromCode(ILX/HjN;)LX/HjN;

    move-result-object v3

    iget-object v4, p0, LX/HkH;->a:LX/HkJ;

    if-nez v2, :cond_1

    move-object v0, v1

    :goto_0
    invoke-virtual {v3, v0}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v0

    invoke-static {v4, v0}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, LX/HkH;->a:LX/HkJ;

    new-instance v1, LX/Hjr;

    sget-object v2, LX/HjN;->NETWORK_ERROR:LX/HjN;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/Hjr;-><init>(LX/HjN;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V

    goto :goto_1

    :cond_1
    move-object v0, v2

    goto :goto_0

    :catch_0
    :cond_2
    iget-object v0, p0, LX/HkH;->a:LX/HkJ;

    new-instance v1, LX/Hjr;

    sget-object v2, LX/HjN;->NETWORK_ERROR:LX/HjN;

    invoke-virtual {p1}, LX/Hka;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/Hjr;-><init>(LX/HjN;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V

    goto :goto_1
.end method
