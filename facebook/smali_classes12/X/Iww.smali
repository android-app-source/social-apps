.class public final LX/Iww;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V
    .locals 0

    .prologue
    .line 2631111
    iput-object p1, p0, LX/Iww;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2631112
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2631113
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2631114
    if-nez p1, :cond_1

    .line 2631115
    :cond_0
    :goto_0
    return-void

    .line 2631116
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2631117
    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel;

    .line 2631118
    if-eqz v0, :cond_3

    .line 2631119
    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2631120
    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 2631121
    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2631122
    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method
