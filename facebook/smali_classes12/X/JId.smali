.class public final LX/JId;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JIe;


# direct methods
.method public constructor <init>(LX/JIe;)V
    .locals 0

    .prologue
    .line 2678113
    iput-object p1, p0, LX/JId;->a:LX/JIe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x7cb0479f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2678114
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2678115
    iget-object v0, p0, LX/JId;->a:LX/JIe;

    iget-object v0, v0, LX/JIe;->g:LX/Emj;

    sget-object v1, LX/Emo;->TAG:LX/Emo;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    iget-object v5, p0, LX/JId;->a:LX/JIe;

    iget-object v5, v5, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-interface/range {v0 .. v5}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2678116
    iget-object v0, p0, LX/JId;->a:LX/JIe;

    invoke-virtual {v0, v2}, LX/JIe;->a(Ljava/lang/String;)Z

    move-result v0

    .line 2678117
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 2678118
    if-eqz v0, :cond_0

    .line 2678119
    iget-object v0, p0, LX/JId;->a:LX/JIe;

    .line 2678120
    iget-object v1, v0, LX/JIe;->p:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2678121
    iget-object v1, v0, LX/JIe;->p:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2678122
    :goto_0
    iget-object v0, p0, LX/JId;->a:LX/JIe;

    .line 2678123
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678124
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    iget-object v1, p0, LX/JId;->a:LX/JIe;

    iget-object v1, v1, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->m()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LX/JJF;->a(I)V

    .line 2678125
    const v0, -0x6a064d88

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2678126
    :cond_0
    iget-object v0, p0, LX/JId;->a:LX/JIe;

    .line 2678127
    iget-object v1, v0, LX/JIe;->o:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2678128
    iget-object v1, v0, LX/JIe;->o:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2678129
    :goto_1
    goto :goto_0

    .line 2678130
    :cond_1
    iget-object v1, v0, LX/JIe;->o:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2678131
    :cond_2
    iget-object v1, v0, LX/JIe;->p:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
