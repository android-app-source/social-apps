.class public final LX/Hyf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524384
    iput-object p1, p0, LX/Hyf;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2524385
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2524386
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2524387
    if-eqz p1, :cond_0

    .line 2524388
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524389
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hyf;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    if-nez v0, :cond_1

    .line 2524390
    :cond_0
    :goto_0
    return-void

    .line 2524391
    :cond_1
    iget-object v0, p0, LX/Hyf;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    .line 2524392
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524393
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2524394
    iget-object v2, v1, LX/Hyx;->n:LX/0TD;

    new-instance v3, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;

    invoke-direct {v3, v1, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;-><init>(LX/Hyx;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V

    const v4, -0x17abadc8

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2524395
    iget-object v0, p0, LX/Hyf;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    .line 2524396
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524397
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2524398
    if-eqz v0, :cond_2

    iget-object v2, v1, LX/Hz2;->j:LX/I2Z;

    if-nez v2, :cond_3

    .line 2524399
    :cond_2
    :goto_1
    goto :goto_0

    .line 2524400
    :cond_3
    iget-object v2, v1, LX/Hz2;->j:LX/I2Z;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 2524401
    if-eqz v3, :cond_4

    iget-object v4, v2, LX/I2Z;->i:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 2524402
    :cond_4
    const/4 v4, 0x0

    .line 2524403
    :goto_2
    move-object v2, v4

    .line 2524404
    if-eqz v2, :cond_2

    .line 2524405
    new-instance v3, LX/7vC;

    invoke-direct {v3, v2}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    .line 2524406
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v2

    .line 2524407
    iput-object v2, v3, LX/7vC;->b:Ljava/lang/String;

    .line 2524408
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, LX/5O7;->a(J)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2524409
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v2

    .line 2524410
    iput-object v2, v3, LX/7vC;->I:Ljava/util/Date;

    .line 2524411
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v2

    .line 2524412
    iput-object v2, v3, LX/7vC;->J:Ljava/util/Date;

    .line 2524413
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2524414
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 2524415
    iput-object v2, v3, LX/7vC;->K:Ljava/util/TimeZone;

    .line 2524416
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 2524417
    iput-object v2, v3, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2524418
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    .line 2524419
    iput-object v2, v3, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2524420
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->r()Z

    move-result v2

    .line 2524421
    iput-boolean v2, v3, LX/7vC;->H:Z

    .line 2524422
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    .line 2524423
    if-eqz v2, :cond_6

    .line 2524424
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 2524425
    iput-object v4, v3, LX/7vC;->O:Ljava/lang/String;

    .line 2524426
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 2524427
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2524428
    iput-object v2, v3, LX/7vC;->P:Ljava/lang/String;

    .line 2524429
    :cond_6
    iget-object v2, v1, LX/Hz2;->j:LX/I2Z;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/I2Z;->a(Ljava/lang/String;Lcom/facebook/events/model/Event;)V

    .line 2524430
    invoke-static {v1}, LX/Hz2;->m(LX/Hz2;)V

    goto/16 :goto_1

    :cond_7
    iget-object v5, v2, LX/I2Z;->d:Ljava/util/List;

    iget-object v4, v2, LX/I2Z;->i:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/model/Event;

    goto/16 :goto_2
.end method
