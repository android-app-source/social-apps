.class public final LX/IYX;
.super LX/278;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DeprecatedClass"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

.field private b:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

.field private c:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V
    .locals 0

    .prologue
    .line 2587807
    iput-object p1, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-direct {p0}, LX/278;-><init>()V

    .line 2587808
    iput-object p2, p0, LX/IYX;->b:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 2587809
    iput-object p3, p0, LX/IYX;->c:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2587810
    iput p4, p0, LX/IYX;->d:I

    .line 2587811
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2587812
    if-nez p1, :cond_9

    .line 2587813
    iget-object v0, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    .line 2587814
    iput-boolean v8, v0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->R:Z

    .line 2587815
    iget-object v0, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2587816
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2587817
    :goto_0
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v2, :cond_0

    .line 2587818
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v3, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v3, v3, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->H:LX/0Tf;

    new-instance v4, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity$LoginAppSessionListener$1;

    invoke-direct {v4, p0, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity$LoginAppSessionListener$1;-><init>(LX/IYX;Ljava/lang/String;)V

    const-wide/16 v6, 0x1388

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v6, v7, v5}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v3

    .line 2587819
    iput-object v3, v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2587820
    :cond_0
    iget v2, p0, LX/IYX;->d:I

    if-ne v2, v8, :cond_3

    .line 2587821
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    .line 2587822
    iget-object v3, v2, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    .line 2587823
    sget-object v4, LX/26p;->r:LX/0Tn;

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 2587824
    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2587825
    :cond_1
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v2}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2587826
    :goto_1
    return-void

    .line 2587827
    :cond_2
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2587828
    goto :goto_0

    .line 2587829
    :cond_3
    iget v2, p0, LX/IYX;->d:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2587830
    iget-object v0, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->l()V

    goto :goto_1

    .line 2587831
    :cond_4
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v2}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v2

    invoke-interface {v2}, LX/Gb4;->d()V

    .line 2587832
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->J:LX/0fW;

    invoke-virtual {v2, v0}, LX/0fW;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2587833
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->I:LX/2Di;

    const/4 v4, 0x1

    .line 2587834
    const/4 v3, 0x0

    invoke-static {v2, v4, v4, v3}, LX/2Di;->a(LX/2Di;ZZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2587835
    :cond_5
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v3, "after_login_redirect_to_notifications_extra"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 2587836
    if-eqz v2, :cond_6

    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v2, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->c(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2587837
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->K:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0hM;->P:LX/0Tn;

    invoke-interface {v2, v3, v8}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2587838
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->L:LX/8D3;

    iget-object v3, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v3, v3, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v4, "ndid"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v4, v4, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v5, "landing_experience"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v5, v5, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v6, "logged_in_user_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2587839
    iget-object v6, v2, LX/8D3;->a:LX/0Zb;

    const-string v7, "logged_out_push_converted"

    invoke-static {v7}, LX/8D3;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "uid"

    invoke-virtual {v7, v8, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "ndid"

    invoke-virtual {v7, v8, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "landing_experience"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "logged_in_user_id"

    invoke-virtual {v7, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2587840
    :cond_6
    iget-object v0, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    invoke-virtual {v0}, LX/10M;->l()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2587841
    iget-object v0, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->t(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    goto/16 :goto_1

    .line 2587842
    :cond_7
    iget-object v0, p0, LX/IYX;->b:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    if-nez v0, :cond_8

    const-string v0, ""

    .line 2587843
    :goto_2
    iget-object v2, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    .line 2587844
    invoke-static {v2, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b$redex0(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Ljava/lang/String;)V

    .line 2587845
    iput-object v1, p0, LX/IYX;->b:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 2587846
    iget-object v0, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    goto/16 :goto_1

    .line 2587847
    :cond_8
    iget-object v0, p0, LX/IYX;->b:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 2587848
    iget-object v2, v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->c:Ljava/lang/String;

    move-object v0, v2

    .line 2587849
    goto :goto_2

    .line 2587850
    :cond_9
    iput-object v1, p0, LX/IYX;->b:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 2587851
    iget-object v0, p0, LX/IYX;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    iget-object v1, p0, LX/IYX;->c:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget v2, p0, LX/IYX;->d:I

    .line 2587852
    invoke-static {v0, p1, v1, v2}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Ljava/lang/Throwable;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    .line 2587853
    goto/16 :goto_1
.end method
