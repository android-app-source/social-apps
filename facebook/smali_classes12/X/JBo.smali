.class public final LX/JBo;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2660667
    const-class v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    const v0, 0x1c2b774e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchTimelineCollectionsSectionViewQuery"

    const-string v6, "39d01fda2ab9a22c3075c5a8cfe9ccb4"

    const-string v7, "node"

    const-string v8, "10155207561621729"

    const-string v9, "10155259088426729"

    .line 2660668
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2660669
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2660670
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2660671
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2660672
    sparse-switch v0, :sswitch_data_0

    .line 2660673
    :goto_0
    return-object p1

    .line 2660674
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2660675
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2660676
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2660677
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2660678
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2660679
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2660680
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2660681
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2660682
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2660683
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2660684
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2660685
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2660686
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x592bc66d -> :sswitch_0
        -0x41a91745 -> :sswitch_9
        -0x3b73c98f -> :sswitch_2
        -0x17fb7f63 -> :sswitch_7
        -0x17e5f441 -> :sswitch_3
        -0x14c64f74 -> :sswitch_b
        -0x9ac82a1 -> :sswitch_8
        0x180aba4 -> :sswitch_5
        0x24991595 -> :sswitch_6
        0x291d8de0 -> :sswitch_a
        0x3052e0ff -> :sswitch_1
        0x4b13ca50 -> :sswitch_c
        0x5f424068 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2660687
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2660688
    :goto_1
    return v0

    .line 2660689
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2660690
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
