.class public LX/IDz;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/IDy;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2551372
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2551373
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/DHs;LX/DHr;)LX/IDy;
    .locals 10

    .prologue
    .line 2551374
    new-instance v0, LX/IDy;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    .line 2551375
    new-instance v2, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-direct {v2, v1}, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;-><init>(LX/0tX;)V

    .line 2551376
    move-object v8, v2

    .line 2551377
    check-cast v8, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;

    const-class v1, LX/IE8;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/IE8;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v9}, LX/IDy;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/DHs;LX/DHr;Ljava/util/concurrent/Executor;LX/03V;Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;LX/IE8;)V

    .line 2551378
    return-object v0
.end method
