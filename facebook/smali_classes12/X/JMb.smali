.class public final LX/JMb;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Landroid/graphics/Picture;


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 2684419
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2684420
    new-instance v0, Landroid/graphics/Picture;

    invoke-direct {v0}, Landroid/graphics/Picture;-><init>()V

    iput-object v0, p0, LX/JMb;->a:Landroid/graphics/Picture;

    .line 2684421
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 2684422
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/text/Layout;

    .line 2684423
    :try_start_0
    iget-object v1, p0, LX/JMb;->a:Landroid/graphics/Picture;

    const/4 v2, 0x0

    .line 2684424
    if-nez v0, :cond_1

    .line 2684425
    :cond_0
    move v2, v2

    .line 2684426
    invoke-static {v0}, LX/JMd;->b(Landroid/text/Layout;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Picture;->beginRecording(II)Landroid/graphics/Canvas;

    move-result-object v1

    .line 2684427
    invoke-virtual {v0, v1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 2684428
    iget-object v0, p0, LX/JMb;->a:Landroid/graphics/Picture;

    invoke-virtual {v0}, Landroid/graphics/Picture;->endRecording()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2684429
    :goto_0
    return-void

    :catch_0
    goto :goto_0

    .line 2684430
    :cond_1
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result p1

    move v3, v2

    .line 2684431
    :goto_1
    if-ge v3, p1, :cond_0

    .line 2684432
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineRight(I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 2684433
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1
.end method
