.class public final LX/JNF;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JNG;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/JNG;


# direct methods
.method public constructor <init>(LX/JNG;)V
    .locals 1

    .prologue
    .line 2685464
    iput-object p1, p0, LX/JNF;->d:LX/JNG;

    .line 2685465
    move-object v0, p1

    .line 2685466
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2685467
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2685468
    const-string v0, "EventsSuggestionItemActionViewComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2685469
    if-ne p0, p1, :cond_1

    .line 2685470
    :cond_0
    :goto_0
    return v0

    .line 2685471
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2685472
    goto :goto_0

    .line 2685473
    :cond_3
    check-cast p1, LX/JNF;

    .line 2685474
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2685475
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2685476
    if-eq v2, v3, :cond_0

    .line 2685477
    iget-object v2, p0, LX/JNF;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JNF;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    iget-object v3, p1, LX/JNF;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2685478
    goto :goto_0

    .line 2685479
    :cond_5
    iget-object v2, p1, LX/JNF;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    if-nez v2, :cond_4

    .line 2685480
    :cond_6
    iget-object v2, p0, LX/JNF;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JNF;->b:Ljava/lang/String;

    iget-object v3, p1, LX/JNF;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2685481
    goto :goto_0

    .line 2685482
    :cond_8
    iget-object v2, p1, LX/JNF;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2685483
    :cond_9
    iget-object v2, p0, LX/JNF;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JNF;->c:Ljava/lang/String;

    iget-object v3, p1, LX/JNF;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2685484
    goto :goto_0

    .line 2685485
    :cond_a
    iget-object v2, p1, LX/JNF;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
