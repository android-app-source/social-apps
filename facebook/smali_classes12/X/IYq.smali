.class public LX/IYq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/0tX;

.field public final d:LX/0fz;

.field public final e:LX/1Ck;

.field public final f:LX/0w9;

.field public g:LX/IYm;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0fz;LX/1Ck;LX/0w9;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2588176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2588177
    new-instance v0, LX/IYo;

    invoke-direct {v0, p0}, LX/IYo;-><init>(LX/IYq;)V

    iput-object v0, p0, LX/IYq;->a:LX/0QK;

    .line 2588178
    iput-object p1, p0, LX/IYq;->b:Ljava/util/concurrent/Executor;

    .line 2588179
    iput-object p2, p0, LX/IYq;->c:LX/0tX;

    .line 2588180
    iput-object p3, p0, LX/IYq;->d:LX/0fz;

    .line 2588181
    iput-object p4, p0, LX/IYq;->e:LX/1Ck;

    .line 2588182
    iput-object p5, p0, LX/IYq;->f:LX/0w9;

    .line 2588183
    return-void
.end method

.method public static b(LX/0QB;)LX/IYq;
    .locals 6

    .prologue
    .line 2588184
    new-instance v0, LX/IYq;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0fz;->b(LX/0QB;)LX/0fz;

    move-result-object v3

    check-cast v3, LX/0fz;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/0w9;->a(LX/0QB;)LX/0w9;

    move-result-object v5

    check-cast v5, LX/0w9;

    invoke-direct/range {v0 .. v5}, LX/IYq;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0fz;LX/1Ck;LX/0w9;)V

    .line 2588185
    return-object v0
.end method
