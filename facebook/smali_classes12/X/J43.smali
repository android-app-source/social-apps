.class public final LX/J43;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/J4L;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J45;

.field private b:LX/J6P;


# direct methods
.method public constructor <init>(LX/J45;LX/J6P;)V
    .locals 0

    .prologue
    .line 2643532
    iput-object p1, p0, LX/J43;->a:LX/J45;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2643533
    iput-object p2, p0, LX/J43;->b:LX/J6P;

    .line 2643534
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2643542
    iget-object v0, p0, LX/J43;->a:LX/J45;

    iget-object v0, v0, LX/J45;->h:LX/03V;

    const-string v1, "privacy_checkup_manager_fetch_data_failed"

    const-string v2, "Data fetch failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2643543
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2643535
    check-cast p1, LX/J4L;

    .line 2643536
    iget-object v0, p0, LX/J43;->a:LX/J45;

    .line 2643537
    if-nez p1, :cond_0

    .line 2643538
    :goto_0
    iget-object v0, p0, LX/J43;->b:LX/J6P;

    invoke-virtual {v0}, LX/J6P;->a()V

    .line 2643539
    return-void

    .line 2643540
    :cond_0
    iget-object v1, p1, LX/J4L;->c:LX/J4K;

    invoke-virtual {v0, v1}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v1

    .line 2643541
    invoke-virtual {v1, p1}, LX/J4L;->a(LX/J4L;)V

    goto :goto_0
.end method
