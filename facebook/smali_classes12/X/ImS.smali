.class public LX/ImS;
.super LX/7Gb;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609875
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImS;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Im8;LX/2Sx;LX/7GB;LX/7G3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609873
    invoke-direct {p0, p1, p2, p3, p4}, LX/7Gb;-><init>(LX/7G8;LX/2Sx;LX/7GB;LX/7G3;)V

    .line 2609874
    return-void
.end method

.method public static a(LX/0QB;)LX/ImS;
    .locals 10

    .prologue
    .line 2609844
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609845
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609846
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609847
    if-nez v1, :cond_0

    .line 2609848
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609849
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609850
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609851
    sget-object v1, LX/ImS;->a:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609852
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609853
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609854
    :cond_1
    if-nez v1, :cond_4

    .line 2609855
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609856
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609857
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609858
    new-instance p0, LX/ImS;

    invoke-static {v0}, LX/Im8;->a(LX/0QB;)LX/Im8;

    move-result-object v1

    check-cast v1, LX/Im8;

    invoke-static {v0}, LX/2Sx;->a(LX/0QB;)LX/2Sx;

    move-result-object v7

    check-cast v7, LX/2Sx;

    invoke-static {v0}, LX/7GB;->a(LX/0QB;)LX/7GB;

    move-result-object v8

    check-cast v8, LX/7GB;

    invoke-static {v0}, LX/7G3;->a(LX/0QB;)LX/7G3;

    move-result-object v9

    check-cast v9, LX/7G3;

    invoke-direct {p0, v1, v7, v8, v9}, LX/ImS;-><init>(LX/Im8;LX/2Sx;LX/7GB;LX/7G3;)V

    .line 2609859
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609860
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609861
    if-nez v1, :cond_2

    .line 2609862
    sget-object v0, LX/ImS;->a:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImS;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609863
    :goto_1
    if-eqz v0, :cond_3

    .line 2609864
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609865
    :goto_3
    check-cast v0, LX/ImS;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609866
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609867
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609868
    :catchall_1
    move-exception v0

    .line 2609869
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609870
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609871
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609872
    :cond_2
    :try_start_8
    sget-object v0, LX/ImS;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImS;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
