.class public final LX/HWr;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/4At;

.field public final synthetic b:LX/HMI;

.field public final synthetic c:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/4At;LX/HMI;)V
    .locals 0

    .prologue
    .line 2477118
    iput-object p1, p0, LX/HWr;->c:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iput-object p2, p0, LX/HWr;->a:LX/4At;

    iput-object p3, p0, LX/HWr;->b:LX/HMI;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 2477119
    invoke-super {p0, p1}, LX/2h0;->onCancel(Ljava/util/concurrent/CancellationException;)V

    .line 2477120
    iget-object v0, p0, LX/HWr;->a:LX/4At;

    if-eqz v0, :cond_0

    .line 2477121
    iget-object v0, p0, LX/HWr;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2477122
    :cond_0
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 2477114
    iget-object v0, p0, LX/HWr;->a:LX/4At;

    if-eqz v0, :cond_0

    .line 2477115
    iget-object v0, p0, LX/HWr;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2477116
    :cond_0
    iget-object v0, p0, LX/HWr;->b:LX/HMI;

    invoke-interface {v0, p1}, LX/HMI;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2477117
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2477107
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2477108
    iget-object v0, p0, LX/HWr;->a:LX/4At;

    if-eqz v0, :cond_0

    .line 2477109
    iget-object v0, p0, LX/HWr;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2477110
    :cond_0
    iget-object v0, p0, LX/HWr;->b:LX/HMI;

    invoke-interface {v0}, LX/HMI;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2477111
    iget-object v0, p0, LX/HWr;->c:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->c()V

    .line 2477112
    :cond_1
    iget-object v0, p0, LX/HWr;->b:LX/HMI;

    invoke-interface {v0, p1}, LX/HMI;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 2477113
    return-void
.end method
