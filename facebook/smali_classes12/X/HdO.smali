.class public LX/HdO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HdM;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicSourceHscrollItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2488309
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/HdO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicSourceHscrollItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2488306
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2488307
    iput-object p1, p0, LX/HdO;->b:LX/0Ot;

    .line 2488308
    return-void
.end method

.method public static a(LX/0QB;)LX/HdO;
    .locals 4

    .prologue
    .line 2488310
    const-class v1, LX/HdO;

    monitor-enter v1

    .line 2488311
    :try_start_0
    sget-object v0, LX/HdO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2488312
    sput-object v2, LX/HdO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2488313
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2488314
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2488315
    new-instance v3, LX/HdO;

    const/16 p0, 0x371a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HdO;-><init>(LX/0Ot;)V

    .line 2488316
    move-object v0, v3

    .line 2488317
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2488318
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HdO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2488319
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2488320
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2488288
    check-cast p2, LX/HdN;

    .line 2488289
    iget-object v0, p0, LX/HdO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/sources/TopicSourceHscrollItemComponentSpec;

    iget-object v1, p2, LX/HdN;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    .line 2488290
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2488291
    const v3, 0x7f0b22f7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float v2, v3, v2

    float-to-int v2, v2

    .line 2488292
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    add-int/lit8 v4, v2, 0x3

    invoke-interface {v3, v4}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v3

    const/4 p2, 0x0

    const/4 p0, 0x1

    .line 2488293
    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {p0, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 2488294
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v7

    iget-object v4, v0, Lcom/facebook/topics/sections/sources/TopicSourceHscrollItemComponentSpec;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;->a()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->l()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v4

    sget-object v8, Lcom/facebook/topics/sections/sources/TopicSourceHscrollItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v7, 0x7f021992

    invoke-interface {v4, v7}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    const/16 v7, 0x8

    invoke-interface {v4, v7, v5}, LX/1Di;->e(II)LX/1Di;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v4, v5, p2}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0, p2}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v4

    move-object v2, v4

    .line 2488295
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2488296
    invoke-virtual {v1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;->a()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2488297
    const/4 v5, 0x0

    .line 2488298
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    .line 2488299
    iget-object v4, v0, Lcom/facebook/topics/sections/sources/TopicSourceHscrollItemComponentSpec;->c:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    const v6, 0x7f0207dd

    invoke-virtual {v4, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    const v6, 0x7f0a008d

    invoke-virtual {v4, v6}, LX/2xv;->j(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    move-object v4, v4

    .line 2488300
    invoke-virtual {v3, v4}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f021991

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Di;->c(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x5

    invoke-interface {v3, v4, v5}, LX/1Di;->m(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4, v5}, LX/1Di;->m(II)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 2488301
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2488302
    :cond_0
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2488303
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2488304
    invoke-static {}, LX/1dS;->b()V

    .line 2488305
    const/4 v0, 0x0

    return-object v0
.end method
