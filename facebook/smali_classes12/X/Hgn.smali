.class public final LX/Hgn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 2494532
    const/4 v15, 0x0

    .line 2494533
    const/4 v14, 0x0

    .line 2494534
    const/4 v13, 0x0

    .line 2494535
    const/4 v12, 0x0

    .line 2494536
    const/4 v11, 0x0

    .line 2494537
    const/4 v10, 0x0

    .line 2494538
    const/4 v9, 0x0

    .line 2494539
    const/4 v8, 0x0

    .line 2494540
    const/4 v7, 0x0

    .line 2494541
    const/4 v6, 0x0

    .line 2494542
    const/4 v5, 0x0

    .line 2494543
    const/4 v4, 0x0

    .line 2494544
    const/4 v3, 0x0

    .line 2494545
    const/4 v2, 0x0

    .line 2494546
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 2494547
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2494548
    const/4 v2, 0x0

    .line 2494549
    :goto_0
    return v2

    .line 2494550
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2494551
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_c

    .line 2494552
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 2494553
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2494554
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 2494555
    const-string v17, "group_cover_photo"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 2494556
    invoke-static/range {p0 .. p1}, LX/Hgk;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 2494557
    :cond_2
    const-string v17, "group_purposes"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 2494558
    invoke-static/range {p0 .. p1}, LX/Hgl;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 2494559
    :cond_3
    const-string v17, "has_viewer_favorited"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 2494560
    const/4 v4, 0x1

    .line 2494561
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 2494562
    :cond_4
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 2494563
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 2494564
    :cond_5
    const-string v17, "is_multi_company_group"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 2494565
    const/4 v3, 0x1

    .line 2494566
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 2494567
    :cond_6
    const-string v17, "name"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 2494568
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2494569
    :cond_7
    const-string v17, "profile_picture"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 2494570
    invoke-static/range {p0 .. p1}, LX/Hgm;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2494571
    :cond_8
    const-string v17, "subscribe_status"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 2494572
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto/16 :goto_1

    .line 2494573
    :cond_9
    const-string v17, "unread_count"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 2494574
    const/4 v2, 0x1

    .line 2494575
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto/16 :goto_1

    .line 2494576
    :cond_a
    const-string v17, "url"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 2494577
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 2494578
    :cond_b
    const-string v17, "viewer_join_state"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 2494579
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto/16 :goto_1

    .line 2494580
    :cond_c
    const/16 v16, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2494581
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 2494582
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 2494583
    if-eqz v4, :cond_d

    .line 2494584
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->a(IZ)V

    .line 2494585
    :cond_d
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 2494586
    if-eqz v3, :cond_e

    .line 2494587
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 2494588
    :cond_e
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2494589
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2494590
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2494591
    if-eqz v2, :cond_f

    .line 2494592
    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v3}, LX/186;->a(III)V

    .line 2494593
    :cond_f
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2494594
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2494595
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x7

    const/4 v2, 0x0

    .line 2494596
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2494597
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2494598
    if-eqz v0, :cond_3

    .line 2494599
    const-string v1, "group_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494600
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2494601
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2494602
    if-eqz v1, :cond_2

    .line 2494603
    const-string v5, "photo"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494604
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2494605
    const/4 v5, 0x0

    invoke-virtual {p0, v1, v5}, LX/15i;->g(II)I

    move-result v5

    .line 2494606
    if-eqz v5, :cond_1

    .line 2494607
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494608
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2494609
    const/4 v0, 0x0

    invoke-virtual {p0, v5, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2494610
    if-eqz v0, :cond_0

    .line 2494611
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494612
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2494613
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2494614
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2494615
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2494616
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2494617
    if-eqz v0, :cond_7

    .line 2494618
    const-string v1, "group_purposes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494619
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2494620
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2494621
    if-eqz v1, :cond_6

    .line 2494622
    const-string v5, "edges"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494623
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2494624
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v6

    if-ge v5, v6, :cond_5

    .line 2494625
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result v6

    .line 2494626
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2494627
    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 2494628
    if-eqz v7, :cond_4

    .line 2494629
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494630
    invoke-static {p0, v7, p2, p3}, LX/9TZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2494631
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2494632
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2494633
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2494634
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2494635
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2494636
    if-eqz v0, :cond_8

    .line 2494637
    const-string v1, "has_viewer_favorited"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494638
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2494639
    :cond_8
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2494640
    if-eqz v0, :cond_9

    .line 2494641
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494642
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2494643
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2494644
    if-eqz v0, :cond_a

    .line 2494645
    const-string v1, "is_multi_company_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494646
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2494647
    :cond_a
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2494648
    if-eqz v0, :cond_b

    .line 2494649
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494650
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2494651
    :cond_b
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2494652
    if-eqz v0, :cond_d

    .line 2494653
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494654
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2494655
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2494656
    if-eqz v1, :cond_c

    .line 2494657
    const-string v5, "uri"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494658
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2494659
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2494660
    :cond_d
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2494661
    if-eqz v0, :cond_e

    .line 2494662
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494663
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2494664
    :cond_e
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2494665
    if-eqz v0, :cond_f

    .line 2494666
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494667
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2494668
    :cond_f
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2494669
    if-eqz v0, :cond_10

    .line 2494670
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494671
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2494672
    :cond_10
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2494673
    if-eqz v0, :cond_11

    .line 2494674
    const-string v0, "viewer_join_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2494675
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2494676
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2494677
    return-void
.end method
