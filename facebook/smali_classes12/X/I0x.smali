.class public final LX/I0x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final synthetic b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 0

    .prologue
    .line 2528242
    iput-object p1, p0, LX/I0x;->b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iput-object p2, p0, LX/I0x;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x6f803541

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2528243
    iget-object v1, p0, LX/I0x;->b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iget-object v2, p0, LX/I0x;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-static {v1, v2}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->setPublicRsvpButtonColor(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 2528244
    iget-object v1, p0, LX/I0x;->b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a:LX/I7w;

    iget-object v2, p0, LX/I0x;->b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iget-object v2, v2, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->k:LX/7oa;

    iget-object v3, p0, LX/I0x;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_CALENDAR_TAB_INVITATION:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v1, v2, v3, v4}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2528245
    const v1, -0x265142a

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
