.class public final LX/IDp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public final synthetic d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic e:LX/IDs;


# direct methods
.method public constructor <init>(LX/IDs;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 0

    .prologue
    .line 2551193
    iput-object p1, p0, LX/IDp;->e:LX/IDs;

    iput-wide p2, p0, LX/IDp;->a:J

    iput-object p4, p0, LX/IDp;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object p5, p0, LX/IDp;->c:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object p6, p0, LX/IDp;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    .line 2551194
    iget-object v1, p0, LX/IDp;->e:LX/IDs;

    iget-wide v2, p0, LX/IDp;->a:J

    iget-object v4, p0, LX/IDp;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v5, p0, LX/IDp;->c:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v6, 0x0

    .line 2551195
    invoke-static/range {v1 .. v6}, LX/IDs;->b$redex0(LX/IDs;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    .line 2551196
    iget-object v0, p0, LX/IDp;->e:LX/IDs;

    .line 2551197
    invoke-virtual {v0, p1}, LX/2hY;->a(Ljava/lang/Throwable;)V

    .line 2551198
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2551199
    iget-object v1, p0, LX/IDp;->e:LX/IDs;

    iget-wide v2, p0, LX/IDp;->a:J

    iget-object v4, p0, LX/IDp;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v5, p0, LX/IDp;->c:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v6, 0x0

    .line 2551200
    invoke-static/range {v1 .. v6}, LX/IDs;->b$redex0(LX/IDs;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    .line 2551201
    return-void
.end method
