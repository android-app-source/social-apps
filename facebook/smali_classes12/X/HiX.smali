.class public LX/HiX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/addresstypeahead/helper/AddressTypeAheadResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/HiJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V
    .locals 2
    .param p2    # Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/addresstypeahead/helper/AddressTypeAheadResultHandler;",
            ">;",
            "Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2497456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497457
    iput-object p1, p0, LX/HiX;->b:Ljava/util/Set;

    .line 2497458
    iput-object p2, p0, LX/HiX;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    .line 2497459
    iget-object v0, p0, LX/HiX;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    new-instance v1, LX/HiW;

    invoke-direct {v1, p0}, LX/HiW;-><init>(LX/HiX;)V

    .line 2497460
    iput-object v1, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->w:LX/HiW;

    .line 2497461
    return-void
.end method
