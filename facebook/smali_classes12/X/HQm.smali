.class public final enum LX/HQm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HQm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HQm;

.field public static final enum HIDDEN:LX/HQm;

.field public static final enum OPEN_HOURS_ONLY:LX/HQm;

.field public static final enum RATING_AND_OPEN_HOURS:LX/HQm;

.field public static final enum RATING_ONLY:LX/HQm;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2463979
    new-instance v0, LX/HQm;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v2}, LX/HQm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HQm;->HIDDEN:LX/HQm;

    .line 2463980
    new-instance v0, LX/HQm;

    const-string v1, "RATING_ONLY"

    invoke-direct {v0, v1, v3}, LX/HQm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HQm;->RATING_ONLY:LX/HQm;

    .line 2463981
    new-instance v0, LX/HQm;

    const-string v1, "OPEN_HOURS_ONLY"

    invoke-direct {v0, v1, v4}, LX/HQm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HQm;->OPEN_HOURS_ONLY:LX/HQm;

    .line 2463982
    new-instance v0, LX/HQm;

    const-string v1, "RATING_AND_OPEN_HOURS"

    invoke-direct {v0, v1, v5}, LX/HQm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HQm;->RATING_AND_OPEN_HOURS:LX/HQm;

    .line 2463983
    const/4 v0, 0x4

    new-array v0, v0, [LX/HQm;

    sget-object v1, LX/HQm;->HIDDEN:LX/HQm;

    aput-object v1, v0, v2

    sget-object v1, LX/HQm;->RATING_ONLY:LX/HQm;

    aput-object v1, v0, v3

    sget-object v1, LX/HQm;->OPEN_HOURS_ONLY:LX/HQm;

    aput-object v1, v0, v4

    sget-object v1, LX/HQm;->RATING_AND_OPEN_HOURS:LX/HQm;

    aput-object v1, v0, v5

    sput-object v0, LX/HQm;->$VALUES:[LX/HQm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2463978
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HQm;
    .locals 1

    .prologue
    .line 2463985
    const-class v0, LX/HQm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HQm;

    return-object v0
.end method

.method public static values()[LX/HQm;
    .locals 1

    .prologue
    .line 2463984
    sget-object v0, LX/HQm;->$VALUES:[LX/HQm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HQm;

    return-object v0
.end method
