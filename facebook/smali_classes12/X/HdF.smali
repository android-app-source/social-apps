.class public final LX/HdF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2488111
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2488112
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2488113
    :goto_0
    return v1

    .line 2488114
    :cond_0
    const-string v7, "topic_does_viewer_follow"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2488115
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 2488116
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 2488117
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2488118
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2488119
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 2488120
    const-string v7, "__type__"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "__typename"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2488121
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    goto :goto_1

    .line 2488122
    :cond_3
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2488123
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2488124
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2488125
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2488126
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2488127
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2488128
    if-eqz v0, :cond_6

    .line 2488129
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2488130
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2488131
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2488132
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2488133
    if-eqz v0, :cond_0

    .line 2488134
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2488135
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2488136
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2488137
    if-eqz v0, :cond_1

    .line 2488138
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2488139
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2488140
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2488141
    if-eqz v0, :cond_2

    .line 2488142
    const-string v1, "topic_does_viewer_follow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2488143
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2488144
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2488145
    return-void
.end method
