.class public final enum LX/JTb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JTb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JTb;

.field public static final enum album:LX/JTb;

.field public static final enum playlist:LX/JTb;

.field public static final enum single:LX/JTb;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2696856
    new-instance v0, LX/JTb;

    const-string v1, "album"

    invoke-direct {v0, v1, v2}, LX/JTb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTb;->album:LX/JTb;

    .line 2696857
    new-instance v0, LX/JTb;

    const-string v1, "playlist"

    invoke-direct {v0, v1, v3}, LX/JTb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTb;->playlist:LX/JTb;

    .line 2696858
    new-instance v0, LX/JTb;

    const-string v1, "single"

    invoke-direct {v0, v1, v4}, LX/JTb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTb;->single:LX/JTb;

    .line 2696859
    const/4 v0, 0x3

    new-array v0, v0, [LX/JTb;

    sget-object v1, LX/JTb;->album:LX/JTb;

    aput-object v1, v0, v2

    sget-object v1, LX/JTb;->playlist:LX/JTb;

    aput-object v1, v0, v3

    sget-object v1, LX/JTb;->single:LX/JTb;

    aput-object v1, v0, v4

    sput-object v0, LX/JTb;->$VALUES:[LX/JTb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2696855
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JTb;
    .locals 1

    .prologue
    .line 2696861
    const-class v0, LX/JTb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JTb;

    return-object v0
.end method

.method public static values()[LX/JTb;
    .locals 1

    .prologue
    .line 2696860
    sget-object v0, LX/JTb;->$VALUES:[LX/JTb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JTb;

    return-object v0
.end method
