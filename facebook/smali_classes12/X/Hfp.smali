.class public final LX/Hfp;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Hfr;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 2492383
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2492384
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "userGroupConnectionNodeModel"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "workGroupsTabListItemEventsHandler"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "callerContext"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "index"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Hfp;->b:[Ljava/lang/String;

    .line 2492385
    iput v3, p0, LX/Hfp;->c:I

    .line 2492386
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Hfp;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Hfp;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Hfp;LX/1De;IILcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;)V
    .locals 1

    .prologue
    .line 2492379
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2492380
    iput-object p4, p0, LX/Hfp;->a:Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492381
    iget-object v0, p0, LX/Hfp;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2492382
    return-void
.end method


# virtual methods
.method public final a(LX/Hfh;)LX/Hfp;
    .locals 2

    .prologue
    .line 2492376
    iget-object v0, p0, LX/Hfp;->a:Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    iput-object p1, v0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->b:LX/Hfh;

    .line 2492377
    iget-object v0, p0, LX/Hfp;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2492378
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/Hfp;
    .locals 2

    .prologue
    .line 2492373
    iget-object v0, p0, LX/Hfp;->a:Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    iput-object p1, v0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2492374
    iget-object v0, p0, LX/Hfp;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2492375
    return-object p0
.end method

.method public final a(Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)LX/Hfp;
    .locals 2

    .prologue
    .line 2492370
    iget-object v0, p0, LX/Hfp;->a:Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    iput-object p1, v0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    .line 2492371
    iget-object v0, p0, LX/Hfp;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2492372
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2492366
    invoke-super {p0}, LX/1X5;->a()V

    .line 2492367
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hfp;->a:Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492368
    sget-object v0, LX/Hfr;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2492369
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Hfr;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2492356
    iget-object v1, p0, LX/Hfp;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Hfp;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Hfp;->c:I

    if-ge v1, v2, :cond_2

    .line 2492357
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2492358
    :goto_0
    iget v2, p0, LX/Hfp;->c:I

    if-ge v0, v2, :cond_1

    .line 2492359
    iget-object v2, p0, LX/Hfp;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2492360
    iget-object v2, p0, LX/Hfp;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2492361
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2492362
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2492363
    :cond_2
    iget-object v0, p0, LX/Hfp;->a:Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492364
    invoke-virtual {p0}, LX/Hfp;->a()V

    .line 2492365
    return-object v0
.end method

.method public final h(I)LX/Hfp;
    .locals 2

    .prologue
    .line 2492353
    iget-object v0, p0, LX/Hfp;->a:Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    iput p1, v0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->e:I

    .line 2492354
    iget-object v0, p0, LX/Hfp;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2492355
    return-object p0
.end method
