.class public LX/JIe;
.super LX/Emg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Emg",
        "<",
        "LX/JJF;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/03V;

.field private final e:LX/JIY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JIY",
            "<",
            "LX/JJq;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/Eny;

.field public final g:LX/Emj;

.field public final h:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

.field private final i:LX/JIN;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

.field public m:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Landroid/view/View$OnClickListener;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0Or;LX/0Or;LX/03V;LX/JJt;LX/0Or;LX/JIO;LX/0Or;Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/Eny;LX/Emj;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/Eny;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/JJt;",
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;",
            ">;",
            "LX/JIO;",
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;",
            ">;",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;",
            "Lcom/facebook/entitycards/model/EntityCardMutationService;",
            "LX/Emj;",
            "Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2678132
    invoke-direct {p0}, LX/Emg;-><init>()V

    .line 2678133
    iput-object p1, p0, LX/JIe;->a:Ljava/util/concurrent/Executor;

    .line 2678134
    iput-object p2, p0, LX/JIe;->b:LX/0Or;

    .line 2678135
    iput-object p3, p0, LX/JIe;->c:LX/0Or;

    .line 2678136
    iput-object p4, p0, LX/JIe;->d:LX/03V;

    .line 2678137
    new-instance v0, LX/JIZ;

    invoke-direct {v0, p0}, LX/JIZ;-><init>(LX/JIe;)V

    move-object v0, v0

    .line 2678138
    iput-object v0, p0, LX/JIe;->e:LX/JIY;

    .line 2678139
    iget-object v0, p0, LX/JIe;->e:LX/JIY;

    invoke-virtual {p5, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2678140
    iput-object p10, p0, LX/JIe;->f:LX/Eny;

    .line 2678141
    iput-object p11, p0, LX/JIe;->g:LX/Emj;

    .line 2678142
    iput-object p12, p0, LX/JIe;->h:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2678143
    invoke-virtual {p7, p12}, LX/JIO;->a(Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)LX/JIN;

    move-result-object v0

    iput-object v0, p0, LX/JIe;->i:LX/JIN;

    .line 2678144
    iput-object p6, p0, LX/JIe;->j:LX/0Or;

    .line 2678145
    iput-object p8, p0, LX/JIe;->k:LX/0Or;

    .line 2678146
    iput-object p9, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JIe;->o:Ljava/util/List;

    .line 2678148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JIe;->p:Ljava/util/List;

    .line 2678149
    return-void
.end method

.method private static a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;)Z
    .locals 2

    .prologue
    .line 2678258
    const-string v0, "VISIBLE"

    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(LX/JIe;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2678245
    invoke-virtual {p0}, LX/Eme;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    .line 2678246
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;

    .line 2678247
    new-instance v3, LX/JJE;

    invoke-virtual {v0}, LX/JJF;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/JJE;-><init>(Landroid/content/Context;)V

    .line 2678248
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/JJE;->setTag(Ljava/lang/Object;)V

    .line 2678249
    invoke-static {v1}, LX/JIe;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/JJE;->setSelected(Z)V

    .line 2678250
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/JJE;->setText(Ljava/lang/CharSequence;)V

    .line 2678251
    iget-object v1, p0, LX/JIe;->n:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_0

    .line 2678252
    new-instance v1, LX/JId;

    invoke-direct {v1, p0}, LX/JId;-><init>(LX/JIe;)V

    iput-object v1, p0, LX/JIe;->n:Landroid/view/View$OnClickListener;

    .line 2678253
    :cond_0
    iget-object v1, p0, LX/JIe;->n:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 2678254
    invoke-virtual {v3, v1}, LX/JJE;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2678255
    iget-object v1, v0, LX/JJF;->i:Lcom/facebook/widget/FlowLayout;

    iget-object v4, v0, LX/JJF;->i:Lcom/facebook/widget/FlowLayout;

    invoke-virtual {v4}, Lcom/facebook/widget/FlowLayout;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v3, v4}, Lcom/facebook/widget/FlowLayout;->addView(Landroid/view/View;I)V

    .line 2678256
    goto :goto_0

    .line 2678257
    :cond_1
    return-void
.end method

.method private static b(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;)Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;
    .locals 10

    .prologue
    .line 2678220
    new-instance v0, LX/FTC;

    invoke-direct {v0}, LX/FTC;-><init>()V

    .line 2678221
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/FTC;->a:Ljava/lang/String;

    .line 2678222
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/FTC;->b:Ljava/lang/String;

    .line 2678223
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/FTC;->c:Ljava/lang/String;

    .line 2678224
    move-object v1, v0

    .line 2678225
    invoke-static {p0}, LX/JIe;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HIDDEN"

    .line 2678226
    :goto_0
    iput-object v0, v1, LX/FTC;->c:Ljava/lang/String;

    .line 2678227
    move-object v0, v1

    .line 2678228
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 2678229
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2678230
    iget-object v3, v0, LX/FTC;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2678231
    iget-object v5, v0, LX/FTC;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2678232
    iget-object v7, v0, LX/FTC;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2678233
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 2678234
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 2678235
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 2678236
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 2678237
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2678238
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2678239
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2678240
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2678241
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2678242
    new-instance v3, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;

    invoke-direct {v3, v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;-><init>(LX/15i;)V

    .line 2678243
    move-object v0, v3

    .line 2678244
    return-object v0

    :cond_0
    const-string v0, "VISIBLE"

    goto :goto_0
.end method

.method public static e(LX/JIe;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2678216
    iget-object v0, p0, LX/JIe;->i:LX/JIN;

    iget-object v1, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/JIe;->o:Ljava/util/List;

    iget-object v3, p0, LX/JIe;->p:Ljava/util/List;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/JIN;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2678217
    iget-object v1, p0, LX/JIe;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2678218
    iget-object v1, p0, LX/JIe;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2678219
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 13
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 2678179
    const/4 v1, 0x0

    .line 2678180
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2678181
    iget-object v0, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;

    .line 2678182
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2678183
    invoke-static {v0}, LX/JIe;->b(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;)Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;

    move-result-object v0

    .line 2678184
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2678185
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2678186
    :cond_0
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v0, v1

    goto :goto_1

    .line 2678187
    :cond_1
    invoke-static {v1}, LX/JIe;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel$NodesModel;)Z

    move-result v1

    .line 2678188
    iget-object v0, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-static {v0}, LX/FT2;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)LX/FT2;

    move-result-object v0

    new-instance v2, LX/FTB;

    invoke-direct {v2}, LX/FTB;-><init>()V

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2678189
    iput-object v3, v2, LX/FTB;->a:LX/0Px;

    .line 2678190
    move-object v2, v2

    .line 2678191
    iget-object v3, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    .line 2678192
    iput-object v3, v2, LX/FTB;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 2678193
    move-object v2, v2

    .line 2678194
    invoke-virtual {v2}, LX/FTB;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v2

    .line 2678195
    iput-object v2, v0, LX/FT2;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    .line 2678196
    move-object v2, v0

    .line 2678197
    new-instance v3, LX/FTA;

    invoke-direct {v3}, LX/FTA;-><init>()V

    iget-object v0, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->m()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;->a()I

    move-result v4

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :goto_2
    add-int/2addr v0, v4

    .line 2678198
    iput v0, v3, LX/FTA;->a:I

    .line 2678199
    move-object v0, v3

    .line 2678200
    const/4 v11, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 2678201
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 2678202
    invoke-virtual {v7, v11}, LX/186;->c(I)V

    .line 2678203
    iget v8, v0, LX/FTA;->a:I

    invoke-virtual {v7, v10, v8, v10}, LX/186;->a(III)V

    .line 2678204
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 2678205
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 2678206
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 2678207
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2678208
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2678209
    new-instance v8, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    invoke-direct {v8, v7}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;-><init>(LX/15i;)V

    .line 2678210
    move-object v0, v8

    .line 2678211
    iput-object v0, v2, LX/FT2;->f:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    .line 2678212
    move-object v0, v2

    .line 2678213
    invoke-virtual {v0}, LX/FT2;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v0

    iput-object v0, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678214
    return v1

    .line 2678215
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2678159
    iget-object v0, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    if-nez v0, :cond_1

    .line 2678160
    :cond_0
    :goto_0
    return-void

    .line 2678161
    :cond_1
    invoke-virtual {p0}, LX/Eme;->a()LX/0am;

    move-result-object v0

    .line 2678162
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2678163
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    .line 2678164
    iget-object v1, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->o()Ljava/lang/String;

    move-result-object v1

    .line 2678165
    iget-object v2, v0, LX/JJF;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2678166
    new-instance v1, LX/JIU;

    invoke-direct {v1, p0}, LX/JIU;-><init>(LX/JIe;)V

    .line 2678167
    iget-object v2, v0, LX/JJF;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2678168
    iget-object v1, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->n()Ljava/lang/String;

    move-result-object v1

    .line 2678169
    iget-object v2, v0, LX/JJF;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2678170
    iget-object v1, v0, LX/JJF;->i:Lcom/facebook/widget/FlowLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/FlowLayout;->removeAllViews()V

    .line 2678171
    iget-object v1, v0, LX/JJF;->i:Lcom/facebook/widget/FlowLayout;

    iget-object v2, v0, LX/JJF;->j:LX/JJE;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FlowLayout;->addView(Landroid/view/View;)V

    .line 2678172
    iget-object v1, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    check-cast v1, Ljava/util/List;

    invoke-static {p0, v1}, LX/JIe;->a$redex0(LX/JIe;Ljava/util/List;)V

    .line 2678173
    new-instance v1, LX/JIV;

    invoke-direct {v1, p0}, LX/JIV;-><init>(LX/JIe;)V

    .line 2678174
    iget-object v2, v0, LX/JJF;->j:LX/JJE;

    invoke-virtual {v2, v1}, LX/JJE;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2678175
    new-instance v1, LX/JIW;

    invoke-direct {v1, p0}, LX/JIW;-><init>(LX/JIe;)V

    .line 2678176
    iget-object v2, v0, LX/JJF;->m:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2678177
    iget-object v1, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->m()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LX/JJF;->a(I)V

    goto :goto_0

    .line 2678178
    :cond_2
    iget-object v1, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v1

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2678153
    check-cast p1, LX/JJF;

    .line 2678154
    iget-object v0, p0, LX/JIe;->m:LX/0Vd;

    if-eqz v0, :cond_0

    .line 2678155
    iget-object v0, p0, LX/JIe;->m:LX/0Vd;

    invoke-virtual {v0}, LX/0Vd;->dispose()V

    .line 2678156
    :cond_0
    invoke-static {p0}, LX/JIe;->e(LX/JIe;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2678157
    invoke-super {p0, p1}, LX/Emg;->b(Ljava/lang/Object;)V

    .line 2678158
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2678150
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iput-object p1, p0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678151
    invoke-virtual {p0}, LX/JIe;->b()V

    .line 2678152
    return-void
.end method
