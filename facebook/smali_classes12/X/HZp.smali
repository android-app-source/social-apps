.class public final LX/HZp;
.super LX/7hR;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationSuccessFragment;)V
    .locals 0

    .prologue
    .line 2482639
    iput-object p1, p0, LX/HZp;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-direct {p0}, LX/7hR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2482631
    iget-object v0, p0, LX/HZp;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->g:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 2482632
    iget-object v0, p0, LX/HZp;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->g:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 2482633
    :cond_0
    return-void
.end method

.method public final a(LX/8YK;)V
    .locals 2

    .prologue
    .line 2482634
    invoke-virtual {p1}, LX/8YK;->b()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2482635
    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    .line 2482636
    iget-object v1, p0, LX/HZp;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v1, v1, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleX(F)V

    .line 2482637
    iget-object v1, p0, LX/HZp;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v1, v1, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleY(F)V

    .line 2482638
    return-void
.end method
