.class public LX/J22;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/5fv;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/5fv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2639735
    iput-object p1, p0, LX/J22;->a:Landroid/content/res/Resources;

    .line 2639736
    iput-object p2, p0, LX/J22;->b:LX/5fv;

    .line 2639737
    return-void
.end method

.method public static a(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;LX/0Px;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesInterfaces$Action;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/J2n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2639724
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2639725
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;

    .line 2639726
    const/4 v1, 0x0

    .line 2639727
    invoke-virtual {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->b()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2639728
    new-instance v1, LX/J2g;

    invoke-virtual {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->b()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2639729
    iget-object v6, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->b:LX/6xh;

    move-object v6, v6

    .line 2639730
    invoke-direct {v1, v5, v6}, LX/J2g;-><init>(Ljava/lang/String;LX/6xh;)V

    .line 2639731
    :cond_0
    new-instance v5, LX/J2n;

    invoke-virtual {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->a()Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->c()Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    move-result-object v0

    invoke-direct {v5, v6, v7, v0, v1}, LX/J2n;-><init>(Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;LX/J2g;)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639732
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2639733
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/J2w;)LX/J20;
    .locals 1

    .prologue
    .line 2639738
    new-instance v0, LX/J21;

    invoke-direct {v0, p0}, LX/J21;-><init>(LX/J2w;)V

    return-object v0
.end method

.method public static a(LX/3Ab;LX/0Pz;)V
    .locals 1
    .param p0    # LX/3Ab;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Ab;",
            "LX/0Pz",
            "<",
            "LX/J20;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2639721
    if-eqz p0, :cond_0

    .line 2639722
    new-instance v0, LX/J2c;

    invoke-direct {v0, p0}, LX/J2c;-><init>(LX/3Ab;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639723
    :cond_0
    return-void
.end method

.method public static a(LX/J22;Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;LX/J2q;)V
    .locals 11

    .prologue
    .line 2639678
    invoke-virtual {p2}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2639679
    :goto_1
    return-void

    .line 2639680
    :sswitch_0
    const-string v2, "PaymentPriceListViewExtension"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "PaymentProgressViewExtension"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "PaymentOrderSummaryViewExtension"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "PaymentActivityImageViewExtension"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 2639681
    :pswitch_0
    invoke-virtual {p2}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    .line 2639682
    invoke-interface {p2}, LX/J2J;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2639683
    :goto_2
    goto :goto_1

    .line 2639684
    :pswitch_1
    invoke-virtual {p2}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->a()LX/0Px;

    move-result-object v0

    .line 2639685
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2639686
    invoke-interface {p2}, LX/J2K;->mL_()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;

    .line 2639687
    new-instance v6, LX/J2l;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v7, v1}, LX/J2l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639688
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2639689
    :cond_1
    new-instance v1, LX/J2m;

    new-instance v2, LX/J2l;

    invoke-interface {p2}, LX/J2K;->mM_()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2}, LX/J2K;->mM_()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, LX/J2l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-static {p1, v0}, LX/J22;->a(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;LX/0Px;)LX/0Px;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/J2m;-><init>(LX/J2l;LX/0Px;LX/0Px;)V

    .line 2639690
    iput-object v1, p3, LX/J2q;->c:LX/J2e;

    .line 2639691
    goto/16 :goto_1

    .line 2639692
    :pswitch_2
    invoke-virtual {p2}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->a()LX/0Px;

    move-result-object v0

    .line 2639693
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2639694
    invoke-interface {p2}, LX/J2I;->d()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;

    .line 2639695
    new-instance v6, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->b()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;->b()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/math/BigDecimal;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->b()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v7, v8}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 2639696
    new-instance v7, LX/J2h;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->a()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, LX/J22;->b:LX/5fv;

    invoke-virtual {v9, v6}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->c()LX/0Px;

    move-result-object v1

    invoke-direct {v7, v8, v6, v1}, LX/J2h;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v3, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639697
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 2639698
    :cond_2
    new-instance v1, LX/J2i;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p1, v0}, LX/J22;->a(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;LX/0Px;)LX/0Px;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/J2i;-><init>(LX/0Px;LX/0Px;)V

    .line 2639699
    iput-object v1, p3, LX/J2q;->c:LX/J2e;

    .line 2639700
    goto/16 :goto_1

    .line 2639701
    :pswitch_3
    invoke-virtual {p2}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->a()LX/0Px;

    move-result-object v0

    .line 2639702
    const/4 v1, 0x0

    .line 2639703
    invoke-interface {p2}, LX/J2H;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2639704
    invoke-interface {p2}, LX/J2H;->c()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;

    .line 2639705
    new-instance v2, LX/J2d;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;->a()I

    move-result v3

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;->c()I

    move-result v4

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1}, LX/J2d;-><init>(IILjava/lang/String;)V

    move-object v1, v2

    .line 2639706
    :cond_3
    new-instance v2, LX/J2f;

    invoke-static {p1, v0}, LX/J22;->a(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;LX/0Px;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v1, v3}, LX/J2f;-><init>(LX/J2d;LX/0Px;)V

    .line 2639707
    iput-object v2, p3, LX/J2q;->c:LX/J2e;

    .line 2639708
    goto/16 :goto_1

    .line 2639709
    :cond_4
    invoke-interface {p2}, LX/J2J;->e()LX/0Px;

    move-result-object v4

    .line 2639710
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2639711
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel$CurrencyAmountModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel$CurrencyAmountModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 2639712
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move-object v3, v1

    :goto_5
    if-ge v2, v6, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;

    .line 2639713
    new-instance v7, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel$CurrencyAmountModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel$CurrencyAmountModel;->b()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/math/BigDecimal;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel$CurrencyAmountModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel$CurrencyAmountModel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 2639714
    invoke-virtual {v3, v7}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    .line 2639715
    new-instance v8, LX/73b;

    invoke-virtual {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v9, p0, LX/J22;->b:LX/5fv;

    invoke-virtual {v9, v7}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v1, v7}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639716
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 2639717
    :cond_5
    new-instance v1, LX/73b;

    iget-object v2, p0, LX/J22;->a:Landroid/content/res/Resources;

    const v4, 0x7f0838d5

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/J22;->b:LX/5fv;

    invoke-virtual {v4, v3}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639718
    new-instance v1, LX/J2k;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p1, v0}, LX/J22;->a(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;LX/0Px;)LX/0Px;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/J2k;-><init>(LX/0Px;LX/0Px;)V

    .line 2639719
    iput-object v1, p3, LX/J2q;->c:LX/J2e;

    .line 2639720
    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x2cb1c1c4 -> :sswitch_2
        -0x168d55a7 -> :sswitch_0
        0x62db1ba7 -> :sswitch_1
        0x6e9fd974 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
