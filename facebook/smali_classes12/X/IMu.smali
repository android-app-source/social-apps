.class public final LX/IMu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/search/CommunitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V
    .locals 0

    .prologue
    .line 2570303
    iput-object p1, p0, LX/IMu;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 5

    .prologue
    .line 2570304
    if-nez p2, :cond_1

    .line 2570305
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "community_id"

    iget-object v2, p0, LX/IMu;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v2, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, LX/IMu;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v2, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v2}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2570306
    iget-object v1, p0, LX/IMu;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    if-eqz v1, :cond_0

    .line 2570307
    const-string v1, "question_id"

    iget-object v2, p0, LX/IMu;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v2, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2570308
    :cond_0
    iget-object v1, p0, LX/IMu;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->h:LX/0if;

    sget-object v2, LX/0ig;->aS:LX/0ih;

    const-string v3, "did_search_query"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2570309
    :cond_1
    return-void
.end method
