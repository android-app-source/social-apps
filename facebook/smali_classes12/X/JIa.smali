.class public final LX/JIa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/JIe;


# direct methods
.method public constructor <init>(LX/JIe;LX/4BY;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2678056
    iput-object p1, p0, LX/JIa;->c:LX/JIe;

    iput-object p2, p0, LX/JIa;->a:LX/4BY;

    iput-object p3, p0, LX/JIa;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 2678057
    iget-object v0, p0, LX/JIa;->c:LX/JIe;

    iget-object v0, v0, LX/JIe;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 2678058
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/JIa;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2678059
    const-string v2, "curation_card_model_extra"

    iget-object v3, p0, LX/JIa;->c:LX/JIe;

    iget-object v3, v3, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-static {v1, v2, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2678060
    const-string v2, "discovery_curation_logging_data_extra_key"

    iget-object v3, p0, LX/JIa;->c:LX/JIe;

    iget-object v3, v3, LX/JIe;->h:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    invoke-static {v1, v2, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2678061
    move-object v2, v1

    .line 2678062
    const/4 v3, 0x1

    iget-object v1, p0, LX/JIa;->b:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v2, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2678063
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2678051
    iget-object v0, p0, LX/JIa;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2678052
    iget-object v0, p0, LX/JIa;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2678053
    iget-object v0, p0, LX/JIa;->b:Landroid/content/Context;

    iget-object v1, p0, LX/JIa;->b:Landroid/content/Context;

    const v2, 0x7f083a70

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2678054
    invoke-direct {p0}, LX/JIa;->b()V

    .line 2678055
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2678047
    iget-object v0, p0, LX/JIa;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2678048
    iget-object v0, p0, LX/JIa;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2678049
    invoke-direct {p0}, LX/JIa;->b()V

    .line 2678050
    :cond_0
    return-void
.end method
