.class public LX/JK3;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBEventsNativeModule"
.end annotation


# static fields
.field private static final a:Ljava/util/Date;


# instance fields
.field private final b:LX/3fr;

.field private final c:LX/Bie;

.field private final d:LX/HxA;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/DBC;

.field public final g:LX/DBA;

.field private final h:LX/HxN;

.field private final i:LX/0tX;

.field private final j:Ljava/util/concurrent/ExecutorService;

.field private final k:LX/Bl6;

.field private final l:LX/JK1;

.field private final m:LX/JK2;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1nQ;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2680088
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sput-object v0, LX/JK3;->a:Ljava/util/Date;

    return-void
.end method

.method public constructor <init>(LX/5pY;LX/3fr;LX/Bie;LX/HxA;Lcom/facebook/content/SecureContextHelper;LX/DBC;LX/DBA;LX/HxN;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/Bl6;LX/0Or;)V
    .locals 2
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            "LX/3fr;",
            "LX/Bie;",
            "LX/HxA;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/DBC;",
            "LX/DBA;",
            "LX/HxN;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/Bl6;",
            "LX/0Or",
            "<",
            "LX/1nQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680089
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2680090
    new-instance v0, LX/JK1;

    invoke-direct {v0, p0}, LX/JK1;-><init>(LX/JK3;)V

    iput-object v0, p0, LX/JK3;->l:LX/JK1;

    .line 2680091
    new-instance v0, LX/JK2;

    invoke-direct {v0, p0}, LX/JK2;-><init>(LX/JK3;)V

    iput-object v0, p0, LX/JK3;->m:LX/JK2;

    .line 2680092
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2680093
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5on;)V

    .line 2680094
    iput-object p2, p0, LX/JK3;->b:LX/3fr;

    .line 2680095
    iput-object p3, p0, LX/JK3;->c:LX/Bie;

    .line 2680096
    iput-object p4, p0, LX/JK3;->d:LX/HxA;

    .line 2680097
    iput-object p5, p0, LX/JK3;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2680098
    iput-object p6, p0, LX/JK3;->f:LX/DBC;

    .line 2680099
    iput-object p7, p0, LX/JK3;->g:LX/DBA;

    .line 2680100
    iput-object p8, p0, LX/JK3;->h:LX/HxN;

    .line 2680101
    iput-object p9, p0, LX/JK3;->i:LX/0tX;

    .line 2680102
    iput-object p10, p0, LX/JK3;->j:Ljava/util/concurrent/ExecutorService;

    .line 2680103
    iput-object p11, p0, LX/JK3;->k:LX/Bl6;

    .line 2680104
    iput-object p12, p0, LX/JK3;->n:LX/0Or;

    .line 2680105
    invoke-direct {p0}, LX/JK3;->h()V

    .line 2680106
    return-void
.end method

.method private a(Ljava/lang/String;LX/JJy;)V
    .locals 3

    .prologue
    .line 2680107
    invoke-static {}, LX/7oV;->e()LX/7oI;

    move-result-object v0

    .line 2680108
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2680109
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2680110
    iget-object v1, p0, LX/JK3;->i:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2680111
    new-instance v1, LX/JK0;

    invoke-direct {v1, p0, p2}, LX/JK0;-><init>(LX/JK3;LX/JJy;)V

    iget-object v2, p0, LX/JK3;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2680112
    return-void
.end method

.method public static synthetic b(LX/JK3;)LX/5pY;
    .locals 1

    .prologue
    .line 2680113
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2680114
    return-object v0
.end method

.method public static synthetic c(LX/JK3;)LX/5pY;
    .locals 1

    .prologue
    .line 2680118
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2680119
    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2680115
    iget-object v0, p0, LX/JK3;->f:LX/DBC;

    sget-object v1, Lcom/facebook/events/common/EventAnalyticsParams;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2680116
    iput-object v1, v0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2680117
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2680124
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2680125
    :goto_0
    return-void

    .line 2680126
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2680127
    :pswitch_0
    iget-object v0, p0, LX/JK3;->f:LX/DBC;

    invoke-virtual {v0, p1, p2, p3}, LX/DBC;->a(IILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
    .end packed-switch
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2680120
    invoke-super {p0}, LX/5pb;->d()V

    .line 2680121
    iget-object v0, p0, LX/JK3;->k:LX/Bl6;

    iget-object v1, p0, LX/JK3;->l:LX/JK1;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2680122
    iget-object v0, p0, LX/JK3;->k:LX/Bl6;

    iget-object v1, p0, LX/JK3;->m:LX/JK2;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2680123
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2680083
    invoke-super {p0}, LX/5pb;->f()V

    .line 2680084
    iget-object v0, p0, LX/JK3;->k:LX/Bl6;

    iget-object v1, p0, LX/JK3;->l:LX/JK1;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2680085
    iget-object v0, p0, LX/JK3;->k:LX/Bl6;

    iget-object v1, p0, LX/JK3;->m:LX/JK2;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2680086
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2680087
    const-string v0, "FBEventsNativeModule"

    return-object v0
.end method

.method public openAllBirthdaysListView(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680079
    iget-object v0, p0, LX/JK3;->d:LX/HxA;

    .line 2680080
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2680081
    const/4 p0, 0x1

    invoke-static {v0, v1, p0}, LX/HxA;->a(LX/HxA;Landroid/content/Context;Z)V

    .line 2680082
    return-void
.end method

.method public openComposerToPerson(ILjava/lang/String;)V
    .locals 8
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680063
    iget-object v2, p0, LX/JK3;->o:LX/2RQ;

    invoke-virtual {v2, p2}, LX/2RQ;->a(Ljava/lang/String;)LX/2RR;

    move-result-object v2

    sget-object v3, LX/3Oq;->FRIEND:LX/3Oq;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2680064
    iput-object v3, v2, LX/2RR;->c:Ljava/util/Collection;

    .line 2680065
    move-object v2, v2

    .line 2680066
    iget-object v3, p0, LX/JK3;->b:LX/3fr;

    invoke-virtual {v3, v2}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v4

    .line 2680067
    :try_start_0
    invoke-interface {v4}, LX/6N1;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 2680068
    invoke-interface {v4}, LX/6N1;->close()V

    .line 2680069
    :goto_0
    return-void

    .line 2680070
    :cond_0
    :try_start_1
    invoke-interface {v4}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    move-object v3, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2680071
    invoke-interface {v4}, LX/6N1;->close()V

    .line 2680072
    sget-object v2, LX/JK3;->a:Ljava/util/Date;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->C()I

    move-result v5

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->D()I

    move-result v6

    invoke-static {v2, v4, v5, v6}, LX/6RS;->a(Ljava/util/Date;Ljava/util/TimeZone;II)Ljava/util/Calendar;

    move-result-object v6

    .line 2680073
    iget-object v2, p0, LX/JK3;->h:LX/HxN;

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v6

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v7

    move-object v3, p2

    .line 2680074
    sget-object v0, LX/21D;->EVENT:LX/21D;

    const-string v1, "eventDashboardCelebrationsFromReact"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v3, v4, v5, v1}, LX/HxN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v2}, LX/HxN;->a()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_EVENTS_DASHBOARD_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2680075
    iget-object v1, v2, LX/HxN;->b:LX/7v8;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v6, p0}, LX/7v8;->a(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2680076
    iget-object p0, v2, LX/HxN;->a:LX/1Kf;

    invoke-interface {p0, v1, v0, v7}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2680077
    goto :goto_0

    .line 2680078
    :catchall_0
    move-exception v2

    invoke-interface {v4}, LX/6N1;->close()V

    throw v2
.end method

.method public openCreateEvent(I)V
    .locals 11
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680058
    iget-object v0, p0, LX/JK3;->c:LX/Bie;

    .line 2680059
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2680060
    sget-object v2, Lcom/facebook/events/common/EventAnalyticsParams;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_HEADER:Lcom/facebook/events/common/ActionMechanism;

    const/4 v6, 0x0

    .line 2680061
    const/4 v10, 0x1

    move-object v4, v0

    move-object v5, v1

    move-object v7, v6

    move-object v8, v2

    move-object v9, v3

    invoke-static/range {v4 .. v10}, LX/Bie;->a(LX/Bie;Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;Z)V

    .line 2680062
    return-void
.end method

.method public openDiscoveryListViewForCutType(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680049
    iget-object v0, p0, LX/JK3;->d:LX/HxA;

    .line 2680050
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2680051
    sget-object v4, Lcom/facebook/events/common/EventActionContext;->b:Lcom/facebook/events/common/EventActionContext;

    const-string v5, "event_suggestions"

    move-object v2, p2

    move-object v3, p3

    .line 2680052
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    iget-object p0, v0, LX/HxA;->b:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/ComponentName;

    invoke-virtual {p1, p0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object p0

    .line 2680053
    const-string p1, "target_fragment"

    sget-object p2, LX/0cQ;->EVENTS_SUGGESTIONS_FRAGMENT:LX/0cQ;

    invoke-virtual {p2}, LX/0cQ;->ordinal()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2680054
    invoke-static {v2, v3, v4, v5}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2680055
    const/high16 p1, 0x10000000

    invoke-virtual {p0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2680056
    iget-object p1, v0, LX/HxA;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2680057
    return-void
.end method

.method public openEditEvent(ILX/5pG;Lcom/facebook/react/bridge/Callback;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680045
    const-string v0, "id"

    invoke-interface {p2, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2680046
    new-instance v1, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v2, Lcom/facebook/events/common/EventActionContext;->b:Lcom/facebook/events/common/EventActionContext;

    const-string v3, "unknown"

    const-string v4, "event_dashboard"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2680047
    new-instance v2, LX/JJz;

    invoke-direct {v2, p0, p3, v1}, LX/JJz;-><init>(LX/JK3;Lcom/facebook/react/bridge/Callback;Lcom/facebook/events/common/EventAnalyticsParams;)V

    invoke-direct {p0, v0, v2}, LX/JK3;->a(Ljava/lang/String;LX/JJy;)V

    .line 2680048
    return-void
.end method

.method public openEventPermalink(ILjava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680041
    sget-object v0, LX/0ax;->B:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2680042
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2680043
    const-class v2, LX/IBz;

    invoke-virtual {v0, v2}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/IBz;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/IBz;->a(Landroid/net/Uri;)V

    .line 2680044
    return-void
.end method

.method public openInviteToEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680039
    iget-object v0, p0, LX/JK3;->f:LX/DBC;

    invoke-static {p3}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v3

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v1}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x1f5

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v7

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    invoke-virtual/range {v0 .. v7}, LX/DBC;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZLjava/lang/String;ILandroid/app/Activity;)V

    .line 2680040
    return-void
.end method

.method public openSubscribedEvents(ILjava/lang/String;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680034
    iget-object v0, p0, LX/JK3;->d:LX/HxA;

    .line 2680035
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2680036
    sget-object v2, Lcom/facebook/events/common/EventActionContext;->b:Lcom/facebook/events/common/EventActionContext;

    const-string v3, "event_subscriptions"

    .line 2680037
    const/4 p0, 0x1

    invoke-static {v0, v1, v2, v3, p0}, LX/HxA;->a(LX/HxA;Landroid/content/Context;Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Z)V

    .line 2680038
    return-void
.end method

.method public reportEventsDashboardLoadedEvent(Ljava/lang/String;I)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680032
    iget-object v0, p0, LX/JK3;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nQ;

    iget-object v3, p0, LX/JK3;->p:Ljava/lang/String;

    sget-object v1, Lcom/facebook/events/common/ActionSource;->MOBILE_BOOKMARK_TAB:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v1}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    const/4 v5, 0x0

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, LX/1nQ;->a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 2680033
    return-void
.end method
