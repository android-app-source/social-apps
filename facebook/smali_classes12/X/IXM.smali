.class public final enum LX/IXM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IXM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IXM;

.field public static final enum FAILED:LX/IXM;

.field public static final enum LOADING:LX/IXM;

.field public static final enum SUCCESS:LX/IXM;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2585939
    new-instance v0, LX/IXM;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, LX/IXM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IXM;->LOADING:LX/IXM;

    .line 2585940
    new-instance v0, LX/IXM;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v3}, LX/IXM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IXM;->FAILED:LX/IXM;

    .line 2585941
    new-instance v0, LX/IXM;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, LX/IXM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IXM;->SUCCESS:LX/IXM;

    .line 2585942
    const/4 v0, 0x3

    new-array v0, v0, [LX/IXM;

    sget-object v1, LX/IXM;->LOADING:LX/IXM;

    aput-object v1, v0, v2

    sget-object v1, LX/IXM;->FAILED:LX/IXM;

    aput-object v1, v0, v3

    sget-object v1, LX/IXM;->SUCCESS:LX/IXM;

    aput-object v1, v0, v4

    sput-object v0, LX/IXM;->$VALUES:[LX/IXM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2585943
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IXM;
    .locals 1

    .prologue
    .line 2585944
    const-class v0, LX/IXM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IXM;

    return-object v0
.end method

.method public static values()[LX/IXM;
    .locals 1

    .prologue
    .line 2585945
    sget-object v0, LX/IXM;->$VALUES:[LX/IXM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IXM;

    return-object v0
.end method
