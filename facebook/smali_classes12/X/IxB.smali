.class public LX/IxB;
.super LX/2s5;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/content/Context;

.field private final c:LX/0ad;

.field private final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0gc;Landroid/content/Context;LX/0ad;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p2    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2631324
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2631325
    iput-object p1, p0, LX/IxB;->a:Ljava/lang/String;

    .line 2631326
    iput-object p3, p0, LX/IxB;->b:Landroid/content/Context;

    .line 2631327
    iput-object p4, p0, LX/IxB;->c:LX/0ad;

    .line 2631328
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/IxB;->d:Landroid/util/SparseArray;

    .line 2631329
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2631317
    invoke-static {}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->values()[Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2631318
    sget-object v1, LX/IxA;->a:[I

    invoke-virtual {v0}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2631319
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown Pages Launchpoint Fragment Type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2631320
    :pswitch_0
    iget-object v0, p0, LX/IxB;->b:Landroid/content/Context;

    const v1, 0x7f083102

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2631321
    :goto_0
    return-object v0

    .line 2631322
    :pswitch_1
    iget-object v0, p0, LX/IxB;->b:Landroid/content/Context;

    const v1, 0x7f083103

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2631323
    :pswitch_2
    iget-object v0, p0, LX/IxB;->b:Landroid/content/Context;

    const v1, 0x7f083104

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2631299
    invoke-static {}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->values()[Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2631300
    sget-object v1, LX/IxA;->a:[I

    invoke-virtual {v0}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2631301
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown Pages Launchpoint Fragment Type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2631302
    :pswitch_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2631303
    new-instance v1, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;

    invoke-direct {v1}, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;-><init>()V

    .line 2631304
    const-string v2, "ptr_enabled"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2631305
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2631306
    move-object v0, v1

    .line 2631307
    :goto_0
    return-object v0

    .line 2631308
    :pswitch_1
    new-instance v0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;

    invoke-direct {v0}, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;-><init>()V

    move-object v0, v0

    .line 2631309
    goto :goto_0

    .line 2631310
    :pswitch_2
    new-instance v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    invoke-direct {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;-><init>()V

    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    iget-object v2, p0, LX/IxB;->a:Ljava/lang/String;

    sget-object v3, Lcom/facebook/api/feedtype/FeedType$Name;->e:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v2, v3}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    .line 2631311
    iput-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 2631312
    move-object v0, v0

    .line 2631313
    const/4 v1, 0x0

    .line 2631314
    iput-boolean v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->c:Z

    .line 2631315
    move-object v0, v0

    .line 2631316
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->d()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2631292
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 2631293
    iget-object v1, p0, LX/IxB;->d:Landroid/util/SparseArray;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2631294
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2631296
    iget-object v0, p0, LX/IxB;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 2631297
    invoke-super {p0, p1, p2, p3}, LX/2s5;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2631298
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2631295
    invoke-static {}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->values()[Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
