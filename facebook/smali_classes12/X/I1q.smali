.class public final LX/I1q;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V
    .locals 0

    .prologue
    .line 2529400
    iput-object p1, p0, LX/I1q;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2529401
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2529402
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529403
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2529404
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2529405
    if-eqz v0, :cond_0

    .line 2529406
    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2529407
    iget-object v1, p0, LX/I1q;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->k:LX/I1o;

    .line 2529408
    iget-object p0, v1, LX/I1o;->c:Ljava/util/HashMap;

    .line 2529409
    iget-object p1, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object p1, p1

    .line 2529410
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2529411
    iget-object p0, v1, LX/I1o;->c:Ljava/util/HashMap;

    .line 2529412
    iget-object p1, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object p1, p1

    .line 2529413
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 2529414
    iget-object p1, v1, LX/I1o;->b:Ljava/util/List;

    invoke-interface {p1, p0, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2529415
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2529416
    :cond_0
    return-void
.end method
