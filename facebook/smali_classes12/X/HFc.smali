.class public LX/HFc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2443951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2443952
    iput-object p1, p0, LX/HFc;->a:LX/0tX;

    .line 2443953
    return-void
.end method

.method public static b(LX/0QB;)LX/HFc;
    .locals 2

    .prologue
    .line 2443938
    new-instance v1, LX/HFc;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/HFc;-><init>(LX/0tX;)V

    .line 2443939
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "III)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/HF0;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2443940
    new-instance v0, LX/4DK;

    invoke-direct {v0}, LX/4DK;-><init>()V

    invoke-virtual {v0, p1}, LX/4DK;->a(Ljava/lang/String;)LX/4DK;

    move-result-object v0

    .line 2443941
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2443942
    const-string v2, "CITY"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2443943
    const-string v2, "page_category"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2443944
    const-string v1, "ANDROID_PAGES"

    invoke-virtual {v0, v1}, LX/4DK;->f(Ljava/lang/String;)LX/4DK;

    .line 2443945
    new-instance v1, LX/HG5;

    invoke-direct {v1}, LX/HG5;-><init>()V

    move-object v1, v1

    .line 2443946
    const-string v2, "query_params"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    const-string v2, "num_results"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "icon_width"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "icon_height"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/HG5;

    .line 2443947
    iget-object v2, p0, LX/HFc;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v2, LX/HFY;

    invoke-direct {v2, p0}, LX/HFY;-><init>(LX/HFc;)V

    .line 2443948
    sget-object p1, LX/131;->INSTANCE:LX/131;

    move-object p1, p1

    .line 2443949
    invoke-static {v1, v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2443950
    return-object v0
.end method
