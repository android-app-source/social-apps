.class public final LX/HF7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HEu;

.field public final synthetic b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;LX/HEu;)V
    .locals 0

    .prologue
    .line 2443068
    iput-object p1, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iput-object p2, p0, LX/HF7;->a:LX/HEu;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2443069
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2443070
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->p:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443071
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->b:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    const-string v2, "create page mutation failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2443072
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2443073
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;

    const/4 v4, 0x1

    .line 2443074
    if-eqz p1, :cond_0

    .line 2443075
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2443076
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2443077
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 2443078
    iget-object v1, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443079
    iput-object v0, v1, LX/HFJ;->a:Ljava/lang/String;

    .line 2443080
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    iget-object v1, p0, LX/HF7;->a:LX/HEu;

    .line 2443081
    iput-object v1, v0, LX/HFJ;->d:LX/HEu;

    .line 2443082
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->g:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "save_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HF7;->a:LX/HEu;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443083
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->c()V

    .line 2443084
    :goto_1
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->p:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443085
    :cond_0
    return-void

    .line 2443086
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2443087
    :cond_2
    if-eqz v1, :cond_3

    .line 2443088
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    const v2, 0x7f0d22d2

    .line 2443089
    invoke-virtual {v0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    move-object v0, v3

    .line 2443090
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443091
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2443092
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2443093
    :cond_3
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2443094
    iget-object v0, p0, LX/HF7;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->b:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    const-string v2, "unknown error: Page creation failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
