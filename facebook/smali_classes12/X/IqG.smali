.class public final enum LX/IqG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IqG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IqG;

.field public static final enum DONE:LX/IqG;

.field public static final enum LOADING:LX/IqG;

.field public static final enum NOT_STARTED:LX/IqG;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2616788
    new-instance v0, LX/IqG;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, LX/IqG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IqG;->NOT_STARTED:LX/IqG;

    .line 2616789
    new-instance v0, LX/IqG;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/IqG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IqG;->LOADING:LX/IqG;

    .line 2616790
    new-instance v0, LX/IqG;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, LX/IqG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IqG;->DONE:LX/IqG;

    .line 2616791
    const/4 v0, 0x3

    new-array v0, v0, [LX/IqG;

    sget-object v1, LX/IqG;->NOT_STARTED:LX/IqG;

    aput-object v1, v0, v2

    sget-object v1, LX/IqG;->LOADING:LX/IqG;

    aput-object v1, v0, v3

    sget-object v1, LX/IqG;->DONE:LX/IqG;

    aput-object v1, v0, v4

    sput-object v0, LX/IqG;->$VALUES:[LX/IqG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2616792
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IqG;
    .locals 1

    .prologue
    .line 2616793
    const-class v0, LX/IqG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IqG;

    return-object v0
.end method

.method public static values()[LX/IqG;
    .locals 1

    .prologue
    .line 2616794
    sget-object v0, LX/IqG;->$VALUES:[LX/IqG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IqG;

    return-object v0
.end method
