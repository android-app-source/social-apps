.class public LX/IVl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D3l;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/D3l",
        "<",
        "LX/IVj;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1EV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3my;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/content/SecureContextHelper;

.field private g:LX/DOL;

.field private h:LX/DQW;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/DOL;LX/DQW;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2582244
    iput-object p1, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2582245
    iput-object p2, p0, LX/IVl;->g:LX/DOL;

    .line 2582246
    iput-object p3, p0, LX/IVl;->h:LX/DQW;

    .line 2582247
    return-void
.end method

.method public static a(LX/0QB;)LX/IVl;
    .locals 8

    .prologue
    .line 2582248
    const-class v1, LX/IVl;

    monitor-enter v1

    .line 2582249
    :try_start_0
    sget-object v0, LX/IVl;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582250
    sput-object v2, LX/IVl;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582251
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582252
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582253
    new-instance v3, LX/IVl;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/DOL;->a(LX/0QB;)LX/DOL;

    move-result-object v5

    check-cast v5, LX/DOL;

    invoke-static {v0}, LX/DQW;->b(LX/0QB;)LX/DQW;

    move-result-object v6

    check-cast v6, LX/DQW;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct {v3, v4, v5, v6, v7}, LX/IVl;-><init>(Lcom/facebook/content/SecureContextHelper;LX/DOL;LX/DQW;LX/0ad;)V

    .line 2582254
    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, LX/1EV;->b(LX/0QB;)LX/1EV;

    move-result-object v5

    check-cast v5, LX/1EV;

    invoke-static {v0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v6

    check-cast v6, LX/3my;

    const/16 v7, 0xbca

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0xbd2

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2582255
    iput-object v4, v3, LX/IVl;->a:LX/17W;

    iput-object v5, v3, LX/IVl;->b:LX/1EV;

    iput-object v6, v3, LX/IVl;->c:LX/3my;

    iput-object v7, v3, LX/IVl;->d:LX/0Ot;

    iput-object p0, v3, LX/IVl;->e:LX/0Ot;

    .line 2582256
    move-object v0, v3

    .line 2582257
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582258
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582259
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582260
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/IVl;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2582261
    iget-object v0, p0, LX/IVl;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    const-string v1, "4474"

    const-class v2, LX/3lO;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3lO;

    .line 2582262
    if-nez v0, :cond_0

    .line 2582263
    :goto_0
    return-void

    .line 2582264
    :cond_0
    iput-object p1, v0, LX/3lO;->a:Landroid/view/View;

    .line 2582265
    iget-object v0, p0, LX/IVl;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_TYPE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 2582266
    const-class v3, LX/0i1;

    const/4 p0, 0x0

    invoke-virtual {v0, v1, v2, v3, p0}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 2582267
    goto :goto_0
.end method

.method public static a(LX/IVl;LX/IVj;)Z
    .locals 2

    .prologue
    .line 2582268
    iget-object v0, p0, LX/IVl;->c:LX/3my;

    iget-object v1, p1, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    return v0
.end method

.method public static k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V
    .locals 2

    .prologue
    .line 2582269
    iput-object p0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b:LX/D3l;

    .line 2582270
    iget-boolean v0, p2, LX/IVj;->c:Z

    if-nez v0, :cond_0

    .line 2582271
    const/4 v0, 0x1

    .line 2582272
    iput-boolean v0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->l:Z

    .line 2582273
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e86

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2582274
    iput v0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->k:I

    .line 2582275
    :cond_0
    iget-object v0, p2, LX/IVj;->b:LX/IVk;

    .line 2582276
    sget-object v1, LX/IVi;->a:[I

    invoke-virtual {v0}, LX/IVk;->ordinal()I

    move-result p0

    aget v1, v1, p0

    packed-switch v1, :pswitch_data_0

    .line 2582277
    :pswitch_0
    const/4 v1, 0x0

    .line 2582278
    :goto_0
    move v0, v1

    .line 2582279
    invoke-virtual {p1, v0, p2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ZLjava/lang/Object;)V

    .line 2582280
    return-void

    .line 2582281
    :pswitch_1
    const/4 v1, 0x1

    .line 2582282
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2582283
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 2582284
    instance-of v1, v0, LX/IVj;

    if-nez v1, :cond_1

    .line 2582285
    :cond_0
    :goto_0
    return-void

    .line 2582286
    :cond_1
    check-cast v0, LX/IVj;

    .line 2582287
    sget-object v1, LX/IVi;->a:[I

    iget-object v2, v0, LX/IVj;->b:LX/IVk;

    invoke-virtual {v2}, LX/IVk;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2582288
    :pswitch_1
    iget-object v1, p0, LX/IVl;->g:LX/DOL;

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2582289
    invoke-static {v1}, LX/DOL;->a(LX/DOL;)Landroid/content/Intent;

    move-result-object v2

    .line 2582290
    const-string v3, "target_fragment"

    sget-object v4, LX/0cQ;->GROUPS_PINNED_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2582291
    const-string v3, "group_feed_model"

    invoke-static {v2, v3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2582292
    const-string v3, "group_feed_id"

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2582293
    move-object v0, v2

    .line 2582294
    iget-object v1, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2582295
    :pswitch_2
    const-string v1, "https://m.facebook.com/groups/%s?view=members"

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2582296
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2582297
    sget-object v2, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2582298
    iget-object v0, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2582299
    :pswitch_3
    iget-object v1, p0, LX/IVl;->h:LX/DQW;

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1, v0}, LX/DQW;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/content/Intent;

    move-result-object v0

    .line 2582300
    iget-object v1, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2582301
    :pswitch_4
    iget-object v1, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2582302
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2582303
    sget-object v2, LX/0ax;->do:Ljava/lang/String;

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2582304
    iget-object v0, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2582305
    :pswitch_5
    iget-object v1, p0, LX/IVl;->g:LX/DOL;

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2582306
    invoke-static {v1}, LX/DOL;->a(LX/DOL;)Landroid/content/Intent;

    move-result-object v2

    .line 2582307
    const-string v3, "target_fragment"

    sget-object v4, LX/0cQ;->GROUP_PENDING_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2582308
    const-string v3, "group_feed_model"

    invoke-static {v2, v3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2582309
    const-string v3, "group_feed_id"

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2582310
    move-object v0, v2

    .line 2582311
    iget-object v1, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2582312
    :pswitch_6
    iget-object v1, p0, LX/IVl;->h:LX/DQW;

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    const-string v2, "group_context_rows"

    invoke-virtual {v1, v0, v2}, LX/DQW;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2582313
    iget-object v1, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2582314
    :pswitch_7
    iget-object v1, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/IVl;->h:LX/DQW;

    iget-object v3, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v3

    const-string v4, "group_context_rows"

    const-string v5, "https://m.facebook.com/groups/%s/madminpanel/reported/"

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2582315
    if-nez v0, :cond_3

    .line 2582316
    const/4 v6, 0x0

    .line 2582317
    :goto_1
    move v0, v6

    .line 2582318
    invoke-virtual {v2, v3, v4, v5, v0}, LX/DQW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2582319
    :pswitch_8
    iget-object v1, p0, LX/IVl;->b:LX/1EV;

    invoke-virtual {v1}, LX/1EV;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2582320
    sget-object v1, LX/0ax;->gd:Ljava/lang/String;

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2582321
    iget-object v1, p0, LX/IVl;->a:LX/17W;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 2582322
    :cond_2
    iget-object v1, p0, LX/IVl;->g:LX/DOL;

    iget-object v0, v0, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1, v0}, LX/DOL;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/content/Intent;

    move-result-object v0

    .line 2582323
    iget-object v1, p0, LX/IVl;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v6

    const-string p0, "gk_groups_flagged_reported_post_queue"

    invoke-static {v6, p0}, LX/88m;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;Ljava/lang/String;)Z

    move-result v6

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)Z
    .locals 13

    .prologue
    .line 2582324
    const/4 v0, 0x0

    .line 2582325
    sget-object v1, LX/IVi;->a:[I

    iget-object v2, p2, LX/IVj;->b:LX/IVk;

    invoke-virtual {v2}, LX/IVk;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2582326
    :goto_0
    return v0

    .line 2582327
    :pswitch_0
    const/4 v10, 0x0

    .line 2582328
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_3

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->a()I

    move-result v3

    if-lez v3, :cond_3

    .line 2582329
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 2582330
    const v3, 0x7f0b0e87

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2582331
    const v3, 0x7f0b00b2

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2582332
    mul-int/lit8 v3, v5, 0x2

    add-int/2addr v3, v4

    .line 2582333
    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582334
    const v3, 0x7f0d0107

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setId(I)V

    .line 2582335
    const v7, 0x7f020c3b

    const-string v8, "group_feed"

    move-object v3, p1

    move v6, v5

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILjava/lang/String;)V

    .line 2582336
    const v3, 0x7f081b8a

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b0050

    invoke-static {v9, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582337
    const/4 v3, 0x3

    const v4, 0x7f0b004e

    invoke-static {v9, v4}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v4

    invoke-virtual {p1, v10, v3, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582338
    invoke-virtual {p1, v10}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582339
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    .line 2582340
    const/4 v3, 0x1

    .line 2582341
    :goto_1
    move v0, v3

    .line 2582342
    goto :goto_0

    .line 2582343
    :pswitch_1
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_13

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-eqz v3, :cond_13

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;

    move-result-object v3

    if-eqz v3, :cond_13

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->A()I

    move-result v3

    if-lez v3, :cond_13

    .line 2582344
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2582345
    const/4 v9, 0x0

    .line 2582346
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;

    .line 2582347
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->c()Z

    move-result v7

    if-eqz v7, :cond_15

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_15

    .line 2582348
    const/4 v7, 0x3

    if-gt v9, v7, :cond_0

    .line 2582349
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2582350
    add-int/lit8 v3, v9, 0x1

    .line 2582351
    :goto_3
    add-int/lit8 v4, v4, 0x1

    move v9, v3

    goto :goto_2

    .line 2582352
    :cond_0
    if-nez v9, :cond_4

    .line 2582353
    const/4 v3, 0x0

    .line 2582354
    :goto_4
    move v0, v3

    .line 2582355
    goto/16 :goto_0

    .line 2582356
    :pswitch_2
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2582357
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2582358
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2582359
    sget-object v0, LX/IVi;->b:[I

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_1

    move-object v0, v4

    move-object v3, v4

    .line 2582360
    :goto_5
    if-eqz v3, :cond_1

    if-nez v0, :cond_20

    .line 2582361
    :cond_1
    :goto_6
    move v0, v2

    .line 2582362
    goto/16 :goto_0

    .line 2582363
    :pswitch_3
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2582364
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_2

    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_27

    :cond_2
    move v1, v2

    .line 2582365
    :goto_7
    move v0, v1

    .line 2582366
    goto/16 :goto_0

    .line 2582367
    :pswitch_4
    const/4 v9, 0x0

    .line 2582368
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_2d

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-eqz v3, :cond_2d

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v3

    if-eqz v3, :cond_2d

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2d

    .line 2582369
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 2582370
    const v3, 0x7f0b0e87

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2582371
    const v3, 0x7f0b00b2

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2582372
    mul-int/lit8 v3, v5, 0x2

    add-int/2addr v3, v4

    .line 2582373
    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582374
    const v7, 0x7f020c38

    const-string v8, "group_feed"

    move-object v3, p1

    move v6, v5

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILjava/lang/String;)V

    .line 2582375
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;

    .line 2582376
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    const v6, 0x7f0b0050

    invoke-static {v10, v6}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v6

    invoke-virtual {p1, v4, v5, v6}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582377
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    const v5, 0x7f0b004e

    invoke-static {v10, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582378
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582379
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    .line 2582380
    const/4 v3, 0x1

    .line 2582381
    :goto_8
    move v0, v3

    .line 2582382
    goto/16 :goto_0

    .line 2582383
    :pswitch_5
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2582384
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_2f

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-eqz v3, :cond_2f

    .line 2582385
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->q()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2582386
    if-eqz v3, :cond_2e

    move v3, v9

    :goto_9
    if-eqz v3, :cond_31

    .line 2582387
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->q()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2582388
    invoke-virtual {v4, v3, v10}, LX/15i;->j(II)I

    move-result v3

    if-lez v3, :cond_30

    move v3, v9

    :goto_a
    if-eqz v3, :cond_32

    .line 2582389
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2582390
    const v3, 0x7f0b0e87

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2582391
    const v3, 0x7f0b00b2

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2582392
    mul-int/lit8 v3, v5, 0x2

    add-int/2addr v3, v4

    .line 2582393
    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582394
    const v3, 0x7f0d0106

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setId(I)V

    .line 2582395
    const v7, 0x7f020c37

    const-string v8, "group_feed"

    move-object v3, p1

    move v6, v5

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILjava/lang/String;)V

    .line 2582396
    const v3, 0x7f081b73

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b0050

    invoke-static {v11, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582397
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->q()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v4, v3, v10}, LX/15i;->j(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582398
    const/4 v3, 0x0

    const/4 v4, 0x3

    const v5, 0x7f0b004e

    invoke-static {v11, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582399
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    .line 2582400
    :goto_b
    move v0, v9

    .line 2582401
    goto/16 :goto_0

    .line 2582402
    :pswitch_6
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_33

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-eqz v3, :cond_33

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v3

    if-eqz v3, :cond_33

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->a()I

    move-result v3

    if-lez v3, :cond_33

    .line 2582403
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 2582404
    const v3, 0x7f0b0e87

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2582405
    const v3, 0x7f0b00b2

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2582406
    mul-int/lit8 v3, v5, 0x2

    add-int/2addr v3, v4

    .line 2582407
    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582408
    const v7, 0x7f020c3d

    const-string v8, "group_feed"

    move-object v3, p1

    move v6, v5

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILjava/lang/String;)V

    .line 2582409
    const v3, 0x7f081b74

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b0050

    invoke-static {v9, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582410
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582411
    const/4 v3, 0x0

    const/4 v4, 0x3

    const v5, 0x7f0b004e

    invoke-static {v9, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582412
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    .line 2582413
    const/4 v3, 0x1

    .line 2582414
    :goto_c
    move v0, v3

    .line 2582415
    goto/16 :goto_0

    .line 2582416
    :pswitch_7
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2582417
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_35

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-eqz v3, :cond_35

    .line 2582418
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->p()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2582419
    if-eqz v3, :cond_34

    move v3, v9

    :goto_d
    if-eqz v3, :cond_37

    .line 2582420
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->p()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2582421
    invoke-virtual {v4, v3, v10}, LX/15i;->j(II)I

    move-result v3

    if-lez v3, :cond_36

    move v3, v9

    :goto_e
    if-eqz v3, :cond_38

    .line 2582422
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2582423
    const v3, 0x7f0b0e87

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2582424
    const v3, 0x7f0b00b2

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2582425
    mul-int/lit8 v3, v5, 0x2

    add-int/2addr v3, v4

    .line 2582426
    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582427
    const v3, 0x7f0d0105

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setId(I)V

    .line 2582428
    const v7, 0x7f020c37

    const-string v8, "group_admin_cog_icon"

    move-object v3, p1

    move v6, v5

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILjava/lang/String;)V

    .line 2582429
    const v3, 0x7f081b72

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b0050

    invoke-static {v11, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582430
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->p()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v4, v3, v10}, LX/15i;->j(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582431
    const/4 v3, 0x0

    const/4 v4, 0x3

    const v5, 0x7f0b004e

    invoke-static {v11, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582432
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    .line 2582433
    :goto_f
    move v0, v9

    .line 2582434
    goto/16 :goto_0

    .line 2582435
    :pswitch_8
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2582436
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_3a

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-eqz v3, :cond_3a

    .line 2582437
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->r()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2582438
    if-eqz v3, :cond_39

    move v3, v9

    :goto_10
    if-eqz v3, :cond_3c

    .line 2582439
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->r()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2582440
    invoke-virtual {v4, v3, v10}, LX/15i;->j(II)I

    move-result v3

    if-lez v3, :cond_3b

    move v3, v9

    :goto_11
    if-eqz v3, :cond_3d

    .line 2582441
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2582442
    const v3, 0x7f0b0e87

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2582443
    const v3, 0x7f0b00b2

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2582444
    mul-int/lit8 v3, v5, 0x2

    add-int/2addr v3, v4

    .line 2582445
    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582446
    const v7, 0x7f020c37

    const-string v8, "group_feed"

    move-object v3, p1

    move v6, v5

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILjava/lang/String;)V

    .line 2582447
    const v3, 0x7f081b7b

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b0050

    invoke-static {v11, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582448
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->r()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v4, v3, v10}, LX/15i;->j(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582449
    const/4 v3, 0x0

    const/4 v4, 0x3

    const v5, 0x7f0b004e

    invoke-static {v11, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582450
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    .line 2582451
    :goto_12
    move v0, v9

    .line 2582452
    goto/16 :goto_0

    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 2582453
    :cond_4
    const/4 v5, 0x0

    .line 2582454
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v3, 0x0

    move v6, v3

    :goto_13
    if-ge v6, v8, :cond_14

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;

    .line 2582455
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->c()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2582456
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->a()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    .line 2582457
    if-eqz v4, :cond_6

    const/4 v4, 0x1

    :goto_14
    if-eqz v4, :cond_9

    .line 2582458
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->a()LX/1vs;

    move-result-object v4

    iget-object v11, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2582459
    const/4 v12, 0x0

    invoke-virtual {v11, v4, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    :goto_15
    if-eqz v4, :cond_a

    .line 2582460
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->a()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2582461
    :goto_16
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 2582462
    const v4, 0x7f0b0e87

    invoke-virtual {v11, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2582463
    const v5, 0x7f0b00b2

    invoke-virtual {v11, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2582464
    mul-int/lit8 v6, v5, 0x2

    add-int/2addr v6, v4

    .line 2582465
    invoke-virtual {p1, v6}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582466
    if-eqz v3, :cond_b

    .line 2582467
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const-string v8, "group_feed"

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIILandroid/net/Uri;Ljava/lang/String;)V

    .line 2582468
    :goto_17
    const-string v3, ""

    .line 2582469
    invoke-static {p0, p2}, LX/IVl;->a(LX/IVl;LX/IVj;)Z

    move-result v4

    .line 2582470
    const/4 v5, 0x3

    if-le v9, v5, :cond_d

    .line 2582471
    if-eqz v4, :cond_c

    const v3, 0x7f081b87

    .line 2582472
    :goto_18
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->A()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2582473
    :cond_5
    :goto_19
    const/4 v4, 0x2

    const v5, 0x7f0b0050

    invoke-static {v11, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582474
    const/4 v3, 0x0

    const/4 v4, 0x3

    const v5, 0x7f0b004e

    invoke-static {v11, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582475
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582476
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    .line 2582477
    const/4 v3, 0x1

    goto/16 :goto_4

    .line 2582478
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_14

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_14

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_15

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_15

    .line 2582479
    :cond_a
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto/16 :goto_13

    .line 2582480
    :cond_b
    const v7, 0x7f020c39

    const-string v8, "group_feed"

    move-object v3, p1

    move v6, v5

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIIILjava/lang/String;)V

    goto :goto_17

    .line 2582481
    :cond_c
    const v3, 0x7f081b83

    goto :goto_18

    .line 2582482
    :cond_d
    const/4 v5, 0x3

    if-ne v9, v5, :cond_f

    .line 2582483
    if-eqz v4, :cond_e

    const v3, 0x7f081b86

    .line 2582484
    :goto_1a
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_19

    .line 2582485
    :cond_e
    const v3, 0x7f081b82

    goto :goto_1a

    .line 2582486
    :cond_f
    const/4 v5, 0x2

    if-ne v9, v5, :cond_11

    .line 2582487
    if-eqz v4, :cond_10

    const v3, 0x7f081b85

    .line 2582488
    :goto_1b
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_19

    .line 2582489
    :cond_10
    const v3, 0x7f081b81

    goto :goto_1b

    .line 2582490
    :cond_11
    const/4 v5, 0x1

    if-ne v9, v5, :cond_5

    .line 2582491
    if-eqz v4, :cond_12

    const v3, 0x7f081b84

    .line 2582492
    :goto_1c
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_19

    .line 2582493
    :cond_12
    const v3, 0x7f081b80

    goto :goto_1c

    .line 2582494
    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_14
    move-object v3, v5

    goto/16 :goto_16

    :cond_15
    move v3, v9

    goto/16 :goto_3

    .line 2582495
    :pswitch_9
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 2582496
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 2582497
    const v0, 0x7f081b56

    new-array v3, v1, [Ljava/lang/Object;

    iget-object v6, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v2

    invoke-virtual {v5, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2582498
    const v0, 0x7f081b97

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->d()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v5, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 2582499
    :cond_16
    const v0, 0x7f081b57

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2582500
    const v0, 0x7f081b95

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 2582501
    :cond_17
    iget-object v0, p0, LX/IVl;->c:LX/3my;

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2582502
    const v0, 0x7f081b8c

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2582503
    const v0, 0x7f081b8d

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2582504
    invoke-static {p0, p1}, LX/IVl;->a(LX/IVl;Landroid/view/View;)V

    goto/16 :goto_5

    .line 2582505
    :cond_18
    invoke-static {p0, p2}, LX/IVl;->a(LX/IVl;LX/IVj;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 2582506
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->t()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2582507
    if-eqz v0, :cond_19

    move v0, v1

    :goto_1d
    if-eqz v0, :cond_1b

    .line 2582508
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->t()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2582509
    const v0, 0x7f081b96

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_19
    move v0, v2

    .line 2582510
    goto :goto_1d

    :cond_1a
    move v0, v2

    goto :goto_1d

    .line 2582511
    :cond_1b
    const v0, 0x7f081b59

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2582512
    const v0, 0x7f081b95

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 2582513
    :pswitch_a
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 2582514
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->t()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2582515
    const v0, 0x7f081b94

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 2582516
    :cond_1c
    const v0, 0x7f081b55

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2582517
    const v0, 0x7f081b93

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 2582518
    :pswitch_b
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->E()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 2582519
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->t()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2582520
    if-eqz v0, :cond_1d

    move v0, v1

    :goto_1e
    if-eqz v0, :cond_1f

    .line 2582521
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->t()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2582522
    :goto_1f
    const v3, 0x7f081b92

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v8, v3

    move-object v3, v0

    move-object v0, v8

    goto/16 :goto_5

    :cond_1d
    move v0, v2

    .line 2582523
    goto :goto_1e

    :cond_1e
    move v0, v2

    goto :goto_1e

    .line 2582524
    :cond_1f
    const v0, 0x7f081b5b

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1f

    .line 2582525
    :cond_20
    const v2, 0x7f0b0e87

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2582526
    const v6, 0x7f0b00b2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2582527
    mul-int/lit8 v7, v6, 0x2

    add-int/2addr v2, v7

    .line 2582528
    invoke-virtual {p1, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582529
    new-instance v2, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 2582530
    sget-object v7, LX/IVi;->b:[I

    iget-object v8, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_2

    .line 2582531
    :goto_20
    const v7, -0x958e80

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2582532
    div-int/lit8 v7, v6, 0x2

    div-int/lit8 v8, v6, 0x2

    invoke-virtual {v2, v7, v6, v8, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setPadding(IIII)V

    .line 2582533
    move-object v2, v2

    .line 2582534
    invoke-virtual {p1, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Landroid/view/View;)V

    .line 2582535
    const/4 v2, 0x2

    const v6, 0x7f0b0050

    invoke-static {v5, v6}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v6

    invoke-virtual {p1, v3, v2, v6}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582536
    const/4 v2, 0x3

    const v3, 0x7f0b004e

    invoke-static {v5, v3}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v3

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582537
    invoke-virtual {p1, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582538
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    move v2, v1

    .line 2582539
    goto/16 :goto_6

    .line 2582540
    :pswitch_c
    iget-object v7, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v7

    if-eqz v7, :cond_24

    .line 2582541
    iget-object v7, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v7, v8, :cond_21

    .line 2582542
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02078b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_20

    .line 2582543
    :cond_21
    iget-object v7, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v7, v8, :cond_22

    .line 2582544
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0207f5

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_20

    .line 2582545
    :cond_22
    iget-object v7, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v7, v8, :cond_23

    .line 2582546
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02084d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_20

    .line 2582547
    :cond_23
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0208b2

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_20

    .line 2582548
    :cond_24
    iget-object v7, p0, LX/IVl;->c:LX/3my;

    iget-object v8, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v7

    if-eqz v7, :cond_25

    .line 2582549
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020ec4

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_20

    .line 2582550
    :cond_25
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0208b2

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_20

    .line 2582551
    :pswitch_d
    iget-object v7, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v7, v8, :cond_26

    .line 2582552
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02084d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_20

    .line 2582553
    :cond_26
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020917

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_20

    .line 2582554
    :pswitch_e
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020914

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_20

    .line 2582555
    :cond_27
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_29

    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_29

    move v0, v1

    .line 2582556
    :goto_21
    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_28

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v3

    if-eqz v3, :cond_28

    iget-object v3, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel$InviterModel;

    move-result-object v3

    if-eqz v3, :cond_28

    move v2, v1

    .line 2582557
    :cond_28
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2582558
    const v4, 0x7f0b0e87

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2582559
    const v5, 0x7f0b00b2

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2582560
    mul-int/lit8 v6, v5, 0x2

    add-int/2addr v4, v6

    .line 2582561
    invoke-virtual {p1, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2582562
    if-eqz v2, :cond_2a

    if-eqz v0, :cond_2a

    .line 2582563
    iget-object v0, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->w()Ljava/lang/String;

    move-result-object v0

    .line 2582564
    :goto_22
    new-instance v2, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 2582565
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0208ed

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2582566
    const v4, -0x958e80

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2582567
    div-int/lit8 v4, v5, 0x2

    div-int/lit8 v6, v5, 0x2

    invoke-virtual {v2, v4, v5, v6, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setPadding(IIII)V

    .line 2582568
    move-object v2, v2

    .line 2582569
    invoke-virtual {p1, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Landroid/view/View;)V

    .line 2582570
    const/4 v2, 0x2

    const v4, 0x7f0b0050

    invoke-static {v3, v4}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v4

    invoke-virtual {p1, v0, v2, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2582571
    const/4 v0, 0x3

    const v2, 0x7f0b004e

    invoke-static {v3, v2}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v2

    invoke-virtual {p1, v7, v0, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2582572
    invoke-virtual {p1, v7}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2582573
    invoke-static {p0, p1, p2}, LX/IVl;->k(LX/IVl;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/IVj;)V

    goto/16 :goto_7

    :cond_29
    move v0, v2

    .line 2582574
    goto/16 :goto_21

    .line 2582575
    :cond_2a
    invoke-static {p0, p2}, LX/IVl;->a(LX/IVl;LX/IVj;)Z

    move-result v0

    if-eqz v0, :cond_2b

    const v0, 0x7f081b76

    :goto_23
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_22

    :cond_2b
    iget-object v0, p0, LX/IVl;->c:LX/3my;

    iget-object v2, p2, LX/IVj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-eqz v0, :cond_2c

    const v0, 0x7f081b77

    goto :goto_23

    :cond_2c
    const v0, 0x7f081b75

    goto :goto_23

    :cond_2d
    move v3, v9

    goto/16 :goto_8

    :cond_2e
    move v3, v10

    .line 2582576
    goto/16 :goto_9

    :cond_2f
    move v3, v10

    goto/16 :goto_9

    :cond_30
    move v3, v10

    goto/16 :goto_a

    :cond_31
    move v3, v10

    goto/16 :goto_a

    :cond_32
    move v9, v10

    .line 2582577
    goto/16 :goto_b

    :cond_33
    const/4 v3, 0x0

    goto/16 :goto_c

    :cond_34
    move v3, v10

    .line 2582578
    goto/16 :goto_d

    :cond_35
    move v3, v10

    goto/16 :goto_d

    :cond_36
    move v3, v10

    goto/16 :goto_e

    :cond_37
    move v3, v10

    goto/16 :goto_e

    :cond_38
    move v9, v10

    .line 2582579
    goto/16 :goto_f

    :cond_39
    move v3, v10

    .line 2582580
    goto/16 :goto_10

    :cond_3a
    move v3, v10

    goto/16 :goto_10

    :cond_3b
    move v3, v10

    goto/16 :goto_11

    :cond_3c
    move v3, v10

    goto/16 :goto_11

    :cond_3d
    move v9, v10

    .line 2582581
    goto/16 :goto_12

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
