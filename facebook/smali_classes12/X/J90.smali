.class public LX/J90;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/J90;


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/1mR;

.field public final c:LX/JAU;

.field private final d:LX/J9L;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/0sX;


# direct methods
.method public constructor <init>(LX/0tX;LX/1mR;LX/JAU;LX/J9L;Ljava/util/concurrent/ExecutorService;LX/0sX;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652360
    iput-object p1, p0, LX/J90;->a:LX/0tX;

    .line 2652361
    iput-object p2, p0, LX/J90;->b:LX/1mR;

    .line 2652362
    iput-object p3, p0, LX/J90;->c:LX/JAU;

    .line 2652363
    iput-object p4, p0, LX/J90;->d:LX/J9L;

    .line 2652364
    iput-object p5, p0, LX/J90;->e:Ljava/util/concurrent/ExecutorService;

    .line 2652365
    iput-object p6, p0, LX/J90;->f:LX/0sX;

    .line 2652366
    return-void
.end method

.method public static a(LX/J90;LX/0gW;Ljava/lang/String;IZLX/0am;)LX/0gW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW;",
            "Ljava/lang/String;",
            "IZ",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0gW;"
        }
    .end annotation

    .prologue
    .line 2652367
    const-string v0, "collection_list_item_image_size"

    iget-object v1, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v1}, LX/JAU;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "collection_table_item_image_size"

    iget-object v2, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v2}, LX/JAU;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "collections"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "collections_after"

    invoke-virtual {p5}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "collections_per_app"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "facepile_image_size"

    iget-object v2, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v2}, LX/JAU;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "is_requestable_fields_enabled"

    invoke-static {p4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "items_per_collection"

    iget-object v2, p0, LX/J90;->d:LX/J9L;

    invoke-virtual {v2}, LX/J9L;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "num_collections"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "num_mutual_friends"

    const-string v2, "4"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "suggestions_after"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "suggestions_per_collection"

    .line 2652368
    const/4 v2, 0x4

    move v2, v2

    .line 2652369
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "automatic_photo_captioning_enabled"

    iget-object v2, p0, LX/J90;->f:LX/0sX;

    invoke-virtual {v2}, LX/0sX;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2652370
    return-object p1
.end method

.method public static a(LX/0QB;)LX/J90;
    .locals 10

    .prologue
    .line 2652371
    sget-object v0, LX/J90;->g:LX/J90;

    if-nez v0, :cond_1

    .line 2652372
    const-class v1, LX/J90;

    monitor-enter v1

    .line 2652373
    :try_start_0
    sget-object v0, LX/J90;->g:LX/J90;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2652374
    if-eqz v2, :cond_0

    .line 2652375
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2652376
    new-instance v3, LX/J90;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v5

    check-cast v5, LX/1mR;

    invoke-static {v0}, LX/JAU;->a(LX/0QB;)LX/JAU;

    move-result-object v6

    check-cast v6, LX/JAU;

    invoke-static {v0}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object v7

    check-cast v7, LX/J9L;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v9

    check-cast v9, LX/0sX;

    invoke-direct/range {v3 .. v9}, LX/J90;-><init>(LX/0tX;LX/1mR;LX/JAU;LX/J9L;Ljava/util/concurrent/ExecutorService;LX/0sX;)V

    .line 2652377
    move-object v0, v3

    .line 2652378
    sput-object v0, LX/J90;->g:LX/J90;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2652379
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2652380
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2652381
    :cond_1
    sget-object v0, LX/J90;->g:LX/J90;

    return-object v0

    .line 2652382
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2652383
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;IZLjava/lang/String;)LX/JBI;
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2652384
    new-instance v0, LX/JBI;

    invoke-direct {v0}, LX/JBI;-><init>()V

    move-object v1, v0

    .line 2652385
    invoke-static {p4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-static/range {v0 .. v5}, LX/J90;->a(LX/J90;LX/0gW;Ljava/lang/String;IZLX/0am;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/JBI;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)LX/JBh;
    .locals 4

    .prologue
    .line 2652386
    new-instance v0, LX/JBh;

    invoke-direct {v0}, LX/JBh;-><init>()V

    move-object v0, v0

    .line 2652387
    const-string v1, "collection_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "collection_list_item_image_size"

    iget-object v3, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v3}, LX/JAU;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "collection_table_item_image_size"

    iget-object v3, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v3}, LX/JAU;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "facepile_image_size"

    iget-object v3, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v3}, LX/JAU;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "items_after"

    invoke-virtual {v1, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "items_per_collection"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/J90;->f:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2652388
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;II)LX/JBp;
    .locals 4

    .prologue
    .line 2652389
    new-instance v0, LX/JBp;

    invoke-direct {v0}, LX/JBp;-><init>()V

    move-object v0, v0

    .line 2652390
    const-string v1, "app_section_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "collection_id"

    invoke-virtual {v1, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "collection_list_item_image_size"

    iget-object v3, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v3}, LX/JAU;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "collection_table_item_image_size"

    iget-object v3, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v3}, LX/JAU;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "facepile_image_size"

    iget-object v3, p0, LX/J90;->c:LX/JAU;

    invoke-virtual {v3}, LX/JAU;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "items_per_collection"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "suggestions_per_collection"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "suggestions_after"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/J90;->f:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2652391
    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2652392
    iget-object v0, p0, LX/J90;->b:LX/1mR;

    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2652393
    return-void
.end method
