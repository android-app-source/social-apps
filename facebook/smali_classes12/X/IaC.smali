.class public final LX/IaC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V
    .locals 0

    .prologue
    .line 2590706
    iput-object p1, p0, LX/IaC;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2590707
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2590708
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2590709
    iget-object v0, p0, LX/IaC;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->h:Lcom/facebook/widget/text/BetterButton;

    iget-object v1, p0, LX/IaC;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2590710
    return-void
.end method
