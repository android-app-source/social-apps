.class public final enum LX/JPM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JPM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JPM;

.field public static final enum ACTIVITY_INSIGHTS:LX/JPM;

.field public static final enum ADS_ACTIVE_CAMPAIGN_INSIGHTS:LX/JPM;

.field public static final enum ADS_COMPLETED_CAMPAIGN_INSIGHTS:LX/JPM;

.field public static final enum ADS_MULTI_CAMPAIGNS_INSIGHTS:LX/JPM;

.field public static final enum AYMT:LX/JPM;

.field public static final enum LIKE_FOLLOWER:LX/JPM;

.field public static final enum LIKE_INSIGHTS:LX/JPM;

.field public static final enum MULTI_PAGE:LX/JPM;

.field public static final enum POST_ENGAGEMENT:LX/JPM;

.field public static final enum REACH_INSIGHTS:LX/JPM;

.field public static final enum RESPONSE_INSIGHTS:LX/JPM;

.field public static final enum VIDEO_INSIGHTS:LX/JPM;

.field public static final enum VIEW_INSIGHTS:LX/JPM;

.field public static final enum VISIT_PAGE:LX/JPM;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2689435
    new-instance v0, LX/JPM;

    const-string v1, "ACTIVITY_INSIGHTS"

    invoke-direct {v0, v1, v3}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->ACTIVITY_INSIGHTS:LX/JPM;

    .line 2689436
    new-instance v0, LX/JPM;

    const-string v1, "AYMT"

    invoke-direct {v0, v1, v4}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->AYMT:LX/JPM;

    .line 2689437
    new-instance v0, LX/JPM;

    const-string v1, "LIKE_FOLLOWER"

    invoke-direct {v0, v1, v5}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->LIKE_FOLLOWER:LX/JPM;

    .line 2689438
    new-instance v0, LX/JPM;

    const-string v1, "LIKE_INSIGHTS"

    invoke-direct {v0, v1, v6}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->LIKE_INSIGHTS:LX/JPM;

    .line 2689439
    new-instance v0, LX/JPM;

    const-string v1, "MULTI_PAGE"

    invoke-direct {v0, v1, v7}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->MULTI_PAGE:LX/JPM;

    .line 2689440
    new-instance v0, LX/JPM;

    const-string v1, "POST_ENGAGEMENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->POST_ENGAGEMENT:LX/JPM;

    .line 2689441
    new-instance v0, LX/JPM;

    const-string v1, "REACH_INSIGHTS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->REACH_INSIGHTS:LX/JPM;

    .line 2689442
    new-instance v0, LX/JPM;

    const-string v1, "RESPONSE_INSIGHTS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->RESPONSE_INSIGHTS:LX/JPM;

    .line 2689443
    new-instance v0, LX/JPM;

    const-string v1, "VIDEO_INSIGHTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->VIDEO_INSIGHTS:LX/JPM;

    .line 2689444
    new-instance v0, LX/JPM;

    const-string v1, "VIEW_INSIGHTS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->VIEW_INSIGHTS:LX/JPM;

    .line 2689445
    new-instance v0, LX/JPM;

    const-string v1, "VISIT_PAGE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->VISIT_PAGE:LX/JPM;

    .line 2689446
    new-instance v0, LX/JPM;

    const-string v1, "ADS_ACTIVE_CAMPAIGN_INSIGHTS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->ADS_ACTIVE_CAMPAIGN_INSIGHTS:LX/JPM;

    .line 2689447
    new-instance v0, LX/JPM;

    const-string v1, "ADS_COMPLETED_CAMPAIGN_INSIGHTS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->ADS_COMPLETED_CAMPAIGN_INSIGHTS:LX/JPM;

    .line 2689448
    new-instance v0, LX/JPM;

    const-string v1, "ADS_MULTI_CAMPAIGNS_INSIGHTS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/JPM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JPM;->ADS_MULTI_CAMPAIGNS_INSIGHTS:LX/JPM;

    .line 2689449
    const/16 v0, 0xe

    new-array v0, v0, [LX/JPM;

    sget-object v1, LX/JPM;->ACTIVITY_INSIGHTS:LX/JPM;

    aput-object v1, v0, v3

    sget-object v1, LX/JPM;->AYMT:LX/JPM;

    aput-object v1, v0, v4

    sget-object v1, LX/JPM;->LIKE_FOLLOWER:LX/JPM;

    aput-object v1, v0, v5

    sget-object v1, LX/JPM;->LIKE_INSIGHTS:LX/JPM;

    aput-object v1, v0, v6

    sget-object v1, LX/JPM;->MULTI_PAGE:LX/JPM;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/JPM;->POST_ENGAGEMENT:LX/JPM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/JPM;->REACH_INSIGHTS:LX/JPM;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/JPM;->RESPONSE_INSIGHTS:LX/JPM;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/JPM;->VIDEO_INSIGHTS:LX/JPM;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/JPM;->VIEW_INSIGHTS:LX/JPM;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/JPM;->VISIT_PAGE:LX/JPM;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/JPM;->ADS_ACTIVE_CAMPAIGN_INSIGHTS:LX/JPM;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/JPM;->ADS_COMPLETED_CAMPAIGN_INSIGHTS:LX/JPM;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/JPM;->ADS_MULTI_CAMPAIGNS_INSIGHTS:LX/JPM;

    aput-object v2, v0, v1

    sput-object v0, LX/JPM;->$VALUES:[LX/JPM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2689450
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JPM;
    .locals 1

    .prologue
    .line 2689451
    const-class v0, LX/JPM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JPM;

    return-object v0
.end method

.method public static values()[LX/JPM;
    .locals 1

    .prologue
    .line 2689452
    sget-object v0, LX/JPM;->$VALUES:[LX/JPM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JPM;

    return-object v0
.end method
