.class public final LX/JCy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JA0;

.field public final synthetic b:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V
    .locals 0

    .prologue
    .line 2662968
    iput-object p1, p0, LX/JCy;->b:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    iput-object p2, p0, LX/JCy;->a:LX/JA0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x68356bbe

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662969
    sget-object v1, LX/JD4;->a:[I

    iget-object v2, p0, LX/JCy;->a:LX/JA0;

    invoke-virtual {v2}, LX/JA0;->g()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2662970
    iget-object v1, p0, LX/JCy;->b:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->e:LX/03V;

    const-string v2, "ListCollectionItemView"

    const-string v3, "unknown subscribe status"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662971
    :goto_0
    const v1, 0x54dab697

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2662972
    :pswitch_0
    iget-object v1, p0, LX/JCy;->b:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    iget-object v2, p0, LX/JCy;->a:LX/JA0;

    invoke-static {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->c(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V

    goto :goto_0

    .line 2662973
    :pswitch_1
    iget-object v1, p0, LX/JCy;->b:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    iget-object v2, p0, LX/JCy;->a:LX/JA0;

    invoke-static {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->d(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
