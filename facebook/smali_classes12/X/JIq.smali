.class public final LX/JIq;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/JIl;
.implements LX/6WM;


# instance fields
.field public final l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private final m:LX/JIw;

.field private final n:LX/JJ0;

.field public o:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

.field public p:Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/fig/mediagrid/FigMediaGrid;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/timeline/protiles/views/ProtilesFooterView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Landroid/widget/TextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;LX/JIw;LX/JJ0;)V
    .locals 0

    .prologue
    .line 2678382
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2678383
    iput-object p1, p0, LX/JIq;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2678384
    iput-object p2, p0, LX/JIq;->m:LX/JIw;

    .line 2678385
    iput-object p3, p0, LX/JIq;->n:LX/JJ0;

    .line 2678386
    return-void
.end method


# virtual methods
.method public final A()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 2678380
    iget-boolean v0, p0, LX/JIq;->t:Z

    const-string v1, "called getFooterViewText before inflating"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2678381
    iget-object v0, p0, LX/JIq;->s:Landroid/widget/TextView;

    return-object v0
.end method

.method public final a(ILX/1aZ;Landroid/graphics/Rect;)V
    .locals 8

    .prologue
    .line 2678378
    iget-object v0, p0, LX/JIq;->m:LX/JIw;

    iget-object v1, p0, LX/JIq;->q:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iget-object v2, p0, LX/JIq;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    invoke-static {v2}, LX/JJ0;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;)LX/0Px;

    move-result-object v3

    iget-object v4, p0, LX/JIq;->n:LX/JJ0;

    sget-object v7, LX/Emo;->PHOTO_PROTILE_PHOTO:LX/Emo;

    move v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v7}, LX/JIw;->a(Lcom/facebook/fig/mediagrid/FigMediaGrid;ILX/0Px;LX/Fx7;LX/1aZ;Landroid/graphics/Rect;LX/Emo;)V

    .line 2678379
    return-void
.end method

.method public final a(LX/0Px;LX/6WP;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1aZ;",
            ">;",
            "LX/6WP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2678387
    iget-boolean v0, p0, LX/JIq;->t:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "Called setItems before inflating"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2678388
    iget-object v0, p0, LX/JIq;->q:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(LX/0Px;LX/6WP;)Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2678389
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2678375
    iget-boolean v0, p0, LX/JIq;->t:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "Called setMediaGridVisibility before inflating"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2678376
    iget-object v0, p0, LX/JIq;->q:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->setVisibility(I)V

    .line 2678377
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0xe0c9ce3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2678363
    iget-object v0, p0, LX/JIq;->p:Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/JIq;->r:Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    if-ne p1, v0, :cond_1

    .line 2678364
    :cond_0
    iget-object v0, p0, LX/JIq;->p:Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    if-ne p1, v0, :cond_2

    sget-object v0, LX/Emo;->PHOTO_PROTILE_HEADER:LX/Emo;

    .line 2678365
    :goto_0
    iget-object v2, p0, LX/JIq;->m:LX/JIw;

    .line 2678366
    iget-object v3, v2, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, v2, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 2678367
    :cond_1
    :goto_1
    const v0, -0x46a3bd71

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2678368
    :cond_2
    sget-object v0, LX/Emo;->PHOTO_PROTILE_FOOTER:LX/Emo;

    goto :goto_0

    .line 2678369
    :cond_3
    iget-object v3, v2, LX/JIw;->o:LX/Emj;

    iget-object v4, v2, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v7

    iget-object v8, v2, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-object v4, v0

    invoke-interface/range {v3 .. v8}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2678370
    sget-object v3, LX/0ax;->bZ:Ljava/lang/String;

    iget-object v4, v2, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2678371
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2678372
    const-string v3, "profile_name"

    iget-object v6, v2, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v6}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->ce_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2678373
    iget-object v3, v2, LX/JIw;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17W;

    .line 2678374
    iget-object v6, v2, LX/JIw;->k:Landroid/content/Context;

    invoke-virtual {v3, v6, v4, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1
.end method
