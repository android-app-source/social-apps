.class public final LX/I1u;
.super LX/BlD;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V
    .locals 0

    .prologue
    .line 2529441
    iput-object p1, p0, LX/I1u;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    invoke-direct {p0}, LX/BlD;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2529442
    check-cast p1, LX/BlC;

    .line 2529443
    iget-object v0, p0, LX/I1u;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->k:LX/I1o;

    iget-object v1, p1, LX/BlC;->a:Ljava/lang/String;

    .line 2529444
    iget-object v2, v0, LX/I1o;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2529445
    iget-object v2, v0, LX/I1o;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2529446
    iget-object p0, v0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {p0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2529447
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2529448
    const/4 v2, 0x0

    move p0, v2

    :goto_0
    iget-object v2, v0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p0, v2, :cond_0

    .line 2529449
    iget-object p1, v0, LX/I1o;->c:Ljava/util/HashMap;

    iget-object v2, v0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/Event;

    .line 2529450
    iget-object v1, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v1

    .line 2529451
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2529452
    add-int/lit8 v2, p0, 0x1

    move p0, v2

    goto :goto_0

    .line 2529453
    :cond_0
    return-void
.end method
