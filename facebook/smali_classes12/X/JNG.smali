.class public LX/JNG;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JNI;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNG",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JNI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685486
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2685487
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JNG;->b:LX/0Zi;

    .line 2685488
    iput-object p1, p0, LX/JNG;->a:LX/0Ot;

    .line 2685489
    return-void
.end method

.method public static a(LX/0QB;)LX/JNG;
    .locals 4

    .prologue
    .line 2685529
    const-class v1, LX/JNG;

    monitor-enter v1

    .line 2685530
    :try_start_0
    sget-object v0, LX/JNG;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685531
    sput-object v2, LX/JNG;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685532
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685533
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685534
    new-instance v3, LX/JNG;

    const/16 p0, 0x1ee3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNG;-><init>(LX/0Ot;)V

    .line 2685535
    move-object v0, v3

    .line 2685536
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685537
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685538
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685539
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 13

    .prologue
    .line 2685540
    check-cast p2, LX/JNF;

    .line 2685541
    iget-object v0, p0, LX/JNG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JNI;

    iget-object v1, p2, LX/JNF;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    iget-object v2, p2, LX/JNF;->b:Ljava/lang/String;

    iget-object v3, p2, LX/JNF;->c:Ljava/lang/String;

    .line 2685542
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v4

    .line 2685543
    if-nez v4, :cond_0

    .line 2685544
    :goto_0
    return-void

    .line 2685545
    :cond_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v5

    .line 2685546
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v6

    .line 2685547
    if-eqz v5, :cond_3

    .line 2685548
    iget-object v5, v0, LX/JNI;->b:LX/7vW;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v7

    .line 2685549
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v7, v8, :cond_1

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v7, v8, :cond_5

    .line 2685550
    :cond_1
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2685551
    :goto_1
    move-object v7, v8

    .line 2685552
    const-string v8, "unknown"

    iget-object v9, v0, LX/JNI;->h:LX/0kx;

    const-string v10, "native_newsfeed"

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/facebook/events/common/ActionMechanism;->EVENTS_SUGGESTION_UNIT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v5 .. v10}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2685553
    :goto_2
    if-eqz v5, :cond_2

    .line 2685554
    iget-object v6, v0, LX/JNI;->d:LX/1Ck;

    new-instance v7, LX/JNH;

    invoke-direct {v7, v0}, LX/JNH;-><init>(LX/JNI;)V

    invoke-virtual {v6, v0, v5, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2685555
    :cond_2
    goto :goto_0

    .line 2685556
    :cond_3
    iget-object v5, v0, LX/JNI;->c:LX/7vZ;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v7

    .line 2685557
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v7, v8, :cond_4

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v7, v8, :cond_6

    .line 2685558
    :cond_4
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2685559
    :goto_3
    move-object v7, v8

    .line 2685560
    const-string v8, "unknown"

    iget-object v9, v0, LX/JNI;->h:LX/0kx;

    const-string v10, "native_newsfeed"

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/facebook/events/common/ActionMechanism;->EVENTS_SUGGESTION_UNIT:Lcom/facebook/events/common/ActionMechanism;

    invoke-static {v3}, LX/JNI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object v11, v2

    invoke-virtual/range {v5 .. v12}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto :goto_2

    :cond_5
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_1

    :cond_6
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2685507
    check-cast p2, LX/JNF;

    .line 2685508
    iget-object v0, p0, LX/JNG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JNI;

    iget-object v1, p2, LX/JNF;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    const/4 v7, 0x1

    const v8, -0x6f6b64

    .line 2685509
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    .line 2685510
    if-nez v2, :cond_0

    .line 2685511
    const/4 v2, 0x0

    .line 2685512
    :goto_0
    move-object v0, v2

    .line 2685513
    return-object v0

    .line 2685514
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/Bne;->c(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)I

    move-result v4

    .line 2685515
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v6

    invoke-static {v3, v5, v6}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v3

    .line 2685516
    if-eqz v3, :cond_1

    const v5, -0xa76f01

    .line 2685517
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    .line 2685518
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v3, v9, :cond_2

    .line 2685519
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v9, :cond_3

    .line 2685520
    const v9, 0x7f0207d6

    .line 2685521
    :goto_2
    move v9, v9

    .line 2685522
    :goto_3
    move v6, v9

    .line 2685523
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0b64

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v9

    move-object v2, v0

    move-object v3, p1

    invoke-static/range {v2 .. v7}, LX/JNI;->a(LX/JNI;LX/1De;IIIZ)LX/1Di;

    move-result-object v2

    invoke-interface {v9, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    const v4, 0x7f0812ad

    const v6, 0x7f0209c5

    const/4 v7, 0x0

    move-object v2, v0

    move-object v3, p1

    move v5, v8

    invoke-static/range {v2 .. v7}, LX/JNI;->a(LX/JNI;LX/1De;IIIZ)LX/1Di;

    move-result-object v2

    invoke-interface {v9, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0

    :cond_1
    move v5, v8

    .line 2685524
    goto :goto_1

    .line 2685525
    :cond_2
    invoke-static {v6}, LX/Bne;->j(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2685526
    const v9, 0x7f0207d6

    .line 2685527
    :goto_4
    move v9, v9

    .line 2685528
    goto :goto_3

    :cond_3
    const v9, 0x7f02085a

    goto :goto_2

    :cond_4
    const v9, 0x7f020855

    goto :goto_4
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2685490
    invoke-static {}, LX/1dS;->b()V

    .line 2685491
    iget v0, p1, LX/1dQ;->b:I

    .line 2685492
    sparse-switch v0, :sswitch_data_0

    .line 2685493
    :goto_0
    return-object v2

    .line 2685494
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2685495
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/JNG;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 2685496
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2685497
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2685498
    check-cast v1, LX/JNF;

    .line 2685499
    iget-object v3, p0, LX/JNG;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JNI;

    iget-object v4, v1, LX/JNF;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    iget-object v5, v1, LX/JNF;->c:Ljava/lang/String;

    .line 2685500
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v6

    .line 2685501
    if-nez v6, :cond_0

    .line 2685502
    :goto_1
    goto :goto_0

    .line 2685503
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 2685504
    iget-object p1, v3, LX/JNI;->g:LX/1nQ;

    iget-object p2, v3, LX/JNI;->h:LX/0kx;

    const-string p0, "native_newsfeed"

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    sget-object p0, Lcom/facebook/events/common/ActionMechanism;->EVENTS_SUGGESTION_UNIT:Lcom/facebook/events/common/ActionMechanism;

    invoke-static {v5}, LX/JNI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p2, p0, v1}, LX/1nQ;->b(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)V

    .line 2685505
    iget-object p1, v3, LX/JNI;->f:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1Kf;

    const/4 p2, 0x0

    sget-object p0, LX/21D;->NEWSFEED:LX/21D;

    const-string v1, "shareEvent"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v4, 0x403827a

    invoke-static {v0, v4}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p0

    invoke-interface {p1, p2, p0, v7}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2685506
    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2cc49489 -> :sswitch_0
        0x43bb1758 -> :sswitch_1
    .end sparse-switch
.end method
