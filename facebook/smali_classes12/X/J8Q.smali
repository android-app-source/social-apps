.class public LX/J8Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J8M;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/J8M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/J8M;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2652060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652061
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/J8Q;->a:Ljava/util/List;

    .line 2652062
    return-void
.end method

.method public varargs constructor <init>([LX/J8M;)V
    .locals 1

    .prologue
    .line 2652063
    invoke-static {p1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, LX/J8Q;-><init>(Ljava/util/List;)V

    .line 2652064
    return-void
.end method


# virtual methods
.method public final set()V
    .locals 2

    .prologue
    .line 2652065
    iget-object v0, p0, LX/J8Q;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J8M;

    .line 2652066
    invoke-interface {v0}, LX/J8M;->set()V

    goto :goto_0

    .line 2652067
    :cond_0
    return-void
.end method
