.class public final enum LX/I4j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I4j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I4j;

.field public static final enum MULTIPLE_SELECTION:LX/I4j;

.field public static final enum NO_SELECTION:LX/I4j;

.field public static final enum SINGLE_SELECTION:LX/I4j;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2534345
    new-instance v0, LX/I4j;

    const-string v1, "MULTIPLE_SELECTION"

    invoke-direct {v0, v1, v2}, LX/I4j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I4j;->MULTIPLE_SELECTION:LX/I4j;

    .line 2534346
    new-instance v0, LX/I4j;

    const-string v1, "SINGLE_SELECTION"

    invoke-direct {v0, v1, v3}, LX/I4j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I4j;->SINGLE_SELECTION:LX/I4j;

    .line 2534347
    new-instance v0, LX/I4j;

    const-string v1, "NO_SELECTION"

    invoke-direct {v0, v1, v4}, LX/I4j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I4j;->NO_SELECTION:LX/I4j;

    .line 2534348
    const/4 v0, 0x3

    new-array v0, v0, [LX/I4j;

    sget-object v1, LX/I4j;->MULTIPLE_SELECTION:LX/I4j;

    aput-object v1, v0, v2

    sget-object v1, LX/I4j;->SINGLE_SELECTION:LX/I4j;

    aput-object v1, v0, v3

    sget-object v1, LX/I4j;->NO_SELECTION:LX/I4j;

    aput-object v1, v0, v4

    sput-object v0, LX/I4j;->$VALUES:[LX/I4j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2534350
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I4j;
    .locals 1

    .prologue
    .line 2534351
    const-class v0, LX/I4j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I4j;

    return-object v0
.end method

.method public static values()[LX/I4j;
    .locals 1

    .prologue
    .line 2534349
    sget-object v0, LX/I4j;->$VALUES:[LX/I4j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I4j;

    return-object v0
.end method
