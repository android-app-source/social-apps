.class public final LX/IUi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IUk;


# direct methods
.method public constructor <init>(LX/IUk;)V
    .locals 0

    .prologue
    .line 2580967
    iput-object p1, p0, LX/IUi;->a:LX/IUk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x39b66e9c

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580968
    iget-object v1, p0, LX/IUi;->a:LX/IUk;

    invoke-static {v1}, LX/IUk;->g(LX/IUk;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2580969
    const v1, 0x413d14c8

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2580970
    :goto_0
    return-void

    .line 2580971
    :cond_0
    iget-object v1, p0, LX/IUi;->a:LX/IUk;

    iget-object v1, v1, LX/IUk;->d:LX/3mF;

    iget-object v2, p0, LX/IUi;->a:LX/IUk;

    iget-object v2, v2, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mobile_group_join"

    .line 2580972
    new-instance v4, LX/B26;

    invoke-direct {v4}, LX/B26;-><init>()V

    move-object v4, v4

    .line 2580973
    const-string v5, "input"

    new-instance v6, LX/4GI;

    invoke-direct {v6}, LX/4GI;-><init>()V

    .line 2580974
    const-string p1, "invite_id"

    invoke-virtual {v6, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580975
    move-object v6, v6

    .line 2580976
    const-string p1, "source"

    invoke-virtual {v6, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580977
    move-object v6, v6

    .line 2580978
    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2580979
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2580980
    iget-object v5, v1, LX/3mF;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 2580981
    iget-object v2, p0, LX/IUi;->a:LX/IUk;

    iget-object v2, v2, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2580982
    invoke-static {v2}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v2

    .line 2580983
    const/4 v3, 0x0

    iput-object v3, v2, LX/9OQ;->E:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    .line 2580984
    invoke-virtual {v2}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2580985
    iget-object v3, p0, LX/IUi;->a:LX/IUk;

    iget-object v4, p0, LX/IUi;->a:LX/IUk;

    iget-object v4, v4, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v3, v4, v2, v1}, LX/IUk;->a$redex0(LX/IUk;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2580986
    const v1, -0x4d2a7eb    # -9.000539E35f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
