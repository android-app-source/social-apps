.class public LX/JL9;
.super LX/9mi;
.source ""


# instance fields
.field public final A:LX/K4b;

.field public final a:LX/JJb;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0mh;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/JG4;

.field public final e:LX/GNq;

.field public final f:LX/JLJ;

.field public final g:LX/JJs;

.field public final h:LX/JJv;

.field public final i:LX/JJo;

.field public final j:LX/F5N;

.field public final k:LX/F5U;

.field public final l:LX/9DM;

.field public final m:LX/JJl;

.field public final n:LX/JK4;

.field public final o:LX/JLT;

.field public final p:LX/JLX;

.field private final q:LX/JLZ;

.field public final r:LX/JLd;

.field public final s:LX/JLf;

.field public final t:LX/JJh;

.field public final u:LX/JLE;

.field public final v:LX/JLL;

.field public final w:LX/JLk;

.field public final x:LX/JLp;

.field public final y:LX/JM2;

.field public final z:LX/JM6;


# direct methods
.method public constructor <init>(LX/JJb;LX/0Ot;LX/0Ot;LX/JG4;LX/GNq;LX/JLJ;LX/JJs;LX/JJv;LX/JJo;LX/F5N;LX/F5U;LX/9DM;LX/JJl;LX/JK4;LX/JLT;LX/JLX;LX/JLZ;LX/JLd;LX/JLf;LX/JJh;LX/JLE;LX/JLL;LX/JLk;LX/JLp;LX/JM2;LX/JM6;LX/K4b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JJb;",
            "LX/0Ot",
            "<",
            "LX/0mh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/JG4;",
            "LX/GNq;",
            "LX/JLJ;",
            "LX/JJs;",
            "LX/JJv;",
            "LX/JJo;",
            "LX/F5N;",
            "LX/F5U;",
            "LX/9DM;",
            "LX/JJl;",
            "LX/JK4;",
            "LX/JLT;",
            "LX/JLX;",
            "LX/JLZ;",
            "LX/JLd;",
            "LX/JLf;",
            "LX/JJh;",
            "LX/JLE;",
            "LX/JLL;",
            "LX/JLk;",
            "LX/JLp;",
            "LX/JM2;",
            "LX/JM6;",
            "LX/K4b;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680675
    invoke-direct {p0}, LX/9mi;-><init>()V

    .line 2680676
    iput-object p1, p0, LX/JL9;->a:LX/JJb;

    .line 2680677
    iput-object p2, p0, LX/JL9;->b:LX/0Ot;

    .line 2680678
    iput-object p3, p0, LX/JL9;->c:LX/0Ot;

    .line 2680679
    iput-object p4, p0, LX/JL9;->d:LX/JG4;

    .line 2680680
    iput-object p5, p0, LX/JL9;->e:LX/GNq;

    .line 2680681
    iput-object p6, p0, LX/JL9;->f:LX/JLJ;

    .line 2680682
    iput-object p7, p0, LX/JL9;->g:LX/JJs;

    .line 2680683
    iput-object p8, p0, LX/JL9;->h:LX/JJv;

    .line 2680684
    iput-object p9, p0, LX/JL9;->i:LX/JJo;

    .line 2680685
    iput-object p10, p0, LX/JL9;->j:LX/F5N;

    .line 2680686
    iput-object p11, p0, LX/JL9;->k:LX/F5U;

    .line 2680687
    iput-object p12, p0, LX/JL9;->l:LX/9DM;

    .line 2680688
    iput-object p13, p0, LX/JL9;->m:LX/JJl;

    .line 2680689
    iput-object p14, p0, LX/JL9;->n:LX/JK4;

    .line 2680690
    move-object/from16 v0, p15

    iput-object v0, p0, LX/JL9;->o:LX/JLT;

    .line 2680691
    move-object/from16 v0, p16

    iput-object v0, p0, LX/JL9;->p:LX/JLX;

    .line 2680692
    move-object/from16 v0, p17

    iput-object v0, p0, LX/JL9;->q:LX/JLZ;

    .line 2680693
    move-object/from16 v0, p18

    iput-object v0, p0, LX/JL9;->r:LX/JLd;

    .line 2680694
    move-object/from16 v0, p19

    iput-object v0, p0, LX/JL9;->s:LX/JLf;

    .line 2680695
    move-object/from16 v0, p20

    iput-object v0, p0, LX/JL9;->t:LX/JJh;

    .line 2680696
    move-object/from16 v0, p21

    iput-object v0, p0, LX/JL9;->u:LX/JLE;

    .line 2680697
    move-object/from16 v0, p22

    iput-object v0, p0, LX/JL9;->v:LX/JLL;

    .line 2680698
    move-object/from16 v0, p23

    iput-object v0, p0, LX/JL9;->w:LX/JLk;

    .line 2680699
    move-object/from16 v0, p24

    iput-object v0, p0, LX/JL9;->x:LX/JLp;

    .line 2680700
    move-object/from16 v0, p25

    iput-object v0, p0, LX/JL9;->y:LX/JM2;

    .line 2680701
    move-object/from16 v0, p26

    iput-object v0, p0, LX/JL9;->z:LX/JM6;

    .line 2680702
    move-object/from16 v0, p27

    iput-object v0, p0, LX/JL9;->A:LX/K4b;

    .line 2680703
    return-void
.end method

.method public static b(LX/0QB;)LX/JL9;
    .locals 30

    .prologue
    .line 2680673
    new-instance v2, LX/JL9;

    const-class v3, LX/JJb;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/JJb;

    const/16 v4, 0xd4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x542

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const-class v6, LX/JG4;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/JG4;

    const-class v7, LX/GNq;

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/GNq;

    const-class v8, LX/JLJ;

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/JLJ;

    const-class v9, LX/JJs;

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/JJs;

    const-class v10, LX/JJv;

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/JJv;

    const-class v11, LX/JJo;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/JJo;

    const-class v12, LX/F5N;

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/F5N;

    const-class v13, LX/F5U;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/F5U;

    const-class v14, LX/9DM;

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/9DM;

    const-class v15, LX/JJl;

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/JJl;

    const-class v16, LX/JK4;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/JK4;

    const-class v17, LX/JLT;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/JLT;

    const-class v18, LX/JLX;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/JLX;

    const-class v19, LX/JLZ;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/JLZ;

    const-class v20, LX/JLd;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/JLd;

    const-class v21, LX/JLf;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/JLf;

    const-class v22, LX/JJh;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v22

    check-cast v22, LX/JJh;

    const-class v23, LX/JLE;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/JLE;

    const-class v24, LX/JLL;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/JLL;

    const-class v25, LX/JLk;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/JLk;

    const-class v26, LX/JLp;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/JLp;

    const-class v27, LX/JM2;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/JM2;

    const-class v28, LX/JM6;

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/JM6;

    const-class v29, LX/K4b;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/K4b;

    invoke-direct/range {v2 .. v29}, LX/JL9;-><init>(LX/JJb;LX/0Ot;LX/0Ot;LX/JG4;LX/GNq;LX/JLJ;LX/JJs;LX/JJv;LX/JJo;LX/F5N;LX/F5U;LX/9DM;LX/JJl;LX/JK4;LX/JLT;LX/JLX;LX/JLZ;LX/JLd;LX/JLf;LX/JJh;LX/JLE;LX/JLL;LX/JLk;LX/JLp;LX/JM2;LX/JM6;LX/K4b;)V

    .line 2680674
    return-object v2
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/react/bridge/JavaScriptModule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2680704
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5pY;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/5pS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2680672
    const/16 v0, 0x1a

    new-array v0, v0, [LX/5pS;

    const/4 v1, 0x0

    new-instance v2, LX/5pS;

    const-class v3, LX/JJa;

    new-instance v4, LX/JKt;

    invoke-direct {v4, p0, p1}, LX/JKt;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, LX/5pS;

    const-class v3, LX/JJc;

    new-instance v4, LX/JL1;

    invoke-direct {v4, p0, p1}, LX/JL1;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LX/5pS;

    const-class v3, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;

    new-instance v4, LX/JL2;

    invoke-direct {v4, p0, p1}, LX/JL2;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, LX/5pS;

    const-class v3, LX/GNp;

    new-instance v4, LX/JL3;

    invoke-direct {v4, p0, p1}, LX/JL3;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, LX/5pS;

    const-class v3, LX/JLI;

    new-instance v4, LX/JL4;

    invoke-direct {v4, p0, p1}, LX/JL4;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, LX/5pS;

    const-class v3, LX/JJr;

    new-instance v4, LX/JL5;

    invoke-direct {v4, p0, p1}, LX/JL5;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, LX/5pS;

    const-class v3, LX/JJu;

    new-instance v4, LX/JL6;

    invoke-direct {v4, p0, p1}, LX/JL6;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, LX/5pS;

    const-class v3, LX/JJn;

    new-instance v4, LX/JL7;

    invoke-direct {v4, p0, p1}, LX/JL7;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, LX/5pS;

    const-class v3, LX/F5M;

    new-instance v4, LX/JL8;

    invoke-direct {v4, p0, p1}, LX/JL8;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, LX/5pS;

    const-class v3, LX/F5T;

    new-instance v4, LX/JKj;

    invoke-direct {v4, p0, p1}, LX/JKj;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, LX/5pS;

    const-class v3, LX/9DL;

    new-instance v4, LX/JKk;

    invoke-direct {v4, p0, p1}, LX/JKk;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, LX/5pS;

    const-class v3, LX/JJk;

    new-instance v4, LX/JKl;

    invoke-direct {v4, p0, p1}, LX/JKl;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, LX/5pS;

    const-class v3, LX/JK3;

    new-instance v4, LX/JKm;

    invoke-direct {v4, p0, p1}, LX/JKm;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, LX/5pS;

    const-class v3, LX/JLS;

    new-instance v4, LX/JKn;

    invoke-direct {v4, p0, p1}, LX/JKn;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, LX/5pS;

    const-class v3, LX/JLW;

    new-instance v4, LX/JKo;

    invoke-direct {v4, p0, p1}, LX/JKo;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, LX/5pS;

    const-class v3, LX/JLY;

    new-instance v4, LX/JKp;

    invoke-direct {v4, p0, p1}, LX/JKp;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, LX/5pS;

    const-class v3, LX/JLc;

    new-instance v4, LX/JKq;

    invoke-direct {v4, p0, p1}, LX/JKq;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, LX/5pS;

    const-class v3, LX/JLe;

    new-instance v4, LX/JKr;

    invoke-direct {v4, p0, p1}, LX/JKr;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, LX/5pS;

    const-class v3, LX/JJg;

    new-instance v4, LX/JKs;

    invoke-direct {v4, p0, p1}, LX/JKs;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, LX/5pS;

    const-class v3, LX/JLD;

    new-instance v4, LX/JKu;

    invoke-direct {v4, p0, p1}, LX/JKu;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, LX/5pS;

    const-class v3, LX/JLK;

    new-instance v4, LX/JKv;

    invoke-direct {v4, p0, p1}, LX/JKv;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, LX/5pS;

    const-class v3, LX/JLj;

    new-instance v4, LX/JKw;

    invoke-direct {v4, p0, p1}, LX/JKw;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, LX/5pS;

    const-class v3, LX/JLo;

    new-instance v4, LX/JKx;

    invoke-direct {v4, p0, p1}, LX/JKx;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, LX/5pS;

    const-class v3, LX/JM1;

    new-instance v4, LX/JKy;

    invoke-direct {v4, p0, p1}, LX/JKy;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, LX/5pS;

    const-class v3, LX/JM5;

    new-instance v4, LX/JKz;

    invoke-direct {v4, p0, p1}, LX/JKz;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, LX/5pS;

    const-class v3, LX/K4a;

    new-instance v4, LX/JL0;

    invoke-direct {v4, p0, p1}, LX/JL0;-><init>(LX/JL9;LX/5pY;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/5qR;
    .locals 1

    .prologue
    .line 2680671
    new-instance v0, LX/JKi;

    invoke-direct {v0}, LX/JKi;-><init>()V

    return-object v0
.end method

.method public final c(LX/5pY;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2680670
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
