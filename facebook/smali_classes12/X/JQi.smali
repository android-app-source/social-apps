.class public LX/JQi;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQi",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692102
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2692103
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JQi;->b:LX/0Zi;

    .line 2692104
    iput-object p1, p0, LX/JQi;->a:LX/0Ot;

    .line 2692105
    return-void
.end method

.method public static a(LX/0QB;)LX/JQi;
    .locals 4

    .prologue
    .line 2692106
    const-class v1, LX/JQi;

    monitor-enter v1

    .line 2692107
    :try_start_0
    sget-object v0, LX/JQi;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692108
    sput-object v2, LX/JQi;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692109
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692110
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692111
    new-instance v3, LX/JQi;

    const/16 p0, 0x200a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQi;-><init>(LX/0Ot;)V

    .line 2692112
    move-object v0, v3

    .line 2692113
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692114
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692115
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692116
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2692117
    const v0, -0x77224791

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2692118
    check-cast p2, LX/JQh;

    .line 2692119
    iget-object v0, p0, LX/JQi;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;

    iget-object v1, p2, LX/JQh;->a:LX/JQj;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->a(LX/1De;LX/JQj;)LX/1Dg;

    move-result-object v0

    .line 2692120
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2692121
    invoke-static {}, LX/1dS;->b()V

    .line 2692122
    iget v0, p1, LX/1dQ;->b:I

    .line 2692123
    packed-switch v0, :pswitch_data_0

    .line 2692124
    :goto_0
    return-object v2

    .line 2692125
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2692126
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2692127
    check-cast v1, LX/JQh;

    .line 2692128
    iget-object v3, p0, LX/JQi;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;

    iget-object v4, v1, LX/JQh;->a:LX/JQj;

    .line 2692129
    iget-object v5, v4, LX/JQj;->a:Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v5

    .line 2692130
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 2692131
    if-eqz v5, :cond_0

    .line 2692132
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLJobOpening;->j()Ljava/lang/String;

    move-result-object v5

    .line 2692133
    iget-object p2, v4, LX/JQj;->b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    if-eqz p2, :cond_1

    .line 2692134
    iget-object p2, v4, LX/JQj;->b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n()LX/0Px;

    move-result-object p2

    iget-object p0, v4, LX/JQj;->a:Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    invoke-virtual {p2, p0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result p2

    .line 2692135
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "job_carousel_item_click"

    invoke-direct {p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "job_carousel_index"

    invoke-virtual {p0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v1, "job_id"

    invoke-virtual {p0, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v1, "waterfall_session_id"

    sget-object v0, LX/17Q;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object p2, p0

    .line 2692136
    iget-object p0, v3, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->e:LX/0Zb;

    invoke-interface {p0, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2692137
    sget-object p2, LX/0ax;->iR:Ljava/lang/String;

    iget-object p0, v4, LX/JQj;->c:Ljava/lang/String;

    sget-object v1, LX/17Q;->a:Ljava/lang/String;

    invoke-static {p2, v5, p0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2692138
    iget-object p2, v3, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->d:LX/17W;

    invoke-virtual {p2, p1, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2692139
    :cond_0
    :goto_1
    goto :goto_0

    .line 2692140
    :cond_1
    sget-object p2, LX/0ax;->iQ:Ljava/lang/String;

    invoke-static {p2, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2692141
    iget-object p2, v3, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->d:LX/17W;

    invoke-virtual {p2, p1, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x77224791
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/JQg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/JQi",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2692142
    new-instance v1, LX/JQh;

    invoke-direct {v1, p0}, LX/JQh;-><init>(LX/JQi;)V

    .line 2692143
    iget-object v2, p0, LX/JQi;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JQg;

    .line 2692144
    if-nez v2, :cond_0

    .line 2692145
    new-instance v2, LX/JQg;

    invoke-direct {v2, p0}, LX/JQg;-><init>(LX/JQi;)V

    .line 2692146
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/JQg;->a$redex0(LX/JQg;LX/1De;IILX/JQh;)V

    .line 2692147
    move-object v1, v2

    .line 2692148
    move-object v0, v1

    .line 2692149
    return-object v0
.end method
