.class public final LX/IDK;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/IDG;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendlist/fragment/FriendListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V
    .locals 0

    .prologue
    .line 2550758
    iput-object p1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2550759
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-boolean v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->E:Z

    if-nez v0, :cond_1

    .line 2550760
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    const/4 v1, 0x1

    .line 2550761
    iput-boolean v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->E:Z

    .line 2550762
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    .line 2550763
    iput-boolean v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->F:Z

    .line 2550764
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2550765
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->B:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-virtual {v1}, Lcom/facebook/friendlist/fragment/FriendListFragment;->e()I

    move-result v1

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2550766
    :cond_0
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->c()V

    .line 2550767
    :cond_1
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2550768
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->s:Landroid/view/View;

    if-nez v0, :cond_2

    .line 2550769
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    .line 2550770
    iget-object v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->s:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 2550771
    :cond_2
    :goto_0
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2550772
    :cond_3
    return-void

    .line 2550773
    :cond_4
    iget-object v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->r:Landroid/view/View;

    const v2, 0x7f0d12c1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->s:Landroid/view/View;

    .line 2550774
    iget-object v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->s:Landroid/view/View;

    if-nez v1, :cond_5

    .line 2550775
    iget-object v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->r:Landroid/view/View;

    const v2, 0x7f0d12c0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2550776
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->s:Landroid/view/View;

    .line 2550777
    :cond_5
    iget-object v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->s:Landroid/view/View;

    new-instance v2, LX/IDL;

    invoke-direct {v2, v0}, LX/IDL;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2550778
    check-cast p1, LX/IDG;

    const/4 v0, 0x1

    .line 2550779
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2550780
    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    .line 2550781
    iget-object v2, p1, LX/IDG;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-object v2, v2

    .line 2550782
    iput-object v2, v1, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2550783
    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2550784
    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    .line 2550785
    iput-boolean v0, v1, Lcom/facebook/friendlist/fragment/FriendListFragment;->G:Z

    .line 2550786
    :cond_0
    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-boolean v1, v1, Lcom/facebook/friendlist/fragment/FriendListFragment;->E:Z

    if-nez v1, :cond_3

    .line 2550787
    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    .line 2550788
    iput-boolean v0, v1, Lcom/facebook/friendlist/fragment/FriendListFragment;->E:Z

    .line 2550789
    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    .line 2550790
    iput-boolean v0, v1, Lcom/facebook/friendlist/fragment/FriendListFragment;->F:Z

    .line 2550791
    iget-object v1, p1, LX/IDG;->a:LX/0Px;

    move-object v1, v1

    .line 2550792
    if-eqz v1, :cond_1

    .line 2550793
    iget-object v1, p1, LX/IDG;->a:LX/0Px;

    move-object v1, v1

    .line 2550794
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2550795
    :cond_1
    :goto_0
    if-eqz v0, :cond_3

    .line 2550796
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2550797
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->B:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-virtual {v1}, Lcom/facebook/friendlist/fragment/FriendListFragment;->e()I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2550798
    :cond_2
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->c()V

    .line 2550799
    :cond_3
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_4

    .line 2550800
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v1, v1, Lcom/facebook/friendlist/fragment/FriendListFragment;->r:Landroid/view/View;

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2550801
    :cond_4
    iget-object v0, p0, LX/IDK;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    .line 2550802
    iget-object v1, p1, LX/IDG;->a:LX/0Px;

    move-object v1, v1

    .line 2550803
    const/4 v4, 0x0

    .line 2550804
    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    if-nez v3, :cond_7

    .line 2550805
    new-instance v3, LX/4a7;

    invoke-direct {v3}, LX/4a7;-><init>()V

    .line 2550806
    iput-boolean v4, v3, LX/4a7;->c:Z

    .line 2550807
    move-object v3, v3

    .line 2550808
    iput-boolean v4, v3, LX/4a7;->b:Z

    .line 2550809
    move-object v3, v3

    .line 2550810
    invoke-virtual {v3}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2550811
    invoke-static {v0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->p$redex0(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    .line 2550812
    :cond_5
    :goto_1
    return-void

    .line 2550813
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 2550814
    :cond_7
    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    iget-object v4, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->c:Ljava/lang/String;

    iget-object v5, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/facebook/friendlist/fragment/FriendListFragment;->a(Z)LX/0Rl;

    move-result-object v4

    .line 2550815
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_8
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/IDH;

    .line 2550816
    invoke-interface {v4, v6}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-object v8, v3, LX/IE2;->a:Ljava/util/Map;

    invoke-virtual {v6}, LX/IDH;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 2550817
    iget-object v8, v3, LX/IE2;->a:Ljava/util/Map;

    invoke-virtual {v6}, LX/IDH;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2550818
    :cond_9
    invoke-static {v3}, LX/IE2;->e(LX/IE2;)V

    .line 2550819
    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    const v4, 0x64f8aff2

    invoke-static {v3, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2550820
    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    invoke-virtual {v3}, LX/IE2;->getFilter()Landroid/widget/Filter;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->J:Landroid/widget/Filter$FilterListener;

    invoke-virtual {v3, v4, v5}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    .line 2550821
    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->l()Z

    move-result v3

    if-nez v3, :cond_b

    .line 2550822
    :cond_a
    invoke-static {v0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->p$redex0(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    .line 2550823
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b()LX/DHs;

    move-result-object v3

    sget-object v4, LX/DHs;->PYMK:LX/DHs;

    invoke-virtual {v3, v4}, LX/DHs;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    invoke-virtual {v3}, LX/IE2;->d()Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    invoke-virtual {v3}, LX/IE2;->a()I

    move-result v3

    if-lez v3, :cond_5

    .line 2550824
    :cond_c
    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->t:Landroid/view/View;

    if-eqz v3, :cond_d

    .line 2550825
    iget-object v3, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->t:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2550826
    :cond_d
    goto/16 :goto_1
.end method
