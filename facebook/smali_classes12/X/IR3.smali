.class public final LX/IR3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/IR6;


# direct methods
.method public constructor <init>(LX/IR6;)V
    .locals 0

    .prologue
    .line 2576091
    iput-object p1, p0, LX/IR3;->a:LX/IR6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2576092
    iget-object v0, p0, LX/IR3;->a:LX/IR6;

    iget-object v0, v0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    .line 2576093
    new-instance v1, LX/0ju;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2576094
    new-instance v2, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2576095
    const/high16 p0, 0x80000

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setInputType(I)V

    .line 2576096
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f081bbc

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f081bb6

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/IR8;

    invoke-direct {p1, v0, v2}, LX/IR8;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, p0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f081bb7

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance p0, LX/IR7;

    invoke-direct {p0, v0}, LX/IR7;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V

    invoke-virtual {v1, v2, p0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2576097
    const/4 v0, 0x1

    return v0
.end method
