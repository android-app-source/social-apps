.class public final LX/HiZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/addresstypeahead/helper/AddressTypeAheadResultHandler;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/addresstypeahead/helper/AddressTypeAheadResultHandler;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 2497464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497465
    iput-object p1, p0, LX/HiZ;->a:LX/0QB;

    .line 2497466
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2497468
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/HiZ;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2497469
    packed-switch p2, :pswitch_data_0

    .line 2497470
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2497471
    :pswitch_0
    new-instance p2, LX/Ibp;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/Ic4;->b(LX/0QB;)LX/Ic4;

    move-result-object v1

    check-cast v1, LX/Ic4;

    invoke-static {p1}, LX/Ibu;->b(LX/0QB;)LX/Ibu;

    move-result-object v2

    check-cast v2, LX/Ibu;

    invoke-static {p1}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object p0

    check-cast p0, LX/IZK;

    invoke-direct {p2, v0, v1, v2, p0}, LX/Ibp;-><init>(Landroid/content/Context;LX/Ic4;LX/Ibu;LX/IZK;)V

    .line 2497472
    move-object v0, p2

    .line 2497473
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2497467
    const/4 v0, 0x1

    return v0
.end method
