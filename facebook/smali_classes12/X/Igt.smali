.class public LX/Igt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ige;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2601822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601823
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/Igt;->b:Ljava/util/List;

    .line 2601824
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/Igt;->c:Ljava/util/List;

    .line 2601825
    iput p1, p0, LX/Igt;->a:I

    .line 2601826
    return-void
.end method

.method public static a(LX/Igt;JZ)V
    .locals 3

    .prologue
    .line 2601819
    iget-object v0, p0, LX/Igt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ige;

    .line 2601820
    invoke-interface {v0, p1, p2, p3}, LX/Ige;->a(JZ)V

    goto :goto_0

    .line 2601821
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 2601816
    iget-object v0, p0, LX/Igt;->c:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2601817
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/Igt;->a(LX/Igt;JZ)V

    .line 2601818
    :cond_0
    return-void
.end method

.method public final a(LX/Ige;)V
    .locals 1

    .prologue
    .line 2601814
    iget-object v0, p0, LX/Igt;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2601815
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 4

    .prologue
    .line 2601808
    iget-wide v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    const/4 v2, 0x1

    .line 2601809
    invoke-virtual {p0}, LX/Igt;->b()I

    move-result v3

    iget p1, p0, LX/Igt;->a:I

    if-ge v3, p1, :cond_0

    iget-object v3, p0, LX/Igt;->c:Ljava/util/List;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2601810
    iget-object v3, p0, LX/Igt;->c:Ljava/util/List;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2601811
    invoke-static {p0, v0, v1, v2}, LX/Igt;->a(LX/Igt;JZ)V

    .line 2601812
    :goto_0
    move v0, v2

    .line 2601813
    return v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2601807
    iget-object v0, p0, LX/Igt;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 2601801
    iget-wide v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    invoke-virtual {p0, v0, v1}, LX/Igt;->a(J)V

    .line 2601802
    return-void
.end method

.method public final c(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 4

    .prologue
    .line 2601804
    iget-wide v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    .line 2601805
    iget-object v2, p0, LX/Igt;->c:Ljava/util/List;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    move v0, v2

    .line 2601806
    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2601803
    iget-object v0, p0, LX/Igt;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
