.class public LX/HhJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[B

.field public static final b:[B

.field public static final c:[B

.field private static final d:Ljava/lang/String;

.field private static final e:Ljavax/net/ssl/HostnameVerifier;

.field private static f:Ljavax/net/ssl/SSLSocketFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2495322
    const-class v0, LX/HhJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HhJ;->d:Ljava/lang/String;

    .line 2495323
    const-string v0, "http.keepAlive"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2495324
    const/16 v0, 0x25

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/HhJ;->a:[B

    .line 2495325
    const/16 v0, 0x48

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, LX/HhJ;->b:[B

    .line 2495326
    const/16 v0, 0x4f

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, LX/HhJ;->c:[B

    .line 2495327
    new-instance v0, LX/HhH;

    invoke-direct {v0}, LX/HhH;-><init>()V

    sput-object v0, LX/HhJ;->e:Ljavax/net/ssl/HostnameVerifier;

    .line 2495328
    const/4 v0, 0x0

    sput-object v0, LX/HhJ;->f:Ljavax/net/ssl/SSLSocketFactory;

    return-void

    nop

    :array_0
    .array-data 1
        0x10t
        0x23t
        0x0t
        0x6t
        0x4dt
        0x51t
        0x49t
        0x73t
        0x64t
        0x70t
        0x3t
        0x2t
        0x0t
        0x3t
        0x0t
        0x15t
        0x46t
        0x42t
        0x4dt
        0x51t
        0x54t
        0x54t
        0x5ft
        0x68t
        0x65t
        0x61t
        0x6ct
        0x74t
        0x68t
        0x5ft
        0x63t
        0x68t
        0x65t
        0x63t
        0x6bt
        0x65t
        0x72t
    .end array-data

    nop

    :array_1
    .array-data 1
        -0x80t
        0x46t
        0x1t
        0x3t
        0x1t
        0x0t
        0x2dt
        0x0t
        0x0t
        0x0t
        0x10t
        0x1t
        0x0t
        -0x80t
        0x3t
        0x0t
        -0x80t
        0x7t
        0x0t
        -0x40t
        0x6t
        0x0t
        0x40t
        0x2t
        0x0t
        -0x80t
        0x4t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x4t
        0x0t
        -0x2t
        -0x1t
        0x0t
        0x0t
        0xat
        0x0t
        -0x2t
        -0x2t
        0x0t
        0x0t
        0x9t
        0x0t
        0x0t
        0x64t
        0x0t
        0x0t
        0x62t
        0x0t
        0x0t
        0x3t
        0x0t
        0x0t
        0x6t
        0x1ft
        0x17t
        0xct
        -0x5at
        0x2ft
        0x0t
        0x78t
        -0x4t
        0x46t
        0x55t
        0x2et
        -0x4ft
        -0x7dt
        0x39t
        -0xft
        -0x16t
    .end array-data

    .line 2495329
    :array_2
    .array-data 1
        0x16t
        0x3t
        0x1t
        0x0t
        0x4at
        0x2t
        0x0t
        0x0t
        0x46t
        0x3t
        0x1t
        0x42t
        -0x7bt
        0x45t
        -0x59t
        0x27t
        -0x57t
        0x5dt
        -0x60t
        -0x4dt
        -0x3bt
        -0x19t
        0x53t
        -0x26t
        0x48t
        0x2bt
        0x3ft
        -0x3at
        0x5at
        -0x36t
        -0x77t
        -0x3ft
        0x58t
        0x52t
        -0x5ft
        0x78t
        0x3ct
        0x5bt
        0x17t
        0x46t
        0x0t
        -0x7bt
        0x3ft
        0x20t
        0xet
        -0x2dt
        0x6t
        0x72t
        0x5bt
        0x5bt
        0x1bt
        0x5ft
        0x15t
        -0x54t
        0x13t
        -0x7t
        -0x78t
        0x53t
        -0x63t
        -0x65t
        -0x18t
        0x3dt
        0x7bt
        0xct
        0x30t
        0x32t
        0x6et
        0x38t
        0x4dt
        -0x5et
        0x75t
        0x57t
        0x41t
        0x6ct
        0x34t
        0x5ct
        0x0t
        0x4t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2495330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;IILjava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "LX/HhN;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    .line 2495331
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v2

    .line 2495332
    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 2495333
    :try_start_0
    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, p0, p1}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1, p2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 2495334
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CONNECT zrtest.example.org:443 HTTP/1.1\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Host: en.m.wikipedia.org\r\n\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2495335
    move-object v0, v0

    .line 2495336
    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 2495337
    const/16 v0, 0x2000

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 2495338
    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 2495339
    invoke-static {v3}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/InputStream;)Ljava/nio/channels/ReadableByteChannel;

    move-result-object v1

    .line 2495340
    invoke-interface {v1, v0}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    .line 2495341
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 2495342
    new-instance v4, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const-string v5, "UTF-8"

    invoke-static {v5}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 2495343
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 2495344
    const-string v0, "\r\n"

    const/4 v1, 0x2

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 2495345
    const-string v1, " "

    const/4 v5, 0x3

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 2495346
    array-length v5, v1

    if-ne v5, v7, :cond_0

    .line 2495347
    const/4 v0, 0x1

    aget-object v0, v1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    .line 2495348
    :goto_0
    if-eqz p3, :cond_2

    .line 2495349
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HhN;

    .line 2495350
    invoke-interface {v0, v1, v4}, LX/HhN;->a(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2495351
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->close()V

    throw v0

    .line 2495352
    :cond_0
    :try_start_1
    array-length v5, v1

    if-ne v5, v6, :cond_1

    .line 2495353
    const/4 v0, 0x1

    aget-object v0, v1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    .line 2495354
    goto :goto_0

    .line 2495355
    :cond_1
    new-instance v1, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid HTTP response: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2495356
    :cond_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2495357
    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->close()V

    return v1
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;I)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 2495358
    const/4 v1, 0x0

    .line 2495359
    if-nez p2, :cond_0

    .line 2495360
    :try_start_0
    const-string p2, "/health_check"

    .line 2495361
    :cond_0
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2495362
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2495363
    :cond_1
    new-instance v0, Ljava/net/URL;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2495364
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2495365
    :try_start_1
    invoke-static {v0, p4}, LX/HhJ;->a(Ljava/net/HttpURLConnection;I)V

    .line 2495366
    if-eqz p3, :cond_3

    .line 2495367
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2495368
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2495369
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_2

    .line 2495370
    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    :cond_2
    throw v0

    .line 2495371
    :cond_3
    :try_start_2
    sget-object v1, LX/HhJ;->e:Ljavax/net/ssl/HostnameVerifier;

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 2495372
    invoke-static {}, LX/HhJ;->b()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 2495373
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v1

    .line 2495374
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getResponseMessage()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2495375
    if-eqz v0, :cond_4

    .line 2495376
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    :cond_4
    return v1

    .line 2495377
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;ILjava/util/List;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "LX/HhN;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2495400
    const/4 v1, 0x0

    .line 2495401
    if-nez p2, :cond_0

    .line 2495402
    :try_start_0
    const-string p2, "/health_check"

    .line 2495403
    :cond_0
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2495404
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2495405
    :cond_1
    new-instance v0, Ljava/net/URL;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2495406
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2495407
    :try_start_1
    invoke-static {v0, p4}, LX/HhJ;->a(Ljava/net/HttpURLConnection;I)V

    .line 2495408
    if-eqz p3, :cond_3

    .line 2495409
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2495410
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2495411
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_2

    .line 2495412
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v0

    .line 2495413
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    .line 2495414
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    .line 2495415
    if-eqz p5, :cond_4

    .line 2495416
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HhN;

    .line 2495417
    invoke-interface {v1, v2, v0}, LX/HhN;->a(ILjava/net/HttpURLConnection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 2495418
    :cond_4
    if-eqz v0, :cond_5

    .line 2495419
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    return v2

    .line 2495420
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2495378
    :try_start_0
    invoke-static {p0, p1, p2}, LX/HhJ;->d(Ljava/lang/String;II)Ljava/net/Socket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 2495379
    :try_start_1
    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 2495380
    :try_start_2
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2495381
    :try_start_3
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 2495382
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 2495383
    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 2495384
    const/16 v0, 0x5dc

    new-array v0, v0, [B

    .line 2495385
    const/4 v3, 0x0

    const/16 v5, 0x5dc

    invoke-virtual {v1, v0, v3, v5}, Ljava/io/InputStream;->read([BII)I

    .line 2495386
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2495387
    invoke-static {v2, v6}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495388
    invoke-static {v1, v6}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495389
    invoke-static {v4, v6}, LX/HhJ;->a(Ljava/net/Socket;Z)V

    .line 2495390
    return-object v3

    .line 2495391
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_0
    invoke-static {v2, v7}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495392
    invoke-static {v1, v7}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495393
    invoke-static {v3, v7}, LX/HhJ;->a(Ljava/net/Socket;Z)V

    throw v0

    .line 2495394
    :catchall_1
    move-exception v0

    move-object v2, v1

    move-object v3, v4

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto :goto_0

    :catchall_3
    move-exception v0

    move-object v3, v4

    goto :goto_0
.end method

.method public static a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2495395
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v1

    .line 2495396
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2495397
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2495398
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2495399
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 2495303
    :try_start_0
    invoke-static {p0, p1, p2}, LX/HhJ;->d(Ljava/lang/String;II)Ljava/net/Socket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2495304
    :try_start_1
    invoke-static {}, LX/HhJ;->b()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, p1, v3}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v4

    .line 2495305
    :try_start_2
    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v3

    .line 2495306
    :try_start_3
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 2495307
    :try_start_4
    sget-object v0, LX/HhJ;->a:[B

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 2495308
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 2495309
    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 2495310
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 2495311
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 2495312
    if-gez v0, :cond_0

    .line 2495313
    new-instance v0, Ljava/io/IOException;

    const-string v3, "No MQTT response"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2495314
    :catchall_0
    move-exception v0

    move-object v3, v4

    :goto_0
    invoke-static {v2, v6}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495315
    invoke-static {v1, v6}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495316
    invoke-static {v3, v6}, LX/HhJ;->a(Ljava/net/Socket;Z)V

    throw v0

    .line 2495317
    :cond_0
    invoke-static {v2, v5}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495318
    invoke-static {v1, v5}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495319
    invoke-static {v4, v5}, LX/HhJ;->a(Ljava/net/Socket;Z)V

    .line 2495320
    return-void

    .line 2495321
    :catchall_1
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    goto :goto_0

    :catchall_3
    move-exception v0

    move-object v2, v1

    move-object v3, v4

    goto :goto_0

    :catchall_4
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IILjava/util/Map;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/HhN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2495298
    :try_start_0
    const-string v0, "NOOP"

    invoke-static {p0, p1, p2, v0}, LX/HhJ;->a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2495299
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2495300
    const-string v0, "/favicon.ico"

    invoke-static {p0, p1, v0, p3, p2}, LX/HhJ;->a(Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2495301
    :cond_0
    return-void

    .line 2495302
    :catchall_0
    move-exception v0

    throw v0
.end method

.method private static a(Ljava/net/HttpURLConnection;I)V
    .locals 2

    .prologue
    .line 2495208
    const-string v0, "Connection"

    const-string v1, "Close"

    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495209
    const-string v0, "Cache-Control"

    const-string v1, "no-cache, no-store, must-revalidate"

    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495210
    const-string v0, "Pragma"

    const-string v1, "no-cache"

    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495211
    const-string v0, "Expires"

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495212
    const-string v0, "Accept-Encoding"

    const-string v1, "identity"

    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495213
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 2495214
    invoke-virtual {p0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 2495215
    invoke-virtual {p0, p1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 2495216
    return-void
.end method

.method private static a(Ljava/net/Socket;Z)V
    .locals 1
    .param p0    # Ljava/net/Socket;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2495292
    if-nez p0, :cond_1

    .line 2495293
    :cond_0
    :goto_0
    return-void

    .line 2495294
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2495295
    :catch_0
    move-exception v0

    .line 2495296
    if-nez p1, :cond_0

    .line 2495297
    throw v0
.end method

.method public static b(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 2495274
    :try_start_0
    invoke-static {p0, p1, p2}, LX/HhJ;->d(Ljava/lang/String;II)Ljava/net/Socket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2495275
    :try_start_1
    invoke-static {}, LX/HhJ;->b()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, p1, v3}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 2495276
    :try_start_2
    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v3

    .line 2495277
    :try_start_3
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2495278
    :try_start_4
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 2495279
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 2495280
    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 2495281
    const/16 v0, 0x5dc

    new-array v0, v0, [B

    .line 2495282
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    .line 2495283
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2495284
    invoke-static {v2, v5}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495285
    invoke-static {v1, v5}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495286
    invoke-static {v4, v5}, LX/HhJ;->a(Ljava/net/Socket;Z)V

    .line 2495287
    return-object v3

    .line 2495288
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_0
    invoke-static {v2, v6}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495289
    invoke-static {v1, v6}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495290
    invoke-static {v3, v6}, LX/HhJ;->a(Ljava/net/Socket;Z)V

    throw v0

    .line 2495291
    :catchall_1
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v2, v1

    move-object v3, v4

    goto :goto_0

    :catchall_3
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto :goto_0

    :catchall_4
    move-exception v0

    move-object v3, v4

    goto :goto_0
.end method

.method private static declared-synchronized b()Ljavax/net/ssl/SSLSocketFactory;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "TrulyRandom"
        }
    .end annotation

    .prologue
    .line 2495266
    const-class v1, LX/HhJ;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/HhJ;->f:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    .line 2495267
    sget-object v0, LX/HhJ;->f:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2495268
    :goto_0
    monitor-exit v1

    return-object v0

    .line 2495269
    :cond_0
    :try_start_1
    new-instance v0, LX/HhI;

    invoke-direct {v0}, LX/HhI;-><init>()V

    .line 2495270
    const-string v2, "SSL"

    invoke-static {v2}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v2

    .line 2495271
    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljavax/net/ssl/TrustManager;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v2, v3, v4, v0}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 2495272
    invoke-virtual {v2}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    sput-object v0, LX/HhJ;->f:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2495273
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Ljava/lang/String;II)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 2495241
    const/4 v6, 0x1

    .line 2495242
    :try_start_0
    invoke-static {p0, p1, p2}, LX/HhJ;->d(Ljava/lang/String;II)Ljava/net/Socket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 2495243
    :try_start_1
    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v3

    .line 2495244
    :try_start_2
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 2495245
    :try_start_3
    sget-object v0, LX/HhJ;->b:[B

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 2495246
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 2495247
    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 2495248
    sget-object v0, LX/HhJ;->c:[B

    array-length v0, v0

    new-array v7, v0, [B

    move v3, v5

    .line 2495249
    :cond_0
    array-length v0, v7

    if-ge v3, v0, :cond_3

    .line 2495250
    array-length v0, v7

    sub-int/2addr v0, v3

    invoke-virtual {v1, v7, v3, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 2495251
    if-gez v0, :cond_2

    .line 2495252
    new-instance v0, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "EOF while reading turn response, read="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2495253
    :catchall_0
    move-exception v0

    move-object v3, v4

    :goto_0
    invoke-static {v2, v6}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495254
    invoke-static {v1, v6}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495255
    invoke-static {v3, v6}, LX/HhJ;->a(Ljava/net/Socket;Z)V

    throw v0

    .line 2495256
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 2495257
    add-int/lit8 v0, v0, -0x1

    .line 2495258
    :cond_2
    if-lez v0, :cond_0

    .line 2495259
    :try_start_4
    sget-object v8, LX/HhJ;->c:[B

    aget-byte v8, v8, v3

    aget-byte v9, v7, v3

    if-eq v8, v9, :cond_1

    .line 2495260
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "IP "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " did not get expected TURN response, response="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v7, v5}, LX/1u4;->a([BZ)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2495261
    :cond_3
    invoke-static {v2, v5}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495262
    invoke-static {v1, v5}, LX/0VN;->a(Ljava/io/Closeable;Z)V

    .line 2495263
    invoke-static {v4, v5}, LX/HhJ;->a(Ljava/net/Socket;Z)V

    .line 2495264
    return-void

    .line 2495265
    :catchall_1
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v2, v1

    move-object v3, v4

    goto :goto_0

    :catchall_3
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;II)V
    .locals 5

    .prologue
    .line 2495225
    const/4 v2, 0x0

    .line 2495226
    :try_start_0
    new-instance v1, Ljava/net/DatagramSocket;

    invoke-direct {v1}, Ljava/net/DatagramSocket;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2495227
    :try_start_1
    invoke-virtual {v1, p2}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    .line 2495228
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 2495229
    const-string v2, "health"

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 2495230
    new-instance v3, Ljava/net/DatagramPacket;

    array-length v4, v2

    invoke-direct {v3, v2, v4, v0, p1}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 2495231
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/net/DatagramSocket;->setBroadcast(Z)V

    .line 2495232
    invoke-virtual {v1, v3}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 2495233
    const/16 v0, 0x5dc

    new-array v0, v0, [B

    .line 2495234
    new-instance v2, Ljava/net/DatagramPacket;

    const/16 v3, 0x5dc

    invoke-direct {v2, v0, v3}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 2495235
    invoke-virtual {v1, v2}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 2495236
    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/net/DatagramPacket;->getLength()I

    move-result v2

    invoke-direct {v3, v0, v4, v2}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2495237
    invoke-virtual {v1}, Ljava/net/DatagramSocket;->close()V

    return-void

    .line 2495238
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 2495239
    invoke-virtual {v1}, Ljava/net/DatagramSocket;->close()V

    :cond_0
    throw v0

    .line 2495240
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;II)Ljava/net/Socket;
    .locals 3

    .prologue
    const/16 v1, 0x20

    const/4 v2, 0x0

    .line 2495217
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    .line 2495218
    invoke-virtual {v0, v1}, Ljava/net/Socket;->setReceiveBufferSize(I)V

    .line 2495219
    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSendBufferSize(I)V

    .line 2495220
    invoke-virtual {v0, v2}, Ljava/net/Socket;->setKeepAlive(Z)V

    .line 2495221
    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/net/Socket;->setSoLinger(ZI)V

    .line 2495222
    invoke-virtual {v0, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 2495223
    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, p0, p1}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1, p2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 2495224
    return-object v0
.end method
