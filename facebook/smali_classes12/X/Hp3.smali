.class public LX/Hp3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2507181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;III)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    .line 2507161
    const v0, 0x7f010013

    invoke-static {p0, v0, p1}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    move-object v0, v0

    .line 2507162
    const v1, 0x7f010054

    invoke-static {v0, v1, p3}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v1

    .line 2507163
    invoke-static {v0, p2}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const v3, 0x1010033

    const/high16 v4, 0x3f000000    # 0.5f

    .line 2507164
    new-instance p0, Landroid/util/TypedValue;

    invoke-direct {p0}, Landroid/util/TypedValue;-><init>()V

    .line 2507165
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, v3, p0, p2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2507166
    invoke-virtual {p0}, Landroid/util/TypedValue;->getFloat()F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-static {p0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p0

    .line 2507167
    :goto_0
    move-object p0, p0

    .line 2507168
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Float;

    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    move-result p0

    move v0, p0

    .line 2507169
    const/4 p3, 0x2

    const/4 p1, 0x1

    const/4 v3, 0x0

    .line 2507170
    new-array v4, p3, [[I

    new-array v5, p1, [I

    const p0, 0x101009e

    aput p0, v5, v3

    aput-object v5, v4, v3

    new-array v5, p1, [I

    const p0, -0x101009e

    aput p0, v5, v3

    aput-object v5, v4, p1

    .line 2507171
    new-array v5, p3, [I

    aput v1, v5, v3

    invoke-static {v1, v0}, LX/47Z;->b(IF)I

    move-result p0

    aput p0, v5, p1

    .line 2507172
    new-instance p0, LX/A7L;

    invoke-direct {p0}, LX/A7L;-><init>()V

    .line 2507173
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 2507174
    :goto_1
    if-ge v3, p3, :cond_0

    .line 2507175
    aget-object p1, v4, v3

    .line 2507176
    aget p2, v5, v3

    .line 2507177
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p1, p2, v2}, LX/A7L;->a([ILjava/lang/Integer;Landroid/graphics/drawable/Drawable;)V

    .line 2507178
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2507179
    :cond_0
    move-object v0, p0

    .line 2507180
    return-object v0

    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p0

    goto :goto_0
.end method
