.class public final LX/IA1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/IA4;


# direct methods
.method public constructor <init>(LX/IA4;Z)V
    .locals 0

    .prologue
    .line 2544408
    iput-object p1, p0, LX/IA1;->b:LX/IA4;

    iput-boolean p2, p0, LX/IA1;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/7TY;IILX/Blc;)V
    .locals 3

    .prologue
    .line 2544409
    invoke-virtual {p1, p2}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    iget-object v1, p0, LX/IA1;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->m:LX/0wM;

    const v2, -0x958e80

    invoke-virtual {v1, p3, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/IA0;

    invoke-direct {v1, p0, p4}, LX/IA0;-><init>(LX/IA1;LX/Blc;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2544410
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x3b39b0d7

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2544411
    iget-object v1, p0, LX/IA1;->b:LX/IA4;

    invoke-virtual {v1}, LX/IA4;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2544412
    new-instance v2, LX/3Af;

    invoke-direct {v2, v1}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2544413
    new-instance v3, LX/7TY;

    invoke-direct {v3, v1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2544414
    iget-object v1, p0, LX/IA1;->b:LX/IA4;

    invoke-static {v1, v3}, LX/IA4;->a$redex0(LX/IA4;LX/7TY;)V

    .line 2544415
    iget-object v1, p0, LX/IA1;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->w:LX/Blc;

    sget-object v4, LX/Blc;->PRIVATE_GOING:LX/Blc;

    if-eq v1, v4, :cond_0

    iget-boolean v1, p0, LX/IA1;->a:Z

    if-eqz v1, :cond_0

    .line 2544416
    const v1, 0x7f08129b

    const v4, 0x7f020857

    sget-object v5, LX/Blc;->PRIVATE_GOING:LX/Blc;

    invoke-direct {p0, v3, v1, v4, v5}, LX/IA1;->a(LX/7TY;IILX/Blc;)V

    .line 2544417
    :cond_0
    iget-object v1, p0, LX/IA1;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->w:LX/Blc;

    sget-object v4, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    if-eq v1, v4, :cond_1

    iget-boolean v1, p0, LX/IA1;->a:Z

    if-eqz v1, :cond_1

    .line 2544418
    const v1, 0x7f08129c

    const v4, 0x7f02085c

    sget-object v5, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    invoke-direct {p0, v3, v1, v4, v5}, LX/IA1;->a(LX/7TY;IILX/Blc;)V

    .line 2544419
    :cond_1
    iget-object v1, p0, LX/IA1;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->w:LX/Blc;

    sget-object v4, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    if-eq v1, v4, :cond_2

    .line 2544420
    const v1, 0x7f08129d

    const v4, 0x7f02085e

    sget-object v5, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    invoke-direct {p0, v3, v1, v4, v5}, LX/IA1;->a(LX/7TY;IILX/Blc;)V

    .line 2544421
    :cond_2
    invoke-virtual {v2, v3}, LX/3Af;->a(LX/1OM;)V

    .line 2544422
    invoke-virtual {v2}, LX/3Af;->show()V

    .line 2544423
    const v1, 0x4c56d00e    # 5.6311864E7f

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
