.class public final LX/I7Q;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538919
    iput-object p1, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2538920
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->I(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2538921
    :goto_0
    return-void

    .line 2538922
    :cond_0
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2538923
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ap:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2538924
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->NETWORK_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->b(LX/IBP;)V

    .line 2538925
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    invoke-virtual {v0}, LX/IBQ;->c()V

    .line 2538926
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_1

    .line 2538927
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->s:LX/IBN;

    invoke-virtual {v0}, LX/IBN;->a()V

    .line 2538928
    :cond_1
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static {v0, v2}, Lcom/facebook/events/permalink/EventPermalinkFragment;->b(Lcom/facebook/events/permalink/EventPermalinkFragment;Z)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2538929
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2538930
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->I(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2538931
    :goto_0
    return-void

    .line 2538932
    :cond_0
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ap:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2538933
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2538934
    iget-object v0, p0, LX/I7Q;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static {v0, p1, v2}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a$redex0(Lcom/facebook/events/permalink/EventPermalinkFragment;Lcom/facebook/graphql/executor/GraphQLResult;Z)V

    goto :goto_0
.end method
