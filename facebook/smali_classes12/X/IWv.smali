.class public final LX/IWv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IWF;

.field public final synthetic b:LX/IWw;


# direct methods
.method public constructor <init>(LX/IWw;LX/IWF;)V
    .locals 0

    .prologue
    .line 2585309
    iput-object p1, p0, LX/IWv;->b:LX/IWw;

    iput-object p2, p0, LX/IWv;->a:LX/IWF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x36e5a89e

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2585310
    iget-object v1, p0, LX/IWv;->a:LX/IWF;

    .line 2585311
    iget-object v3, v1, LX/IWF;->a:Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;

    .line 2585312
    iget-object v4, v3, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/9at;

    sget-object v5, LX/9au;->ALBUMSTAB:LX/9au;

    const/4 v6, 0x0

    new-instance v7, LX/89I;

    iget-object v8, v3, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->f:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    sget-object v10, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v7, v8, v9, v10}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v7}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/content/Intent;

    move-result-object v4

    .line 2585313
    iget-object v5, v3, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->c:Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0x7c7

    invoke-interface {v5, v4, v6, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2585314
    const v1, 0x68f91dd

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
