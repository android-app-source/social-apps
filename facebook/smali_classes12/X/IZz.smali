.class public final LX/IZz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpConfirmPhoneCodeMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IaD;

.field public final synthetic b:LX/Ia0;


# direct methods
.method public constructor <init>(LX/Ia0;LX/IaD;)V
    .locals 0

    .prologue
    .line 2590488
    iput-object p1, p0, LX/IZz;->b:LX/Ia0;

    iput-object p2, p0, LX/IZz;->a:LX/IaD;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2590508
    iget-object v0, p0, LX/IZz;->b:LX/Ia0;

    iget-object v0, v0, LX/Ia0;->a:LX/03V;

    const-string v1, "ConfirmPhoneCodeMutator"

    const-string v2, "Can\'t get request mutation result"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2590509
    iget-object v0, p0, LX/IZz;->a:LX/IaD;

    invoke-virtual {v0}, LX/IaD;->a()V

    .line 2590510
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2590489
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2590490
    if-eqz p1, :cond_0

    .line 2590491
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2590492
    if-eqz v0, :cond_0

    .line 2590493
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2590494
    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpConfirmPhoneCodeMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpConfirmPhoneCodeMutationModel;->a()Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2590495
    :cond_0
    iget-object v0, p0, LX/IZz;->b:LX/Ia0;

    iget-object v0, v0, LX/Ia0;->a:LX/03V;

    const-string v1, "ConfirmPhoneCodeMutator"

    const-string v2, "Confirmed phone number is empty"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590496
    iget-object v0, p0, LX/IZz;->a:LX/IaD;

    new-instance v1, Ljava/lang/Throwable;

    iget-object v2, p0, LX/IZz;->b:LX/Ia0;

    iget-object v2, v2, LX/Ia0;->d:Landroid/content/Context;

    const v3, 0x7f083a40

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/IaD;->a()V

    .line 2590497
    :goto_0
    return-void

    .line 2590498
    :cond_1
    iget-object v1, p0, LX/IZz;->a:LX/IaD;

    .line 2590499
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2590500
    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpConfirmPhoneCodeMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpConfirmPhoneCodeMutationModel;->a()Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    move-result-object v0

    .line 2590501
    iget-object v2, v1, LX/IaD;->a:LX/IaE;

    iget-object v2, v2, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    invoke-static {v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->e$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V

    .line 2590502
    iget-object v2, v1, LX/IaD;->a:LX/IaE;

    iget-object v2, v2, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->e:LX/IZw;

    const-string v3, "success_verify_sms_code"

    invoke-virtual {v2, v3}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590503
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2590504
    const-string v3, "confirmed_phone_number"

    invoke-static {v2, v3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2590505
    iget-object v3, v1, LX/IaD;->a:LX/IaE;

    iget-object v3, v3, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    const/4 p0, -0x1

    invoke-virtual {v3, p0, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2590506
    iget-object v2, v1, LX/IaD;->a:LX/IaE;

    iget-object v2, v2, LX/IaE;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 2590507
    goto :goto_0
.end method
