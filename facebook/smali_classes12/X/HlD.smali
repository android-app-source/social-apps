.class public final enum LX/HlD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HlD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/HlD;

.field public static final enum b:LX/HlD;

.field private static final synthetic c:[LX/HlD;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LX/HlD;

    const-string v1, "TEXTURE_VIEW"

    invoke-direct {v0, v1, v2}, LX/HlD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HlD;->a:LX/HlD;

    new-instance v0, LX/HlD;

    const-string v1, "VIDEO_VIEW"

    invoke-direct {v0, v1, v3}, LX/HlD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HlD;->b:LX/HlD;

    const/4 v0, 0x2

    new-array v0, v0, [LX/HlD;

    sget-object v1, LX/HlD;->a:LX/HlD;

    aput-object v1, v0, v2

    sget-object v1, LX/HlD;->b:LX/HlD;

    aput-object v1, v0, v3

    sput-object v0, LX/HlD;->c:[LX/HlD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HlD;
    .locals 1

    const-class v0, LX/HlD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HlD;

    return-object v0
.end method

.method public static values()[LX/HlD;
    .locals 1

    sget-object v0, LX/HlD;->c:[LX/HlD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HlD;

    return-object v0
.end method
