.class public LX/IPR;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IPR;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574147
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2574148
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2574149
    const-string v1, "extra_parent_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2574150
    sget-object v1, LX/0ax;->K:Ljava/lang/String;

    const-string v2, "group_feed_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->GROUP_EVENTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2574151
    return-void
.end method

.method public static a(LX/0QB;)LX/IPR;
    .locals 3

    .prologue
    .line 2574152
    sget-object v0, LX/IPR;->a:LX/IPR;

    if-nez v0, :cond_1

    .line 2574153
    const-class v1, LX/IPR;

    monitor-enter v1

    .line 2574154
    :try_start_0
    sget-object v0, LX/IPR;->a:LX/IPR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2574155
    if-eqz v2, :cond_0

    .line 2574156
    :try_start_1
    new-instance v0, LX/IPR;

    invoke-direct {v0}, LX/IPR;-><init>()V

    .line 2574157
    move-object v0, v0

    .line 2574158
    sput-object v0, LX/IPR;->a:LX/IPR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574159
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2574160
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2574161
    :cond_1
    sget-object v0, LX/IPR;->a:LX/IPR;

    return-object v0

    .line 2574162
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2574163
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
