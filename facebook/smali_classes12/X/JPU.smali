.class public LX/JPU;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/OnePageRowComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JPU",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/OnePageRowComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2689819
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2689820
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JPU;->b:LX/0Zi;

    .line 2689821
    iput-object p1, p0, LX/JPU;->a:LX/0Ot;

    .line 2689822
    return-void
.end method

.method public static a(LX/0QB;)LX/JPU;
    .locals 4

    .prologue
    .line 2689823
    const-class v1, LX/JPU;

    monitor-enter v1

    .line 2689824
    :try_start_0
    sget-object v0, LX/JPU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2689825
    sput-object v2, LX/JPU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2689826
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2689827
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2689828
    new-instance v3, LX/JPU;

    const/16 p0, 0x1fe0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JPU;-><init>(LX/0Ot;)V

    .line 2689829
    move-object v0, v3

    .line 2689830
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2689831
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JPU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2689832
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2689833
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2689834
    check-cast p2, LX/JPT;

    .line 2689835
    iget-object v0, p0, LX/JPU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/OnePageRowComponentSpec;

    iget-object v1, p2, LX/JPT;->b:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 p2, 0x7

    const/4 p0, 0x0

    const/4 v8, 0x1

    .line 2689836
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 2689837
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v3

    .line 2689838
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->aS()Ljava/lang/String;

    move-result-object v4

    .line 2689839
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b257f

    invoke-interface {v5, v6}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b257a

    invoke-interface {v5, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/feedplugins/hpp/OnePageRowComponentSpec;->a:LX/1nu;

    sget-object v7, Lcom/facebook/feedplugins/hpp/OnePageRowComponentSpec;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {p1, v6, v7, v2}, LX/JPK;->a(LX/1De;LX/1nu;Lcom/facebook/common/callercontext/CallerContext;Landroid/net/Uri;)LX/1Di;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b2592

    invoke-interface {v5, v6}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v6, 0x7f0b2582

    invoke-virtual {v3, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v6, 0x7f0b257b

    invoke-interface {v3, v8, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b2589

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a010e

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b2590

    invoke-interface {v3, p0, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    const v4, 0x7f0207eb

    invoke-virtual {v3, v4}, LX/1o5;->h(I)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b257d

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b257d

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b2590

    invoke-interface {v3, p2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2591

    invoke-interface {v2, p2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2591

    invoke-interface {v2, p0, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    .line 2689840
    const v3, 0x7b8c1f5a

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2689841
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2689842
    return-object v0

    .line 2689843
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2689844
    invoke-static {}, LX/1dS;->b()V

    .line 2689845
    iget v0, p1, LX/1dQ;->b:I

    .line 2689846
    packed-switch v0, :pswitch_data_0

    .line 2689847
    :goto_0
    return-object v2

    .line 2689848
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2689849
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2689850
    check-cast v1, LX/JPT;

    .line 2689851
    iget-object v3, p0, LX/JPU;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hpp/OnePageRowComponentSpec;

    iget-object p1, v1, LX/JPT;->b:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2689852
    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/OnePageRowComponentSpec;->b:LX/1nA;

    invoke-static {p1}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p0, v1}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2689853
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7b8c1f5a
        :pswitch_0
    .end packed-switch
.end method
