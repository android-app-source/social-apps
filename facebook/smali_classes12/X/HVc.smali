.class public LX/HVc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HVc;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475238
    iput-object p1, p0, LX/HVc;->a:LX/0Ot;

    .line 2475239
    return-void
.end method

.method public static a(LX/0QB;)LX/HVc;
    .locals 4

    .prologue
    .line 2475240
    sget-object v0, LX/HVc;->b:LX/HVc;

    if-nez v0, :cond_1

    .line 2475241
    const-class v1, LX/HVc;

    monitor-enter v1

    .line 2475242
    :try_start_0
    sget-object v0, LX/HVc;->b:LX/HVc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2475243
    if-eqz v2, :cond_0

    .line 2475244
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2475245
    new-instance v3, LX/HVc;

    const/16 p0, 0x455

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HVc;-><init>(LX/0Ot;)V

    .line 2475246
    move-object v0, v3

    .line 2475247
    sput-object v0, LX/HVc;->b:LX/HVc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2475248
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2475249
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2475250
    :cond_1
    sget-object v0, LX/HVc;->b:LX/HVc;

    return-object v0

    .line 2475251
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2475252
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
