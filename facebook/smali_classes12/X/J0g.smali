.class public LX/J0g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637590
    iput-object p1, p0, LX/J0g;->a:LX/0Or;

    .line 2637591
    return-void
.end method

.method public static a(LX/0QB;)LX/J0g;
    .locals 2

    .prologue
    .line 2637559
    new-instance v0, LX/J0g;

    const/16 v1, 0x12cc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/J0g;-><init>(LX/0Or;)V

    .line 2637560
    move-object v0, v0

    .line 2637561
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2637562
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;

    .line 2637563
    iget-object v0, p0, LX/J0g;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2637564
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Logged in user found to be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2637565
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2637566
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "platform_context_id"

    .line 2637567
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2637568
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2637569
    const-string v2, "%s/%s"

    iget-object v0, p0, LX/J0g;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2637570
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2637571
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->a:LX/J1S;

    move-object v3, v3

    .line 2637572
    iget-object v3, v3, LX/J1S;->mutation:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2637573
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "mutate_payment_platform_context"

    .line 2637574
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2637575
    move-object v2, v2

    .line 2637576
    const-string v3, "POST"

    .line 2637577
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2637578
    move-object v2, v2

    .line 2637579
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2637580
    move-object v0, v2

    .line 2637581
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2637582
    move-object v0, v0

    .line 2637583
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2637584
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637585
    move-object v0, v0

    .line 2637586
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2637587
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637588
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
