.class public LX/Htv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0wM;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516944
    iput-object p1, p0, LX/Htv;->a:Landroid/content/res/Resources;

    .line 2516945
    iput-object p2, p0, LX/Htv;->b:LX/0wM;

    .line 2516946
    return-void
.end method

.method public static a(LX/0QB;)LX/Htv;
    .locals 3

    .prologue
    .line 2516947
    const-class v1, LX/Htv;

    monitor-enter v1

    .line 2516948
    :try_start_0
    sget-object v0, LX/Htv;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2516949
    sput-object v2, LX/Htv;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2516950
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2516951
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Htv;->b(LX/0QB;)LX/Htv;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2516952
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Htv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2516953
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2516954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/Htv;
    .locals 3

    .prologue
    .line 2516955
    new-instance v2, LX/Htv;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-direct {v2, v0, v1}, LX/Htv;-><init>(Landroid/content/res/Resources;LX/0wM;)V

    .line 2516956
    return-object v2
.end method


# virtual methods
.method public final a(LX/Hu0;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 2516957
    invoke-virtual {p1}, LX/Hu0;->f()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Htv;->b:LX/0wM;

    .line 2516958
    iget v1, p1, LX/Hu0;->a:I

    move v1, v1

    .line 2516959
    iget-object v2, p0, LX/Htv;->a:Landroid/content/res/Resources;

    invoke-virtual {p1}, LX/Hu0;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Htv;->a:Landroid/content/res/Resources;

    .line 2516960
    iget v1, p1, LX/Hu0;->a:I

    move v1, v1

    .line 2516961
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/Hu0;Lcom/facebook/composer/inlinesprouts/SproutListItem;)V
    .locals 1

    .prologue
    .line 2516962
    iget-object v0, p1, LX/Hu0;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2516963
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2516964
    iget-object v0, p1, LX/Hu0;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2516965
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2516966
    iget-object v0, p1, LX/Hu0;->g:LX/Hsa;

    move-object v0, v0

    .line 2516967
    invoke-virtual {p2, v0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->setNuxProvider(LX/Hsa;)V

    .line 2516968
    invoke-virtual {p0, p1}, LX/Htv;->a(LX/Hu0;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2516969
    return-void
.end method
