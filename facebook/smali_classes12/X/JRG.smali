.class public final LX/JRG;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JRI;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JRH;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2693073
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2693074
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "footerText"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "actionButtonText"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "props"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JRG;->b:[Ljava/lang/String;

    .line 2693075
    iput v3, p0, LX/JRG;->c:I

    .line 2693076
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JRG;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JRG;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JRG;LX/1De;IILX/JRH;)V
    .locals 1

    .prologue
    .line 2693069
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2693070
    iput-object p4, p0, LX/JRG;->a:LX/JRH;

    .line 2693071
    iget-object v0, p0, LX/JRG;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2693072
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JRG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "LX/JRG;"
        }
    .end annotation

    .prologue
    .line 2693066
    iget-object v0, p0, LX/JRG;->a:LX/JRH;

    iput-object p1, v0, LX/JRH;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693067
    iget-object v0, p0, LX/JRG;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2693068
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2693077
    invoke-super {p0}, LX/1X5;->a()V

    .line 2693078
    const/4 v0, 0x0

    iput-object v0, p0, LX/JRG;->a:LX/JRH;

    .line 2693079
    sget-object v0, LX/JRI;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2693080
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/JRG;
    .locals 2

    .prologue
    .line 2693063
    iget-object v0, p0, LX/JRG;->a:LX/JRH;

    iput-object p1, v0, LX/JRH;->a:Ljava/lang/String;

    .line 2693064
    iget-object v0, p0, LX/JRG;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2693065
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/JRG;
    .locals 2

    .prologue
    .line 2693060
    iget-object v0, p0, LX/JRG;->a:LX/JRH;

    iput-object p1, v0, LX/JRH;->b:Ljava/lang/String;

    .line 2693061
    iget-object v0, p0, LX/JRG;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2693062
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JRI;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2693050
    iget-object v1, p0, LX/JRG;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JRG;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JRG;->c:I

    if-ge v1, v2, :cond_2

    .line 2693051
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2693052
    :goto_0
    iget v2, p0, LX/JRG;->c:I

    if-ge v0, v2, :cond_1

    .line 2693053
    iget-object v2, p0, LX/JRG;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2693054
    iget-object v2, p0, LX/JRG;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2693055
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2693056
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2693057
    :cond_2
    iget-object v0, p0, LX/JRG;->a:LX/JRH;

    .line 2693058
    invoke-virtual {p0}, LX/JRG;->a()V

    .line 2693059
    return-object v0
.end method
