.class public final LX/HLV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HJP;

.field public final synthetic b:LX/HLX;


# direct methods
.method public constructor <init>(LX/HLX;LX/HJP;)V
    .locals 0

    .prologue
    .line 2455923
    iput-object p1, p0, LX/HLV;->b:LX/HLX;

    iput-object p2, p0, LX/HLV;->a:LX/HJP;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2455901
    iget-object v0, p0, LX/HLV;->a:LX/HJP;

    invoke-virtual {v0}, LX/HJP;->a()V

    .line 2455902
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2455903
    check-cast p1, LX/0Px;

    .line 2455904
    if-nez p1, :cond_0

    .line 2455905
    iget-object v0, p0, LX/HLV;->a:LX/HJP;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "result is NULL"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/HJP;->a()V

    .line 2455906
    :goto_0
    return-void

    .line 2455907
    :cond_0
    iget-object v0, p0, LX/HLV;->a:LX/HJP;

    .line 2455908
    iget-object v3, v0, LX/HJP;->e:Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;

    iget-object v3, v3, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->d:LX/9XE;

    iget-object v4, v0, LX/HJP;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455909
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v5

    .line 2455910
    invoke-interface {v4}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v6, v0, LX/HJP;->b:LX/HL6;

    iget-object v6, v6, LX/HL6;->a:Ljava/lang/String;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    int-to-long v7, v7

    .line 2455911
    iget-object v9, v3, LX/9XE;->a:LX/0Zb;

    sget-object v10, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_SUCCESS:LX/9XB;

    invoke-static {v10, v4, v5}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "tapped_friend_id"

    invoke-virtual {v10, v11, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "pages_count"

    invoke-virtual {v10, v11, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    invoke-interface {v9, v10}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2455912
    iget-object v3, v0, LX/HJP;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455913
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v4

    .line 2455914
    check-cast v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-static {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v3

    invoke-static {v3}, LX/9vz;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/9vz;

    move-result-object v3

    .line 2455915
    iput-object p1, v3, LX/9vz;->d:LX/0Px;

    .line 2455916
    move-object v3, v3

    .line 2455917
    invoke-virtual {v3}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v5

    .line 2455918
    iget-object v3, v0, LX/HJP;->c:LX/2km;

    check-cast v3, LX/1Pr;

    iget-object v4, v0, LX/HJP;->d:LX/HLF;

    iget-object v6, v0, LX/HJP;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v3, v4, v6}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HLG;

    .line 2455919
    const/4 v4, 0x0

    iput-boolean v4, v3, LX/HLG;->a:Z

    .line 2455920
    iget-object v4, v0, LX/HJP;->c:LX/2km;

    check-cast v4, LX/1Pr;

    iget-object v6, v0, LX/HJP;->d:LX/HLF;

    invoke-interface {v4, v6, v3}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2455921
    iget-object v3, v0, LX/HJP;->c:LX/2km;

    check-cast v3, LX/3U8;

    iget-object v4, v0, LX/HJP;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v3, v4, v5}, LX/3U8;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V

    .line 2455922
    goto :goto_0
.end method
