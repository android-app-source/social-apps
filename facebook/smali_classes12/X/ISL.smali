.class public LX/ISL;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1EV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DOL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/B0k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public g:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2577802
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ISL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2577803
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    .line 2577804
    const v0, 0x7f0e0786

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2577805
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/ISL;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/1EV;->b(LX/0QB;)LX/1EV;

    move-result-object v4

    check-cast v4, LX/1EV;

    invoke-static {v0}, LX/DOL;->a(LX/0QB;)LX/DOL;

    move-result-object v5

    check-cast v5, LX/DOL;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p2

    check-cast p2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/B0k;->a(LX/0QB;)LX/B0k;

    move-result-object v0

    check-cast v0, LX/B0k;

    iput-object v3, v2, LX/ISL;->a:LX/17W;

    iput-object v4, v2, LX/ISL;->b:LX/1EV;

    iput-object v5, v2, LX/ISL;->c:LX/DOL;

    iput-object p2, v2, LX/ISL;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, v2, LX/ISL;->e:LX/B0k;

    .line 2577806
    const v0, 0x7f03084f

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2577807
    const v0, 0x7f0d15af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/ISL;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2577808
    new-instance v1, LX/ISK;

    invoke-direct {v1, p0, p1}, LX/ISK;-><init>(LX/ISL;Landroid/content/Context;)V

    .line 2577809
    const v0, 0x7f0d15ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2577810
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2577811
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2577812
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2577813
    iput-object p1, p0, LX/ISL;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2577814
    const/4 v0, 0x0

    .line 2577815
    iget-object v1, p0, LX/ISL;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ISL;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ISL;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2577816
    iget-object v0, p0, LX/ISL;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2577817
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2577818
    if-lez v0, :cond_1

    .line 2577819
    iget-object v1, p0, LX/ISL;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577820
    iget-object v1, p0, LX/ISL;->g:Lcom/facebook/resources/ui/FbTextView;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2577821
    :goto_0
    return-void

    .line 2577822
    :cond_1
    iget-object v1, p0, LX/ISL;->g:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
