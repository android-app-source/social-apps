.class public LX/HNc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1Ck;

.field public b:LX/HNV;

.field public c:LX/HNW;

.field public d:LX/HNb;

.field public e:LX/HO0;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

.field public h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

.field public i:LX/CYE;

.field public j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ck;LX/HNV;LX/HNW;Ljava/lang/String;LX/CYE;Ljava/util/Map;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/CYE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/Map;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/HNV;",
            "LX/HNW;",
            "Ljava/lang/String;",
            "LX/CYE;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458644
    iput-object p1, p0, LX/HNc;->a:LX/1Ck;

    .line 2458645
    iput-object p2, p0, LX/HNc;->b:LX/HNV;

    .line 2458646
    iput-object p3, p0, LX/HNc;->c:LX/HNW;

    .line 2458647
    iput-object p4, p0, LX/HNc;->f:Ljava/lang/String;

    .line 2458648
    iput-object p5, p0, LX/HNc;->i:LX/CYE;

    .line 2458649
    iput-object p6, p0, LX/HNc;->j:Ljava/util/Map;

    .line 2458650
    iput-object p7, p0, LX/HNc;->g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2458651
    iput-object p8, p0, LX/HNc;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    .line 2458652
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 2

    .prologue
    .line 2458653
    if-nez p0, :cond_0

    .line 2458654
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2458655
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LEGACY_CTA_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 2458656
    iget-object v0, p0, LX/HNc;->i:LX/CYE;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HNc;->i:LX/CYE;

    .line 2458657
    iget-boolean p0, v0, LX/CYE;->mUseActionFlow:Z

    move v0, p0

    .line 2458658
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    .line 2458659
    iget-object v0, p0, LX/HNc;->a:LX/1Ck;

    const-string v1, "add_cta_action_mutation"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HNc;->a:LX/1Ck;

    const-string v1, "replace_cta_action_mutation"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2458660
    :cond_0
    :goto_0
    return-void

    .line 2458661
    :cond_1
    iget-object v1, p0, LX/HNc;->a:LX/1Ck;

    const-string v2, "create_cta_mutation"

    iget-object v0, p0, LX/HNc;->b:LX/HNV;

    iget-object v3, p0, LX/HNc;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v3

    iget-object v4, p0, LX/HNc;->f:Ljava/lang/String;

    iget-object v5, p0, LX/HNc;->g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iget-object v6, p0, LX/HNc;->j:Ljava/util/Map;

    invoke-direct {p0}, LX/HNc;->c()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 2458662
    :goto_1
    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v9, v7

    .line 2458663
    :goto_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2458664
    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 2458665
    new-instance v12, LX/4Hi;

    invoke-direct {v12}, LX/4Hi;-><init>()V

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v12, v8}, LX/4Hi;->a(Ljava/lang/String;)LX/4Hi;

    move-result-object v8

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, LX/4Hi;->b(Ljava/lang/String;)LX/4Hi;

    move-result-object v7

    invoke-interface {v10, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2458666
    :cond_2
    const-string v7, "NONE"

    move-object v9, v7

    goto :goto_2

    .line 2458667
    :cond_3
    new-instance v7, LX/4Hg;

    invoke-direct {v7}, LX/4Hg;-><init>()V

    .line 2458668
    const-string v8, "page_id"

    invoke-virtual {v7, v8, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458669
    move-object v7, v7

    .line 2458670
    const-string v8, "MOBILE_PAGE_PRESENCE_CALL_TO_ACTION"

    .line 2458671
    const-string v11, "source"

    invoke-virtual {v7, v11, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458672
    move-object v7, v7

    .line 2458673
    const-string v8, "cta_type"

    invoke-virtual {v7, v8, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458674
    move-object v7, v7

    .line 2458675
    const-string v8, "fields_data"

    invoke-virtual {v7, v8, v10}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2458676
    move-object v7, v7

    .line 2458677
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 2458678
    const-string v9, "is_for_primary_cta"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2458679
    move-object v7, v7

    .line 2458680
    new-instance v8, LX/8FI;

    invoke-direct {v8}, LX/8FI;-><init>()V

    move-object v8, v8

    .line 2458681
    const-string v9, "input"

    invoke-virtual {v8, v9, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/8FI;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2458682
    iget-object v8, v3, LX/HNU;->a:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v7}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v0, v7

    .line 2458683
    new-instance v3, LX/HNX;

    invoke-direct {v3, p0}, LX/HNX;-><init>(LX/HNc;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1
.end method
