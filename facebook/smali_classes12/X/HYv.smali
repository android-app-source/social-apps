.class public LX/HYv;
.super LX/HYp;
.source ""


# instance fields
.field public final b:LX/HYq;

.field public final c:LX/HYs;

.field public final d:Lcom/facebook/registration/model/SimpleRegFormData;

.field public final e:LX/HZt;

.field public final f:LX/0Uh;

.field public final g:LX/HaQ;

.field public h:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "LX/HYi;",
            "LX/HYm;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/HYj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/HYq;LX/HYs;Lcom/facebook/registration/model/SimpleRegFormData;LX/HZt;LX/0Uh;LX/HaQ;)V
    .locals 9
    .param p5    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2481136
    invoke-direct {p0}, LX/HYp;-><init>()V

    .line 2481137
    iput-object p1, p0, LX/HYv;->b:LX/HYq;

    .line 2481138
    iput-object p2, p0, LX/HYv;->c:LX/HYs;

    .line 2481139
    iput-object p3, p0, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2481140
    iput-object p4, p0, LX/HYv;->e:LX/HZt;

    .line 2481141
    iput-object p5, p0, LX/HYv;->f:LX/0Uh;

    .line 2481142
    iput-object p6, p0, LX/HYv;->g:LX/HaQ;

    .line 2481143
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->START_COMPLETED:LX/HYj;

    iget-object p3, p0, LX/HYv;->b:LX/HYq;

    invoke-virtual {p3}, LX/HYp;->c()LX/HYm;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481144
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    iget-object p2, p0, LX/HYv;->b:LX/HYq;

    .line 2481145
    iget-object p3, p2, LX/HYp;->a:Ljava/util/Map;

    invoke-static {p3}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object p3

    move-object p2, p3

    .line 2481146
    invoke-interface {p1, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2481147
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->PHONE_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationNameFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481148
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->EMAIL_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationNameFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481149
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->PREFILL_EMAIL_UNFINISHED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationEmailFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481150
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->NAME_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481151
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->BIRTHDAY_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationGenderFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481152
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->GENDER_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481153
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->PASSWORD_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481154
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->TERMS_ACCEPTED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481155
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->CREATE_ERROR:LX/HYj;

    invoke-static {p0}, LX/HYv;->l(LX/HYv;)LX/HYm;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481156
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->VALIDATION_START:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481157
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->VALIDATION_SUCCESS:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481158
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->VALIDATION_ERROR:LX/HYj;

    invoke-static {p0}, LX/HYv;->l(LX/HYv;)LX/HYm;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481159
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->CREATE_SUCCESS:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481160
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->ERROR_CONTINUE:LX/HYj;

    iget-object p3, p0, LX/HYv;->b:LX/HYq;

    invoke-virtual {p3}, LX/HYp;->e()LX/HYm;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481161
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->UNKNOWN_ERROR:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationErrorFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481162
    iget-object p1, p0, LX/HYv;->g:LX/HaQ;

    invoke-virtual {p1}, LX/HaQ;->a()Z

    move-result p1

    if-nez p1, :cond_0

    .line 2481163
    :goto_0
    iget-object p1, p0, LX/HYv;->f:LX/0Uh;

    const/16 p2, 0x30

    invoke-virtual {p1, p2}, LX/0Uh;->a(I)LX/03R;

    move-result-object p1

    .line 2481164
    iget-object p2, p0, LX/HYv;->e:LX/HZt;

    const-string p3, "reg_additional_email_step"

    invoke-virtual {p2, p3, p1}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2481165
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, LX/03R;->asBoolean(Z)Z

    move-result p1

    move p1, p1

    .line 2481166
    if-nez p1, :cond_1

    .line 2481167
    :goto_1
    iget-object p1, p0, LX/HYv;->f:LX/0Uh;

    const/16 p2, 0x3c

    invoke-virtual {p1, p2}, LX/0Uh;->a(I)LX/03R;

    move-result-object p1

    .line 2481168
    iget-object p2, p0, LX/HYv;->e:LX/HZt;

    const-string p3, "reg_reorder_cp_step"

    invoke-virtual {p2, p3, p1}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2481169
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, LX/03R;->asBoolean(Z)Z

    move-result p1

    move p1, p1

    .line 2481170
    if-nez p1, :cond_3

    .line 2481171
    :goto_2
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object p1

    iput-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    .line 2481172
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->START:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationStartFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481173
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    iget-object p3, p0, LX/HYv;->c:LX/HYs;

    invoke-virtual {p3}, LX/HYp;->e()LX/HYm;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481174
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->PHONE:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481175
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->EMAIL:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationEmailFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481176
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->NAME:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationNameFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481177
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->BIRTHDAY:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481178
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->GENDER:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationGenderFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481179
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->PASSWORD:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481180
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->CREATE:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481181
    iget-object p1, p0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object p2, LX/HYi;->UNKNOWN:LX/HYi;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationErrorFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->c()LX/HZ1;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481182
    sget-object v0, LX/HYj;->START_COMPLETED:LX/HYj;

    sget-object v1, LX/HYj;->PHONE_ACQUIRED:LX/HYj;

    sget-object v2, LX/HYj;->EMAIL_ACQUIRED:LX/HYj;

    sget-object v3, LX/HYj;->NAME_ACQUIRED:LX/HYj;

    sget-object v4, LX/HYj;->BIRTHDAY_ACQUIRED:LX/HYj;

    sget-object v5, LX/HYj;->GENDER_ACQUIRED:LX/HYj;

    const/4 v6, 0x2

    new-array v6, v6, [LX/HYj;

    const/4 v7, 0x0

    sget-object v8, LX/HYj;->PASSWORD_ACQUIRED:LX/HYj;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, LX/HYj;->TERMS_ACCEPTED:LX/HYj;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/HYv;->i:LX/0Rf;

    .line 2481183
    return-void

    .line 2481184
    :cond_0
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->TERMS_ACCEPTED:LX/HYj;

    iget-object p3, p0, LX/HYv;->b:LX/HYq;

    invoke-virtual {p3}, LX/HYp;->c()LX/HYm;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481185
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->PASSWORD_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2481186
    :cond_1
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->PASSWORD_ACQUIRED:LX/HYj;

    new-instance p3, LX/HYt;

    invoke-direct {p3, p0}, LX/HYt;-><init>(LX/HYv;)V

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481187
    iget-object p2, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p3, LX/HYj;->ADDITIONAL_EMAIL_ACQUIRED:LX/HYj;

    iget-object p1, p0, LX/HYv;->g:LX/HaQ;

    invoke-virtual {p1}, LX/HaQ;->a()Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-direct {p1, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p1}, LX/HZ1;->c()LX/HZ1;

    move-result-object p1

    :goto_3
    invoke-interface {p2, p3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_2
    new-instance p1, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;

    invoke-direct {p1, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p1}, LX/HZ1;->c()LX/HZ1;

    move-result-object p1

    goto :goto_3

    .line 2481188
    :cond_3
    iget-object p1, p0, LX/HYv;->g:LX/HaQ;

    invoke-virtual {p1}, LX/HaQ;->a()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 2481189
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->TERMS_ACCEPTED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationNameFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481190
    :goto_4
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->NAME_ACQUIRED:LX/HYj;

    iget-object p3, p0, LX/HYv;->b:LX/HYq;

    .line 2481191
    const/4 p4, 0x1

    const/4 p5, 0x0

    invoke-virtual {p3, p4, p5}, LX/HYp;->a(ZZ)LX/HYm;

    move-result-object p4

    move-object p3, p4

    .line 2481192
    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481193
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->PHONE_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481194
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->EMAIL_ACQUIRED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p3}, LX/HZ1;->b()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 2481195
    :cond_4
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->START_COMPLETED:LX/HYj;

    new-instance p3, LX/HZ1;

    const-class p4, Lcom/facebook/registration/fragment/RegistrationNameFragment;

    invoke-direct {p3, p4}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4
.end method

.method public static b(LX/0QB;)LX/HYv;
    .locals 7

    .prologue
    .line 2481128
    new-instance v0, LX/HYv;

    .line 2481129
    new-instance v4, LX/HYq;

    invoke-static {p0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v1

    check-cast v1, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {p0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v2

    check-cast v2, LX/HZt;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v1, v2, v3}, LX/HYq;-><init>(Lcom/facebook/registration/model/SimpleRegFormData;LX/HZt;LX/0Uh;)V

    .line 2481130
    move-object v1, v4

    .line 2481131
    check-cast v1, LX/HYq;

    .line 2481132
    new-instance v4, LX/HYs;

    invoke-static {p0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v2

    check-cast v2, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v2, v3}, LX/HYs;-><init>(Lcom/facebook/registration/model/SimpleRegFormData;LX/0Uh;)V

    .line 2481133
    move-object v2, v4

    .line 2481134
    check-cast v2, LX/HYs;

    invoke-static {p0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v3

    check-cast v3, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {p0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v4

    check-cast v4, LX/HZt;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {p0}, LX/HaQ;->b(LX/0QB;)LX/HaQ;

    move-result-object v6

    check-cast v6, LX/HaQ;

    invoke-direct/range {v0 .. v6}, LX/HYv;-><init>(LX/HYq;LX/HYs;Lcom/facebook/registration/model/SimpleRegFormData;LX/HZt;LX/0Uh;LX/HaQ;)V

    .line 2481135
    return-object v0
.end method

.method public static l(LX/HYv;)LX/HYm;
    .locals 1

    .prologue
    .line 2481127
    new-instance v0, LX/HYu;

    invoke-direct {v0, p0}, LX/HYu;-><init>(LX/HYv;)V

    return-object v0
.end method


# virtual methods
.method public final a(ZZ)LX/HYm;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2481105
    iget-object v0, p0, LX/HYv;->g:LX/HaQ;

    invoke-virtual {v0}, LX/HaQ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2481106
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    .line 2481107
    iput-boolean p1, v0, LX/HZ1;->b:Z

    .line 2481108
    move-object v0, v0

    .line 2481109
    iput-boolean p2, v0, LX/HZ1;->c:Z

    .line 2481110
    move-object v0, v0

    .line 2481111
    iput v2, v0, LX/HZ1;->d:I

    .line 2481112
    move-object v0, v0

    .line 2481113
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationStartFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    .line 2481114
    iput-boolean p1, v0, LX/HZ1;->b:Z

    .line 2481115
    move-object v0, v0

    .line 2481116
    iput-boolean p2, v0, LX/HZ1;->c:Z

    .line 2481117
    move-object v0, v0

    .line 2481118
    iput v2, v0, LX/HZ1;->d:I

    .line 2481119
    move-object v0, v0

    .line 2481120
    goto :goto_0
.end method

.method public final a(LX/HYj;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2481121
    iget-object v0, p0, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HYv;->i:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2481122
    sget-object p1, LX/HYj;->VALIDATION_START:LX/HYj;

    .line 2481123
    :cond_0
    iget-object v0, p0, LX/HYp;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HYm;

    .line 2481124
    if-nez v0, :cond_1

    .line 2481125
    iget-object v0, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object v1, LX/HYj;->UNKNOWN_ERROR:LX/HYj;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HYm;

    .line 2481126
    :cond_1
    invoke-interface {v0}, LX/HYm;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
