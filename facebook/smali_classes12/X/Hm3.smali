.class public final enum LX/Hm3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hm3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hm3;

.field public static final enum APK:LX/Hm3;

.field public static final enum ERROR:LX/Hm3;

.field public static final enum INCOMPATIBLE_VERSION:LX/Hm3;

.field public static final enum INTEND_TO_SEND:LX/Hm3;

.field public static final enum NOTHING_TO_SEND:LX/Hm3;

.field public static final enum PREFLIGHT_INFO:LX/Hm3;

.field public static final enum TRANSFER_SUCCESS:LX/Hm3;


# instance fields
.field private final mAsInt:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2500024
    new-instance v0, LX/Hm3;

    const-string v1, "ERROR"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v4, v2}, LX/Hm3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hm3;->ERROR:LX/Hm3;

    .line 2500025
    new-instance v0, LX/Hm3;

    const-string v1, "INCOMPATIBLE_VERSION"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v5, v2}, LX/Hm3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hm3;->INCOMPATIBLE_VERSION:LX/Hm3;

    .line 2500026
    new-instance v0, LX/Hm3;

    const-string v1, "PREFLIGHT_INFO"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v6, v2}, LX/Hm3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hm3;->PREFLIGHT_INFO:LX/Hm3;

    .line 2500027
    new-instance v0, LX/Hm3;

    const-string v1, "NOTHING_TO_SEND"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v7, v2}, LX/Hm3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hm3;->NOTHING_TO_SEND:LX/Hm3;

    .line 2500028
    new-instance v0, LX/Hm3;

    const-string v1, "INTEND_TO_SEND"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v8, v2}, LX/Hm3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hm3;->INTEND_TO_SEND:LX/Hm3;

    .line 2500029
    new-instance v0, LX/Hm3;

    const-string v1, "APK"

    const/4 v2, 0x5

    const/16 v3, 0x6b

    invoke-direct {v0, v1, v2, v3}, LX/Hm3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hm3;->APK:LX/Hm3;

    .line 2500030
    new-instance v0, LX/Hm3;

    const-string v1, "TRANSFER_SUCCESS"

    const/4 v2, 0x6

    const/16 v3, 0x6c

    invoke-direct {v0, v1, v2, v3}, LX/Hm3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hm3;->TRANSFER_SUCCESS:LX/Hm3;

    .line 2500031
    const/4 v0, 0x7

    new-array v0, v0, [LX/Hm3;

    sget-object v1, LX/Hm3;->ERROR:LX/Hm3;

    aput-object v1, v0, v4

    sget-object v1, LX/Hm3;->INCOMPATIBLE_VERSION:LX/Hm3;

    aput-object v1, v0, v5

    sget-object v1, LX/Hm3;->PREFLIGHT_INFO:LX/Hm3;

    aput-object v1, v0, v6

    sget-object v1, LX/Hm3;->NOTHING_TO_SEND:LX/Hm3;

    aput-object v1, v0, v7

    sget-object v1, LX/Hm3;->INTEND_TO_SEND:LX/Hm3;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Hm3;->APK:LX/Hm3;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Hm3;->TRANSFER_SUCCESS:LX/Hm3;

    aput-object v2, v0, v1

    sput-object v0, LX/Hm3;->$VALUES:[LX/Hm3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2500021
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2500022
    iput p3, p0, LX/Hm3;->mAsInt:I

    .line 2500023
    return-void
.end method

.method public static fromInt(I)LX/Hm3;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2500016
    invoke-static {}, LX/Hm3;->values()[LX/Hm3;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2500017
    invoke-virtual {v0}, LX/Hm3;->getInt()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 2500018
    :goto_1
    return-object v0

    .line 2500019
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2500020
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hm3;
    .locals 1

    .prologue
    .line 2500013
    const-class v0, LX/Hm3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hm3;

    return-object v0
.end method

.method public static values()[LX/Hm3;
    .locals 1

    .prologue
    .line 2500015
    sget-object v0, LX/Hm3;->$VALUES:[LX/Hm3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hm3;

    return-object v0
.end method


# virtual methods
.method public final getInt()I
    .locals 1

    .prologue
    .line 2500014
    iget v0, p0, LX/Hm3;->mAsInt:I

    return v0
.end method
