.class public final LX/HOr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;)V
    .locals 0

    .prologue
    .line 2461033
    iput-object p1, p0, LX/HOr;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x26

    const v1, 0x3e17769

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2461034
    const-string v0, "extra_result"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object v0

    .line 2461035
    sget-object v2, LX/7m7;->SUCCESS:LX/7m7;

    if-eq v0, v2, :cond_0

    sget-object v2, LX/7m7;->CANCELLED:LX/7m7;

    if-eq v0, v2, :cond_0

    .line 2461036
    iget-object v0, p0, LX/HOr;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->o:LX/0kL;

    new-instance v2, LX/27k;

    iget-object v3, p0, LX/HOr;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080039

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2461037
    iget-object v0, p0, LX/HOr;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Cannot edit post"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461038
    :cond_0
    const/16 v0, 0x27

    const v2, 0x6646d176

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
