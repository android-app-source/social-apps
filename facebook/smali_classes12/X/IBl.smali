.class public LX/IBl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:LX/0wM;

.field public final c:I

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wM;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2547579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2547580
    iput-object p1, p0, LX/IBl;->a:Landroid/content/Context;

    .line 2547581
    iput-object p2, p0, LX/IBl;->b:LX/0wM;

    .line 2547582
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/IBl;->d:Landroid/content/res/Resources;

    .line 2547583
    iget-object v0, p0, LX/IBl;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1445

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/IBl;->c:I

    .line 2547584
    return-void
.end method

.method private static a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 2547585
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 2547586
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2547587
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, p2, v0, v1, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2547588
    return-object p0
.end method

.method public static b(LX/0QB;)LX/IBl;
    .locals 3

    .prologue
    .line 2547589
    new-instance v2, LX/IBl;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-direct {v2, v0, v1}, LX/IBl;-><init>(Landroid/content/Context;LX/0wM;)V

    .line 2547590
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;IZ)V
    .locals 4

    .prologue
    .line 2547591
    iget-object v0, p0, LX/IBl;->b:LX/0wM;

    const v1, -0x958e80

    invoke-virtual {v0, p2, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2547592
    const/4 p2, 0x0

    const/4 v3, 0x0

    .line 2547593
    if-nez p3, :cond_0

    .line 2547594
    iget v1, p0, LX/IBl;->c:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 2547595
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 2547596
    invoke-static {p1, v0, p2, p2, p2}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2547597
    invoke-static {p1, v1, v3, v3, v3}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 2547598
    :cond_0
    return-void
.end method

.method public final a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 5
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v4, 0x11

    .line 2547599
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    iget-object v2, p0, LX/IBl;->a:Landroid/content/Context;

    if-eqz p4, :cond_3

    const v0, 0x7f0e082e

    :goto_0
    invoke-direct {v1, v2, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2547600
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    iget-object v3, p0, LX/IBl;->a:Landroid/content/Context;

    if-eqz p4, :cond_4

    const v0, 0x7f0e082f

    :goto_1
    invoke-direct {v2, v3, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2547601
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2547602
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2547603
    invoke-static {v0, p2, v1, v4}, LX/IBl;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;

    .line 2547604
    :cond_0
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2547605
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 2547606
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2547607
    :cond_1
    invoke-static {v0, p3, v2, v4}, LX/IBl;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;

    .line 2547608
    :cond_2
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2547609
    return-void

    .line 2547610
    :cond_3
    const v0, 0x7f0e082c

    goto :goto_0

    .line 2547611
    :cond_4
    const v0, 0x7f0e082d

    goto :goto_1
.end method
