.class public LX/JSt;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

.field private b:F


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;)V
    .locals 1

    .prologue
    .line 2696184
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 2696185
    iget v0, p1, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->e:F

    move v0, v0

    .line 2696186
    iput v0, p0, LX/JSt;->b:F

    .line 2696187
    iput-object p1, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    .line 2696188
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2696179
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    .line 2696180
    iget v1, v0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->e:F

    move v0, v1

    .line 2696181
    iput v0, p0, LX/JSt;->b:F

    .line 2696182
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->clearAnimation()V

    .line 2696183
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2696189
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/JSt;->setDuration(J)V

    .line 2696190
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->setVisibility(I)V

    .line 2696191
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    invoke-virtual {v0, p0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2696192
    return-void
.end method

.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 2696175
    iget v0, p0, LX/JSt;->b:F

    const/high16 v1, 0x43b40000    # 360.0f

    iget v2, p0, LX/JSt;->b:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    .line 2696176
    iget-object v1, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->setAngle(F)V

    .line 2696177
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->requestLayout()V

    .line 2696178
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2696170
    const/4 v0, 0x0

    iput v0, p0, LX/JSt;->b:F

    .line 2696171
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->a()V

    .line 2696172
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->clearAnimation()V

    .line 2696173
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->setVisibility(I)V

    .line 2696174
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2696165
    iget-object v0, p0, LX/JSt;->a:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    .line 2696166
    iget v1, v0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->e:F

    move v0, v1

    .line 2696167
    iput v0, p0, LX/JSt;->b:F

    .line 2696168
    invoke-virtual {p0, p1}, LX/JSt;->a(I)V

    .line 2696169
    return-void
.end method
