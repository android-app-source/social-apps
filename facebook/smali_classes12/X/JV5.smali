.class public LX/JV5;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mX",
        "<",
        "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/JVN;

.field private d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;LX/25M;LX/JVN;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;",
            ">;",
            "LX/1Pf;",
            "LX/25M;",
            "LX/JVN;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700178
    invoke-direct {p0, p1, p2, p4, p5}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2700179
    iput-object p6, p0, LX/JV5;->c:LX/JVN;

    .line 2700180
    iput-object p3, p0, LX/JV5;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700181
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2700182
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2700183
    check-cast p2, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    .line 2700184
    iget-object v0, p0, LX/JV5;->c:LX/JVN;

    const/4 v1, 0x0

    .line 2700185
    new-instance v2, LX/JVM;

    invoke-direct {v2, v0}, LX/JVM;-><init>(LX/JVN;)V

    .line 2700186
    sget-object v3, LX/JVN;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JVL;

    .line 2700187
    if-nez v3, :cond_0

    .line 2700188
    new-instance v3, LX/JVL;

    invoke-direct {v3}, LX/JVL;-><init>()V

    .line 2700189
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JVL;->a$redex0(LX/JVL;LX/1De;IILX/JVM;)V

    .line 2700190
    move-object v2, v3

    .line 2700191
    move-object v1, v2

    .line 2700192
    move-object v0, v1

    .line 2700193
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v1

    .line 2700194
    iget-object v2, v0, LX/JVL;->a:LX/JVM;

    iput-object v1, v2, LX/JVM;->a:Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 2700195
    iget-object v2, v0, LX/JVL;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2700196
    move-object v0, v0

    .line 2700197
    iget-object v1, p0, LX/JV5;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700198
    iget-object v2, v0, LX/JVL;->a:LX/JVM;

    iput-object v1, v2, LX/JVM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700199
    iget-object v2, v0, LX/JVL;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2700200
    move-object v0, v0

    .line 2700201
    iget-object v1, v0, LX/JVL;->a:LX/JVM;

    iput-object p2, v1, LX/JVM;->c:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    .line 2700202
    iget-object v1, v0, LX/JVL;->d:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2700203
    move-object v0, v0

    .line 2700204
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2700205
    const/4 v0, 0x0

    return v0
.end method
