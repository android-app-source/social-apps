.class public LX/JCI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;

.field private static final b:I

.field private static final c:I

.field private static volatile h:LX/JCI;


# instance fields
.field private final d:LX/JDA;

.field private final e:LX/J94;

.field private final f:LX/JDT;

.field public final g:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2661855
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/JCI;->a:Ljava/lang/Object;

    .line 2661856
    invoke-static {}, LX/JCG;->values()[LX/JCG;

    move-result-object v0

    array-length v0, v0

    sput v0, LX/JCI;->b:I

    .line 2661857
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->values()[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v0

    array-length v0, v0

    sput v0, LX/JCI;->c:I

    return-void
.end method

.method public constructor <init>(LX/JDA;LX/J94;LX/JDT;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2661729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2661730
    iput-object p1, p0, LX/JCI;->d:LX/JDA;

    .line 2661731
    iput-object p2, p0, LX/JCI;->e:LX/J94;

    .line 2661732
    iput-object p3, p0, LX/JCI;->f:LX/JDT;

    .line 2661733
    iput-object p4, p0, LX/JCI;->g:Lcom/facebook/content/SecureContextHelper;

    .line 2661734
    return-void
.end method

.method public static a(LX/0QB;)LX/JCI;
    .locals 7

    .prologue
    .line 2661842
    sget-object v0, LX/JCI;->h:LX/JCI;

    if-nez v0, :cond_1

    .line 2661843
    const-class v1, LX/JCI;

    monitor-enter v1

    .line 2661844
    :try_start_0
    sget-object v0, LX/JCI;->h:LX/JCI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2661845
    if-eqz v2, :cond_0

    .line 2661846
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2661847
    new-instance p0, LX/JCI;

    invoke-static {v0}, LX/JDA;->a(LX/0QB;)LX/JDA;

    move-result-object v3

    check-cast v3, LX/JDA;

    invoke-static {v0}, LX/J94;->a(LX/0QB;)LX/J94;

    move-result-object v4

    check-cast v4, LX/J94;

    invoke-static {v0}, LX/JDT;->b(LX/0QB;)LX/JDT;

    move-result-object v5

    check-cast v5, LX/JDT;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5, v6}, LX/JCI;-><init>(LX/JDA;LX/J94;LX/JDT;Lcom/facebook/content/SecureContextHelper;)V

    .line 2661848
    move-object v0, p0

    .line 2661849
    sput-object v0, LX/JCI;->h:LX/JCI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2661850
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2661851
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2661852
    :cond_1
    sget-object v0, LX/JCI;->h:LX/JCI;

    return-object v0

    .line 2661853
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2661854
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/JCI;Ljava/lang/String;)Landroid/view/View$OnLongClickListener;
    .locals 1

    .prologue
    .line 2661841
    new-instance v0, LX/JCE;

    invoke-direct {v0, p0, p1}, LX/JCE;-><init>(LX/JCI;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(LX/JCI;Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2661836
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2661837
    invoke-virtual {p2, p3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2661838
    new-instance v0, LX/JCD;

    invoke-direct {v0, p0, p4}, LX/JCD;-><init>(LX/JCI;Landroid/content/Intent;)V

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2661839
    invoke-static {p0, p3}, LX/JCI;->a(LX/JCI;Ljava/lang/String;)Landroid/view/View$OnLongClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2661840
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 2661797
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2661798
    sget v1, LX/JCI;->c:I

    if-ge p2, v1, :cond_0

    .line 2661799
    iget-object v1, p0, LX/JCI;->d:LX/JDA;

    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->values()[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v2

    aget-object v2, v2, p2

    invoke-virtual {v1, v2, v0, p1}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    .line 2661800
    invoke-static {v1, v0}, LX/J94;->a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 2661801
    :goto_0
    return-object v0

    .line 2661802
    :cond_0
    invoke-static {}, LX/JCG;->values()[LX/JCG;

    move-result-object v1

    sget v2, LX/JCI;->c:I

    sub-int v2, p2, v2

    aget-object v1, v1, v2

    .line 2661803
    sget-object v2, LX/JCF;->a:[I

    invoke-virtual {v1}, LX/JCG;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2661804
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2661805
    :pswitch_0
    invoke-virtual {v1}, LX/JCG;->getItemLayoutResId()I

    move-result v1

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2661806
    invoke-static {v1, v0}, LX/J94;->a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2661807
    :pswitch_1
    new-instance v1, LX/JDe;

    invoke-direct {v1, p1}, LX/JDe;-><init>(Landroid/content/Context;)V

    .line 2661808
    invoke-static {v1, v0}, LX/J94;->b(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2661809
    :pswitch_2
    new-instance v1, LX/JDe;

    invoke-direct {v1, p1}, LX/JDe;-><init>(Landroid/content/Context;)V

    .line 2661810
    invoke-static {v1, v0}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2661811
    :pswitch_3
    new-instance v1, LX/JDe;

    invoke-direct {v1, p1}, LX/JDe;-><init>(Landroid/content/Context;)V

    .line 2661812
    invoke-static {v1, v0}, LX/J94;->a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2661813
    :pswitch_4
    invoke-virtual {v1}, LX/JCG;->getItemLayoutResId()I

    move-result v1

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2661814
    new-instance v2, LX/JCH;

    invoke-direct {v2, v1}, LX/JCH;-><init>(Landroid/view/View;)V

    .line 2661815
    invoke-virtual {v2}, LX/JCH;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2661816
    invoke-static {v1, v0}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 2661817
    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 2661818
    :pswitch_5
    invoke-virtual {v1}, LX/JCG;->getItemLayoutResId()I

    move-result v1

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2661819
    new-instance v2, LX/JCH;

    invoke-direct {v2, v1}, LX/JCH;-><init>(Landroid/view/View;)V

    .line 2661820
    invoke-virtual {v2}, LX/JCH;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2661821
    invoke-static {v1, v0}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 2661822
    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 2661823
    :pswitch_6
    invoke-virtual {v1}, LX/JCG;->getItemLayoutResId()I

    move-result v1

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2661824
    invoke-static {v1, v0}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2661825
    :pswitch_7
    invoke-virtual {v1}, LX/JCG;->getItemLayoutResId()I

    move-result v1

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2661826
    invoke-static {v1, v0}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2661827
    :pswitch_8
    invoke-virtual {v1}, LX/JCG;->getItemLayoutResId()I

    move-result v1

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2661828
    const v2, 0x7f0d0963

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2661829
    invoke-static {v1, v0}, LX/J94;->b(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2661830
    :pswitch_9
    invoke-virtual {v1}, LX/JCG;->getItemLayoutResId()I

    move-result v1

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2661831
    const v2, 0x7f0d0963

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2661832
    invoke-static {v1, v0}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2661833
    :pswitch_a
    invoke-virtual {v1}, LX/JCG;->getItemLayoutResId()I

    move-result v1

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2661834
    const v2, 0x7f0d0963

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2661835
    invoke-static {v1, v0}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;)V
    .locals 8

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2661735
    const v0, 0x7f0d0964

    invoke-virtual {p2, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2661736
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2661737
    const v0, 0x7f0d0967

    invoke-virtual {p2, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2661738
    const v1, 0x7f0d0968

    invoke-virtual {p2, v1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2661739
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v5, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    if-eqz v2, :cond_10

    .line 2661740
    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2661741
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v5, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2661742
    sget-object v2, LX/JCF;->b:[I

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2661743
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v4

    .line 2661744
    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_1

    :cond_4
    move v2, v4

    goto :goto_1

    .line 2661745
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2661746
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2661747
    iget-object v0, p0, LX/JCI;->f:LX/JDT;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/JDT;->a(Lcom/facebook/resources/ui/FbTextView;LX/0Px;)V

    goto :goto_2

    .line 2661748
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2661749
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2661750
    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    :goto_3
    if-eqz v3, :cond_0

    .line 2661751
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2661752
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_5
    move v3, v4

    .line 2661753
    goto :goto_3

    :cond_6
    move v3, v4

    goto :goto_3

    .line 2661754
    :pswitch_2
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 2661755
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2661756
    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    :goto_4
    if-eqz v3, :cond_0

    .line 2661757
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2661758
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_7
    move v3, v4

    .line 2661759
    goto :goto_4

    :cond_8
    move v3, v4

    goto :goto_4

    .line 2661760
    :pswitch_3
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_a

    .line 2661761
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2661762
    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    :goto_5
    if-eqz v3, :cond_0

    .line 2661763
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2661764
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2661765
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/JCI;->a(LX/JCI;Ljava/lang/String;)Landroid/view/View$OnLongClickListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto/16 :goto_2

    :cond_9
    move v3, v4

    .line 2661766
    goto :goto_5

    :cond_a
    move v3, v4

    goto :goto_5

    .line 2661767
    :pswitch_4
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2661768
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2661769
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2661770
    const-string v2, "\n"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v()LX/0Px;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p2, v1, v2, v0}, LX/JCI;->a(LX/JCI;Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 2661771
    :pswitch_5
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    move v0, v3

    :goto_6
    if-eqz v0, :cond_0

    .line 2661772
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2661773
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "tel:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2661774
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2661775
    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p2, v1, v2, v0}, LX/JCI;->a(LX/JCI;Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_b
    move v0, v4

    .line 2661776
    goto :goto_6

    :cond_c
    move v0, v4

    goto :goto_6

    .line 2661777
    :pswitch_6
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    :goto_7
    if-eqz v3, :cond_0

    .line 2661778
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2661779
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mailto:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2661780
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2661781
    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p2, v1, v2, v0}, LX/JCI;->a(LX/JCI;Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_d
    move v3, v4

    .line 2661782
    goto :goto_7

    :cond_e
    move v3, v4

    goto :goto_7

    .line 2661783
    :pswitch_7
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2661784
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2661785
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "geo:0,0?q="

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v()LX/0Px;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2661786
    const-string v2, "\n"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v()LX/0Px;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p2, v1, v2, v0}, LX/JCI;->a(LX/JCI;Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 2661787
    :pswitch_8
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2661788
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v4

    .line 2661789
    :goto_8
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u()LX/2uF;

    move-result-object v3

    invoke-virtual {v3}, LX/39O;->c()I

    move-result v3

    if-ge v0, v3, :cond_f

    .line 2661790
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u()LX/2uF;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v5, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2661791
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2661792
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 2661793
    :cond_f
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2661794
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2661795
    :cond_10
    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2661796
    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
