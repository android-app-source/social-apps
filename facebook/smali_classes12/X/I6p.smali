.class public final LX/I6p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/I6r;


# direct methods
.method public constructor <init>(LX/I6r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2537969
    iput-object p1, p0, LX/I6p;->b:LX/I6r;

    iput-object p2, p0, LX/I6p;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2537970
    new-instance v0, LX/I6s;

    invoke-direct {v0}, LX/I6s;-><init>()V

    move-object v0, v0

    .line 2537971
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2537972
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const/4 v2, 0x1

    .line 2537973
    iput-boolean v2, v1, LX/0zO;->p:Z

    .line 2537974
    move-object v1, v1

    .line 2537975
    new-instance v2, LX/I6s;

    invoke-direct {v2}, LX/I6s;-><init>()V

    const-string v3, "event_id"

    iget-object v4, p0, LX/I6p;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    .line 2537976
    iget-object v3, v2, LX/0gW;->e:LX/0w7;

    move-object v2, v3

    .line 2537977
    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2537978
    iget-object v1, p0, LX/I6p;->b:LX/I6r;

    iget-object v1, v1, LX/I6r;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
