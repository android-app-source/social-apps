.class public final enum LX/Hty;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hty;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hty;

.field public static final enum BIRTHDAY:LX/Hty;

.field public static final enum BRANDED_CONTENT:LX/Hty;

.field public static final enum FACECAST:LX/Hty;

.field public static final enum LIGHTWEIGHT_LOCATION:LX/Hty;

.field public static final enum LOCATION:LX/Hty;

.field public static final enum MINUTIAE:LX/Hty;

.field public static final enum PHOTO_GALLERY:LX/Hty;

.field public static final enum SLIDESHOW:LX/Hty;

.field public static final enum STORYLINE:LX/Hty;

.field public static final enum TAG_PEOPLE:LX/Hty;


# instance fields
.field private final mAnalyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2517002
    new-instance v0, LX/Hty;

    const-string v1, "PHOTO_GALLERY"

    const-string v2, "composer_media_inline_sprout"

    invoke-direct {v0, v1, v4, v2}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->PHOTO_GALLERY:LX/Hty;

    .line 2517003
    new-instance v0, LX/Hty;

    const-string v1, "TAG_PEOPLE"

    const-string v2, "composer_user_tagging_inline_sprout"

    invoke-direct {v0, v1, v5, v2}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->TAG_PEOPLE:LX/Hty;

    .line 2517004
    new-instance v0, LX/Hty;

    const-string v1, "MINUTIAE"

    const-string v2, "composer_activity_tagging_inline_sprout"

    invoke-direct {v0, v1, v6, v2}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->MINUTIAE:LX/Hty;

    .line 2517005
    new-instance v0, LX/Hty;

    const-string v1, "LOCATION"

    const-string v2, "composer_place_tagging_inline_sprout"

    invoke-direct {v0, v1, v7, v2}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->LOCATION:LX/Hty;

    .line 2517006
    new-instance v0, LX/Hty;

    const-string v1, "LIGHTWEIGHT_LOCATION"

    const-string v2, "location_lightweight_picker_sprout"

    invoke-direct {v0, v1, v8, v2}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->LIGHTWEIGHT_LOCATION:LX/Hty;

    .line 2517007
    new-instance v0, LX/Hty;

    const-string v1, "FACECAST"

    const/4 v2, 0x5

    const-string v3, "composer_facecast_inline_sprout"

    invoke-direct {v0, v1, v2, v3}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->FACECAST:LX/Hty;

    .line 2517008
    new-instance v0, LX/Hty;

    const-string v1, "BRANDED_CONTENT"

    const/4 v2, 0x6

    const-string v3, "branded_content_inline_sprout"

    invoke-direct {v0, v1, v2, v3}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->BRANDED_CONTENT:LX/Hty;

    .line 2517009
    new-instance v0, LX/Hty;

    const-string v1, "SLIDESHOW"

    const/4 v2, 0x7

    const-string v3, "composer_slideshow_inline_sprout"

    invoke-direct {v0, v1, v2, v3}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->SLIDESHOW:LX/Hty;

    .line 2517010
    new-instance v0, LX/Hty;

    const-string v1, "BIRTHDAY"

    const/16 v2, 0x8

    const-string v3, "composer_birthday_inline_sprout"

    invoke-direct {v0, v1, v2, v3}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->BIRTHDAY:LX/Hty;

    .line 2517011
    new-instance v0, LX/Hty;

    const-string v1, "STORYLINE"

    const/16 v2, 0x9

    const-string v3, "composer_storyline_inline_sprout"

    invoke-direct {v0, v1, v2, v3}, LX/Hty;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hty;->STORYLINE:LX/Hty;

    .line 2517012
    const/16 v0, 0xa

    new-array v0, v0, [LX/Hty;

    sget-object v1, LX/Hty;->PHOTO_GALLERY:LX/Hty;

    aput-object v1, v0, v4

    sget-object v1, LX/Hty;->TAG_PEOPLE:LX/Hty;

    aput-object v1, v0, v5

    sget-object v1, LX/Hty;->MINUTIAE:LX/Hty;

    aput-object v1, v0, v6

    sget-object v1, LX/Hty;->LOCATION:LX/Hty;

    aput-object v1, v0, v7

    sget-object v1, LX/Hty;->LIGHTWEIGHT_LOCATION:LX/Hty;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Hty;->FACECAST:LX/Hty;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Hty;->BRANDED_CONTENT:LX/Hty;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Hty;->SLIDESHOW:LX/Hty;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Hty;->BIRTHDAY:LX/Hty;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Hty;->STORYLINE:LX/Hty;

    aput-object v2, v0, v1

    sput-object v0, LX/Hty;->$VALUES:[LX/Hty;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2517013
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2517014
    iput-object p3, p0, LX/Hty;->mAnalyticsName:Ljava/lang/String;

    .line 2517015
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hty;
    .locals 1

    .prologue
    .line 2517016
    const-class v0, LX/Hty;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hty;

    return-object v0
.end method

.method public static values()[LX/Hty;
    .locals 1

    .prologue
    .line 2517017
    sget-object v0, LX/Hty;->$VALUES:[LX/Hty;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hty;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2517018
    iget-object v0, p0, LX/Hty;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method
