.class public final enum LX/HkY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HkY;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/HkY;

.field public static final enum b:LX/HkY;

.field private static final synthetic e:[LX/HkY;


# instance fields
.field private c:Z

.field private d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, LX/HkY;

    const-string v1, "GET"

    invoke-direct {v0, v1, v3, v2, v3}, LX/HkY;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/HkY;->a:LX/HkY;

    new-instance v0, LX/HkY;

    const-string v1, "POST"

    invoke-direct {v0, v1, v2, v2, v2}, LX/HkY;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/HkY;->b:LX/HkY;

    const/4 v0, 0x2

    new-array v0, v0, [LX/HkY;

    sget-object v1, LX/HkY;->a:LX/HkY;

    aput-object v1, v0, v3

    sget-object v1, LX/HkY;->b:LX/HkY;

    aput-object v1, v0, v2

    sput-object v0, LX/HkY;->e:[LX/HkY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, LX/HkY;->c:Z

    iput-boolean p4, p0, LX/HkY;->d:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HkY;
    .locals 1

    const-class v0, LX/HkY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HkY;

    return-object v0
.end method

.method public static values()[LX/HkY;
    .locals 1

    sget-object v0, LX/HkY;->e:[LX/HkY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HkY;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, LX/HkY;->c:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, LX/HkY;->d:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, LX/HkY;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
