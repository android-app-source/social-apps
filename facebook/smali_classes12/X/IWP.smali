.class public final enum LX/IWP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IWP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IWP;

.field public static final enum FETCH_GROUP_ALBUMS:LX/IWP;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2583844
    new-instance v0, LX/IWP;

    const-string v1, "FETCH_GROUP_ALBUMS"

    invoke-direct {v0, v1, v2}, LX/IWP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IWP;->FETCH_GROUP_ALBUMS:LX/IWP;

    .line 2583845
    const/4 v0, 0x1

    new-array v0, v0, [LX/IWP;

    sget-object v1, LX/IWP;->FETCH_GROUP_ALBUMS:LX/IWP;

    aput-object v1, v0, v2

    sput-object v0, LX/IWP;->$VALUES:[LX/IWP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2583847
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IWP;
    .locals 1

    .prologue
    .line 2583848
    const-class v0, LX/IWP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IWP;

    return-object v0
.end method

.method public static values()[LX/IWP;
    .locals 1

    .prologue
    .line 2583846
    sget-object v0, LX/IWP;->$VALUES:[LX/IWP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IWP;

    return-object v0
.end method
