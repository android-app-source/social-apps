.class public LX/JQ6;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mX",
        "<",
        "LX/4ZX;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;

.field private final e:LX/1Pm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;LX/1Pm;LX/25M;Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4ZX;",
            ">;",
            "LX/1Pm;",
            "LX/25M;",
            "Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691022
    invoke-direct {p0, p1, p3, p4, p5}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2691023
    iput-object p2, p0, LX/JQ6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691024
    iput-object p6, p0, LX/JQ6;->d:Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;

    .line 2691025
    iput-object p4, p0, LX/JQ6;->e:LX/1Pm;

    .line 2691026
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2691021
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2691027
    check-cast p2, LX/4ZX;

    .line 2691028
    iget-object v0, p0, LX/JQ6;->d:Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;

    const/4 v1, 0x0

    .line 2691029
    new-instance v2, LX/JPy;

    invoke-direct {v2, v0}, LX/JPy;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;)V

    .line 2691030
    sget-object v3, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JPx;

    .line 2691031
    if-nez v3, :cond_0

    .line 2691032
    new-instance v3, LX/JPx;

    invoke-direct {v3}, LX/JPx;-><init>()V

    .line 2691033
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JPx;->a$redex0(LX/JPx;LX/1De;IILX/JPy;)V

    .line 2691034
    move-object v2, v3

    .line 2691035
    move-object v1, v2

    .line 2691036
    move-object v0, v1

    .line 2691037
    iget-object v1, v0, LX/JPx;->a:LX/JPy;

    iput-object p2, v1, LX/JPy;->b:LX/4ZX;

    .line 2691038
    iget-object v1, v0, LX/JPx;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2691039
    move-object v0, v0

    .line 2691040
    iget-object v1, p0, LX/JQ6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691041
    iget-object v2, v0, LX/JPx;->a:LX/JPy;

    iput-object v1, v2, LX/JPy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691042
    iget-object v2, v0, LX/JPx;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2691043
    move-object v0, v0

    .line 2691044
    iget-object v1, p0, LX/JQ6;->e:LX/1Pm;

    .line 2691045
    iget-object v2, v0, LX/JPx;->a:LX/JPy;

    iput-object v1, v2, LX/JPy;->c:LX/1Pm;

    .line 2691046
    iget-object v2, v0, LX/JPx;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2691047
    move-object v0, v0

    .line 2691048
    iget-object v1, p0, LX/3mX;->l:LX/3mj;

    move-object v1, v1

    .line 2691049
    iget-object v2, v0, LX/JPx;->a:LX/JPy;

    iput-object v1, v2, LX/JPy;->f:LX/3mj;

    .line 2691050
    iget-object v2, v0, LX/JPx;->d:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2691051
    move-object v0, v0

    .line 2691052
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2691020
    const/4 v0, 0x0

    return v0
.end method
