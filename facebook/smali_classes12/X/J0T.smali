.class public LX/J0T;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;",
        "Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637126
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2637127
    return-void
.end method

.method public static a(LX/0QB;)LX/J0T;
    .locals 2

    .prologue
    .line 2637128
    new-instance v1, LX/J0T;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-direct {v1, v0}, LX/J0T;-><init>(LX/0sO;)V

    .line 2637129
    move-object v0, v1

    .line 2637130
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2637131
    check-cast p1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;

    .line 2637132
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637133
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    move-object v0, v0

    .line 2637134
    sget-object v1, LX/J1G;->INCOMING:LX/J1G;

    if-ne v0, v1, :cond_0

    .line 2637135
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;

    .line 2637136
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingPaymentRequestsQueryModel$IncomingPeerToPeerPaymentRequestsModel;->a()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2637137
    move-object v1, v0

    .line 2637138
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2637139
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2637140
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2637141
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2637142
    :cond_0
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel;

    .line 2637143
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel$OutgoingPeerToPeerPaymentRequestsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingPaymentRequestsQueryModel$OutgoingPeerToPeerPaymentRequestsModel;->a()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2637144
    move-object v1, v0

    goto :goto_0

    .line 2637145
    :cond_1
    new-instance v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;

    invoke-direct {v0, v3}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2637146
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 2

    .prologue
    .line 2637147
    check-cast p1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;

    .line 2637148
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    move-object v0, v0

    .line 2637149
    sget-object v1, LX/J1G;->INCOMING:LX/J1G;

    if-ne v0, v1, :cond_0

    .line 2637150
    new-instance v0, LX/DtX;

    invoke-direct {v0}, LX/DtX;-><init>()V

    move-object v0, v0

    .line 2637151
    :goto_0
    return-object v0

    .line 2637152
    :cond_0
    new-instance v0, LX/Dta;

    invoke-direct {v0}, LX/Dta;-><init>()V

    move-object v0, v0

    .line 2637153
    goto :goto_0
.end method
