.class public final enum LX/ICR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ICR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ICR;

.field public static final enum FIVE_STAR:LX/ICR;

.field public static final enum FOUR_STAR:LX/ICR;

.field public static final enum ONE_STAR:LX/ICR;

.field public static final enum THREE_STAR:LX/ICR;

.field public static final enum TWO_STAR:LX/ICR;


# instance fields
.field private final mCS5Rating:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2549380
    new-instance v0, LX/ICR;

    const-string v1, "ONE_STAR"

    const-string v2, "ONE_STAR"

    invoke-direct {v0, v1, v3, v2}, LX/ICR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICR;->ONE_STAR:LX/ICR;

    .line 2549381
    new-instance v0, LX/ICR;

    const-string v1, "TWO_STAR"

    const-string v2, "TWO_STAR"

    invoke-direct {v0, v1, v4, v2}, LX/ICR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICR;->TWO_STAR:LX/ICR;

    .line 2549382
    new-instance v0, LX/ICR;

    const-string v1, "THREE_STAR"

    const-string v2, "THREE_STAR"

    invoke-direct {v0, v1, v5, v2}, LX/ICR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICR;->THREE_STAR:LX/ICR;

    .line 2549383
    new-instance v0, LX/ICR;

    const-string v1, "FOUR_STAR"

    const-string v2, "FOUR_STAR"

    invoke-direct {v0, v1, v6, v2}, LX/ICR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICR;->FOUR_STAR:LX/ICR;

    .line 2549384
    new-instance v0, LX/ICR;

    const-string v1, "FIVE_STAR"

    const-string v2, "FIVE_STAR"

    invoke-direct {v0, v1, v7, v2}, LX/ICR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICR;->FIVE_STAR:LX/ICR;

    .line 2549385
    const/4 v0, 0x5

    new-array v0, v0, [LX/ICR;

    sget-object v1, LX/ICR;->ONE_STAR:LX/ICR;

    aput-object v1, v0, v3

    sget-object v1, LX/ICR;->TWO_STAR:LX/ICR;

    aput-object v1, v0, v4

    sget-object v1, LX/ICR;->THREE_STAR:LX/ICR;

    aput-object v1, v0, v5

    sget-object v1, LX/ICR;->FOUR_STAR:LX/ICR;

    aput-object v1, v0, v6

    sget-object v1, LX/ICR;->FIVE_STAR:LX/ICR;

    aput-object v1, v0, v7

    sput-object v0, LX/ICR;->$VALUES:[LX/ICR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2549386
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2549387
    iput-object p3, p0, LX/ICR;->mCS5Rating:Ljava/lang/String;

    .line 2549388
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ICR;
    .locals 1

    .prologue
    .line 2549389
    const-class v0, LX/ICR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ICR;

    return-object v0
.end method

.method public static values()[LX/ICR;
    .locals 1

    .prologue
    .line 2549390
    sget-object v0, LX/ICR;->$VALUES:[LX/ICR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ICR;

    return-object v0
.end method


# virtual methods
.method public final toEventName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2549391
    sget-object v0, LX/ICP;->c:[I

    invoke-virtual {p0}, LX/ICR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2549392
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2549393
    :pswitch_0
    const-string v0, "story_gallery_survey_ratings_one_star"

    goto :goto_0

    .line 2549394
    :pswitch_1
    const-string v0, "story_gallery_survey_ratings_two_star"

    goto :goto_0

    .line 2549395
    :pswitch_2
    const-string v0, "story_gallery_survey_ratings_three_star"

    goto :goto_0

    .line 2549396
    :pswitch_3
    const-string v0, "story_gallery_survey_ratings_four_star"

    goto :goto_0

    .line 2549397
    :pswitch_4
    const-string v0, "story_gallery_survey_ratings_five_star"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2549398
    iget-object v0, p0, LX/ICR;->mCS5Rating:Ljava/lang/String;

    return-object v0
.end method
