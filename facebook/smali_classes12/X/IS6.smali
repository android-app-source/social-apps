.class public final LX/IS6;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic b:LX/ISI;


# direct methods
.method public constructor <init>(LX/ISI;LX/DML;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 0

    .prologue
    .line 2577442
    iput-object p1, p0, LX/IS6;->b:LX/ISI;

    iput-object p3, p0, LX/IS6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 2577443
    check-cast p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    .line 2577444
    iget-object v0, p0, LX/IS6;->b:LX/ISI;

    invoke-static {v0}, LX/ISI;->r(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2577445
    iget-object v0, p0, LX/IS6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    const/16 v3, 0x8

    const/4 p0, 0x0

    .line 2577446
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->r()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    move-result-object v2

    .line 2577447
    iget-object v1, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2577448
    iget-object v1, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2577449
    iget-object v1, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2577450
    iget-object v1, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->m:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2577451
    iget-object v1, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->k:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577452
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->l:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577453
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->m:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v1

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$MessagesModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577454
    iget-object v1, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2577455
    iget-object v1, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->n:Landroid/widget/Button;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2577456
    iget-object v1, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->n:Landroid/widget/Button;

    new-instance v2, LX/IQd;

    invoke-direct {v2, p1, v0}, LX/IQd;-><init>(Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2577457
    :goto_0
    return-void

    .line 2577458
    :cond_0
    iget-object v0, p0, LX/IS6;->b:LX/ISI;

    iget-object v0, v0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v1, p0, LX/IS6;->b:LX/ISI;

    iget-object v1, v1, LX/ISI;->c:LX/IRb;

    iget-object v2, p0, LX/IS6;->b:LX/ISI;

    iget-object v2, v2, LX/ISI;->s:Ljava/lang/String;

    const/16 p0, 0x8

    const/4 v5, 0x0

    .line 2577459
    if-eqz v2, :cond_1

    .line 2577460
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v5

    :goto_1
    if-ge v4, v7, :cond_1

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    .line 2577461
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2577462
    iput-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    .line 2577463
    :cond_1
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2577464
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v3, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2577465
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2577466
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->m:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2577467
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    if-nez v3, :cond_2

    .line 2577468
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    iput-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    .line 2577469
    :cond_2
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->k:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577470
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->l:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577471
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    iget-object v6, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2577472
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2577473
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->n:Landroid/widget/Button;

    iget-object v4, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->c:LX/0W9;

    invoke-virtual {v5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2577474
    :cond_3
    iget-boolean v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->r:Z

    if-nez v3, :cond_4

    .line 2577475
    const-string v3, "IMPRESSION"

    invoke-static {p1, v0, v3}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->a$redex0(Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;)V

    .line 2577476
    :cond_4
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->i:LX/ISp;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, LX/ISp;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/ISe;)V

    .line 2577477
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->n:Landroid/widget/Button;

    new-instance v4, LX/IQe;

    invoke-direct {v4, p1, v0, v1}, LX/IQe;-><init>(Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2577478
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v4, LX/IQf;

    invoke-direct {v4, p1, v0, v1}, LX/IQf;-><init>(Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2577479
    goto/16 :goto_0

    .line 2577480
    :cond_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_1
.end method
