.class public final enum LX/IzE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IzE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IzE;

.field public static final enum CARDS_EXIST_AND_ALL_IN_CACHE:LX/IzE;

.field public static final enum CARDS_EXIST_BUT_NOT_ALL_IN_CACHE:LX/IzE;

.field public static final enum NO_CARDS_EXIST:LX/IzE;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2634338
    new-instance v0, LX/IzE;

    const-string v1, "CARDS_EXIST_AND_ALL_IN_CACHE"

    invoke-direct {v0, v1, v2}, LX/IzE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IzE;->CARDS_EXIST_AND_ALL_IN_CACHE:LX/IzE;

    .line 2634339
    new-instance v0, LX/IzE;

    const-string v1, "NO_CARDS_EXIST"

    invoke-direct {v0, v1, v3}, LX/IzE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IzE;->NO_CARDS_EXIST:LX/IzE;

    .line 2634340
    new-instance v0, LX/IzE;

    const-string v1, "CARDS_EXIST_BUT_NOT_ALL_IN_CACHE"

    invoke-direct {v0, v1, v4}, LX/IzE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IzE;->CARDS_EXIST_BUT_NOT_ALL_IN_CACHE:LX/IzE;

    .line 2634341
    const/4 v0, 0x3

    new-array v0, v0, [LX/IzE;

    sget-object v1, LX/IzE;->CARDS_EXIST_AND_ALL_IN_CACHE:LX/IzE;

    aput-object v1, v0, v2

    sget-object v1, LX/IzE;->NO_CARDS_EXIST:LX/IzE;

    aput-object v1, v0, v3

    sget-object v1, LX/IzE;->CARDS_EXIST_BUT_NOT_ALL_IN_CACHE:LX/IzE;

    aput-object v1, v0, v4

    sput-object v0, LX/IzE;->$VALUES:[LX/IzE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2634342
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IzE;
    .locals 1

    .prologue
    .line 2634343
    const-class v0, LX/IzE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IzE;

    return-object v0
.end method

.method public static values()[LX/IzE;
    .locals 1

    .prologue
    .line 2634344
    sget-object v0, LX/IzE;->$VALUES:[LX/IzE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IzE;

    return-object v0
.end method
