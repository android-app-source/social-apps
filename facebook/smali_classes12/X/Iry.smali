.class public final LX/Iry;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:LX/Is1;


# direct methods
.method public constructor <init>(LX/Is1;)V
    .locals 0

    .prologue
    .line 2619121
    iput-object p1, p0, LX/Iry;->a:LX/Is1;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2619115
    iget-object v0, p0, LX/Iry;->a:LX/Is1;

    iget-object v0, v0, LX/Is1;->d:LX/Irx;

    .line 2619116
    iget-boolean v1, v0, LX/Iqg;->m:Z

    move v0, v1

    .line 2619117
    if-eqz v0, :cond_0

    .line 2619118
    :goto_0
    return-void

    .line 2619119
    :cond_0
    iget-object v0, p0, LX/Iry;->a:LX/Is1;

    iget-object v0, v0, LX/Is1;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2619120
    iget-object v0, p0, LX/Iry;->a:LX/Is1;

    iget-object v0, v0, LX/Is1;->d:LX/Irx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Iqg;->c(Z)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2619110
    sget-object v0, LX/Is1;->a:Ljava/lang/String;

    const-string v1, "Failed to load image: %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Iry;->a:LX/Is1;

    iget-object v4, v4, LX/Is1;->d:LX/Irx;

    .line 2619111
    iget-object p1, v4, LX/Irx;->a:Lcom/facebook/stickers/model/Sticker;

    move-object v4, p1

    .line 2619112
    iget-object v4, v4, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2619113
    iget-object v0, p0, LX/Iry;->a:LX/Is1;

    iget-object v0, v0, LX/Is1;->d:LX/Irx;

    invoke-virtual {v0, v5}, LX/Iqg;->c(Z)V

    .line 2619114
    return-void
.end method
