.class public LX/I8V;
.super LX/1Cv;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field public e:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2541594
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I8V;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2541562
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2541563
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/I8V;->b:LX/0Px;

    .line 2541564
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I8V;->c:Z

    .line 2541565
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 2541593
    iget-object v0, p0, LX/I8V;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2541584
    invoke-static {}, LX/I8U;->values()[LX/I8U;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2541585
    sget-object v1, LX/I8U;->ACTIVITY_ROW:LX/I8U;

    if-ne v0, v1, :cond_0

    .line 2541586
    new-instance v0, LX/I8M;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I8M;-><init>(Landroid/content/Context;)V

    .line 2541587
    :goto_0
    return-object v0

    .line 2541588
    :cond_0
    sget-object v1, LX/I8U;->PROGRESS_BAR_ROW:LX/I8U;

    if-ne v0, v1, :cond_1

    .line 2541589
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0304e3

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2541590
    :cond_1
    sget-object v1, LX/I8U;->NULL_STATE:LX/I8U;

    if-ne v0, v1, :cond_2

    .line 2541591
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0304e2

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2541592
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2541577
    invoke-static {}, LX/I8U;->values()[LX/I8U;

    move-result-object v0

    aget-object v0, v0, p4

    .line 2541578
    sget-object v1, LX/I8U;->ACTIVITY_ROW:LX/I8U;

    if-ne v0, v1, :cond_1

    .line 2541579
    check-cast p3, LX/I8M;

    .line 2541580
    check-cast p2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    iget-object v0, p0, LX/I8V;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v1, p0, LX/I8V;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p3, p2, v0, v1}, LX/I8M;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541581
    :cond_0
    :goto_0
    return-void

    .line 2541582
    :cond_1
    sget-object v1, LX/I8U;->PROGRESS_BAR_ROW:LX/I8U;

    if-ne v0, v1, :cond_0

    .line 2541583
    const v0, 0x7f0d0e2d

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, p0, LX/I8V;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2541595
    iget-object v0, p0, LX/I8V;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2541574
    invoke-direct {p0}, LX/I8V;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2541575
    sget-object v0, LX/I8V;->a:Ljava/lang/Object;

    .line 2541576
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/I8V;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2541573
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2541567
    invoke-direct {p0}, LX/I8V;->a()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2541568
    iget-object v0, p0, LX/I8V;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/I8V;->c:Z

    if-nez v0, :cond_0

    .line 2541569
    sget-object v0, LX/I8U;->NULL_STATE:LX/I8U;

    invoke-virtual {v0}, LX/I8U;->ordinal()I

    move-result v0

    .line 2541570
    :goto_0
    return v0

    .line 2541571
    :cond_0
    sget-object v0, LX/I8U;->PROGRESS_BAR_ROW:LX/I8U;

    invoke-virtual {v0}, LX/I8U;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2541572
    :cond_1
    sget-object v0, LX/I8U;->ACTIVITY_ROW:LX/I8U;

    invoke-virtual {v0}, LX/I8U;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2541566
    invoke-static {}, LX/I8U;->values()[LX/I8U;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
