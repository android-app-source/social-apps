.class public final LX/I07;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hxk;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/I08;


# direct methods
.method public constructor <init>(LX/I08;LX/Hxk;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2526564
    iput-object p1, p0, LX/I07;->c:LX/I08;

    iput-object p2, p0, LX/I07;->a:LX/Hxk;

    iput-object p3, p0, LX/I07;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2526563
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2526542
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 2526543
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2526544
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;

    .line 2526545
    iget-object v1, p0, LX/I07;->a:LX/Hxk;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2526546
    const/4 v4, 0x1

    .line 2526547
    iget-object v2, p0, LX/I07;->b:Ljava/lang/String;

    .line 2526548
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2526549
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2526550
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v3

    .line 2526551
    :goto_0
    if-ge v5, v8, :cond_3

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 2526552
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    move v1, v3

    .line 2526553
    :goto_1
    const/4 v3, 0x0

    .line 2526554
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2526555
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2526556
    :goto_2
    iget-object v3, p0, LX/I07;->a:LX/Hxk;

    invoke-interface {v3, v1, v2, v0, v6}, LX/Hxk;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 2526557
    :cond_0
    return-void

    .line 2526558
    :cond_1
    if-nez v2, :cond_2

    .line 2526559
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 2526560
    :cond_2
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2526561
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1

    :cond_4
    move v1, v3

    .line 2526562
    goto :goto_1

    :cond_5
    move-object v0, v3

    goto :goto_2
.end method
