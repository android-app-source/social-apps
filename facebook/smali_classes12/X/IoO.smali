.class public final enum LX/IoO;
.super LX/IoL;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;IFFIZ)V
    .locals 8

    .prologue
    .line 2611791
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, LX/IoL;-><init>(Ljava/lang/String;IFFIZLX/IoI;)V

    return-void
.end method


# virtual methods
.method public final getButtonText(Landroid/content/res/Resources;LX/IoK;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2611792
    sget-object v0, LX/IoJ;->a:[I

    invoke-virtual {p2}, LX/IoK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2611793
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid flowType for SlidingButton"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2611794
    :pswitch_0
    const v0, 0x7f082d59

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2611795
    :goto_0
    return-object v0

    :pswitch_1
    const v0, 0x7f082d5a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
