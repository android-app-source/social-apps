.class public final LX/Hd6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hd7;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

.field public final synthetic b:LX/Hd7;


# direct methods
.method public constructor <init>(LX/Hd7;)V
    .locals 1

    .prologue
    .line 2487831
    iput-object p1, p0, LX/Hd6;->b:LX/Hd7;

    .line 2487832
    move-object v0, p1

    .line 2487833
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2487834
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2487835
    const-string v0, "TopicFollowButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2487836
    if-ne p0, p1, :cond_1

    .line 2487837
    :cond_0
    :goto_0
    return v0

    .line 2487838
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2487839
    goto :goto_0

    .line 2487840
    :cond_3
    check-cast p1, LX/Hd6;

    .line 2487841
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2487842
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2487843
    if-eq v2, v3, :cond_0

    .line 2487844
    iget-object v2, p0, LX/Hd6;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Hd6;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    iget-object v3, p1, LX/Hd6;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2487845
    goto :goto_0

    .line 2487846
    :cond_4
    iget-object v2, p1, LX/Hd6;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
