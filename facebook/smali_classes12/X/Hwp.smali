.class public LX/Hwp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/0aG;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/2AD;

.field public final d:LX/2AD;

.field public final e:LX/Fqw;

.field public final f:LX/0si;

.field public final g:LX/3Ek;

.field public final h:LX/FS8;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bl3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0aG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0si;LX/Fqw;LX/2AD;LX/2AD;LX/3Ek;LX/FS8;LX/0Ot;)V
    .locals 0
    .param p5    # LX/2AD;
        .annotation runtime Lcom/facebook/notifications/annotations/RegularNotificationCache;
        .end annotation
    .end param
    .param p6    # LX/2AD;
        .annotation runtime Lcom/facebook/notifications/annotations/OverflowedNotificationCache;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0si;",
            "LX/Fqw;",
            "LX/2AD;",
            "LX/2AD;",
            "LX/3Ek;",
            "LX/FS8;",
            "LX/0Ot",
            "<",
            "LX/Bl3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2521434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2521435
    iput-object p1, p0, LX/Hwp;->a:LX/0aG;

    .line 2521436
    iput-object p2, p0, LX/Hwp;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2521437
    iput-object p5, p0, LX/Hwp;->c:LX/2AD;

    .line 2521438
    iput-object p6, p0, LX/Hwp;->d:LX/2AD;

    .line 2521439
    iput-object p4, p0, LX/Hwp;->e:LX/Fqw;

    .line 2521440
    iput-object p3, p0, LX/Hwp;->f:LX/0si;

    .line 2521441
    iput-object p7, p0, LX/Hwp;->g:LX/3Ek;

    .line 2521442
    iput-object p8, p0, LX/Hwp;->h:LX/FS8;

    .line 2521443
    iput-object p9, p0, LX/Hwp;->i:LX/0Ot;

    .line 2521444
    return-void
.end method
