.class public final LX/HI0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 2449733
    const/16 v17, 0x0

    .line 2449734
    const/16 v16, 0x0

    .line 2449735
    const/4 v15, 0x0

    .line 2449736
    const/4 v14, 0x0

    .line 2449737
    const/4 v13, 0x0

    .line 2449738
    const/4 v12, 0x0

    .line 2449739
    const/4 v7, 0x0

    .line 2449740
    const-wide/16 v10, 0x0

    .line 2449741
    const-wide/16 v8, 0x0

    .line 2449742
    const/4 v6, 0x0

    .line 2449743
    const/4 v5, 0x0

    .line 2449744
    const/4 v4, 0x0

    .line 2449745
    const/4 v3, 0x0

    .line 2449746
    const/4 v2, 0x0

    .line 2449747
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_10

    .line 2449748
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2449749
    const/4 v2, 0x0

    .line 2449750
    :goto_0
    return v2

    .line 2449751
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    if-eq v2, v0, :cond_a

    .line 2449752
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2449753
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2449754
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 2449755
    const-string v19, "boosting_status"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 2449756
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2449757
    :cond_1
    const-string v19, "budget"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 2449758
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 2449759
    :cond_2
    const-string v19, "has_editable_promotion"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 2449760
    const/4 v2, 0x1

    .line 2449761
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v16, v9

    move v9, v2

    goto :goto_1

    .line 2449762
    :cond_3
    const-string v19, "kpi"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 2449763
    const/4 v2, 0x1

    .line 2449764
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v15, v7

    move v7, v2

    goto :goto_1

    .line 2449765
    :cond_4
    const-string v19, "reach"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 2449766
    const/4 v2, 0x1

    .line 2449767
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v14, v6

    move v6, v2

    goto/16 :goto_1

    .line 2449768
    :cond_5
    const-string v19, "reset_period"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 2449769
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLAdproLimitResetPeriod;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdproLimitResetPeriod;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2449770
    :cond_6
    const-string v19, "spent"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 2449771
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2449772
    :cond_7
    const-string v19, "start_time"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 2449773
    const/4 v2, 0x1

    .line 2449774
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 2449775
    :cond_8
    const-string v19, "stop_time"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2449776
    const/4 v2, 0x1

    .line 2449777
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v10

    move v8, v2

    goto/16 :goto_1

    .line 2449778
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2449779
    :cond_a
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2449780
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2449781
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2449782
    if-eqz v9, :cond_b

    .line 2449783
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2449784
    :cond_b
    if-eqz v7, :cond_c

    .line 2449785
    const/4 v2, 0x3

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v7}, LX/186;->a(III)V

    .line 2449786
    :cond_c
    if-eqz v6, :cond_d

    .line 2449787
    const/4 v2, 0x4

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v6}, LX/186;->a(III)V

    .line 2449788
    :cond_d
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2449789
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2449790
    if-eqz v3, :cond_e

    .line 2449791
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2449792
    :cond_e
    if-eqz v8, :cond_f

    .line 2449793
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2449794
    :cond_f
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_10
    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v7

    move v7, v5

    move-wide/from16 v21, v8

    move v9, v6

    move v8, v2

    move v6, v4

    move-wide v4, v10

    move-wide/from16 v10, v21

    goto/16 :goto_1
.end method
