.class public final LX/HO2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459163
    iput-object p1, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x58ec3d35

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459164
    iget-object v1, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b:LX/CY3;

    iget-object v2, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    .line 2459165
    iget-object v3, v1, LX/CY3;->a:LX/0Zb;

    sget-object p1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_DELETE:LX/CY4;

    invoke-static {p1, v2}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v3, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2459166
    iget-object v1, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v2, LX/0ig;->ak:LX/0ih;

    const-string v3, "tap_delete_button"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2459167
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f081686

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    const v3, 0x7f081687

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    const v3, 0x7f080019

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->N:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    const v3, 0x7f080017

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/HO2;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->O:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2459168
    const v1, -0x15233a1e

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
