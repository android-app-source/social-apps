.class public final LX/JIc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/JIe;


# direct methods
.method public constructor <init>(LX/JIe;LX/4BY;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2678103
    iput-object p1, p0, LX/JIc;->c:LX/JIe;

    iput-object p2, p0, LX/JIc;->a:LX/4BY;

    iput-object p3, p0, LX/JIc;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2678104
    iget-object v0, p0, LX/JIc;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2678105
    iget-object v0, p0, LX/JIc;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2678106
    iget-object v0, p0, LX/JIc;->b:Landroid/content/Context;

    iget-object v1, p0, LX/JIc;->b:Landroid/content/Context;

    const v2, 0x7f083a70

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2678107
    iget-object v0, p0, LX/JIc;->c:LX/JIe;

    iget-object v0, v0, LX/JIe;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/JIc;->b:Landroid/content/Context;

    sget-object v2, LX/0ax;->dj:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2678108
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2678109
    iget-object v0, p0, LX/JIc;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2678110
    iget-object v0, p0, LX/JIc;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2678111
    iget-object v0, p0, LX/JIc;->c:LX/JIe;

    iget-object v0, v0, LX/JIe;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/JIc;->b:Landroid/content/Context;

    sget-object v2, LX/0ax;->dj:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2678112
    :cond_0
    return-void
.end method
