.class public final LX/IXL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2585934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585935
    iput-object p1, p0, LX/IXL;->a:Ljava/lang/String;

    .line 2585936
    iput-object p3, p0, LX/IXL;->c:Ljava/lang/String;

    .line 2585937
    iput-object p2, p0, LX/IXL;->b:Ljava/lang/String;

    .line 2585938
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2585927
    if-ne p0, p1, :cond_0

    .line 2585928
    const/4 v0, 0x1

    .line 2585929
    :goto_0
    return v0

    .line 2585930
    :cond_0
    instance-of v0, p1, LX/IXL;

    if-nez v0, :cond_1

    .line 2585931
    const/4 v0, 0x0

    goto :goto_0

    .line 2585932
    :cond_1
    check-cast p1, LX/IXL;

    .line 2585933
    iget-object v0, p0, LX/IXL;->c:Ljava/lang/String;

    iget-object v1, p1, LX/IXL;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2585926
    iget-object v0, p0, LX/IXL;->c:Ljava/lang/String;

    invoke-static {v0}, LX/1bi;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
