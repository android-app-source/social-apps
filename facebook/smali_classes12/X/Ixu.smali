.class public final LX/Ixu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<",
        "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;)V
    .locals 0

    .prologue
    .line 2632509
    iput-object p1, p0, LX/Ixu;->a:Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2632510
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2632511
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2632512
    const v3, 0x130084

    .line 2632513
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 2632514
    iget-object v1, p0, LX/Ixu;->a:Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;

    iget-object v1, v1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2632515
    iget-object v1, p0, LX/Ixu;->a:Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;

    iget-object v1, v1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "list_component"

    invoke-interface {v1, v3, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2632516
    iget-object v1, p0, LX/Ixu;->a:Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;

    iget-object v1, v1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2632517
    iget-object v2, p1, LX/2nj;->c:LX/2nk;

    move-object v2, v2

    .line 2632518
    invoke-virtual {v2}, LX/2nk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2632519
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2632520
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 2632521
    iget-object v1, p0, LX/Ixu;->a:Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;

    iget-object v1, v1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x130084

    const/16 v3, 0x61

    invoke-interface {v1, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2632522
    return-void
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2632523
    const/4 v3, 0x2

    .line 2632524
    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 2632525
    iget-object v1, p0, LX/Ixu;->a:Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;

    iget-object v1, v1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x130084

    invoke-interface {v1, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2632526
    return-void
.end method
