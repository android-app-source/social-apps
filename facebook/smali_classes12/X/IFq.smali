.class public final LX/IFq;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IIR;

.field public final synthetic b:LX/IFv;


# direct methods
.method public constructor <init>(LX/IFv;LX/IIR;)V
    .locals 0

    .prologue
    .line 2554895
    iput-object p1, p0, LX/IFq;->b:LX/IFv;

    iput-object p2, p0, LX/IFq;->a:LX/IIR;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2554896
    iget-object v0, p0, LX/IFq;->a:LX/IIR;

    .line 2554897
    iget-object v1, v0, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_FETCH_DATA:LX/IG6;

    invoke-virtual {v1, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2554898
    iget-object v1, v0, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_FETCH_DATA2:LX/IG6;

    invoke-virtual {v1, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2554899
    iget-object v1, v0, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->q(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2554900
    iget-object v1, v0, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->F(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2554901
    iget-object v1, v0, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->j:LX/03V;

    const-string p0, "friends_nearby_fetch_failed"

    invoke-virtual {v1, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2554902
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2554903
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2554904
    iget-object v1, p0, LX/IFq;->a:LX/IIR;

    .line 2554905
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2554906
    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;

    .line 2554907
    iget-object v2, v1, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v3, LX/IG6;->DASHBOARD_FETCH_DATA:LX/IG6;

    invoke-virtual {v2, v3}, LX/IG7;->b(LX/IG6;)V

    .line 2554908
    iget-object v2, v1, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v3, LX/IG6;->DASHBOARD_FETCH_DATA2:LX/IG6;

    invoke-virtual {v2, v3}, LX/IG7;->b(LX/IG6;)V

    .line 2554909
    iget-object v2, v1, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->q(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2554910
    iget-object v2, v1, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->F(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2554911
    iget-object v2, v1, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2554912
    invoke-static {v2, v0}, LX/IFX;->b(LX/IFX;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;)V

    .line 2554913
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/IFX;->y:Z

    .line 2554914
    invoke-static {v2}, LX/IFX;->o(LX/IFX;)V

    .line 2554915
    invoke-static {v2}, LX/IFX;->t(LX/IFX;)V

    .line 2554916
    iget-object v2, v1, LX/IIR;->a:LX/IIm;

    invoke-virtual {v2}, LX/IIm;->i()V

    .line 2554917
    iget-object v2, v1, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ac:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2554918
    iget-object v3, v1, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v1, LX/IIR;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ac:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/location/ImmutableLocation;

    .line 2554919
    iget-object p0, v3, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->f:LX/1Ck;

    sget-object p1, LX/IFy;->l:LX/IFy;

    new-instance v1, LX/IIS;

    invoke-direct {v1, v3, v2}, LX/IIS;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Lcom/facebook/location/ImmutableLocation;)V

    new-instance v0, LX/IIT;

    invoke-direct {v0, v3}, LX/IIT;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {p0, p1, v1, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2554920
    :cond_0
    return-void
.end method
