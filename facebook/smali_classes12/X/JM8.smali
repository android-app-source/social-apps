.class public final LX/JM8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/JM9;


# direct methods
.method public constructor <init>(LX/JM9;)V
    .locals 0

    .prologue
    .line 2682880
    iput-object p1, p0, LX/JM8;->a:LX/JM9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 12

    .prologue
    .line 2682881
    iget-object v0, p0, LX/JM8;->a:LX/JM9;

    iget-object v0, v0, LX/JM9;->b:LX/JMA;

    iget-object v0, v0, LX/JMA;->a:LX/JKh;

    if-eqz v0, :cond_0

    .line 2682882
    iget-object v0, p0, LX/JM8;->a:LX/JM9;

    iget-object v0, v0, LX/JM9;->b:LX/JMA;

    iget-object v0, v0, LX/JMA;->a:LX/JKh;

    iget-object v1, p0, LX/JM8;->a:LX/JM9;

    iget-object v1, v1, LX/JM9;->b:LX/JMA;

    iget-object v1, v1, LX/JMA;->b:Ljava/lang/String;

    .line 2682883
    const-string v2, "EventsDashboardFragment"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2682884
    iget-object v2, v0, LX/JKh;->a:LX/JK6;

    const v11, 0x6001f

    const/4 v10, 0x2

    const v9, 0x60018

    .line 2682885
    iget-object v5, v2, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v6, 0x0

    invoke-interface {v5, v11, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(II)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    .line 2682886
    iget-object v5, v2, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v5, v11, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2682887
    iget-object v5, v2, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v6, "bridgeStartupType"

    const-string v7, "cold"

    invoke-interface {v5, v9, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2682888
    :goto_0
    iget-object v5, v2, LX/JK6;->a:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    const v7, 0x6001c

    const-string v8, "nativeTimeAfterJS"

    invoke-static {v2, v5, v6, v7, v8}, LX/JK6;->a(LX/JK6;JILjava/lang/String;)V

    .line 2682889
    iget-object v5, v2, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v5, v9, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2682890
    :cond_0
    :goto_1
    return-void

    .line 2682891
    :cond_1
    const-string v2, "2097168"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2682892
    iget-object v2, v0, LX/JKh;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x200010

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_1

    .line 2682893
    :cond_2
    iget-object v5, v2, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v6, "bridgeStartupType"

    const-string v7, "warm"

    invoke-interface {v5, v9, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
