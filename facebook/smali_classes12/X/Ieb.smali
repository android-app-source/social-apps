.class public LX/Ieb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2598850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;LX/IeW;LX/3NE;LX/3NH;LX/3NI;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;)LX/3Mi;
    .locals 6
    .param p6    # LX/3NE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/3NH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/3NI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2598851
    new-instance v0, LX/3NS;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2598852
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2598853
    if-eqz p8, :cond_0

    .line 2598854
    new-instance v2, LX/3NU;

    invoke-direct {v2, p8, v5, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598855
    :cond_0
    new-instance v2, LX/3NU;

    invoke-direct {v2, p4, v5, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598856
    new-instance v2, LX/3NU;

    invoke-direct {v2, p5, v5, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598857
    if-eqz p7, :cond_1

    .line 2598858
    new-instance v2, LX/3NU;

    const v3, 0x7f0802d8

    invoke-virtual {p3, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p7, v3, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598859
    :cond_1
    if-eqz p6, :cond_2

    .line 2598860
    new-instance v2, LX/3NU;

    invoke-direct {v2, p6, v5, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598861
    :cond_2
    const/4 v2, 0x0

    .line 2598862
    iput-boolean v2, p9, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->j:Z

    .line 2598863
    new-instance v2, LX/3NU;

    const v3, 0x7f0802d3

    invoke-virtual {p3, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p9, v3, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598864
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2598865
    invoke-direct {v0, v1, p0, p1, p2}, LX/3NS;-><init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V

    return-object v0
.end method
