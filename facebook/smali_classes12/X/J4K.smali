.class public final enum LX/J4K;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J4K;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J4K;

.field public static final enum ALIGNMENT_ROADBLOCK_STEP:LX/J4K;

.field public static final enum APPS_STEP:LX/J4K;

.field public static final enum COMPOSER_STEP:LX/J4K;

.field public static final enum GENERIC_STEP:LX/J4K;

.field public static final enum PROFILE_STEP:LX/J4K;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2643924
    new-instance v0, LX/J4K;

    const-string v1, "COMPOSER_STEP"

    invoke-direct {v0, v1, v2}, LX/J4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J4K;->COMPOSER_STEP:LX/J4K;

    .line 2643925
    new-instance v0, LX/J4K;

    const-string v1, "PROFILE_STEP"

    invoke-direct {v0, v1, v3}, LX/J4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J4K;->PROFILE_STEP:LX/J4K;

    .line 2643926
    new-instance v0, LX/J4K;

    const-string v1, "APPS_STEP"

    invoke-direct {v0, v1, v4}, LX/J4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J4K;->APPS_STEP:LX/J4K;

    .line 2643927
    new-instance v0, LX/J4K;

    const-string v1, "GENERIC_STEP"

    invoke-direct {v0, v1, v5}, LX/J4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J4K;->GENERIC_STEP:LX/J4K;

    .line 2643928
    new-instance v0, LX/J4K;

    const-string v1, "ALIGNMENT_ROADBLOCK_STEP"

    invoke-direct {v0, v1, v6}, LX/J4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J4K;->ALIGNMENT_ROADBLOCK_STEP:LX/J4K;

    .line 2643929
    const/4 v0, 0x5

    new-array v0, v0, [LX/J4K;

    sget-object v1, LX/J4K;->COMPOSER_STEP:LX/J4K;

    aput-object v1, v0, v2

    sget-object v1, LX/J4K;->PROFILE_STEP:LX/J4K;

    aput-object v1, v0, v3

    sget-object v1, LX/J4K;->APPS_STEP:LX/J4K;

    aput-object v1, v0, v4

    sget-object v1, LX/J4K;->GENERIC_STEP:LX/J4K;

    aput-object v1, v0, v5

    sget-object v1, LX/J4K;->ALIGNMENT_ROADBLOCK_STEP:LX/J4K;

    aput-object v1, v0, v6

    sput-object v0, LX/J4K;->$VALUES:[LX/J4K;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2643930
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J4K;
    .locals 1

    .prologue
    .line 2643931
    const-class v0, LX/J4K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J4K;

    return-object v0
.end method

.method public static values()[LX/J4K;
    .locals 1

    .prologue
    .line 2643932
    sget-object v0, LX/J4K;->$VALUES:[LX/J4K;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J4K;

    return-object v0
.end method
