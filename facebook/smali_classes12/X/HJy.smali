.class public final LX/HJy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HJz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public b:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/HJz;


# direct methods
.method public constructor <init>(LX/HJz;)V
    .locals 1

    .prologue
    .line 2453533
    iput-object p1, p0, LX/HJy;->c:LX/HJz;

    .line 2453534
    move-object v0, p1

    .line 2453535
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2453536
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2453551
    const-string v0, "PageOpenHoursUnitComponentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2453537
    if-ne p0, p1, :cond_1

    .line 2453538
    :cond_0
    :goto_0
    return v0

    .line 2453539
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2453540
    goto :goto_0

    .line 2453541
    :cond_3
    check-cast p1, LX/HJy;

    .line 2453542
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2453543
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2453544
    if-eq v2, v3, :cond_0

    .line 2453545
    iget-object v2, p0, LX/HJy;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HJy;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HJy;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2453546
    goto :goto_0

    .line 2453547
    :cond_5
    iget-object v2, p1, LX/HJy;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_4

    .line 2453548
    :cond_6
    iget-object v2, p0, LX/HJy;->b:LX/2km;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/HJy;->b:LX/2km;

    iget-object v3, p1, LX/HJy;->b:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2453549
    goto :goto_0

    .line 2453550
    :cond_7
    iget-object v2, p1, LX/HJy;->b:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
