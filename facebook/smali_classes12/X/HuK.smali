.class public final LX/HuK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2517356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/5CI;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 8

    .prologue
    .line 2517357
    if-nez p0, :cond_0

    .line 2517358
    const/4 v0, 0x0

    .line 2517359
    :goto_0
    return-object v0

    .line 2517360
    :cond_0
    new-instance v0, LX/4Y6;

    invoke-direct {v0}, LX/4Y6;-><init>()V

    .line 2517361
    invoke-interface {p0}, LX/5CH;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 2517362
    iput-object v1, v0, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2517363
    invoke-interface {p0}, LX/5CH;->c()LX/0Px;

    move-result-object v1

    .line 2517364
    iput-object v1, v0, LX/4Y6;->f:LX/0Px;

    .line 2517365
    invoke-interface {p0}, LX/5CH;->d()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v1

    .line 2517366
    if-nez v1, :cond_1

    .line 2517367
    const/4 v2, 0x0

    .line 2517368
    :goto_1
    move-object v1, v2

    .line 2517369
    iput-object v1, v0, LX/4Y6;->h:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2517370
    invoke-interface {p0}, LX/5CH;->e()Ljava/lang/String;

    move-result-object v1

    .line 2517371
    iput-object v1, v0, LX/4Y6;->n:Ljava/lang/String;

    .line 2517372
    invoke-interface {p0}, LX/5CH;->ae_()Z

    move-result v1

    .line 2517373
    iput-boolean v1, v0, LX/4Y6;->o:Z

    .line 2517374
    invoke-interface {p0}, LX/5CH;->af_()LX/1k1;

    move-result-object v1

    .line 2517375
    if-nez v1, :cond_2

    .line 2517376
    const/4 v3, 0x0

    .line 2517377
    :goto_2
    move-object v1, v3

    .line 2517378
    iput-object v1, v0, LX/4Y6;->p:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 2517379
    invoke-interface {p0}, LX/5CH;->j()Ljava/lang/String;

    move-result-object v1

    .line 2517380
    iput-object v1, v0, LX/4Y6;->r:Ljava/lang/String;

    .line 2517381
    invoke-interface {p0}, LX/5CI;->r()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2517382
    if-nez v1, :cond_3

    .line 2517383
    const/4 v3, 0x0

    .line 2517384
    :goto_3
    move-object v1, v3

    .line 2517385
    iput-object v1, v0, LX/4Y6;->s:Lcom/facebook/graphql/model/GraphQLRating;

    .line 2517386
    invoke-interface {p0}, LX/5CI;->s()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2517387
    if-nez v1, :cond_4

    .line 2517388
    const/4 v3, 0x0

    .line 2517389
    :goto_4
    move-object v1, v3

    .line 2517390
    iput-object v1, v0, LX/4Y6;->u:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 2517391
    invoke-interface {p0}, LX/5CH;->k()LX/1Fb;

    move-result-object v1

    .line 2517392
    if-nez v1, :cond_5

    .line 2517393
    const/4 v2, 0x0

    .line 2517394
    :goto_5
    move-object v1, v2

    .line 2517395
    iput-object v1, v0, LX/4Y6;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2517396
    invoke-interface {p0}, LX/5CH;->l()Z

    move-result v1

    .line 2517397
    iput-boolean v1, v0, LX/4Y6;->G:Z

    .line 2517398
    invoke-interface {p0}, LX/5CH;->m()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v1

    .line 2517399
    if-nez v1, :cond_6

    .line 2517400
    const/4 v2, 0x0

    .line 2517401
    :goto_6
    move-object v1, v2

    .line 2517402
    iput-object v1, v0, LX/4Y6;->K:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 2517403
    invoke-interface {p0}, LX/5CH;->n()Z

    move-result v1

    .line 2517404
    iput-boolean v1, v0, LX/4Y6;->M:Z

    .line 2517405
    invoke-interface {p0}, LX/5CH;->o()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v1

    .line 2517406
    iput-object v1, v0, LX/4Y6;->O:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 2517407
    invoke-interface {p0}, LX/5CH;->p()Ljava/lang/String;

    move-result-object v1

    .line 2517408
    iput-object v1, v0, LX/4Y6;->P:Ljava/lang/String;

    .line 2517409
    invoke-interface {p0}, LX/5CH;->q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    .line 2517410
    iput-object v1, v0, LX/4Y6;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2517411
    invoke-interface {p0}, LX/5CI;->t()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2517412
    if-nez v1, :cond_b

    .line 2517413
    const/4 v3, 0x0

    .line 2517414
    :goto_7
    move-object v1, v3

    .line 2517415
    iput-object v1, v0, LX/4Y6;->S:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    .line 2517416
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto/16 :goto_0

    .line 2517417
    :cond_1
    new-instance v2, LX/4XY;

    invoke-direct {v2}, LX/4XY;-><init>()V

    .line 2517418
    invoke-virtual {v1}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2517419
    iput-object v3, v2, LX/4XY;->P:Ljava/lang/String;

    .line 2517420
    invoke-virtual {v2}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    goto/16 :goto_1

    .line 2517421
    :cond_2
    new-instance v3, LX/4X8;

    invoke-direct {v3}, LX/4X8;-><init>()V

    .line 2517422
    invoke-interface {v1}, LX/1k1;->a()D

    move-result-wide v5

    .line 2517423
    iput-wide v5, v3, LX/4X8;->b:D

    .line 2517424
    invoke-interface {v1}, LX/1k1;->b()D

    move-result-wide v5

    .line 2517425
    iput-wide v5, v3, LX/4X8;->c:D

    .line 2517426
    invoke-virtual {v3}, LX/4X8;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v3

    goto/16 :goto_2

    .line 2517427
    :cond_3
    new-instance v3, LX/4YW;

    invoke-direct {v3}, LX/4YW;-><init>()V

    .line 2517428
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->j(II)I

    move-result v4

    .line 2517429
    iput v4, v3, LX/4YW;->c:I

    .line 2517430
    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, LX/15i;->l(II)D

    move-result-wide v5

    .line 2517431
    iput-wide v5, v3, LX/4YW;->d:D

    .line 2517432
    invoke-virtual {v3}, LX/4YW;->a()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v3

    goto/16 :goto_3

    .line 2517433
    :cond_4
    new-instance v3, LX/4Xc;

    invoke-direct {v3}, LX/4Xc;-><init>()V

    .line 2517434
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->j(II)I

    move-result v4

    .line 2517435
    iput v4, v3, LX/4Xc;->b:I

    .line 2517436
    invoke-virtual {v3}, LX/4Xc;->a()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v3

    goto/16 :goto_4

    .line 2517437
    :cond_5
    new-instance v2, LX/2dc;

    invoke-direct {v2}, LX/2dc;-><init>()V

    .line 2517438
    invoke-interface {v1}, LX/1Fb;->a()I

    move-result v3

    .line 2517439
    iput v3, v2, LX/2dc;->c:I

    .line 2517440
    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    .line 2517441
    iput-object v3, v2, LX/2dc;->h:Ljava/lang/String;

    .line 2517442
    invoke-interface {v1}, LX/1Fb;->c()I

    move-result v3

    .line 2517443
    iput v3, v2, LX/2dc;->i:I

    .line 2517444
    invoke-virtual {v2}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    goto/16 :goto_5

    .line 2517445
    :cond_6
    new-instance v2, LX/4Z9;

    invoke-direct {v2}, LX/4Z9;-><init>()V

    .line 2517446
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->e()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddItemActionInfoModel;

    move-result-object v3

    .line 2517447
    if-nez v3, :cond_7

    .line 2517448
    const/4 v4, 0x0

    .line 2517449
    :goto_8
    move-object v3, v4

    .line 2517450
    iput-object v3, v2, LX/4Z9;->b:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 2517451
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->A_()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddedItemStateInfoModel;

    move-result-object v3

    .line 2517452
    if-nez v3, :cond_8

    .line 2517453
    const/4 v4, 0x0

    .line 2517454
    :goto_9
    move-object v3, v4

    .line 2517455
    iput-object v3, v2, LX/4Z9;->c:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 2517456
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2517457
    iput-object v3, v2, LX/4Z9;->h:Ljava/lang/String;

    .line 2517458
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 2517459
    iput-object v3, v2, LX/4Z9;->j:Ljava/lang/String;

    .line 2517460
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->d()LX/1oQ;

    move-result-object v3

    .line 2517461
    if-nez v3, :cond_9

    .line 2517462
    const/4 v4, 0x0

    .line 2517463
    :goto_a
    move-object v3, v4

    .line 2517464
    iput-object v3, v2, LX/4Z9;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2517465
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->z_()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$SavedDashboardSectionModel;

    move-result-object v3

    .line 2517466
    if-nez v3, :cond_a

    .line 2517467
    const/4 v4, 0x0

    .line 2517468
    :goto_b
    move-object v3, v4

    .line 2517469
    iput-object v3, v2, LX/4Z9;->n:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    .line 2517470
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2517471
    iput-object v3, v2, LX/4Z9;->r:Ljava/lang/String;

    .line 2517472
    invoke-virtual {v2}, LX/4Z9;->a()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    goto/16 :goto_6

    .line 2517473
    :cond_7
    new-instance v4, LX/4ZB;

    invoke-direct {v4}, LX/4ZB;-><init>()V

    .line 2517474
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddItemActionInfoModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2517475
    iput-object v5, v4, LX/4ZB;->b:Ljava/lang/String;

    .line 2517476
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddItemActionInfoModel;->b()LX/176;

    move-result-object v5

    invoke-static {v5}, LX/HuK;->a(LX/176;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 2517477
    iput-object v5, v4, LX/4ZB;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2517478
    invoke-virtual {v4}, LX/4ZB;->a()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v4

    goto :goto_8

    .line 2517479
    :cond_8
    new-instance v4, LX/4ZB;

    invoke-direct {v4}, LX/4ZB;-><init>()V

    .line 2517480
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddedItemStateInfoModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2517481
    iput-object v5, v4, LX/4ZB;->b:Ljava/lang/String;

    .line 2517482
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddedItemStateInfoModel;->b()LX/176;

    move-result-object v5

    invoke-static {v5}, LX/HuK;->a(LX/176;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 2517483
    iput-object v5, v4, LX/4ZB;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2517484
    invoke-virtual {v4}, LX/4ZB;->a()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v4

    goto :goto_9

    .line 2517485
    :cond_9
    new-instance v4, LX/4YH;

    invoke-direct {v4}, LX/4YH;-><init>()V

    .line 2517486
    invoke-interface {v3}, LX/1oQ;->c()Ljava/lang/String;

    move-result-object v5

    .line 2517487
    iput-object v5, v4, LX/4YH;->h:Ljava/lang/String;

    .line 2517488
    invoke-interface {v3}, LX/1oQ;->m()Ljava/lang/String;

    move-result-object v5

    .line 2517489
    iput-object v5, v4, LX/4YH;->l:Ljava/lang/String;

    .line 2517490
    invoke-virtual {v4}, LX/4YH;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v4

    goto :goto_a

    .line 2517491
    :cond_a
    new-instance v4, LX/4Yi;

    invoke-direct {v4}, LX/4Yi;-><init>()V

    .line 2517492
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$SavedDashboardSectionModel;->a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v5

    .line 2517493
    iput-object v5, v4, LX/4Yi;->b:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 2517494
    invoke-virtual {v4}, LX/4Yi;->a()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v4

    goto :goto_b

    .line 2517495
    :cond_b
    new-instance v3, LX/4ZU;

    invoke-direct {v3}, LX/4ZU;-><init>()V

    .line 2517496
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->j(II)I

    move-result v4

    .line 2517497
    iput v4, v3, LX/4ZU;->b:I

    .line 2517498
    invoke-virtual {v3}, LX/4ZU;->a()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v3

    goto/16 :goto_7
.end method

.method public static a(LX/176;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2517499
    if-nez p0, :cond_0

    .line 2517500
    const/4 v0, 0x0

    .line 2517501
    :goto_0
    return-object v0

    .line 2517502
    :cond_0
    new-instance v3, LX/173;

    invoke-direct {v3}, LX/173;-><init>()V

    .line 2517503
    invoke-interface {p0}, LX/176;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2517504
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 2517505
    :goto_1
    invoke-interface {p0}, LX/176;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2517506
    invoke-interface {p0}, LX/176;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jz;

    .line 2517507
    if-nez v0, :cond_5

    .line 2517508
    const/4 v5, 0x0

    .line 2517509
    :goto_2
    move-object v0, v5

    .line 2517510
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2517511
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2517512
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2517513
    iput-object v0, v3, LX/173;->b:LX/0Px;

    .line 2517514
    :cond_2
    invoke-interface {p0}, LX/176;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2517515
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2517516
    :goto_3
    invoke-interface {p0}, LX/176;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2517517
    invoke-interface {p0}, LX/176;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 2517518
    if-nez v0, :cond_6

    .line 2517519
    const/4 v4, 0x0

    .line 2517520
    :goto_4
    move-object v0, v4

    .line 2517521
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2517522
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2517523
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2517524
    iput-object v0, v3, LX/173;->e:LX/0Px;

    .line 2517525
    :cond_4
    invoke-interface {p0}, LX/176;->a()Ljava/lang/String;

    move-result-object v0

    .line 2517526
    iput-object v0, v3, LX/173;->f:Ljava/lang/String;

    .line 2517527
    invoke-virtual {v3}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 2517528
    :cond_5
    new-instance v5, LX/4Vo;

    invoke-direct {v5}, LX/4Vo;-><init>()V

    .line 2517529
    invoke-interface {v0}, LX/1jz;->a()I

    move-result v6

    .line 2517530
    iput v6, v5, LX/4Vo;->b:I

    .line 2517531
    invoke-interface {v0}, LX/1jz;->b()I

    move-result v6

    .line 2517532
    iput v6, v5, LX/4Vo;->c:I

    .line 2517533
    invoke-interface {v0}, LX/1jz;->c()I

    move-result v6

    .line 2517534
    iput v6, v5, LX/4Vo;->d:I

    .line 2517535
    invoke-virtual {v5}, LX/4Vo;->a()Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    move-result-object v5

    goto :goto_2

    .line 2517536
    :cond_6
    new-instance v4, LX/4W6;

    invoke-direct {v4}, LX/4W6;-><init>()V

    .line 2517537
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v5

    .line 2517538
    if-nez v5, :cond_7

    .line 2517539
    const/4 v6, 0x0

    .line 2517540
    :goto_5
    move-object v5, v6

    .line 2517541
    iput-object v5, v4, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 2517542
    invoke-interface {v0}, LX/1W5;->b()I

    move-result v5

    .line 2517543
    iput v5, v4, LX/4W6;->c:I

    .line 2517544
    invoke-interface {v0}, LX/1W5;->c()I

    move-result v5

    .line 2517545
    iput v5, v4, LX/4W6;->d:I

    .line 2517546
    invoke-virtual {v4}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v4

    goto :goto_4

    .line 2517547
    :cond_7
    new-instance v6, LX/170;

    invoke-direct {v6}, LX/170;-><init>()V

    .line 2517548
    invoke-interface {v5}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 2517549
    iput-object v7, v6, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2517550
    invoke-interface {v5}, LX/171;->c()LX/0Px;

    move-result-object v7

    .line 2517551
    iput-object v7, v6, LX/170;->b:LX/0Px;

    .line 2517552
    invoke-interface {v5}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v7

    .line 2517553
    iput-object v7, v6, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 2517554
    invoke-interface {v5}, LX/171;->e()Ljava/lang/String;

    move-result-object v7

    .line 2517555
    iput-object v7, v6, LX/170;->o:Ljava/lang/String;

    .line 2517556
    invoke-interface {v5}, LX/171;->v_()Ljava/lang/String;

    move-result-object v7

    .line 2517557
    iput-object v7, v6, LX/170;->A:Ljava/lang/String;

    .line 2517558
    invoke-interface {v5}, LX/171;->w_()Ljava/lang/String;

    move-result-object v7

    .line 2517559
    iput-object v7, v6, LX/170;->X:Ljava/lang/String;

    .line 2517560
    invoke-interface {v5}, LX/171;->j()Ljava/lang/String;

    move-result-object v7

    .line 2517561
    iput-object v7, v6, LX/170;->Y:Ljava/lang/String;

    .line 2517562
    invoke-virtual {v6}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    goto :goto_5
.end method
