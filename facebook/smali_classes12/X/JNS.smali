.class public LX/JNS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/6RZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2685938
    invoke-static {v0, v0}, LX/1mh;->a(II)I

    move-result v0

    sput v0, LX/JNS;->a:I

    return-void
.end method

.method public constructor <init>(LX/6RZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2685936
    iput-object p1, p0, LX/JNS;->b:LX/6RZ;

    .line 2685937
    return-void
.end method

.method public static a(LX/0QB;)LX/JNS;
    .locals 4

    .prologue
    .line 2685924
    const-class v1, LX/JNS;

    monitor-enter v1

    .line 2685925
    :try_start_0
    sget-object v0, LX/JNS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685926
    sput-object v2, LX/JNS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685927
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685928
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685929
    new-instance p0, LX/JNS;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v3

    check-cast v3, LX/6RZ;

    invoke-direct {p0, v3}, LX/JNS;-><init>(LX/6RZ;)V

    .line 2685930
    move-object v0, p0

    .line 2685931
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685932
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685933
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685934
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
