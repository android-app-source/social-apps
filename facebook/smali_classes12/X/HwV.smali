.class public final LX/HwV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/HwW;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public e:I

.field public f:Landroid/support/v4/app/Fragment;

.field public g:Lcom/facebook/photos/base/media/PhotoItem;

.field public h:Lcom/facebook/photos/base/tagging/FaceBox;

.field public i:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>(LX/HwW;)V
    .locals 0

    .prologue
    .line 2520987
    iput-object p1, p0, LX/HwV;->a:LX/HwW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520988
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2520989
    new-instance v0, LX/BIh;

    invoke-direct {v0}, LX/BIh;-><init>()V

    iget-object v1, p0, LX/HwV;->f:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2520990
    iput-object v1, v0, LX/BIh;->a:Landroid/content/Context;

    .line 2520991
    move-object v0, v0

    .line 2520992
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/HwV;->b:LX/0Px;

    invoke-static {v2}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2520993
    iput-object v1, v0, LX/BIh;->b:Ljava/util/ArrayList;

    .line 2520994
    move-object v0, v0

    .line 2520995
    iget-object v1, p0, LX/HwV;->b:LX/0Px;

    invoke-static {v1}, LX/7kq;->c(LX/0Px;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2520996
    iput-object v1, v0, LX/BIh;->c:Ljava/util/ArrayList;

    .line 2520997
    move-object v0, v0

    .line 2520998
    iget-object v1, p0, LX/HwV;->g:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    .line 2520999
    iput-object v1, v0, LX/BIh;->d:Lcom/facebook/ipc/media/MediaIdKey;

    .line 2521000
    move-object v0, v0

    .line 2521001
    iget-object v1, p0, LX/HwV;->c:Ljava/lang/String;

    .line 2521002
    iput-object v1, v0, LX/BIh;->e:Ljava/lang/String;

    .line 2521003
    move-object v0, v0

    .line 2521004
    iget-object v1, p0, LX/HwV;->d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 2521005
    iput-object v1, v0, LX/BIh;->f:LX/2rw;

    .line 2521006
    move-object v0, v0

    .line 2521007
    iget-object v1, p0, LX/HwV;->d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 2521008
    iput-wide v2, v0, LX/BIh;->g:J

    .line 2521009
    move-object v0, v0

    .line 2521010
    iget-object v1, p0, LX/HwV;->h:Lcom/facebook/photos/base/tagging/FaceBox;

    .line 2521011
    iput-object v1, v0, LX/BIh;->h:Lcom/facebook/photos/base/tagging/FaceBox;

    .line 2521012
    move-object v0, v0

    .line 2521013
    iget-object v1, p0, LX/HwV;->i:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2521014
    iput-object v1, v0, LX/BIh;->i:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2521015
    move-object v0, v0

    .line 2521016
    iget-boolean v1, p0, LX/HwV;->j:Z

    .line 2521017
    iput-boolean v1, v0, LX/BIh;->j:Z

    .line 2521018
    move-object v1, v0

    .line 2521019
    iget-boolean v0, p0, LX/HwV;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2521020
    :goto_0
    iput-boolean v0, v1, LX/BIh;->k:Z

    .line 2521021
    move-object v0, v1

    .line 2521022
    iget-boolean v1, p0, LX/HwV;->k:Z

    .line 2521023
    iput-boolean v1, v0, LX/BIh;->l:Z

    .line 2521024
    move-object v0, v0

    .line 2521025
    new-instance v5, Landroid/content/Intent;

    iget-object v4, v0, LX/BIh;->a:Landroid/content/Context;

    const-class v6, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;

    invoke-direct {v5, v4, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "extra_taggable_gallery_photo_list"

    iget-object v4, v0, LX/BIh;->b:Ljava/util/ArrayList;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "extras_taggable_gallery_creative_editing_data_list"

    iget-object v4, v0, LX/BIh;->c:Ljava/util/ArrayList;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "extra_taggable_gallery_photo_item_id"

    iget-object v4, v0, LX/BIh;->d:Lcom/facebook/ipc/media/MediaIdKey;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Parcelable;

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "extra_session_id"

    iget-object v4, v0, LX/BIh;->e:Ljava/lang/String;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "extra_media_container_type"

    iget-object v4, v0, LX/BIh;->f:LX/2rw;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2rw;

    invoke-static {v4}, LX/2rw;->convertToObjectType(LX/2rw;)I

    move-result v4

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "extra_media_container_id"

    iget-wide v6, v0, LX/BIh;->g:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "extra_taggable_gallery_goto_facebox"

    iget-object v6, v0, LX/BIh;->h:Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "tag_typeahead_data_source_metadata"

    iget-object v6, v0, LX/BIh;->i:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "show_tag_expansion_information"

    iget-boolean v6, v0, LX/BIh;->j:Z

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "extra_is_friend_tagging_enabled"

    iget-boolean v6, v0, LX/BIh;->k:Z

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "extra_is_product_tagging_enabled"

    iget-boolean v6, v0, LX/BIh;->l:Z

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    move-object v0, v4

    .line 2521026
    iget-object v1, p0, LX/HwV;->a:LX/HwW;

    iget-object v1, v1, LX/HwW;->a:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_PHOTO_THUMBNAIL_CLICKED:LX/0ge;

    iget-object v3, p0, LX/HwV;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2521027
    iget-object v1, p0, LX/HwV;->a:LX/HwW;

    iget-object v1, v1, LX/HwW;->b:Lcom/facebook/content/SecureContextHelper;

    iget v2, p0, LX/HwV;->e:I

    iget-object v3, p0, LX/HwV;->f:Landroid/support/v4/app/Fragment;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2521028
    return-void

    .line 2521029
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
