.class public final LX/HNj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2458845
    iput-object p1, p0, LX/HNj;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2458854
    iget-object v0, p0, LX/HNj;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    invoke-static {v0, p1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Ljava/lang/Throwable;)V

    .line 2458855
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2458846
    check-cast p1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel;

    .line 2458847
    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel;->j()LX/0Px;

    move-result-object v0

    .line 2458848
    if-eqz p1, :cond_0

    .line 2458849
    iget-object v1, p0, LX/HNj;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    invoke-static {v0}, LX/CYR;->a(LX/0Px;)Z

    move-result v0

    .line 2458850
    iput-boolean v0, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->i:Z

    .line 2458851
    iget-object v0, p0, LX/HNj;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->b(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)V

    .line 2458852
    :goto_0
    return-void

    .line 2458853
    :cond_0
    iget-object v0, p0, LX/HNj;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "View permission for page cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
