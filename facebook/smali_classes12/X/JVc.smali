.class public LX/JVc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JVc;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2701241
    return-void
.end method

.method public static a(LX/0QB;)LX/JVc;
    .locals 3

    .prologue
    .line 2701242
    sget-object v0, LX/JVc;->a:LX/JVc;

    if-nez v0, :cond_1

    .line 2701243
    const-class v1, LX/JVc;

    monitor-enter v1

    .line 2701244
    :try_start_0
    sget-object v0, LX/JVc;->a:LX/JVc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2701245
    if-eqz v2, :cond_0

    .line 2701246
    :try_start_1
    new-instance v0, LX/JVc;

    invoke-direct {v0}, LX/JVc;-><init>()V

    .line 2701247
    move-object v0, v0

    .line 2701248
    sput-object v0, LX/JVc;->a:LX/JVc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701249
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2701250
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2701251
    :cond_1
    sget-object v0, LX/JVc;->a:LX/JVc;

    return-object v0

    .line 2701252
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2701253
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 3

    .prologue
    .line 2701254
    const-class v0, LX/JVe;

    sget-object v1, LX/0ja;->a:LX/3AL;

    sget-object v2, LX/0ja;->e:LX/3AM;

    invoke-virtual {p1, v0, v1, v2}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    .line 2701255
    return-void
.end method
