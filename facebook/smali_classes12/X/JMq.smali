.class public LX/JMq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JMr;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JMq",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JMr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2684804
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2684805
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JMq;->b:LX/0Zi;

    .line 2684806
    iput-object p1, p0, LX/JMq;->a:LX/0Ot;

    .line 2684807
    return-void
.end method

.method public static a(LX/0QB;)LX/JMq;
    .locals 4

    .prologue
    .line 2684793
    const-class v1, LX/JMq;

    monitor-enter v1

    .line 2684794
    :try_start_0
    sget-object v0, LX/JMq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2684795
    sput-object v2, LX/JMq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2684796
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2684797
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2684798
    new-instance v3, LX/JMq;

    const/16 p0, 0x1ea0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JMq;-><init>(LX/0Ot;)V

    .line 2684799
    move-object v0, v3

    .line 2684800
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2684801
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JMq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2684802
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2684803
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2684775
    check-cast p2, LX/JMo;

    .line 2684776
    iget-object v0, p0, LX/JMq;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JMr;

    iget-object v1, p2, LX/JMo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x6

    const/4 p0, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 2684777
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    iget-object v5, v0, LX/JMr;->a:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    const v6, 0x7f0214f6

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    const v6, 0x7f0a00e6

    invoke-virtual {v5, v6}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0917

    invoke-interface {v4, v8, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/16 v5, 0xa

    invoke-interface {v4, v7, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    .line 2684778
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2684779
    check-cast v2, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    const v5, 0x7f0b0050

    invoke-virtual {v2, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0a00e6

    invoke-virtual {v2, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v2, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p0, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    const/4 v5, 0x3

    invoke-interface {v2, v5, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f020aea

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/16 v5, 0xd

    invoke-interface {v4, v5}, LX/1Di;->j(I)LX/1Di;

    move-result-object v4

    const/16 v5, 0xe

    invoke-interface {v4, v5}, LX/1Di;->r(I)LX/1Di;

    move-result-object v4

    const/16 v5, 0x8

    const/16 v6, 0xc

    invoke-interface {v4, v5, v6}, LX/1Di;->n(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b00ea

    invoke-interface {v4, v7, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b00eb

    invoke-interface {v4, v8, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    .line 2684780
    const v5, 0x4d14c4fa    # 1.55996064E8f

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2684781
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2684782
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2684783
    invoke-static {}, LX/1dS;->b()V

    .line 2684784
    iget v0, p1, LX/1dQ;->b:I

    .line 2684785
    packed-switch v0, :pswitch_data_0

    .line 2684786
    :goto_0
    return-object v2

    .line 2684787
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2684788
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2684789
    check-cast v1, LX/JMo;

    .line 2684790
    iget-object p1, p0, LX/JMq;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/JMo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/JMo;->b:LX/1Pn;

    .line 2684791
    check-cast p2, LX/1Pk;

    invoke-interface {p2}, LX/1Pk;->e()LX/1SX;

    move-result-object p0

    invoke-virtual {p0, p1, v0}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2684792
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4d14c4fa
        :pswitch_0
    .end packed-switch
.end method
