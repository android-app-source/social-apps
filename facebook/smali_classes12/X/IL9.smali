.class public LX/IL9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2567681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2567682
    return-void
.end method

.method public static b(LX/0QB;)LX/IL9;
    .locals 2

    .prologue
    .line 2567683
    new-instance v1, LX/IL9;

    invoke-direct {v1}, LX/IL9;-><init>()V

    .line 2567684
    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2567685
    iput-object v0, v1, LX/IL9;->a:LX/0Zb;

    .line 2567686
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 2567687
    iget-object v0, p0, LX/IL9;->a:LX/0Zb;

    const-string v1, "community_fetch_forum_groups_failure"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2567688
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2567689
    const-string v1, "community_onboarding_nux"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "community_id"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    sget-object v1, LX/IL8;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    sget-object v1, LX/IL8;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2567690
    :cond_0
    return-void
.end method
