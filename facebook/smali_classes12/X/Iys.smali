.class public abstract LX/Iys;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VIEW_CONFIGURATION::",
        "Lcom/facebook/payments/p2p/P2pFlowViewConfiguration;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/Iyu;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/P2pPaymentExtension;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TVIEW_CONFIGURATION;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Iyu;LX/0Px;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Iyu;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/P2pPaymentExtension;",
            ">;",
            "LX/0Ot",
            "<TVIEW_CONFIGURATION;>;)V"
        }
    .end annotation

    .prologue
    .line 2633986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633987
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iyu;

    iput-object v0, p0, LX/Iys;->a:LX/Iyu;

    .line 2633988
    iput-object p2, p0, LX/Iys;->b:LX/0Px;

    .line 2633989
    iput-object p3, p0, LX/Iys;->c:LX/0Ot;

    .line 2633990
    return-void
.end method
