.class public final LX/IsE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/graphics/Typeface;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IsJ;


# direct methods
.method public constructor <init>(LX/IsJ;)V
    .locals 0

    .prologue
    .line 2619392
    iput-object p1, p0, LX/IsE;->a:LX/IsJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2619393
    const-string v0, "TextLayerPresenter"

    const-string v1, "Failed to fetch custom font."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2619394
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2619395
    check-cast p1, Landroid/graphics/Typeface;

    .line 2619396
    iget-object v0, p0, LX/IsE;->a:LX/IsJ;

    .line 2619397
    iget-boolean v1, v0, LX/Iqk;->k:Z

    move v0, v1

    .line 2619398
    if-nez v0, :cond_0

    .line 2619399
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2619400
    iget-object v0, p0, LX/IsE;->a:LX/IsJ;

    iget-object v0, v0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2619401
    iget-object v0, p0, LX/IsE;->a:LX/IsJ;

    iget-object v0, v0, LX/IsJ;->j:LX/Irm;

    if-eqz v0, :cond_0

    .line 2619402
    iget-object v0, p0, LX/IsE;->a:LX/IsJ;

    iget-object v0, v0, LX/IsJ;->j:LX/Irm;

    iget-object v1, p0, LX/IsE;->a:LX/IsJ;

    iget-object v1, v1, LX/IsJ;->g:LX/IsC;

    iget-object v2, p0, LX/IsE;->a:LX/IsJ;

    iget-object v2, v2, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    const/4 p1, 0x0

    .line 2619403
    invoke-static {v1, v2}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(LX/Iqg;Landroid/view/View;)LX/Iqo;

    move-result-object v4

    move-object v3, v4

    .line 2619404
    iget-object v4, v0, LX/Irm;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {v4}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v4

    const/high16 p0, -0x80000000

    invoke-static {v4, p0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 2619405
    invoke-static {p1, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p0

    .line 2619406
    invoke-virtual {v2, v4, p0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->measure(II)V

    .line 2619407
    iget-object p0, v0, LX/Irm;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result p0

    invoke-static {v3, p0}, LX/Irf;->a(LX/Iqo;I)I

    move-result p0

    iget-object p1, v0, LX/Irm;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {p1}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result p1

    invoke-static {v3, p1}, LX/Irf;->b(LX/Iqo;I)I

    move-result v3

    .line 2619408
    invoke-static {v1, v2, p0, v3}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(LX/Iqg;Landroid/view/View;II)V

    .line 2619409
    :cond_0
    return-void
.end method
