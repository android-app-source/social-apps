.class public LX/Idw;
.super Landroid/view/ViewGroup;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2598228
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 2598229
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2598226
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2598227
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2598224
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2598225
    return-void
.end method

.method private static a(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 2598222
    add-int v0, p1, p3

    add-int v1, p2, p4

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 2598223
    return-void
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 2598221
    instance-of v0, p1, Landroid/widget/LinearLayout$LayoutParams;

    return v0
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2598220
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2598219
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, LX/Idw;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2598218
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 2598169
    invoke-virtual {p0}, LX/Idw;->getChildCount()I

    move-result v0

    .line 2598170
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2598171
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should be two children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2598172
    :cond_0
    invoke-virtual {p0}, LX/Idw;->getPaddingTop()I

    move-result v2

    .line 2598173
    invoke-virtual {p0}, LX/Idw;->getPaddingBottom()I

    move-result v0

    .line 2598174
    invoke-virtual {p0}, LX/Idw;->getHeight()I

    move-result v1

    .line 2598175
    invoke-virtual {p0}, LX/Idw;->getPaddingLeft()I

    move-result v3

    .line 2598176
    sub-int v4, v1, v2

    sub-int v5, v4, v0

    .line 2598177
    sub-int v6, v1, v0

    .line 2598178
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    const/4 v0, 0x2

    if-ge v4, v0, :cond_1

    .line 2598179
    invoke-virtual {p0, v4}, LX/Idw;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 2598180
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    .line 2598181
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2598182
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 2598183
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 2598184
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    and-int/lit8 v1, v1, 0x70

    sparse-switch v1, :sswitch_data_0

    move v1, v2

    .line 2598185
    :goto_1
    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v10

    .line 2598186
    invoke-static {v7, v3, v1, v8, v9}, LX/Idw;->a(Landroid/view/View;IIII)V

    .line 2598187
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v8

    add-int/2addr v0, v3

    .line 2598188
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v3, v0

    goto :goto_0

    .line 2598189
    :sswitch_0
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    .line 2598190
    goto :goto_1

    .line 2598191
    :sswitch_1
    sub-int v1, v5, v9

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v10

    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v1, v10

    .line 2598192
    goto :goto_1

    .line 2598193
    :sswitch_2
    sub-int v1, v6, v9

    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v1, v10

    .line 2598194
    goto :goto_1

    .line 2598195
    :cond_1
    return-void

    :cond_2
    move v0, v3

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method public final onMeasure(II)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 2598196
    invoke-virtual {p0}, LX/Idw;->getChildCount()I

    move-result v0

    .line 2598197
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2598198
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should be two children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2598199
    :cond_0
    invoke-virtual {p0, v3}, LX/Idw;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 2598200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Idw;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2598201
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    .line 2598202
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 2598203
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object v0, p0

    move v4, p2

    move v5, v3

    .line 2598204
    invoke-virtual/range {v0 .. v5}, LX/Idw;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 2598205
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v2

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v7, v0, v2

    .line 2598206
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2598207
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    move-object v4, p0

    move-object v5, v10

    move v6, p1

    move v8, p2

    move v9, v3

    .line 2598208
    invoke-virtual/range {v4 .. v9}, LX/Idw;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 2598209
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2598210
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 2598211
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2598212
    invoke-virtual {p0}, LX/Idw;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, LX/Idw;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 2598213
    invoke-virtual {p0}, LX/Idw;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, LX/Idw;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2598214
    invoke-virtual {p0}, LX/Idw;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2598215
    invoke-virtual {p0}, LX/Idw;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2598216
    invoke-static {v0, p1}, LX/Idw;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, LX/Idw;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/Idw;->setMeasuredDimension(II)V

    .line 2598217
    return-void

    :cond_1
    move v7, v3

    move v1, v3

    move v2, v3

    goto :goto_0
.end method
