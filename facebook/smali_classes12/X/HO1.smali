.class public final LX/HO1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459149
    iput-object p1, p0, LX/HO1;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x77f972aa

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2459150
    iget-object v0, p0, LX/HO1;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->F:LX/HOg;

    iget-object v1, p0, LX/HO1;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    .line 2459151
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 2459152
    iget-object v2, p0, LX/HO1;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    .line 2459153
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v3

    .line 2459154
    iget-object v3, p0, LX/HO1;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v4, p0, LX/HO1;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->isShown()Z

    move-result v4

    iget-object v5, p0, LX/HO1;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    .line 2459155
    iget-object v8, v0, LX/HOg;->o:LX/CYR;

    invoke-virtual {v8, v1}, LX/CYR;->a(Landroid/view/View;)V

    .line 2459156
    iget-object v8, v0, LX/HOg;->m:LX/0if;

    sget-object v9, LX/0ig;->ak:LX/0ih;

    const-string v10, "tap_change_cta_button"

    invoke-virtual {v8, v9, v10}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2459157
    if-eqz v5, :cond_0

    .line 2459158
    iget-boolean v8, v5, LX/CYE;->mUseActionFlow:Z

    move v8, v8

    .line 2459159
    if-eqz v8, :cond_0

    iget-object v8, v0, LX/HOg;->e:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iget-object v10, v0, LX/HOg;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    iget-object v11, v0, LX/HOg;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-static {v8, v9, v5, v10, v11}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->a(JLX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    move-result-object v8

    :goto_0
    invoke-static {v2, v3, v8}, LX/CYR;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2459160
    iget-object v8, v0, LX/HOg;->h:LX/CY3;

    iget-object v9, v0, LX/HOg;->e:Ljava/lang/String;

    iget-object v10, v0, LX/HOg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v10}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v10

    invoke-static {v4, v10}, LX/CYR;->a(ZLcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/CY3;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459161
    const v0, -0xe236c03

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2459162
    :cond_0
    iget-object v8, v0, LX/HOg;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    iget-object v9, v0, LX/HOg;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    iget-object v10, v0, LX/HOg;->e:Ljava/lang/String;

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v8, v9, v10, v11, v12}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Ljava/lang/String;ZLX/CYE;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    move-result-object v8

    goto :goto_0
.end method
