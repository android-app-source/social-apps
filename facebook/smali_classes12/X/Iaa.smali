.class public final LX/Iaa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V
    .locals 0

    .prologue
    .line 2590961
    iput-object p1, p0, LX/Iaa;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x5e5a33fd

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2590962
    iget-object v1, p0, LX/Iaa;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-nez v1, :cond_0

    .line 2590963
    iget-object v1, p0, LX/Iaa;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->i:LX/IZw;

    const-string v2, "click_payment_row"

    const-string v3, "add_card"

    invoke-virtual {v1, v2, v3}, LX/IZw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590964
    iget-object v1, p0, LX/Iaa;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k:LX/Io9;

    iget-object v2, p0, LX/Iaa;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->l:LX/0Px;

    .line 2590965
    invoke-static {v1, v2}, LX/Io9;->c(LX/Io9;LX/0Px;)V

    .line 2590966
    :goto_0
    const v1, -0x70dadf09

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2590967
    :cond_0
    iget-object v1, p0, LX/Iaa;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->i:LX/IZw;

    const-string v2, "click_payment_row"

    const-string v3, "change_card"

    invoke-virtual {v1, v2, v3}, LX/IZw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590968
    iget-object v1, p0, LX/Iaa;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k:LX/Io9;

    iget-object v2, p0, LX/Iaa;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->l:LX/0Px;

    .line 2590969
    iget-object v5, v1, LX/Io9;->g:Landroid/content/Context;

    iget-object v6, v1, LX/Io9;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->c(LX/0Px;)LX/0Px;

    move-result-object v7

    invoke-static {v6, v7}, LX/Gza;->a(Landroid/content/Context;LX/0Px;)LX/0Px;

    move-result-object v6

    iget-object v7, v1, LX/Io9;->h:Landroid/content/res/Resources;

    const v8, 0x7f082c84

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, LX/Io9;->h:Landroid/content/res/Resources;

    const v9, 0x7f082c88

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-instance v10, LX/Io8;

    invoke-direct {v10, v1, v2}, LX/Io8;-><init>(LX/Io9;LX/0Px;)V

    invoke-static/range {v5 .. v10}, LX/Gza;->a(Landroid/content/Context;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GzZ;)LX/2EJ;

    move-result-object v5

    move-object v4, v5

    .line 2590970
    invoke-virtual {v4}, LX/2EJ;->show()V

    .line 2590971
    goto :goto_0
.end method
