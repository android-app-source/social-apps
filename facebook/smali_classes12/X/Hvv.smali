.class public final LX/Hvv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/5Rn;

.field public final synthetic b:Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;LX/5Rn;)V
    .locals 0

    .prologue
    .line 2520012
    iput-object p1, p0, LX/Hvv;->b:Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;

    iput-object p2, p0, LX/Hvv;->a:LX/5Rn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x35623f11

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2520013
    iget-object v1, p0, LX/Hvv;->a:LX/5Rn;

    sget-object v2, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-ne v1, v2, :cond_1

    .line 2520014
    iget-object v1, p0, LX/Hvv;->b:Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;

    iget-object v1, v1, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->r:LX/Hw8;

    .line 2520015
    iget-object v6, v1, LX/Hw8;->d:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/32 v8, 0xa1220

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    .line 2520016
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    iput-object v6, v1, LX/Hw8;->d:Ljava/util/Calendar;

    .line 2520017
    iget-object v6, v1, LX/Hw8;->d:Ljava/util/Calendar;

    const/16 v7, 0xc

    const/16 v8, 0xb

    invoke-virtual {v6, v7, v8}, Ljava/util/Calendar;->add(II)V

    .line 2520018
    iget-object v6, v1, LX/Hw8;->c:LX/Hw7;

    invoke-virtual {v6}, LX/Hw7;->b()V

    .line 2520019
    iget-object v6, v1, LX/Hw8;->c:LX/Hw7;

    invoke-virtual {v6}, LX/Hw7;->c()V

    .line 2520020
    :cond_0
    iget-object v6, v1, LX/Hw8;->c:LX/Hw7;

    invoke-virtual {v6}, LX/Hw7;->show()V

    .line 2520021
    :goto_0
    const v1, -0x4527c31a

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2520022
    :cond_1
    iget-object v1, p0, LX/Hvv;->b:Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;

    iget-object v2, p0, LX/Hvv;->a:LX/5Rn;

    const-wide/16 v4, 0x0

    .line 2520023
    invoke-static {v1, v2, v4, v5}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->a$redex0(Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;LX/5Rn;J)V

    .line 2520024
    goto :goto_0
.end method
