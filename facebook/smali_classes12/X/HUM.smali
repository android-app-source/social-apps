.class public final LX/HUM;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2472104
    const-class v1, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;

    const v0, 0x49205d85

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PoliticalIssuesQuery"

    const-string v6, "dab6354493957dc3f2435dd29409ad58"

    const-string v7, "viewer"

    const-string v8, "10155072967156729"

    const-string v9, "10155259089811729"

    .line 2472105
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2472106
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2472107
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2472108
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2472109
    sparse-switch v0, :sswitch_data_0

    .line 2472110
    :goto_0
    return-object p1

    .line 2472111
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2472112
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2472113
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2472114
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2472115
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2472116
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2472117
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2472118
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2472119
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2472120
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x69b6761e -> :sswitch_4
        -0x5590afb1 -> :sswitch_8
        -0x2fe52f35 -> :sswitch_0
        -0x25a646c8 -> :sswitch_1
        -0x2177e47b -> :sswitch_2
        0x1918b88b -> :sswitch_3
        0x2292beef -> :sswitch_7
        0x26d0c0ff -> :sswitch_6
        0x5127afb0 -> :sswitch_9
        0x73a026b5 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2472121
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2472122
    :goto_1
    return v0

    .line 2472123
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2472124
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
