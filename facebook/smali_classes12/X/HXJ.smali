.class public final LX/HXJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V
    .locals 0

    .prologue
    .line 2478659
    iput-object p1, p0, LX/HXJ;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2478660
    iget-object v0, p0, LX/HXJ;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    .line 2478661
    new-instance v3, LX/HR8;

    invoke-direct {v3}, LX/HR8;-><init>()V

    move-object v3, v3

    .line 2478662
    const-string v4, "page_id"

    iget-wide v5, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->i:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2478663
    const-string v4, "need_profile_pic"

    iget-boolean v5, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->p:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2478664
    const-string v4, "need_profile_permission"

    iget-boolean v5, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->o:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2478665
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    move-object v0, v3

    .line 2478666
    iget-object v1, p0, LX/HXJ;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-object v2, p0, LX/HXJ;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->q:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2478667
    iput-object v0, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2478668
    iget-object v0, p0, LX/HXJ;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method
