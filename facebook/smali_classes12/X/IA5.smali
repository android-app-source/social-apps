.class public final enum LX/IA5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IA5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IA5;

.field public static final enum EARLIER:LX/IA5;

.field public static final enum EMAILS:LX/IA5;

.field public static final enum FRIENDS:LX/IA5;

.field public static final enum NON_FRIENDS:LX/IA5;

.field public static final enum SELF:LX/IA5;

.field public static final enum SMS:LX/IA5;

.field public static final enum TODAY:LX/IA5;

.field public static final enum YESTERDAY:LX/IA5;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2544665
    new-instance v0, LX/IA5;

    const-string v1, "SELF"

    invoke-direct {v0, v1, v3}, LX/IA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IA5;->SELF:LX/IA5;

    .line 2544666
    new-instance v0, LX/IA5;

    const-string v1, "FRIENDS"

    invoke-direct {v0, v1, v4}, LX/IA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IA5;->FRIENDS:LX/IA5;

    .line 2544667
    new-instance v0, LX/IA5;

    const-string v1, "NON_FRIENDS"

    invoke-direct {v0, v1, v5}, LX/IA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IA5;->NON_FRIENDS:LX/IA5;

    .line 2544668
    new-instance v0, LX/IA5;

    const-string v1, "EMAILS"

    invoke-direct {v0, v1, v6}, LX/IA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IA5;->EMAILS:LX/IA5;

    .line 2544669
    new-instance v0, LX/IA5;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v7}, LX/IA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IA5;->SMS:LX/IA5;

    .line 2544670
    new-instance v0, LX/IA5;

    const-string v1, "TODAY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/IA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IA5;->TODAY:LX/IA5;

    .line 2544671
    new-instance v0, LX/IA5;

    const-string v1, "YESTERDAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/IA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IA5;->YESTERDAY:LX/IA5;

    .line 2544672
    new-instance v0, LX/IA5;

    const-string v1, "EARLIER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/IA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IA5;->EARLIER:LX/IA5;

    .line 2544673
    const/16 v0, 0x8

    new-array v0, v0, [LX/IA5;

    sget-object v1, LX/IA5;->SELF:LX/IA5;

    aput-object v1, v0, v3

    sget-object v1, LX/IA5;->FRIENDS:LX/IA5;

    aput-object v1, v0, v4

    sget-object v1, LX/IA5;->NON_FRIENDS:LX/IA5;

    aput-object v1, v0, v5

    sget-object v1, LX/IA5;->EMAILS:LX/IA5;

    aput-object v1, v0, v6

    sget-object v1, LX/IA5;->SMS:LX/IA5;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/IA5;->TODAY:LX/IA5;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IA5;->YESTERDAY:LX/IA5;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IA5;->EARLIER:LX/IA5;

    aput-object v2, v0, v1

    sput-object v0, LX/IA5;->$VALUES:[LX/IA5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2544664
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IA5;
    .locals 1

    .prologue
    .line 2544663
    const-class v0, LX/IA5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IA5;

    return-object v0
.end method

.method public static values()[LX/IA5;
    .locals 1

    .prologue
    .line 2544662
    sget-object v0, LX/IA5;->$VALUES:[LX/IA5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IA5;

    return-object v0
.end method
