.class public LX/IIB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/IFy;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0tX;

.field public final c:LX/03V;

.field public final d:LX/0Uh;

.field public final e:LX/0yD;

.field public final f:LX/IID;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0wM;

.field public final i:LX/GUm;

.field public final j:LX/0kL;

.field private final k:Lcom/facebook/content/SecureContextHelper;

.field private final l:LX/IGQ;

.field public final m:LX/0aG;

.field public final n:LX/3LX;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;LX/03V;LX/0Uh;LX/0yD;LX/IID;LX/0Or;LX/0wM;LX/GUm;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/IGQ;LX/0aG;LX/3LX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/0tX;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0yD;",
            "LX/IID;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0wM;",
            "LX/GUm;",
            "LX/0kL;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/IGQ;",
            "LX/0aG;",
            "LX/3LX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2561425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2561426
    iput-object p1, p0, LX/IIB;->a:LX/1Ck;

    .line 2561427
    iput-object p2, p0, LX/IIB;->b:LX/0tX;

    .line 2561428
    iput-object p3, p0, LX/IIB;->c:LX/03V;

    .line 2561429
    iput-object p4, p0, LX/IIB;->d:LX/0Uh;

    .line 2561430
    iput-object p5, p0, LX/IIB;->e:LX/0yD;

    .line 2561431
    iput-object p6, p0, LX/IIB;->f:LX/IID;

    .line 2561432
    iput-object p7, p0, LX/IIB;->g:LX/0Or;

    .line 2561433
    iput-object p8, p0, LX/IIB;->h:LX/0wM;

    .line 2561434
    iput-object p9, p0, LX/IIB;->i:LX/GUm;

    .line 2561435
    iput-object p10, p0, LX/IIB;->j:LX/0kL;

    .line 2561436
    iput-object p11, p0, LX/IIB;->k:Lcom/facebook/content/SecureContextHelper;

    .line 2561437
    iput-object p12, p0, LX/IIB;->l:LX/IGQ;

    .line 2561438
    iput-object p13, p0, LX/IIB;->m:LX/0aG;

    .line 2561439
    iput-object p14, p0, LX/IIB;->n:LX/3LX;

    .line 2561440
    return-void
.end method

.method public static a(LX/0QB;)LX/IIB;
    .locals 1

    .prologue
    .line 2561529
    invoke-static {p0}, LX/IIB;->b(LX/0QB;)LX/IIB;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/IIB;Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2561530
    iget-object v0, p0, LX/IIB;->f:LX/IID;

    .line 2561531
    const-string v1, "friends_nearby_dashboard_ping"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2561532
    const-string v2, "targetID"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "action"

    const-string v4, "message"

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2561533
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561534
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2561535
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2561536
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2561537
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2561538
    iget-object v0, p0, LX/IIB;->k:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2561539
    return-void
.end method

.method public static a$redex0(LX/IIB;JLcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;)V
    .locals 11

    .prologue
    .line 2561490
    invoke-virtual {p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->c()LX/0Px;

    move-result-object v2

    .line 2561491
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2561492
    iget-object v0, p0, LX/IIB;->c:LX/03V;

    const-string v1, "nearby_friends_self_view_region_topic_id_empty"

    const-string v2, "Region topic id is empty"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561493
    :cond_0
    return-void

    .line 2561494
    :cond_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2561495
    new-instance v4, LX/96G;

    invoke-direct {v4}, LX/96G;-><init>()V

    move-object v4, v4

    .line 2561496
    new-instance v5, LX/4Du;

    invoke-direct {v5}, LX/4Du;-><init>()V

    .line 2561497
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "hub_id"

    invoke-virtual {v6, v7, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    .line 2561498
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    .line 2561499
    const-string v8, "session_id"

    invoke-virtual {v5, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561500
    invoke-virtual {p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 2561501
    const-string v8, "page_id"

    invoke-virtual {v5, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561502
    sget-object v7, LX/BeB;->PLACE_TOPIC:LX/BeB;

    invoke-virtual {v7}, LX/BeB;->getValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    .line 2561503
    const-string v8, "field_type"

    invoke-virtual {v5, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561504
    invoke-virtual {v6}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2561505
    const-string v7, "knowledge_value"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561506
    const-string v6, "disagree"

    .line 2561507
    const-string v7, "sentiment"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561508
    const-string v6, "vote"

    .line 2561509
    const-string v7, "action"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561510
    const-string v6, "android_nearby_friends_self_view_unknown_region"

    .line 2561511
    const-string v7, "endpoint"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561512
    const-string v6, "android_nearby_friends_self_view_context_menu"

    .line 2561513
    const-string v7, "entry_point"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561514
    const-string v6, "input"

    invoke-virtual {v4, v6, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2561515
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2561516
    iget-object v5, p0, LX/IIB;->a:LX/1Ck;

    .line 2561517
    sget-object v6, LX/IFy;->q:LX/IFy;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v6

    check-cast v6, LX/IFy;

    move-object v0, v6

    .line 2561518
    iget-object v6, p0, LX/IIB;->b:LX/0tX;

    sget-object v7, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v6, v4, v7}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v6, LX/IIA;

    invoke-direct {v6, p0}, LX/IIA;-><init>(LX/IIB;)V

    invoke-virtual {v5, v0, v4, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2561519
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0
.end method

.method public static a$redex0(LX/IIB;JLcom/facebook/widget/SwitchCompat;LX/0gc;LX/IIm;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 2561520
    const v0, 0x7f080024

    const/4 v1, 0x0

    invoke-static {v0, v2, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 2561521
    const-string v1, "pause_progress"

    invoke-virtual {v0, p4, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2561522
    invoke-static {}, LX/BaX;->a()LX/BaW;

    move-result-object v1

    .line 2561523
    new-instance v2, LX/4Gn;

    invoke-direct {v2}, LX/4Gn;-><init>()V

    .line 2561524
    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    long-to-int v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4Gn;->a(Ljava/lang/Integer;)LX/4Gn;

    .line 2561525
    const-string v3, "input"

    invoke-virtual {v1, v3, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2561526
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2561527
    iget-object v2, p0, LX/IIB;->a:LX/1Ck;

    sget-object v3, LX/IFy;->j:LX/IFy;

    iget-object v4, p0, LX/IIB;->b:LX/0tX;

    sget-object v5, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v4, v1, v5}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v4, LX/II8;

    invoke-direct {v4, p0, v0, p5, p3}, LX/II8;-><init>(LX/IIB;Landroid/support/v4/app/DialogFragment;LX/IIm;Lcom/facebook/widget/SwitchCompat;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2561528
    return-void
.end method

.method private static b(LX/0QB;)LX/IIB;
    .locals 15

    .prologue
    .line 2561484
    new-instance v0, LX/IIB;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {p0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v5

    check-cast v5, LX/0yD;

    invoke-static {p0}, LX/IID;->a(LX/0QB;)LX/IID;

    move-result-object v6

    check-cast v6, LX/IID;

    const/16 v7, 0x1399

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {p0}, LX/GUm;->b(LX/0QB;)LX/GUm;

    move-result-object v9

    check-cast v9, LX/GUm;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    .line 2561485
    new-instance v12, LX/IGQ;

    invoke-direct {v12}, LX/IGQ;-><init>()V

    .line 2561486
    move-object v12, v12

    .line 2561487
    move-object v12, v12

    .line 2561488
    check-cast v12, LX/IGQ;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v13

    check-cast v13, LX/0aG;

    invoke-static {p0}, LX/3LX;->b(LX/0QB;)LX/3LX;

    move-result-object v14

    check-cast v14, LX/3LX;

    invoke-direct/range {v0 .. v14}, LX/IIB;-><init>(LX/1Ck;LX/0tX;LX/03V;LX/0Uh;LX/0yD;LX/IID;LX/0Or;LX/0wM;LX/GUm;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/IGQ;LX/0aG;LX/3LX;)V

    .line 2561489
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/0gc;JLcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;LX/0am;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/0gc;",
            "J",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryInterfaces$FriendsNearbyLocationSharingFields$NearbyFriendsRegion;",
            "LX/0am",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2561469
    invoke-static/range {p5 .. p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2561470
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2561471
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2561472
    iget-object v0, p0, LX/IIB;->f:LX/IID;

    invoke-virtual {v0}, LX/IID;->f()V

    .line 2561473
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2561474
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 2561475
    new-instance v9, LX/6WS;

    invoke-direct {v9, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2561476
    invoke-virtual {v9}, LX/5OM;->c()LX/5OG;

    move-result-object v10

    .line 2561477
    const v1, 0x7f083871

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f083872

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, LX/5OG;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v11

    .line 2561478
    new-instance v1, LX/II2;

    move-object v2, p0

    move-object v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v7}, LX/II2;-><init>(LX/IIB;LX/0gc;JLcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;LX/0am;)V

    invoke-virtual {v11, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2561479
    const v1, 0x7f083873

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f083874

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v1, v0}, LX/5OG;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 2561480
    new-instance v1, LX/II3;

    move-object v2, p0

    move-object v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, LX/II3;-><init>(LX/IIB;LX/0gc;JLcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2561481
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, LX/5OM;->a(Z)V

    .line 2561482
    invoke-virtual {v9, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2561483
    return-void
.end method

.method public final a(Landroid/view/View;LX/0gc;LX/IFX;LX/IIm;)V
    .locals 11

    .prologue
    .line 2561443
    check-cast p1, Lcom/facebook/widget/SwitchCompat;

    .line 2561444
    invoke-virtual {p1}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2561445
    iget-object v0, p0, LX/IIB;->f:LX/IID;

    invoke-virtual {v0}, LX/IID;->d()V

    .line 2561446
    invoke-virtual {p0, p1, p2, p4}, LX/IIB;->a(Lcom/facebook/widget/SwitchCompat;LX/0gc;LX/IIm;)V

    .line 2561447
    :goto_0
    return-void

    .line 2561448
    :cond_0
    iget-object v0, p0, LX/IIB;->f:LX/IID;

    .line 2561449
    const-string v1, "friends_nearby_dashboard_pause_tap"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2561450
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561451
    invoke-virtual {p1}, Lcom/facebook/widget/SwitchCompat;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2561452
    iget-object v1, p0, LX/IIB;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, LX/3Af;

    .line 2561453
    new-instance v8, LX/7TY;

    invoke-direct {v8, v2}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2561454
    const v1, 0x7f083122

    invoke-virtual {v8, v1}, LX/7TY;->f(I)V

    .line 2561455
    iget-object v1, p0, LX/IIB;->h:LX/0wM;

    const v3, 0x7f0207fd

    const v4, 0x7f0a00fc

    invoke-static {v2, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v3, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 2561456
    const v1, 0x7f083123

    invoke-virtual {v8, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v10

    .line 2561457
    invoke-virtual {v10, v9}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2561458
    new-instance v1, LX/II4;

    move-object v2, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/II4;-><init>(LX/IIB;LX/IFX;Lcom/facebook/widget/SwitchCompat;LX/0gc;LX/IIm;)V

    invoke-virtual {v10, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2561459
    const v1, 0x7f083124

    invoke-virtual {v8, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v10

    .line 2561460
    invoke-virtual {v10, v9}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2561461
    new-instance v1, LX/II5;

    move-object v2, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/II5;-><init>(LX/IIB;LX/IFX;Lcom/facebook/widget/SwitchCompat;LX/0gc;LX/IIm;)V

    invoke-virtual {v10, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2561462
    const v1, 0x7f083125

    invoke-virtual {v8, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v10

    .line 2561463
    invoke-virtual {v10, v9}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2561464
    new-instance v1, LX/II6;

    move-object v2, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/II6;-><init>(LX/IIB;LX/IFX;Lcom/facebook/widget/SwitchCompat;LX/0gc;LX/IIm;)V

    invoke-virtual {v10, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2561465
    invoke-virtual {v7, v8}, LX/3Af;->a(LX/1OM;)V

    .line 2561466
    new-instance v1, LX/II7;

    invoke-direct {v1, p0, p1}, LX/II7;-><init>(LX/IIB;Lcom/facebook/widget/SwitchCompat;)V

    invoke-virtual {v7, v1}, LX/3Af;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2561467
    invoke-virtual {v7}, LX/3Af;->show()V

    .line 2561468
    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/widget/SwitchCompat;LX/0gc;LX/IIm;)V
    .locals 7

    .prologue
    .line 2561441
    const-wide/16 v2, 0x0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v1 .. v6}, LX/IIB;->a$redex0(LX/IIB;JLcom/facebook/widget/SwitchCompat;LX/0gc;LX/IIm;)V

    .line 2561442
    return-void
.end method
