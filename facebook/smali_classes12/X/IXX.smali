.class public LX/IXX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static D:LX/0Xm;

.field public static z:Z


# instance fields
.field public A:Ljava/lang/String;

.field public B:J

.field public C:J

.field private final a:LX/CH7;

.field public final b:LX/0So;

.field private final c:LX/Ckw;

.field public final d:LX/Chv;

.field private final e:LX/Cl7;

.field private final f:LX/ClO;

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/Chz;

.field public final i:LX/Ci2;

.field public final j:LX/ChP;

.field public k:J

.field public l:J

.field public m:J

.field public n:J

.field public o:J

.field public p:J

.field public q:J

.field public r:Ljava/lang/String;

.field public s:LX/0ta;

.field public t:Z

.field public u:I

.field public v:J

.field public final w:LX/0p3;

.field public x:Z

.field public y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2586216
    const/4 v0, 0x1

    sput-boolean v0, LX/IXX;->z:Z

    return-void
.end method

.method public constructor <init>(LX/Chv;LX/Ckw;LX/Cl7;LX/0So;LX/8bK;LX/CH7;LX/ClO;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2586217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2586218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/IXX;->g:Ljava/util/HashMap;

    .line 2586219
    new-instance v0, LX/IXU;

    invoke-direct {v0, p0}, LX/IXU;-><init>(LX/IXX;)V

    iput-object v0, p0, LX/IXX;->h:LX/Chz;

    .line 2586220
    new-instance v0, LX/IXV;

    invoke-direct {v0, p0}, LX/IXV;-><init>(LX/IXX;)V

    iput-object v0, p0, LX/IXX;->i:LX/Ci2;

    .line 2586221
    new-instance v0, LX/IXW;

    invoke-direct {v0, p0}, LX/IXW;-><init>(LX/IXX;)V

    iput-object v0, p0, LX/IXX;->j:LX/ChP;

    .line 2586222
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/IXX;->o:J

    .line 2586223
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IXX;->x:Z

    .line 2586224
    iput-object p4, p0, LX/IXX;->b:LX/0So;

    .line 2586225
    iput-object p1, p0, LX/IXX;->d:LX/Chv;

    .line 2586226
    iput-object p2, p0, LX/IXX;->c:LX/Ckw;

    .line 2586227
    iput-object p3, p0, LX/IXX;->e:LX/Cl7;

    .line 2586228
    iget-object v0, p0, LX/IXX;->d:LX/Chv;

    iget-object v1, p0, LX/IXX;->i:LX/Ci2;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2586229
    iget-object v0, p0, LX/IXX;->d:LX/Chv;

    iget-object v1, p0, LX/IXX;->j:LX/ChP;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2586230
    iget-object v0, p0, LX/IXX;->d:LX/Chv;

    iget-object v1, p0, LX/IXX;->h:LX/Chz;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2586231
    invoke-virtual {p5}, LX/8bK;->a()LX/0p3;

    move-result-object v0

    iput-object v0, p0, LX/IXX;->w:LX/0p3;

    .line 2586232
    iput-object p6, p0, LX/IXX;->a:LX/CH7;

    .line 2586233
    iput-object p7, p0, LX/IXX;->f:LX/ClO;

    .line 2586234
    return-void
.end method

.method public static a(LX/0QB;)LX/IXX;
    .locals 11

    .prologue
    .line 2586235
    const-class v1, LX/IXX;

    monitor-enter v1

    .line 2586236
    :try_start_0
    sget-object v0, LX/IXX;->D:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2586237
    sput-object v2, LX/IXX;->D:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2586238
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2586239
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2586240
    new-instance v3, LX/IXX;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v5

    check-cast v5, LX/Ckw;

    invoke-static {v0}, LX/Cl7;->a(LX/0QB;)LX/Cl7;

    move-result-object v6

    check-cast v6, LX/Cl7;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v8

    check-cast v8, LX/8bK;

    invoke-static {v0}, LX/CH7;->a(LX/0QB;)LX/CH7;

    move-result-object v9

    check-cast v9, LX/CH7;

    invoke-static {v0}, LX/ClO;->a(LX/0QB;)LX/ClO;

    move-result-object v10

    check-cast v10, LX/ClO;

    invoke-direct/range {v3 .. v10}, LX/IXX;-><init>(LX/Chv;LX/Ckw;LX/Cl7;LX/0So;LX/8bK;LX/CH7;LX/ClO;)V

    .line 2586241
    move-object v0, v3

    .line 2586242
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2586243
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IXX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2586244
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2586245
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2586246
    iget-object v0, p0, LX/IXX;->r:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2586247
    :goto_0
    return-void

    .line 2586248
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2586249
    const-string v0, "TTI"

    iget-wide v4, p0, LX/IXX;->B:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586250
    const-string v0, "visible_render_time"

    iget-wide v4, p0, LX/IXX;->B:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586251
    const-string v0, "time_to_settle"

    iget-wide v4, p0, LX/IXX;->C:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586252
    const-string v0, "render_time"

    iget-wide v4, p0, LX/IXX;->o:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586253
    iget-wide v4, p0, LX/IXX;->n:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 2586254
    :goto_1
    const-string v3, "did_generate_story_view"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586255
    const-string v3, "webview_breakdown"

    iget-object v4, p0, LX/IXX;->g:Ljava/util/HashMap;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586256
    const-string v3, "webview_total"

    iget v4, p0, LX/IXX;->u:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586257
    const-string v3, "data_freshness"

    iget-object v4, p0, LX/IXX;->s:LX/0ta;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586258
    iget-object v3, p0, LX/IXX;->s:LX/0ta;

    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v3, v4, :cond_3

    .line 2586259
    :goto_2
    const-string v3, "cached"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586260
    iget-object v1, p0, LX/IXX;->e:LX/Cl7;

    invoke-virtual {v1}, LX/Cl7;->b()F

    move-result v1

    .line 2586261
    const-string v3, "percent_scrolled"

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586262
    const-string v3, "total_time_open"

    iget-object v4, p0, LX/IXX;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p0, LX/IXX;->v:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586263
    const-string v3, "connection_quality_at_start"

    iget-object v4, p0, LX/IXX;->w:LX/0p3;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586264
    const-string v3, "fetched_from_memory_cache"

    iget-boolean v4, p0, LX/IXX;->x:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586265
    const-string v3, "first_open_since_start"

    iget-boolean v4, p0, LX/IXX;->y:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586266
    const-string v3, "click_source"

    iget-object v4, p0, LX/IXX;->A:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586267
    const-string v3, "original_prefetch_source"

    iget-object v4, p0, LX/IXX;->a:LX/CH7;

    iget-object v5, p0, LX/IXX;->r:Ljava/lang/String;

    .line 2586268
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2586269
    const/4 v6, 0x0

    .line 2586270
    :goto_3
    move-object v4, v6

    .line 2586271
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586272
    iget-object v3, p0, LX/IXX;->c:LX/Ckw;

    const-string v4, "android_native_article_perf"

    invoke-virtual {v3, v4, v2}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 2586273
    iget-object v2, p0, LX/IXX;->f:LX/ClO;

    .line 2586274
    iget-boolean v3, v2, LX/ClO;->b:Z

    move v2, v3

    .line 2586275
    if-eqz v2, :cond_1

    .line 2586276
    new-instance v2, LX/ClL;

    const-string v3, "android_native_article_perf"

    invoke-direct {v2, v3}, LX/ClL;-><init>(Ljava/lang/String;)V

    const-string v3, "TTI"

    iget-wide v4, p0, LX/IXX;->B:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v2

    const-string v3, "did_generate_story_view"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v2, "percent_scrolled"

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    invoke-virtual {v0}, LX/ClL;->a()LX/ClM;

    move-result-object v0

    .line 2586277
    iget-object v1, p0, LX/IXX;->f:LX/ClO;

    invoke-virtual {v1, v0}, LX/ClO;->a(LX/ClM;)V

    .line 2586278
    :cond_1
    const-wide/16 v8, 0x0

    .line 2586279
    iput-wide v8, p0, LX/IXX;->m:J

    .line 2586280
    iput-wide v8, p0, LX/IXX;->q:J

    .line 2586281
    iput-wide v8, p0, LX/IXX;->C:J

    .line 2586282
    iput-wide v8, p0, LX/IXX;->p:J

    .line 2586283
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 2586284
    goto/16 :goto_1

    .line 2586285
    :cond_3
    iget-boolean v1, p0, LX/IXX;->t:Z

    goto/16 :goto_2

    :cond_4
    iget-object v6, v4, LX/CH7;->j:LX/0QI;

    invoke-interface {v6, v5}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/CH2;

    goto :goto_3
.end method
