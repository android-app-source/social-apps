.class public LX/IzD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final c:LX/IzD;

.field public static final d:LX/IzD;


# instance fields
.field public final a:Lcom/facebook/payments/p2p/model/PaymentCard;

.field public final b:LX/IzC;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2634332
    new-instance v0, LX/IzD;

    sget-object v1, LX/IzC;->CARD_EXISTS_BUT_NOT_IN_CACHE:LX/IzC;

    invoke-direct {v0, v2, v1}, LX/IzD;-><init>(Lcom/facebook/payments/p2p/model/PaymentCard;LX/IzC;)V

    sput-object v0, LX/IzD;->c:LX/IzD;

    .line 2634333
    new-instance v0, LX/IzD;

    sget-object v1, LX/IzC;->CARD_DOES_NOT_EXIST:LX/IzC;

    invoke-direct {v0, v2, v1}, LX/IzD;-><init>(Lcom/facebook/payments/p2p/model/PaymentCard;LX/IzC;)V

    sput-object v0, LX/IzD;->d:LX/IzD;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/p2p/model/PaymentCard;LX/IzC;)V
    .locals 0

    .prologue
    .line 2634334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634335
    iput-object p1, p0, LX/IzD;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2634336
    iput-object p2, p0, LX/IzD;->b:LX/IzC;

    .line 2634337
    return-void
.end method
