.class public final LX/Hza;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/Hze;


# direct methods
.method public constructor <init>(LX/Hze;I)V
    .locals 0

    .prologue
    .line 2525763
    iput-object p1, p0, LX/Hza;->b:LX/Hze;

    iput p2, p0, LX/Hza;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2525764
    invoke-static {}, LX/7oV;->h()LX/7oO;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2525765
    new-instance v1, LX/7oO;

    invoke-direct {v1}, LX/7oO;-><init>()V

    const-string v2, "profile_image_size"

    iget v3, p0, LX/Hza;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "cover_image_portrait_size"

    iget-object v3, p0, LX/Hza;->b:LX/Hze;

    iget-object v3, v3, LX/Hze;->g:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "cover_image_landscape_size"

    iget-object v3, p0, LX/Hza;->b:LX/Hze;

    iget-object v3, v3, LX/Hze;->g:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "after_cursor"

    iget-object v3, p0, LX/Hza;->b:LX/Hze;

    iget-object v3, v3, LX/Hze;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_count"

    iget-object v3, p0, LX/Hza;->b:LX/Hze;

    iget v3, v3, LX/Hze;->i:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2525766
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2525767
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2525768
    iget-object v1, p0, LX/Hza;->b:LX/Hze;

    iget-object v1, v1, LX/Hze;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
