.class public final LX/HRR;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;ZZ)V
    .locals 0

    .prologue
    .line 2465184
    iput-object p1, p0, LX/HRR;->c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iput-boolean p2, p0, LX/HRR;->a:Z

    iput-boolean p3, p0, LX/HRR;->b:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2465180
    iget-boolean v0, p0, LX/HRR;->a:Z

    if-nez v0, :cond_1

    .line 2465181
    :cond_0
    :goto_0
    return-void

    .line 2465182
    :cond_1
    iget-object v0, p0, LX/HRR;->c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->o:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HRR;->c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->E:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/HRR;->b:Z

    if-eqz v0, :cond_0

    .line 2465183
    iget-object v0, p0, LX/HRR;->c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a$redex0(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;Z)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2465169
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2465170
    iget-object v1, p0, LX/HRR;->c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2465171
    :goto_0
    iput-object v0, v1, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    .line 2465172
    iget-object v0, p0, LX/HRR;->c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->k(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    .line 2465173
    iget-object v0, p0, LX/HRR;->c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    const/4 v1, 0x0

    .line 2465174
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->C:Z

    .line 2465175
    iget-object v0, p0, LX/HRR;->c:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    const/4 v1, 0x1

    .line 2465176
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->P:Z

    .line 2465177
    return-void

    .line 2465178
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2465179
    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    goto :goto_0
.end method
