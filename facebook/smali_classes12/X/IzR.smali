.class public final LX/IzR;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field private static final b:LX/0sv;

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2634602
    new-instance v0, LX/0U1;

    const-string v1, "credential_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzR;->a:LX/0U1;

    .line 2634603
    new-instance v0, LX/2Qn;

    sget-object v1, LX/IzR;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const-string v2, "payment_card_ids"

    sget-object v3, LX/IzQ;->a:LX/0U1;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/2Qn;-><init>(LX/0Px;Ljava/lang/String;LX/0Px;)V

    sput-object v0, LX/IzR;->b:LX/0sv;

    .line 2634604
    sget-object v0, LX/IzR;->a:LX/0U1;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IzR;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2634605
    const-string v0, "primary_payment_card_id"

    sget-object v1, LX/IzR;->c:LX/0Px;

    sget-object v2, LX/IzR;->b:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 2634606
    return-void
.end method
