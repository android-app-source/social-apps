.class public final LX/HSf;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HSR;

.field public final synthetic b:I

.field public final synthetic c:LX/HSk;


# direct methods
.method public constructor <init>(LX/HSk;LX/HSR;I)V
    .locals 0

    .prologue
    .line 2467412
    iput-object p1, p0, LX/HSf;->c:LX/HSk;

    iput-object p2, p0, LX/HSf;->a:LX/HSR;

    iput p3, p0, LX/HSf;->b:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2467413
    iget-object v0, p0, LX/HSf;->a:LX/HSR;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/HSR;->a(Ljava/util/List;Ljava/lang/String;Z)V

    .line 2467414
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2467415
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2467416
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2467417
    if-eqz v0, :cond_0

    .line 2467418
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2467419
    check-cast v0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel;->a()Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel$OwnedEventsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2467420
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2467421
    check-cast v0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel;->a()Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel$OwnedEventsModel;

    move-result-object v2

    .line 2467422
    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel$OwnedEventsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel$OwnedEventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel$OwnedEventsModel;->a()LX/0Px;

    move-result-object v0

    .line 2467423
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PagePastEventsQueryModel$OwnedEventsModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v4, v2, LX/1vs;->b:I

    .line 2467424
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2467425
    if-nez v4, :cond_2

    const/4 v2, 0x0

    .line 2467426
    :goto_1
    if-nez v4, :cond_3

    .line 2467427
    :goto_2
    iget-object v3, p0, LX/HSf;->a:LX/HSR;

    invoke-virtual {v3, v0, v2, v1}, LX/HSR;->a(Ljava/util/List;Ljava/lang/String;Z)V

    .line 2467428
    :cond_0
    return-void

    .line 2467429
    :cond_1
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0

    .line 2467430
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2467431
    :cond_2
    invoke-virtual {v3, v4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2467432
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v3, v4, v1}, LX/15i;->h(II)Z

    move-result v1

    goto :goto_2
.end method
