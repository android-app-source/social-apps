.class public LX/JMH;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/JMH;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 2683434
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2683435
    iput-object p2, p0, LX/JMH;->a:Ljava/lang/String;

    .line 2683436
    iput-boolean p3, p0, LX/JMH;->b:Z

    .line 2683437
    iput-boolean p4, p0, LX/JMH;->c:Z

    .line 2683438
    return-void
.end method

.method private j()LX/5pH;
    .locals 3

    .prologue
    .line 2683429
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2683430
    const-string v1, "isAttached"

    iget-boolean v2, p0, LX/JMH;->b:Z

    invoke-interface {v0, v1, v2}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2683431
    const-string v1, "isTop"

    iget-boolean v2, p0, LX/JMH;->c:Z

    invoke-interface {v0, v1, v2}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2683432
    const-string v1, "feedUnitId"

    iget-object v2, p0, LX/JMH;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2683433
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2683425
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2683426
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/JMH;->j()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2683427
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683428
    const-string v0, "reactVpvEvent"

    return-object v0
.end method
