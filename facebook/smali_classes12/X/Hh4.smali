.class public final LX/Hh4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hh0;

.field public final synthetic b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;LX/Hh0;)V
    .locals 0

    .prologue
    .line 2494815
    iput-object p1, p0, LX/Hh4;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iput-object p2, p0, LX/Hh4;->a:LX/Hh0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2494816
    iget-object v0, p0, LX/Hh4;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2494817
    :goto_0
    return-void

    .line 2494818
    :cond_0
    iget-object v0, p0, LX/Hh4;->a:LX/Hh0;

    invoke-interface {v0}, LX/Hh0;->c()V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2494819
    check-cast p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    .line 2494820
    iget-object v0, p0, LX/Hh4;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2494821
    :goto_0
    return-void

    .line 2494822
    :cond_0
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->i:Ljava/lang/String;

    move-object v0, v0

    .line 2494823
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2494824
    iget-object v0, p0, LX/Hh4;->a:LX/Hh0;

    invoke-interface {v0}, LX/Hh0;->c()V

    goto :goto_0

    .line 2494825
    :cond_1
    iget-object v0, p0, LX/Hh4;->a:LX/Hh0;

    invoke-interface {v0}, LX/Hh0;->b()V

    .line 2494826
    iget-object v0, p0, LX/Hh4;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-static {v0, p1}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a$redex0(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;)V

    goto :goto_0
.end method
