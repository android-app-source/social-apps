.class public final LX/Ir1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic b:Landroid/app/Dialog;

.field public final synthetic c:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 2617382
    iput-object p1, p0, LX/Ir1;->c:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iput-object p2, p0, LX/Ir1;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    iput-object p3, p0, LX/Ir1;->b:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2617383
    iget-object v0, p0, LX/Ir1;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "discarded_changes"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2617384
    iget-object v0, p0, LX/Ir1;->c:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->n:LX/0Zb;

    iget-object v1, p0, LX/Ir1;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2617385
    iget-object v0, p0, LX/Ir1;->c:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-boolean v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ir1;->c:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    if-eqz v0, :cond_0

    .line 2617386
    iget-object v0, p0, LX/Ir1;->c:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    invoke-virtual {v0}, LX/IrP;->k()V

    .line 2617387
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2617388
    iget-object v0, p0, LX/Ir1;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2617389
    return-void
.end method
