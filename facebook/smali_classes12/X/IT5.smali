.class public final LX/IT5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IT4;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/ITB;


# direct methods
.method public constructor <init>(LX/ITB;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2578919
    iput-object p1, p0, LX/IT5;->b:LX/ITB;

    iput-object p2, p0, LX/IT5;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/IX1;)V
    .locals 9

    .prologue
    .line 2578920
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    if-nez v0, :cond_0

    .line 2578921
    :goto_0
    return-void

    .line 2578922
    :cond_0
    sget-object v0, LX/IT6;->a:[I

    invoke-virtual {p1}, LX/IX1;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2578923
    :pswitch_0
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0, v1}, LX/ITJ;->d(LX/9N6;)V

    goto :goto_0

    .line 2578924
    :pswitch_1
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->k:LX/IT7;

    invoke-interface {v1}, LX/IT7;->b()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, LX/IT5;->b:LX/ITB;

    iget-object v2, v2, LX/ITB;->j:LX/ITJ;

    iget-object v3, p0, LX/IT5;->b:LX/ITB;

    iget-boolean v3, v3, LX/ITB;->l:Z

    iget-object v4, p0, LX/IT5;->b:LX/ITB;

    iget-object v4, v4, LX/ITB;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v5, p0, LX/IT5;->b:LX/ITB;

    iget-object v5, v5, LX/ITB;->f:LX/DPA;

    iget-object v6, p0, LX/IT5;->b:LX/ITB;

    iget-object v6, v6, LX/ITB;->o:LX/DPp;

    iget-object v7, p0, LX/IT5;->b:LX/ITB;

    iget-object v7, v7, LX/ITB;->a:LX/0Ot;

    iget-object v8, p0, LX/IT5;->b:LX/ITB;

    iget-object v8, v8, LX/ITB;->g:LX/3my;

    invoke-static/range {v0 .. v8}, LX/ITU;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Landroid/view/View;LX/ITJ;ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;LX/DPA;LX/DPp;LX/0Ot;LX/3my;)V

    goto :goto_0

    .line 2578925
    :pswitch_2
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    .line 2578926
    if-eqz v1, :cond_1

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_7

    .line 2578927
    :cond_1
    iget-object v3, v0, LX/ITJ;->o:LX/03V;

    sget-object v4, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "groupInformation or groupId is null in addMembersToGroup"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578928
    :goto_1
    goto :goto_0

    .line 2578929
    :pswitch_3
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    .line 2578930
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_9

    .line 2578931
    :cond_2
    iget-object v3, v0, LX/ITJ;->o:LX/03V;

    sget-object v4, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "groupInformation or groupId is null in openGroupInfo"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578932
    :goto_2
    goto/16 :goto_0

    .line 2578933
    :pswitch_4
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    invoke-static {v1}, LX/ITB;->getSearchQuery(LX/ITB;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    .line 2578934
    iget-object v2, v0, LX/ITJ;->p:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    invoke-virtual {v2, v1}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2578935
    goto/16 :goto_0

    .line 2578936
    :pswitch_5
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    .line 2578937
    if-eqz v1, :cond_3

    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->F()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_a

    .line 2578938
    :cond_3
    iget-object v3, v0, LX/ITJ;->o:LX/03V;

    sget-object v4, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "groupInformation or url is null in startSharingIntent"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578939
    :goto_3
    goto/16 :goto_0

    .line 2578940
    :pswitch_6
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    .line 2578941
    if-eqz v1, :cond_4

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_b

    .line 2578942
    :cond_4
    iget-object v3, v0, LX/ITJ;->o:LX/03V;

    sget-object v4, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "groupInformation or groupId is null in reportGroup"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578943
    :goto_4
    goto/16 :goto_0

    .line 2578944
    :pswitch_7
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0, v1}, LX/ITJ;->a(LX/9N6;)V

    goto/16 :goto_0

    .line 2578945
    :pswitch_8
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0, v1}, LX/ITJ;->b(LX/9N6;)V

    goto/16 :goto_0

    .line 2578946
    :pswitch_9
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2578947
    if-eqz v1, :cond_5

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_c

    .line 2578948
    :cond_5
    iget-object v2, v0, LX/ITJ;->o:LX/03V;

    sget-object v3, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "groupInformation or groupId is null in createShortcut"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578949
    :goto_5
    goto/16 :goto_0

    .line 2578950
    :pswitch_a
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    .line 2578951
    if-eqz v1, :cond_6

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_12

    .line 2578952
    :cond_6
    iget-object v3, v0, LX/ITJ;->o:LX/03V;

    sget-object v4, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "groupInformation or groupId is null in viewPhotos"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578953
    :goto_6
    goto/16 :goto_0

    .line 2578954
    :pswitch_b
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    .line 2578955
    const-string v3, "create_new_group"

    invoke-static {v0, v3}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2578956
    iget-object v3, v0, LX/ITJ;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 2578957
    iget-object v3, v0, LX/ITJ;->e:LX/17W;

    sget-object v4, LX/0ax;->do:Ljava/lang/String;

    const-string v5, "https://m.facebook.com/groups/create?renderBasic=true"

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2578958
    :goto_7
    goto/16 :goto_0

    .line 2578959
    :pswitch_c
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    .line 2578960
    const-string v3, "group_side_conversation_displayed"

    invoke-static {v0, v3}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2578961
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    iget-object v3, v0, LX/ITJ;->k:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    .line 2578962
    const-string v4, "group_feed_id"

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2578963
    const-string v4, "target_fragment"

    sget-object v5, LX/0cQ;->GROUP_CREATE_SIDE_CONVERSATION_FRAGMENT:LX/0cQ;

    invoke-virtual {v5}, LX/0cQ;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2578964
    iget-object v4, v0, LX/ITJ;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2578965
    goto/16 :goto_0

    .line 2578966
    :pswitch_d
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, LX/ITJ;->e(LX/9N6;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2578967
    :pswitch_e
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, LX/ITJ;->i(LX/9N6;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2578968
    :pswitch_f
    iget-object v0, p0, LX/IT5;->b:LX/ITB;

    iget-object v0, v0, LX/ITB;->j:LX/ITJ;

    iget-object v1, p0, LX/IT5;->b:LX/ITB;

    iget-object v1, v1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IT5;->a:Landroid/content/Context;

    .line 2578969
    iget-object v3, v0, LX/ITJ;->r:LX/DQW;

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/DQW;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2578970
    iget-object v4, v0, LX/ITJ;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2578971
    goto/16 :goto_0

    .line 2578972
    :cond_7
    const-string v3, "add_members_to_group"

    invoke-static {v0, v3}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2578973
    iget-object v4, v0, LX/ITJ;->q:LX/DVF;

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-nez v3, :cond_8

    const/4 v3, 0x0

    :goto_8
    invoke-static {v1}, LX/IQV;->a(LX/9N6;)Z

    move-result v6

    invoke-interface {v4, v5, v3, v6, v2}, LX/DVF;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZLandroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    .line 2578974
    iget-object v4, v0, LX/ITJ;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2578975
    :cond_8
    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v3

    goto :goto_8

    .line 2578976
    :cond_9
    const-string v3, "open_group_info"

    invoke-static {v0, v3}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2578977
    iget-object v3, v0, LX/ITJ;->r:LX/DQW;

    invoke-virtual {v3, v1}, LX/DQW;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/content/Intent;

    move-result-object v3

    .line 2578978
    iget-object v4, v0, LX/ITJ;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_2

    .line 2578979
    :cond_a
    const-string v3, "share_group"

    invoke-static {v0, v3}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2578980
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.SEND"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2578981
    const-string v4, "text/plain"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2578982
    const-string v4, "android.intent.extra.TEXT"

    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->F()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2578983
    const v4, 0x7f081bcf

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    .line 2578984
    iget-object v4, v0, LX/ITJ;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_3

    .line 2578985
    :cond_b
    const-string v3, "report_group"

    invoke-static {v0, v3}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2578986
    const-string v3, "/report/id/?id=%s"

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2578987
    sget-object v4, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2578988
    iget-object v4, v0, LX/ITJ;->e:LX/17W;

    invoke-virtual {v4, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_4

    .line 2578989
    :cond_c
    const-string v2, "create_shortcut_group"

    invoke-static {v0, v2}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2578990
    sget-object v2, LX/0ax;->C:Ljava/lang/String;

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2578991
    const/4 v5, 0x0

    .line 2578992
    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_e

    .line 2578993
    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2578994
    const-class v8, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v7, v2, v4, v8}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    if-eqz v2, :cond_d

    move v2, v3

    :goto_9
    if-eqz v2, :cond_10

    .line 2578995
    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v8, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v7, v2, v4, v8}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->l()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2578996
    if-eqz v2, :cond_f

    :goto_a
    if-eqz v3, :cond_11

    .line 2578997
    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v2, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2578998
    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2578999
    :goto_b
    iget-object v3, v0, LX/ITJ;->h:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    invoke-interface {v4}, LX/9Mh;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/46b;->ROUNDED:LX/46b;

    invoke-virtual {v3, v6, v4, v2, v5}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/46b;)V

    .line 2579000
    iget-object v2, v0, LX/ITJ;->i:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f081ba7

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    goto/16 :goto_5

    :cond_d
    move v2, v4

    .line 2579001
    goto :goto_9

    :cond_e
    move v2, v4

    goto :goto_9

    :cond_f
    move v3, v4

    goto :goto_a

    :cond_10
    move v3, v4

    goto :goto_a

    :cond_11
    move-object v2, v5

    goto :goto_b

    .line 2579002
    :cond_12
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    iget-object v3, v0, LX/ITJ;->k:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    .line 2579003
    const-string v4, "group_feed_id"

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2579004
    const-string v4, "target_fragment"

    sget-object v5, LX/0cQ;->GROUP_PHOTOS_FRAGMENT:LX/0cQ;

    invoke-virtual {v5}, LX/0cQ;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2579005
    iget-object v4, v0, LX/ITJ;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_6

    .line 2579006
    :cond_13
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2579007
    if-eqz v1, :cond_15

    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    if-eqz v4, :cond_15

    .line 2579008
    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    invoke-interface {v4}, LX/9Mh;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    .line 2579009
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v4, v5, :cond_14

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v4, v5, :cond_15

    .line 2579010
    :cond_14
    const-string v4, "parent_group_id"

    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    invoke-interface {v5}, LX/9Mh;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579011
    const-string v4, "parent_group_or_page_name"

    invoke-interface {v1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    invoke-interface {v5}, LX/9Mh;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579012
    :cond_15
    const-string v4, "ref"

    const-string v5, "GROUP_MALL_MORE_DROPDOWN"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579013
    iget-object v4, v0, LX/ITJ;->e:LX/17W;

    sget-object v5, LX/0ax;->N:Ljava/lang/String;

    invoke-virtual {v4, v2, v5, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
