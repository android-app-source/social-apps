.class public LX/In5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/IzG;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/0Zb;

.field private final f:LX/5fv;

.field private final g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final h:LX/Imi;


# direct methods
.method public constructor <init>(LX/0Or;LX/IzG;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/5fv;LX/0Zb;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;LX/Imi;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pGroupCommerceEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/IzG;",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/5fv;",
            "LX/0Zb;",
            "Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;",
            "LX/Imi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610257
    iput-object p1, p0, LX/In5;->a:LX/0Or;

    .line 2610258
    iput-object p2, p0, LX/In5;->b:LX/IzG;

    .line 2610259
    iput-object p3, p0, LX/In5;->c:Landroid/content/Context;

    .line 2610260
    iput-object p4, p0, LX/In5;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2610261
    iput-object p5, p0, LX/In5;->f:LX/5fv;

    .line 2610262
    iput-object p6, p0, LX/In5;->e:LX/0Zb;

    .line 2610263
    iput-object p7, p0, LX/In5;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2610264
    iput-object p8, p0, LX/In5;->h:LX/Imi;

    .line 2610265
    return-void
.end method
