.class public final LX/HNf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HNb;


# instance fields
.field public final synthetic a:LX/HNg;


# direct methods
.method public constructor <init>(LX/HNg;)V
    .locals 0

    .prologue
    .line 2458704
    iput-object p1, p0, LX/HNf;->a:LX/HNg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2458692
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-virtual {v0, v5}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->a(Z)V

    .line 2458693
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->e:LX/CYT;

    iget-object v1, p0, LX/HNf;->a:LX/HNg;

    iget-object v1, v1, LX/HNg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CYT;->b(Ljava/lang/String;)V

    .line 2458694
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2458695
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, LX/HNf;->a:LX/HNg;

    iget-object v1, v1, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iget-object v2, p0, LX/HNf;->a:LX/HNg;

    iget-object v2, v2, LX/HNg;->a:Ljava/lang/String;

    iget-object v3, p0, LX/HNf;->a:LX/HNg;

    iget-object v3, v3, LX/HNg;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/HNf;->a:LX/HNg;

    iget-object v4, v4, LX/HNg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;Z)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/CYR;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2458696
    :goto_0
    return-void

    .line 2458697
    :cond_0
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2458698
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v1, p0, LX/HNf;->a:LX/HNg;

    iget-object v1, v1, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->aG:Ljava/lang/String;

    iget-object v3, p0, LX/HNf;->a:LX/HNg;

    iget-object v3, v3, LX/HNg;->a:Ljava/lang/String;

    const-string v4, "admin_publish_services"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2458699
    iget-object v1, p0, LX/HNf;->a:LX/HNg;

    iget-object v1, v1, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HNf;->a:LX/HNg;

    iget-object v2, v2, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2458700
    :cond_1
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->f:LX/CYR;

    iget-object v1, p0, LX/HNf;->a:LX/HNg;

    iget-object v1, v1, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081688

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/HNf;->a:LX/HNg;

    iget-object v4, v4, LX/HNg;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HNf;->a:LX/HNg;

    iget-object v2, v2, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, LX/HNf;->a:LX/HNg;

    iget-object v3, v3, LX/HNg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/CYR;->a(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2458701
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->a(Z)V

    .line 2458702
    iget-object v0, p0, LX/HNf;->a:LX/HNg;

    iget-object v0, v0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->a:LX/0kL;

    new-instance v1, LX/27k;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2458703
    return-void
.end method
