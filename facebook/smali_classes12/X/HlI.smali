.class public LX/HlI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0oy;

.field public final b:LX/0pn;

.field public final c:LX/18A;

.field public final d:LX/0Ym;

.field public e:Lcom/facebook/api/feedtype/FeedType;


# direct methods
.method public constructor <init>(LX/0oy;LX/0pn;LX/18A;LX/0Ym;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2498330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498331
    iput-object p1, p0, LX/HlI;->a:LX/0oy;

    .line 2498332
    iput-object p2, p0, LX/HlI;->b:LX/0pn;

    .line 2498333
    iput-object p3, p0, LX/HlI;->c:LX/18A;

    .line 2498334
    iput-object p4, p0, LX/HlI;->d:LX/0Ym;

    .line 2498335
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2498336
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2498337
    sget-object v7, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v8, p0, LX/HlI;->a:LX/0oy;

    invoke-virtual {v8}, LX/0oy;->i()I

    move-result v8

    int-to-long v9, v8

    invoke-virtual {v7, v9, v10}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v7

    .line 2498338
    iget-object v9, p0, LX/HlI;->b:LX/0pn;

    .line 2498339
    iget-object v10, p0, LX/HlI;->e:Lcom/facebook/api/feedtype/FeedType;

    if-nez v10, :cond_0

    .line 2498340
    iget-object v10, p0, LX/HlI;->d:LX/0Ym;

    invoke-virtual {v10}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v10

    iput-object v10, p0, LX/HlI;->e:Lcom/facebook/api/feedtype/FeedType;

    .line 2498341
    :cond_0
    iget-object v10, p0, LX/HlI;->e:Lcom/facebook/api/feedtype/FeedType;

    move-object v10, v10

    .line 2498342
    invoke-virtual {v9, v10, v7, v8}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;J)LX/0Px;

    move-result-object v7

    move-object v4, v7

    .line 2498343
    if-eqz v4, :cond_2

    .line 2498344
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2498345
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "User"

    .line 2498346
    iget-object v6, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v6, v6

    .line 2498347
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_1

    .line 2498348
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2498349
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2498350
    :cond_2
    return-object v3
.end method
