.class public LX/I0f;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/events/common/EventAnalyticsParams;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/I0X;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:LX/0hB;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hB;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2527452
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2527453
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2527454
    iput-object v0, p0, LX/I0f;->b:LX/0Px;

    .line 2527455
    iput-object p1, p0, LX/I0f;->c:Landroid/content/Context;

    .line 2527456
    iput-object p2, p0, LX/I0f;->d:LX/0hB;

    .line 2527457
    return-void
.end method

.method public static a$redex0(LX/I0f;LX/25T;)V
    .locals 3

    .prologue
    .line 2527473
    if-nez p1, :cond_0

    .line 2527474
    :goto_0
    return-void

    .line 2527475
    :cond_0
    iget-object v0, p0, LX/I0f;->d:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    iget-object v1, p0, LX/I0f;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 2527476
    iput v0, p1, LX/25T;->f:I

    .line 2527477
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2527460
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2527461
    invoke-static {}, LX/I0Y;->values()[LX/I0Y;

    move-result-object v0

    aget-object v0, v0, p2

    .line 2527462
    sget-object v2, LX/I0d;->a:[I

    invoke-virtual {v0}, LX/I0Y;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2527463
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2527464
    :pswitch_0
    new-instance v1, LX/I0c;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/I0c;-><init>(LX/I0f;Landroid/content/Context;)V

    .line 2527465
    new-instance v0, LX/I0U;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b15c3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v2}, LX/I0U;-><init>(I)V

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2527466
    new-instance v0, LX/I0e;

    invoke-direct {v0, v1}, LX/I0e;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2527467
    :pswitch_1
    new-instance v0, LX/I0e;

    const v2, 0x7f030545

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I0e;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2527468
    :pswitch_2
    new-instance v0, LX/I0e;

    const v2, 0x7f03054a

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I0e;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2527469
    :pswitch_3
    new-instance v0, LX/I0e;

    const v2, 0x7f030543

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I0e;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2527470
    :pswitch_4
    new-instance v0, LX/I0e;

    const v2, 0x7f030548

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I0e;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2527471
    :pswitch_5
    new-instance v0, LX/I0e;

    const v2, 0x7f030541

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I0e;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2527472
    :pswitch_6
    new-instance v0, LX/I0e;

    const v2, 0x7f0304c6

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I0e;-><init>(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2527478
    iget-object v0, p0, LX/I0f;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0X;

    iget-object v1, v0, LX/I0X;->a:LX/I0Y;

    .line 2527479
    iget-object v0, p0, LX/I0f;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0X;

    iget-object v0, v0, LX/I0X;->b:Ljava/lang/Object;

    .line 2527480
    sget-object v2, LX/I0d;->a:[I

    invoke-virtual {v1}, LX/I0Y;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2527481
    :goto_0
    return-void

    .line 2527482
    :pswitch_0
    new-instance v1, LX/I0T;

    invoke-direct {v1}, LX/I0T;-><init>()V

    .line 2527483
    check-cast v0, LX/0Px;

    iget-object v2, p0, LX/I0f;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527484
    iput-object v0, v1, LX/I0T;->a:LX/0Px;

    .line 2527485
    iput-object v2, v1, LX/I0T;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527486
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2527487
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2527488
    new-instance v1, LX/25T;

    invoke-virtual {v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/25U;

    invoke-direct {v3}, LX/25U;-><init>()V

    new-instance v4, LX/25V;

    invoke-direct {v4}, LX/25V;-><init>()V

    invoke-direct {v1, v2, v3, v4}, LX/25T;-><init>(Landroid/content/Context;LX/25U;LX/25V;)V

    .line 2527489
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1P1;->b(I)V

    .line 2527490
    invoke-static {p0, v1}, LX/I0f;->a$redex0(LX/I0f;LX/25T;)V

    .line 2527491
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    goto :goto_0

    .line 2527492
    :pswitch_1
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2527493
    :pswitch_2
    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527494
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;

    iget-object v2, p0, LX/I0f;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_EVENT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    .line 2527495
    :pswitch_3
    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527496
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;

    iget-object v2, p0, LX/I0f;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto :goto_0

    .line 2527497
    :pswitch_4
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2527498
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V

    goto :goto_0

    .line 2527499
    :pswitch_5
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;

    check-cast v0, Ljava/lang/Long;

    iget-object v2, p0, LX/I0f;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a(Ljava/lang/Long;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2527459
    iget-object v0, p0, LX/I0f;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0X;

    iget-object v0, v0, LX/I0X;->a:LX/I0Y;

    invoke-virtual {v0}, LX/I0Y;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2527458
    iget-object v0, p0, LX/I0f;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
