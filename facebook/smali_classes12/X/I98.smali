.class public LX/I98;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field private final c:LX/1Kf;

.field private final d:LX/1nQ;

.field private final e:LX/0SI;

.field private final f:LX/0ad;

.field private g:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field private k:Z

.field private l:Lcom/facebook/events/model/Event;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2542384
    const-class v0, LX/I98;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/I98;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/1Kf;LX/1nQ;LX/0SI;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2542543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2542544
    iput-object p1, p0, LX/I98;->b:LX/03V;

    .line 2542545
    iput-object p2, p0, LX/I98;->c:LX/1Kf;

    .line 2542546
    iput-object p3, p0, LX/I98;->d:LX/1nQ;

    .line 2542547
    iput-object p4, p0, LX/I98;->e:LX/0SI;

    .line 2542548
    iput-object p5, p0, LX/I98;->f:LX/0ad;

    .line 2542549
    return-void
.end method

.method public static a(LX/0QB;)LX/I98;
    .locals 7

    .prologue
    .line 2542540
    new-instance v1, LX/I98;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v3

    check-cast v3, LX/1Kf;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v4

    check-cast v4, LX/1nQ;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v5

    check-cast v5, LX/0SI;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct/range {v1 .. v6}, LX/I98;-><init>(LX/03V;LX/1Kf;LX/1nQ;LX/0SI;LX/0ad;)V

    .line 2542541
    move-object v0, v1

    .line 2542542
    return-object v0
.end method

.method public static a$redex0(LX/I98;Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2542436
    iget-object v0, p0, LX/I98;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v0, :cond_1

    .line 2542437
    iget-boolean v0, p0, LX/I98;->j:Z

    if-nez v0, :cond_0

    .line 2542438
    const/4 v3, 0x1

    .line 2542439
    const-class v0, Landroid/support/v4/app/FragmentActivity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 2542440
    if-nez v0, :cond_5

    .line 2542441
    iget-object v0, p0, LX/I98;->b:LX/03V;

    sget-object v1, LX/I98;->a:Ljava/lang/String;

    const-string v2, "Failed to find a fragment activity"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2542442
    iput-boolean v3, v1, LX/0VK;->d:Z

    .line 2542443
    move-object v1, v1

    .line 2542444
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2542445
    :cond_0
    :goto_0
    return-void

    .line 2542446
    :cond_1
    iget-object v0, p0, LX/I98;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ba()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;

    move-result-object v0

    .line 2542447
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    move-result-object v1

    .line 2542448
    invoke-static {v1}, LX/I98;->b(Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    .line 2542449
    new-instance v3, LX/4Z3;

    invoke-direct {v3}, LX/4Z3;-><init>()V

    .line 2542450
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 2542451
    iput-object v4, v3, LX/4Z3;->e:Ljava/lang/String;

    .line 2542452
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->k()Ljava/lang/String;

    move-result-object v4

    .line 2542453
    iput-object v4, v3, LX/4Z3;->g:Ljava/lang/String;

    .line 2542454
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v4

    .line 2542455
    iput-object v4, v3, LX/4Z3;->i:Ljava/lang/String;

    .line 2542456
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->n()Ljava/lang/String;

    move-result-object v4

    .line 2542457
    iput-object v4, v3, LX/4Z3;->u:Ljava/lang/String;

    .line 2542458
    iput-object v2, v3, LX/4Z3;->k:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2542459
    invoke-virtual {v3}, LX/4Z3;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v2

    move-object v1, v2

    .line 2542460
    new-instance v2, LX/2dc;

    invoke-direct {v2}, LX/2dc;-><init>()V

    .line 2542461
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2542462
    iput-object v3, v2, LX/2dc;->h:Ljava/lang/String;

    .line 2542463
    invoke-virtual {v2}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object v2, v2

    .line 2542464
    new-instance v3, LX/4Z5;

    invoke-direct {v3}, LX/4Z5;-><init>()V

    .line 2542465
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 2542466
    iput-object v4, v3, LX/4Z5;->c:Ljava/lang/String;

    .line 2542467
    iput-object v2, v3, LX/4Z5;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2542468
    invoke-virtual {v3}, LX/4Z5;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v2

    move-object v2, v2

    .line 2542469
    new-instance v3, LX/4X0;

    invoke-direct {v3}, LX/4X0;-><init>()V

    .line 2542470
    iput-object v1, v3, LX/4X0;->d:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 2542471
    move-object v1, v3

    .line 2542472
    iput-object v2, v1, LX/4X0;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 2542473
    move-object v1, v1

    .line 2542474
    new-instance v2, LX/4XR;

    invoke-direct {v2}, LX/4XR;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2542475
    iput-object v3, v2, LX/4XR;->fO:Ljava/lang/String;

    .line 2542476
    move-object v2, v2

    .line 2542477
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2542478
    iput-object v0, v2, LX/4XR;->iB:Ljava/lang/String;

    .line 2542479
    move-object v0, v2

    .line 2542480
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2542481
    iput-object v3, v2, LX/39x;->p:LX/0Px;

    .line 2542482
    move-object v2, v2

    .line 2542483
    new-instance v3, LX/4XR;

    invoke-direct {v3}, LX/4XR;-><init>()V

    iget-object v4, p0, LX/I98;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v4

    .line 2542484
    iput-object v4, v3, LX/4XR;->fO:Ljava/lang/String;

    .line 2542485
    move-object v3, v3

    .line 2542486
    invoke-virtual {v3}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    .line 2542487
    iput-object v3, v2, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2542488
    move-object v2, v2

    .line 2542489
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2542490
    iput-object v2, v0, LX/4XR;->iO:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2542491
    move-object v0, v0

    .line 2542492
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2542493
    iput-object v0, v1, LX/4X0;->c:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2542494
    move-object v0, v1

    .line 2542495
    invoke-virtual {v0}, LX/4X0;->a()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    .line 2542496
    invoke-static {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivity;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 2542497
    if-nez v0, :cond_2

    .line 2542498
    iget-object v0, p0, LX/I98;->b:LX/03V;

    sget-object v1, LX/I98;->a:Ljava/lang/String;

    const-string v2, "Failed to create an inline activity model for an event"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2542499
    :cond_2
    sget-object v1, LX/21D;->EVENT:LX/21D;

    const-string v2, "eventWallStatus"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    new-instance v2, LX/89I;

    iget-object v3, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542500
    iget-object v4, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2542501
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v3, LX/2rw;->EVENT:LX/2rw;

    invoke-direct {v2, v4, v5, v3}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v3, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542502
    iget-object v4, v3, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2542503
    iput-object v3, v2, LX/89I;->c:Ljava/lang/String;

    .line 2542504
    move-object v2, v2

    .line 2542505
    iget-object v3, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542506
    iget-object v4, v3, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v3, v4

    .line 2542507
    iput-object v3, v2, LX/89I;->d:Ljava/lang/String;

    .line 2542508
    move-object v2, v2

    .line 2542509
    iget-object v3, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542510
    iget-object v4, v3, Lcom/facebook/events/model/Event;->ap:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-object v3, v4

    .line 2542511
    invoke-virtual {v2, v3}, LX/89I;->a(LX/2rX;)LX/89I;

    move-result-object v2

    invoke-virtual {v2}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/I98;->f:LX/0ad;

    sget-short v2, LX/1EB;->an:S

    invoke-interface {v1, v2, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2542512
    iget-object v1, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542513
    iget-object v2, v1, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v1, v2

    .line 2542514
    if-eqz v1, :cond_3

    .line 2542515
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    iget-object v2, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542516
    iget-object v3, v2, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2542517
    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    iget-object v2, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542518
    iget-object v3, v2, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v2, v3

    .line 2542519
    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    iget-object v2, p0, LX/I98;->e:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2542520
    :cond_3
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2542521
    iget-object v2, p0, LX/I98;->c:LX/1Kf;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    const/16 v4, 0x1fb

    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v4, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2542522
    iget-object v0, p0, LX/I98;->d:LX/1nQ;

    iget-object v2, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542523
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2542524
    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->HEADER:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2542525
    iget-object v4, v0, LX/1nQ;->i:LX/0Zb;

    const-string v5, "event_say_thanks_button_click"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2542526
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2542527
    const-string v5, "event_permalink"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2542528
    iget-object v5, v0, LX/1nQ;->j:LX/0kv;

    iget-object v6, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v5, v6}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2542529
    const-string v5, "Event"

    invoke-virtual {v4, v5}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2542530
    invoke-virtual {v4, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2542531
    const-string v5, "mechanism"

    invoke-virtual {v4, v5, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542532
    const-string v5, "composer_session_id"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542533
    const-string v5, "event_id"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542534
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2542535
    :cond_4
    goto/16 :goto_0

    .line 2542536
    :cond_5
    const v1, 0x7f081f77

    invoke-static {v1, v3, v3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    iput-object v1, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    .line 2542537
    iget-object v1, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    new-instance v2, LX/I96;

    invoke-direct {v2, p0}, LX/I96;-><init>(LX/I98;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2542538
    iget-object v1, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    new-instance v2, LX/I97;

    invoke-direct {v2, p0}, LX/I97;-><init>(LX/I98;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2542539
    iget-object v1, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "thanksDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static b(Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 6

    .prologue
    .line 2542413
    new-instance v2, LX/4Z6;

    invoke-direct {v2}, LX/4Z6;-><init>()V

    .line 2542414
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    .line 2542415
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v3

    .line 2542416
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2542417
    iput-object v0, v2, LX/4Z6;->b:Ljava/lang/String;

    .line 2542418
    if-eqz v3, :cond_1

    .line 2542419
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2542420
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2542421
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;

    .line 2542422
    if-nez v0, :cond_2

    .line 2542423
    const/4 v5, 0x0

    .line 2542424
    :goto_1
    move-object v0, v5

    .line 2542425
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2542426
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2542427
    :cond_0
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2542428
    iput-object v0, v2, LX/4Z6;->c:LX/0Px;

    .line 2542429
    :cond_1
    invoke-virtual {v2}, LX/4Z6;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    return-object v0

    .line 2542430
    :cond_2
    new-instance v5, LX/4Vm;

    invoke-direct {v5}, LX/4Vm;-><init>()V

    .line 2542431
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->a()I

    move-result p0

    .line 2542432
    iput p0, v5, LX/4Vm;->b:I

    .line 2542433
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object p0

    .line 2542434
    iput-object p0, v5, LX/4Vm;->c:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 2542435
    invoke-virtual {v5}, LX/4Vm;->a()Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    move-result-object v5

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Z)V
    .locals 5
    .param p3    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2542385
    iput-object p2, p0, LX/I98;->l:Lcom/facebook/events/model/Event;

    .line 2542386
    iget-boolean v0, p0, LX/I98;->k:Z

    if-nez v0, :cond_1

    .line 2542387
    iget-object v0, p0, LX/I98;->d:LX/1nQ;

    .line 2542388
    iget-object v1, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2542389
    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HEADER:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2542390
    iget-object v3, v0, LX/1nQ;->i:LX/0Zb;

    const-string v4, "event_say_thanks_button_impression"

    const/4 p2, 0x0

    invoke-interface {v3, v4, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2542391
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2542392
    const-string v4, "event_permalink"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2542393
    iget-object v4, v0, LX/1nQ;->j:LX/0kv;

    iget-object p2, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v4, p2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2542394
    const-string v4, "Event"

    invoke-virtual {v3, v4}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2542395
    invoke-virtual {v3, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2542396
    const-string v4, "mechanism"

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542397
    const-string v4, "event_id"

    invoke-virtual {v3, v4, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542398
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2542399
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I98;->k:Z

    .line 2542400
    :cond_1
    iput-object p3, p0, LX/I98;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2542401
    iput-boolean p4, p0, LX/I98;->j:Z

    .line 2542402
    iget-object v0, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2542403
    iget-object v0, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2542404
    iget-boolean v0, p0, LX/I98;->j:Z

    if-eqz v0, :cond_2

    .line 2542405
    iget-object v0, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2542406
    :cond_2
    iget-object v0, p0, LX/I98;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v0, :cond_3

    .line 2542407
    iget-object v0, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2542408
    iget-object v0, p0, LX/I98;->i:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, v0}, LX/I98;->a$redex0(LX/I98;Landroid/content/Context;)V

    .line 2542409
    :cond_3
    iget-object v0, p0, LX/I98;->g:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_4

    .line 2542410
    new-instance v0, LX/I95;

    invoke-direct {v0, p0}, LX/I95;-><init>(LX/I98;)V

    iput-object v0, p0, LX/I98;->g:Landroid/view/View$OnClickListener;

    .line 2542411
    :cond_4
    iget-object v0, p0, LX/I98;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2542412
    return-void
.end method
