.class public LX/HwT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final b:LX/0gd;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0il;LX/0gd;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/0gd;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2520931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520932
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HwT;->a:Ljava/lang/ref/WeakReference;

    .line 2520933
    iput-object p2, p0, LX/HwT;->b:LX/0gd;

    .line 2520934
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/HwT;->c:Ljava/lang/String;

    .line 2520935
    return-void
.end method

.method private static a(LX/0Rf;LX/0Px;LX/0Pz;LX/0Pz;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2520973
    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    .line 2520974
    invoke-static {v1, p0}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-virtual {v0}, LX/0Ro;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2520975
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 2520976
    iget-object p1, v0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v0, p1

    .line 2520977
    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2520978
    :cond_0
    invoke-static {p0, v1}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-virtual {v0}, LX/0Ro;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2520979
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 2520980
    iget-object v2, v0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v0, v2

    .line 2520981
    invoke-virtual {p3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2520982
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2520971
    iget-object v0, p0, LX/HwT;->b:LX/0gd;

    sget-object v1, LX/0ge;->COMPOSER_ATTACH_PHOTO_CANCEL:LX/0ge;

    iget-object v2, p0, LX/HwT;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2520972
    return-void
.end method

.method public final a(LX/0Px;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2520942
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2520943
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2520944
    iget-object v0, p0, LX/HwT;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2520945
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    .line 2520946
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 2520947
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2520948
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2520949
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2520950
    :cond_0
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    move-object v0, v2

    .line 2520951
    invoke-static {v0, p1, v1, v3}, LX/HwT;->a(LX/0Rf;LX/0Px;LX/0Pz;LX/0Pz;)V

    .line 2520952
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2520953
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2520954
    const/4 v1, 0x0

    .line 2520955
    if-eqz p1, :cond_6

    .line 2520956
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v4, v1

    :goto_1
    if-ge v4, v5, :cond_6

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2520957
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 2520958
    iget-object v6, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v6

    .line 2520959
    sget-object v6, LX/4gQ;->Video:LX/4gQ;

    if-ne v0, v6, :cond_5

    .line 2520960
    const/4 v0, 0x1

    .line 2520961
    :goto_2
    move v4, v0

    .line 2520962
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2520963
    :cond_1
    iget-object v0, p0, LX/HwT;->b:LX/0gd;

    iget-object v1, p0, LX/HwT;->c:Ljava/lang/String;

    move v5, p2

    invoke-virtual/range {v0 .. v5}, LX/0gd;->a(Ljava/lang/String;LX/0Px;LX/0Px;ZZ)V

    .line 2520964
    :cond_2
    if-eqz v4, :cond_4

    .line 2520965
    iget-object v0, p0, LX/HwT;->b:LX/0gd;

    sget-object v1, LX/0ge;->COMPOSER_ATTACH_MOVIE:LX/0ge;

    iget-object v2, p0, LX/HwT;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2520966
    :cond_3
    :goto_3
    return-void

    .line 2520967
    :cond_4
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2520968
    iget-object v0, p0, LX/HwT;->b:LX/0gd;

    sget-object v1, LX/0ge;->COMPOSER_ATTACH_PHOTO:LX/0ge;

    iget-object v2, p0, LX/HwT;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto :goto_3

    .line 2520969
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_6
    move v0, v1

    .line 2520970
    goto :goto_2
.end method

.method public final a(ZZ)V
    .locals 4

    .prologue
    .line 2520936
    iget-object v0, p0, LX/HwT;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2520937
    if-eqz p1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->l(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2520938
    iget-object v1, p0, LX/HwT;->b:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_PHOTO_REMOVE:LX/0ge;

    iget-object v3, p0, LX/HwT;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2520939
    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2520940
    iget-object v0, p0, LX/HwT;->b:LX/0gd;

    sget-object v1, LX/0ge;->COMPOSER_ATTACH_MOVIE_REMOVE:LX/0ge;

    iget-object v2, p0, LX/HwT;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2520941
    :cond_1
    return-void
.end method
