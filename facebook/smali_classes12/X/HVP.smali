.class public LX/HVP;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Z

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2475031
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/HVP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475032
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2475029
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/HVP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475030
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2475020
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475021
    sget-object v0, LX/03r;->CustomViewStub:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2475022
    sget-object v1, LX/03r;->PageViewPlaceholder:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2475023
    const/16 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, LX/HVP;->a:I

    .line 2475024
    const/16 v2, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, LX/HVP;->b:Z

    .line 2475025
    const/16 v2, 0x1

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, LX/HVP;->c:Z

    .line 2475026
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2475027
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2475028
    return-void
.end method


# virtual methods
.method public getAttachedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2475019
    iget-object v0, p0, LX/HVP;->d:Landroid/view/View;

    return-object v0
.end method

.method public getInflatedLayoutId()I
    .locals 1

    .prologue
    .line 2475018
    iget v0, p0, LX/HVP;->a:I

    return v0
.end method
