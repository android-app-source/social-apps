.class public final LX/IXN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public final b:J

.field public final c:LX/IXO;

.field public final synthetic d:LX/IXQ;

.field public e:LX/IXM;


# direct methods
.method public constructor <init>(LX/IXQ;LX/IXO;)V
    .locals 2

    .prologue
    .line 2585946
    iput-object p1, p0, LX/IXN;->d:LX/IXQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585947
    iget-object v0, p1, LX/IXQ;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/IXN;->b:J

    .line 2585948
    sget-object v0, LX/IXM;->LOADING:LX/IXM;

    iput-object v0, p0, LX/IXN;->e:LX/IXM;

    .line 2585949
    iput-object p2, p0, LX/IXN;->c:LX/IXO;

    .line 2585950
    return-void
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 2585951
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2585952
    const-string v1, "progress"

    iget v2, p0, LX/IXN;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585953
    const-string v1, "time_spent_loading"

    iget-object v2, p0, LX/IXN;->d:LX/IXQ;

    iget-object v2, v2, LX/IXQ;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/IXN;->b:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585954
    const-string v1, "tracker_type"

    iget-object v2, p0, LX/IXN;->c:LX/IXO;

    invoke-virtual {v2}, LX/IXO;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585955
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    return-object v1
.end method
