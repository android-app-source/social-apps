.class public abstract LX/He1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/app/Activity;

.field public b:LX/0hx;

.field public c:Z

.field public d:Landroid/widget/EditText;

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(LX/0hx;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 2489568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2489569
    iput-object p2, p0, LX/He1;->a:Landroid/app/Activity;

    .line 2489570
    iput-object p1, p0, LX/He1;->b:LX/0hx;

    .line 2489571
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/He1;->c:Z

    .line 2489572
    return-void
.end method

.method public static a(LX/He1;)V
    .locals 2

    .prologue
    .line 2489566
    iget-object v0, p0, LX/He1;->e:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2489567
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/TextWatcher;)V
    .locals 2

    .prologue
    .line 2489562
    iget-object v0, p0, LX/He1;->d:Landroid/widget/EditText;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2489563
    iput-object p1, p0, LX/He1;->f:Landroid/text/TextWatcher;

    .line 2489564
    iget-object v0, p0, LX/He1;->d:Landroid/widget/EditText;

    iget-object v1, p0, LX/He1;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2489565
    return-void
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract d()V
.end method
