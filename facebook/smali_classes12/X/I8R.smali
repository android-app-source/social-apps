.class public LX/I8R;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/I7i;

.field private final c:LX/DBC;

.field private final d:LX/I8u;

.field private final e:LX/I91;

.field private final f:LX/I98;

.field private final g:Lcom/facebook/events/common/EventAnalyticsParams;

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/I8Q;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field public j:Lcom/facebook/events/model/Event;

.field public k:Z

.field private l:LX/I89;

.field private m:LX/I88;

.field public n:Z

.field private o:Z

.field private p:Z

.field public q:LX/IBk;

.field private r:LX/0ad;

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/I89;LX/I7i;LX/DBC;LX/I8u;Lcom/facebook/events/common/EventAnalyticsParams;LX/I91;LX/I98;LX/IBk;LX/0ad;)V
    .locals 1

    .prologue
    .line 2541273
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2541274
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2541275
    iput-object v0, p0, LX/I8R;->h:LX/0Px;

    .line 2541276
    iput-object p7, p0, LX/I8R;->e:LX/I91;

    .line 2541277
    iput-object p8, p0, LX/I8R;->f:LX/I98;

    .line 2541278
    iput-object p1, p0, LX/I8R;->a:Landroid/content/Context;

    .line 2541279
    iput-object p3, p0, LX/I8R;->b:LX/I7i;

    .line 2541280
    iput-object p4, p0, LX/I8R;->c:LX/DBC;

    .line 2541281
    iput-object p5, p0, LX/I8R;->d:LX/I8u;

    .line 2541282
    iput-object p6, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2541283
    iput-object p2, p0, LX/I8R;->l:LX/I89;

    .line 2541284
    iput-object p9, p0, LX/I8R;->q:LX/IBk;

    .line 2541285
    iput-object p10, p0, LX/I8R;->r:LX/0ad;

    .line 2541286
    return-void
.end method

.method private static a(LX/I8Q;)I
    .locals 3

    .prologue
    .line 2541253
    sget-object v0, LX/I8P;->a:[I

    invoke-virtual {p0}, LX/I8Q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2541254
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2541255
    :pswitch_0
    const v0, 0x7f03024e

    .line 2541256
    :goto_0
    return v0

    .line 2541257
    :pswitch_1
    const v0, 0x7f03024e

    goto :goto_0

    .line 2541258
    :pswitch_2
    const v0, 0x7f0304e8

    goto :goto_0

    .line 2541259
    :pswitch_3
    const v0, 0x7f0304f3

    goto :goto_0

    .line 2541260
    :pswitch_4
    const v0, 0x7f0304e4

    goto :goto_0

    .line 2541261
    :pswitch_5
    const v0, 0x7f0304e6

    goto :goto_0

    .line 2541262
    :pswitch_6
    const v0, 0x7f030501

    goto :goto_0

    .line 2541263
    :pswitch_7
    const v0, 0x7f0304fc

    goto :goto_0

    .line 2541264
    :pswitch_8
    const v0, 0x7f0304fd

    goto :goto_0

    .line 2541265
    :pswitch_9
    const v0, 0x7f0304f9

    goto :goto_0

    .line 2541266
    :pswitch_a
    const v0, 0x7f0304f8

    goto :goto_0

    .line 2541267
    :pswitch_b
    const v0, 0x7f0304fb

    goto :goto_0

    .line 2541268
    :pswitch_c
    const v0, 0x7f0304f1

    goto :goto_0

    .line 2541269
    :pswitch_d
    const v0, 0x7f0304f0

    goto :goto_0

    .line 2541270
    :pswitch_e
    const v0, 0x7f0304e4

    goto :goto_0

    .line 2541271
    :pswitch_f
    const v0, 0x7f0304e6

    goto :goto_0

    .line 2541272
    :pswitch_10
    const v0, 0x7f0304ed

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 2541252
    iget-object v0, p0, LX/I8R;->h:LX/0Px;

    sget-object v1, LX/I8Q;->TOXICLE_CHECKIN_CALL_TO_ACTION:LX/I8Q;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/I8R;->h:LX/0Px;

    sget-object v1, LX/I8Q;->TOXICLE_BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/I8R;->h:LX/0Px;

    sget-object v1, LX/I8Q;->TOXICLE_SAY_THANKS_CALL_TO_ACTION:LX/I8Q;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/I8R;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2541146
    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_0

    .line 2541147
    :goto_0
    return-void

    .line 2541148
    :cond_0
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2541149
    iget-boolean v0, p0, LX/I8R;->n:Z

    if-eqz v0, :cond_11

    sget-object v0, LX/I8Q;->TOXICLE_EVENT_COVER:LX/I8Q;

    :goto_1
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541150
    iget-boolean v0, p0, LX/I8R;->n:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541151
    iget-boolean v3, v0, Lcom/facebook/events/model/Event;->y:Z

    move v0, v3

    .line 2541152
    if-eqz v0, :cond_1

    .line 2541153
    sget-object v0, LX/I8Q;->CANCEL_EVENT_BANNER:LX/I8Q;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541154
    :cond_1
    iget-boolean v0, p0, LX/I8R;->n:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541155
    iget-boolean v3, v0, Lcom/facebook/events/model/Event;->z:Z

    move v0, v3

    .line 2541156
    if-eqz v0, :cond_2

    .line 2541157
    sget-object v0, LX/I8Q;->DRAFT_EVENT_BANNER:LX/I8Q;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541158
    :cond_2
    iget-object v0, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2541159
    if-eqz v0, :cond_1a

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->X()I

    move-result v3

    if-lez v3, :cond_1a

    const/4 v3, 0x1

    :goto_2
    move v0, v3

    .line 2541160
    if-eqz v0, :cond_3

    .line 2541161
    sget-object v0, LX/I8Q;->PURCHASED_TICKETS_SUMMARY:LX/I8Q;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541162
    :cond_3
    iget-object v0, p0, LX/I8R;->d:LX/I8u;

    iget-object v3, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v5, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2541163
    iget-object v6, v0, LX/I8u;->b:LX/I8s;

    invoke-virtual {v6, v3, v5}, LX/I8s;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 2541164
    const/4 v6, 0x2

    .line 2541165
    :goto_3
    move v0, v6

    .line 2541166
    iput-boolean v2, p0, LX/I8R;->o:Z

    .line 2541167
    const/4 v3, 0x2

    if-ne v0, v3, :cond_13

    .line 2541168
    iput-boolean v1, p0, LX/I8R;->o:Z

    .line 2541169
    iget-boolean v0, p0, LX/I8R;->n:Z

    if-eqz v0, :cond_12

    sget-object v0, LX/I8Q;->TOXICLE_BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

    :goto_4
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541170
    :cond_4
    :goto_5
    iget-boolean v0, p0, LX/I8R;->n:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541171
    iget-boolean v3, v0, Lcom/facebook/events/model/Event;->H:Z

    move v0, v3

    .line 2541172
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541173
    iget-object v3, v0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v3

    .line 2541174
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541175
    iget-object v3, v0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v3

    .line 2541176
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v0, v3

    .line 2541177
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2541178
    sget-object v0, LX/I8Q;->TOXICLE_INVITED_BY_BAR:LX/I8Q;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541179
    :cond_5
    iget-boolean v0, p0, LX/I8R;->n:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541180
    iget-boolean v3, v0, Lcom/facebook/events/model/Event;->H:Z

    move v0, v3

    .line 2541181
    if-nez v0, :cond_6

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541182
    iget-object v3, v0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, v3

    .line 2541183
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v0, v3, :cond_6

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v3, :cond_6

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541184
    iget-object v3, v0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v3

    .line 2541185
    if-eqz v0, :cond_6

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541186
    iget-object v3, v0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v3

    .line 2541187
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v0, v3

    .line 2541188
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2541189
    sget-object v0, LX/I8Q;->TOXICLE_MESSAGE_INVITER_BAR:LX/I8Q;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541190
    :cond_6
    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541191
    iget-boolean v3, v0, Lcom/facebook/events/model/Event;->y:Z

    move v0, v3

    .line 2541192
    if-nez v0, :cond_7

    .line 2541193
    sget-object v0, LX/I8Q;->TOXICLE_ACTION_BAR:LX/I8Q;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541194
    :cond_7
    sget-object v0, LX/I8Q;->SUMMARY_LINES:LX/I8Q;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541195
    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541196
    iget-object v3, v0, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v0, v3

    .line 2541197
    if-eqz v0, :cond_17

    new-array v3, v1, [Ljava/lang/CharSequence;

    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    move v0, v1

    .line 2541198
    :goto_6
    iget-object v3, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541199
    iget-object v5, v3, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v3, v5

    .line 2541200
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v3, v5, :cond_18

    iget-boolean v3, p0, LX/I8R;->n:Z

    if-eqz v3, :cond_18

    move v3, v1

    .line 2541201
    :goto_7
    if-eqz v0, :cond_8

    if-eqz v3, :cond_8

    .line 2541202
    sget-object v5, LX/I8Q;->TOXICLE_DETAILS_VIEW:LX/I8Q;

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541203
    :cond_8
    iget-boolean v5, p0, LX/I8R;->n:Z

    if-nez v5, :cond_9

    .line 2541204
    sget-object v5, LX/I8Q;->HEADER_SHADOW:LX/I8Q;

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541205
    :cond_9
    iget-object v5, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541206
    iget-object v6, v5, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v5, v6

    .line 2541207
    invoke-static {v5}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, LX/I8R;->s:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    iget-boolean v5, p0, LX/I8R;->p:Z

    if-nez v5, :cond_a

    iget-object v5, p0, LX/I8R;->r:LX/0ad;

    sget-short v6, LX/347;->b:S

    invoke-interface {v5, v6, v2}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2541208
    sget-object v5, LX/I8Q;->COPY_EVENT_INVITES:LX/I8Q;

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541209
    iput-boolean v1, p0, LX/I8R;->p:Z

    .line 2541210
    :cond_a
    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541211
    iget-object v5, v1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v5

    .line 2541212
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eq v1, v5, :cond_10

    .line 2541213
    iget-object v1, p0, LX/I8R;->r:LX/0ad;

    sget-short v5, LX/347;->O:S

    invoke-interface {v1, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541214
    iget-boolean v5, v1, Lcom/facebook/events/model/Event;->z:Z

    move v1, v5

    .line 2541215
    if-nez v1, :cond_d

    iget-object v1, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v1, :cond_b

    iget-boolean v1, p0, LX/I8R;->k:Z

    if-eqz v1, :cond_c

    :cond_b
    iget-object v1, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v1, :cond_d

    iget-object v1, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->an()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2541216
    :cond_c
    sget-object v1, LX/I8Q;->GUEST_LIST:LX/I8Q;

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541217
    :cond_d
    iget-object v1, p0, LX/I8R;->r:LX/0ad;

    sget-short v5, LX/347;->O:S

    invoke-interface {v1, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_e

    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541218
    iget-boolean v2, v1, Lcom/facebook/events/model/Event;->y:Z

    move v1, v2

    .line 2541219
    if-nez v1, :cond_e

    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    sget-object v2, LX/7vK;->INVITE:LX/7vK;

    invoke-virtual {v1, v2}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2541220
    sget-object v1, LX/I8Q;->INVITE_FRIENDS:LX/I8Q;

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541221
    :cond_e
    iget-object v1, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v1, :cond_19

    .line 2541222
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2541223
    :goto_8
    if-nez v3, :cond_10

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_f

    if-eqz v0, :cond_10

    .line 2541224
    :cond_f
    sget-object v0, LX/I8Q;->DETAILS_VIEW:LX/I8Q;

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541225
    :cond_10
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/I8R;->h:LX/0Px;

    goto/16 :goto_0

    .line 2541226
    :cond_11
    sget-object v0, LX/I8Q;->EVENT_COVER:LX/I8Q;

    goto/16 :goto_1

    .line 2541227
    :cond_12
    sget-object v0, LX/I8Q;->BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

    goto/16 :goto_4

    .line 2541228
    :cond_13
    if-ne v0, v1, :cond_15

    .line 2541229
    iget-boolean v0, p0, LX/I8R;->n:Z

    if-eqz v0, :cond_14

    sget-object v0, LX/I8Q;->TOXICLE_CHECKIN_CALL_TO_ACTION:LX/I8Q;

    :goto_9
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_5

    :cond_14
    sget-object v0, LX/I8Q;->CHECKIN_CALL_TO_ACTION:LX/I8Q;

    goto :goto_9

    .line 2541230
    :cond_15
    const/4 v3, 0x3

    if-ne v0, v3, :cond_4

    .line 2541231
    iget-boolean v0, p0, LX/I8R;->n:Z

    if-eqz v0, :cond_16

    sget-object v0, LX/I8Q;->TOXICLE_SAY_THANKS_CALL_TO_ACTION:LX/I8Q;

    :goto_a
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_5

    :cond_16
    sget-object v0, LX/I8Q;->SAY_THANKS_CALL_TO_ACTION:LX/I8Q;

    goto :goto_a

    :cond_17
    move v0, v2

    .line 2541232
    goto/16 :goto_6

    :cond_18
    move v3, v2

    .line 2541233
    goto/16 :goto_7

    .line 2541234
    :cond_19
    iget-object v1, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v1}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v1

    goto :goto_8

    :cond_1a
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 2541235
    :cond_1b
    iget-object v6, v0, LX/I8u;->a:LX/I8t;

    .line 2541236
    iget-object v7, v3, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v7, v7

    .line 2541237
    invoke-static {v7}, Lcom/facebook/events/model/Event;->b(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v7

    if-eqz v7, :cond_1e

    iget-object v7, v6, LX/I8t;->a:LX/I7W;

    const/4 v5, 0x1

    .line 2541238
    invoke-static {v7, v3}, LX/I7W;->c(LX/I7W;Lcom/facebook/events/model/Event;)I

    move-result v6

    if-ne v6, v5, :cond_1f

    :goto_b
    move v7, v5

    .line 2541239
    if-eqz v7, :cond_1e

    .line 2541240
    iget-boolean v7, v3, Lcom/facebook/events/model/Event;->y:Z

    move v7, v7

    .line 2541241
    if-nez v7, :cond_1e

    invoke-virtual {v3}, Lcom/facebook/events/model/Event;->R()Z

    move-result v7

    if-eqz v7, :cond_1e

    const/4 v7, 0x1

    :goto_c
    move v6, v7

    .line 2541242
    if-eqz v6, :cond_1c

    .line 2541243
    const/4 v6, 0x1

    goto/16 :goto_3

    .line 2541244
    :cond_1c
    iget-object v6, v0, LX/I8u;->c:LX/I99;

    .line 2541245
    iget-object v7, v3, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v7, v7

    .line 2541246
    invoke-static {v7}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v7

    if-eqz v7, :cond_20

    iget-object v7, v6, LX/I99;->a:LX/I7W;

    invoke-virtual {v7, v3}, LX/I7W;->a(Lcom/facebook/events/model/Event;)Z

    move-result v7

    if-eqz v7, :cond_20

    .line 2541247
    iget-object v7, v6, LX/I99;->b:LX/0ad;

    sget-short v0, LX/347;->f:S

    const/4 v3, 0x0

    invoke-interface {v7, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v7

    move v7, v7

    .line 2541248
    if-eqz v7, :cond_20

    const/4 v7, 0x1

    :goto_d
    move v6, v7

    .line 2541249
    if-eqz v6, :cond_1d

    .line 2541250
    const/4 v6, 0x3

    goto/16 :goto_3

    .line 2541251
    :cond_1d
    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_1e
    const/4 v7, 0x0

    goto :goto_c

    :cond_1f
    const/4 v5, 0x0

    goto :goto_b

    :cond_20
    const/4 v7, 0x0

    goto :goto_d
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2541133
    invoke-static {}, LX/I8Q;->values()[LX/I8Q;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2541134
    sget-object v1, LX/I8Q;->SUMMARY_LINES:LX/I8Q;

    if-ne v0, v1, :cond_0

    .line 2541135
    new-instance v1, LX/IBj;

    iget-object v0, p0, LX/I8R;->a:Landroid/content/Context;

    iget-boolean v2, p0, LX/I8R;->n:Z

    invoke-direct {v1, v0, v2}, LX/IBj;-><init>(Landroid/content/Context;Z)V

    .line 2541136
    :goto_0
    return-object v1

    .line 2541137
    :cond_0
    sget-object v1, LX/I8Q;->TOXICLE_ACTION_BAR:LX/I8Q;

    if-ne v0, v1, :cond_1

    .line 2541138
    iget-object v0, p0, LX/I8R;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030505

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2541139
    const v0, 0x7f0d0193

    invoke-virtual {v1, v0}, Landroid/view/View;->setId(I)V

    .line 2541140
    iget-object v2, p0, LX/I8R;->l:LX/I89;

    move-object v0, v1

    check-cast v0, LX/AhO;

    invoke-virtual {v2, v0}, LX/I89;->a(LX/AhO;)LX/I88;

    move-result-object v0

    iput-object v0, p0, LX/I8R;->m:LX/I88;

    goto :goto_0

    .line 2541141
    :cond_1
    sget-object v1, LX/I8Q;->TOXICLE_EVENT_COVER:LX/I8Q;

    if-ne v0, v1, :cond_2

    .line 2541142
    new-instance v1, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, LX/I8R;->a:Landroid/content/Context;

    const v3, 0x7f0e05b1

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;-><init>(Landroid/content/Context;Z)V

    goto :goto_0

    .line 2541143
    :cond_2
    sget-object v1, LX/I8Q;->EVENT_COVER:LX/I8Q;

    if-ne v0, v1, :cond_3

    .line 2541144
    new-instance v1, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, p0, LX/I8R;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;-><init>(Landroid/content/Context;Z)V

    goto :goto_0

    .line 2541145
    :cond_3
    iget-object v1, p0, LX/I8R;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {v0}, LX/I8R;->a(LX/I8Q;)I

    move-result v0

    invoke-virtual {v1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 2541287
    sget-object v0, LX/I8P;->a:[I

    check-cast p2, LX/I8Q;

    invoke-virtual {p2}, LX/I8Q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2541288
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    move-object v0, p3

    .line 2541289
    check-cast v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    .line 2541290
    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v3, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541291
    iget-boolean v6, v5, Lcom/facebook/events/model/Event;->z:Z

    move v5, v6

    .line 2541292
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;ZZ)V

    goto :goto_0

    :pswitch_2
    move-object v0, p3

    .line 2541293
    check-cast v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    .line 2541294
    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v3, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2541295
    iget-boolean v5, v4, Lcom/facebook/events/model/Event;->z:Z

    move v5, v5

    .line 2541296
    move v4, v6

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;ZZ)V

    goto :goto_0

    :pswitch_3
    move-object v0, p3

    .line 2541297
    check-cast v0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541298
    :pswitch_4
    check-cast p3, Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    invoke-virtual {p3, v0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a(Lcom/facebook/events/model/Event;)V

    goto :goto_0

    .line 2541299
    :pswitch_5
    iget-object v0, p0, LX/I8R;->d:LX/I8u;

    .line 2541300
    iget-object v1, v0, LX/I8u;->b:LX/I8s;

    move-object v0, v1

    .line 2541301
    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    check-cast p3, Landroid/widget/TextView;

    iget-object v3, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2, p3, v3}, LX/I8s;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Landroid/widget/TextView;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto :goto_0

    .line 2541302
    :pswitch_6
    iget-object v0, p0, LX/I8R;->d:LX/I8u;

    .line 2541303
    iget-object v1, v0, LX/I8u;->b:LX/I8s;

    move-object v0, v1

    .line 2541304
    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    check-cast p3, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;

    .line 2541305
    iget-object v3, p3, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;->a:Landroid/widget/TextView;

    move-object v3, v3

    .line 2541306
    iget-object v4, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/I8s;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Landroid/widget/TextView;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto :goto_0

    .line 2541307
    :pswitch_7
    iget-boolean v0, p0, LX/I8R;->o:Z

    if-eqz v0, :cond_0

    move-object v0, p3

    .line 2541308
    check-cast v0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;

    invoke-virtual {v0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->a()V

    .line 2541309
    :cond_0
    check-cast p3, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;

    iget-object v0, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2541310
    iput-object v0, p3, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2541311
    goto :goto_0

    .line 2541312
    :pswitch_8
    check-cast p3, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;

    .line 2541313
    iget-object v0, p3, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;->a:Landroid/widget/TextView;

    move-object v0, v0

    .line 2541314
    const v1, 0x7f081ee3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2541315
    iget-object v1, p0, LX/I8R;->e:LX/I91;

    iget-object v2, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v3, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-boolean v4, p0, LX/I8R;->k:Z

    invoke-virtual {v1, v0, v2, v3, v4}, LX/I91;->a(Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Z)V

    goto/16 :goto_0

    .line 2541316
    :pswitch_9
    check-cast p3, Landroid/widget/TextView;

    .line 2541317
    const v0, 0x7f081ee3

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2541318
    iget-object v0, p0, LX/I8R;->e:LX/I91;

    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-boolean v3, p0, LX/I8R;->k:Z

    invoke-virtual {v0, p3, v1, v2, v3}, LX/I91;->a(Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Z)V

    goto/16 :goto_0

    .line 2541319
    :pswitch_a
    check-cast p3, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {p0}, LX/I8R;->b()Z

    move-result v2

    invoke-virtual {p3, v0, v1, v2}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;Z)V

    goto/16 :goto_0

    .line 2541320
    :pswitch_b
    check-cast p3, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {p0}, LX/I8R;->b()Z

    move-result v2

    invoke-virtual {p3, v0, v1, v2}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;Z)V

    goto/16 :goto_0

    .line 2541321
    :pswitch_c
    check-cast p3, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p3, v0, v1}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541322
    iget-object v0, p0, LX/I8R;->m:LX/I88;

    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v3, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v7, p0, LX/I8R;->b:LX/I7i;

    iget-object v5, p0, LX/I8R;->c:LX/DBC;

    iget-object v8, p0, LX/I8R;->h:LX/0Px;

    sget-object v9, LX/I8Q;->TOXICLE_INVITED_BY_BAR:LX/I8Q;

    invoke-virtual {v8, v9}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, LX/I8R;->h:LX/0Px;

    sget-object v9, LX/I8Q;->TOXICLE_MESSAGE_INVITER_BAR:LX/I8Q;

    invoke-virtual {v8, v9}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-direct {p0}, LX/I8R;->b()Z

    move-result v8

    if-eqz v8, :cond_1

    move v6, v4

    :cond_1
    move-object v4, v7

    const/4 v8, 0x1

    .line 2541323
    iget-object v9, v0, LX/I88;->b:LX/AhO;

    if-nez v6, :cond_7

    move v7, v8

    :goto_1
    iget p0, v0, LX/I88;->n:I

    invoke-virtual {v9, v7, v8, p0}, LX/AhO;->a(ZZI)V

    .line 2541324
    iget-object v7, v0, LX/I88;->j:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/I7Z;

    .line 2541325
    iget-boolean v8, v1, Lcom/facebook/events/model/Event;->H:Z

    move v8, v8

    .line 2541326
    iget-object v9, v1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v9, v9

    .line 2541327
    const/4 v6, 0x2

    const/4 p1, 0x1

    .line 2541328
    if-eqz v8, :cond_8

    .line 2541329
    :cond_2
    :goto_2
    iget-object v7, v0, LX/I88;->d:LX/I8J;

    .line 2541330
    iput-object v3, v7, LX/I8J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2541331
    iput-object v4, v7, LX/I8J;->d:LX/I7i;

    .line 2541332
    iput-object v5, v7, LX/I8J;->e:LX/DBC;

    .line 2541333
    iput-object v1, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541334
    iget-object v7, v0, LX/I88;->d:LX/I8J;

    .line 2541335
    iput-object v1, v7, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2541336
    iput-object v2, v7, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2541337
    iget-object v8, v7, LX/I8J;->g:LX/DBA;

    iget-object v9, v7, LX/I8J;->a:Lcom/facebook/events/model/Event;

    iget-object p0, v7, LX/I8J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v8, v9, p0}, LX/DBA;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541338
    iget-object v8, v7, LX/I8J;->h:LX/I7w;

    iget-object v9, v7, LX/I8J;->a:Lcom/facebook/events/model/Event;

    iget-object p0, v7, LX/I8J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v8, v9, p0}, LX/I7w;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541339
    iget-object v8, v7, LX/I8J;->k:LX/DBE;

    iget-object v9, v7, LX/I8J;->a:Lcom/facebook/events/model/Event;

    iget-object p0, v7, LX/I8J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v8, v9, p0}, LX/DBE;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541340
    iget-object v8, v7, LX/I8J;->l:LX/DBH;

    iget-object v9, v7, LX/I8J;->a:Lcom/facebook/events/model/Event;

    iget-object p0, v7, LX/I8J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v8, v9, p0}, LX/DBH;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541341
    iget-object v8, v7, LX/I8J;->j:LX/I7n;

    .line 2541342
    iput-object v1, v8, LX/I7n;->g:Lcom/facebook/events/model/Event;

    .line 2541343
    iget-object v9, v8, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    if-eqz v9, :cond_3

    iget-object v9, v8, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v9}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2541344
    iget-object v9, v8, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v9}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2541345
    invoke-virtual {v8}, LX/I7n;->a()V

    .line 2541346
    :cond_3
    invoke-virtual {v7}, LX/I8J;->a()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2541347
    iget-object v8, v7, LX/I8J;->i:LX/I7z;

    iget-object v9, v7, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2541348
    iput-object v9, v8, LX/I7z;->a:Lcom/facebook/events/model/Event;

    .line 2541349
    iget-object p0, v9, Lcom/facebook/events/model/Event;->F:Ljava/lang/String;

    move-object p0, p0

    .line 2541350
    iput-object p0, v8, LX/I7z;->b:Ljava/lang/String;

    .line 2541351
    iget-object p0, v9, Lcom/facebook/events/model/Event;->G:Ljava/lang/String;

    move-object p0, p0

    .line 2541352
    iput-object p0, v8, LX/I7z;->c:Ljava/lang/String;

    .line 2541353
    :cond_4
    iget-object v8, v7, LX/I8J;->m:LX/I82;

    iget-object v9, v7, LX/I8J;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v8, v9}, LX/I82;->a(Lcom/facebook/events/model/Event;)LX/I81;

    move-result-object v8

    iput-object v8, v7, LX/I8J;->f:LX/I81;

    .line 2541354
    iget-object v7, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    if-nez v7, :cond_d

    .line 2541355
    :cond_5
    :goto_3
    goto/16 :goto_0

    .line 2541356
    :pswitch_d
    check-cast p3, LX/IBj;

    .line 2541357
    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v2, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-boolean v3, p0, LX/I8R;->n:Z

    .line 2541358
    iput-boolean v3, p3, LX/IBj;->i:Z

    .line 2541359
    iget-object v4, p3, LX/IBj;->b:LX/IBo;

    invoke-static {p3, v4, v0, v1, v2}, LX/IBj;->a(LX/IBj;Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541360
    iget-object v4, p3, LX/IBj;->c:LX/IBg;

    invoke-static {p3, v4, v0, v1, v2}, LX/IBj;->a(LX/IBj;Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541361
    iget-object v4, p3, LX/IBj;->d:LX/IBi;

    invoke-static {p3, v4, v0, v1, v2}, LX/IBj;->a(LX/IBj;Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541362
    iget-object v4, p3, LX/IBj;->e:LX/IBn;

    invoke-static {p3, v4, v0, v1, v2}, LX/IBj;->a(LX/IBj;Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541363
    goto :goto_4

    .line 2541364
    :goto_4
    iget-boolean v4, v0, Lcom/facebook/events/model/Event;->H:Z

    move v4, v4

    .line 2541365
    if-nez v4, :cond_6

    .line 2541366
    iget-object v4, v0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v4, v4

    .line 2541367
    if-eqz v4, :cond_6

    .line 2541368
    iget-object v4, v0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v4, v4

    .line 2541369
    iget-object v5, v4, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v4, v5

    .line 2541370
    if-nez v4, :cond_2e

    .line 2541371
    :cond_6
    iget-object v4, p3, LX/IBj;->f:LX/IBZ;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, LX/IBZ;->setVisibility(I)V

    .line 2541372
    :goto_5
    iget-object v4, p3, LX/IBj;->g:LX/IBY;

    invoke-static {p3, v4, v0, v1, v2}, LX/IBj;->a(LX/IBj;Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541373
    iget-object v4, p3, LX/IBj;->h:LX/IBh;

    invoke-static {p3, v4, v0, v1, v2}, LX/IBj;->a(LX/IBj;Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2541374
    goto/16 :goto_0

    .line 2541375
    :pswitch_e
    check-cast p3, Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    .line 2541376
    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v2, p0, LX/I8R;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {p3, v0, v1, v2}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V

    goto/16 :goto_0

    .line 2541377
    :pswitch_f
    check-cast p3, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8R;->c:LX/DBC;

    invoke-virtual {p3, v0, v1}, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a(Lcom/facebook/events/model/Event;LX/DBC;)V

    goto/16 :goto_0

    .line 2541378
    :pswitch_10
    check-cast p3, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    invoke-virtual {p3, v0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->a(Lcom/facebook/events/model/Event;)V

    goto/16 :goto_0

    .line 2541379
    :pswitch_11
    check-cast p3, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;

    .line 2541380
    const v0, 0x7f081ed7

    invoke-virtual {p3, v0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->setTitle(I)V

    .line 2541381
    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {p3, v0, v1}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)V

    goto/16 :goto_0

    .line 2541382
    :pswitch_12
    check-cast p3, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;

    .line 2541383
    iget-object v0, p3, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;->a:Landroid/widget/TextView;

    move-object v0, v0

    .line 2541384
    const v1, 0x7f081eeb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2541385
    iget-object v1, p0, LX/I8R;->f:LX/I98;

    iget-object v2, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v3, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-boolean v4, p0, LX/I8R;->k:Z

    invoke-virtual {v1, v0, v2, v3, v4}, LX/I98;->a(Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Z)V

    goto/16 :goto_0

    .line 2541386
    :pswitch_13
    check-cast p3, Landroid/widget/TextView;

    .line 2541387
    const v0, 0x7f081eeb

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2541388
    iget-object v0, p0, LX/I8R;->f:LX/I98;

    iget-object v1, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-boolean v3, p0, LX/I8R;->k:Z

    invoke-virtual {v0, p3, v1, v2, v3}, LX/I98;->a(Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Z)V

    goto/16 :goto_0

    .line 2541389
    :pswitch_14
    check-cast p3, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;

    iget-object v0, p0, LX/I8R;->j:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8R;->s:Ljava/lang/String;

    iget-object v2, p0, LX/I8R;->c:LX/DBC;

    invoke-virtual {p3, v0, v1, v2}, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;LX/DBC;)V

    goto/16 :goto_0

    .line 2541390
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 2541391
    :cond_8
    iget p0, v7, LX/I7Z;->a:I

    if-nez p0, :cond_a

    .line 2541392
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v9, p0, :cond_9

    .line 2541393
    iput p1, v7, LX/I7Z;->a:I

    goto/16 :goto_2

    .line 2541394
    :cond_9
    iput v6, v7, LX/I7Z;->a:I

    goto/16 :goto_2

    .line 2541395
    :cond_a
    iget p0, v7, LX/I7Z;->a:I

    if-ne p0, p1, :cond_c

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v9, p0, :cond_b

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v9, p0, :cond_c

    .line 2541396
    :cond_b
    iput v6, v7, LX/I7Z;->a:I

    goto/16 :goto_2

    .line 2541397
    :cond_c
    iget p0, v7, LX/I7Z;->a:I

    if-ne p0, v6, :cond_2

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v9, p0, :cond_2

    .line 2541398
    iput p1, v7, LX/I7Z;->a:I

    goto/16 :goto_2

    .line 2541399
    :cond_d
    new-instance v7, LX/I80;

    const/4 v8, 0x4

    invoke-direct {v7, v8}, LX/I80;-><init>(I)V

    .line 2541400
    iget-object v8, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    invoke-static {v8}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v8

    .line 2541401
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541402
    iget-boolean p0, v9, Lcom/facebook/events/model/Event;->z:Z

    move v9, p0

    .line 2541403
    if-eqz v9, :cond_1a

    .line 2541404
    sget-object v9, LX/I83;->EDIT:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    .line 2541405
    sget-object v9, LX/I83;->PUBLISH_EVENT:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    .line 2541406
    sget-object v9, LX/I83;->RESCHEDULE_EVENT:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    .line 2541407
    sget-object v9, LX/I83;->DUPLICATE_EVENT:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->b(LX/I83;)V

    .line 2541408
    sget-object v9, LX/I83;->REMOVE_SCHEDULE:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->b(LX/I83;)V

    .line 2541409
    :goto_6
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541410
    iget-boolean p0, v9, Lcom/facebook/events/model/Event;->z:Z

    move v9, p0

    .line 2541411
    if-nez v9, :cond_13

    .line 2541412
    if-nez v8, :cond_1e

    .line 2541413
    invoke-static {v0, v7}, LX/I88;->i(LX/I88;LX/I80;)V

    .line 2541414
    :goto_7
    if-nez v8, :cond_e

    .line 2541415
    iget-object v8, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v8}, Lcom/facebook/events/model/Event;->av()Z

    move-result v8

    .line 2541416
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541417
    iget-object p0, v9, Lcom/facebook/events/model/Event;->aa:Ljava/lang/String;

    move-object v9, p0

    .line 2541418
    if-eqz v9, :cond_e

    .line 2541419
    if-eqz v8, :cond_23

    .line 2541420
    sget-object v9, LX/I83;->PHOTOS:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    .line 2541421
    :cond_e
    :goto_8
    iget-object v8, v0, LX/I88;->d:LX/I8J;

    invoke-virtual {v8}, LX/I8J;->a()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 2541422
    iget-object v8, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541423
    iget-boolean v9, v8, Lcom/facebook/events/model/Event;->E:Z

    move v8, v9

    .line 2541424
    if-eqz v8, :cond_24

    sget-object v8, LX/I83;->SAVED:LX/I83;

    :goto_9
    invoke-virtual {v7, v8}, LX/I80;->b(LX/I83;)V

    .line 2541425
    :cond_f
    iget-object v8, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541426
    iget-object v9, v8, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v8, v9

    .line 2541427
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eq v8, v9, :cond_10

    iget-object v8, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541428
    iget-object v9, v8, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v8, v9

    .line 2541429
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v8, v9, :cond_11

    iget-object v8, v0, LX/I88;->k:LX/0ad;

    sget-char v9, LX/347;->u:C

    const-string p0, ""

    invoke-interface {v8, v9, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "send"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_11

    iget-object v8, v0, LX/I88;->k:LX/0ad;

    sget-char v9, LX/347;->u:C

    const-string p0, ""

    invoke-interface {v8, v9, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "share"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_11

    .line 2541430
    :cond_10
    sget-object v8, LX/I83;->COPY_LINK:LX/I83;

    invoke-virtual {v7, v8}, LX/I80;->b(LX/I83;)V

    .line 2541431
    :cond_11
    iget-object v8, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v8}, Lcom/facebook/events/model/Event;->c()Z

    .line 2541432
    sget-object v8, LX/I83;->NOTIFICATION_SETTINGS:LX/I83;

    invoke-virtual {v7, v8}, LX/I80;->b(LX/I83;)V

    .line 2541433
    iget-object v8, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v9, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v8, v9}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v8

    if-nez v8, :cond_25

    .line 2541434
    sget-object v8, LX/I83;->REPORT_EVENT:LX/I83;

    invoke-virtual {v7, v8}, LX/I80;->b(LX/I83;)V

    .line 2541435
    :cond_12
    :goto_a
    sget-object v8, LX/I83;->CREATE_EVENT:LX/I83;

    invoke-virtual {v7, v8}, LX/I80;->b(LX/I83;)V

    .line 2541436
    :cond_13
    iget-object v8, v0, LX/I88;->b:LX/AhO;

    .line 2541437
    iget v9, v7, LX/I80;->d:I

    add-int/lit8 v9, v9, 0x1

    iget p0, v7, LX/I80;->c:I

    invoke-static {v9, p0}, Ljava/lang/Math;->min(II)I

    move-result v9

    move v9, v9

    .line 2541438
    invoke-virtual {v8, v9}, LX/AhO;->setMaxNumOfVisibleButtons(I)V

    .line 2541439
    invoke-virtual {v7}, LX/I80;->iterator()Ljava/util/Iterator;

    move-result-object v7

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Iterator;)LX/0Px;

    move-result-object v7

    .line 2541440
    iget-object v8, v0, LX/I88;->e:LX/0Px;

    invoke-virtual {v7, v8}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 2541441
    iput-object v7, v0, LX/I88;->e:LX/0Px;

    .line 2541442
    const/4 p0, 0x0

    .line 2541443
    iget-object v7, v0, LX/I88;->b:LX/AhO;

    invoke-virtual {v7}, LX/AhO;->b()V

    .line 2541444
    iget-object v7, v0, LX/I88;->b:LX/AhO;

    invoke-virtual {v7}, LX/AhO;->clear()V

    .line 2541445
    iget-object v7, v0, LX/I88;->e:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p1

    move v9, p0

    :goto_b
    if-ge v9, p1, :cond_19

    iget-object v7, v0, LX/I88;->e:LX/0Px;

    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/I83;

    .line 2541446
    invoke-virtual {v7}, LX/I83;->getIconResId()I

    move-result v8

    .line 2541447
    iget-object v3, v0, LX/I88;->k:LX/0ad;

    sget-short v4, LX/347;->N:S

    invoke-interface {v3, v4, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 2541448
    const v3, 0x7f020dcb

    if-ne v8, v3, :cond_14

    .line 2541449
    const v8, 0x7f020dcc

    .line 2541450
    :cond_14
    const v3, 0x7f020850

    if-ne v8, v3, :cond_15

    .line 2541451
    const v8, 0x7f020886

    .line 2541452
    :cond_15
    iget-object v3, v0, LX/I88;->b:LX/AhO;

    invoke-virtual {v7}, LX/I83;->ordinal()I

    move-result v4

    invoke-virtual {v7}, LX/I83;->getTitleResId()I

    move-result v1

    invoke-virtual {v3, p0, v4, p0, v1}, LX/AhO;->a(IIII)LX/3qv;

    move-result-object v3

    .line 2541453
    sget-object v4, LX/I83;->CANCELLED:LX/I83;

    if-eq v7, v4, :cond_26

    const/4 v4, 0x1

    :goto_c
    move v4, v4

    .line 2541454
    invoke-interface {v3, v4}, LX/3qv;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v3

    const/4 v4, 0x0

    .line 2541455
    sget-object v1, LX/I85;->a:[I

    invoke-virtual {v7}, LX/I83;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 2541456
    :goto_d
    :pswitch_15
    move v4, v4

    .line 2541457
    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v8

    .line 2541458
    invoke-virtual {v7}, LX/I83;->isOverflow()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 2541459
    invoke-static {v8, p0}, LX/3rl;->a(Landroid/view/MenuItem;I)Z

    .line 2541460
    :goto_e
    sget-object v3, LX/I83;->SAVE:LX/I83;

    if-ne v7, v3, :cond_16

    .line 2541461
    iget-object v3, v0, LX/I88;->l:LX/I86;

    .line 2541462
    iput-object v8, v3, LX/I86;->a:Landroid/view/MenuItem;

    .line 2541463
    :cond_16
    sget-object v3, LX/I83;->SAVED:LX/I83;

    if-ne v7, v3, :cond_17

    .line 2541464
    iget-object v7, v0, LX/I88;->m:LX/I87;

    .line 2541465
    iput-object v8, v7, LX/I87;->a:Landroid/view/MenuItem;

    .line 2541466
    :cond_17
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    goto :goto_b

    .line 2541467
    :cond_18
    const/4 v3, 0x2

    invoke-static {v8, v3}, LX/3rl;->a(Landroid/view/MenuItem;I)Z

    goto :goto_e

    .line 2541468
    :cond_19
    iget-object v7, v0, LX/I88;->b:LX/AhO;

    invoke-virtual {v7}, LX/AhO;->d()V

    .line 2541469
    iget-object v7, v0, LX/I88;->b:LX/AhO;

    invoke-virtual {v7, p0}, LX/AhO;->setVisibility(I)V

    .line 2541470
    goto/16 :goto_3

    .line 2541471
    :cond_1a
    if-eqz v8, :cond_1b

    .line 2541472
    invoke-static {v0, v7}, LX/I88;->b(LX/I88;LX/I80;)V

    goto/16 :goto_6

    .line 2541473
    :cond_1b
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541474
    iget-boolean p0, v9, Lcom/facebook/events/model/Event;->B:Z

    move v9, p0

    .line 2541475
    if-eqz v9, :cond_29

    .line 2541476
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object p0, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v9, p0}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v9

    if-eqz v9, :cond_1c

    .line 2541477
    sget-object v9, LX/I83;->EDIT_OVERFLOW:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->b(LX/I83;)V

    .line 2541478
    :cond_1c
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541479
    iget-boolean p0, v9, Lcom/facebook/events/model/Event;->H:Z

    move v9, p0

    .line 2541480
    if-eqz v9, :cond_27

    .line 2541481
    sget-object v9, LX/I83;->TOXICLE_PRIVATE_GOING:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    .line 2541482
    sget-object v9, LX/I83;->TOXICLE_PRIVATE_MAYBE:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    .line 2541483
    sget-object v9, LX/I83;->TOXICLE_PRIVATE_CANT_GO:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    .line 2541484
    :cond_1d
    :goto_f
    goto/16 :goto_6

    .line 2541485
    :cond_1e
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541486
    iget-object p0, v9, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v9, p0

    .line 2541487
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    if-ne v9, p0, :cond_1f

    .line 2541488
    invoke-static {v7}, LX/I88;->h(LX/I80;)V

    goto/16 :goto_7

    .line 2541489
    :cond_1f
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541490
    iget-object p0, v9, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v9, p0

    .line 2541491
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    if-ne v9, p0, :cond_20

    .line 2541492
    invoke-static {v7}, LX/I88;->g(LX/I80;)V

    goto/16 :goto_7

    .line 2541493
    :cond_20
    iget-object v9, v0, LX/I88;->k:LX/0ad;

    sget-char p0, LX/347;->u:C

    const-string p1, ""

    invoke-interface {v9, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string p0, "invite"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_22

    .line 2541494
    iget-object v9, v0, LX/I88;->k:LX/0ad;

    sget-char p0, LX/347;->u:C

    const-string p1, ""

    invoke-interface {v9, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string p0, "send"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_21

    .line 2541495
    invoke-static {v7}, LX/I88;->h(LX/I80;)V

    goto/16 :goto_7

    .line 2541496
    :cond_21
    iget-object v9, v0, LX/I88;->k:LX/0ad;

    sget-char p0, LX/347;->u:C

    const-string p1, ""

    invoke-interface {v9, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string p0, "share"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_22

    .line 2541497
    invoke-static {v7}, LX/I88;->g(LX/I80;)V

    goto/16 :goto_7

    .line 2541498
    :cond_22
    invoke-static {v0, v7}, LX/I88;->i(LX/I88;LX/I80;)V

    goto/16 :goto_7

    .line 2541499
    :cond_23
    sget-object v9, LX/I83;->PHOTOS:LX/I83;

    sget-object p0, LX/I83;->PHOTOS_OVERFLOW:LX/I83;

    invoke-virtual {v7, v9, p0}, LX/I80;->a(LX/I83;LX/I83;)V

    goto/16 :goto_8

    .line 2541500
    :cond_24
    sget-object v8, LX/I83;->SAVE:LX/I83;

    goto/16 :goto_9

    .line 2541501
    :cond_25
    iget-object v8, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v8}, Lcom/facebook/events/model/Event;->ag()Ljava/util/EnumSet;

    move-result-object v8

    sget-object v9, LX/7vK;->CREATE_REPEAT_EVENT:LX/7vK;

    invoke-virtual {v8, v9}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    iget-object v8, v0, LX/I88;->k:LX/0ad;

    sget-short v9, LX/347;->n:S

    const/4 p0, 0x0

    invoke-interface {v8, v9, p0}, LX/0ad;->a(SZ)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 2541502
    sget-object v8, LX/I83;->DUPLICATE_EVENT:LX/I83;

    invoke-virtual {v7, v8}, LX/I80;->b(LX/I83;)V

    goto/16 :goto_a

    :cond_26
    const/4 v4, 0x0

    goto/16 :goto_c

    .line 2541503
    :pswitch_16
    const/4 v4, 0x1

    goto/16 :goto_d

    .line 2541504
    :cond_27
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v9}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v9

    .line 2541505
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v9, p0, :cond_2b

    .line 2541506
    sget-object p0, LX/I83;->TOXICLE_PRIVATE_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    .line 2541507
    :goto_10
    move-object p0, p0

    .line 2541508
    if-eqz p0, :cond_28

    .line 2541509
    invoke-virtual {v7, p0}, LX/I80;->a(LX/I83;)V

    .line 2541510
    :cond_28
    goto/16 :goto_f

    .line 2541511
    :cond_29
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object p0, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v9, p0}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v9

    if-eqz v9, :cond_2a

    .line 2541512
    sget-object v9, LX/I83;->EDIT:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    goto/16 :goto_f

    .line 2541513
    :cond_2a
    iget-object v9, v0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2541514
    iget-boolean p0, v9, Lcom/facebook/events/model/Event;->y:Z

    move v9, p0

    .line 2541515
    if-eqz v9, :cond_1d

    .line 2541516
    sget-object v9, LX/I83;->CANCELLED:LX/I83;

    invoke-virtual {v7, v9}, LX/I80;->a(LX/I83;)V

    goto/16 :goto_f

    .line 2541517
    :cond_2b
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v9, p0, :cond_2c

    .line 2541518
    sget-object p0, LX/I83;->TOXICLE_PRIVATE_MAYBE_SELECTED_WITH_CHEVRON:LX/I83;

    goto :goto_10

    .line 2541519
    :cond_2c
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v9, p0, :cond_2d

    .line 2541520
    sget-object p0, LX/I83;->TOXICLE_PRIVATE_CANT_GO_SELECTED_WITH_CHEVRON:LX/I83;

    goto :goto_10

    .line 2541521
    :cond_2d
    const/4 p0, 0x0

    goto :goto_10

    .line 2541522
    :cond_2e
    iget-object v4, p3, LX/IBj;->f:LX/IBZ;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/IBZ;->setVisibility(I)V

    .line 2541523
    iget-object v4, p3, LX/IBj;->f:LX/IBZ;

    invoke-static {p3, v4, v0, v1, v2}, LX/IBj;->a(LX/IBj;Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_8
        :pswitch_5
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_c
        :pswitch_d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
    .end packed-switch
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2541132
    iget-object v0, p0, LX/I8R;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2541131
    iget-object v0, p0, LX/I8R;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2541130
    iget-object v0, p0, LX/I8R;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8Q;

    invoke-virtual {v0}, LX/I8Q;->ordinal()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2541128
    iget-object v0, p0, LX/I8R;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8Q;

    invoke-virtual {v0}, LX/I8Q;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2541129
    invoke-static {}, LX/I8Q;->values()[LX/I8Q;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
