.class public final LX/HXb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/HXi;


# direct methods
.method public constructor <init>(LX/HXi;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2479132
    iput-object p1, p0, LX/HXb;->c:LX/HXi;

    iput-object p2, p0, LX/HXb;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/HXb;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    .line 2479133
    iget-object v0, p0, LX/HXb;->c:LX/HXi;

    iget-object v1, p0, LX/HXb;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/HXb;->b:Landroid/view/View;

    .line 2479134
    iget-object v4, v0, LX/HXi;->k:LX/9XE;

    const-string v5, "pages_native_timeline"

    sget-object v6, LX/9X8;->EVENT_ADMIN_UNPIN_STORY:LX/9X8;

    iget-object v3, v0, LX/D2z;->a:LX/5SB;

    .line 2479135
    iget-wide v10, v3, LX/5SB;->b:J

    move-wide v7, v10

    .line 2479136
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, LX/9XE;->a(Ljava/lang/String;LX/9X2;JLjava/lang/String;)V

    .line 2479137
    new-instance v4, LX/4At;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f08183f

    invoke-direct {v4, v3, v5}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2479138
    invoke-virtual {v4}, LX/4At;->beginShowingProgress()V

    .line 2479139
    new-instance v3, LX/HXm;

    invoke-direct {v3}, LX/HXm;-><init>()V

    move-object v5, v3

    .line 2479140
    new-instance v3, LX/4Hv;

    invoke-direct {v3}, LX/4Hv;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    .line 2479141
    const-string v7, "story_id"

    invoke-virtual {v3, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479142
    move-object v6, v3

    .line 2479143
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    .line 2479144
    const-string v7, "actor_id"

    invoke-virtual {v6, v7, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479145
    move-object v3, v6

    .line 2479146
    iget-object v6, v5, LX/0gW;->h:Ljava/lang/String;

    move-object v6, v6

    .line 2479147
    const-string v7, "client_mutation_id"

    invoke-virtual {v3, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479148
    move-object v3, v3

    .line 2479149
    const-string v6, "input"

    invoke-virtual {v5, v6, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2479150
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2479151
    iget-object v5, v0, LX/HXi;->l:LX/0Sh;

    iget-object v6, v0, LX/HXi;->j:LX/0tX;

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v6, LX/HXe;

    invoke-direct {v6, v0, v1, v4, v2}, LX/HXe;-><init>(LX/HXi;Lcom/facebook/graphql/model/GraphQLStory;LX/4At;Landroid/view/View;)V

    invoke-virtual {v5, v3, v6}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2479152
    const/4 v0, 0x1

    return v0
.end method
