.class public LX/JOy;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JOz;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JOy",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JOz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2688594
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2688595
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JOy;->b:LX/0Zi;

    .line 2688596
    iput-object p1, p0, LX/JOy;->a:LX/0Ot;

    .line 2688597
    return-void
.end method

.method public static a(LX/0QB;)LX/JOy;
    .locals 4

    .prologue
    .line 2688583
    const-class v1, LX/JOy;

    monitor-enter v1

    .line 2688584
    :try_start_0
    sget-object v0, LX/JOy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688585
    sput-object v2, LX/JOy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688586
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688587
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688588
    new-instance v3, LX/JOy;

    const/16 p0, 0x1fd2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JOy;-><init>(LX/0Ot;)V

    .line 2688589
    move-object v0, v3

    .line 2688590
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688591
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688592
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688593
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2688568
    check-cast p2, LX/JOx;

    .line 2688569
    iget-object v0, p0, LX/JOy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JOz;

    iget-object v1, p2, LX/JOx;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688570
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->o()I

    move-result v4

    .line 2688571
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->p()D

    move-result-wide v5

    .line 2688572
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v3, ""

    .line 2688573
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    .line 2688574
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b258c

    invoke-interface {v8, v9}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v8

    invoke-static {v0, p1, v4, v5, v6}, LX/JOz;->a(LX/JOz;LX/1De;ID)LX/1Dh;

    move-result-object v4

    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 v9, 0x1

    .line 2688575
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    const v8, 0x7f0b2571

    invoke-interface {v5, v6, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b257b

    invoke-interface {v5, v9, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v8, 0x7f0a010e

    invoke-virtual {v6, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    const v8, 0x7f0b258a

    invoke-virtual {v6, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    move-object v3, v5

    .line 2688576
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/4 v8, 0x1

    .line 2688577
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    const v6, 0x7f0b2571

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b258a

    invoke-interface {v4, v8, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JOz;->c:LX/3AZ;

    invoke-virtual {v5, p1}, LX/3AZ;->c(LX/1De;)LX/3Aa;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/3Aa;->b(Z)LX/3Aa;

    move-result-object v5

    invoke-static {v7}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/3Ab;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/3Aa;->a(LX/3Ab;)LX/3Aa;

    move-result-object v5

    const v6, 0x7f0a010c

    invoke-virtual {v5, v6}, LX/3Aa;->j(I)LX/3Aa;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, LX/3Aa;->h(I)LX/3Aa;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/3Aa;->a(Landroid/text/TextUtils$TruncateAt;)LX/3Aa;

    move-result-object v5

    const v6, 0x7f0b2582

    invoke-virtual {v5, v6}, LX/3Aa;->n(I)LX/3Aa;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    move-object v4, v4

    .line 2688578
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2688579
    const v4, 0x62b005e0

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2688580
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2688581
    return-object v0

    .line 2688582
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2688546
    invoke-static {}, LX/1dS;->b()V

    .line 2688547
    iget v0, p1, LX/1dQ;->b:I

    .line 2688548
    packed-switch v0, :pswitch_data_0

    .line 2688549
    :goto_0
    return-object v2

    .line 2688550
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2688551
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2688552
    check-cast v1, LX/JOx;

    .line 2688553
    iget-object v3, p0, LX/JOy;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JOz;

    iget-object v4, v1, LX/JOx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/JOx;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688554
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p1

    .line 2688555
    if-nez p1, :cond_2

    if-eqz v4, :cond_2

    .line 2688556
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 2688557
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p1

    move-object p2, p1

    .line 2688558
    :goto_1
    if-nez p2, :cond_0

    .line 2688559
    iget-object p1, v3, LX/JOz;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JPN;

    const-string p2, "Cannot open profile without GraphQLPage"

    invoke-virtual {p1, v4, v5, p2}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2688560
    :goto_2
    goto :goto_0

    .line 2688561
    :cond_0
    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p1

    if-nez p1, :cond_1

    .line 2688562
    iget-object p1, v3, LX/JOz;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JPN;

    const-string p2, "Cannot open profile LinkifyTargetBuilder.getLinkifyTarget(page) is null"

    invoke-virtual {p1, v4, v5, p2}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_2

    .line 2688563
    :cond_1
    iget-object p1, v3, LX/JOz;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JPN;

    invoke-virtual {p1, v4, v5}, LX/JPN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    .line 2688564
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2688565
    const-string p0, "extra_is_admin"

    const/4 v1, 0x1

    invoke-virtual {p1, p0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2688566
    const-string p0, "extra_page_tab"

    sget-object v1, LX/BEQ;->INSIGHTS:LX/BEQ;

    invoke-virtual {p1, p0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2688567
    iget-object p0, v3, LX/JOz;->a:LX/1nA;

    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p2

    invoke-virtual {p0, v0, p2, p1}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    goto :goto_2

    :cond_2
    move-object p2, p1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x62b005e0
        :pswitch_0
    .end packed-switch
.end method
