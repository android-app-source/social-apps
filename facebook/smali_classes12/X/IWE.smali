.class public LX/IWE;
.super LX/DK1;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0gc;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 2583540
    invoke-direct {p0, p1}, LX/DK1;-><init>(LX/0gc;)V

    .line 2583541
    iput-object p2, p0, LX/IWE;->b:Ljava/lang/String;

    .line 2583542
    iput-object p3, p0, LX/IWE;->c:Ljava/lang/String;

    .line 2583543
    iput-object p4, p0, LX/IWE;->a:Landroid/content/res/Resources;

    .line 2583544
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2583545
    packed-switch p1, :pswitch_data_0

    .line 2583546
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2583547
    :pswitch_0
    iget-object v0, p0, LX/IWE;->a:Landroid/content/res/Resources;

    const v1, 0x7f083895

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2583548
    :pswitch_1
    iget-object v0, p0, LX/IWE;->a:Landroid/content/res/Resources;

    const v1, 0x7f083896

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 2583549
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2583550
    const-string v0, "group_feed_id"

    iget-object v2, p0, LX/IWE;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2583551
    const-string v0, "group_name"

    iget-object v2, p0, LX/IWE;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2583552
    packed-switch p1, :pswitch_data_0

    .line 2583553
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2583554
    :pswitch_0
    new-instance v0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;

    invoke-direct {v0}, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;-><init>()V

    .line 2583555
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2583556
    const-string v2, "pandora_instance_id"

    new-instance v3, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    iget-object v4, p0, LX/IWE;->b:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 2583557
    :pswitch_1
    new-instance v0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;

    invoke-direct {v0}, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;-><init>()V

    .line 2583558
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2583559
    const/4 v0, 0x2

    return v0
.end method
