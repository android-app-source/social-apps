.class public LX/Izp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile e:LX/Izp;


# instance fields
.field private final b:LX/6Oo;

.field private final c:LX/03V;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2635688
    const-class v0, LX/Izp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Izp;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6Oo;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2635683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635684
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Izp;->d:Ljava/util/Map;

    .line 2635685
    iput-object p1, p0, LX/Izp;->b:LX/6Oo;

    .line 2635686
    iput-object p2, p0, LX/Izp;->c:LX/03V;

    .line 2635687
    return-void
.end method

.method public static a(LX/DtK;)LX/Izo;
    .locals 6

    .prologue
    .line 2635615
    sget-object v0, LX/Izn;->a:[I

    invoke-virtual {p0}, LX/DtK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2635616
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown transaction query type encountered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2635617
    :pswitch_0
    const-string v1, "recent_all_transactions"

    .line 2635618
    sget-object v0, LX/IzT;->b:LX/0U1;

    .line 2635619
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2635620
    sget-object v0, LX/IzT;->a:LX/0U1;

    .line 2635621
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2635622
    sget-object v4, LX/IzL;->a:LX/IzK;

    .line 2635623
    :goto_0
    new-instance v0, LX/Izo;

    invoke-direct/range {v0 .. v4}, LX/Izo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/IzK;)V

    return-object v0

    .line 2635624
    :pswitch_1
    const-string v1, "recent_incoming_transactions"

    .line 2635625
    sget-object v0, LX/IzU;->b:LX/0U1;

    .line 2635626
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2635627
    sget-object v0, LX/IzU;->a:LX/0U1;

    .line 2635628
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2635629
    sget-object v4, LX/IzL;->b:LX/IzK;

    goto :goto_0

    .line 2635630
    :pswitch_2
    const-string v1, "recent_outgoing_transactions"

    .line 2635631
    sget-object v0, LX/IzV;->b:LX/0U1;

    .line 2635632
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2635633
    sget-object v0, LX/IzV;->a:LX/0U1;

    .line 2635634
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2635635
    sget-object v4, LX/IzL;->c:LX/IzK;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/Izp;
    .locals 5

    .prologue
    .line 2635670
    sget-object v0, LX/Izp;->e:LX/Izp;

    if-nez v0, :cond_1

    .line 2635671
    const-class v1, LX/Izp;

    monitor-enter v1

    .line 2635672
    :try_start_0
    sget-object v0, LX/Izp;->e:LX/Izp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2635673
    if-eqz v2, :cond_0

    .line 2635674
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2635675
    new-instance p0, LX/Izp;

    invoke-static {v0}, LX/6Oo;->b(LX/0QB;)LX/6Oo;

    move-result-object v3

    check-cast v3, LX/6Oo;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/Izp;-><init>(LX/6Oo;LX/03V;)V

    .line 2635676
    move-object v0, p0

    .line 2635677
    sput-object v0, LX/Izp;->e:LX/Izp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635678
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2635679
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635680
    :cond_1
    sget-object v0, LX/Izp;->e:LX/Izp;

    return-object v0

    .line 2635681
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2635682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2635657
    invoke-virtual {p0, p1}, LX/Izp;->b(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    .line 2635658
    if-nez v0, :cond_0

    .line 2635659
    const/4 v0, 0x0

    .line 2635660
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/Dto;

    invoke-direct {v1}, LX/Dto;-><init>()V

    .line 2635661
    iput-object p1, v1, LX/Dto;->b:Ljava/lang/String;

    .line 2635662
    move-object v1, v1

    .line 2635663
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v2

    .line 2635664
    iput-boolean v2, v1, LX/Dto;->c:Z

    .line 2635665
    move-object v1, v1

    .line 2635666
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    .line 2635667
    iput-object v0, v1, LX/Dto;->d:Ljava/lang/String;

    .line 2635668
    move-object v0, v1

    .line 2635669
    invoke-virtual {v0}, LX/Dto;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2635641
    new-instance v3, LX/0cA;

    invoke-direct {v3}, LX/0cA;-><init>()V

    .line 2635642
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2635643
    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2635644
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2635645
    :cond_0
    iget-object v0, p0, LX/Izp;->b:LX/6Oo;

    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    sget-object v3, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v0, v2, v3}, LX/6Oo;->a(LX/0Rf;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2635646
    const-wide/16 v2, 0xa

    :try_start_0
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const v5, 0x66fa1206

    invoke-static {v0, v2, v3, v4, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2635647
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/Contact;

    .line 2635648
    iget-object v4, p0, LX/Izp;->d:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2635649
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2635650
    :catch_0
    move-exception v0

    .line 2635651
    iget-object v1, p0, LX/Izp;->c:LX/03V;

    sget-object v2, LX/Izp;->a:Ljava/lang/String;

    const-string v3, "InterruptedException raised while waiting for contact fetching futures to return."

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2635652
    :cond_1
    :goto_2
    return-void

    .line 2635653
    :catch_1
    move-exception v0

    .line 2635654
    iget-object v1, p0, LX/Izp;->c:LX/03V;

    sget-object v2, LX/Izp;->a:Ljava/lang/String;

    const-string v3, "ExecutionException raised while waiting for contact fetching futures to return."

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2635655
    :catch_2
    move-exception v0

    .line 2635656
    iget-object v1, p0, LX/Izp;->c:LX/03V;

    sget-object v2, LX/Izp;->a:Ljava/lang/String;

    const-string v3, "TimeoutException raised while waiting for contact fetching futures to return."

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2635636
    iget-object v0, p0, LX/Izp;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2635637
    iget-object v0, p0, LX/Izp;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2635638
    :goto_0
    return-object v0

    .line 2635639
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Izp;->a(LX/0Px;)V

    .line 2635640
    iget-object v0, p0, LX/Izp;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    goto :goto_0
.end method
