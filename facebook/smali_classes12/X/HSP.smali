.class public final LX/HSP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V
    .locals 0

    .prologue
    .line 2467017
    iput-object p1, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0xbcc3815

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2467000
    iget-object v0, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->u:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    if-ne v0, v1, :cond_0

    .line 2467001
    const-string v2, "LOCAL"

    .line 2467002
    iget-object v0, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->LOCAL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 2467003
    iput-object v1, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->u:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 2467004
    :goto_0
    iget-object v0, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->g:LX/7va;

    iget-object v1, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-wide v4, v1, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->r:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PAGE_EVENT_LIST_SUBSCRIBE_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    iget-object v4, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->y:Lcom/facebook/events/common/EventAnalyticsParams;

    const/4 v5, 0x0

    .line 2467005
    new-instance v7, LX/4Il;

    invoke-direct {v7}, LX/4Il;-><init>()V

    iget-object v8, v4, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v9, v4, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    iget-object p1, v4, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-static {v3, v8, v9, p1, v5}, LX/7va;->a(Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/4EL;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4Il;->a(LX/4EL;)LX/4Il;

    move-result-object v7

    invoke-virtual {v7, v1}, LX/4Il;->a(Ljava/lang/String;)LX/4Il;

    move-result-object v7

    invoke-virtual {v7, v2}, LX/4Il;->b(Ljava/lang/String;)LX/4Il;

    move-result-object v7

    .line 2467006
    invoke-static {}, LX/7uR;->c()LX/7uB;

    move-result-object v8

    .line 2467007
    const-string v9, "input"

    invoke-virtual {v8, v9, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2467008
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2467009
    iget-object v8, v0, LX/7va;->a:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2467010
    iget-object v0, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    .line 2467011
    iget-object v0, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->f:LX/CXj;

    new-instance v1, LX/CXl;

    iget-object v2, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-wide v2, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->r:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->u:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    invoke-direct {v1, v2, v3}, LX/CXl;-><init>(Ljava/lang/Long;Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2467012
    const v0, 0x2c055a7b

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2467013
    :cond_0
    const-string v2, "NONE"

    .line 2467014
    iget-object v0, p0, LX/HSP;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 2467015
    iput-object v1, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->u:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 2467016
    goto :goto_0
.end method
