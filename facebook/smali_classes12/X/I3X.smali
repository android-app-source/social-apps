.class public final enum LX/I3X;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I3X;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I3X;

.field public static final enum FETCH_EVENTS_SUGGESTIONS_CUTS:LX/I3X;

.field public static final enum FETCH_EVENTS_SUGGESTIONS_FOR_CUT:LX/I3X;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2531487
    new-instance v0, LX/I3X;

    const-string v1, "FETCH_EVENTS_SUGGESTIONS_CUTS"

    invoke-direct {v0, v1, v2}, LX/I3X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I3X;->FETCH_EVENTS_SUGGESTIONS_CUTS:LX/I3X;

    .line 2531488
    new-instance v0, LX/I3X;

    const-string v1, "FETCH_EVENTS_SUGGESTIONS_FOR_CUT"

    invoke-direct {v0, v1, v3}, LX/I3X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I3X;->FETCH_EVENTS_SUGGESTIONS_FOR_CUT:LX/I3X;

    .line 2531489
    const/4 v0, 0x2

    new-array v0, v0, [LX/I3X;

    sget-object v1, LX/I3X;->FETCH_EVENTS_SUGGESTIONS_CUTS:LX/I3X;

    aput-object v1, v0, v2

    sget-object v1, LX/I3X;->FETCH_EVENTS_SUGGESTIONS_FOR_CUT:LX/I3X;

    aput-object v1, v0, v3

    sput-object v0, LX/I3X;->$VALUES:[LX/I3X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2531484
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I3X;
    .locals 1

    .prologue
    .line 2531486
    const-class v0, LX/I3X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I3X;

    return-object v0
.end method

.method public static values()[LX/I3X;
    .locals 1

    .prologue
    .line 2531485
    sget-object v0, LX/I3X;->$VALUES:[LX/I3X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I3X;

    return-object v0
.end method
