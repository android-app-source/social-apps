.class public LX/JRF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JRF",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692939
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2692940
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JRF;->b:LX/0Zi;

    .line 2692941
    iput-object p1, p0, LX/JRF;->a:LX/0Ot;

    .line 2692942
    return-void
.end method

.method public static a(LX/0QB;)LX/JRF;
    .locals 4

    .prologue
    .line 2692943
    const-class v1, LX/JRF;

    monitor-enter v1

    .line 2692944
    :try_start_0
    sget-object v0, LX/JRF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692945
    sput-object v2, LX/JRF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692946
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692947
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692948
    new-instance v3, LX/JRF;

    const/16 p0, 0x2023

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JRF;-><init>(LX/0Ot;)V

    .line 2692949
    move-object v0, v3

    .line 2692950
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692951
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692952
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692953
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2692954
    check-cast p2, LX/JRE;

    .line 2692955
    iget-object v0, p0, LX/JRF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;

    iget-object v1, p2, LX/JRE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JRE;->b:LX/1Pk;

    const/4 v7, 0x1

    .line 2692956
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692957
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 2692958
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    .line 2692959
    iget-object v4, v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->b:LX/JRM;

    invoke-virtual {v4, p1}, LX/JRM;->c(LX/1De;)LX/JRK;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/JRK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JRK;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/JRK;->a(LX/1Pk;)LX/JRK;

    move-result-object v6

    .line 2692960
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2692961
    check-cast v4, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 2692962
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v4, v2

    .line 2692963
    invoke-virtual {v6, v4}, LX/JRK;->b(Ljava/lang/String;)LX/JRK;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    .line 2692964
    iget-object v4, v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->c:LX/8yV;

    invoke-virtual {v4, p1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v4

    sget-object v6, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v6}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v4

    .line 2692965
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2692966
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->p()LX/0Px;

    move-result-object v6

    if-nez v6, :cond_1

    move-object v6, v8

    .line 2692967
    :goto_1
    move-object v6, v6

    .line 2692968
    invoke-virtual {v4, v6}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v4

    const v6, 0x7f0b25a5

    invoke-virtual {v4, v6}, LX/8yU;->h(I)LX/8yU;

    move-result-object v4

    const v6, 0x7f0b25a6

    invoke-virtual {v4, v6}, LX/8yU;->l(I)LX/8yU;

    move-result-object v4

    const v6, 0x7f0b25a7

    invoke-virtual {v4, v6}, LX/8yU;->k(I)LX/8yU;

    move-result-object v4

    const/4 v6, 0x5

    invoke-virtual {v4, v6}, LX/8yU;->m(I)LX/8yU;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/8yU;->a(Z)LX/8yU;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v4, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    const v6, 0x7f0b25a8

    invoke-interface {v4, v7, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2692969
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    .line 2692970
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    :goto_2
    move-object v3, v6

    .line 2692971
    invoke-virtual {v4, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00d6

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b25a8

    invoke-interface {v3, v7, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const v6, 0x7f0b010f

    invoke-interface {v3, v4, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2692972
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a098d

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b25a7

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x4

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b25a9

    invoke-interface {v3, v7, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2692973
    iget-object v3, v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->d:LX/JRI;

    invoke-virtual {v3, p1}, LX/JRI;->c(LX/1De;)LX/JRG;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/JRG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JRG;

    move-result-object v4

    .line 2692974
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692975
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 2692976
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    :goto_3
    move-object v3, v6

    .line 2692977
    invoke-virtual {v4, v3}, LX/JRG;->b(Ljava/lang/String;)LX/JRG;

    move-result-object v4

    iget-object v6, v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->e:LX/JRC;

    .line 2692978
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692979
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 2692980
    iget-object v7, v6, LX/JRC;->a:LX/23i;

    invoke-virtual {v7}, LX/23i;->f()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    .line 2692981
    :goto_4
    if-eqz v7, :cond_7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    :goto_5
    move-object v3, v7

    .line 2692982
    invoke-virtual {v4, v3}, LX/JRG;->c(Ljava/lang/String;)LX/JRG;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    .line 2692983
    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2692984
    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2692985
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->p()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p2

    const/4 v6, 0x0

    move v9, v6

    :goto_6
    if-ge v9, p2, :cond_3

    invoke-virtual {p0, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLUser;

    .line 2692986
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2692987
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2692988
    :cond_2
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    goto :goto_6

    :cond_3
    move-object v6, v8

    .line 2692989
    goto/16 :goto_1

    :cond_4
    const/4 v6, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v6, 0x0

    goto :goto_3

    .line 2692990
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    goto :goto_4

    .line 2692991
    :cond_7
    const/4 v7, 0x0

    goto :goto_5
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2692992
    invoke-static {}, LX/1dS;->b()V

    .line 2692993
    const/4 v0, 0x0

    return-object v0
.end method
