.class public LX/HMN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HMI;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pZ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQc;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field private f:Landroid/content/Intent;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BQc;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2457051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457052
    iput-object p2, p0, LX/HMN;->b:LX/0Ot;

    .line 2457053
    iput-object p3, p0, LX/HMN;->c:LX/0Ot;

    .line 2457054
    iput-object p4, p0, LX/HMN;->d:LX/0Ot;

    .line 2457055
    iput-object p1, p0, LX/HMN;->a:LX/0Ot;

    .line 2457056
    iput-object p5, p0, LX/HMN;->e:Landroid/content/Context;

    .line 2457057
    return-void
.end method


# virtual methods
.method public final a()LX/4At;
    .locals 3

    .prologue
    .line 2457058
    new-instance v0, LX/4At;

    iget-object v1, p0, LX/HMN;->e:Landroid/content/Context;

    const v2, 0x7f0817c1

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public final a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p3    # LX/8A4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/8A4;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "Landroid/content/Intent;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457059
    iput-object p5, p0, LX/HMN;->f:Landroid/content/Intent;

    .line 2457060
    iget-object v0, p0, LX/HMN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iH;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/HMM;

    invoke-direct {v1, p0}, LX/HMM;-><init>(LX/HMN;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 9

    .prologue
    .line 2457061
    iget-object v1, p0, LX/HMN;->f:Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2457062
    iget-object v2, p0, LX/HMN;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BQc;

    sget-object v3, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    const-string v4, "extra_profile_pic_expiration"

    const-wide/16 v6, 0x0

    invoke-virtual {v1, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v6, "staging_ground_photo_caption"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "profile_photo_method_extra"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v8, v0

    invoke-virtual/range {v2 .. v8}, LX/BQc;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2457063
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2457064
    iget-object v0, p0, LX/HMN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/HMN;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081829

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2457065
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2457066
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2457067
    const/4 v0, 0x1

    return v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457068
    const/16 v0, 0xc35

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
