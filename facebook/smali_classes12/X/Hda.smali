.class public final LX/Hda;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hdb;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

.field public b:LX/1Pm;

.field public final synthetic c:LX/Hdb;


# direct methods
.method public constructor <init>(LX/Hdb;)V
    .locals 1

    .prologue
    .line 2488805
    iput-object p1, p0, LX/Hda;->c:LX/Hdb;

    .line 2488806
    move-object v0, p1

    .line 2488807
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2488808
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2488804
    const-string v0, "TopicTopSourcesComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2488790
    if-ne p0, p1, :cond_1

    .line 2488791
    :cond_0
    :goto_0
    return v0

    .line 2488792
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2488793
    goto :goto_0

    .line 2488794
    :cond_3
    check-cast p1, LX/Hda;

    .line 2488795
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2488796
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2488797
    if-eq v2, v3, :cond_0

    .line 2488798
    iget-object v2, p0, LX/Hda;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Hda;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    iget-object v3, p1, LX/Hda;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2488799
    goto :goto_0

    .line 2488800
    :cond_5
    iget-object v2, p1, LX/Hda;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    if-nez v2, :cond_4

    .line 2488801
    :cond_6
    iget-object v2, p0, LX/Hda;->b:LX/1Pm;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Hda;->b:LX/1Pm;

    iget-object v3, p1, LX/Hda;->b:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2488802
    goto :goto_0

    .line 2488803
    :cond_7
    iget-object v2, p1, LX/Hda;->b:LX/1Pm;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
