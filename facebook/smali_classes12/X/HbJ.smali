.class public LX/HbJ;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HbN;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Hbb;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/Had;

.field public c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/HbN;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field private e:LX/HbZ;

.field private f:LX/HbP;


# direct methods
.method public constructor <init>(Ljava/util/List;LX/HbZ;LX/HbP;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Hbb;",
            ">;",
            "LX/HbZ;",
            "LX/HbP;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2485395
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2485396
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/HbJ;->a:LX/0Px;

    .line 2485397
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/HbJ;->c:Landroid/util/SparseArray;

    .line 2485398
    const/4 v0, -0x1

    iput v0, p0, LX/HbJ;->d:I

    .line 2485399
    iput-object p2, p0, LX/HbJ;->e:LX/HbZ;

    .line 2485400
    iput-object p3, p0, LX/HbJ;->f:LX/HbP;

    .line 2485401
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2485378
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2485379
    if-eq p2, v0, :cond_0

    const/4 v2, 0x2

    if-eq p2, v2, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2485380
    packed-switch p2, :pswitch_data_0

    .line 2485381
    invoke-static {}, LX/0nE;->a()Ljava/lang/AssertionError;

    .line 2485382
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 2485383
    goto :goto_0

    .line 2485384
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0312cc

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2485385
    iget-object v0, p0, LX/HbJ;->f:LX/HbP;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2485386
    new-instance p1, LX/HbO;

    invoke-static {v0}, LX/Hbq;->b(LX/0QB;)LX/Hbq;

    move-result-object v3

    check-cast v3, LX/Hbq;

    invoke-static {v0}, LX/HaZ;->a(LX/0QB;)LX/HaZ;

    move-result-object p0

    check-cast p0, LX/HaZ;

    invoke-direct {p1, v1, v2, v3, p0}, LX/HbO;-><init>(Landroid/view/View;Landroid/content/Context;LX/Hbq;LX/HaZ;)V

    .line 2485387
    move-object v0, p1

    .line 2485388
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 2485389
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0312cd

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2485390
    iget-object v0, p0, LX/HbJ;->e:LX/HbZ;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d2bbf

    invoke-virtual {v0, v1, v2, v3}, LX/HbZ;->a(Landroid/view/View;Landroid/content/Context;I)LX/HbY;

    move-result-object v0

    .line 2485391
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 2485392
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0312d2

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2485393
    iget-object v0, p0, LX/HbJ;->e:LX/HbZ;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d2bda

    invoke-virtual {v0, v1, v2, v3}, LX/HbZ;->a(Landroid/view/View;Landroid/content/Context;I)LX/HbY;

    move-result-object v0

    .line 2485394
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2485362
    check-cast p1, LX/HbN;

    const/4 v3, -0x1

    .line 2485363
    iget-object v0, p0, LX/HbJ;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hbb;

    .line 2485364
    iget v1, p1, LX/HbN;->n:I

    move v1, v1

    .line 2485365
    if-eq v1, v3, :cond_0

    .line 2485366
    iget-object v1, p0, LX/HbJ;->c:Landroid/util/SparseArray;

    .line 2485367
    iget v2, p1, LX/HbN;->n:I

    move v2, v2

    .line 2485368
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 2485369
    :cond_0
    iget-object v1, p0, LX/HbJ;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2485370
    invoke-virtual {p1, v0, p2}, LX/HbN;->a(LX/Hbb;I)V

    .line 2485371
    iget v1, p0, LX/HbJ;->d:I

    if-eq v1, v3, :cond_2

    iget v1, p0, LX/HbJ;->d:I

    add-int/lit8 v1, v1, -0x1

    if-eq p2, v1, :cond_1

    iget v1, p0, LX/HbJ;->d:I

    add-int/lit8 v1, v1, 0x1

    if-ne p2, v1, :cond_2

    .line 2485372
    :cond_1
    iget v1, p0, LX/HbJ;->d:I

    invoke-virtual {p0, v1}, LX/HbJ;->e(I)V

    .line 2485373
    :cond_2
    iget-object v1, v0, LX/Hbb;->d:LX/Hba;

    move-object v0, v1

    .line 2485374
    sget-object v1, LX/Hba;->CONCLUSION:LX/Hba;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, LX/HbJ;->b:LX/Had;

    if-eqz v0, :cond_3

    .line 2485375
    check-cast p1, LX/HbY;

    iget-object v0, p0, LX/HbJ;->b:LX/Had;

    .line 2485376
    iput-object v0, p1, LX/HbY;->x:LX/Had;

    .line 2485377
    :cond_3
    return-void
.end method

.method public final e(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2485352
    iput p1, p0, LX/HbJ;->d:I

    .line 2485353
    iget-object v0, p0, LX/HbJ;->c:Landroid/util/SparseArray;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_0

    .line 2485354
    iget-object v0, p0, LX/HbJ;->c:Landroid/util/SparseArray;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HbN;

    .line 2485355
    instance-of v1, v0, LX/HbY;

    if-eqz v1, :cond_0

    .line 2485356
    check-cast v0, LX/HbY;

    invoke-virtual {v0, v2}, LX/HbY;->c(I)V

    .line 2485357
    :cond_0
    iget-object v0, p0, LX/HbJ;->c:Landroid/util/SparseArray;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_1

    .line 2485358
    iget-object v0, p0, LX/HbJ;->c:Landroid/util/SparseArray;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HbN;

    .line 2485359
    instance-of v1, v0, LX/HbY;

    if-eqz v1, :cond_1

    .line 2485360
    check-cast v0, LX/HbY;

    invoke-virtual {v0, v2}, LX/HbY;->c(I)V

    .line 2485361
    :cond_1
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2485347
    if-nez p1, :cond_0

    .line 2485348
    const/4 v0, 0x0

    .line 2485349
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/HbJ;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hbb;

    .line 2485350
    iget-object v1, v0, LX/Hbb;->d:LX/Hba;

    move-object v0, v1

    .line 2485351
    sget-object v1, LX/Hba;->CONCLUSION:LX/Hba;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2485346
    iget-object v0, p0, LX/HbJ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
