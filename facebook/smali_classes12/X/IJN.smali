.class public final LX/IJN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IFW;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 0

    .prologue
    .line 2563480
    iput-object p1, p0, LX/IJN;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2563481
    iget-object v0, p0, LX/IJN;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->c$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    .line 2563482
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 2563483
    iget-object v0, p0, LX/IJN;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    .line 2563484
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6ax;

    .line 2563485
    if-nez v2, :cond_1

    .line 2563486
    :cond_0
    :goto_0
    return-void

    .line 2563487
    :cond_1
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    invoke-virtual {v1, p1}, LX/IFX;->e(Ljava/lang/String;)LX/IFb;

    move-result-object v1

    .line 2563488
    if-eqz v1, :cond_0

    .line 2563489
    iget-object v3, v1, LX/IFb;->a:Lcom/facebook/location/ImmutableLocation;

    move-object v7, v3

    .line 2563490
    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v5

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v9

    invoke-direct {v3, v5, v6, v9, v10}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2563491
    iget-object v4, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    invoke-static {v4, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2563492
    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->m(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    .line 2563493
    invoke-static {v3}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;)LX/6aM;

    move-result-object v4

    const-wide/16 v5, 0x2ee

    const/4 v8, 0x0

    invoke-static {v0, v4, v5, v6, v8}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/6aM;JLjava/lang/Runnable;)V

    .line 2563494
    iget-object v4, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    iget-object v5, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->s:LX/IJ4;

    invoke-virtual {v4, v1, v5}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(LX/IFR;LX/IJ4;)V

    .line 2563495
    :cond_2
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->c:LX/6aE;

    const/16 v4, 0x2ee

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v5

    new-instance v6, LX/IJO;

    invoke-direct {v6, v0, p1, v3, v7}, LX/IJO;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Ljava/lang/String;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/location/ImmutableLocation;)V

    invoke-virtual/range {v1 .. v6}, LX/6aE;->a(LX/6ax;Lcom/facebook/android/maps/model/LatLng;ILX/6al;LX/IJO;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2563496
    iget-object v0, p0, LX/IJN;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->c$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    .line 2563497
    return-void
.end method
