.class public final LX/IHs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2561202
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2561203
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2561204
    :goto_0
    return v1

    .line 2561205
    :cond_0
    const-string v8, "remaining_duration"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2561206
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 2561207
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 2561208
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2561209
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2561210
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2561211
    const-string v8, "message"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2561212
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2561213
    :cond_2
    const-string v8, "ping_type"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2561214
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 2561215
    :cond_3
    const-string v8, "recipient"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2561216
    invoke-static {p0, p1}, LX/IHr;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2561217
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2561218
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2561219
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2561220
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 2561221
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2561222
    if-eqz v0, :cond_6

    .line 2561223
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 2561224
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2561225
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2561226
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2561227
    if-eqz v0, :cond_0

    .line 2561228
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561229
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2561230
    :cond_0
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2561231
    if-eqz v0, :cond_1

    .line 2561232
    const-string v0, "ping_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561233
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2561234
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2561235
    if-eqz v0, :cond_2

    .line 2561236
    const-string v1, "recipient"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561237
    invoke-static {p0, v0, p2, p3}, LX/IHr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2561238
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2561239
    if-eqz v0, :cond_3

    .line 2561240
    const-string v1, "remaining_duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561241
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2561242
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2561243
    return-void
.end method
