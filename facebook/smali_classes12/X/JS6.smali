.class public final LX/JS6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public final b:Lcom/facebook/graphql/model/GraphQLApplication;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphObject;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAudio;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLMusicType;Lcom/facebook/graphql/model/GraphQLApplication;Ljava/util/List;LX/0Px;LX/0Px;)V
    .locals 0
    .param p2    # Lcom/facebook/graphql/model/GraphQLApplication;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLMusicType;",
            "Lcom/facebook/graphql/model/GraphQLApplication;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphObject;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAudio;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2694681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2694682
    iput-object p1, p0, LX/JS6;->a:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 2694683
    iput-object p2, p0, LX/JS6;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 2694684
    iput-object p3, p0, LX/JS6;->c:Ljava/util/List;

    .line 2694685
    iput-object p4, p0, LX/JS6;->d:LX/0Px;

    .line 2694686
    iput-object p5, p0, LX/JS6;->e:LX/0Px;

    .line 2694687
    return-void
.end method
