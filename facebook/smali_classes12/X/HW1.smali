.class public final LX/HW1;
.super LX/CXv;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2475523
    iput-object p1, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    invoke-direct {p0, p2}, LX/CXv;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 2475524
    check-cast p1, LX/CXu;

    .line 2475525
    iget-object v0, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->g:LX/HW4;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    if-nez v0, :cond_1

    .line 2475526
    :cond_0
    :goto_0
    return-void

    .line 2475527
    :cond_1
    iget-object v0, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_2

    .line 2475528
    iget-object v0, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->J:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2475529
    iget-object v0, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    .line 2475530
    :cond_2
    iget-object v0, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v1, p1, LX/CXu;->b:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    .line 2475531
    iput-object v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->K:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    .line 2475532
    iget-object v0, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->g:LX/HW4;

    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2475533
    iget-object v0, p0, LX/HW1;->b:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method
