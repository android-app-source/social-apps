.class public final LX/IgX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IgW;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;)V
    .locals 0

    .prologue
    .line 2601205
    iput-object p1, p0, LX/IgX;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;B)V
    .locals 0

    .prologue
    .line 2601206
    invoke-direct {p0, p1}, LX/IgX;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2601207
    iget-object v0, p0, LX/IgX;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->setResult(I)V

    .line 2601208
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 3

    .prologue
    .line 2601209
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2601210
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2601211
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2601212
    const-string v2, "extra_media_items"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2601213
    iget-object v0, p0, LX/IgX;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2601214
    iget-object v0, p0, LX/IgX;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;

    invoke-virtual {v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->finish()V

    .line 2601215
    return-void
.end method
