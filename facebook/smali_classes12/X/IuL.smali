.class public LX/IuL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final E:Ljava/lang/Object;

.field private static final a:Ljava/lang/String;


# instance fields
.field public final A:LX/2PE;

.field public final B:LX/IuQ;

.field private final C:LX/IuR;

.field public final D:LX/IuN;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/outbound/Sender;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Dp4;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2cT;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2P4;

.field public final f:LX/2P0;

.field private final g:LX/Dob;

.field public final h:LX/2Ox;

.field public final i:LX/2PJ;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/2Ow;

.field public final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/2NB;

.field private final p:LX/IuF;

.field private final q:LX/Iu4;

.field private final r:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

.field private final s:LX/2PP;

.field private final t:LX/2PQ;

.field private final u:LX/43C;

.field public final v:LX/IuT;

.field public final w:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

.field private final x:LX/1Er;

.field public final y:LX/2MO;

.field private final z:LX/26j;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2626495
    const-class v0, LX/IuL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IuL;->a:Ljava/lang/String;

    .line 2626496
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/IuL;->E:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/0Or;LX/FDp;LX/Dp4;LX/0Or;LX/2P4;LX/2P0;LX/DoY;LX/Dob;LX/2Ox;LX/34G;LX/3Ec;LX/2PJ;LX/0Or;LX/0Or;LX/0Or;LX/2Ow;LX/0Or;LX/2NB;LX/IuF;LX/Iu4;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/2PP;LX/2PQ;LX/7V0;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/43C;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;LX/2Oi;LX/1Er;LX/2MO;LX/1Hr;LX/26j;LX/2cD;LX/2PE;)V
    .locals 25
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p16    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/outbound/Sender;",
            ">;",
            "LX/FDp;",
            "LX/Dp4;",
            "LX/0Or",
            "<",
            "LX/2cT;",
            ">;",
            "LX/2P4;",
            "LX/2P0;",
            "LX/DoY;",
            "LX/Dob;",
            "LX/2Ox;",
            "LX/34G;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            "LX/2PJ;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;",
            "LX/2Ow;",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/2NB;",
            "LX/IuF;",
            "LX/Iu4;",
            "Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;",
            "LX/2PP;",
            "LX/2PQ;",
            "LX/7V0;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/43C;",
            "Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;",
            "LX/2Oi;",
            "LX/1Er;",
            "LX/2MO;",
            "LX/1Hr;",
            "LX/26j;",
            "LX/2cD;",
            "LX/2PE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2626517
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 2626518
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->b:LX/0Or;

    .line 2626519
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->c:LX/Dp4;

    .line 2626520
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->d:LX/0Or;

    .line 2626521
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->e:LX/2P4;

    .line 2626522
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->f:LX/2P0;

    .line 2626523
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->g:LX/Dob;

    .line 2626524
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->h:LX/2Ox;

    .line 2626525
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->i:LX/2PJ;

    .line 2626526
    move-object/from16 v0, p14

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->j:LX/0Or;

    .line 2626527
    move-object/from16 v0, p15

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->k:LX/0Or;

    .line 2626528
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->l:LX/0Or;

    .line 2626529
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->m:LX/2Ow;

    .line 2626530
    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->n:LX/0Or;

    .line 2626531
    move-object/from16 v0, p19

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->o:LX/2NB;

    .line 2626532
    move-object/from16 v0, p20

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->p:LX/IuF;

    .line 2626533
    move-object/from16 v0, p21

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->q:LX/Iu4;

    .line 2626534
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->r:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    .line 2626535
    move-object/from16 v0, p23

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->s:LX/2PP;

    .line 2626536
    move-object/from16 v0, p24

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->t:LX/2PQ;

    .line 2626537
    move-object/from16 v0, p27

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->u:LX/43C;

    .line 2626538
    move-object/from16 v0, p28

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->w:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    .line 2626539
    move-object/from16 v0, p30

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->x:LX/1Er;

    .line 2626540
    move-object/from16 v0, p31

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->y:LX/2MO;

    .line 2626541
    move-object/from16 v0, p33

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->z:LX/26j;

    .line 2626542
    move-object/from16 v0, p35

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuL;->A:LX/2PE;

    .line 2626543
    new-instance v2, LX/IuT;

    move-object/from16 v0, p32

    invoke-direct {v2, v0}, LX/IuT;-><init>(LX/1Hr;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/IuL;->v:LX/IuT;

    .line 2626544
    new-instance v2, LX/IuN;

    new-instance v4, LX/IuU;

    move-object/from16 v0, p10

    move-object/from16 v1, p11

    invoke-direct {v4, v0, v1}, LX/IuU;-><init>(LX/2Ox;LX/34G;)V

    move-object/from16 v3, p10

    move-object/from16 v5, p16

    move-object/from16 v6, p17

    move-object/from16 v7, p18

    invoke-direct/range {v2 .. v7}, LX/IuN;-><init>(LX/2Ox;LX/IuU;LX/0Or;LX/2Ow;LX/0Or;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/IuL;->D:LX/IuN;

    .line 2626545
    new-instance v2, LX/IuQ;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/IuL;->v:LX/IuT;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/IuL;->D:LX/IuN;

    move-object/from16 v22, v0

    move-object/from16 v3, p1

    move-object/from16 v4, p12

    move-object/from16 v5, p3

    move-object/from16 v6, p10

    move-object/from16 v7, p29

    move-object/from16 v8, p6

    move-object/from16 v10, p8

    move-object/from16 v11, p19

    move-object/from16 v12, p7

    move-object/from16 v13, p22

    move-object/from16 v14, p25

    move-object/from16 v15, p17

    move-object/from16 v16, p2

    move-object/from16 v17, p26

    move-object/from16 v18, p14

    move-object/from16 v19, p13

    move-object/from16 v20, p28

    move-object/from16 v21, p18

    move-object/from16 v23, p34

    move-object/from16 v24, p35

    invoke-direct/range {v2 .. v24}, LX/IuQ;-><init>(LX/0TD;LX/3Ec;LX/FDp;LX/2Ox;LX/2Oi;LX/2P4;LX/IuT;LX/DoY;LX/2NB;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/7V0;LX/2Ow;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2PJ;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;LX/0Or;LX/IuN;LX/2cD;LX/2PE;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/IuL;->B:LX/IuQ;

    .line 2626546
    new-instance v2, LX/IuR;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/IuL;->v:LX/IuT;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/IuL;->D:LX/IuN;

    move-object/from16 v21, v0

    move-object/from16 v3, p12

    move-object/from16 v4, p3

    move-object/from16 v5, p10

    move-object/from16 v6, p29

    move-object/from16 v7, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p19

    move-object/from16 v11, p7

    move-object/from16 v12, p22

    move-object/from16 v13, p25

    move-object/from16 v14, p17

    move-object/from16 v15, p2

    move-object/from16 v16, p26

    move-object/from16 v17, p14

    move-object/from16 v18, p13

    move-object/from16 v19, p28

    move-object/from16 v20, p18

    invoke-direct/range {v2 .. v21}, LX/IuR;-><init>(LX/3Ec;LX/FDp;LX/2Ox;LX/2Oi;LX/2P4;LX/IuT;LX/DoY;LX/2NB;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/7V0;LX/2Ow;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2PJ;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;LX/0Or;LX/IuN;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/IuL;->C:LX/IuR;

    .line 2626547
    return-void
.end method

.method public static a(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/6f7;
    .locals 5

    .prologue
    .line 2626497
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    .line 2626498
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    .line 2626499
    iget-object v0, v1, LX/6f7;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2626500
    iput-object v0, v1, LX/6f7;->n:Ljava/lang/String;

    .line 2626501
    iput-object p1, v1, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626502
    sget-object v0, LX/Doq;->a:LX/Doq;

    move-object v0, v0

    .line 2626503
    invoke-virtual {v0}, LX/Doq;->a()J

    move-result-wide v2

    .line 2626504
    iput-wide v2, v1, LX/6f7;->c:J

    .line 2626505
    sget-object v0, LX/Doq;->a:LX/Doq;

    move-object v0, v0

    .line 2626506
    invoke-virtual {v0}, LX/Doq;->a()J

    move-result-wide v2

    .line 2626507
    iput-wide v2, v1, LX/6f7;->d:J

    .line 2626508
    sget-object v0, LX/2uW;->ADMIN:LX/2uW;

    .line 2626509
    iput-object v0, v1, LX/6f7;->l:LX/2uW;

    .line 2626510
    new-instance v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, p0, LX/IuL;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    iget-object v0, p0, LX/IuL;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2626511
    iput-object v2, v1, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2626512
    invoke-static {}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->newBuilder()LX/6ev;

    move-result-object v0

    .line 2626513
    invoke-virtual {v0}, LX/6ev;->a()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-result-object v0

    .line 2626514
    iput-object v0, v1, LX/6f7;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 2626515
    iput-object p2, v1, LX/6f7;->f:Ljava/lang/String;

    .line 2626516
    return-object v1
.end method

.method public static a(LX/0QB;)LX/IuL;
    .locals 7

    .prologue
    .line 2626468
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2626469
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2626470
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2626471
    if-nez v1, :cond_0

    .line 2626472
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2626473
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2626474
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2626475
    sget-object v1, LX/IuL;->E:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2626476
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2626477
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2626478
    :cond_1
    if-nez v1, :cond_4

    .line 2626479
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2626480
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2626481
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/IuL;->b(LX/0QB;)LX/IuL;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2626482
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2626483
    if-nez v1, :cond_2

    .line 2626484
    sget-object v0, LX/IuL;->E:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuL;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2626485
    :goto_1
    if-eqz v0, :cond_3

    .line 2626486
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2626487
    :goto_3
    check-cast v0, LX/IuL;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2626488
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2626489
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2626490
    :catchall_1
    move-exception v0

    .line 2626491
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2626492
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2626493
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2626494
    :cond_2
    :try_start_8
    sget-object v0, LX/IuL;->E:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuL;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a()Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2626459
    iget-object v0, p0, LX/IuL;->s:LX/2PP;

    iget-object v1, p0, LX/IuL;->p:LX/IuF;

    invoke-virtual {v1}, LX/IuF;->a()[B

    move-result-object v1

    const/4 v3, 0x0

    .line 2626460
    invoke-virtual {v0}, LX/2PI;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2626461
    sget-object v2, LX/2PP;->b:Ljava/lang/Class;

    const-string v3, "Stored procedure sender not available"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2626462
    :goto_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/IuL;->a(LX/IuL;Z)V

    .line 2626463
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626464
    return-object v0

    .line 2626465
    :cond_0
    new-instance v5, LX/DpM;

    iget-object v2, v0, LX/2PP;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v4, v0, LX/2PP;->c:LX/2PJ;

    invoke-virtual {v4}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v2, v4}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    iget-object v2, v0, LX/2PP;->d:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const/16 v8, 0x1e

    move-object v4, v3

    move-object v9, v3

    move-object v10, v1

    invoke-static/range {v3 .. v10}, LX/Dpm;->a(LX/Dpe;LX/DpM;LX/DpM;JILX/DpO;[B)LX/DpN;

    move-result-object v2

    .line 2626466
    invoke-static {v2}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2PI;->a([B)V

    .line 2626467
    goto :goto_0
.end method

.method public static a(LX/IuL;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/attachment/Attachment;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;",
            "Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2626438
    const/4 v2, 0x0

    .line 2626439
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 2626440
    iget-object v0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v0, v0, Lcom/facebook/messaging/model/attachment/ImageData;->g:Ljava/lang/String;

    .line 2626441
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    move v1, v0

    .line 2626442
    goto :goto_0

    :cond_0
    move v0, v2

    .line 2626443
    goto :goto_1

    .line 2626444
    :cond_1
    move v7, v1

    .line 2626445
    const/16 v0, 0x7800

    if-gt v7, v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2626446
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2626447
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2626448
    iget-object v1, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    iget-object v2, p5, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2626449
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2626450
    iget-object v1, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, LX/IuL;->b(LX/IuL;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2626451
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v7

    const/16 v2, 0x7800

    if-gt v1, v2, :cond_6

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 2626452
    if-nez v1, :cond_2

    .line 2626453
    const/4 v3, 0x0

    .line 2626454
    :cond_2
    iget-object v1, p5, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->d:Ljava/lang/String;

    iget-object v4, p5, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->e:[B

    iget-object v5, p5, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->f:Ljava/lang/String;

    iget-object v6, p5, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->g:Ljava/lang/Long;

    move-object v2, p1

    invoke-static/range {v0 .. v6}, LX/Iu5;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;)Lcom/facebook/messaging/model/attachment/Attachment;

    move-result-object v0

    .line 2626455
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2626456
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 2626457
    :cond_4
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2626458
    :cond_5
    return-object v8

    :cond_6
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public static a(LX/IuL;JJ)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2626423
    iget-object v0, p0, LX/IuL;->e:LX/2P4;

    invoke-virtual {v0, p1, p2}, LX/2P4;->a(J)LX/0Px;

    move-result-object v0

    .line 2626424
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2626425
    :goto_0
    return-void

    .line 2626426
    :cond_0
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626427
    iget-object v1, p0, LX/IuL;->g:LX/Dob;

    invoke-virtual {v1, p1, p2}, LX/Dob;->b(J)Lcom/facebook/user/model/User;

    move-result-object v2

    .line 2626428
    if-nez v2, :cond_1

    .line 2626429
    sget-object v0, LX/IuL;->a:Ljava/lang/String;

    const-string v1, "Failed to find user %d in the tincan database."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2626430
    :cond_1
    iget-object v1, p0, LX/IuL;->n:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0806bc

    new-array v4, v4, [Ljava/lang/Object;

    .line 2626431
    iget-object v6, v2, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v2, v6

    .line 2626432
    aput-object v2, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2626433
    invoke-static {p0, v0, v1}, LX/IuL;->a(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/6f7;

    move-result-object v0

    .line 2626434
    iput-wide p3, v0, LX/6f7;->c:J

    .line 2626435
    move-object v0, v0

    .line 2626436
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2626437
    invoke-static {p0, v0}, LX/IuL;->a(LX/IuL;Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0
.end method

.method private static a(LX/IuL;JLjava/lang/String;)V
    .locals 11

    .prologue
    .line 2626418
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626419
    iget-object v0, p0, LX/IuL;->v:LX/IuT;

    .line 2626420
    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/IuT;->a(LX/IuT;ILX/DpZ;Ljava/lang/Integer;)LX/IuS;

    move-result-object v1

    move-object v4, v1

    .line 2626421
    iget-object v0, p0, LX/IuL;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v1, p0, LX/IuL;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    iget-object v3, p0, LX/IuL;->i:LX/2PJ;

    invoke-virtual {v3}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v7, v4, LX/IuS;->a:[B

    iget-object v8, v4, LX/IuS;->b:[B

    iget-object v4, p0, LX/IuL;->p:LX/IuF;

    invoke-virtual {v4}, LX/IuF;->a()[B

    move-result-object v9

    const/4 v10, 0x0

    move-wide v4, p1

    move-object v6, p3

    invoke-virtual/range {v0 .. v10}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(JLjava/lang/String;JLjava/lang/String;[B[B[BZ)V

    .line 2626422
    return-void
.end method

.method public static a(LX/IuL;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 5

    .prologue
    .line 2626412
    iget-object v0, p0, LX/IuL;->v:LX/IuT;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 2626413
    invoke-static {v1}, LX/DpZ;->b(Ljava/lang/String;)LX/DpZ;

    move-result-object v3

    .line 2626414
    const/4 v4, 0x5

    invoke-static {v0, v4, v3, v2}, LX/IuT;->a(LX/IuT;ILX/DpZ;Ljava/lang/Integer;)LX/IuS;

    move-result-object v3

    move-object v0, v3

    .line 2626415
    iget-object v0, v0, LX/IuS;->a:[B

    .line 2626416
    iget-object v1, p0, LX/IuL;->D:LX/IuN;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, v2}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;[BLjava/lang/String;)V

    .line 2626417
    return-void
.end method

.method public static a(LX/IuL;Z)V
    .locals 4

    .prologue
    .line 2626406
    iget-object v0, p0, LX/IuL;->h:LX/2Ox;

    invoke-virtual {v0, p1}, LX/2Ox;->a(Z)V

    .line 2626407
    iget-object v0, p0, LX/IuL;->e:LX/2P4;

    invoke-virtual {v0}, LX/2P4;->a()LX/0Rf;

    move-result-object v2

    .line 2626408
    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626409
    iget-object v1, p0, LX/IuL;->l:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    invoke-virtual {v1, v0, p1}, LX/2Oe;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V

    goto :goto_0

    .line 2626410
    :cond_0
    iget-object v0, p0, LX/IuL;->m:LX/2Ow;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Ow;->a(LX/0Px;)V

    .line 2626411
    return-void
.end method

.method private static b(LX/0QB;)LX/IuL;
    .locals 38

    .prologue
    .line 2626548
    new-instance v2, LX/IuL;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    const/16 v4, 0xdd9

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/FDp;->a(LX/0QB;)LX/FDp;

    move-result-object v5

    check-cast v5, LX/FDp;

    invoke-static/range {p0 .. p0}, LX/Dp4;->a(LX/0QB;)LX/Dp4;

    move-result-object v6

    check-cast v6, LX/Dp4;

    const/16 v7, 0xdcd

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/2P4;->a(LX/0QB;)LX/2P4;

    move-result-object v8

    check-cast v8, LX/2P4;

    invoke-static/range {p0 .. p0}, LX/2P0;->a(LX/0QB;)LX/2P0;

    move-result-object v9

    check-cast v9, LX/2P0;

    invoke-static/range {p0 .. p0}, LX/DoY;->a(LX/0QB;)LX/DoY;

    move-result-object v10

    check-cast v10, LX/DoY;

    invoke-static/range {p0 .. p0}, LX/Dob;->a(LX/0QB;)LX/Dob;

    move-result-object v11

    check-cast v11, LX/Dob;

    invoke-static/range {p0 .. p0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v12

    check-cast v12, LX/2Ox;

    invoke-static/range {p0 .. p0}, LX/34G;->a(LX/0QB;)LX/34G;

    move-result-object v13

    check-cast v13, LX/34G;

    invoke-static/range {p0 .. p0}, LX/3Ec;->a(LX/0QB;)LX/3Ec;

    move-result-object v14

    check-cast v14, LX/3Ec;

    invoke-static/range {p0 .. p0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v15

    check-cast v15, LX/2PJ;

    const/16 v16, 0x15e7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x12cb

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    const/16 v18, 0xce7

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v19

    check-cast v19, LX/2Ow;

    const-class v20, Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/2NB;->a(LX/0QB;)LX/2NB;

    move-result-object v21

    check-cast v21, LX/2NB;

    invoke-static/range {p0 .. p0}, LX/IuF;->a(LX/0QB;)LX/IuF;

    move-result-object v22

    check-cast v22, LX/IuF;

    invoke-static/range {p0 .. p0}, LX/Iu4;->a(LX/0QB;)LX/Iu4;

    move-result-object v23

    check-cast v23, LX/Iu4;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    move-result-object v24

    check-cast v24, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-static/range {p0 .. p0}, LX/2PP;->a(LX/0QB;)LX/2PP;

    move-result-object v25

    check-cast v25, LX/2PP;

    invoke-static/range {p0 .. p0}, LX/2PQ;->a(LX/0QB;)LX/2PQ;

    move-result-object v26

    check-cast v26, LX/2PQ;

    invoke-static/range {p0 .. p0}, LX/7V1;->a(LX/0QB;)LX/7V1;

    move-result-object v27

    check-cast v27, LX/7V0;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v28

    check-cast v28, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/43D;->a(LX/0QB;)LX/43C;

    move-result-object v29

    check-cast v29, LX/43C;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    move-result-object v30

    check-cast v30, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    invoke-static/range {p0 .. p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v31

    check-cast v31, LX/2Oi;

    invoke-static/range {p0 .. p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v32

    check-cast v32, LX/1Er;

    invoke-static/range {p0 .. p0}, LX/2MO;->a(LX/0QB;)LX/2MO;

    move-result-object v33

    check-cast v33, LX/2MO;

    invoke-static/range {p0 .. p0}, LX/1Hr;->a(LX/0QB;)LX/1Hr;

    move-result-object v34

    check-cast v34, LX/1Hr;

    invoke-static/range {p0 .. p0}, LX/26j;->a(LX/0QB;)LX/26j;

    move-result-object v35

    check-cast v35, LX/26j;

    invoke-static/range {p0 .. p0}, LX/2cD;->a(LX/0QB;)LX/2cD;

    move-result-object v36

    check-cast v36, LX/2cD;

    invoke-static/range {p0 .. p0}, LX/2PE;->a(LX/0QB;)LX/2PE;

    move-result-object v37

    check-cast v37, LX/2PE;

    invoke-direct/range {v2 .. v37}, LX/IuL;-><init>(LX/0TD;LX/0Or;LX/FDp;LX/Dp4;LX/0Or;LX/2P4;LX/2P0;LX/DoY;LX/Dob;LX/2Ox;LX/34G;LX/3Ec;LX/2PJ;LX/0Or;LX/0Or;LX/0Or;LX/2Ow;LX/0Or;LX/2NB;LX/IuF;LX/Iu4;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/2PP;LX/2PQ;LX/7V0;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/43C;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;LX/2Oi;LX/1Er;LX/2MO;LX/1Hr;LX/26j;LX/2cD;LX/2PE;)V

    .line 2626549
    return-object v2
.end method

.method public static b(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/IuO;
    .locals 3
    .param p0    # LX/IuL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2626396
    if-nez p1, :cond_2

    .line 2626397
    iget-object v0, p0, LX/IuL;->z:LX/26j;

    const/4 v1, 0x0

    .line 2626398
    invoke-virtual {v0}, LX/26j;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/26j;->a:LX/0Uh;

    const/16 p1, 0x1ec

    invoke-virtual {v2, p1, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 2626399
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IuL;->B:LX/IuQ;

    .line 2626400
    :goto_0
    return-object v0

    .line 2626401
    :cond_1
    iget-object v0, p0, LX/IuL;->C:LX/IuR;

    goto :goto_0

    .line 2626402
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->TINCAN:LX/5e9;

    if-ne v0, v1, :cond_3

    .line 2626403
    iget-object v0, p0, LX/IuL;->C:LX/IuR;

    goto :goto_0

    .line 2626404
    :cond_3
    iget-object v0, p0, LX/IuL;->A:LX/2PE;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2PE;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 2626405
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/IuL;->C:LX/IuR;

    goto :goto_0

    :cond_4
    iget-object v0, p0, LX/IuL;->B:LX/IuQ;

    goto :goto_0
.end method

.method private b()Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2626386
    iget-object v0, p0, LX/IuL;->h:LX/2Ox;

    invoke-virtual {v0}, LX/2Ox;->a()V

    .line 2626387
    iget-object v0, p0, LX/IuL;->t:LX/2PQ;

    iget-object v1, p0, LX/IuL;->p:LX/IuF;

    invoke-virtual {v1}, LX/IuF;->a()[B

    move-result-object v1

    const/4 v3, 0x0

    .line 2626388
    invoke-virtual {v0}, LX/2PI;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2626389
    sget-object v2, LX/2PQ;->b:Ljava/lang/Class;

    const-string v3, "Stored procedure sender not available"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2626390
    :goto_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/IuL;->a(LX/IuL;Z)V

    .line 2626391
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626392
    return-object v0

    .line 2626393
    :cond_0
    new-instance v5, LX/DpM;

    iget-object v2, v0, LX/2PQ;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v4, v0, LX/2PQ;->c:LX/2PJ;

    invoke-virtual {v4}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v2, v4}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    iget-object v2, v0, LX/2PQ;->d:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const/16 v8, 0x1e

    move-object v4, v3

    move-object v9, v3

    move-object v10, v1

    invoke-static/range {v3 .. v10}, LX/Dpm;->a(LX/Dpe;LX/DpM;LX/DpM;JILX/DpO;[B)LX/DpN;

    move-result-object v2

    .line 2626394
    invoke-static {v2}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2PI;->a([B)V

    .line 2626395
    goto :goto_0
.end method

.method private static b(LX/IuL;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2626376
    new-instance v0, LX/431;

    const/16 v1, 0x2a

    invoke-direct {v0, v2, v1}, LX/431;-><init>(II)V

    .line 2626377
    :try_start_0
    iget-object v1, p0, LX/IuL;->u:LX/43C;

    invoke-interface {v1, p1, v0}, LX/43C;->a(Ljava/lang/String;LX/431;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2626378
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2626379
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 p0, 0x46

    invoke-virtual {v0, v2, p0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2626380
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    move-object v0, v1

    .line 2626381
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2626382
    :goto_0
    return-object v0

    .line 2626383
    :catch_0
    move-exception v0

    .line 2626384
    sget-object v1, LX/IuL;->a:Ljava/lang/String;

    const-string v2, "Error while generating a thumbnail"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2626385
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/IuL;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 3

    .prologue
    .line 2626365
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 2626366
    :cond_0
    :goto_0
    return-void

    .line 2626367
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2626368
    iget-object v1, p0, LX/IuL;->x:LX/1Er;

    sget-object v2, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    .line 2626369
    invoke-static {v1, v2}, LX/1Er;->a(LX/1Er;LX/46h;)LX/1Es;

    move-result-object p0

    .line 2626370
    if-eqz v0, :cond_2

    iget-object v1, p0, LX/1Es;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2626371
    :cond_2
    const/4 v1, 0x0

    .line 2626372
    :goto_1
    move p0, v1

    .line 2626373
    move v1, p0

    .line 2626374
    if-eqz v1, :cond_0

    .line 2626375
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto :goto_1
.end method

.method public static c(LX/1qK;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2

    .prologue
    .line 2626361
    iget-object v0, p0, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626362
    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2626363
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626364
    return-object v0
.end method

.method private l(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    .line 2626339
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626340
    const-string v1, "is_multidevice"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2626341
    const-string v2, "prekey_bundle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2626342
    new-instance v4, LX/DpM;

    const-string v3, "user_id_to"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v5, "device_id_to"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v3, v5}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    .line 2626343
    new-instance v8, LX/DpU;

    const-string v3, "prekey"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    const-string v5, "prekey_id"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v8, v3, v5}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    .line 2626344
    new-instance v7, LX/Dpf;

    new-instance v3, LX/DpU;

    const-string v5, "signed_prekey"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v5

    const-string v6, "signed_prekey_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v3, v5, v6}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    const-string v5, "signed_prekey_signature"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v5

    invoke-direct {v7, v3, v5}, LX/Dpf;-><init>(LX/DpU;[B)V

    .line 2626345
    new-instance v3, LX/DpK;

    const-string v5, "codename"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "identity_key"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, LX/DpK;-><init>(LX/DpM;Ljava/lang/String;[BLX/Dpf;LX/DpU;LX/DpL;)V

    move-object v0, v3

    .line 2626346
    iget-object v2, p0, LX/IuL;->c:LX/Dp4;

    .line 2626347
    iget-object v3, v0, LX/DpK;->msg_to:LX/DpM;

    iget-object v3, v3, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 2626348
    iget-object v5, v0, LX/DpK;->msg_to:LX/DpM;

    iget-object v5, v5, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-static {v3, v4, v5}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2626349
    iget-object v3, v2, LX/Dp4;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/IuD;

    .line 2626350
    :try_start_0
    iget-object v3, v2, LX/Dp4;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DoJ;

    iget-object v5, v0, LX/DpK;->msg_to:LX/DpM;

    iget-object v5, v5, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v6, v0, LX/DpK;->identity_key:[B

    iget-object v7, v0, LX/DpK;->pre_key_with_id:LX/DpU;

    iget-object v7, v7, LX/DpU;->id:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, v0, LX/DpK;->pre_key_with_id:LX/DpU;

    iget-object v8, v8, LX/DpU;->public_key:[B

    iget-object v9, v0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v9, v9, LX/Dpf;->public_key_with_id:LX/DpU;

    iget-object v9, v9, LX/DpU;->id:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v10, v0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v10, v10, LX/Dpf;->public_key_with_id:LX/DpU;

    iget-object v10, v10, LX/DpU;->public_key:[B

    iget-object v11, v0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v11, v11, LX/Dpf;->signature:[B

    move v13, v1

    invoke-virtual/range {v3 .. v13}, LX/DoJ;->a(Ljava/lang/String;Ljava/lang/String;[BI[BI[B[BLX/IuD;Z)V
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/Eaq; {:try_start_0 .. :try_end_0} :catch_0

    .line 2626351
    iget-object v3, v2, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-virtual {v3, v4, v1}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Ljava/lang/String;Z)V

    .line 2626352
    :goto_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626353
    return-object v0

    .line 2626354
    :catch_0
    move-exception v3

    .line 2626355
    :goto_1
    sget-object v5, LX/Dp4;->a:Ljava/lang/Class;

    const-string v6, "Error processing pre-keys from server"

    invoke-static {v5, v6, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2626356
    iget-object v3, v2, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    .line 2626357
    iget-object v5, v3, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->l:LX/IuH;

    invoke-virtual {v5, v4}, LX/IuH;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v5

    .line 2626358
    iget-object v6, v3, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->v:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    iget-object v7, v3, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->j:Landroid/content/res/Resources;

    const v8, 0x7f0806c7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 2626359
    goto :goto_0

    .line 2626360
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method private m(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2626312
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626313
    const-string v1, "thread_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2626314
    iget-object v1, p0, LX/IuL;->r:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    .line 2626315
    sget-object v2, LX/Dop;->COMPLETED:LX/Dop;

    invoke-static {v1, v0, v2}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dop;)V

    .line 2626316
    if-eqz v0, :cond_0

    sget-object v1, LX/5e9;->TINCAN_MULTI_ENDPOINT:LX/5e9;

    iget-object v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v1, v2}, LX/5e9;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2626317
    iget-object v3, p0, LX/IuL;->j:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-object v5, p0, LX/IuL;->i:LX/2PJ;

    invoke-virtual {v5}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2626318
    new-instance v4, LX/Eay;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/Eap;

    const/4 v7, 0x0

    invoke-direct {v6, v3, v7}, LX/Eap;-><init>(Ljava/lang/String;I)V

    invoke-direct {v4, v5, v6}, LX/Eay;-><init>(Ljava/lang/String;LX/Eap;)V

    .line 2626319
    new-instance v3, LX/Eax;

    iget-object v5, p0, LX/IuL;->A:LX/2PE;

    invoke-direct {v3, v5}, LX/Eax;-><init>(LX/2PE;)V

    .line 2626320
    invoke-virtual {v3, v4}, LX/Eax;->a(LX/Eay;)LX/EbB;

    move-result-object v3

    invoke-virtual {v3}, LX/EbB;->a()[B

    move-result-object v3

    .line 2626321
    iget-object v4, p0, LX/IuL;->B:LX/IuQ;

    invoke-virtual {v4, v0, v3}, LX/IuQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;[B)V

    .line 2626322
    :cond_0
    invoke-static {p0, v0}, LX/IuL;->b(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/IuO;

    move-result-object v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2626323
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2626324
    iget-object v2, v1, LX/IuO;->q:LX/2P0;

    invoke-virtual {v2, v0}, LX/2P0;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v2

    .line 2626325
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v7, v3

    .line 2626326
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_0
    if-ltz v6, :cond_4

    .line 2626327
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    .line 2626328
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v8, LX/2uW;->PENDING_SEND:LX/2uW;

    if-ne v3, v8, :cond_3

    move v3, v4

    :goto_1
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2626329
    invoke-static {v2}, LX/IuO;->e(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, LX/IuO;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, LX/IuO;->h(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2626330
    :cond_1
    :try_start_0
    invoke-static {v1, v2}, LX/IuO;->d(LX/IuO;Lcom/facebook/messaging/model/messages/Message;)LX/IuS;

    move-result-object v3

    .line 2626331
    invoke-virtual {v1, v2, v3}, LX/IuO;->c(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2626332
    :cond_2
    :goto_2
    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_0

    :cond_3
    move v3, v5

    .line 2626333
    goto :goto_1

    .line 2626334
    :catch_0
    move-exception v3

    .line 2626335
    sget-object v8, LX/IuO;->m:Ljava/lang/String;

    const-string v9, "Failed to send queued message with id %s"

    new-array p0, v4, [Ljava/lang/Object;

    iget-object p1, v2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    aput-object p1, p0, v5

    invoke-static {v8, v3, v9, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2626336
    iget-object v3, v1, LX/IuO;->g:LX/IuN;

    invoke-virtual {v3, v2}, LX/IuN;->b(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_2

    .line 2626337
    :cond_4
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626338
    return-object v0
.end method

.method private o(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2626299
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626300
    const-string v1, "thread_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626301
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2626302
    iget-object v1, p0, LX/IuL;->e:LX/2P4;

    iget-wide v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {v1, v2, v3}, LX/2P4;->b(J)Ljava/lang/String;

    move-result-object v1

    .line 2626303
    if-eqz v1, :cond_0

    invoke-static {p0, v0}, LX/IuL;->b(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/IuO;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/IuO;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2626304
    iget-wide v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {p0, v2, v3, v1}, LX/IuL;->a(LX/IuL;JLjava/lang/String;)V

    .line 2626305
    :cond_0
    iget-object v1, p0, LX/IuL;->q:LX/Iu4;

    .line 2626306
    :try_start_0
    iget-object v2, v1, LX/Iu4;->b:Landroid/content/Context;

    invoke-static {v2, v0}, LX/6bn;->a(Landroid/content/Context;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/io/File;

    move-result-object v2

    .line 2626307
    invoke-static {v2}, LX/BZT;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2626308
    :goto_0
    iget-object v1, p0, LX/IuL;->h:LX/2Ox;

    invoke-virtual {v1, v0}, LX/2Ox;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626309
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2626310
    :catch_0
    move-exception v2

    .line 2626311
    sget-object v3, LX/Iu4;->a:Ljava/lang/String;

    const-string v4, "Failed to delete thread attachment files for %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v0, v5, p1

    invoke-static {v3, v2, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private p(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2626048
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2626049
    const-string v0, "thread_key"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626050
    const-string v2, "timestamp_us"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2626051
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2626052
    iget-object v1, p0, LX/IuL;->e:LX/2P4;

    iget-wide v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {v1, v2, v3}, LX/2P4;->b(J)Ljava/lang/String;

    move-result-object v7

    .line 2626053
    if-eqz v7, :cond_0

    invoke-static {p0, v0}, LX/IuL;->b(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/IuO;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/IuO;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2626054
    iget-object v1, p0, LX/IuL;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v2, p0, LX/IuL;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/IuL;->i:LX/2PJ;

    invoke-virtual {v4}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v4

    iget-wide v5, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    iget-object v10, p0, LX/IuL;->p:LX/IuF;

    invoke-virtual {v10}, LX/IuF;->a()[B

    move-result-object v10

    invoke-virtual/range {v1 .. v10}, Lcom/facebook/messaging/tincan/outbound/Sender;->b(JLjava/lang/String;JLjava/lang/String;J[B)V

    .line 2626055
    :cond_0
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method private q(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    .line 2626210
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626211
    const-string v1, "upload_status"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;

    .line 2626212
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->c:LX/FGR;

    sget-object v2, LX/FGR;->Success:LX/FGR;

    if-ne v1, v2, :cond_0

    .line 2626213
    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 2626214
    iget-object v3, p0, LX/IuL;->f:LX/2P0;

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2P0;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v12

    .line 2626215
    if-nez v12, :cond_2

    .line 2626216
    :goto_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626217
    return-object v0

    .line 2626218
    :cond_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626219
    iget-object v1, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->c:LX/FGR;

    sget-object v2, LX/FGR;->Failure:LX/FGR;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2626220
    iget-object v3, p0, LX/IuL;->f:LX/2P0;

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2P0;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 2626221
    if-nez v4, :cond_d

    .line 2626222
    :goto_2
    goto :goto_0

    .line 2626223
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 2626224
    :cond_2
    iget-object v3, v12, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v3, :cond_a

    .line 2626225
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, v12, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2626226
    :goto_3
    move-object v5, v3

    .line 2626227
    iget-object v3, v0, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a:Ljava/lang/String;

    .line 2626228
    iget-object v4, p0, LX/IuL;->f:LX/2P0;

    .line 2626229
    invoke-static {v4, v3}, LX/2P0;->e(LX/2P0;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v8

    .line 2626230
    if-nez v8, :cond_b

    .line 2626231
    const/4 v6, 0x0

    .line 2626232
    :goto_4
    move-object v4, v6

    .line 2626233
    iget-object v6, p0, LX/IuL;->o:LX/2NB;

    invoke-virtual {v6, v4}, LX/2NB;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    move-object v7, v4

    .line 2626234
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2626235
    iget-object v4, v12, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    move-object v3, p0

    move-object v8, v0

    invoke-static/range {v3 .. v8}, LX/IuL;->a(LX/IuL;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;)Ljava/util/List;

    move-result-object v7

    .line 2626236
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v3

    invoke-virtual {v3, v12}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v3

    .line 2626237
    iput-object v5, v3, LX/6f7;->i:Ljava/util/List;

    .line 2626238
    move-object v3, v3

    .line 2626239
    iput-object v7, v3, LX/6f7;->r:Ljava/util/List;

    .line 2626240
    move-object v3, v3

    .line 2626241
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 2626242
    iget-object v3, p0, LX/IuL;->v:LX/IuT;

    invoke-virtual {v3, v5}, LX/IuT;->a(Lcom/facebook/messaging/model/messages/Message;)LX/IuS;

    move-result-object v8

    .line 2626243
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v3, v9

    .line 2626244
    :goto_5
    iget-object v4, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v4}, LX/IuL;->b(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/IuO;

    move-result-object v13

    .line 2626245
    iget-object v4, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v13, v4, v9}, LX/IuO;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    move v4, v10

    .line 2626246
    :goto_6
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_7

    if-nez v4, :cond_7

    .line 2626247
    :goto_7
    if-eqz v10, :cond_8

    iget-object v7, v12, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v9, LX/2uW;->FAILED_SEND:LX/2uW;

    if-ne v7, v9, :cond_8

    .line 2626248
    iget-object v4, v5, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v7, LX/2uW;->FAILED_SEND:LX/2uW;

    if-ne v4, v7, :cond_c

    const/4 v4, 0x1

    :goto_8
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 2626249
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v4

    sget-object v7, LX/2uW;->PENDING_SEND:LX/2uW;

    .line 2626250
    iput-object v7, v4, LX/6f7;->l:LX/2uW;

    .line 2626251
    move-object v4, v4

    .line 2626252
    sget-object v7, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    .line 2626253
    iput-object v7, v4, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 2626254
    move-object v4, v4

    .line 2626255
    invoke-virtual {v4}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    move-object v4, v4

    .line 2626256
    :goto_9
    iget-object v5, p0, LX/IuL;->h:LX/2Ox;

    iget-object v7, v8, LX/IuS;->a:[B

    invoke-virtual {v5, v4, v7, v3}, LX/2Ox;->a(Lcom/facebook/messaging/model/messages/Message;[BLjava/lang/String;)V

    .line 2626257
    if-eqz v10, :cond_3

    .line 2626258
    invoke-virtual {v13, v4, v8}, LX/IuO;->c(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)V

    .line 2626259
    :cond_3
    iget-object v3, p0, LX/IuL;->D:LX/IuN;

    invoke-virtual {v3, v4}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626260
    iget-object v3, p0, LX/IuL;->m:LX/2Ow;

    iget-object v4, v12, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, v4}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626261
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2626262
    iget-object v5, p0, LX/IuL;->y:LX/2MO;

    invoke-virtual {v5, v3}, LX/2MO;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v5

    .line 2626263
    iget-object v6, p0, LX/IuL;->y:LX/2MO;

    invoke-virtual {v6, v3}, LX/2MO;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v6

    .line 2626264
    invoke-static {p0, v3}, LX/IuL;->b(LX/IuL;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2626265
    invoke-static {p0, v5}, LX/IuL;->b(LX/IuL;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2626266
    invoke-static {p0, v6}, LX/IuL;->b(LX/IuL;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2626267
    goto :goto_a

    .line 2626268
    :cond_4
    goto/16 :goto_0

    .line 2626269
    :cond_5
    iget-object v3, p0, LX/IuL;->o:LX/2NB;

    invoke-virtual {v3, v7}, LX/2NB;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    :cond_6
    move v4, v11

    .line 2626270
    goto :goto_6

    :cond_7
    move v10, v11

    .line 2626271
    goto :goto_7

    .line 2626272
    :cond_8
    if-eqz v4, :cond_9

    .line 2626273
    invoke-virtual {v13, v5}, LX/IuO;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    goto :goto_9

    :cond_9
    move-object v4, v5

    goto :goto_9

    :cond_a
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_3

    .line 2626274
    :cond_b
    iget-object v6, v4, LX/2P0;->i:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Dom;

    iget-object v7, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v6, v7}, LX/Dom;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v7

    .line 2626275
    iget-object v13, v4, LX/2P0;->h:LX/2Oy;

    iget-object v6, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, [B

    invoke-virtual {v13, v7, v6}, LX/2Oy;->c([B[B)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_4

    .line 2626276
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_8

    .line 2626277
    :cond_d
    iget-object v3, v4, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v5, LX/2uW;->REGULAR:LX/2uW;

    if-eq v3, v5, :cond_e

    const/4 v3, 0x1

    :goto_b
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2626278
    new-instance v3, LX/6fO;

    invoke-direct {v3}, LX/6fO;-><init>()V

    sget-object v5, LX/6fP;->MEDIA_UPLOAD_FAILED:LX/6fP;

    .line 2626279
    iput-object v5, v3, LX/6fO;->a:LX/6fP;

    .line 2626280
    move-object v3, v3

    .line 2626281
    sget-object v5, LX/Doq;->a:LX/Doq;

    move-object v5, v5

    .line 2626282
    invoke-virtual {v5}, LX/Doq;->a()J

    move-result-wide v5

    .line 2626283
    iput-wide v5, v3, LX/6fO;->c:J

    .line 2626284
    move-object v5, v3

    .line 2626285
    iget-object v3, p0, LX/IuL;->n:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0806c1

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2626286
    iput-object v3, v5, LX/6fO;->b:Ljava/lang/String;

    .line 2626287
    move-object v3, v5

    .line 2626288
    invoke-virtual {v3}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v3

    .line 2626289
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v4

    sget-object v5, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 2626290
    iput-object v5, v4, LX/6f7;->l:LX/2uW;

    .line 2626291
    move-object v4, v4

    .line 2626292
    iput-object v3, v4, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 2626293
    move-object v4, v4

    .line 2626294
    invoke-virtual {v4}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 2626295
    iget-object v5, p0, LX/IuL;->h:LX/2Ox;

    iget-object v6, v4, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v3}, LX/2Ox;->a(Ljava/lang/String;Lcom/facebook/messaging/model/send/SendError;)V

    .line 2626296
    iget-object v3, p0, LX/IuL;->D:LX/IuN;

    invoke-virtual {v3, v4}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626297
    iget-object v3, p0, LX/IuL;->m:LX/2Ow;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, v4}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto/16 :goto_2

    .line 2626298
    :cond_e
    const/4 v3, 0x0

    goto :goto_b
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 2626056
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2626057
    const-string v1, "TincanNewMessage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2626058
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626059
    const-string v1, "packet_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2626060
    new-instance v2, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;

    const-string p1, "message_data"

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-direct {v2, v1, p1}, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;-><init>(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    move-object v1, v2

    .line 2626061
    iget-object v0, p0, LX/IuL;->c:LX/Dp4;

    invoke-virtual {v0, v1}, LX/Dp4;->a(Lcom/facebook/messaging/tincan/omnistore/TincanMessage;)V

    .line 2626062
    iget-object v0, p0, LX/IuL;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cT;

    iget-object v1, v1, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2cT;->a(Ljava/lang/String;)V

    .line 2626063
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626064
    move-object v0, v0

    .line 2626065
    :goto_0
    return-object v0

    .line 2626066
    :cond_0
    const-string v1, "TincanSendMessage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2626067
    invoke-static {p1}, LX/IuL;->c(LX/1qK;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2626068
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v1}, LX/IuL;->b(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/IuO;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/IuO;->b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2626069
    goto :goto_0

    .line 2626070
    :cond_1
    const-string v1, "UpdateUploadStatus"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2626071
    invoke-direct {p0, p1}, LX/IuL;->q(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2626072
    :cond_2
    const-string v1, "TincanDeleteThread"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2626073
    invoke-direct {p0, p1}, LX/IuL;->o(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2626074
    :cond_3
    const-string v1, "TincanSendReadReceipt"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2626075
    invoke-direct {p0, p1}, LX/IuL;->p(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2626076
    :cond_4
    const-string v1, "TincanAdminMessage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2626077
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2626078
    const-string v0, "thread_key"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626079
    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2626080
    invoke-static {p0, v0, v1}, LX/IuL;->a(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/6f7;

    move-result-object v0

    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2626081
    invoke-static {p0, v0}, LX/IuL;->a(LX/IuL;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626082
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2626083
    goto :goto_0

    .line 2626084
    :cond_5
    const-string v1, "TincanSetSalamanderError"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2626085
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626086
    iget-object v1, p0, LX/IuL;->f:LX/2P0;

    const-string v2, "message_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2P0;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2626087
    if-eqz v0, :cond_6

    .line 2626088
    iget-object v1, p0, LX/IuL;->D:LX/IuN;

    invoke-virtual {v1, v0}, LX/IuN;->b(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626089
    :cond_6
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626090
    move-object v0, v0

    .line 2626091
    goto/16 :goto_0

    .line 2626092
    :cond_7
    const-string v1, "TincanSetRetryableSendError"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2626093
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626094
    iget-object v1, p0, LX/IuL;->f:LX/2P0;

    const-string v2, "message_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2P0;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2626095
    const-string v2, "error_text"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2626096
    if-eqz v1, :cond_8

    .line 2626097
    iget-object v2, p0, LX/IuL;->w:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2626098
    iget-object v2, p0, LX/IuL;->D:LX/IuN;

    invoke-virtual {v2, v0}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626099
    iget-object v0, p0, LX/IuL;->m:LX/2Ow;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626100
    :cond_8
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626101
    move-object v0, v0

    .line 2626102
    goto/16 :goto_0

    .line 2626103
    :cond_9
    const-string v1, "TincanPostSendMessageUpdate"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2626104
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v4, v4

    .line 2626105
    iget-object v5, p0, LX/IuL;->f:LX/2P0;

    const-string v6, "message_id"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/2P0;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 2626106
    if-eqz v5, :cond_b

    .line 2626107
    const-string v6, "timestamp_us"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    .line 2626108
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v8

    .line 2626109
    iput-wide v6, v8, LX/6f7;->c:J

    .line 2626110
    move-object v8, v8

    .line 2626111
    iget-object v9, v5, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v10, LX/2uW;->PENDING_SEND:LX/2uW;

    if-ne v9, v10, :cond_a

    .line 2626112
    sget-object v9, LX/2uW;->REGULAR:LX/2uW;

    .line 2626113
    iput-object v9, v8, LX/6f7;->l:LX/2uW;

    .line 2626114
    iget-object v9, p0, LX/IuL;->h:LX/2Ox;

    iget-object v10, v5, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 2626115
    iget-object v11, v8, LX/6f7;->l:LX/2uW;

    move-object v11, v11

    .line 2626116
    invoke-virtual {v9, v10, v11}, LX/2Ox;->a(Ljava/lang/String;LX/2uW;)V

    .line 2626117
    :cond_a
    invoke-virtual {v8}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v8

    .line 2626118
    iget-object v9, p0, LX/IuL;->h:LX/2Ox;

    iget-object v10, v8, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v9, v10, v6, v7}, LX/2Ox;->b(Ljava/lang/String;J)V

    .line 2626119
    const-string v6, "facebook_hmac"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v4

    .line 2626120
    iget-object v6, p0, LX/IuL;->h:LX/2Ox;

    iget-object v7, v8, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v4}, LX/2Ox;->b(Ljava/lang/String;[B)V

    .line 2626121
    iget-object v4, p0, LX/IuL;->D:LX/IuN;

    invoke-virtual {v4, v8}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626122
    iget-object v4, p0, LX/IuL;->m:LX/2Ow;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v5}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626123
    :cond_b
    sget-object v4, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v4, v4

    .line 2626124
    move-object v0, v4

    .line 2626125
    goto/16 :goto_0

    .line 2626126
    :cond_c
    const-string v1, "TincanSetPrimaryDevice"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2626127
    invoke-direct {p0}, LX/IuL;->a()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 2626128
    :cond_d
    const-string v1, "TincanSetNonPrimaryDevice"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2626129
    invoke-direct {p0}, LX/IuL;->b()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 2626130
    :cond_e
    const-string v1, "TincanDeviceBecameNonPrimary"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2626131
    iget-object v0, p0, LX/IuL;->h:LX/2Ox;

    invoke-virtual {v0}, LX/2Ox;->a()V

    .line 2626132
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/IuL;->a(LX/IuL;Z)V

    .line 2626133
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626134
    move-object v0, v0

    .line 2626135
    goto/16 :goto_0

    .line 2626136
    :cond_f
    const-string v1, "TincanSentMessageToNonPrimaryDevice"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2626137
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v4, v4

    .line 2626138
    const-string v5, "message_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2626139
    iget-object v5, p0, LX/IuL;->f:LX/2P0;

    invoke-virtual {v5, v4}, LX/2P0;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 2626140
    if-nez v5, :cond_17

    .line 2626141
    sget-object v4, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v4, v4

    .line 2626142
    :goto_1
    move-object v0, v4

    .line 2626143
    goto/16 :goto_0

    .line 2626144
    :cond_10
    const-string v1, "TincanOtherDeviceSwitched"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 2626145
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v4, v4

    .line 2626146
    const-string v5, "from_user_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2626147
    const-string v5, "timestamp_us"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2626148
    const-wide/16 v8, 0x3e8

    div-long/2addr v4, v8

    invoke-static {p0, v6, v7, v4, v5}, LX/IuL;->a(LX/IuL;JJ)V

    .line 2626149
    sget-object v4, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v4, v4

    .line 2626150
    move-object v0, v4

    .line 2626151
    goto/16 :goto_0

    .line 2626152
    :cond_11
    const-string v1, "TincanProcessNewPreKey"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2626153
    invoke-direct {p0, p1}, LX/IuL;->l(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 2626154
    :cond_12
    const-string v1, "FetchRawMessageContent"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2626155
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2626156
    const-string v0, "thread_key"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626157
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626158
    const-string v2, "count"

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2626159
    iget-object v2, p0, LX/IuL;->f:LX/2P0;

    invoke-virtual {v2, v0, v1}, LX/2P0;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)LX/0Px;

    move-result-object v0

    .line 2626160
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2626161
    goto/16 :goto_0

    .line 2626162
    :cond_13
    const-string v1, "TincanRetrySendMessage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 2626163
    invoke-static {p1}, LX/IuL;->c(LX/1qK;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 2626164
    iget-object v5, v4, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v5}, LX/IuL;->b(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/IuO;

    move-result-object v5

    .line 2626165
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v6

    .line 2626166
    sget-object v7, LX/Doq;->a:LX/Doq;

    move-object v7, v7

    .line 2626167
    invoke-virtual {v7}, LX/Doq;->a()J

    move-result-wide v8

    .line 2626168
    iput-wide v8, v6, LX/6f7;->c:J

    .line 2626169
    move-object v7, v6

    .line 2626170
    iput-wide v8, v7, LX/6f7;->d:J

    .line 2626171
    move-object v7, v7

    .line 2626172
    invoke-virtual {v7}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    .line 2626173
    invoke-virtual {v6}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v6

    .line 2626174
    iget-object v7, v5, LX/IuO;->p:LX/DoY;

    iget-object v8, v4, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, LX/DoY;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_19

    .line 2626175
    invoke-virtual {v5, v6}, LX/IuO;->b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v6

    .line 2626176
    :goto_2
    move-object v4, v6

    .line 2626177
    move-object v0, v4

    .line 2626178
    goto/16 :goto_0

    .line 2626179
    :cond_14
    const-string v1, "TincanAdminMessageForMessage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 2626180
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2626181
    const-string v1, "message_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2626182
    iget-object v2, p0, LX/IuL;->f:LX/2P0;

    invoke-virtual {v2, v1}, LX/2P0;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2626183
    if-nez v1, :cond_1a

    .line 2626184
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626185
    :goto_3
    move-object v0, v0

    .line 2626186
    goto/16 :goto_0

    .line 2626187
    :cond_15
    const-string v1, "CompletePrekeyDelivery"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 2626188
    invoke-direct {p0, p1}, LX/IuL;->m(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 2626189
    :cond_16
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2626190
    :cond_17
    iget-object v6, p0, LX/IuL;->w:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    iget-object v4, p0, LX/IuL;->n:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0806c5

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v5, v4}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 2626191
    iget-object v6, p0, LX/IuL;->D:LX/IuN;

    invoke-virtual {v6, v4}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626192
    iget-object v4, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v4}, LX/IuL;->b(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/IuO;

    move-result-object v4

    iget-object v6, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, LX/IuO;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 2626193
    iget-object v4, p0, LX/IuL;->h:LX/2Ox;

    iget-object v6, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v6}, LX/2Ox;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626194
    iget-object v4, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v6, v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 2626195
    sget-object v4, LX/Doq;->a:LX/Doq;

    move-object v4, v4

    .line 2626196
    invoke-virtual {v4}, LX/Doq;->a()J

    move-result-wide v8

    invoke-static {p0, v6, v7, v8, v9}, LX/IuL;->a(LX/IuL;JJ)V

    .line 2626197
    :cond_18
    iget-object v4, p0, LX/IuL;->m:LX/2Ow;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v5}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626198
    sget-object v4, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v4, v4

    .line 2626199
    goto/16 :goto_1

    .line 2626200
    :cond_19
    iget-object v7, v5, LX/IuO;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v8, v6, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v7, v8}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626201
    invoke-static {v5, v6}, LX/IuO;->d(LX/IuO;Lcom/facebook/messaging/model/messages/Message;)LX/IuS;

    move-result-object v7

    .line 2626202
    iget-object v8, v5, LX/IuO;->h:LX/2NB;

    iget-object v9, v6, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v8, v9}, LX/2NB;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    .line 2626203
    iget-object v9, v5, LX/IuO;->g:LX/IuN;

    iget-object v7, v7, LX/IuS;->a:[B

    invoke-virtual {v9, v6, v7, v8}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;[BLjava/lang/String;)V

    .line 2626204
    invoke-static {v6}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v6

    goto/16 :goto_2

    .line 2626205
    :cond_1a
    const-string v2, "message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2626206
    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v1, v0}, LX/IuL;->a(LX/IuL;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/6f7;

    move-result-object v0

    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2626207
    invoke-static {p0, v0}, LX/IuL;->a(LX/IuL;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626208
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2626209
    goto/16 :goto_3
.end method
