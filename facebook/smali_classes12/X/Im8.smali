.class public LX/Im8;
.super LX/7G8;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final l:Ljava/lang/Object;


# instance fields
.field public final b:LX/ImA;

.field private final c:LX/Im5;

.field public final d:LX/ImB;

.field private final e:LX/IzM;

.field private final f:LX/7G1;

.field private final g:LX/2Sx;

.field public final h:LX/03V;

.field private final i:LX/7GB;

.field public final j:Ljava/lang/String;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Ge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2608966
    const-class v0, LX/Im8;

    sput-object v0, LX/Im8;->a:Ljava/lang/Class;

    .line 2608967
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Im8;->l:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/6Po;LX/7Ge;LX/0Ot;Ljava/util/concurrent/ScheduledExecutorService;LX/ImA;LX/Im5;LX/ImB;LX/0SG;LX/IzM;LX/7G1;LX/2Sx;LX/03V;LX/7GB;Ljava/lang/String;)V
    .locals 12
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p14    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6Po;",
            "LX/7Ge;",
            "LX/0Ot",
            "<",
            "LX/7Ge;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/ImA;",
            "LX/Im5;",
            "LX/ImB;",
            "LX/0SG;",
            "LX/IzM;",
            "LX/7G1;",
            "LX/2Sx;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/7GB;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2608924
    move-object v1, p0

    move-object/from16 v2, p13

    move-object/from16 v3, p5

    move-object/from16 v4, p11

    move-object/from16 v5, p10

    move-object v6, p1

    move-object/from16 v7, p8

    move-object v8, p2

    move-object/from16 v9, p4

    move-object/from16 v10, p14

    move-object/from16 v11, p6

    invoke-direct/range {v1 .. v11}, LX/7G8;-><init>(LX/7GB;LX/7GD;LX/2Sx;LX/7G1;LX/6Po;LX/0SG;LX/7Ge;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/String;LX/7Fx;)V

    .line 2608925
    move-object/from16 v0, p5

    iput-object v0, p0, LX/Im8;->b:LX/ImA;

    .line 2608926
    move-object/from16 v0, p6

    iput-object v0, p0, LX/Im8;->c:LX/Im5;

    .line 2608927
    move-object/from16 v0, p7

    iput-object v0, p0, LX/Im8;->d:LX/ImB;

    .line 2608928
    move-object/from16 v0, p9

    iput-object v0, p0, LX/Im8;->e:LX/IzM;

    .line 2608929
    move-object/from16 v0, p10

    iput-object v0, p0, LX/Im8;->f:LX/7G1;

    .line 2608930
    move-object/from16 v0, p11

    iput-object v0, p0, LX/Im8;->g:LX/2Sx;

    .line 2608931
    move-object/from16 v0, p12

    iput-object v0, p0, LX/Im8;->h:LX/03V;

    .line 2608932
    move-object/from16 v0, p13

    iput-object v0, p0, LX/Im8;->i:LX/7GB;

    .line 2608933
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Im8;->j:Ljava/lang/String;

    .line 2608934
    iput-object p3, p0, LX/Im8;->k:LX/0Ot;

    .line 2608935
    return-void
.end method

.method public static a(LX/0QB;)LX/Im8;
    .locals 7

    .prologue
    .line 2608939
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2608940
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2608941
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2608942
    if-nez v1, :cond_0

    .line 2608943
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2608944
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2608945
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2608946
    sget-object v1, LX/Im8;->l:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2608947
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2608948
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2608949
    :cond_1
    if-nez v1, :cond_4

    .line 2608950
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2608951
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2608952
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Im8;->b(LX/0QB;)LX/Im8;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2608953
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2608954
    if-nez v1, :cond_2

    .line 2608955
    sget-object v0, LX/Im8;->l:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Im8;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2608956
    :goto_1
    if-eqz v0, :cond_3

    .line 2608957
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2608958
    :goto_3
    check-cast v0, LX/Im8;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2608959
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2608960
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2608961
    :catchall_1
    move-exception v0

    .line 2608962
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2608963
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2608964
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2608965
    :cond_2
    :try_start_8
    sget-object v0, LX/Im8;->l:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Im8;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Im8;
    .locals 15

    .prologue
    .line 2608968
    new-instance v0, LX/Im8;

    invoke-static {p0}, LX/6Po;->a(LX/0QB;)LX/6Po;

    move-result-object v1

    check-cast v1, LX/6Po;

    invoke-static {p0}, LX/7Ge;->a(LX/0QB;)LX/7Ge;

    move-result-object v2

    check-cast v2, LX/7Ge;

    const/16 v3, 0x35ca

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/ImA;->a(LX/0QB;)LX/ImA;

    move-result-object v5

    check-cast v5, LX/ImA;

    invoke-static {p0}, LX/Im5;->a(LX/0QB;)LX/Im5;

    move-result-object v6

    check-cast v6, LX/Im5;

    invoke-static {p0}, LX/ImB;->a(LX/0QB;)LX/ImB;

    move-result-object v7

    check-cast v7, LX/ImB;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {p0}, LX/IzM;->a(LX/0QB;)LX/IzM;

    move-result-object v9

    check-cast v9, LX/IzM;

    invoke-static {p0}, LX/7G1;->a(LX/0QB;)LX/7G1;

    move-result-object v10

    check-cast v10, LX/7G1;

    invoke-static {p0}, LX/2Sx;->a(LX/0QB;)LX/2Sx;

    move-result-object v11

    check-cast v11, LX/2Sx;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {p0}, LX/7GB;->a(LX/0QB;)LX/7GB;

    move-result-object v13

    check-cast v13, LX/7GB;

    invoke-static {p0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-direct/range {v0 .. v14}, LX/Im8;-><init>(LX/6Po;LX/7Ge;LX/0Ot;Ljava/util/concurrent/ScheduledExecutorService;LX/ImA;LX/Im5;LX/ImB;LX/0SG;LX/IzM;LX/7G1;LX/2Sx;LX/03V;LX/7GB;Ljava/lang/String;)V

    .line 2608969
    return-object v0
.end method

.method private b()Ljava/lang/Long;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2608936
    new-instance v6, Lcom/facebook/messaging/payment/sync/connection/PaymentsSyncConnectionHandler$2;

    invoke-direct {v6, p0}, Lcom/facebook/messaging/payment/sync/connection/PaymentsSyncConnectionHandler$2;-><init>(LX/Im8;)V

    .line 2608937
    iget-object v0, p0, LX/Im8;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Ge;

    const-wide/32 v2, 0x493e0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v1 .. v6}, LX/7Ge;->a(JJLX/7G5;)LX/7Gd;

    move-result-object v0

    .line 2608938
    invoke-virtual {v0}, LX/7Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2608903
    iget-object v0, p0, LX/Im8;->f:LX/7G1;

    sget-object v1, LX/7GT;->PAYMENTS_QUEUE_TYPE:LX/7GT;

    invoke-virtual {v0, v1, p1}, LX/7G1;->a(LX/7GT;Lcom/facebook/sync/analytics/FullRefreshReason;)V

    .line 2608904
    invoke-direct {p0}, LX/Im8;->b()Ljava/lang/Long;

    move-result-object v1

    .line 2608905
    if-nez v1, :cond_0

    .line 2608906
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to fetch initial payment sequence id from the server.  viewerContextUserId = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/Im8;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2608907
    sget-object v1, LX/Im8;->a:Ljava/lang/Class;

    invoke-static {v1, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2608908
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2608909
    :goto_0
    return-object v0

    .line 2608910
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, LX/Im8;->j:Ljava/lang/String;

    .line 2608911
    new-instance v12, LX/Im7;

    invoke-direct {v12, p0, v2, v3, v0}, LX/Im7;-><init>(LX/Im8;JLjava/lang/String;)V

    .line 2608912
    iget-object v6, p0, LX/Im8;->k:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/7Ge;

    const-wide/32 v8, 0x493e0

    const-wide/16 v10, 0xfa

    invoke-virtual/range {v7 .. v12}, LX/7Ge;->a(JJLX/7G5;)LX/7Gd;

    move-result-object v6

    .line 2608913
    invoke-virtual {v6}, LX/7Gd;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/76M;

    move-object v2, v6

    .line 2608914
    iget-boolean v0, v2, LX/76M;->a:Z

    if-nez v0, :cond_1

    .line 2608915
    sget-object v0, LX/Im8;->a:Ljava/lang/Class;

    const-string v3, "Failed to create payment queue with sequenceId %d, viewerContextUserId = %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    iget-object v5, p0, LX/Im8;->j:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2608916
    invoke-virtual {v2}, LX/76M;->a()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2608917
    :cond_1
    iget-object v0, v2, LX/76M;->b:Ljava/lang/Object;

    check-cast v0, LX/7GC;

    iget-object v0, v0, LX/7GC;->b:Ljava/lang/String;

    .line 2608918
    iget-object v3, p0, LX/Im8;->e:LX/IzM;

    sget-object v4, LX/IzL;->e:LX/IzK;

    invoke-virtual {v3, v4, v0}, LX/2Iu;->b(LX/0To;Ljava/lang/String;)V

    .line 2608919
    iget-object v0, p0, LX/Im8;->e:LX/IzM;

    sget-object v3, LX/IzL;->f:LX/IzK;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, LX/2Iu;->b(LX/0To;J)V

    .line 2608920
    iget-object v0, p0, LX/Im8;->i:LX/7GB;

    iget-object v1, p0, LX/Im8;->c:LX/Im5;

    invoke-virtual {v0, v1}, LX/7GB;->d(LX/7Fx;)V

    .line 2608921
    iget-object v0, p0, LX/Im8;->g:LX/2Sx;

    iget-object v1, p0, LX/Im8;->j:Ljava/lang/String;

    sget-object v3, LX/7GT;->PAYMENTS_QUEUE_TYPE:LX/7GT;

    invoke-static {v1, v3}, LX/7G9;->a(Ljava/lang/String;LX/7GT;)LX/7G9;

    move-result-object v1

    iget-wide v2, v2, LX/76M;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/2Sx;->a(LX/7G9;J)V

    .line 2608922
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2608923
    goto :goto_0
.end method
