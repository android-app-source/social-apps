.class public final LX/HGk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/pages/common/photos/PagePhotosType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2446505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2446506
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/pages/common/photos/PagePhotosType;->valueOf(Ljava/lang/String;)Lcom/facebook/pages/common/photos/PagePhotosType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2446507
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->UNSET_PAGE_PHOTOS_TYPE:Lcom/facebook/pages/common/photos/PagePhotosType;

    goto :goto_0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2446508
    new-array v0, p1, [Lcom/facebook/pages/common/photos/PagePhotosType;

    return-object v0
.end method
