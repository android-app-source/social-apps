.class public LX/I2i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/I2Y;


# static fields
.field public static final a:Ljava/lang/Object;

.field public static final b:Ljava/lang/Object;


# instance fields
.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HyQ;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/I2V",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:LX/Hx7;

.field private final k:LX/0SG;

.field private final l:LX/6RZ;

.field private final m:LX/I2S;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2530732
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2i;->a:Ljava/lang/Object;

    .line 2530733
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2i;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/6RZ;LX/I2S;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2530720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2530721
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2i;->c:Ljava/util/List;

    .line 2530722
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2i;->d:Ljava/util/List;

    .line 2530723
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/I2i;->e:Ljava/util/List;

    .line 2530724
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2i;->f:Ljava/util/List;

    .line 2530725
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I2i;->g:Ljava/util/HashMap;

    .line 2530726
    iput-boolean v2, p0, LX/I2i;->h:Z

    .line 2530727
    iput-boolean v2, p0, LX/I2i;->i:Z

    .line 2530728
    iput-object p1, p0, LX/I2i;->k:LX/0SG;

    .line 2530729
    iput-object p2, p0, LX/I2i;->l:LX/6RZ;

    .line 2530730
    iput-object p3, p0, LX/I2i;->m:LX/I2S;

    .line 2530731
    return-void
.end method

.method public static a(LX/I2i;)V
    .locals 5

    .prologue
    .line 2530697
    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2530698
    iget-boolean v0, p0, LX/I2i;->i:Z

    if-eqz v0, :cond_0

    .line 2530699
    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v1, LX/I2b;->g:LX/I2b;

    sget-object v2, LX/I2Z;->c:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530700
    :goto_0
    return-void

    .line 2530701
    :cond_0
    iget-object v0, p0, LX/I2i;->j:LX/Hx7;

    sget-object v1, LX/Hx7;->DISCOVER:LX/Hx7;

    if-ne v0, v1, :cond_3

    .line 2530702
    invoke-direct {p0}, LX/I2i;->d()V

    .line 2530703
    iget-object v0, p0, LX/I2i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2530704
    :cond_1
    const/4 v2, 0x0

    .line 2530705
    iget-object v0, p0, LX/I2i;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2530706
    :cond_2
    goto :goto_0

    .line 2530707
    :cond_3
    iget-object v0, p0, LX/I2i;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2530708
    iget-object v0, p0, LX/I2i;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p0, v0}, LX/I2i;->b(LX/I2i;I)V

    .line 2530709
    :cond_4
    :goto_1
    invoke-direct {p0}, LX/I2i;->d()V

    goto :goto_0

    .line 2530710
    :cond_5
    iget-object v0, p0, LX/I2i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2530711
    iget-object v2, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v3, LX/I2b;->i:LX/I2b;

    invoke-static {v3, v0}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2530712
    :cond_6
    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v1, LX/I2b;->l:LX/I2b;

    sget-object v3, LX/I2i;->b:Ljava/lang/Object;

    invoke-static {v1, v3}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530713
    iget-object v0, p0, LX/I2i;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HyQ;

    .line 2530714
    iget-object v1, v0, LX/HyQ;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    move-object v1, v1

    .line 2530715
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_7

    .line 2530716
    iget-object v1, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v4, LX/I2b;->j:LX/I2b;

    invoke-static {v4, v0}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    move v1, v2

    .line 2530717
    goto :goto_4

    :cond_9
    move v1, v2

    goto :goto_4

    .line 2530718
    :cond_a
    iget-boolean v0, p0, LX/I2i;->h:Z

    if-nez v0, :cond_4

    .line 2530719
    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v1, LX/I2b;->d:LX/I2b;

    sget-object v2, LX/Hx6;->UPCOMING:LX/Hx6;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/I2i;
    .locals 4

    .prologue
    .line 2530695
    new-instance v3, LX/I2i;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v1

    check-cast v1, LX/6RZ;

    invoke-static {p0}, LX/I2S;->b(LX/0QB;)LX/I2S;

    move-result-object v2

    check-cast v2, LX/I2S;

    invoke-direct {v3, v0, v1, v2}, LX/I2i;-><init>(LX/0SG;LX/6RZ;LX/I2S;)V

    .line 2530696
    return-object v3
.end method

.method public static b(LX/I2i;I)V
    .locals 13

    .prologue
    .line 2530671
    const/4 v7, 0x0

    .line 2530672
    iget-object v0, p0, LX/I2i;->k:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v10

    .line 2530673
    invoke-static {v10, v11}, LX/6Rc;->a(J)LX/6Rc;

    move-result-object v9

    .line 2530674
    invoke-static {v10, v11}, LX/6Rc;->b(J)LX/6Rc;

    move-result-object v12

    .line 2530675
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, p1, :cond_1

    .line 2530676
    iget-object v0, p0, LX/I2i;->c:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/events/model/Event;

    .line 2530677
    invoke-virtual {v6}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v0

    .line 2530678
    invoke-virtual {v6}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    .line 2530679
    invoke-virtual {v9, v0, v1}, LX/6Rc;->c(J)Z

    move-result v3

    .line 2530680
    invoke-virtual {v12, v0, v1}, LX/6Rc;->c(J)Z

    move-result v4

    .line 2530681
    iget-object v5, p0, LX/I2i;->l:LX/6RZ;

    invoke-virtual {v5, v0, v1, v10, v11}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v1

    .line 2530682
    iget-object v0, p0, LX/I2i;->m:LX/I2S;

    move v5, p1

    invoke-virtual/range {v0 .. v5}, LX/I2S;->a(LX/6RY;Ljava/util/Date;ZZI)Ljava/lang/String;

    move-result-object v0

    .line 2530683
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2530684
    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v1, LX/I2b;->b:LX/I2b;

    iget-object v2, p0, LX/I2i;->c:Ljava/util/List;

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v7

    .line 2530685
    :goto_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move-object v7, v0

    goto :goto_0

    .line 2530686
    :cond_0
    iget-object v1, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v2, LX/I2b;->a:LX/I2b;

    invoke-static {v2, v0}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530687
    iget-object v1, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v2, LX/I2b;->b:LX/I2b;

    invoke-static {v2, v6}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2530688
    :cond_1
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2530692
    iget-boolean v0, p0, LX/I2i;->h:Z

    if-eqz v0, :cond_0

    .line 2530693
    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    sget-object v1, LX/I2b;->c:LX/I2b;

    sget-object v2, LX/I2i;->a:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530694
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2530691
    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2V;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2530690
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2530689
    iget-object v0, p0, LX/I2i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
