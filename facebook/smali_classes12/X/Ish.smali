.class public LX/Ish;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/messaging/model/threads/ThreadSummary;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/Isg;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/user/model/User;LX/Isg;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2621192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2621193
    iput-object p1, p0, LX/Ish;->a:Lcom/facebook/user/model/User;

    .line 2621194
    iput-object v0, p0, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2621195
    iput-object p2, p0, LX/Ish;->c:LX/Isg;

    .line 2621196
    iput-object v0, p0, LX/Ish;->d:Ljava/lang/String;

    .line 2621197
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/User;Ljava/lang/String;LX/Isg;)V
    .locals 1

    .prologue
    .line 2621171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2621172
    iput-object p1, p0, LX/Ish;->a:Lcom/facebook/user/model/User;

    .line 2621173
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2621174
    iput-object p3, p0, LX/Ish;->c:LX/Isg;

    .line 2621175
    iput-object p2, p0, LX/Ish;->d:Ljava/lang/String;

    .line 2621176
    return-void
.end method


# virtual methods
.method public final e()J
    .locals 2

    .prologue
    .line 2621198
    iget-object v0, p0, LX/Ish;->a:Lcom/facebook/user/model/User;

    if-eqz v0, :cond_0

    .line 2621199
    iget-object v0, p0, LX/Ish;->a:Lcom/facebook/user/model/User;

    .line 2621200
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2621201
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 2621202
    :goto_0
    return-wide v0

    .line 2621203
    :cond_0
    iget-object v0, p0, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2621204
    iget-object v0, p0, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2621178
    instance-of v2, p1, LX/Ish;

    if-nez v2, :cond_1

    .line 2621179
    :cond_0
    :goto_0
    return v0

    .line 2621180
    :cond_1
    check-cast p1, LX/Ish;

    .line 2621181
    iget-object v2, p0, LX/Ish;->a:Lcom/facebook/user/model/User;

    if-eqz v2, :cond_2

    .line 2621182
    iget-object v2, p1, LX/Ish;->a:Lcom/facebook/user/model/User;

    move-object v2, v2

    .line 2621183
    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Ish;->a:Lcom/facebook/user/model/User;

    .line 2621184
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2621185
    iget-object v3, p1, LX/Ish;->a:Lcom/facebook/user/model/User;

    move-object v3, v3

    .line 2621186
    iget-object p0, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, p0

    .line 2621187
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2621188
    :cond_2
    iget-object v2, p1, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v2

    .line 2621189
    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2621190
    iget-object v3, p1, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v3, v3

    .line 2621191
    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2621177
    invoke-virtual {p0}, LX/Ish;->e()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method
