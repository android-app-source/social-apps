.class public final enum LX/IF3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IF3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IF3;

.field public static final enum CANCEL:LX/IF3;

.field public static final enum FETCH_STYLE_EMPTY:LX/IF3;

.field public static final enum FETCH_STYLE_FAILED:LX/IF3;

.field public static final enum FETCH_STYLE_SUCCESS:LX/IF3;

.field public static final enum PICKER_COLLAPSE:LX/IF3;

.field public static final enum PICKER_EXPAND:LX/IF3;

.field public static final enum POST:LX/IF3;

.field public static final enum STYLE_CHANGED:LX/IF3;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2553537
    new-instance v0, LX/IF3;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v3}, LX/IF3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF3;->CANCEL:LX/IF3;

    .line 2553538
    new-instance v0, LX/IF3;

    const-string v1, "FETCH_STYLE_EMPTY"

    invoke-direct {v0, v1, v4}, LX/IF3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF3;->FETCH_STYLE_EMPTY:LX/IF3;

    .line 2553539
    new-instance v0, LX/IF3;

    const-string v1, "FETCH_STYLE_FAILED"

    invoke-direct {v0, v1, v5}, LX/IF3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF3;->FETCH_STYLE_FAILED:LX/IF3;

    .line 2553540
    new-instance v0, LX/IF3;

    const-string v1, "FETCH_STYLE_SUCCESS"

    invoke-direct {v0, v1, v6}, LX/IF3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF3;->FETCH_STYLE_SUCCESS:LX/IF3;

    .line 2553541
    new-instance v0, LX/IF3;

    const-string v1, "PICKER_COLLAPSE"

    invoke-direct {v0, v1, v7}, LX/IF3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF3;->PICKER_COLLAPSE:LX/IF3;

    .line 2553542
    new-instance v0, LX/IF3;

    const-string v1, "PICKER_EXPAND"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/IF3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF3;->PICKER_EXPAND:LX/IF3;

    .line 2553543
    new-instance v0, LX/IF3;

    const-string v1, "POST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/IF3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF3;->POST:LX/IF3;

    .line 2553544
    new-instance v0, LX/IF3;

    const-string v1, "STYLE_CHANGED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/IF3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF3;->STYLE_CHANGED:LX/IF3;

    .line 2553545
    const/16 v0, 0x8

    new-array v0, v0, [LX/IF3;

    sget-object v1, LX/IF3;->CANCEL:LX/IF3;

    aput-object v1, v0, v3

    sget-object v1, LX/IF3;->FETCH_STYLE_EMPTY:LX/IF3;

    aput-object v1, v0, v4

    sget-object v1, LX/IF3;->FETCH_STYLE_FAILED:LX/IF3;

    aput-object v1, v0, v5

    sget-object v1, LX/IF3;->FETCH_STYLE_SUCCESS:LX/IF3;

    aput-object v1, v0, v6

    sget-object v1, LX/IF3;->PICKER_COLLAPSE:LX/IF3;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/IF3;->PICKER_EXPAND:LX/IF3;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IF3;->POST:LX/IF3;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IF3;->STYLE_CHANGED:LX/IF3;

    aput-object v2, v0, v1

    sput-object v0, LX/IF3;->$VALUES:[LX/IF3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2553546
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IF3;
    .locals 1

    .prologue
    .line 2553547
    const-class v0, LX/IF3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IF3;

    return-object v0
.end method

.method public static values()[LX/IF3;
    .locals 1

    .prologue
    .line 2553548
    sget-object v0, LX/IF3;->$VALUES:[LX/IF3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IF3;

    return-object v0
.end method
