.class public final LX/HP1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HBQ;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/8A4;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V
    .locals 1

    .prologue
    .line 2461227
    iput-object p1, p0, LX/HP1;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2461228
    const/4 v0, 0x0

    iput-object v0, p0, LX/HP1;->b:LX/0Px;

    return-void
.end method


# virtual methods
.method public final a()LX/8A4;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2461229
    iget-object v0, p0, LX/HP1;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HP1;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    .line 2461230
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2461231
    if-nez v0, :cond_1

    .line 2461232
    :cond_0
    const/4 v0, 0x0

    .line 2461233
    :goto_0
    return-object v0

    .line 2461234
    :cond_1
    iget-object v0, p0, LX/HP1;->b:LX/0Px;

    iget-object v1, p0, LX/HP1;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    .line 2461235
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2461236
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 2461237
    iget-object v0, p0, LX/HP1;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    .line 2461238
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2461239
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/HP1;->b:LX/0Px;

    .line 2461240
    new-instance v0, LX/8A4;

    iget-object v1, p0, LX/HP1;->b:LX/0Px;

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LX/HP1;->c:LX/8A4;

    .line 2461241
    :cond_2
    iget-object v0, p0, LX/HP1;->c:LX/8A4;

    goto :goto_0
.end method
