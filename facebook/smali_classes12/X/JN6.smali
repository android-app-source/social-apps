.class public LX/JN6;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JN7;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JN6",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JN7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685194
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2685195
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JN6;->b:LX/0Zi;

    .line 2685196
    iput-object p1, p0, LX/JN6;->a:LX/0Ot;

    .line 2685197
    return-void
.end method

.method public static a(LX/0QB;)LX/JN6;
    .locals 4

    .prologue
    .line 2685200
    const-class v1, LX/JN6;

    monitor-enter v1

    .line 2685201
    :try_start_0
    sget-object v0, LX/JN6;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685202
    sput-object v2, LX/JN6;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685203
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685204
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685205
    new-instance v3, LX/JN6;

    const/16 p0, 0x1ede

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JN6;-><init>(LX/0Ot;)V

    .line 2685206
    move-object v0, v3

    .line 2685207
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685208
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JN6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685209
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685210
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2685211
    check-cast p2, LX/JN5;

    .line 2685212
    iget-object v0, p0, LX/JN6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JN7;

    iget-object v1, p2, LX/JN5;->a:LX/1Pn;

    iget-object v2, p2, LX/JN5;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2685213
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v4

    .line 2685214
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2685215
    check-cast v3, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 2685216
    iput-object v3, v4, LX/3mP;->d:LX/25L;

    .line 2685217
    move-object v4, v4

    .line 2685218
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2685219
    check-cast v3, LX/0jW;

    .line 2685220
    iput-object v3, v4, LX/3mP;->e:LX/0jW;

    .line 2685221
    move-object v3, v4

    .line 2685222
    const/16 v4, 0x8

    .line 2685223
    iput v4, v3, LX/3mP;->b:I

    .line 2685224
    move-object v3, v3

    .line 2685225
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v7

    .line 2685226
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2685227
    move-object v4, v3

    check-cast v4, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    .line 2685228
    iget-object v3, v0, LX/JN7;->b:LX/JND;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n()LX/0Px;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->r()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v4, p1

    move-object v6, v1

    invoke-virtual/range {v3 .. v9}, LX/JND;->a(Landroid/content/Context;LX/0Px;Ljava/lang/Object;LX/25M;Ljava/lang/String;Ljava/lang/String;)LX/JNC;

    move-result-object v3

    .line 2685229
    iget-object v4, v0, LX/JN7;->a:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    const/16 v5, 0xa

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0a00d5

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2685230
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2685198
    invoke-static {}, LX/1dS;->b()V

    .line 2685199
    const/4 v0, 0x0

    return-object v0
.end method
