.class public LX/HkR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static g:[I

.field private static final h:Ljava/lang/String;


# instance fields
.field public final a:LX/HkP;

.field public final b:LX/HkT;

.field public c:Ljava/lang/String;

.field public d:LX/HkU;

.field public e:I

.field public f:I

.field private i:I

.field public j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x14

    new-array v0, v0, [I

    sput-object v0, LX/HkR;->g:[I

    const-class v0, LX/HkR;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HkR;->h:Ljava/lang/String;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    const-string v0, "http.keepAlive"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-le v0, v1, :cond_1

    invoke-static {}, Ljava/net/CookieHandler;->getDefault()Ljava/net/CookieHandler;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/net/CookieManager;

    invoke-direct {v0}, Ljava/net/CookieManager;-><init>()V

    invoke-static {v0}, Ljava/net/CookieHandler;->setDefault(Ljava/net/CookieHandler;)V

    :cond_1
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0}, LX/HkR;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(LX/HkT;Ljava/lang/String;)V
    .locals 1

    new-instance v0, LX/HkQ;

    invoke-direct {v0}, LX/HkQ;-><init>()V

    invoke-direct {p0, p1, p2, v0}, LX/HkR;-><init>(LX/HkT;Ljava/lang/String;LX/HkP;)V

    return-void
.end method

.method private constructor <init>(LX/HkT;Ljava/lang/String;LX/HkP;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, LX/HkR;->c:Ljava/lang/String;

    new-instance v0, LX/HkU;

    invoke-direct {v0}, LX/HkU;-><init>()V

    iput-object v0, p0, LX/HkR;->d:LX/HkU;

    const/16 v0, 0x7d0

    iput v0, p0, LX/HkR;->e:I

    const/16 v0, 0x1f40

    iput v0, p0, LX/HkR;->f:I

    const/4 v0, 0x3

    iput v0, p0, LX/HkR;->i:I

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/HkR;->j:Ljava/util/Map;

    iput-object p2, p0, LX/HkR;->c:Ljava/lang/String;

    iput-object p3, p0, LX/HkR;->a:LX/HkP;

    iput-object p1, p0, LX/HkR;->b:LX/HkT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/HkR;-><init>(Landroid/content/Context;LX/Hk2;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Hk2;)V
    .locals 4

    invoke-direct {p0}, LX/HkR;-><init>()V

    const-string v0, "user-agent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Hk2;->k:LX/Hk2;

    if-eq p2, v2, :cond_0

    sget-object v2, LX/Hk2;->j:LX/Hk2;

    if-eq p2, v2, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const-string v2, "http.agent"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " [FBAN/AudienceNetworkForAndroid;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "FBSN/Android"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";FBSV/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/Hk1;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";FBAB/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/Hk1;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";FBAV/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/Hk1;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";FBBV/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, LX/Hk1;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";FBLC/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HkR;->j:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, LX/Hko;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LX/Hko;->b()V

    :cond_1
    return-void

    :cond_2
    sget-object v2, LX/Hkp;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_3

    :try_start_0
    invoke-static {p1}, Landroid/webkit/WebSettings;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    sput-object v2, LX/Hkp;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_3
    :try_start_1
    const-string v2, "android.webkit.WebSettings"

    const-string v3, "android.webkit.WebView"

    invoke-static {p1, v2, v3}, LX/Hkp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, LX/Hkp;->a:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_4
    :goto_1
    sget-object v2, LX/Hkp;->a:Ljava/lang/String;

    goto/16 :goto_0

    :catch_1
    :try_start_2
    const-string v2, "android.webkit.WebSettingsClassic"

    const-string v3, "android.webkit.WebViewClassic"

    invoke-static {p1, v2, v3}, LX/Hkp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, LX/Hkp;->a:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    new-instance v2, Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, LX/Hkp;->a:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/webkit/WebView;->destroy()V

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    new-instance v0, LX/HkT;

    invoke-direct {v0}, LX/HkT;-><init>()V

    invoke-direct {p0, v0, p1}, LX/HkR;-><init>(LX/HkT;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/net/HttpURLConnection;[B)I
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    const v2, 0x6e13fdc0

    invoke-static {p1, v2}, LX/04e;->c(Ljava/net/URLConnection;I)Ljava/io/OutputStream;

    move-result-object v2

    move-object v1, v2

    if-eqz v1, :cond_0

    invoke-virtual {v1, p2}, Ljava/io/OutputStream;->write([B)V

    :cond_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    return v0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_1
    throw v0

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_1
.end method

.method private static a(LX/HkR;Ljava/lang/String;LX/HkY;Ljava/lang/String;[B)LX/Hkb;
    .locals 6

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/HkR;->k:Z

    invoke-direct {p0, p1}, LX/HkR;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-result-object v2

    :try_start_1
    invoke-direct {p0, v2, p2, p3}, LX/HkR;->a(Ljava/net/HttpURLConnection;LX/HkY;Ljava/lang/String;)V

    iget-object v0, p0, LX/HkR;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, LX/HkR;->j:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    goto :goto_1

    :goto_1
    const v0, 0x705be586

    invoke-static {v2, v0}, LX/04e;->a(Ljava/net/URLConnection;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HkR;->k:Z

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getDoOutput()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p4, :cond_1

    invoke-direct {p0, v2, p4}, LX/HkR;->a(Ljava/net/HttpURLConnection;[B)I

    :cond_1
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getDoInput()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v2}, LX/HkR;->b(Ljava/net/HttpURLConnection;)LX/Hkb;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    move-result-object v0

    :goto_2
    goto :goto_3

    :goto_3
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    :goto_4
    return-object v0

    :cond_3
    :try_start_2
    new-instance v0, LX/Hkb;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, LX/Hkb;-><init>(Ljava/net/HttpURLConnection;[B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v3, v2

    move-object v2, v0

    :goto_5
    :try_start_3
    invoke-direct {p0, v3}, LX/HkR;->c(Ljava/net/HttpURLConnection;)LX/Hkb;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v0

    if-eqz v0, :cond_4

    :try_start_4
    iget v1, v0, LX/Hkb;->a:I

    move v1, v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-lez v1, :cond_4

    goto :goto_6

    :goto_6
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_4

    :cond_4
    :try_start_5
    new-instance v1, LX/Hka;

    invoke-direct {v1, v2, v0}, LX/Hka;-><init>(Ljava/lang/Exception;LX/Hkb;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v1

    move-object v2, v3

    move-object v0, v1

    :goto_7
    goto :goto_8

    :goto_8
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    throw v0

    :catch_1
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v1, :cond_7

    :try_start_7
    iget v0, v4, LX/Hkb;->a:I

    move v0, v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-lez v0, :cond_7

    goto :goto_9

    :goto_9
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_6
    move-object v0, v1

    goto :goto_4

    :cond_7
    :try_start_8
    new-instance v0, LX/Hka;

    invoke-direct {v0, v2, v4}, LX/Hka;-><init>(Ljava/lang/Exception;LX/Hkb;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_7

    :catchall_2
    if-eqz v1, :cond_9

    iget v0, v4, LX/Hkb;->a:I

    move v0, v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-lez v0, :cond_9

    goto :goto_a

    :goto_a
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_8
    move-object v0, v1

    goto :goto_4

    :cond_9
    :try_start_9
    new-instance v0, LX/Hka;

    invoke-direct {v0, v2, v4}, LX/Hka;-><init>(Ljava/lang/Exception;LX/Hkb;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_7

    :catchall_4
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    move-object v2, v0

    move-object v3, v1

    goto :goto_5
.end method

.method private a(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/HkR;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    move-object v0, v2

    return-object v0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not a valid URL"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private a(Ljava/net/HttpURLConnection;LX/HkY;Ljava/lang/String;)V
    .locals 2

    iget v0, p0, LX/HkR;->e:I

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    iget v0, p0, LX/HkR;->f:I

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    invoke-virtual {p2}, LX/HkY;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/HkY;->b()Z

    move-result v1

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {p2}, LX/HkY;->a()Z

    move-result v1

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    if-eqz p3, :cond_0

    const-string v1, "Content-Type"

    invoke-virtual {p1, v1, p3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v1, "Accept-Charset"

    const-string p0, "UTF-8"

    invoke-virtual {p1, v1, p0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static b(LX/HkR;LX/HkW;)LX/Hkb;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p1, LX/HkW;->a:Ljava/lang/String;

    move-object v1, v1

    iget-object v2, p1, LX/HkW;->b:LX/HkY;

    move-object v2, v2

    iget-object v3, p1, LX/HkW;->c:Ljava/lang/String;

    move-object v3, v3

    iget-object v4, p1, LX/HkW;->d:[B

    move-object v4, v4

    invoke-static {p0, v1, v2, v3, v4}, LX/HkR;->a(LX/HkR;Ljava/lang/String;LX/HkY;Ljava/lang/String;[B)LX/Hkb;
    :try_end_0
    .catch LX/Hka; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    iget-object v2, p0, LX/HkR;->a:LX/HkP;

    invoke-virtual {v2, v1}, LX/HkP;->a(LX/Hka;)Z

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v2, p0, LX/HkR;->a:LX/HkP;

    new-instance v3, LX/Hka;

    invoke-direct {v3, v1, v0}, LX/Hka;-><init>(Ljava/lang/Exception;LX/Hkb;)V

    invoke-virtual {v2, v3}, LX/HkP;->a(LX/Hka;)Z

    goto :goto_0
.end method

.method private b(Ljava/net/HttpURLConnection;)LX/Hkb;
    .locals 3

    const/4 v2, 0x0

    :try_start_0
    const v1, 0x3009b4ef

    invoke-static {p1, v1}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v1

    move-object v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, LX/HkR;->a:LX/HkP;

    invoke-virtual {v0, v1}, LX/HkP;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    :cond_0
    new-instance v0, LX/Hkb;

    invoke-direct {v0, p1, v2}, LX/Hkb;-><init>(Ljava/net/HttpURLConnection;[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private c(Ljava/net/HttpURLConnection;)LX/Hkb;
    .locals 3

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, LX/HkR;->a:LX/HkP;

    invoke-virtual {v0, v1}, LX/HkP;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    :cond_0
    new-instance v0, LX/Hkb;

    invoke-direct {v0, p1, v2}, LX/Hkb;-><init>(Ljava/net/HttpURLConnection;[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/HkW;)LX/Hkb;
    .locals 14

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    iget v3, p0, LX/HkR;->i:I

    if-ge v2, v3, :cond_4

    :try_start_0
    sget-object v3, LX/HkR;->g:[I

    add-int/lit8 v4, v2, 0x2

    aget v3, v3, v4

    mul-int/lit16 v3, v3, 0x3e8

    move v3, v3

    iput v3, p0, LX/HkR;->e:I

    goto :goto_1

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v3, p1, LX/HkW;->a:Ljava/lang/String;

    move-object v3, v3

    iget-object v4, p1, LX/HkW;->b:LX/HkY;

    move-object v4, v4

    iget-object v5, p1, LX/HkW;->c:Ljava/lang/String;

    move-object v5, v5

    iget-object v6, p1, LX/HkW;->d:[B

    move-object v6, v6

    invoke-static {p0, v3, v4, v5, v6}, LX/HkR;->a(LX/HkR;Ljava/lang/String;LX/HkY;Ljava/lang/String;[B)LX/Hkb;
    :try_end_0
    .catch LX/Hka; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-eqz v3, :cond_2

    move-object v0, v3

    :goto_2
    return-object v0

    :catch_0
    move-exception v3

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v0

    const-wide/16 v11, 0xa

    add-long/2addr v9, v11

    goto :goto_3

    :goto_3
    iget-boolean v11, p0, LX/HkR;->k:Z

    if-eqz v11, :cond_6

    iget v11, p0, LX/HkR;->f:I

    int-to-long v11, v11

    cmp-long v9, v9, v11

    if-ltz v9, :cond_5

    :cond_0
    :goto_4
    move v4, v7

    if-eqz v4, :cond_1

    iget v4, p0, LX/HkR;->i:I

    add-int/lit8 v4, v4, -0x1

    if-lt v2, v4, :cond_2

    :cond_1
    iget-object v4, p0, LX/HkR;->a:LX/HkP;

    invoke-virtual {v4, v3}, LX/HkP;->a(LX/Hka;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget v4, p0, LX/HkR;->i:I

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_3

    :try_start_1
    iget v4, p0, LX/HkR;->e:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_1
    throw v3

    :cond_3
    throw v3

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    move v7, v8

    goto :goto_4

    :cond_6
    iget v11, p0, LX/HkR;->e:I

    int-to-long v11, v11

    cmp-long v9, v9, v11

    if-gez v9, :cond_0

    move v7, v8

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;LX/Hkc;)LX/Hkb;
    .locals 1

    new-instance v0, LX/HkX;

    invoke-direct {v0, p1, p2}, LX/HkX;-><init>(Ljava/lang/String;LX/Hkc;)V

    invoke-static {p0, v0}, LX/HkR;->b(LX/HkR;LX/HkW;)LX/Hkb;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    if-lez p1, :cond_0

    const/16 v0, 0x12

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Maximum retries must be between 1 and 18"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput p1, p0, LX/HkR;->i:I

    return-void
.end method
