.class public final LX/HUL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2471982
    iput-object p1, p0, LX/HUL;->c:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iput-object p2, p0, LX/HUL;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HUL;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2471983
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2471984
    iget-object v1, p0, LX/HUL;->c:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->p:LX/3Sb;

    invoke-interface {v1, v0}, LX/3Sb;->a(I)LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2471985
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pagesIssuesTab_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2471986
    iget-object v3, p0, LX/HUL;->c:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->p:LX/3Sb;

    invoke-interface {v3, v0}, LX/3Sb;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471987
    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2471988
    iget-object v3, p0, LX/HUL;->c:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->d:LX/HU1;

    iget-object v4, p0, LX/HUL;->a:Ljava/lang/String;

    .line 2471989
    const-string v6, "pages_issues_share_sentiment_click"

    invoke-static {v3, v6, v4}, LX/HU1;->c(LX/HU1;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2471990
    const-string p1, "interaction_type"

    invoke-virtual {v6, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2471991
    iget-object p1, v3, LX/HU1;->a:LX/0Zb;

    invoke-interface {p1, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2471992
    iget-object v1, p0, LX/HUL;->c:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v3, p0, LX/HUL;->b:Ljava/lang/String;

    invoke-static {v1, v3, v2, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2471993
    return v5
.end method
