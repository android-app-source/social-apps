.class public LX/I1g;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/I1f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529279
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2529280
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/common/EventAnalyticsParams;)LX/I1f;
    .locals 13

    .prologue
    .line 2529281
    new-instance v0, LX/I1f;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v3

    check-cast v3, LX/1My;

    const-class v1, LX/I6c;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/I6c;

    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v5

    check-cast v5, LX/1DS;

    invoke-static {p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v6

    check-cast v6, LX/1Db;

    const/16 v1, 0x6bd

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v1, LX/I1I;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/I1I;

    const-class v1, LX/I1Z;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/I1Z;

    const-class v1, LX/I1O;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/I1O;

    const-class v1, LX/I1S;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/I1S;

    invoke-static {p0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v12

    check-cast v12, LX/0jU;

    move-object v1, p1

    invoke-direct/range {v0 .. v12}, LX/I1f;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/1My;LX/I6c;LX/1DS;LX/1Db;LX/0Ot;LX/I1I;LX/I1Z;LX/I1O;LX/I1S;LX/0jU;)V

    .line 2529282
    return-object v0
.end method
