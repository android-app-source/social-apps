.class public final LX/HxD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HxG;


# direct methods
.method public constructor <init>(LX/HxG;)V
    .locals 0

    .prologue
    .line 2522182
    iput-object p1, p0, LX/HxD;->a:LX/HxG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x32e6b3ae

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522183
    iget-object v1, p0, LX/HxD;->a:LX/HxG;

    iget-object v1, v1, LX/HxG;->b:LX/7v8;

    iget-object v2, p0, LX/HxD;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->r:Ljava/lang/String;

    .line 2522184
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/7v7;->BIRTHDAYS_CLICK_MESSENGER:LX/7v7;

    invoke-virtual {v5}, LX/7v7;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2522185
    invoke-static {v1, v3, v2}, LX/7v8;->a(LX/7v8;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 2522186
    sget-object v1, LX/0ax;->ai:Ljava/lang/String;

    iget-object v2, p0, LX/HxD;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2522187
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2522188
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2522189
    const/high16 v1, 0x10000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2522190
    iget-object v1, p0, LX/HxD;->a:LX/HxG;

    iget-object v1, v1, LX/HxG;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2522191
    const v1, 0x37a42705

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
