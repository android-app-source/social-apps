.class public abstract LX/HkP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/HkU;


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, LX/HkU;

    invoke-direct {v0}, LX/HkU;-><init>()V

    invoke-direct {p0, v0}, LX/HkP;-><init>(LX/HkU;)V

    return-void
.end method

.method private constructor <init>(LX/HkU;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/HkP;->a:LX/HkU;

    return-void
.end method


# virtual methods
.method public final a(LX/Hka;)Z
    .locals 3

    iget-object v0, p1, LX/Hka;->a:LX/Hkb;

    move-object v0, v0

    goto :goto_0

    :goto_0
    if-eqz v0, :cond_0

    iget v1, v0, LX/Hkb;->a:I

    move v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/io/InputStream;)[B
    .locals 4

    const/16 v0, 0x4000

    new-array v0, v0, [B

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
