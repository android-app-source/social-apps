.class public final LX/I8e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/I8d;


# instance fields
.field public final synthetic a:LX/I8k;


# direct methods
.method public constructor <init>(LX/I8k;)V
    .locals 0

    .prologue
    .line 2541646
    iput-object p1, p0, LX/I8e;->a:LX/I8k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/IBr;Z)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2541647
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->w:LX/I5f;

    invoke-virtual {v0}, LX/I5f;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/IBr;->DISCUSSION:LX/IBr;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->K:LX/I7G;

    if-eqz v0, :cond_0

    .line 2541648
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->K:LX/I7G;

    .line 2541649
    iget-object v1, v0, LX/I7G;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-virtual {v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->k()V

    .line 2541650
    :cond_0
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    const/4 v1, 0x0

    .line 2541651
    iget-object v2, v0, LX/I8k;->a:LX/0Zb;

    const-string v4, "event_permalink_tab_bar_tapped"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2541652
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2541653
    const-string v2, "event_permalink"

    invoke-virtual {v4, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2541654
    iget-object v2, v0, LX/I8k;->f:LX/0kv;

    iget-object v5, v0, LX/I8k;->b:Landroid/content/Context;

    invoke-virtual {v2, v5}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2541655
    const-string v2, "Event"

    invoke-virtual {v4, v2}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2541656
    iget-object v2, v0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    if-eqz v2, :cond_1

    .line 2541657
    iget-object v2, v0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    .line 2541658
    iget-object v5, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v5

    .line 2541659
    invoke-virtual {v4, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2541660
    :cond_1
    iget-object v2, v0, LX/I8k;->F:LX/IBq;

    .line 2541661
    iget-object v5, v2, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-nez v5, :cond_12

    .line 2541662
    iget-object v5, v2, LX/IBq;->l:LX/IBr;

    .line 2541663
    :goto_0
    move-object v2, v5

    .line 2541664
    if-eqz v2, :cond_2

    .line 2541665
    sget-object v5, LX/I8f;->a:[I

    invoke-virtual {v2}, LX/IBr;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_0

    :cond_2
    move-object v2, v1

    .line 2541666
    :goto_1
    const-string v5, "previous_tab"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2541667
    const-string v2, "tapped_tab"

    invoke-virtual {v4, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2541668
    const-string v2, "source_module"

    iget-object v1, v0, LX/I8k;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz v1, :cond_f

    iget-object v1, v0, LX/I8k;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    :goto_2
    invoke-virtual {v4, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2541669
    const-string v2, "ref_module"

    iget-object v1, v0, LX/I8k;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz v1, :cond_10

    iget-object v1, v0, LX/I8k;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    :goto_3
    invoke-virtual {v4, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2541670
    const-string v2, "ref_mechanism"

    iget-object v1, v0, LX/I8k;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz v1, :cond_11

    iget-object v1, v0, LX/I8k;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    :goto_4
    invoke-virtual {v4, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2541671
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2541672
    :cond_3
    sget-object v0, LX/IBr;->ACTIVITY:LX/IBr;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->L:LX/I7H;

    if-eqz v0, :cond_4

    .line 2541673
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->L:LX/I7H;

    iget-object v1, p0, LX/I8e;->a:LX/I8k;

    iget-object v1, v1, LX/I8k;->N:Ljava/lang/String;

    iget-object v2, p0, LX/I8e;->a:LX/I8k;

    iget-boolean v2, v2, LX/I8k;->O:Z

    invoke-virtual {v0, v1, v2}, LX/I7H;->a(Ljava/lang/String;Z)V

    .line 2541674
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    invoke-virtual {v0, p1, v3}, LX/I8k;->a(LX/IBr;I)V

    .line 2541675
    :cond_4
    if-eqz p2, :cond_5

    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->F:LX/IBq;

    sget-object v1, LX/IBr;->DISCUSSION:LX/IBr;

    invoke-virtual {v0, v1}, LX/IBq;->b(LX/IBr;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->H:LX/2ja;

    if-eqz v0, :cond_5

    .line 2541676
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->H:LX/2ja;

    invoke-virtual {v0}, LX/2ja;->e()V

    .line 2541677
    :cond_5
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->I:LX/I8j;

    if-nez v0, :cond_6

    .line 2541678
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    new-instance v1, LX/I8j;

    invoke-direct {v1}, LX/I8j;-><init>()V

    .line 2541679
    iput-object v1, v0, LX/I8k;->I:LX/I8j;

    .line 2541680
    :cond_6
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    iget-object v0, v0, LX/I8k;->u:LX/1P0;

    invoke-virtual {v0}, LX/1OR;->f()Landroid/os/Parcelable;

    move-result-object v0

    .line 2541681
    if-eqz p2, :cond_9

    iget-object v1, p0, LX/I8e;->a:LX/I8k;

    iget-object v1, v1, LX/I8k;->F:LX/IBq;

    invoke-virtual {v1}, LX/IBq;->d()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2541682
    iget-object v1, p0, LX/I8e;->a:LX/I8k;

    iget-object v1, v1, LX/I8k;->F:LX/IBq;

    sget-object v2, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {v1, v2}, LX/IBq;->b(LX/IBr;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/I8e;->a:LX/I8k;

    iget-object v1, v1, LX/I8k;->I:LX/I8j;

    iget-object v1, v1, LX/I8j;->a:Landroid/os/Parcelable;

    .line 2541683
    :goto_5
    if-eqz v1, :cond_8

    .line 2541684
    iget-object v2, p0, LX/I8e;->a:LX/I8k;

    iget-object v2, v2, LX/I8k;->u:LX/1P0;

    invoke-virtual {v2, v1}, LX/1OR;->a(Landroid/os/Parcelable;)V

    .line 2541685
    :goto_6
    iget-object v1, p0, LX/I8e;->a:LX/I8k;

    invoke-static {v1, v0, p2}, LX/I8k;->a$redex0(LX/I8k;Landroid/os/Parcelable;Z)V

    .line 2541686
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    invoke-virtual {v0}, LX/I8k;->j()V

    .line 2541687
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2541688
    :goto_7
    return-void

    .line 2541689
    :cond_7
    iget-object v1, p0, LX/I8e;->a:LX/I8k;

    iget-object v1, v1, LX/I8k;->I:LX/I8j;

    iget-object v1, v1, LX/I8j;->b:Landroid/os/Parcelable;

    goto :goto_5

    .line 2541690
    :cond_8
    iget-object v1, p0, LX/I8e;->a:LX/I8k;

    invoke-static {v1}, LX/I8k;->s(LX/I8k;)V

    goto :goto_6

    .line 2541691
    :cond_9
    iget-object v1, p0, LX/I8e;->a:LX/I8k;

    iget-object v2, p0, LX/I8e;->a:LX/I8k;

    iget-object v2, v2, LX/I8k;->F:LX/IBq;

    invoke-virtual {v2}, LX/IBq;->d()Z

    move-result v2

    if-nez v2, :cond_a

    const/4 v0, 0x0

    :cond_a
    invoke-static {v1, v0, p2}, LX/I8k;->a$redex0(LX/I8k;Landroid/os/Parcelable;Z)V

    .line 2541692
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    invoke-virtual {v0}, LX/I8k;->j()V

    .line 2541693
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2541694
    iget-object v0, p0, LX/I8e;->a:LX/I8k;

    invoke-static {v0}, LX/I8k;->s(LX/I8k;)V

    goto :goto_7

    .line 2541695
    :pswitch_0
    const-string v2, "About"

    .line 2541696
    if-eqz p2, :cond_b

    const-string v1, "Discussion"

    :goto_8
    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    .line 2541697
    goto/16 :goto_1

    .line 2541698
    :cond_b
    const-string v1, "About"

    goto :goto_8

    .line 2541699
    :pswitch_1
    const-string v2, "Discussion"

    .line 2541700
    if-eqz p2, :cond_c

    const-string v1, "About"

    :goto_9
    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    .line 2541701
    goto/16 :goto_1

    .line 2541702
    :cond_c
    const-string v1, "Discussion"

    goto :goto_9

    .line 2541703
    :pswitch_2
    const-string v2, "Event"

    .line 2541704
    if-eqz p2, :cond_d

    const-string v1, "Activity"

    :goto_a
    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    .line 2541705
    goto/16 :goto_1

    .line 2541706
    :cond_d
    const-string v1, "Event"

    goto :goto_a

    .line 2541707
    :pswitch_3
    const-string v2, "Activity"

    .line 2541708
    if-eqz p2, :cond_e

    const-string v1, "Event"

    :goto_b
    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    goto/16 :goto_1

    :cond_e
    const-string v1, "Activity"

    goto :goto_b

    .line 2541709
    :cond_f
    const-string v1, "event_permalink"

    goto/16 :goto_2

    .line 2541710
    :cond_10
    const-string v1, "unknown"

    goto/16 :goto_3

    .line 2541711
    :cond_11
    const-string v1, "unknown"

    goto/16 :goto_4

    :cond_12
    iget-object v5, v2, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    .line 2541712
    iget-object v2, v5, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->d:LX/IBr;

    move-object v5, v2

    .line 2541713
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
