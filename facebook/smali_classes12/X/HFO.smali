.class public final LX/HFO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2443486
    iput-object p1, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iput-object p2, p0, LX/HFO;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2443487
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2443488
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443489
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->a:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    const-string v2, "save address failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2443490
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2443491
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2443492
    if-eqz p1, :cond_0

    .line 2443493
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2443494
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2443495
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 2443496
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2443497
    iput-object v0, v2, LX/HFJ;->f:Ljava/lang/String;

    .line 2443498
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    iget-object v1, p0, LX/HFO;->a:Ljava/lang/String;

    .line 2443499
    iput-object v1, v0, LX/HFJ;->g:Ljava/lang/String;

    .line 2443500
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2443501
    iput-object v0, v2, LX/HFJ;->h:Ljava/lang/String;

    .line 2443502
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    invoke-virtual {v1, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2443503
    iput-object v0, v2, LX/HFJ;->i:Ljava/lang/String;

    .line 2443504
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_2

    const-string v0, ""

    .line 2443505
    :goto_1
    iget-object v1, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443506
    iput-object v0, v1, LX/HFJ;->j:Ljava/lang/String;

    .line 2443507
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2443508
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->g:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443509
    :goto_2
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b()V

    .line 2443510
    :goto_3
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v5}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443511
    :cond_0
    return-void

    .line 2443512
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    move-result-object v0

    goto/16 :goto_0

    .line 2443513
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2443514
    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2443515
    :cond_3
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->g:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    iget-object v2, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2443516
    :cond_4
    iget-object v0, p0, LX/HFO;->b:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const v2, 0x7f0d22d2

    .line 2443517
    invoke-virtual {v0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    move-object v0, v3

    .line 2443518
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443519
    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2443520
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
