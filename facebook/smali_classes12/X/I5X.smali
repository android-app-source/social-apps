.class public final LX/I5X;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/feed/data/EventFeedStories;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I5f;


# direct methods
.method public constructor <init>(LX/I5f;)V
    .locals 0

    .prologue
    .line 2535840
    iput-object p1, p0, LX/I5X;->a:LX/I5f;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2535841
    iget-object v0, p0, LX/I5X;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->o:LX/I5b;

    invoke-virtual {v0}, LX/I5a;->b()V

    .line 2535842
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2535843
    check-cast p1, Lcom/facebook/events/feed/data/EventFeedStories;

    .line 2535844
    iget-object v0, p1, Lcom/facebook/events/feed/data/EventFeedStories;->a:LX/0Px;

    move-object v1, v0

    .line 2535845
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    .line 2535846
    iget-object v0, p1, Lcom/facebook/events/feed/data/EventFeedStories;->c:LX/0ta;

    move-object v3, v0

    .line 2535847
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v3, v0, :cond_2

    .line 2535848
    :goto_0
    iget-object v0, p1, Lcom/facebook/events/feed/data/EventFeedStories;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-object v0, v0

    .line 2535849
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2535850
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2535851
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    :cond_0
    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v3, v4, :cond_1

    sget-object v4, LX/0ta;->NO_DATA:LX/0ta;

    if-ne v3, v4, :cond_3

    .line 2535852
    :cond_1
    iget-object v3, p0, LX/I5X;->a:LX/I5f;

    const/4 v4, 0x1

    .line 2535853
    iput-boolean v4, v3, LX/I5f;->q:Z

    .line 2535854
    :goto_1
    iget-object v3, p0, LX/I5X;->a:LX/I5f;

    iget-object v3, v3, LX/I5f;->c:LX/0fz;

    invoke-virtual {v3, v1, v0}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    .line 2535855
    iget-object v0, p0, LX/I5X;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->n:LX/I7I;

    invoke-virtual {v0}, LX/I7I;->a()V

    .line 2535856
    iget-object v0, p0, LX/I5X;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->o:LX/I5b;

    invoke-virtual {v0, v2}, LX/I5b;->a(I)V

    .line 2535857
    return-void

    .line 2535858
    :cond_2
    goto :goto_0

    .line 2535859
    :cond_3
    iget-object v3, p0, LX/I5X;->a:LX/I5f;

    const/4 v4, 0x0

    .line 2535860
    iput-boolean v4, v3, LX/I5f;->q:Z

    .line 2535861
    goto :goto_1
.end method
