.class public final LX/IV7;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/IV8;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public b:LX/IRb;

.field public final synthetic c:LX/IV8;


# direct methods
.method public constructor <init>(LX/IV8;)V
    .locals 1

    .prologue
    .line 2581352
    iput-object p1, p0, LX/IV7;->c:LX/IV8;

    .line 2581353
    move-object v0, p1

    .line 2581354
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2581355
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2581356
    const-string v0, "GroupJoinButtonRowComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2581357
    if-ne p0, p1, :cond_1

    .line 2581358
    :cond_0
    :goto_0
    return v0

    .line 2581359
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2581360
    goto :goto_0

    .line 2581361
    :cond_3
    check-cast p1, LX/IV7;

    .line 2581362
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2581363
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2581364
    if-eq v2, v3, :cond_0

    .line 2581365
    iget-object v2, p0, LX/IV7;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/IV7;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v3, p1, LX/IV7;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2581366
    goto :goto_0

    .line 2581367
    :cond_5
    iget-object v2, p1, LX/IV7;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-nez v2, :cond_4

    .line 2581368
    :cond_6
    iget-object v2, p0, LX/IV7;->b:LX/IRb;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/IV7;->b:LX/IRb;

    iget-object v3, p1, LX/IV7;->b:LX/IRb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2581369
    goto :goto_0

    .line 2581370
    :cond_7
    iget-object v2, p1, LX/IV7;->b:LX/IRb;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
