.class public final LX/Ita;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6cX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6cX",
        "<",
        "LX/765;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/Itc;

.field private c:LX/6md;


# direct methods
.method public constructor <init>(LX/Itc;J)V
    .locals 0

    .prologue
    .line 2623530
    iput-object p1, p0, LX/Ita;->b:LX/Itc;

    iput-wide p2, p0, LX/Ita;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([B)V
    .locals 2

    .prologue
    .line 2623531
    :try_start_0
    iget-object v0, p0, LX/Ita;->b:LX/Itc;

    invoke-static {v0, p1}, LX/Itc;->a(LX/Itc;[B)LX/1su;

    move-result-object v0

    .line 2623532
    invoke-static {v0}, LX/6md;->b(LX/1su;)LX/6md;

    move-result-object v0

    iput-object v0, p0, LX/Ita;->c:LX/6md;
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    .line 2623533
    :goto_0
    return-void

    .line 2623534
    :catch_0
    sget-object v0, LX/Itc;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to read SendMessageResponse"

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 2623535
    iget-object v1, p0, LX/Ita;->b:LX/Itc;

    iget-object v1, v1, LX/Itc;->b:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2623536
    iget-object v1, p0, LX/Ita;->c:LX/6md;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Ita;->c:LX/6md;

    iget-object v1, v1, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    .line 2623537
    :cond_0
    :goto_0
    return v0

    .line 2623538
    :cond_1
    iget-object v1, p0, LX/Ita;->c:LX/6md;

    iget-object v1, v1, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    .line 2623539
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v4, p0, LX/Ita;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2623540
    iget-object v0, p0, LX/Ita;->b:LX/Itc;

    iget-object v1, p0, LX/Ita;->c:LX/6md;

    invoke-static {v0, v1}, LX/Itc;->a$redex0(LX/Itc;LX/6md;)LX/765;

    move-result-object v0

    return-object v0
.end method
