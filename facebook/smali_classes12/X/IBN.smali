.class public LX/IBN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/IBN;


# instance fields
.field public final a:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2546981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2546982
    iput-object p1, p0, LX/IBN;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2546983
    return-void
.end method

.method public static a(LX/0QB;)LX/IBN;
    .locals 4

    .prologue
    .line 2546984
    sget-object v0, LX/IBN;->b:LX/IBN;

    if-nez v0, :cond_1

    .line 2546985
    const-class v1, LX/IBN;

    monitor-enter v1

    .line 2546986
    :try_start_0
    sget-object v0, LX/IBN;->b:LX/IBN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2546987
    if-eqz v2, :cond_0

    .line 2546988
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2546989
    new-instance p0, LX/IBN;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/IBN;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 2546990
    move-object v0, p0

    .line 2546991
    sput-object v0, LX/IBN;->b:LX/IBN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2546992
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2546993
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2546994
    :cond_1
    sget-object v0, LX/IBN;->b:LX/IBN;

    return-object v0

    .line 2546995
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2546996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2546997
    iget-object v0, p0, LX/IBN;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x60015

    const-string v2, "EventPermalinkFragment"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;Ljava/lang/String;)V

    .line 2546998
    return-void
.end method
