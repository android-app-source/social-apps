.class public final LX/JAk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2656001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel$RangesModel$EntityModel;
    .locals 14

    .prologue
    .line 2655965
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2655966
    iget-object v1, p0, LX/JAk;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2655967
    iget-object v2, p0, LX/JAk;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 2655968
    iget-object v3, p0, LX/JAk;->c:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2655969
    iget-object v4, p0, LX/JAk;->d:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2655970
    iget-object v5, p0, LX/JAk;->e:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2655971
    iget-object v6, p0, LX/JAk;->f:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2655972
    iget-object v7, p0, LX/JAk;->j:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2655973
    iget-object v8, p0, LX/JAk;->k:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2655974
    iget-object v9, p0, LX/JAk;->l:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2655975
    iget-object v10, p0, LX/JAk;->m:LX/0Px;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 2655976
    iget-object v11, p0, LX/JAk;->n:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2655977
    iget-object v12, p0, LX/JAk;->o:Ljava/lang/String;

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2655978
    const/16 v13, 0xf

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 2655979
    const/4 v13, 0x0

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 2655980
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2655981
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2655982
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2655983
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2655984
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2655985
    const/4 v1, 0x6

    iget-boolean v2, p0, LX/JAk;->g:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2655986
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/JAk;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2655987
    const/16 v1, 0x8

    iget-boolean v2, p0, LX/JAk;->i:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2655988
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2655989
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2655990
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2655991
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2655992
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2655993
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 2655994
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2655995
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2655996
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2655997
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2655998
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2655999
    new-instance v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel$RangesModel$EntityModel;

    invoke-direct {v1, v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel$RangesModel$EntityModel;-><init>(LX/15i;)V

    .line 2656000
    return-object v1
.end method
