.class public LX/IfT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2599753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2599754
    iput-object p1, p0, LX/IfT;->a:LX/0Zb;

    .line 2599755
    return-void
.end method

.method public static b(LX/0QB;)LX/IfT;
    .locals 2

    .prologue
    .line 2599756
    new-instance v1, LX/IfT;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/IfT;-><init>(LX/0Zb;)V

    .line 2599757
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2599740
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "contacts_you_may_know_people"

    .line 2599741
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2599742
    move-object v0, v0

    .line 2599743
    iget-object v1, p0, LX/IfT;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2599744
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599745
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "cymk_click_add"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "contacts_you_may_know_people"

    .line 2599746
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2599747
    move-object v0, v0

    .line 2599748
    const-string v1, "id"

    iget-object v2, p2, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599749
    iget-object p2, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, p2

    .line 2599750
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "top"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "surface"

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2599751
    iget-object v1, p0, LX/IfT;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2599752
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599724
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2599725
    iget-object v3, p2, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599726
    new-instance v5, LX/0m9;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/0m9;-><init>(LX/0mC;)V

    .line 2599727
    const-string v6, "id"

    iget-object v0, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599728
    iget-object p2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p2

    .line 2599729
    invoke-virtual {v5, v6, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2599730
    const-string v0, "type"

    const-string v6, "top"

    invoke-virtual {v5, v0, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2599731
    const-string v0, "surface"

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2599732
    invoke-virtual {v2, v5}, LX/162;->a(LX/0lF;)LX/162;

    .line 2599733
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2599734
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "cymk_view_loaded"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "contacts_you_may_know_people"

    .line 2599735
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2599736
    move-object v0, v0

    .line 2599737
    const-string v1, "impression_units"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2599738
    iget-object v1, p0, LX/IfT;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2599739
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599708
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "cymk_click_profile"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "contacts_you_may_know_people"

    .line 2599709
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2599710
    move-object v0, v0

    .line 2599711
    const-string v1, "id"

    iget-object v2, p2, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599712
    iget-object p2, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, p2

    .line 2599713
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "top"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "surface"

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2599714
    iget-object v1, p0, LX/IfT;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2599715
    return-void
.end method

.method public final c(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599716
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "cymk_click_hide"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "contacts_you_may_know_people"

    .line 2599717
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2599718
    move-object v0, v0

    .line 2599719
    const-string v1, "id"

    iget-object v2, p2, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599720
    iget-object p2, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, p2

    .line 2599721
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "top"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "surface"

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2599722
    iget-object v1, p0, LX/IfT;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2599723
    return-void
.end method
