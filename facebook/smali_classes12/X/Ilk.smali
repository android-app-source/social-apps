.class public LX/Ilk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IlW;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:LX/IlU;


# direct methods
.method public constructor <init>(LX/Ill;)V
    .locals 1

    .prologue
    .line 2608141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2608142
    iget-object v0, p1, LX/Ill;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2608143
    if-nez v0, :cond_0

    .line 2608144
    iget v0, p1, LX/Ill;->b:I

    move v0, v0

    .line 2608145
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2608146
    iget-object v0, p1, LX/Ill;->c:LX/IlU;

    move-object v0, v0

    .line 2608147
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2608148
    iget-object v0, p1, LX/Ill;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2608149
    iput-object v0, p0, LX/Ilk;->a:Ljava/lang/String;

    .line 2608150
    iget v0, p1, LX/Ill;->b:I

    move v0, v0

    .line 2608151
    iput v0, p0, LX/Ilk;->b:I

    .line 2608152
    iget-object v0, p1, LX/Ill;->c:LX/IlU;

    move-object v0, v0

    .line 2608153
    iput-object v0, p0, LX/Ilk;->c:LX/IlU;

    .line 2608154
    return-void

    .line 2608155
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newBuilder()LX/Ill;
    .locals 1

    .prologue
    .line 2608156
    new-instance v0, LX/Ill;

    invoke-direct {v0}, LX/Ill;-><init>()V

    return-object v0
.end method
