.class public final LX/IyO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/6xg;

.field public final b:Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final g:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;


# direct methods
.method public constructor <init>(LX/6xg;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;)V
    .locals 2

    .prologue
    .line 2633253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633254
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    sget-object v1, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 2633255
    iput-object v1, v0, LX/6wu;->a:LX/6ws;

    .line 2633256
    move-object v0, v0

    .line 2633257
    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    .line 2633258
    iput-object v1, v0, LX/6wu;->b:LX/73i;

    .line 2633259
    move-object v0, v0

    .line 2633260
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    iput-object v0, p0, LX/IyO;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 2633261
    iput-object p1, p0, LX/IyO;->a:LX/6xg;

    .line 2633262
    iput-object p2, p0, LX/IyO;->g:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2633263
    iput-object p3, p0, LX/IyO;->b:Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

    .line 2633264
    return-void
.end method
