.class public final LX/J1h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/J1i;


# direct methods
.method public constructor <init>(LX/J1i;)V
    .locals 0

    .prologue
    .line 2639094
    iput-object p1, p0, LX/J1h;->a:LX/J1i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2639097
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2639098
    iget-object v0, p0, LX/J1h;->a:LX/J1i;

    iget-object v0, v0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 2639099
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    .line 2639100
    const-class v3, LX/J1l;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J1l;

    .line 2639101
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 2639102
    invoke-interface {v2, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 2639103
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2639104
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2639096
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2639095
    return-void
.end method
