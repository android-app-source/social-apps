.class public LX/HjZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public final c:Landroid/content/Context;

.field public final d:Landroid/view/View;

.field public final e:I

.field public final f:LX/Hj7;

.field public final g:Landroid/os/Handler;

.field public final h:Ljava/lang/Runnable;

.field public final i:Z

.field public volatile j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;ILX/Hj7;)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/HjZ;-><init>(Landroid/content/Context;Landroid/view/View;IZLX/Hj7;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;IZLX/Hj7;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LX/HjZ;->a:I

    const/16 v0, 0x3e8

    iput v0, p0, LX/HjZ;->b:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/HjZ;->g:Landroid/os/Handler;

    new-instance v0, Lcom/facebook/ads/internal/adapters/e$b;

    invoke-direct {v0, p0}, Lcom/facebook/ads/internal/adapters/e$b;-><init>(LX/HjZ;)V

    iput-object v0, p0, LX/HjZ;->h:Ljava/lang/Runnable;

    iput-object p1, p0, LX/HjZ;->c:Landroid/content/Context;

    iput-object p2, p0, LX/HjZ;->d:Landroid/view/View;

    iput p3, p0, LX/HjZ;->e:I

    iput-object p5, p0, LX/HjZ;->f:LX/Hj7;

    iput-boolean p4, p0, LX/HjZ;->i:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    iget-boolean v0, p0, LX/HjZ;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/HjZ;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HjZ;->g:Landroid/os/Handler;

    iget-object v1, p0, LX/HjZ;->h:Ljava/lang/Runnable;

    iget v2, p0, LX/HjZ;->a:I

    int-to-long v2, v2

    const v4, -0x4b5b837a

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, LX/HjZ;->g:Landroid/os/Handler;

    iget-object v1, p0, LX/HjZ;->h:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    return-void
.end method
