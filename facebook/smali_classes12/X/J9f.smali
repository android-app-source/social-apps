.class public final LX/J9f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2653152
    iput-object p1, p0, LX/J9f;->b:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iput-object p2, p0, LX/J9f;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2653153
    iget-object v0, p0, LX/J9f;->b:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->l:LX/J90;

    iget-object v1, p0, LX/J9f;->b:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->H:Ljava/lang/String;

    iget-object v2, p0, LX/J9f;->a:Ljava/lang/String;

    iget-object v3, p0, LX/J9f;->b:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v3, v3, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    .line 2653154
    iget-object p0, v3, LX/J9W;->r:LX/J9l;

    invoke-interface {p0}, LX/J9k;->a()I

    move-result p0

    move v3, p0

    .line 2653155
    invoke-virtual {v0, v1, v2, v3}, LX/J90;->a(Ljava/lang/String;Ljava/lang/String;I)LX/JBh;

    move-result-object v4

    .line 2653156
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object p0, LX/0zS;->c:LX/0zS;

    invoke-virtual {v4, p0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 2653157
    iget-object p0, v0, LX/J90;->a:LX/0tX;

    invoke-virtual {p0, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2653158
    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2653159
    return-object v0
.end method
