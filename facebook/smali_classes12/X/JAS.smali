.class public final LX/JAS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2655695
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2655696
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2655697
    :goto_0
    return v1

    .line 2655698
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2655699
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2655700
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2655701
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2655702
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2655703
    const-string v4, "ranges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2655704
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2655705
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 2655706
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2655707
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2655708
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_d

    .line 2655709
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2655710
    :goto_3
    move v3, v4

    .line 2655711
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2655712
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2655713
    goto :goto_1

    .line 2655714
    :cond_3
    const-string v4, "text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2655715
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2655716
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2655717
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2655718
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2655719
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2655720
    :cond_6
    const-string v11, "length"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 2655721
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v8, v6

    move v6, v5

    .line 2655722
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 2655723
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2655724
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2655725
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_7

    if-eqz v10, :cond_7

    .line 2655726
    const-string v11, "entity"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2655727
    invoke-static {p0, p1}, LX/JAR;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_4

    .line 2655728
    :cond_8
    const-string v11, "offset"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2655729
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v5

    goto :goto_4

    .line 2655730
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2655731
    :cond_a
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2655732
    invoke-virtual {p1, v4, v9}, LX/186;->b(II)V

    .line 2655733
    if-eqz v6, :cond_b

    .line 2655734
    invoke-virtual {p1, v5, v8, v4}, LX/186;->a(III)V

    .line 2655735
    :cond_b
    if-eqz v3, :cond_c

    .line 2655736
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v7, v4}, LX/186;->a(III)V

    .line 2655737
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_d
    move v3, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    goto :goto_4
.end method
