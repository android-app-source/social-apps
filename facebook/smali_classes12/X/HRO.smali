.class public final LX/HRO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4oV;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V
    .locals 0

    .prologue
    .line 2465145
    iput-object p1, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2465129
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->O:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->P:Z

    if-eqz v0, :cond_1

    .line 2465130
    :cond_0
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v3, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->H:Landroid/view/View;

    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v4, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->getMeasuredHeight()I

    move-result v0

    if-ge p2, v0, :cond_3

    move v0, v1

    :goto_0
    sget-object v5, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->N:[I

    iget-object v6, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v6, v6, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->k:LX/0hB;

    invoke-virtual {v6}, LX/0hB;->d()I

    move-result v6

    invoke-static {v3, v4, v0, v5, v6}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2465131
    iget-object v4, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2465132
    iput-boolean v0, v4, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->O:Z

    .line 2465133
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->O:Z

    if-eqz v0, :cond_1

    .line 2465134
    iget-object v4, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->E_(I)V

    .line 2465135
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    .line 2465136
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->P:Z

    .line 2465137
    :cond_1
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->K:I

    if-eq v0, p2, :cond_2

    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2465138
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->getMeasuredHeight()I

    move-result v0

    if-ge p2, v0, :cond_4

    .line 2465139
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2465140
    :goto_1
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    .line 2465141
    iput p2, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->K:I

    .line 2465142
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 2465143
    goto :goto_0

    .line 2465144
    :cond_4
    iget-object v0, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, p0, LX/HRO;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    goto :goto_1
.end method
