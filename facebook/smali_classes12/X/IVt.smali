.class public final LX/IVt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1xz;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

.field private final b:LX/1SX;

.field private final c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private final d:LX/1nq;

.field private final e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nq;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/ui/api/FeedMenuHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2582946
    iput-object p1, p0, LX/IVt;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2582947
    iput-object p2, p0, LX/IVt;->d:LX/1nq;

    .line 2582948
    iput-object p3, p0, LX/IVt;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582949
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582950
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/IVt;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2582951
    iput-object p4, p0, LX/IVt;->b:LX/1SX;

    .line 2582952
    new-instance v0, LX/IVu;

    invoke-direct {v0, p1, p3}, LX/IVu;-><init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iput-object v0, p0, LX/IVt;->f:LX/1KL;

    .line 2582953
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 2

    .prologue
    .line 2582954
    iget-object v0, p0, LX/IVt;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IVt;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IVt;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2582955
    :cond_0
    const/4 v0, 0x0

    .line 2582956
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    iget-object v1, p0, LX/IVt;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2582957
    iget-object v0, p0, LX/IVt;->f:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2582958
    iget-object v0, p0, LX/IVt;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 2582959
    iget-object v0, p0, LX/IVt;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582960
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 2582961
    check-cast v0, LX/0jW;

    return-object v0
.end method

.method public final d()LX/1nq;
    .locals 1

    .prologue
    .line 2582962
    iget-object v0, p0, LX/IVt;->d:LX/1nq;

    return-object v0
.end method

.method public final e()I
    .locals 5

    .prologue
    .line 2582963
    iget-object v0, p0, LX/IVt;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->c:LX/Bs7;

    iget-object v1, p0, LX/IVt;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/IVt;->b:LX/1SX;

    iget-object v3, p0, LX/IVt;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->d:LX/1DR;

    invoke-virtual {v3}, LX/1DR;->a()I

    move-result v3

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Bs7;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;II)I

    move-result v0

    return v0
.end method
