.class public LX/IBz;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBFacebookReactNavigator"
.end annotation


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/IC1;

.field private final c:LX/98q;

.field private final d:LX/17W;

.field public final e:LX/Bnj;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:LX/1xP;

.field private h:Z


# direct methods
.method public constructor <init>(LX/5pY;Lcom/facebook/content/SecureContextHelper;LX/IC1;LX/98q;LX/17W;LX/Bnj;LX/1xP;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2547957
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2547958
    iput-object p7, p0, LX/IBz;->g:LX/1xP;

    .line 2547959
    iput-object p2, p0, LX/IBz;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2547960
    iput-object p3, p0, LX/IBz;->b:LX/IC1;

    .line 2547961
    iput-object p4, p0, LX/IBz;->c:LX/98q;

    .line 2547962
    iput-object p5, p0, LX/IBz;->d:LX/17W;

    .line 2547963
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IBz;->h:Z

    .line 2547964
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2547965
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2547966
    iput-object p6, p0, LX/IBz;->e:LX/Bnj;

    .line 2547967
    iput-object p8, p0, LX/IBz;->f:Ljava/util/concurrent/Executor;

    .line 2547968
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2548052
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2548053
    new-instance v0, LX/5pA;

    const-string v1, "Attempted to build Uri with an unsupported type"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2548054
    :sswitch_0
    const-string v1, "User"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "Page"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "Group"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "Photo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "Story"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 2548055
    :pswitch_0
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    .line 2548056
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2548057
    invoke-static {v0, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2548058
    :goto_2
    return-object v0

    .line 2548059
    :pswitch_1
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    goto :goto_1

    .line 2548060
    :pswitch_2
    sget-object v0, LX/0ax;->C:Ljava/lang/String;

    goto :goto_1

    .line 2548061
    :pswitch_3
    sget-object v0, LX/0ax;->bV:Ljava/lang/String;

    goto :goto_1

    .line 2548062
    :pswitch_4
    sget-object v0, LX/0ax;->bx:Ljava/lang/String;

    goto :goto_1

    .line 2548063
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x41e065f -> :sswitch_2
        0x4984e12 -> :sswitch_3
        0x4c808d5 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(I)Z
    .locals 2

    .prologue
    .line 2548040
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2548041
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    invoke-virtual {v0, p1}, LX/5rQ;->a(I)I

    move-result v0

    .line 2548042
    iget-object v1, p0, LX/IBz;->c:LX/98q;

    .line 2548043
    iget-object p0, v1, LX/98q;->c:LX/98n;

    if-eqz p0, :cond_1

    .line 2548044
    iget-object p0, v1, LX/98q;->c:LX/98n;

    .line 2548045
    iget-object v1, p0, LX/98n;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    .line 2548046
    iget-object p0, v1, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    move-object v1, p0

    .line 2548047
    if-eqz v1, :cond_2

    .line 2548048
    iget p0, v1, Lcom/facebook/react/ReactRootView;->e:I

    move v1, p0

    .line 2548049
    :goto_0
    move p0, v1

    .line 2548050
    :goto_1
    move v1, p0

    .line 2548051
    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 2548031
    iget-boolean v0, p0, LX/IBz;->h:Z

    if-nez v0, :cond_0

    .line 2548032
    :goto_0
    return-void

    .line 2548033
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IBz;->h:Z

    .line 2548034
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2548035
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2548036
    if-nez v0, :cond_1

    .line 2548037
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2548038
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2548039
    :cond_1
    iget-object v2, p0, LX/IBz;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final bM_()V
    .locals 1

    .prologue
    .line 2548029
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IBz;->h:Z

    .line 2548030
    return-void
.end method

.method public final bN_()V
    .locals 1

    .prologue
    .line 2548027
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IBz;->h:Z

    .line 2548028
    return-void
.end method

.method public final bO_()V
    .locals 1

    .prologue
    .line 2548025
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IBz;->h:Z

    .line 2548026
    return-void
.end method

.method public clearRightBarButton(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2548020
    invoke-direct {p0, p1}, LX/IBz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2548021
    :goto_0
    return-void

    .line 2548022
    :cond_0
    iget-object v0, p0, LX/IBz;->c:LX/98q;

    .line 2548023
    iget-object p0, v0, LX/98q;->a:LX/0Sh;

    new-instance p1, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$4;

    invoke-direct {p1, v0}, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$4;-><init>(LX/98q;)V

    invoke-virtual {p0, p1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2548024
    goto :goto_0
.end method

.method public dismiss(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2548017
    invoke-direct {p0, p1}, LX/IBz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2548018
    :goto_0
    return-void

    .line 2548019
    :cond_0
    iget-object v0, p0, LX/IBz;->c:LX/98q;

    invoke-virtual {v0}, LX/98q;->a()V

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2548016
    const-string v0, "FBFacebookReactNavigator"

    return-object v0
.end method

.method public getSavedInstanceState(ILcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2548009
    invoke-direct {p0, p1}, LX/IBz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2548010
    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2548011
    :goto_0
    return-void

    .line 2548012
    :cond_0
    iget-object v0, p0, LX/IBz;->c:LX/98q;

    invoke-virtual {v0}, LX/98q;->c()Landroid/os/Bundle;

    move-result-object v0

    .line 2548013
    if-eqz v0, :cond_1

    .line 2548014
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, LX/5op;->a(Landroid/os/Bundle;)LX/5pH;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 2548015
    :cond_1
    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public openSearchURL(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2548007
    iget-object v0, p0, LX/IBz;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/fbreact/navigation/FbReactNavigationJavaModule$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/fbreact/navigation/FbReactNavigationJavaModule$1;-><init>(LX/IBz;ILjava/lang/String;Ljava/lang/String;)V

    const v2, -0x19f3d119

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2548008
    return-void
.end method

.method public openTarget(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2547997
    const/4 v0, -0x1

    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2547998
    const-string v0, "URL"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2547999
    iget-object v0, p0, LX/IBz;->d:LX/17W;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2548000
    :cond_1
    :goto_1
    return-void

    .line 2548001
    :sswitch_0
    const-string v1, "URL"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "File"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 2548002
    :pswitch_0
    iget-object v0, p0, LX/IBz;->d:LX/17W;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    .line 2548003
    :pswitch_1
    iget-object v0, p0, LX/IBz;->g:LX/1xP;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p2, v2, v3}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    goto :goto_1

    .line 2548004
    :cond_2
    invoke-static {p2, p3}, LX/IBz;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2548005
    if-eqz v0, :cond_1

    .line 2548006
    invoke-virtual {p0, v0}, LX/IBz;->a(Landroid/net/Uri;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1494f -> :sswitch_0
        0x21699c -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public openURL(ILjava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2547990
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2547991
    :cond_0
    :goto_0
    return-void

    .line 2547992
    :cond_1
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2547993
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fb:/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/IBz;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 2547994
    :cond_2
    const-string v0, "fb:/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2547995
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/IBz;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 2547996
    :cond_3
    const-string v0, "URL"

    invoke-virtual {p0, p1, p2, v0}, LX/IBz;->openTarget(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerRoutes(LX/5pC;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2547989
    return-void
.end method

.method public setInstanceStateToSave(ILX/5pG;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2547984
    invoke-direct {p0, p1}, LX/IBz;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2547985
    :cond_0
    :goto_0
    return-void

    .line 2547986
    :cond_1
    invoke-static {p2}, LX/5op;->a(LX/5pG;)Landroid/os/Bundle;

    move-result-object v0

    .line 2547987
    if-eqz v0, :cond_0

    .line 2547988
    iget-object v1, p0, LX/IBz;->c:LX/98q;

    invoke-virtual {v1, v0}, LX/98q;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public setNavBarTintColor(ILjava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2547979
    invoke-direct {p0, p1}, LX/IBz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2547980
    :goto_0
    return-void

    .line 2547981
    :cond_0
    iget-object v0, p0, LX/IBz;->c:LX/98q;

    .line 2547982
    iget-object p0, v0, LX/98q;->a:LX/0Sh;

    new-instance p1, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$5;

    invoke-direct {p1, v0, p2}, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$5;-><init>(LX/98q;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2547983
    goto :goto_0
.end method

.method public setNavigationBarTitle(ILjava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2547974
    invoke-direct {p0, p1}, LX/IBz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2547975
    :goto_0
    return-void

    .line 2547976
    :cond_0
    iget-object v0, p0, LX/IBz;->c:LX/98q;

    .line 2547977
    iget-object p0, v0, LX/98q;->a:LX/0Sh;

    new-instance p1, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$1;

    invoke-direct {p1, v0, p2}, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$1;-><init>(LX/98q;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2547978
    goto :goto_0
.end method

.method public setRightBarButton(ILX/5pG;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2547969
    invoke-direct {p0, p1}, LX/IBz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2547970
    :goto_0
    return-void

    .line 2547971
    :cond_0
    iget-object v0, p0, LX/IBz;->c:LX/98q;

    .line 2547972
    iget-object p0, v0, LX/98q;->a:LX/0Sh;

    new-instance p1, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$3;

    invoke-direct {p1, v0, p2}, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$3;-><init>(LX/98q;LX/5pG;)V

    invoke-virtual {p0, p1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2547973
    goto :goto_0
.end method
