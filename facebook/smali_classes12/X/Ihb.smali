.class public final LX/Ihb;
.super LX/6LW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6LW",
        "<",
        "Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;",
        "LX/0Px",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;)V
    .locals 0

    .prologue
    .line 2602796
    iput-object p1, p0, LX/Ihb;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    invoke-direct {p0}, LX/6LW;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2602797
    check-cast p2, LX/0Px;

    .line 2602798
    iget-object v0, p0, LX/Ihb;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->f:LX/IhW;

    .line 2602799
    iput-object p2, v0, LX/IhW;->e:LX/0Px;

    .line 2602800
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2602801
    iget-object v0, p0, LX/Ihb;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2602802
    iget-object v0, p0, LX/Ihb;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->f:LX/IhW;

    iget-object v1, p0, LX/Ihb;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/IhW;->a(Ljava/util/List;)V

    .line 2602803
    :cond_0
    return-void
.end method
