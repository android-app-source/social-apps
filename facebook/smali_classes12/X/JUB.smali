.class public final LX/JUB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

.field public final synthetic b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V
    .locals 0

    .prologue
    .line 2697821
    iput-object p1, p0, LX/JUB;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    iput-object p2, p0, LX/JUB;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x424256ab

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697814
    iget-object v1, p0, LX/JUB;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->g:LX/JUO;

    iget-object v2, p0, LX/JUB;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 2697815
    sget-object v3, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SEE_ALL:LX/6VK;

    .line 2697816
    iget-object v5, v1, LX/JUO;->a:LX/0Zb;

    invoke-virtual {v3}, LX/6VK;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 2697817
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2697818
    const-string v6, "native_newsfeed"

    invoke-virtual {v5, v6}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "feed_type"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v5

    invoke-virtual {v5}, LX/0oG;->d()V

    .line 2697819
    :cond_0
    iget-object v1, p0, LX/JUB;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2697820
    const v1, 0x7ca5e019

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
