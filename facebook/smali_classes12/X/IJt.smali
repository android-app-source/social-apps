.class public LX/IJt;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2564678
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2564679
    sget-object v0, LX/0ax;->fA:Ljava/lang/String;

    const-string v1, "{source unknown}"

    const-string v2, "{campaign_id none}"

    const-string v3, "{story_id none}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->THROWBACK_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2564680
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2564681
    const-string v1, "composer_source_surface"

    sget-object v2, LX/21D;->URI_HANDLER:LX/21D;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2564682
    sget-object v1, LX/0ax;->fD:Ljava/lang/String;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "{campaign_id}"

    aput-object v3, v2, v6

    const-string v3, "{campaign_type}"

    aput-object v3, v2, v5

    const-string v3, "{source}"

    aput-object v3, v2, v7

    const-string v3, "{direct_source}"

    aput-object v3, v2, v8

    const-string v3, "{share_preview}"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "{share_preview_title}"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "{default_share_message}"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2564683
    sget-object v1, LX/0ax;->fE:Ljava/lang/String;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "{campaign_id}"

    aput-object v3, v2, v6

    const-string v3, "{campaign_type}"

    aput-object v3, v2, v5

    const-string v3, "{source}"

    aput-object v3, v2, v7

    const-string v3, "{direct_source}"

    aput-object v3, v2, v8

    const-string v3, "{share_preview}"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "{share_preview_title}"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2564684
    sget-object v0, LX/0ax;->fB:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->DAILY_DIALOGUE_WEATHER_PERMALINK_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2564685
    return-void
.end method

.method public static a(LX/0QB;)LX/IJt;
    .locals 1

    .prologue
    .line 2564686
    new-instance v0, LX/IJt;

    invoke-direct {v0}, LX/IJt;-><init>()V

    .line 2564687
    move-object v0, v0

    .line 2564688
    return-object v0
.end method
