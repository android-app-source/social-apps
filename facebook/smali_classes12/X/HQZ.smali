.class public final LX/HQZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/HQY;

.field public final b:LX/HQW;

.field public final c:LX/HQX;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/HQY;LX/HQW;LX/HQX;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2463691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2463692
    iput-object p1, p0, LX/HQZ;->a:LX/HQY;

    .line 2463693
    iput-object p2, p0, LX/HQZ;->b:LX/HQW;

    .line 2463694
    iput-object p3, p0, LX/HQZ;->c:LX/HQX;

    .line 2463695
    iput-object p4, p0, LX/HQZ;->d:Ljava/lang/String;

    .line 2463696
    iput-object p5, p0, LX/HQZ;->e:Ljava/lang/String;

    .line 2463697
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2463698
    if-ne p0, p1, :cond_1

    .line 2463699
    :cond_0
    :goto_0
    return v0

    .line 2463700
    :cond_1
    instance-of v2, p1, LX/HQZ;

    if-nez v2, :cond_2

    move v0, v1

    .line 2463701
    goto :goto_0

    .line 2463702
    :cond_2
    check-cast p1, LX/HQZ;

    .line 2463703
    iget-object v2, p0, LX/HQZ;->a:LX/HQY;

    iget-object v3, p1, LX/HQZ;->a:LX/HQY;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/HQZ;->b:LX/HQW;

    iget-object v3, p1, LX/HQZ;->b:LX/HQW;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/HQZ;->c:LX/HQX;

    iget-object v3, p1, LX/HQZ;->c:LX/HQX;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/HQZ;->d:Ljava/lang/String;

    iget-object v3, p1, LX/HQZ;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/HQZ;->e:Ljava/lang/String;

    iget-object v3, p1, LX/HQZ;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2463704
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/HQZ;->a:LX/HQY;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/HQZ;->b:LX/HQW;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/HQZ;->c:LX/HQX;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/HQZ;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/HQZ;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
