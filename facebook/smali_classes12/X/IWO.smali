.class public final LX/IWO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IWQ;


# direct methods
.method public constructor <init>(LX/IWQ;)V
    .locals 0

    .prologue
    .line 2583843
    iput-object p1, p0, LX/IWO;->a:LX/IWQ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2583841
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->j:LX/IWG;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/IWG;->a(Z)V

    .line 2583842
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2583802
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2583803
    if-eqz p1, :cond_0

    .line 2583804
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583805
    if-nez v0, :cond_1

    .line 2583806
    :cond_0
    :goto_0
    return-void

    .line 2583807
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583808
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    .line 2583809
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v3, :cond_2

    .line 2583810
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583811
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v0, v3, :cond_2

    .line 2583812
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->j:LX/IWG;

    invoke-virtual {v0, v1, v2}, LX/IWG;->a(ZZ)V

    goto :goto_0

    .line 2583813
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583814
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel;

    move-result-object v3

    .line 2583815
    iget-object v4, p0, LX/IWO;->a:LX/IWQ;

    .line 2583816
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583817
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v0

    .line 2583818
    iput-object v0, v4, LX/IWQ;->i:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 2583819
    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v5, p0, LX/IWO;->a:LX/IWQ;

    invoke-virtual {v4, v0, v1}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 2583820
    :goto_1
    iput-boolean v0, v5, LX/IWQ;->l:Z

    .line 2583821
    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v5, p0, LX/IWO;->a:LX/IWQ;

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2583822
    iput-object v0, v5, LX/IWQ;->k:Ljava/lang/String;

    .line 2583823
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->j:LX/IWG;

    invoke-virtual {v0, v2}, LX/IWG;->a(Z)V

    .line 2583824
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->h:LX/0Px;

    if-nez v0, :cond_5

    .line 2583825
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel;->a()LX/0Px;

    move-result-object v3

    .line 2583826
    iput-object v3, v0, LX/IWQ;->h:LX/0Px;

    .line 2583827
    :goto_2
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->j:LX/IWG;

    iget-object v3, p0, LX/IWO;->a:LX/IWQ;

    iget-object v3, v3, LX/IWQ;->h:LX/0Px;

    iget-object v4, p0, LX/IWO;->a:LX/IWQ;

    iget-object v4, v4, LX/IWQ;->i:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 2583828
    iget-object v5, v0, LX/IWG;->a:Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;

    iget-object v5, v5, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->i:LX/IWC;

    .line 2583829
    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    .line 2583830
    iput-object v3, v5, LX/IWC;->a:LX/0Px;

    .line 2583831
    iput-object v4, v5, LX/IWC;->d:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 2583832
    const v0, 0x47900b34

    invoke-static {v5, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2583833
    :cond_3
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2583834
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->j:LX/IWG;

    invoke-virtual {v0, v2, v1}, LX/IWG;->a(ZZ)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 2583835
    goto :goto_1

    .line 2583836
    :cond_5
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2583837
    iget-object v4, p0, LX/IWO;->a:LX/IWQ;

    iget-object v5, p0, LX/IWO;->a:LX/IWQ;

    iget-object v5, v5, LX/IWQ;->h:LX/0Px;

    invoke-virtual {v0, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2583838
    iput-object v0, v4, LX/IWQ;->h:LX/0Px;

    .line 2583839
    goto :goto_2

    .line 2583840
    :cond_6
    iget-object v0, p0, LX/IWO;->a:LX/IWQ;

    iget-object v0, v0, LX/IWQ;->j:LX/IWG;

    invoke-virtual {v0, v1, v1}, LX/IWG;->a(ZZ)V

    goto/16 :goto_0
.end method
