.class public final LX/ICe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/ICn;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ICf;


# direct methods
.method public constructor <init>(LX/ICf;)V
    .locals 0

    .prologue
    .line 2549595
    iput-object p1, p0, LX/ICe;->a:LX/ICf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2549596
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2549597
    check-cast p1, LX/ICn;

    .line 2549598
    if-nez p1, :cond_0

    .line 2549599
    :goto_0
    return-void

    .line 2549600
    :cond_0
    iget-object v0, p0, LX/ICe;->a:LX/ICf;

    iget-object v0, v0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->r:LX/ICw;

    .line 2549601
    iget-object v1, p1, LX/ICn;->a:LX/0Px;

    move-object v1, v1

    .line 2549602
    const/4 v2, 0x0

    .line 2549603
    new-instance v4, LX/0cA;

    invoke-direct {v4}, LX/0cA;-><init>()V

    .line 2549604
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ICk;

    .line 2549605
    invoke-virtual {v2}, LX/ICk;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v6, v7, :cond_3

    .line 2549606
    iget-object v6, v0, LX/ICw;->c:Ljava/util/Map;

    invoke-virtual {v2}, LX/ICk;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ICk;

    .line 2549607
    if-eqz v2, :cond_3

    .line 2549608
    invoke-virtual {v4, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2549609
    iget-object v3, v0, LX/ICw;->c:Ljava/util/Map;

    invoke-virtual {v2}, LX/ICk;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2549610
    const/4 v3, 0x1

    move v2, v3

    :goto_2
    move v3, v2

    .line 2549611
    goto :goto_1

    .line 2549612
    :cond_1
    if-eqz v3, :cond_2

    .line 2549613
    iget-object v2, v0, LX/ICw;->b:Ljava/util/List;

    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 2549614
    const v2, 0x2ef60cb7

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2549615
    :cond_2
    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_2
.end method
