.class public final LX/IXK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/IXQ;

.field public b:J

.field public c:J

.field public d:Z

.field public e:Z

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/IXN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/IXQ;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/IXN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2585899
    iput-object p1, p0, LX/IXK;->a:LX/IXQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585900
    iput-object p2, p0, LX/IXK;->f:Ljava/util/Map;

    .line 2585901
    return-void
.end method

.method public static b(LX/IXK;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2585902
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2585903
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2585904
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2585905
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 2585906
    const/4 v0, 0x1

    .line 2585907
    iget-object v1, p0, LX/IXK;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v0

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2585908
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2585909
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXN;

    .line 2585910
    iget-object v9, v0, LX/IXN;->e:LX/IXM;

    sget-object v10, LX/IXM;->FAILED:LX/IXM;

    if-ne v9, v10, :cond_0

    .line 2585911
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 2585912
    goto :goto_0

    .line 2585913
    :cond_0
    iget-object v9, v0, LX/IXN;->e:LX/IXM;

    sget-object v10, LX/IXM;->LOADING:LX/IXM;

    if-ne v9, v10, :cond_2

    .line 2585914
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2585915
    invoke-virtual {v0}, LX/IXN;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 2585916
    invoke-interface {v7, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v3

    :goto_1
    move v2, v0

    .line 2585917
    goto :goto_0

    .line 2585918
    :cond_1
    iget-object v0, p0, LX/IXK;->a:LX/IXQ;

    iget-object v0, v0, LX/IXQ;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 2585919
    const-string v1, "tracker_count"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585920
    const-string v0, "trackers_all_loaded"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585921
    const-string v0, "trackers_that_failed"

    invoke-interface {v4, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585922
    const-string v0, "unfinished_trackers"

    invoke-interface {v4, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585923
    const-string v0, "trackers_finished_duration"

    iget-wide v2, p0, LX/IXK;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585924
    const-string v0, "unfinished_tracker_info"

    invoke-interface {v4, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585925
    return-object v4

    :cond_2
    move v0, v2

    goto :goto_1
.end method
