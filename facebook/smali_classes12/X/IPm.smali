.class public final LX/IPm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/IPn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;

.field public b:I

.field public c:Z

.field public final synthetic d:LX/IPn;


# direct methods
.method public constructor <init>(LX/IPn;)V
    .locals 1

    .prologue
    .line 2574587
    iput-object p1, p0, LX/IPm;->d:LX/IPn;

    .line 2574588
    move-object v0, p1

    .line 2574589
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2574590
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2574606
    const-string v0, "GroupMallDrawerItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2574591
    if-ne p0, p1, :cond_1

    .line 2574592
    :cond_0
    :goto_0
    return v0

    .line 2574593
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2574594
    goto :goto_0

    .line 2574595
    :cond_3
    check-cast p1, LX/IPm;

    .line 2574596
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2574597
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2574598
    if-eq v2, v3, :cond_0

    .line 2574599
    iget-object v2, p0, LX/IPm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/IPm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;

    iget-object v3, p1, LX/IPm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2574600
    goto :goto_0

    .line 2574601
    :cond_5
    iget-object v2, p1, LX/IPm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;

    if-nez v2, :cond_4

    .line 2574602
    :cond_6
    iget v2, p0, LX/IPm;->b:I

    iget v3, p1, LX/IPm;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2574603
    goto :goto_0

    .line 2574604
    :cond_7
    iget-boolean v2, p0, LX/IPm;->c:Z

    iget-boolean v3, p1, LX/IPm;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2574605
    goto :goto_0
.end method
