.class public final LX/HSQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;Z)V
    .locals 0

    .prologue
    .line 2467018
    iput-object p1, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iput-boolean p2, p0, LX/HSQ;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ZZLcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;ILjava/util/List;Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2467019
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    .line 2467020
    iput-boolean p1, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->A:Z

    .line 2467021
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_1

    .line 2467022
    :cond_0
    :goto_0
    return-void

    .line 2467023
    :cond_1
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    sget-object v1, LX/HSK;->UPCOMING:LX/HSK;

    invoke-static {v0, p5, v1}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a$redex0(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;Ljava/util/List;LX/HSK;)V

    .line 2467024
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    .line 2467025
    iput-object p6, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->o:Ljava/lang/String;

    .line 2467026
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    .line 2467027
    iput p4, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->v:I

    .line 2467028
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    .line 2467029
    iput-boolean p2, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->t:Z

    .line 2467030
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->m(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2467031
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    .line 2467032
    iput-object p3, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->u:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 2467033
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    .line 2467034
    :goto_1
    if-eqz p7, :cond_2

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2467035
    :cond_2
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    sget-object v1, LX/HSX;->PAST:LX/HSX;

    .line 2467036
    iput-object v1, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->w:LX/HSX;

    .line 2467037
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    const/4 v1, 0x0

    .line 2467038
    iput-object v1, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->o:Ljava/lang/String;

    .line 2467039
    :cond_3
    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/HSQ;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->z:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2467040
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->z:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2467041
    iget-object v1, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    const/4 p2, 0x0

    .line 2467042
    if-nez v0, :cond_6

    .line 2467043
    invoke-virtual {v1, p2}, LX/3Tf;->p_(I)I

    move-result p1

    .line 2467044
    :goto_2
    move v0, p1

    .line 2467045
    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 2467046
    iget-object v1, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    goto :goto_0

    .line 2467047
    :cond_4
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->o(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    goto :goto_1

    .line 2467048
    :cond_5
    iget-object v0, p0, LX/HSQ;->b:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->l(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    goto :goto_0

    .line 2467049
    :cond_6
    iget-object p1, v1, LX/HSM;->h:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 2467050
    iget-object p1, v1, LX/HSM;->h:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v1, p2}, LX/3Tf;->p_(I)I

    move-result p2

    add-int/2addr p1, p2

    goto :goto_2

    .line 2467051
    :cond_7
    const/4 p1, -0x1

    goto :goto_2
.end method
