.class public final enum LX/IyQ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IyQ;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IyQ;

.field public static final enum MULTI:LX/IyQ;

.field public static final enum NOT_PRESENT:LX/IyQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2633296
    new-instance v0, LX/IyQ;

    const-string v1, "NOT_PRESENT"

    invoke-direct {v0, v1, v2}, LX/IyQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IyQ;->NOT_PRESENT:LX/IyQ;

    .line 2633297
    new-instance v0, LX/IyQ;

    const-string v1, "MULTI"

    invoke-direct {v0, v1, v3}, LX/IyQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IyQ;->MULTI:LX/IyQ;

    .line 2633298
    const/4 v0, 0x2

    new-array v0, v0, [LX/IyQ;

    sget-object v1, LX/IyQ;->NOT_PRESENT:LX/IyQ;

    aput-object v1, v0, v2

    sget-object v1, LX/IyQ;->MULTI:LX/IyQ;

    aput-object v1, v0, v3

    sput-object v0, LX/IyQ;->$VALUES:[LX/IyQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2633299
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static of(Ljava/lang/String;)LX/IyQ;
    .locals 2

    .prologue
    .line 2633300
    invoke-static {}, LX/IyQ;->values()[LX/IyQ;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    sget-object v1, LX/IyQ;->NOT_PRESENT:LX/IyQ;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IyQ;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/IyQ;
    .locals 1

    .prologue
    .line 2633301
    const-class v0, LX/IyQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IyQ;

    return-object v0
.end method

.method public static values()[LX/IyQ;
    .locals 1

    .prologue
    .line 2633302
    sget-object v0, LX/IyQ;->$VALUES:[LX/IyQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IyQ;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 2633303
    invoke-virtual {p0}, LX/IyQ;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 2633304
    invoke-virtual {p0}, LX/IyQ;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
