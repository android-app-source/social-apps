.class public final LX/JHr;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;)V
    .locals 0

    .prologue
    .line 2677143
    iput-object p1, p0, LX/JHr;->a:Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2677136
    iget-object v0, p0, LX/JHr;->a:Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;

    const/4 v1, 0x0

    .line 2677137
    iput-object v1, v0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->i:LX/1Mv;

    .line 2677138
    sget-object v0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->a:Ljava/lang/Class;

    const-string v1, "Failure to load nearby friends"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2677139
    iget-object v0, p0, LX/JHr;->a:Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;

    .line 2677140
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2677141
    invoke-static {v0, v1}, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->a$redex0(Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;LX/0Px;)V

    .line 2677142
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2677130
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2677131
    iget-object v0, p0, LX/JHr;->a:Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;

    const/4 v1, 0x0

    .line 2677132
    iput-object v1, v0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->i:LX/1Mv;

    .line 2677133
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contacts/picker/service/ContactPickerNearbyResult;

    .line 2677134
    iget-object v1, p0, LX/JHr;->a:Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/contacts/picker/service/ContactPickerNearbyResult;->a:LX/0Px;

    invoke-static {v1, v0}, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->a$redex0(Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;LX/0Px;)V

    .line 2677135
    return-void
.end method
