.class public LX/IMk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1Ck;

.field public final b:LX/0tX;

.field public final c:Landroid/content/res/Resources;

.field private final d:LX/0Zb;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;Landroid/content/res/Resources;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2570123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2570124
    iput-object p1, p0, LX/IMk;->a:LX/1Ck;

    .line 2570125
    iput-object p2, p0, LX/IMk;->b:LX/0tX;

    .line 2570126
    iput-object p3, p0, LX/IMk;->c:Landroid/content/res/Resources;

    .line 2570127
    iput-object p4, p0, LX/IMk;->d:LX/0Zb;

    .line 2570128
    return-void
.end method

.method public static a(LX/0QB;)LX/IMk;
    .locals 7

    .prologue
    .line 2570129
    const-class v1, LX/IMk;

    monitor-enter v1

    .line 2570130
    :try_start_0
    sget-object v0, LX/IMk;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2570131
    sput-object v2, LX/IMk;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2570132
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2570133
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2570134
    new-instance p0, LX/IMk;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-direct {p0, v3, v4, v5, v6}, LX/IMk;-><init>(LX/1Ck;LX/0tX;Landroid/content/res/Resources;LX/0Zb;)V

    .line 2570135
    move-object v0, p0

    .line 2570136
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2570137
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IMk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2570138
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2570139
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/IMk;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Throwable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2570140
    instance-of v0, p4, LX/4Ua;

    if-eqz v0, :cond_1

    .line 2570141
    check-cast p4, LX/4Ua;

    iget-object v0, p4, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    .line 2570142
    :goto_0
    iget-object v2, p0, LX/IMk;->d:LX/0Zb;

    const-string v3, "group_email_submit_invalid_email_by_server"

    invoke-interface {v2, v3, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2570143
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2570144
    const-string v3, "group_email_verification"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "group_id"

    invoke-virtual {v2, v3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "email_address"

    invoke-virtual {v2, v3, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    sget-object v3, LX/IL8;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    const-string v2, "email_domains"

    const-string v3, ";"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p3, v4, v1

    invoke-static {v3, v4}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2570145
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;LX/ILc;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/ILc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2570146
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2570147
    :goto_0
    return-void

    .line 2570148
    :cond_0
    new-instance v0, LX/4Fp;

    invoke-direct {v0}, LX/4Fp;-><init>()V

    .line 2570149
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570150
    move-object v0, v0

    .line 2570151
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2570152
    const-string v2, "email"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570153
    move-object v0, v0

    .line 2570154
    new-instance v1, LX/IMl;

    invoke-direct {v1}, LX/IMl;-><init>()V

    move-object v1, v1

    .line 2570155
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/IMl;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2570156
    iget-object v1, p0, LX/IMk;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2570157
    iget-object v7, p0, LX/IMk;->a:LX/1Ck;

    sget-object v8, LX/IMj;->TASK_JOIN_GROUP_WITH_EMAIL:LX/IMj;

    new-instance v0, LX/IMh;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/IMh;-><init>(LX/IMk;LX/ILc;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v7, v8, v6, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
