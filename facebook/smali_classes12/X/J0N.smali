.class public LX/J0N;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentAccountEnabledStatus;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J0N;


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636920
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2636921
    return-void
.end method

.method public static a(LX/0QB;)LX/J0N;
    .locals 4

    .prologue
    .line 2636922
    sget-object v0, LX/J0N;->b:LX/J0N;

    if-nez v0, :cond_1

    .line 2636923
    const-class v1, LX/J0N;

    monitor-enter v1

    .line 2636924
    :try_start_0
    sget-object v0, LX/J0N;->b:LX/J0N;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2636925
    if-eqz v2, :cond_0

    .line 2636926
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2636927
    new-instance p0, LX/J0N;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-direct {p0, v3}, LX/J0N;-><init>(LX/0sO;)V

    .line 2636928
    move-object v0, p0

    .line 2636929
    sput-object v0, LX/J0N;->b:LX/J0N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2636930
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2636931
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2636932
    :cond_1
    sget-object v0, LX/J0N;->b:LX/J0N;

    return-object v0

    .line 2636933
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2636934
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2636935
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;

    .line 2636936
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchPaymentAccountEnabledStatusQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2636937
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 2636938
    new-instance v0, LX/Dtc;

    invoke-direct {v0}, LX/Dtc;-><init>()V

    move-object v0, v0

    .line 2636939
    return-object v0
.end method
