.class public final LX/JOQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;)V
    .locals 0

    .prologue
    .line 2687533
    iput-object p1, p0, LX/JOQ;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x1bc21606

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2687534
    iget-object v1, p0, LX/JOQ;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->g:LX/9Tk;

    iget-object v2, p0, LX/JOQ;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->h:Ljava/lang/String;

    const-string v3, "find_friends"

    .line 2687535
    iget-object v4, v1, LX/9Tk;->a:LX/0Zb;

    sget-object v5, LX/9Tj;->FIND_FRIENDS_CLICKED:LX/9Tj;

    invoke-virtual {v5}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "empty_feed_uuid"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "unit"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2687536
    sget-object v4, LX/0ig;->b:LX/0ih;

    const/4 v5, 0x0

    .line 2687537
    iput-boolean v5, v4, LX/0ih;->d:Z

    .line 2687538
    move-object v4, v4

    .line 2687539
    const/16 v5, 0x258

    .line 2687540
    iput v5, v4, LX/0ih;->c:I

    .line 2687541
    iget-object v4, v1, LX/9Tk;->b:LX/0if;

    sget-object v5, LX/0ig;->b:LX/0ih;

    invoke-virtual {v4, v5}, LX/0if;->a(LX/0ih;)V

    .line 2687542
    iget-object v1, p0, LX/JOQ;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->e:LX/Ezy;

    const/4 v2, 0x0

    .line 2687543
    iget-object v4, v1, LX/Ezy;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, v1, LX/Ezy;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, v1, LX/Ezy;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3, v5}, LX/1nR;->a(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/0Tn;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v4, v3, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    move v3, v3

    .line 2687544
    if-nez v3, :cond_2

    .line 2687545
    :cond_0
    :goto_0
    move v1, v2

    .line 2687546
    if-eqz v1, :cond_1

    .line 2687547
    iget-object v1, p0, LX/JOQ;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->c:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->cW:Ljava/lang/String;

    sget-object v4, LX/5Oz;->CI_PROMOTION_EMPTY_FEED:LX/5Oz;

    invoke-virtual {v4}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/5P0;->SUGGESTIONS:LX/5P0;

    invoke-virtual {v5}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2687548
    :goto_1
    const v1, 0x46e2c28d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2687549
    :cond_1
    iget-object v1, p0, LX/JOQ;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/JOQ;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    :cond_2
    iget-object v3, v1, LX/Ezy;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    iget-object v4, v1, LX/Ezy;->c:LX/0ad;

    sget-object v5, LX/0c0;->Live:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    sget v7, LX/2e4;->a:I

    const/4 v8, 0x3

    invoke-interface {v4, v5, v6, v7, v8}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v4

    if-le v3, v4, :cond_3

    iget-object v3, v1, LX/Ezy;->c:LX/0ad;

    sget-short v4, LX/2e4;->b:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method
