.class public LX/I2P;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/I2N;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/I2P;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I2Q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2530251
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/I2P;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/I2Q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530248
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2530249
    iput-object p1, p0, LX/I2P;->b:LX/0Ot;

    .line 2530250
    return-void
.end method

.method public static a(LX/0QB;)LX/I2P;
    .locals 4

    .prologue
    .line 2530252
    sget-object v0, LX/I2P;->c:LX/I2P;

    if-nez v0, :cond_1

    .line 2530253
    const-class v1, LX/I2P;

    monitor-enter v1

    .line 2530254
    :try_start_0
    sget-object v0, LX/I2P;->c:LX/I2P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2530255
    if-eqz v2, :cond_0

    .line 2530256
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2530257
    new-instance v3, LX/I2P;

    const/16 p0, 0x1b1c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/I2P;-><init>(LX/0Ot;)V

    .line 2530258
    move-object v0, v3

    .line 2530259
    sput-object v0, LX/I2P;->c:LX/I2P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2530260
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2530261
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2530262
    :cond_1
    sget-object v0, LX/I2P;->c:LX/I2P;

    return-object v0

    .line 2530263
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2530264
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2530247
    const v0, -0x57dc26a9

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2530231
    iget-object v0, p0, LX/I2P;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2Q;

    const/4 p2, 0x3

    const/4 p0, -0x1

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2530232
    iput-object p1, v0, LX/I2Q;->c:Landroid/content/Context;

    .line 2530233
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08219c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2530234
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    sget-object v3, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, v3}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    const v3, 0x7f0a010e

    invoke-virtual {v1, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b0050

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const-string v3, "roboto-medium"

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Di;->y(I)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b006f

    invoke-interface {v1, v4, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b0072

    invoke-interface {v1, v6, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b0069

    invoke-interface {v1, v5, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b006c

    invoke-interface {v1, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const v3, 0x7f0207ed

    invoke-virtual {v2, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Di;->y(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b006f

    invoke-interface {v2, v4, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0072

    invoke-interface {v2, v6, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0069

    invoke-interface {v2, v5, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b006c

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2530235
    const v2, -0x57dc26a9

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2530236
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2530237
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2530238
    invoke-static {}, LX/1dS;->b()V

    .line 2530239
    iget v0, p1, LX/1dQ;->b:I

    .line 2530240
    packed-switch v0, :pswitch_data_0

    .line 2530241
    :goto_0
    return-object v1

    .line 2530242
    :pswitch_0
    iget-object p1, p0, LX/I2P;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/I2Q;

    .line 2530243
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    iget-object p2, p1, LX/I2Q;->a:LX/0Or;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/content/ComponentName;

    invoke-virtual {p0, p2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object p2

    .line 2530244
    const-string p0, "target_fragment"

    sget-object v0, LX/0cQ;->EVENTS_UPCOMING_BIRTHDAYS_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    invoke-virtual {p2, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2530245
    iget-object p0, p1, LX/I2Q;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p1, LX/I2Q;->c:Landroid/content/Context;

    invoke-interface {p0, p2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2530246
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x57dc26a9
        :pswitch_0
    .end packed-switch
.end method
