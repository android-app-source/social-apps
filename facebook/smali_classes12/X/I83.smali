.class public final enum LX/I83;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I83;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I83;

.field public static final enum CANCELLED:LX/I83;

.field public static final enum CANT_GO:LX/I83;

.field public static final enum CANT_GO_SELECTED:LX/I83;

.field public static final enum COPY_LINK:LX/I83;

.field public static final enum CREATE_EVENT:LX/I83;

.field public static final enum CREATOR_PROMOTE:LX/I83;

.field public static final enum CREATOR_PROMOTED:LX/I83;

.field public static final enum DELETE:LX/I83;

.field public static final enum DUPLICATE_EVENT:LX/I83;

.field public static final enum EDIT:LX/I83;

.field public static final enum EDIT_OVERFLOW:LX/I83;

.field public static final enum EXPORT_TO_CALENDAR:LX/I83;

.field public static final enum GOING:LX/I83;

.field public static final enum GOING_SELECTED:LX/I83;

.field public static final enum INVITE:LX/I83;

.field public static final enum INVITE_OPTIONS:LX/I83;

.field public static final enum INVITE_OR_SHARE:LX/I83;

.field public static final enum INVITE_OR_SHARE_OVERFLOW:LX/I83;

.field public static final enum INVITE_OVERFLOW:LX/I83;

.field public static final enum JOIN:LX/I83;

.field public static final enum MAYBE:LX/I83;

.field public static final enum MAYBED:LX/I83;

.field public static final enum NOTIFICATION_SETTINGS:LX/I83;

.field public static final enum OPTIMISTIC_JOIN:LX/I83;

.field public static final enum PHOTOS:LX/I83;

.field public static final enum PHOTOS_OVERFLOW:LX/I83;

.field public static final enum POST:LX/I83;

.field public static final enum PRIVATE_CANT_GO_SELECTED:LX/I83;

.field public static final enum PRIVATE_GOING_SELECTED:LX/I83;

.field public static final enum PRIVATE_INVITED:LX/I83;

.field public static final enum PRIVATE_MAYBE_SELECTED:LX/I83;

.field public static final enum PUBLIC_GOING:LX/I83;

.field public static final enum PUBLIC_GOING_SELECTED:LX/I83;

.field public static final enum PUBLIC_GOING_SELECTED_WITH_CHEVRON:LX/I83;

.field public static final enum PUBLIC_INTERESTED:LX/I83;

.field public static final enum PUBLIC_INTERESTED_SELECTED:LX/I83;

.field public static final enum PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON:LX/I83;

.field public static final enum PUBLIC_INVITED:LX/I83;

.field public static final enum PUBLIC_INVITE_QE_SEND:LX/I83;

.field public static final enum PUBLIC_INVITE_QE_SEND_OVERFLOW:LX/I83;

.field public static final enum PUBLIC_INVITE_QE_SHARE:LX/I83;

.field public static final enum PUBLIC_INVITE_QE_SHARE_OVERFLOW:LX/I83;

.field public static final enum PUBLISH_EVENT:LX/I83;

.field public static final enum REMOVE_SCHEDULE:LX/I83;

.field public static final enum REPORT_EVENT:LX/I83;

.field public static final enum RESCHEDULE_EVENT:LX/I83;

.field public static final enum SAVE:LX/I83;

.field public static final enum SAVED:LX/I83;

.field public static final enum SHARE:LX/I83;

.field public static final enum SHARE_OVERFLOW:LX/I83;

.field public static final enum TOXICLE_PRIVATE_CANT_GO:LX/I83;

.field public static final enum TOXICLE_PRIVATE_CANT_GO_SELECTED_WITH_CHEVRON:LX/I83;

.field public static final enum TOXICLE_PRIVATE_GOING:LX/I83;

.field public static final enum TOXICLE_PRIVATE_GOING_SELECTED_WITH_CHEVRON:LX/I83;

.field public static final enum TOXICLE_PRIVATE_MAYBE:LX/I83;

.field public static final enum TOXICLE_PRIVATE_MAYBE_SELECTED_WITH_CHEVRON:LX/I83;

.field public static final enum TOXICLE_PUBLIC_GOING:LX/I83;

.field public static final enum TOXICLE_PUBLIC_GOING_SELECTED:LX/I83;

.field public static final enum TOXICLE_PUBLIC_GOING_SELECTED_WITH_CHEVRON:LX/I83;

.field public static final enum TOXICLE_PUBLIC_IGNORE:LX/I83;

.field public static final enum TOXICLE_PUBLIC_INTERESTED:LX/I83;

.field public static final enum TOXICLE_PUBLIC_INTERESTED_OVERFLOW:LX/I83;

.field public static final enum TOXICLE_PUBLIC_INTERESTED_SELECTED:LX/I83;

.field public static final enum TOXICLE_PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON:LX/I83;


# instance fields
.field private final mIconResId:I

.field private final mTitleResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2540576
    new-instance v0, LX/I83;

    const-string v1, "CREATOR_PROMOTE"

    const v2, 0x7f081f7d

    const v3, 0x7f020926

    invoke-direct {v0, v1, v5, v2, v3}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->CREATOR_PROMOTE:LX/I83;

    .line 2540577
    new-instance v0, LX/I83;

    const-string v1, "CREATOR_PROMOTED"

    const v2, 0x7f081f7e

    const v3, 0x7f020926

    invoke-direct {v0, v1, v6, v2, v3}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->CREATOR_PROMOTED:LX/I83;

    .line 2540578
    new-instance v0, LX/I83;

    const-string v1, "CANCELLED"

    const v2, 0x7f081f4a

    const v3, 0x7f02085e

    invoke-direct {v0, v1, v7, v2, v3}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->CANCELLED:LX/I83;

    .line 2540579
    new-instance v0, LX/I83;

    const-string v1, "GOING"

    const v2, 0x7f081f45

    const v3, 0x7f020857

    invoke-direct {v0, v1, v8, v2, v3}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->GOING:LX/I83;

    .line 2540580
    new-instance v0, LX/I83;

    const-string v1, "GOING_SELECTED"

    const v2, 0x7f081f45

    const v3, 0x7f020857

    invoke-direct {v0, v1, v9, v2, v3}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->GOING_SELECTED:LX/I83;

    .line 2540581
    new-instance v0, LX/I83;

    const-string v1, "PRIVATE_GOING_SELECTED"

    const/4 v2, 0x5

    const v3, 0x7f081f45

    const v4, 0x7f020857

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PRIVATE_GOING_SELECTED:LX/I83;

    .line 2540582
    new-instance v0, LX/I83;

    const-string v1, "MAYBED"

    const/4 v2, 0x6

    const v3, 0x7f081f47

    const v4, 0x7f02085c

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->MAYBED:LX/I83;

    .line 2540583
    new-instance v0, LX/I83;

    const-string v1, "PRIVATE_MAYBE_SELECTED"

    const/4 v2, 0x7

    const v3, 0x7f081f47

    const v4, 0x7f02085c

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PRIVATE_MAYBE_SELECTED:LX/I83;

    .line 2540584
    new-instance v0, LX/I83;

    const-string v1, "CANT_GO"

    const/16 v2, 0x8

    const v3, 0x7f081f48

    const v4, 0x7f02085e

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->CANT_GO:LX/I83;

    .line 2540585
    new-instance v0, LX/I83;

    const-string v1, "CANT_GO_SELECTED"

    const/16 v2, 0x9

    const v3, 0x7f081f48

    const v4, 0x7f02085e

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->CANT_GO_SELECTED:LX/I83;

    .line 2540586
    new-instance v0, LX/I83;

    const-string v1, "PRIVATE_CANT_GO_SELECTED"

    const/16 v2, 0xa

    const v3, 0x7f081f48

    const v4, 0x7f02085e

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PRIVATE_CANT_GO_SELECTED:LX/I83;

    .line 2540587
    new-instance v0, LX/I83;

    const-string v1, "JOIN"

    const/16 v2, 0xb

    const v3, 0x7f081f43

    const v4, 0x7f020855

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->JOIN:LX/I83;

    .line 2540588
    new-instance v0, LX/I83;

    const-string v1, "OPTIMISTIC_JOIN"

    const/16 v2, 0xc

    const v3, 0x7f081f43

    const v4, 0x7f020855

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->OPTIMISTIC_JOIN:LX/I83;

    .line 2540589
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_INTERESTED_SELECTED"

    const/16 v2, 0xd

    const v3, 0x7f081f80

    const v4, 0x7f020650

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_INTERESTED_SELECTED:LX/I83;

    .line 2540590
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_INTERESTED"

    const/16 v2, 0xe

    const v3, 0x7f081f80

    const v4, 0x7f020650

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_INTERESTED:LX/I83;

    .line 2540591
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON"

    const/16 v2, 0xf

    const v3, 0x7f081f80

    const v4, 0x7f020dc4

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON:LX/I83;

    .line 2540592
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_GOING"

    const/16 v2, 0x10

    const v3, 0x7f081f45

    const v4, 0x7f020857

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_GOING:LX/I83;

    .line 2540593
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_GOING_SELECTED"

    const/16 v2, 0x11

    const v3, 0x7f081f45

    const v4, 0x7f020857

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_GOING_SELECTED:LX/I83;

    .line 2540594
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_GOING_SELECTED_WITH_CHEVRON"

    const/16 v2, 0x12

    const v3, 0x7f081f45

    const v4, 0x7f020c13

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    .line 2540595
    new-instance v0, LX/I83;

    const-string v1, "MAYBE"

    const/16 v2, 0x13

    const v3, 0x7f081f46

    const v4, 0x7f02085c

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->MAYBE:LX/I83;

    .line 2540596
    new-instance v0, LX/I83;

    const-string v1, "PHOTOS"

    const/16 v2, 0x14

    const v3, 0x7f081f59

    const v4, 0x7f02095b

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PHOTOS:LX/I83;

    .line 2540597
    new-instance v0, LX/I83;

    const-string v1, "PHOTOS_OVERFLOW"

    const/16 v2, 0x15

    const v3, 0x7f081f59

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PHOTOS_OVERFLOW:LX/I83;

    .line 2540598
    new-instance v0, LX/I83;

    const-string v1, "INVITE_OR_SHARE"

    const/16 v2, 0x16

    const v3, 0x7f082f34    # 1.810201E38f

    const v4, 0x7f020dcb

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->INVITE_OR_SHARE:LX/I83;

    .line 2540599
    new-instance v0, LX/I83;

    const-string v1, "INVITE_OR_SHARE_OVERFLOW"

    const/16 v2, 0x17

    const v3, 0x7f082f34    # 1.810201E38f

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->INVITE_OR_SHARE_OVERFLOW:LX/I83;

    .line 2540600
    new-instance v0, LX/I83;

    const-string v1, "SHARE"

    const/16 v2, 0x18

    const v3, 0x7f081f56

    const v4, 0x7f0209c5

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->SHARE:LX/I83;

    .line 2540601
    new-instance v0, LX/I83;

    const-string v1, "SHARE_OVERFLOW"

    const/16 v2, 0x19

    const v3, 0x7f081f56

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->SHARE_OVERFLOW:LX/I83;

    .line 2540602
    new-instance v0, LX/I83;

    const-string v1, "INVITE"

    const/16 v2, 0x1a

    const v3, 0x7f082f34    # 1.810201E38f

    const v4, 0x7f020850

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->INVITE:LX/I83;

    .line 2540603
    new-instance v0, LX/I83;

    const-string v1, "INVITE_OPTIONS"

    const/16 v2, 0x1b

    const v3, 0x7f082f34    # 1.810201E38f

    const v4, 0x7f020dcb

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->INVITE_OPTIONS:LX/I83;

    .line 2540604
    new-instance v0, LX/I83;

    const-string v1, "PRIVATE_INVITED"

    const/16 v2, 0x1c

    const v3, 0x7f081f5a

    const v4, 0x7f020855

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PRIVATE_INVITED:LX/I83;

    .line 2540605
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_INVITED"

    const/16 v2, 0x1d

    const v3, 0x7f081f5a

    const v4, 0x7f020855

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_INVITED:LX/I83;

    .line 2540606
    new-instance v0, LX/I83;

    const-string v1, "INVITE_OVERFLOW"

    const/16 v2, 0x1e

    const v3, 0x7f082f34    # 1.810201E38f

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->INVITE_OVERFLOW:LX/I83;

    .line 2540607
    new-instance v0, LX/I83;

    const-string v1, "POST"

    const/16 v2, 0x1f

    const v3, 0x7f081f4f

    const v4, 0x7f02080f

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->POST:LX/I83;

    .line 2540608
    new-instance v0, LX/I83;

    const-string v1, "SAVE"

    const/16 v2, 0x20

    const v3, 0x7f081f4b

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->SAVE:LX/I83;

    .line 2540609
    new-instance v0, LX/I83;

    const-string v1, "SAVED"

    const/16 v2, 0x21

    const v3, 0x7f081f4d

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->SAVED:LX/I83;

    .line 2540610
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PRIVATE_GOING"

    const/16 v2, 0x22

    const v3, 0x7f081f45

    const v4, 0x7f0207d6

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PRIVATE_GOING:LX/I83;

    .line 2540611
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PRIVATE_MAYBE"

    const/16 v2, 0x23

    const v3, 0x7f081f46

    const v4, 0x7f020999

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PRIVATE_MAYBE:LX/I83;

    .line 2540612
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PRIVATE_CANT_GO"

    const/16 v2, 0x24

    const v3, 0x7f081f48

    const v4, 0x7f020818

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PRIVATE_CANT_GO:LX/I83;

    .line 2540613
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PRIVATE_GOING_SELECTED_WITH_CHEVRON"

    const/16 v2, 0x25

    const v3, 0x7f081f45

    const v4, 0x7f021998

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PRIVATE_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    .line 2540614
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PRIVATE_MAYBE_SELECTED_WITH_CHEVRON"

    const/16 v2, 0x26

    const v3, 0x7f081f46

    const v4, 0x7f02199a

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PRIVATE_MAYBE_SELECTED_WITH_CHEVRON:LX/I83;

    .line 2540615
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PRIVATE_CANT_GO_SELECTED_WITH_CHEVRON"

    const/16 v2, 0x27

    const v3, 0x7f081f48

    const v4, 0x7f021997

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PRIVATE_CANT_GO_SELECTED_WITH_CHEVRON:LX/I83;

    .line 2540616
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PUBLIC_IGNORE"

    const/16 v2, 0x28

    const v3, 0x7f081f49

    const v4, 0x7f020818

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PUBLIC_IGNORE:LX/I83;

    .line 2540617
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PUBLIC_INTERESTED"

    const/16 v2, 0x29

    const v3, 0x7f081f80

    const v4, 0x7f0209e1

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PUBLIC_INTERESTED:LX/I83;

    .line 2540618
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PUBLIC_INTERESTED_SELECTED"

    const/16 v2, 0x2a

    const v3, 0x7f081f80

    const v4, 0x7f0209e1

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PUBLIC_INTERESTED_SELECTED:LX/I83;

    .line 2540619
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PUBLIC_GOING"

    const/16 v2, 0x2b

    const v3, 0x7f081f45

    const v4, 0x7f0207d6

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PUBLIC_GOING:LX/I83;

    .line 2540620
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PUBLIC_GOING_SELECTED"

    const/16 v2, 0x2c

    const v3, 0x7f081f45

    const v4, 0x7f0207d6

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PUBLIC_GOING_SELECTED:LX/I83;

    .line 2540621
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PUBLIC_GOING_SELECTED_WITH_CHEVRON"

    const/16 v2, 0x2d

    const v3, 0x7f081f45

    const v4, 0x7f021998

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PUBLIC_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    .line 2540622
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON"

    const/16 v2, 0x2e

    const v3, 0x7f081f80

    const v4, 0x7f021999

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON:LX/I83;

    .line 2540623
    new-instance v0, LX/I83;

    const-string v1, "TOXICLE_PUBLIC_INTERESTED_OVERFLOW"

    const/16 v2, 0x2f

    const v3, 0x7f081f80

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->TOXICLE_PUBLIC_INTERESTED_OVERFLOW:LX/I83;

    .line 2540624
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_INVITE_QE_SEND"

    const/16 v2, 0x30

    const v3, 0x7f081f58

    const v4, 0x7f021538

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_INVITE_QE_SEND:LX/I83;

    .line 2540625
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_INVITE_QE_SEND_OVERFLOW"

    const/16 v2, 0x31

    const v3, 0x7f081f58

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_INVITE_QE_SEND_OVERFLOW:LX/I83;

    .line 2540626
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_INVITE_QE_SHARE"

    const/16 v2, 0x32

    const v3, 0x7f081f57

    const v4, 0x7f021539

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_INVITE_QE_SHARE:LX/I83;

    .line 2540627
    new-instance v0, LX/I83;

    const-string v1, "PUBLIC_INVITE_QE_SHARE_OVERFLOW"

    const/16 v2, 0x33

    const v3, 0x7f081f57

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLIC_INVITE_QE_SHARE_OVERFLOW:LX/I83;

    .line 2540628
    new-instance v0, LX/I83;

    const-string v1, "EDIT"

    const/16 v2, 0x34

    const v3, 0x7f082f35

    const v4, 0x7f020952

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->EDIT:LX/I83;

    .line 2540629
    new-instance v0, LX/I83;

    const-string v1, "EDIT_OVERFLOW"

    const/16 v2, 0x35

    const v3, 0x7f082f35

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->EDIT_OVERFLOW:LX/I83;

    .line 2540630
    new-instance v0, LX/I83;

    const-string v1, "NOTIFICATION_SETTINGS"

    const/16 v2, 0x36

    const v3, 0x7f081f5c

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->NOTIFICATION_SETTINGS:LX/I83;

    .line 2540631
    new-instance v0, LX/I83;

    const-string v1, "REPORT_EVENT"

    const/16 v2, 0x37

    const v3, 0x7f081f5d

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->REPORT_EVENT:LX/I83;

    .line 2540632
    new-instance v0, LX/I83;

    const-string v1, "COPY_LINK"

    const/16 v2, 0x38

    const v3, 0x7f081f5e

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->COPY_LINK:LX/I83;

    .line 2540633
    new-instance v0, LX/I83;

    const-string v1, "EXPORT_TO_CALENDAR"

    const/16 v2, 0x39

    const v3, 0x7f081ef9

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->EXPORT_TO_CALENDAR:LX/I83;

    .line 2540634
    new-instance v0, LX/I83;

    const-string v1, "DELETE"

    const/16 v2, 0x3a

    const v3, 0x7f082f36

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->DELETE:LX/I83;

    .line 2540635
    new-instance v0, LX/I83;

    const-string v1, "CREATE_EVENT"

    const/16 v2, 0x3b

    const v3, 0x7f081f60

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->CREATE_EVENT:LX/I83;

    .line 2540636
    new-instance v0, LX/I83;

    const-string v1, "DUPLICATE_EVENT"

    const/16 v2, 0x3c

    const v3, 0x7f081f61

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->DUPLICATE_EVENT:LX/I83;

    .line 2540637
    new-instance v0, LX/I83;

    const-string v1, "PUBLISH_EVENT"

    const/16 v2, 0x3d

    const v3, 0x7f082f38

    const v4, 0x7f0209c4

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->PUBLISH_EVENT:LX/I83;

    .line 2540638
    new-instance v0, LX/I83;

    const-string v1, "RESCHEDULE_EVENT"

    const/16 v2, 0x3e

    const v3, 0x7f082f3a

    const v4, 0x7f0207fd

    invoke-direct {v0, v1, v2, v3, v4}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->RESCHEDULE_EVENT:LX/I83;

    .line 2540639
    new-instance v0, LX/I83;

    const-string v1, "REMOVE_SCHEDULE"

    const/16 v2, 0x3f

    const v3, 0x7f081f62

    invoke-direct {v0, v1, v2, v3, v5}, LX/I83;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/I83;->REMOVE_SCHEDULE:LX/I83;

    .line 2540640
    const/16 v0, 0x40

    new-array v0, v0, [LX/I83;

    sget-object v1, LX/I83;->CREATOR_PROMOTE:LX/I83;

    aput-object v1, v0, v5

    sget-object v1, LX/I83;->CREATOR_PROMOTED:LX/I83;

    aput-object v1, v0, v6

    sget-object v1, LX/I83;->CANCELLED:LX/I83;

    aput-object v1, v0, v7

    sget-object v1, LX/I83;->GOING:LX/I83;

    aput-object v1, v0, v8

    sget-object v1, LX/I83;->GOING_SELECTED:LX/I83;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/I83;->PRIVATE_GOING_SELECTED:LX/I83;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/I83;->MAYBED:LX/I83;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/I83;->PRIVATE_MAYBE_SELECTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/I83;->CANT_GO:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/I83;->CANT_GO_SELECTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/I83;->PRIVATE_CANT_GO_SELECTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/I83;->JOIN:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/I83;->OPTIMISTIC_JOIN:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/I83;->PUBLIC_INTERESTED_SELECTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/I83;->PUBLIC_INTERESTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/I83;->PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/I83;->PUBLIC_GOING:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/I83;->PUBLIC_GOING_SELECTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/I83;->PUBLIC_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/I83;->MAYBE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/I83;->PHOTOS:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/I83;->PHOTOS_OVERFLOW:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/I83;->INVITE_OR_SHARE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/I83;->INVITE_OR_SHARE_OVERFLOW:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/I83;->SHARE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/I83;->SHARE_OVERFLOW:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/I83;->INVITE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/I83;->INVITE_OPTIONS:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/I83;->PRIVATE_INVITED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/I83;->PUBLIC_INVITED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/I83;->INVITE_OVERFLOW:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/I83;->POST:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/I83;->SAVE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/I83;->SAVED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/I83;->TOXICLE_PRIVATE_GOING:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/I83;->TOXICLE_PRIVATE_MAYBE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/I83;->TOXICLE_PRIVATE_CANT_GO:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/I83;->TOXICLE_PRIVATE_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/I83;->TOXICLE_PRIVATE_MAYBE_SELECTED_WITH_CHEVRON:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/I83;->TOXICLE_PRIVATE_CANT_GO_SELECTED_WITH_CHEVRON:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/I83;->TOXICLE_PUBLIC_IGNORE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/I83;->TOXICLE_PUBLIC_INTERESTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/I83;->TOXICLE_PUBLIC_INTERESTED_SELECTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/I83;->TOXICLE_PUBLIC_GOING:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/I83;->TOXICLE_PUBLIC_GOING_SELECTED:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/I83;->TOXICLE_PUBLIC_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/I83;->TOXICLE_PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/I83;->TOXICLE_PUBLIC_INTERESTED_OVERFLOW:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/I83;->PUBLIC_INVITE_QE_SEND:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/I83;->PUBLIC_INVITE_QE_SEND_OVERFLOW:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/I83;->PUBLIC_INVITE_QE_SHARE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/I83;->PUBLIC_INVITE_QE_SHARE_OVERFLOW:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/I83;->EDIT:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/I83;->EDIT_OVERFLOW:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/I83;->NOTIFICATION_SETTINGS:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/I83;->REPORT_EVENT:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/I83;->COPY_LINK:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/I83;->EXPORT_TO_CALENDAR:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/I83;->DELETE:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/I83;->CREATE_EVENT:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/I83;->DUPLICATE_EVENT:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/I83;->PUBLISH_EVENT:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/I83;->RESCHEDULE_EVENT:LX/I83;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/I83;->REMOVE_SCHEDULE:LX/I83;

    aput-object v2, v0, v1

    sput-object v0, LX/I83;->$VALUES:[LX/I83;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2540572
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2540573
    iput p3, p0, LX/I83;->mTitleResId:I

    .line 2540574
    iput p4, p0, LX/I83;->mIconResId:I

    .line 2540575
    return-void
.end method

.method public static fromOrdinal(I)LX/I83;
    .locals 1

    .prologue
    .line 2540571
    invoke-static {}, LX/I83;->values()[LX/I83;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/I83;
    .locals 1

    .prologue
    .line 2540570
    const-class v0, LX/I83;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I83;

    return-object v0
.end method

.method public static values()[LX/I83;
    .locals 1

    .prologue
    .line 2540569
    sget-object v0, LX/I83;->$VALUES:[LX/I83;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I83;

    return-object v0
.end method


# virtual methods
.method public final getIconResId()I
    .locals 1

    .prologue
    .line 2540568
    iget v0, p0, LX/I83;->mIconResId:I

    return v0
.end method

.method public final getTitleResId()I
    .locals 1

    .prologue
    .line 2540567
    iget v0, p0, LX/I83;->mTitleResId:I

    return v0
.end method

.method public final isOverflow()Z
    .locals 1

    .prologue
    .line 2540566
    iget v0, p0, LX/I83;->mIconResId:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
