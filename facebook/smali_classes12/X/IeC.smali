.class public final LX/IeC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IeD;


# direct methods
.method public constructor <init>(LX/IeD;)V
    .locals 0

    .prologue
    .line 2598389
    iput-object p1, p0, LX/IeC;->a:LX/IeD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 2598390
    check-cast p1, Lcom/facebook/user/model/User;

    check-cast p2, Lcom/facebook/user/model/User;

    .line 2598391
    iget v0, p1, Lcom/facebook/user/model/User;->m:F

    move v0, v0

    .line 2598392
    iget v1, p2, Lcom/facebook/user/model/User;->m:F

    move v1, v1

    .line 2598393
    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    .line 2598394
    const/4 v0, -0x1

    .line 2598395
    :goto_0
    return v0

    .line 2598396
    :cond_0
    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 2598397
    const/4 v0, 0x1

    goto :goto_0

    .line 2598398
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
