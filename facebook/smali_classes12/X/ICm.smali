.class public final LX/ICm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;",
        ">;",
        "LX/ICn;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;)V
    .locals 0

    .prologue
    .line 2549759
    iput-object p1, p0, LX/ICm;->a:Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2549760
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2549761
    if-eqz p1, :cond_0

    .line 2549762
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2549763
    if-eqz v0, :cond_0

    .line 2549764
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2549765
    check-cast v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->a()Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2549766
    :cond_0
    new-instance v0, LX/ICn;

    .line 2549767
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2549768
    invoke-static {}, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/ICn;-><init>(LX/0Px;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)V

    .line 2549769
    :goto_0
    return-object v0

    .line 2549770
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2549771
    check-cast v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->a()Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    move-result-object v2

    .line 2549772
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2549773
    invoke-virtual {v2}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;

    .line 2549774
    new-instance v6, LX/ICk;

    invoke-direct {v6, v0}, LX/ICk;-><init>(Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2549775
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2549776
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    .line 2549777
    :goto_2
    new-instance v1, LX/ICn;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/ICn;-><init>(LX/0Px;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)V

    move-object v0, v1

    goto :goto_0

    .line 2549778
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    goto :goto_2
.end method
