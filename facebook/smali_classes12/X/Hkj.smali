.class public LX/Hkj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, LX/Hkj;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, LX/Hkj;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, LX/Hkj;->c:Ljava/util/Map;

    return-void
.end method

.method public static a(LX/Hk0;)Z
    .locals 12

    const/4 v1, 0x0

    invoke-static {p0}, LX/Hkj;->d(LX/Hk0;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, LX/Hkj;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/Hkj;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, LX/Hk0;->c:LX/Hjw;

    move-object v0, v0

    const-wide/16 v8, -0x3e8

    sget-object v10, LX/Hkj;->a:Ljava/util/Map;

    invoke-interface {v10, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    sget-object v8, LX/Hkj;->a:Ljava/util/Map;

    invoke-interface {v8, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    :goto_1
    :pswitch_0
    move-wide v2, v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    cmp-long v0, v4, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v10, LX/Hki;->a:[I

    invoke-virtual {v0}, LX/Hjw;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    goto :goto_1

    :pswitch_1
    const-wide/16 v8, 0x3a98

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(LX/Hk0;)V
    .locals 4

    sget-object v0, LX/Hkj;->b:Ljava/util/Map;

    invoke-static {p0}, LX/Hkj;->d(LX/Hk0;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static d(LX/Hk0;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    const-string v2, "%s:%s:%s:%d:%d:%d"

    const/4 v0, 0x6

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v0, p0, LX/Hk0;->a:Ljava/lang/String;

    move-object v0, v0

    aput-object v0, v3, v1

    const/4 v0, 0x1

    iget-object v4, p0, LX/Hk0;->c:LX/Hjw;

    move-object v4, v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    iget-object v4, p0, LX/Hk0;->e:LX/Hk2;

    aput-object v4, v3, v0

    const/4 v4, 0x3

    iget-object v0, p0, LX/Hk0;->i:LX/Hj3;

    move-object v0, v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x4

    iget-object v4, p0, LX/Hk0;->i:LX/Hj3;

    move-object v4, v4

    if-nez v4, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x5

    iget v1, p0, LX/Hk0;->h:I

    move v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hk0;->i:LX/Hj3;

    move-object v0, v0

    invoke-virtual {v0}, LX/Hj3;->getHeight()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, LX/Hk0;->i:LX/Hj3;

    move-object v1, v1

    invoke-virtual {v1}, LX/Hj3;->getWidth()I

    move-result v1

    goto :goto_1
.end method
