.class public final LX/JHx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DHx;


# instance fields
.field public final synthetic a:LX/3OL;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/3LW;


# direct methods
.method public constructor <init>(LX/3LW;LX/3OL;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2677287
    iput-object p1, p0, LX/JHx;->c:LX/3LW;

    iput-object p2, p0, LX/JHx;->a:LX/3OL;

    iput-object p3, p0, LX/JHx;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V
    .locals 5

    .prologue
    .line 2677262
    sget-object v0, LX/JHz;->b:[I

    iget-object v1, p0, LX/JHx;->a:LX/3OL;

    invoke-virtual {v1}, LX/3OL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2677263
    iget-object v0, p0, LX/JHx;->c:LX/3LW;

    iget-object v0, v0, LX/3LW;->h:LX/3LQ;

    iget-object v1, p0, LX/JHx;->b:Ljava/lang/String;

    .line 2677264
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "friends_nearby_divebar_wave"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "background_location"

    .line 2677265
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2677266
    move-object v2, v2

    .line 2677267
    const-string v3, "target_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "action"

    const-string v4, "message"

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2677268
    iget-object v3, v0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2677269
    iget-object v0, p0, LX/JHx;->c:LX/3LW;

    invoke-virtual {p1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/JHx;->b:Ljava/lang/String;

    .line 2677270
    sget-object v3, LX/0ax;->ai:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2677271
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 2677272
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2677273
    const/high16 v3, 0x10000000

    invoke-virtual {v4, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2677274
    iget-object v3, v0, LX/3LW;->g:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2677275
    :goto_0
    return-void

    .line 2677276
    :pswitch_0
    iget-object v0, p0, LX/JHx;->c:LX/3LW;

    iget-object v0, v0, LX/3LW;->h:LX/3LQ;

    iget-object v1, p0, LX/JHx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3LQ;->c(Ljava/lang/String;)V

    .line 2677277
    iget-object v0, p0, LX/JHx;->c:LX/3LW;

    iget-object v0, v0, LX/3LW;->j:LX/3LX;

    iget-object v1, p0, LX/JHx;->b:Ljava/lang/String;

    new-instance v2, LX/JHu;

    invoke-direct {v2, p0, p1}, LX/JHu;-><init>(LX/JHx;Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    invoke-virtual {v0, v1, v2}, LX/3LX;->a(Ljava/lang/String;LX/DI1;)V

    goto :goto_0

    .line 2677278
    :pswitch_1
    iget-object v0, p0, LX/JHx;->c:LX/3LW;

    iget-object v0, v0, LX/3LW;->h:LX/3LQ;

    iget-object v1, p0, LX/JHx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3LQ;->c(Ljava/lang/String;)V

    .line 2677279
    iget-object v0, p0, LX/JHx;->c:LX/3LW;

    iget-object v0, v0, LX/3LW;->j:LX/3LX;

    iget-object v1, p0, LX/JHx;->b:Ljava/lang/String;

    new-instance v2, LX/JHv;

    invoke-direct {v2, p0, p1}, LX/JHv;-><init>(LX/JHx;Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    invoke-virtual {v0, v1, v2}, LX/3LX;->a(Ljava/lang/String;LX/DI1;)V

    goto :goto_0

    .line 2677280
    :pswitch_2
    iget-object v0, p0, LX/JHx;->c:LX/3LW;

    iget-object v0, v0, LX/3LW;->h:LX/3LQ;

    iget-object v1, p0, LX/JHx;->b:Ljava/lang/String;

    .line 2677281
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "friends_nearby_divebar_wave"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "background_location"

    .line 2677282
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2677283
    move-object v2, v2

    .line 2677284
    const-string v3, "target_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "action"

    const-string v4, "wave_canceled"

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2677285
    iget-object v3, v0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2677286
    iget-object v0, p0, LX/JHx;->c:LX/3LW;

    iget-object v0, v0, LX/3LW;->j:LX/3LX;

    iget-object v1, p0, LX/JHx;->b:Ljava/lang/String;

    new-instance v2, LX/JHw;

    invoke-direct {v2, p0, p1}, LX/JHw;-><init>(LX/JHx;Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;)V

    invoke-virtual {v0, v1, v2}, LX/3LX;->b(Ljava/lang/String;LX/DI1;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
