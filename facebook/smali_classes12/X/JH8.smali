.class public LX/JH8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1aX;

.field public final b:Landroid/content/Context;

.field public c:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/699;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/698;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/android/maps/model/LatLng;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/68w;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/1Ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ai",
            "<",
            "Lcom/facebook/imagepipeline/image/ImageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2672430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2672431
    new-instance v0, LX/JH7;

    invoke-direct {v0, p0}, LX/JH7;-><init>(LX/JH8;)V

    iput-object v0, p0, LX/JH8;->m:LX/1Ai;

    .line 2672432
    iput-object p1, p0, LX/JH8;->b:Landroid/content/Context;

    .line 2672433
    new-instance v0, LX/1Uo;

    iget-object v1, p0, LX/JH8;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, LX/1Up;->c:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    const/4 v1, 0x0

    .line 2672434
    iput v1, v0, LX/1Uo;->d:I

    .line 2672435
    move-object v0, v0

    .line 2672436
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    move-object v0, v0

    .line 2672437
    invoke-static {v0, p1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, LX/JH8;->a:LX/1aX;

    .line 2672438
    iget-object v0, p0, LX/JH8;->a:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2672439
    return-void
.end method

.method public static d(LX/JH8;)LX/68w;
    .locals 1

    .prologue
    .line 2672440
    iget-object v0, p0, LX/JH8;->l:LX/68w;

    if-eqz v0, :cond_0

    .line 2672441
    iget-object v0, p0, LX/JH8;->l:LX/68w;

    .line 2672442
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/690;->a(F)LX/68w;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2672443
    iget-object v0, p0, LX/JH8;->e:LX/698;

    if-nez v0, :cond_0

    .line 2672444
    :goto_0
    return-void

    .line 2672445
    :cond_0
    iget-object v0, p0, LX/JH8;->e:LX/698;

    invoke-static {p0}, LX/JH8;->d(LX/JH8;)LX/68w;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/698;->a(LX/68w;)V

    goto :goto_0
.end method
