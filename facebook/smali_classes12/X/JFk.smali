.class public final LX/JFk;
.super LX/4sh;
.source ""

# interfaces
.implements LX/2NW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4sh",
        "<",
        "Lcom/google/android/gms/location/places/personalized/PlaceUserData;",
        ">;",
        "LX/2NW;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget v0, p1, Lcom/google/android/gms/common/data/DataHolder;->h:I

    move v0, v0

    invoke-static {v0}, LX/JF6;->b(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/JFk;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->CREATOR:LX/JFl;

    invoke-direct {p0, p1, v0}, LX/4sh;-><init>(Lcom/google/android/gms/common/data/DataHolder;Landroid/os/Parcelable$Creator;)V

    if-eqz p1, :cond_0

    iget v0, p1, Lcom/google/android/gms/common/data/DataHolder;->h:I

    move v0, v0

    iget v1, p2, Lcom/google/android/gms/common/api/Status;->i:I

    move v1, v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/1ol;->b(Z)V

    iput-object p2, p0, LX/JFk;->b:Lcom/google/android/gms/common/api/Status;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, LX/JFk;->b:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method
