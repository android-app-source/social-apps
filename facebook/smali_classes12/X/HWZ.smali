.class public final LX/HWZ;
.super LX/CXx;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2476645
    iput-object p1, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-direct {p0, p2}, LX/CXx;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method

.method private a(LX/CXw;)V
    .locals 7

    .prologue
    .line 2476646
    iget-object v0, p1, LX/CXw;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iget-object v1, p1, LX/CXw;->c:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-static {v0, v1}, LX/HNE;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2476647
    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    if-eqz v0, :cond_0

    .line 2476648
    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a()V

    .line 2476649
    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->invalidate()V

    .line 2476650
    :cond_0
    :goto_0
    return-void

    .line 2476651
    :cond_1
    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-static {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->j(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;)V

    .line 2476652
    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-static {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->k(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;)V

    .line 2476653
    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iget-object v1, p1, LX/CXw;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iget-object v2, p1, LX/CXw;->c:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    iget-object v3, p1, LX/CXw;->d:LX/0ta;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->a(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/0ta;)LX/HN7;

    move-result-object v4

    .line 2476654
    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iget-object v6, v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->i:LX/HNE;

    iget-object v1, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p1, LX/CXw;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iget-object v3, p1, LX/CXw;->c:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/HNE;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/HN7;Z)LX/5fq;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a(LX/5fq;)V

    .line 2476655
    iget-object v0, p0, LX/HWZ;->b:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2476656
    check-cast p1, LX/CXw;

    invoke-direct {p0, p1}, LX/HWZ;->a(LX/CXw;)V

    return-void
.end method
