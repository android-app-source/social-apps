.class public final LX/HLK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HIR;

.field public final synthetic b:LX/HLL;


# direct methods
.method public constructor <init>(LX/HLL;LX/HIR;)V
    .locals 0

    .prologue
    .line 2455767
    iput-object p1, p0, LX/HLK;->b:LX/HLL;

    iput-object p2, p0, LX/HLK;->a:LX/HIR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2455768
    iget-object v0, p0, LX/HLK;->b:LX/HLL;

    iget-object v1, v0, LX/HLL;->a:LX/HLM;

    iget-object v2, p0, LX/HLK;->a:LX/HIR;

    iget-object v0, p0, LX/HLK;->a:LX/HIR;

    iget-boolean v0, v0, LX/HIR;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, LX/HLM;->a$redex0(LX/HLM;LX/HIR;Z)V

    .line 2455769
    return-void

    .line 2455770
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 2455771
    iget-object v0, p0, LX/HLK;->b:LX/HLL;

    iget-object v0, v0, LX/HLL;->a:LX/HLM;

    iget-object v0, v0, LX/HLM;->e:LX/9XE;

    iget-object v1, p0, LX/HLK;->b:LX/HLL;

    iget-object v1, v1, LX/HLL;->a:LX/HLM;

    iget-object v1, v1, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455772
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v2

    .line 2455773
    invoke-interface {v1}, LX/9uc;->M()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/HLK;->a:LX/HIR;

    iget-object v1, v1, LX/HIR;->a:Ljava/lang/String;

    iget-object v4, p0, LX/HLK;->a:LX/HIR;

    iget-boolean v4, v4, LX/HIR;->d:Z

    .line 2455774
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    sget-object p1, LX/9XA;->EVENT_CITY_HUB_PYML_MODULE_LIKE_ERROR:LX/9XA;

    invoke-static {p1, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "does_viewer_like"

    invoke-virtual {p1, p2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "tapped_page_id"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v5, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2455775
    iget-object v0, p0, LX/HLK;->b:LX/HLL;

    iget-object v0, v0, LX/HLL;->a:LX/HLM;

    iget-object v1, p0, LX/HLK;->a:LX/HIR;

    iget-object v2, p0, LX/HLK;->a:LX/HIR;

    iget-boolean v2, v2, LX/HIR;->d:Z

    invoke-static {v0, v1, v2}, LX/HLM;->a$redex0(LX/HLM;LX/HIR;Z)V

    .line 2455776
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2455777
    iget-object v0, p0, LX/HLK;->b:LX/HLL;

    iget-object v0, v0, LX/HLL;->a:LX/HLM;

    iget-object v0, v0, LX/HLM;->e:LX/9XE;

    iget-object v1, p0, LX/HLK;->b:LX/HLL;

    iget-object v1, v1, LX/HLL;->a:LX/HLM;

    iget-object v1, v1, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455778
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v2

    .line 2455779
    invoke-interface {v1}, LX/9uc;->M()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/HLK;->a:LX/HIR;

    iget-object v1, v1, LX/HIR;->a:Ljava/lang/String;

    iget-object v4, p0, LX/HLK;->a:LX/HIR;

    iget-boolean v4, v4, LX/HIR;->d:Z

    .line 2455780
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    sget-object p0, LX/9XB;->EVENT_CITY_HUB_PYML_MODULE_LIKE_SUCCESS:LX/9XB;

    invoke-static {p0, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "does_viewer_like"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "tapped_page_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v5, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2455781
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2455782
    iget-object v0, p0, LX/HLK;->b:LX/HLL;

    iget-object v0, v0, LX/HLL;->a:LX/HLM;

    iget-object v1, p0, LX/HLK;->a:LX/HIR;

    iget-object v2, p0, LX/HLK;->a:LX/HIR;

    iget-boolean v2, v2, LX/HIR;->d:Z

    invoke-static {v0, v1, v2}, LX/HLM;->a$redex0(LX/HLM;LX/HIR;Z)V

    .line 2455783
    return-void
.end method
