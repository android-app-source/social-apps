.class public final LX/ISA;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/INZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ISI;


# direct methods
.method public constructor <init>(LX/ISI;LX/DML;)V
    .locals 0

    .prologue
    .line 2577544
    iput-object p1, p0, LX/ISA;->a:LX/ISI;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 2577515
    check-cast p1, LX/INZ;

    .line 2577516
    iget-object v0, p0, LX/ISA;->a:LX/ISI;

    iget-object v0, v0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    const/4 v6, 0x0

    const/16 p0, 0x8

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 2577517
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2577518
    :cond_0
    :goto_0
    return-void

    .line 2577519
    :cond_1
    const-string v1, "community_questions_nux_shown"

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, LX/INZ;->a$redex0(LX/INZ;Ljava/lang/String;Ljava/lang/String;)V

    .line 2577520
    iget-object v2, p1, LX/INZ;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, LX/INZ;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082ff5

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    iget-object v1, p1, LX/INZ;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577521
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->e()LX/0Px;

    move-result-object v2

    .line 2577522
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->d()LX/0Px;

    move-result-object v1

    .line 2577523
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3, v1, v2}, LX/INZ;->a(LX/INZ;Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/util/Set;

    move-result-object v3

    .line 2577524
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 2577525
    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    .line 2577526
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2577527
    iget-object v4, p1, LX/INZ;->j:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v5, v6, v1}, LX/INZ;->a(LX/INZ;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-static {v4, v1, v5}, LX/INZ;->a(Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;Landroid/view/View$OnClickListener;)V

    .line 2577528
    :cond_2
    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    if-le v1, v7, :cond_3

    .line 2577529
    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    .line 2577530
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2577531
    iget-object v4, p1, LX/INZ;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v5, v6, v1}, LX/INZ;->a(LX/INZ;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-static {v4, v1, v5}, LX/INZ;->a(Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;Landroid/view/View$OnClickListener;)V

    .line 2577532
    :cond_3
    :goto_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    if-le v1, v8, :cond_4

    .line 2577533
    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    .line 2577534
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2577535
    iget-object v2, p1, LX/INZ;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v4, v5, v1}, LX/INZ;->a(LX/INZ;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-static {v2, v1, v4}, LX/INZ;->a(Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;Landroid/view/View$OnClickListener;)V

    .line 2577536
    :cond_4
    :goto_3
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2577537
    iget-object v1, p1, LX/INZ;->m:Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;

    invoke-virtual {v1}, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->a()V

    .line 2577538
    iget-object v1, p1, LX/INZ;->h:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f082ff7

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2577539
    iget-object v1, p1, LX/INZ;->i:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f082ff8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2577540
    :cond_5
    iget-object v1, p1, LX/INZ;->n:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/INX;

    invoke-direct {v2, p1, v0}, LX/INX;-><init>(LX/INZ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2577541
    :cond_6
    iget-object v1, p1, LX/INZ;->j:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2577542
    :cond_7
    iget-object v1, p1, LX/INZ;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    goto :goto_2

    .line 2577543
    :cond_8
    iget-object v1, p1, LX/INZ;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    goto :goto_3
.end method
