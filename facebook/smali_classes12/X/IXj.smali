.class public final LX/IXj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Landroid/support/v4/view/ViewPager;

.field public final synthetic b:LX/IXn;


# direct methods
.method public constructor <init>(LX/IXn;Landroid/support/v4/view/ViewPager;)V
    .locals 0

    .prologue
    .line 2586575
    iput-object p1, p0, LX/IXj;->b:LX/IXn;

    iput-object p2, p0, LX/IXj;->a:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 2586576
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2586577
    iget-object v1, p0, LX/IXj;->a:Landroid/support/v4/view/ViewPager;

    .line 2586578
    iget-boolean v2, v1, Landroid/support/v4/view/ViewPager;->S:Z

    move v1, v2

    .line 2586579
    if-nez v1, :cond_0

    .line 2586580
    iget-object v1, p0, LX/IXj;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->c()Z

    .line 2586581
    :cond_0
    iget-object v1, p0, LX/IXj;->a:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, LX/IXj;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->a(F)V

    .line 2586582
    return-void
.end method
