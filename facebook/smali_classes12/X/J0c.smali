.class public LX/J0c;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;",
        "Lcom/facebook/payments/p2p/model/PaymentTransaction;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/J0c;


# instance fields
.field private final b:LX/J0h;


# direct methods
.method public constructor <init>(LX/J0h;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637421
    invoke-direct {p0, p2}, LX/0ro;-><init>(LX/0sO;)V

    .line 2637422
    iput-object p1, p0, LX/J0c;->b:LX/J0h;

    .line 2637423
    return-void
.end method

.method public static a(LX/0QB;)LX/J0c;
    .locals 5

    .prologue
    .line 2637408
    sget-object v0, LX/J0c;->c:LX/J0c;

    if-nez v0, :cond_1

    .line 2637409
    const-class v1, LX/J0c;

    monitor-enter v1

    .line 2637410
    :try_start_0
    sget-object v0, LX/J0c;->c:LX/J0c;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637411
    if-eqz v2, :cond_0

    .line 2637412
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2637413
    new-instance p0, LX/J0c;

    invoke-static {v0}, LX/J0h;->b(LX/0QB;)LX/J0h;

    move-result-object v3

    check-cast v3, LX/J0h;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v4

    check-cast v4, LX/0sO;

    invoke-direct {p0, v3, v4}, LX/J0c;-><init>(LX/J0h;LX/0sO;)V

    .line 2637414
    move-object v0, p0

    .line 2637415
    sput-object v0, LX/J0c;->c:LX/J0c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637416
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637417
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637418
    :cond_1
    sget-object v0, LX/J0c;->c:LX/J0c;

    return-object v0

    .line 2637419
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637420
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2637406
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2637407
    iget-object v1, p0, LX/J0c;->b:LX/J0h;

    invoke-virtual {v1, v0}, LX/J0h;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;)Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2637400
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2637401
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;

    .line 2637402
    new-instance v0, LX/Dtg;

    invoke-direct {v0}, LX/Dtg;-><init>()V

    move-object v0, v0

    .line 2637403
    const-string v1, "transaction_id"

    .line 2637404
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2637405
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
