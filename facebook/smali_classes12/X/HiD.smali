.class public final LX/HiD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[I

.field public final b:[I

.field public final c:I


# direct methods
.method public constructor <init>([I)V
    .locals 6

    .prologue
    .line 2497044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497045
    invoke-static {p1}, Ljava/util/Arrays;->sort([I)V

    .line 2497046
    const/4 v0, 0x1

    .line 2497047
    array-length v1, p1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    .line 2497048
    array-length v2, p1

    .line 2497049
    :cond_0
    move v0, v2

    .line 2497050
    iput v0, p0, LX/HiD;->c:I

    .line 2497051
    iget v0, p0, LX/HiD;->c:I

    new-array v0, v0, [I

    iput-object v0, p0, LX/HiD;->a:[I

    .line 2497052
    iget v0, p0, LX/HiD;->c:I

    new-array v0, v0, [I

    iput-object v0, p0, LX/HiD;->b:[I

    .line 2497053
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2497054
    array-length v0, p1

    if-nez v0, :cond_4

    .line 2497055
    :cond_1
    return-void

    .line 2497056
    :cond_2
    const/4 v1, 0x0

    aget v1, p1, v1

    move v2, v0

    .line 2497057
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 2497058
    aget v3, p1, v0

    if-eq v3, v1, :cond_3

    .line 2497059
    aget v1, p1, v0

    .line 2497060
    add-int/lit8 v2, v2, 0x1

    .line 2497061
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2497062
    :cond_4
    aget v0, p1, v2

    .line 2497063
    iget-object v3, p0, LX/HiD;->a:[I

    aput v0, v3, v2

    .line 2497064
    iget-object v3, p0, LX/HiD;->b:[I

    aput v1, v3, v2

    .line 2497065
    array-length v3, p1

    if-eq v3, v1, :cond_1

    move v3, v2

    move v2, v0

    move v0, v1

    .line 2497066
    :goto_1
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 2497067
    aget v4, p1, v0

    if-ne v4, v2, :cond_5

    .line 2497068
    iget-object v4, p0, LX/HiD;->b:[I

    aget v5, v4, v3

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v3

    .line 2497069
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2497070
    :cond_5
    aget v2, p1, v0

    .line 2497071
    add-int/lit8 v3, v3, 0x1

    .line 2497072
    iget-object v4, p0, LX/HiD;->a:[I

    aput v2, v4, v3

    .line 2497073
    iget-object v4, p0, LX/HiD;->b:[I

    aput v1, v4, v3

    goto :goto_2
.end method
