.class public LX/JOm;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JOn;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JOm",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JOn;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2688200
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2688201
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JOm;->b:LX/0Zi;

    .line 2688202
    iput-object p1, p0, LX/JOm;->a:LX/0Ot;

    .line 2688203
    return-void
.end method

.method public static a(LX/0QB;)LX/JOm;
    .locals 4

    .prologue
    .line 2688204
    const-class v1, LX/JOm;

    monitor-enter v1

    .line 2688205
    :try_start_0
    sget-object v0, LX/JOm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688206
    sput-object v2, LX/JOm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688207
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688208
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688209
    new-instance v3, LX/JOm;

    const/16 p0, 0x1fcc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JOm;-><init>(LX/0Ot;)V

    .line 2688210
    move-object v0, v3

    .line 2688211
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688212
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688213
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2688215
    check-cast p2, LX/JOk;

    .line 2688216
    iget-object v0, p0, LX/JOm;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JOn;

    iget-object v1, p2, LX/JOk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JOk;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p2, LX/JOk;->c:LX/1Pn;

    .line 2688217
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b257f

    invoke-interface {v4, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b2580

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3d

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    iget-object v4, v0, LX/JOn;->a:LX/JOi;

    const/4 v6, 0x0

    .line 2688218
    new-instance p0, LX/JOg;

    invoke-direct {p0, v4}, LX/JOg;-><init>(LX/JOi;)V

    .line 2688219
    iget-object p2, v4, LX/JOi;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JOh;

    .line 2688220
    if-nez p2, :cond_0

    .line 2688221
    new-instance p2, LX/JOh;

    invoke-direct {p2, v4}, LX/JOh;-><init>(LX/JOi;)V

    .line 2688222
    :cond_0
    invoke-static {p2, p1, v6, v6, p0}, LX/JOh;->a$redex0(LX/JOh;LX/1De;IILX/JOg;)V

    .line 2688223
    move-object p0, p2

    .line 2688224
    move-object v6, p0

    .line 2688225
    move-object v4, v6

    .line 2688226
    iget-object v6, v4, LX/JOh;->a:LX/JOg;

    iput-object v1, v6, LX/JOg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2688227
    iget-object v6, v4, LX/JOh;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2688228
    move-object v4, v4

    .line 2688229
    iget-object v6, v4, LX/JOh;->a:LX/JOg;

    iput-object v2, v6, LX/JOg;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688230
    iget-object v6, v4, LX/JOh;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2688231
    move-object v6, v4

    .line 2688232
    move-object v4, v3

    check-cast v4, LX/1Po;

    .line 2688233
    iget-object p0, v6, LX/JOh;->a:LX/JOg;

    iput-object v4, p0, LX/JOg;->c:LX/1Po;

    .line 2688234
    iget-object p0, v6, LX/JOh;->e:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2688235
    move-object v4, v6

    .line 2688236
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JOn;->b:LX/JOt;

    invoke-virtual {v5, p1}, LX/JOt;->c(LX/1De;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/JOs;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/JOs;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/JOs;->a(LX/1Pn;)LX/JOs;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b2581

    invoke-interface {v4, v5, v6}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2688237
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2688238
    invoke-static {}, LX/1dS;->b()V

    .line 2688239
    const/4 v0, 0x0

    return-object v0
.end method
