.class public LX/IIE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field private static d:LX/0Xm;


# instance fields
.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2561608
    sget-object v0, LX/IFK;->a:LX/0Tn;

    const-string v1, "visit_counters/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2561609
    sput-object v0, LX/IIE;->a:LX/0Tn;

    const-string v1, "to_list"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/IIE;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2561610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2561611
    iput-object p1, p0, LX/IIE;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2561612
    return-void
.end method

.method public static a(LX/0QB;)LX/IIE;
    .locals 4

    .prologue
    .line 2561613
    const-class v1, LX/IIE;

    monitor-enter v1

    .line 2561614
    :try_start_0
    sget-object v0, LX/IIE;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2561615
    sput-object v2, LX/IIE;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2561616
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2561617
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2561618
    new-instance p0, LX/IIE;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/IIE;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2561619
    move-object v0, p0

    .line 2561620
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2561621
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IIE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2561622
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2561623
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2561624
    sget-object v0, LX/IIE;->b:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 2561625
    iget-object v0, p0, LX/IIE;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/IIE;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method
