.class public final enum LX/JHX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JHX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JHX;

.field public static final enum BUFFERING:LX/JHX;

.field public static final enum ENDED:LX/JHX;

.field public static final enum ERROR:LX/JHX;

.field public static final enum IDLE:LX/JHX;

.field public static final enum PLAYING:LX/JHX;

.field public static final enum PREPARING:LX/JHX;

.field public static final enum READY:LX/JHX;

.field public static final enum UNDEFINED:LX/JHX;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2673166
    new-instance v0, LX/JHX;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, LX/JHX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JHX;->IDLE:LX/JHX;

    .line 2673167
    new-instance v0, LX/JHX;

    const-string v1, "PREPARING"

    invoke-direct {v0, v1, v4}, LX/JHX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JHX;->PREPARING:LX/JHX;

    .line 2673168
    new-instance v0, LX/JHX;

    const-string v1, "READY"

    invoke-direct {v0, v1, v5}, LX/JHX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JHX;->READY:LX/JHX;

    .line 2673169
    new-instance v0, LX/JHX;

    const-string v1, "BUFFERING"

    invoke-direct {v0, v1, v6}, LX/JHX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JHX;->BUFFERING:LX/JHX;

    .line 2673170
    new-instance v0, LX/JHX;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v7}, LX/JHX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JHX;->PLAYING:LX/JHX;

    .line 2673171
    new-instance v0, LX/JHX;

    const-string v1, "ENDED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/JHX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JHX;->ENDED:LX/JHX;

    .line 2673172
    new-instance v0, LX/JHX;

    const-string v1, "ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/JHX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JHX;->ERROR:LX/JHX;

    .line 2673173
    new-instance v0, LX/JHX;

    const-string v1, "UNDEFINED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/JHX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JHX;->UNDEFINED:LX/JHX;

    .line 2673174
    const/16 v0, 0x8

    new-array v0, v0, [LX/JHX;

    sget-object v1, LX/JHX;->IDLE:LX/JHX;

    aput-object v1, v0, v3

    sget-object v1, LX/JHX;->PREPARING:LX/JHX;

    aput-object v1, v0, v4

    sget-object v1, LX/JHX;->READY:LX/JHX;

    aput-object v1, v0, v5

    sget-object v1, LX/JHX;->BUFFERING:LX/JHX;

    aput-object v1, v0, v6

    sget-object v1, LX/JHX;->PLAYING:LX/JHX;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/JHX;->ENDED:LX/JHX;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/JHX;->ERROR:LX/JHX;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/JHX;->UNDEFINED:LX/JHX;

    aput-object v2, v0, v1

    sput-object v0, LX/JHX;->$VALUES:[LX/JHX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2673175
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JHX;
    .locals 1

    .prologue
    .line 2673176
    const-class v0, LX/JHX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JHX;

    return-object v0
.end method

.method public static values()[LX/JHX;
    .locals 1

    .prologue
    .line 2673177
    sget-object v0, LX/JHX;->$VALUES:[LX/JHX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JHX;

    return-object v0
.end method
