.class public LX/JLi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/JLi;


# instance fields
.field public a:LX/JLU;

.field public final b:LX/JLh;

.field public final c:LX/JLg;

.field public final d:LX/0b3;

.field public final e:Landroid/os/Handler;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0b3;Landroid/os/Handler;)V
    .locals 2
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2681438
    new-instance v0, LX/JLh;

    invoke-direct {v0, p0}, LX/JLh;-><init>(LX/JLi;)V

    iput-object v0, p0, LX/JLi;->b:LX/JLh;

    .line 2681439
    new-instance v0, LX/JLg;

    invoke-direct {v0, p0}, LX/JLg;-><init>(LX/JLi;)V

    iput-object v0, p0, LX/JLi;->c:LX/JLg;

    .line 2681440
    iput-object p1, p0, LX/JLi;->d:LX/0b3;

    .line 2681441
    iput-object p2, p0, LX/JLi;->e:Landroid/os/Handler;

    .line 2681442
    return-void
.end method

.method public static a(LX/0QB;)LX/JLi;
    .locals 5

    .prologue
    .line 2681443
    sget-object v0, LX/JLi;->h:LX/JLi;

    if-nez v0, :cond_1

    .line 2681444
    const-class v1, LX/JLi;

    monitor-enter v1

    .line 2681445
    :try_start_0
    sget-object v0, LX/JLi;->h:LX/JLi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2681446
    if-eqz v2, :cond_0

    .line 2681447
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2681448
    new-instance p0, LX/JLi;

    invoke-static {v0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v3

    check-cast v3, LX/0b3;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-direct {p0, v3, v4}, LX/JLi;-><init>(LX/0b3;Landroid/os/Handler;)V

    .line 2681449
    move-object v0, p0

    .line 2681450
    sput-object v0, LX/JLi;->h:LX/JLi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2681451
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2681452
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2681453
    :cond_1
    sget-object v0, LX/JLi;->h:LX/JLi;

    return-object v0

    .line 2681454
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2681455
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/JLi;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2681456
    iput-object v2, p0, LX/JLi;->f:Ljava/lang/String;

    .line 2681457
    iget-object v0, p0, LX/JLi;->g:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2681458
    iget-object v0, p0, LX/JLi;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/JLi;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2681459
    iput-object v2, p0, LX/JLi;->g:Ljava/lang/Runnable;

    .line 2681460
    :cond_0
    iget-object v0, p0, LX/JLi;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2681461
    iget-object v0, p0, LX/JLi;->d:LX/0b3;

    iget-object v1, p0, LX/JLi;->b:LX/JLh;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2681462
    iget-object v0, p0, LX/JLi;->d:LX/0b3;

    iget-object v1, p0, LX/JLi;->c:LX/JLg;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2681463
    :cond_1
    return-void
.end method
