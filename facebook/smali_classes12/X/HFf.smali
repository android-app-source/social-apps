.class public LX/HFf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HFf;


# instance fields
.field public final a:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2443991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2443992
    iput-object p1, p0, LX/HFf;->a:LX/0if;

    .line 2443993
    return-void
.end method

.method public static a(LX/0QB;)LX/HFf;
    .locals 4

    .prologue
    .line 2444006
    sget-object v0, LX/HFf;->b:LX/HFf;

    if-nez v0, :cond_1

    .line 2444007
    const-class v1, LX/HFf;

    monitor-enter v1

    .line 2444008
    :try_start_0
    sget-object v0, LX/HFf;->b:LX/HFf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2444009
    if-eqz v2, :cond_0

    .line 2444010
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2444011
    new-instance p0, LX/HFf;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/HFf;-><init>(LX/0if;)V

    .line 2444012
    move-object v0, p0

    .line 2444013
    sput-object v0, LX/HFf;->b:LX/HFf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2444014
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2444015
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2444016
    :cond_1
    sget-object v0, LX/HFf;->b:LX/HFf;

    return-object v0

    .line 2444017
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2444018
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2444004
    iget-object v0, p0, LX/HFf;->a:LX/0if;

    sget-object v1, LX/0ig;->ao:LX/0ih;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "backFrom_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2444005
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 5

    .prologue
    .line 2444000
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 2444001
    iget-object v2, p0, LX/HFf;->a:LX/0if;

    sget-object v3, LX/0ig;->ao:LX/0ih;

    const-string v4, "name_check"

    if-eqz p1, :cond_0

    const-string v0, "passed"

    :goto_0
    invoke-virtual {v2, v3, v4, v0, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2444002
    return-void

    .line 2444003
    :cond_0
    const-string v0, "failed"

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2444019
    iget-object v0, p0, LX/HFf;->a:LX/0if;

    sget-object v1, LX/0ig;->ao:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2444020
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2443998
    iget-object v0, p0, LX/HFf;->a:LX/0if;

    sget-object v1, LX/0ig;->ao:LX/0ih;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "skipped_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2443999
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2443996
    iget-object v0, p0, LX/HFf;->a:LX/0if;

    sget-object v1, LX/0ig;->ao:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2443997
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2443994
    iget-object v0, p0, LX/HFf;->a:LX/0if;

    sget-object v1, LX/0ig;->ao:LX/0ih;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "finished_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2443995
    return-void
.end method
