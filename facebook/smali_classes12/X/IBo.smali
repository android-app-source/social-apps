.class public LX/IBo;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements LX/IBX;


# instance fields
.field public a:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IBl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2547678
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2547679
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/IBo;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object p1

    check-cast p1, LX/6RZ;

    invoke-static {v0}, LX/IBl;->b(LX/0QB;)LX/IBl;

    move-result-object v0

    check-cast v0, LX/IBl;

    iput-object p1, p0, LX/IBo;->a:LX/6RZ;

    iput-object v0, p0, LX/IBo;->b:LX/IBl;

    .line 2547680
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 4
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547681
    iget-object v0, p0, LX/IBo;->a:LX/6RZ;

    .line 2547682
    iget-boolean v1, p1, Lcom/facebook/events/model/Event;->N:Z

    move v1, v1

    .line 2547683
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/6RZ;->c(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2547684
    iget-object v1, p0, LX/IBo;->a:LX/6RZ;

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v3

    .line 2547685
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    invoke-static {v1, v2, v3, p1}, LX/6RZ;->a(LX/6RZ;Ljava/util/Date;Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    .line 2547686
    iget-object v2, p0, LX/IBo;->b:LX/IBl;

    invoke-virtual {v2, p0, v0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 2547687
    iget-object v0, p0, LX/IBo;->b:LX/IBl;

    const v1, 0x7f02033c

    invoke-virtual {v0, p0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;IZ)V

    .line 2547688
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 1
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547689
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
