.class public LX/HiF;
.super LX/HiE;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HiH;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field public c:LX/HiH;

.field private d:LX/HiH;

.field public e:LX/HiH;

.field private f:LX/HiH;

.field private g:LX/HiH;

.field private h:LX/HiH;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2497128
    invoke-direct {p0}, LX/HiE;-><init>()V

    return-void
.end method

.method public static a(FF)F
    .locals 2

    .prologue
    .line 2497078
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method private static a(FFFFII)F
    .locals 9

    .prologue
    .line 2497117
    const/high16 v2, 0x40400000    # 3.0f

    const/high16 v5, 0x40c00000    # 6.0f

    const/high16 v8, 0x3f800000    # 1.0f

    move v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move v6, p4

    move v7, p5

    .line 2497118
    const/4 p0, 0x6

    new-array p0, p0, [F

    const/4 p1, 0x0

    invoke-static {v0, v1}, LX/HiF;->a(FF)F

    move-result p2

    aput p2, p0, p1

    const/4 p1, 0x1

    aput v2, p0, p1

    const/4 p1, 0x2

    invoke-static {v3, v4}, LX/HiF;->a(FF)F

    move-result p2

    aput p2, p0, p1

    const/4 p1, 0x3

    aput v5, p0, p1

    const/4 p1, 0x4

    int-to-float p2, v6

    int-to-float p3, v7

    div-float/2addr p2, p3

    aput p2, p0, p1

    const/4 p1, 0x5

    aput v8, p0, p1

    const/4 p2, 0x0

    .line 2497119
    const/4 p1, 0x0

    move p3, p2

    :goto_0
    array-length v0, p0

    if-ge p1, v0, :cond_0

    .line 2497120
    aget v0, p0, p1

    .line 2497121
    add-int/lit8 v1, p1, 0x1

    aget v1, p0, v1

    .line 2497122
    mul-float/2addr v0, v1

    add-float/2addr p3, v0

    .line 2497123
    add-float/2addr p2, v1

    .line 2497124
    add-int/lit8 p1, p1, 0x2

    goto :goto_0

    .line 2497125
    :cond_0
    div-float p1, p3, p2

    move p0, p1

    .line 2497126
    move v0, p0

    .line 2497127
    return v0
.end method

.method private static a(LX/HiF;FFFFFF)LX/HiH;
    .locals 10

    .prologue
    .line 2497107
    const/4 v8, 0x0

    .line 2497108
    const/4 v7, 0x0

    .line 2497109
    iget-object v0, p0, LX/HiF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/HiH;

    .line 2497110
    invoke-virtual {v6}, LX/HiH;->b()[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1

    .line 2497111
    invoke-virtual {v6}, LX/HiH;->b()[F

    move-result-object v1

    const/4 v2, 0x2

    aget v2, v1, v2

    .line 2497112
    cmpl-float v1, v0, p5

    if-ltz v1, :cond_2

    cmpg-float v1, v0, p6

    if-gtz v1, :cond_2

    cmpl-float v1, v2, p2

    if-ltz v1, :cond_2

    cmpg-float v1, v2, p3

    if-gtz v1, :cond_2

    invoke-direct {p0, v6}, LX/HiF;->a(LX/HiH;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2497113
    invoke-virtual {v6}, LX/HiH;->c()I

    move-result v4

    iget v5, p0, LX/HiF;->b:I

    move v1, p4

    move v3, p1

    invoke-static/range {v0 .. v5}, LX/HiF;->a(FFFFII)F

    move-result v0

    .line 2497114
    if-eqz v8, :cond_0

    cmpl-float v1, v0, v7

    if-lez v1, :cond_2

    :cond_0
    :goto_1
    move v7, v0

    move-object v8, v6

    .line 2497115
    goto :goto_0

    .line 2497116
    :cond_1
    return-object v8

    :cond_2
    move v0, v7

    move-object v6, v8

    goto :goto_1
.end method

.method private a(LX/HiH;)Z
    .locals 1

    .prologue
    .line 2497106
    iget-object v0, p0, LX/HiF;->c:LX/HiH;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/HiF;->e:LX/HiH;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/HiF;->g:LX/HiH;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/HiF;->d:LX/HiH;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/HiF;->f:LX/HiH;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/HiF;->h:LX/HiH;

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/HiH;)[F
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 2497103
    new-array v0, v3, [F

    .line 2497104
    invoke-virtual {p0}, LX/HiH;->b()[F

    move-result-object v1

    invoke-static {v1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2497105
    return-object v0
.end method

.method private d()V
    .locals 15

    .prologue
    const v14, 0x3ecccccd    # 0.4f

    const v5, 0x3eb33333    # 0.35f

    const v2, 0x3e99999a    # 0.3f

    const/4 v13, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 2497129
    const/high16 v1, 0x3f000000    # 0.5f

    const v3, 0x3f333333    # 0.7f

    move-object v0, p0

    move v6, v4

    invoke-static/range {v0 .. v6}, LX/HiF;->a(LX/HiF;FFFFFF)LX/HiH;

    move-result-object v0

    iput-object v0, p0, LX/HiF;->c:LX/HiH;

    .line 2497130
    const v7, 0x3f3d70a4    # 0.74f

    const v8, 0x3f0ccccd    # 0.55f

    move-object v6, p0

    move v9, v4

    move v10, v4

    move v11, v5

    move v12, v4

    invoke-static/range {v6 .. v12}, LX/HiF;->a(LX/HiF;FFFFFF)LX/HiH;

    move-result-object v0

    iput-object v0, p0, LX/HiF;->g:LX/HiH;

    .line 2497131
    const v7, 0x3e851eb8    # 0.26f

    const v9, 0x3ee66666    # 0.45f

    move-object v6, p0

    move v8, v13

    move v10, v4

    move v11, v5

    move v12, v4

    invoke-static/range {v6 .. v12}, LX/HiF;->a(LX/HiF;FFFFFF)LX/HiH;

    move-result-object v0

    iput-object v0, p0, LX/HiF;->e:LX/HiH;

    .line 2497132
    const/high16 v6, 0x3f000000    # 0.5f

    const v8, 0x3f333333    # 0.7f

    move-object v5, p0

    move v7, v2

    move v9, v2

    move v10, v13

    move v11, v14

    invoke-static/range {v5 .. v11}, LX/HiF;->a(LX/HiF;FFFFFF)LX/HiH;

    move-result-object v0

    iput-object v0, p0, LX/HiF;->d:LX/HiH;

    .line 2497133
    const v6, 0x3f3d70a4    # 0.74f

    const v7, 0x3f0ccccd    # 0.55f

    move-object v5, p0

    move v8, v4

    move v9, v2

    move v10, v13

    move v11, v14

    invoke-static/range {v5 .. v11}, LX/HiF;->a(LX/HiF;FFFFFF)LX/HiH;

    move-result-object v0

    iput-object v0, p0, LX/HiF;->h:LX/HiH;

    .line 2497134
    const v4, 0x3e851eb8    # 0.26f

    const v6, 0x3ee66666    # 0.45f

    move-object v3, p0

    move v5, v13

    move v7, v2

    move v8, v13

    move v9, v14

    invoke-static/range {v3 .. v9}, LX/HiF;->a(LX/HiF;FFFFFF)LX/HiH;

    move-result-object v0

    iput-object v0, p0, LX/HiF;->f:LX/HiH;

    .line 2497135
    return-void
.end method

.method private f()I
    .locals 3

    .prologue
    .line 2497097
    const/4 v0, 0x0

    .line 2497098
    iget-object v1, p0, LX/HiF;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HiH;

    .line 2497099
    iget p0, v0, LX/HiH;->e:I

    move v0, p0

    .line 2497100
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 2497101
    goto :goto_0

    .line 2497102
    :cond_0
    return v1
.end method


# virtual methods
.method public final a()LX/HiH;
    .locals 1

    .prologue
    .line 2497096
    iget-object v0, p0, LX/HiF;->c:LX/HiH;

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/HiH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2497081
    iput-object p1, p0, LX/HiF;->a:Ljava/util/List;

    .line 2497082
    invoke-direct {p0}, LX/HiF;->f()I

    move-result v0

    iput v0, p0, LX/HiF;->b:I

    .line 2497083
    invoke-direct {p0}, LX/HiF;->d()V

    .line 2497084
    const/4 p1, 0x2

    const/4 v2, 0x0

    .line 2497085
    iget-object v0, p0, LX/HiF;->c:LX/HiH;

    if-nez v0, :cond_0

    .line 2497086
    iget-object v0, p0, LX/HiF;->e:LX/HiH;

    if-eqz v0, :cond_0

    .line 2497087
    iget-object v0, p0, LX/HiF;->e:LX/HiH;

    invoke-static {v0}, LX/HiF;->b(LX/HiH;)[F

    move-result-object v0

    .line 2497088
    const/high16 v1, 0x3f000000    # 0.5f

    aput v1, v0, p1

    .line 2497089
    new-instance v1, LX/HiH;

    invoke-static {v0}, LX/3qk;->a([F)I

    move-result v0

    invoke-direct {v1, v0, v2}, LX/HiH;-><init>(II)V

    iput-object v1, p0, LX/HiF;->c:LX/HiH;

    .line 2497090
    :cond_0
    iget-object v0, p0, LX/HiF;->e:LX/HiH;

    if-nez v0, :cond_1

    .line 2497091
    iget-object v0, p0, LX/HiF;->c:LX/HiH;

    if-eqz v0, :cond_1

    .line 2497092
    iget-object v0, p0, LX/HiF;->c:LX/HiH;

    invoke-static {v0}, LX/HiF;->b(LX/HiH;)[F

    move-result-object v0

    .line 2497093
    const v1, 0x3e851eb8    # 0.26f

    aput v1, v0, p1

    .line 2497094
    new-instance v1, LX/HiH;

    invoke-static {v0}, LX/3qk;->a([F)I

    move-result v0

    invoke-direct {v1, v0, v2}, LX/HiH;-><init>(II)V

    iput-object v1, p0, LX/HiF;->e:LX/HiH;

    .line 2497095
    :cond_1
    return-void
.end method

.method public final b()LX/HiH;
    .locals 1

    .prologue
    .line 2497080
    iget-object v0, p0, LX/HiF;->e:LX/HiH;

    return-object v0
.end method

.method public final c()LX/HiH;
    .locals 1

    .prologue
    .line 2497079
    iget-object v0, p0, LX/HiF;->f:LX/HiH;

    return-object v0
.end method
