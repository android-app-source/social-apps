.class public final LX/HOA;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459665
    iput-object p1, p0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 8

    .prologue
    .line 2459666
    iget-object v0, p0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a()LX/CY5;

    move-result-object v0

    sget-object v1, LX/CY5;->NONE:LX/CY5;

    if-eq v0, v1, :cond_0

    .line 2459667
    :goto_0
    return-void

    .line 2459668
    :cond_0
    iget-object v0, p0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459669
    iget-object v0, p0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "send_user_request_key"

    iget-object v2, p0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->c:LX/HNV;

    iget-object v3, p0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v2

    iget-object v3, p0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->e:Ljava/lang/String;

    iget-object v4, p0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getFieldValues()Ljava/util/Map;

    move-result-object v4

    .line 2459670
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2459671
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 2459672
    new-instance p2, LX/4Hi;

    invoke-direct {p2}, LX/4Hi;-><init>()V

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p2, v6}, LX/4Hi;->a(Ljava/lang/String;)LX/4Hi;

    move-result-object v6

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, LX/4Hi;->b(Ljava/lang/String;)LX/4Hi;

    move-result-object v5

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2459673
    :cond_1
    new-instance v5, LX/4Hk;

    invoke-direct {v5}, LX/4Hk;-><init>()V

    .line 2459674
    const-string v6, "page_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459675
    move-object v5, v5

    .line 2459676
    const-string v6, "MOBILE_PAGE_PRESENCE_CALL_TO_ACTION"

    .line 2459677
    const-string p1, "source"

    invoke-virtual {v5, p1, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459678
    move-object v5, v5

    .line 2459679
    const-string v6, "fields_data"

    invoke-virtual {v5, v6, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2459680
    move-object v5, v5

    .line 2459681
    new-instance v6, LX/8FH;

    invoke-direct {v6}, LX/8FH;-><init>()V

    move-object v6, v6

    .line 2459682
    const-string v7, "input"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/8FH;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2459683
    iget-object v6, v2, LX/HNU;->a:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    invoke-static {v5}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v2, v5

    .line 2459684
    new-instance v3, LX/HO9;

    invoke-direct {v3, p0}, LX/HO9;-><init>(LX/HOA;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method
