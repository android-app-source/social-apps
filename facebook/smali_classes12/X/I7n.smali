.class public LX/I7n;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/03V;

.field public final d:Landroid/content/Context;

.field public final e:LX/0tX;

.field public final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/events/model/Event;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2540130
    const-class v0, LX/I7n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/I7n;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/03V;Landroid/content/Context;LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2540131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540132
    iput-object p1, p0, LX/I7n;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2540133
    iput-object p2, p0, LX/I7n;->c:LX/03V;

    .line 2540134
    iput-object p3, p0, LX/I7n;->d:Landroid/content/Context;

    .line 2540135
    iput-object p4, p0, LX/I7n;->e:LX/0tX;

    .line 2540136
    iput-object p5, p0, LX/I7n;->f:LX/1Ck;

    .line 2540137
    return-void
.end method

.method public static a(LX/0QB;)LX/I7n;
    .locals 7

    .prologue
    .line 2540138
    new-instance v1, LX/I7n;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-direct/range {v1 .. v6}, LX/I7n;-><init>(Lcom/facebook/content/SecureContextHelper;LX/03V;Landroid/content/Context;LX/0tX;LX/1Ck;)V

    .line 2540139
    move-object v0, v1

    .line 2540140
    return-object v0
.end method

.method public static d(LX/I7n;)Z
    .locals 1

    .prologue
    .line 2540141
    iget-object v0, p0, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2540142
    iget-object v0, p0, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2540143
    const/4 v0, 0x0

    iput-object v0, p0, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    .line 2540144
    const/4 v0, 0x1

    .line 2540145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2540146
    iget-object v0, p0, LX/I7n;->g:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_1

    .line 2540147
    :cond_0
    :goto_0
    return-void

    .line 2540148
    :cond_1
    iget-object v0, p0, LX/I7n;->i:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2540149
    if-nez v0, :cond_2

    .line 2540150
    iget-object v0, p0, LX/I7n;->g:Lcom/facebook/events/model/Event;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2540151
    iget-object v0, p0, LX/I7n;->f:LX/1Ck;

    const-string v1, "fetch_creation_story"

    new-instance v2, LX/I7l;

    invoke-direct {v2, p0}, LX/I7l;-><init>(LX/I7n;)V

    new-instance v3, LX/I7m;

    invoke-direct {v3, p0}, LX/I7m;-><init>(LX/I7n;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2540152
    const/4 v3, 0x1

    .line 2540153
    iget-object v0, p0, LX/I7n;->d:Landroid/content/Context;

    const-class v1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 2540154
    if-nez v0, :cond_4

    .line 2540155
    iget-object v0, p0, LX/I7n;->c:LX/03V;

    sget-object v1, LX/I7n;->a:Ljava/lang/String;

    const-string v2, "Failed to find a fragment activity"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2540156
    iput-boolean v3, v1, LX/0VK;->d:Z

    .line 2540157
    move-object v1, v1

    .line 2540158
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2540159
    :goto_2
    goto :goto_0

    .line 2540160
    :cond_2
    iget-object v0, p0, LX/I7n;->d:Landroid/content/Context;

    const v1, 0x7f080acc

    iget-object v2, p0, LX/I7n;->g:Lcom/facebook/events/model/Event;

    .line 2540161
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2540162
    iget-object v3, p0, LX/I7n;->i:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    invoke-virtual {v3}, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/I7n;->g:Lcom/facebook/events/model/Event;

    .line 2540163
    iget-object v5, v4, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v4, v5

    .line 2540164
    const-string v5, "event"

    invoke-static/range {v0 .. v5}, LX/8wJ;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2540165
    if-eqz v0, :cond_0

    .line 2540166
    iget-object v1, p0, LX/I7n;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/I7n;->d:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2540167
    :cond_4
    const v1, 0x7f081f77

    invoke-static {v1, v3, v3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    iput-object v1, p0, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    .line 2540168
    iget-object v1, p0, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    new-instance v2, LX/I7j;

    invoke-direct {v2, p0}, LX/I7j;-><init>(LX/I7n;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2540169
    iget-object v1, p0, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    new-instance v2, LX/I7k;

    invoke-direct {v2, p0}, LX/I7k;-><init>(LX/I7n;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2540170
    iget-object v1, p0, LX/I7n;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "promote_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_2
.end method
