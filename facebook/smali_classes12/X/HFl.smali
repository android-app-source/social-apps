.class public final LX/HFl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V
    .locals 0

    .prologue
    .line 2444171
    iput-object p1, p0, LX/HFl;->a:Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x57325976

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2444172
    iget-object v1, p0, LX/HFl;->a:Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    const/4 p0, 0x1

    .line 2444173
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->p:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, p0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2444174
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->q:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, p0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2444175
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444176
    iget-object p0, v2, LX/HFJ;->k:Landroid/net/Uri;

    move-object v2, p0

    .line 2444177
    if-nez v2, :cond_0

    .line 2444178
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->i:LX/HFf;

    sget-object p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->j:Ljava/lang/String;

    const-string p1, "save_"

    invoke-virtual {v2, p0, p1}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444179
    :goto_0
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    iget-object p0, v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->r:Landroid/net/Uri;

    .line 2444180
    iput-object p0, v2, LX/HFJ;->k:Landroid/net/Uri;

    .line 2444181
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->q:Lcom/facebook/fig/button/FigButton;

    const p0, 0x7f082219

    invoke-virtual {v2, p0}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2444182
    const/16 v1, 0x27

    const v2, -0x119396d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2444183
    :cond_0
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->i:LX/HFf;

    sget-object p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->j:Ljava/lang/String;

    const-string p1, "update_"

    invoke-virtual {v2, p0, p1}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
