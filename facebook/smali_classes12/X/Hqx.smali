.class public final LX/Hqx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public final synthetic b:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 0

    .prologue
    .line 2510740
    iput-object p1, p0, LX/Hqx;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iput-object p2, p0, LX/Hqx;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2510741
    iget-object v0, p0, LX/Hqx;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2510742
    new-instance v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;

    invoke-direct {v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;-><init>()V

    .line 2510743
    iget-object v0, p0, LX/Hqx;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->co:LX/93W;

    .line 2510744
    iput-object v0, v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->C:LX/93W;

    .line 2510745
    iget-object v0, p0, LX/Hqx;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->a(LX/2rw;)V

    .line 2510746
    iget-object v0, p0, LX/Hqx;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->x:LX/ARv;

    .line 2510747
    invoke-static {v0}, LX/ARv;->c(LX/ARv;)LX/0P1;

    move-result-object v4

    .line 2510748
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 2510749
    invoke-virtual {v4}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2rw;

    .line 2510750
    invoke-virtual {v4, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ARu;

    .line 2510751
    iget-boolean v7, v3, LX/ARu;->a:Z

    if-nez v7, :cond_1

    iget-object v3, v3, LX/ARu;->b:LX/0Rf;

    invoke-virtual {v3}, LX/0Rf;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2510752
    :cond_1
    invoke-virtual {v5, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2510753
    :cond_2
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    move-object v0, v2

    .line 2510754
    iput-object v0, v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->G:LX/0Rf;

    .line 2510755
    new-instance v0, LX/Hqu;

    invoke-direct {v0, p0, v1}, LX/Hqu;-><init>(LX/Hqx;Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    .line 2510756
    iput-object v0, v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->H:LX/9UI;

    .line 2510757
    new-instance v0, LX/Hqv;

    invoke-direct {v0, p0, v1}, LX/Hqv;-><init>(LX/Hqx;Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    .line 2510758
    iput-object v0, v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->I:LX/9U7;

    .line 2510759
    new-instance v0, LX/Hqw;

    invoke-direct {v0, p0, v1}, LX/Hqw;-><init>(LX/Hqx;Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;)V

    .line 2510760
    iput-object v0, v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->J:LX/ARk;

    .line 2510761
    iget-object v0, p0, LX/Hqx;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    if-eqz v0, :cond_3

    .line 2510762
    invoke-virtual {v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacySelectorFragment;->a()Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    move-result-object v0

    iget-object v2, p0, LX/Hqx;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v2, v2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, v2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 2510763
    :cond_3
    iget-object v0, p0, LX/Hqx;->b:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2510764
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v2

    .line 2510765
    const-string v2, "AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2510766
    return-void
.end method
