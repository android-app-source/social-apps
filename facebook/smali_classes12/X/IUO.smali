.class public final LX/IUO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DNQ;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V
    .locals 0

    .prologue
    .line 2579911
    iput-object p1, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2579912
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->H(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2579913
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2579914
    iget-boolean v1, v0, LX/DNR;->A:Z

    if-eqz v1, :cond_2

    iget-boolean v1, v0, LX/DNR;->B:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2579915
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2579916
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v1

    .line 2579917
    invoke-virtual {v0}, LX/0fz;->v()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2579918
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v1

    .line 2579919
    iget-boolean v1, v0, LX/0fz;->o:Z

    move v0, v1

    .line 2579920
    if-eqz v0, :cond_0

    .line 2579921
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    invoke-virtual {v0}, LX/DNR;->d()V

    .line 2579922
    :cond_0
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->L(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2579923
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->af:LX/IPj;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2579924
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2579925
    if-eqz v0, :cond_1

    .line 2579926
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->af:LX/IPj;

    iget-object v1, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2579927
    iget-object v2, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v2

    .line 2579928
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r()I

    move-result v1

    iget-object v2, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2579929
    iget-object v3, v2, LX/DNR;->c:LX/0fz;

    move-object v2, v3

    .line 2579930
    iget-object v3, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    invoke-static {v1, v2, v3}, LX/IPu;->a(ILX/0fz;LX/1Qq;)I

    move-result v1

    .line 2579931
    iput v1, v0, LX/IPj;->v:I

    .line 2579932
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2579933
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    invoke-virtual {v0, p1}, LX/DNA;->b(Z)V

    .line 2579934
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2579935
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->a()V

    .line 2579936
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    .line 2579937
    iget-object v1, v0, LX/DNA;->a:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2579938
    iget-object v1, v0, LX/DNA;->c:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 2579939
    iget-object v1, v0, LX/DNA;->c:Landroid/view/View;

    const v2, 0x7f0d08bd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/4nk;

    .line 2579940
    if-eqz v1, :cond_0

    .line 2579941
    iget-object v2, v0, LX/DNX;->j:Ljava/lang/String;

    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/4nk;->a(Ljava/lang/String;Z)V

    .line 2579942
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2579943
    iget-object v0, p0, LX/IUO;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->R:LX/DNp;

    invoke-virtual {v0, p1}, LX/DNp;->a(Z)V

    .line 2579944
    return-void
.end method
