.class public LX/JEv;
.super LX/4sY;
.source ""

# interfaces
.implements LX/2NW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4sY",
        "<",
        "LX/JEu;",
        ">;",
        "LX/2NW;"
    }
.end annotation


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, LX/4sY;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object p2, p0, LX/JEv;->b:Landroid/content/Context;

    iget v0, p1, Lcom/google/android/gms/common/data/DataHolder;->h:I

    move v0, v0

    invoke-static {v0}, LX/JF6;->b(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iput-object v0, p0, LX/JEv;->c:Lcom/google/android/gms/common/api/Status;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/common/data/DataHolder;->i:Landroid/os/Bundle;

    move-object v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/common/data/DataHolder;->i:Landroid/os/Bundle;

    move-object v0, v0

    const-string v1, "com.google.android.gms.location.places.PlaceBuffer.ATTRIBUTIONS_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JEv;->d:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/JEv;->d:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, LX/JEv;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    new-instance v0, LX/JFc;

    iget-object v1, p0, LX/4sY;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v0, v1, p1}, LX/JFc;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-object v0
.end method
