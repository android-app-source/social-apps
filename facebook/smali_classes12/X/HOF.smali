.class public final LX/HOF;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;)V
    .locals 0

    .prologue
    .line 2459851
    iput-object p1, p0, LX/HOF;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2459852
    iget-object v0, p0, LX/HOF;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->k:Landroid/widget/ViewSwitcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 2459853
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2459854
    check-cast p1, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;

    .line 2459855
    iget-object v0, p0, LX/HOF;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->k:Landroid/widget/ViewSwitcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 2459856
    iget-object v0, p0, LX/HOF;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    .line 2459857
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->m:LX/HON;

    new-instance v2, LX/HOG;

    invoke-direct {v2, v0}, LX/HOG;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;)V

    .line 2459858
    iput-object v2, v1, LX/HON;->f:LX/HOG;

    .line 2459859
    iget-object v3, v1, LX/HON;->e:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 2459860
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->b()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2459861
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->b()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    .line 2459862
    const/4 v3, 0x0

    move v6, v3

    :goto_0
    if-ge v6, v8, :cond_1

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2459863
    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq v4, v5, :cond_1

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq v4, v5, :cond_1

    .line 2459864
    iget-object v4, v1, LX/HON;->d:Landroid/content/Context;

    const v5, 0x7f083690

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2459865
    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gB_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2459866
    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gB_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->d()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 2459867
    :goto_1
    iget-object v4, v1, LX/HON;->c:LX/H9D;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v9

    invoke-virtual {v4, v9}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v4

    check-cast v4, LX/H8Z;

    .line 2459868
    invoke-interface {v4}, LX/H8Z;->a()LX/HA7;

    move-result-object v9

    .line 2459869
    iget-object v4, v1, LX/HON;->e:Ljava/util/Map;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2459870
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2459871
    iget-object p0, v1, LX/HON;->e:Ljava/util/Map;

    invoke-interface {p0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2459872
    :goto_2
    new-instance p0, LX/HOK;

    sget-object v0, LX/HOL;->ACTION_ITEM:LX/HOL;

    invoke-direct {p0, v0, v5, v3, v9}, LX/HOK;-><init>(LX/HOL;Ljava/lang/String;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;LX/HA7;)V

    invoke-interface {v4, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2459873
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_0

    .line 2459874
    :cond_0
    iget-object v4, v1, LX/HON;->e:Ljava/util/Map;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    goto :goto_2

    .line 2459875
    :cond_1
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2459876
    return-void

    :cond_2
    move-object v5, v4

    goto :goto_1
.end method
