.class public final LX/JPr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JPv;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public final synthetic c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;LX/JPv;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V
    .locals 0

    .prologue
    .line 2690541
    iput-object p1, p0, LX/JPr;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iput-object p2, p0, LX/JPr;->a:LX/JPv;

    iput-object p3, p0, LX/JPr;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x2

    const v1, -0x1484bfe4

    invoke-static {v0, v6, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2690542
    iget-object v0, p0, LX/JPr;->a:LX/JPv;

    iget-object v0, v0, LX/JPv;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 2690543
    :goto_0
    iget-object v0, p0, LX/JPr;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v1, p0, LX/JPr;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v2, p0, LX/JPr;->a:LX/JPv;

    iget-object v2, v2, LX/JPv;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-static {v1, v2}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    iget-object v2, p0, LX/JPr;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/JPr;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, LX/JPr;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2690544
    :goto_1
    iget-object v8, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->d:LX/0Zb;

    .line 2690545
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v10, "page_admin_panel_profile_tap"

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v10, "tracking"

    invoke-virtual {v9, v10, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "admin_id"

    invoke-virtual {v9, v10, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "event_name"

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v10, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "page_id"

    invoke-virtual {v9, v10, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "is_ego_show_single_page_item"

    invoke-virtual {v9, v10, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "source"

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->PROFILE_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v10, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "page_admin_panel"

    .line 2690546
    iput-object v10, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2690547
    move-object v9, v9

    .line 2690548
    move-object v9, v9

    .line 2690549
    invoke-interface {v8, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2690550
    iget-object v0, p0, LX/JPr;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->b:LX/1nA;

    iget-object v1, p0, LX/JPr;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-static {v1}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2690551
    const v0, -0x1e63c7c0

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    .line 2690552
    :cond_0
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 2690553
    :cond_1
    const-wide/16 v4, 0x0

    goto :goto_1
.end method
