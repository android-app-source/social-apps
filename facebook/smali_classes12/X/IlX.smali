.class public LX/IlX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Lc",
        "<",
        "Ljava/lang/Object;",
        "Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final c:LX/0Zb;

.field private final d:LX/0SG;

.field private final e:Ljava/util/concurrent/Executor;

.field private f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2607842
    const-class v0, LX/IlX;

    sput-object v0, LX/IlX;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;LX/0Zb;LX/0SG;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607844
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IlX;->f:Z

    .line 2607845
    iput-object p1, p0, LX/IlX;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2607846
    iput-object p2, p0, LX/IlX;->c:LX/0Zb;

    .line 2607847
    iput-object p3, p0, LX/IlX;->d:LX/0SG;

    .line 2607848
    iput-object p4, p0, LX/IlX;->e:Ljava/util/concurrent/Executor;

    .line 2607849
    return-void
.end method
