.class public LX/Hsb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hsa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$SetsInlineSproutsState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/Hsa;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final c:LX/3kp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2514676
    const-class v0, LX/Hsb;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Hsb;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/3kp;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/3kp;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2514678
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hsb;->b:Ljava/lang/ref/WeakReference;

    .line 2514679
    iput-object p2, p0, LX/Hsb;->c:LX/3kp;

    .line 2514680
    iget-object v0, p0, LX/Hsb;->c:LX/3kp;

    sget-object v1, LX/0dp;->r:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2514681
    iget-object v0, p0, LX/Hsb;->c:LX/3kp;

    const/4 v1, 0x3

    .line 2514682
    iput v1, v0, LX/3kp;->b:I

    .line 2514683
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 2514684
    iget-object v0, p0, LX/Hsb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514685
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v1

    .line 2514686
    invoke-virtual {v1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isFacecastNuxShowing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2514687
    iget-object v2, p0, LX/Hsb;->c:LX/3kp;

    invoke-virtual {v2}, LX/3kp;->a()V

    .line 2514688
    invoke-static {v1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->setIsFacecastNuxShowing(Z)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->a()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v1

    .line 2514689
    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v2, LX/Hsb;->a:LX/0jK;

    invoke-virtual {v0, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2514690
    :cond_0
    const v0, 0x7f03091e

    return v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2514691
    iget-object v0, p0, LX/Hsb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514692
    iget-object v1, p0, LX/Hsb;->c:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isFacecastNuxShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 2514693
    const/4 v0, 0x0

    return-object v0
.end method
