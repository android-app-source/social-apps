.class public final LX/J6z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;)V
    .locals 0

    .prologue
    .line 2650434
    iput-object p1, p0, LX/J6z;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x4c2207e8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650435
    iget-object v1, p0, LX/J6z;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    iget-object v1, v1, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->p:LX/Hrh;

    .line 2650436
    iget-object v3, v1, LX/Hrh;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    invoke-virtual {v3}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2650437
    iget-object v3, v1, LX/Hrh;->d:LX/Hrk;

    iget-object v3, v3, LX/Hrk;->i:LX/8Ps;

    iget-object v4, v1, LX/Hrh;->d:LX/Hrk;

    iget-object v4, v4, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object v4, v4, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const-string v5, "traditional_composer"

    .line 2650438
    iget-object v6, v3, LX/8Ps;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v7, LX/5nc;->DISMISSED:LX/5nc;

    invoke-static {v3}, LX/8Ps;->a(LX/8Ps;)Ljava/lang/Long;

    move-result-object v8

    invoke-static {v4}, LX/8Ps;->a(LX/1oU;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v4}, LX/8Ps;->a(LX/1oU;)Ljava/lang/String;

    move-result-object v10

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/5nc;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2650439
    iget-object v3, v1, LX/Hrh;->c:LX/HqM;

    invoke-virtual {v3}, LX/HqM;->a()V

    .line 2650440
    const v1, -0x41f94e18

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
