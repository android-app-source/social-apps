.class public final LX/IYp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IYq;


# direct methods
.method public constructor <init>(LX/IYq;)V
    .locals 0

    .prologue
    .line 2588175
    iput-object p1, p0, LX/IYp;->a:LX/IYq;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2588171
    iget-object v0, p0, LX/IYp;->a:LX/IYq;

    iget-object v0, v0, LX/IYq;->g:LX/IYm;

    .line 2588172
    iget-object v1, v0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/looknow/LookNowPermalinkFragment;->h:LX/IYi;

    sget-object v2, LX/IYh;->LOOK_NOW_PERMALINK_LOAD_FAILURE:LX/IYh;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, LX/IYi;->a(LX/IYh;Ljava/lang/String;)V

    .line 2588173
    iget-object v1, v0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/looknow/LookNowPermalinkFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v2, v0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    const p0, 0x7f0838aa

    invoke-virtual {v2, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2588174
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2588166
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 2588167
    if-eqz p1, :cond_0

    .line 2588168
    iget-object v0, p0, LX/IYp;->a:LX/IYq;

    iget-object v0, v0, LX/IYq;->d:LX/0fz;

    invoke-virtual {v0, p1}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 2588169
    :cond_0
    iget-object v0, p0, LX/IYp;->a:LX/IYq;

    iget-object v0, v0, LX/IYq;->g:LX/IYm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/IYm;->a(Z)V

    .line 2588170
    return-void
.end method
