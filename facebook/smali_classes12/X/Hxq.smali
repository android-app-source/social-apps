.class public final enum LX/Hxq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hxq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hxq;

.field public static final enum SECONDARY_NAVIGATION:LX/Hxq;

.field public static final enum STANDALONE:LX/Hxq;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2522878
    new-instance v0, LX/Hxq;

    const-string v1, "STANDALONE"

    invoke-direct {v0, v1, v2}, LX/Hxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hxq;->STANDALONE:LX/Hxq;

    .line 2522879
    new-instance v0, LX/Hxq;

    const-string v1, "SECONDARY_NAVIGATION"

    invoke-direct {v0, v1, v3}, LX/Hxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hxq;->SECONDARY_NAVIGATION:LX/Hxq;

    .line 2522880
    const/4 v0, 0x2

    new-array v0, v0, [LX/Hxq;

    sget-object v1, LX/Hxq;->STANDALONE:LX/Hxq;

    aput-object v1, v0, v2

    sget-object v1, LX/Hxq;->SECONDARY_NAVIGATION:LX/Hxq;

    aput-object v1, v0, v3

    sput-object v0, LX/Hxq;->$VALUES:[LX/Hxq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2522882
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hxq;
    .locals 1

    .prologue
    .line 2522883
    const-class v0, LX/Hxq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hxq;

    return-object v0
.end method

.method public static values()[LX/Hxq;
    .locals 1

    .prologue
    .line 2522881
    sget-object v0, LX/Hxq;->$VALUES:[LX/Hxq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hxq;

    return-object v0
.end method
