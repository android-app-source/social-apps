.class public final LX/IIt;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562230
    iput-object p1, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method


# virtual methods
.method public final f()V
    .locals 3

    .prologue
    .line 2562231
    iget-object v0, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_INIT:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->b(LX/IG6;)V

    .line 2562232
    iget-object v0, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->I(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562233
    iget-object v0, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562234
    iget-object v0, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562235
    iget-object v0, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->M(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562236
    iget-object v0, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    sget-object v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->c:Lcom/facebook/location/FbLocationOperationParams;

    sget-object v2, LX/IG6;->DASHBOARD_INIT_LOCATION:LX/IG6;

    invoke-static {v0, p0, v1, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;Lcom/facebook/location/FbLocationOperationParams;LX/IG6;)V

    .line 2562237
    return-void
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 2562238
    iget-object v0, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const v2, 0x7f08384d

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->b(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/String;)V

    .line 2562239
    return-void
.end method

.method public final q()V
    .locals 3

    .prologue
    .line 2562240
    iget-object v0, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIt;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aA:LX/IIm;

    .line 2562241
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562242
    if-eqz v2, :cond_0

    .line 2562243
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562244
    :goto_0
    return-void

    .line 2562245
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562246
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method
