.class public final enum LX/Isg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Isg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Isg;

.field public static final enum BOTS:LX/Isg;

.field public static final enum DIRECT_M:LX/Isg;

.field public static final enum RECENT_SEARCHES:LX/Isg;

.field public static final enum TOP_FRIENDS:LX/Isg;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2621163
    new-instance v0, LX/Isg;

    const-string v1, "TOP_FRIENDS"

    invoke-direct {v0, v1, v2}, LX/Isg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Isg;->TOP_FRIENDS:LX/Isg;

    .line 2621164
    new-instance v0, LX/Isg;

    const-string v1, "BOTS"

    invoke-direct {v0, v1, v3}, LX/Isg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Isg;->BOTS:LX/Isg;

    .line 2621165
    new-instance v0, LX/Isg;

    const-string v1, "RECENT_SEARCHES"

    invoke-direct {v0, v1, v4}, LX/Isg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Isg;->RECENT_SEARCHES:LX/Isg;

    .line 2621166
    new-instance v0, LX/Isg;

    const-string v1, "DIRECT_M"

    invoke-direct {v0, v1, v5}, LX/Isg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Isg;->DIRECT_M:LX/Isg;

    .line 2621167
    const/4 v0, 0x4

    new-array v0, v0, [LX/Isg;

    sget-object v1, LX/Isg;->TOP_FRIENDS:LX/Isg;

    aput-object v1, v0, v2

    sget-object v1, LX/Isg;->BOTS:LX/Isg;

    aput-object v1, v0, v3

    sget-object v1, LX/Isg;->RECENT_SEARCHES:LX/Isg;

    aput-object v1, v0, v4

    sget-object v1, LX/Isg;->DIRECT_M:LX/Isg;

    aput-object v1, v0, v5

    sput-object v0, LX/Isg;->$VALUES:[LX/Isg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2621168
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Isg;
    .locals 1

    .prologue
    .line 2621169
    const-class v0, LX/Isg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Isg;

    return-object v0
.end method

.method public static values()[LX/Isg;
    .locals 1

    .prologue
    .line 2621170
    sget-object v0, LX/Isg;->$VALUES:[LX/Isg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Isg;

    return-object v0
.end method
