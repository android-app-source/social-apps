.class public final LX/HoO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;)V
    .locals 0

    .prologue
    .line 2505644
    iput-object p1, p0, LX/HoO;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2505645
    iget-object v0, p0, LX/HoO;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchElectionHubNTTab"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2505646
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2505647
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2505648
    if-eqz p1, :cond_0

    .line 2505649
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505650
    if-nez v0, :cond_1

    .line 2505651
    :cond_0
    :goto_0
    return-void

    .line 2505652
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505653
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2505654
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505655
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2505656
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505657
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/5eL;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;)Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    iget-object v1, p0, LX/HoO;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    iget-object v1, v1, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->f:LX/CNc;

    invoke-static {v0, v1}, LX/CNu;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/CNc;)LX/0Px;

    move-result-object v0

    .line 2505658
    iget-object v1, p0, LX/HoO;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    iget-object v1, v1, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->f:LX/CNc;

    iget-object v1, v1, LX/CNc;->b:LX/CNS;

    invoke-virtual {v1, v0}, LX/CNS;->a(LX/0Px;)V

    .line 2505659
    :cond_2
    iget-object v0, p0, LX/HoO;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->g:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2505660
    iget-object v0, p0, LX/HoO;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->g:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0
.end method
