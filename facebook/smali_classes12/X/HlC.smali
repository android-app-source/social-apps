.class public LX/HlC;
.super Landroid/view/TextureView;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements LX/HlB;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field public static final i:Ljava/lang/String;


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/net/Uri;

.field private c:LX/Hl9;

.field public d:Landroid/view/Surface;

.field public e:Landroid/media/MediaPlayer;

.field private f:Z

.field private g:Z

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, LX/HlC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HlC;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, LX/HlC;->a:Landroid/view/View;

    iput-object p2, p0, LX/HlC;->b:Landroid/net/Uri;

    invoke-virtual {p0, p0}, LX/HlC;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method

.method public getCurrentPosition()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, LX/HlC;->e:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/HlC;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    :cond_0
    return v0
.end method

.method public final onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 0

    return-void
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, LX/HlC;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    iput-boolean v2, p0, LX/HlC;->g:Z

    :cond_0
    iget v0, p0, LX/HlC;->h:I

    if-lez v0, :cond_2

    iget v0, p0, LX/HlC;->h:I

    iget-object v1, p0, LX/HlC;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    if-lt v0, v1, :cond_1

    iput v2, p0, LX/HlC;->h:I

    :cond_1
    iget-object v0, p0, LX/HlC;->e:Landroid/media/MediaPlayer;

    iget v1, p0, LX/HlC;->h:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    iput v2, p0, LX/HlC;->h:I

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HlC;->f:Z

    return-void
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/HlC;->d:Landroid/view/Surface;

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    :try_start_0
    invoke-virtual {p0}, LX/HlC;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object p2, p0, LX/HlC;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v0, p0, LX/HlC;->d:Landroid/view/Surface;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    new-instance v0, LX/HlA;

    iget-object p2, p0, LX/HlC;->a:Landroid/view/View;

    invoke-direct {v0, p2}, LX/HlA;-><init>(Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setLooping(Z)V

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->prepareAsync()V

    iput-object p1, p0, LX/HlC;->e:Landroid/media/MediaPlayer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->release()V

    sget-object p1, LX/HlC;->i:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "Cannot prepare media player with SurfaceTexture: "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method

.method public setFrameVideoViewListener(LX/Hl9;)V
    .locals 0

    iput-object p1, p0, LX/HlC;->c:LX/Hl9;

    return-void
.end method

.method public final start()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, LX/HlC;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HlC;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    :goto_0
    invoke-virtual {p0}, LX/HlC;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/HlC;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v1}, LX/HlC;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HlC;->g:Z

    goto :goto_0
.end method
