.class public final enum LX/Hjc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hjc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/Hjc;

.field public static final enum b:LX/Hjc;

.field private static final synthetic c:[LX/Hjc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LX/Hjc;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/Hjc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hjc;->a:LX/Hjc;

    new-instance v0, LX/Hjc;

    const-string v1, "AN"

    invoke-direct {v0, v1, v3}, LX/Hjc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hjc;->b:LX/Hjc;

    const/4 v0, 0x2

    new-array v0, v0, [LX/Hjc;

    sget-object v1, LX/Hjc;->a:LX/Hjc;

    aput-object v1, v0, v2

    sget-object v1, LX/Hjc;->b:LX/Hjc;

    aput-object v1, v0, v3

    sput-object v0, LX/Hjc;->c:[LX/Hjc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/Hjc;
    .locals 2

    if-nez p0, :cond_0

    sget-object v0, LX/Hjc;->a:LX/Hjc;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    const-class v0, LX/Hjc;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hjc;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v0, LX/Hjc;->a:LX/Hjc;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hjc;
    .locals 1

    const-class v0, LX/Hjc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hjc;

    return-object v0
.end method

.method public static values()[LX/Hjc;
    .locals 1

    sget-object v0, LX/Hjc;->c:[LX/Hjc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hjc;

    return-object v0
.end method
