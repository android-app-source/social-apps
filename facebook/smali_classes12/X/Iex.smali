.class public final LX/Iex;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel;",
        ">;",
        "Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Iey;


# direct methods
.method public constructor <init>(LX/Iey;)V
    .locals 0

    .prologue
    .line 2599219
    iput-object p1, p0, LX/Iex;->a:LX/Iey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2599220
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2599221
    iget-object v0, p0, LX/Iex;->a:LX/Iey;

    .line 2599222
    if-eqz p1, :cond_0

    .line 2599223
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2599224
    if-nez v1, :cond_1

    .line 2599225
    :cond_0
    const/4 v1, 0x0

    .line 2599226
    :goto_0
    move-object v0, v1

    .line 2599227
    return-object v0

    .line 2599228
    :cond_1
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2599229
    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel;

    .line 2599230
    iget-object p0, v0, LX/Iey;->c:LX/Iew;

    invoke-virtual {p0, v1}, LX/Iew;->a(Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel;)Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    move-result-object v1

    goto :goto_0
.end method
