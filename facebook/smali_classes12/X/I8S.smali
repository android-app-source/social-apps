.class public final enum LX/I8S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I8S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I8S;

.field public static final enum EVENT_PERMALINK_GAP_VIEW:LX/I8S;

.field public static final enum PENDING_POSTS_STATUS:LX/I8S;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2541524
    new-instance v0, LX/I8S;

    const-string v1, "EVENT_PERMALINK_GAP_VIEW"

    invoke-direct {v0, v1, v2}, LX/I8S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8S;->EVENT_PERMALINK_GAP_VIEW:LX/I8S;

    .line 2541525
    new-instance v0, LX/I8S;

    const-string v1, "PENDING_POSTS_STATUS"

    invoke-direct {v0, v1, v3}, LX/I8S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8S;->PENDING_POSTS_STATUS:LX/I8S;

    .line 2541526
    const/4 v0, 0x2

    new-array v0, v0, [LX/I8S;

    sget-object v1, LX/I8S;->EVENT_PERMALINK_GAP_VIEW:LX/I8S;

    aput-object v1, v0, v2

    sget-object v1, LX/I8S;->PENDING_POSTS_STATUS:LX/I8S;

    aput-object v1, v0, v3

    sput-object v0, LX/I8S;->$VALUES:[LX/I8S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2541527
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I8S;
    .locals 1

    .prologue
    .line 2541528
    const-class v0, LX/I8S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I8S;

    return-object v0
.end method

.method public static values()[LX/I8S;
    .locals 1

    .prologue
    .line 2541529
    sget-object v0, LX/I8S;->$VALUES:[LX/I8S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I8S;

    return-object v0
.end method
