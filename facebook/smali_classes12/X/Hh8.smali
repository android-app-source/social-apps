.class public LX/Hh8;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Hh8;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2495076
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2495077
    const-string v0, "carrier_manager?ref={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ref"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->CARRIER_MANAGER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2495078
    return-void
.end method

.method public static a(LX/0QB;)LX/Hh8;
    .locals 3

    .prologue
    .line 2495079
    sget-object v0, LX/Hh8;->a:LX/Hh8;

    if-nez v0, :cond_1

    .line 2495080
    const-class v1, LX/Hh8;

    monitor-enter v1

    .line 2495081
    :try_start_0
    sget-object v0, LX/Hh8;->a:LX/Hh8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2495082
    if-eqz v2, :cond_0

    .line 2495083
    :try_start_1
    new-instance v0, LX/Hh8;

    invoke-direct {v0}, LX/Hh8;-><init>()V

    .line 2495084
    move-object v0, v0

    .line 2495085
    sput-object v0, LX/Hh8;->a:LX/Hh8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2495086
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2495087
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2495088
    :cond_1
    sget-object v0, LX/Hh8;->a:LX/Hh8;

    return-object v0

    .line 2495089
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2495090
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
