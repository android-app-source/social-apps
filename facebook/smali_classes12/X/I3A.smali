.class public final LX/I3A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hxd;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;)V
    .locals 0

    .prologue
    .line 2531085
    iput-object p1, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;ILjava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
            ">;I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2531086
    iget-object v0, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_0

    .line 2531087
    :goto_0
    return-void

    .line 2531088
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2531089
    iget-object v0, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->b:LX/1nQ;

    iget-object v1, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    invoke-virtual {v1}, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531090
    iget-object v3, v2, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v2, v3

    .line 2531091
    invoke-virtual {v2}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v2

    iget-object v3, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531092
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v3, v4

    .line 2531093
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1nQ;->a(Ljava/lang/String;IIZ)V

    .line 2531094
    :cond_1
    new-instance v0, LX/I39;

    invoke-direct {v0, p0}, LX/I39;-><init>(LX/I3A;)V

    invoke-static {p1, v0}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    .line 2531095
    iget-object v1, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->i:LX/I3J;

    invoke-virtual {v1, v0}, LX/I3J;->a(Ljava/util/List;)V

    .line 2531096
    iget-object v0, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    .line 2531097
    iput-object p3, v0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->g:Ljava/lang/String;

    .line 2531098
    iget-object v0, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    .line 2531099
    iput-boolean p4, v0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->h:Z

    .line 2531100
    iget-object v0, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->i:LX/I3J;

    iget-object v1, p0, LX/I3A;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-boolean v1, v1, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->h:Z

    invoke-virtual {v0, v1}, LX/I3J;->a(Z)V

    goto :goto_0
.end method
