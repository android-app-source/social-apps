.class public LX/Iv9;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/widget/ProgressBar;

.field private c:Landroid/content/Context;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/widget/ProgressBar;Landroid/content/Context;Lcom/facebook/resources/ui/FbTextView;)V
    .locals 0

    .prologue
    .line 2627992
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 2627993
    iput-object p1, p0, LX/Iv9;->a:Ljava/lang/String;

    .line 2627994
    iput-object p2, p0, LX/Iv9;->b:Landroid/widget/ProgressBar;

    .line 2627995
    iput-object p3, p0, LX/Iv9;->c:Landroid/content/Context;

    .line 2627996
    iput-object p4, p0, LX/Iv9;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2627997
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2627998
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Iv9;->e:Z

    .line 2627999
    iget-object v0, p0, LX/Iv9;->b:Landroid/widget/ProgressBar;

    iget-object v1, p0, LX/Iv9;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, LX/Iut;->a(Landroid/view/View;Landroid/view/View;I)V

    .line 2628000
    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2628001
    iget-boolean v0, p0, LX/Iv9;->e:Z

    if-nez v0, :cond_0

    .line 2628002
    iget-object v0, p0, LX/Iv9;->b:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2628003
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 2628004
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2628005
    :cond_0
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2628006
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 2628007
    invoke-direct {p0}, LX/Iv9;->a()V

    .line 2628008
    :cond_0
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2628009
    iget-object v0, p0, LX/Iv9;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2628010
    const/4 v0, 0x0

    .line 2628011
    :goto_0
    return v0

    .line 2628012
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2628013
    iget-object v1, p0, LX/Iv9;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2628014
    const/4 v0, 0x1

    goto :goto_0
.end method
