.class public final enum LX/J4a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J4a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J4a;

.field public static final enum FETCH_PHOTOS:LX/J4a;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2644226
    new-instance v0, LX/J4a;

    const-string v1, "FETCH_PHOTOS"

    invoke-direct {v0, v1, v2}, LX/J4a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J4a;->FETCH_PHOTOS:LX/J4a;

    .line 2644227
    const/4 v0, 0x1

    new-array v0, v0, [LX/J4a;

    sget-object v1, LX/J4a;->FETCH_PHOTOS:LX/J4a;

    aput-object v1, v0, v2

    sput-object v0, LX/J4a;->$VALUES:[LX/J4a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2644228
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J4a;
    .locals 1

    .prologue
    .line 2644229
    const-class v0, LX/J4a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J4a;

    return-object v0
.end method

.method public static values()[LX/J4a;
    .locals 1

    .prologue
    .line 2644230
    sget-object v0, LX/J4a;->$VALUES:[LX/J4a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J4a;

    return-object v0
.end method
