.class public LX/J9q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J9l;


# instance fields
.field private final a:LX/JDA;

.field private final b:LX/J94;

.field private final c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field private final d:Landroid/view/LayoutInflater;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLInterfaces$AppCollectionItem$;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;


# direct methods
.method public constructor <init>(LX/JDA;LX/J94;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;)V
    .locals 2
    .param p3    # Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/view/LayoutInflater;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2653617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2653618
    iput-object p1, p0, LX/J9q;->a:LX/JDA;

    .line 2653619
    iput-object p2, p0, LX/J9q;->b:LX/J94;

    .line 2653620
    iput-object p3, p0, LX/J9q;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 2653621
    iput-object p4, p0, LX/J9q;->d:Landroid/view/LayoutInflater;

    .line 2653622
    iget-object v0, p0, LX/J9q;->a:LX/JDA;

    iget-object v1, p0, LX/J9q;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0, v1}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)I

    move-result v0

    iput v0, p0, LX/J9q;->f:I

    .line 2653623
    const/4 v0, 0x0

    iput-object v0, p0, LX/J9q;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653624
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2653616
    const/16 v0, 0x24

    iget v1, p0, LX/J9q;->f:I

    rem-int/2addr v0, v1

    rsub-int/lit8 v0, v0, 0x24

    return v0
.end method

.method public final a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/0us;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateCollection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2653595
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653596
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653597
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653598
    iput-object p2, p0, LX/J9q;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653599
    const/4 v1, 0x0

    .line 2653600
    iget-object v0, p0, LX/J9q;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 2653601
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/J9q;->e:Ljava/util/List;

    .line 2653602
    :cond_0
    :goto_0
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2653603
    iget-object v0, p0, LX/J9q;->e:Ljava/util/List;

    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v2

    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    iget v4, p0, LX/J9q;->f:I

    add-int/2addr v4, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2653604
    iget v0, p0, LX/J9q;->f:I

    add-int/2addr v1, v0

    goto :goto_0

    .line 2653605
    :cond_1
    iget-object v0, p0, LX/J9q;->e:Ljava/util/List;

    iget-object v2, p0, LX/J9q;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2653606
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, LX/J9q;->f:I

    if-ge v2, v3, :cond_0

    .line 2653607
    iget-object v2, p0, LX/J9q;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2653608
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2653609
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2653610
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2653611
    :goto_1
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    iget v3, p0, LX/J9q;->f:I

    if-ge v0, v3, :cond_2

    .line 2653612
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2653613
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2653614
    :cond_2
    iget-object v0, p0, LX/J9q;->e:Ljava/util/List;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2653615
    :cond_3
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->b()LX/0us;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;LX/J9V;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2653586
    iget-object v0, p0, LX/J9q;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->GRID:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2653587
    iget-object v0, p0, LX/J9q;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f0302a5

    invoke-static {p1, v0, v1}, LX/JDa;->b(Landroid/content/Context;Landroid/view/LayoutInflater;I)LX/JDa;

    move-result-object v0

    .line 2653588
    :goto_0
    sget-object v1, LX/J9p;->a:[I

    invoke-virtual {p2}, LX/J9V;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2653589
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown type in TableCollectionSubAdapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2653590
    :cond_0
    iget-object v0, p0, LX/J9q;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2653591
    iget-object v0, p0, LX/J9q;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f0302ac

    invoke-static {p1, v0, v1}, LX/JDa;->b(Landroid/content/Context;Landroid/view/LayoutInflater;I)LX/JDa;

    move-result-object v0

    goto :goto_0

    .line 2653592
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unexpected collection style"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2653593
    :pswitch_0
    iget-object v2, p0, LX/J9q;->d:Landroid/view/LayoutInflater;

    invoke-static {v0, v2}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 2653594
    :goto_1
    return-object v0

    :pswitch_1
    iget-object v2, p0, LX/J9q;->d:Landroid/view/LayoutInflater;

    invoke-static {v0, v2}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2653569
    iget-object v0, p0, LX/J9q;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2653570
    iget-object v0, p0, LX/J9q;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2653571
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;LX/9lP;)V
    .locals 2

    .prologue
    .line 2653582
    invoke-static {p2}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/JDa;

    .line 2653583
    check-cast p1, Ljava/util/List;

    iget-object v1, p0, LX/J9q;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/J9q;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    :goto_0
    invoke-virtual {v0, p1, p3, v1}, LX/JDa;->a(Ljava/util/List;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V

    .line 2653584
    return-void

    .line 2653585
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2653581
    iget-object v0, p0, LX/J9q;->a:LX/JDA;

    iget-object v1, p0, LX/J9q;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0, v1}, LX/JDA;->b(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)I

    move-result v0

    return v0
.end method

.method public final b(I)LX/J9V;
    .locals 1

    .prologue
    .line 2653578
    invoke-virtual {p0}, LX/J9q;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2653579
    sget-object v0, LX/J9V;->SUB_ADAPTER_ITEM_BOTTOM:LX/J9V;

    .line 2653580
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/J9V;->SUB_ADAPTER_ITEM_MIDDLE:LX/J9V;

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2653576
    const/4 v0, 0x0

    iput-object v0, p0, LX/J9q;->e:Ljava/util/List;

    .line 2653577
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2653573
    iget-object v0, p0, LX/J9q;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2653574
    iget-object v0, p0, LX/J9q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2653575
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
    .locals 1

    .prologue
    .line 2653572
    iget-object v0, p0, LX/J9q;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    return-object v0
.end method
