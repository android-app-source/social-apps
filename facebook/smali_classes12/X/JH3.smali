.class public final LX/JH3;
.super LX/JGu;
.source ""


# instance fields
.field public e:Landroid/text/Layout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(FFFFIZLandroid/text/Layout;)V
    .locals 0
    .param p7    # Landroid/text/Layout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2672221
    invoke-direct/range {p0 .. p6}, LX/JGu;-><init>(FFFFIZ)V

    .line 2672222
    iput-object p7, p0, LX/JH3;->e:Landroid/text/Layout;

    .line 2672223
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Layout;)V
    .locals 0

    .prologue
    .line 2672252
    iput-object p1, p0, LX/JH3;->e:Landroid/text/Layout;

    .line 2672253
    return-void
.end method

.method public final a(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2672241
    invoke-super {p0, p1}, LX/JGu;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2672242
    :goto_0
    return v0

    .line 2672243
    :cond_0
    iget-object v0, p0, LX/JH3;->e:Landroid/text/Layout;

    if-eqz v0, :cond_2

    .line 2672244
    iget-object v0, p0, LX/JH3;->e:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2672245
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v4, Lcom/facebook/catalyst/shadow/flat/RCTRawText;

    invoke-interface {v0, v2, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/catalyst/shadow/flat/RCTRawText;

    .line 2672246
    array-length v4, v0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v5, v0, v3

    .line 2672247
    iget p0, v5, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v5, p0

    .line 2672248
    if-ne v5, p1, :cond_1

    move v0, v1

    .line 2672249
    goto :goto_0

    .line 2672250
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    .line 2672251
    goto :goto_0
.end method

.method public final b(FF)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2672224
    iget-object v0, p0, LX/JH3;->e:Landroid/text/Layout;

    if-eqz v0, :cond_0

    .line 2672225
    iget-object v0, p0, LX/JH3;->e:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2672226
    instance-of v1, v0, Landroid/text/Spanned;

    if-eqz v1, :cond_0

    .line 2672227
    iget v1, p0, LX/JGu;->f:F

    move v1, v1

    .line 2672228
    sub-float v1, p2, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 2672229
    iget-object v2, p0, LX/JH3;->e:Landroid/text/Layout;

    invoke-virtual {v2, v4}, Landroid/text/Layout;->getLineTop(I)I

    move-result v2

    if-lt v1, v2, :cond_0

    iget-object v2, p0, LX/JH3;->e:Landroid/text/Layout;

    iget-object v3, p0, LX/JH3;->e:Landroid/text/Layout;

    invoke-virtual {v3}, Landroid/text/Layout;->getLineCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2672230
    iget v2, p0, LX/JGu;->e:F

    move v2, v2

    .line 2672231
    sub-float v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    .line 2672232
    iget-object v3, p0, LX/JH3;->e:Landroid/text/Layout;

    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v1

    .line 2672233
    iget-object v3, p0, LX/JH3;->e:Landroid/text/Layout;

    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v3

    cmpg-float v3, v3, v2

    if-gtz v3, :cond_0

    iget-object v3, p0, LX/JH3;->e:Landroid/text/Layout;

    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineRight(I)F

    move-result v3

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_0

    .line 2672234
    iget-object v3, p0, LX/JH3;->e:Landroid/text/Layout;

    invoke-virtual {v3, v1, v2}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v1

    .line 2672235
    check-cast v0, Landroid/text/Spanned;

    .line 2672236
    const-class v2, Lcom/facebook/catalyst/shadow/flat/RCTRawText;

    invoke-interface {v0, v1, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/catalyst/shadow/flat/RCTRawText;

    .line 2672237
    array-length v1, v0

    if-eqz v1, :cond_0

    .line 2672238
    aget-object v0, v0, v4

    .line 2672239
    iget v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v1

    .line 2672240
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/JGu;->b(FF)I

    move-result v0

    goto :goto_0
.end method
