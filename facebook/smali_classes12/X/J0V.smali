.class public LX/J0V;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
        ">;>;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J0V;


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637194
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2637195
    return-void
.end method

.method public static a(LX/0QB;)LX/J0V;
    .locals 4

    .prologue
    .line 2637196
    sget-object v0, LX/J0V;->b:LX/J0V;

    if-nez v0, :cond_1

    .line 2637197
    const-class v1, LX/J0V;

    monitor-enter v1

    .line 2637198
    :try_start_0
    sget-object v0, LX/J0V;->b:LX/J0V;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637199
    if-eqz v2, :cond_0

    .line 2637200
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2637201
    new-instance p0, LX/J0V;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-direct {p0, v3}, LX/J0V;-><init>(LX/0sO;)V

    .line 2637202
    move-object v0, p0

    .line 2637203
    sput-object v0, LX/J0V;->b:LX/J0V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637204
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637205
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637206
    :cond_1
    sget-object v0, LX/J0V;->b:LX/J0V;

    return-object v0

    .line 2637207
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2637209
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllThemesQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllThemesQueryModel;

    .line 2637210
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllThemesQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2637211
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 2637212
    new-instance v0, LX/DtU;

    invoke-direct {v0}, LX/DtU;-><init>()V

    move-object v0, v0

    .line 2637213
    return-object v0
.end method
