.class public LX/IRw;
.super LX/DNC;
.source ""


# instance fields
.field private final a:LX/IRJ;

.field private final b:LX/DOp;

.field private final c:LX/IVs;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field private final f:LX/DNR;

.field private final g:LX/DNJ;

.field private final h:Ljava/lang/String;

.field private final i:LX/1PT;

.field private final j:LX/1Cv;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/1Qq;


# direct methods
.method public constructor <init>(LX/IRJ;LX/DOp;LX/IVs;LX/0Ot;Landroid/content/Context;LX/DNR;LX/DNJ;Ljava/lang/String;LX/1Cv;LX/1PT;)V
    .locals 0
    .param p6    # LX/DNR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/DNJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/1Cv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IRJ;",
            "LX/DOp;",
            "LX/IVs;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;",
            ">;",
            "Landroid/content/Context;",
            "LX/DNR;",
            "LX/DNJ;",
            "Ljava/lang/String;",
            "LX/1Cv;",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2577355
    invoke-direct {p0}, LX/DNC;-><init>()V

    .line 2577356
    iput-object p1, p0, LX/IRw;->a:LX/IRJ;

    .line 2577357
    iput-object p2, p0, LX/IRw;->b:LX/DOp;

    .line 2577358
    iput-object p3, p0, LX/IRw;->c:LX/IVs;

    .line 2577359
    iput-object p4, p0, LX/IRw;->d:LX/0Ot;

    .line 2577360
    iput-object p5, p0, LX/IRw;->e:Landroid/content/Context;

    .line 2577361
    iput-object p6, p0, LX/IRw;->f:LX/DNR;

    .line 2577362
    iput-object p7, p0, LX/IRw;->g:LX/DNJ;

    .line 2577363
    iput-object p8, p0, LX/IRw;->h:Ljava/lang/String;

    .line 2577364
    iput-object p9, p0, LX/IRw;->j:LX/1Cv;

    .line 2577365
    iput-object p10, p0, LX/IRw;->i:LX/1PT;

    .line 2577366
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2577354
    iget-object v0, p0, LX/IRw;->a:LX/IRJ;

    sget-object v1, LX/IRH;->NORMAL:LX/IRH;

    iget-object v2, p0, LX/IRw;->e:Landroid/content/Context;

    iget-object v3, p0, LX/IRw;->i:LX/1PT;

    new-instance v4, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleControllerResponder$1;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleControllerResponder$1;-><init>(LX/IRw;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/1Qq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2577353
    iget-object v0, p0, LX/IRw;->k:LX/1Qq;

    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2577343
    iget-object v0, p0, LX/IRw;->b:LX/DOp;

    iget-object v1, p0, LX/IRw;->c:LX/IVs;

    iget-object v2, p0, LX/IRw;->d:LX/0Ot;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, LX/IRw;->f:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/IRw;->k:LX/1Qq;

    .line 2577344
    iget-object v0, p0, LX/IRw;->k:LX/1Qq;

    return-object v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2577349
    const v0, 0x7f0d0d65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2577350
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2577351
    iget-object v1, p0, LX/IRw;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577352
    return-void
.end method

.method public final b()LX/1Cv;
    .locals 1

    .prologue
    .line 2577348
    iget-object v0, p0, LX/IRw;->j:LX/1Cv;

    return-object v0
.end method

.method public final d()LX/1Cv;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2577347
    iget-object v0, p0, LX/IRw;->j:LX/1Cv;

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2577345
    iget-object v0, p0, LX/IRw;->g:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2577346
    return-void
.end method
