.class public final LX/IB9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/model/Event;

.field public final synthetic b:Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;Lcom/facebook/events/model/Event;)V
    .locals 0

    .prologue
    .line 2546680
    iput-object p1, p0, LX/IB9;->b:Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;

    iput-object p2, p0, LX/IB9;->a:Lcom/facebook/events/model/Event;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x1c00b348

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2546668
    iget-object v0, p0, LX/IB9;->b:Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;

    iget-object v0, v0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2546669
    iget-object v3, p0, LX/IB9;->a:Lcom/facebook/events/model/Event;

    .line 2546670
    iget-object p1, v3, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v3, p1

    .line 2546671
    if-eqz v3, :cond_0

    .line 2546672
    sget-object v3, LX/0ax;->aE:Ljava/lang/String;

    iget-object p1, p0, LX/IB9;->a:Lcom/facebook/events/model/Event;

    .line 2546673
    iget-object p0, p1, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object p1, p0

    .line 2546674
    invoke-static {v3, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2546675
    :goto_0
    move-object v3, v3

    .line 2546676
    invoke-interface {v0, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2546677
    const v0, -0x1f28b80

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    sget-object p1, LX/0ax;->bE:Ljava/lang/String;

    iget-object v3, p0, LX/IB9;->b:Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;

    iget-object v3, v3, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 2546678
    iget-object p0, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, p0

    .line 2546679
    invoke-static {p1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
