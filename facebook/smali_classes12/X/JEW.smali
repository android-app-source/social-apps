.class public LX/JEW;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Tn;

.field public static final b:LX/2d5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2d5",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/2cv;

.field private final d:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2666086
    sget-object v0, LX/4oz;->a:LX/0Tn;

    const-string v1, "group_fiest_post_prompt"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2666087
    sput-object v0, LX/JEW;->a:LX/0Tn;

    const-string v1, "group_first_post_prompt_ids"

    const-class v2, Ljava/util/List;

    invoke-static {v0, v1, v2}, LX/2d5;->a(LX/0Tn;Ljava/lang/String;Ljava/lang/Class;)LX/2d5;

    move-result-object v0

    sput-object v0, LX/JEW;->b:LX/2d5;

    return-void
.end method

.method public constructor <init>(LX/2cv;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2666088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2666089
    iput-object p1, p0, LX/JEW;->c:LX/2cv;

    .line 2666090
    iput-object p2, p0, LX/JEW;->d:LX/0Uh;

    .line 2666091
    return-void
.end method

.method public static a(LX/0QB;)LX/JEW;
    .locals 3

    .prologue
    .line 2666092
    new-instance v2, LX/JEW;

    invoke-static {p0}, LX/2cv;->b(LX/0QB;)LX/2cv;

    move-result-object v0

    check-cast v0, LX/2cv;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v0, v1}, LX/JEW;-><init>(LX/2cv;LX/0Uh;)V

    .line 2666093
    move-object v0, v2

    .line 2666094
    return-object v0
.end method

.method public static a(LX/JEW;)Z
    .locals 3

    .prologue
    .line 2666095
    iget-object v0, p0, LX/JEW;->d:LX/0Uh;

    const/16 v1, 0x2df

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static b(LX/JEW;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2666096
    iget-object v0, p0, LX/JEW;->c:LX/2cv;

    sget-object v1, LX/JEW;->b:LX/2d5;

    invoke-virtual {v0, v1}, LX/2cv;->a(LX/2d5;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2666097
    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2666098
    :goto_0
    return-object v0

    .line 2666099
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p0, LX/JEW;->c:LX/2cv;

    sget-object v2, LX/JEW;->b:LX/2d5;

    invoke-virtual {v0, v2}, LX/2cv;->a(LX/2d5;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    goto :goto_0
.end method
