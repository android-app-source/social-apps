.class public final LX/JS1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/Rect;

.field public final b:Lcom/facebook/graphql/model/GraphQLApplication;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphObject;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAudio;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;Lcom/facebook/graphql/model/GraphQLApplication;Ljava/util/List;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Rect;",
            "Lcom/facebook/graphql/model/GraphQLApplication;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphObject;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAudio;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2694595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2694596
    iput-object p1, p0, LX/JS1;->a:Landroid/graphics/Rect;

    .line 2694597
    iput-object p2, p0, LX/JS1;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 2694598
    iput-object p3, p0, LX/JS1;->c:Ljava/util/List;

    .line 2694599
    iput-object p4, p0, LX/JS1;->d:LX/0Px;

    .line 2694600
    iput-object p5, p0, LX/JS1;->e:LX/0Px;

    .line 2694601
    return-void
.end method
