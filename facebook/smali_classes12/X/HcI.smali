.class public final LX/HcI;
.super LX/HcB;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/HcB",
        "<",
        "Lcom/facebook/share/model/SharePhoto;",
        "LX/HcI;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/graphics/Bitmap;

.field public b:Landroid/net/Uri;

.field public c:Z

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486909
    invoke-direct {p0}, LX/HcB;-><init>()V

    return-void
.end method

.method public static a(LX/HcI;Lcom/facebook/share/model/SharePhoto;)LX/HcI;
    .locals 2

    .prologue
    .line 2486910
    if-nez p1, :cond_0

    .line 2486911
    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, LX/HcB;->a(Lcom/facebook/share/model/ShareMedia;)LX/HcB;

    move-result-object v0

    check-cast v0, LX/HcI;

    .line 2486912
    iget-object v1, p1, Lcom/facebook/share/model/SharePhoto;->a:Landroid/graphics/Bitmap;

    move-object v1, v1

    .line 2486913
    iput-object v1, v0, LX/HcI;->a:Landroid/graphics/Bitmap;

    .line 2486914
    move-object v0, v0

    .line 2486915
    iget-object v1, p1, Lcom/facebook/share/model/SharePhoto;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2486916
    iput-object v1, v0, LX/HcI;->b:Landroid/net/Uri;

    .line 2486917
    move-object v0, v0

    .line 2486918
    iget-boolean v1, p1, Lcom/facebook/share/model/SharePhoto;->c:Z

    move v1, v1

    .line 2486919
    iput-boolean v1, v0, LX/HcI;->c:Z

    .line 2486920
    move-object v0, v0

    .line 2486921
    iget-object v1, p1, Lcom/facebook/share/model/SharePhoto;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2486922
    iput-object v1, v0, LX/HcI;->d:Ljava/lang/String;

    .line 2486923
    move-object p0, v0

    .line 2486924
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/share/model/ShareMedia;)LX/HcB;
    .locals 1

    .prologue
    .line 2486925
    check-cast p1, Lcom/facebook/share/model/SharePhoto;

    invoke-static {p0, p1}, LX/HcI;->a(LX/HcI;Lcom/facebook/share/model/SharePhoto;)LX/HcI;

    move-result-object v0

    return-object v0
.end method
