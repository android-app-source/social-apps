.class public LX/JEM;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/fbui/glyph/GlyphView;

.field public d:Landroid/view/View;

.field public e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2665863
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2665864
    const/4 v4, 0x0

    .line 2665865
    const v0, 0x7f0315a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2665866
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/JEM;->setOrientation(I)V

    .line 2665867
    iput-object p1, p0, LX/JEM;->e:Landroid/content/Context;

    .line 2665868
    const v0, 0x7f0d30c1

    invoke-virtual {p0, v0}, LX/JEM;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JEM;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2665869
    const v0, 0x7f0d30c2

    invoke-virtual {p0, v0}, LX/JEM;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JEM;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2665870
    const v0, 0x7f0d30c0

    invoke-virtual {p0, v0}, LX/JEM;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/JEM;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2665871
    const v0, 0x7f0d30c3

    invoke-virtual {p0, v0}, LX/JEM;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JEM;->d:Landroid/view/View;

    .line 2665872
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2665873
    invoke-virtual {p0}, LX/JEM;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b24b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, LX/JEM;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b24b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v1, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2665874
    invoke-virtual {p0, v0}, LX/JEM;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2665875
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;Z)V
    .locals 3

    .prologue
    .line 2665876
    iget-object v1, p0, LX/JEM;->d:Landroid/view/View;

    if-eqz p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2665877
    iget-object v0, p0, LX/JEM;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, LX/JEM;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p3}, LX/JEN;->a(Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2665878
    iget-object v0, p0, LX/JEM;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2665879
    iget-object v0, p0, LX/JEM;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2665880
    return-void

    .line 2665881
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
