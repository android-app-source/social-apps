.class public final enum LX/IF4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IF4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IF4;

.field public static final enum ATTACHMENT_ADD:LX/IF4;

.field public static final enum AUTO_DEFAULT:LX/IF4;

.field public static final enum DRAFT_RECOVERY:LX/IF4;

.field public static final enum NEWLINE_COUNT:LX/IF4;

.field public static final enum OTHER:LX/IF4;

.field public static final enum PREVIOUSLY_SELECTED:LX/IF4;

.field public static final enum STICKY_STYLES:LX/IF4;

.field public static final enum TEXT_DELETED:LX/IF4;

.field public static final enum TEXT_TOO_LONG:LX/IF4;

.field public static final enum USER_SELECT:LX/IF4;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2553552
    new-instance v0, LX/IF4;

    const-string v1, "ATTACHMENT_ADD"

    invoke-direct {v0, v1, v3}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->ATTACHMENT_ADD:LX/IF4;

    .line 2553553
    new-instance v0, LX/IF4;

    const-string v1, "AUTO_DEFAULT"

    invoke-direct {v0, v1, v4}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->AUTO_DEFAULT:LX/IF4;

    .line 2553554
    new-instance v0, LX/IF4;

    const-string v1, "NEWLINE_COUNT"

    invoke-direct {v0, v1, v5}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->NEWLINE_COUNT:LX/IF4;

    .line 2553555
    new-instance v0, LX/IF4;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v6}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->OTHER:LX/IF4;

    .line 2553556
    new-instance v0, LX/IF4;

    const-string v1, "STICKY_STYLES"

    invoke-direct {v0, v1, v7}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->STICKY_STYLES:LX/IF4;

    .line 2553557
    new-instance v0, LX/IF4;

    const-string v1, "TEXT_DELETED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->TEXT_DELETED:LX/IF4;

    .line 2553558
    new-instance v0, LX/IF4;

    const-string v1, "TEXT_TOO_LONG"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->TEXT_TOO_LONG:LX/IF4;

    .line 2553559
    new-instance v0, LX/IF4;

    const-string v1, "USER_SELECT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->USER_SELECT:LX/IF4;

    .line 2553560
    new-instance v0, LX/IF4;

    const-string v1, "PREVIOUSLY_SELECTED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->PREVIOUSLY_SELECTED:LX/IF4;

    .line 2553561
    new-instance v0, LX/IF4;

    const-string v1, "DRAFT_RECOVERY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/IF4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IF4;->DRAFT_RECOVERY:LX/IF4;

    .line 2553562
    const/16 v0, 0xa

    new-array v0, v0, [LX/IF4;

    sget-object v1, LX/IF4;->ATTACHMENT_ADD:LX/IF4;

    aput-object v1, v0, v3

    sget-object v1, LX/IF4;->AUTO_DEFAULT:LX/IF4;

    aput-object v1, v0, v4

    sget-object v1, LX/IF4;->NEWLINE_COUNT:LX/IF4;

    aput-object v1, v0, v5

    sget-object v1, LX/IF4;->OTHER:LX/IF4;

    aput-object v1, v0, v6

    sget-object v1, LX/IF4;->STICKY_STYLES:LX/IF4;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/IF4;->TEXT_DELETED:LX/IF4;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IF4;->TEXT_TOO_LONG:LX/IF4;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IF4;->USER_SELECT:LX/IF4;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/IF4;->PREVIOUSLY_SELECTED:LX/IF4;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/IF4;->DRAFT_RECOVERY:LX/IF4;

    aput-object v2, v0, v1

    sput-object v0, LX/IF4;->$VALUES:[LX/IF4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2553551
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IF4;
    .locals 1

    .prologue
    .line 2553550
    const-class v0, LX/IF4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IF4;

    return-object v0
.end method

.method public static values()[LX/IF4;
    .locals 1

    .prologue
    .line 2553549
    sget-object v0, LX/IF4;->$VALUES:[LX/IF4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IF4;

    return-object v0
.end method
