.class public final LX/JLC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private final a:Lcom/facebook/react/bridge/Callback;

.field private final b:I

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/Callback;I)V
    .locals 1

    .prologue
    .line 2680734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2680735
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JLC;->c:Z

    .line 2680736
    iput-object p1, p0, LX/JLC;->a:Lcom/facebook/react/bridge/Callback;

    .line 2680737
    iput p2, p0, LX/JLC;->b:I

    .line 2680738
    return-void
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 2680739
    iget-boolean v0, p0, LX/JLC;->c:Z

    if-nez v0, :cond_0

    .line 2680740
    iget-object v0, p0, LX/JLC;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LX/JLC;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2680741
    :cond_0
    return-void
.end method
