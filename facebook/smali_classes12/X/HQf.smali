.class public LX/HQf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HQc;


# static fields
.field public static final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0ta;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final c:LX/3fB;

.field private final d:LX/CXj;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/os/ParcelUuid;

.field private g:Ljava/lang/String;

.field private h:Z

.field private final i:LX/HQe;

.field private j:Z

.field private final k:LX/HQd;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2463809
    sget-object v0, LX/0ta;->NO_DATA:LX/0ta;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    sput-object v0, LX/HQf;->a:LX/0am;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3fB;LX/CXj;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/3fB;",
            "LX/CXj;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2463801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2463802
    new-instance v0, LX/HQe;

    invoke-direct {v0, p0}, LX/HQe;-><init>(LX/HQf;)V

    iput-object v0, p0, LX/HQf;->i:LX/HQe;

    .line 2463803
    new-instance v0, LX/HQd;

    invoke-direct {v0, p0}, LX/HQd;-><init>(LX/HQf;)V

    iput-object v0, p0, LX/HQf;->k:LX/HQd;

    .line 2463804
    iput-object p1, p0, LX/HQf;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2463805
    iput-object p2, p0, LX/HQf;->c:LX/3fB;

    .line 2463806
    iput-object p3, p0, LX/HQf;->d:LX/CXj;

    .line 2463807
    iput-object p4, p0, LX/HQf;->e:LX/0Ot;

    .line 2463808
    return-void
.end method

.method public static a$redex0(LX/HQf;LX/0am;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/0ta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2463795
    iget-boolean v0, p0, LX/HQf;->h:Z

    if-eqz v0, :cond_0

    .line 2463796
    :goto_0
    return-void

    .line 2463797
    :cond_0
    iget-object v0, p0, LX/HQf;->c:LX/3fB;

    const-string v1, "ProfilePhotoDownloaded"

    invoke-virtual {v0, v1}, LX/3fB;->c(Ljava/lang/String;)LX/3fB;

    .line 2463798
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HQf;->h:Z

    .line 2463799
    iget-object v0, p0, LX/HQf;->d:LX/CXj;

    new-instance v1, LX/CXq;

    iget-object v2, p0, LX/HQf;->f:Landroid/os/ParcelUuid;

    sget-object v3, LX/CXp;->PROFILE_PHOTO_COMPLETE:LX/CXp;

    invoke-direct {v1, v2, v3, p1}, LX/CXq;-><init>(Landroid/os/ParcelUuid;LX/CXp;LX/0am;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2463800
    iget-object v0, p0, LX/HQf;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x130065

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/HQf;
    .locals 5

    .prologue
    .line 2463793
    new-instance v3, LX/HQf;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/3fB;->a(LX/0QB;)LX/3fB;

    move-result-object v1

    check-cast v1, LX/3fB;

    invoke-static {p0}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v2

    check-cast v2, LX/CXj;

    const/16 v4, 0x259

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v3, v0, v1, v2, v4}, LX/HQf;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3fB;LX/CXj;LX/0Ot;)V

    .line 2463794
    return-object v3
.end method

.method private b(LX/CZd;LX/HQa;)V
    .locals 3

    .prologue
    const v2, 0x130065

    .line 2463778
    iget-object v0, p2, LX/HQa;->g:LX/HQZ;

    iget-object v0, v0, LX/HQZ;->a:LX/HQY;

    sget-object v1, LX/HQY;->NOT_VISIBLE:LX/HQY;

    if-ne v0, v1, :cond_1

    .line 2463779
    sget-object v0, LX/HQf;->a:LX/0am;

    invoke-static {p0, v0}, LX/HQf;->a$redex0(LX/HQf;LX/0am;)V

    .line 2463780
    :cond_0
    :goto_0
    return-void

    .line 2463781
    :cond_1
    iget-object v0, p2, LX/HQa;->g:LX/HQZ;

    iget-object v0, v0, LX/HQZ;->d:Ljava/lang/String;

    .line 2463782
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2463783
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463784
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2463785
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463786
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2463787
    iget-object v0, p0, LX/HQf;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "tiny_profile_pic"

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2463788
    :cond_2
    iget-object v0, p2, LX/HQa;->g:LX/HQZ;

    iget-object v0, v0, LX/HQZ;->c:LX/HQX;

    sget-object v1, LX/HQX;->NO_DATA:LX/HQX;

    if-ne v0, v1, :cond_3

    iget-boolean v0, p2, LX/HQa;->d:Z

    if-eqz v0, :cond_3

    .line 2463789
    sget-object v0, LX/HQf;->a:LX/0am;

    invoke-static {p0, v0}, LX/HQf;->a$redex0(LX/HQf;LX/0am;)V

    goto :goto_0

    .line 2463790
    :cond_3
    iget-object v0, p2, LX/HQa;->g:LX/HQZ;

    iget-object v0, v0, LX/HQZ;->c:LX/HQX;

    sget-object v1, LX/HQX;->HAS_DATA:LX/HQX;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, LX/HQf;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2463791
    iget-object v0, p0, LX/HQf;->c:LX/3fB;

    const-string v1, "ProfilePhotoDownloaded"

    invoke-virtual {v0, v1}, LX/3fB;->a(Ljava/lang/String;)LX/3fB;

    .line 2463792
    iget-object v0, p0, LX/HQf;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x13007a

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(II)V

    goto :goto_0
.end method

.method public static b$redex0(LX/HQf;LX/0am;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/0ta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2463772
    iget-boolean v0, p0, LX/HQf;->j:Z

    if-eqz v0, :cond_0

    .line 2463773
    :goto_0
    return-void

    .line 2463774
    :cond_0
    iget-object v0, p0, LX/HQf;->c:LX/3fB;

    const-string v1, "CoverPhotoDownloaded"

    invoke-virtual {v0, v1}, LX/3fB;->c(Ljava/lang/String;)LX/3fB;

    .line 2463775
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HQf;->j:Z

    .line 2463776
    iget-object v0, p0, LX/HQf;->d:LX/CXj;

    new-instance v1, LX/CXq;

    iget-object v2, p0, LX/HQf;->f:Landroid/os/ParcelUuid;

    sget-object v3, LX/CXp;->COVER_PHOTO_COMPLETE:LX/CXp;

    invoke-direct {v1, v2, v3, p1}, LX/CXq;-><init>(Landroid/os/ParcelUuid;LX/CXp;LX/0am;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2463777
    iget-object v0, p0, LX/HQf;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x13007b

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method

.method private c(LX/CZd;LX/HQa;)V
    .locals 3

    .prologue
    .line 2463762
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463763
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p2, LX/HQa;->d:Z

    if-eqz v0, :cond_1

    .line 2463764
    sget-object v0, LX/HQf;->a:LX/0am;

    invoke-static {p0, v0}, LX/HQf;->b$redex0(LX/HQf;LX/0am;)V

    .line 2463765
    :cond_0
    :goto_0
    return-void

    .line 2463766
    :cond_1
    iget-object v0, p2, LX/HQa;->c:LX/HQV;

    iget-object v0, v0, LX/HQV;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2463767
    iget-boolean v0, p2, LX/HQa;->d:Z

    if-eqz v0, :cond_0

    .line 2463768
    sget-object v0, LX/HQf;->a:LX/0am;

    invoke-static {p0, v0}, LX/HQf;->b$redex0(LX/HQf;LX/0am;)V

    goto :goto_0

    .line 2463769
    :cond_2
    invoke-direct {p0}, LX/HQf;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2463770
    iget-object v0, p0, LX/HQf;->c:LX/3fB;

    const-string v1, "CoverPhotoDownloaded"

    invoke-virtual {v0, v1}, LX/3fB;->a(Ljava/lang/String;)LX/3fB;

    .line 2463771
    iget-object v0, p0, LX/HQf;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x13007b

    const v2, 0x13007a

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(II)V

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 2463810
    iget-boolean v0, p0, LX/HQf;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 2463761
    iget-boolean v0, p0, LX/HQf;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1cC;
    .locals 1

    .prologue
    .line 2463760
    iget-object v0, p0, LX/HQf;->i:LX/HQe;

    return-object v0
.end method

.method public final a(LX/CZd;LX/HQa;)V
    .locals 0

    .prologue
    .line 2463757
    invoke-direct {p0, p1, p2}, LX/HQf;->b(LX/CZd;LX/HQa;)V

    .line 2463758
    invoke-direct {p0, p1, p2}, LX/HQf;->c(LX/CZd;LX/HQa;)V

    .line 2463759
    return-void
.end method

.method public final a(Landroid/os/ParcelUuid;)V
    .locals 3

    .prologue
    .line 2463744
    iput-object p1, p0, LX/HQf;->f:Landroid/os/ParcelUuid;

    .line 2463745
    iget-object v0, p0, LX/HQf;->f:Landroid/os/ParcelUuid;

    if-nez v0, :cond_0

    .line 2463746
    iget-object v0, p0, LX/HQf;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UUID in context header view is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2463747
    :goto_0
    return-void

    .line 2463748
    :cond_0
    iget-object v0, p0, LX/HQf;->f:Landroid/os/ParcelUuid;

    invoke-virtual {v0}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/HQf;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2463754
    if-eqz p1, :cond_0

    .line 2463755
    sget-object v0, LX/HQf;->a:LX/0am;

    invoke-static {p0, v0}, LX/HQf;->b$redex0(LX/HQf;LX/0am;)V

    .line 2463756
    :cond_0
    return-void
.end method

.method public final b()LX/1cC;
    .locals 1

    .prologue
    .line 2463753
    iget-object v0, p0, LX/HQf;->k:LX/HQd;

    return-object v0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/16 v3, 0x5b

    .line 2463749
    iget-object v0, p0, LX/HQf;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x13007b

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 2463750
    iget-object v0, p0, LX/HQf;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x130080

    iget-object v2, p0, LX/HQf;->g:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2463751
    sget-object v0, LX/HQf;->a:LX/0am;

    invoke-static {p0, v0}, LX/HQf;->b$redex0(LX/HQf;LX/0am;)V

    .line 2463752
    return-void
.end method
