.class public final LX/JHV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ju;


# instance fields
.field public final synthetic a:LX/JHW;


# direct methods
.method public constructor <init>(LX/JHW;)V
    .locals 0

    .prologue
    .line 2673086
    iput-object p1, p0, LX/JHV;->a:LX/JHW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IIIF)V
    .locals 2

    .prologue
    .line 2673077
    iget-object v0, p0, LX/JHV;->a:LX/JHW;

    iget-object v0, v0, LX/JHW;->h:LX/JHU;

    if-nez v0, :cond_0

    .line 2673078
    :goto_0
    return-void

    .line 2673079
    :cond_0
    iget-object v0, p0, LX/JHV;->a:LX/JHW;

    iget-object v0, v0, LX/JHW;->h:LX/JHU;

    .line 2673080
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object p0

    .line 2673081
    const-string v1, "target"

    iget-object p3, v0, LX/JHU;->a:LX/JHW;

    invoke-virtual {p3}, LX/JHW;->getId()I

    move-result p3

    invoke-interface {p0, v1, p3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2673082
    const-string v1, "videoWidth"

    invoke-interface {p0, v1, p1}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2673083
    const-string v1, "videoHeight"

    invoke-interface {p0, v1, p2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2673084
    iget-object v1, v0, LX/JHU;->b:LX/5rJ;

    const-class p3, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    invoke-virtual {v1, p3}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    iget-object p3, v0, LX/JHU;->a:LX/JHW;

    invoke-virtual {p3}, LX/JHW;->getId()I

    move-result p3

    const-string p4, "topVideoSizeDetected"

    invoke-interface {v1, p3, p4, p0}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2673085
    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 0

    .prologue
    .line 2673076
    return-void
.end method

.method public final a(LX/0L3;)V
    .locals 0

    .prologue
    .line 2673087
    return-void
.end method

.method public final a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 0

    .prologue
    .line 2673075
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 0

    .prologue
    .line 2673074
    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 2673073
    return-void
.end method
