.class public LX/Ija;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6y0;


# instance fields
.field private final a:LX/6yY;


# direct methods
.method public constructor <init>(LX/6yY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605924
    iput-object p1, p0, LX/Ija;->a:LX/6yY;

    .line 2605925
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605922
    iget-object v0, p0, LX/Ija;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605921
    iget-object v0, p0, LX/Ija;->a:LX/6yY;

    invoke-virtual {v0, p1, p2}, LX/6yY;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Lcom/facebook/messaging/dialog/ConfirmActionParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2605920
    iget-object v0, p0, LX/Ija;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2605919
    iget-object v0, p0, LX/Ija;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605913
    iget-object v0, p0, LX/Ija;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605918
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605917
    iget-object v0, p0, LX/Ija;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605916
    const/4 v0, 0x0

    return v0
.end method

.method public final h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605915
    const/4 v0, 0x1

    return v0
.end method

.method public final i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605914
    const/4 v0, 0x0

    return v0
.end method
