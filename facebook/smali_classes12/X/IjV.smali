.class public LX/IjV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6yS;


# instance fields
.field private final a:LX/6yY;

.field private b:LX/6qh;


# direct methods
.method public constructor <init>(LX/6yY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605813
    iput-object p1, p0, LX/IjV;->a:LX/6yY;

    .line 2605814
    return-void
.end method

.method private a(LX/IjO;Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;)V
    .locals 1

    .prologue
    .line 2605815
    invoke-static {p0, p2}, LX/IjV;->a(LX/IjV;Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2605816
    const/16 v0, 0x8

    .line 2605817
    iget-object p0, p1, LX/IjO;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2605818
    invoke-static {p1}, LX/IjO;->e(LX/IjO;)V

    .line 2605819
    :cond_0
    return-void
.end method

.method public static a(LX/IjV;Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;)Z
    .locals 1

    .prologue
    .line 2605820
    iget-object v0, p0, LX/IjV;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6E6;
    .locals 2

    .prologue
    .line 2605821
    check-cast p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    .line 2605822
    new-instance v0, LX/IjP;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/IjP;-><init>(Landroid/content/Context;)V

    .line 2605823
    const/16 v1, 0x8

    .line 2605824
    invoke-static {p0, p2}, LX/IjV;->a(LX/IjV;Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2605825
    invoke-virtual {v0, v1}, LX/IjP;->setVisibilityOfIsPrimaryCardTextView(I)V

    .line 2605826
    :goto_0
    iget-boolean v1, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    .line 2605827
    if-eqz v1, :cond_2

    .line 2605828
    const/4 v1, 0x0

    .line 2605829
    iget-object p1, v0, LX/IjP;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2605830
    iget-object p1, v0, LX/IjP;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1, v1}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 2605831
    iget-object p1, v0, LX/IjP;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0a0801

    invoke-static {v0, p1, v1}, LX/IjP;->a(LX/IjP;Lcom/facebook/resources/ui/FbTextView;I)V

    .line 2605832
    :goto_1
    iget-object v1, p0, LX/IjV;->b:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 2605833
    return-object v0

    .line 2605834
    :cond_0
    iget-boolean p1, p2, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->g:Z

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :cond_1
    invoke-virtual {v0, v1}, LX/IjP;->setVisibilityOfIsPrimaryCardTextView(I)V

    goto :goto_0

    .line 2605835
    :cond_2
    iget-object p1, v0, LX/IjP;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2605836
    iget-object p1, v0, LX/IjP;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 2605837
    iget-object p1, v0, LX/IjP;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0a0800

    invoke-static {v0, p1, v1}, LX/IjP;->a(LX/IjP;Lcom/facebook/resources/ui/FbTextView;I)V

    .line 2605838
    goto :goto_1
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 2605839
    iput-object p1, p0, LX/IjV;->b:LX/6qh;

    .line 2605840
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6E6;
    .locals 5

    .prologue
    .line 2605841
    move-object v0, p2

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    .line 2605842
    iget-object v1, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    check-cast v1, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2605843
    iget-boolean v2, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->j:Z

    .line 2605844
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605845
    new-instance v3, LX/IjO;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/IjO;-><init>(Landroid/content/Context;)V

    .line 2605846
    iput-object v1, v3, LX/IjO;->e:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2605847
    iput-boolean v2, v3, LX/IjO;->f:Z

    .line 2605848
    invoke-direct {p0, v3, v0}, LX/IjV;->a(LX/IjO;Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;)V

    .line 2605849
    const/16 p1, 0x8

    .line 2605850
    invoke-static {p0, v0}, LX/IjV;->a(LX/IjV;Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2605851
    invoke-virtual {v3, p1}, LX/IjO;->setVisibilityOfMakePrimaryButton(I)V

    .line 2605852
    :goto_0
    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    .line 2605853
    if-eqz v0, :cond_3

    .line 2605854
    const/4 v0, 0x0

    .line 2605855
    iget-object v1, v3, LX/IjO;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 2605856
    iget-object v1, v3, LX/IjO;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 2605857
    iget-object v1, v3, LX/IjO;->b:Lcom/facebook/resources/ui/FbTextView;

    const v0, 0x7f0a0801

    invoke-static {v3, v1, v0}, LX/IjO;->a(LX/IjO;Lcom/facebook/resources/ui/FbTextView;I)V

    .line 2605858
    :goto_1
    iput-object p2, v3, LX/IjO;->h:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 2605859
    iget-object v0, p0, LX/IjV;->b:LX/6qh;

    .line 2605860
    iput-object v0, v3, LX/IjO;->g:LX/6qh;

    .line 2605861
    iget-object v1, v3, LX/IjO;->b:Lcom/facebook/resources/ui/FbTextView;

    new-instance v0, LX/IjM;

    invoke-direct {v0, v3}, LX/IjM;-><init>(LX/IjO;)V

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2605862
    iget-object v1, v3, LX/IjO;->d:Lcom/facebook/resources/ui/FbTextView;

    new-instance v0, LX/IjN;

    invoke-direct {v0, v3}, LX/IjN;-><init>(LX/IjO;)V

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2605863
    return-object v3

    .line 2605864
    :cond_0
    iget-boolean v2, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->g:Z

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/PaymentCard;->l()LX/DtH;

    move-result-object v2

    sget-object v4, LX/DtH;->DEBIT_CARD:LX/DtH;

    if-eq v2, v4, :cond_2

    .line 2605865
    :cond_1
    invoke-virtual {v3, p1}, LX/IjO;->setVisibilityOfMakePrimaryButton(I)V

    goto :goto_0

    .line 2605866
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, LX/IjO;->setVisibilityOfMakePrimaryButton(I)V

    goto :goto_0

    .line 2605867
    :cond_3
    const/4 v0, 0x1

    .line 2605868
    iget-object v1, v3, LX/IjO;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 2605869
    iget-object v1, v3, LX/IjO;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 2605870
    iget-object v1, v3, LX/IjO;->b:Lcom/facebook/resources/ui/FbTextView;

    const v0, 0x7f0a080c

    invoke-static {v3, v1, v0}, LX/IjO;->a(LX/IjO;Lcom/facebook/resources/ui/FbTextView;I)V

    .line 2605871
    goto :goto_1
.end method
