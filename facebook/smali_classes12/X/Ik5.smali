.class public LX/Ik5;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomLinearLayout;"
    }
.end annotation


# static fields
.field private static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/IkC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/InT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IkA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

.field private f:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field private g:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private h:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private i:Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

.field private j:Lcom/facebook/payments/ui/SingleItemInfoView;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2606371
    const v0, 0x7f082c74

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "https://m.facebook.com/legal/m"

    invoke-static {v0, v1}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v0

    const v1, 0x7f082c73

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "https://m.facebook.com/help/messenger-app/870471599656315"

    invoke-static {v1, v2}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Ik5;->d:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2606366
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Ik5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2606367
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2606369
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ik5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606370
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2606372
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606373
    const-class v0, LX/Ik5;

    invoke-static {v0, p0}, LX/Ik5;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2606374
    const v0, 0x7f030b34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2606375
    const v0, 0x7f0d1c3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    iput-object v0, p0, LX/Ik5;->e:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    .line 2606376
    const v0, 0x7f0d1c3b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, LX/Ik5;->f:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2606377
    const v0, 0x7f0d1c39

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik5;->g:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606378
    const v0, 0x7f0d0a10

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik5;->h:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606379
    const v0, 0x7f0d1c3a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

    iput-object v0, p0, LX/Ik5;->i:Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

    .line 2606380
    const v0, 0x7f0d1aac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/SingleItemInfoView;

    iput-object v0, p0, LX/Ik5;->j:Lcom/facebook/payments/ui/SingleItemInfoView;

    .line 2606381
    return-void
.end method

.method private static a(LX/Ik5;LX/IkC;LX/InT;LX/IkA;)V
    .locals 0

    .prologue
    .line 2606368
    iput-object p1, p0, LX/Ik5;->a:LX/IkC;

    iput-object p2, p0, LX/Ik5;->b:LX/InT;

    iput-object p3, p0, LX/Ik5;->c:LX/IkA;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Ik5;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, LX/Ik5;

    invoke-static {v2}, LX/IkC;->a(LX/0QB;)LX/IkC;

    move-result-object v0

    check-cast v0, LX/IkC;

    invoke-static {v2}, LX/InT;->b(LX/0QB;)LX/InT;

    move-result-object v1

    check-cast v1, LX/InT;

    invoke-static {v2}, LX/IkA;->b(LX/0QB;)LX/IkA;

    move-result-object v2

    check-cast v2, LX/IkA;

    invoke-static {p0, v0, v1, v2}, LX/Ik5;->a(LX/Ik5;LX/IkC;LX/InT;LX/IkA;)V

    return-void
.end method
