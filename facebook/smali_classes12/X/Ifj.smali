.class public LX/Ifj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0SG;

.field private final c:LX/94j;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/94j;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2600099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600100
    iput-object p1, p0, LX/Ifj;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2600101
    iput-object p2, p0, LX/Ifj;->b:LX/0SG;

    .line 2600102
    iput-object p3, p0, LX/Ifj;->c:LX/94j;

    .line 2600103
    return-void
.end method

.method public static b(LX/0QB;)LX/Ifj;
    .locals 4

    .prologue
    .line 2600104
    new-instance v3, LX/Ifj;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/94j;->b(LX/0QB;)LX/94j;

    move-result-object v2

    check-cast v2, LX/94j;

    invoke-direct {v3, v0, v1, v2}, LX/Ifj;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/94j;)V

    .line 2600105
    return-object v3
.end method
