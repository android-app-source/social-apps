.class public final LX/ID5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<+",
        "Lcom/facebook/graphql/modelutil/BaseModel;",
        ">;>;",
        "LX/ID4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;


# direct methods
.method public constructor <init>(Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;)V
    .locals 0

    .prologue
    .line 2550344
    iput-object p1, p0, LX/ID5;->a:Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2550328
    check-cast p1, Ljava/util/List;

    const/4 v0, 0x0

    .line 2550329
    if-eqz p1, :cond_2

    .line 2550330
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    move-object v2, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2550331
    if-eqz v0, :cond_0

    .line 2550332
    iget-object v4, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2550333
    if-eqz v4, :cond_0

    .line 2550334
    iget-object v4, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2550335
    instance-of v4, v4, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;

    if-eqz v4, :cond_1

    .line 2550336
    iget-object v2, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v2

    .line 2550337
    check-cast v0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;

    move-object v2, v0

    goto :goto_0

    .line 2550338
    :cond_1
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2550339
    instance-of v1, v1, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointBucketFieldsModel;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2550340
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 2550341
    check-cast v0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointBucketFieldsModel;

    move-object v1, v0

    .line 2550342
    goto :goto_0

    :cond_2
    move-object v1, v0

    move-object v2, v0

    .line 2550343
    :cond_3
    new-instance v0, LX/ID4;

    invoke-direct {v0, v2, v1}, LX/ID4;-><init>(Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointBucketFieldsModel;)V

    return-object v0
.end method
