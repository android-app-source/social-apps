.class public LX/HL7;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/3mX",
        "<",
        "LX/HL6;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/HJS;

.field private final d:LX/HJO;

.field private final e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field private final f:LX/HLF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/2km;LX/25M;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/HJS;LX/HJO;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2km;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/HLF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/HL6;",
            ">;TE;",
            "LX/25M;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HLF;",
            "LX/HJS;",
            "LX/HJO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455591
    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2455592
    iput-object p7, p0, LX/HL7;->c:LX/HJS;

    .line 2455593
    iput-object p8, p0, LX/HL7;->d:LX/HJO;

    .line 2455594
    iput-object p5, p0, LX/HL7;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455595
    iput-object p6, p0, LX/HL7;->f:LX/HLF;

    .line 2455596
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2455581
    iget-object v0, p0, LX/HL7;->c:LX/HJS;

    const/4 v1, 0x0

    .line 2455582
    new-instance v2, LX/HJR;

    invoke-direct {v2, v0}, LX/HJR;-><init>(LX/HJS;)V

    .line 2455583
    iget-object p0, v0, LX/HJS;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HJQ;

    .line 2455584
    if-nez p0, :cond_0

    .line 2455585
    new-instance p0, LX/HJQ;

    invoke-direct {p0, v0}, LX/HJQ;-><init>(LX/HJS;)V

    .line 2455586
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HJQ;->a$redex0(LX/HJQ;LX/1De;IILX/HJR;)V

    .line 2455587
    move-object v2, p0

    .line 2455588
    move-object v1, v2

    .line 2455589
    move-object v0, v1

    .line 2455590
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2455554
    check-cast p2, LX/HL6;

    .line 2455555
    iget-object v0, p0, LX/HL7;->d:LX/HJO;

    const/4 v1, 0x0

    .line 2455556
    new-instance v2, LX/HJN;

    invoke-direct {v2, v0}, LX/HJN;-><init>(LX/HJO;)V

    .line 2455557
    iget-object v3, v0, LX/HJO;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HJM;

    .line 2455558
    if-nez v3, :cond_0

    .line 2455559
    new-instance v3, LX/HJM;

    invoke-direct {v3, v0}, LX/HJM;-><init>(LX/HJO;)V

    .line 2455560
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/HJM;->a$redex0(LX/HJM;LX/1De;IILX/HJN;)V

    .line 2455561
    move-object v2, v3

    .line 2455562
    move-object v1, v2

    .line 2455563
    move-object v1, v1

    .line 2455564
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/2km;

    .line 2455565
    iget-object v2, v1, LX/HJM;->a:LX/HJN;

    iput-object v0, v2, LX/HJN;->b:LX/2km;

    .line 2455566
    iget-object v2, v1, LX/HJM;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2455567
    move-object v0, v1

    .line 2455568
    iget-object v1, p0, LX/HL7;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455569
    iget-object v2, v0, LX/HJM;->a:LX/HJN;

    iput-object v1, v2, LX/HJN;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455570
    iget-object v2, v0, LX/HJM;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2455571
    move-object v0, v0

    .line 2455572
    iget-object v1, v0, LX/HJM;->a:LX/HJN;

    iput-object p2, v1, LX/HJN;->a:LX/HL6;

    .line 2455573
    iget-object v1, v0, LX/HJM;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2455574
    move-object v0, v0

    .line 2455575
    iget-object v1, p0, LX/HL7;->f:LX/HLF;

    .line 2455576
    iget-object v2, v0, LX/HJM;->a:LX/HJN;

    iput-object v1, v2, LX/HJN;->d:LX/HLF;

    .line 2455577
    iget-object v2, v0, LX/HJM;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2455578
    move-object v0, v0

    .line 2455579
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2455580
    const/4 v0, 0x0

    return v0
.end method
