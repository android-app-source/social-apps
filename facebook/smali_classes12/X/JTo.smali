.class public LX/JTo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1vb;


# direct methods
.method public constructor <init>(LX/1vb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2697300
    iput-object p1, p0, LX/JTo;->a:LX/1vb;

    .line 2697301
    return-void
.end method

.method public static a(LX/0QB;)LX/JTo;
    .locals 4

    .prologue
    .line 2697302
    const-class v1, LX/JTo;

    monitor-enter v1

    .line 2697303
    :try_start_0
    sget-object v0, LX/JTo;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697304
    sput-object v2, LX/JTo;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697305
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697306
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697307
    new-instance p0, LX/JTo;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v3

    check-cast v3, LX/1vb;

    invoke-direct {p0, v3}, LX/JTo;-><init>(LX/1vb;)V

    .line 2697308
    move-object v0, p0

    .line 2697309
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697310
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JTo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697311
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697312
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
