.class public LX/HX9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:LX/03R;

.field public final c:Landroid/os/ParcelUuid;

.field public final d:Landroid/location/Location;

.field public final e:Z

.field public final f:LX/89z;


# direct methods
.method public constructor <init>(LX/HX8;)V
    .locals 2

    .prologue
    .line 2478438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2478439
    iget-wide v0, p1, LX/HX8;->a:J

    iput-wide v0, p0, LX/HX9;->a:J

    .line 2478440
    iget-object v0, p1, LX/HX8;->b:LX/03R;

    iput-object v0, p0, LX/HX9;->b:LX/03R;

    .line 2478441
    iget-object v0, p1, LX/HX8;->c:Landroid/os/ParcelUuid;

    iput-object v0, p0, LX/HX9;->c:Landroid/os/ParcelUuid;

    .line 2478442
    iget-object v0, p1, LX/HX8;->d:Landroid/location/Location;

    iput-object v0, p0, LX/HX9;->d:Landroid/location/Location;

    .line 2478443
    iget-boolean v0, p1, LX/HX8;->e:Z

    iput-boolean v0, p0, LX/HX9;->e:Z

    .line 2478444
    iget-object v0, p1, LX/HX8;->f:LX/89z;

    iput-object v0, p0, LX/HX9;->f:LX/89z;

    .line 2478445
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2478446
    iget-wide v0, p0, LX/HX9;->a:J

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2478447
    if-ne p0, p1, :cond_1

    .line 2478448
    :cond_0
    :goto_0
    return v0

    .line 2478449
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2478450
    goto :goto_0

    .line 2478451
    :cond_3
    check-cast p1, LX/HX9;

    .line 2478452
    iget-wide v2, p0, LX/HX9;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, LX/HX9;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/HX9;->b:LX/03R;

    iget-object v3, p1, LX/HX9;->b:LX/03R;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/HX9;->c:Landroid/os/ParcelUuid;

    iget-object v3, p1, LX/HX9;->c:Landroid/os/ParcelUuid;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/HX9;->d:Landroid/location/Location;

    iget-object v3, p1, LX/HX9;->d:Landroid/location/Location;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, LX/HX9;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, LX/HX9;->e:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/HX9;->f:LX/89z;

    iget-object v3, p1, LX/HX9;->f:LX/89z;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2478453
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, LX/HX9;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/HX9;->b:LX/03R;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/HX9;->c:Landroid/os/ParcelUuid;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/HX9;->d:Landroid/location/Location;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LX/HX9;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/HX9;->f:LX/89z;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
