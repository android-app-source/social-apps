.class public LX/Ife;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/Ies;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IfT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ieu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IfP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IfW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1CX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ie8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public j:LX/1P0;

.field public k:I

.field private final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/JiY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:LX/If7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 2599969
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2599970
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2599971
    iput-object v0, p0, LX/Ife;->h:LX/0Ot;

    .line 2599972
    const/4 v0, 0x0

    iput v0, p0, LX/Ife;->k:I

    .line 2599973
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Ife;->l:Ljava/util/Set;

    .line 2599974
    new-instance v0, LX/IfX;

    invoke-direct {v0, p0}, LX/IfX;-><init>(LX/Ife;)V

    iput-object v0, p0, LX/Ife;->o:LX/If7;

    .line 2599975
    const/4 v4, 0x0

    .line 2599976
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, LX/Ife;

    new-instance v5, LX/Ies;

    invoke-static {p1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    invoke-direct {v5, v3}, LX/Ies;-><init>(Landroid/view/LayoutInflater;)V

    move-object v3, v5

    check-cast v3, LX/Ies;

    invoke-static {p1}, LX/IfT;->b(LX/0QB;)LX/IfT;

    move-result-object v5

    check-cast v5, LX/IfT;

    invoke-static {p1}, LX/Ieu;->a(LX/0QB;)LX/Ieu;

    move-result-object v6

    check-cast v6, LX/Ieu;

    invoke-static {p1}, LX/IfP;->a(LX/0QB;)LX/IfP;

    move-result-object v7

    check-cast v7, LX/IfP;

    const/16 v8, 0x272c

    invoke-static {p1, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    const/16 v10, 0x12af

    invoke-static {p1, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v0, 0x270b

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, LX/Ife;->a:LX/Ies;

    iput-object v5, v2, LX/Ife;->b:LX/IfT;

    iput-object v6, v2, LX/Ife;->c:LX/Ieu;

    iput-object v7, v2, LX/Ife;->d:LX/IfP;

    iput-object v8, v2, LX/Ife;->e:LX/0Ot;

    iput-object v9, v2, LX/Ife;->f:Ljava/util/concurrent/Executor;

    iput-object v10, v2, LX/Ife;->g:LX/0Ot;

    iput-object p1, v2, LX/Ife;->h:LX/0Ot;

    .line 2599977
    const v0, 0x7f030379

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2599978
    const v0, 0x7f0d0b3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/Ife;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2599979
    invoke-virtual {p0}, LX/Ife;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b24e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2599980
    invoke-virtual {p0}, LX/Ife;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b24e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2599981
    new-instance v2, LX/1P0;

    invoke-virtual {p0}, LX/Ife;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/Ife;->j:LX/1P0;

    .line 2599982
    iget-object v2, p0, LX/Ife;->j:LX/1P0;

    invoke-virtual {v2, v4}, LX/1P1;->b(I)V

    .line 2599983
    iget-object v2, p0, LX/Ife;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/IfY;

    invoke-direct {v3, p0, v1, v0}, LX/IfY;-><init>(LX/Ife;II)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2599984
    iget-object v0, p0, LX/Ife;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/Ife;->j:LX/1P0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2599985
    iget-object v0, p0, LX/Ife;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2599986
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v1

    .line 2599987
    iput-boolean v4, v0, LX/1Of;->g:Z

    .line 2599988
    iget-object v0, p0, LX/Ife;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/Ife;->a:LX/Ies;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2599989
    iget-object v0, p0, LX/Ife;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/IfZ;

    invoke-direct {v1, p0}, LX/IfZ;-><init>(LX/Ife;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2599990
    iget-object v0, p0, LX/Ife;->d:LX/IfP;

    iget-object v1, p0, LX/Ife;->o:LX/If7;

    invoke-virtual {v0, v1}, LX/IfP;->a(LX/If7;)V

    .line 2599991
    iget-object v0, p0, LX/Ife;->c:LX/Ieu;

    const-string v1, "PEOPLE_TAB"

    invoke-virtual {v0, v1}, LX/Ieu;->a(Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    move-result-object v0

    invoke-static {p0, v0}, LX/Ife;->setData(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;)V

    .line 2599992
    return-void
.end method

.method public static a$redex0(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V
    .locals 7

    .prologue
    .line 2599946
    if-eqz p2, :cond_0

    .line 2599947
    iget-object v0, p0, LX/Ife;->b:LX/IfT;

    const-string v1, "cymk_notice_accepted"

    invoke-virtual {v0, v1}, LX/IfT;->a(Ljava/lang/String;)V

    .line 2599948
    :cond_0
    iget-object v0, p0, LX/Ife;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IfW;

    const-string v1, "PEOPLE_TAB"

    .line 2599949
    new-instance v2, LX/CLD;

    invoke-direct {v2}, LX/CLD;-><init>()V

    move-object v2, v2

    .line 2599950
    new-instance v3, LX/4Dz;

    invoke-direct {v3}, LX/4Dz;-><init>()V

    iget-object v4, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599951
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2599952
    const-string v5, "suggestion_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2599953
    move-object v3, v3

    .line 2599954
    const-string v4, "suggestion_surface"

    invoke-virtual {v3, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2599955
    move-object v3, v3

    .line 2599956
    const-string v4, "input"

    invoke-virtual {v2, v4, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    const-string v4, "small_img_size"

    invoke-static {}, LX/0wB;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "big_img_size"

    invoke-static {}, LX/0wB;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "huge_img_size"

    invoke-static {}, LX/0wB;->d()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "is_for_messenger"

    sget-object v5, LX/01T;->MESSENGER:LX/01T;

    iget-object v6, v0, LX/IfW;->g:LX/00H;

    .line 2599957
    iget-object p2, v6, LX/00H;->j:LX/01T;

    move-object v6, p2

    .line 2599958
    invoke-virtual {v5, v6}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2599959
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    invoke-static {p1}, LX/IfW;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)LX/0jT;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v2

    .line 2599960
    const/4 v3, 0x1

    .line 2599961
    iput-boolean v3, v2, LX/399;->d:Z

    .line 2599962
    iget-object v3, v0, LX/IfW;->b:LX/0tX;

    sget-object v4, LX/3Fz;->a:LX/3Fz;

    invoke-virtual {v3, v2, v4}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2599963
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 2599964
    new-instance v4, LX/IfU;

    invoke-direct {v4, v0, v3, v1}, LX/IfU;-><init>(LX/IfW;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V

    iget-object v5, v0, LX/IfW;->d:Ljava/util/concurrent/Executor;

    invoke-static {v2, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2599965
    move-object v0, v3

    .line 2599966
    new-instance v1, LX/Ifc;

    invoke-direct {v1, p0, p1}, LX/Ifc;-><init>(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    iget-object v2, p0, LX/Ife;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2599967
    iget-object v0, p0, LX/Ife;->c:LX/Ieu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Ieu;->a(Z)V

    .line 2599968
    return-void
.end method

.method public static b(LX/Ife;)V
    .locals 8

    .prologue
    .line 2599910
    iget v0, p0, LX/Ife;->k:I

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Ife;->m:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    if-nez v0, :cond_1

    .line 2599911
    :cond_0
    :goto_0
    return-void

    .line 2599912
    :cond_1
    iget-object v0, p0, LX/Ife;->j:LX/1P0;

    invoke-virtual {v0}, LX/1P1;->m()I

    move-result v0

    .line 2599913
    iget-object v1, p0, LX/Ife;->j:LX/1P0;

    invoke-virtual {v1}, LX/1P1;->o()I

    move-result v3

    .line 2599914
    if-ltz v0, :cond_0

    iget-object v1, p0, LX/Ife;->m:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    iget-object v1, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    if-gt v0, v3, :cond_0

    .line 2599915
    const/4 v1, 0x0

    move v2, v0

    .line 2599916
    :goto_1
    if-gt v2, v3, :cond_4

    .line 2599917
    iget-object v0, p0, LX/Ife;->m:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    iget-object v0, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599918
    iget-object v4, p0, LX/Ife;->l:Ljava/util/Set;

    iget-object v5, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599919
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2599920
    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2599921
    if-nez v1, :cond_2

    .line 2599922
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2599923
    :cond_2
    new-instance v4, LX/IfS;

    invoke-direct {v4, v0, v2}, LX/IfS;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;I)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2599924
    iget-object v4, p0, LX/Ife;->l:Ljava/util/Set;

    iget-object v0, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599925
    iget-object v5, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v5

    .line 2599926
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2599927
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2599928
    :cond_4
    if-eqz v1, :cond_0

    .line 2599929
    iget-object v0, p0, LX/Ife;->b:LX/IfT;

    const-string v2, "PEOPLE_TAB"

    .line 2599930
    new-instance v4, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v3}, LX/162;-><init>(LX/0mC;)V

    .line 2599931
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IfS;

    .line 2599932
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 2599933
    const-string v7, "id"

    iget-object p0, v3, LX/IfS;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    iget-object p0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599934
    iget-object v1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p0, v1

    .line 2599935
    invoke-virtual {v6, v7, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2599936
    const-string v7, "type"

    const-string p0, "top"

    invoke-virtual {v6, v7, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2599937
    const-string v7, "surface"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2599938
    const-string v7, "pos"

    iget v3, v3, LX/IfS;->b:I

    invoke-virtual {v6, v7, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2599939
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_2

    .line 2599940
    :cond_5
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "cymk_view_impression"

    invoke-direct {v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "contacts_you_may_know_people"

    .line 2599941
    iput-object v5, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2599942
    move-object v3, v3

    .line 2599943
    const-string v5, "impression_units"

    invoke-virtual {v3, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2599944
    iget-object v4, v0, LX/IfT;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2599945
    goto/16 :goto_0
.end method

.method public static b$redex0(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V
    .locals 2

    .prologue
    .line 2599906
    if-eqz p2, :cond_0

    .line 2599907
    iget-object v0, p0, LX/Ife;->b:LX/IfT;

    const-string v1, "cymk_notice_declined"

    invoke-virtual {v0, v1}, LX/IfT;->a(Ljava/lang/String;)V

    .line 2599908
    :cond_0
    iget-object v0, p0, LX/Ife;->a:LX/Ies;

    invoke-virtual {v0, p1}, LX/Ies;->c(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599909
    return-void
.end method

.method public static setData(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;)V
    .locals 2

    .prologue
    .line 2599896
    iput-object p1, p0, LX/Ife;->m:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    .line 2599897
    iget-object v0, p0, LX/Ife;->a:LX/Ies;

    .line 2599898
    iget-object v1, v0, LX/Ies;->b:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    if-ne v1, p1, :cond_0

    .line 2599899
    :goto_0
    iget-object v0, p0, LX/Ife;->b:LX/IfT;

    const-string v1, "PEOPLE_TAB"

    invoke-virtual {v0, v1, p1}, LX/IfT;->a(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;)V

    .line 2599900
    return-void

    .line 2599901
    :cond_0
    iput-object p1, v0, LX/Ies;->b:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    .line 2599902
    iget-object v1, v0, LX/Ies;->b:Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    iget-object v1, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    iput-object v1, v0, LX/Ies;->c:LX/0Px;

    .line 2599903
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, v0, LX/Ies;->e:Ljava/util/Set;

    .line 2599904
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, v0, LX/Ies;->f:Ljava/util/Set;

    .line 2599905
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 2599893
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2599894
    invoke-static {p0}, LX/Ife;->b(LX/Ife;)V

    .line 2599895
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x560d6398

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2599890
    iget-object v1, p0, LX/Ife;->d:LX/IfP;

    iget-object v2, p0, LX/Ife;->o:LX/If7;

    invoke-virtual {v1, v2}, LX/IfP;->b(LX/If7;)V

    .line 2599891
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2599892
    const/16 v1, 0x2d

    const v2, 0x38af6948

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
