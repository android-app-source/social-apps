.class public LX/Iuh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final l:Ljava/lang/Object;


# instance fields
.field private final a:[LX/Iua;

.field private final b:LX/3QU;

.field private final c:LX/290;

.field private final d:LX/Iuc;

.field private final e:LX/Iul;

.field private final f:LX/2Ly;

.field private final g:LX/2a9;

.field private final h:LX/IuY;

.field public final i:LX/Iui;

.field private final j:LX/Di5;

.field private final k:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2627139
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Iuh;->l:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3QU;LX/Iul;LX/Iub;LX/Ium;LX/Iuc;LX/Iud;LX/Iuj;LX/290;LX/2Ly;LX/2a9;LX/Iui;LX/Di5;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2627140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2627141
    new-instance v0, LX/IuY;

    invoke-direct {v0}, LX/IuY;-><init>()V

    iput-object v0, p0, LX/Iuh;->h:LX/IuY;

    .line 2627142
    iput-object p1, p0, LX/Iuh;->b:LX/3QU;

    .line 2627143
    const/4 v0, 0x6

    new-array v0, v0, [LX/Iua;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const/4 v1, 0x2

    aput-object p4, v0, v1

    const/4 v1, 0x3

    aput-object p5, v0, v1

    const/4 v1, 0x4

    aput-object p6, v0, v1

    const/4 v1, 0x5

    aput-object p7, v0, v1

    iput-object v0, p0, LX/Iuh;->a:[LX/Iua;

    .line 2627144
    iput-object p5, p0, LX/Iuh;->d:LX/Iuc;

    .line 2627145
    iput-object p2, p0, LX/Iuh;->e:LX/Iul;

    .line 2627146
    iput-object p8, p0, LX/Iuh;->c:LX/290;

    .line 2627147
    iput-object p9, p0, LX/Iuh;->f:LX/2Ly;

    .line 2627148
    iput-object p10, p0, LX/Iuh;->g:LX/2a9;

    .line 2627149
    iput-object p11, p0, LX/Iuh;->i:LX/Iui;

    .line 2627150
    iput-object p12, p0, LX/Iuh;->j:LX/Di5;

    .line 2627151
    iput-object p13, p0, LX/Iuh;->k:LX/0Uh;

    .line 2627152
    return-void
.end method

.method public static a(LX/0QB;)LX/Iuh;
    .locals 7

    .prologue
    .line 2627044
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2627045
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2627046
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2627047
    if-nez v1, :cond_0

    .line 2627048
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2627049
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2627050
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2627051
    sget-object v1, LX/Iuh;->l:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2627052
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2627053
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2627054
    :cond_1
    if-nez v1, :cond_4

    .line 2627055
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2627056
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2627057
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Iuh;->b(LX/0QB;)LX/Iuh;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2627058
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2627059
    if-nez v1, :cond_2

    .line 2627060
    sget-object v0, LX/Iuh;->l:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iuh;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2627061
    :goto_1
    if-eqz v0, :cond_3

    .line 2627062
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2627063
    :goto_3
    check-cast v0, LX/Iuh;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2627064
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2627065
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2627066
    :catchall_1
    move-exception v0

    .line 2627067
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2627068
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2627069
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2627070
    :cond_2
    :try_start_8
    sget-object v0, LX/Iuh;->l:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iuh;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/notify/NewMessageNotification;Lcom/facebook/messaging/service/model/NewMessageResult;LX/Iuf;Lorg/json/JSONObject;)V
    .locals 14

    .prologue
    .line 2627124
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2627125
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v5, -0x1

    .line 2627126
    :goto_0
    iget-object v0, p0, LX/Iuh;->f:LX/2Ly;

    invoke-virtual {v0, v1}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v6

    .line 2627127
    const/4 v7, -0x1

    .line 2627128
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2627129
    if-eqz v0, :cond_0

    .line 2627130
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v7, v0

    .line 2627131
    :cond_0
    invoke-static {v1}, LX/2gS;->a(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v2

    .line 2627132
    iget-object v0, p0, LX/Iuh;->d:LX/Iuc;

    invoke-virtual {v0}, LX/Iuc;->c()J

    move-result-wide v8

    .line 2627133
    iget-object v0, p0, LX/Iuh;->e:LX/Iul;

    invoke-virtual {v0}, LX/Iul;->b()J

    move-result-wide v10

    .line 2627134
    invoke-static/range {p2 .. p2}, LX/2gS;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)J

    move-result-wide v12

    .line 2627135
    iget-object v0, p0, LX/Iuh;->c:LX/290;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual/range {p4 .. p4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, LX/Iuf;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v13}, LX/290;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJJ)V

    .line 2627136
    return-void

    .line 2627137
    :cond_1
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    goto :goto_0

    .line 2627138
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/Iuh;
    .locals 14

    .prologue
    .line 2627153
    new-instance v0, LX/Iuh;

    invoke-static {p0}, LX/3QU;->b(LX/0QB;)LX/3QU;

    move-result-object v1

    check-cast v1, LX/3QU;

    invoke-static {p0}, LX/Iul;->a(LX/0QB;)LX/Iul;

    move-result-object v2

    check-cast v2, LX/Iul;

    invoke-static {p0}, LX/Iub;->a(LX/0QB;)LX/Iub;

    move-result-object v3

    check-cast v3, LX/Iub;

    .line 2627154
    new-instance v4, LX/Ium;

    invoke-direct {v4}, LX/Ium;-><init>()V

    .line 2627155
    move-object v4, v4

    .line 2627156
    move-object v4, v4

    .line 2627157
    check-cast v4, LX/Ium;

    invoke-static {p0}, LX/Iuc;->a(LX/0QB;)LX/Iuc;

    move-result-object v5

    check-cast v5, LX/Iuc;

    .line 2627158
    new-instance v9, LX/Iud;

    invoke-direct {v9}, LX/Iud;-><init>()V

    .line 2627159
    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/IiA;->a(LX/0QB;)LX/IiA;

    move-result-object v7

    check-cast v7, LX/IiA;

    invoke-static {p0}, LX/297;->b(LX/0QB;)LX/297;

    move-result-object v8

    check-cast v8, LX/297;

    .line 2627160
    iput-object v6, v9, LX/Iud;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v7, v9, LX/Iud;->b:LX/IiA;

    iput-object v8, v9, LX/Iud;->c:LX/297;

    .line 2627161
    move-object v6, v9

    .line 2627162
    check-cast v6, LX/Iud;

    .line 2627163
    new-instance v8, LX/Iuj;

    invoke-static {p0}, LX/297;->b(LX/0QB;)LX/297;

    move-result-object v7

    check-cast v7, LX/297;

    invoke-direct {v8, v7}, LX/Iuj;-><init>(LX/297;)V

    .line 2627164
    move-object v7, v8

    .line 2627165
    check-cast v7, LX/Iuj;

    invoke-static {p0}, LX/290;->b(LX/0QB;)LX/290;

    move-result-object v8

    check-cast v8, LX/290;

    invoke-static {p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v9

    check-cast v9, LX/2Ly;

    invoke-static {p0}, LX/2a9;->a(LX/0QB;)LX/2a9;

    move-result-object v10

    check-cast v10, LX/2a9;

    invoke-static {p0}, LX/Iui;->a(LX/0QB;)LX/Iui;

    move-result-object v11

    check-cast v11, LX/Iui;

    invoke-static {p0}, LX/Di5;->a(LX/0QB;)LX/Di5;

    move-result-object v12

    check-cast v12, LX/Di5;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-direct/range {v0 .. v13}, LX/Iuh;-><init>(LX/3QU;LX/Iul;LX/Iub;LX/Ium;LX/Iuc;LX/Iud;LX/Iuj;LX/290;LX/2Ly;LX/2a9;LX/Iui;LX/Di5;LX/0Uh;)V

    .line 2627166
    return-object v0
.end method

.method private b(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/notify/NewMessageNotification;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2627093
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v0

    .line 2627094
    iget-object v0, p0, LX/Iuh;->h:LX/IuY;

    invoke-virtual {v0, v1}, LX/IuY;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2627095
    const/4 v0, 0x0

    .line 2627096
    :goto_0
    return-object v0

    .line 2627097
    :cond_0
    sget-object v0, LX/Iuf;->UNSET:LX/Iuf;

    .line 2627098
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 2627099
    iget-object v9, p0, LX/Iuh;->a:[LX/Iua;

    array-length v10, v9

    move v4, v3

    move v6, v3

    move-object v7, v0

    :goto_1
    if-ge v4, v10, :cond_2

    aget-object v11, v9, v4

    .line 2627100
    invoke-interface {v11, p1}, LX/Iua;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)LX/Iuf;

    move-result-object v12

    .line 2627101
    instance-of v0, v11, LX/Iud;

    if-eqz v0, :cond_1

    sget-object v0, LX/Iuf;->FORCE_BUZZ:LX/Iuf;

    invoke-virtual {v0, v12}, LX/Iuf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_2
    or-int/2addr v6, v0

    .line 2627102
    iget v0, v7, LX/Iuf;->priority:I

    iget v5, v12, LX/Iuf;->priority:I

    if-le v0, v5, :cond_3

    :goto_3
    move-object v5, v7

    .line 2627103
    :try_start_0
    invoke-interface {v11}, LX/Iua;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12}, LX/Iuf;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2627104
    :goto_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-object v7, v5

    goto :goto_1

    :cond_1
    move v0, v3

    .line 2627105
    goto :goto_2

    .line 2627106
    :cond_2
    new-instance v5, LX/Dnh;

    invoke-direct {v5}, LX/Dnh;-><init>()V

    .line 2627107
    sget-object v0, LX/Iug;->a:[I

    invoke-virtual {v7}, LX/Iuf;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 2627108
    iput-boolean v3, v5, LX/Dnh;->d:Z

    .line 2627109
    :goto_5
    iget-object v0, p0, LX/Iuh;->g:LX/2a9;

    invoke-virtual {v0, v1}, LX/2a9;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2627110
    iget-object v0, p0, LX/Iuh;->b:LX/3QU;

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2627111
    iget-object v3, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v3, v3

    .line 2627112
    if-nez v3, :cond_4

    .line 2627113
    sget-object v3, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2627114
    :goto_6
    move-object v3, v3

    .line 2627115
    new-instance v4, Lcom/facebook/push/PushProperty;

    sget-object v9, LX/3B4;->MQTT:LX/3B4;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "zp"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-wide/16 v12, 0x0

    invoke-direct {v4, v9, v10, v12, v13}, Lcom/facebook/push/PushProperty;-><init>(LX/3B4;Ljava/lang/String;J)V

    invoke-virtual {v5}, LX/Dnh;->a()Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, LX/3QU;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadCustomization;Lcom/facebook/push/PushProperty;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Z)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v0

    .line 2627116
    invoke-direct {p0, v0, p1, v7, v8}, LX/Iuh;->a(Lcom/facebook/messaging/notify/NewMessageNotification;Lcom/facebook/messaging/service/model/NewMessageResult;LX/Iuf;Lorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 2627117
    :pswitch_0
    iput-boolean v2, v5, LX/Dnh;->d:Z

    .line 2627118
    goto :goto_5

    .line 2627119
    :pswitch_1
    iput-boolean v2, v5, LX/Dnh;->d:Z

    .line 2627120
    iput-boolean v2, v5, LX/Dnh;->c:Z

    .line 2627121
    iput-boolean v2, v5, LX/Dnh;->a:Z

    .line 2627122
    iput-boolean v2, v5, LX/Dnh;->b:Z

    .line 2627123
    goto :goto_5

    :catch_0
    goto :goto_4

    :cond_3
    move-object v7, v12

    goto :goto_3

    :cond_4
    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/notify/NewMessageNotification;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2627084
    invoke-direct {p0, p1}, LX/Iuh;->b(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v0

    .line 2627085
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Iuh;->k:LX/0Uh;

    const/16 v2, 0x2e4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2627086
    iget-object v1, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v1

    .line 2627087
    iget-object v2, p0, LX/Iuh;->i:LX/Iui;

    .line 2627088
    iget-object v4, v2, LX/Iui;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v1, v4}, LX/2gS;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2627089
    :cond_0
    :goto_0
    return-object v0

    .line 2627090
    :cond_1
    sget-object v4, LX/2gS;->e:LX/2bA;

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/2bA;

    .line 2627091
    iget-wide v6, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    iget-object v5, v2, LX/Iui;->a:LX/6cy;

    const-wide/16 v8, -0x1

    invoke-virtual {v5, v4, v8, v9}, LX/48u;->a(LX/0To;J)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 2627092
    iget-object v5, v2, LX/Iui;->a:LX/6cy;

    iget-wide v6, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v5, v4, v6, v7}, LX/48u;->b(LX/0To;J)V

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2627081
    iget-object v0, p0, LX/Iuh;->d:LX/Iuc;

    .line 2627082
    iget-object v1, v0, LX/Iuc;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object p0, LX/2gS;->b:LX/0Tn;

    invoke-interface {v1, p0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2627083
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 8

    .prologue
    .line 2627071
    iget-object v0, p0, LX/Iuh;->k:LX/0Uh;

    const/16 v1, 0x2e4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2627072
    :cond_0
    return-void

    .line 2627073
    :cond_1
    iget-object v5, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2627074
    if-eqz v5, :cond_2

    sget-object v0, LX/6ek;->INBOX:LX/6ek;

    iget-object v1, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-virtual {v0, v1}, LX/6ek;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2627075
    :cond_2
    iget-object v0, p0, LX/Iuh;->i:LX/Iui;

    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0, v1}, LX/Iui;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Ljava/util/List;

    move-result-object v0

    .line 2627076
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/messages/Message;

    .line 2627077
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2627078
    invoke-direct {p0, v1}, LX/Iuh;->b(Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v1

    .line 2627079
    if-eqz v1, :cond_3

    .line 2627080
    iget-object v2, p0, LX/Iuh;->j:LX/Di5;

    invoke-virtual {v2, v1}, LX/Di5;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto :goto_0
.end method
