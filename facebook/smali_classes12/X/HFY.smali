.class public final LX/HFY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "LX/HF0;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HFc;


# direct methods
.method public constructor <init>(LX/HFc;)V
    .locals 0

    .prologue
    .line 2443867
    iput-object p1, p0, LX/HFY;->a:LX/HFc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2443868
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2443869
    if-eqz p1, :cond_0

    .line 2443870
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2443871
    if-eqz v0, :cond_0

    .line 2443872
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2443873
    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2443874
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2443875
    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2443876
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2443877
    :goto_0
    return-object v0

    .line 2443878
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2443879
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2443880
    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;->a()LX/0Px;

    move-result-object v4

    .line 2443881
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;

    .line 2443882
    if-eqz v0, :cond_2

    .line 2443883
    new-instance v6, LX/HF0;

    invoke-direct {v6}, LX/HF0;-><init>()V

    .line 2443884
    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v7

    .line 2443885
    iput-object v7, v6, LX/HF0;->a:Ljava/lang/String;

    .line 2443886
    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;->j()Ljava/lang/String;

    move-result-object v7

    .line 2443887
    iput-object v7, v6, LX/HF0;->b:Ljava/lang/String;

    .line 2443888
    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;->k()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v7, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2443889
    iput-object v0, v6, LX/HF0;->c:Ljava/lang/String;

    .line 2443890
    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2443891
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2443892
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
