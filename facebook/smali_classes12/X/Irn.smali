.class public final LX/Irn;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V
    .locals 0

    .prologue
    .line 2618627
    iput-object p1, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)LX/Iqg;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2618628
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 2618629
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v2, v0

    .line 2618630
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 2618631
    iget-object v3, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v3, v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2618632
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 2618633
    iget-object v3, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a$redex0(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;III)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2618634
    iget-object v1, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v1, v0}, LX/Ird;->a(I)LX/Iqg;

    move-result-object v0

    .line 2618635
    :goto_1
    return-object v0

    .line 2618636
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2618637
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2618638
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618639
    iget-object v1, v0, LX/Ird;->c:LX/Iqg;

    move-object v0, v1

    .line 2618640
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/Iqg;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2618641
    iget-object v1, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    invoke-virtual {v1}, LX/Iqk;->g()V

    .line 2618642
    iget-object v1, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/IrL;->b(Z)V

    .line 2618643
    iget-object v1, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v1, v0}, LX/Ird;->c(LX/Iqg;)V

    .line 2618644
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->requestFocus()Z

    .line 2618645
    return-void

    .line 2618646
    :cond_1
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    if-eqz v0, :cond_0

    .line 2618647
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    invoke-virtual {v0}, LX/Iqk;->g()V

    goto :goto_0
.end method


# virtual methods
.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2618648
    invoke-direct {p0, p1}, LX/Irn;->a(Landroid/view/MotionEvent;)LX/Iqg;

    move-result-object v2

    .line 2618649
    if-eqz v2, :cond_1

    .line 2618650
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    .line 2618651
    iput-boolean v1, v0, LX/Iqk;->k:Z

    .line 2618652
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    if-eqz v0, :cond_0

    .line 2618653
    instance-of p0, v2, LX/IsC;

    if-nez p0, :cond_0

    .line 2618654
    iget-boolean p0, v2, LX/Iqg;->f:Z

    move p0, p0

    .line 2618655
    if-nez p0, :cond_2

    const/4 p0, 0x1

    :goto_0
    invoke-virtual {v2, p0}, LX/Iqg;->a(Z)V

    .line 2618656
    :cond_0
    move v0, v1

    .line 2618657
    :goto_1
    return v0

    .line 2618658
    :cond_1
    invoke-direct {p0}, LX/Irn;->a()V

    .line 2618659
    const/4 v0, 0x0

    goto :goto_1

    .line 2618660
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2618661
    const/4 v0, 0x0

    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v1, 0x1

    .line 2618662
    iget-object v2, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v2, v2, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618663
    iget-object v3, v2, LX/Ird;->c:LX/Iqg;

    move-object v2, v3

    .line 2618664
    iget-object v3, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v3}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2618665
    :cond_0
    :goto_0
    return v0

    .line 2618666
    :cond_1
    iget-object v3, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-boolean v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->v:Z

    if-nez v3, :cond_2

    .line 2618667
    iget-object v2, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v2, v3, v4}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;II)LX/Iqg;

    move-result-object v2

    .line 2618668
    :cond_2
    if-eqz v2, :cond_0

    .line 2618669
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    .line 2618670
    iput-boolean v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->v:Z

    .line 2618671
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    if-eqz v0, :cond_3

    .line 2618672
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    iget-object v3, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-boolean v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->v:Z

    invoke-virtual {v0, v3}, LX/IrL;->a(Z)V

    .line 2618673
    :cond_3
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->o:LX/IrM;

    if-eqz v0, :cond_4

    .line 2618674
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    iget-object v3, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->o:LX/IrM;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    .line 2618675
    iget-object v7, v3, LX/IrM;->a:LX/IrP;

    iget-object v7, v7, LX/IrP;->m:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_5

    .line 2618676
    const/4 v7, 0x0

    .line 2618677
    :goto_1
    move v3, v7

    .line 2618678
    iput-boolean v3, v0, LX/Iqk;->h:Z

    .line 2618679
    iget-object v5, v0, LX/Iqk;->c:Landroid/view/View;

    if-eqz v3, :cond_6

    iget-object v4, v0, LX/Iqk;->b:LX/Iqg;

    .line 2618680
    iget v7, v4, LX/Iqg;->g:F

    move v4, v7

    .line 2618681
    const/high16 v7, 0x40400000    # 3.0f

    div-float/2addr v4, v7

    :goto_2
    invoke-virtual {v5, v4}, Landroid/view/View;->setAlpha(F)V

    .line 2618682
    :cond_4
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    .line 2618683
    iput-boolean v1, v0, LX/Iqk;->k:Z

    .line 2618684
    iget v0, v2, LX/Iqg;->b:F

    move v0, v0

    .line 2618685
    iget v3, v2, LX/Iqg;->c:F

    move v3, v3

    .line 2618686
    iget-object v4, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v4, v4, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v4}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v6

    .line 2618687
    iget-object v5, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v5, v5, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v5}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    .line 2618688
    sub-float/2addr v0, p3

    .line 2618689
    sub-float/2addr v3, p4

    .line 2618690
    neg-float v6, v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2618691
    neg-float v4, v5

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 2618692
    invoke-virtual {v2, v0, v3}, LX/Iqg;->a(FF)V

    move v0, v1

    .line 2618693
    goto/16 :goto_0

    .line 2618694
    :cond_5
    iget-object v7, v3, LX/IrM;->a:LX/IrP;

    iget-object v7, v7, LX/IrP;->m:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getX()F

    move-result v7

    float-to-int v7, v7

    .line 2618695
    iget-object v8, v3, LX/IrM;->a:LX/IrP;

    iget-object v8, v8, LX/IrP;->m:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v8

    float-to-int v8, v8

    .line 2618696
    iget-object v9, v3, LX/IrM;->b:Landroid/graphics/Rect;

    iget-object p1, v3, LX/IrM;->a:LX/IrP;

    iget-object p1, p1, LX/IrP;->m:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    add-int/2addr p1, v7

    iget-object p2, v3, LX/IrM;->a:LX/IrP;

    iget-object p2, p2, LX/IrP;->m:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p2

    add-int/2addr p2, v8

    invoke-virtual {v9, v7, v8, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 2618697
    iget-object v7, v3, LX/IrM;->b:Landroid/graphics/Rect;

    invoke-virtual {v7, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    goto/16 :goto_1

    .line 2618698
    :cond_6
    iget-object v4, v0, LX/Iqk;->b:LX/Iqg;

    .line 2618699
    iget v7, v4, LX/Iqg;->g:F

    move v4, v7

    .line 2618700
    goto :goto_2
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2618701
    iget-object v2, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-boolean v2, v2, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->w:Z

    if-eqz v2, :cond_0

    .line 2618702
    :goto_0
    return v0

    .line 2618703
    :cond_0
    invoke-direct {p0, p1}, LX/Irn;->a(Landroid/view/MotionEvent;)LX/Iqg;

    move-result-object v2

    .line 2618704
    if-eqz v2, :cond_2

    .line 2618705
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618706
    iget-object p1, v0, LX/Ird;->c:LX/Iqg;

    move-object v0, p1

    .line 2618707
    if-eq v2, v0, :cond_1

    .line 2618708
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v0, v2}, LX/Ird;->b(LX/Iqg;)V

    :goto_1
    move v0, v1

    .line 2618709
    goto :goto_0

    .line 2618710
    :cond_1
    iget-object v0, p0, LX/Irn;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    .line 2618711
    invoke-virtual {v0}, LX/Iqk;->onClick()V

    .line 2618712
    iput-boolean v1, v0, LX/Iqk;->k:Z

    .line 2618713
    goto :goto_1

    .line 2618714
    :cond_2
    invoke-direct {p0}, LX/Irn;->a()V

    goto :goto_0
.end method
