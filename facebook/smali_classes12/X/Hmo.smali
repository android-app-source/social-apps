.class public LX/Hmo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0TD;

.field public final b:Landroid/content/Context;

.field public c:LX/1Ml;

.field public d:Z

.field public e:Ljava/net/ServerSocket;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Ml;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2501110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501111
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Hmo;->d:Z

    .line 2501112
    iput-object p1, p0, LX/Hmo;->b:Landroid/content/Context;

    .line 2501113
    iput-object p2, p0, LX/Hmo;->c:LX/1Ml;

    .line 2501114
    invoke-static {p3}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v0

    iput-object v0, p0, LX/Hmo;->a:LX/0TD;

    .line 2501115
    return-void
.end method

.method public static b(LX/Hmo;)V
    .locals 1

    .prologue
    .line 2501116
    :try_start_0
    iget-object v0, p0, LX/Hmo;->e:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->close()V

    .line 2501117
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hmo;->e:Ljava/net/ServerSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2501118
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
