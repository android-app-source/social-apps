.class public final enum LX/Hmd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hmd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hmd;

.field public static final enum APK_RECEIVING:LX/Hmd;

.field public static final enum APK_VERIFYING:LX/Hmd;

.field public static final enum CONFIRM_SCREEN_CLICKED:LX/Hmd;

.field public static final enum CONFIRM_SCREEN_OPENED:LX/Hmd;

.field public static final enum CONNECT_INPUT_INVALID:LX/Hmd;

.field public static final enum CONNECT_INPUT_SCREEN_CLICKED:LX/Hmd;

.field public static final enum CONNECT_INPUT_SCREEN_OPENED:LX/Hmd;

.field public static final enum CONNECT_INPUT_VALID:LX/Hmd;

.field public static final enum ERROR_SCREEN_OPENED:LX/Hmd;

.field public static final enum GOOGLE_PLAY_ERROR_OPENED:LX/Hmd;

.field public static final enum INCOMPATIBLE_VERSION_SCREEN_OPENED:LX/Hmd;

.field public static final enum INITIALIZING:LX/Hmd;

.field public static final enum INSTALL_SCREEN_CLICKED:LX/Hmd;

.field public static final enum INSTALL_SCREEN_OPENED:LX/Hmd;

.field public static final enum INSTALL_SUCCEEDED:LX/Hmd;

.field public static final enum INSUFFICIENT_STORAGE_OPENED:LX/Hmd;

.field public static final enum NOTHING_TO_SEND_SCREEN_OPENED:LX/Hmd;

.field public static final enum PREFLIGHT_INFO_SENDING:LX/Hmd;

.field public static final enum RECEIVER_FLOW_BACK_PRESSED:LX/Hmd;

.field public static final enum SENDER_ERROR_SCREEN_OPENED:LX/Hmd;

.field public static final enum SOCKET_CREATE_FAILED:LX/Hmd;

.field public static final enum SOCKET_CREATE_SUCCEEDED:LX/Hmd;

.field public static final enum TIMESTAMP_CHECK_FAILED:LX/Hmd;

.field public static final enum TRY_AGAIN_CLICKED:LX/Hmd;

.field public static final enum WIFI_CHANGE_FAILED:LX/Hmd;

.field public static final enum WIFI_CHANGE_SUCCEEDED:LX/Hmd;


# instance fields
.field public final actionName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2500909
    new-instance v0, LX/Hmd;

    const-string v1, "TIMESTAMP_CHECK_FAILED"

    const-string v2, "timestamp_check_failed"

    invoke-direct {v0, v1, v4, v2}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->TIMESTAMP_CHECK_FAILED:LX/Hmd;

    .line 2500910
    new-instance v0, LX/Hmd;

    const-string v1, "CONFIRM_SCREEN_OPENED"

    const-string v2, "confirm_screen_opened"

    invoke-direct {v0, v1, v5, v2}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->CONFIRM_SCREEN_OPENED:LX/Hmd;

    .line 2500911
    new-instance v0, LX/Hmd;

    const-string v1, "CONFIRM_SCREEN_CLICKED"

    const-string v2, "confirm_screen_clicked"

    invoke-direct {v0, v1, v6, v2}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->CONFIRM_SCREEN_CLICKED:LX/Hmd;

    .line 2500912
    new-instance v0, LX/Hmd;

    const-string v1, "CONNECT_INPUT_SCREEN_OPENED"

    const-string v2, "connect_input_screen_opened"

    invoke-direct {v0, v1, v7, v2}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->CONNECT_INPUT_SCREEN_OPENED:LX/Hmd;

    .line 2500913
    new-instance v0, LX/Hmd;

    const-string v1, "CONNECT_INPUT_SCREEN_CLICKED"

    const-string v2, "connect_input_screen_clicked"

    invoke-direct {v0, v1, v8, v2}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->CONNECT_INPUT_SCREEN_CLICKED:LX/Hmd;

    .line 2500914
    new-instance v0, LX/Hmd;

    const-string v1, "CONNECT_INPUT_INVALID"

    const/4 v2, 0x5

    const-string v3, "connect_input_invalid"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->CONNECT_INPUT_INVALID:LX/Hmd;

    .line 2500915
    new-instance v0, LX/Hmd;

    const-string v1, "CONNECT_INPUT_VALID"

    const/4 v2, 0x6

    const-string v3, "connect_input_valid"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->CONNECT_INPUT_VALID:LX/Hmd;

    .line 2500916
    new-instance v0, LX/Hmd;

    const-string v1, "WIFI_CHANGE_SUCCEEDED"

    const/4 v2, 0x7

    const-string v3, "wifi_connect_succeeded"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->WIFI_CHANGE_SUCCEEDED:LX/Hmd;

    .line 2500917
    new-instance v0, LX/Hmd;

    const-string v1, "WIFI_CHANGE_FAILED"

    const/16 v2, 0x8

    const-string v3, "wifi_connect_failed"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->WIFI_CHANGE_FAILED:LX/Hmd;

    .line 2500918
    new-instance v0, LX/Hmd;

    const-string v1, "SOCKET_CREATE_SUCCEEDED"

    const/16 v2, 0x9

    const-string v3, "socket_create_succeeded"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->SOCKET_CREATE_SUCCEEDED:LX/Hmd;

    .line 2500919
    new-instance v0, LX/Hmd;

    const-string v1, "SOCKET_CREATE_FAILED"

    const/16 v2, 0xa

    const-string v3, "socket_create_failed"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->SOCKET_CREATE_FAILED:LX/Hmd;

    .line 2500920
    new-instance v0, LX/Hmd;

    const-string v1, "INITIALIZING"

    const/16 v2, 0xb

    const-string v3, "initializing"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->INITIALIZING:LX/Hmd;

    .line 2500921
    new-instance v0, LX/Hmd;

    const-string v1, "PREFLIGHT_INFO_SENDING"

    const/16 v2, 0xc

    const-string v3, "preflight_info_sending"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->PREFLIGHT_INFO_SENDING:LX/Hmd;

    .line 2500922
    new-instance v0, LX/Hmd;

    const-string v1, "APK_RECEIVING"

    const/16 v2, 0xd

    const-string v3, "apk_receiving"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->APK_RECEIVING:LX/Hmd;

    .line 2500923
    new-instance v0, LX/Hmd;

    const-string v1, "APK_VERIFYING"

    const/16 v2, 0xe

    const-string v3, "apk_verifying"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->APK_VERIFYING:LX/Hmd;

    .line 2500924
    new-instance v0, LX/Hmd;

    const-string v1, "INSTALL_SCREEN_OPENED"

    const/16 v2, 0xf

    const-string v3, "install_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->INSTALL_SCREEN_OPENED:LX/Hmd;

    .line 2500925
    new-instance v0, LX/Hmd;

    const-string v1, "INSTALL_SCREEN_CLICKED"

    const/16 v2, 0x10

    const-string v3, "install_screen_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->INSTALL_SCREEN_CLICKED:LX/Hmd;

    .line 2500926
    new-instance v0, LX/Hmd;

    const-string v1, "INSTALL_SUCCEEDED"

    const/16 v2, 0x11

    const-string v3, "install_succeeded"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->INSTALL_SUCCEEDED:LX/Hmd;

    .line 2500927
    new-instance v0, LX/Hmd;

    const-string v1, "INCOMPATIBLE_VERSION_SCREEN_OPENED"

    const/16 v2, 0x12

    const-string v3, "incompatible_version_opened"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->INCOMPATIBLE_VERSION_SCREEN_OPENED:LX/Hmd;

    .line 2500928
    new-instance v0, LX/Hmd;

    const-string v1, "NOTHING_TO_SEND_SCREEN_OPENED"

    const/16 v2, 0x13

    const-string v3, "nothing_to_send_opened"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->NOTHING_TO_SEND_SCREEN_OPENED:LX/Hmd;

    .line 2500929
    new-instance v0, LX/Hmd;

    const-string v1, "SENDER_ERROR_SCREEN_OPENED"

    const/16 v2, 0x14

    const-string v3, "sender_error_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->SENDER_ERROR_SCREEN_OPENED:LX/Hmd;

    .line 2500930
    new-instance v0, LX/Hmd;

    const-string v1, "ERROR_SCREEN_OPENED"

    const/16 v2, 0x15

    const-string v3, "error_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->ERROR_SCREEN_OPENED:LX/Hmd;

    .line 2500931
    new-instance v0, LX/Hmd;

    const-string v1, "TRY_AGAIN_CLICKED"

    const/16 v2, 0x16

    const-string v3, "try_again_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->TRY_AGAIN_CLICKED:LX/Hmd;

    .line 2500932
    new-instance v0, LX/Hmd;

    const-string v1, "RECEIVER_FLOW_BACK_PRESSED"

    const/16 v2, 0x17

    const-string v3, "beam_receiver_flow_back_pressed"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->RECEIVER_FLOW_BACK_PRESSED:LX/Hmd;

    .line 2500933
    new-instance v0, LX/Hmd;

    const-string v1, "INSUFFICIENT_STORAGE_OPENED"

    const/16 v2, 0x18

    const-string v3, "insufficient_storage_opened"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->INSUFFICIENT_STORAGE_OPENED:LX/Hmd;

    .line 2500934
    new-instance v0, LX/Hmd;

    const-string v1, "GOOGLE_PLAY_ERROR_OPENED"

    const/16 v2, 0x19

    const-string v3, "google_play_error_opened"

    invoke-direct {v0, v1, v2, v3}, LX/Hmd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmd;->GOOGLE_PLAY_ERROR_OPENED:LX/Hmd;

    .line 2500935
    const/16 v0, 0x1a

    new-array v0, v0, [LX/Hmd;

    sget-object v1, LX/Hmd;->TIMESTAMP_CHECK_FAILED:LX/Hmd;

    aput-object v1, v0, v4

    sget-object v1, LX/Hmd;->CONFIRM_SCREEN_OPENED:LX/Hmd;

    aput-object v1, v0, v5

    sget-object v1, LX/Hmd;->CONFIRM_SCREEN_CLICKED:LX/Hmd;

    aput-object v1, v0, v6

    sget-object v1, LX/Hmd;->CONNECT_INPUT_SCREEN_OPENED:LX/Hmd;

    aput-object v1, v0, v7

    sget-object v1, LX/Hmd;->CONNECT_INPUT_SCREEN_CLICKED:LX/Hmd;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Hmd;->CONNECT_INPUT_INVALID:LX/Hmd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Hmd;->CONNECT_INPUT_VALID:LX/Hmd;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Hmd;->WIFI_CHANGE_SUCCEEDED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Hmd;->WIFI_CHANGE_FAILED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Hmd;->SOCKET_CREATE_SUCCEEDED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Hmd;->SOCKET_CREATE_FAILED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Hmd;->INITIALIZING:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Hmd;->PREFLIGHT_INFO_SENDING:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Hmd;->APK_RECEIVING:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Hmd;->APK_VERIFYING:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Hmd;->INSTALL_SCREEN_OPENED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Hmd;->INSTALL_SCREEN_CLICKED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Hmd;->INSTALL_SUCCEEDED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Hmd;->INCOMPATIBLE_VERSION_SCREEN_OPENED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/Hmd;->NOTHING_TO_SEND_SCREEN_OPENED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/Hmd;->SENDER_ERROR_SCREEN_OPENED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/Hmd;->ERROR_SCREEN_OPENED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/Hmd;->TRY_AGAIN_CLICKED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/Hmd;->RECEIVER_FLOW_BACK_PRESSED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/Hmd;->INSUFFICIENT_STORAGE_OPENED:LX/Hmd;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/Hmd;->GOOGLE_PLAY_ERROR_OPENED:LX/Hmd;

    aput-object v2, v0, v1

    sput-object v0, LX/Hmd;->$VALUES:[LX/Hmd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2500936
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2500937
    iput-object p3, p0, LX/Hmd;->actionName:Ljava/lang/String;

    .line 2500938
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hmd;
    .locals 1

    .prologue
    .line 2500908
    const-class v0, LX/Hmd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hmd;

    return-object v0
.end method

.method public static values()[LX/Hmd;
    .locals 1

    .prologue
    .line 2500907
    sget-object v0, LX/Hmd;->$VALUES:[LX/Hmd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hmd;

    return-object v0
.end method
