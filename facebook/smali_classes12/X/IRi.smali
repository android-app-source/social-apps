.class public final LX/IRi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 0

    .prologue
    .line 2576675
    iput-object p1, p0, LX/IRi;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x74d3c309

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576676
    const-string v1, "extra_request_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2576677
    if-nez v1, :cond_0

    .line 2576678
    sget-object v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->af:Ljava/lang/String;

    const-string v2, "There is no composer session id"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576679
    const/16 v1, 0x27

    const v2, -0x736c32c9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2576680
    :goto_0
    return-void

    .line 2576681
    :cond_0
    iget-object v2, p0, LX/IRi;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ad:LX/CGV;

    .line 2576682
    iget-object v3, v2, LX/CGV;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2576683
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2576684
    iget-object v1, p0, LX/IRi;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->mJ_()V

    .line 2576685
    :cond_1
    const v1, -0x62ef6f83    # -1.913294E-21f

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
