.class public final enum LX/IX1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IX1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IX1;

.field public static final enum ADD_MEMBERS:LX/IX1;

.field public static final enum CHANNELS:LX/IX1;

.field public static final enum CREATE_GROUP:LX/IX1;

.field public static final enum CREATE_GROUP_CHAT:LX/IX1;

.field public static final enum CREATE_GROUP_EVENT:LX/IX1;

.field public static final enum CREATE_SHORTCUT:LX/IX1;

.field public static final enum CREATE_SUBGROUP:LX/IX1;

.field public static final enum EDIT_NOTIFICATION_SETTING:LX/IX1;

.field public static final enum FAVORITE_GROUP:LX/IX1;

.field public static final enum INFO:LX/IX1;

.field public static final enum INFO_ICON:LX/IX1;

.field public static final enum JOIN:LX/IX1;

.field public static final enum JOINED:LX/IX1;

.field public static final enum REPORT_GROUP:LX/IX1;

.field public static final enum REQUESTED:LX/IX1;

.field public static final enum SEARCH:LX/IX1;

.field public static final enum SHARE:LX/IX1;

.field public static final enum SHARE_MAYBE:LX/IX1;

.field public static final enum UNFAVORITE_GROUP:LX/IX1;

.field public static final enum VIEW_PHOTOS:LX/IX1;


# instance fields
.field public final checkable:Z

.field public final checked:Z

.field public final enabled:Z

.field public final iconId:I

.field public final iconMediumResId:I

.field public final labelResId:I

.field public final showAsAction:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 2585410
    new-instance v0, LX/IX1;

    const-string v1, "CREATE_GROUP"

    const/4 v2, 0x0

    const v3, 0x7f081be8

    const v4, 0x7f020973

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0108

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->CREATE_GROUP:LX/IX1;

    .line 2585411
    new-instance v0, LX/IX1;

    const-string v1, "CREATE_SUBGROUP"

    const/4 v2, 0x1

    const v3, 0x7f081be9

    const v4, 0x7f020970

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    const v9, 0x7f0d0109

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->CREATE_SUBGROUP:LX/IX1;

    .line 2585412
    new-instance v0, LX/IX1;

    const-string v1, "JOIN"

    const/4 v2, 0x2

    const v3, 0x7f081be3

    const v4, 0x7f020c49

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    const v9, 0x7f0d010a

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->JOIN:LX/IX1;

    .line 2585413
    new-instance v0, LX/IX1;

    const-string v1, "REQUESTED"

    const/4 v2, 0x3

    const v3, 0x7f081be4

    const v4, 0x7f020c49

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    const v9, 0x7f0d010b

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->REQUESTED:LX/IX1;

    .line 2585414
    new-instance v0, LX/IX1;

    const-string v1, "JOINED"

    const/4 v2, 0x4

    const v3, 0x7f081be5

    const v4, 0x7f020c49

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    const v9, 0x7f0d010c

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->JOINED:LX/IX1;

    .line 2585415
    new-instance v0, LX/IX1;

    const-string v1, "INFO"

    const/4 v2, 0x5

    const v3, 0x7f081bea

    const v4, 0x7f0208ed

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d010d

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->INFO:LX/IX1;

    .line 2585416
    new-instance v0, LX/IX1;

    const-string v1, "INFO_ICON"

    const/4 v2, 0x6

    const v3, 0x7f081be7

    const v4, 0x7f0208ed

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0104

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->INFO_ICON:LX/IX1;

    .line 2585417
    new-instance v0, LX/IX1;

    const-string v1, "ADD_MEMBERS"

    const/4 v2, 0x7

    const v3, 0x7f081be6

    const v4, 0x7f020886

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d010e

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->ADD_MEMBERS:LX/IX1;

    .line 2585418
    new-instance v0, LX/IX1;

    const-string v1, "SEARCH"

    const/16 v2, 0x8

    const v3, 0x7f081bed    # 1.8092E38f

    const v4, 0x7f02091b

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d010f

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->SEARCH:LX/IX1;

    .line 2585419
    new-instance v0, LX/IX1;

    const-string v1, "CHANNELS"

    const/16 v2, 0x9

    const v3, 0x7f081b52

    invoke-static {}, LX/10A;->a()I

    move-result v4

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0110

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->CHANNELS:LX/IX1;

    .line 2585420
    new-instance v0, LX/IX1;

    const-string v1, "SHARE"

    const/16 v2, 0xa

    const v3, 0x7f081bee

    const v4, 0x7f0209c5

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0111

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->SHARE:LX/IX1;

    .line 2585421
    new-instance v0, LX/IX1;

    const-string v1, "SHARE_MAYBE"

    const/16 v2, 0xb

    const v3, 0x7f081bee

    const v4, 0x7f0209c5

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0112

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->SHARE_MAYBE:LX/IX1;

    .line 2585422
    new-instance v0, LX/IX1;

    const-string v1, "CREATE_SHORTCUT"

    const/16 v2, 0xc

    const v3, 0x7f081bec

    const v4, 0x7f0209e1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0113

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->CREATE_SHORTCUT:LX/IX1;

    .line 2585423
    new-instance v0, LX/IX1;

    const-string v1, "CREATE_GROUP_CHAT"

    const/16 v2, 0xd

    const v3, 0x7f081b4e

    invoke-static {}, LX/10A;->a()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0114

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->CREATE_GROUP_CHAT:LX/IX1;

    .line 2585424
    new-instance v0, LX/IX1;

    const-string v1, "CREATE_GROUP_EVENT"

    const/16 v2, 0xe

    const v3, 0x7f081b53

    const v4, 0x7f020855

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0115

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->CREATE_GROUP_EVENT:LX/IX1;

    .line 2585425
    new-instance v0, LX/IX1;

    const-string v1, "VIEW_PHOTOS"

    const/16 v2, 0xf

    const v3, 0x7f081beb

    const v4, 0x7f02095e

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0116

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->VIEW_PHOTOS:LX/IX1;

    .line 2585426
    new-instance v0, LX/IX1;

    const-string v1, "EDIT_NOTIFICATION_SETTING"

    const/16 v2, 0x10

    const v3, 0x7f081bf8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0117

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->EDIT_NOTIFICATION_SETTING:LX/IX1;

    .line 2585427
    new-instance v0, LX/IX1;

    const-string v1, "REPORT_GROUP"

    const/16 v2, 0x11

    const v3, 0x7f081bef

    const v4, 0x7f0209ae

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0118

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->REPORT_GROUP:LX/IX1;

    .line 2585428
    new-instance v0, LX/IX1;

    const-string v1, "FAVORITE_GROUP"

    const/16 v2, 0x12

    const v3, 0x7f081bf0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d0119

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->FAVORITE_GROUP:LX/IX1;

    .line 2585429
    new-instance v0, LX/IX1;

    const-string v1, "UNFAVORITE_GROUP"

    const/16 v2, 0x13

    const v3, 0x7f081bf1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7f0d011a

    invoke-direct/range {v0 .. v9}, LX/IX1;-><init>(Ljava/lang/String;IIIIZZZI)V

    sput-object v0, LX/IX1;->UNFAVORITE_GROUP:LX/IX1;

    .line 2585430
    const/16 v0, 0x14

    new-array v0, v0, [LX/IX1;

    const/4 v1, 0x0

    sget-object v2, LX/IX1;->CREATE_GROUP:LX/IX1;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/IX1;->CREATE_SUBGROUP:LX/IX1;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/IX1;->JOIN:LX/IX1;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/IX1;->REQUESTED:LX/IX1;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/IX1;->JOINED:LX/IX1;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/IX1;->INFO:LX/IX1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IX1;->INFO_ICON:LX/IX1;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IX1;->ADD_MEMBERS:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/IX1;->SEARCH:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/IX1;->CHANNELS:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/IX1;->SHARE:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/IX1;->SHARE_MAYBE:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/IX1;->CREATE_SHORTCUT:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/IX1;->CREATE_GROUP_CHAT:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/IX1;->CREATE_GROUP_EVENT:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/IX1;->VIEW_PHOTOS:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/IX1;->EDIT_NOTIFICATION_SETTING:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/IX1;->REPORT_GROUP:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/IX1;->FAVORITE_GROUP:LX/IX1;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/IX1;->UNFAVORITE_GROUP:LX/IX1;

    aput-object v2, v0, v1

    sput-object v0, LX/IX1;->$VALUES:[LX/IX1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIZZZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIZZZI)V"
        }
    .end annotation

    .prologue
    .line 2585431
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2585432
    iput p3, p0, LX/IX1;->labelResId:I

    .line 2585433
    iput p4, p0, LX/IX1;->iconMediumResId:I

    .line 2585434
    iput p5, p0, LX/IX1;->showAsAction:I

    .line 2585435
    iput-boolean p6, p0, LX/IX1;->enabled:Z

    .line 2585436
    iput-boolean p8, p0, LX/IX1;->checked:Z

    .line 2585437
    iput-boolean p7, p0, LX/IX1;->checkable:Z

    .line 2585438
    iput p9, p0, LX/IX1;->iconId:I

    .line 2585439
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IX1;
    .locals 1

    .prologue
    .line 2585440
    const-class v0, LX/IX1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IX1;

    return-object v0
.end method

.method public static values()[LX/IX1;
    .locals 1

    .prologue
    .line 2585441
    sget-object v0, LX/IX1;->$VALUES:[LX/IX1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IX1;

    return-object v0
.end method
