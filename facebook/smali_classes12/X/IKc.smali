.class public final LX/IKc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2567003
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 2567004
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2567005
    :goto_0
    return v1

    .line 2567006
    :cond_0
    const-string v12, "is_viewer_subscribed"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2567007
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    .line 2567008
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_9

    .line 2567009
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2567010
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2567011
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2567012
    const-string v12, "customization_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2567013
    invoke-static {p0, p1}, LX/IKR;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2567014
    :cond_2
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2567015
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2567016
    :cond_3
    const-string v12, "image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2567017
    invoke-static {p0, p1}, LX/IKS;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2567018
    :cond_4
    const-string v12, "joinable_mode"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2567019
    invoke-static {p0, p1}, LX/IKJ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2567020
    :cond_5
    const-string v12, "messages"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2567021
    invoke-static {p0, p1}, LX/IKX;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2567022
    :cond_6
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2567023
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2567024
    :cond_7
    const-string v12, "participants"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 2567025
    invoke-static {p0, p1}, LX/IKb;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2567026
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2567027
    :cond_9
    const/16 v11, 0x8

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2567028
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 2567029
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 2567030
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2567031
    if-eqz v0, :cond_a

    .line 2567032
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 2567033
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2567034
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2567035
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2567036
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2567037
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2567038
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2567039
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2567040
    if-eqz v0, :cond_0

    .line 2567041
    const-string v1, "customization_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567042
    invoke-static {p0, v0, p2}, LX/IKR;->a(LX/15i;ILX/0nX;)V

    .line 2567043
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2567044
    if-eqz v0, :cond_1

    .line 2567045
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567046
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2567047
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2567048
    if-eqz v0, :cond_3

    .line 2567049
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567050
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2567051
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2567052
    if-eqz v1, :cond_2

    .line 2567053
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567054
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2567055
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2567056
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2567057
    if-eqz v0, :cond_4

    .line 2567058
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567059
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2567060
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2567061
    if-eqz v0, :cond_5

    .line 2567062
    const-string v1, "joinable_mode"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567063
    invoke-static {p0, v0, p2}, LX/IKJ;->a(LX/15i;ILX/0nX;)V

    .line 2567064
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2567065
    if-eqz v0, :cond_6

    .line 2567066
    const-string v1, "messages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567067
    invoke-static {p0, v0, p2, p3}, LX/IKX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2567068
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2567069
    if-eqz v0, :cond_7

    .line 2567070
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567071
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2567072
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2567073
    if-eqz v0, :cond_8

    .line 2567074
    const-string v1, "participants"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567075
    invoke-static {p0, v0, p2, p3}, LX/IKb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2567076
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2567077
    return-void
.end method
