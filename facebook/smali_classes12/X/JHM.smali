.class public LX/JHM;
.super LX/7au;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;
.implements LX/5pQ;
.implements LX/6ag;


# instance fields
.field public a:Lcom/google/android/gms/maps/model/LatLngBounds;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field private c:I

.field private final d:Landroid/graphics/Point;

.field private final e:Landroid/graphics/Point;

.field private final f:LX/6an;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2672711
    invoke-direct {p0, p1}, LX/7au;-><init>(Landroid/content/Context;)V

    .line 2672712
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JHM;->b:Z

    .line 2672713
    const/4 v0, 0x2

    iput v0, p0, LX/JHM;->c:I

    .line 2672714
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/JHM;->d:Landroid/graphics/Point;

    .line 2672715
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/JHM;->e:Landroid/graphics/Point;

    .line 2672716
    new-instance v0, LX/JHK;

    invoke-direct {v0, p0}, LX/JHK;-><init>(LX/JHM;)V

    iput-object v0, p0, LX/JHM;->f:LX/6an;

    .line 2672717
    invoke-virtual {p0}, LX/JHM;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2672718
    return-void
.end method

.method public static a(LX/JHM;Lcom/google/android/gms/maps/model/LatLngBounds;II)V
    .locals 1

    .prologue
    .line 2672719
    new-instance v0, LX/JHL;

    invoke-direct {v0, p0, p1, p2, p3}, LX/JHL;-><init>(LX/JHM;Lcom/google/android/gms/maps/model/LatLngBounds;II)V

    invoke-virtual {p0, v0}, LX/7au;->a(LX/6an;)V

    .line 2672720
    return-void
.end method

.method public static a$redex0(LX/JHM;LX/5pX;LX/7an;)V
    .locals 14

    .prologue
    .line 2672721
    invoke-virtual/range {p2 .. p2}, LX/7an;->e()LX/7aw;

    move-result-object v0

    .line 2672722
    iget-object v1, p0, LX/JHM;->d:Landroid/graphics/Point;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 2672723
    iget-object v1, p0, LX/JHM;->d:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, LX/7aw;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    .line 2672724
    iget-object v1, p0, LX/JHM;->e:Landroid/graphics/Point;

    invoke-virtual {p0}, LX/JHM;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/JHM;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 2672725
    iget-object v1, p0, LX/JHM;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, LX/7aw;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v7

    .line 2672726
    iget-wide v0, v7, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v2, v6, Lcom/google/android/gms/maps/model/LatLng;->b:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    .line 2672727
    iget-wide v0, v6, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v0, v1}, Ljava/lang/Math;->signum(D)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, v7, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v0, v1}, Ljava/lang/Math;->signum(D)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 2672728
    const-wide v0, 0x4076800000000000L    # 360.0

    add-double/2addr v8, v0

    .line 2672729
    :cond_0
    const-class v0, LX/5rQ;

    invoke-virtual {p1, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    invoke-virtual {v0}, LX/5rQ;->i()LX/5s9;

    move-result-object v10

    new-instance v0, LX/JHT;

    invoke-virtual {p0}, LX/JHM;->getId()I

    move-result v1

    invoke-virtual/range {p2 .. p2}, LX/7an;->a()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual/range {p2 .. p2}, LX/7an;->a()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v12, v7, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double v6, v12, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-direct/range {v0 .. v9}, LX/JHT;-><init>(IDDDD)V

    invoke-virtual {v10, v0}, LX/5s9;->a(LX/5r0;)V

    .line 2672730
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 1

    .prologue
    .line 2672731
    const/4 v0, 0x1

    .line 2672732
    iput-boolean v0, p0, LX/JHM;->b:Z

    .line 2672733
    iget-object v0, p0, LX/JHM;->f:LX/6an;

    invoke-virtual {p0, v0}, LX/7au;->a(LX/6an;)V

    .line 2672734
    return-void
.end method

.method public final bM_()V
    .locals 0

    .prologue
    .line 2672735
    invoke-virtual {p0}, LX/7au;->b()V

    .line 2672736
    return-void
.end method

.method public final bN_()V
    .locals 0

    .prologue
    .line 2672737
    invoke-virtual {p0}, LX/7au;->d()V

    .line 2672738
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2672739
    invoke-virtual {p0}, LX/7au;->f()V

    .line 2672740
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2672741
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2672742
    :goto_0
    invoke-super {p0, p1}, LX/7au;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 2672743
    :pswitch_0
    invoke-virtual {p0}, LX/JHM;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 2672744
    :pswitch_1
    invoke-virtual {p0}, LX/JHM;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 2672745
    sub-int v0, p4, p2

    .line 2672746
    sub-int v1, p5, p3

    .line 2672747
    iget-object v2, p0, LX/JHM;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz v2, :cond_0

    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 2672748
    iget-object v2, p0, LX/JHM;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-static {p0, v2, v0, v1}, LX/JHM;->a(LX/JHM;Lcom/google/android/gms/maps/model/LatLngBounds;II)V

    .line 2672749
    const/4 v0, 0x0

    iput-object v0, p0, LX/JHM;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 2672750
    :cond_0
    invoke-super/range {p0 .. p5}, LX/7au;->onLayout(ZIIII)V

    .line 2672751
    return-void
.end method

.method public final onPreDraw()Z
    .locals 1

    .prologue
    .line 2672752
    iget-boolean v0, p0, LX/JHM;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/JHM;->c:I

    if-lez v0, :cond_0

    .line 2672753
    iget v0, p0, LX/JHM;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/JHM;->c:I

    .line 2672754
    :cond_0
    iget-boolean v0, p0, LX/JHM;->b:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/JHM;->c:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
