.class public final LX/HFE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V
    .locals 0

    .prologue
    .line 2443287
    iput-object p1, p0, LX/HFE;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x3509ca5f    # -8067792.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443288
    iget-object v1, p0, LX/HFE;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    const/4 p0, 0x1

    .line 2443289
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->q:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, p0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443290
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->r:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, p0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443291
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443292
    iget-object p0, v2, LX/HFJ;->l:Landroid/net/Uri;

    move-object v2, p0

    .line 2443293
    if-nez v2, :cond_0

    .line 2443294
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->f:LX/HFf;

    sget-object p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->k:Ljava/lang/String;

    const-string p1, "save_"

    invoke-virtual {v2, p0, p1}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443295
    :goto_0
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    iget-object p0, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->s:Landroid/net/Uri;

    .line 2443296
    iput-object p0, v2, LX/HFJ;->l:Landroid/net/Uri;

    .line 2443297
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->r:Lcom/facebook/fig/button/FigButton;

    const p0, 0x7f082219

    invoke-virtual {v2, p0}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2443298
    const/16 v1, 0x27

    const v2, 0x2159a723

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2443299
    :cond_0
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->f:LX/HFf;

    sget-object p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->k:Ljava/lang/String;

    const-string p1, "update_"

    invoke-virtual {v2, p0, p1}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
