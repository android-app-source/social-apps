.class public LX/IXg;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "LX/IYM;",
        "Lcom/facebook/instantarticles/model/data/UfiBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/IYM;)V
    .locals 0

    .prologue
    .line 2586549
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2586550
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 2

    .prologue
    .line 2586551
    check-cast p1, LX/IXe;

    .line 2586552
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2586553
    check-cast v0, LX/IYM;

    .line 2586554
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 2586555
    iget-object v1, p1, LX/IXe;->a:LX/ClW;

    move-object v1, v1

    .line 2586556
    invoke-interface {v0, v1}, LX/IYM;->a(LX/ClW;)V

    .line 2586557
    instance-of v0, v0, LX/CnL;

    if-eqz v0, :cond_0

    .line 2586558
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2586559
    check-cast v0, LX/CnL;

    .line 2586560
    invoke-interface {v0}, LX/CnL;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2586561
    iget-object v1, p1, LX/IXe;->b:LX/Cly;

    move-object v1, v1

    .line 2586562
    invoke-interface {v0, v1}, LX/CnL;->setFeedbackHeaderTitle(LX/Cly;)V

    .line 2586563
    iget-object v1, p1, LX/IXe;->c:LX/CmD;

    move-object v1, v1

    .line 2586564
    invoke-interface {v0, v1}, LX/CnL;->setFeedbackHeaderAuthorByline(LX/CmD;)V

    .line 2586565
    iget-object v1, p1, LX/IXe;->d:LX/CmQ;

    move-object v1, v1

    .line 2586566
    invoke-interface {v0, v1}, LX/CnL;->setLogoInformation(LX/CmQ;)V

    .line 2586567
    iget-object v1, p1, LX/IXe;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2586568
    invoke-interface {v0, v1}, LX/CnL;->setShareUrl(Ljava/lang/String;)V

    .line 2586569
    :cond_0
    return-void
.end method
