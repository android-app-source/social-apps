.class public final LX/JIZ;
.super LX/JIY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/JIY",
        "<",
        "LX/JJq;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JIe;


# direct methods
.method public constructor <init>(LX/JIe;)V
    .locals 0

    .prologue
    .line 2678043
    iput-object p1, p0, LX/JIZ;->a:LX/JIe;

    invoke-direct {p0}, LX/JIY;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/JJq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2678044
    const-class v0, LX/JJq;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2678045
    iget-object v0, p0, LX/JIZ;->a:LX/JIe;

    iget-object v0, v0, LX/JIe;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    iget-object v1, p0, LX/JIZ;->a:LX/JIe;

    iget-object v1, v1, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iget-object v2, p0, LX/JIZ;->a:LX/JIe;

    iget-object v2, v2, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/JIX;

    invoke-direct {v1, p0}, LX/JIX;-><init>(LX/JIZ;)V

    iget-object v2, p0, LX/JIZ;->a:LX/JIe;

    iget-object v2, v2, LX/JIe;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2678046
    return-void
.end method
