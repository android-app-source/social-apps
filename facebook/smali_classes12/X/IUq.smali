.class public LX/IUq;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field public b:LX/IUo;

.field public c:LX/IUp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2581085
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2581086
    invoke-virtual {p0}, LX/IUq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2581087
    invoke-virtual {p0, v0, v0, v0, v0}, LX/IUq;->setPadding(IIII)V

    .line 2581088
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x7f0a00d5

    invoke-static {p1, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/IUq;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2581089
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/IUq;->a:Landroid/graphics/Paint;

    .line 2581090
    iget-object v0, p0, LX/IUq;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/IUq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2581091
    iget-object v0, p0, LX/IUq;->a:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2581092
    return-void
.end method

.method public static a(LX/IUq;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2581093
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2581094
    invoke-virtual {p0}, LX/IUq;->removeAllViews()V

    .line 2581095
    invoke-virtual {p0, p1}, LX/IUq;->addView(Landroid/view/View;)V

    .line 2581096
    :cond_0
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2581097
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2581098
    invoke-virtual {p0}, LX/IUq;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2581099
    const/4 v1, 0x0

    int-to-float v2, v0

    invoke-virtual {p0}, LX/IUq;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/IUq;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2581100
    return-void
.end method
