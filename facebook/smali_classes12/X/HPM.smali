.class public final LX/HPM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HPN;


# direct methods
.method public constructor <init>(LX/HPN;)V
    .locals 0

    .prologue
    .line 2461770
    iput-object p1, p0, LX/HPM;->a:LX/HPN;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2461777
    iget-object v0, p0, LX/HPM;->a:LX/HPN;

    iget-object v0, v0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, p0, LX/HPM;->a:LX/HPN;

    iget-object v1, v1, LX/HPN;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v2, p0, LX/HPM;->a:LX/HPN;

    iget-boolean v2, v2, LX/HPN;->b:Z

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a$redex0(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Z)V

    .line 2461778
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2461771
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2461772
    if-eqz p1, :cond_0

    .line 2461773
    iget-object v0, p0, LX/HPM;->a:LX/HPN;

    iget-object v0, v0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iput-object p1, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2461774
    iget-object v0, p0, LX/HPM;->a:LX/HPN;

    iget-object v0, v0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->r:LX/0SI;

    iget-object v1, p0, LX/HPM;->a:LX/HPN;

    iget-object v1, v1, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {v0, v1}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2461775
    :cond_0
    iget-object v0, p0, LX/HPM;->a:LX/HPN;

    iget-object v0, v0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, p0, LX/HPM;->a:LX/HPN;

    iget-object v1, v1, LX/HPN;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v2, p0, LX/HPM;->a:LX/HPN;

    iget-boolean v2, v2, LX/HPN;->b:Z

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a$redex0(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Z)V

    .line 2461776
    return-void
.end method
