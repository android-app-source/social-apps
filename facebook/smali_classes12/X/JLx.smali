.class public LX/JLx;
.super LX/JLw;
.source ""


# instance fields
.field public a:LX/JLs;

.field public b:Z

.field public c:Z

.field private d:I

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/5pX;)V
    .locals 1

    .prologue
    .line 2682758
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JLw;-><init>(LX/5pX;LX/F6L;)V

    .line 2682759
    new-instance v0, Lcom/facebook/fbreact/perf/ObservableFbReactScrollView$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbreact/perf/ObservableFbReactScrollView$1;-><init>(LX/JLx;)V

    iput-object v0, p0, LX/JLx;->e:Ljava/lang/Runnable;

    .line 2682760
    return-void
.end method

.method public static setScrollState(LX/JLx;I)V
    .locals 2

    .prologue
    .line 2682761
    iget v0, p0, LX/JLx;->d:I

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/JLx;->a:LX/JLs;

    if-eqz v0, :cond_0

    .line 2682762
    iget-object v0, p0, LX/JLx;->a:LX/JLs;

    .line 2682763
    packed-switch p1, :pswitch_data_0

    .line 2682764
    :cond_0
    :goto_0
    iput p1, p0, LX/JLx;->d:I

    .line 2682765
    return-void

    .line 2682766
    :pswitch_0
    iget-object v1, v0, LX/JLs;->a:Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;

    iget-object v1, v1, Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-virtual {v1}, Lcom/facebook/common/perftest/DrawFrameLogger;->b()V

    goto :goto_0

    .line 2682767
    :pswitch_1
    iget-object v1, v0, LX/JLs;->a:Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;

    iget-object v1, v1, Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-virtual {v1}, Lcom/facebook/common/perftest/DrawFrameLogger;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final onScrollChanged(IIII)V
    .locals 4

    .prologue
    .line 2682749
    invoke-super {p0, p1, p2, p3, p4}, LX/JLw;->onScrollChanged(IIII)V

    .line 2682750
    sub-int v0, p4, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lez v0, :cond_1

    .line 2682751
    iget-object v0, p0, LX/JLx;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/JLx;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2682752
    iget-boolean v0, p0, LX/JLx;->c:Z

    if-eqz v0, :cond_2

    .line 2682753
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/JLx;->setScrollState(LX/JLx;I)V

    .line 2682754
    :cond_0
    :goto_0
    iget-object v0, p0, LX/JLx;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x28

    invoke-virtual {p0, v0, v2, v3}, LX/JLx;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2682755
    :cond_1
    return-void

    .line 2682756
    :cond_2
    iget-object v0, p0, LX/JLx;->a:LX/JLs;

    if-eqz v0, :cond_0

    .line 2682757
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/JLx;->setScrollState(LX/JLx;I)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    const v0, -0x5685934d

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2682740
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 2682741
    if-ne v1, v4, :cond_1

    .line 2682742
    iput-boolean v2, p0, LX/JLx;->c:Z

    .line 2682743
    iput-boolean v2, p0, LX/JLx;->b:Z

    .line 2682744
    :cond_0
    :goto_0
    invoke-super {p0, p1}, LX/JLw;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x5719d699

    invoke-static {v2, v0}, LX/02F;->a(II)V

    return v1

    .line 2682745
    :cond_1
    if-ne v1, v2, :cond_0

    .line 2682746
    iget-boolean v1, p0, LX/JLx;->c:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, LX/JLx;->b:Z

    if-nez v1, :cond_2

    .line 2682747
    invoke-static {p0, v3}, LX/JLx;->setScrollState(LX/JLx;I)V

    .line 2682748
    :cond_2
    iput-boolean v3, p0, LX/JLx;->c:Z

    goto :goto_0
.end method
