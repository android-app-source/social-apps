.class public LX/J0U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/J0U;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637155
    return-void
.end method

.method public static a(LX/0QB;)LX/J0U;
    .locals 3

    .prologue
    .line 2637165
    sget-object v0, LX/J0U;->a:LX/J0U;

    if-nez v0, :cond_1

    .line 2637166
    const-class v1, LX/J0U;

    monitor-enter v1

    .line 2637167
    :try_start_0
    sget-object v0, LX/J0U;->a:LX/J0U;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637168
    if-eqz v2, :cond_0

    .line 2637169
    :try_start_1
    new-instance v0, LX/J0U;

    invoke-direct {v0}, LX/J0U;-><init>()V

    .line 2637170
    move-object v0, v0

    .line 2637171
    sput-object v0, LX/J0U;->a:LX/J0U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637172
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637173
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637174
    :cond_1
    sget-object v0, LX/J0U;->a:LX/J0U;

    return-object v0

    .line 2637175
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637176
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2637177
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2637178
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer() { peer_to_peer_payments { iris_sequence_id } }"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637179
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetchIrisSequenceId"

    .line 2637180
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2637181
    move-object v1, v1

    .line 2637182
    const-string v2, "GET"

    .line 2637183
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2637184
    move-object v1, v1

    .line 2637185
    const-string v2, "graphql"

    .line 2637186
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2637187
    move-object v1, v1

    .line 2637188
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2637189
    move-object v0, v1

    .line 2637190
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2637191
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637192
    move-object v0, v0

    .line 2637193
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2637156
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637157
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2637158
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 2637159
    :goto_0
    invoke-virtual {v0}, LX/15w;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "peer_to_peer_payments"

    if-eq v1, v2, :cond_0

    .line 2637160
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    goto :goto_0

    .line 2637161
    :cond_0
    invoke-virtual {v0}, LX/15w;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2637162
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 2637163
    const-class v1, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;

    .line 2637164
    :goto_1
    return-object v0

    :cond_1
    new-instance v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
