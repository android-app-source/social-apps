.class public final LX/I7C;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538682
    iput-object p1, p0, LX/I7C;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2538683
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2538658
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2538659
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2538660
    if-eqz v0, :cond_0

    .line 2538661
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2538662
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2538663
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2538664
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2538665
    :cond_0
    :goto_0
    return-void

    .line 2538666
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2538667
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    move-result-object v0

    .line 2538668
    iget-object v1, p0, LX/I7C;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2538669
    iput-object v2, v1, LX/I8k;->N:Ljava/lang/String;

    .line 2538670
    iget-object v1, p0, LX/I7C;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v2

    .line 2538671
    iput-boolean v2, v1, LX/I8k;->O:Z

    .line 2538672
    iget-object v1, p0, LX/I7C;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;->a()LX/0Px;

    move-result-object v0

    .line 2538673
    iget-object v2, v1, LX/I8k;->k:LX/I8V;

    iget-boolean p0, v1, LX/I8k;->O:Z

    .line 2538674
    iget-boolean p1, v2, LX/I8V;->c:Z

    if-eq p1, p0, :cond_2

    .line 2538675
    iput-boolean p0, v2, LX/I8V;->c:Z

    .line 2538676
    const p1, -0x31efa6

    invoke-static {v2, p1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2538677
    :cond_2
    iget-object v2, v1, LX/I8k;->k:LX/I8V;

    .line 2538678
    new-instance p0, LX/0Pz;

    invoke-direct {p0}, LX/0Pz;-><init>()V

    iget-object p1, v2, LX/I8V;->b:LX/0Px;

    invoke-virtual {p0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object p0

    invoke-virtual {p0, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object p0

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    iput-object p0, v2, LX/I8V;->b:LX/0Px;

    .line 2538679
    invoke-virtual {v1}, LX/I8k;->j()V

    .line 2538680
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2538681
    goto :goto_0
.end method
