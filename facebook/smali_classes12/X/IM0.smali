.class public final LX/IM0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2568869
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2568870
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2568871
    :goto_0
    return v1

    .line 2568872
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2568873
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2568874
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2568875
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2568876
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2568877
    const-string v5, "group_friend_members"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2568878
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2568879
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_a

    .line 2568880
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2568881
    :goto_2
    move v3, v4

    .line 2568882
    goto :goto_1

    .line 2568883
    :cond_2
    const-string v5, "group_member_profiles"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2568884
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2568885
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_f

    .line 2568886
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2568887
    :goto_3
    move v2, v4

    .line 2568888
    goto :goto_1

    .line 2568889
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2568890
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2568891
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2568892
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2568893
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2568894
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2568895
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 2568896
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 2568897
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2568898
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2568899
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_6

    if-eqz v7, :cond_6

    .line 2568900
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2568901
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v5

    goto :goto_4

    .line 2568902
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2568903
    :cond_8
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2568904
    if-eqz v3, :cond_9

    .line 2568905
    invoke-virtual {p1, v4, v6, v4}, LX/186;->a(III)V

    .line 2568906
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v3, v4

    move v6, v4

    goto :goto_4

    .line 2568907
    :cond_b
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_d

    .line 2568908
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2568909
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2568910
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_b

    if-eqz v7, :cond_b

    .line 2568911
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2568912
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v6, v2

    move v2, v5

    goto :goto_5

    .line 2568913
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 2568914
    :cond_d
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2568915
    if-eqz v2, :cond_e

    .line 2568916
    invoke-virtual {p1, v4, v6, v4}, LX/186;->a(III)V

    .line 2568917
    :cond_e
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_f
    move v2, v4

    move v6, v4

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2568918
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2568919
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2568920
    if-eqz v0, :cond_1

    .line 2568921
    const-string v1, "group_friend_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2568922
    const/4 v1, 0x0

    .line 2568923
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2568924
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2568925
    if-eqz v1, :cond_0

    .line 2568926
    const-string p3, "count"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2568927
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2568928
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2568929
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2568930
    if-eqz v0, :cond_3

    .line 2568931
    const-string v1, "group_member_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2568932
    const/4 v1, 0x0

    .line 2568933
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2568934
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2568935
    if-eqz v1, :cond_2

    .line 2568936
    const-string p3, "count"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2568937
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2568938
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2568939
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2568940
    if-eqz v0, :cond_4

    .line 2568941
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2568942
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2568943
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2568944
    return-void
.end method
