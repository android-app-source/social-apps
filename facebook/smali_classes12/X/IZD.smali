.class public final LX/IZD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/IZ7;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/IZ7;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 2588596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2588597
    iput-object p1, p0, LX/IZD;->a:LX/0QB;

    .line 2588598
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2588599
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/IZD;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2588600
    packed-switch p2, :pswitch_data_0

    .line 2588601
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2588602
    :pswitch_0
    new-instance v0, LX/IZ8;

    invoke-direct {v0}, LX/IZ8;-><init>()V

    .line 2588603
    move-object v0, v0

    .line 2588604
    move-object v0, v0

    .line 2588605
    :goto_0
    return-object v0

    .line 2588606
    :pswitch_1
    new-instance v0, LX/IaI;

    invoke-direct {v0}, LX/IaI;-><init>()V

    .line 2588607
    move-object v0, v0

    .line 2588608
    move-object v0, v0

    .line 2588609
    goto :goto_0

    .line 2588610
    :pswitch_2
    new-instance v0, LX/Iaf;

    invoke-direct {v0}, LX/Iaf;-><init>()V

    .line 2588611
    move-object v0, v0

    .line 2588612
    move-object v0, v0

    .line 2588613
    goto :goto_0

    .line 2588614
    :pswitch_3
    new-instance v0, LX/Iaj;

    invoke-direct {v0}, LX/Iaj;-><init>()V

    .line 2588615
    move-object v0, v0

    .line 2588616
    move-object v0, v0

    .line 2588617
    goto :goto_0

    .line 2588618
    :pswitch_4
    new-instance v0, LX/Iaq;

    invoke-direct {v0}, LX/Iaq;-><init>()V

    .line 2588619
    move-object v0, v0

    .line 2588620
    move-object v0, v0

    .line 2588621
    goto :goto_0

    .line 2588622
    :pswitch_5
    new-instance v0, LX/Iaw;

    invoke-direct {v0}, LX/Iaw;-><init>()V

    .line 2588623
    move-object v0, v0

    .line 2588624
    move-object v0, v0

    .line 2588625
    goto :goto_0

    .line 2588626
    :pswitch_6
    new-instance v0, LX/Jfg;

    invoke-direct {v0}, LX/Jfg;-><init>()V

    .line 2588627
    move-object v0, v0

    .line 2588628
    move-object v0, v0

    .line 2588629
    goto :goto_0

    .line 2588630
    :pswitch_7
    new-instance v0, LX/Icf;

    invoke-direct {v0}, LX/Icf;-><init>()V

    .line 2588631
    move-object v0, v0

    .line 2588632
    move-object v0, v0

    .line 2588633
    goto :goto_0

    .line 2588634
    :pswitch_8
    new-instance v0, LX/IdK;

    invoke-direct {v0}, LX/IdK;-><init>()V

    .line 2588635
    move-object v0, v0

    .line 2588636
    move-object v0, v0

    .line 2588637
    goto :goto_0

    .line 2588638
    :pswitch_9
    new-instance v0, LX/JgN;

    invoke-direct {v0}, LX/JgN;-><init>()V

    .line 2588639
    move-object v0, v0

    .line 2588640
    move-object v0, v0

    .line 2588641
    goto :goto_0

    .line 2588642
    :pswitch_a
    new-instance v0, LX/Ikb;

    invoke-direct {v0}, LX/Ikb;-><init>()V

    .line 2588643
    move-object v0, v0

    .line 2588644
    move-object v0, v0

    .line 2588645
    goto :goto_0

    .line 2588646
    :pswitch_b
    new-instance v0, LX/Iks;

    invoke-direct {v0}, LX/Iks;-><init>()V

    .line 2588647
    move-object v0, v0

    .line 2588648
    move-object v0, v0

    .line 2588649
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2588650
    const/16 v0, 0xc

    return v0
.end method
