.class public final enum LX/ILX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ILX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ILX;

.field public static final enum FETCH_COMMUNITY_FORUM_GROUPS:LX/ILX;

.field public static final enum JOIN_GROUPS:LX/ILX;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2568025
    new-instance v0, LX/ILX;

    const-string v1, "FETCH_COMMUNITY_FORUM_GROUPS"

    invoke-direct {v0, v1, v2}, LX/ILX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ILX;->FETCH_COMMUNITY_FORUM_GROUPS:LX/ILX;

    .line 2568026
    new-instance v0, LX/ILX;

    const-string v1, "JOIN_GROUPS"

    invoke-direct {v0, v1, v3}, LX/ILX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ILX;->JOIN_GROUPS:LX/ILX;

    .line 2568027
    const/4 v0, 0x2

    new-array v0, v0, [LX/ILX;

    sget-object v1, LX/ILX;->FETCH_COMMUNITY_FORUM_GROUPS:LX/ILX;

    aput-object v1, v0, v2

    sget-object v1, LX/ILX;->JOIN_GROUPS:LX/ILX;

    aput-object v1, v0, v3

    sput-object v0, LX/ILX;->$VALUES:[LX/ILX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2568022
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ILX;
    .locals 1

    .prologue
    .line 2568024
    const-class v0, LX/ILX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ILX;

    return-object v0
.end method

.method public static values()[LX/ILX;
    .locals 1

    .prologue
    .line 2568023
    sget-object v0, LX/ILX;->$VALUES:[LX/ILX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ILX;

    return-object v0
.end method
