.class public final LX/Hr1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hr0;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 0

    .prologue
    .line 2510847
    iput-object p1, p0, LX/Hr1;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 2510835
    iget-object v0, p0, LX/Hr1;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2510836
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldShowPageVoiceSwitcher()Z

    move-result v1

    const-string v2, "Page Voice Switcher is not enabled in the configuration, but yet we try to launch it."

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2510837
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2510838
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    .line 2510839
    const-string v1, "pvs_group_id_extra"

    iget-wide v5, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2510840
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->ab:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ix9;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 2510841
    iget-object v6, v2, LX/Ix9;->a:LX/17Y;

    sget-object v7, LX/0ax;->aB:Ljava/lang/String;

    invoke-interface {v6, v5, v7}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 2510842
    const-string v8, "target_type_extra"

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/Serializable;

    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2510843
    invoke-virtual {v7, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2510844
    move-object v2, v7

    .line 2510845
    const/16 v3, 0x12

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2510846
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2510833
    iget-object v0, p0, LX/Hr1;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->ar(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2510834
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2510820
    iget-object v0, p0, LX/Hr1;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->av(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2510821
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2510831
    iget-object v0, p0, LX/Hr1;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->aA(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2510832
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 2510824
    iget-object v0, p0, LX/Hr1;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2510825
    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_MINUTIAE_ICON_CLICKED:LX/0ge;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2510826
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    .line 2510827
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->H:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/925;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v4}, LX/Hre;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b()LX/0Px;

    move-result-object p0

    invoke-static {v3, v4, v2, p0}, LX/925;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0Px;)Landroid/content/Intent;

    move-result-object v2

    .line 2510828
    if-eqz v2, :cond_0

    .line 2510829
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/4 v3, 0x6

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2510830
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2510822
    iget-object v0, p0, LX/Hr1;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->P$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2510823
    return-void
.end method
