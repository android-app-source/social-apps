.class public final LX/JMJ;
.super LX/1Ah;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ah",
        "<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JMK;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/JMK;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2683472
    iput-object p1, p0, LX/JMJ;->a:LX/JMK;

    invoke-direct {p0}, LX/1Ah;-><init>()V

    .line 2683473
    iput-object p2, p0, LX/JMJ;->b:Ljava/lang/String;

    .line 2683474
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2683475
    iget-object v0, p0, LX/JMJ;->a:LX/JMK;

    invoke-static {v0}, LX/JMK;->getEventDispatcher(LX/JMK;)LX/5s9;

    move-result-object v0

    new-instance v1, LX/K0K;

    iget-object v2, p0, LX/JMJ;->a:LX/JMK;

    invoke-virtual {v2}, LX/JMK;->getId()I

    move-result v2

    const/4 v3, 0x4

    iget-object v4, p0, LX/JMJ;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, LX/K0K;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2683476
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2683477
    check-cast p2, LX/1ln;

    .line 2683478
    if-eqz p2, :cond_0

    .line 2683479
    iget-object v0, p0, LX/JMJ;->a:LX/JMK;

    invoke-static {v0}, LX/JMK;->getEventDispatcher(LX/JMK;)LX/5s9;

    move-result-object v0

    new-instance v1, LX/K0K;

    iget-object v2, p0, LX/JMJ;->a:LX/JMK;

    invoke-virtual {v2}, LX/JMK;->getId()I

    move-result v2

    const/4 v3, 0x3

    iget-object v4, p0, LX/JMJ;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, LX/K0K;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2683480
    iget-object v0, p0, LX/JMJ;->a:LX/JMK;

    invoke-static {v0}, LX/JMK;->getEventDispatcher(LX/JMK;)LX/5s9;

    move-result-object v0

    new-instance v1, LX/K0K;

    iget-object v2, p0, LX/JMJ;->a:LX/JMK;

    invoke-virtual {v2}, LX/JMK;->getId()I

    move-result v2

    const/4 v3, 0x2

    iget-object v4, p0, LX/JMJ;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, LX/K0K;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2683481
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2683482
    iget-object v0, p0, LX/JMJ;->a:LX/JMK;

    invoke-static {v0}, LX/JMK;->getEventDispatcher(LX/JMK;)LX/5s9;

    move-result-object v0

    new-instance v1, LX/K0K;

    iget-object v2, p0, LX/JMJ;->a:LX/JMK;

    invoke-virtual {v2}, LX/JMK;->getId()I

    move-result v2

    const/4 v3, 0x3

    iget-object v4, p0, LX/JMJ;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, LX/K0K;-><init>(IILjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2683483
    return-void
.end method
