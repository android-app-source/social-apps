.class public final LX/Ifb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IfB;


# instance fields
.field public final synthetic a:LX/0gc;

.field public final synthetic b:LX/Ife;


# direct methods
.method public constructor <init>(LX/Ife;LX/0gc;)V
    .locals 0

    .prologue
    .line 2599876
    iput-object p1, p0, LX/Ifb;->b:LX/Ife;

    iput-object p2, p0, LX/Ifb;->a:LX/0gc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 4

    .prologue
    .line 2599873
    iget-object v0, p0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->b:LX/IfT;

    const-string v1, "PEOPLE_TAB"

    invoke-virtual {v0, v1, p1}, LX/IfT;->a(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599874
    iget-object v0, p0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ie8;

    iget-object v1, p0, LX/Ifb;->b:LX/Ife;

    invoke-virtual {v1}, LX/Ife;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Ifb;->a:LX/0gc;

    iget-object v3, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v0, v1, v2, v3}, LX/Ie8;->a(Landroid/content/Context;LX/0gc;Lcom/facebook/user/model/User;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/Ifa;

    invoke-direct {v1, p0, p1}, LX/Ifa;-><init>(LX/Ifb;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    iget-object v2, p0, LX/Ifb;->b:LX/Ife;

    iget-object v2, v2, LX/Ife;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2599875
    return-void
.end method

.method public final b(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 2

    .prologue
    .line 2599877
    iget-object v0, p0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->b:LX/IfT;

    const-string v1, "PEOPLE_TAB"

    invoke-virtual {v0, v1, p1}, LX/IfT;->b(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599878
    return-void
.end method

.method public final c(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 6

    .prologue
    .line 2599856
    iget-object v0, p0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IfW;

    const-string v1, "PEOPLE_TAB"

    .line 2599857
    new-instance v2, LX/CLE;

    invoke-direct {v2}, LX/CLE;-><init>()V

    move-object v2, v2

    .line 2599858
    new-instance v3, LX/4HA;

    invoke-direct {v3}, LX/4HA;-><init>()V

    iget-object v4, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599859
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2599860
    const-string v5, "suggestion_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2599861
    move-object v3, v3

    .line 2599862
    const-string v4, "input"

    invoke-virtual {v2, v4, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2599863
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    invoke-static {p1}, LX/IfW;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)LX/0jT;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v2

    .line 2599864
    iget-object v3, v0, LX/IfW;->b:LX/0tX;

    sget-object v4, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v3, v2, v4}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2599865
    new-instance v3, LX/IfV;

    invoke-direct {v3, v0, v1}, LX/IfV;-><init>(LX/IfW;Ljava/lang/String;)V

    iget-object v4, v0, LX/IfW;->d:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2599866
    iget-object v0, p0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->d:LX/IfP;

    const-string v1, "PEOPLE_TAB"

    iget-object v2, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599867
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2599868
    invoke-virtual {v0, v1, v2}, LX/IfP;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2599869
    iget-object v0, p0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->b:LX/IfT;

    const-string v1, "PEOPLE_TAB"

    invoke-virtual {v0, v1, p1}, LX/IfT;->c(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599870
    iget-object v0, p0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->a:LX/Ies;

    invoke-virtual {v0, p1}, LX/Ies;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599871
    iget-object v0, p0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->c:LX/Ieu;

    const-string v1, "PEOPLE_TAB"

    invoke-virtual {v0, v1}, LX/Ieu;->b(Ljava/lang/String;)Z

    .line 2599872
    return-void
.end method

.method public final d(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)Z
    .locals 1

    .prologue
    .line 2599855
    const/4 v0, 0x0

    return v0
.end method
