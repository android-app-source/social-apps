.class public LX/IDE;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/4a7;


# instance fields
.field private final b:LX/0SG;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/2dl;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private final g:LX/DHs;

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2550494
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    .line 2550495
    iput-boolean v1, v0, LX/4a7;->c:Z

    .line 2550496
    move-object v0, v0

    .line 2550497
    iput-boolean v1, v0, LX/4a7;->b:Z

    .line 2550498
    move-object v0, v0

    .line 2550499
    sput-object v0, LX/IDE;->a:LX/4a7;

    return-void
.end method

.method public constructor <init>(LX/0SG;Ljava/util/concurrent/ExecutorService;LX/2dl;LX/0Ot;Ljava/lang/String;LX/DHs;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/DHs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2dl;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "Ljava/lang/String;",
            "LX/DHs;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2550500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2550501
    const/4 v0, 0x0

    iput v0, p0, LX/IDE;->h:I

    .line 2550502
    iput-object p1, p0, LX/IDE;->b:LX/0SG;

    .line 2550503
    iput-object p2, p0, LX/IDE;->c:Ljava/util/concurrent/ExecutorService;

    .line 2550504
    iput-object p3, p0, LX/IDE;->d:LX/2dl;

    .line 2550505
    iput-object p4, p0, LX/IDE;->e:LX/0Ot;

    .line 2550506
    iput-object p5, p0, LX/IDE;->f:Ljava/lang/String;

    .line 2550507
    iput-object p6, p0, LX/IDE;->g:LX/DHs;

    .line 2550508
    return-void
.end method

.method private a(LX/0zO;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2550509
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, LX/IDE;->f:Ljava/lang/String;

    .line 2550510
    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "friendlist_"

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2550511
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    .line 2550512
    iput-object v0, p1, LX/0zO;->d:Ljava/util/Set;

    .line 2550513
    move-object v0, p1

    .line 2550514
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0xe10

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0zO;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;",
            "LX/IDG;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550515
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/IDE;->a(LX/0zO;LX/0QK;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0zO;LX/0QK;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;",
            "LX/IDG;",
            ">;",
            "LX/0TF",
            "<",
            "LX/IDG;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550529
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/IDE;->a(LX/0zO;LX/0QK;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0zO;LX/0QK;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;",
            "LX/IDG;",
            ">;",
            "LX/0TF",
            "<",
            "LX/IDG;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550516
    iget-object v0, p0, LX/IDE;->d:LX/2dl;

    iget-object v1, p0, LX/IDE;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1My;

    new-instance v3, LX/ID6;

    invoke-direct {v3, p0, p3, p2}, LX/ID6;-><init>(LX/IDE;LX/0TF;LX/0QK;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2550517
    iget-object v4, p1, LX/0zO;->m:LX/0gW;

    move-object v4, v4

    .line 2550518
    iget-object v5, v4, LX/0gW;->f:Ljava/lang/String;

    move-object v4, v5

    .line 2550519
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, LX/IDE;->h:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/IDE;->h:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/2dl;->a(LX/0zO;LX/1My;LX/0TF;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2550520
    iget-object v1, p0, LX/IDE;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0zO;LX/0QK;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;",
            "LX/IDG;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550521
    iget-object v0, p0, LX/IDE;->d:LX/2dl;

    invoke-virtual {v0, p1, p3}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, LX/IDE;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550522
    new-instance v0, LX/IEG;

    invoke-direct {v0}, LX/IEG;-><init>()V

    move-object v0, v0

    .line 2550523
    const-string v1, "profile_id"

    iget-object v2, p0, LX/IDE;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after_param"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2550524
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-direct {p0, v0}, LX/IDE;->a(LX/0zO;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/List;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)LX/IDG;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;",
            ">;",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;",
            ")",
            "LX/IDG;"
        }
    .end annotation

    .prologue
    .line 2550525
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2550526
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;

    .line 2550527
    new-instance v3, LX/IDH;

    invoke-direct {v3, v0}, LX/IDH;-><init>(Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;)V

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2550528
    :cond_0
    new-instance v0, LX/IDG;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/IDG;-><init>(LX/0Px;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)V

    return-object v0
.end method

.method private c()LX/0zO;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550487
    iget-object v0, p0, LX/IDE;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    const-wide/16 v2, 0xe

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x15180

    mul-long/2addr v0, v2

    .line 2550488
    new-instance v2, LX/IEJ;

    invoke-direct {v2}, LX/IEJ;-><init>()V

    move-object v2, v2

    .line 2550489
    const-string v3, "profile_id"

    iget-object v4, p0, LX/IDE;->f:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "after_timestamp"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2550490
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-direct {p0, v0}, LX/IDE;->a(LX/0zO;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550491
    new-instance v0, LX/IEL;

    invoke-direct {v0}, LX/IEL;-><init>()V

    move-object v0, v0

    .line 2550492
    const-string v1, "profile_id"

    iget-object v2, p0, LX/IDE;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after_param"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2550493
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-direct {p0, v0}, LX/IDE;->a(LX/0zO;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method private d()LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;",
            ">;",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550450
    new-instance v0, LX/ID7;

    invoke-direct {v0, p0}, LX/ID7;-><init>(LX/IDE;)V

    return-object v0
.end method

.method private d(Ljava/lang/String;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550451
    new-instance v0, LX/IEH;

    invoke-direct {v0}, LX/IEH;-><init>()V

    move-object v0, v0

    .line 2550452
    const-string v1, "profile_id"

    iget-object v2, p0, LX/IDE;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after_param"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2550453
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-direct {p0, v0}, LX/IDE;->a(LX/0zO;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method private e()LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;",
            ">;",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550454
    new-instance v0, LX/ID8;

    invoke-direct {v0, p0}, LX/ID8;-><init>(LX/IDE;)V

    return-object v0
.end method

.method private e(Ljava/lang/String;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550455
    new-instance v0, LX/IEI;

    invoke-direct {v0}, LX/IEI;-><init>()V

    move-object v0, v0

    .line 2550456
    const-string v1, "location_param"

    sget-object v2, LX/2hC;->SELF_PROFILE:LX/2hC;

    iget-object v2, v2, LX/2hC;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after_param"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2550457
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-direct {p0, v0}, LX/IDE;->a(LX/0zO;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method private f()LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;",
            ">;",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550458
    new-instance v0, LX/ID9;

    invoke-direct {v0, p0}, LX/ID9;-><init>(LX/IDE;)V

    return-object v0
.end method

.method private f(Ljava/lang/String;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550459
    new-instance v0, LX/IEK;

    invoke-direct {v0}, LX/IEK;-><init>()V

    move-object v0, v0

    .line 2550460
    const-string v1, "profile_id"

    iget-object v2, p0, LX/IDE;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after_param"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2550461
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-direct {p0, v0}, LX/IDE;->a(LX/0zO;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method private g()LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;",
            ">;",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550462
    new-instance v0, LX/IDA;

    invoke-direct {v0, p0}, LX/IDA;-><init>(LX/IDE;)V

    return-object v0
.end method

.method private h()LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;",
            ">;",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550463
    new-instance v0, LX/IDB;

    invoke-direct {v0, p0}, LX/IDB;-><init>(LX/IDE;)V

    return-object v0
.end method

.method private i()LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;",
            ">;",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550464
    new-instance v0, LX/IDC;

    invoke-direct {v0, p0}, LX/IDC;-><init>(LX/IDE;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550465
    sget-object v0, LX/IDD;->a:[I

    iget-object v1, p0, LX/IDE;->g:LX/DHs;

    invoke-virtual {v1}, LX/DHs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2550466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected value for FriendListType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2550467
    :pswitch_0
    invoke-direct {p0, p1}, LX/IDE;->b(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->d()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IDE;->a(LX/0zO;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2550468
    :goto_0
    return-object v0

    .line 2550469
    :pswitch_1
    invoke-direct {p0, p1}, LX/IDE;->c(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->e()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IDE;->a(LX/0zO;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2550470
    :pswitch_2
    invoke-direct {p0, p1}, LX/IDE;->d(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->f()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IDE;->a(LX/0zO;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2550471
    :pswitch_3
    invoke-direct {p0, p1}, LX/IDE;->e(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->g()LX/0QK;

    move-result-object v1

    const-string v2, "PROFILE_PYMK_QUERY_TAG"

    invoke-direct {p0, v0, v1, v2}, LX/IDE;->a(LX/0zO;LX/0QK;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2550472
    :pswitch_4
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2550473
    invoke-direct {p0}, LX/IDE;->c()LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->h()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IDE;->a(LX/0zO;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2550474
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2550475
    :pswitch_5
    invoke-direct {p0, p1}, LX/IDE;->f(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->i()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IDE;->a(LX/0zO;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/IDG;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/IDG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2550476
    sget-object v0, LX/IDD;->a:[I

    iget-object v1, p0, LX/IDE;->g:LX/DHs;

    invoke-virtual {v1}, LX/DHs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2550477
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected value for FriendListType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2550478
    :pswitch_0
    invoke-direct {p0, p1}, LX/IDE;->b(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->d()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, LX/IDE;->a(LX/0zO;LX/0QK;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2550479
    :goto_0
    return-object v0

    .line 2550480
    :pswitch_1
    invoke-direct {p0, p1}, LX/IDE;->c(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->e()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, LX/IDE;->a(LX/0zO;LX/0QK;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2550481
    :pswitch_2
    invoke-direct {p0, p1}, LX/IDE;->d(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->f()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, LX/IDE;->a(LX/0zO;LX/0QK;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2550482
    :pswitch_3
    invoke-direct {p0, p1}, LX/IDE;->e(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->g()LX/0QK;

    move-result-object v1

    const-string v2, "PROFILE_PYMK_QUERY_TAG"

    invoke-direct {p0, v0, v1, p2, v2}, LX/IDE;->a(LX/0zO;LX/0QK;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2550483
    :pswitch_4
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2550484
    invoke-direct {p0}, LX/IDE;->c()LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->h()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, LX/IDE;->a(LX/0zO;LX/0QK;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2550485
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2550486
    :pswitch_5
    invoke-direct {p0, p1}, LX/IDE;->f(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    invoke-direct {p0}, LX/IDE;->i()LX/0QK;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, LX/IDE;->a(LX/0zO;LX/0QK;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
