.class public final LX/Hhx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;)V
    .locals 0

    .prologue
    .line 2496505
    iput-object p1, p0, LX/Hhx;->a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x46ca57e5    # 25899.947f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2496497
    iget-object v1, p0, LX/Hhx;->a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->a:LX/Hht;

    iget-object v2, p0, LX/Hhx;->a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    iget-object v2, v2, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->e:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Hht;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2496498
    iget-object v1, p0, LX/Hhx;->a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->e:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v2, p0, LX/Hhx;->a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083759

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setError(Ljava/lang/CharSequence;)V

    .line 2496499
    iget-object v1, p0, LX/Hhx;->a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->b:LX/0if;

    sget-object v2, LX/0ig;->Y:LX/0ih;

    const-string v3, "error_phone_number_submit"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2496500
    const v1, 0x198e4238

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2496501
    :goto_0
    return-void

    .line 2496502
    :cond_0
    iget-object v1, p0, LX/Hhx;->a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->b:LX/0if;

    sget-object v2, LX/0ig;->Y:LX/0ih;

    const-string v3, "valid_phone_number_submit"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2496503
    iget-object v1, p0, LX/Hhx;->a:Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2496504
    const v1, 0x62b87c3c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
