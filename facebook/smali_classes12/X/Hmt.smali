.class public final LX/Hmt;
.super LX/Hmq;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Hmq",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hmi;

.field public final synthetic b:Lcom/facebook/beam/protocol/BeamPreflightInfo;

.field public final synthetic c:Lcom/facebook/beam/protocol/BeamPreflightInfo;

.field public final synthetic d:LX/Hmz;


# direct methods
.method public constructor <init>(LX/Hmz;LX/Hmi;Lcom/facebook/beam/protocol/BeamPreflightInfo;Lcom/facebook/beam/protocol/BeamPreflightInfo;)V
    .locals 0

    .prologue
    .line 2501133
    iput-object p1, p0, LX/Hmt;->d:LX/Hmz;

    iput-object p2, p0, LX/Hmt;->a:LX/Hmi;

    iput-object p3, p0, LX/Hmt;->b:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    iput-object p4, p0, LX/Hmt;->c:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-direct {p0, p1}, LX/Hmq;-><init>(LX/Hmz;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 2501134
    iget-object v0, p0, LX/Hmt;->a:LX/Hmi;

    iget-object v1, p0, LX/Hmt;->b:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    iget-object v2, p0, LX/Hmt;->c:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501135
    iget-object v3, v0, LX/Hmi;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2501136
    const-wide/16 v11, 0x0

    const/4 v5, 0x1

    .line 2501137
    iget-object v6, v2, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v6, v6

    .line 2501138
    iget-object v7, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v7, v7

    .line 2501139
    if-eqz v6, :cond_0

    if-nez v7, :cond_a

    .line 2501140
    :cond_0
    :goto_0
    move v3, v5

    .line 2501141
    if-nez v3, :cond_1

    .line 2501142
    iget-object v3, v0, LX/Hmi;->c:Ljava/util/List;

    sget-object v4, LX/Hmh;->INSUFFICIENT_STORAGE:LX/Hmh;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2501143
    :cond_1
    iget-object v3, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v3, v3

    .line 2501144
    iget-object v4, v2, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v4, v4

    .line 2501145
    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    iget v3, v3, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionCode:I

    iget v4, v4, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionCode:I

    if-le v3, v4, :cond_b

    :cond_2
    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 2501146
    if-nez v3, :cond_3

    .line 2501147
    iget-object v3, v0, LX/Hmi;->c:Ljava/util/List;

    sget-object v4, LX/Hmh;->OLDER_VERSION_CODE:LX/Hmh;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2501148
    :cond_3
    invoke-static {v1, v2}, LX/Hmi;->d(Lcom/facebook/beam/protocol/BeamPreflightInfo;Lcom/facebook/beam/protocol/BeamPreflightInfo;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2501149
    iget-object v3, v0, LX/Hmi;->c:Ljava/util/List;

    sget-object v4, LX/Hmh;->OLDER_MAJOR_VERSION:LX/Hmh;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2501150
    :cond_4
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2501151
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v5, v5

    .line 2501152
    iget-object v6, v2, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v6, v6

    .line 2501153
    if-eqz v5, :cond_5

    iget-object v9, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    if-eqz v9, :cond_5

    if-eqz v6, :cond_5

    iget-object v9, v6, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    if-nez v9, :cond_c

    :cond_5
    move v5, v7

    .line 2501154
    :goto_2
    move v3, v5

    .line 2501155
    if-nez v3, :cond_6

    .line 2501156
    iget-object v3, v0, LX/Hmi;->c:Ljava/util/List;

    sget-object v4, LX/Hmh;->INCOMPATIBLE_CPU:LX/Hmh;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2501157
    :cond_6
    iget-object v5, v2, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v5, v5

    .line 2501158
    iget-object v6, v0, LX/Hmi;->b:LX/Hmj;

    .line 2501159
    iget-object v8, v6, LX/Hmj;->a:LX/0W3;

    sget-wide v10, LX/0X5;->is:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v9}, LX/0W4;->a(JI)I

    move-result v8

    .line 2501160
    if-eqz v8, :cond_14

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    :goto_3
    move-object v6, v8

    .line 2501161
    if-eqz v5, :cond_7

    iget-object v7, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mSDKVersion:Ljava/lang/Integer;

    if-eqz v7, :cond_7

    if-eqz v6, :cond_7

    iget-object v5, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mSDKVersion:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-lt v5, v6, :cond_13

    :cond_7
    const/4 v5, 0x1

    :goto_4
    move v3, v5

    .line 2501162
    if-nez v3, :cond_8

    .line 2501163
    iget-object v3, v0, LX/Hmi;->c:Ljava/util/List;

    sget-object v4, LX/Hmh;->MIN_SDK_VERSION:LX/Hmh;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2501164
    :cond_8
    invoke-static {v0, v2}, LX/Hmi;->b(LX/Hmi;Lcom/facebook/beam/protocol/BeamPreflightInfo;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 2501165
    iget-object v3, v0, LX/Hmi;->c:Ljava/util/List;

    sget-object v4, LX/Hmh;->COMPATIBLE_DPI:LX/Hmh;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2501166
    :cond_9
    iget-object v3, v0, LX/Hmi;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    move v0, v3

    .line 2501167
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 2501168
    :cond_a
    iget-object v8, v6, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mAvailableSpace:Ljava/lang/Long;

    if-eqz v8, :cond_0

    iget-object v8, v6, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mAvailableSpace:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v8, v9, v11

    if-eqz v8, :cond_0

    iget-object v8, v7, Lcom/facebook/beam/protocol/BeamPackageInfo;->mApkSize:Ljava/lang/Long;

    if-eqz v8, :cond_0

    iget-object v8, v7, Lcom/facebook/beam/protocol/BeamPackageInfo;->mApkSize:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v8, v9, v11

    if-eqz v8, :cond_0

    .line 2501169
    iget-object v7, v7, Lcom/facebook/beam/protocol/BeamPackageInfo;->mApkSize:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-object v6, v6, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mAvailableSpace:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v6, v7, v9

    if-lez v6, :cond_0

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 2501170
    :cond_c
    iget-object v9, v0, LX/Hmi;->b:LX/Hmj;

    .line 2501171
    iget-object v10, v9, LX/Hmj;->a:LX/0W3;

    sget-wide v12, LX/0X5;->it:J

    invoke-interface {v10, v12, v13}, LX/0W4;->a(J)Z

    move-result v10

    move v9, v10

    .line 2501172
    if-eqz v9, :cond_d

    move v5, v7

    .line 2501173
    goto/16 :goto_2

    .line 2501174
    :cond_d
    iget-object v9, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_10

    iget-object v9, v6, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_10

    iget-object v5, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v6, v6, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2501175
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_f

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_f

    const-string v9, "arm"

    invoke-virtual {v5, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e

    const-string v9, "arm"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_f

    :cond_e
    const-string v9, "x86"

    invoke-virtual {v5, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_12

    const-string v9, "x86"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_12

    :cond_f
    const/4 v9, 0x1

    :goto_5
    move v5, v9

    .line 2501176
    if-eqz v5, :cond_11

    :cond_10
    move v5, v7

    goto/16 :goto_2

    :cond_11
    move v5, v8

    goto/16 :goto_2

    :cond_12
    const/4 v9, 0x0

    goto :goto_5

    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_4

    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_3
.end method
