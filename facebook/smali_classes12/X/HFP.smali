.class public final LX/HFP;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443542
    iput-object p1, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2443521
    iget-object v0, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2443522
    iget-object v0, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443523
    iget-object v0, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->a:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    const-string v2, "save website failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2443524
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2443525
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;

    const/4 v3, 0x0

    .line 2443526
    if-eqz p1, :cond_0

    .line 2443527
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2443528
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2443529
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 2443530
    iget-object v1, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2443531
    iput-object v0, v1, LX/HFJ;->e:Ljava/lang/String;

    .line 2443532
    iget-object v0, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->g:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    iget-object v2, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443533
    iget-object v0, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b()V

    .line 2443534
    :goto_1
    iget-object v0, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443535
    :cond_0
    return-void

    .line 2443536
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    move-result-object v0

    goto :goto_0

    .line 2443537
    :cond_2
    iget-object v0, p0, LX/HFP;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const v2, 0x7f0d22d2

    .line 2443538
    invoke-virtual {v0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object p1

    move-object v0, p1

    .line 2443539
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443540
    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2443541
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
