.class public final LX/JCK;
.super LX/J9u;
.source ""


# instance fields
.field public final synthetic a:LX/JCL;


# direct methods
.method public constructor <init>(LX/JCL;)V
    .locals 0

    .prologue
    .line 2661864
    iput-object p1, p0, LX/JCK;->a:LX/JCL;

    invoke-direct {p0}, LX/J9u;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2661865
    check-cast p1, LX/J9t;

    .line 2661866
    iget-object v0, p0, LX/JCK;->a:LX/JCL;

    iget-object v0, v0, LX/JCL;->c:LX/JCJ;

    iget-object v1, p0, LX/JCK;->a:LX/JCL;

    iget-object v1, v1, LX/JCL;->b:LX/9lP;

    .line 2661867
    iget-object v2, v1, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2661868
    iget-object v2, p0, LX/JCK;->a:LX/JCL;

    iget-object v2, v2, LX/JCL;->b:LX/9lP;

    invoke-static {v2}, LX/J8p;->a(LX/9lP;)LX/9lQ;

    move-result-object v2

    iget-object v3, p1, LX/J9t;->e:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2661869
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2661870
    invoke-virtual {v0}, LX/J8p;->b()Ljava/lang/String;

    move-result-object v5

    const-string v6, "collection_title"

    const/4 v7, 0x0

    invoke-static {v5, v6, v4, v7}, LX/3zi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2661871
    const-string v5, "profile_id"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2661872
    sget-object v5, LX/9lQ;->UNDEFINED:LX/9lQ;

    if-eq v2, v5, :cond_0

    .line 2661873
    const-string v5, "relationship_type"

    invoke-virtual {v2}, LX/9lQ;->getValue()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2661874
    :cond_0
    iget-object v5, v0, LX/J8p;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2661875
    iget-object v0, p1, LX/J9t;->b:Landroid/os/Bundle;

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2661876
    :goto_0
    iget-object v1, p1, LX/J9t;->c:Ljava/lang/String;

    iget-object v2, p1, LX/J9t;->d:LX/JBL;

    iget-object v3, p1, LX/J9t;->e:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iget-object v4, p1, LX/J9t;->f:LX/1Fb;

    iget-object v5, p0, LX/JCK;->a:LX/JCL;

    iget-object v5, v5, LX/JCL;->b:LX/9lP;

    .line 2661877
    iget-object v6, v5, LX/9lP;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v5, v6

    .line 2661878
    iget-object v6, p0, LX/JCK;->a:LX/JCL;

    iget-object v6, v6, LX/JCL;->b:LX/9lP;

    .line 2661879
    iget-object v7, v6, LX/9lP;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v6, v7

    .line 2661880
    iget-object v7, p1, LX/J9t;->g:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, LX/J8v;->a(Landroid/os/Bundle;Ljava/lang/String;LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Ljava/lang/String;)V

    .line 2661881
    iget-object v1, p0, LX/JCK;->a:LX/JCL;

    iget-object v1, v1, LX/JCL;->d:LX/17W;

    iget-object v2, p0, LX/JCK;->a:LX/JCL;

    iget-object v2, v2, LX/JCL;->a:Landroid/content/Context;

    iget-object v3, p1, LX/J9t;->a:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2661882
    return-void

    .line 2661883
    :cond_1
    iget-object v0, p1, LX/J9t;->b:Landroid/os/Bundle;

    goto :goto_0
.end method
