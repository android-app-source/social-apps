.class public LX/ITj;
.super LX/ISH;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:LX/3my;

.field public B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5QP;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/IRb;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DTp;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/B1P;

.field public final g:LX/0ad;

.field public final h:LX/0Uh;

.field private final i:LX/IVn;

.field public final j:LX/DRd;

.field public final k:LX/IVC;

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public n:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

.field public o:LX/ITA;

.field private p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:LX/0iA;

.field public v:Landroid/content/Context;

.field public w:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public x:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public y:LX/DRc;

.field public z:LX/DPp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2579716
    const-class v0, LX/ITj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ITj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/B1P;LX/IRb;LX/DRc;LX/DPp;LX/0ad;LX/0Uh;LX/IVn;LX/DRd;LX/0iA;Landroid/content/Context;LX/3my;LX/IVC;LX/0Ot;LX/0Or;)V
    .locals 3
    .param p5    # LX/IRb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/DRc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DTp;",
            ">;",
            "LX/B1P;",
            "LX/IRb;",
            "LX/DRc;",
            "Lcom/facebook/groups/info/GroupInfoFragment$GroupLeaveActionResponder;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/IVn;",
            "LX/DRd;",
            "LX/0iA;",
            "Landroid/content/Context;",
            "LX/3my;",
            "LX/IVC;",
            "LX/0Ot",
            "<",
            "LX/5QP;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2579694
    invoke-direct {p0}, LX/ISH;-><init>()V

    .line 2579695
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/ITj;->p:LX/0Px;

    .line 2579696
    iput-object p1, p0, LX/ITj;->c:LX/0Ot;

    .line 2579697
    iput-object p7, p0, LX/ITj;->z:LX/DPp;

    .line 2579698
    iput-object p2, p0, LX/ITj;->d:LX/0Ot;

    .line 2579699
    iput-object p3, p0, LX/ITj;->e:LX/0Ot;

    .line 2579700
    iput-object p4, p0, LX/ITj;->f:LX/B1P;

    .line 2579701
    iput-object p5, p0, LX/ITj;->b:LX/IRb;

    .line 2579702
    iput-object p6, p0, LX/ITj;->y:LX/DRc;

    .line 2579703
    iput-object p8, p0, LX/ITj;->g:LX/0ad;

    .line 2579704
    iput-object p9, p0, LX/ITj;->h:LX/0Uh;

    .line 2579705
    iput-object p10, p0, LX/ITj;->i:LX/IVn;

    .line 2579706
    iput-object p11, p0, LX/ITj;->j:LX/DRd;

    .line 2579707
    iput-object p12, p0, LX/ITj;->u:LX/0iA;

    .line 2579708
    move-object/from16 v0, p13

    iput-object v0, p0, LX/ITj;->v:Landroid/content/Context;

    .line 2579709
    move-object/from16 v0, p14

    iput-object v0, p0, LX/ITj;->A:LX/3my;

    .line 2579710
    move-object/from16 v0, p15

    iput-object v0, p0, LX/ITj;->k:LX/IVC;

    .line 2579711
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ITj;->B:LX/0Ot;

    .line 2579712
    move-object/from16 v0, p17

    iput-object v0, p0, LX/ITj;->l:LX/0Or;

    .line 2579713
    iget-object v1, p0, LX/ITj;->f:LX/B1P;

    new-instance v2, LX/ITa;

    invoke-direct {v2, p0}, LX/ITa;-><init>(LX/ITj;)V

    invoke-virtual {v1, v2}, LX/B1P;->a(LX/ITa;)V

    .line 2579714
    return-void
.end method

.method public static a(LX/ITj;Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2579717
    iget-object v0, p0, LX/ITj;->u:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v2, p1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/1XZ;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13G;

    .line 2579718
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2579719
    :goto_0
    return-object v0

    .line 2579720
    :cond_0
    iget-object v2, p0, LX/ITj;->v:Landroid/content/Context;

    invoke-interface {v0, v2}, LX/13G;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 2579721
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2579722
    goto :goto_0

    .line 2579723
    :cond_1
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "qp_definition"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    goto :goto_0
.end method

.method public static b(LX/ITj;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 2

    .prologue
    .line 2579724
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->C()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2579725
    iget-object v0, p0, LX/ITj;->g:LX/0ad;

    sget-short v1, LX/88j;->h:S

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2579726
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/ITj;)Z
    .locals 2

    .prologue
    .line 2579727
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/IQY;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/ITj;->g(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/ITj;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2579728
    iget-object v1, p0, LX/ITj;->h:LX/0Uh;

    const/16 v2, 0x2e8

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->E()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static g(LX/ITj;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2579729
    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->q()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->q()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/39O;->c()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(LX/ITj;)Z
    .locals 3

    .prologue
    .line 2579500
    const/4 v0, 0x0

    .line 2579501
    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2579502
    iget-object v1, p0, LX/ITj;->j:LX/DRd;

    iget-object v2, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/DRd;->a(Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;)Z

    move-result v0

    .line 2579503
    :cond_0
    move v0, v0

    .line 2579504
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DRd;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(LX/ITj;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2579730
    iget-object v2, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2579731
    :cond_0
    :goto_0
    return v0

    .line 2579732
    :cond_1
    iget-object v2, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2579733
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iget-object v3, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2579734
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CANNOT_JOIN_OR_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iget-object v3, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iget-object v3, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2579735
    :cond_2
    iget-object v2, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v2

    .line 2579736
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2579737
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->hI_()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CANNOT_JOIN_OR_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->hI_()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static s(LX/ITj;)Z
    .locals 2

    .prologue
    .line 2579738
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/ITj;->p(LX/ITj;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(LX/ITj;)Z
    .locals 2

    .prologue
    .line 2579739
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ITj;->A:LX/3my;

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3my;->a(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(LX/ITj;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2579740
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/ITj;->v(LX/ITj;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static v(LX/ITj;)Z
    .locals 1

    .prologue
    .line 2579715
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2579692
    sget-object v0, LX/IUI;->I:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2579693
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2579686
    iget-object v0, p0, LX/ITj;->o:LX/ITA;

    if-eqz v0, :cond_0

    .line 2579687
    iget-object v0, p0, LX/ITj;->o:LX/ITA;

    invoke-interface {v0}, LX/ITA;->a()V

    .line 2579688
    :cond_0
    iget-object v0, p0, LX/ITj;->f:LX/B1P;

    .line 2579689
    iget-object p0, v0, LX/B1P;->a:LX/1Ck;

    invoke-virtual {p0}, LX/1Ck;->c()V

    .line 2579690
    const/4 p0, 0x0

    iput-object p0, v0, LX/B1P;->d:LX/ITa;

    .line 2579691
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2579683
    check-cast p2, LX/DMB;

    .line 2579684
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2579685
    return-void
.end method

.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V
    .locals 7

    .prologue
    .line 2579680
    new-instance v6, LX/IVp;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {v6, v0}, LX/IVp;-><init>(LX/0Pz;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 2579681
    invoke-virtual/range {v0 .. v6}, LX/ITj;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZLX/IVp;)V

    .line 2579682
    return-void
.end method

.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZLX/IVp;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
            "ZZZZ",
            "LX/IVp",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2579511
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, LX/ITj;->s:Z

    if-ne v0, p2, :cond_0

    iget-boolean v0, p0, LX/ITj;->q:Z

    if-ne v0, p3, :cond_0

    iget-boolean v0, p0, LX/ITj;->r:Z

    if-ne v0, p4, :cond_0

    if-nez p5, :cond_0

    .line 2579512
    :goto_0
    return-void

    .line 2579513
    :cond_0
    iput-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2579514
    iput-boolean p2, p0, LX/ITj;->s:Z

    .line 2579515
    iput-boolean p3, p0, LX/ITj;->q:Z

    .line 2579516
    if-eqz p3, :cond_1

    if-eqz p4, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/ITj;->r:Z

    .line 2579517
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2579518
    iget-object v0, p0, LX/ITj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/ITj;->a:Ljava/lang/String;

    const-string v2, "Tried to bind a group without a group id"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2579519
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2579520
    :cond_2
    new-instance v0, LX/ITb;

    sget-object v1, LX/IUI;->a:LX/DML;

    invoke-direct {v0, p0, v1}, LX/ITb;-><init>(LX/ITj;LX/DML;)V

    sget-object v1, LX/IVo;->COVER_HEADER:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579521
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_19

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_19

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 2579522
    invoke-static {p0}, LX/ITj;->e(LX/ITj;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, LX/ITj;->f(LX/ITj;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, LX/ITj;->p(LX/ITj;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, LX/ITj;->g(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    :cond_3
    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2579523
    if-nez v0, :cond_1d

    .line 2579524
    new-instance v0, LX/ITc;

    sget-object v1, LX/IUI;->j:LX/DML;

    invoke-direct {v0, p0, v1}, LX/ITc;-><init>(LX/ITj;LX/DML;)V

    sget-object v1, LX/IVo;->ACTION_BAR:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579525
    :cond_4
    :goto_3
    iget-object v0, p0, LX/ITj;->n:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-static {v0}, LX/IV1;->b(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;)I

    move-result v0

    if-lez v0, :cond_5

    .line 2579526
    new-instance v0, LX/ITf;

    sget-object v1, LX/IUI;->l:LX/DML;

    invoke-direct {v0, p0, v1}, LX/ITf;-><init>(LX/ITj;LX/DML;)V

    sget-object v1, LX/IVo;->SUGGESTIONS_CHAIN:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579527
    :cond_5
    invoke-static {p0}, LX/ITj;->p(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2579528
    new-instance v0, LX/ITg;

    sget-object v1, LX/IUI;->q:LX/DML;

    invoke-direct {v0, p0, v1}, LX/ITg;-><init>(LX/ITj;LX/DML;)V

    sget-object v1, LX/IVo;->COMMUNITY_EMAIL_VERIFICATION:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579529
    :cond_6
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2579530
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {p0}, LX/ITj;->g(LX/ITj;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    :cond_8
    move p2, v2

    .line 2579531
    :goto_4
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    invoke-static {p0}, LX/ITj;->g(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_21

    :cond_a
    move v0, v2

    .line 2579532
    :goto_5
    invoke-static {p0, p1}, LX/ITj;->b(LX/ITj;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result p3

    .line 2579533
    if-eqz p2, :cond_22

    if-nez v0, :cond_b

    if-eqz p3, :cond_22

    :cond_b
    :goto_6
    move v0, v2

    .line 2579534
    if-eqz v0, :cond_e

    .line 2579535
    invoke-static {p0}, LX/ITj;->g(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2579536
    new-instance v0, LX/ITh;

    sget-object v1, LX/IUI;->n:LX/DML;

    invoke-direct {v0, p0, v1, p1}, LX/ITh;-><init>(LX/ITj;LX/DML;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    sget-object v1, LX/IVo;->DESCRIPTION:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579537
    :cond_c
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {p0, p1}, LX/ITj;->b(LX/ITj;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2579538
    :cond_d
    new-instance v0, LX/ITi;

    sget-object v1, LX/IUI;->n:LX/DML;

    invoke-direct {v0, p0, v1, p1}, LX/ITi;-><init>(LX/ITj;LX/DML;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    sget-object v1, LX/IVo;->COMMUNITY_DESCRIPTION:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579539
    :cond_e
    invoke-static {p0}, LX/ITj;->t(LX/ITj;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2579540
    invoke-static {p0}, LX/ITj;->s(LX/ITj;)Z

    move-result v0

    if-nez v0, :cond_f

    invoke-static {p0}, LX/ITj;->t(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_24

    :cond_f
    const/4 v0, 0x1

    :goto_7
    move v0, v0

    .line 2579541
    if-eqz v0, :cond_10

    invoke-static {p0}, LX/ITj;->s(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    :cond_10
    const/4 v0, 0x1

    :goto_8
    move v0, v0

    .line 2579542
    if-eqz v0, :cond_19

    .line 2579543
    iget-object v0, p0, LX/ITj;->i:LX/IVn;

    iget-object v1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2579544
    iput-object v1, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2579545
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_11

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-nez v2, :cond_11

    .line 2579546
    iget-object v2, v0, LX/IVn;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    sget-object p1, LX/IVn;->a:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "FetchGroupInformationModel\'s admin aware group is null: "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p3, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579547
    :cond_11
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2579548
    iget-object v2, v0, LX/IVn;->c:LX/DOK;

    if-eqz v2, :cond_2b

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_2b

    iget-object v2, v0, LX/IVn;->c:LX/DOK;

    iget-object p1, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, LX/DOK;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2b

    const/4 v2, 0x1

    :goto_9
    move v2, v2

    .line 2579549
    if-nez v2, :cond_25

    .line 2579550
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_12

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_12

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->A()I

    move-result v2

    if-lez v2, :cond_12

    .line 2579551
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->FRIENDS_IN_GROUP:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2579552
    :cond_12
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->PRIVACY:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2579553
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_13

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_13

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->w()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_15

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v2

    if-eqz v2, :cond_13

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel$InviterModel;

    move-result-object v2

    if-nez v2, :cond_15

    .line 2579554
    :cond_13
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2, p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2, p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2, p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    :cond_14
    const/4 v2, 0x1

    :goto_a
    move v2, v2

    .line 2579555
    if-eqz v2, :cond_2c

    :cond_15
    const/4 v2, 0x1

    :goto_b
    move v2, v2

    .line 2579556
    if-eqz v2, :cond_16

    .line 2579557
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->ABOUT:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2579558
    :cond_16
    :goto_c
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_2e

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v2

    if-eqz v2, :cond_2e

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->M()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPinnedPostModel$GroupPinnedStoriesModel;->a()I

    move-result v2

    if-lez v2, :cond_2e

    const/4 v2, 0x1

    :goto_d
    move v2, v2

    .line 2579559
    if-eqz v2, :cond_17

    .line 2579560
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->PINNED_POST:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2579561
    :cond_17
    const/4 p2, 0x0

    .line 2579562
    move p1, p2

    :goto_e
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_19

    .line 2579563
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_18

    const/4 v2, 0x1

    :goto_f
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    .line 2579564
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IVk;

    .line 2579565
    new-instance p4, LX/IVm;

    sget-object v1, LX/IUI;->b:LX/DML;

    invoke-direct {p4, v0, v1, v2, p3}, LX/IVm;-><init>(LX/IVn;LX/DML;LX/IVk;Ljava/lang/Boolean;)V

    .line 2579566
    invoke-virtual {p6, p4, v2}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579567
    add-int/lit8 v2, p1, 0x1

    move p1, v2

    goto :goto_e

    :cond_18
    move v2, p2

    .line 2579568
    goto :goto_f

    .line 2579569
    :cond_19
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_1a

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    if-eqz v0, :cond_1a

    iget-boolean v0, p0, LX/ITj;->s:Z

    if-eqz v0, :cond_1a

    .line 2579570
    const/4 v2, 0x0

    .line 2579571
    iget-object v0, p0, LX/ITj;->w:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-eqz v0, :cond_3d

    iget-object v0, p0, LX/ITj;->x:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    if-eqz v0, :cond_3d

    .line 2579572
    iget-object v1, p0, LX/ITj;->w:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 2579573
    iget-object v0, p0, LX/ITj;->x:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 2579574
    :goto_10
    if-eqz v1, :cond_1a

    .line 2579575
    new-instance v2, LX/DaP;

    sget-object p1, LX/IUI;->v:LX/DML;

    invoke-direct {v2, p1}, LX/DaP;-><init>(LX/DML;)V

    sget-object p1, LX/IVo;->THIN_BORDER:LX/IVo;

    invoke-virtual {p6, v2, p1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579576
    new-instance v2, LX/ITZ;

    sget-object p1, LX/IUI;->w:LX/DML;

    invoke-direct {v2, p0, p1, v1, v0}, LX/ITZ;-><init>(LX/ITj;LX/DML;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    sget-object v0, LX/IVo;->QUICK_PROMOTION:LX/IVo;

    invoke-virtual {p6, v2, v0}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579577
    :cond_1a
    invoke-static {p0}, LX/ITj;->j(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2579578
    new-instance v0, LX/ITV;

    sget-object v1, LX/IUI;->f:LX/DML;

    invoke-direct {v0, p0, v1}, LX/ITV;-><init>(LX/ITj;LX/DML;)V

    sget-object v1, LX/IVo;->LEARNING_TAB_BAR:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579579
    iget-object v0, p0, LX/ITj;->y:LX/DRc;

    if-eqz v0, :cond_1b

    .line 2579580
    iget-object v0, p0, LX/ITj;->y:LX/DRc;

    .line 2579581
    iget v1, p6, LX/IVp;->d:I

    move v1, v1

    .line 2579582
    add-int/lit8 v1, v1, -0x1

    .line 2579583
    iput v1, v0, LX/DRc;->g:I

    .line 2579584
    :cond_1b
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2579585
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_51

    .line 2579586
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2579587
    if-eqz v0, :cond_50

    move v0, v1

    :goto_11
    if-eqz v0, :cond_53

    .line 2579588
    iget-object v0, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->A()LX/1vs;

    move-result-object v0

    iget-object p1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2579589
    invoke-virtual {p1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    if-eqz v0, :cond_52

    move v0, v1

    :goto_12
    if-eqz v0, :cond_54

    iget-object v0, p0, LX/ITj;->g:LX/0ad;

    sget-short p1, LX/88j;->i:S

    invoke-interface {v0, p1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 2579590
    invoke-static {p0}, LX/ITj;->j(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_55

    iget-object v0, p0, LX/ITj;->y:LX/DRc;

    if-eqz v0, :cond_55

    iget-object v0, p0, LX/ITj;->y:LX/DRc;

    invoke-virtual {v0}, LX/DRc;->c()Z

    move-result v0

    if-eqz v0, :cond_55

    const/4 v0, 0x1

    :goto_13
    move v0, v0

    .line 2579591
    if-nez v0, :cond_54

    :goto_14
    move v0, v1

    .line 2579592
    if-eqz v0, :cond_1c

    .line 2579593
    new-instance v0, LX/ITW;

    sget-object v1, LX/IUI;->g:LX/DML;

    invoke-direct {v0, p0, v1}, LX/ITW;-><init>(LX/ITj;LX/DML;)V

    sget-object v1, LX/IVo;->DISCUSSION_TOPICS_BAR:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2579594
    :cond_1c
    invoke-virtual {p6}, LX/IVp;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/ITj;->p:LX/0Px;

    .line 2579595
    const v0, -0x76c8fd97

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_0

    .line 2579596
    :cond_1d
    invoke-static {p0}, LX/ITj;->f(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 2579597
    new-instance v0, LX/ITd;

    sget-object v1, LX/IUI;->m:LX/DML;

    invoke-direct {v0, p0, v1}, LX/ITd;-><init>(LX/ITj;LX/DML;)V

    sget-object v1, LX/IVo;->MULIT_COMPANY_INVITE_REDEMPTION:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    goto/16 :goto_3

    .line 2579598
    :cond_1e
    invoke-static {p0}, LX/ITj;->e(LX/ITj;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2579599
    new-instance v0, LX/ITe;

    sget-object v1, LX/IUI;->k:LX/DML;

    invoke-direct {v0, p0, v1, p1}, LX/ITe;-><init>(LX/ITj;LX/DML;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    sget-object v1, LX/IVo;->JOIN:LX/IVo;

    invoke-virtual {p6, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    goto/16 :goto_3

    :cond_1f
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_20
    move p2, v1

    .line 2579600
    goto/16 :goto_4

    :cond_21
    move v0, v1

    .line 2579601
    goto/16 :goto_5

    :cond_22
    move v2, v1

    .line 2579602
    goto/16 :goto_6

    :cond_23
    const/4 v0, 0x0

    goto/16 :goto_8

    :cond_24
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 2579603
    :cond_25
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_2f

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_2f

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v2

    if-eqz v2, :cond_2f

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2f

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->P()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2f

    const/4 v2, 0x1

    :goto_15
    move v2, v2

    .line 2579604
    if-eqz v2, :cond_26

    .line 2579605
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->UPCOMING_EVENT:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2579606
    :cond_26
    const/4 p1, 0x1

    const/4 p2, 0x0

    .line 2579607
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_31

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_31

    .line 2579608
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->r()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2579609
    if-eqz v2, :cond_30

    move v2, p1

    :goto_16
    if-eqz v2, :cond_33

    .line 2579610
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->r()LX/1vs;

    move-result-object v2

    iget-object p3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2579611
    invoke-virtual {p3, v2, p2}, LX/15i;->j(II)I

    move-result v2

    if-lez v2, :cond_32

    :goto_17
    move v2, p1

    .line 2579612
    if-eqz v2, :cond_27

    .line 2579613
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->REPORTED_POST:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2579614
    :cond_27
    const/4 p1, 0x1

    const/4 p2, 0x0

    .line 2579615
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_35

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_35

    .line 2579616
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->p()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2579617
    if-eqz v2, :cond_34

    move v2, p1

    :goto_18
    if-eqz v2, :cond_37

    .line 2579618
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->p()LX/1vs;

    move-result-object v2

    iget-object p3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2579619
    invoke-virtual {p3, v2, p2}, LX/15i;->j(II)I

    move-result v2

    if-lez v2, :cond_36

    :goto_19
    move v2, p1

    .line 2579620
    if-eqz v2, :cond_28

    .line 2579621
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->MEMBER_REQUESTS:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2579622
    :cond_28
    const/4 p1, 0x1

    const/4 p2, 0x0

    .line 2579623
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_39

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_39

    .line 2579624
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->q()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2579625
    if-eqz v2, :cond_38

    move v2, p1

    :goto_1a
    if-eqz v2, :cond_3b

    .line 2579626
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->q()LX/1vs;

    move-result-object v2

    iget-object p3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2579627
    invoke-virtual {p3, v2, p2}, LX/15i;->j(II)I

    move-result v2

    if-lez v2, :cond_3a

    :goto_1b
    move v2, p1

    .line 2579628
    if-eqz v2, :cond_29

    .line 2579629
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->PENDING_POST:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2579630
    :cond_29
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v2}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v2

    invoke-static {v2}, LX/DJw;->a(LX/DZC;)Z

    move-result v2

    .line 2579631
    iget-object p1, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz p1, :cond_3c

    iget-object p1, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object p1

    if-eqz p1, :cond_3c

    if-eqz v2, :cond_2a

    .line 2579632
    iget-object p1, v0, LX/IVn;->b:LX/0ad;

    sget-short p2, LX/DIp;->a:S

    const/4 p3, 0x0

    invoke-interface {p1, p2, p3}, LX/0ad;->a(SZ)Z

    move-result p1

    move p1, p1

    .line 2579633
    if-eqz p1, :cond_3c

    if-eqz v2, :cond_3c

    :cond_2a
    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v2

    if-eqz v2, :cond_3c

    iget-object v2, v0, LX/IVn;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->a()I

    move-result v2

    if-lez v2, :cond_3c

    const/4 v2, 0x1

    :goto_1c
    move v2, v2

    .line 2579634
    if-eqz v2, :cond_16

    .line 2579635
    iget-object v2, v0, LX/IVn;->g:Ljava/util/List;

    sget-object p1, LX/IVk;->YOUR_SALE_POSTS:LX/IVk;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c

    :cond_2b
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_2c
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_2d
    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_2e
    const/4 v2, 0x0

    goto/16 :goto_d

    :cond_2f
    const/4 v2, 0x0

    goto/16 :goto_15

    :cond_30
    move v2, p2

    goto/16 :goto_16

    :cond_31
    move v2, p2

    goto/16 :goto_16

    :cond_32
    move p1, p2

    goto/16 :goto_17

    :cond_33
    move p1, p2

    goto/16 :goto_17

    :cond_34
    move v2, p2

    goto/16 :goto_18

    :cond_35
    move v2, p2

    goto/16 :goto_18

    :cond_36
    move p1, p2

    goto/16 :goto_19

    :cond_37
    move p1, p2

    goto/16 :goto_19

    :cond_38
    move v2, p2

    goto/16 :goto_1a

    :cond_39
    move v2, p2

    goto/16 :goto_1a

    :cond_3a
    move p1, p2

    goto/16 :goto_1b

    :cond_3b
    move p1, p2

    goto/16 :goto_1b

    :cond_3c
    const/4 v2, 0x0

    goto :goto_1c

    .line 2579636
    :cond_3d
    const/4 v0, 0x0

    .line 2579637
    invoke-static {p0}, LX/ITj;->u(LX/ITj;)Z

    move-result v1

    if-eqz v1, :cond_3e

    iget-object v1, p0, LX/ITj;->h:LX/0Uh;

    const/16 p1, 0x4ee

    invoke-virtual {v1, p1, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3e

    const/4 v0, 0x1

    :cond_3e
    move v0, v0

    .line 2579638
    if-eqz v0, :cond_49

    .line 2579639
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ADS_ELIGIBLE_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 2579640
    invoke-static {p0, v0}, LX/ITj;->a(LX/ITj;Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v1

    .line 2579641
    :goto_1d
    if-nez v1, :cond_40

    const/4 p1, 0x0

    .line 2579642
    invoke-static {p0}, LX/ITj;->u(LX/ITj;)Z

    move-result p2

    if-eqz p2, :cond_3f

    iget-object p2, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object p2

    if-eqz p2, :cond_3f

    iget-object p2, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object p2

    sget-object p3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne p2, p3, :cond_3f

    iget-object p2, p0, LX/ITj;->h:LX/0Uh;

    const/16 p3, 0x4ed

    invoke-virtual {p2, p3, p1}, LX/0Uh;->a(IZ)Z

    move-result p2

    if-eqz p2, :cond_3f

    const/4 p1, 0x1

    :cond_3f
    move p1, p1

    .line 2579643
    if-eqz p1, :cond_40

    .line 2579644
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ADMIN_GROUP_ADS_ELIGIBLE_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 2579645
    invoke-static {p0, v0}, LX/ITj;->a(LX/ITj;Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v1

    .line 2579646
    :cond_40
    if-nez v1, :cond_42

    .line 2579647
    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz p1, :cond_41

    invoke-static {p0}, LX/ITj;->v(LX/ITj;)Z

    move-result p1

    if-eqz p1, :cond_4a

    .line 2579648
    :cond_41
    const/4 p1, 0x0

    .line 2579649
    :goto_1e
    move p1, p1

    .line 2579650
    if-eqz p1, :cond_42

    .line 2579651
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SPECIFIC_IDS_GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 2579652
    invoke-static {p0, v0}, LX/ITj;->a(LX/ITj;Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v1

    .line 2579653
    :cond_42
    if-nez v1, :cond_43

    .line 2579654
    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz p1, :cond_4b

    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object p1

    if-eqz p1, :cond_4b

    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object p1

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne p1, p2, :cond_4b

    const/4 p1, 0x1

    :goto_1f
    move p1, p1

    .line 2579655
    if-eqz p1, :cond_43

    .line 2579656
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ADMIN_GROUP_MALL_MULTITIER_ENABLED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 2579657
    invoke-static {p0, v0}, LX/ITj;->a(LX/ITj;Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v1

    .line 2579658
    :cond_43
    if-nez v1, :cond_44

    .line 2579659
    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz p1, :cond_4c

    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object p1

    if-eqz p1, :cond_4c

    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object p1

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne p1, p2, :cond_4c

    const/4 p1, 0x1

    :goto_20
    move p1, p1

    .line 2579660
    if-eqz p1, :cond_44

    .line 2579661
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MOD_GROUP_MALL_MULTITIER_ENABLED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 2579662
    invoke-static {p0, v0}, LX/ITj;->a(LX/ITj;Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v1

    .line 2579663
    :cond_44
    if-nez v1, :cond_46

    const/4 p2, 0x0

    .line 2579664
    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz p1, :cond_45

    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object p1

    if-eqz p1, :cond_45

    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;->a()LX/0Px;

    move-result-object p1

    if-nez p1, :cond_4d

    :cond_45
    move p1, p2

    .line 2579665
    :goto_21
    move p1, p1

    .line 2579666
    if-nez p1, :cond_46

    .line 2579667
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MEMBER_BIO_ELIGIBLE_GROUP_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 2579668
    invoke-static {p0, v0}, LX/ITj;->a(LX/ITj;Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v1

    .line 2579669
    :cond_46
    if-nez v1, :cond_47

    .line 2579670
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 2579671
    invoke-static {p0, v0}, LX/ITj;->a(LX/ITj;Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v1

    .line 2579672
    :cond_47
    iput-object v1, p0, LX/ITj;->w:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 2579673
    if-eqz v1, :cond_48

    if-eqz v0, :cond_48

    .line 2579674
    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v2, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    iput-object v2, p0, LX/ITj;->x:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-object v0, v2

    goto/16 :goto_10

    :cond_48
    move-object v0, v2

    goto/16 :goto_10

    :cond_49
    move-object v0, v2

    move-object v1, v2

    goto/16 :goto_1d

    :cond_4a
    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object p1

    const-string p2, "gk_gk_group_survey"

    invoke-static {p1, p2}, LX/88m;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;Ljava/lang/String;)Z

    move-result p1

    goto/16 :goto_1e

    :cond_4b
    const/4 p1, 0x0

    goto :goto_1f

    :cond_4c
    const/4 p1, 0x0

    goto :goto_20

    .line 2579675
    :cond_4d
    iget-object p1, p0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->L()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel;->a()LX/0Px;

    move-result-object p4

    invoke-virtual {p4}, LX/0Px;->size()I

    move-result p5

    move p3, p2

    :goto_22
    if-ge p3, p5, :cond_4f

    invoke-virtual {p4, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel$EdgesModel;

    .line 2579676
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerMemberBioInformationModel$GroupMemberProfilesModel$EdgesModel$BioStoryModel;

    move-result-object p1

    if-eqz p1, :cond_4e

    .line 2579677
    const/4 p1, 0x1

    goto :goto_21

    .line 2579678
    :cond_4e
    add-int/lit8 p1, p3, 0x1

    move p3, p1

    goto :goto_22

    :cond_4f
    move p1, p2

    .line 2579679
    goto :goto_21

    :cond_50
    move v0, v2

    goto/16 :goto_11

    :cond_51
    move v0, v2

    goto/16 :goto_11

    :cond_52
    move v0, v2

    goto/16 :goto_12

    :cond_53
    move v0, v2

    goto/16 :goto_12

    :cond_54
    move v1, v2

    goto/16 :goto_14

    :cond_55
    const/4 v0, 0x0

    goto/16 :goto_13
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2579510
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2579509
    iget-object v0, p0, LX/ITj;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2579508
    iget-object v0, p0, LX/ITj;->p:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2579507
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2579506
    sget-object v1, LX/IUI;->I:LX/0Px;

    iget-object v0, p0, LX/ITj;->p:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2579505
    sget-object v0, LX/IUI;->I:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
