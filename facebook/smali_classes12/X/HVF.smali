.class public LX/HVF;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:J


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HDT;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/HSx;

.field private m:LX/CXj;

.field private n:LX/01T;

.field private o:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2474955
    sget-wide v0, LX/0X5;->j:J

    sput-wide v0, LX/HVF;->a:J

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/HSx;LX/CXj;LX/01T;LX/0W3;)V
    .locals 0
    .param p9    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HDT;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;",
            "LX/HSx;",
            "LX/CXj;",
            "LX/01T;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2474956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2474957
    iput-object p1, p0, LX/HVF;->b:LX/0Ot;

    .line 2474958
    iput-object p2, p0, LX/HVF;->c:LX/0Ot;

    .line 2474959
    iput-object p3, p0, LX/HVF;->d:LX/0Ot;

    .line 2474960
    iput-object p4, p0, LX/HVF;->e:LX/0Ot;

    .line 2474961
    iput-object p5, p0, LX/HVF;->f:LX/0Ot;

    .line 2474962
    iput-object p6, p0, LX/HVF;->g:LX/0Ot;

    .line 2474963
    iput-object p7, p0, LX/HVF;->h:LX/0Ot;

    .line 2474964
    iput-object p8, p0, LX/HVF;->i:LX/0Ot;

    .line 2474965
    iput-object p9, p0, LX/HVF;->j:LX/0Ot;

    .line 2474966
    iput-object p10, p0, LX/HVF;->k:LX/0Ot;

    .line 2474967
    iput-object p11, p0, LX/HVF;->l:LX/HSx;

    .line 2474968
    iput-object p12, p0, LX/HVF;->m:LX/CXj;

    .line 2474969
    iput-object p13, p0, LX/HVF;->n:LX/01T;

    .line 2474970
    iput-object p14, p0, LX/HVF;->o:LX/0W3;

    .line 2474971
    return-void
.end method
