.class public LX/IFJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/IFJ;


# instance fields
.field public final a:LX/EnT;

.field private final b:LX/EnR;


# direct methods
.method public constructor <init>(LX/EnT;LX/EnR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2553994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2553995
    iput-object p1, p0, LX/IFJ;->a:LX/EnT;

    .line 2553996
    iput-object p2, p0, LX/IFJ;->b:LX/EnR;

    .line 2553997
    return-void
.end method

.method public static a(LX/0QB;)LX/IFJ;
    .locals 5

    .prologue
    .line 2553998
    sget-object v0, LX/IFJ;->c:LX/IFJ;

    if-nez v0, :cond_1

    .line 2553999
    const-class v1, LX/IFJ;

    monitor-enter v1

    .line 2554000
    :try_start_0
    sget-object v0, LX/IFJ;->c:LX/IFJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2554001
    if-eqz v2, :cond_0

    .line 2554002
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2554003
    new-instance p0, LX/IFJ;

    invoke-static {v0}, LX/EnT;->a(LX/0QB;)LX/EnT;

    move-result-object v3

    check-cast v3, LX/EnT;

    invoke-static {v0}, LX/EnR;->a(LX/0QB;)LX/EnR;

    move-result-object v4

    check-cast v4, LX/EnR;

    invoke-direct {p0, v3, v4}, LX/IFJ;-><init>(LX/EnT;LX/EnR;)V

    .line 2554004
    move-object v0, p0

    .line 2554005
    sput-object v0, LX/IFJ;->c:LX/IFJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2554006
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2554007
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2554008
    :cond_1
    sget-object v0, LX/IFJ;->c:LX/IFJ;

    return-object v0

    .line 2554009
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2554010
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
