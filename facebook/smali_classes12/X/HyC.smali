.class public final LX/HyC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final synthetic b:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 0

    .prologue
    .line 2523689
    iput-object p1, p0, LX/HyC;->b:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    iput-object p2, p0, LX/HyC;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x4a31d909

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2523690
    iget-object v1, p0, LX/HyC;->b:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->l:LX/HyD;

    if-eqz v1, :cond_0

    .line 2523691
    iget-object v1, p0, LX/HyC;->b:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->l:LX/HyD;

    iget-object v2, p0, LX/HyC;->b:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->b:Lcom/facebook/events/model/Event;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_INLINE_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    iget-object v4, p0, LX/HyC;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-interface {v1, v2, v3, v4}, LX/HyD;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 2523692
    :cond_0
    const v1, 0x754f654f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
