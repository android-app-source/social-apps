.class public LX/IPt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IPn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bcw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2574805
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2574806
    iput-object v0, p0, LX/IPt;->a:LX/0Ot;

    .line 2574807
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2574808
    iput-object v0, p0, LX/IPt;->b:LX/0Ot;

    .line 2574809
    return-void
.end method

.method public static a(LX/0QB;)LX/IPt;
    .locals 5

    .prologue
    .line 2574810
    const-class v1, LX/IPt;

    monitor-enter v1

    .line 2574811
    :try_start_0
    sget-object v0, LX/IPt;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2574812
    sput-object v2, LX/IPt;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2574813
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2574814
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2574815
    new-instance v3, LX/IPt;

    invoke-direct {v3}, LX/IPt;-><init>()V

    .line 2574816
    const/16 v4, 0x23f8

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1943

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2574817
    iput-object v4, v3, LX/IPt;->a:LX/0Ot;

    iput-object p0, v3, LX/IPt;->b:LX/0Ot;

    .line 2574818
    move-object v0, v3

    .line 2574819
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2574820
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IPt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574821
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2574822
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(ZLX/BcL;Ljava/lang/Throwable;LX/BcP;)V
    .locals 3

    .prologue
    .line 2574823
    sget-object v0, LX/IPs;->a:[I

    invoke-virtual {p1}, LX/BcL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2574824
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wrong loading state is given "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2574825
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {p3, v0}, LX/IPr;->a(LX/BcP;Z)V

    .line 2574826
    :goto_0
    invoke-static {p3, p0, p1, p2}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 2574827
    return-void

    .line 2574828
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p3, v0}, LX/IPr;->a(LX/BcP;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
