.class public final LX/HZN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/registration/fragment/RegistrationGenderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationGenderFragment;)V
    .locals 0

    .prologue
    .line 2482161
    iput-object p1, p0, LX/HZN;->a:Lcom/facebook/registration/fragment/RegistrationGenderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    .prologue
    .line 2482162
    const v0, 0x7f0d041b

    if-ne p2, v0, :cond_0

    .line 2482163
    iget-object v0, p0, LX/HZN;->a:Lcom/facebook/registration/fragment/RegistrationGenderFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v1, LX/F8q;->MALE:LX/F8q;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setGender(LX/F8q;)V

    .line 2482164
    :goto_0
    return-void

    .line 2482165
    :cond_0
    iget-object v0, p0, LX/HZN;->a:Lcom/facebook/registration/fragment/RegistrationGenderFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v1, LX/F8q;->FEMALE:LX/F8q;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setGender(LX/F8q;)V

    goto :goto_0
.end method
