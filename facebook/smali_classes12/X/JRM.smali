.class public LX/JRM;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JRN;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JRM",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JRN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693255
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2693256
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JRM;->b:LX/0Zi;

    .line 2693257
    iput-object p1, p0, LX/JRM;->a:LX/0Ot;

    .line 2693258
    return-void
.end method

.method public static a(LX/0QB;)LX/JRM;
    .locals 4

    .prologue
    .line 2693240
    const-class v1, LX/JRM;

    monitor-enter v1

    .line 2693241
    :try_start_0
    sget-object v0, LX/JRM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693242
    sput-object v2, LX/JRM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693243
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693244
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693245
    new-instance v3, LX/JRM;

    const/16 p0, 0x2027

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JRM;-><init>(LX/0Ot;)V

    .line 2693246
    move-object v0, v3

    .line 2693247
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693248
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693249
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2693251
    check-cast p2, LX/JRL;

    .line 2693252
    iget-object v0, p0, LX/JRM;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JRN;

    iget-object v1, p2, LX/JRL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JRL;->b:LX/1Pk;

    iget-object v3, p2, LX/JRL;->c:Ljava/lang/String;

    .line 2693253
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b0050

    invoke-virtual {v5, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0a00d6

    invoke-virtual {v5, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 p0, 0x6

    const p2, 0x7f0b010f

    invoke-interface {v5, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    const/4 p0, 0x1

    const p2, 0x7f0b25aa

    invoke-interface {v5, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v5, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JRN;->a:LX/1vb;

    invoke-virtual {v5, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v5

    sget-object p0, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-virtual {v5, p0}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v5

    invoke-interface {v2}, LX/1Pk;->e()LX/1SX;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2693254
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2693230
    invoke-static {}, LX/1dS;->b()V

    .line 2693231
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/JRK;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/JRM",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2693232
    new-instance v1, LX/JRL;

    invoke-direct {v1, p0}, LX/JRL;-><init>(LX/JRM;)V

    .line 2693233
    iget-object v2, p0, LX/JRM;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JRK;

    .line 2693234
    if-nez v2, :cond_0

    .line 2693235
    new-instance v2, LX/JRK;

    invoke-direct {v2, p0}, LX/JRK;-><init>(LX/JRM;)V

    .line 2693236
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/JRK;->a$redex0(LX/JRK;LX/1De;IILX/JRL;)V

    .line 2693237
    move-object v1, v2

    .line 2693238
    move-object v0, v1

    .line 2693239
    return-object v0
.end method
