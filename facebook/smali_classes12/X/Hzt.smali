.class public LX/Hzt;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1Db;

.field private b:Landroid/content/Context;

.field public c:LX/I2o;

.field private d:LX/I2p;

.field private e:Lcom/facebook/events/common/EventAnalyticsParams;

.field public f:LX/I2i;

.field public g:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:LX/1My;

.field public i:LX/1Qq;

.field public j:LX/1DS;

.field private k:LX/1Jg;

.field private l:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

.field private m:LX/2jY;

.field private n:LX/2ja;

.field private o:LX/Hx7;

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Hx7;Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;LX/2jY;LX/2ja;LX/I2p;LX/I2i;LX/1My;LX/1DL;LX/1DS;LX/1Db;LX/0Ot;)V
    .locals 1
    .param p1    # LX/Hx7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/2ja;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Hx7;",
            "Landroid/content/Context;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;",
            "LX/2jY;",
            "LX/2ja;",
            "LX/I2p;",
            "LX/I2i;",
            "LX/1My;",
            "LX/1DL;",
            "LX/1DS;",
            "LX/1Db;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2526325
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2526326
    iput-object p2, p0, LX/Hzt;->b:Landroid/content/Context;

    .line 2526327
    iput-object p4, p0, LX/Hzt;->l:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    .line 2526328
    iput-object p5, p0, LX/Hzt;->m:LX/2jY;

    .line 2526329
    iput-object p7, p0, LX/Hzt;->d:LX/I2p;

    .line 2526330
    iput-object p8, p0, LX/Hzt;->f:LX/I2i;

    .line 2526331
    new-instance v0, LX/Hzs;

    invoke-direct {v0, p0}, LX/Hzs;-><init>(LX/Hzt;)V

    move-object v0, v0

    .line 2526332
    iput-object v0, p0, LX/Hzt;->g:LX/0TF;

    .line 2526333
    iput-object p9, p0, LX/Hzt;->h:LX/1My;

    .line 2526334
    invoke-virtual {p10}, LX/1DL;->a()LX/1Jg;

    move-result-object v0

    iput-object v0, p0, LX/Hzt;->k:LX/1Jg;

    .line 2526335
    iput-object p11, p0, LX/Hzt;->j:LX/1DS;

    .line 2526336
    iput-object p12, p0, LX/Hzt;->a:LX/1Db;

    .line 2526337
    iput-object p13, p0, LX/Hzt;->p:LX/0Ot;

    .line 2526338
    iput-object p6, p0, LX/Hzt;->n:LX/2ja;

    .line 2526339
    iput-object p3, p0, LX/Hzt;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2526340
    invoke-static {p0}, LX/Hzt;->g(LX/Hzt;)V

    .line 2526341
    iget-object v0, p0, LX/Hzt;->i:LX/1Qq;

    if-nez v0, :cond_0

    .line 2526342
    iget-object v0, p0, LX/Hzt;->j:LX/1DS;

    iget-object p2, p0, LX/Hzt;->p:LX/0Ot;

    iget-object p3, p0, LX/Hzt;->f:LX/I2i;

    invoke-virtual {v0, p2, p3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    iget-object p2, p0, LX/Hzt;->c:LX/I2o;

    .line 2526343
    iput-object p2, v0, LX/1Ql;->f:LX/1PW;

    .line 2526344
    move-object v0, v0

    .line 2526345
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/Hzt;->i:LX/1Qq;

    .line 2526346
    :cond_0
    invoke-virtual {p0, p1}, LX/Hzt;->a(LX/Hx7;)V

    .line 2526347
    return-void
.end method

.method private b(LX/I05;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2526314
    iget-object v3, p0, LX/Hzt;->f:LX/I2i;

    sget-object v0, LX/I05;->ERROR:LX/I05;

    if-ne p1, v0, :cond_0

    move v0, v1

    .line 2526315
    :goto_0
    iget-boolean p1, v3, LX/I2i;->i:Z

    if-eq v0, p1, :cond_2

    .line 2526316
    iput-boolean v0, v3, LX/I2i;->i:Z

    .line 2526317
    invoke-static {v3}, LX/I2i;->a(LX/I2i;)V

    .line 2526318
    const/4 p1, 0x1

    .line 2526319
    :goto_1
    move v0, p1

    .line 2526320
    if-eqz v0, :cond_1

    .line 2526321
    invoke-static {p0}, LX/Hzt;->k(LX/Hzt;)V

    .line 2526322
    :goto_2
    return v1

    :cond_0
    move v0, v2

    .line 2526323
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2526324
    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    goto :goto_1
.end method

.method public static g(LX/Hzt;)V
    .locals 21

    .prologue
    .line 2526311
    move-object/from16 v0, p0

    iget-object v15, v0, LX/Hzt;->d:LX/I2p;

    const-string v17, "event_dashboard"

    move-object/from16 v0, p0

    iget-object v0, v0, LX/Hzt;->b:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static {}, LX/I6Y;->b()LX/I6Y;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/Hzt;->k:LX/1Jg;

    invoke-static/range {p0 .. p0}, LX/Hzt;->i(LX/Hzt;)LX/1PY;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/Hzt;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/Hzt;->l:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/Hzt;->n:LX/2ja;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/Hzt;->m:LX/2jY;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/Hzt;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/Hzt;->l:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    new-instance v1, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Hzt;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/Hzt;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Hzt;->m:LX/2jY;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Hzt;->m:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v4

    :goto_0
    const-string v5, "unknown"

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v0, v0, LX/Hzt;->f:LX/I2i;

    move-object/from16 v16, v0

    move-object v2, v15

    move-object/from16 v3, v17

    move-object/from16 v4, v18

    move-object/from16 v5, v19

    move-object/from16 v6, v20

    move-object v15, v1

    invoke-virtual/range {v2 .. v16}, LX/I2p;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1Jg;LX/1PY;Landroid/content/Context;LX/0o8;LX/2ja;LX/2jY;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/I2Y;)LX/I2o;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/Hzt;->c:LX/I2o;

    .line 2526312
    return-void

    .line 2526313
    :cond_0
    const-string v4, "unknown"

    goto :goto_0
.end method

.method private static i(LX/Hzt;)LX/1PY;
    .locals 1

    .prologue
    .line 2526274
    new-instance v0, LX/Hzr;

    invoke-direct {v0, p0}, LX/Hzr;-><init>(LX/Hzt;)V

    return-object v0
.end method

.method public static k(LX/Hzt;)V
    .locals 1

    .prologue
    .line 2526308
    iget-object v0, p0, LX/Hzt;->i:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2526309
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2526310
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2526306
    iget-object v0, p0, LX/Hzt;->i:LX/1Qq;

    invoke-interface {v0, p2, p1}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2526307
    new-instance v1, LX/Hyz;

    invoke-direct {v1, v0, p1, p2}, LX/Hyz;-><init>(Landroid/view/View;Landroid/view/ViewGroup;I)V

    return-object v1
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 2526348
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 2526349
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    .line 2526350
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2526291
    check-cast p1, LX/Hyz;

    .line 2526292
    iget-object v0, p0, LX/Hzt;->i:LX/1Qq;

    iget-object v1, p0, LX/Hzt;->i:LX/1Qq;

    invoke-interface {v1, p2}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    .line 2526293
    iget v1, p1, LX/Hyz;->m:I

    move v4, v1

    .line 2526294
    iget-object v1, p1, LX/Hyz;->l:Landroid/view/ViewGroup;

    move-object v5, v1

    .line 2526295
    move v1, p2

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 2526296
    iget-object v0, p0, LX/Hzt;->i:LX/1Qq;

    invoke-interface {v0, p2}, LX/1Qr;->h_(I)I

    move-result v1

    .line 2526297
    if-ltz v1, :cond_0

    iget-object v0, p0, LX/Hzt;->f:LX/I2i;

    invoke-virtual {v0}, LX/I2i;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2526298
    iget-object v0, p0, LX/Hzt;->f:LX/I2i;

    invoke-virtual {v0, v1}, LX/I2i;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2V;

    .line 2526299
    if-eqz v0, :cond_0

    .line 2526300
    iget-object v2, v0, LX/I2V;->b:LX/I2b;

    move-object v2, v2

    .line 2526301
    sget-object v3, LX/I2b;->i:LX/I2b;

    if-ne v2, v3, :cond_0

    .line 2526302
    iget-object v2, p0, LX/Hzt;->n:LX/2ja;

    .line 2526303
    iget-object v3, v0, LX/I2V;->a:Ljava/lang/Object;

    move-object v0, v3

    .line 2526304
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v2, v0, v1, p2}, LX/2ja;->a(LX/9qX;II)V

    .line 2526305
    :cond_0
    return-void
.end method

.method public final a(LX/Hx7;)V
    .locals 2

    .prologue
    .line 2526279
    iget-object v0, p0, LX/Hzt;->o:LX/Hx7;

    if-ne v0, p1, :cond_1

    .line 2526280
    :cond_0
    :goto_0
    return-void

    .line 2526281
    :cond_1
    iput-object p1, p0, LX/Hzt;->o:LX/Hx7;

    .line 2526282
    iget-object v0, p0, LX/Hzt;->c:LX/I2o;

    invoke-interface {v0, p1}, LX/I2m;->a(LX/Hx7;)V

    .line 2526283
    iget-object v0, p0, LX/Hzt;->f:LX/I2i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hzt;->f:LX/I2i;

    .line 2526284
    iget-object v1, v0, LX/I2i;->j:LX/Hx7;

    if-eq v1, p1, :cond_2

    .line 2526285
    iput-object p1, v0, LX/I2i;->j:LX/Hx7;

    .line 2526286
    invoke-static {v0}, LX/I2i;->a(LX/I2i;)V

    .line 2526287
    const/4 v1, 0x1

    .line 2526288
    :goto_1
    move v0, v1

    .line 2526289
    if-eqz v0, :cond_0

    .line 2526290
    invoke-static {p0}, LX/Hzt;->k(LX/Hzt;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/I05;)V
    .locals 1

    .prologue
    .line 2526275
    invoke-direct {p0, p1}, LX/Hzt;->b(LX/I05;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2526276
    sget-object v0, LX/I05;->FIRST_LOAD:LX/I05;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/I05;->LOADING_MORE:LX/I05;

    if-ne p1, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Hzt;->a(Ljava/lang/Boolean;)V

    .line 2526277
    :cond_1
    return-void

    .line 2526278
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 2526265
    iget-object v0, p0, LX/Hzt;->f:LX/I2i;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2526266
    iget-boolean p1, v0, LX/I2i;->h:Z

    if-eq v1, p1, :cond_1

    .line 2526267
    iput-boolean v1, v0, LX/I2i;->h:Z

    .line 2526268
    invoke-static {v0}, LX/I2i;->a(LX/I2i;)V

    .line 2526269
    const/4 p1, 0x1

    .line 2526270
    :goto_0
    move v0, p1

    .line 2526271
    if-eqz v0, :cond_0

    .line 2526272
    invoke-static {p0}, LX/Hzt;->k(LX/Hzt;)V

    .line 2526273
    :cond_0
    return-void

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2526264
    iget-object v0, p0, LX/Hzt;->i:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2526263
    iget-object v0, p0, LX/Hzt;->i:LX/1Qq;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Hzt;->i:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    goto :goto_0
.end method
