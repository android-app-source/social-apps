.class public LX/IGh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/11S;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0SG;LX/11S;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2556444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2556445
    iput-object p1, p0, LX/IGh;->a:LX/0SG;

    .line 2556446
    iput-object p2, p0, LX/IGh;->b:LX/11S;

    .line 2556447
    iput-object p3, p0, LX/IGh;->c:Landroid/content/res/Resources;

    .line 2556448
    return-void
.end method

.method private static a(JII)J
    .locals 4

    .prologue
    const/16 v3, 0xc

    .line 2556449
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2556450
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2556451
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0x1e

    if-lt v1, v2, :cond_0

    .line 2556452
    const/16 v1, 0xb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 2556453
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 2556454
    invoke-virtual {v0, p2, p3}, Ljava/util/Calendar;->add(II)V

    .line 2556455
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(LX/IGh;JLjava/lang/String;)LX/IGe;
    .locals 3

    .prologue
    .line 2556420
    const/16 v0, 0xa

    const/4 v1, 0x1

    invoke-static {p1, p2, v0, v1}, LX/IGh;->a(JII)J

    move-result-wide v0

    .line 2556421
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a(J)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    const-string v1, "one_hour"

    invoke-static {p0, v0, p3, v1}, LX/IGh;->a(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;
    .locals 5

    .prologue
    .line 2556460
    new-instance v0, LX/IGe;

    iget-object v1, p0, LX/IGh;->c:Landroid/content/res/Resources;

    const v2, 0x7f08387f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, LX/IGh;->a(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2, p3}, LX/IGe;-><init>(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2556456
    sget-object v0, LX/IGg;->a:[I

    iget-object v1, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    invoke-virtual {v1}, LX/IGG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2556457
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid ping type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2556458
    :pswitch_0
    iget-object v1, p0, LX/IGh;->b:LX/11S;

    sget-object v2, LX/1lB;->EXACT_TIME_DATE_STYLE:LX/1lB;

    iget-object v0, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2556459
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/IGh;->c:Landroid/content/res/Resources;

    const v1, 0x7f083880

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(JJJ)Z
    .locals 7

    .prologue
    .line 2556461
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2556462
    sub-long v2, p2, p4

    add-long v4, p2, p4

    move-wide v0, p0

    invoke-static/range {v0 .. v5}, LX/IGh;->b(JJJ)Z

    move-result v0

    return v0

    .line 2556463
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/IGe;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2556436
    iget-object v0, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    sget-object v3, LX/IGG;->FOREVER:LX/IGG;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 2556437
    :goto_0
    return v0

    .line 2556438
    :cond_0
    iget-object v0, p0, LX/IGe;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    sget-object v3, LX/IGG;->FOREVER:LX/IGG;

    if-ne v0, v3, :cond_1

    move v0, v2

    .line 2556439
    goto :goto_0

    .line 2556440
    :cond_1
    iget-object v0, p0, LX/IGe;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private static a(LX/IGh;LX/0am;LX/IGe;J)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;",
            ">;",
            "LX/IGe;",
            "J)Z"
        }
    .end annotation

    .prologue
    .line 2556441
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2556442
    :cond_0
    const/4 v0, 0x0

    .line 2556443
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p2, LX/IGe;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-wide v6, p3

    invoke-static/range {v2 .. v7}, LX/IGh;->a(JJJ)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/IGh;JLjava/lang/String;)LX/IGe;
    .locals 3

    .prologue
    .line 2556434
    const/16 v0, 0xa

    const/4 v1, 0x3

    invoke-static {p1, p2, v0, v1}, LX/IGh;->a(JII)J

    move-result-wide v0

    .line 2556435
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a(J)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    const-string v1, "three_hours"

    invoke-static {p0, v0, p3, v1}, LX/IGh;->a(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;
    .locals 5

    .prologue
    .line 2556433
    new-instance v0, LX/IGe;

    iget-object v1, p0, LX/IGh;->c:Landroid/content/res/Resources;

    const v2, 0x7f08387e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, LX/IGh;->a(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2, p3}, LX/IGe;-><init>(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static b(JJJ)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2556429
    cmp-long v0, p2, p4

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2556430
    cmp-long v0, p0, p2

    if-ltz v0, :cond_1

    cmp-long v0, p0, p4

    if-gtz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 2556431
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2556432
    goto :goto_1
.end method

.method private static c(LX/IGh;JLjava/lang/String;)LX/IGe;
    .locals 5

    .prologue
    .line 2556422
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2556423
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2556424
    const/16 v1, 0xb

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2556425
    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2556426
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-gez v1, :cond_0

    .line 2556427
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 2556428
    :cond_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a(J)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    const-string v1, "next_eight_am"

    invoke-static {p0, v0, p3, v1}, LX/IGh;->a(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;

    move-result-object v0

    return-object v0
.end method

.method private static d(LX/IGh;JLjava/lang/String;)LX/IGe;
    .locals 3

    .prologue
    .line 2556418
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-static {p1, p2, v0, v1}, LX/IGh;->a(JII)J

    move-result-wide v0

    .line 2556419
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a(J)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    const-string v1, "one_day"

    invoke-static {p0, v0, p3, v1}, LX/IGh;->a(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0am;Ljava/lang/String;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/IGe;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/32 v10, 0xdbba00

    const-wide/32 v6, 0x1b7740

    const/4 v1, 0x0

    .line 2556388
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2556389
    iget-object v0, p0, LX/IGh;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2556390
    invoke-static {p0, v2, v3, p2}, LX/IGh;->a(LX/IGh;JLjava/lang/String;)LX/IGe;

    move-result-object v0

    .line 2556391
    invoke-static {p0, p1, v0, v6, v7}, LX/IGh;->a(LX/IGh;LX/0am;LX/IGe;J)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2556392
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2556393
    :cond_0
    invoke-static {p0, v2, v3, p2}, LX/IGh;->b(LX/IGh;JLjava/lang/String;)LX/IGe;

    move-result-object v0

    .line 2556394
    invoke-static {p0, p1, v0, v6, v7}, LX/IGh;->a(LX/IGh;LX/0am;LX/IGe;J)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2556395
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2556396
    :cond_1
    invoke-static {p0, v2, v3, p2}, LX/IGh;->c(LX/IGh;JLjava/lang/String;)LX/IGe;

    move-result-object v5

    .line 2556397
    iget-object v0, v5, LX/IGe;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2556398
    sub-long v8, v6, v2

    cmp-long v0, v8, v10

    if-lez v0, :cond_2

    const-wide/32 v8, 0x36ee80

    invoke-static {p0, p1, v5, v8, v9}, LX/IGh;->a(LX/IGh;LX/0am;LX/IGe;J)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2556399
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2556400
    :cond_2
    invoke-static {p0, v2, v3, p2}, LX/IGh;->d(LX/IGh;JLjava/lang/String;)LX/IGe;

    move-result-object v0

    .line 2556401
    sub-long v2, v6, v2

    const-wide/32 v6, 0x44aa200

    cmp-long v2, v2, v6

    if-gez v2, :cond_3

    invoke-static {p0, p1, v0, v10, v11}, LX/IGh;->a(LX/IGh;LX/0am;LX/IGe;J)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2556402
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2556403
    :cond_3
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    sget-object v2, LX/IGG;->FOREVER:LX/IGG;

    if-eq v0, v2, :cond_5

    .line 2556404
    :cond_4
    invoke-static {}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b()Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    const-string v2, "forever"

    invoke-static {p0, v0, p2, v2}, LX/IGh;->a(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2556405
    :cond_5
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2556406
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2556407
    :goto_0
    return-object v0

    .line 2556408
    :cond_6
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2556409
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v1

    move v2, v1

    :goto_1
    if-ge v3, v6, :cond_7

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IGe;

    .line 2556410
    if-nez v2, :cond_9

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    invoke-static {v0, v1}, LX/IGh;->a(LX/IGe;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2556411
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    const-string v2, "existing"

    invoke-static {p0, v1, p2, v2}, LX/IGh;->b(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2556412
    const/4 v1, 0x1

    .line 2556413
    :goto_2
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2556414
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v1

    goto :goto_1

    .line 2556415
    :cond_7
    if-nez v2, :cond_8

    .line 2556416
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    const-string v1, "existing"

    invoke-static {p0, v0, p2, v1}, LX/IGh;->b(LX/IGh;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;Ljava/lang/String;Ljava/lang/String;)LX/IGe;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2556417
    :cond_8
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    :cond_9
    move v1, v2

    goto :goto_2
.end method
