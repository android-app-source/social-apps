.class public LX/Ieh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3NP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2598896
    new-instance v0, LX/Ieg;

    invoke-direct {v0}, LX/Ieg;-><init>()V

    sput-object v0, LX/Ieh;->a:LX/3NP;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2598897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/3Mk;LX/3N9;LX/3NA;LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Mk;",
            "LX/3N9;",
            "LX/3NA;",
            "LX/0SF;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/res/Resources;",
            ")",
            "LX/0Px",
            "<",
            "LX/3NU;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2598898
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2598899
    iput-boolean v3, p0, LX/3Mk;->p:Z

    .line 2598900
    iput-boolean v3, p0, LX/3Mk;->q:Z

    .line 2598901
    new-instance v1, LX/3NU;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v3}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598902
    new-instance v1, LX/3NU;

    new-instance v2, LX/3NS;

    const/4 p0, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2598903
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2598904
    iput-boolean v5, p1, LX/3N9;->g:Z

    .line 2598905
    new-instance v4, LX/3NU;

    invoke-direct {v4, p1, p0, v6}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598906
    iput-boolean v5, p2, LX/3NA;->m:Z

    .line 2598907
    new-instance v4, LX/3NU;

    .line 2598908
    new-instance v5, LX/3NV;

    sget-object p1, LX/Ieh;->a:LX/3NP;

    invoke-direct {v5, p2, p4, p1}, LX/3NV;-><init>(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;LX/3NP;)V

    move-object v5, v5

    .line 2598909
    invoke-direct {v4, v5, p0, v6}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598910
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 2598911
    invoke-direct {v2, v3, p3, p4, p5}, LX/3NS;-><init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V

    const v3, 0x7f0802d5

    invoke-virtual {p6, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598912
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/3Mk;Landroid/content/res/Resources;LX/3N9;LX/3NA;)LX/3Mi;
    .locals 8

    .prologue
    .line 2598913
    new-instance v7, LX/3NS;

    move-object v0, p3

    move-object v1, p5

    move-object v2, p6

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, LX/Ieh;->a(LX/3Mk;LX/3N9;LX/3NA;LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;)LX/0Px;

    move-result-object v0

    invoke-direct {v7, v0, p0, p1, p2}, LX/3NS;-><init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V

    return-object v7
.end method
