.class public LX/IXa;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2586291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/B5Y;)LX/ClZ;
    .locals 4

    .prologue
    .line 2586292
    new-instance v1, LX/ClZ;

    invoke-interface {p0}, LX/B5Y;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, LX/B5Y;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, LX/B5Y;->n()LX/8Yp;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, LX/ClZ;-><init>(Ljava/lang/String;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;LX/8Yp;)V

    return-object v1

    :cond_0
    invoke-interface {p0}, LX/B5Y;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/8Z4;)LX/Cly;
    .locals 2

    .prologue
    .line 2586293
    const/4 v0, 0x0

    .line 2586294
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2586295
    new-instance v0, LX/CmL;

    invoke-direct {v0, p0}, LX/CmL;-><init>(LX/8Z4;)V

    sget-object v1, LX/Clb;->TITLE:LX/Clb;

    .line 2586296
    iput-object v1, v0, LX/CmL;->b:LX/Clb;

    .line 2586297
    move-object v0, v0

    .line 2586298
    invoke-virtual {v0}, LX/CmL;->c()LX/Cly;

    move-result-object v0

    .line 2586299
    :cond_0
    return-object v0
.end method

.method public static a(LX/ClZ;)LX/CmQ;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2586300
    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/CmP;

    invoke-direct {v1}, LX/CmP;-><init>()V

    .line 2586301
    iget-object v2, p0, LX/ClZ;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-object v2, v2

    .line 2586302
    iput-object v2, v1, LX/CmP;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    .line 2586303
    move-object v1, v1

    .line 2586304
    iget-object v2, p0, LX/ClZ;->c:LX/8Yp;

    move-object v2, v2

    .line 2586305
    iput-object v2, v1, LX/CmP;->b:LX/8Yp;

    .line 2586306
    move-object v1, v1

    .line 2586307
    iget-object v2, p0, LX/ClZ;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2586308
    invoke-virtual {v1, v2}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v1

    new-instance v2, LX/Cmy;

    sget-object v3, LX/Cmw;->b:LX/Cmw;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v0, v4}, LX/Cmy;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;I)V

    .line 2586309
    iput-object v2, v1, LX/Cm7;->e:LX/Cml;

    .line 2586310
    move-object v0, v1

    .line 2586311
    invoke-virtual {v0}, LX/Cm7;->b()LX/Clr;

    move-result-object v0

    check-cast v0, LX/CmQ;

    goto :goto_0
.end method
