.class public final LX/HUk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HUl;


# direct methods
.method public constructor <init>(LX/HUl;)V
    .locals 0

    .prologue
    .line 2473592
    iput-object p1, p0, LX/HUk;->a:LX/HUl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5722752

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473593
    iget-object v1, p0, LX/HUk;->a:LX/HUl;

    iget-object v1, v1, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->d:LX/9XE;

    iget-object v2, p0, LX/HUk;->a:LX/HUl;

    iget-object v2, v2, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-wide v2, v2, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->q:J

    .line 2473594
    iget-object v5, v1, LX/9XE;->a:LX/0Zb;

    sget-object v6, LX/9XI;->EVENT_TAPPED_ADD_VIDEO:LX/9XI;

    invoke-static {v6, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2473595
    iget-object v1, p0, LX/HUk;->a:LX/HUl;

    iget-object v1, v1, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473596
    new-instance v5, LX/HUb;

    invoke-direct {v5, v1}, LX/HUb;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    .line 2473597
    new-instance v7, LX/4At;

    iget-object v6, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->m:Landroid/content/Context;

    const v8, 0x7f0817c1

    invoke-direct {v7, v6, v8}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2473598
    iget-object v6, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->g:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/3iH;

    iget-wide v8, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->q:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    new-instance v9, LX/HUc;

    invoke-direct {v9, v1, v5, v7}, LX/HUc;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;LX/0Rl;LX/4At;)V

    iget-object v7, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->l:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-virtual {v6, v8, v9, v7}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    .line 2473599
    const v1, -0x2a61d33b

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
