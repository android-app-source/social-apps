.class public LX/HfU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2491937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2491938
    iput-object p1, p0, LX/HfU;->a:LX/0Uh;

    .line 2491939
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2491940
    iget-object v0, p0, LX/HfU;->a:LX/0Uh;

    const/16 v1, 0x2de

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2491941
    new-instance v0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;

    invoke-direct {v0}, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;-><init>()V

    .line 2491942
    :goto_0
    return-object v0

    .line 2491943
    :cond_0
    new-instance v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;-><init>()V

    .line 2491944
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2491945
    const-string v2, "doNotSetTitleBar"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2491946
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method
