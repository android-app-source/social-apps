.class public final enum LX/Iv7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iv7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iv7;

.field public static final enum Apps:LX/Iv7;

.field public static final enum Error:LX/Iv7;

.field public static final enum Loading:LX/Iv7;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2627943
    new-instance v0, LX/Iv7;

    const-string v1, "Loading"

    invoke-direct {v0, v1, v2}, LX/Iv7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iv7;->Loading:LX/Iv7;

    .line 2627944
    new-instance v0, LX/Iv7;

    const-string v1, "Error"

    invoke-direct {v0, v1, v3}, LX/Iv7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iv7;->Error:LX/Iv7;

    .line 2627945
    new-instance v0, LX/Iv7;

    const-string v1, "Apps"

    invoke-direct {v0, v1, v4}, LX/Iv7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iv7;->Apps:LX/Iv7;

    .line 2627946
    const/4 v0, 0x3

    new-array v0, v0, [LX/Iv7;

    sget-object v1, LX/Iv7;->Loading:LX/Iv7;

    aput-object v1, v0, v2

    sget-object v1, LX/Iv7;->Error:LX/Iv7;

    aput-object v1, v0, v3

    sget-object v1, LX/Iv7;->Apps:LX/Iv7;

    aput-object v1, v0, v4

    sput-object v0, LX/Iv7;->$VALUES:[LX/Iv7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2627947
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iv7;
    .locals 1

    .prologue
    .line 2627948
    const-class v0, LX/Iv7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iv7;

    return-object v0
.end method

.method public static values()[LX/Iv7;
    .locals 1

    .prologue
    .line 2627949
    sget-object v0, LX/Iv7;->$VALUES:[LX/Iv7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iv7;

    return-object v0
.end method
