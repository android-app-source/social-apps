.class public final LX/Ibl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/IdB;

.field public final synthetic c:LX/Ibm;


# direct methods
.method public constructor <init>(LX/Ibm;Ljava/lang/String;LX/IdB;)V
    .locals 0

    .prologue
    .line 2594751
    iput-object p1, p0, LX/Ibl;->c:LX/Ibm;

    iput-object p2, p0, LX/Ibl;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Ibl;->b:LX/IdB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2594752
    iget-object v0, p0, LX/Ibl;->b:LX/IdB;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/IdB;->a(Landroid/location/Address;)V

    .line 2594753
    iget-object v0, p0, LX/Ibl;->c:LX/Ibm;

    iget-object v0, v0, LX/Ibm;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2594754
    :goto_0
    return-void

    .line 2594755
    :cond_0
    iget-object v0, p0, LX/Ibl;->c:LX/Ibm;

    iget-object v0, v0, LX/Ibm;->i:LX/03V;

    sget-object v1, LX/Ibm;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t geocode address line: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Ibl;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2594756
    check-cast p1, LX/0Px;

    .line 2594757
    const/4 v0, 0x0

    .line 2594758
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2594759
    :cond_0
    iget-object v1, p0, LX/Ibl;->c:LX/Ibm;

    iget-object v1, v1, LX/Ibm;->i:LX/03V;

    sget-object v2, LX/Ibm;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Geocoder returned no results for address: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/Ibl;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2594760
    :goto_0
    iget-object v1, p0, LX/Ibl;->b:LX/IdB;

    invoke-virtual {v1, v0}, LX/IdB;->a(Landroid/location/Address;)V

    .line 2594761
    return-void

    .line 2594762
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    goto :goto_0
.end method
