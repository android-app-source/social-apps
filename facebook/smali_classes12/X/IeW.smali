.class public LX/IeW;
.super LX/3Ml;
.source ""


# instance fields
.field public c:LX/IsN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/3ND;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Z


# direct methods
.method private constructor <init>(LX/0Zr;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2598702
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 2598703
    return-void
.end method

.method public static b(LX/0QB;)LX/IeW;
    .locals 6

    .prologue
    .line 2598704
    new-instance v2, LX/IeW;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-direct {v2, v0}, LX/IeW;-><init>(LX/0Zr;)V

    .line 2598705
    new-instance v5, LX/IsN;

    invoke-direct {v5}, LX/IsN;-><init>()V

    .line 2598706
    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-static {p0}, LX/3MW;->b(LX/0QB;)LX/3MW;

    move-result-object v1

    check-cast v1, LX/3MW;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/3Mv;->a(LX/0QB;)LX/3Mv;

    move-result-object v4

    check-cast v4, LX/3Mv;

    .line 2598707
    iput-object v0, v5, LX/IsN;->a:LX/0TD;

    iput-object v1, v5, LX/IsN;->b:LX/3MW;

    iput-object v3, v5, LX/IsN;->c:LX/0tX;

    iput-object v4, v5, LX/IsN;->d:LX/3Mv;

    .line 2598708
    move-object v0, v5

    .line 2598709
    check-cast v0, LX/IsN;

    invoke-static {p0}, LX/3ND;->b(LX/0QB;)LX/3ND;

    move-result-object v1

    check-cast v1, LX/3ND;

    .line 2598710
    iput-object v0, v2, LX/IeW;->c:LX/IsN;

    iput-object v1, v2, LX/IeW;->d:LX/3ND;

    .line 2598711
    return-object v2
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2598712
    new-instance v2, LX/39y;

    invoke-direct {v2}, LX/39y;-><init>()V

    .line 2598713
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2598714
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2598715
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 2598716
    iput v3, v2, LX/39y;->b:I

    move-object v0, v2

    .line 2598717
    :goto_1
    return-object v0

    .line 2598718
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 2598719
    :cond_1
    const-class v1, LX/IsR;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 2598720
    iget-object v4, p0, LX/IeW;->d:LX/3ND;

    .line 2598721
    iget-object v5, v4, LX/3ND;->b:LX/0ad;

    sget-short v6, LX/IsT;->b:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v4, v5

    .line 2598722
    if-eqz v4, :cond_2

    .line 2598723
    sget-object v4, LX/IsR;->CONTACTS:LX/IsR;

    invoke-virtual {v1, v4}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 2598724
    :cond_2
    iget-object v4, p0, LX/IeW;->d:LX/3ND;

    .line 2598725
    iget-object v5, v4, LX/3ND;->b:LX/0ad;

    sget-short v6, LX/IsT;->a:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v4, v5

    .line 2598726
    if-eqz v4, :cond_3

    iget-boolean v4, p0, LX/IeW;->e:Z

    if-nez v4, :cond_3

    .line 2598727
    sget-object v4, LX/IsR;->GROUP_THREADS:LX/IsR;

    invoke-virtual {v1, v4}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 2598728
    :cond_3
    move-object v1, v1

    .line 2598729
    invoke-virtual {v1}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2598730
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 2598731
    iput v3, v2, LX/39y;->b:I

    move-object v0, v2

    .line 2598732
    goto :goto_1

    .line 2598733
    :cond_4
    :try_start_0
    new-instance v4, LX/IsO;

    const/16 v5, 0x2d

    invoke-direct {v4, v0, v1, v5}, LX/IsO;-><init>(Ljava/lang/String;Ljava/util/EnumSet;I)V

    .line 2598734
    iget-object v0, p0, LX/IeW;->c:LX/IsN;

    invoke-virtual {v0, v4}, LX/IsN;->a(LX/IsO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v1, 0x45e84447

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 2598735
    :goto_2
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2598736
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2598737
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    :goto_3
    if-ge v3, v5, :cond_5

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IsP;

    .line 2598738
    iget-object v6, p0, LX/3Ml;->b:LX/3Md;

    iget-object v0, v0, LX/IsP;->b:Ljava/lang/Object;

    invoke-interface {v6, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598739
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 2598740
    :catch_0
    move-exception v0

    .line 2598741
    const-string v1, "ContactPickerServerBlendedSearchFilter"

    const-string v4, "Exception during filtering"

    invoke-static {v1, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2598742
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2598743
    move-object v1, v0

    goto :goto_2

    .line 2598744
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2598745
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    iput v1, v2, LX/39y;->b:I

    .line 2598746
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    :goto_4
    move-object v0, v2

    .line 2598747
    goto/16 :goto_1

    .line 2598748
    :cond_6
    const/4 v0, -0x1

    iput v0, v2, LX/39y;->b:I

    .line 2598749
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    goto :goto_4
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2598750
    iput-boolean p1, p0, LX/IeW;->e:Z

    .line 2598751
    return-void
.end method
