.class public final enum LX/IYY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IYY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IYY;

.field public static final enum DEFAULT:LX/IYY;

.field public static final enum LOGIN_APPROVALS_CODE_ENTRY:LX/IYY;

.field public static final enum NO_NONCE_EXISTS:LX/IYY;

.field public static final enum NO_PASSCODE_SET:LX/IYY;

.field public static final enum PASSCODE_ENTRY:LX/IYY;

.field public static final enum PASSWORD_ACCOUNT:LX/IYY;

.field public static final enum PASSWORD_ENTRY:LX/IYY;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2587854
    new-instance v0, LX/IYY;

    const-string v1, "PASSWORD_ACCOUNT"

    invoke-direct {v0, v1, v3}, LX/IYY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IYY;->PASSWORD_ACCOUNT:LX/IYY;

    .line 2587855
    new-instance v0, LX/IYY;

    const-string v1, "NO_PASSCODE_SET"

    invoke-direct {v0, v1, v4}, LX/IYY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IYY;->NO_PASSCODE_SET:LX/IYY;

    .line 2587856
    new-instance v0, LX/IYY;

    const-string v1, "PASSCODE_ENTRY"

    invoke-direct {v0, v1, v5}, LX/IYY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IYY;->PASSCODE_ENTRY:LX/IYY;

    .line 2587857
    new-instance v0, LX/IYY;

    const-string v1, "PASSWORD_ENTRY"

    invoke-direct {v0, v1, v6}, LX/IYY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IYY;->PASSWORD_ENTRY:LX/IYY;

    .line 2587858
    new-instance v0, LX/IYY;

    const-string v1, "LOGIN_APPROVALS_CODE_ENTRY"

    invoke-direct {v0, v1, v7}, LX/IYY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IYY;->LOGIN_APPROVALS_CODE_ENTRY:LX/IYY;

    .line 2587859
    new-instance v0, LX/IYY;

    const-string v1, "NO_NONCE_EXISTS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/IYY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IYY;->NO_NONCE_EXISTS:LX/IYY;

    .line 2587860
    new-instance v0, LX/IYY;

    const-string v1, "DEFAULT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/IYY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IYY;->DEFAULT:LX/IYY;

    .line 2587861
    const/4 v0, 0x7

    new-array v0, v0, [LX/IYY;

    sget-object v1, LX/IYY;->PASSWORD_ACCOUNT:LX/IYY;

    aput-object v1, v0, v3

    sget-object v1, LX/IYY;->NO_PASSCODE_SET:LX/IYY;

    aput-object v1, v0, v4

    sget-object v1, LX/IYY;->PASSCODE_ENTRY:LX/IYY;

    aput-object v1, v0, v5

    sget-object v1, LX/IYY;->PASSWORD_ENTRY:LX/IYY;

    aput-object v1, v0, v6

    sget-object v1, LX/IYY;->LOGIN_APPROVALS_CODE_ENTRY:LX/IYY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/IYY;->NO_NONCE_EXISTS:LX/IYY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IYY;->DEFAULT:LX/IYY;

    aput-object v2, v0, v1

    sput-object v0, LX/IYY;->$VALUES:[LX/IYY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2587864
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IYY;
    .locals 1

    .prologue
    .line 2587863
    const-class v0, LX/IYY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IYY;

    return-object v0
.end method

.method public static values()[LX/IYY;
    .locals 1

    .prologue
    .line 2587862
    sget-object v0, LX/IYY;->$VALUES:[LX/IYY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IYY;

    return-object v0
.end method
