.class public LX/Hsx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/HsK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesRemovedUrls;",
        ":",
        "LX/0j5;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicSetters$SetsRemovedURLs",
        "<TMutation;>;:",
        "Lcom/facebook/ipc/composer/intent/ComposerShareParams$SetsShareParams",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/HsK;"
    }
.end annotation


# instance fields
.field private final a:LX/0Uh;

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/feedattachment/GifComposerAttachment$Callback;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/composer/feedattachment/GifAttachmentView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0il;LX/HqP;)V
    .locals 2
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/HqP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "TServices;",
            "Lcom/facebook/composer/feedattachment/GifComposerAttachment$Callback;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2515236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2515237
    iput-object p1, p0, LX/Hsx;->a:LX/0Uh;

    .line 2515238
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hsx;->c:Ljava/lang/ref/WeakReference;

    .line 2515239
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hsx;->b:Ljava/lang/ref/WeakReference;

    .line 2515240
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2515226
    iget-object v0, p0, LX/Hsx;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515227
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v1, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 2515228
    iget-object v0, p0, LX/Hsx;->d:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    .line 2515229
    move-object v0, v0

    .line 2515230
    check-cast v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;

    .line 2515231
    iget-object v2, v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2515232
    :goto_0
    return-void

    .line 2515233
    :cond_0
    iput-object v1, v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->f:Ljava/lang/String;

    .line 2515234
    iget-object v2, v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->a:LX/1Ad;

    sget-object p0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    iget-object p0, v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    iget-object p0, v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->e:LX/1Ai;

    invoke-virtual {v2, p0}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 2515235
    iget-object p0, v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 2515225
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2515206
    new-instance v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/composer/feedattachment/GifAttachmentView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Hsx;->d:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    .line 2515207
    iget-object v0, p0, LX/Hsx;->d:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2515208
    iget-object v0, p0, LX/Hsx;->d:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    new-instance v1, LX/Hsw;

    invoke-direct {v1, p0}, LX/Hsw;-><init>(LX/Hsx;)V

    invoke-virtual {v0, v1}, Lcom/facebook/composer/feedattachment/GifAttachmentView;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2515209
    invoke-direct {p0}, LX/Hsx;->c()V

    .line 2515210
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2515222
    iget-object v0, p0, LX/Hsx;->d:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Hsx;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2515223
    :cond_0
    :goto_0
    return-void

    .line 2515224
    :cond_1
    invoke-direct {p0}, LX/Hsx;->c()V

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2515216
    iget-object v0, p0, LX/Hsx;->a:LX/0Uh;

    sget v1, LX/7l1;->f:I

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 2515217
    :goto_0
    return v0

    .line 2515218
    :cond_0
    iget-object v0, p0, LX/Hsx;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515219
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v0, v2

    .line 2515220
    goto :goto_0

    .line 2515221
    :cond_2
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v0}, LX/9J0;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2515211
    iget-object v0, p0, LX/Hsx;->d:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    .line 2515212
    move-object v0, v0

    .line 2515213
    check-cast v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/feedattachment/GifAttachmentView;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2515214
    iput-object v1, p0, LX/Hsx;->d:Lcom/facebook/composer/feedattachment/GifAttachmentView;

    .line 2515215
    return-void
.end method
