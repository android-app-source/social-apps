.class public LX/JOn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/JOi;

.field public final b:LX/JOt;


# direct methods
.method public constructor <init>(LX/JOi;LX/JOt;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2688240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2688241
    iput-object p1, p0, LX/JOn;->a:LX/JOi;

    .line 2688242
    iput-object p2, p0, LX/JOn;->b:LX/JOt;

    .line 2688243
    return-void
.end method

.method public static a(LX/0QB;)LX/JOn;
    .locals 5

    .prologue
    .line 2688244
    const-class v1, LX/JOn;

    monitor-enter v1

    .line 2688245
    :try_start_0
    sget-object v0, LX/JOn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688246
    sput-object v2, LX/JOn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688247
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688248
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688249
    new-instance p0, LX/JOn;

    invoke-static {v0}, LX/JOi;->a(LX/0QB;)LX/JOi;

    move-result-object v3

    check-cast v3, LX/JOi;

    invoke-static {v0}, LX/JOt;->a(LX/0QB;)LX/JOt;

    move-result-object v4

    check-cast v4, LX/JOt;

    invoke-direct {p0, v3, v4}, LX/JOn;-><init>(LX/JOi;LX/JOt;)V

    .line 2688250
    move-object v0, p0

    .line 2688251
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688252
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688253
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688254
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
