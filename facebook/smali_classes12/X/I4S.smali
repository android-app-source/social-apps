.class public final LX/I4S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;)V
    .locals 0

    .prologue
    .line 2534041
    iput-object p1, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2534042
    iget-object v0, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v0, v0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    if-nez v0, :cond_0

    .line 2534043
    :goto_0
    return-void

    .line 2534044
    :cond_0
    iget-object v0, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    .line 2534045
    iget-object v1, v0, LX/CnT;->d:LX/CnG;

    move-object v0, v1

    .line 2534046
    check-cast v0, LX/I4b;

    invoke-virtual {v0}, LX/I4b;->a()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    iget-object v1, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v1, v1, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->j:LX/Bne;

    iget-object v2, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v2, v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-virtual {v2}, LX/I4K;->g()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    iget-object v3, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v3, v3, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-virtual {v3}, LX/I4K;->i()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v3

    invoke-virtual {v1, v2, p2, v3}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    .line 2534047
    iget-object v0, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v0, v0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534048
    iget-object v1, v0, LX/I4K;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    move-object v2, v1

    .line 2534049
    iget-object v0, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v0, v0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->g:LX/7vW;

    iget-object v1, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v1, v1, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-interface {v1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_2

    iget-object v3, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    :goto_1
    if-eqz v2, :cond_1

    iget-object v4, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    :cond_1
    const-string v5, "event_collection"

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_COLLECTION_EVENT_CARD:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    :cond_2
    move-object v3, v4

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2534050
    iget-object v0, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v0, v0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    if-nez v0, :cond_0

    .line 2534051
    :goto_0
    return-void

    .line 2534052
    :cond_0
    iget-object v0, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    .line 2534053
    iget-object v1, v0, LX/CnT;->d:LX/CnG;

    move-object v0, v1

    .line 2534054
    check-cast v0, LX/I4b;

    invoke-virtual {v0}, LX/I4b;->a()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    iget-object v1, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v1, v1, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->j:LX/Bne;

    iget-object v2, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v2, v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-virtual {v2}, LX/I4K;->g()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    iget-object v3, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v3, v3, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-virtual {v3}, LX/I4K;->h()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p2}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    .line 2534055
    iget-object v0, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v0, v0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534056
    iget-object v1, v0, LX/I4K;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    move-object v2, v1

    .line 2534057
    iget-object v0, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v0, v0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->h:LX/7vZ;

    iget-object v1, p0, LX/I4S;->a:Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    iget-object v1, v1, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-interface {v1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_2

    iget-object v3, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    :goto_1
    if-eqz v2, :cond_1

    iget-object v4, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    :cond_1
    const-string v5, "event_collection"

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_COLLECTION_EVENT_CARD:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    :cond_2
    move-object v3, v4

    goto :goto_1
.end method
