.class public LX/Itk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:LX/FDt;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/service/SendApiHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/7V0;

.field private final d:LX/Iuh;

.field private final e:LX/Itm;

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2624279
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Itk;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDt;LX/7V0;LX/Iuh;LX/Itm;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDt;",
            "LX/7V0;",
            "LX/Iuh;",
            "LX/Itm;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/service/SendApiHandler;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2624270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2624271
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624272
    iput-object v0, p0, LX/Itk;->f:LX/0Ot;

    .line 2624273
    iput-object p1, p0, LX/Itk;->a:LX/FDt;

    .line 2624274
    iput-object p2, p0, LX/Itk;->c:LX/7V0;

    .line 2624275
    iput-object p3, p0, LX/Itk;->d:LX/Iuh;

    .line 2624276
    iput-object p4, p0, LX/Itk;->e:LX/Itm;

    .line 2624277
    iput-object p5, p0, LX/Itk;->b:LX/0Ot;

    .line 2624278
    return-void
.end method

.method public static a(LX/0QB;)LX/Itk;
    .locals 13

    .prologue
    .line 2624239
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2624240
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2624241
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2624242
    if-nez v1, :cond_0

    .line 2624243
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2624244
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2624245
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2624246
    sget-object v1, LX/Itk;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2624247
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2624248
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2624249
    :cond_1
    if-nez v1, :cond_4

    .line 2624250
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2624251
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2624252
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2624253
    new-instance v7, LX/Itk;

    invoke-static {v0}, LX/FDt;->a(LX/0QB;)LX/FDt;

    move-result-object v8

    check-cast v8, LX/FDt;

    invoke-static {v0}, LX/7V1;->a(LX/0QB;)LX/7V1;

    move-result-object v9

    check-cast v9, LX/7V0;

    invoke-static {v0}, LX/Iuh;->a(LX/0QB;)LX/Iuh;

    move-result-object v10

    check-cast v10, LX/Iuh;

    invoke-static {v0}, LX/Itm;->a(LX/0QB;)LX/Itm;

    move-result-object v11

    check-cast v11, LX/Itm;

    const/16 v12, 0x292b

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v7 .. v12}, LX/Itk;-><init>(LX/FDt;LX/7V0;LX/Iuh;LX/Itm;LX/0Ot;)V

    .line 2624254
    const/16 v8, 0x292f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2624255
    iput-object v8, v7, LX/Itk;->f:LX/0Ot;

    .line 2624256
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2624257
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2624258
    if-nez v1, :cond_2

    .line 2624259
    sget-object v0, LX/Itk;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itk;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2624260
    :goto_1
    if-eqz v0, :cond_3

    .line 2624261
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2624262
    :goto_3
    check-cast v0, LX/Itk;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2624263
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2624264
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2624265
    :catchall_1
    move-exception v0

    .line 2624266
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2624267
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2624268
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2624269
    :cond_2
    :try_start_8
    sget-object v0, LX/Itk;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itk;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 8

    .prologue
    .line 2624215
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624216
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2624217
    const/4 v2, 0x0

    .line 2624218
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2624219
    :goto_1
    move-object v1, v1

    .line 2624220
    iget-object v0, p0, LX/Itk;->a:LX/FDt;

    invoke-virtual {v0}, LX/FDt;->a()V

    .line 2624221
    iget-object v0, p0, LX/Itk;->d:LX/Iuh;

    invoke-virtual {v0}, LX/Iuh;->a()V

    .line 2624222
    :try_start_0
    iget-object v0, p0, LX/Itk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/service/SendApiHandler;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2624223
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2624224
    :catch_0
    move-exception v0

    .line 2624225
    iget-object v2, p0, LX/Itk;->e:LX/Itm;

    sget-object v3, LX/6f3;->UNKNOWN:LX/6f3;

    invoke-virtual {v2, v0, v1, v3}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v0

    .line 2624226
    throw v0

    .line 2624227
    :cond_1
    invoke-static {}, LX/5zj;->values()[LX/5zj;

    move-result-object v0

    array-length v0, v0

    new-array v4, v0, [I

    .line 2624228
    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2624229
    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->ordinal()I

    move-result v0

    aget v7, v4, v0

    add-int/lit8 v7, v7, 0x1

    aput v7, v4, v0

    .line 2624230
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 2624231
    :cond_2
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v3

    move v0, v2

    .line 2624232
    :goto_3
    array-length v2, v4

    if-ge v0, v2, :cond_4

    .line 2624233
    aget v2, v4, v0

    if-eqz v2, :cond_3

    .line 2624234
    invoke-static {}, LX/5zj;->values()[LX/5zj;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2624235
    aget v5, v4, v0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 2624236
    invoke-virtual {v3, v2, v5}, LX/6f7;->a(Ljava/lang/String;Ljava/lang/String;)LX/6f7;

    .line 2624237
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2624238
    :cond_4
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    goto :goto_1
.end method

.method public final b(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 4

    .prologue
    .line 2624210
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624211
    :try_start_0
    iget-object v0, p0, LX/Itk;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itp;

    invoke-virtual {v0, p1}, LX/Itp;->a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2624212
    :catch_0
    move-exception v0

    .line 2624213
    iget-object v2, p0, LX/Itk;->e:LX/Itm;

    sget-object v3, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v2, v0, v1, v3}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v0

    .line 2624214
    throw v0
.end method
