.class public LX/Iyp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2633953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2633954
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2633955
    const/4 v0, 0x0

    .line 2633956
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(LX/0lF;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 5

    .prologue
    .line 2633957
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2633958
    const-string v0, "price_list"

    invoke-static {p0, v0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2633959
    const-string v2, "label"

    invoke-static {v0, v2}, LX/Iyp;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2633960
    const-string v3, "Price"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2633961
    const-string v1, "currency_amount"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 2633962
    new-instance v2, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v3, "currency"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/math/BigDecimal;

    const-string p0, "amount"

    invoke-virtual {v1, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3, v4}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    move-object v0, v2

    .line 2633963
    return-object v0

    .line 2633964
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find Price in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
