.class public LX/IXo;
.super LX/0gF;
.source ""

# interfaces
.implements LX/0Ya;
.implements LX/Che;
.implements LX/CsQ;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CqB;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/CqD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2586694
    const-class v0, LX/IXo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IXo;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0gc;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2586690
    invoke-direct {p0, p1}, LX/0gF;-><init>(LX/0gc;)V

    .line 2586691
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IXo;->c:Ljava/util/List;

    .line 2586692
    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/IXo;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/IXo;->a:LX/03V;

    .line 2586693
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/richdocument/view/carousel/PageableFragment;)I
    .locals 2

    .prologue
    .line 2586685
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2586686
    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqB;

    iget-object v0, v0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    if-ne v0, p1, :cond_0

    .line 2586687
    :goto_1
    return v1

    .line 2586688
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2586689
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2586679
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2586680
    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqB;

    .line 2586681
    iget-object v0, v0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    if-ne v0, p1, :cond_0

    .line 2586682
    :goto_1
    return v1

    .line 2586683
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2586684
    :cond_1
    const/4 v1, -0x2

    goto :goto_1
.end method

.method public final a()LX/0gG;
    .locals 0

    .prologue
    .line 2586678
    return-object p0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2586695
    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqB;

    iget-object v0, v0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    return-object v0
.end method

.method public final a(Lcom/facebook/richdocument/view/carousel/PageableFragment;I)V
    .locals 6

    .prologue
    .line 2586645
    if-eqz p1, :cond_1

    .line 2586646
    new-instance v0, LX/CqB;

    invoke-direct {v0, p1}, LX/CqB;-><init>(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V

    .line 2586647
    iget-object v1, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2586648
    iget-object v1, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2586649
    iget-object v1, p0, LX/IXo;->d:LX/CqD;

    invoke-interface {v1, v0}, LX/CqD;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v1

    .line 2586650
    if-eqz v1, :cond_0

    if-nez p1, :cond_3

    .line 2586651
    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_6

    .line 2586652
    :cond_1
    :goto_1
    return-void

    .line 2586653
    :cond_2
    iget-object v1, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2586654
    :try_start_0
    invoke-virtual {p0}, LX/0gG;->kV_()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2586655
    :catch_0
    iget-object v0, p0, LX/IXo;->a:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/IXo;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_addFragment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IndexOutOfBoundsException while adding at: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_1

    .line 2586656
    :cond_3
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2586657
    iget-object v3, p1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2586658
    if-eqz v2, :cond_0

    .line 2586659
    const-string v4, "extra_instant_articles_can_log_open_link_on_settle"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2586660
    if-eqz v3, :cond_0

    .line 2586661
    const-string v4, "extra_instant_articles_referrer"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2586662
    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2586663
    const-string v5, "extra_instant_articles_referrer"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2586664
    :cond_4
    const-string v4, "open_action"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2586665
    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2586666
    const-string v5, "open_action"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2586667
    :cond_5
    const-string v4, "click_source_document_chaining_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2586668
    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2586669
    const-string v5, "click_source_document_chaining_id"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2586670
    const-string v4, "click_source_document_depth"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 2586671
    const-string v4, "click_source_document_depth"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 2586672
    :cond_6
    if-ltz p2, :cond_1

    iget-object v1, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt p2, v1, :cond_1

    .line 2586673
    iget-object v1, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CqB;

    .line 2586674
    iget-object v2, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le p2, v2, :cond_7

    .line 2586675
    iget-object v2, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2586676
    :goto_2
    invoke-virtual {p0}, LX/0gG;->kV_()V

    goto/16 :goto_1

    .line 2586677
    :cond_7
    iget-object v2, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v2, p2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2586637
    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;
    .locals 1

    .prologue
    .line 2586642
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2586643
    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqB;

    iget-object v0, v0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    .line 2586644
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getActiveFragmentIndex()I
    .locals 1

    .prologue
    .line 2586641
    const/4 v0, 0x0

    return v0
.end method

.method public final getFragmentCount()I
    .locals 1

    .prologue
    .line 2586640
    iget-object v0, p0, LX/IXo;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final setFragmentPager(LX/CqD;)V
    .locals 0

    .prologue
    .line 2586638
    iput-object p1, p0, LX/IXo;->d:LX/CqD;

    .line 2586639
    return-void
.end method
