.class public LX/IlK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ikp;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p2    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607647
    const v0, 0x7f0311a2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/IlK;->a:Landroid/view/ViewGroup;

    .line 2607648
    iget-object v0, p0, LX/IlK;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d2964

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/IlK;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2607649
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2607650
    iget-object v0, p0, LX/IlK;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final a(LX/Il0;)V
    .locals 2

    .prologue
    .line 2607651
    iget-object v0, p0, LX/IlK;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2607652
    iget-object v1, p1, LX/Il0;->k:LX/0am;

    move-object v1, v1

    .line 2607653
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2607654
    return-void
.end method
