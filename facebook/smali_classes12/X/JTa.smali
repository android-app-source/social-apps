.class public final enum LX/JTa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JTa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JTa;

.field public static final enum ALBUM:LX/JTa;

.field public static final enum MINUTIAE_ID:LX/JTa;

.field public static final enum MUSICIAN:LX/JTa;

.field public static final enum PROVIDER:LX/JTa;

.field public static final enum SONG:LX/JTa;

.field public static final enum SONG_ID:LX/JTa;

.field public static final enum STORY_ID:LX/JTa;

.field public static final enum STORY_TYPE:LX/JTa;

.field public static final enum TRACKING_CODES:LX/JTa;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2696845
    new-instance v0, LX/JTa;

    const-string v1, "SONG_ID"

    const-string v2, "music_id"

    invoke-direct {v0, v1, v4, v2}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->SONG_ID:LX/JTa;

    .line 2696846
    new-instance v0, LX/JTa;

    const-string v1, "STORY_ID"

    const-string v2, "content_id"

    invoke-direct {v0, v1, v5, v2}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->STORY_ID:LX/JTa;

    .line 2696847
    new-instance v0, LX/JTa;

    const-string v1, "PROVIDER"

    const-string v2, "platform_name"

    invoke-direct {v0, v1, v6, v2}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->PROVIDER:LX/JTa;

    .line 2696848
    new-instance v0, LX/JTa;

    const-string v1, "STORY_TYPE"

    const-string v2, "story_type"

    invoke-direct {v0, v1, v7, v2}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->STORY_TYPE:LX/JTa;

    .line 2696849
    new-instance v0, LX/JTa;

    const-string v1, "MINUTIAE_ID"

    const-string v2, "minutiae_action"

    invoke-direct {v0, v1, v8, v2}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->MINUTIAE_ID:LX/JTa;

    .line 2696850
    new-instance v0, LX/JTa;

    const-string v1, "TRACKING_CODES"

    const/4 v2, 0x5

    const-string v3, "tracking"

    invoke-direct {v0, v1, v2, v3}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->TRACKING_CODES:LX/JTa;

    .line 2696851
    new-instance v0, LX/JTa;

    const-string v1, "ALBUM"

    const/4 v2, 0x6

    const-string v3, "album_title"

    invoke-direct {v0, v1, v2, v3}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->ALBUM:LX/JTa;

    .line 2696852
    new-instance v0, LX/JTa;

    const-string v1, "SONG"

    const/4 v2, 0x7

    const-string v3, "audio_title"

    invoke-direct {v0, v1, v2, v3}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->SONG:LX/JTa;

    .line 2696853
    new-instance v0, LX/JTa;

    const-string v1, "MUSICIAN"

    const/16 v2, 0x8

    const-string v3, "artist_name"

    invoke-direct {v0, v1, v2, v3}, LX/JTa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTa;->MUSICIAN:LX/JTa;

    .line 2696854
    const/16 v0, 0x9

    new-array v0, v0, [LX/JTa;

    sget-object v1, LX/JTa;->SONG_ID:LX/JTa;

    aput-object v1, v0, v4

    sget-object v1, LX/JTa;->STORY_ID:LX/JTa;

    aput-object v1, v0, v5

    sget-object v1, LX/JTa;->PROVIDER:LX/JTa;

    aput-object v1, v0, v6

    sget-object v1, LX/JTa;->STORY_TYPE:LX/JTa;

    aput-object v1, v0, v7

    sget-object v1, LX/JTa;->MINUTIAE_ID:LX/JTa;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/JTa;->TRACKING_CODES:LX/JTa;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/JTa;->ALBUM:LX/JTa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/JTa;->SONG:LX/JTa;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/JTa;->MUSICIAN:LX/JTa;

    aput-object v2, v0, v1

    sput-object v0, LX/JTa;->$VALUES:[LX/JTa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2696839
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2696840
    iput-object p3, p0, LX/JTa;->name:Ljava/lang/String;

    .line 2696841
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JTa;
    .locals 1

    .prologue
    .line 2696844
    const-class v0, LX/JTa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JTa;

    return-object v0
.end method

.method public static values()[LX/JTa;
    .locals 1

    .prologue
    .line 2696843
    sget-object v0, LX/JTa;->$VALUES:[LX/JTa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JTa;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2696842
    iget-object v0, p0, LX/JTa;->name:Ljava/lang/String;

    return-object v0
.end method
