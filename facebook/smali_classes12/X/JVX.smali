.class public final LX/JVX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9hN;


# instance fields
.field private final a:LX/1qa;

.field public final b:Landroid/view/View;

.field private final c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(LX/1qa;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2700984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2700985
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/JVX;->c:Landroid/graphics/Rect;

    .line 2700986
    iput-object p1, p0, LX/JVX;->a:LX/1qa;

    .line 2700987
    iput-object p2, p0, LX/JVX;->b:Landroid/view/View;

    .line 2700988
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/9hO;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2700989
    iget-object v0, p0, LX/JVX;->b:Landroid/view/View;

    .line 2700990
    const v2, 0x7f0d00f8

    invoke-virtual {v0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2700991
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-nez v3, :cond_5

    .line 2700992
    :cond_0
    const/4 v2, 0x0

    .line 2700993
    :goto_0
    move-object v0, v2

    .line 2700994
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2700995
    iget-object v0, p0, LX/JVX;->b:Landroid/view/View;

    .line 2700996
    :goto_1
    move-object v2, v0

    .line 2700997
    if-nez v2, :cond_1

    move-object v0, v1

    .line 2700998
    :goto_2
    return-object v0

    .line 2700999
    :cond_1
    const v0, 0x7f0d00f8

    invoke-virtual {v2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2701000
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2701001
    goto :goto_2

    .line 2701002
    :cond_2
    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 2701003
    if-nez v3, :cond_3

    move-object v0, v1

    .line 2701004
    goto :goto_2

    .line 2701005
    :cond_3
    iget-object v0, p0, LX/JVX;->c:Landroid/graphics/Rect;

    invoke-virtual {v2, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2701006
    new-instance v0, LX/9hO;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    iget-object v4, p0, LX/JVX;->c:Landroid/graphics/Rect;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-static {v1, v2, v4, v5}, LX/9hP;->a(IILandroid/graphics/Rect;Landroid/widget/ImageView$ScaleType;)LX/9hP;

    move-result-object v1

    invoke-static {v3}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/9hO;-><init>(LX/9hP;LX/1bf;)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
