.class public final LX/JGd;
.super LX/5qw;
.source ""


# direct methods
.method public constructor <init>(LX/5rq;)V
    .locals 1

    .prologue
    .line 2668222
    new-instance v0, Lcom/facebook/catalyst/shadow/flat/FlatRootViewManager;

    invoke-direct {v0}, Lcom/facebook/catalyst/shadow/flat/FlatRootViewManager;-><init>()V

    invoke-direct {p0, p1, v0}, LX/5qw;-><init>(LX/5rq;Lcom/facebook/react/uimanager/RootViewManager;)V

    .line 2668223
    return-void
.end method


# virtual methods
.method public final a(IIIII)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 2668213
    invoke-virtual {p0, p1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2668214
    sub-int v1, p4, p2

    .line 2668215
    sub-int v2, p5, p3

    .line 2668216
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    if-ne v3, v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    if-eq v3, v2, :cond_1

    .line 2668217
    :cond_0
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 2668218
    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 2668219
    :goto_0
    return-void

    .line 2668220
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 2668221
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_0
.end method

.method public final a(ILX/5rH;LX/5rJ;)V
    .locals 1

    .prologue
    .line 2668208
    new-instance v0, LX/JGs;

    invoke-direct {v0, p3}, LX/JGs;-><init>(Landroid/content/Context;)V

    .line 2668209
    invoke-virtual {p2, v0}, LX/5rH;->addView(Landroid/view/View;)V

    .line 2668210
    invoke-virtual {p2, p1}, LX/5rH;->setId(I)V

    .line 2668211
    invoke-virtual {p0, p1, v0}, LX/5qw;->a(ILandroid/view/ViewGroup;)V

    .line 2668212
    return-void
.end method

.method public final a(I[I[I)V
    .locals 6

    .prologue
    .line 2668196
    invoke-virtual {p0, p1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2668197
    instance-of v1, v0, LX/JGs;

    if-eqz v1, :cond_0

    .line 2668198
    check-cast v0, LX/JGs;

    invoke-virtual {v0, p0, p2, p3}, LX/JGs;->a(LX/JGd;[I[I)V

    .line 2668199
    :goto_0
    return-void

    .line 2668200
    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    .line 2668201
    invoke-virtual {p0, p1}, LX/5qw;->b(I)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/uimanager/ViewGroupManager;

    .line 2668202
    new-instance v3, Ljava/util/ArrayList;

    array-length v2, p2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2668203
    array-length v4, p2

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_1

    aget v5, p2, v2

    .line 2668204
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 2668205
    invoke-virtual {p0, v5}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668206
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2668207
    :cond_1
    invoke-virtual {v1, v0, v3}, Lcom/facebook/react/uimanager/ViewGroupManager;->a(Landroid/view/ViewGroup;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(I[LX/JGM;Landroid/util/SparseIntArray;[F[F[LX/JGQ;[LX/JGu;[F[FZ)V
    .locals 6
    .param p2    # [LX/JGM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # [LX/JGQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # [LX/JGu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2668137
    invoke-virtual {p0, p1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/JGs;

    .line 2668138
    if-eqz p2, :cond_0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move/from16 v5, p10

    .line 2668139
    invoke-virtual/range {v0 .. v5}, LX/JGs;->a([LX/JGM;Landroid/util/SparseIntArray;[F[FZ)V

    .line 2668140
    :cond_0
    if-eqz p6, :cond_1

    .line 2668141
    invoke-virtual {v0, p6}, LX/JGs;->a([LX/JGQ;)V

    .line 2668142
    :cond_1
    if-eqz p7, :cond_2

    .line 2668143
    invoke-virtual {v0, p7, p8, p9}, LX/JGs;->a([LX/JGu;[F[F)V

    .line 2668144
    :cond_2
    return-void
.end method

.method public final a(I[LX/JGM;[LX/JGQ;[LX/JGu;)V
    .locals 1
    .param p2    # [LX/JGM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [LX/JGQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [LX/JGu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2668187
    invoke-virtual {p0, p1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/JGs;

    .line 2668188
    if-eqz p2, :cond_0

    .line 2668189
    iput-object p2, v0, LX/JGs;->j:[LX/JGM;

    .line 2668190
    invoke-virtual {v0}, LX/JGs;->invalidate()V

    .line 2668191
    :cond_0
    if-eqz p3, :cond_1

    .line 2668192
    invoke-virtual {v0, p3}, LX/JGs;->a([LX/JGQ;)V

    .line 2668193
    :cond_1
    if-eqz p4, :cond_2

    .line 2668194
    iput-object p4, v0, LX/JGs;->l:[LX/JGu;

    .line 2668195
    :cond_2
    return-void
.end method

.method public final a(Landroid/util/SparseIntArray;)V
    .locals 5

    .prologue
    .line 2668170
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/util/SparseIntArray;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 2668171
    invoke-virtual {p1, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    .line 2668172
    const/4 v0, 0x0

    .line 2668173
    if-lez v1, :cond_1

    .line 2668174
    :try_start_0
    invoke-virtual {p0, v1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2668175
    invoke-virtual {p0, v0}, LX/JGd;->a(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 2668176
    :goto_1
    invoke-virtual {p1, v2}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v0

    .line 2668177
    if-lez v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-nez v4, :cond_0

    .line 2668178
    invoke-virtual {p0, v0}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2668179
    instance-of v4, v0, LX/JGs;

    if-eqz v4, :cond_0

    .line 2668180
    check-cast v0, LX/JGs;

    .line 2668181
    iget-object v4, v0, LX/JGs;->v:LX/JGS;

    if-eqz v4, :cond_0

    .line 2668182
    iget-object v4, v0, LX/JGs;->v:LX/JGS;

    invoke-virtual {v4, v1}, LX/JGS;->a(Landroid/view/View;)V

    .line 2668183
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2668184
    :catch_0
    move-object v1, v0

    goto :goto_1

    .line 2668185
    :cond_1
    neg-int v1, v1

    invoke-virtual {p0, v1}, LX/5qw;->c(I)V

    move-object v1, v0

    goto :goto_1

    .line 2668186
    :cond_2
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2668157
    invoke-super {p0, p1}, LX/5qw;->a(Landroid/view/View;)V

    .line 2668158
    instance-of v0, p1, LX/JGs;

    if-eqz v0, :cond_0

    .line 2668159
    check-cast p1, LX/JGs;

    .line 2668160
    invoke-virtual {p1}, LX/JGs;->getRemoveClippedSubviews()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2668161
    iget-object v0, p1, LX/JGs;->v:LX/JGS;

    if-nez v0, :cond_1

    .line 2668162
    sget-object v0, LX/JGs;->u:Landroid/util/SparseArray;

    .line 2668163
    :goto_0
    move-object v2, v0

    .line 2668164
    const/4 v0, 0x0

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    .line 2668165
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2668166
    :try_start_0
    invoke-virtual {p0, v0}, LX/JGd;->a(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2668167
    :goto_2
    invoke-virtual {p1, v0}, LX/JGs;->b(Landroid/view/View;)V

    .line 2668168
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :catch_0
    goto :goto_2

    .line 2668169
    :cond_0
    return-void

    :cond_1
    iget-object v0, p1, LX/JGs;->v:LX/JGS;

    invoke-virtual {v0}, LX/JGS;->c()Landroid/util/SparseArray;

    move-result-object v0

    goto :goto_0
.end method

.method public final a([I)V
    .locals 5

    .prologue
    .line 2668148
    array-length v3, p1

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    aget v1, p1, v2

    .line 2668149
    invoke-virtual {p0, v1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2668150
    instance-of v4, v0, LX/JGs;

    if-eqz v4, :cond_0

    .line 2668151
    check-cast v0, LX/JGs;

    invoke-virtual {v0}, LX/JGs;->detachAllViewsFromParent()V

    .line 2668152
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2668153
    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    .line 2668154
    invoke-virtual {p0, v1}, LX/5qw;->b(I)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/uimanager/ViewGroupManager;

    .line 2668155
    invoke-virtual {v1, v0}, Lcom/facebook/react/uimanager/ViewGroupManager;->c(Landroid/view/ViewGroup;)V

    goto :goto_1

    .line 2668156
    :cond_1
    return-void
.end method

.method public final b(IIIII)V
    .locals 1

    .prologue
    .line 2668146
    invoke-virtual {p0, p1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/View;->setPadding(IIII)V

    .line 2668147
    return-void
.end method

.method public final d(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2668145
    invoke-super {p0, p1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
