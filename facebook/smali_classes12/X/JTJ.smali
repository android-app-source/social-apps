.class public final LX/JTJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JTL;


# direct methods
.method public constructor <init>(LX/JTL;)V
    .locals 0

    .prologue
    .line 2696566
    iput-object p1, p0, LX/JTJ;->a:LX/JTL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0xb830b97

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2696567
    iget-object v1, p0, LX/JTJ;->a:LX/JTL;

    iget-object v1, v1, LX/JTL;->c:LX/JSe;

    .line 2696568
    iget-object v3, v1, LX/JSe;->i:Landroid/net/Uri;

    move-object v1, v3

    .line 2696569
    if-nez v1, :cond_0

    .line 2696570
    const v1, 0x21949401

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2696571
    :goto_0
    return-void

    .line 2696572
    :cond_0
    iget-object v2, p0, LX/JTJ;->a:LX/JTL;

    iget-object v2, v2, LX/JTL;->a:LX/JTM;

    iget-object v2, v2, LX/JTM;->b:LX/JTI;

    iget-object v3, p0, LX/JTJ;->a:LX/JTL;

    iget-object v3, v3, LX/JTL;->d:LX/JTY;

    iget-object v4, p0, LX/JTJ;->a:LX/JTL;

    .line 2696573
    new-instance p0, LX/JTK;

    invoke-direct {p0, v4}, LX/JTK;-><init>(LX/JTL;)V

    move-object v4, p0

    .line 2696574
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696575
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696576
    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    .line 2696577
    const/4 v5, 0x0

    move v6, v5

    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_4

    .line 2696578
    const-string v8, "track"

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v6, v5, :cond_3

    .line 2696579
    add-int/lit8 v5, v6, 0x1

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2696580
    :goto_2
    move-object v5, v5

    .line 2696581
    if-nez v5, :cond_2

    .line 2696582
    if-eqz v3, :cond_1

    .line 2696583
    sget-object v5, LX/JTV;->deep_link:LX/JTV;

    invoke-virtual {v3, v5}, LX/JTY;->a(LX/JTV;)V

    .line 2696584
    :cond_1
    invoke-virtual {v2, v1}, LX/JTI;->a(Landroid/net/Uri;)V

    .line 2696585
    :cond_2
    iget-object v5, v2, LX/JTI;->e:LX/JTO;

    const/4 v6, 0x0

    iget-object v7, v2, LX/JTI;->e:LX/JTO;

    .line 2696586
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696587
    new-instance v8, LX/JTG;

    move-object v9, v2

    move-object v10, v3

    move-object v11, v1

    move-object v12, v7

    move-object v13, v4

    invoke-direct/range {v8 .. v13}, LX/JTG;-><init>(LX/JTI;LX/JTY;Landroid/net/Uri;LX/JTO;LX/JTK;)V

    move-object v7, v8

    .line 2696588
    invoke-virtual {v5, v6, v1, v4, v7}, LX/JTO;->a(Ljava/lang/String;Landroid/net/Uri;LX/JTK;LX/JTF;)V

    .line 2696589
    const v1, 0x65fc03b0

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2696590
    :cond_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 2696591
    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method
