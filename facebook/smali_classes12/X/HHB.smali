.class public LX/HHB;
.super LX/Dcc;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/0tX;

.field private final c:LX/DvI;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/DvI;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2447266
    invoke-direct {p0}, LX/Dcc;-><init>()V

    .line 2447267
    iput-object p1, p0, LX/HHB;->a:Ljava/util/concurrent/ExecutorService;

    .line 2447268
    iput-object p2, p0, LX/HHB;->b:LX/0tX;

    .line 2447269
    iput-object p3, p0, LX/HHB;->c:LX/DvI;

    .line 2447270
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;",
            "IZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2447271
    new-instance v0, LX/HHH;

    invoke-direct {v0}, LX/HHH;-><init>()V

    move-object v0, v0

    .line 2447272
    check-cast p3, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    .line 2447273
    const-string v1, "page_id"

    .line 2447274
    iget-object v2, p3, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2447275
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "before"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after"

    invoke-virtual {v1, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "count"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2447276
    iget-object v1, p0, LX/HHB;->c:LX/DvI;

    invoke-virtual {v1, v0}, LX/DvI;->a(LX/0gW;)LX/0gW;

    .line 2447277
    iget-object v1, p0, LX/HHB;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    if-eqz p5, :cond_0

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    invoke-virtual {v2, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2447278
    new-instance v1, LX/HHA;

    invoke-direct {v1}, LX/HHA;-><init>()V

    iget-object v2, p0, LX/HHB;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2447279
    :cond_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
