.class public LX/Hly;
.super LX/45y;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/Hly;


# instance fields
.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2GC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Hli;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2D6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2499694
    const-class v0, LX/Hly;

    sput-object v0, LX/Hly;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2499692
    invoke-direct {p0}, LX/45y;-><init>()V

    .line 2499693
    return-void
.end method

.method public static a(LX/0QB;)LX/Hly;
    .locals 10

    .prologue
    .line 2499671
    sget-object v0, LX/Hly;->i:LX/Hly;

    if-nez v0, :cond_1

    .line 2499672
    const-class v1, LX/Hly;

    monitor-enter v1

    .line 2499673
    :try_start_0
    sget-object v0, LX/Hly;->i:LX/Hly;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2499674
    if-eqz v2, :cond_0

    .line 2499675
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2499676
    new-instance v3, LX/Hly;

    invoke-direct {v3}, LX/Hly;-><init>()V

    .line 2499677
    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/2GC;->a(LX/0QB;)LX/2GC;

    move-result-object v6

    check-cast v6, LX/2GC;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    invoke-static {v0}, LX/Hli;->a(LX/0QB;)LX/Hli;

    move-result-object v8

    check-cast v8, LX/Hli;

    invoke-static {v0}, LX/2D6;->a(LX/0QB;)LX/2D6;

    move-result-object v9

    check-cast v9, LX/2D6;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    .line 2499678
    iput-object v4, v3, LX/Hly;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v5, v3, LX/Hly;->c:LX/0Or;

    iput-object v6, v3, LX/Hly;->d:LX/2GC;

    iput-object v7, v3, LX/Hly;->e:LX/0Uo;

    iput-object v8, v3, LX/Hly;->f:LX/Hli;

    iput-object v9, v3, LX/Hly;->g:LX/2D6;

    iput-object p0, v3, LX/Hly;->h:LX/0W3;

    .line 2499679
    move-object v0, v3

    .line 2499680
    sput-object v0, LX/Hly;->i:LX/Hly;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2499681
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2499682
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2499683
    :cond_1
    sget-object v0, LX/Hly;->i:LX/Hly;

    return-object v0

    .line 2499684
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2499685
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2499691
    const/4 v0, 0x0

    return v0
.end method

.method public final a(ILandroid/os/Bundle;LX/45o;)Z
    .locals 3

    .prologue
    .line 2499686
    iget-object v0, p0, LX/Hly;->g:LX/2D6;

    invoke-virtual {v0}, LX/2D6;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2499687
    const/4 v0, 0x0

    .line 2499688
    :goto_0
    return v0

    .line 2499689
    :cond_0
    iget-object v0, p0, LX/Hly;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;

    invoke-direct {v1, p0, p3}, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;-><init>(LX/Hly;LX/45o;)V

    const v2, -0x677f0cb5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2499690
    const/4 v0, 0x1

    goto :goto_0
.end method
