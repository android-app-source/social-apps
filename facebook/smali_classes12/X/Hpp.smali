.class public LX/Hpp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/Hpt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0lB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Hpg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2509900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2509901
    return-void
.end method

.method public static a(LX/0QB;)LX/Hpp;
    .locals 6

    .prologue
    .line 2509902
    const-class v1, LX/Hpp;

    monitor-enter v1

    .line 2509903
    :try_start_0
    sget-object v0, LX/Hpp;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2509904
    sput-object v2, LX/Hpp;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2509905
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2509906
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2509907
    new-instance p0, LX/Hpp;

    invoke-direct {p0}, LX/Hpp;-><init>()V

    .line 2509908
    invoke-static {v0}, LX/Hpt;->a(LX/0QB;)LX/Hpt;

    move-result-object v3

    check-cast v3, LX/Hpt;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    const-class v5, LX/Hpg;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Hpg;

    .line 2509909
    iput-object v3, p0, LX/Hpp;->a:LX/Hpt;

    iput-object v4, p0, LX/Hpp;->b:LX/0lB;

    iput-object v5, p0, LX/Hpp;->c:LX/Hpg;

    .line 2509910
    move-object v0, p0

    .line 2509911
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2509912
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hpp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2509913
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2509914
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/Hpp;LX/BcP;LX/5KI;Lcom/facebook/java2js/JSValue;)LX/BcO;
    .locals 4

    .prologue
    .line 2509915
    const-string v0, "sectionType"

    invoke-virtual {p3, v0}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2509916
    const-string v0, "props"

    invoke-virtual {p3, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    .line 2509917
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2509918
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2509919
    :sswitch_0
    const-string v3, "CSComponentSection"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "CSGraphQLConnectionSection"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "CSGroupSection"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 2509920
    :pswitch_0
    const/4 v3, 0x0

    .line 2509921
    const-string v0, "componentGenerator"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    .line 2509922
    invoke-virtual {v0}, Lcom/facebook/java2js/JSValue;->isNull()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/java2js/JSValue;->isUndefined()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    const/4 v0, 0x0

    .line 2509923
    :goto_1
    if-nez v0, :cond_5

    new-instance v0, LX/Hpo;

    invoke-direct {v0, p0}, LX/Hpo;-><init>(LX/Hpp;)V

    .line 2509924
    :goto_2
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object v0

    const-string v1, "key"

    invoke-virtual {v2, v1}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Bcc;->b(Ljava/lang/String;)LX/Bcc;

    move-result-object v0

    invoke-virtual {v0}, LX/Bcc;->b()LX/BcO;

    move-result-object v0

    move-object v0, v0

    .line 2509925
    :goto_3
    return-object v0

    .line 2509926
    :pswitch_1
    const-string v0, "queryRequest"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v1

    .line 2509927
    const-string v0, "renderEdge"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    .line 2509928
    iget-object v0, p0, LX/Hpp;->a:LX/Hpt;

    .line 2509929
    new-instance p2, LX/Hps;

    invoke-direct {p2, v0}, LX/Hps;-><init>(LX/Hpt;)V

    .line 2509930
    sget-object p3, LX/Hpt;->a:LX/0Zi;

    invoke-virtual {p3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/Hpr;

    .line 2509931
    if-nez p3, :cond_2

    .line 2509932
    new-instance p3, LX/Hpr;

    invoke-direct {p3}, LX/Hpr;-><init>()V

    .line 2509933
    :cond_2
    iput-object p2, p3, LX/BcN;->a:LX/BcO;

    .line 2509934
    iput-object p2, p3, LX/Hpr;->a:LX/Hps;

    .line 2509935
    iget-object v0, p3, LX/Hpr;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2509936
    move-object p2, p3

    .line 2509937
    move-object p2, p2

    .line 2509938
    const-string v0, "toolbox"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    const-class p3, LX/Hpa;

    invoke-virtual {v0, p3}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hpa;

    .line 2509939
    iget-object p3, p2, LX/Hpr;->a:LX/Hps;

    iput-object v0, p3, LX/Hps;->b:LX/Hpa;

    .line 2509940
    iget-object p3, p2, LX/Hpr;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {p3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2509941
    move-object v0, p2

    .line 2509942
    const-string p2, "queryParams"

    invoke-virtual {v1, p2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object p2

    const-class p3, Ljava/lang/Object;

    invoke-virtual {p2, p3}, Lcom/facebook/java2js/JSValue;->asMap(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object p2

    .line 2509943
    iget-object p3, v0, LX/Hpr;->a:LX/Hps;

    iput-object p2, p3, LX/Hps;->c:Ljava/util/Map;

    .line 2509944
    iget-object p3, v0, LX/Hpr;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {p3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2509945
    move-object p2, v0

    .line 2509946
    const-string v0, "connectionPath"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    const-class p3, Ljava/lang/String;

    invoke-virtual {v0, p3}, Lcom/facebook/java2js/JSValue;->asArray(Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 2509947
    iget-object p3, p2, LX/Hpr;->a:LX/Hps;

    iput-object v0, p3, LX/Hps;->d:[Ljava/lang/String;

    .line 2509948
    iget-object p3, p2, LX/Hpr;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {p3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2509949
    move-object v0, p2

    .line 2509950
    const p2, -0x613b35b8

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v3, p3, p0

    invoke-static {p1, p2, p3}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p2

    move-object v3, p2

    .line 2509951
    iget-object p2, v0, LX/Hpr;->a:LX/Hps;

    iput-object v3, p2, LX/Hps;->g:LX/BcQ;

    .line 2509952
    move-object v0, v0

    .line 2509953
    const-string v3, "batch"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v1

    const-string v3, "name"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2509954
    new-instance v3, LX/Hpf;

    invoke-direct {v3, v1}, LX/Hpf;-><init>(Ljava/lang/String;)V

    .line 2509955
    move-object v1, v3

    .line 2509956
    iget-object v3, v0, LX/Hpr;->a:LX/Hps;

    iput-object v1, v3, LX/Hps;->e:LX/Hpf;

    .line 2509957
    iget-object v3, v0, LX/Hpr;->d:Ljava/util/BitSet;

    const/4 p2, 0x3

    invoke-virtual {v3, p2}, Ljava/util/BitSet;->set(I)V

    .line 2509958
    move-object v0, v0

    .line 2509959
    invoke-virtual {v0}, LX/Hpr;->b()LX/BcO;

    move-result-object v0

    move-object v0, v0

    .line 2509960
    goto/16 :goto_3

    .line 2509961
    :pswitch_2
    const-string v0, "children"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    const-class v1, Lcom/facebook/java2js/JSValue;

    invoke-virtual {v0, v1}, Lcom/facebook/java2js/JSValue;->asArray(Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/java2js/JSValue;

    .line 2509962
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, LX/Hpn;

    invoke-direct {v1, p0, p1, p2}, LX/Hpn;-><init>(LX/Hpp;LX/BcP;LX/5KI;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    .line 2509963
    new-instance v1, LX/Hpw;

    invoke-direct {v1}, LX/Hpw;-><init>()V

    .line 2509964
    sget-object p0, LX/Hpx;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Hpv;

    .line 2509965
    if-nez p0, :cond_3

    .line 2509966
    new-instance p0, LX/Hpv;

    invoke-direct {p0}, LX/Hpv;-><init>()V

    .line 2509967
    :cond_3
    iput-object v1, p0, LX/BcN;->a:LX/BcO;

    .line 2509968
    iput-object v1, p0, LX/Hpv;->a:LX/Hpw;

    .line 2509969
    iget-object p2, p0, LX/Hpv;->d:Ljava/util/BitSet;

    invoke-virtual {p2}, Ljava/util/BitSet;->clear()V

    .line 2509970
    move-object v1, p0

    .line 2509971
    move-object v1, v1

    .line 2509972
    iget-object p0, v1, LX/Hpv;->a:LX/Hpw;

    iput-object v0, p0, LX/Hpw;->b:Ljava/util/List;

    .line 2509973
    iget-object p0, v1, LX/Hpv;->d:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2509974
    move-object v0, v1

    .line 2509975
    invoke-virtual {v0}, LX/Hpv;->b()LX/BcO;

    move-result-object v0

    move-object v0, v0

    .line 2509976
    goto/16 :goto_3

    .line 2509977
    :cond_4
    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/facebook/java2js/JSValue;->callAsFunction([Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v0

    goto/16 :goto_1

    .line 2509978
    :cond_5
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2412280a -> :sswitch_2
        0x601fddce -> :sswitch_1
        0x6eb03fb8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
