.class public final LX/IcD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IcA;

.field public final synthetic b:LX/IcF;


# direct methods
.method public constructor <init>(LX/IcF;LX/IcA;)V
    .locals 0

    .prologue
    .line 2595304
    iput-object p1, p0, LX/IcD;->b:LX/IcF;

    iput-object p2, p0, LX/IcD;->a:LX/IcA;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2595305
    iget-object v0, p0, LX/IcD;->a:LX/IcA;

    if-eqz v0, :cond_0

    .line 2595306
    iget-object v0, p0, LX/IcD;->a:LX/IcA;

    invoke-virtual {v0}, LX/IcA;->a()V

    .line 2595307
    :cond_0
    iget-object v0, p0, LX/IcD;->b:LX/IcF;

    iget-object v0, v0, LX/IcF;->d:LX/03V;

    const-string v1, "RidePromoShareLoader"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595308
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2595309
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v4, -0x62cf0c15

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2595310
    iget-object v0, p0, LX/IcD;->a:LX/IcA;

    if-nez v0, :cond_0

    .line 2595311
    :goto_0
    return-void

    .line 2595312
    :cond_0
    if-eqz p1, :cond_1

    .line 2595313
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595314
    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_9

    .line 2595315
    iget-object v0, p0, LX/IcD;->b:LX/IcF;

    iget-object v0, v0, LX/IcF;->d:LX/03V;

    const-string v1, "RidePromoShareLoader"

    const-string v2, "GraphQL return invalid results"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595316
    iget-object v0, p0, LX/IcD;->a:LX/IcA;

    invoke-virtual {v0}, LX/IcA;->a()V

    goto :goto_0

    .line 2595317
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595318
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2595319
    if-nez v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    .line 2595320
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595321
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2595322
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_2

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    .line 2595323
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595324
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_5
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2595325
    invoke-virtual {v3, v0, v1}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_3

    .line 2595326
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_5

    :cond_8
    move v0, v2

    .line 2595327
    goto :goto_3

    .line 2595328
    :cond_9
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595329
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_6
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2595330
    iget-object v3, p0, LX/IcD;->a:LX/IcA;

    invoke-virtual {v2, v0, v1}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v2, v0}, LX/IcA;->a(LX/15i;I)V

    goto/16 :goto_0

    .line 2595331
    :cond_a
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_6
.end method
