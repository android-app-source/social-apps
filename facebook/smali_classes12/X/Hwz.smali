.class public final LX/Hwz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/7oa;",
        "Lcom/facebook/events/carousel/EventCardViewBinder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hx5;


# direct methods
.method public constructor <init>(LX/Hx5;)V
    .locals 0

    .prologue
    .line 2521508
    iput-object p1, p0, LX/Hwz;->a:LX/Hx5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2521497
    check-cast p1, LX/7oa;

    .line 2521498
    if-nez p1, :cond_0

    .line 2521499
    const/4 v0, 0x0

    .line 2521500
    :goto_0
    return-object v0

    .line 2521501
    :cond_0
    invoke-static {p1}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v0

    .line 2521502
    invoke-interface {p1}, LX/7oa;->W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2521503
    invoke-interface {p1}, LX/7oa;->W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2521504
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2521505
    iput-object v1, v0, LX/7vC;->ak:Ljava/lang/String;

    .line 2521506
    :cond_1
    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2521507
    iget-object v1, p0, LX/Hwz;->a:LX/Hx5;

    iget-object v1, v1, LX/Hx5;->M:LX/Gct;

    invoke-interface {p1}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v2

    iget-object v3, p0, LX/Hwz;->a:LX/Hx5;

    iget-object v3, v3, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    const/4 v4, 0x1

    new-array v4, v4, [LX/I3G;

    const/4 v5, 0x0

    iget-object v6, p0, LX/Hwz;->a:LX/Hx5;

    iget-object v6, v6, LX/Hx5;->K:LX/I3G;

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, LX/Gct;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/util/List;)Lcom/facebook/events/carousel/EventCardViewBinder;

    move-result-object v0

    goto :goto_0
.end method
