.class public final LX/HXf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:Landroid/view/View;

.field public final synthetic d:LX/HXi;


# direct methods
.method public constructor <init>(LX/HXi;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2479204
    iput-object p1, p0, LX/HXf;->d:LX/HXi;

    iput-object p2, p0, LX/HXf;->a:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object p3, p0, LX/HXf;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p4, p0, LX/HXf;->c:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 11

    .prologue
    .line 2479198
    iget-object v0, p0, LX/HXf;->d:LX/HXi;

    iget-object v8, v0, LX/D2z;->b:LX/BPq;

    new-instance v0, LX/BPR;

    iget-object v1, p0, LX/HXf;->d:LX/HXi;

    iget-object v1, v1, LX/D2z;->a:LX/5SB;

    .line 2479199
    iget-object v2, v1, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v1, v2

    .line 2479200
    iget-object v2, p0, LX/HXf;->d:LX/HXi;

    iget-object v2, v2, LX/D2z;->a:LX/5SB;

    .line 2479201
    iget-wide v9, v2, LX/5SB;->b:J

    move-wide v2, v9

    .line 2479202
    iget-object v4, p0, LX/HXf;->a:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v6, p0, LX/HXf;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/HXf;->c:Landroid/view/View;

    invoke-direct/range {v0 .. v7}, LX/BPR;-><init>(Landroid/os/ParcelUuid;JJLjava/lang/String;Landroid/view/View;)V

    invoke-virtual {v8, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2479203
    return-void
.end method
