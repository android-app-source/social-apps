.class public LX/IPO;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IPO;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574109
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2574110
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2574111
    const-string v1, "extra_parent_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2574112
    sget-object v1, LX/0ax;->G:Ljava/lang/String;

    const-string v2, "extra_navigation_source"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/ReactFragmentActivity;

    sget-object v3, LX/0cQ;->GROUPS_DISCOVER_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2574113
    sget-object v1, LX/0ax;->V:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/ReactFragmentActivity;

    sget-object v3, LX/0cQ;->GROUPS_DISCOVER_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2574114
    sget-object v1, LX/0ax;->T:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/ReactFragmentActivity;

    sget-object v3, LX/0cQ;->GROUPS_DISCOVER_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2574115
    return-void
.end method

.method public static a(LX/0QB;)LX/IPO;
    .locals 3

    .prologue
    .line 2574116
    sget-object v0, LX/IPO;->a:LX/IPO;

    if-nez v0, :cond_1

    .line 2574117
    const-class v1, LX/IPO;

    monitor-enter v1

    .line 2574118
    :try_start_0
    sget-object v0, LX/IPO;->a:LX/IPO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2574119
    if-eqz v2, :cond_0

    .line 2574120
    :try_start_1
    new-instance v0, LX/IPO;

    invoke-direct {v0}, LX/IPO;-><init>()V

    .line 2574121
    move-object v0, v0

    .line 2574122
    sput-object v0, LX/IPO;->a:LX/IPO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574123
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2574124
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2574125
    :cond_1
    sget-object v0, LX/IPO;->a:LX/IPO;

    return-object v0

    .line 2574126
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2574127
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
