.class public LX/JN2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JN2;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2685097
    return-void
.end method

.method public static a(LX/0QB;)LX/JN2;
    .locals 3

    .prologue
    .line 2685098
    sget-object v0, LX/JN2;->a:LX/JN2;

    if-nez v0, :cond_1

    .line 2685099
    const-class v1, LX/JN2;

    monitor-enter v1

    .line 2685100
    :try_start_0
    sget-object v0, LX/JN2;->a:LX/JN2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2685101
    if-eqz v2, :cond_0

    .line 2685102
    :try_start_1
    new-instance v0, LX/JN2;

    invoke-direct {v0}, LX/JN2;-><init>()V

    .line 2685103
    move-object v0, v0

    .line 2685104
    sput-object v0, LX/JN2;->a:LX/JN2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685105
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2685106
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2685107
    :cond_1
    sget-object v0, LX/JN2;->a:LX/JN2;

    return-object v0

    .line 2685108
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2685109
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 3

    .prologue
    .line 2685110
    const-class v0, LX/JN3;

    sget-object v1, LX/0ja;->a:LX/3AL;

    sget-object v2, LX/0ja;->e:LX/3AM;

    invoke-virtual {p1, v0, v1, v2}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    .line 2685111
    return-void
.end method
