.class public LX/Hy6;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field public a:LX/HxP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2523556
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2523557
    const-class v0, LX/Hy6;

    invoke-static {v0, p0}, LX/Hy6;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2523558
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2523559
    const v0, 0x7f0a00d5

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/Hy6;->setBackgroundColor(I)V

    .line 2523560
    const v0, 0x7f0e08ea

    invoke-virtual {p0, p1, v0}, LX/Hy6;->setTextAppearance(Landroid/content/Context;I)V

    .line 2523561
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, LX/Hy6;->setGravity(I)V

    .line 2523562
    const v0, 0x7f0b1586

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v0, p0, LX/Hy6;->a:LX/HxP;

    invoke-virtual {v0}, LX/HxP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b15ec

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    const v3, 0x7f0b1586

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0b1587

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2523563
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x11

    if-lt v5, p1, :cond_1

    .line 2523564
    invoke-virtual {p0, v2, v0, v3, v4}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 2523565
    :goto_1
    const v0, 0x7f0b1588

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/Hy6;->setMinimumHeight(I)V

    .line 2523566
    return-void

    .line 2523567
    :cond_0
    const v0, 0x7f0b1587

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 2523568
    :cond_1
    invoke-virtual {p0, v2, v0, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Hy6;

    invoke-static {p0}, LX/HxP;->a(LX/0QB;)LX/HxP;

    move-result-object v1

    check-cast v1, LX/HxP;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    iput-object v1, p1, LX/Hy6;->a:LX/HxP;

    iput-object p0, p1, LX/Hy6;->b:Ljava/lang/Boolean;

    return-void
.end method

.method public static b(LX/Hy6;LX/Hx6;)V
    .locals 3
    .param p0    # LX/Hy6;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2523569
    sget-object v0, LX/Hy5;->a:[I

    invoke-virtual {p1}, LX/Hx6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2523570
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported filter type for empty list label: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2523571
    :pswitch_0
    const v0, 0x7f0821a0

    invoke-virtual {p0, v0}, LX/Hy6;->setText(I)V

    .line 2523572
    :goto_0
    return-void

    .line 2523573
    :pswitch_1
    const v0, 0x7f0821a1

    invoke-virtual {p0, v0}, LX/Hy6;->setText(I)V

    goto :goto_0

    .line 2523574
    :pswitch_2
    iget-object v0, p0, LX/Hy6;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0821a3

    :goto_1
    invoke-virtual {p0, v0}, LX/Hy6;->setText(I)V

    goto :goto_0

    :cond_0
    const v0, 0x7f0821a2

    goto :goto_1

    .line 2523575
    :pswitch_3
    const v0, 0x7f0821a4

    invoke-virtual {p0, v0}, LX/Hy6;->setText(I)V

    goto :goto_0

    .line 2523576
    :pswitch_4
    const v0, 0x7f0821a6

    invoke-virtual {p0, v0}, LX/Hy6;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
