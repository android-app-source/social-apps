.class public final LX/J7Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J7b;


# direct methods
.method public constructor <init>(LX/J7b;)V
    .locals 0

    .prologue
    .line 2651035
    iput-object p1, p0, LX/J7Z;->a:LX/J7b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2651036
    iget-object v0, p0, LX/J7Z;->a:LX/J7b;

    iget-object v0, v0, LX/J7b;->b:LX/03V;

    const-string v1, "fail_to_fetch_story_gallery_survey_with_story"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2651037
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2651038
    check-cast p1, LX/0Px;

    .line 2651039
    iget-object v0, p0, LX/J7Z;->a:LX/J7b;

    .line 2651040
    iput-object p1, v0, LX/J7b;->f:LX/0Px;

    .line 2651041
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2651042
    iget-object p0, v0, LX/J7b;->e:LX/J7a;

    invoke-interface {p0}, LX/J7a;->b()V

    .line 2651043
    :goto_0
    return-void

    .line 2651044
    :cond_0
    iget-object p0, v0, LX/J7b;->e:LX/J7a;

    invoke-interface {p0}, LX/J7a;->a()V

    .line 2651045
    iget-object p0, v0, LX/J7b;->e:LX/J7a;

    invoke-interface {p0}, LX/J7a;->c()V

    goto :goto_0
.end method
