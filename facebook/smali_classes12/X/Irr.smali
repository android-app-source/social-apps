.class public final LX/Irr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V
    .locals 0

    .prologue
    .line 2618776
    iput-object p1, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2618777
    iget-object v2, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-boolean v2, v2, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->x:Z

    if-nez v2, :cond_0

    .line 2618778
    :goto_0
    return v0

    .line 2618779
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 2618780
    if-nez v2, :cond_2

    .line 2618781
    iget-object v3, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    .line 2618782
    iput-boolean v0, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->w:Z

    .line 2618783
    iget-object v3, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    .line 2618784
    iget-object v7, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v7}, LX/Ird;->h()I

    move-result v8

    move v7, v6

    :goto_1
    if-ge v7, v8, :cond_1

    .line 2618785
    invoke-static {v3, v4, v5, v7}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a$redex0(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;III)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 2618786
    const/4 v6, 0x1

    .line 2618787
    :cond_1
    move v3, v6

    .line 2618788
    if-nez v3, :cond_2

    .line 2618789
    iget-object v0, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c()Z

    move-result v0

    goto :goto_0

    .line 2618790
    :cond_2
    iget-object v3, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v3}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2618791
    iget-object v3, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->q:LX/Irc;

    invoke-virtual {v3, p2}, LX/Irc;->a(Landroid/view/MotionEvent;)Z

    .line 2618792
    iget-object v3, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->r:LX/IrY;

    invoke-virtual {v3, p2}, LX/IrY;->a(Landroid/view/MotionEvent;)Z

    .line 2618793
    :cond_3
    iget-object v3, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->p:Landroid/view/GestureDetector;

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2618794
    if-ne v2, v1, :cond_5

    .line 2618795
    iget-object v2, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    .line 2618796
    iput-boolean v0, v2, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->v:Z

    .line 2618797
    iget-object v0, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    if-eqz v0, :cond_4

    .line 2618798
    iget-object v0, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    iget-object v2, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-boolean v2, v2, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->v:Z

    invoke-virtual {v0, v2}, LX/IrL;->a(Z)V

    .line 2618799
    :cond_4
    iget-object v0, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    .line 2618800
    iget-boolean v2, v0, LX/Iqk;->h:Z

    move v0, v2

    .line 2618801
    if-eqz v0, :cond_5

    .line 2618802
    iget-object v0, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    iget-object v2, p0, LX/Irr;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v2, v2, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    .line 2618803
    iget-object v3, v2, LX/Iqk;->b:LX/Iqg;

    move-object v2, v3

    .line 2618804
    invoke-virtual {v0, v2}, LX/Ird;->c(LX/Iqg;)V

    :cond_5
    move v0, v1

    .line 2618805
    goto/16 :goto_0

    .line 2618806
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method
