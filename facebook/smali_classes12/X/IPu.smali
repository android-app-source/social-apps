.class public LX/IPu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2574829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ILX/0fz;LX/1Qq;)I
    .locals 4

    .prologue
    .line 2574830
    const/4 v0, 0x1

    invoke-static {v0, p0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2574831
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2574832
    invoke-virtual {p1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    .line 2574833
    const v3, 0x4c808d5

    if-ne v2, v3, :cond_0

    .line 2574834
    add-int/lit8 v1, v1, -0x1

    .line 2574835
    if-nez v1, :cond_0

    .line 2574836
    invoke-interface {p2, v0}, LX/1Qr;->n_(I)I

    move-result v0

    .line 2574837
    :goto_1
    return v0

    .line 2574838
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2574839
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
