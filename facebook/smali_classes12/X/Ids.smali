.class public LX/Ids;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Idf;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:[LX/Idf;

.field private final c:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/Idr;",
            ">;"
        }
    .end annotation
.end field

.field private d:[J

.field private e:I

.field private f:[I

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2598071
    const-class v0, LX/Ids;

    sput-object v0, LX/Ids;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>([LX/Idf;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2598034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2598035
    invoke-virtual {p1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Idf;

    iput-object v0, p0, LX/Ids;->b:[LX/Idf;

    .line 2598036
    new-instance v2, Ljava/util/PriorityQueue;

    const/4 v3, 0x4

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    new-instance v0, LX/Idp;

    invoke-direct {v0}, LX/Idp;-><init>()V

    :goto_0
    invoke-direct {v2, v3, v0}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v2, p0, LX/Ids;->c:Ljava/util/PriorityQueue;

    .line 2598037
    const/16 v0, 0x10

    new-array v0, v0, [J

    iput-object v0, p0, LX/Ids;->d:[J

    .line 2598038
    iput v1, p0, LX/Ids;->e:I

    .line 2598039
    iget-object v0, p0, LX/Ids;->b:[LX/Idf;

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/Ids;->f:[I

    .line 2598040
    const/4 v0, -0x1

    iput v0, p0, LX/Ids;->g:I

    .line 2598041
    iget-object v0, p0, LX/Ids;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->clear()V

    .line 2598042
    iget-object v0, p0, LX/Ids;->b:[LX/Idf;

    array-length v2, v0

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    .line 2598043
    iget-object v1, p0, LX/Ids;->b:[LX/Idf;

    aget-object v1, v1, v0

    .line 2598044
    new-instance v3, LX/Idr;

    invoke-direct {v3, v1, v0}, LX/Idr;-><init>(LX/Idf;I)V

    .line 2598045
    invoke-virtual {v3}, LX/Idr;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Ids;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v3}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 2598046
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2598047
    :cond_1
    new-instance v0, LX/Idq;

    invoke-direct {v0}, LX/Idq;-><init>()V

    goto :goto_0

    .line 2598048
    :cond_2
    return-void
.end method

.method private c()LX/Idr;
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const/4 v4, 0x0

    .line 2598022
    iget-object v0, p0, LX/Ids;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idr;

    .line 2598023
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2598024
    :goto_0
    return-object v0

    .line 2598025
    :cond_0
    iget v1, v0, LX/Idr;->a:I

    iget v2, p0, LX/Ids;->g:I

    if-ne v1, v2, :cond_1

    .line 2598026
    iget v1, p0, LX/Ids;->e:I

    add-int/lit8 v1, v1, -0x1

    .line 2598027
    iget-object v2, p0, LX/Ids;->d:[J

    aget-wide v4, v2, v1

    add-long/2addr v4, v6

    aput-wide v4, v2, v1

    goto :goto_0

    .line 2598028
    :cond_1
    iget v1, v0, LX/Idr;->a:I

    iput v1, p0, LX/Ids;->g:I

    .line 2598029
    iget-object v1, p0, LX/Ids;->d:[J

    array-length v1, v1

    iget v2, p0, LX/Ids;->e:I

    if-ne v1, v2, :cond_2

    .line 2598030
    iget v1, p0, LX/Ids;->e:I

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [J

    .line 2598031
    iget-object v2, p0, LX/Ids;->d:[J

    iget v3, p0, LX/Ids;->e:I

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2598032
    iput-object v1, p0, LX/Ids;->d:[J

    .line 2598033
    :cond_2
    iget-object v1, p0, LX/Ids;->d:[J

    iget v2, p0, LX/Ids;->e:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/Ids;->e:I

    iget v3, p0, LX/Ids;->g:I

    int-to-long v4, v3

    const/16 v3, 0x20

    shl-long/2addr v4, v3

    or-long/2addr v4, v6

    aput-wide v4, v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/Idk;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2598049
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/Ids;->b()I

    move-result v1

    if-le p1, v1, :cond_1

    .line 2598050
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " out of range max is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/Ids;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2598051
    :cond_1
    iget-object v1, p0, LX/Ids;->f:[I

    .line 2598052
    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 2598053
    iget v3, p0, LX/Ids;->e:I

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    .line 2598054
    iget-object v2, p0, LX/Ids;->d:[J

    aget-wide v4, v2, v1

    .line 2598055
    const-wide/16 v6, -0x1

    and-long/2addr v6, v4

    long-to-int v6, v6

    .line 2598056
    const/16 v2, 0x20

    shr-long/2addr v4, v2

    long-to-int v4, v4

    .line 2598057
    add-int v2, v0, v6

    if-le v2, p1, :cond_3

    .line 2598058
    iget-object v1, p0, LX/Ids;->f:[I

    aget v1, v1, v4

    sub-int v0, p1, v0

    add-int/2addr v0, v1

    .line 2598059
    iget-object v1, p0, LX/Ids;->b:[LX/Idf;

    aget-object v1, v1, v4

    invoke-interface {v1, v0}, LX/Idf;->a(I)LX/Idk;

    move-result-object v0

    .line 2598060
    :cond_2
    :goto_1
    return-object v0

    .line 2598061
    :cond_3
    add-int v2, v0, v6

    .line 2598062
    iget-object v0, p0, LX/Ids;->f:[I

    aget v5, v0, v4

    add-int/2addr v5, v6

    aput v5, v0, v4

    .line 2598063
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 2598064
    :cond_4
    invoke-virtual {v1}, LX/Idr;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ids;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 2598065
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 2598066
    :cond_6
    invoke-direct {p0}, LX/Ids;->c()LX/Idr;

    move-result-object v1

    .line 2598067
    if-nez v1, :cond_7

    const/4 v0, 0x0

    goto :goto_1

    .line 2598068
    :cond_7
    if-ne v0, p1, :cond_4

    .line 2598069
    iget-object v0, v1, LX/Idr;->c:LX/Idk;

    .line 2598070
    invoke-virtual {v1}, LX/Idr;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/Ids;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;)LX/Idk;
    .locals 4

    .prologue
    .line 2598008
    iget-object v2, p0, LX/Ids;->b:[LX/Idf;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2598009
    invoke-interface {v0, p1}, LX/Idf;->a(Landroid/net/Uri;)LX/Idk;

    move-result-object v0

    .line 2598010
    if-eqz v0, :cond_0

    .line 2598011
    :goto_1
    return-object v0

    .line 2598012
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2598013
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 2598014
    const/4 v0, 0x0

    iget-object v1, p0, LX/Ids;->b:[LX/Idf;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2598015
    iget-object v2, p0, LX/Ids;->b:[LX/Idf;

    aget-object v2, v2, v0

    invoke-interface {v2}, LX/Idf;->a()V

    .line 2598016
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2598017
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2598018
    iget-object v2, p0, LX/Ids;->b:[LX/Idf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2598019
    invoke-interface {v4}, LX/Idf;->b()I

    move-result v4

    add-int/2addr v1, v4

    .line 2598020
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2598021
    :cond_0
    return v1
.end method
