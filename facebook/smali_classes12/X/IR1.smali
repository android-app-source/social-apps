.class public final LX/IR1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IR2;


# direct methods
.method public constructor <init>(LX/IR2;)V
    .locals 0

    .prologue
    .line 2575985
    iput-object p1, p0, LX/IR1;->a:LX/IR2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2575992
    iget-object v0, p0, LX/IR1;->a:LX/IR2;

    iget-object v0, v0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/IR1;->a:LX/IR2;

    iget-object v2, v2, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081bc1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2575993
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575986
    iget-object v0, p0, LX/IR1;->a:LX/IR2;

    iget-object v0, v0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    .line 2575987
    const/4 p1, 0x0

    iput-object p1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    .line 2575988
    const/4 p1, 0x0

    iput-boolean p1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->j:Z

    .line 2575989
    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->b(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    .line 2575990
    iget-object v0, p0, LX/IR1;->a:LX/IR2;

    iget-object v0, v0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->l(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    .line 2575991
    return-void
.end method
