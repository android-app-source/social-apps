.class public LX/HwY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2521067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2521068
    return-void
.end method

.method public static a(ILandroid/content/Intent;LX/0Px;)LX/HwX;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/HwX;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2521037
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    move-object v0, v4

    .line 2521038
    :goto_0
    return-object v0

    .line 2521039
    :cond_0
    const-string v0, "extra_photo_items_list"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 2521040
    const-string v0, "extras_taggable_gallery_creative_editing_data_list"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 2521041
    if-eqz v7, :cond_1

    .line 2521042
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2521043
    :cond_1
    const-string v0, "extra_are_media_items_modified"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2521044
    if-nez v0, :cond_3

    move-object v0, v4

    .line 2521045
    goto :goto_0

    :cond_2
    move v0, v3

    .line 2521046
    goto :goto_1

    .line 2521047
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2521048
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v9

    move v5, v3

    :goto_2
    if-ge v5, v9, :cond_8

    invoke-virtual {p2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2521049
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2521050
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v10

    move v2, v3

    .line 2521051
    :goto_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_a

    .line 2521052
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/facebook/ipc/media/MediaIdKey;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2521053
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 2521054
    if-eqz v7, :cond_9

    .line 2521055
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 2521056
    :goto_4
    if-eqz v1, :cond_7

    .line 2521057
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v1

    .line 2521058
    if-eqz v2, :cond_4

    .line 2521059
    iput-object v2, v1, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 2521060
    :cond_4
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2521061
    iput-object v0, v1, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2521062
    invoke-virtual {v1}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2521063
    :cond_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    .line 2521064
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2521065
    :cond_7
    invoke-static {v0}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v1

    goto :goto_5

    .line 2521066
    :cond_8
    new-instance v0, LX/HwX;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/HwX;-><init>(LX/0Px;LX/0Px;)V

    goto/16 :goto_0

    :cond_9
    move-object v2, v4

    goto :goto_4

    :cond_a
    move-object v2, v4

    move-object v1, v4

    goto :goto_4
.end method
