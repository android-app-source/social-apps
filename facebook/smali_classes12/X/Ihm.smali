.class public final LX/Ihm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;)V
    .locals 0

    .prologue
    .line 2602973
    iput-object p1, p0, LX/Ihm;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x1b47558

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2602974
    iget-object v2, p0, LX/Ihm;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-object v2, v2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->k:LX/IhC;

    if-eqz v2, :cond_2

    .line 2602975
    iget-object v2, p0, LX/Ihm;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-object v2, v2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2602976
    iget-object v2, p0, LX/Ihm;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-boolean v2, v2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->i:Z

    if-nez v2, :cond_0

    .line 2602977
    iget-object v2, p0, LX/Ihm;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-object v3, p0, LX/Ihm;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-boolean v3, v3, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->j:Z

    if-nez v3, :cond_3

    .line 2602978
    :goto_0
    if-eqz v0, :cond_4

    .line 2602979
    iget-object v3, v2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 2602980
    :goto_1
    iput-boolean v0, v2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->j:Z

    .line 2602981
    :cond_0
    iget-object v0, p0, LX/Ihm;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->k:LX/IhC;

    iget-object v2, p0, LX/Ihm;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-object v2, v2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602982
    iget-object v3, v0, LX/IhC;->a:LX/IhD;

    iget-object v3, v3, LX/IhD;->r:LX/IhV;

    if-eqz v3, :cond_1

    .line 2602983
    iget-object v3, v0, LX/IhC;->a:LX/IhD;

    iget-object v3, v3, LX/IhD;->r:LX/IhV;

    const/16 p1, 0x1e

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2602984
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-ne v6, p1, :cond_6

    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v6, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 2602985
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->e:LX/0Px;

    invoke-virtual {v6, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 2602986
    new-instance v7, LX/IhA;

    invoke-direct {v7}, LX/IhA;-><init>()V

    .line 2602987
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-static {p0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p0

    iput-object p0, v7, LX/IhA;->c:LX/0am;

    .line 2602988
    move-object v7, v7

    .line 2602989
    invoke-virtual {v7}, LX/IhA;->a()LX/IhB;

    move-result-object v7

    .line 2602990
    iget-object p0, v3, LX/IhV;->a:LX/IhW;

    invoke-virtual {p0, v6, v7}, LX/1OM;->a(ILjava/lang/Object;)V

    .line 2602991
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0823da

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v4, v5

    invoke-virtual {v6, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2602992
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->a:Landroid/content/Context;

    invoke-static {v6, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 2602993
    :cond_1
    :goto_2
    iget-object v4, v0, LX/IhC;->a:LX/IhD;

    iget-object v3, v0, LX/IhC;->a:LX/IhD;

    iget-boolean v3, v3, LX/IhD;->q:Z

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_3
    invoke-virtual {v4, v3}, LX/IhD;->b(Z)V

    .line 2602994
    :cond_2
    const v0, -0x4b833408

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2602995
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2602996
    :cond_4
    iget-object v3, v2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->reverse()V

    goto/16 :goto_1

    .line 2602997
    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    .line 2602998
    :cond_6
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->g:LX/Ihj;

    iget-boolean v6, v6, LX/Ihj;->d:Z

    if-eqz v6, :cond_7

    .line 2602999
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v6, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2603000
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v6, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2603001
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_a

    move v6, v4

    .line 2603002
    :goto_4
    if-eqz v6, :cond_7

    .line 2603003
    new-instance v6, LX/IhA;

    invoke-direct {v6}, LX/IhA;-><init>()V

    iget-object v7, v3, LX/IhV;->a:LX/IhW;

    iget-object v7, v7, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_9

    .line 2603004
    :goto_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v7}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v7

    iput-object v7, v6, LX/IhA;->b:LX/0am;

    .line 2603005
    move-object v4, v6

    .line 2603006
    invoke-virtual {v4}, LX/IhA;->a()LX/IhB;

    move-result-object v4

    .line 2603007
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v7, v3, LX/IhV;->a:LX/IhW;

    invoke-virtual {v7}, LX/1OM;->ij_()I

    move-result v7

    invoke-virtual {v6, v5, v7, v4}, LX/1OM;->a(IILjava/lang/Object;)V

    .line 2603008
    :cond_7
    iget-object v4, v3, LX/IhV;->a:LX/IhW;

    iget-object v4, v4, LX/IhW;->d:LX/Ihc;

    .line 2603009
    iget-object v5, v4, LX/Ihc;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object v5, v5, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->e:LX/IhK;

    invoke-interface {v5, v2}, LX/IhK;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2603010
    goto :goto_2

    .line 2603011
    :cond_8
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v6, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2603012
    iget-object v6, v3, LX/IhV;->a:LX/IhW;

    iget-object v6, v6, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-ne v6, v4, :cond_a

    move v6, v4

    .line 2603013
    goto :goto_4

    :cond_9
    move v4, v5

    .line 2603014
    goto :goto_5

    :cond_a
    move v6, v5

    goto :goto_4
.end method
