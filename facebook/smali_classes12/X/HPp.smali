.class public LX/HPp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/HPp;


# instance fields
.field public final a:LX/CfW;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/9XD;

.field private final d:LX/8Do;


# direct methods
.method public constructor <init>(LX/CfW;LX/0Ot;LX/9XD;LX/8Do;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CfW;",
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;",
            "LX/9XD;",
            "LX/8Do;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2462536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2462537
    iput-object p1, p0, LX/HPp;->a:LX/CfW;

    .line 2462538
    iput-object p2, p0, LX/HPp;->b:LX/0Ot;

    .line 2462539
    iput-object p3, p0, LX/HPp;->c:LX/9XD;

    .line 2462540
    iput-object p4, p0, LX/HPp;->d:LX/8Do;

    .line 2462541
    return-void
.end method

.method public static a(LX/0QB;)LX/HPp;
    .locals 7

    .prologue
    .line 2462542
    sget-object v0, LX/HPp;->e:LX/HPp;

    if-nez v0, :cond_1

    .line 2462543
    const-class v1, LX/HPp;

    monitor-enter v1

    .line 2462544
    :try_start_0
    sget-object v0, LX/HPp;->e:LX/HPp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2462545
    if-eqz v2, :cond_0

    .line 2462546
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2462547
    new-instance v6, LX/HPp;

    invoke-static {v0}, LX/CfW;->b(LX/0QB;)LX/CfW;

    move-result-object v3

    check-cast v3, LX/CfW;

    const/16 v4, 0x1061

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/9XD;->a(LX/0QB;)LX/9XD;

    move-result-object v4

    check-cast v4, LX/9XD;

    invoke-static {v0}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v5

    check-cast v5, LX/8Do;

    invoke-direct {v6, v3, p0, v4, v5}, LX/HPp;-><init>(LX/CfW;LX/0Ot;LX/9XD;LX/8Do;)V

    .line 2462548
    move-object v0, v6

    .line 2462549
    sput-object v0, LX/HPp;->e:LX/HPp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2462550
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2462551
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2462552
    :cond_1
    sget-object v0, LX/HPp;->e:LX/HPp;

    return-object v0

    .line 2462553
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2462554
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/HPp;Ljava/lang/Long;Ljava/lang/String;)Lcom/facebook/reaction/ReactionQueryParams;
    .locals 5

    .prologue
    .line 2462555
    new-instance v0, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v0}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    .line 2462556
    iput-object p1, v0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 2462557
    move-object v0, v0

    .line 2462558
    iput-object p1, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2462559
    move-object v0, v0

    .line 2462560
    iget-object v1, p0, LX/HPp;->d:LX/8Do;

    invoke-virtual {v1}, LX/8Do;->j()I

    move-result v1

    int-to-long v2, v1

    .line 2462561
    iput-wide v2, v0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2462562
    move-object v0, v0

    .line 2462563
    sget-object v1, LX/0wD;->TIMELINE:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v1

    .line 2462564
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    .line 2462565
    move-object v0, v0

    .line 2462566
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2462567
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2462568
    move-object v0, v0

    .line 2462569
    const-string v1, "ANDROID_PAGE_HOME"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2462570
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    .line 2462571
    move-object v0, v0

    .line 2462572
    const-string v1, "page_reaction_early_fetch"

    .line 2462573
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    .line 2462574
    move-object v0, v0

    .line 2462575
    const-string v1, "InitialLoad"

    .line 2462576
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    .line 2462577
    move-object v0, v0

    .line 2462578
    const-string v1, "page_reaction_initial_fetch_tag"

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    .line 2462579
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    .line 2462580
    move-object v0, v0

    .line 2462581
    iput-object p2, v0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    .line 2462582
    move-object v0, v0

    .line 2462583
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Long;LX/0v6;ILX/0Ve;Ljava/util/concurrent/ExecutorService;Z)LX/2jY;
    .locals 8
    .param p4    # LX/0Ve;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2462584
    iget-object v0, p0, LX/HPp;->c:LX/9XD;

    invoke-virtual {v0}, LX/9XD;->a()LX/89z;

    move-result-object v0

    invoke-virtual {v0}, LX/89z;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2462585
    invoke-static {p0, p1, v0}, LX/HPp;->a(LX/HPp;Ljava/lang/Long;Ljava/lang/String;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v4

    .line 2462586
    iget-object v0, p0, LX/HPp;->a:LX/CfW;

    const-string v3, "ANDROID_PAGE_HOME"

    move-object v1, p2

    move v2, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, LX/CfW;->a(LX/0v6;ILjava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;LX/0Ve;Ljava/util/concurrent/ExecutorService;Z)LX/2jY;

    move-result-object v0

    return-object v0
.end method
