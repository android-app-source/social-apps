.class public LX/Hvj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsKeyboardUp;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j9;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        "DerivedData::",
        "LX/5RE;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:LX/1c9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1c9",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;"
        }
    .end annotation
.end field

.field private B:LX/Hvi;

.field private C:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public D:Z

.field public E:Z

.field private F:Z

.field public G:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Z

.field public final b:LX/Hvb;

.field public final c:Landroid/view/View;

.field private final d:LX/Be2;

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final f:LX/87Y;

.field public final g:LX/IF5;

.field public final h:LX/IF6;

.field public final i:LX/0tO;

.field private final j:LX/AQ0;

.field public final k:LX/CEp;

.field public final l:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final m:LX/0hB;

.field public final n:LX/0Uh;

.field public o:LX/HvV;

.field public p:LX/Hva;

.field public q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

.field public r:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/fbui/glyph/GlyphButton;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Hvp;",
            ">;"
        }
    .end annotation
.end field

.field public t:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public v:I

.field public w:Z

.field private x:Z

.field public y:Z

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2519766
    const-class v0, LX/Hvj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hvj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/Be2;LX/0il;LX/Hvb;LX/87Y;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/IF5;LX/0tO;LX/CEp;Ljava/util/concurrent/ExecutorService;LX/0hB;LX/0Uh;LX/IF6;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Be2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/Be2;",
            "TServices;",
            "LX/Hvb;",
            "LX/87Y;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/IF5;",
            "LX/0tO;",
            "LX/CEp;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0hB;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/IF6;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2519713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2519714
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hvj;->w:Z

    .line 2519715
    const v0, 0x7fffffff

    iput v0, p0, LX/Hvj;->z:I

    .line 2519716
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->A:LX/1c9;

    .line 2519717
    sget-object v0, LX/Hvi;->NEED_TO_CHECK:LX/Hvi;

    iput-object v0, p0, LX/Hvj;->B:LX/Hvi;

    .line 2519718
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Hvj;->F:Z

    .line 2519719
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    .line 2519720
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    .line 2519721
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Be2;

    iput-object v0, p0, LX/Hvj;->d:LX/Be2;

    .line 2519722
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    .line 2519723
    iput-object p4, p0, LX/Hvj;->b:LX/Hvb;

    .line 2519724
    iput-object p5, p0, LX/Hvj;->f:LX/87Y;

    .line 2519725
    iput-object p7, p0, LX/Hvj;->g:LX/IF5;

    .line 2519726
    new-instance v0, LX/AQ0;

    invoke-direct {v0}, LX/AQ0;-><init>()V

    iput-object v0, p0, LX/Hvj;->j:LX/AQ0;

    .line 2519727
    iput-object p6, p0, LX/Hvj;->C:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2519728
    iput-object p8, p0, LX/Hvj;->i:LX/0tO;

    .line 2519729
    iput-object p9, p0, LX/Hvj;->k:LX/CEp;

    .line 2519730
    iput-object p10, p0, LX/Hvj;->l:Ljava/util/concurrent/ExecutorService;

    .line 2519731
    iput-object p11, p0, LX/Hvj;->m:LX/0hB;

    .line 2519732
    iput-object p12, p0, LX/Hvj;->n:LX/0Uh;

    .line 2519733
    iput-object p13, p0, LX/Hvj;->h:LX/IF6;

    .line 2519734
    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 2519735
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->i()I

    move-result v0

    iput v0, p0, LX/Hvj;->v:I

    .line 2519736
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    const v1, 0x7fffffff

    .line 2519737
    iget-object p3, v0, LX/0tO;->a:LX/0ad;

    sget-short p4, LX/0wk;->y:S

    const/4 p5, 0x0

    invoke-interface {p3, p4, p5}, LX/0ad;->a(SZ)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 2519738
    iget-object p3, v0, LX/0tO;->a:LX/0ad;

    sget p4, LX/0wk;->C:I

    invoke-interface {p3, p4, v1}, LX/0ad;->a(II)I

    move-result v1

    .line 2519739
    :cond_0
    move v0, v1

    .line 2519740
    iput v0, p0, LX/Hvj;->z:I

    .line 2519741
    iget-object v0, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2519742
    sget-object p3, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {p3, v1}, LX/2rw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, LX/2rw;->USER:LX/2rw;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j6;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v1, v0}, LX/2rw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, p2

    :goto_0
    iput-boolean v0, p0, LX/Hvj;->y:Z

    .line 2519743
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    if-ne v0, p2, :cond_4

    .line 2519744
    iget-object v0, p0, LX/Hvj;->f:LX/87Y;

    .line 2519745
    iget-object v1, v0, LX/87Y;->d:LX/0Px;

    move-object v0, v1

    .line 2519746
    invoke-static {v0}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->A:LX/1c9;

    .line 2519747
    iget-object v0, p0, LX/Hvj;->b:LX/Hvb;

    iget-object v1, p0, LX/Hvj;->A:LX/1c9;

    invoke-virtual {v0, v1}, LX/Hvb;->a(LX/1c9;)LX/Hva;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->p:LX/Hva;

    .line 2519748
    iget-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    const v1, 0x7f0d2a2b

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    iput-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    .line 2519749
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->r:LX/0am;

    .line 2519750
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    iget-object v1, p0, LX/Hvj;->p:LX/Hva;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2519751
    iget-object v0, p0, LX/Hvj;->o:LX/HvV;

    if-eqz v0, :cond_2

    .line 2519752
    iget-object v0, p0, LX/Hvj;->p:LX/Hva;

    iget-object v1, p0, LX/Hvj;->o:LX/HvV;

    .line 2519753
    iput-object v1, v0, LX/Hva;->f:LX/HvV;

    .line 2519754
    invoke-static {p0}, LX/Hvj;->h(LX/Hvj;)V

    .line 2519755
    invoke-static {p0}, LX/Hvj;->t(LX/Hvj;)V

    .line 2519756
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hvj;->D:Z

    .line 2519757
    :goto_1
    return-void

    :cond_3
    move v0, p1

    .line 2519758
    goto :goto_0

    .line 2519759
    :cond_4
    iget-object v0, p0, LX/Hvj;->n:LX/0Uh;

    const/16 v1, 0x2ee

    invoke-virtual {v0, v1, p1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2519760
    iget-object v0, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2519761
    iget-object v1, p0, LX/Hvj;->h:LX/IF6;

    .line 2519762
    iget-object p1, v1, LX/IF6;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p2, 0xe000c

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result p3

    invoke-interface {p1, p2, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2519763
    iget-object v1, p0, LX/Hvj;->k:LX/CEp;

    iget-object p1, p0, LX/Hvj;->m:LX/0hB;

    invoke-virtual {p1}, LX/0hB;->b()F

    move-result p1

    invoke-virtual {v1, p1}, LX/CEp;->a(F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance p1, LX/Hvc;

    invoke-direct {p1, p0, v0}, LX/Hvc;-><init>(LX/Hvj;Ljava/lang/String;)V

    iget-object v0, p0, LX/Hvj;->l:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, p1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2519764
    goto :goto_1

    .line 2519765
    :cond_5
    invoke-static {p0}, LX/Hvj;->e(LX/Hvj;)V

    goto :goto_1
.end method

.method public static a(LX/Hvj;I)F
    .locals 5

    .prologue
    .line 2519707
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2519708
    const v1, 0x7f0b0ce6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    .line 2519709
    const v2, 0x7f0b0ce4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 2519710
    const v3, 0x7f0b0cdd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 2519711
    const v4, 0x7f0b0cde

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 2519712
    add-float/2addr v0, v3

    int-to-float v3, p1

    add-float/2addr v1, v2

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    return v0
.end method

.method private a(LX/IF4;)V
    .locals 3

    .prologue
    .line 2519698
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v1, p0, LX/Hvj;->f:LX/87Y;

    .line 2519699
    iget-object v2, v1, LX/87Y;->d:LX/0Px;

    move-object v1, v2

    .line 2519700
    invoke-static {v1}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v1

    invoke-static {v0, v1}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/1c9;)I

    move-result v0

    .line 2519701
    :goto_0
    iget-object v1, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 2519702
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2519703
    iget-object v1, p0, LX/Hvj;->p:LX/Hva;

    invoke-virtual {v1, v0, p1}, LX/Hva;->a(ILX/IF4;)V

    .line 2519704
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Hvj;->w:Z

    .line 2519705
    return-void

    .line 2519706
    :cond_1
    sget-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v1, p0, LX/Hvj;->A:LX/1c9;

    invoke-static {v0, v1}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/1c9;)I

    move-result v0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2519689
    iget-object v0, p0, LX/Hvj;->t:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2519690
    :goto_0
    return-void

    .line 2519691
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, LX/Hvj;->E:Z

    if-eqz v0, :cond_1

    .line 2519692
    iget-object v0, p0, LX/Hvj;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2519693
    const/4 v0, -0x1

    iget-object v1, p0, LX/Hvj;->p:LX/Hva;

    .line 2519694
    iget p1, v1, LX/Hva;->d:I

    move v1, p1

    .line 2519695
    invoke-static {p0, v0, v1, v2}, LX/Hvj;->a$redex0(LX/Hvj;IIZ)V

    goto :goto_0

    .line 2519696
    :cond_1
    iget-object v0, p0, LX/Hvj;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 2519697
    iget-object v0, p0, LX/Hvj;->t:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2519685
    iget-object v0, p0, LX/Hvj;->j:LX/AQ0;

    .line 2519686
    sget-object p0, LX/0Q7;->a:LX/0Px;

    move-object p0, p0

    .line 2519687
    invoke-static {v0, p1, p0}, LX/AQ0;->b(LX/AQ0;Ljava/lang/CharSequence;LX/0Px;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2519688
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/Hvj;IIZ)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2519661
    iget-object v0, p0, LX/Hvj;->t:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Hvj;->E:Z

    if-nez v0, :cond_1

    .line 2519662
    :cond_0
    :goto_0
    return-void

    .line 2519663
    :cond_1
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2519664
    const v1, 0x7f0b0cdd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 2519665
    iget-object v2, p0, LX/Hvj;->t:Landroid/view/View;

    if-nez v2, :cond_4

    .line 2519666
    const/high16 v2, -0x40800000    # -1.0f

    .line 2519667
    :cond_2
    :goto_1
    move v2, v2

    .line 2519668
    add-float/2addr v1, v2

    .line 2519669
    invoke-static {p0, p1}, LX/Hvj;->a(LX/Hvj;I)F

    move-result v2

    sub-float/2addr v2, v1

    .line 2519670
    invoke-static {p0, p2}, LX/Hvj;->a(LX/Hvj;I)F

    move-result v3

    sub-float v1, v3, v1

    .line 2519671
    const v3, 0x7f0b0ce4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const v4, 0x7f0b0ce3

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sub-float v0, v3, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    .line 2519672
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    add-float/2addr v2, v0

    add-float/2addr v0, v1

    invoke-direct {v3, v2, v0, v5, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2519673
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2519674
    iget v0, p0, LX/Hvj;->v:I

    int-to-long v0, v0

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2519675
    if-eqz p3, :cond_3

    .line 2519676
    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    invoke-virtual {v3, v0}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2519677
    :cond_3
    iget-object v0, p0, LX/Hvj;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 2519678
    :cond_4
    iget-object v2, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v2}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2519679
    const v3, 0x7f0b0ce4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 2519680
    const v4, 0x7f0b0ce6

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v4, v2

    .line 2519681
    iget-object v2, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->computeHorizontalScrollOffset()I

    move-result v2

    int-to-float v6, v2

    .line 2519682
    iget-object v2, p0, LX/Hvj;->t:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v2

    add-float/2addr v2, v6

    .line 2519683
    cmpl-float v3, v6, v3

    if-lez v3, :cond_2

    .line 2519684
    sub-float/2addr v2, v4

    goto :goto_1
.end method

.method public static e(LX/Hvj;)V
    .locals 2

    .prologue
    .line 2519656
    iget-object v0, p0, LX/Hvj;->f:LX/87Y;

    .line 2519657
    iget-object v1, v0, LX/87Y;->d:LX/0Px;

    move-object v0, v1

    .line 2519658
    invoke-static {v0}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->A:LX/1c9;

    .line 2519659
    invoke-static {p0}, LX/Hvj;->f$redex0(LX/Hvj;)V

    .line 2519660
    return-void
.end method

.method public static f$redex0(LX/Hvj;)V
    .locals 6

    .prologue
    .line 2519436
    iget-object v0, p0, LX/Hvj;->b:LX/Hvb;

    iget-object v1, p0, LX/Hvj;->A:LX/1c9;

    invoke-virtual {v0, v1}, LX/Hvb;->a(LX/1c9;)LX/Hva;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->p:LX/Hva;

    .line 2519437
    iget-object v0, p0, LX/Hvj;->o:LX/HvV;

    if-eqz v0, :cond_0

    .line 2519438
    iget-object v0, p0, LX/Hvj;->p:LX/Hva;

    iget-object v1, p0, LX/Hvj;->o:LX/HvV;

    .line 2519439
    iput-object v1, v0, LX/Hva;->f:LX/HvV;

    .line 2519440
    :cond_0
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2519441
    iget-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    const v1, 0x7f0d2a2e

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    iput-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    .line 2519442
    iget-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    const v1, 0x7f0d2a2d

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->u:LX/0am;

    .line 2519443
    iput-boolean v4, p0, LX/Hvj;->E:Z

    .line 2519444
    iget-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    const v1, 0x7f0d2a30

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->r:LX/0am;

    .line 2519445
    iget-object v0, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/Hvd;

    invoke-direct {v1, p0}, LX/Hvd;-><init>(LX/Hvj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2519446
    new-instance v1, LX/Hvp;

    const/4 v0, 0x2

    new-array v2, v0, [Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f021662

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aput-object v0, v2, v4

    iget-object v0, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f021661

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-direct {v1, v2}, LX/Hvp;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->s:LX/0am;

    .line 2519447
    iget-object v0, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v1, p0, LX/Hvj;->s:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2519448
    iget-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    const v1, 0x7f0d2a2f

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Hvj;->t:Landroid/view/View;

    .line 2519449
    iget-object v0, p0, LX/Hvj;->t:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2519450
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    new-instance v1, LX/Hve;

    invoke-direct {v1, p0}, LX/Hve;-><init>(LX/Hvj;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2519451
    :cond_1
    iget-object v0, p0, LX/Hvj;->p:LX/Hva;

    new-instance v1, LX/Hvf;

    invoke-direct {v1, p0}, LX/Hvf;-><init>(LX/Hvj;)V

    .line 2519452
    iput-object v1, v0, LX/Hva;->e:LX/Hvf;

    .line 2519453
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    iget-object v1, p0, LX/Hvj;->p:LX/Hva;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2519454
    iput-boolean v5, p0, LX/Hvj;->D:Z

    .line 2519455
    invoke-static {p0}, LX/Hvj;->t(LX/Hvj;)V

    .line 2519456
    invoke-static {p0}, LX/Hvj;->h(LX/Hvj;)V

    .line 2519457
    return-void
.end method

.method public static h(LX/Hvj;)V
    .locals 11

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2519627
    iget-object v0, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2519628
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    .line 2519629
    if-eqz v1, :cond_2

    instance-of v2, v1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-eqz v2, :cond_2

    move v2, v3

    .line 2519630
    :goto_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j9;

    invoke-interface {v0}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    .line 2519631
    if-eqz v0, :cond_3

    instance-of v5, v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-eqz v5, :cond_3

    move v5, v3

    .line 2519632
    :goto_1
    iget-object v6, p0, LX/Hvj;->i:LX/0tO;

    const/4 v8, 0x0

    .line 2519633
    iget-object v9, v6, LX/0tO;->a:LX/0ad;

    sget-short v10, LX/0wk;->y:S

    invoke-interface {v9, v10, v8}, LX/0ad;->a(SZ)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 2519634
    iget-object v9, v6, LX/0tO;->a:LX/0ad;

    sget-short v10, LX/0wk;->F:S

    invoke-interface {v9, v10, v8}, LX/0ad;->a(SZ)Z

    move-result v8

    .line 2519635
    :cond_0
    :goto_2
    move v6, v8

    .line 2519636
    if-eqz v2, :cond_5

    .line 2519637
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    if-ne v0, v3, :cond_4

    move-object v0, v1

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v1, p0, LX/Hvj;->f:LX/87Y;

    .line 2519638
    iget-object v2, v1, LX/87Y;->d:LX/0Px;

    move-object v1, v2

    .line 2519639
    invoke-static {v1}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v1

    invoke-static {v0, v1}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/1c9;)I

    move-result v0

    :goto_3
    move v1, v0

    .line 2519640
    :goto_4
    if-eq v1, v7, :cond_1

    .line 2519641
    iget-object v2, p0, LX/Hvj;->p:LX/Hva;

    if-eqz v5, :cond_8

    sget-object v0, LX/IF4;->DRAFT_RECOVERY:LX/IF4;

    :goto_5
    invoke-virtual {v2, v1, v0}, LX/Hva;->a(ILX/IF4;)V

    .line 2519642
    iput-boolean v4, p0, LX/Hvj;->w:Z

    .line 2519643
    :cond_1
    return-void

    :cond_2
    move v2, v4

    .line 2519644
    goto :goto_0

    :cond_3
    move v5, v4

    .line 2519645
    goto :goto_1

    .line 2519646
    :cond_4
    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v0, p0, LX/Hvj;->A:LX/1c9;

    invoke-static {v1, v0}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/1c9;)I

    move-result v0

    goto :goto_3

    .line 2519647
    :cond_5
    if-eqz v5, :cond_7

    .line 2519648
    iget-object v1, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v1}, LX/0tO;->l()I

    move-result v1

    if-ne v1, v3, :cond_6

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v1, p0, LX/Hvj;->f:LX/87Y;

    .line 2519649
    iget-object v2, v1, LX/87Y;->d:LX/0Px;

    move-object v1, v2

    .line 2519650
    invoke-static {v1}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v1

    invoke-static {v0, v1}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/1c9;)I

    move-result v0

    :goto_6
    move v1, v0

    goto :goto_4

    :cond_6
    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v1, p0, LX/Hvj;->A:LX/1c9;

    invoke-static {v0, v1}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/1c9;)I

    move-result v0

    goto :goto_6

    .line 2519651
    :cond_7
    if-eqz v6, :cond_1

    .line 2519652
    iget-object v0, p0, LX/Hvj;->C:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/composer/textstyle/ComposerRichTextController;->a:LX/0Tn;

    invoke-interface {v0, v1, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    move v1, v0

    goto :goto_4

    .line 2519653
    :cond_8
    sget-object v0, LX/IF4;->STICKY_STYLES:LX/IF4;

    goto :goto_5

    .line 2519654
    :cond_9
    iget-object v9, v6, LX/0tO;->a:LX/0ad;

    sget-short v10, LX/0wk;->q:S

    invoke-interface {v9, v10, v8}, LX/0ad;->a(SZ)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2519655
    iget-object v9, v6, LX/0tO;->a:LX/0ad;

    sget-short v10, LX/0wk;->t:S

    invoke-interface {v9, v10, v8}, LX/0ad;->a(SZ)Z

    move-result v8

    goto :goto_2
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2519594
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->d()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2519595
    iget-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2519596
    iget-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, LX/Hvj;->v:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2519597
    :goto_0
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2519598
    iget-boolean v0, p0, LX/Hvj;->w:Z

    if-nez v0, :cond_3

    .line 2519599
    :cond_0
    :goto_1
    return-void

    .line 2519600
    :cond_1
    iget-object v0, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2519601
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    const/4 v3, 0x0

    .line 2519602
    iget-object v1, v0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->y:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2519603
    iget-object v1, v0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->x:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2519604
    :goto_2
    move v0, v1

    .line 2519605
    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/Hvj;->H:Z

    if-nez v0, :cond_2

    .line 2519606
    invoke-static {p0}, LX/Hvj;->l(LX/Hvj;)V

    .line 2519607
    :cond_2
    iget-object v0, p0, LX/Hvj;->d:LX/Be2;

    iget-object v1, p0, LX/Hvj;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0ce7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-interface {v0, v1}, LX/Be2;->setBottomMargin(I)V

    goto :goto_0

    .line 2519608
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2519609
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    if-ne v0, v4, :cond_5

    move v0, v1

    .line 2519610
    :goto_3
    iget-object v2, p0, LX/Hvj;->f:LX/87Y;

    invoke-virtual {v2}, LX/87Y;->b()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 2519611
    iget-object v2, p0, LX/Hvj;->f:LX/87Y;

    invoke-virtual {v2, v0}, LX/87Y;->a(I)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getCanDefault()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2519612
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2519613
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move v2, v1

    .line 2519614
    :goto_4
    iget-object v0, p0, LX/Hvj;->A:LX/1c9;

    invoke-virtual {v0}, LX/1c9;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 2519615
    iget-object v0, p0, LX/Hvj;->A:LX/1c9;

    invoke-virtual {v0, v2}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getCanDefault()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2519616
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2519617
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2519618
    :cond_7
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2519619
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 2519620
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2519621
    :goto_5
    iget-object v2, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 2519622
    iget-object v2, p0, LX/Hvj;->p:LX/Hva;

    sget-object v3, LX/IF4;->AUTO_DEFAULT:LX/IF4;

    invoke-virtual {v2, v0, v3}, LX/Hva;->a(ILX/IF4;)V

    .line 2519623
    iput-boolean v1, p0, LX/Hvj;->w:Z

    goto/16 :goto_1

    .line 2519624
    :cond_8
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 2519625
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 2519626
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_5

    :cond_9
    iget-object v1, v0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->q:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    goto/16 :goto_2
.end method

.method private k()V
    .locals 4

    .prologue
    .line 2519588
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->d()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2519589
    iget-object v0, p0, LX/Hvj;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, LX/Hvj;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, LX/Hvj;->v:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2519590
    :goto_0
    return-void

    .line 2519591
    :cond_0
    invoke-static {p0}, LX/Hvj;->n(LX/Hvj;)V

    .line 2519592
    iget-object v0, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2519593
    iget-object v0, p0, LX/Hvj;->d:LX/Be2;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/Be2;->setBottomMargin(I)V

    goto :goto_0
.end method

.method public static l(LX/Hvj;)V
    .locals 11

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2519563
    iget-object v0, p0, LX/Hvj;->u:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2519564
    iget-boolean v1, p0, LX/Hvj;->E:Z

    if-nez v1, :cond_0

    .line 2519565
    iput-boolean v2, p0, LX/Hvj;->E:Z

    .line 2519566
    iget-boolean v1, p0, LX/Hvj;->F:Z

    if-nez v1, :cond_1

    .line 2519567
    iput-boolean v2, p0, LX/Hvj;->F:Z

    .line 2519568
    iget-object v1, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2519569
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2519570
    new-instance v0, Lcom/facebook/composer/textstyle/RichTextStylePickerController$5;

    invoke-direct {v0, p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerController$5;-><init>(LX/Hvj;)V

    iput-object v0, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    .line 2519571
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    const v2, 0x3f44dec2

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2519572
    :cond_0
    :goto_0
    return-void

    .line 2519573
    :cond_1
    iget-object v1, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2519574
    invoke-direct {p0}, LX/Hvj;->p()V

    .line 2519575
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2519576
    new-instance v0, Lcom/facebook/composer/textstyle/RichTextStylePickerController$6;

    invoke-direct {v0, p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerController$6;-><init>(LX/Hvj;)V

    iput-object v0, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    .line 2519577
    invoke-direct {p0, v2}, LX/Hvj;->a(Z)V

    .line 2519578
    iget-object v5, p0, LX/Hvj;->u:LX/0am;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 2519579
    const/4 v6, 0x0

    move v7, v6

    :goto_1
    iget-object v6, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v6}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getChildCount()I

    move-result v6

    if-ge v7, v6, :cond_2

    .line 2519580
    iget-object v6, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v6, v7}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 2519581
    invoke-virtual {v6}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v6, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    iget v8, p0, LX/Hvj;->v:I

    int-to-long v9, v8

    invoke-virtual {v6, v9, v10}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    new-instance v8, LX/Hvg;

    invoke-direct {v8, p0, v5}, LX/Hvg;-><init>(LX/Hvj;Landroid/view/View;)V

    invoke-virtual {v6, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2519582
    iget-object v8, p0, LX/Hvj;->g:LX/IF5;

    iget-object v6, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0il;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v6, LX/0j0;

    invoke-interface {v6}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v6

    .line 2519583
    iget-object v9, v8, LX/IF5;->a:LX/0Zb;

    sget-object v10, LX/IF3;->PICKER_EXPAND:LX/IF3;

    invoke-virtual {v10}, LX/IF3;->name()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, LX/IF5;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    invoke-interface {v9, v10}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2519584
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 2519585
    :cond_2
    iget-object v0, p0, LX/Hvj;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hvp;

    iget v1, p0, LX/Hvj;->v:I

    .line 2519586
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/Hvp;->a(LX/Hvp;II)V

    .line 2519587
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    iget v2, p0, LX/Hvj;->v:I

    int-to-long v2, v2

    const v4, 0x482539ce

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto/16 :goto_0
.end method

.method public static n(LX/Hvj;)V
    .locals 9

    .prologue
    .line 2519548
    iget-object v0, p0, LX/Hvj;->u:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2519549
    iget-boolean v1, p0, LX/Hvj;->E:Z

    if-eqz v1, :cond_1

    .line 2519550
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Hvj;->E:Z

    .line 2519551
    invoke-direct {p0}, LX/Hvj;->p()V

    .line 2519552
    new-instance v1, Lcom/facebook/composer/textstyle/RichTextStylePickerController$8;

    invoke-direct {v1, p0, v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerController$8;-><init>(LX/Hvj;Landroid/view/View;)V

    iput-object v1, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    .line 2519553
    const/4 v5, 0x0

    :goto_0
    iget-object v6, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v6}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getChildCount()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 2519554
    iget-object v6, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v6, v5}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 2519555
    invoke-virtual {v6}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-static {p0, v5}, LX/Hvj;->a(LX/Hvj;I)F

    move-result v7

    neg-float v7, v7

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    iget v7, p0, LX/Hvj;->v:I

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2519556
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2519557
    :cond_0
    iget-object v0, p0, LX/Hvj;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hvp;

    iget v1, p0, LX/Hvj;->v:I

    .line 2519558
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/Hvp;->a(LX/Hvp;II)V

    .line 2519559
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    iget v2, p0, LX/Hvj;->v:I

    int-to-long v2, v2

    const v4, -0x28e51be2

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2519560
    iget-object v1, p0, LX/Hvj;->g:LX/IF5;

    iget-object v0, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2519561
    iget-object v2, v1, LX/IF5;->a:LX/0Zb;

    sget-object v3, LX/IF3;->PICKER_COLLAPSE:LX/IF3;

    invoke-virtual {v3}, LX/IF3;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/IF5;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2519562
    :cond_1
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2519538
    invoke-direct {p0, v0}, LX/Hvj;->a(Z)V

    .line 2519539
    iget-object v1, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    .line 2519540
    :goto_0
    return-void

    .line 2519541
    :cond_0
    :goto_1
    iget-object v1, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v1}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2519542
    iget-object v1, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2519543
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 2519544
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2519545
    :cond_1
    iget-object v0, p0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 2519546
    iget-object v0, p0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2519547
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hvj;->G:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public static t(LX/Hvj;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 2519486
    iget-boolean v0, p0, LX/Hvj;->D:Z

    if-nez v0, :cond_0

    .line 2519487
    :goto_0
    return-void

    .line 2519488
    :cond_0
    iget-object v0, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->h()I

    move-result v4

    .line 2519489
    iget-object v0, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2519490
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j2;

    invoke-interface {v1}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    move-object v1, v0

    .line 2519491
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5RE;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v6

    .line 2519492
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->k()Z

    move-result v7

    .line 2519493
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isInlineSproutsOpen()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v3

    .line 2519494
    :goto_1
    invoke-static {v8}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    if-gt v1, v4, :cond_6

    move v1, v3

    .line 2519495
    :goto_2
    iget-object v4, p0, LX/Hvj;->i:LX/0tO;

    invoke-virtual {v4}, LX/0tO;->l()I

    move-result v4

    if-ne v4, v3, :cond_8

    iget-object v4, p0, LX/Hvj;->f:LX/87Y;

    invoke-virtual {v4}, LX/87Y;->b()I

    move-result v4

    if-lez v4, :cond_7

    move v4, v3

    .line 2519496
    :goto_3
    const-string v5, "\r\n|\r|\n"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 2519497
    const/4 v5, 0x0

    .line 2519498
    :goto_4
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 2519499
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 2519500
    :cond_2
    move v5, v5

    .line 2519501
    iget v9, p0, LX/Hvj;->z:I

    if-gt v5, v9, :cond_a

    move v5, v3

    .line 2519502
    :goto_5
    sget-object v9, LX/Hvh;->b:[I

    iget-object v10, p0, LX/Hvj;->B:LX/Hvi;

    invoke-virtual {v10}, LX/Hvi;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 2519503
    sget-object v9, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    if-ne v6, v9, :cond_b

    if-nez v7, :cond_b

    const/4 v7, 0x1

    .line 2519504
    iget-object v6, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0il;

    .line 2519505
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v6, LX/0ip;

    invoke-interface {v6}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v6

    .line 2519506
    if-eqz v6, :cond_3

    iget-object v9, v6, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-nez v9, :cond_13

    :cond_3
    move v6, v7

    .line 2519507
    :goto_6
    move v6, v6

    .line 2519508
    if-eqz v6, :cond_b

    invoke-direct {p0, v8}, LX/Hvj;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_b

    move v7, v3

    .line 2519509
    :goto_7
    if-eqz v7, :cond_c

    sget-object v6, LX/Hvi;->ELIGIBLE:LX/Hvi;

    :goto_8
    iput-object v6, p0, LX/Hvj;->B:LX/Hvi;

    move v6, v7

    .line 2519510
    :goto_9
    if-eqz v0, :cond_d

    if-eqz v1, :cond_d

    if-eqz v6, :cond_d

    iget-boolean v0, p0, LX/Hvj;->y:Z

    if-eqz v0, :cond_d

    if-eqz v4, :cond_d

    if-eqz v5, :cond_d

    .line 2519511
    iget-boolean v0, p0, LX/Hvj;->x:Z

    if-eqz v0, :cond_4

    .line 2519512
    iget-object v0, p0, LX/Hvj;->p:LX/Hva;

    .line 2519513
    iget v1, v0, LX/Hva;->h:I

    if-nez v1, :cond_16

    .line 2519514
    :cond_4
    :goto_a
    iput-boolean v2, p0, LX/Hvj;->x:Z

    .line 2519515
    invoke-direct {p0}, LX/Hvj;->j()V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 2519516
    goto/16 :goto_1

    :cond_6
    move v1, v2

    .line 2519517
    goto/16 :goto_2

    :cond_7
    move v4, v2

    .line 2519518
    goto/16 :goto_3

    :cond_8
    iget-object v4, p0, LX/Hvj;->A:LX/1c9;

    invoke-virtual {v4}, LX/1c9;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    move v4, v3

    goto/16 :goto_3

    :cond_9
    move v4, v2

    goto/16 :goto_3

    :cond_a
    move v5, v2

    .line 2519519
    goto :goto_5

    :pswitch_0
    move v6, v3

    .line 2519520
    goto :goto_9

    :pswitch_1
    move v6, v2

    .line 2519521
    goto :goto_9

    :cond_b
    move v7, v2

    .line 2519522
    goto :goto_7

    .line 2519523
    :cond_c
    sget-object v6, LX/Hvi;->NOT_ELIGIBLE:LX/Hvi;

    goto :goto_8

    .line 2519524
    :cond_d
    if-eqz v1, :cond_e

    if-eqz v5, :cond_e

    if-eqz v6, :cond_e

    .line 2519525
    invoke-direct {p0}, LX/Hvj;->k()V

    goto/16 :goto_0

    .line 2519526
    :cond_e
    iput-boolean v3, p0, LX/Hvj;->x:Z

    .line 2519527
    invoke-direct {p0}, LX/Hvj;->k()V

    .line 2519528
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2519529
    sget-object v0, LX/IF4;->TEXT_DELETED:LX/IF4;

    invoke-direct {p0, v0}, LX/Hvj;->a(LX/IF4;)V

    goto/16 :goto_0

    .line 2519530
    :cond_f
    if-eqz v6, :cond_10

    if-nez v1, :cond_10

    .line 2519531
    sget-object v0, LX/IF4;->TEXT_TOO_LONG:LX/IF4;

    invoke-direct {p0, v0}, LX/Hvj;->a(LX/IF4;)V

    goto/16 :goto_0

    .line 2519532
    :cond_10
    if-nez v6, :cond_11

    .line 2519533
    sget-object v0, LX/IF4;->ATTACHMENT_ADD:LX/IF4;

    invoke-direct {p0, v0}, LX/Hvj;->a(LX/IF4;)V

    goto/16 :goto_0

    .line 2519534
    :cond_11
    if-nez v5, :cond_12

    .line 2519535
    sget-object v0, LX/IF4;->NEWLINE_COUNT:LX/IF4;

    invoke-direct {p0, v0}, LX/Hvj;->a(LX/IF4;)V

    goto/16 :goto_0

    .line 2519536
    :cond_12
    sget-object v0, LX/IF4;->OTHER:LX/IF4;

    invoke-direct {p0, v0}, LX/Hvj;->a(LX/IF4;)V

    goto/16 :goto_0

    :cond_13
    iget-object v9, v6, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v9}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->o()Z

    move-result v9

    if-eqz v9, :cond_14

    iget-boolean v6, v6, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    if-eqz v6, :cond_15

    :cond_14
    move v6, v7

    goto/16 :goto_6

    :cond_15
    const/4 v6, 0x0

    goto/16 :goto_6

    .line 2519537
    :cond_16
    iget v1, v0, LX/Hva;->h:I

    sget-object v3, LX/IF4;->PREVIOUSLY_SELECTED:LX/IF4;

    invoke-virtual {v0, v1, v3}, LX/Hva;->a(ILX/IF4;)V

    goto/16 :goto_a

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 6

    .prologue
    .line 2519461
    iget-boolean v0, p0, LX/Hvj;->D:Z

    if-nez v0, :cond_1

    .line 2519462
    :cond_0
    :goto_0
    return-void

    .line 2519463
    :cond_1
    iget-object v0, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2519464
    sget-object v1, LX/Hvh;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2519465
    :pswitch_0
    invoke-static {p0}, LX/Hvj;->t(LX/Hvj;)V

    goto :goto_0

    .line 2519466
    :pswitch_1
    iget-object v1, p0, LX/Hvj;->p:LX/Hva;

    .line 2519467
    iget v2, v1, LX/Hva;->d:I

    move v1, v2

    .line 2519468
    if-eqz v1, :cond_0

    .line 2519469
    iget-object v2, p0, LX/Hvj;->g:LX/IF5;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getName()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v4, p0, LX/Hvj;->p:LX/Hva;

    .line 2519470
    iget p0, v4, LX/Hva;->g:I

    move v4, p0

    .line 2519471
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j2;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    .line 2519472
    iget-object v5, v2, LX/IF5;->a:LX/0Zb;

    sget-object p0, LX/IF3;->POST:LX/IF3;

    invoke-virtual {p0}, LX/IF3;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, LX/IF5;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "name"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "select_count"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "text_length"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v5, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2519473
    goto/16 :goto_0

    :cond_2
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2519474
    :pswitch_2
    iget-object v1, p0, LX/Hvj;->p:LX/Hva;

    .line 2519475
    iget v2, v1, LX/Hva;->d:I

    move v1, v2

    .line 2519476
    if-eqz v1, :cond_0

    .line 2519477
    iget-object v2, p0, LX/Hvj;->g:LX/IF5;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getName()Ljava/lang/String;

    move-result-object v1

    :goto_2
    iget-object v4, p0, LX/Hvj;->p:LX/Hva;

    .line 2519478
    iget v5, v4, LX/Hva;->g:I

    move v4, v5

    .line 2519479
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    invoke-virtual {v0}, LX/5RF;->name()Ljava/lang/String;

    move-result-object v0

    .line 2519480
    iget-object v5, v2, LX/IF5;->a:LX/0Zb;

    sget-object p0, LX/IF3;->CANCEL:LX/IF3;

    invoke-virtual {p0}, LX/IF3;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, LX/IF5;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "name"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "select_count"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "content_type"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v5, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2519481
    goto/16 :goto_0

    :cond_3
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2519482
    :pswitch_3
    invoke-direct {p0}, LX/Hvj;->p()V

    .line 2519483
    iget-object v1, p0, LX/Hvj;->h:LX/IF6;

    iget-object v0, p0, LX/Hvj;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2519484
    iget-object v2, v1, LX/IF6;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0xe000c

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 2519485
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2519458
    sget-object v0, LX/Hvi;->NEED_TO_CHECK:LX/Hvi;

    iput-object v0, p0, LX/Hvj;->B:LX/Hvi;

    .line 2519459
    invoke-static {p0}, LX/Hvj;->t(LX/Hvj;)V

    .line 2519460
    return-void
.end method
