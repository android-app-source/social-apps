.class public LX/JUx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/2g9;

.field private final b:LX/5up;


# direct methods
.method public constructor <init>(LX/2g9;LX/5up;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2699950
    iput-object p1, p0, LX/JUx;->a:LX/2g9;

    .line 2699951
    iput-object p2, p0, LX/JUx;->b:LX/5up;

    .line 2699952
    return-void
.end method

.method public static a(LX/0QB;)LX/JUx;
    .locals 5

    .prologue
    .line 2699953
    const-class v1, LX/JUx;

    monitor-enter v1

    .line 2699954
    :try_start_0
    sget-object v0, LX/JUx;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699955
    sput-object v2, LX/JUx;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699956
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699957
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699958
    new-instance p0, LX/JUx;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    invoke-static {v0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v4

    check-cast v4, LX/5up;

    invoke-direct {p0, v3, v4}, LX/JUx;-><init>(LX/2g9;LX/5up;)V

    .line 2699959
    move-object v0, p0

    .line 2699960
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699961
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699962
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699963
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public onSaveClick(Landroid/view/View;LX/1De;ZLcom/facebook/graphql/model/GraphQLNode;LX/JVP;)V
    .locals 6
    .param p4    # Lcom/facebook/graphql/model/GraphQLNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/JVP;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2699964
    if-eqz p3, :cond_1

    sget-object v1, LX/5uo;->UNSAVE:LX/5uo;

    .line 2699965
    :goto_0
    iget-object v0, p0, LX/JUx;->b:LX/5up;

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    const-string v3, "native_netego"

    const-string v4, "toggle_button"

    new-instance v5, LX/JUw;

    invoke-direct {v5, p0}, LX/JUw;-><init>(LX/JUx;)V

    invoke-virtual/range {v0 .. v5}, LX/5up;->a(LX/5uo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2699966
    if-nez p3, :cond_2

    const/4 v0, 0x1

    .line 2699967
    :goto_1
    iget-object v2, p2, LX/1De;->g:LX/1X1;

    move-object v2, v2

    .line 2699968
    if-nez v2, :cond_3

    .line 2699969
    :goto_2
    if-eqz p5, :cond_0

    .line 2699970
    sget-object v0, LX/5uo;->SAVE:LX/5uo;

    if-ne v1, v0, :cond_4

    const/4 v0, 0x1

    move v2, v0

    .line 2699971
    :goto_3
    iget-object v4, p5, LX/JVP;->d:Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    iget-object v0, p5, LX/JVP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2699972
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2699973
    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    iget-object v5, p5, LX/JVP;->b:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    if-eqz v2, :cond_5

    const-string v3, "pdfy_save"

    :goto_4
    invoke-static {v4, v0, v5, v3}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->a$redex0(Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;Ljava/lang/String;)V

    .line 2699974
    iget-object v0, p5, LX/JVP;->d:Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    iget-object v3, v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->g:LX/1g6;

    if-eqz v2, :cond_6

    sget-object v0, LX/7iQ;->PDFY_PRODUCT_SAVE:LX/7iQ;

    :goto_5
    iget-object v2, p5, LX/JVP;->c:Lcom/facebook/graphql/model/GraphQLProductItem;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, LX/1g6;->a(LX/7iQ;Ljava/lang/String;)V

    .line 2699975
    :cond_0
    return-void

    .line 2699976
    :cond_1
    sget-object v1, LX/5uo;->SAVE:LX/5uo;

    goto :goto_0

    .line 2699977
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2699978
    :cond_3
    check-cast v2, LX/JUt;

    .line 2699979
    new-instance v3, LX/JUu;

    iget-object v4, v2, LX/JUt;->d:LX/JUv;

    invoke-direct {v3, v4, v0}, LX/JUu;-><init>(LX/JUv;Z)V

    move-object v2, v3

    .line 2699980
    invoke-virtual {p2, v2}, LX/1De;->a(LX/48B;)V

    goto :goto_2

    .line 2699981
    :cond_4
    const/4 v0, 0x0

    move v2, v0

    goto :goto_3

    .line 2699982
    :cond_5
    const-string v3, "pdfy_unsave"

    goto :goto_4

    .line 2699983
    :cond_6
    sget-object v0, LX/7iQ;->PDFY_PRODUCT_UNSAVE:LX/7iQ;

    goto :goto_5
.end method
