.class public LX/JVr;
.super LX/5Jq;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/5Jq;"
    }
.end annotation


# instance fields
.field private final b:LX/1De;

.field private final c:LX/JVl;

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JVl;LX/1De;IFLX/0Px;LX/1Po;)V
    .locals 2
    .param p2    # LX/1De;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1Po;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JVl;",
            "LX/1De;",
            "IF",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;TE;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701715
    invoke-direct {p0, p2, p3, p4}, LX/5Jq;-><init>(Landroid/content/Context;IF)V

    .line 2701716
    iput-object p1, p0, LX/JVr;->c:LX/JVl;

    .line 2701717
    iput-object p2, p0, LX/JVr;->b:LX/1De;

    .line 2701718
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/JVr;->d:LX/0Px;

    .line 2701719
    iget-object v0, p0, LX/JVr;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Binder not rendering an empty list of attachments"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2701720
    iput-object p6, p0, LX/JVr;->e:LX/1Po;

    .line 2701721
    return-void

    .line 2701722
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2701723
    iget-object v0, p0, LX/JVr;->c:LX/JVl;

    iget-object v1, p0, LX/JVr;->b:LX/1De;

    const/4 v2, 0x0

    .line 2701724
    new-instance v3, LX/JVk;

    invoke-direct {v3, v0}, LX/JVk;-><init>(LX/JVl;)V

    .line 2701725
    iget-object p1, v0, LX/JVl;->b:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JVj;

    .line 2701726
    if-nez p1, :cond_0

    .line 2701727
    new-instance p1, LX/JVj;

    invoke-direct {p1, v0}, LX/JVj;-><init>(LX/JVl;)V

    .line 2701728
    :cond_0
    invoke-static {p1, v1, v2, v2, v3}, LX/JVj;->a$redex0(LX/JVj;LX/1De;IILX/JVk;)V

    .line 2701729
    move-object v3, p1

    .line 2701730
    move-object v2, v3

    .line 2701731
    move-object v1, v2

    .line 2701732
    iget-object v0, p0, LX/JVr;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701733
    iget-object v2, v1, LX/JVj;->a:LX/JVk;

    iput-object v0, v2, LX/JVk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701734
    iget-object v2, v1, LX/JVj;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2701735
    move-object v0, v1

    .line 2701736
    iget-object v1, p0, LX/JVr;->e:LX/1Po;

    .line 2701737
    iget-object v2, v0, LX/JVj;->a:LX/JVk;

    iput-object v1, v2, LX/JVk;->b:LX/1Po;

    .line 2701738
    iget-object v2, v0, LX/JVj;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2701739
    move-object v0, v0

    .line 2701740
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2701741
    iget-object v0, p0, LX/JVr;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2701742
    const/4 v0, 0x0

    return v0
.end method
