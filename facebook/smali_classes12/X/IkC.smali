.class public LX/IkC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/J1d;

.field private final b:LX/5fv;

.field private final c:LX/IkB;

.field private final d:[I


# direct methods
.method public constructor <init>(LX/J1d;LX/5fv;LX/IkB;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606479
    iput-object p1, p0, LX/IkC;->a:LX/J1d;

    .line 2606480
    iput-object p2, p0, LX/IkC;->b:LX/5fv;

    .line 2606481
    iput-object p3, p0, LX/IkC;->c:LX/IkB;

    .line 2606482
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/IkC;->d:[I

    .line 2606483
    return-void
.end method

.method public static a(LX/0QB;)LX/IkC;
    .locals 6

    .prologue
    .line 2606484
    const-class v1, LX/IkC;

    monitor-enter v1

    .line 2606485
    :try_start_0
    sget-object v0, LX/IkC;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606486
    sput-object v2, LX/IkC;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606487
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606488
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606489
    new-instance p0, LX/IkC;

    invoke-static {v0}, LX/J1d;->b(LX/0QB;)LX/J1d;

    move-result-object v3

    check-cast v3, LX/J1d;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v4

    check-cast v4, LX/5fv;

    invoke-static {v0}, LX/IkB;->a(LX/0QB;)LX/IkB;

    move-result-object v5

    check-cast v5, LX/IkB;

    invoke-direct {p0, v3, v4, v5}, LX/IkC;-><init>(LX/J1d;LX/5fv;LX/IkB;)V

    .line 2606490
    move-object v0, p0

    .line 2606491
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606492
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606493
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
