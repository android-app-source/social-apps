.class public final LX/Hmg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:F

.field public final c:F


# direct methods
.method public constructor <init>(IFF)V
    .locals 0

    .prologue
    .line 2500978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2500979
    iput p1, p0, LX/Hmg;->a:I

    .line 2500980
    iput p2, p0, LX/Hmg;->b:F

    .line 2500981
    iput p3, p0, LX/Hmg;->c:F

    .line 2500982
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2500983
    instance-of v0, p1, LX/Hmg;

    if-eqz v0, :cond_0

    iget v1, p0, LX/Hmg;->a:I

    move-object v0, p1

    check-cast v0, LX/Hmg;

    iget v0, v0, LX/Hmg;->a:I

    if-ne v1, v0, :cond_0

    iget v1, p0, LX/Hmg;->b:F

    move-object v0, p1

    check-cast v0, LX/Hmg;

    iget v0, v0, LX/Hmg;->b:F

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    iget v0, p0, LX/Hmg;->c:F

    check-cast p1, LX/Hmg;

    iget v1, p1, LX/Hmg;->c:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2500984
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
