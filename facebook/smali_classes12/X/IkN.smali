.class public LX/IkN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/Duh;

.field private final c:LX/IkA;

.field private final d:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/Duh;LX/IkA;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606657
    iput-object p1, p0, LX/IkN;->a:Landroid/content/res/Resources;

    .line 2606658
    iput-object p2, p0, LX/IkN;->b:LX/Duh;

    .line 2606659
    iput-object p3, p0, LX/IkN;->c:LX/IkA;

    .line 2606660
    iput-object p4, p0, LX/IkN;->d:LX/0SG;

    .line 2606661
    return-void
.end method

.method public static a(LX/0QB;)LX/IkN;
    .locals 7

    .prologue
    .line 2606662
    const-class v1, LX/IkN;

    monitor-enter v1

    .line 2606663
    :try_start_0
    sget-object v0, LX/IkN;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606664
    sput-object v2, LX/IkN;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606665
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606666
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606667
    new-instance p0, LX/IkN;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/Duh;->a(LX/0QB;)LX/Duh;

    move-result-object v4

    check-cast v4, LX/Duh;

    invoke-static {v0}, LX/IkA;->b(LX/0QB;)LX/IkA;

    move-result-object v5

    check-cast v5, LX/IkA;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {p0, v3, v4, v5, v6}, LX/IkN;-><init>(Landroid/content/res/Resources;LX/Duh;LX/IkA;LX/0SG;)V

    .line 2606668
    move-object v0, p0

    .line 2606669
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606670
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606671
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606672
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
