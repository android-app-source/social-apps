.class public LX/ICD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/2g9;

.field public final b:LX/17W;

.field public final c:LX/0Zb;


# direct methods
.method public constructor <init>(LX/2g9;LX/17W;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2549048
    iput-object p1, p0, LX/ICD;->a:LX/2g9;

    .line 2549049
    iput-object p2, p0, LX/ICD;->b:LX/17W;

    .line 2549050
    iput-object p3, p0, LX/ICD;->c:LX/0Zb;

    .line 2549051
    return-void
.end method

.method public static a(LX/0QB;)LX/ICD;
    .locals 6

    .prologue
    .line 2549052
    const-class v1, LX/ICD;

    monitor-enter v1

    .line 2549053
    :try_start_0
    sget-object v0, LX/ICD;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2549054
    sput-object v2, LX/ICD;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2549055
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2549056
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2549057
    new-instance p0, LX/ICD;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {p0, v3, v4, v5}, LX/ICD;-><init>(LX/2g9;LX/17W;LX/0Zb;)V

    .line 2549058
    move-object v0, p0

    .line 2549059
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2549060
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ICD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2549061
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2549062
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
