.class public final LX/Hsh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hsi;


# direct methods
.method public constructor <init>(LX/Hsi;)V
    .locals 0

    .prologue
    .line 2514802
    iput-object p1, p0, LX/Hsh;->a:LX/Hsi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2514803
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    .line 2514804
    if-eqz p1, :cond_1

    .line 2514805
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/LinksPreview;

    .line 2514806
    :goto_0
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2514807
    :goto_1
    return-object v0

    .line 2514808
    :cond_0
    iget-object v2, v0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    .line 2514809
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/share/model/LinksPreview;->a()Lcom/facebook/share/model/LinksPreview$Media;

    move-result-object v3

    .line 2514810
    iget-object v4, v0, Lcom/facebook/share/model/LinksPreview;->misinformationData:Lcom/facebook/share/model/LinksPreview$MisinformationData;

    .line 2514811
    if-nez v4, :cond_5

    .line 2514812
    const/4 v5, 0x0

    .line 2514813
    :goto_3
    move-object v4, v5

    .line 2514814
    new-instance v5, LX/39x;

    invoke-direct {v5}, LX/39x;-><init>()V

    .line 2514815
    iput-object v2, v5, LX/39x;->t:Ljava/lang/String;

    .line 2514816
    move-object v2, v5

    .line 2514817
    iget-object v5, v0, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    .line 2514818
    iput-object v5, v2, LX/39x;->r:Ljava/lang/String;

    .line 2514819
    move-object v2, v2

    .line 2514820
    iget-object v5, v0, Lcom/facebook/share/model/LinksPreview;->caption:Ljava/lang/String;

    invoke-static {v5}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 2514821
    iput-object v5, v2, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2514822
    move-object v5, v2

    .line 2514823
    if-nez v3, :cond_3

    const/4 v2, 0x0

    .line 2514824
    :goto_4
    iput-object v2, v5, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2514825
    move-object v3, v5

    .line 2514826
    if-eqz v4, :cond_4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2514827
    :goto_5
    iput-object v2, v3, LX/39x;->b:LX/0Px;

    .line 2514828
    move-object v2, v3

    .line 2514829
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    move-object v0, v2

    .line 2514830
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 2514831
    :cond_2
    iget-object v2, v0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    goto :goto_2

    .line 2514832
    :cond_3
    new-instance v2, LX/4XB;

    invoke-direct {v2}, LX/4XB;-><init>()V

    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    iget-object v7, v3, Lcom/facebook/share/model/LinksPreview$Media;->src:Ljava/lang/String;

    .line 2514833
    iput-object v7, v6, LX/2dc;->h:Ljava/lang/String;

    .line 2514834
    move-object v6, v6

    .line 2514835
    iget v7, v3, Lcom/facebook/share/model/LinksPreview$Media;->width:I

    .line 2514836
    iput v7, v6, LX/2dc;->i:I

    .line 2514837
    move-object v6, v6

    .line 2514838
    iget v3, v3, Lcom/facebook/share/model/LinksPreview$Media;->height:I

    .line 2514839
    iput v3, v6, LX/2dc;->c:I

    .line 2514840
    move-object v3, v6

    .line 2514841
    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 2514842
    iput-object v3, v2, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2514843
    move-object v2, v2

    .line 2514844
    invoke-virtual {v2}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    goto :goto_4

    .line 2514845
    :cond_4
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2514846
    goto :goto_5

    :cond_5
    new-instance v5, LX/4Ys;

    invoke-direct {v5}, LX/4Ys;-><init>()V

    new-instance v6, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v7, -0x67292209

    invoke-direct {v6, v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2514847
    iput-object v6, v5, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2514848
    move-object v5, v5

    .line 2514849
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->title:Ljava/lang/String;

    .line 2514850
    iput-object v6, v5, LX/4Ys;->bz:Ljava/lang/String;

    .line 2514851
    move-object v5, v5

    .line 2514852
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->url:Ljava/lang/String;

    .line 2514853
    iput-object v6, v5, LX/4Ys;->bD:Ljava/lang/String;

    .line 2514854
    move-object v5, v5

    .line 2514855
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->linkType:Ljava/lang/String;

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v6

    .line 2514856
    iput-object v6, v5, LX/4Ys;->as:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2514857
    move-object v5, v5

    .line 2514858
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->subtitle:Ljava/lang/String;

    .line 2514859
    iput-object v6, v5, LX/4Ys;->bu:Ljava/lang/String;

    .line 2514860
    move-object v5, v5

    .line 2514861
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->ctaText:Ljava/lang/String;

    .line 2514862
    iput-object v6, v5, LX/4Ys;->w:Ljava/lang/String;

    .line 2514863
    move-object v5, v5

    .line 2514864
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->alertDescription:Ljava/lang/String;

    .line 2514865
    iput-object v6, v5, LX/4Ys;->h:Ljava/lang/String;

    .line 2514866
    move-object v5, v5

    .line 2514867
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->reshareAlertTitle:Ljava/lang/String;

    .line 2514868
    iput-object v6, v5, LX/4Ys;->ba:Ljava/lang/String;

    .line 2514869
    move-object v5, v5

    .line 2514870
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->disputeText:Ljava/lang/String;

    .line 2514871
    iput-object v6, v5, LX/4Ys;->F:Ljava/lang/String;

    .line 2514872
    move-object v5, v5

    .line 2514873
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->disputeFormUri:Ljava/lang/String;

    .line 2514874
    iput-object v6, v5, LX/4Ys;->E:Ljava/lang/String;

    .line 2514875
    move-object v5, v5

    .line 2514876
    iget-object v6, v4, Lcom/facebook/share/model/LinksPreview$MisinformationData;->actions:LX/0Px;

    .line 2514877
    if-nez v6, :cond_6

    .line 2514878
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 2514879
    :goto_6
    move-object v6, v7

    .line 2514880
    iput-object v6, v5, LX/4Ys;->c:LX/0Px;

    .line 2514881
    move-object v5, v5

    .line 2514882
    invoke-virtual {v5}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    goto/16 :goto_3

    .line 2514883
    :cond_6
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p0

    .line 2514884
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    const/4 v7, 0x0

    move v8, v7

    :goto_7
    if-ge v8, p1, :cond_7

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/share/model/LinksPreview$MisinformationAction;

    .line 2514885
    new-instance v1, LX/4XJ;

    invoke-direct {v1}, LX/4XJ;-><init>()V

    iget-object v4, v7, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->actionType:Ljava/lang/String;

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    move-result-object v4

    .line 2514886
    iput-object v4, v1, LX/4XJ;->b:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    .line 2514887
    move-object v1, v1

    .line 2514888
    iget-object v4, v7, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->uri:Ljava/lang/String;

    .line 2514889
    iput-object v4, v1, LX/4XJ;->e:Ljava/lang/String;

    .line 2514890
    move-object v1, v1

    .line 2514891
    iget-object v4, v7, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->title:Ljava/lang/String;

    .line 2514892
    iput-object v4, v1, LX/4XJ;->d:Ljava/lang/String;

    .line 2514893
    move-object v1, v1

    .line 2514894
    iget-object v7, v7, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->subtitle:Ljava/lang/String;

    .line 2514895
    iput-object v7, v1, LX/4XJ;->c:Ljava/lang/String;

    .line 2514896
    move-object v7, v1

    .line 2514897
    new-instance v1, Lcom/facebook/graphql/model/GraphQLMisinformationAction;

    invoke-direct {v1, v7}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;-><init>(LX/4XJ;)V

    .line 2514898
    move-object v7, v1

    .line 2514899
    invoke-virtual {p0, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2514900
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_7

    .line 2514901
    :cond_7
    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    goto :goto_6
.end method
