.class public final LX/Hbn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

.field private b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

.field private c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;


# direct methods
.method public constructor <init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;)V
    .locals 0

    .prologue
    .line 2486214
    iput-object p1, p0, LX/Hbn;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2486215
    iput-object p2, p0, LX/Hbn;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486216
    iput-object p3, p0, LX/Hbn;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486217
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2486218
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2486219
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2486220
    iget-object v0, p0, LX/Hbn;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    invoke-static {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->n(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2486221
    iget-object v0, p0, LX/Hbn;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    iget-object v0, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->j:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b()V

    .line 2486222
    :goto_0
    iget-object v0, p0, LX/Hbn;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v3

    .line 2486223
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2486224
    :goto_1
    return-void

    .line 2486225
    :cond_0
    iget-object v0, p0, LX/Hbn;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    iget-object v0, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->j:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->c()V

    goto :goto_0

    .line 2486226
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2486227
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    .line 2486228
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v6, 0x6

    if-lt v0, v6, :cond_2

    move v0, v1

    .line 2486229
    :goto_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v3, v4, :cond_3

    move v3, v1

    .line 2486230
    :goto_3
    if-eqz v5, :cond_5

    .line 2486231
    iget-object v4, p0, LX/Hbn;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486232
    iput-boolean v1, v4, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->b:Z

    .line 2486233
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 2486234
    iget-object v0, p0, LX/Hbn;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iget-object v2, p0, LX/Hbn;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486235
    iget-object v3, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f:LX/HaS;

    move-object v2, v3

    .line 2486236
    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordStrength(LX/HaS;)V

    .line 2486237
    iget-object v0, p0, LX/Hbn;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0, v1}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordsMatch(Z)V

    .line 2486238
    iget-object v0, p0, LX/Hbn;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->c()V

    .line 2486239
    iget-object v0, p0, LX/Hbn;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0, v1}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordsMatch(Z)V

    goto :goto_1

    :cond_2
    move v0, v2

    .line 2486240
    goto :goto_2

    :cond_3
    move v3, v2

    .line 2486241
    goto :goto_3

    .line 2486242
    :cond_4
    iget-object v0, p0, LX/Hbn;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    sget-object v1, LX/HaS;->NULL:LX/HaS;

    invoke-virtual {v0, v1}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordStrength(LX/HaS;)V

    .line 2486243
    iget-object v0, p0, LX/Hbn;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordsMatch(Z)V

    .line 2486244
    iget-object v0, p0, LX/Hbn;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordsMatch(Z)V

    goto :goto_1

    .line 2486245
    :cond_5
    iget-object v0, p0, LX/Hbn;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    iget-boolean v0, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->b:Z

    if-eqz v0, :cond_6

    .line 2486246
    iget-object v0, p0, LX/Hbn;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b()V

    .line 2486247
    iget-object v0, p0, LX/Hbn;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordsMatch(Z)V

    .line 2486248
    :cond_6
    iget-object v0, p0, LX/Hbn;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486249
    iput-boolean v2, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->b:Z

    .line 2486250
    iget-object v0, p0, LX/Hbn;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b()V

    goto :goto_1
.end method
