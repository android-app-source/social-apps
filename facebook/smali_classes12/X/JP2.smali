.class public LX/JP2;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JP3;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JP2",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JP3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2688667
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2688668
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JP2;->b:LX/0Zi;

    .line 2688669
    iput-object p1, p0, LX/JP2;->a:LX/0Ot;

    .line 2688670
    return-void
.end method

.method public static a(LX/0QB;)LX/JP2;
    .locals 4

    .prologue
    .line 2688671
    const-class v1, LX/JP2;

    monitor-enter v1

    .line 2688672
    :try_start_0
    sget-object v0, LX/JP2;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688673
    sput-object v2, LX/JP2;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688674
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688675
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688676
    new-instance v3, LX/JP2;

    const/16 p0, 0x1fd4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JP2;-><init>(LX/0Ot;)V

    .line 2688677
    move-object v0, v3

    .line 2688678
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688679
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JP2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688680
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688681
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2688682
    check-cast p2, LX/JP1;

    .line 2688683
    iget-object v0, p0, LX/JP2;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JP3;

    iget-object v1, p2, LX/JP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JP1;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p2, LX/JP1;->c:LX/1Pn;

    .line 2688684
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b257f

    invoke-interface {v4, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b2580

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3d

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    iget-object v4, v0, LX/JP3;->a:LX/JOy;

    const/4 v6, 0x0

    .line 2688685
    new-instance p0, LX/JOx;

    invoke-direct {p0, v4}, LX/JOx;-><init>(LX/JOy;)V

    .line 2688686
    iget-object p2, v4, LX/JOy;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JOw;

    .line 2688687
    if-nez p2, :cond_0

    .line 2688688
    new-instance p2, LX/JOw;

    invoke-direct {p2, v4}, LX/JOw;-><init>(LX/JOy;)V

    .line 2688689
    :cond_0
    invoke-static {p2, p1, v6, v6, p0}, LX/JOw;->a$redex0(LX/JOw;LX/1De;IILX/JOx;)V

    .line 2688690
    move-object p0, p2

    .line 2688691
    move-object v6, p0

    .line 2688692
    move-object v4, v6

    .line 2688693
    iget-object v6, v4, LX/JOw;->a:LX/JOx;

    iput-object v1, v6, LX/JOx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2688694
    iget-object v6, v4, LX/JOw;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2688695
    move-object v4, v4

    .line 2688696
    iget-object v6, v4, LX/JOw;->a:LX/JOx;

    iput-object v2, v6, LX/JOx;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688697
    iget-object v6, v4, LX/JOw;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2688698
    move-object v6, v4

    .line 2688699
    move-object v4, v3

    check-cast v4, LX/1Po;

    .line 2688700
    iget-object p0, v6, LX/JOw;->a:LX/JOx;

    iput-object v4, p0, LX/JOx;->c:LX/1Po;

    .line 2688701
    iget-object p0, v6, LX/JOw;->e:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2688702
    move-object v4, v6

    .line 2688703
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JP3;->b:LX/JOt;

    invoke-virtual {v5, p1}, LX/JOt;->c(LX/1De;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/JOs;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/JOs;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/JOs;->a(LX/1Pn;)LX/JOs;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b2581

    invoke-interface {v4, v5, v6}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2688704
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2688705
    invoke-static {}, LX/1dS;->b()V

    .line 2688706
    const/4 v0, 0x0

    return-object v0
.end method
