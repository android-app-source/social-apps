.class public LX/IVG;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pf;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IVG",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581611
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2581612
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/IVG;->b:LX/0Zi;

    .line 2581613
    iput-object p1, p0, LX/IVG;->a:LX/0Ot;

    .line 2581614
    return-void
.end method

.method public static a(LX/0QB;)LX/IVG;
    .locals 4

    .prologue
    .line 2581615
    const-class v1, LX/IVG;

    monitor-enter v1

    .line 2581616
    :try_start_0
    sget-object v0, LX/IVG;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581617
    sput-object v2, LX/IVG;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581618
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581619
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581620
    new-instance v3, LX/IVG;

    const/16 p0, 0x2425

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IVG;-><init>(LX/0Ot;)V

    .line 2581621
    move-object v0, v3

    .line 2581622
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581623
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581624
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2581626
    check-cast p2, LX/IVF;

    .line 2581627
    iget-object v0, p0, LX/IVG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;

    iget-object v1, p2, LX/IVF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/IVF;->b:LX/1Pp;

    .line 2581628
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581629
    if-eqz v3, :cond_0

    .line 2581630
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581631
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2581632
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581633
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2581634
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581635
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2581636
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581637
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2581638
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581639
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2581640
    :cond_0
    const/4 v3, 0x0

    .line 2581641
    :goto_0
    move-object v0, v3

    .line 2581642
    return-object v0

    .line 2581643
    :cond_1
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581644
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2581645
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581646
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 2581647
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x1

    const/16 p0, 0xc

    invoke-interface {v5, v6, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v5

    .line 2581648
    iget-object v6, v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->b:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v6

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object p0

    invoke-virtual {v6, p0}, LX/1nw;->a(LX/4Ab;)LX/1nw;

    move-result-object v6

    sget-object p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    .line 2581649
    const/4 p0, 0x2

    invoke-interface {v6, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v6

    const/4 p0, 0x5

    const p2, 0x7f0b123c

    invoke-interface {v6, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const p0, 0x7f0b123b

    invoke-interface {v6, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const p0, 0x7f0b123b

    invoke-interface {v6, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    move-object v4, v6

    .line 2581650
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x0

    .line 2581651
    const v5, 0x7f0e0120

    invoke-static {p1, p0, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    sget-object v6, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v5, v6}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    move-object v3, v5

    .line 2581652
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object v3, v2

    .line 2581653
    check-cast v3, LX/1Po;

    invoke-interface {v3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    sget-object v5, LX/1Qt;->LEARNING_UNIT:LX/1Qt;

    invoke-virtual {v3, v5}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2581654
    check-cast v2, LX/1Pk;

    invoke-interface {v2}, LX/1Pk;->e()LX/1SX;

    move-result-object v3

    .line 2581655
    iget-object v5, v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->d:LX/1VE;

    invoke-virtual {v5, v1, v3}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;)LX/1dl;

    move-result-object v5

    .line 2581656
    iget-object v6, v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->c:LX/1vb;

    invoke-virtual {v6, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v3

    move-object v3, v3

    .line 2581657
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2581658
    :cond_2
    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2581659
    invoke-static {}, LX/1dS;->b()V

    .line 2581660
    const/4 v0, 0x0

    return-object v0
.end method
