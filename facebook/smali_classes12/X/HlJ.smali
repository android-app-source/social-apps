.class public LX/HlJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0kb;

.field public final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0kb;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2498351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498352
    iput-object p1, p0, LX/HlJ;->a:LX/0kb;

    .line 2498353
    iput-object p2, p0, LX/HlJ;->b:LX/0Zb;

    .line 2498354
    return-void
.end method

.method public static a(LX/HlJ;LX/0oG;)V
    .locals 2

    .prologue
    .line 2498355
    const-string v0, "network_type"

    iget-object v1, p0, LX/HlJ;->a:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2498356
    const-string v0, "network_subtype"

    iget-object v1, p0, LX/HlJ;->a:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2498357
    invoke-virtual {p1}, LX/0oG;->d()V

    .line 2498358
    return-void
.end method


# virtual methods
.method public final a(LX/0oG;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2498359
    invoke-virtual {p1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2498360
    const-string v0, "status"

    const-string v1, "fail"

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2498361
    const-string v0, "error_message"

    invoke-virtual {p1, v0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2498362
    invoke-static {p0, p1}, LX/HlJ;->a(LX/HlJ;LX/0oG;)V

    .line 2498363
    :cond_0
    return-void
.end method
