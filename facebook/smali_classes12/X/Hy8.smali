.class public LX/Hy8;
.super LX/0gG;
.source ""

# interfaces
.implements LX/6Ub;


# static fields
.field public static final a:[LX/Hx6;

.field public static final b:[LX/Hx6;


# instance fields
.field public final c:Landroid/content/Context;

.field public d:[LX/Hx6;

.field public e:[LX/Hy4;

.field public f:LX/Hy7;

.field public g:Lcom/facebook/events/dashboard/EventsDashboardFragment;

.field private h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2523631
    new-array v0, v6, [LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    aput-object v1, v0, v2

    sget-object v1, LX/Hx6;->INVITED:LX/Hx6;

    aput-object v1, v0, v3

    sget-object v1, LX/Hx6;->HOSTING:LX/Hx6;

    aput-object v1, v0, v4

    sget-object v1, LX/Hx6;->PAST:LX/Hx6;

    aput-object v1, v0, v5

    sput-object v0, LX/Hy8;->a:[LX/Hx6;

    .line 2523632
    const/4 v0, 0x5

    new-array v0, v0, [LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    aput-object v1, v0, v2

    sget-object v1, LX/Hx6;->INVITED:LX/Hx6;

    aput-object v1, v0, v3

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    aput-object v1, v0, v4

    sget-object v1, LX/Hx6;->HOSTING:LX/Hx6;

    aput-object v1, v0, v5

    sget-object v1, LX/Hx6;->PAST:LX/Hx6;

    aput-object v1, v0, v6

    sput-object v0, LX/Hy8;->b:[LX/Hx6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2523627
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2523628
    iput-object p1, p0, LX/Hy8;->c:Landroid/content/Context;

    .line 2523629
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hy8;->h:Z

    .line 2523630
    return-void
.end method

.method public static a(LX/0QB;)LX/Hy8;
    .locals 2

    .prologue
    .line 2523624
    new-instance v1, LX/Hy8;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/Hy8;-><init>(Landroid/content/Context;)V

    .line 2523625
    move-object v0, v1

    .line 2523626
    return-object v0
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2523623
    iget-object v0, p0, LX/Hy8;->c:Landroid/content/Context;

    iget-object v1, p0, LX/Hy8;->d:[LX/Hx6;

    aget-object v1, v1, p1

    iget v1, v1, LX/Hx6;->menuStringResId:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/6Ua;
    .locals 1

    .prologue
    .line 2523622
    iget-object v0, p0, LX/Hy8;->f:LX/Hy7;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2523617
    iget-object v0, p0, LX/Hy8;->e:[LX/Hy4;

    aget-object v0, v0, p2

    .line 2523618
    if-nez v0, :cond_0

    .line 2523619
    new-instance v0, LX/Hy4;

    iget-object v1, p0, LX/Hy8;->d:[LX/Hx6;

    aget-object v1, v1, p2

    iget-object v2, p0, LX/Hy8;->g:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-direct {v0, v1, v2}, LX/Hy4;-><init>(LX/Hx6;Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523620
    iget-object v1, p0, LX/Hy8;->e:[LX/Hy4;

    aput-object v0, v1, p2

    .line 2523621
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2523584
    check-cast p3, LX/Hy4;

    .line 2523585
    iget-boolean p0, p3, LX/Hy4;->c:Z

    if-eqz p0, :cond_0

    .line 2523586
    iget-object p0, p3, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2523587
    const/4 p0, 0x0

    iput-boolean p0, p3, LX/Hy4;->c:Z

    .line 2523588
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2523614
    check-cast p2, LX/Hy4;

    .line 2523615
    iget-object v0, p2, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2523616
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2523613
    iget-object v0, p0, LX/Hy8;->d:[LX/Hx6;

    array-length v0, v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 2523589
    check-cast p3, LX/Hy4;

    .line 2523590
    iget-boolean v0, p3, LX/Hy4;->c:Z

    if-nez v0, :cond_2

    .line 2523591
    invoke-static {p3, p1}, LX/Hy4;->c(LX/Hy4;Landroid/view/ViewGroup;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2523592
    const/4 v0, 0x1

    iput-boolean v0, p3, LX/Hy4;->c:Z

    .line 2523593
    :cond_0
    :goto_0
    iget-boolean v0, p0, LX/Hy8;->h:Z

    if-eqz v0, :cond_1

    .line 2523594
    iget-object v0, p0, LX/Hy8;->g:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    if-eqz v0, :cond_1

    .line 2523595
    iget-object v0, p0, LX/Hy8;->g:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-virtual {v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->c()V

    .line 2523596
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Hy8;->h:Z

    .line 2523597
    :cond_1
    return-void

    .line 2523598
    :cond_2
    iget-object v0, p3, LX/Hy4;->e:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    if-eqz v0, :cond_0

    .line 2523599
    iget-object v0, p3, LX/Hy4;->e:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v1, p3, LX/Hy4;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p3, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget-object p2, p3, LX/Hy4;->d:LX/Hx6;

    .line 2523600
    iget-object p3, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    .line 2523601
    iget-object p1, p3, LX/HxQ;->c:LX/Hx6;

    move-object p3, p1

    .line 2523602
    if-eq p2, p3, :cond_4

    .line 2523603
    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    .line 2523604
    iput-object v2, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2523605
    sget-object p1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne p2, p1, :cond_5

    .line 2523606
    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->q(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523607
    :cond_3
    :goto_1
    iget-object p3, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    invoke-virtual {p3, p2}, LX/HxQ;->a(LX/Hx6;)V

    .line 2523608
    sget-object p3, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq p2, p3, :cond_4

    .line 2523609
    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->n(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523610
    :cond_4
    goto :goto_0

    .line 2523611
    :cond_5
    sget-object p1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne p3, p1, :cond_3

    .line 2523612
    iget-object p3, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    invoke-static {v0, p3}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a(Lcom/facebook/events/dashboard/EventsDashboardFragment;Lcom/facebook/widget/listview/BetterListView;)V

    goto :goto_1
.end method
