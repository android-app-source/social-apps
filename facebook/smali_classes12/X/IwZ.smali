.class public LX/IwZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/IwW;

.field private final c:LX/IwY;


# direct methods
.method public constructor <init>(LX/0Or;LX/IwW;LX/IwY;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/IwW;",
            "LX/IwY;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2630722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2630723
    iput-object p1, p0, LX/IwZ;->a:LX/0Or;

    .line 2630724
    iput-object p2, p0, LX/IwZ;->b:LX/IwW;

    .line 2630725
    iput-object p3, p0, LX/IwZ;->c:LX/IwY;

    .line 2630726
    return-void
.end method

.method public static a(LX/0QB;)LX/IwZ;
    .locals 8

    .prologue
    .line 2630693
    const-class v1, LX/IwZ;

    monitor-enter v1

    .line 2630694
    :try_start_0
    sget-object v0, LX/IwZ;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2630695
    sput-object v2, LX/IwZ;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2630696
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2630697
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2630698
    new-instance v5, LX/IwZ;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    .line 2630699
    new-instance v7, LX/IwW;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v4

    check-cast v4, LX/0sO;

    invoke-direct {v7, v3, v4}, LX/IwW;-><init>(Landroid/content/res/Resources;LX/0sO;)V

    .line 2630700
    move-object v3, v7

    .line 2630701
    check-cast v3, LX/IwW;

    .line 2630702
    new-instance p0, LX/IwY;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v7

    check-cast v7, LX/0sO;

    invoke-direct {p0, v4, v7}, LX/IwY;-><init>(Landroid/content/res/Resources;LX/0sO;)V

    .line 2630703
    move-object v4, p0

    .line 2630704
    check-cast v4, LX/IwY;

    invoke-direct {v5, v6, v3, v4}, LX/IwZ;-><init>(LX/0Or;LX/IwW;LX/IwY;)V

    .line 2630705
    move-object v0, v5

    .line 2630706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2630707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IwZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2630708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2630709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/0e6;Landroid/os/Parcelable;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2630710
    iget-object v0, p0, LX/IwZ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    .line 2630711
    invoke-virtual {v0, p1, p2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2630712
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2630713
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2630714
    iget-object v1, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 2630715
    const-string v2, "fetch_initial_recommended_pages"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2630716
    iget-object v0, p0, LX/IwZ;->b:LX/IwW;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/IwZ;->a(LX/0e6;Landroid/os/Parcelable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2630717
    :goto_0
    return-object v0

    .line 2630718
    :cond_0
    const-string v2, "fetch_recommended_pages_in_category"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2630719
    const-string v1, "fetchRecommendedPagesInCategory"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/methods/FetchRecommendedPagesInCategory$Params;

    .line 2630720
    iget-object v1, p0, LX/IwZ;->c:LX/IwY;

    invoke-direct {p0, v1, v0}, LX/IwZ;->a(LX/0e6;Landroid/os/Parcelable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2630721
    :cond_1
    new-instance v0, LX/4B3;

    invoke-direct {v0, v1}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v0
.end method
