.class public final LX/J6X;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 0

    .prologue
    .line 2649732
    iput-object p1, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 2649733
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->u:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-wide v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->C:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 2649734
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v1, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649735
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649736
    const-string v2, "step_success"

    iget-object v3, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v5, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v5, v5, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v6, v6, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649737
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649738
    iput v8, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    .line 2649739
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649740
    iput v8, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    .line 2649741
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 2649742
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 2649743
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649744
    iput-object v4, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    .line 2649745
    :cond_0
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    const-string v1, "SEND_PROGRESS_DIALOG_TAG"

    invoke-static {v0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->b(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;Ljava/lang/String;)V

    .line 2649746
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    if-eqz v0, :cond_1

    .line 2649747
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 2649748
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649749
    iput-object v4, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    .line 2649750
    :cond_1
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iput-object v4, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->p:LX/0Px;

    .line 2649751
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v0}, LX/J3t;->a()V

    .line 2649752
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v1}, LX/J3t;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    .line 2649753
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->finish()V

    .line 2649754
    :goto_0
    return-void

    .line 2649755
    :cond_2
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2649756
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    iget-object v1, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v2, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2649757
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    .line 2649758
    invoke-static {v0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;LX/0h5;)V

    .line 2649759
    const/4 v0, 0x1

    .line 2649760
    sput-boolean v0, LX/J3t;->j:Z

    .line 2649761
    iget-object v0, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v1}, LX/J3t;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewPager;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2649762
    const v1, 0x7f0d26ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649763
    iget-object v1, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v2, p0, LX/J6X;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v2}, LX/J3t;->c()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2649764
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2649765
    invoke-direct {p0}, LX/J6X;->a()V

    return-void
.end method
