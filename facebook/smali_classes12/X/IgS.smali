.class public final LX/IgS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "LX/0Px",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/loader/LocalMediaLoader;)V
    .locals 0

    .prologue
    .line 2601110
    iput-object p1, p0, LX/IgS;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2601111
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2601112
    if-nez p1, :cond_0

    .line 2601113
    const/4 v0, 0x0

    .line 2601114
    :goto_0
    return-object v0

    .line 2601115
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/service/LocalMediaLoadResult;

    .line 2601116
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2601117
    iget-object v3, v0, Lcom/facebook/messaging/media/service/LocalMediaLoadResult;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601118
    iget-object v5, p0, LX/IgS;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    iget-object v5, v5, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->g:Landroid/os/Bundle;

    const-string v6, "hideGifs"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v5}, Lcom/facebook/ipc/media/data/MimeType;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2601119
    :cond_1
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2601120
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2601121
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
