.class public final LX/Hyd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524354
    iput-object p1, p0, LX/Hyd;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2524355
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2524356
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2524357
    if-eqz p1, :cond_0

    .line 2524358
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524359
    if-eqz v0, :cond_0

    .line 2524360
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524361
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2524362
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524363
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

    move-result-object v0

    .line 2524364
    iget-object v1, p0, LX/Hyd;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    if-eqz v1, :cond_0

    .line 2524365
    iget-object v1, p0, LX/Hyd;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    new-instance v2, LX/Hyc;

    invoke-direct {v2, p0, v0}, LX/Hyc;-><init>(LX/Hyd;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;)V

    invoke-virtual {v1, v2}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setCountsSummarySupplier(LX/0QR;)V

    .line 2524366
    :cond_0
    return-void
.end method
