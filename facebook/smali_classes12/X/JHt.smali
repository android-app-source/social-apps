.class public final LX/JHt;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JHl;

.field public final synthetic b:LX/3LW;


# direct methods
.method public constructor <init>(LX/3LW;LX/JHl;)V
    .locals 0

    .prologue
    .line 2677196
    iput-object p1, p0, LX/JHt;->b:LX/3LW;

    iput-object p2, p0, LX/JHt;->a:LX/JHl;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2677197
    iget-object v0, p0, LX/JHt;->b:LX/3LW;

    iget-object v0, v0, LX/3LW;->f:LX/03V;

    const-string v1, "divebar_nearby_friends_section_data_fetch_fail"

    const-string v2, "fail to fetch divebar nearby friends section data"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677198
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2677199
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2677200
    if-eqz p1, :cond_0

    .line 2677201
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677202
    if-eqz v0, :cond_0

    .line 2677203
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677204
    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2677205
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677206
    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2677207
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677208
    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2677209
    :cond_0
    iget-object v0, p0, LX/JHt;->b:LX/3LW;

    iget-object v0, v0, LX/3LW;->f:LX/03V;

    const-string v1, "divebar_nearby_friends_section_data_fetch_return_empty"

    const-string v2, "fetched divebar nearby friends section data is empty"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677210
    :cond_1
    :goto_0
    return-void

    .line 2677211
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677212
    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsContactsTabModel$ContactsTabsModel$NodesModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsSectionsPageFieldsModel;

    move-result-object v2

    .line 2677213
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsSectionsPageFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsSectionsPageFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsSectionsPageFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel$ContactsSetsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsSectionsPageFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel$ContactsSetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel$ContactsSetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsSectionsPageFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel$ContactsSetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel$ContactsSetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2677214
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsSectionsPageFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel$ContactsSetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewSectionWrapperModel$ContactsSetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListSectionModel;

    .line 2677215
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2677216
    iget-object v2, p0, LX/JHt;->b:LX/3LW;

    iget-object v2, v2, LX/3LW;->l:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2677217
    iget-object v2, p0, LX/JHt;->b:LX/3LW;

    iget-object v2, v2, LX/3LW;->m:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2677218
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListSectionModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListSectionModel$SetItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListSectionModel$SetItemsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_7

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel;

    .line 2677219
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel;->b()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel;->b()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v1

    if-nez v1, :cond_4

    .line 2677220
    :cond_3
    iget-object v0, p0, LX/JHt;->b:LX/3LW;

    iget-object v0, v0, LX/3LW;->f:LX/03V;

    const-string v1, "divebar_nearby_friends_section_data_fetch_return_empty_contact"

    const-string v6, "fetched divebar nearby friends section contact info is empty"

    invoke-virtual {v0, v1, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677221
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2677222
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel;->b()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel;->a()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v1

    .line 2677223
    new-instance v6, LX/0XI;

    invoke-direct {v6}, LX/0XI;-><init>()V

    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v6

    new-instance v7, Lcom/facebook/user/model/Name;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ContactModel$RepresentedProfileModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v7, v1}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 2677224
    iput-object v7, v6, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2677225
    move-object v1, v6

    .line 2677226
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v6

    .line 2677227
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2677228
    iget-object v1, p0, LX/JHt;->b:LX/3LW;

    iget-object v7, v1, LX/3LW;->l:Ljava/util/Map;

    .line 2677229
    iget-object v1, v6, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v8, v1

    .line 2677230
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel;->c()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ItemDescriptionModel;

    move-result-object v1

    if-nez v1, :cond_6

    const/4 v1, 0x0

    :goto_3
    invoke-interface {v7, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2677231
    iget-object v1, p0, LX/JHt;->b:LX/3LW;

    iget-object v1, v1, LX/3LW;->m:Ljava/util/Map;

    .line 2677232
    iget-object v7, v6, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v6, v7

    .line 2677233
    iget-object v7, p0, LX/JHt;->b:LX/3LW;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel;->a()Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    move-result-object v0

    .line 2677234
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    if-eq v0, v8, :cond_5

    iget-object v8, v7, LX/3LW;->i:LX/0ad;

    sget-short v9, LX/3NR;->b:S

    const/4 p1, 0x0

    invoke-interface {v8, v9, p1}, LX/0ad;->a(SZ)Z

    move-result v8

    if-nez v8, :cond_8

    .line 2677235
    :cond_5
    sget-object v8, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    .line 2677236
    :goto_4
    move-object v0, v8

    .line 2677237
    invoke-interface {v1, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2677238
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel;->c()Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ItemDescriptionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsSectionGraphQLModels$NearbyFriendsNewListItemModel$ItemDescriptionModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2677239
    :cond_7
    iget-object v0, p0, LX/JHt;->a:LX/JHl;

    .line 2677240
    iget-object v1, v0, LX/JHl;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    .line 2677241
    iput-object v3, v1, Lcom/facebook/divebar/contacts/DivebarFragment;->I:Ljava/util/List;

    .line 2677242
    iget-object v1, v0, LX/JHl;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-static {v1}, Lcom/facebook/divebar/contacts/DivebarFragment;->r(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 2677243
    goto/16 :goto_0

    .line 2677244
    :cond_8
    sget-object v8, LX/JHz;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 2677245
    sget-object v8, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    goto :goto_4

    .line 2677246
    :pswitch_0
    sget-object v8, LX/3OL;->NOT_SENT:LX/3OL;

    goto :goto_4

    .line 2677247
    :pswitch_1
    sget-object v8, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    goto :goto_4

    .line 2677248
    :pswitch_2
    sget-object v8, LX/3OL;->RECEIVED:LX/3OL;

    goto :goto_4

    .line 2677249
    :pswitch_3
    sget-object v8, LX/3OL;->INTERACTED:LX/3OL;

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
