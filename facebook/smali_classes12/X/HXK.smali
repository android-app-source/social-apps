.class public final LX/HXK;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V
    .locals 0

    .prologue
    .line 2478669
    iput-object p1, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2478670
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2478671
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2478672
    if-eqz p1, :cond_0

    .line 2478673
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478674
    if-nez v0, :cond_1

    .line 2478675
    :cond_0
    :goto_0
    return-void

    .line 2478676
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478677
    check-cast v0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;

    .line 2478678
    iget-object v1, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->a()Z

    move-result v3

    .line 2478679
    iput-boolean v3, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->m:Z

    .line 2478680
    iget-object v1, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->j()Z

    move-result v3

    .line 2478681
    iput-boolean v3, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->n:Z

    .line 2478682
    iget-object v1, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->p:Z

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 2478683
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->k()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v4, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2478684
    iput-object v1, v4, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->l:Ljava/lang/String;

    .line 2478685
    :cond_2
    iget-object v1, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->o:Z

    if-eqz v1, :cond_3

    .line 2478686
    iget-object v1, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->l()LX/0Px;

    move-result-object v0

    .line 2478687
    iput-object v0, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    .line 2478688
    iget-object v0, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-static {v0}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->J(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V

    .line 2478689
    :cond_3
    iget-object v0, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->z:LX/HXN;

    iget-object v1, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->l:Ljava/lang/String;

    iget-object v2, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-boolean v2, v2, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->m:Z

    iget-object v3, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-boolean v3, v3, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->n:Z

    iget-object v4, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-object v4, v4, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    invoke-static {v4}, LX/8A4;->c(LX/0Px;)Z

    move-result v4

    .line 2478690
    iput-object v1, v0, LX/HXN;->f:Ljava/lang/String;

    .line 2478691
    iput-boolean v2, v0, LX/HXN;->g:Z

    .line 2478692
    iput-boolean v3, v0, LX/HXN;->h:Z

    .line 2478693
    iput-boolean v4, v0, LX/HXN;->i:Z

    .line 2478694
    iget-object v0, p0, LX/HXK;->a:Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->z:LX/HXN;

    const v1, -0x700c0d0c

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    :cond_4
    move v1, v2

    .line 2478695
    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_1
.end method
