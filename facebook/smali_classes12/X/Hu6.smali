.class public LX/Hu6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j8;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsCheckinSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0Tn;


# instance fields
.field public final c:LX/9jT;

.field public final d:LX/0y3;

.field public final e:LX/7kl;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/A5m;

.field public final h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final i:LX/HqN;

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2rb;",
            ">;"
        }
    .end annotation
.end field

.field public l:Landroid/location/Location;

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2517157
    const-class v0, LX/Hu6;

    sput-object v0, LX/Hu6;->a:Ljava/lang/Class;

    .line 2517158
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "composer_share_location"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hu6;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/HqN;LX/9jT;LX/0y3;LX/A5m;LX/7kl;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/HqN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/composer/location/ComposerLocationProductsPresenter$Listener;",
            "LX/9jT;",
            "LX/0y3;",
            "LX/A5m;",
            "LX/7kl;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/2rb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517122
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hu6;->h:Ljava/lang/ref/WeakReference;

    .line 2517123
    iput-object p2, p0, LX/Hu6;->i:LX/HqN;

    .line 2517124
    iput-object p3, p0, LX/Hu6;->c:LX/9jT;

    .line 2517125
    iput-object p4, p0, LX/Hu6;->d:LX/0y3;

    .line 2517126
    iput-object p5, p0, LX/Hu6;->g:LX/A5m;

    .line 2517127
    iput-object p7, p0, LX/Hu6;->f:LX/0Ot;

    .line 2517128
    iput-object p6, p0, LX/Hu6;->e:LX/7kl;

    .line 2517129
    iput-object p8, p0, LX/Hu6;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2517130
    iput-object p9, p0, LX/Hu6;->k:LX/0Ot;

    .line 2517131
    return-void
.end method

.method public static a$redex0(LX/Hu6;Landroid/location/Location;Z)V
    .locals 3

    .prologue
    .line 2517136
    iget-object v0, p0, LX/Hu6;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517137
    iget-object v1, p0, LX/Hu6;->e:LX/7kl;

    sget-object v2, LX/2tG;->REQUESTED:LX/2tG;

    invoke-virtual {v1, p2, v2}, LX/7kl;->a(ZLX/2tG;)V

    .line 2517138
    sget-object v1, LX/9jC;->Status:LX/9jC;

    .line 2517139
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2517140
    if-eqz v0, :cond_1

    .line 2517141
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2517142
    sget-object v0, LX/9jC;->Checkin:LX/9jC;

    .line 2517143
    :goto_0
    new-instance v1, LX/9jo;

    invoke-direct {v1}, LX/9jo;-><init>()V

    .line 2517144
    iput-object p1, v1, LX/9jo;->b:Landroid/location/Location;

    .line 2517145
    move-object v1, v1

    .line 2517146
    sget-object v2, LX/9jG;->STATUS:LX/9jG;

    .line 2517147
    iput-object v2, v1, LX/9jo;->c:LX/9jG;

    .line 2517148
    move-object v1, v1

    .line 2517149
    iput-boolean p2, v1, LX/9jo;->d:Z

    .line 2517150
    move-object v1, v1

    .line 2517151
    iput-object v0, v1, LX/9jo;->g:LX/9jC;

    .line 2517152
    move-object v1, v1

    .line 2517153
    iget-object v0, p0, LX/Hu6;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    new-instance v2, LX/Hu5;

    invoke-direct {v2, p0, p2}, LX/Hu5;-><init>(LX/Hu6;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a(LX/9jo;LX/0TF;)V

    .line 2517154
    return-void

    .line 2517155
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2517156
    sget-object v0, LX/9jC;->Photo:LX/9jC;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static e(LX/Hu6;)Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2517134
    iget-object v0, p0, LX/Hu6;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517135
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->k()Z

    move-result v1

    if-nez v1, :cond_0

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c(Z)V
    .locals 2

    .prologue
    .line 2517132
    iget-object v0, p0, LX/Hu6;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hu6;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2517133
    return-void
.end method
