.class public LX/Ipw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;

.field private static final n:LX/1sw;

.field private static final o:LX/1sw;

.field private static final p:LX/1sw;

.field private static final q:LX/1sw;

.field private static final r:LX/1sw;


# instance fields
.field public final availability:Ljava/lang/Integer;

.field public final buyerFbId:Ljava/lang/Long;

.field public final currency:Ljava/lang/String;

.field public final irisSeqId:Ljava/lang/Long;

.field public final name:Ljava/lang/String;

.field public final photoFbIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final platformContextFbId:Ljava/lang/Long;

.field public final platformItemFbId:Ljava/lang/Long;

.field public final priceAmount:Ljava/lang/Long;

.field public final priceAmountOffset:Ljava/lang/Integer;

.field public final referenceURL:Ljava/lang/String;

.field public final sellerFbId:Ljava/lang/Long;

.field public final shouldShowPayOption:Ljava/lang/Boolean;

.field public final shouldShowToBuyer:Ljava/lang/Boolean;

.field public final shouldShowToSeller:Ljava/lang/Boolean;

.field public final timestampMs:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/16 v6, 0x8

    const/16 v3, 0xb

    const/4 v5, 0x2

    const/16 v4, 0xa

    .line 2616047
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPlatformItemInterest"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipw;->b:LX/1sv;

    .line 2616048
    new-instance v0, LX/1sw;

    const-string v1, "platformItemFbId"

    invoke-direct {v0, v1, v4, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->c:LX/1sw;

    .line 2616049
    new-instance v0, LX/1sw;

    const-string v1, "sellerFbId"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->d:LX/1sw;

    .line 2616050
    new-instance v0, LX/1sw;

    const-string v1, "buyerFbId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->e:LX/1sw;

    .line 2616051
    new-instance v0, LX/1sw;

    const-string v1, "shouldShowToSeller"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->f:LX/1sw;

    .line 2616052
    new-instance v0, LX/1sw;

    const-string v1, "shouldShowToBuyer"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->g:LX/1sw;

    .line 2616053
    new-instance v0, LX/1sw;

    const-string v1, "timestampMs"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->h:LX/1sw;

    .line 2616054
    new-instance v0, LX/1sw;

    const-string v1, "name"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->i:LX/1sw;

    .line 2616055
    new-instance v0, LX/1sw;

    const-string v1, "currency"

    invoke-direct {v0, v1, v3, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->j:LX/1sw;

    .line 2616056
    new-instance v0, LX/1sw;

    const-string v1, "priceAmount"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->k:LX/1sw;

    .line 2616057
    new-instance v0, LX/1sw;

    const-string v1, "priceAmountOffset"

    invoke-direct {v0, v1, v6, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->l:LX/1sw;

    .line 2616058
    new-instance v0, LX/1sw;

    const-string v1, "availability"

    invoke-direct {v0, v1, v6, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->m:LX/1sw;

    .line 2616059
    new-instance v0, LX/1sw;

    const-string v1, "referenceURL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->n:LX/1sw;

    .line 2616060
    new-instance v0, LX/1sw;

    const-string v1, "photoFbIds"

    const/16 v2, 0xf

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->o:LX/1sw;

    .line 2616061
    new-instance v0, LX/1sw;

    const-string v1, "platformContextFbId"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->p:LX/1sw;

    .line 2616062
    new-instance v0, LX/1sw;

    const-string v1, "shouldShowPayOption"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->q:LX/1sw;

    .line 2616063
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipw;->r:LX/1sw;

    .line 2616064
    sput-boolean v7, LX/Ipw;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2615748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2615749
    iput-object p1, p0, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    .line 2615750
    iput-object p2, p0, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    .line 2615751
    iput-object p3, p0, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    .line 2615752
    iput-object p4, p0, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    .line 2615753
    iput-object p5, p0, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    .line 2615754
    iput-object p6, p0, LX/Ipw;->timestampMs:Ljava/lang/Long;

    .line 2615755
    iput-object p7, p0, LX/Ipw;->name:Ljava/lang/String;

    .line 2615756
    iput-object p8, p0, LX/Ipw;->currency:Ljava/lang/String;

    .line 2615757
    iput-object p9, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    .line 2615758
    iput-object p10, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    .line 2615759
    iput-object p11, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    .line 2615760
    iput-object p12, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    .line 2615761
    iput-object p13, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    .line 2615762
    iput-object p14, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    .line 2615763
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    .line 2615764
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    .line 2615765
    return-void
.end method

.method private static a(LX/Ipw;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 2616032
    iget-object v0, p0, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 2616033
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'platformItemFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2616034
    :cond_0
    iget-object v0, p0, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 2616035
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'sellerFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2616036
    :cond_1
    iget-object v0, p0, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    if-nez v0, :cond_2

    .line 2616037
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'buyerFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2616038
    :cond_2
    iget-object v0, p0, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    .line 2616039
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'shouldShowToSeller\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2616040
    :cond_3
    iget-object v0, p0, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    .line 2616041
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'shouldShowToBuyer\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2616042
    :cond_4
    iget-object v0, p0, LX/Ipw;->timestampMs:Ljava/lang/Long;

    if-nez v0, :cond_5

    .line 2616043
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'timestampMs\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2616044
    :cond_5
    iget-object v0, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    sget-object v0, LX/Iq4;->a:LX/1sn;

    iget-object v1, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2616045
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'availability\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2616046
    :cond_6
    return-void
.end method

.method public static b(LX/1su;)LX/Ipw;
    .locals 22

    .prologue
    .line 2615952
    const/4 v3, 0x0

    .line 2615953
    const/4 v4, 0x0

    .line 2615954
    const/4 v5, 0x0

    .line 2615955
    const/4 v6, 0x0

    .line 2615956
    const/4 v7, 0x0

    .line 2615957
    const/4 v8, 0x0

    .line 2615958
    const/4 v9, 0x0

    .line 2615959
    const/4 v10, 0x0

    .line 2615960
    const/4 v11, 0x0

    .line 2615961
    const/4 v12, 0x0

    .line 2615962
    const/4 v13, 0x0

    .line 2615963
    const/4 v14, 0x0

    .line 2615964
    const/4 v15, 0x0

    .line 2615965
    const/16 v16, 0x0

    .line 2615966
    const/16 v17, 0x0

    .line 2615967
    const/16 v18, 0x0

    .line 2615968
    invoke-virtual/range {p0 .. p0}, LX/1su;->r()LX/1sv;

    .line 2615969
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2615970
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    if-eqz v19, :cond_12

    .line 2615971
    iget-short v0, v2, LX/1sw;->c:S

    move/from16 v19, v0

    sparse-switch v19, :sswitch_data_0

    .line 2615972
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2615973
    :sswitch_0
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    .line 2615974
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 2615975
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2615976
    :sswitch_1
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 2615977
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 2615978
    :cond_2
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2615979
    :sswitch_2
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 2615980
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 2615981
    :cond_3
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2615982
    :sswitch_3
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 2615983
    invoke-virtual/range {p0 .. p0}, LX/1su;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto/16 :goto_0

    .line 2615984
    :cond_4
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2615985
    :sswitch_4
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    .line 2615986
    invoke-virtual/range {p0 .. p0}, LX/1su;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto/16 :goto_0

    .line 2615987
    :cond_5
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2615988
    :sswitch_5
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    .line 2615989
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    goto/16 :goto_0

    .line 2615990
    :cond_6
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2615991
    :sswitch_6
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xb

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 2615992
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 2615993
    :cond_7
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2615994
    :sswitch_7
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xb

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_8

    .line 2615995
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2615996
    :cond_8
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2615997
    :sswitch_8
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_9

    .line 2615998
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    goto/16 :goto_0

    .line 2615999
    :cond_9
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616000
    :sswitch_9
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_a

    .line 2616001
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/16 :goto_0

    .line 2616002
    :cond_a
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616003
    :sswitch_a
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 2616004
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    goto/16 :goto_0

    .line 2616005
    :cond_b
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616006
    :sswitch_b
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xb

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    .line 2616007
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_0

    .line 2616008
    :cond_c
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616009
    :sswitch_c
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xf

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_e

    .line 2616010
    invoke-virtual/range {p0 .. p0}, LX/1su;->h()LX/1u3;

    move-result-object v19

    .line 2616011
    new-instance v15, Ljava/util/ArrayList;

    const/4 v2, 0x0

    move-object/from16 v0, v19

    iget v0, v0, LX/1u3;->b:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2616012
    const/4 v2, 0x0

    .line 2616013
    :goto_1
    move-object/from16 v0, v19

    iget v0, v0, LX/1u3;->b:I

    move/from16 v20, v0

    if-gez v20, :cond_d

    invoke-static {}, LX/1su;->t()Z

    move-result v20

    if-eqz v20, :cond_0

    .line 2616014
    :goto_2
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    .line 2616015
    move-object/from16 v0, v20

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2616016
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2616017
    :cond_d
    move-object/from16 v0, v19

    iget v0, v0, LX/1u3;->b:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v2, v0, :cond_0

    goto :goto_2

    .line 2616018
    :cond_e
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616019
    :sswitch_d
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    .line 2616020
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    goto/16 :goto_0

    .line 2616021
    :cond_f
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616022
    :sswitch_e
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 2616023
    invoke-virtual/range {p0 .. p0}, LX/1su;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    goto/16 :goto_0

    .line 2616024
    :cond_10
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616025
    :sswitch_f
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v19, v0

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_11

    .line 2616026
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    goto/16 :goto_0

    .line 2616027
    :cond_11
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616028
    :cond_12
    invoke-virtual/range {p0 .. p0}, LX/1su;->e()V

    .line 2616029
    new-instance v2, LX/Ipw;

    invoke-direct/range {v2 .. v18}, LX/Ipw;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;)V

    .line 2616030
    invoke-static {v2}, LX/Ipw;->a(LX/Ipw;)V

    .line 2616031
    return-object v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_9
        0xb -> :sswitch_a
        0xc -> :sswitch_b
        0xd -> :sswitch_c
        0xe -> :sswitch_d
        0xf -> :sswitch_e
        0x3e8 -> :sswitch_f
    .end sparse-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2616065
    if-eqz p2, :cond_a

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2616066
    :goto_0
    if-eqz p2, :cond_b

    const-string v0, "\n"

    move-object v2, v0

    .line 2616067
    :goto_1
    if-eqz p2, :cond_c

    const-string v0, " "

    move-object v1, v0

    .line 2616068
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaPlatformItemInterest"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2616069
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616070
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616071
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616072
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616073
    const-string v0, "platformItemFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616074
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616075
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616076
    iget-object v0, p0, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    if-nez v0, :cond_d

    .line 2616077
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616078
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616079
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616080
    const-string v0, "sellerFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616081
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616082
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616083
    iget-object v0, p0, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    if-nez v0, :cond_e

    .line 2616084
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616085
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616086
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616087
    const-string v0, "buyerFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616088
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616089
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616090
    iget-object v0, p0, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    if-nez v0, :cond_f

    .line 2616091
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616092
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616093
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616094
    const-string v0, "shouldShowToSeller"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616095
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616096
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616097
    iget-object v0, p0, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    .line 2616098
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616099
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616100
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616101
    const-string v0, "shouldShowToBuyer"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616102
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616103
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616104
    iget-object v0, p0, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    .line 2616105
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616106
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616107
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616108
    const-string v0, "timestampMs"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616109
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616110
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616111
    iget-object v0, p0, LX/Ipw;->timestampMs:Ljava/lang/Long;

    if-nez v0, :cond_12

    .line 2616112
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616113
    :goto_8
    iget-object v0, p0, LX/Ipw;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2616114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616115
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616116
    const-string v0, "name"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616117
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616118
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616119
    iget-object v0, p0, LX/Ipw;->name:Ljava/lang/String;

    if-nez v0, :cond_13

    .line 2616120
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616121
    :cond_0
    :goto_9
    iget-object v0, p0, LX/Ipw;->currency:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2616122
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616123
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616124
    const-string v0, "currency"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616125
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616126
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616127
    iget-object v0, p0, LX/Ipw;->currency:Ljava/lang/String;

    if-nez v0, :cond_14

    .line 2616128
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616129
    :cond_1
    :goto_a
    iget-object v0, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2616130
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616131
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616132
    const-string v0, "priceAmount"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616133
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616134
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616135
    iget-object v0, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    if-nez v0, :cond_15

    .line 2616136
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616137
    :cond_2
    :goto_b
    iget-object v0, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2616138
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616139
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616140
    const-string v0, "priceAmountOffset"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616141
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616142
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616143
    iget-object v0, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    if-nez v0, :cond_16

    .line 2616144
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616145
    :cond_3
    :goto_c
    iget-object v0, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2616146
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616147
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616148
    const-string v0, "availability"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616149
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616150
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616151
    iget-object v0, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    if-nez v0, :cond_17

    .line 2616152
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616153
    :cond_4
    :goto_d
    iget-object v0, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2616154
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616155
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616156
    const-string v0, "referenceURL"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616157
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616158
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616159
    iget-object v0, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    if-nez v0, :cond_19

    .line 2616160
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616161
    :cond_5
    :goto_e
    iget-object v0, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 2616162
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616163
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616164
    const-string v0, "photoFbIds"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616165
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616166
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616167
    iget-object v0, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    if-nez v0, :cond_1a

    .line 2616168
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616169
    :cond_6
    :goto_f
    iget-object v0, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 2616170
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616171
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616172
    const-string v0, "platformContextFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616173
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616174
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616175
    iget-object v0, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    if-nez v0, :cond_1b

    .line 2616176
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616177
    :cond_7
    :goto_10
    iget-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 2616178
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616179
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616180
    const-string v0, "shouldShowPayOption"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616181
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616182
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616183
    iget-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    if-nez v0, :cond_1c

    .line 2616184
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616185
    :cond_8
    :goto_11
    iget-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 2616186
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616187
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616188
    const-string v0, "irisSeqId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616189
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616190
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616191
    iget-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_1d

    .line 2616192
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616193
    :cond_9
    :goto_12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616194
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616195
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2616196
    :cond_a
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2616197
    :cond_b
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2616198
    :cond_c
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 2616199
    :cond_d
    iget-object v0, p0, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2616200
    :cond_e
    iget-object v0, p0, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2616201
    :cond_f
    iget-object v0, p0, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2616202
    :cond_10
    iget-object v0, p0, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2616203
    :cond_11
    iget-object v0, p0, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2616204
    :cond_12
    iget-object v0, p0, LX/Ipw;->timestampMs:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 2616205
    :cond_13
    iget-object v0, p0, LX/Ipw;->name:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 2616206
    :cond_14
    iget-object v0, p0, LX/Ipw;->currency:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 2616207
    :cond_15
    iget-object v0, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 2616208
    :cond_16
    iget-object v0, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 2616209
    :cond_17
    sget-object v0, LX/Iq4;->b:Ljava/util/Map;

    iget-object v5, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2616210
    if-eqz v0, :cond_18

    .line 2616211
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616212
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616213
    :cond_18
    iget-object v5, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2616214
    if-eqz v0, :cond_4

    .line 2616215
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 2616216
    :cond_19
    iget-object v0, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    .line 2616217
    :cond_1a
    iget-object v0, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_f

    .line 2616218
    :cond_1b
    iget-object v0, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_10

    .line 2616219
    :cond_1c
    iget-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    .line 2616220
    :cond_1d
    iget-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_12
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    .line 2615887
    invoke-static {p0}, LX/Ipw;->a(LX/Ipw;)V

    .line 2615888
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2615889
    iget-object v0, p0, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2615890
    sget-object v0, LX/Ipw;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615891
    iget-object v0, p0, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615892
    :cond_0
    iget-object v0, p0, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2615893
    sget-object v0, LX/Ipw;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615894
    iget-object v0, p0, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615895
    :cond_1
    iget-object v0, p0, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2615896
    sget-object v0, LX/Ipw;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615897
    iget-object v0, p0, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615898
    :cond_2
    iget-object v0, p0, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2615899
    sget-object v0, LX/Ipw;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615900
    iget-object v0, p0, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2615901
    :cond_3
    iget-object v0, p0, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 2615902
    sget-object v0, LX/Ipw;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615903
    iget-object v0, p0, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2615904
    :cond_4
    iget-object v0, p0, LX/Ipw;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 2615905
    sget-object v0, LX/Ipw;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615906
    iget-object v0, p0, LX/Ipw;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615907
    :cond_5
    iget-object v0, p0, LX/Ipw;->name:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 2615908
    iget-object v0, p0, LX/Ipw;->name:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 2615909
    sget-object v0, LX/Ipw;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615910
    iget-object v0, p0, LX/Ipw;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2615911
    :cond_6
    iget-object v0, p0, LX/Ipw;->currency:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2615912
    iget-object v0, p0, LX/Ipw;->currency:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2615913
    sget-object v0, LX/Ipw;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615914
    iget-object v0, p0, LX/Ipw;->currency:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2615915
    :cond_7
    iget-object v0, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 2615916
    iget-object v0, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 2615917
    sget-object v0, LX/Ipw;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615918
    iget-object v0, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615919
    :cond_8
    iget-object v0, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 2615920
    iget-object v0, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 2615921
    sget-object v0, LX/Ipw;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615922
    iget-object v0, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2615923
    :cond_9
    iget-object v0, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 2615924
    iget-object v0, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 2615925
    sget-object v0, LX/Ipw;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615926
    iget-object v0, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2615927
    :cond_a
    iget-object v0, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 2615928
    iget-object v0, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 2615929
    sget-object v0, LX/Ipw;->n:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615930
    iget-object v0, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2615931
    :cond_b
    iget-object v0, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    if-eqz v0, :cond_c

    .line 2615932
    iget-object v0, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    if-eqz v0, :cond_c

    .line 2615933
    sget-object v0, LX/Ipw;->o:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615934
    new-instance v0, LX/1u3;

    const/16 v1, 0xa

    iget-object v2, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2615935
    iget-object v0, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2615936
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_0

    .line 2615937
    :cond_c
    iget-object v0, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 2615938
    iget-object v0, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 2615939
    sget-object v0, LX/Ipw;->p:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615940
    iget-object v0, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615941
    :cond_d
    iget-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 2615942
    iget-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 2615943
    sget-object v0, LX/Ipw;->q:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615944
    iget-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2615945
    :cond_e
    iget-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 2615946
    iget-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 2615947
    sget-object v0, LX/Ipw;->r:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615948
    iget-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615949
    :cond_f
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2615950
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2615951
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2615767
    if-nez p1, :cond_1

    .line 2615768
    :cond_0
    :goto_0
    return v0

    .line 2615769
    :cond_1
    instance-of v1, p1, LX/Ipw;

    if-eqz v1, :cond_0

    .line 2615770
    check-cast p1, LX/Ipw;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2615771
    if-nez p1, :cond_3

    .line 2615772
    :cond_2
    :goto_1
    move v0, v2

    .line 2615773
    goto :goto_0

    .line 2615774
    :cond_3
    iget-object v0, p0, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    if-eqz v0, :cond_24

    move v0, v1

    .line 2615775
    :goto_2
    iget-object v3, p1, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    if-eqz v3, :cond_25

    move v3, v1

    .line 2615776
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2615777
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615778
    iget-object v0, p0, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipw;->platformItemFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615779
    :cond_5
    iget-object v0, p0, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    if-eqz v0, :cond_26

    move v0, v1

    .line 2615780
    :goto_4
    iget-object v3, p1, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    if-eqz v3, :cond_27

    move v3, v1

    .line 2615781
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2615782
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615783
    iget-object v0, p0, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipw;->sellerFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615784
    :cond_7
    iget-object v0, p0, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    if-eqz v0, :cond_28

    move v0, v1

    .line 2615785
    :goto_6
    iget-object v3, p1, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    if-eqz v3, :cond_29

    move v3, v1

    .line 2615786
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2615787
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615788
    iget-object v0, p0, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipw;->buyerFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615789
    :cond_9
    iget-object v0, p0, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 2615790
    :goto_8
    iget-object v3, p1, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 2615791
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2615792
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615793
    iget-object v0, p0, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Ipw;->shouldShowToSeller:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615794
    :cond_b
    iget-object v0, p0, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    if-eqz v0, :cond_2c

    move v0, v1

    .line 2615795
    :goto_a
    iget-object v3, p1, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    if-eqz v3, :cond_2d

    move v3, v1

    .line 2615796
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2615797
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615798
    iget-object v0, p0, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Ipw;->shouldShowToBuyer:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615799
    :cond_d
    iget-object v0, p0, LX/Ipw;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_2e

    move v0, v1

    .line 2615800
    :goto_c
    iget-object v3, p1, LX/Ipw;->timestampMs:Ljava/lang/Long;

    if-eqz v3, :cond_2f

    move v3, v1

    .line 2615801
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2615802
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615803
    iget-object v0, p0, LX/Ipw;->timestampMs:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipw;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615804
    :cond_f
    iget-object v0, p0, LX/Ipw;->name:Ljava/lang/String;

    if-eqz v0, :cond_30

    move v0, v1

    .line 2615805
    :goto_e
    iget-object v3, p1, LX/Ipw;->name:Ljava/lang/String;

    if-eqz v3, :cond_31

    move v3, v1

    .line 2615806
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 2615807
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615808
    iget-object v0, p0, LX/Ipw;->name:Ljava/lang/String;

    iget-object v3, p1, LX/Ipw;->name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615809
    :cond_11
    iget-object v0, p0, LX/Ipw;->currency:Ljava/lang/String;

    if-eqz v0, :cond_32

    move v0, v1

    .line 2615810
    :goto_10
    iget-object v3, p1, LX/Ipw;->currency:Ljava/lang/String;

    if-eqz v3, :cond_33

    move v3, v1

    .line 2615811
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 2615812
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615813
    iget-object v0, p0, LX/Ipw;->currency:Ljava/lang/String;

    iget-object v3, p1, LX/Ipw;->currency:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615814
    :cond_13
    iget-object v0, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    if-eqz v0, :cond_34

    move v0, v1

    .line 2615815
    :goto_12
    iget-object v3, p1, LX/Ipw;->priceAmount:Ljava/lang/Long;

    if-eqz v3, :cond_35

    move v3, v1

    .line 2615816
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 2615817
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615818
    iget-object v0, p0, LX/Ipw;->priceAmount:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipw;->priceAmount:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615819
    :cond_15
    iget-object v0, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_36

    move v0, v1

    .line 2615820
    :goto_14
    iget-object v3, p1, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    if-eqz v3, :cond_37

    move v3, v1

    .line 2615821
    :goto_15
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 2615822
    :cond_16
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615823
    iget-object v0, p0, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipw;->priceAmountOffset:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615824
    :cond_17
    iget-object v0, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    if-eqz v0, :cond_38

    move v0, v1

    .line 2615825
    :goto_16
    iget-object v3, p1, LX/Ipw;->availability:Ljava/lang/Integer;

    if-eqz v3, :cond_39

    move v3, v1

    .line 2615826
    :goto_17
    if-nez v0, :cond_18

    if-eqz v3, :cond_19

    .line 2615827
    :cond_18
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615828
    iget-object v0, p0, LX/Ipw;->availability:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipw;->availability:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615829
    :cond_19
    iget-object v0, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    if-eqz v0, :cond_3a

    move v0, v1

    .line 2615830
    :goto_18
    iget-object v3, p1, LX/Ipw;->referenceURL:Ljava/lang/String;

    if-eqz v3, :cond_3b

    move v3, v1

    .line 2615831
    :goto_19
    if-nez v0, :cond_1a

    if-eqz v3, :cond_1b

    .line 2615832
    :cond_1a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615833
    iget-object v0, p0, LX/Ipw;->referenceURL:Ljava/lang/String;

    iget-object v3, p1, LX/Ipw;->referenceURL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615834
    :cond_1b
    iget-object v0, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    if-eqz v0, :cond_3c

    move v0, v1

    .line 2615835
    :goto_1a
    iget-object v3, p1, LX/Ipw;->photoFbIds:Ljava/util/List;

    if-eqz v3, :cond_3d

    move v3, v1

    .line 2615836
    :goto_1b
    if-nez v0, :cond_1c

    if-eqz v3, :cond_1d

    .line 2615837
    :cond_1c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615838
    iget-object v0, p0, LX/Ipw;->photoFbIds:Ljava/util/List;

    iget-object v3, p1, LX/Ipw;->photoFbIds:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615839
    :cond_1d
    iget-object v0, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    if-eqz v0, :cond_3e

    move v0, v1

    .line 2615840
    :goto_1c
    iget-object v3, p1, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    if-eqz v3, :cond_3f

    move v3, v1

    .line 2615841
    :goto_1d
    if-nez v0, :cond_1e

    if-eqz v3, :cond_1f

    .line 2615842
    :cond_1e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615843
    iget-object v0, p0, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipw;->platformContextFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615844
    :cond_1f
    iget-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    if-eqz v0, :cond_40

    move v0, v1

    .line 2615845
    :goto_1e
    iget-object v3, p1, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    if-eqz v3, :cond_41

    move v3, v1

    .line 2615846
    :goto_1f
    if-nez v0, :cond_20

    if-eqz v3, :cond_21

    .line 2615847
    :cond_20
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615848
    iget-object v0, p0, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Ipw;->shouldShowPayOption:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615849
    :cond_21
    iget-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_42

    move v0, v1

    .line 2615850
    :goto_20
    iget-object v3, p1, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_43

    move v3, v1

    .line 2615851
    :goto_21
    if-nez v0, :cond_22

    if-eqz v3, :cond_23

    .line 2615852
    :cond_22
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615853
    iget-object v0, p0, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipw;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_23
    move v2, v1

    .line 2615854
    goto/16 :goto_1

    :cond_24
    move v0, v2

    .line 2615855
    goto/16 :goto_2

    :cond_25
    move v3, v2

    .line 2615856
    goto/16 :goto_3

    :cond_26
    move v0, v2

    .line 2615857
    goto/16 :goto_4

    :cond_27
    move v3, v2

    .line 2615858
    goto/16 :goto_5

    :cond_28
    move v0, v2

    .line 2615859
    goto/16 :goto_6

    :cond_29
    move v3, v2

    .line 2615860
    goto/16 :goto_7

    :cond_2a
    move v0, v2

    .line 2615861
    goto/16 :goto_8

    :cond_2b
    move v3, v2

    .line 2615862
    goto/16 :goto_9

    :cond_2c
    move v0, v2

    .line 2615863
    goto/16 :goto_a

    :cond_2d
    move v3, v2

    .line 2615864
    goto/16 :goto_b

    :cond_2e
    move v0, v2

    .line 2615865
    goto/16 :goto_c

    :cond_2f
    move v3, v2

    .line 2615866
    goto/16 :goto_d

    :cond_30
    move v0, v2

    .line 2615867
    goto/16 :goto_e

    :cond_31
    move v3, v2

    .line 2615868
    goto/16 :goto_f

    :cond_32
    move v0, v2

    .line 2615869
    goto/16 :goto_10

    :cond_33
    move v3, v2

    .line 2615870
    goto/16 :goto_11

    :cond_34
    move v0, v2

    .line 2615871
    goto/16 :goto_12

    :cond_35
    move v3, v2

    .line 2615872
    goto/16 :goto_13

    :cond_36
    move v0, v2

    .line 2615873
    goto/16 :goto_14

    :cond_37
    move v3, v2

    .line 2615874
    goto/16 :goto_15

    :cond_38
    move v0, v2

    .line 2615875
    goto/16 :goto_16

    :cond_39
    move v3, v2

    .line 2615876
    goto/16 :goto_17

    :cond_3a
    move v0, v2

    .line 2615877
    goto/16 :goto_18

    :cond_3b
    move v3, v2

    .line 2615878
    goto/16 :goto_19

    :cond_3c
    move v0, v2

    .line 2615879
    goto/16 :goto_1a

    :cond_3d
    move v3, v2

    .line 2615880
    goto/16 :goto_1b

    :cond_3e
    move v0, v2

    .line 2615881
    goto/16 :goto_1c

    :cond_3f
    move v3, v2

    .line 2615882
    goto/16 :goto_1d

    :cond_40
    move v0, v2

    .line 2615883
    goto/16 :goto_1e

    :cond_41
    move v3, v2

    .line 2615884
    goto/16 :goto_1f

    :cond_42
    move v0, v2

    .line 2615885
    goto :goto_20

    :cond_43
    move v3, v2

    .line 2615886
    goto :goto_21
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2615766
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2615745
    sget-boolean v0, LX/Ipw;->a:Z

    .line 2615746
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipw;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2615747
    return-object v0
.end method
