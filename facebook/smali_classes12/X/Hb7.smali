.class public final LX/Hb7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HbE;


# direct methods
.method public constructor <init>(LX/HbE;)V
    .locals 0

    .prologue
    .line 2485133
    iput-object p1, p0, LX/Hb7;->a:LX/HbE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0xeb3dbf6

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2485099
    iget-object v1, p0, LX/Hb7;->a:LX/HbE;

    iget-object v1, v1, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2485100
    iget-object v1, p0, LX/Hb7;->a:LX/HbE;

    .line 2485101
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2485102
    iget-object v3, v1, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    :goto_0
    if-ltz v3, :cond_1

    .line 2485103
    iget-object v5, v1, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v5, v3}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->e(I)LX/HbG;

    move-result-object v5

    .line 2485104
    iget-boolean p1, v5, LX/HbG;->e:Z

    move p1, p1

    .line 2485105
    if-eqz p1, :cond_0

    .line 2485106
    iget-object p1, v5, LX/HbG;->f:Ljava/lang/String;

    move-object v5, p1

    .line 2485107
    const-string p1, ","

    invoke-virtual {v5, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 2485108
    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2485109
    iget-object v5, v1, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    .line 2485110
    iget-object p1, v5, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->a:Ljava/util/List;

    invoke-interface {p1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2485111
    invoke-virtual {v5, v3}, LX/1OM;->d(I)V

    .line 2485112
    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 2485113
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2485114
    iget-object v3, v1, LX/HbE;->w:Ljava/lang/Runnable;

    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    .line 2485115
    :cond_2
    new-instance v3, LX/4EF;

    invoke-direct {v3}, LX/4EF;-><init>()V

    .line 2485116
    const-string v5, "session_ids"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2485117
    move-object v3, v3

    .line 2485118
    iget-object v5, v1, LX/HbE;->x:LX/HaZ;

    .line 2485119
    iget-object p1, v5, LX/HaZ;->b:Ljava/lang/String;

    move-object v5, p1

    .line 2485120
    const-string p1, "security_checkup_source"

    invoke-virtual {v3, p1, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2485121
    move-object v3, v3

    .line 2485122
    new-instance v5, LX/Haj;

    invoke-direct {v5}, LX/Haj;-><init>()V

    move-object v5, v5

    .line 2485123
    const-string p1, "input"

    invoke-virtual {v5, p1, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2485124
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2485125
    iget-object v3, v1, LX/HbE;->t:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-virtual {v3, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2485126
    iget-object v1, p0, LX/Hb7;->a:LX/HbE;

    .line 2485127
    iget-object v3, v1, LX/HbE;->p:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, v1, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08363f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object p1, v1, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {p1}, LX/1OM;->ij_()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v7, v8

    invoke-virtual {v4, v5, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485128
    iget-object v3, v1, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v4, v1, LX/HbE;->i:Landroid/content/Context;

    const v5, 0x7f08363c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485129
    iget-object v1, p0, LX/Hb7;->a:LX/HbE;

    iget-boolean v1, v1, LX/HbE;->r:Z

    if-eqz v1, :cond_3

    .line 2485130
    iget-object v1, p0, LX/Hb7;->a:LX/HbE;

    iget-object v1, v1, LX/HbE;->q:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    invoke-virtual {v1, v2}, Lcom/facebook/securitycheckup/inner/InertCheckBox;->setChecked(Z)V

    .line 2485131
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerController$2$1;

    invoke-direct {v2, p0}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerController$2$1;-><init>(LX/Hb7;)V

    const-wide/16 v4, 0x2bc

    const v3, -0x1a841634

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2485132
    :cond_3
    const v1, 0x62125420

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
