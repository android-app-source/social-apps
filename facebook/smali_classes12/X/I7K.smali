.class public final LX/I7K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g4;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538735
    iput-object p1, p0, LX/I7K;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final d()V
    .locals 6

    .prologue
    .line 2538736
    iget-object v0, p0, LX/I7K;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    iget-object v1, p0, LX/I7K;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    .line 2538737
    iget-object v2, v0, LX/I8k;->d:LX/I8W;

    invoke-virtual {v2}, LX/I8W;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2538738
    if-eqz v2, :cond_0

    .line 2538739
    iget-object v3, v0, LX/I8k;->p:LX/0QK;

    .line 2538740
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 2538741
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0fz;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2538742
    invoke-interface {v3, v4}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2538743
    :cond_0
    iget-object v0, p0, LX/I7K;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2538744
    return-void

    .line 2538745
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 2538746
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 2538747
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 2538748
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 2538749
    if-eqz v4, :cond_2

    .line 2538750
    iget-object v0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v0

    .line 2538751
    invoke-interface {v3, v4}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
