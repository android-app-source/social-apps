.class public LX/Ihd;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/LayoutInflater;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Ihe;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2602903
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2602904
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2602905
    iput-object v0, p0, LX/Ihd;->d:LX/0Px;

    .line 2602906
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2602898
    iget-object v0, p0, LX/Ihd;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030f39

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2602899
    iget-object v1, p0, LX/Ihd;->b:LX/Ihe;

    .line 2602900
    new-instance p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    invoke-direct {p1, v2, p0, v0}, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;-><init>(LX/1Ad;Landroid/content/res/Resources;Landroid/view/View;)V

    .line 2602901
    move-object v0, p1

    .line 2602902
    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2602907
    iget-object v0, p0, LX/Ihd;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602908
    check-cast p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;

    .line 2602909
    if-nez p2, :cond_0

    .line 2602910
    iget-object v1, p0, LX/Ihd;->c:Landroid/content/res/Resources;

    const v2, 0x7f0b18c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2602911
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2602912
    iget-object v1, p0, LX/Ihd;->c:Landroid/content/res/Resources;

    const v3, 0x7f0b18c9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2602913
    iget-object v3, p0, LX/Ihd;->c:Landroid/content/res/Resources;

    const v4, 0x7f0b18ca

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2602914
    invoke-virtual {v2, v1, v1, v3, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2602915
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2602916
    :cond_0
    iput-object v0, p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->q:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602917
    iget-object v1, p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->q:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v1, :cond_1

    .line 2602918
    iget-object v1, p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->q:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->o:LX/1o9;

    .line 2602919
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 2602920
    move-object v1, v1

    .line 2602921
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 2602922
    iget-object v2, p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->m:LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->o()LX/1Ad;

    move-result-object v3

    sget-object v0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    iget-object v3, p1, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2602923
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2602895
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Ihd;->d:LX/0Px;

    .line 2602896
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2602897
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2602894
    iget-object v0, p0, LX/Ihd;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
