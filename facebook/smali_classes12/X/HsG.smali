.class public final LX/HsG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HsF;


# instance fields
.field public final synthetic a:LX/HsL;


# direct methods
.method public constructor <init>(LX/HsL;)V
    .locals 0

    .prologue
    .line 2514152
    iput-object p1, p0, LX/HsG;->a:LX/HsL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2514153
    iget-object v0, p0, LX/HsG;->a:LX/HsL;

    iget-object v0, v0, LX/HsL;->k:LX/HqG;

    if-eqz v0, :cond_0

    .line 2514154
    iget-object v0, p0, LX/HsG;->a:LX/HsL;

    iget-object v0, v0, LX/HsL;->k:LX/HqG;

    .line 2514155
    iget-object v1, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_COLLAGE_CLICKED:LX/0ge;

    iget-object v1, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2514156
    iget-object v1, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2514157
    iget-object v2, v1, LX/AQ9;->S:LX/AQ4;

    move-object v3, v2

    .line 2514158
    iget-object v1, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2514159
    new-instance v2, LX/HwN;

    invoke-direct {v2}, LX/HwN;-><init>()V

    move-object p0, v2

    .line 2514160
    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    .line 2514161
    iput-object v2, p0, LX/HwN;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2514162
    move-object p0, p0

    .line 2514163
    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 2514164
    iput-object v2, p0, LX/HwN;->o:Ljava/lang/String;

    .line 2514165
    move-object p0, p0

    .line 2514166
    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    .line 2514167
    iput-object v2, p0, LX/HwN;->a:LX/0Px;

    .line 2514168
    move-object p0, p0

    .line 2514169
    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2514170
    iput-object v2, p0, LX/HwN;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2514171
    move-object p0, p0

    .line 2514172
    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->s()LX/0P1;

    move-result-object v2

    .line 2514173
    iput-object v2, p0, LX/HwN;->c:LX/0P1;

    .line 2514174
    move-object v2, p0

    .line 2514175
    iget-object p0, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object p0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {p0}, LX/2zG;->t()Z

    move-result p0

    .line 2514176
    iput-boolean p0, v2, LX/HwN;->d:Z

    .line 2514177
    move-object p0, v2

    .line 2514178
    if-nez v3, :cond_1

    const/4 v2, 0x0

    .line 2514179
    :goto_0
    iput-object v2, p0, LX/HwN;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2514180
    move-object v3, p0

    .line 2514181
    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0iv;->c()Z

    move-result v2

    .line 2514182
    iput-boolean v2, v3, LX/HwN;->e:Z

    .line 2514183
    move-object v2, v3

    .line 2514184
    iget-object v3, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v3}, LX/2rh;->q()Z

    move-result v3

    .line 2514185
    iput-boolean v3, v2, LX/HwN;->f:Z

    .line 2514186
    move-object v2, v2

    .line 2514187
    iget-object v3, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v3}, LX/5Qw;->s()Z

    move-result v3

    .line 2514188
    iput-boolean v3, v2, LX/HwN;->g:Z

    .line 2514189
    move-object v2, v2

    .line 2514190
    iget-object v3, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v3}, LX/2ri;->x()Z

    move-result v3

    .line 2514191
    iput-boolean v3, v2, LX/HwN;->h:Z

    .line 2514192
    move-object v2, v2

    .line 2514193
    iget-object v3, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v3}, LX/5Qx;->y()Z

    move-result v3

    .line 2514194
    iput-boolean v3, v2, LX/HwN;->i:Z

    .line 2514195
    move-object v2, v2

    .line 2514196
    iget-object v3, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v3}, LX/5Qy;->z()Z

    move-result v3

    .line 2514197
    iput-boolean v3, v2, LX/HwN;->j:Z

    .line 2514198
    move-object v2, v2

    .line 2514199
    iget-object v3, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v3}, LX/5Qz;->C()Z

    move-result v3

    .line 2514200
    iput-boolean v3, v2, LX/HwN;->k:Z

    .line 2514201
    move-object v2, v2

    .line 2514202
    iget-object v3, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v3}, LX/5R0;->D()Z

    move-result v3

    .line 2514203
    iput-boolean v3, v2, LX/HwN;->l:Z

    .line 2514204
    move-object v2, v2

    .line 2514205
    iget-object v3, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v3}, LX/5R1;->H()Z

    move-result v3

    .line 2514206
    iput-boolean v3, v2, LX/HwN;->m:Z

    .line 2514207
    move-object v3, v2

    .line 2514208
    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    .line 2514209
    iput-object v2, v3, LX/HwN;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2514210
    move-object v3, v3

    .line 2514211
    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v2}, LX/2zG;->B()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v2

    invoke-static {v2}, LX/94d;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    .line 2514212
    :goto_1
    iput-boolean v2, v3, LX/HwN;->p:Z

    .line 2514213
    move-object v2, v3

    .line 2514214
    invoke-virtual {v2}, LX/HwN;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->a(Landroid/content/Context;Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;)Landroid/content/Intent;

    move-result-object v2

    const/16 v3, 0x84

    iget-object v4, v0, LX/HqG;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2514215
    :cond_0
    return-void

    .line 2514216
    :cond_1
    invoke-interface {v3}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
