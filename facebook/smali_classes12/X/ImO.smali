.class public LX/ImO;
.super LX/ImH;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private a:LX/J0B;

.field private b:LX/6og;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609652
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImO;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/J0B;LX/6og;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609653
    invoke-direct {p0}, LX/ImH;-><init>()V

    .line 2609654
    iput-object p1, p0, LX/ImO;->a:LX/J0B;

    .line 2609655
    iput-object p2, p0, LX/ImO;->b:LX/6og;

    .line 2609656
    return-void
.end method

.method public static a(LX/0QB;)LX/ImO;
    .locals 8

    .prologue
    .line 2609657
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609658
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609659
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609660
    if-nez v1, :cond_0

    .line 2609661
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609662
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609663
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609664
    sget-object v1, LX/ImO;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609665
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609666
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609667
    :cond_1
    if-nez v1, :cond_4

    .line 2609668
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609669
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609670
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609671
    new-instance p0, LX/ImO;

    invoke-static {v0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v1

    check-cast v1, LX/J0B;

    invoke-static {v0}, LX/6og;->a(LX/0QB;)LX/6og;

    move-result-object v7

    check-cast v7, LX/6og;

    invoke-direct {p0, v1, v7}, LX/ImO;-><init>(LX/J0B;LX/6og;)V

    .line 2609672
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609673
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609674
    if-nez v1, :cond_2

    .line 2609675
    sget-object v0, LX/ImO;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImO;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609676
    :goto_1
    if-eqz v0, :cond_3

    .line 2609677
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609678
    :goto_3
    check-cast v0, LX/ImO;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609679
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609680
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609681
    :catchall_1
    move-exception v0

    .line 2609682
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609683
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609684
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609685
    :cond_2
    :try_start_8
    sget-object v0, LX/ImO;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImO;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/7GJ;)Landroid/os/Bundle;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2609686
    iget-object v0, p1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v0}, LX/Ipt;->e()LX/Ipu;

    move-result-object v1

    .line 2609687
    iget-object v0, v1, LX/Ipu;->pinFbId:Ljava/lang/Long;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2609688
    :goto_0
    iget-object v1, p0, LX/ImO;->b:LX/6og;

    invoke-virtual {v1, v0}, LX/6og;->a(Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    .line 2609689
    iget-object v0, p0, LX/ImO;->a:LX/J0B;

    .line 2609690
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2609691
    const-string v2, "com.facebook.payments.auth.ACTION_PIN_UPDATED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2609692
    invoke-static {v0, v1}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2609693
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2609694
    const-string v1, "newPaymentsResult"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2609695
    return-object v0

    .line 2609696
    :cond_0
    new-instance v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    iget-object v1, v1, LX/Ipu;->pinFbId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/facebook/payments/auth/pin/model/PaymentPin;-><init>(J)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2609697
    return-void
.end method
