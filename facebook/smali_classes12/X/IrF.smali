.class public final LX/IrF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IrE;


# instance fields
.field public final synthetic a:LX/IrP;


# direct methods
.method public constructor <init>(LX/IrP;)V
    .locals 0

    .prologue
    .line 2617738
    iput-object p1, p0, LX/IrF;->a:LX/IrP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2617761
    iget-object v0, p0, LX/IrF;->a:LX/IrP;

    .line 2617762
    iget-object v1, v0, LX/IrP;->w:LX/Iqe;

    sget-object p0, LX/Iqe;->TEXT:LX/Iqe;

    if-ne v1, p0, :cond_0

    .line 2617763
    sget-object v1, LX/Iqe;->IDLE:LX/Iqe;

    invoke-static {v0, v1}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617764
    :goto_0
    return-void

    .line 2617765
    :cond_0
    sget-object v1, LX/Iqe;->TEXT:LX/Iqe;

    invoke-static {v0, v1}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2617756
    iget-object v0, p0, LX/IrF;->a:LX/IrP;

    .line 2617757
    iget-object v1, v0, LX/IrP;->w:LX/Iqe;

    sget-object p0, LX/Iqe;->STICKER:LX/Iqe;

    if-ne v1, p0, :cond_0

    .line 2617758
    sget-object v1, LX/Iqe;->IDLE:LX/Iqe;

    invoke-static {v0, v1}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617759
    :goto_0
    return-void

    .line 2617760
    :cond_0
    sget-object v1, LX/Iqe;->STICKER:LX/Iqe;

    invoke-static {v0, v1}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2617751
    iget-object v0, p0, LX/IrF;->a:LX/IrP;

    .line 2617752
    iget-object v1, v0, LX/IrP;->w:LX/Iqe;

    sget-object p0, LX/Iqe;->DOODLE:LX/Iqe;

    if-eq v1, p0, :cond_0

    invoke-static {v0}, LX/IrP;->n(LX/IrP;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2617753
    :cond_0
    sget-object v1, LX/Iqe;->IDLE:LX/Iqe;

    invoke-static {v0, v1}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617754
    :goto_0
    return-void

    .line 2617755
    :cond_1
    sget-object v1, LX/Iqe;->DOODLE:LX/Iqe;

    invoke-static {v0, v1}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2617745
    iget-object v0, p0, LX/IrF;->a:LX/IrP;

    .line 2617746
    iget-object p0, v0, LX/IrP;->t:LX/Iqd;

    if-eqz p0, :cond_0

    .line 2617747
    iget-object p0, v0, LX/IrP;->t:LX/Iqd;

    .line 2617748
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    if-eqz v0, :cond_0

    .line 2617749
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->b()V

    .line 2617750
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2617739
    iget-object v0, p0, LX/IrF;->a:LX/IrP;

    .line 2617740
    iget-object p0, v0, LX/IrP;->t:LX/Iqd;

    if-eqz p0, :cond_0

    .line 2617741
    iget-object p0, v0, LX/IrP;->t:LX/Iqd;

    .line 2617742
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    if-eqz v0, :cond_0

    .line 2617743
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->a()V

    .line 2617744
    :cond_0
    return-void
.end method
