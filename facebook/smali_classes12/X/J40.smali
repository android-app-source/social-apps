.class public final LX/J40;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J4L;

.field public final synthetic b:LX/J45;


# direct methods
.method public constructor <init>(LX/J45;LX/J4L;)V
    .locals 0

    .prologue
    .line 2643501
    iput-object p1, p0, LX/J40;->b:LX/J45;

    iput-object p2, p0, LX/J40;->a:LX/J4L;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2643502
    iget-object v0, p0, LX/J40;->b:LX/J45;

    iget-object v0, v0, LX/J45;->d:LX/J5g;

    iget-object v1, p0, LX/J40;->a:LX/J4L;

    iget-object v1, v1, LX/J4L;->j:Ljava/lang/String;

    const/16 v2, 0xf

    .line 2643503
    new-instance v3, LX/J59;

    invoke-direct {v3}, LX/J59;-><init>()V

    move-object v3, v3

    .line 2643504
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2643505
    new-instance v4, LX/J59;

    invoke-direct {v4}, LX/J59;-><init>()V

    const-string v5, "first"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "after"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    .line 2643506
    iget-object v5, v4, LX/0gW;->e:LX/0w7;

    move-object v4, v5

    .line 2643507
    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2643508
    iget-object v4, v0, LX/J5g;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v0, v3

    .line 2643509
    new-instance v1, LX/J3z;

    invoke-direct {v1, p0}, LX/J3z;-><init>(LX/J40;)V

    iget-object v2, p0, LX/J40;->b:LX/J45;

    iget-object v2, v2, LX/J45;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
