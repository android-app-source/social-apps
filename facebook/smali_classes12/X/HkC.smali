.class public LX/HkC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HkB;


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public a:LX/HjO;

.field public final c:Landroid/content/Context;

.field public final d:Landroid/os/Handler;

.field public final e:Ljava/lang/Runnable;

.field private final f:Ljava/lang/Runnable;

.field public volatile g:Z

.field public h:Z

.field public volatile i:Z

.field public j:LX/HjT;

.field public k:Landroid/view/View;

.field public l:LX/Hjx;

.field public m:LX/Hk0;

.field private n:LX/Hj3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, LX/HkC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HkC;->b:Ljava/lang/String;

    return-void
.end method

.method public static b()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Adapter listener must be called on the main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public static c(LX/HkC;)LX/HkF;
    .locals 2

    iget-object v0, p0, LX/HkC;->n:LX/Hj3;

    if-nez v0, :cond_0

    sget-object v0, LX/HkF;->NATIVE:LX/HkF;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/HkC;->n:LX/Hj3;

    sget-object v1, LX/Hj3;->INTERSTITIAL:LX/Hj3;

    if-ne v0, v1, :cond_1

    sget-object v0, LX/HkF;->INTERSTITIAL:LX/HkF;

    goto :goto_0

    :cond_1
    sget-object v0, LX/HkF;->BANNER:LX/HkF;

    goto :goto_0
.end method

.method public static declared-synchronized d$redex0(LX/HkC;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/facebook/ads/internal/h$3;

    invoke-direct {v1, p0}, Lcom/facebook/ads/internal/h$3;-><init>(LX/HkC;)V

    const v2, -0x2ebe188

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static f(LX/HkC;)V
    .locals 6

    const-wide/16 v4, 0x3e8

    const/4 v1, 0x1

    iget-boolean v0, p0, LX/HkC;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/HkC;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, LX/Hk7;->a:[I

    invoke-static {p0}, LX/HkC;->c(LX/HkC;)LX/HkF;

    move-result-object v2

    invoke-virtual {v2}, LX/HkF;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, LX/HkC;->c:Landroid/content/Context;

    invoke-static {v0}, LX/Hko;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/HkC;->d:Landroid/os/Handler;

    iget-object v2, p0, LX/HkC;->f:Ljava/lang/Runnable;

    const v3, 0x489ec0c

    invoke-static {v0, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    :cond_2
    iget-object v0, p0, LX/HkC;->l:LX/Hjx;

    if-nez v0, :cond_4

    const-wide/16 v2, 0x7530

    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    iget-object v0, p0, LX/HkC;->d:Landroid/os/Handler;

    iget-object v4, p0, LX/HkC;->e:Ljava/lang/Runnable;

    const v5, -0x37e0e161

    invoke-static {v0, v4, v2, v3, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    iput-boolean v1, p0, LX/HkC;->g:Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, LX/HkC;->l:LX/Hjx;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    iget-object v2, p0, LX/HkC;->k:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/HkC;->c:Landroid/content/Context;

    iget-object v3, p0, LX/HkC;->k:Landroid/view/View;

    invoke-static {v2, v3, v0}, LX/Hko;->a(Landroid/content/Context;Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/HkC;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/HkC;->f:Ljava/lang/Runnable;

    const v2, 0x4a6e2f75    # 3902429.2f

    invoke-static {v0, v1, v4, v5, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/HkC;->l:LX/Hjx;

    iget-object v2, v0, LX/Hjx;->c:LX/Hjy;

    move-object v0, v2

    iget v2, v0, LX/Hjy;->b:I

    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v0, p0, LX/HkC;->l:LX/Hjx;

    iget-object v2, v0, LX/Hjx;->c:LX/Hjy;

    move-object v0, v2

    invoke-virtual {v0}, LX/Hjy;->b()J

    move-result-wide v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/Hjy;
    .locals 1

    iget-object v0, p0, LX/HkC;->l:LX/Hjx;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/HkC;->l:LX/Hjx;

    iget-object p0, v0, LX/Hjx;->c:LX/Hjy;

    move-object v0, p0

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/Hjr;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/HkC;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/ads/internal/h$10;

    invoke-direct {v1, p0, p1}, Lcom/facebook/ads/internal/h$10;-><init>(LX/HkC;LX/Hjr;)V

    const v2, -0x631e3fd9

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/HkN;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/HkC;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/ads/internal/h$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/ads/internal/h$1;-><init>(LX/HkC;LX/HkN;)V

    const v2, 0x291db7a3

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
