.class public LX/IkG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/73s;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/73s;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606544
    iput-object p1, p0, LX/IkG;->a:Landroid/content/res/Resources;

    .line 2606545
    iput-object p2, p0, LX/IkG;->b:LX/73s;

    .line 2606546
    return-void
.end method

.method public static a(LX/0QB;)LX/IkG;
    .locals 5

    .prologue
    .line 2606547
    const-class v1, LX/IkG;

    monitor-enter v1

    .line 2606548
    :try_start_0
    sget-object v0, LX/IkG;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606549
    sput-object v2, LX/IkG;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606550
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606551
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606552
    new-instance p0, LX/IkG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/73s;->b(LX/0QB;)LX/73s;

    move-result-object v4

    check-cast v4, LX/73s;

    invoke-direct {p0, v3, v4}, LX/IkG;-><init>(Landroid/content/res/Resources;LX/73s;)V

    .line 2606553
    move-object v0, p0

    .line 2606554
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606555
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606556
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606557
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
