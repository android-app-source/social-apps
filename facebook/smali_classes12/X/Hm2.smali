.class public LX/Hm2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Lcom/facebook/user/model/User;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/common/android/PackageName;
    .end annotation
.end field

.field public final d:Landroid/content/pm/PackageManager;

.field public final e:Landroid/content/pm/PackageInfo;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/facebook/user/model/User;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInfo;)V
    .locals 0
    .param p2    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2499863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2499864
    iput-object p1, p0, LX/Hm2;->a:Landroid/content/Context;

    .line 2499865
    iput-object p3, p0, LX/Hm2;->c:Ljava/lang/String;

    .line 2499866
    iput-object p4, p0, LX/Hm2;->d:Landroid/content/pm/PackageManager;

    .line 2499867
    iput-object p5, p0, LX/Hm2;->e:Landroid/content/pm/PackageInfo;

    .line 2499868
    iput-object p2, p0, LX/Hm2;->b:Lcom/facebook/user/model/User;

    .line 2499869
    return-void
.end method

.method public static b(LX/0QB;)LX/Hm2;
    .locals 6

    .prologue
    .line 2499876
    new-instance v0, LX/Hm2;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-static {p0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0WE;->a(LX/0QB;)Landroid/content/pm/PackageInfo;

    move-result-object v5

    check-cast v5, Landroid/content/pm/PackageInfo;

    invoke-direct/range {v0 .. v5}, LX/Hm2;-><init>(Landroid/content/Context;Lcom/facebook/user/model/User;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInfo;)V

    .line 2499877
    return-object v0
.end method

.method private d()Lcom/facebook/beam/protocol/BeamDeviceInfo;
    .locals 15

    .prologue
    .line 2499878
    new-instance v0, Lcom/facebook/beam/protocol/BeamDeviceInfo;

    .line 2499879
    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    move-object v1, v1

    .line 2499880
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    move-object v2, v2

    .line 2499881
    iget-object v3, p0, LX/Hm2;->a:Landroid/content/Context;

    invoke-static {v3}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, LX/Hm2;->a:Landroid/content/Context;

    invoke-static {v4}, LX/0TM;->a(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x9

    if-lt v5, v6, :cond_0

    iget-object v5, p0, LX/Hm2;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v6

    :goto_0
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {}, LX/0TM;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {}, LX/0TM;->b()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 2499882
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x15

    if-lt v8, v9, :cond_1

    .line 2499883
    sget-object v8, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    .line 2499884
    :goto_1
    move-object v8, v8

    .line 2499885
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v10, p0, LX/Hm2;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    iget v10, v10, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    iget-object v11, p0, LX/Hm2;->a:Landroid/content/Context;

    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 2499886
    :try_start_0
    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string p0, "install_non_market_apps"

    invoke-static {v14, p0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v14

    .line 2499887
    if-ne v14, v12, :cond_2

    :goto_2
    move v11, v12

    .line 2499888
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, Lcom/facebook/beam/protocol/BeamDeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Boolean;)V

    return-object v0

    :cond_0
    const-wide/16 v6, 0x0

    goto :goto_0

    :cond_1
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    sget-object v10, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    goto :goto_1

    .line 2499889
    :catch_0
    move v12, v13

    goto :goto_2

    :cond_2
    move v12, v13

    .line 2499890
    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/facebook/beam/protocol/BeamPreflightInfo;
    .locals 14

    .prologue
    .line 2499870
    new-instance v0, Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2499871
    new-instance v4, Lcom/facebook/beam/protocol/BeamPackageInfo;

    iget-object v5, p0, LX/Hm2;->c:Ljava/lang/String;

    iget-object v6, p0, LX/Hm2;->e:Landroid/content/pm/PackageInfo;

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iget-object v7, p0, LX/Hm2;->e:Landroid/content/pm/PackageInfo;

    iget v7, v7, Landroid/content/pm/PackageInfo;->versionCode:I

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v9, 0x5

    if-lt v8, v9, :cond_0

    iget-object v8, p0, LX/Hm2;->d:Landroid/content/pm/PackageManager;

    iget-object v9, p0, LX/Hm2;->c:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_0
    new-instance v9, Ljava/io/File;

    iget-object v10, p0, LX/Hm2;->e:Landroid/content/pm/PackageInfo;

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v10, p0, LX/Hm2;->e:Landroid/content/pm/PackageInfo;

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v10, v10, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    iget-object v11, p0, LX/Hm2;->e:Landroid/content/pm/PackageInfo;

    iget-object v11, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v11, v11, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    iget-object v12, p0, LX/Hm2;->e:Landroid/content/pm/PackageInfo;

    iget-object v12, v12, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v12, v12, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    iget-object v13, p0, LX/Hm2;->e:Landroid/content/pm/PackageInfo;

    iget-object v13, v13, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v13, v13, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    invoke-direct/range {v4 .. v13}, Lcom/facebook/beam/protocol/BeamPackageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;IIII)V

    move-object v1, v4

    .line 2499872
    new-instance v2, Lcom/facebook/beam/protocol/BeamUserInfo;

    iget-object v3, p0, LX/Hm2;->b:Lcom/facebook/user/model/User;

    .line 2499873
    iget-object v4, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2499874
    iget-object v4, p0, LX/Hm2;->b:Lcom/facebook/user/model/User;

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/beam/protocol/BeamUserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v2

    .line 2499875
    invoke-direct {p0}, LX/Hm2;->d()Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/beam/protocol/BeamPreflightInfo;-><init>(Lcom/facebook/beam/protocol/BeamPackageInfo;Lcom/facebook/beam/protocol/BeamUserInfo;Lcom/facebook/beam/protocol/BeamDeviceInfo;)V

    return-object v0

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method
