.class public LX/JM7;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "Ljava/lang/Object;",
        "LX/JM3;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JM7;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2682866
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2682867
    return-void
.end method

.method public static a(LX/0QB;)LX/JM7;
    .locals 3

    .prologue
    .line 2682868
    sget-object v0, LX/JM7;->a:LX/JM7;

    if-nez v0, :cond_1

    .line 2682869
    const-class v1, LX/JM7;

    monitor-enter v1

    .line 2682870
    :try_start_0
    sget-object v0, LX/JM7;->a:LX/JM7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2682871
    if-eqz v2, :cond_0

    .line 2682872
    :try_start_1
    new-instance v0, LX/JM7;

    invoke-direct {v0}, LX/JM7;-><init>()V

    .line 2682873
    move-object v0, v0

    .line 2682874
    sput-object v0, LX/JM7;->a:LX/JM7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2682875
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2682876
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2682877
    :cond_1
    sget-object v0, LX/JM7;->a:LX/JM7;

    return-object v0

    .line 2682878
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2682879
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
