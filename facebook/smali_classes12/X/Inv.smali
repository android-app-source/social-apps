.class public final LX/Inv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ijx;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 0

    .prologue
    .line 2611121
    iput-object p1, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2611122
    iget-object v0, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->b:LX/03V;

    const-string v1, "EnterPaymentValueFragment"

    const-string v2, "A card already verified event received when adding a new card"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2611123
    return-void
.end method

.method public final a(Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/payments/auth/model/NuxFollowUpAction;)V
    .locals 3

    .prologue
    .line 2611124
    iget-object v0, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611125
    iput-object p2, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 2611126
    iget-object v0, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(LX/0am;)V

    .line 2611127
    iget-object v0, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2611128
    iget-object v0, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->p:LX/Inc;

    iget-object v1, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611129
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v2

    .line 2611130
    iget-object v2, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v2, v2, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/Inc;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611131
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2611132
    iget-object v0, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2611133
    iget-object v0, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->p:LX/Inc;

    iget-object v1, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611134
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v2

    .line 2611135
    iget-object v2, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v2, v2, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/Inc;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611136
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2611137
    iget-object v0, p0, LX/Inv;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611138
    return-void
.end method
