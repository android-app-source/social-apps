.class public LX/IQ0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0kx;

.field private final b:LX/0ad;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0kx;LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2574899
    iput-object p1, p0, LX/IQ0;->a:LX/0kx;

    .line 2574900
    iput-object p2, p0, LX/IQ0;->b:LX/0ad;

    .line 2574901
    iput-object p3, p0, LX/IQ0;->c:LX/0Uh;

    .line 2574902
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2574875
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2574876
    :goto_0
    const-string v1, "group_view_referrer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2574877
    iget-object v1, p0, LX/IQ0;->a:LX/0kx;

    invoke-virtual {v1}, LX/0kx;->c()LX/148;

    move-result-object v1

    .line 2574878
    if-eqz v1, :cond_2

    .line 2574879
    iget-object v2, v1, LX/148;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2574880
    if-eqz v2, :cond_2

    .line 2574881
    const-string v2, "group_view_referrer"

    .line 2574882
    iget-object v3, v1, LX/148;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2574883
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2574884
    :cond_0
    :goto_1
    iget-object v1, p0, LX/IQ0;->b:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short v3, LX/9Lj;->c:S

    invoke-interface {v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2574885
    new-instance v1, Lcom/facebook/groups/feed/ui/GroupTabbedViewFragment;

    invoke-direct {v1}, Lcom/facebook/groups/feed/ui/GroupTabbedViewFragment;-><init>()V

    .line 2574886
    const-string v2, "is_tabbed_mall"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2574887
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    move-object v0, v1

    .line 2574888
    :goto_2
    return-object v0

    .line 2574889
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 2574890
    :cond_2
    const-string v1, "group_view_referrer"

    const-string v2, "unknown"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2574891
    :cond_3
    iget-object v1, p0, LX/IQ0;->c:LX/0Uh;

    const/16 v2, 0x3ca

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2574892
    new-instance v1, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-direct {v1}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;-><init>()V

    .line 2574893
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    move-object v0, v1

    .line 2574894
    goto :goto_2

    .line 2574895
    :cond_4
    new-instance v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-direct {v1}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;-><init>()V

    .line 2574896
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    move-object v0, v1

    .line 2574897
    goto :goto_2
.end method
