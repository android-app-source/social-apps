.class public LX/IFS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IFR;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public e:LX/IFQ;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/IFQ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2554144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554145
    iput-object p1, p0, LX/IFS;->a:Ljava/lang/String;

    .line 2554146
    iput-object p2, p0, LX/IFS;->b:Ljava/lang/String;

    .line 2554147
    iput-object p3, p0, LX/IFS;->d:Ljava/lang/String;

    .line 2554148
    iput-object p4, p0, LX/IFS;->e:LX/IFQ;

    .line 2554149
    iput-object p5, p0, LX/IFS;->c:Ljava/lang/String;

    .line 2554150
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2554151
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554152
    iget-object v0, p0, LX/IFS;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554153
    sget-object v0, LX/IFP;->a:[I

    iget-object v1, p0, LX/IFS;->e:LX/IFQ;

    invoke-virtual {v1}, LX/IFQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2554154
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown invite state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/IFS;->e:LX/IFQ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2554155
    :pswitch_0
    const-string v0, ""

    .line 2554156
    :goto_0
    return-object v0

    .line 2554157
    :pswitch_1
    const v0, 0x7f08385b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2554158
    :pswitch_2
    const v0, 0x7f08385a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2554159
    :pswitch_3
    const v0, 0x7f08385c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/3OL;)V
    .locals 0

    .prologue
    .line 2554160
    return-void
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 2554161
    return-void
.end method

.method public final b(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2554162
    sget-object v0, LX/IFP;->a:[I

    iget-object v1, p0, LX/IFS;->e:LX/IFQ;

    invoke-virtual {v1}, LX/IFQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2554163
    const v0, 0x7f020b79

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2554164
    :pswitch_0
    const v0, 0x7f020b78

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554165
    iget-object v0, p0, LX/IFS;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2554166
    const/4 v0, 0x0

    .line 2554167
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IFS;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2554168
    const v0, 0x7f083846

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/IFS;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0, p1}, LX/IFS;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554142
    iget-object v0, p0, LX/IFS;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2554143
    return-object v0
.end method

.method public final c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2554121
    sget-object v0, LX/IFP;->a:[I

    iget-object v1, p0, LX/IFS;->e:LX/IFQ;

    invoke-virtual {v1}, LX/IFQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2554122
    const v0, 0x7f080032

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2554123
    :pswitch_0
    const v0, 0x7f083869

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554141
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2554138
    const v0, 0x7f083847

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 2554139
    iget-object v3, p0, LX/IFS;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2554140
    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554137
    const-string v0, "invite"

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2554136
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2554135
    const/4 v0, 0x0

    return v0
.end method

.method public final h()LX/3OL;
    .locals 1

    .prologue
    .line 2554134
    sget-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 2554131
    sget-object v0, LX/IFP;->a:[I

    iget-object v1, p0, LX/IFS;->e:LX/IFQ;

    invoke-virtual {v1}, LX/IFQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2554132
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2554133
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2554128
    sget-object v0, LX/IFP;->a:[I

    iget-object v1, p0, LX/IFS;->e:LX/IFQ;

    invoke-virtual {v1}, LX/IFQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2554129
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2554130
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final k()LX/6VG;
    .locals 2

    .prologue
    .line 2554125
    sget-object v0, LX/IFP;->a:[I

    iget-object v1, p0, LX/IFS;->e:LX/IFQ;

    invoke-virtual {v1}, LX/IFQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2554126
    sget-object v0, LX/6VG;->GRAY:LX/6VG;

    :goto_0
    return-object v0

    .line 2554127
    :pswitch_0
    sget-object v0, LX/6VG;->SPECIAL:LX/6VG;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554124
    iget-object v0, p0, LX/IFS;->c:Ljava/lang/String;

    return-object v0
.end method
