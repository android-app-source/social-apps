.class public LX/JLj;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBMarketplaceQuickPerformanceLoggerModule"
.end annotation


# instance fields
.field private final a:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(LX/5pY;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681488
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2681489
    iput-object p2, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2681490
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681491
    const-string v0, "FBMarketplaceQuickPerformanceLoggerModule"

    return-object v0
.end method

.method public markerCancel(II)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681484
    if-eqz p2, :cond_0

    .line 2681485
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 2681486
    :goto_0
    return-void

    .line 2681487
    :cond_0
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto :goto_0
.end method

.method public markerEnd(III)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681492
    if-eqz p2, :cond_0

    .line 2681493
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    int-to-short v1, p3

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2681494
    :goto_0
    return-void

    .line 2681495
    :cond_0
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    int-to-short v1, p3

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method

.method public markerNote(IIILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681476
    if-eqz p2, :cond_1

    .line 2681477
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2681478
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    int-to-short v1, p3

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2681479
    :goto_0
    return-void

    .line 2681480
    :cond_0
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    int-to-short v3, p3

    move v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IISLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2681481
    :cond_1
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2681482
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    int-to-short v1, p3

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    goto :goto_0

    .line 2681483
    :cond_2
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    int-to-short v1, p3

    invoke-interface {v0, p1, v1, p4, p5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public markerStart(IILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681464
    if-eqz p2, :cond_1

    .line 2681465
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2681466
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2681467
    :goto_0
    return-void

    .line 2681468
    :cond_0
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2681469
    :cond_1
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2681470
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto :goto_0

    .line 2681471
    :cond_2
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p3, p4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public markerTag(IILjava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681472
    if-eqz p2, :cond_0

    .line 2681473
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2681474
    :goto_0
    return-void

    .line 2681475
    :cond_0
    iget-object v0, p0, LX/JLj;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_0
.end method
