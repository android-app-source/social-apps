.class public LX/HnM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.lang.System.currentTimeMillis"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/net/wifi/WifiManager;

.field public final d:Landroid/net/wifi/WifiConfiguration;

.field private final e:Ljava/lang/Object;

.field private final f:LX/0Yd;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2501708
    const-class v0, LX/HnM;

    sput-object v0, LX/HnM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;Landroid/net/wifi/WifiConfiguration;)V
    .locals 4

    .prologue
    .line 2501722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501723
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/HnM;->e:Ljava/lang/Object;

    .line 2501724
    new-instance v0, LX/0Yd;

    const-string v1, "android.net.wifi.STATE_CHANGE"

    new-instance v2, LX/HnL;

    invoke-direct {v2, p0}, LX/HnL;-><init>(LX/HnM;)V

    invoke-direct {v0, v1, v2}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    iput-object v0, p0, LX/HnM;->f:LX/0Yd;

    .line 2501725
    iput-object p2, p0, LX/HnM;->c:Landroid/net/wifi/WifiManager;

    .line 2501726
    iput-object p1, p0, LX/HnM;->b:Landroid/content/Context;

    .line 2501727
    iput-object p3, p0, LX/HnM;->d:Landroid/net/wifi/WifiConfiguration;

    .line 2501728
    iget-object v0, p0, LX/HnM;->b:Landroid/content/Context;

    iget-object v1, p0, LX/HnM;->f:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2501729
    return-void
.end method

.method public static b(LX/HnM;)V
    .locals 3

    .prologue
    .line 2501719
    iget-object v1, p0, LX/HnM;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 2501720
    :try_start_0
    iget-object v0, p0, LX/HnM;->e:Ljava/lang/Object;

    const v2, -0xa7f5362

    invoke-static {v0, v2}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 2501721
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static c(LX/HnM;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2501730
    iget-object v0, p0, LX/HnM;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 2501731
    if-nez v0, :cond_0

    move v0, v1

    .line 2501732
    :goto_0
    return v0

    .line 2501733
    :cond_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    .line 2501734
    if-nez v0, :cond_1

    move v0, v1

    .line 2501735
    goto :goto_0

    .line 2501736
    :cond_1
    invoke-static {v0}, LX/HnN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2501737
    iget-object v3, p0, LX/HnM;->d:Landroid/net/wifi/WifiConfiguration;

    iget-object v3, v3, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/HnM;->d:Landroid/net/wifi/WifiConfiguration;

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2501738
    goto :goto_0

    .line 2501739
    :cond_2
    iget-object v0, p0, LX/HnM;->b:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2501740
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 2501741
    if-nez v0, :cond_3

    move v0, v1

    .line 2501742
    goto :goto_0

    .line 2501743
    :cond_3
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2501744
    goto :goto_0

    .line 2501745
    :cond_4
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2501717
    iget-object v0, p0, LX/HnM;->b:Landroid/content/Context;

    iget-object v1, p0, LX/HnM;->f:LX/0Yd;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2501718
    return-void
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    .line 2501709
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2501710
    add-long v2, v0, p1

    .line 2501711
    :goto_0
    invoke-static {p0}, LX/HnM;->c(LX/HnM;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2501712
    :try_start_0
    iget-object v1, p0, LX/HnM;->e:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2501713
    :try_start_1
    iget-object v0, p0, LX/HnM;->e:Ljava/lang/Object;

    const-wide/16 v4, 0xbb8

    const v6, -0x2403eb32

    invoke-static {v0, v4, v5, v6}, LX/02L;->a(Ljava/lang/Object;JI)V

    .line 2501714
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2501715
    :catch_0
    goto :goto_0

    .line 2501716
    :cond_0
    invoke-static {p0}, LX/HnM;->c(LX/HnM;)Z

    move-result v0

    return v0
.end method
