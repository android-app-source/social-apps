.class public final LX/JNd;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JNe;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

.field public e:LX/3mj;

.field public final synthetic f:LX/JNe;


# direct methods
.method public constructor <init>(LX/JNe;)V
    .locals 1

    .prologue
    .line 2686180
    iput-object p1, p0, LX/JNd;->f:LX/JNe;

    .line 2686181
    move-object v0, p1

    .line 2686182
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2686183
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2686184
    const-string v0, "ConnectWithFacebookBottomSectionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2686185
    if-ne p0, p1, :cond_1

    .line 2686186
    :cond_0
    :goto_0
    return v0

    .line 2686187
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2686188
    goto :goto_0

    .line 2686189
    :cond_3
    check-cast p1, LX/JNd;

    .line 2686190
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2686191
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2686192
    if-eq v2, v3, :cond_0

    .line 2686193
    iget-object v2, p0, LX/JNd;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JNd;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget-object v3, p1, LX/JNd;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2686194
    goto :goto_0

    .line 2686195
    :cond_5
    iget-object v2, p1, LX/JNd;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    if-nez v2, :cond_4

    .line 2686196
    :cond_6
    iget-object v2, p0, LX/JNd;->b:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JNd;->b:LX/1Pb;

    iget-object v3, p1, LX/JNd;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2686197
    goto :goto_0

    .line 2686198
    :cond_8
    iget-object v2, p1, LX/JNd;->b:LX/1Pb;

    if-nez v2, :cond_7

    .line 2686199
    :cond_9
    iget v2, p0, LX/JNd;->c:I

    iget v3, p1, LX/JNd;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2686200
    goto :goto_0

    .line 2686201
    :cond_a
    iget-object v2, p0, LX/JNd;->d:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/JNd;->d:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object v3, p1, LX/JNd;->d:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2686202
    goto :goto_0

    .line 2686203
    :cond_c
    iget-object v2, p1, LX/JNd;->d:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-nez v2, :cond_b

    .line 2686204
    :cond_d
    iget-object v2, p0, LX/JNd;->e:LX/3mj;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/JNd;->e:LX/3mj;

    iget-object v3, p1, LX/JNd;->e:LX/3mj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2686205
    goto :goto_0

    .line 2686206
    :cond_e
    iget-object v2, p1, LX/JNd;->e:LX/3mj;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
