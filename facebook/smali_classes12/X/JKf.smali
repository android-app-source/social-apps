.class public LX/JKf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile i:LX/JKf;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Sg;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/98j;

.field public h:LX/JKd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2680424
    const-class v0, LX/JKf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/JKf;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Or;LX/0Sg;LX/0Ot;LX/98j;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/98j;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2680435
    iput-object p1, p0, LX/JKf;->b:LX/0Or;

    .line 2680436
    iput-object p2, p0, LX/JKf;->c:LX/0Ot;

    .line 2680437
    iput-object p3, p0, LX/JKf;->d:LX/0Or;

    .line 2680438
    iput-object p4, p0, LX/JKf;->e:LX/0Sg;

    .line 2680439
    iput-object p5, p0, LX/JKf;->f:LX/0Ot;

    .line 2680440
    iput-object p6, p0, LX/JKf;->g:LX/98j;

    .line 2680441
    return-void
.end method

.method public static a(LX/0QB;)LX/JKf;
    .locals 10

    .prologue
    .line 2680442
    sget-object v0, LX/JKf;->i:LX/JKf;

    if-nez v0, :cond_1

    .line 2680443
    const-class v1, LX/JKf;

    monitor-enter v1

    .line 2680444
    :try_start_0
    sget-object v0, LX/JKf;->i:LX/JKf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2680445
    if-eqz v2, :cond_0

    .line 2680446
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2680447
    new-instance v3, LX/JKf;

    const/16 v4, 0x148e

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x53c

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x15e7

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v7

    check-cast v7, LX/0Sg;

    const/16 v8, 0x245

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/98j;->a(LX/0QB;)LX/98j;

    move-result-object v9

    check-cast v9, LX/98j;

    invoke-direct/range {v3 .. v9}, LX/JKf;-><init>(LX/0Or;LX/0Ot;LX/0Or;LX/0Sg;LX/0Ot;LX/98j;)V

    .line 2680448
    move-object v0, v3

    .line 2680449
    sput-object v0, LX/JKf;->i:LX/JKf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2680450
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2680451
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2680452
    :cond_1
    sget-object v0, LX/JKf;->i:LX/JKf;

    return-object v0

    .line 2680453
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2680454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 2680425
    iget-object v0, p0, LX/JKf;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2680426
    :cond_0
    :goto_0
    return-void

    .line 2680427
    :cond_1
    iget-object v0, p0, LX/JKf;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33u;

    invoke-virtual {v0}, LX/33u;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2680428
    new-instance v0, LX/JKd;

    invoke-direct {v0}, LX/JKd;-><init>()V

    iput-object v0, p0, LX/JKf;->h:LX/JKd;

    .line 2680429
    iget-object v0, p0, LX/JKf;->g:LX/98j;

    iget-object v1, p0, LX/JKf;->h:LX/JKd;

    invoke-virtual {v0, v1}, LX/98j;->a(LX/0o1;)V

    .line 2680430
    iget-object v0, p0, LX/JKf;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33u;

    invoke-virtual {v0}, LX/33u;->c()LX/33y;

    move-result-object v0

    .line 2680431
    invoke-virtual {v0}, LX/33y;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2680432
    new-instance v1, LX/JKe;

    invoke-direct {v1, p0}, LX/JKe;-><init>(LX/JKf;)V

    invoke-virtual {v0, v1}, LX/33y;->a(LX/9mm;)V

    .line 2680433
    invoke-virtual {v0}, LX/33y;->c()V

    goto :goto_0
.end method
