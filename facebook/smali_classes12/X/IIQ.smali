.class public final LX/IIQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/IIm;

.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

.field public final c:LX/IIm;

.field public d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V
    .locals 1

    .prologue
    .line 2561800
    iput-object p1, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iput-object p2, p0, LX/IIQ;->a:LX/IIm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2561801
    iget-object v0, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    iput-object v0, p0, LX/IIQ;->c:LX/IIm;

    .line 2561802
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IIQ;->d:Z

    return-void
.end method

.method public static a(LX/IIQ;LX/0am;LX/0am;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryInterfaces$FriendsNearbyLocationSharingFields;",
            ">;",
            "LX/0am",
            "<",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryInterfaces$FriendsNearbyLocationSharingQuery$PrivacySettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2561803
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2561804
    :goto_0
    return-void

    .line 2561805
    :cond_0
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;

    .line 2561806
    iget-object v1, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->mG_()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    move-result-object v2

    .line 2561807
    iput-object v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ao:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    .line 2561808
    if-nez v0, :cond_7

    const-wide/16 v7, 0x0

    :goto_1
    move-wide v2, v7

    .line 2561809
    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 2561810
    iget-object v1, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    iget-object v4, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v4, v4, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->I:LX/GUm;

    invoke-virtual {v4, v2, v3}, LX/GUm;->a(J)J

    move-result-wide v4

    .line 2561811
    iput-wide v4, v1, LX/IFX;->A:J

    .line 2561812
    iget-object v4, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    new-instance v5, LX/IG1;

    iget-object v1, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v6, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->m:LX/IFO;

    invoke-virtual {p2}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;

    invoke-direct {v5, v6, v0, v1}, LX/IG1;-><init>(LX/IFO;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;)V

    .line 2561813
    iput-object v5, v4, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->l:LX/IG1;

    .line 2561814
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2561815
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->c()Z

    move-result v4

    if-eqz v4, :cond_3

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    if-eqz v1, :cond_3

    .line 2561816
    iget-object v0, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->g:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    .line 2561817
    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v2, :cond_1

    .line 2561818
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IIQ;->d:Z

    .line 2561819
    :goto_3
    iget-object v0, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2561820
    new-instance v2, Landroid/content/Intent;

    iget-object v3, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->z:LX/0aU;

    const-string v4, "BACKGROUND_LOCATION_REPORTING_SETTINGS_REQUEST_REFRESH_ACTION"

    invoke-virtual {v3, v4}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2561821
    const-string v3, "expected_location_history_setting"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2561822
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2561823
    goto :goto_0

    .line 2561824
    :cond_1
    sget-object v2, LX/0yG;->PERMISSION_DENIED:LX/0yG;

    if-ne v0, v2, :cond_2

    invoke-static {v0}, LX/1rv;->b(LX/0yG;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2561825
    iget-object v0, p0, LX/IIQ;->a:LX/IIm;

    invoke-virtual {v0}, LX/IIm;->a()V

    goto :goto_3

    .line 2561826
    :cond_2
    iget-object v0, p0, LX/IIQ;->a:LX/IIm;

    invoke-virtual {v0}, LX/IIm;->b()V

    goto :goto_3

    .line 2561827
    :cond_3
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->k()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->e()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {v0}, LX/IFO;->f(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)I

    move-result v3

    if-lez v3, :cond_9

    :cond_4
    const/4 v3, 0x1

    :goto_4
    move v2, v3

    .line 2561828
    if-eqz v2, :cond_5

    .line 2561829
    iget-object v0, p0, LX/IIQ;->a:LX/IIm;

    invoke-virtual {v0}, LX/IIm;->j()V

    goto :goto_3

    .line 2561830
    :cond_5
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->c()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->j()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_a

    const/4 v7, 0x1

    :goto_5
    move v0, v7

    .line 2561831
    if-eqz v0, :cond_6

    .line 2561832
    iget-object v0, p0, LX/IIQ;->a:LX/IIm;

    invoke-virtual {v0}, LX/IIm;->m()V

    goto :goto_3

    .line 2561833
    :cond_6
    iget-object v0, p0, LX/IIQ;->a:LX/IIm;

    invoke-virtual {v0}, LX/IIm;->l()V

    goto :goto_3

    :cond_7
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->j()J

    move-result-wide v7

    goto/16 :goto_1

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    :cond_a
    const/4 v7, 0x0

    goto :goto_5
.end method

.method public static b(LX/IIQ;LX/0am;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryInterfaces$FriendsNearbyContactsTab;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2561834
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2561835
    :goto_0
    return-void

    .line 2561836
    :cond_0
    iget-object v1, p0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2561837
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;

    const/4 p0, 0x0

    .line 2561838
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel;

    move-result-object v2

    .line 2561839
    if-nez v2, :cond_1

    move-object v2, p0

    .line 2561840
    :goto_1
    move-object v0, v2

    .line 2561841
    iput-object v0, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->an:Ljava/lang/String;

    .line 2561842
    goto :goto_0

    .line 2561843
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel;->a()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    move-object v2, p0

    .line 2561844
    goto :goto_1

    .line 2561845
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel;->a()LX/0Px;

    move-result-object v2

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;

    .line 2561846
    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    move-result-object v2

    .line 2561847
    if-nez v2, :cond_3

    move-object v2, p0

    .line 2561848
    goto :goto_1

    .line 2561849
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;->b()LX/0us;

    move-result-object v2

    .line 2561850
    if-nez v2, :cond_4

    move-object v2, p0

    .line 2561851
    goto :goto_1

    .line 2561852
    :cond_4
    invoke-interface {v2}, LX/0us;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
