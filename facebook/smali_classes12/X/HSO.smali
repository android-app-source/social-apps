.class public final LX/HSO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V
    .locals 0

    .prologue
    .line 2466979
    iput-object p1, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2466980
    add-int v0, p2, p3

    add-int/lit8 v1, p4, -0x3

    if-le v0, v1, :cond_0

    .line 2466981
    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->p:Z

    if-eqz v0, :cond_0

    .line 2466982
    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-static {v0, v2}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a$redex0(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;Z)V

    .line 2466983
    :cond_0
    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    move v1, v0

    .line 2466984
    :goto_0
    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->W:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->X:I

    if-eq v0, v1, :cond_2

    .line 2466985
    :cond_1
    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->P:Landroid/view/View;

    iget-object v3, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    sget-object v4, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->V:[I

    iget-object v5, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v5, v5, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->h:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->d()I

    move-result v5

    invoke-static {v0, v3, p2, v4, v5}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2466986
    iget-object v4, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2466987
    iput-boolean v0, v4, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->W:Z

    .line 2466988
    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->W:Z

    if-eqz v0, :cond_2

    .line 2466989
    iget-object v4, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E_(I)V

    .line 2466990
    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    .line 2466991
    iput v1, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->X:I

    .line 2466992
    :cond_2
    iget-object v0, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2466993
    iget-object v1, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->Q:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget v1, v1, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->S:I

    if-eq v1, v0, :cond_3

    iget-object v1, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2466994
    iget-object v1, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->Q:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v2, p2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2466995
    iget-object v1, p0, LX/HSO;->a:Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    .line 2466996
    iput v0, v1, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->S:I

    .line 2466997
    :cond_3
    return-void

    :cond_4
    move v1, v2

    .line 2466998
    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2466999
    return-void
.end method
