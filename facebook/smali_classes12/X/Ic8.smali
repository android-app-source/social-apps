.class public final LX/Ic8;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Id5;

.field public final synthetic b:LX/Ic9;


# direct methods
.method public constructor <init>(LX/Ic9;LX/Id5;)V
    .locals 0

    .prologue
    .line 2595213
    iput-object p1, p0, LX/Ic8;->b:LX/Ic9;

    iput-object p2, p0, LX/Ic8;->a:LX/Id5;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2595214
    iget-object v0, p0, LX/Ic8;->b:LX/Ic9;

    iget-object v0, v0, LX/Ic9;->a:LX/03V;

    const-string v1, "RidePaymentHelper"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595215
    iget-object v0, p0, LX/Ic8;->a:LX/Id5;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Id5;->a(Ljava/lang/String;)V

    .line 2595216
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2595217
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v4, 0x1a9dacae

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2595218
    iget-object v0, p0, LX/Ic8;->a:LX/Id5;

    if-nez v0, :cond_0

    .line 2595219
    :goto_0
    return-void

    .line 2595220
    :cond_0
    if-eqz p1, :cond_1

    .line 2595221
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595222
    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    .line 2595223
    iget-object v0, p0, LX/Ic8;->b:LX/Ic9;

    iget-object v0, v0, LX/Ic9;->a:LX/03V;

    const-string v1, "RidePaymentHelper"

    const-string v2, "GraphQL return invalid results"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595224
    iget-object v0, p0, LX/Ic8;->a:LX/Id5;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Id5;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2595225
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595226
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2595227
    if-nez v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    .line 2595228
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595229
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2595230
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_2

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2595231
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595232
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2595233
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2595234
    iget-object v3, p0, LX/Ic8;->a:LX/Id5;

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/Id5;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2595235
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    .line 2595236
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
