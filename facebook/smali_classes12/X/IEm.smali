.class public final LX/IEm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/Sticker;

.field public final synthetic b:LX/IEn;


# direct methods
.method public constructor <init>(LX/IEn;Lcom/facebook/stickers/model/Sticker;)V
    .locals 0

    .prologue
    .line 2553350
    iput-object p1, p0, LX/IEm;->b:LX/IEn;

    iput-object p2, p0, LX/IEm;->a:Lcom/facebook/stickers/model/Sticker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x1147e07e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2553351
    iget-object v1, p0, LX/IEm;->b:LX/IEn;

    iget-object v1, v1, LX/IEn;->l:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->b:LX/IEt;

    iget-object v2, p0, LX/IEm;->a:Lcom/facebook/stickers/model/Sticker;

    .line 2553352
    iget-object v4, v1, LX/IEt;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    .line 2553353
    iget-object v5, v4, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->d:LX/IEx;

    iget-object v6, v4, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->j:Ljava/lang/String;

    iget-object v7, v2, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    .line 2553354
    iget-object p0, v5, LX/IEx;->a:LX/0Zb;

    const-string p1, "sticker_selected"

    invoke-static {v5, p1}, LX/IEx;->c(LX/IEx;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string v1, "composer_session_id"

    invoke-virtual {p1, v1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string v1, "sticker_id"

    invoke-virtual {p1, v1, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2553355
    iget-boolean v5, v4, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->i:Z

    if-eqz v5, :cond_1

    .line 2553356
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v6, "extra_birthday_sticker_data"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    .line 2553357
    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v6

    const/4 v7, -0x1

    invoke-virtual {v6, v7, v5}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2553358
    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    .line 2553359
    :cond_0
    :goto_0
    const v1, 0x29c09aae

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2553360
    :cond_1
    iget-object v5, v4, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-eqz v5, :cond_0

    .line 2553361
    iget-object v5, v4, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    invoke-static {v2}, LX/5Rb;->a(Lcom/facebook/stickers/model/Sticker;)Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialStickerData(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    .line 2553362
    iget-object v6, v4, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->b:LX/1Kf;

    iget-object v7, v4, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->j:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    const/4 p0, 0x1

    invoke-interface {v6, v7, v5, p0, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method
