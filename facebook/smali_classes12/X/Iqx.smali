.class public final LX/Iqx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8CF;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V
    .locals 0

    .prologue
    .line 2617349
    iput-object p1, p0, LX/Iqx;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 2617350
    iget-object v0, p0, LX/Iqx;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->M:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setColour(I)V

    .line 2617351
    iget-object v0, p0, LX/Iqx;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->M:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->a()V

    .line 2617352
    iget-object v0, p0, LX/Iqx;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawingview/DrawingView;->setColour(I)V

    .line 2617353
    return-void
.end method

.method public final a(IFFF)V
    .locals 1

    .prologue
    .line 2617354
    iget-object v0, p0, LX/Iqx;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->M:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(IFFF)V

    .line 2617355
    iget-object v0, p0, LX/Iqx;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p4}, Lcom/facebook/drawingview/DrawingView;->setStrokeWidth(F)V

    .line 2617356
    iget-object v0, p0, LX/Iqx;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawingview/DrawingView;->setColour(I)V

    .line 2617357
    return-void
.end method
