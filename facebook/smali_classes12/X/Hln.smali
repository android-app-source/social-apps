.class public LX/Hln;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2499309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2499310
    return-void
.end method

.method public static c(Landroid/content/Context;J)V
    .locals 5

    .prologue
    .line 2499311
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2499312
    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p1

    invoke-static {p0, p1, p2}, LX/Hln;->d(Landroid/content/Context;J)Landroid/app/PendingIntent;

    move-result-object v4

    .line 2499313
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x17

    if-lt p0, p1, :cond_0

    .line 2499314
    invoke-static {v0, v1, v2, v3, v4}, LX/43l;->a(Landroid/app/AlarmManager;IJLandroid/app/PendingIntent;)V

    .line 2499315
    :goto_0
    return-void

    .line 2499316
    :cond_0
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;J)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 2499317
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "action_collect_sensor_data"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_trigger_period_ms"

    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
