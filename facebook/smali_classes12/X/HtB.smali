.class public LX/HtB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/HsK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesRemovedUrls;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j5;",
        ":",
        "LX/0j6;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "PluginData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAreAttachmentsReadOnlyGetter;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicSetters$SetsRemovedURLs",
        "<TMutation;>;:",
        "Lcom/facebook/ipc/composer/intent/ComposerShareParams$SetsShareParams",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0in",
        "<TPluginData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/HsK;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public final b:LX/Hsi;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

.field private final e:LX/Hsl;

.field private final f:LX/03V;

.field public final g:LX/0gd;

.field public final h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final i:LX/0ad;

.field private final j:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/Hsu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2515606
    const-class v0, LX/HtB;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/HtB;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/Hsi;Landroid/content/res/Resources;Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;LX/Hsl;LX/03V;LX/0ad;LX/0gd;LX/1Ck;LX/0il;)V
    .locals 1
    .param p9    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Hsi;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;",
            "LX/Hsl;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "LX/0gd;",
            "LX/1Ck;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2515593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2515594
    new-instance v0, LX/Ht8;

    invoke-direct {v0, p0}, LX/Ht8;-><init>(LX/HtB;)V

    iput-object v0, p0, LX/HtB;->k:Landroid/view/View$OnClickListener;

    .line 2515595
    new-instance v0, LX/Ht9;

    invoke-direct {v0, p0}, LX/Ht9;-><init>(LX/HtB;)V

    iput-object v0, p0, LX/HtB;->l:LX/0Ve;

    .line 2515596
    iput-object p1, p0, LX/HtB;->b:LX/Hsi;

    .line 2515597
    iput-object p2, p0, LX/HtB;->c:Landroid/content/res/Resources;

    .line 2515598
    iput-object p3, p0, LX/HtB;->d:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    .line 2515599
    iput-object p4, p0, LX/HtB;->e:LX/Hsl;

    .line 2515600
    iput-object p5, p0, LX/HtB;->f:LX/03V;

    .line 2515601
    iput-object p6, p0, LX/HtB;->i:LX/0ad;

    .line 2515602
    iput-object p7, p0, LX/HtB;->g:LX/0gd;

    .line 2515603
    iput-object p8, p0, LX/HtB;->j:LX/1Ck;

    .line 2515604
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p9}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    .line 2515605
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 2515576
    iget-object v0, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515577
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    move v0, v0

    .line 2515578
    if-nez v0, :cond_0

    .line 2515579
    const/4 v0, 0x0

    .line 2515580
    :goto_0
    return v0

    .line 2515581
    :cond_0
    new-instance v0, LX/HlK;

    invoke-direct {v0, p2}, LX/HlK;-><init>(Landroid/content/Context;)V

    .line 2515582
    iget-object v1, p0, LX/HtB;->d:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    .line 2515583
    invoke-static {p1, v0}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/HlK;)V

    .line 2515584
    invoke-static {p1}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2515585
    invoke-static {p1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr v2, p2

    .line 2515586
    invoke-static {p1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-static {p2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p2

    invoke-static {v1, p2}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;Landroid/net/Uri;)LX/1aZ;

    move-result-object p2

    .line 2515587
    iget-object v1, v0, LX/HlK;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2515588
    iget-object p2, v0, LX/HlK;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2515589
    :cond_1
    iget-object v1, p0, LX/HtB;->m:LX/Hsu;

    .line 2515590
    iget-object v2, v1, LX/Hsu;->a:Landroid/widget/FrameLayout;

    move-object v1, v2

    .line 2515591
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2515592
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a$redex0(LX/HtB;Ljava/lang/Throwable;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 5

    .prologue
    .line 2515566
    iget-object v0, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515567
    sget-object v1, LX/Hsi;->b:Ljava/lang/IllegalStateException;

    if-eq p1, v1, :cond_0

    .line 2515568
    iget-object v1, p0, LX/HtB;->f:LX/03V;

    const-string v2, "composer_feed_attachment_error_fallback"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sessionId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 2515569
    iput-object p1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2515570
    move-object v0, v0

    .line 2515571
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 2515572
    :cond_0
    new-instance v0, LX/39x;

    invoke-direct {v0}, LX/39x;-><init>()V

    iget-object v1, p0, LX/HtB;->c:Landroid/content/res/Resources;

    const v2, 0x7f08140e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2515573
    iput-object v1, v0, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2515574
    move-object v0, v0

    .line 2515575
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    return-object v0
.end method

.method public static b$redex0(LX/HtB;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 9

    .prologue
    .line 2515514
    iget-object v0, p0, LX/HtB;->m:LX/Hsu;

    if-nez v0, :cond_1

    .line 2515515
    :cond_0
    :goto_0
    return-void

    .line 2515516
    :cond_1
    iget-object v0, p0, LX/HtB;->m:LX/Hsu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Hsu;->setLoadingIndicatorVisibility(Z)V

    .line 2515517
    iget-object v0, p0, LX/HtB;->m:LX/Hsu;

    invoke-virtual {v0}, LX/Hsu;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2515518
    invoke-direct {p0, p1, v2}, LX/HtB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2515519
    iget-object v0, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515520
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 2515521
    const/4 v1, 0x0

    .line 2515522
    iget-object v3, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-eqz v3, :cond_2

    .line 2515523
    new-instance v1, LX/HtA;

    invoke-direct {v1, p0}, LX/HtA;-><init>(LX/HtB;)V

    .line 2515524
    :cond_2
    new-instance v3, LX/1De;

    invoke-direct {v3, v2}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2515525
    iget-object v4, p0, LX/HtB;->e:LX/Hsl;

    new-instance v5, LX/1De;

    invoke-direct {v5, v2}, LX/1De;-><init>(Landroid/content/Context;)V

    const/4 v6, 0x0

    .line 2515526
    new-instance v7, LX/Hsk;

    invoke-direct {v7, v4}, LX/Hsk;-><init>(LX/Hsl;)V

    .line 2515527
    sget-object v8, LX/Hsl;->a:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Hsj;

    .line 2515528
    if-nez v8, :cond_3

    .line 2515529
    new-instance v8, LX/Hsj;

    invoke-direct {v8}, LX/Hsj;-><init>()V

    .line 2515530
    :cond_3
    invoke-static {v8, v5, v6, v6, v7}, LX/Hsj;->a$redex0(LX/Hsj;LX/1De;IILX/Hsk;)V

    .line 2515531
    move-object v7, v8

    .line 2515532
    move-object v6, v7

    .line 2515533
    move-object v4, v6

    .line 2515534
    iget-object v5, v4, LX/Hsj;->a:LX/Hsk;

    iput-object p1, v5, LX/Hsk;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2515535
    iget-object v5, v4, LX/Hsj;->d:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2515536
    move-object v4, v4

    .line 2515537
    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 2515538
    iget-object v5, v4, LX/Hsj;->a:LX/Hsk;

    iput-object v0, v5, LX/Hsk;->b:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 2515539
    move-object v0, v4

    .line 2515540
    iget-object v4, v0, LX/Hsj;->a:LX/Hsk;

    iput-object v1, v4, LX/Hsk;->c:LX/HtA;

    .line 2515541
    move-object v0, v0

    .line 2515542
    invoke-static {v3, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v1

    .line 2515543
    new-instance v0, Lcom/facebook/components/ComponentView;

    invoke-direct {v0, v2}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    .line 2515544
    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2515545
    iget-object v1, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 2515546
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2515547
    if-eqz v1, :cond_4

    .line 2515548
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2515549
    iget-object v3, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    .line 2515550
    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v3, LX/0j5;

    invoke-interface {v3}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    iget-object v4, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    .line 2515551
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f0310e6

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 2515552
    const v3, 0x7f0d281f

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;

    .line 2515553
    invoke-virtual {v3, v4}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2515554
    move-object v3, v5

    .line 2515555
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2515556
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2515557
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2515558
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2515559
    move-object v0, v1

    .line 2515560
    :cond_4
    iget-object v1, p0, LX/HtB;->m:LX/Hsu;

    .line 2515561
    iget-object v2, v1, LX/Hsu;->a:Landroid/widget/FrameLayout;

    move-object v1, v2

    .line 2515562
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2515563
    iget-object v1, p0, LX/HtB;->m:LX/Hsu;

    .line 2515564
    iget-object v2, v1, LX/Hsu;->a:Landroid/widget/FrameLayout;

    move-object v1, v2

    .line 2515565
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2515607
    iget-object v0, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515608
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 2515609
    iget-object v1, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v1, :cond_0

    .line 2515610
    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p0, v0}, LX/HtB;->b$redex0(LX/HtB;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2515611
    :goto_0
    return-void

    .line 2515612
    :cond_0
    iget-object v0, p0, LX/HtB;->j:LX/1Ck;

    const-string v1, "fetchAttachmentPreview"

    new-instance v2, Lcom/facebook/composer/feedattachment/ShareComposerAttachment$3;

    invoke-direct {v2, p0}, Lcom/facebook/composer/feedattachment/ShareComposerAttachment$3;-><init>(LX/HtB;)V

    iget-object v3, p0, LX/HtB;->l:LX/0Ve;

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 2515513
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 7

    .prologue
    .line 2515488
    new-instance v0, LX/Hsu;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Hsu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/HtB;->m:LX/Hsu;

    .line 2515489
    iget-object v1, p0, LX/HtB;->m:LX/Hsu;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2515490
    iget-object v0, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v2, v0

    .line 2515491
    check-cast v2, LX/0in;

    invoke-interface {v2}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AQ9;

    .line 2515492
    iget-object v5, v2, LX/AQ9;->u:LX/ARN;

    move-object v2, v5

    .line 2515493
    if-eqz v2, :cond_2

    move-object v2, v0

    check-cast v2, LX/0in;

    invoke-interface {v2}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AQ9;

    .line 2515494
    iget-object v5, v2, LX/AQ9;->u:LX/ARN;

    move-object v2, v5

    .line 2515495
    invoke-interface {v2}, LX/ARN;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2515496
    :cond_0
    :goto_0
    move v0, v4

    .line 2515497
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, LX/Hsu;->setShowRemoveButton(Z)V

    .line 2515498
    iget-object v0, p0, LX/HtB;->m:LX/Hsu;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2515499
    iget-object v0, p0, LX/HtB;->m:LX/Hsu;

    iget-object v1, p0, LX/HtB;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/Hsu;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2515500
    invoke-direct {p0}, LX/HtB;->c()V

    .line 2515501
    return-void

    .line 2515502
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2515503
    :cond_2
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2515504
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v2, LX/0j5;

    invoke-interface {v2}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v5, LX/2rw;->USER:LX/2rw;

    if-eq v2, v5, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v5, LX/2rw;->GROUP:LX/2rw;

    if-eq v2, v5, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j6;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v2, LX/2rw;->EVENT:LX/2rw;

    if-eq v0, v2, :cond_0

    .line 2515505
    iget-object v0, p0, LX/HtB;->i:LX/0ad;

    sget-short v2, LX/1EB;->Y:S

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2515506
    if-eqz v0, :cond_0

    move v4, v3

    goto :goto_0

    .line 2515507
    :cond_3
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v2, LX/0j5;

    invoke-interface {v2}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    iget-object v5, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 2515508
    if-eqz v5, :cond_8

    .line 2515509
    invoke-static {v5}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;)I

    move-result v2

    .line 2515510
    const v6, 0x1eaef984

    if-eq v2, v6, :cond_4

    iget-object v2, p0, LX/HtB;->i:LX/0ad;

    sget-short v6, LX/1EB;->X:S

    invoke-interface {v2, v6, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v4

    .line 2515511
    :goto_2
    if-eqz v5, :cond_5

    if-nez v2, :cond_6

    :cond_5
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v0, :cond_0

    :cond_6
    move v4, v3

    goto/16 :goto_0

    :cond_7
    move v2, v3

    .line 2515512
    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2515462
    iget-object v0, p0, LX/HtB;->m:LX/Hsu;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/HtB;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2515463
    :cond_0
    :goto_0
    return-void

    .line 2515464
    :cond_1
    iget-object v0, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515465
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 2515466
    const-string v1, ""

    .line 2515467
    iget-object p1, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz p1, :cond_3

    .line 2515468
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p1, "shareable_"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object p1, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2515469
    :goto_1
    iget-object p1, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-eqz p1, :cond_2

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->shouldIncludeReshareContext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2515470
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":include_reshare_context"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2515471
    :cond_2
    move-object v0, v1

    .line 2515472
    iget-object v1, p0, LX/HtB;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2515473
    iput-object v0, p0, LX/HtB;->n:Ljava/lang/String;

    .line 2515474
    invoke-direct {p0}, LX/HtB;->c()V

    goto :goto_0

    .line 2515475
    :cond_3
    iget-object p1, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz p1, :cond_4

    .line 2515476
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p1, "link_"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object p1, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2515477
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p1, "share_preview_only"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2515485
    iget-object v0, p0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515486
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    if-nez v1, :cond_0

    move v0, v2

    .line 2515487
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2515478
    iget-object v0, p0, LX/HtB;->j:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2515479
    iget-object v0, p0, LX/HtB;->m:LX/Hsu;

    .line 2515480
    move-object v0, v0

    .line 2515481
    check-cast v0, LX/Hsu;

    invoke-virtual {v0, v1}, LX/Hsu;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2515482
    iput-object v1, p0, LX/HtB;->m:LX/Hsu;

    .line 2515483
    iput-object v1, p0, LX/HtB;->n:Ljava/lang/String;

    .line 2515484
    return-void
.end method
