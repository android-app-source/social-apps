.class public final LX/JBI;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2658617
    const-class v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsModel;

    const v0, -0x43826ad7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "CollectionAppSectionsNodeQuery"

    const-string v6, "88e9d3a2a49a84f64c02d619bea0ab4b"

    const-string v7, "user"

    const-string v8, "10155207561641729"

    const-string v9, "10155259086521729"

    const-string v0, "collections_sections_end_cursor"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2658618
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2658619
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2658620
    sparse-switch v0, :sswitch_data_0

    .line 2658621
    :goto_0
    return-object p1

    .line 2658622
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2658623
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2658624
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2658625
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2658626
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2658627
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2658628
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2658629
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2658630
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2658631
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2658632
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2658633
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2658634
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2658635
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2658636
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2658637
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2658638
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2658639
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7d8a14ab -> :sswitch_5
        -0x5f7c117a -> :sswitch_9
        -0x5224e9a4 -> :sswitch_c
        -0x41b8e48f -> :sswitch_d
        -0x41a91745 -> :sswitch_e
        -0x3b73c98f -> :sswitch_1
        -0x17fb7f63 -> :sswitch_a
        -0x17e5f441 -> :sswitch_2
        -0x14c64f74 -> :sswitch_10
        -0x9ac82a1 -> :sswitch_b
        0x180aba4 -> :sswitch_7
        0x24991595 -> :sswitch_8
        0x291d8de0 -> :sswitch_f
        0x3052e0ff -> :sswitch_0
        0x46b44ab2 -> :sswitch_4
        0x4b13ca50 -> :sswitch_11
        0x5f424068 -> :sswitch_6
        0x6e802595 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2658640
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2658641
    :goto_1
    return v0

    .line 2658642
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2658643
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
