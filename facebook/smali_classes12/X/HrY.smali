.class public final LX/HrY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2511244
    iput-object p1, p0, LX/HrY;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iput-object p2, p0, LX/HrY;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 6

    .prologue
    .line 2511245
    iget-object v0, p0, LX/HrY;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511246
    :goto_0
    return-void

    .line 2511247
    :cond_0
    iget-object v0, p0, LX/HrY;->b:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2511248
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 2511249
    iget-object v2, p0, LX/HrY;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    .line 2511250
    iget-object v3, v2, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    move-object v2, v3

    .line 2511251
    iget-object v3, p0, LX/HrY;->b:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511252
    iget-object v4, v3, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v4

    .line 2511253
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 2511254
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 2511255
    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 2511256
    const/4 v2, 0x1

    aget v1, v1, v2

    .line 2511257
    iget-object v2, p0, LX/HrY;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->bo:Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;

    .line 2511258
    iget-object v5, v2, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    invoke-virtual {v5}, LX/0zw;->b()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v2, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    invoke-virtual {v5}, LX/0zw;->a()Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v2, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->g:LX/0zw;

    invoke-virtual {v5}, LX/0zw;->a()Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v5

    :goto_1
    move v2, v5

    .line 2511259
    sub-int v1, v3, v1

    sub-int/2addr v1, v4

    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    .line 2511260
    iget-object v0, p0, LX/HrY;->b:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 2511261
    iget-object v2, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2511262
    iget-object v2, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->g()I

    move-result v2

    if-eq v2, v1, :cond_2

    .line 2511263
    iget-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v2, :cond_1

    .line 2511264
    iget-object v2, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v2

    iput-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2511265
    :cond_1
    iget-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v2, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(I)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2511266
    iget-object v2, v0, LX/0jL;->a:LX/0cA;

    sget-object v3, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v2, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2511267
    :cond_2
    move-object v0, v0

    .line 2511268
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511269
    iget-object v0, p0, LX/HrY;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_0

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method
