.class public LX/JNm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/JO3;

.field public final b:LX/JNy;


# direct methods
.method public constructor <init>(LX/JO3;LX/JNy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2686455
    iput-object p1, p0, LX/JNm;->a:LX/JO3;

    .line 2686456
    iput-object p2, p0, LX/JNm;->b:LX/JNy;

    .line 2686457
    return-void
.end method

.method public static a(LX/0QB;)LX/JNm;
    .locals 5

    .prologue
    .line 2686458
    const-class v1, LX/JNm;

    monitor-enter v1

    .line 2686459
    :try_start_0
    sget-object v0, LX/JNm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686460
    sput-object v2, LX/JNm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686461
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686462
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686463
    new-instance p0, LX/JNm;

    invoke-static {v0}, LX/JO3;->a(LX/0QB;)LX/JO3;

    move-result-object v3

    check-cast v3, LX/JO3;

    invoke-static {v0}, LX/JNy;->a(LX/0QB;)LX/JNy;

    move-result-object v4

    check-cast v4, LX/JNy;

    invoke-direct {p0, v3, v4}, LX/JNm;-><init>(LX/JO3;LX/JNy;)V

    .line 2686464
    move-object v0, p0

    .line 2686465
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686466
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686467
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686468
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
