.class public final enum LX/IsB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IsB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IsB;

.field public static final enum BACKGROUND_COLOR_CHANGE:LX/IsB;

.field public static final enum TEXT_CHANGE:LX/IsB;

.field public static final enum TEXT_COLOR_CHANGE:LX/IsB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2619352
    new-instance v0, LX/IsB;

    const-string v1, "TEXT_CHANGE"

    invoke-direct {v0, v1, v2}, LX/IsB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IsB;->TEXT_CHANGE:LX/IsB;

    .line 2619353
    new-instance v0, LX/IsB;

    const-string v1, "TEXT_COLOR_CHANGE"

    invoke-direct {v0, v1, v3}, LX/IsB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IsB;->TEXT_COLOR_CHANGE:LX/IsB;

    .line 2619354
    new-instance v0, LX/IsB;

    const-string v1, "BACKGROUND_COLOR_CHANGE"

    invoke-direct {v0, v1, v4}, LX/IsB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IsB;->BACKGROUND_COLOR_CHANGE:LX/IsB;

    .line 2619355
    const/4 v0, 0x3

    new-array v0, v0, [LX/IsB;

    sget-object v1, LX/IsB;->TEXT_CHANGE:LX/IsB;

    aput-object v1, v0, v2

    sget-object v1, LX/IsB;->TEXT_COLOR_CHANGE:LX/IsB;

    aput-object v1, v0, v3

    sget-object v1, LX/IsB;->BACKGROUND_COLOR_CHANGE:LX/IsB;

    aput-object v1, v0, v4

    sput-object v0, LX/IsB;->$VALUES:[LX/IsB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2619356
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IsB;
    .locals 1

    .prologue
    .line 2619357
    const-class v0, LX/IsB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IsB;

    return-object v0
.end method

.method public static values()[LX/IsB;
    .locals 1

    .prologue
    .line 2619358
    sget-object v0, LX/IsB;->$VALUES:[LX/IsB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IsB;

    return-object v0
.end method
