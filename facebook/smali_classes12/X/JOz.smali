.class public LX/JOz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1nA;

.field private final b:LX/1vg;

.field public final c:LX/3AZ;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1nA;LX/1vg;LX/3AZ;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nA;",
            "LX/1vg;",
            "LX/3AZ;",
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2688603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2688604
    iput-object p1, p0, LX/JOz;->a:LX/1nA;

    .line 2688605
    iput-object p2, p0, LX/JOz;->b:LX/1vg;

    .line 2688606
    iput-object p3, p0, LX/JOz;->c:LX/3AZ;

    .line 2688607
    iput-object p4, p0, LX/JOz;->d:LX/0Ot;

    .line 2688608
    return-void
.end method

.method public static a(LX/JOz;LX/1De;ID)LX/1Dh;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2688598
    double-to-int v1, p3

    .line 2688599
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 2688600
    if-lez v1, :cond_0

    iget-object v0, p0, LX/JOz;->b:LX/1vg;

    const v3, 0x7f020a17

    const v4, 0x7f0a00d4

    invoke-static {p1, v0, v3, v4}, LX/JPK;->a(LX/1De;LX/1vg;II)LX/1dc;

    move-result-object v0

    .line 2688601
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b2571

    invoke-interface {v3, v6, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b258e

    invoke-interface {v3, v5, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v4, 0x7f0a010c

    invoke-virtual {v2, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const v4, 0x7f0b258b

    invoke-virtual {v2, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v3, 0x7f0b258e

    invoke-interface {v0, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v3, 0x7f0b258e

    invoke-interface {v0, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const v3, 0x7f0b2582

    invoke-interface {v0, v5, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    const v3, 0x7f0b2581

    invoke-interface {v0, v6, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "%"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b258a

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b2581

    invoke-interface {v1, v6, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b258a

    invoke-interface {v1, v5, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0

    .line 2688602
    :cond_0
    iget-object v0, p0, LX/JOz;->b:LX/1vg;

    const v3, 0x7f020a12

    const v4, 0x7f0a00d3

    invoke-static {p1, v0, v3, v4}, LX/JPK;->a(LX/1De;LX/1vg;II)LX/1dc;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/JOz;
    .locals 7

    .prologue
    .line 2688609
    const-class v1, LX/JOz;

    monitor-enter v1

    .line 2688610
    :try_start_0
    sget-object v0, LX/JOz;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688611
    sput-object v2, LX/JOz;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688612
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688613
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688614
    new-instance v6, LX/JOz;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/3AZ;->a(LX/0QB;)LX/3AZ;

    move-result-object v5

    check-cast v5, LX/3AZ;

    const/16 p0, 0x1fdc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/JOz;-><init>(LX/1nA;LX/1vg;LX/3AZ;LX/0Ot;)V

    .line 2688615
    move-object v0, v6

    .line 2688616
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688617
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688618
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688619
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
