.class public LX/IPr;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IPo;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IPt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2574802
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/IPr;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IPt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574799
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2574800
    iput-object p1, p0, LX/IPr;->b:LX/0Ot;

    .line 2574801
    return-void
.end method

.method public static a(LX/0QB;)LX/IPr;
    .locals 4

    .prologue
    .line 2574788
    const-class v1, LX/IPr;

    monitor-enter v1

    .line 2574789
    :try_start_0
    sget-object v0, LX/IPr;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2574790
    sput-object v2, LX/IPr;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2574791
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2574792
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2574793
    new-instance v3, LX/IPr;

    const/16 p0, 0x23fb

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IPr;-><init>(LX/0Ot;)V

    .line 2574794
    move-object v0, v3

    .line 2574795
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2574796
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IPr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574797
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2574798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BcP;Z)V
    .locals 3

    .prologue
    .line 2574782
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 2574783
    if-nez v0, :cond_0

    .line 2574784
    :goto_0
    return-void

    .line 2574785
    :cond_0
    check-cast v0, LX/IPp;

    .line 2574786
    new-instance v1, LX/IPq;

    iget-object v2, v0, LX/IPp;->f:LX/IPr;

    invoke-direct {v1, v2, p1}, LX/IPq;-><init>(LX/IPr;Z)V

    move-object v0, v1

    .line 2574787
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 2574748
    invoke-static {}, LX/1dS;->b()V

    .line 2574749
    iget v0, p1, LX/BcQ;->b:I

    .line 2574750
    sparse-switch v0, :sswitch_data_0

    move-object v0, v6

    .line 2574751
    :goto_0
    return-object v0

    .line 2574752
    :sswitch_0
    check-cast p2, LX/BdG;

    .line 2574753
    iget v1, p2, LX/BdG;->a:I

    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v3, v0, v4

    check-cast v3, LX/BcP;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    const/4 v4, 0x1

    aget-object v0, v0, v4

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move-object v0, p0

    .line 2574754
    iget-object v6, v0, LX/IPr;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/IPt;

    .line 2574755
    iget-object p0, v6, LX/IPt;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/IPn;

    const/4 v0, 0x0

    .line 2574756
    new-instance v5, LX/IPm;

    invoke-direct {v5, p0}, LX/IPm;-><init>(LX/IPn;)V

    .line 2574757
    sget-object v6, LX/IPn;->a:LX/0Zi;

    invoke-virtual {v6}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/IPl;

    .line 2574758
    if-nez v6, :cond_0

    .line 2574759
    new-instance v6, LX/IPl;

    invoke-direct {v6}, LX/IPl;-><init>()V

    .line 2574760
    :cond_0
    invoke-static {v6, v3, v0, v0, v5}, LX/IPl;->a$redex0(LX/IPl;LX/1De;IILX/IPm;)V

    .line 2574761
    move-object v5, v6

    .line 2574762
    move-object v0, v5

    .line 2574763
    move-object p0, v0

    .line 2574764
    check-cast v2, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;

    .line 2574765
    iget-object v0, p0, LX/IPl;->a:LX/IPm;

    iput-object v2, v0, LX/IPm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;

    .line 2574766
    iget-object v0, p0, LX/IPl;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    .line 2574767
    move-object p0, p0

    .line 2574768
    iget-object v0, p0, LX/IPl;->a:LX/IPm;

    iput v1, v0, LX/IPm;->b:I

    .line 2574769
    iget-object v0, p0, LX/IPl;->d:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    .line 2574770
    move-object p0, p0

    .line 2574771
    iget-object v0, p0, LX/IPl;->a:LX/IPm;

    iput-boolean v4, v0, LX/IPm;->c:Z

    .line 2574772
    iget-object v0, p0, LX/IPl;->d:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    .line 2574773
    move-object p0, p0

    .line 2574774
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object v6, p0

    .line 2574775
    move-object v0, v6

    .line 2574776
    goto :goto_0

    .line 2574777
    :sswitch_1
    check-cast p2, LX/BcM;

    .line 2574778
    iget-boolean v1, p2, LX/BcM;->a:Z

    iget-object v2, p2, LX/BcM;->b:LX/BcL;

    iget-object v3, p2, LX/BcM;->c:Ljava/lang/Throwable;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v4, v0, v4

    check-cast v4, LX/BcP;

    move-object v0, p0

    .line 2574779
    iget-object p0, v0, LX/IPr;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v1, v2, v3, v4}, LX/IPt;->a(ZLX/BcL;Ljava/lang/Throwable;LX/BcP;)V

    .line 2574780
    move-object v0, v6

    .line 2574781
    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0xe6aa589 -> :sswitch_1
        0x558934c4 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 2574744
    check-cast p1, LX/IPp;

    .line 2574745
    check-cast p2, LX/IPp;

    .line 2574746
    iget-boolean v0, p1, LX/IPp;->b:Z

    iput-boolean v0, p2, LX/IPp;->b:Z

    .line 2574747
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 7

    .prologue
    .line 2574726
    check-cast p3, LX/IPp;

    .line 2574727
    iget-object v0, p0, LX/IPr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IPt;

    iget-boolean v3, p3, LX/IPp;->b:Z

    iget-object v4, p3, LX/IPp;->c:LX/1rs;

    iget v5, p3, LX/IPp;->d:I

    iget-boolean v6, p3, LX/IPp;->e:Z

    move-object v1, p1

    move-object v2, p2

    .line 2574728
    iget-object p0, v0, LX/IPt;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bcw;

    invoke-virtual {p0, v1}, LX/Bcw;->c(LX/BcP;)LX/Bct;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/Bct;->a(LX/1rs;)LX/Bct;

    move-result-object p0

    const-string p1, "GROUP_MALL_DRAWER"

    invoke-virtual {p0, p1}, LX/Bct;->b(Ljava/lang/String;)LX/Bct;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/Bct;->a(I)LX/Bct;

    move-result-object p0

    .line 2574729
    const p1, 0x558934c4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v0, v4

    const/4 v4, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v0, v4

    invoke-static {v1, p1, v0}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p1

    move-object p1, p1

    .line 2574730
    invoke-virtual {p0, p1}, LX/Bct;->b(LX/BcQ;)LX/Bct;

    move-result-object p0

    .line 2574731
    const p1, -0xe6aa589

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v0, v4

    invoke-static {v1, p1, v0}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p1

    move-object p1, p1

    .line 2574732
    invoke-virtual {p0, p1}, LX/Bct;->c(LX/BcQ;)LX/Bct;

    move-result-object p0

    invoke-virtual {p0}, LX/Bct;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2574733
    if-eqz v3, :cond_0

    .line 2574734
    invoke-static {v1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object p0

    invoke-static {v1}, LX/Bdj;->c(LX/1De;)LX/Bdh;

    move-result-object p1

    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object p0

    invoke-virtual {p0}, LX/Bcc;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2574735
    :cond_0
    return-void
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 2

    .prologue
    .line 2574736
    check-cast p2, LX/IPp;

    .line 2574737
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 2574738
    iget-object v1, p0, LX/IPr;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2574739
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2574740
    iput-object v1, v0, LX/1np;->a:Ljava/lang/Object;

    .line 2574741
    iget-object v1, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2574742
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/IPp;->b:Z

    .line 2574743
    return-void
.end method
