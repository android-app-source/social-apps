.class public final LX/JDu;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public b:LX/Hu2;

.field public final c:LX/Hu3;

.field private d:LX/AQx;

.field public final e:LX/AR9;

.field private final f:LX/0ad;

.field public final g:Landroid/view/inputmethod/InputMethodManager;

.field public h:Lcom/facebook/composer/ui/text/ComposerEditText;

.field public i:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2664799
    const-class v0, LX/6oO;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/JDu;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/B5j;Landroid/content/Context;LX/AR9;LX/Hu3;LX/AQx;LX/0ad;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Landroid/content/Context;",
            "LX/AR9;",
            "LX/Hu3;",
            "LX/AQx;",
            "LX/0ad;",
            "Landroid/view/inputmethod/InputMethodManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2664800
    invoke-direct {p0, p2, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 2664801
    iput-object p3, p0, LX/JDu;->e:LX/AR9;

    .line 2664802
    iput-object p4, p0, LX/JDu;->c:LX/Hu3;

    .line 2664803
    iput-object p5, p0, LX/JDu;->d:LX/AQx;

    .line 2664804
    iput-object p6, p0, LX/JDu;->f:LX/0ad;

    .line 2664805
    iput-object p7, p0, LX/JDu;->g:Landroid/view/inputmethod/InputMethodManager;

    .line 2664806
    return-void
.end method

.method private aN()V
    .locals 5

    .prologue
    .line 2664807
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2664808
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v0

    new-instance v1, LX/JDr;

    invoke-direct {v1, p0}, LX/JDr;-><init>(LX/JDu;)V

    new-instance v2, LX/JDs;

    invoke-direct {v2, p0}, LX/JDs;-><init>(LX/JDu;)V

    .line 2664809
    iget-object v3, p0, LX/AQ9;->b:Landroid/content/Context;

    move-object v3, v3

    .line 2664810
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v3, p0, LX/JDu;->i:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, LX/AQy;

    invoke-static {v4, v3, v0, v1, v2}, LX/AQz;->a(Landroid/content/res/Resources;LX/AQy;Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;Landroid/text/TextWatcher;Landroid/view/View$OnClickListener;)V

    .line 2664811
    :cond_0
    return-void
.end method


# virtual methods
.method public final T()V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2664812
    iget-object v0, p0, LX/JDu;->b:LX/Hu2;

    if-eqz v0, :cond_0

    .line 2664813
    iget-object v0, p0, LX/JDu;->b:LX/Hu2;

    invoke-virtual {v0}, LX/Hu2;->a()V

    .line 2664814
    :cond_0
    invoke-direct {p0}, LX/JDu;->aN()V

    .line 2664815
    return-void
.end method

.method public final W()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664816
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final X()LX/ARN;
    .locals 1

    .prologue
    .line 2664817
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final Y()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664829
    new-instance v0, LX/JDk;

    invoke-direct {v0, p0}, LX/JDk;-><init>(LX/JDu;)V

    return-object v0
.end method

.method public final Z()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664764
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 2
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2664818
    const/16 v0, 0xdd

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2664819
    :cond_0
    :goto_0
    return-void

    .line 2664820
    :cond_1
    const-string v0, "extra_composer_life_event_icon_model"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2664821
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/JDu;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    const-string v1, "extra_composer_life_event_icon_model"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_0
.end method

.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 2664822
    sget-object v0, LX/JDj;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2664823
    :goto_0
    return-void

    .line 2664824
    :pswitch_0
    iget-object v0, p0, LX/JDu;->b:LX/Hu2;

    invoke-virtual {v0}, LX/Hu2;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2664825
    invoke-virtual {p0}, LX/AQ9;->T()V

    return-void
.end method

.method public final aB()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664826
    new-instance v0, LX/JDp;

    invoke-direct {v0, p0}, LX/JDp;-><init>(LX/JDu;)V

    return-object v0
.end method

.method public final aC()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664827
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aD()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664828
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aF()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664797
    new-instance v0, LX/JDg;

    invoke-direct {v0, p0}, LX/JDg;-><init>(LX/JDu;)V

    return-object v0
.end method

.method public final aG()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664798
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664757
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664758
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ab()LX/ARN;
    .locals 1

    .prologue
    .line 2664759
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final ac()LX/ARN;
    .locals 1

    .prologue
    .line 2664760
    new-instance v0, LX/JDl;

    invoke-direct {v0, p0}, LX/JDl;-><init>(LX/JDu;)V

    return-object v0
.end method

.method public final ad()LX/ARN;
    .locals 1

    .prologue
    .line 2664761
    new-instance v0, LX/JDm;

    invoke-direct {v0, p0}, LX/JDm;-><init>(LX/JDu;)V

    return-object v0
.end method

.method public final ae()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664762
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final af()LX/ARN;
    .locals 1

    .prologue
    .line 2664763
    new-instance v0, LX/JDn;

    invoke-direct {v0, p0}, LX/JDn;-><init>(LX/JDu;)V

    return-object v0
.end method

.method public final ai()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664765
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aj()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664766
    new-instance v0, LX/JDo;

    invoke-direct {v0, p0}, LX/JDo;-><init>(LX/JDu;)V

    return-object v0
.end method

.method public final am()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664767
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final an()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664768
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2664769
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2664770
    iget-object v0, p0, LX/JDu;->i:LX/0zw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JDu;->i:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JDu;->i:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2664771
    iget-object v0, p0, LX/JDu;->i:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->a()V

    .line 2664772
    :goto_0
    new-instance v0, Lcom/facebook/timeline/lifeevent/LifeEventComposerPlugin$11;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/lifeevent/LifeEventComposerPlugin$11;-><init>(LX/JDu;)V

    .line 2664773
    iget-object v1, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/composer/ui/text/ComposerEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2664774
    return-void

    .line 2664775
    :cond_0
    iget-object v0, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->requestFocus()Z

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewStub;)Z
    .locals 14

    .prologue
    const/4 v3, 0x1

    .line 2664776
    const v0, 0x7f0309e1

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2664777
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 2664778
    const v0, 0x7f0d1913

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/text/ComposerEditText;

    iput-object v0, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    .line 2664779
    iget-object v2, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    .line 2664780
    iget-object v0, p0, LX/AQ9;->K:LX/AQ4;

    move-object v0, v0

    .line 2664781
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2664782
    iget-object v0, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setIncludeFriends(Z)V

    .line 2664783
    iget-object v0, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    new-instance v2, LX/JDq;

    invoke-direct {v2, p0}, LX/JDq;-><init>(LX/JDu;)V

    invoke-virtual {v0, v2}, Lcom/facebook/composer/ui/text/ComposerEditText;->a(LX/Be0;)V

    .line 2664784
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2664785
    iget-object v0, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/composer/ui/text/ComposerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2664786
    iget-object v0, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 2664787
    iget-object v2, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v2}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2, v0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 2664788
    :cond_0
    iget-object v0, p0, LX/JDu;->h:Lcom/facebook/composer/ui/text/ComposerEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/composer/ui/text/ComposerEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2664789
    new-instance v2, LX/0zw;

    const v0, 0x7f0d1912

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, LX/JDu;->i:LX/0zw;

    .line 2664790
    iget-object v4, p0, LX/JDu;->i:LX/0zw;

    invoke-virtual {v4}, LX/0zw;->a()Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;

    .line 2664791
    iget-object v5, p0, LX/JDu;->c:LX/Hu3;

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v6

    new-instance v7, LX/JDf;

    invoke-direct {v7, p0}, LX/JDf;-><init>(LX/JDu;)V

    .line 2664792
    new-instance v8, LX/Hu2;

    const-class v9, Landroid/content/Context;

    invoke-interface {v5, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Context;

    invoke-static {v5}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v13

    check-cast v13, LX/11S;

    move-object v9, v6

    move-object v10, v7

    move-object v11, v4

    invoke-direct/range {v8 .. v13}, LX/Hu2;-><init>(LX/0il;LX/JDf;Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;Landroid/content/Context;LX/11S;)V

    .line 2664793
    move-object v4, v8

    .line 2664794
    iput-object v4, p0, LX/JDu;->b:LX/Hu2;

    .line 2664795
    invoke-direct {p0}, LX/JDu;->aN()V

    .line 2664796
    return v3
.end method
