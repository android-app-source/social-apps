.class public final LX/HZ0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/registration/controller/RegistrationFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/controller/RegistrationFragmentController;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2481243
    iput-object p1, p0, LX/HZ0;->b:Lcom/facebook/registration/controller/RegistrationFragmentController;

    iput-object p2, p0, LX/HZ0;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 6
    .param p1    # Lcom/facebook/fbservice/service/OperationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2481233
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;

    if-eqz v0, :cond_1

    .line 2481234
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;

    .line 2481235
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->b()LX/0Px;

    move-result-object v0

    .line 2481236
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    .line 2481237
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->e()Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2481238
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->e()Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPointList;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2481239
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPoint;

    .line 2481240
    iget-object v5, p0, LX/HZ0;->a:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountCandidateContactPoint;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2481241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2481242
    :cond_1
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2481231
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2481232
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/HZ0;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
