.class public LX/IM6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1Ck;

.field private final b:LX/0tX;

.field private final c:LX/0Zb;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2569064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2569065
    iput-object p1, p0, LX/IM6;->a:LX/1Ck;

    .line 2569066
    iput-object p2, p0, LX/IM6;->b:LX/0tX;

    .line 2569067
    iput-object p3, p0, LX/IM6;->c:LX/0Zb;

    .line 2569068
    return-void
.end method

.method public static a(LX/0QB;)LX/IM6;
    .locals 6

    .prologue
    .line 2569053
    const-class v1, LX/IM6;

    monitor-enter v1

    .line 2569054
    :try_start_0
    sget-object v0, LX/IM6;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2569055
    sput-object v2, LX/IM6;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2569056
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2569057
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2569058
    new-instance p0, LX/IM6;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {p0, v3, v4, v5}, LX/IM6;-><init>(LX/1Ck;LX/0tX;LX/0Zb;)V

    .line 2569059
    move-object v0, p0

    .line 2569060
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2569061
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IM6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2569062
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2569063
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/IM6;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2569043
    iget-object v0, p0, LX/IM6;->c:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2569044
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2569045
    const-string v1, "community_nux_question"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2569046
    const-string v1, "community_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2569047
    const-string v1, "nux_question"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2569048
    const-string v1, "subgroup_id"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2569049
    const-string v1, "is_group_created"

    invoke-virtual {v0, v1, p6}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2569050
    const-string v1, "group_search_query"

    invoke-virtual {v0, v1, p5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2569051
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2569052
    :cond_0
    return-void
.end method

.method public static b(LX/IM6;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2569023
    new-instance v0, LX/4Da;

    invoke-direct {v0}, LX/4Da;-><init>()V

    .line 2569024
    const-string v1, "parent_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569025
    move-object v0, v0

    .line 2569026
    const-string v1, "community_nux_question_id"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569027
    move-object v0, v0

    .line 2569028
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2569029
    const-string v2, "dismiss_question"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2569030
    move-object v0, v0

    .line 2569031
    if-eqz p3, :cond_0

    .line 2569032
    const-string v1, "child_group_id"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569033
    :cond_0
    if-eqz p4, :cond_1

    .line 2569034
    const-string v1, "create_group_name"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569035
    :cond_1
    new-instance v1, LX/ILv;

    invoke-direct {v1}, LX/ILv;-><init>()V

    move-object v1, v1

    .line 2569036
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/ILv;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2569037
    iget-object v1, p0, LX/IM6;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2569038
    iget-object v1, p0, LX/IM6;->a:LX/1Ck;

    sget-object v2, LX/IM5;->TASK_ANSWER_A_QUESTION:LX/IM5;

    new-instance v3, LX/IM4;

    invoke-direct {v3, p0}, LX/IM4;-><init>(LX/IM6;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2569039
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2569040
    const-string v1, "community_questions_nux_answer"

    const/4 v6, 0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, LX/IM6;->a(LX/IM6;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2569041
    invoke-static {p0, p1, p2, p4, p3}, LX/IM6;->b(LX/IM6;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2569042
    return-void
.end method
