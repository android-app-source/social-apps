.class public LX/HWH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/9XE;

.field private final b:LX/8Do;


# direct methods
.method public constructor <init>(LX/9XE;LX/8Do;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475774
    iput-object p1, p0, LX/HWH;->a:LX/9XE;

    .line 2475775
    iput-object p2, p0, LX/HWH;->b:LX/8Do;

    .line 2475776
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2475777
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2475778
    iget-object v2, p0, LX/HWH;->a:LX/9XE;

    sget-object v3, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_ALL_VIDEOS:LX/9XI;

    invoke-virtual {v2, v3, v0, v1}, LX/9XE;->a(LX/9X2;J)V

    .line 2475779
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a(JZZ)Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    move-result-object v0

    return-object v0
.end method
