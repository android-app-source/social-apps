.class public LX/ICI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static q:LX/0Xm;


# instance fields
.field public final a:LX/2g9;

.field public final b:LX/3iV;

.field private final c:LX/0Zb;

.field private final d:LX/2fk;

.field private final e:LX/2fm;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:LX/0bH;

.field private final h:LX/14w;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/1Sl;

.field private final k:LX/0tX;

.field private final l:LX/1Sa;

.field private final m:LX/1Sj;

.field private final n:LX/03V;

.field private final o:LX/2fh;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2g9;LX/3iV;LX/0Zb;LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/14w;LX/0Ot;LX/1Sl;LX/0tX;LX/2fk;LX/2fm;Ljava/util/concurrent/ExecutorService;LX/0Ot;)V
    .locals 1
    .param p15    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2g9;",
            "LX/3iV;",
            "LX/0Zb;",
            "LX/1Sa;",
            "LX/1Sj;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2fh;",
            "LX/0bH;",
            "LX/14w;",
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/2fk;",
            "LX/2fm;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2549159
    iput-object p1, p0, LX/ICI;->a:LX/2g9;

    .line 2549160
    iput-object p2, p0, LX/ICI;->b:LX/3iV;

    .line 2549161
    iput-object p3, p0, LX/ICI;->c:LX/0Zb;

    .line 2549162
    iput-object p13, p0, LX/ICI;->d:LX/2fk;

    .line 2549163
    iput-object p14, p0, LX/ICI;->e:LX/2fm;

    .line 2549164
    move-object/from16 v0, p15

    iput-object v0, p0, LX/ICI;->f:Ljava/util/concurrent/ExecutorService;

    .line 2549165
    iput-object p8, p0, LX/ICI;->g:LX/0bH;

    .line 2549166
    iput-object p9, p0, LX/ICI;->h:LX/14w;

    .line 2549167
    iput-object p10, p0, LX/ICI;->i:LX/0Ot;

    .line 2549168
    iput-object p11, p0, LX/ICI;->j:LX/1Sl;

    .line 2549169
    iput-object p12, p0, LX/ICI;->k:LX/0tX;

    .line 2549170
    iput-object p4, p0, LX/ICI;->l:LX/1Sa;

    .line 2549171
    iput-object p5, p0, LX/ICI;->m:LX/1Sj;

    .line 2549172
    iput-object p6, p0, LX/ICI;->n:LX/03V;

    .line 2549173
    iput-object p7, p0, LX/ICI;->o:LX/2fh;

    .line 2549174
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ICI;->p:LX/0Ot;

    .line 2549175
    return-void
.end method

.method public static a(LX/0QB;)LX/ICI;
    .locals 3

    .prologue
    .line 2549176
    const-class v1, LX/ICI;

    monitor-enter v1

    .line 2549177
    :try_start_0
    sget-object v0, LX/ICI;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2549178
    sput-object v2, LX/ICI;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2549179
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2549180
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/ICI;->b(LX/0QB;)LX/ICI;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2549181
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ICI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2549182
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2549183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 1

    .prologue
    .line 2549184
    const v0, -0x3625f733

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 2549185
    if-nez v0, :cond_0

    .line 2549186
    const/4 v0, 0x0

    .line 2549187
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/ICI;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Po;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2549188
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2549189
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2549190
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2549191
    :cond_0
    :goto_0
    return-void

    .line 2549192
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 2549193
    iget-object v1, p0, LX/ICI;->c:LX/0Zb;

    const-string v2, "for_sale_item_save_button_clicked"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2549194
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2549195
    const-string v2, "for_sale_item_id"

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "surface"

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-static {v2}, LX/217;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/ICI;
    .locals 19

    .prologue
    .line 2549196
    new-instance v2, LX/ICI;

    invoke-static/range {p0 .. p0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    invoke-static/range {p0 .. p0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v4

    check-cast v4, LX/3iV;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v6

    check-cast v6, LX/1Sa;

    invoke-static/range {p0 .. p0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v7

    check-cast v7, LX/1Sj;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static/range {p0 .. p0}, LX/2fh;->a(LX/0QB;)LX/2fh;

    move-result-object v9

    check-cast v9, LX/2fh;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v10

    check-cast v10, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v11

    check-cast v11, LX/14w;

    const/16 v12, 0x3279

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/1Sl;->a(LX/0QB;)LX/1Sl;

    move-result-object v13

    check-cast v13, LX/1Sl;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v14

    check-cast v14, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/2fk;->a(LX/0QB;)LX/2fk;

    move-result-object v15

    check-cast v15, LX/2fk;

    const-class v16, LX/2fm;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/2fm;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v17

    check-cast v17, Ljava/util/concurrent/ExecutorService;

    const/16 v18, 0x327a

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-direct/range {v2 .. v18}, LX/ICI;-><init>(LX/2g9;LX/3iV;LX/0Zb;LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/14w;LX/0Ot;LX/1Sl;LX/0tX;LX/2fk;LX/2fm;Ljava/util/concurrent/ExecutorService;LX/0Ot;)V

    .line 2549197
    return-object v2
.end method


# virtual methods
.method public onSaveClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V
    .locals 26
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Po;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 2549198
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v3}, LX/ICI;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v18

    .line 2549199
    new-instance v3, LX/ICH;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/ICI;->l:LX/1Sa;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/ICI;->m:LX/1Sj;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/ICI;->n:LX/03V;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/ICI;->o:LX/2fh;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/ICI;->g:LX/0bH;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/ICI;->d:LX/2fk;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/ICI;->h:LX/14w;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, LX/ICI;->j:LX/1Sl;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/ICI;->k:LX/0tX;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/ICI;->f:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/ICI;->i:LX/0Ot;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/ICI;->e:LX/2fm;

    move-object/from16 v17, v0

    const-string v19, "native_story"

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/ICI;->p:LX/0Ot;

    move-object/from16 v22, v0

    move-object/from16 v4, p0

    move-object/from16 v20, p3

    move-object/from16 v23, v18

    move-object/from16 v24, p4

    move-object/from16 v25, p3

    invoke-direct/range {v3 .. v25}, LX/ICH;-><init>(LX/ICI;LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;LX/0Ot;Lcom/facebook/graphql/model/GraphQLNode;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2549200
    iget-object v3, v3, LX/2fo;->d:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2549201
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, LX/ICI;->a(LX/ICI;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V

    .line 2549202
    return-void
.end method
