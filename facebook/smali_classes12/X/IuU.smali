.class public LX/IuU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Ox;

.field private final b:LX/34G;


# direct methods
.method public constructor <init>(LX/2Ox;LX/34G;)V
    .locals 0

    .prologue
    .line 2626854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2626855
    iput-object p1, p0, LX/IuU;->a:LX/2Ox;

    .line 2626856
    iput-object p2, p0, LX/IuU;->b:LX/34G;

    .line 2626857
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 5

    .prologue
    const/16 v2, 0x78

    const/4 v4, 0x0

    .line 2626858
    iget-object v0, p0, LX/IuU;->b:LX/34G;

    invoke-virtual {v0, p1}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v0

    .line 2626859
    if-eqz v0, :cond_0

    .line 2626860
    iget-object v1, p0, LX/IuU;->a:LX/2Ox;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2, v0, v4, v4}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2626861
    :goto_0
    return-void

    .line 2626862
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2626863
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 2626864
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2626865
    :cond_1
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626866
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2626867
    iget-object v2, p0, LX/IuU;->a:LX/2Ox;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3, v4, v0, v1}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0
.end method
