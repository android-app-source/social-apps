.class public final LX/IBR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/events/model/Event;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:LX/IBS;


# direct methods
.method public constructor <init>(LX/IBS;Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2547145
    iput-object p1, p0, LX/IBR;->c:LX/IBS;

    iput-object p2, p0, LX/IBR;->a:Landroid/content/Context;

    iput-object p3, p0, LX/IBR;->b:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2547146
    const/4 v6, 0x0

    .line 2547147
    :try_start_0
    iget-object v0, p0, LX/IBR;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, LX/IBR;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2547148
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 2547149
    new-instance v0, LX/Bl1;

    invoke-direct {v0, v1}, LX/Bl1;-><init>(Landroid/database/Cursor;)V

    .line 2547150
    iget-object v2, v0, LX/Bl1;->ak:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2547151
    invoke-virtual {v0}, LX/Bl1;->d()Lcom/facebook/events/model/Event;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2547152
    if-eqz v1, :cond_0

    .line 2547153
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 2547154
    :cond_1
    if-eqz v1, :cond_2

    .line 2547155
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v6

    goto :goto_0

    .line 2547156
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_3

    .line 2547157
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2547158
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method
