.class public final LX/JAL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 2655191
    const/16 v22, 0x0

    .line 2655192
    const/16 v21, 0x0

    .line 2655193
    const/16 v20, 0x0

    .line 2655194
    const/16 v19, 0x0

    .line 2655195
    const/16 v18, 0x0

    .line 2655196
    const/16 v17, 0x0

    .line 2655197
    const/16 v16, 0x0

    .line 2655198
    const/4 v15, 0x0

    .line 2655199
    const/4 v14, 0x0

    .line 2655200
    const/4 v13, 0x0

    .line 2655201
    const/4 v12, 0x0

    .line 2655202
    const/4 v11, 0x0

    .line 2655203
    const/4 v10, 0x0

    .line 2655204
    const/4 v9, 0x0

    .line 2655205
    const/4 v8, 0x0

    .line 2655206
    const/4 v7, 0x0

    .line 2655207
    const/4 v6, 0x0

    .line 2655208
    const/4 v5, 0x0

    .line 2655209
    const/4 v4, 0x0

    .line 2655210
    const/4 v3, 0x0

    .line 2655211
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    .line 2655212
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2655213
    const/4 v3, 0x0

    .line 2655214
    :goto_0
    return v3

    .line 2655215
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2655216
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_13

    .line 2655217
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v23

    .line 2655218
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2655219
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    if-eqz v23, :cond_1

    .line 2655220
    const-string v24, "associated_pages"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 2655221
    invoke-static/range {p0 .. p1}, LX/JAA;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 2655222
    :cond_2
    const-string v24, "date_content"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 2655223
    invoke-static/range {p0 .. p1}, LX/JAB;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 2655224
    :cond_3
    const-string v24, "edit_uri"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 2655225
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto :goto_1

    .line 2655226
    :cond_4
    const-string v24, "email"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 2655227
    invoke-static/range {p0 .. p1}, LX/JAC;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 2655228
    :cond_5
    const-string v24, "has_content"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 2655229
    const/4 v4, 0x1

    .line 2655230
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto :goto_1

    .line 2655231
    :cond_6
    const-string v24, "icon"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 2655232
    invoke-static/range {p0 .. p1}, LX/JAD;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 2655233
    :cond_7
    const-string v24, "icon_link"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 2655234
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 2655235
    :cond_8
    const-string v24, "link_url"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 2655236
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 2655237
    :cond_9
    const-string v24, "list_item_groups"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 2655238
    invoke-static/range {p0 .. p1}, LX/JAP;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 2655239
    :cond_a
    const-string v24, "menu_options"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 2655240
    invoke-static/range {p0 .. p1}, LX/JAF;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 2655241
    :cond_b
    const-string v24, "phone_number"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 2655242
    invoke-static/range {p0 .. p1}, LX/JAG;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 2655243
    :cond_c
    const-string v24, "should_show_beside_title"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 2655244
    const/4 v3, 0x1

    .line 2655245
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 2655246
    :cond_d
    const-string v24, "strings_list_content"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 2655247
    invoke-static/range {p0 .. p1}, LX/JAH;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 2655248
    :cond_e
    const-string v24, "styles"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 2655249
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2655250
    :cond_f
    const-string v24, "text_content"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 2655251
    invoke-static/range {p0 .. p1}, LX/JAI;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 2655252
    :cond_10
    const-string v24, "text_lines"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 2655253
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 2655254
    :cond_11
    const-string v24, "title"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_12

    .line 2655255
    invoke-static/range {p0 .. p1}, LX/JAJ;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2655256
    :cond_12
    const-string v24, "upsell_text"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 2655257
    invoke-static/range {p0 .. p1}, LX/JAK;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 2655258
    :cond_13
    const/16 v23, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2655259
    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2655260
    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2655261
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2655262
    const/16 v20, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2655263
    if-eqz v4, :cond_14

    .line 2655264
    const/4 v4, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 2655265
    :cond_14
    const/4 v4, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2655266
    const/4 v4, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2655267
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 2655268
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 2655269
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 2655270
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 2655271
    if-eqz v3, :cond_15

    .line 2655272
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 2655273
    :cond_15
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2655274
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2655275
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2655276
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2655277
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2655278
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 2655279
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v3, 0xf

    const/16 v2, 0xd

    .line 2655280
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655281
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655282
    if-eqz v0, :cond_1

    .line 2655283
    const-string v1, "associated_pages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655284
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2655285
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 2655286
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/JAA;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2655287
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2655288
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2655289
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655290
    if-eqz v0, :cond_3

    .line 2655291
    const-string v1, "date_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655292
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655293
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2655294
    if-eqz v1, :cond_2

    .line 2655295
    const-string v4, "text"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655296
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655297
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655298
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2655299
    if-eqz v0, :cond_4

    .line 2655300
    const-string v1, "edit_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655301
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655302
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655303
    if-eqz v0, :cond_6

    .line 2655304
    const-string v1, "email"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655305
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655306
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2655307
    if-eqz v1, :cond_5

    .line 2655308
    const-string v4, "deduplicated_email_address"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655309
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655310
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655311
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2655312
    if-eqz v0, :cond_7

    .line 2655313
    const-string v1, "has_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655314
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2655315
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655316
    if-eqz v0, :cond_9

    .line 2655317
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655318
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655319
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2655320
    if-eqz v1, :cond_8

    .line 2655321
    const-string v4, "uri"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655322
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655323
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655324
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2655325
    if-eqz v0, :cond_a

    .line 2655326
    const-string v1, "icon_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655327
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655328
    :cond_a
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2655329
    if-eqz v0, :cond_b

    .line 2655330
    const-string v1, "link_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655331
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655332
    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655333
    if-eqz v0, :cond_c

    .line 2655334
    const-string v1, "list_item_groups"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655335
    invoke-static {p0, v0, p2, p3}, LX/JAP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2655336
    :cond_c
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655337
    if-eqz v0, :cond_e

    .line 2655338
    const-string v1, "menu_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655339
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2655340
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_d

    .line 2655341
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/JAF;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2655342
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2655343
    :cond_d
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2655344
    :cond_e
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655345
    if-eqz v0, :cond_11

    .line 2655346
    const-string v1, "phone_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655347
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655348
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2655349
    if-eqz v1, :cond_f

    .line 2655350
    const-string v4, "display_number"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655351
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655352
    :cond_f
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2655353
    if-eqz v1, :cond_10

    .line 2655354
    const-string v4, "universal_number"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655355
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655356
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655357
    :cond_11
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2655358
    if-eqz v0, :cond_12

    .line 2655359
    const-string v1, "should_show_beside_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655360
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2655361
    :cond_12
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655362
    if-eqz v0, :cond_15

    .line 2655363
    const-string v1, "strings_list_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655364
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2655365
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_14

    .line 2655366
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    .line 2655367
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655368
    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2655369
    if-eqz v5, :cond_13

    .line 2655370
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655371
    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655372
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655373
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2655374
    :cond_14
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2655375
    :cond_15
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2655376
    if-eqz v0, :cond_16

    .line 2655377
    const-string v0, "styles"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655378
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2655379
    :cond_16
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655380
    if-eqz v0, :cond_18

    .line 2655381
    const-string v1, "text_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655382
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655383
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2655384
    if-eqz v1, :cond_17

    .line 2655385
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655386
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655387
    :cond_17
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655388
    :cond_18
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2655389
    if-eqz v0, :cond_19

    .line 2655390
    const-string v0, "text_lines"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655391
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2655392
    :cond_19
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655393
    if-eqz v0, :cond_1b

    .line 2655394
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655395
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655396
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2655397
    if-eqz v1, :cond_1a

    .line 2655398
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655399
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655400
    :cond_1a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655401
    :cond_1b
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2655402
    if-eqz v0, :cond_1d

    .line 2655403
    const-string v1, "upsell_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655404
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655405
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2655406
    if-eqz v1, :cond_1c

    .line 2655407
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655408
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655409
    :cond_1c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655410
    :cond_1d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655411
    return-void
.end method
