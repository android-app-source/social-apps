.class public LX/Izf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Izf;


# instance fields
.field private final a:LX/IzO;


# direct methods
.method public constructor <init>(LX/IzO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634751
    iput-object p1, p0, LX/Izf;->a:LX/IzO;

    .line 2634752
    return-void
.end method

.method public static a(LX/0QB;)LX/Izf;
    .locals 4

    .prologue
    .line 2634753
    sget-object v0, LX/Izf;->b:LX/Izf;

    if-nez v0, :cond_1

    .line 2634754
    const-class v1, LX/Izf;

    monitor-enter v1

    .line 2634755
    :try_start_0
    sget-object v0, LX/Izf;->b:LX/Izf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2634756
    if-eqz v2, :cond_0

    .line 2634757
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2634758
    new-instance p0, LX/Izf;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v3

    check-cast v3, LX/IzO;

    invoke-direct {p0, v3}, LX/Izf;-><init>(LX/IzO;)V

    .line 2634759
    move-object v0, p0

    .line 2634760
    sput-object v0, LX/Izf;->b:LX/Izf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2634761
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2634762
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634763
    :cond_1
    sget-object v0, LX/Izf;->b:LX/Izf;

    return-object v0

    .line 2634764
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2634765
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2634766
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2634767
    const-string v0, "getPaymentCardIds"

    const v1, -0x73c677c4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2634768
    :try_start_0
    iget-object v0, p0, LX/Izf;->a:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2634769
    const-string v1, "payment_card_ids"

    invoke-static {v0, v1}, LX/Izf;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2634770
    :try_start_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2634771
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2634772
    sget-object v2, LX/IzQ;->a:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2634773
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634774
    :catchall_1
    move-exception v0

    const v1, -0xedd31f4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2634775
    :cond_0
    :try_start_3
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2634776
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2634777
    const v1, 0x437ab22e

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0
.end method

.method public final b()Ljava/lang/Long;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2634778
    const-string v1, "getPrimaryPaymentCardId"

    const v2, -0x13332e85

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2634779
    :try_start_0
    iget-object v1, p0, LX/Izf;->a:LX/IzO;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2634780
    const-string v2, "primary_payment_card_id"

    invoke-static {v1, v2}, LX/Izf;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2634781
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gt v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2634782
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2634783
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634784
    const v0, 0x7997a090

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 2634785
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2634786
    :cond_1
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2634787
    sget-object v0, LX/IzR;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 2634788
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2634789
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2634790
    const v1, 0x16403976

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_1

    .line 2634791
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2634792
    :catchall_1
    move-exception v0

    const v1, -0x77ea3a7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
