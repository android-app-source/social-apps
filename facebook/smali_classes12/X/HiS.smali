.class public LX/HiS;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/google/android/gms/maps/model/LatLngBounds;


# instance fields
.field public final b:LX/1wc;

.field public final c:LX/03V;

.field public final d:LX/0tX;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:Ljava/util/Locale;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 2497410
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    const-wide v2, -0x3f99800000000000L    # -180.0

    const-wide v4, -0x3fa9800000000000L    # -90.0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    const-wide v4, 0x4066800000000000L    # 180.0

    const-wide v6, 0x4056800000000000L    # 90.0

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    sput-object v0, LX/HiS;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-void
.end method

.method public constructor <init>(LX/1wc;LX/03V;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0W9;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2497411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497412
    iput-object p1, p0, LX/HiS;->b:LX/1wc;

    .line 2497413
    iput-object p2, p0, LX/HiS;->c:LX/03V;

    .line 2497414
    iput-object p3, p0, LX/HiS;->d:LX/0tX;

    .line 2497415
    iput-object p4, p0, LX/HiS;->e:Ljava/util/concurrent/ExecutorService;

    .line 2497416
    invoke-virtual {p5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/HiS;->f:Ljava/util/Locale;

    .line 2497417
    return-void
.end method

.method public static b(LX/0QB;)LX/HiS;
    .locals 6

    .prologue
    .line 2497418
    new-instance v0, LX/HiS;

    invoke-static {p0}, LX/1wc;->b(LX/0QB;)LX/1wc;

    move-result-object v1

    check-cast v1, LX/1wc;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-direct/range {v0 .. v5}, LX/HiS;-><init>(LX/1wc;LX/03V;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0W9;)V

    .line 2497419
    return-object v0
.end method
