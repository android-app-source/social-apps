.class public LX/He8;
.super Landroid/widget/BaseAdapter;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Ljava/lang/Object;


# instance fields
.field public final c:Landroid/view/LayoutInflater;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/He6;

.field public f:LX/HeJ;

.field public g:LX/HeI;

.field public h:LX/HeL;

.field private i:LX/0kb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2489706
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/He8;->a:Ljava/lang/Object;

    .line 2489707
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/He8;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;LX/0kb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2489700
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2489701
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/He8;->d:Ljava/util/List;

    .line 2489702
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LX/He8;->c:Landroid/view/LayoutInflater;

    .line 2489703
    sget-object v0, LX/He6;->NO_QUERY:LX/He6;

    iput-object v0, p0, LX/He8;->e:LX/He6;

    .line 2489704
    iput-object p2, p0, LX/He8;->i:LX/0kb;

    .line 2489705
    return-void
.end method

.method private a(I)LX/He7;
    .locals 2

    .prologue
    .line 2489649
    iget-object v0, p0, LX/He8;->e:LX/He6;

    sget-object v1, LX/He6;->ONGOING:LX/He6;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/He8;->i:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/He8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2489650
    sget-object v0, LX/He7;->LOADING_MORE:LX/He7;

    .line 2489651
    :goto_0
    return-object v0

    .line 2489652
    :cond_1
    iget-object v0, p0, LX/He8;->e:LX/He6;

    sget-object v1, LX/He6;->COMPLETED:LX/He6;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/He8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2489653
    sget-object v0, LX/He7;->NO_RESULTS:LX/He7;

    goto :goto_0

    .line 2489654
    :cond_2
    sget-object v0, LX/He7;->RESULT:LX/He7;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;LX/He6;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;",
            "LX/He6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2489696
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/He8;->d:Ljava/util/List;

    .line 2489697
    iput-object p2, p0, LX/He8;->e:LX/He6;

    .line 2489698
    const v0, -0x1a19f9ab

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2489699
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2489690
    iget-object v0, p0, LX/He8;->e:LX/He6;

    sget-object v1, LX/He6;->ONGOING:LX/He6;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/He8;->i:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/He8;->e:LX/He6;

    sget-object v1, LX/He6;->NO_QUERY:LX/He6;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/He8;->e:LX/He6;

    sget-object v1, LX/He6;->COMPLETED:LX/He6;

    if-eq v0, v1, :cond_1

    .line 2489691
    :cond_0
    iget-object v0, p0, LX/He8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2489692
    :goto_0
    return v0

    .line 2489693
    :cond_1
    iget-object v0, p0, LX/He8;->e:LX/He6;

    sget-object v1, LX/He6;->COMPLETED:LX/He6;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/He8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2489694
    const/4 v0, 0x1

    goto :goto_0

    .line 2489695
    :cond_2
    iget-object v0, p0, LX/He8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2489683
    invoke-direct {p0, p1}, LX/He8;->a(I)LX/He7;

    move-result-object v0

    .line 2489684
    sget-object v1, LX/He5;->a:[I

    invoke-virtual {v0}, LX/He7;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2489685
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2489686
    :pswitch_0
    sget-object v0, LX/He8;->b:Ljava/lang/Object;

    .line 2489687
    :goto_0
    return-object v0

    .line 2489688
    :pswitch_1
    sget-object v0, LX/He8;->a:Ljava/lang/Object;

    goto :goto_0

    .line 2489689
    :pswitch_2
    iget-object v0, p0, LX/He8;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2489682
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2489681
    invoke-direct {p0, p1}, LX/He8;->a(I)LX/He7;

    move-result-object v0

    invoke-virtual {v0}, LX/He7;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2489656
    invoke-direct {p0, p1}, LX/He8;->a(I)LX/He7;

    move-result-object v0

    .line 2489657
    sget-object v1, LX/He7;->LOADING_MORE:LX/He7;

    if-ne v0, v1, :cond_1

    .line 2489658
    if-nez p2, :cond_0

    .line 2489659
    iget-object v0, p0, LX/He8;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f031537

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p3, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object p2, v0

    .line 2489660
    :cond_0
    :goto_0
    return-object p2

    .line 2489661
    :cond_1
    sget-object v1, LX/He7;->NO_RESULTS:LX/He7;

    if-ne v0, v1, :cond_2

    .line 2489662
    if-nez p2, :cond_0

    .line 2489663
    iget-object v0, p0, LX/He8;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f031538

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p3, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object p2, v0

    .line 2489664
    goto :goto_0

    .line 2489665
    :cond_2
    if-eqz p2, :cond_3

    .line 2489666
    check-cast p2, Lcom/facebook/uberbar/ui/UberbarResultView;

    .line 2489667
    :goto_1
    move-object v1, p2

    .line 2489668
    invoke-virtual {p0, p1}, LX/He8;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    invoke-virtual {v1, v0}, Lcom/facebook/uberbar/ui/UberbarResultView;->a(Lcom/facebook/search/api/SearchTypeaheadResult;)Lcom/facebook/uberbar/ui/UberbarResultView;

    move-result-object v0

    .line 2489669
    iput p1, v0, Lcom/facebook/uberbar/ui/UberbarResultView;->m:I

    .line 2489670
    move-object p2, v0

    .line 2489671
    goto :goto_0

    :cond_3
    new-instance v0, Lcom/facebook/uberbar/ui/UberbarResultView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/uberbar/ui/UberbarResultView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, LX/He8;->f:LX/HeJ;

    .line 2489672
    iput-object v1, v0, Lcom/facebook/uberbar/ui/UberbarResultView;->s:LX/HeJ;

    .line 2489673
    move-object v0, v0

    .line 2489674
    iget-object v1, p0, LX/He8;->g:LX/HeI;

    .line 2489675
    iput-object v1, v0, Lcom/facebook/uberbar/ui/UberbarResultView;->t:LX/HeI;

    .line 2489676
    move-object v0, v0

    .line 2489677
    iget-object v1, p0, LX/He8;->h:LX/HeL;

    .line 2489678
    iput-object v1, v0, Lcom/facebook/uberbar/ui/UberbarResultView;->u:LX/HeL;

    .line 2489679
    move-object p2, v0

    .line 2489680
    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2489655
    invoke-static {}, LX/He7;->values()[LX/He7;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
