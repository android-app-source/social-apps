.class public LX/HOg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

.field public final b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

.field public final c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

.field private final d:LX/CYE;

.field public final e:Ljava/lang/String;

.field public final f:Landroid/content/Context;

.field private final g:LX/1Ck;

.field public final h:LX/CY3;

.field private final i:LX/HNV;

.field private final j:LX/HNW;

.field public final k:Lcom/facebook/content/SecureContextHelper;

.field public final l:LX/17Y;

.field public final m:LX/0if;

.field private final n:LX/0kL;

.field public final o:LX/CYR;

.field private final p:Landroid/app/Activity;

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Ck;LX/CY3;LX/HNV;LX/HNW;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0if;LX/0kL;LX/CYR;LX/0Ot;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;LX/CYE;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 1
    .param p12    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # LX/CYE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1Ck;",
            "LX/CY3;",
            "LX/HNV;",
            "LX/HNW;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/0kL;",
            "LX/CYR;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;",
            "LX/CYE;",
            "Ljava/lang/String;",
            "Landroid/app/Activity;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2460629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2460630
    iput-object p1, p0, LX/HOg;->f:Landroid/content/Context;

    .line 2460631
    iput-object p2, p0, LX/HOg;->g:LX/1Ck;

    .line 2460632
    iput-object p3, p0, LX/HOg;->h:LX/CY3;

    .line 2460633
    iput-object p4, p0, LX/HOg;->i:LX/HNV;

    .line 2460634
    iput-object p5, p0, LX/HOg;->j:LX/HNW;

    .line 2460635
    iput-object p6, p0, LX/HOg;->k:Lcom/facebook/content/SecureContextHelper;

    .line 2460636
    iput-object p7, p0, LX/HOg;->l:LX/17Y;

    .line 2460637
    iput-object p8, p0, LX/HOg;->m:LX/0if;

    .line 2460638
    iput-object p12, p0, LX/HOg;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    .line 2460639
    iput-object p13, p0, LX/HOg;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    .line 2460640
    iput-object p14, p0, LX/HOg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2460641
    move-object/from16 v0, p15

    iput-object v0, p0, LX/HOg;->d:LX/CYE;

    .line 2460642
    move-object/from16 v0, p16

    iput-object v0, p0, LX/HOg;->e:Ljava/lang/String;

    .line 2460643
    iput-object p9, p0, LX/HOg;->n:LX/0kL;

    .line 2460644
    iput-object p10, p0, LX/HOg;->o:LX/CYR;

    .line 2460645
    move-object/from16 v0, p17

    iput-object v0, p0, LX/HOg;->p:Landroid/app/Activity;

    .line 2460646
    iput-object p11, p0, LX/HOg;->q:LX/0Ot;

    .line 2460647
    return-void
.end method

.method public static a$redex0(LX/HOg;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2460583
    iget-object v0, p0, LX/HOg;->n:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/HOg;->p:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08168b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2460584
    iget-object v0, p0, LX/HOg;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2460585
    return-void
.end method

.method public static c(LX/HOg;)V
    .locals 5

    .prologue
    .line 2460627
    iget-object v0, p0, LX/HOg;->p:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08168a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HOg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/HOg;->a(Ljava/lang/String;)V

    .line 2460628
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/HNo;)V
    .locals 10

    .prologue
    .line 2460591
    iget-object v0, p0, LX/HOg;->h:LX/CY3;

    iget-object v1, p0, LX/HOg;->e:Ljava/lang/String;

    .line 2460592
    iget-object v2, v0, LX/CY3;->a:LX/0Zb;

    sget-object v3, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_DELETE_CONFIRM:LX/CY4;

    invoke-static {v3, v1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2460593
    iget-object v0, p0, LX/HOg;->m:LX/0if;

    sget-object v1, LX/0ig;->ak:LX/0ih;

    const-string v2, "tap_delete_confirm_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2460594
    iget-object v0, p0, LX/HOg;->o:LX/CYR;

    invoke-virtual {v0, p1}, LX/CYR;->a(Landroid/view/View;)V

    .line 2460595
    iget-object v0, p0, LX/HOg;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    if-nez v0, :cond_0

    .line 2460596
    iget-object v0, p0, LX/HOg;->n:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/HOg;->p:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08168b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2460597
    iget-object v0, p0, LX/HOg;->p:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08168b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HOg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/HOg;->a(Ljava/lang/String;)V

    .line 2460598
    :goto_0
    return-void

    .line 2460599
    :cond_0
    iget-object v0, p0, LX/HOg;->d:LX/CYE;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HOg;->d:LX/CYE;

    .line 2460600
    iget-boolean v1, v0, LX/CYE;->mUseActionFlow:Z

    move v0, v1

    .line 2460601
    if-eqz v0, :cond_2

    .line 2460602
    iget-object v0, p0, LX/HOg;->g:LX/1Ck;

    const-string v7, "delete_call_to_action_key_action_framework"

    iget-object v1, p0, LX/HOg;->j:LX/HNW;

    iget-object v2, p0, LX/HOg;->e:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/HOg;->d:LX/CYE;

    .line 2460603
    iget-object v5, v4, LX/CYE;->mActionChannelType:Ljava/lang/String;

    move-object v4, v5

    .line 2460604
    iget-object v5, p0, LX/HOg;->d:LX/CYE;

    .line 2460605
    iget-object v6, v5, LX/CYE;->mActionType:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v5, v6

    .line 2460606
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/HOg;->d:LX/CYE;

    .line 2460607
    iget-object p1, v6, LX/CYE;->mActionId:Ljava/lang/String;

    move-object v6, p1

    .line 2460608
    new-instance v8, LX/4Hc;

    invoke-direct {v8}, LX/4Hc;-><init>()V

    invoke-virtual {v8, v5}, LX/4Hc;->a(Ljava/lang/String;)LX/4Hc;

    move-result-object v8

    .line 2460609
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 2460610
    invoke-virtual {v8, v6}, LX/4Hc;->b(Ljava/lang/String;)LX/4Hc;

    .line 2460611
    :cond_1
    new-instance v9, LX/4HZ;

    invoke-direct {v9}, LX/4HZ;-><init>()V

    invoke-virtual {v9, v4}, LX/4HZ;->b(Ljava/lang/String;)LX/4HZ;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/4HZ;->a(LX/4Hc;)LX/4HZ;

    move-result-object v8

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/4HZ;->a(Ljava/lang/String;)LX/4HZ;

    move-result-object v8

    .line 2460612
    invoke-static {}, LX/9Xl;->a()LX/9Xk;

    move-result-object v9

    const-string p1, "input"

    invoke-virtual {v9, p1, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v8

    check-cast v8, LX/9Xk;

    .line 2460613
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v8

    .line 2460614
    iget-object v9, v1, LX/HNW;->b:LX/0tX;

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    invoke-static {v8}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v1, v8

    .line 2460615
    new-instance v2, LX/HOe;

    invoke-direct {v2, p0, p2}, LX/HOe;-><init>(LX/HOg;LX/HNo;)V

    invoke-virtual {v0, v7, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2460616
    :cond_2
    iget-object v0, p0, LX/HOg;->g:LX/1Ck;

    const-string v1, "delete_call_to_action_key"

    iget-object v2, p0, LX/HOg;->i:LX/HNV;

    iget-object v3, p0, LX/HOg;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v2

    iget-object v3, p0, LX/HOg;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    .line 2460617
    new-instance v4, LX/4Hh;

    invoke-direct {v4}, LX/4Hh;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->n()Ljava/lang/String;

    move-result-object v5

    .line 2460618
    const-string v6, "id"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2460619
    move-object v4, v4

    .line 2460620
    const-string v5, "MOBILE_PAGE_PRESENCE_CALL_TO_ACTION"

    .line 2460621
    const-string v6, "source"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2460622
    move-object v4, v4

    .line 2460623
    new-instance v5, LX/8FJ;

    invoke-direct {v5}, LX/8FJ;-><init>()V

    move-object v5, v5

    .line 2460624
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/8FJ;

    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2460625
    iget-object v5, v2, LX/HNU;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2460626
    new-instance v3, LX/HOf;

    invoke-direct {v3, p0, p2}, LX/HOf;-><init>(LX/HOg;LX/HNo;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2460586
    iget-object v0, p0, LX/HOg;->m:LX/0if;

    sget-object v1, LX/0ig;->ak:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2460587
    iget-object v0, p0, LX/HOg;->p:Landroid/app/Activity;

    .line 2460588
    const/4 v1, -0x1

    invoke-static {p1}, LX/CYR;->e(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2460589
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2460590
    return-void
.end method
