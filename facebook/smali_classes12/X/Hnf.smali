.class public final LX/Hnf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 2503405
    const/4 v15, 0x0

    .line 2503406
    const/4 v14, 0x0

    .line 2503407
    const/4 v13, 0x0

    .line 2503408
    const/4 v12, 0x0

    .line 2503409
    const/4 v11, 0x0

    .line 2503410
    const/4 v10, 0x0

    .line 2503411
    const/4 v9, 0x0

    .line 2503412
    const/4 v8, 0x0

    .line 2503413
    const/4 v7, 0x0

    .line 2503414
    const/4 v6, 0x0

    .line 2503415
    const/4 v5, 0x0

    .line 2503416
    const/4 v4, 0x0

    .line 2503417
    const/4 v3, 0x0

    .line 2503418
    const/4 v2, 0x0

    .line 2503419
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 2503420
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2503421
    const/4 v2, 0x0

    .line 2503422
    :goto_0
    return v2

    .line 2503423
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2503424
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_b

    .line 2503425
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 2503426
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2503427
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 2503428
    const-string v17, "can_see_voice_switcher"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 2503429
    const/4 v5, 0x1

    .line 2503430
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 2503431
    :cond_2
    const-string v17, "can_viewer_comment"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 2503432
    const/4 v4, 0x1

    .line 2503433
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto :goto_1

    .line 2503434
    :cond_3
    const-string v17, "can_viewer_like"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 2503435
    const/4 v3, 0x1

    .line 2503436
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 2503437
    :cond_4
    const-string v17, "does_viewer_like"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 2503438
    const/4 v2, 0x1

    .line 2503439
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 2503440
    :cond_5
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 2503441
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 2503442
    :cond_6
    const-string v17, "interactors_friend"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 2503443
    invoke-static/range {p0 .. p1}, LX/Hna;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2503444
    :cond_7
    const-string v17, "interactors_not_friend"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 2503445
    invoke-static/range {p0 .. p1}, LX/Hnc;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2503446
    :cond_8
    const-string v17, "legacy_api_post_id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 2503447
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 2503448
    :cond_9
    const-string v17, "likers"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 2503449
    invoke-static/range {p0 .. p1}, LX/Hnd;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 2503450
    :cond_a
    const-string v17, "top_level_comments"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 2503451
    invoke-static/range {p0 .. p1}, LX/Hne;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2503452
    :cond_b
    const/16 v16, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2503453
    if-eqz v5, :cond_c

    .line 2503454
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->a(IZ)V

    .line 2503455
    :cond_c
    if-eqz v4, :cond_d

    .line 2503456
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->a(IZ)V

    .line 2503457
    :cond_d
    if-eqz v3, :cond_e

    .line 2503458
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->a(IZ)V

    .line 2503459
    :cond_e
    if-eqz v2, :cond_f

    .line 2503460
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 2503461
    :cond_f
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2503462
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2503463
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2503464
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2503465
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2503466
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2503467
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
