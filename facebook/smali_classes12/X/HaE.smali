.class public LX/HaE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/registration/model/RegistrationFormData;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/registration/annotations/RegInstance;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2483601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483602
    iput-object p1, p0, LX/HaE;->a:Ljava/lang/String;

    .line 2483603
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2483607
    check-cast p1, Lcom/facebook/registration/model/RegistrationFormData;

    .line 2483608
    invoke-virtual {p1}, Lcom/facebook/registration/model/RegistrationFormData;->k()Ljava/util/List;

    move-result-object v0

    .line 2483609
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_background"

    const-string v3, "false"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483610
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "reg_instance"

    iget-object v3, p0, LX/HaE;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483611
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483612
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/11I;->VALIDATE_REGISTRATION_DATA:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 2483613
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2483614
    move-object v1, v1

    .line 2483615
    const-string v2, "POST"

    .line 2483616
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2483617
    move-object v1, v1

    .line 2483618
    const-string v2, "method/user.validateregistrationdata"

    .line 2483619
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2483620
    move-object v1, v1

    .line 2483621
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 2483622
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2483623
    move-object v0, v1

    .line 2483624
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2483625
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2483626
    move-object v0, v0

    .line 2483627
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2483604
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2483605
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2483606
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
