.class public LX/Izr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lC;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2635772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635773
    iput-object p1, p0, LX/Izr;->a:LX/0lC;

    .line 2635774
    iput-object p2, p0, LX/Izr;->b:LX/03V;

    .line 2635775
    return-void
.end method

.method public static b(LX/0QB;)LX/Izr;
    .locals 3

    .prologue
    .line 2635776
    new-instance v2, LX/Izr;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/Izr;-><init>(LX/0lC;LX/03V;)V

    .line 2635777
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/CommerceOrder;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2635765
    const-string v0, "deserializeCommerceOrder"

    const v2, -0x7469d375

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635766
    if-nez p1, :cond_0

    .line 2635767
    const v0, -0x771500e2

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 2635768
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/Izr;->a:LX/0lC;

    const-class v2, Lcom/facebook/payments/p2p/model/CommerceOrder;

    invoke-virtual {v0, p1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/CommerceOrder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635769
    const v1, 0x1ca72031

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635770
    :catch_0
    :try_start_1
    iget-object v0, p0, LX/Izr;->b:LX/03V;

    const-string v2, "DbCommerceOrderSerialization"

    const-string v3, "IO Exception when reading CommerceOrder from JSON string."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635771
    const v0, -0x44026661

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x12619fa5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/payments/p2p/model/CommerceOrder;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/facebook/payments/p2p/model/CommerceOrder;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2635757
    const-string v0, "serializeCommerceOrder"

    const v1, 0x1a56ad82

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635758
    if-nez p1, :cond_0

    .line 2635759
    const v0, 0x54060221

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2635760
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/Izr;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2635761
    const v1, -0x65405107

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635762
    :catch_0
    move-exception v0

    .line 2635763
    :try_start_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635764
    :catchall_0
    move-exception v0

    const v1, 0x393cbdab    # 1.7999734E-4f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
