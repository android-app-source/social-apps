.class public final LX/IBf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/model/Event;

.field public final synthetic b:LX/IBg;


# direct methods
.method public constructor <init>(LX/IBg;Lcom/facebook/events/model/Event;)V
    .locals 0

    .prologue
    .line 2547365
    iput-object p1, p0, LX/IBf;->b:LX/IBg;

    iput-object p2, p0, LX/IBf;->a:Lcom/facebook/events/model/Event;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    .line 2547366
    iget-object v0, p0, LX/IBf;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->S()D

    move-result-wide v0

    iget-object v2, p0, LX/IBf;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->T()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v0

    invoke-virtual {v0}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 2547367
    iget-object v1, p0, LX/IBf;->b:LX/IBg;

    iget-object v2, p0, LX/IBf;->a:Lcom/facebook/events/model/Event;

    .line 2547368
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2547369
    iget-object v3, p0, LX/IBf;->a:Lcom/facebook/events/model/Event;

    .line 2547370
    iget-object p0, v3, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v3, p0

    .line 2547371
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "events_order_ride"

    invoke-static {v3, v4, v5, v6, v7}, LX/CK5;->a(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2547372
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2547373
    invoke-virtual {v5, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2547374
    iget-object v4, v1, LX/IBg;->h:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2547375
    iget-object v4, v1, LX/IBg;->b:LX/0Zb;

    const-string v5, "event_location_summary_order_ride"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2547376
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2547377
    const-string v5, "event_permalink"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "Event"

    invoke-virtual {v4, v5}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "source_module"

    iget-object v6, v1, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v6, v6, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "ref_module"

    iget-object v6, v1, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v6, v6, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2547378
    :cond_0
    return-void
.end method
