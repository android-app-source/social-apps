.class public final LX/HFj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageNameUpdateModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2444085
    iput-object p1, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iput-object p2, p0, LX/HFj;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2444086
    iget-object v0, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2444087
    iget-object v0, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->a:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->f:Ljava/lang/String;

    const-string v2, "page name check failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2444088
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2444089
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageNameUpdateModel;

    const/4 v3, 0x0

    .line 2444090
    if-eqz p1, :cond_0

    .line 2444091
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageNameUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageNameUpdateModel$PageModel;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2444092
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageNameUpdateModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2444093
    if-eqz v1, :cond_2

    .line 2444094
    iget-object v0, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    const v2, 0x7f0d22d2

    .line 2444095
    invoke-virtual {v0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object p1

    move-object v0, p1

    .line 2444096
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2444097
    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2444098
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2444099
    iget-object v0, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->e:LX/HFf;

    iget-object v1, p0, LX/HFj;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, LX/HFf;->a(ZLjava/lang/String;)V

    .line 2444100
    :cond_0
    :goto_1
    return-void

    .line 2444101
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageNameUpdateModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageNameUpdateModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageNameUpdateModel$PageModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2444102
    :cond_2
    iget-object v1, p0, LX/HFj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2444103
    iget-object v1, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->h:LX/HFJ;

    .line 2444104
    iput-object v0, v1, LX/HFJ;->b:Ljava/lang/String;

    .line 2444105
    iget-object v0, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->e:LX/HFf;

    const/4 v1, 0x1

    iget-object v2, p0, LX/HFj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/HFf;->a(ZLjava/lang/String;)V

    .line 2444106
    iget-object v0, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->e:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->f:Ljava/lang/String;

    const-string v2, "update_"

    invoke-virtual {v0, v1, v2}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444107
    iget-object v0, p0, LX/HFj;->b:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->l(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;)V

    goto :goto_1
.end method
