.class public final LX/JGL;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "Sounds"
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private final b:LX/0o1;

.field public c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/JG9;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:D
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2667014
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "currentTime"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "isPlaying"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "isPaused"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "loop"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "volume"

    aput-object v2, v0, v1

    sput-object v0, LX/JGL;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 2667015
    new-instance v0, LX/5p7;

    invoke-direct {v0}, LX/5p7;-><init>()V

    invoke-direct {p0, p1, v0}, LX/JGL;-><init>(LX/5pY;LX/0o1;)V

    .line 2667016
    return-void
.end method

.method private constructor <init>(LX/5pY;LX/0o1;)V
    .locals 1

    .prologue
    .line 2667017
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2667018
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JGL;->d:Z

    .line 2667019
    invoke-static {}, LX/5ps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/JGL;->c:Ljava/util/HashMap;

    .line 2667020
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGL;->e:Ljava/lang/String;

    .line 2667021
    iput-object p2, p0, LX/JGL;->b:LX/0o1;

    .line 2667022
    return-void
.end method

.method public static synthetic a(LX/JGL;Ljava/lang/String;)LX/JG9;
    .locals 1

    .prologue
    .line 2667023
    invoke-direct {p0, p1}, LX/JGL;->a(Ljava/lang/String;)LX/JG9;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)LX/JG9;
    .locals 6

    .prologue
    .line 2667024
    invoke-direct {p0}, LX/JGL;->u()V

    .line 2667025
    invoke-static {p1}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2667026
    iget-object v0, p0, LX/JGL;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2667027
    iget-object v0, p0, LX/JGL;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JG9;

    .line 2667028
    :goto_0
    return-object v0

    .line 2667029
    :cond_0
    invoke-static {v1}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v1}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2667030
    :goto_1
    if-eqz v0, :cond_2

    .line 2667031
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2667032
    iget-object v1, p0, LX/JGL;->b:LX/0o1;

    invoke-static {v0, p1, v1}, LX/JG9;->a(Landroid/content/Context;Ljava/lang/String;LX/0o1;)LX/JG9;

    move-result-object v0

    .line 2667033
    :goto_2
    iget-object v1, p0, LX/JGL;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2667034
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2667035
    :cond_2
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2667036
    const/4 v5, 0x0

    .line 2667037
    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    .line 2667038
    const/4 v2, 0x3

    invoke-virtual {v3, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2667039
    :try_start_0
    invoke-virtual {v3, v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2667040
    new-instance v2, LX/JG9;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, LX/JG9;-><init>(Landroid/media/MediaPlayer;Z)V

    .line 2667041
    new-instance v4, LX/JG8;

    invoke-direct {v4, v2}, LX/JG8;-><init>(LX/JG9;)V

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2667042
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2667043
    :goto_3
    move-object v0, v2

    .line 2667044
    goto :goto_2

    .line 2667045
    :catch_0
    move-exception v2

    .line 2667046
    const-string v3, "React"

    const-string v4, "ReactMediaPlayer failed to set data source"

    invoke-static {v3, v4, v2}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2667047
    new-instance v2, LX/JG9;

    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    invoke-direct {v2, v3, v5}, LX/JG9;-><init>(Landroid/media/MediaPlayer;Z)V

    goto :goto_3
.end method

.method private a(Ljava/lang/String;D)V
    .locals 2

    .prologue
    .line 2667048
    new-instance v0, LX/JGG;

    invoke-direct {v0, p0, p2, p3}, LX/JGG;-><init>(LX/JGL;D)V

    invoke-direct {p0, p1, v0}, LX/JGL;->a(Ljava/lang/String;LX/JGB;)V

    .line 2667049
    return-void
.end method

.method private a(Ljava/lang/String;LX/JGB;)V
    .locals 3

    .prologue
    .line 2667050
    new-instance v0, LX/JGJ;

    .line 2667051
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2667052
    invoke-direct {v0, p0, v1, p1, p2}, LX/JGJ;-><init>(LX/JGL;LX/5pX;Ljava/lang/String;LX/JGB;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/JGJ;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2667053
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 2667054
    new-instance v0, LX/JGK;

    .line 2667055
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2667056
    invoke-direct {v0, p0, v1}, LX/JGK;-><init>(LX/JGL;LX/5pX;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/JGK;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2667057
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 2667058
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2667059
    invoke-virtual {v0}, LX/5pX;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2667060
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2667061
    invoke-virtual {v0}, LX/5pX;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2667062
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not on an AsyncTask thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2667063
    :cond_1
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2667064
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2667065
    const/4 v0, 0x0

    :goto_0
    sget-object v2, LX/JGL;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2667066
    sget-object v2, LX/JGL;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667067
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2667068
    :cond_0
    const-string v0, "Properties"

    invoke-static {v0, v1}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final bM_()V
    .locals 3

    .prologue
    .line 2667001
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JGL;->d:Z

    .line 2667002
    iget-object v0, p0, LX/JGL;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2667003
    monitor-enter p0

    .line 2667004
    :try_start_0
    iget-wide v0, p0, LX/JGL;->f:D

    .line 2667005
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2667006
    iget-object v2, p0, LX/JGL;->e:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v1}, LX/JGL;->a(Ljava/lang/String;D)V

    .line 2667007
    :cond_0
    return-void

    .line 2667008
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final bN_()V
    .locals 2

    .prologue
    .line 2667009
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JGL;->d:Z

    .line 2667010
    iget-object v0, p0, LX/JGL;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2667011
    iget-object v0, p0, LX/JGL;->e:Ljava/lang/String;

    new-instance v1, LX/JGC;

    invoke-direct {v1, p0}, LX/JGC;-><init>(LX/JGL;)V

    invoke-direct {p0, v0, v1}, LX/JGL;->a(Ljava/lang/String;LX/JGB;)V

    .line 2667012
    :cond_0
    invoke-direct {p0}, LX/JGL;->i()V

    .line 2667013
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2666975
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2666980
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2666981
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2666982
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 2666983
    invoke-direct {p0}, LX/JGL;->i()V

    .line 2666984
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2666985
    const-string v0, "Sounds"

    return-object v0
.end method

.method public final getState(LX/5pG;LX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666986
    const-string v0, "uri"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/JGI;

    invoke-direct {v1, p0, p2, p3}, LX/JGI;-><init>(LX/JGL;LX/5pC;Lcom/facebook/react/bridge/Callback;)V

    invoke-direct {p0, v0, v1}, LX/JGL;->a(Ljava/lang/String;LX/JGB;)V

    .line 2666987
    return-void
.end method

.method public final pause(LX/5pG;Lcom/facebook/react/bridge/Callback;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666988
    const-string v0, "uri"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/JGE;

    invoke-direct {v1, p0, p1, p2}, LX/JGE;-><init>(LX/JGL;LX/5pG;Lcom/facebook/react/bridge/Callback;)V

    invoke-direct {p0, v0, v1}, LX/JGL;->a(Ljava/lang/String;LX/JGB;)V

    .line 2666989
    return-void
.end method

.method public final play(LX/5pG;D)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666990
    const-string v0, "uri"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2666991
    new-instance v1, LX/JGD;

    invoke-direct {v1, p0, p2, p3, v0}, LX/JGD;-><init>(LX/JGL;DLjava/lang/String;)V

    invoke-direct {p0, v0, v1}, LX/JGL;->a(Ljava/lang/String;LX/JGB;)V

    .line 2666992
    return-void
.end method

.method public final preloadFiles(LX/5pC;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666993
    new-instance v0, LX/JGH;

    .line 2666994
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2666995
    invoke-direct {v0, p0, v1, p1}, LX/JGH;-><init>(LX/JGL;LX/5pX;LX/5pC;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/JGH;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2666996
    return-void
.end method

.method public final setCurrentTime(LX/5pG;D)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666997
    const-string v0, "uri"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, LX/JGL;->a(Ljava/lang/String;D)V

    .line 2666998
    return-void
.end method

.method public final setPauseOnAppBackground(Ljava/lang/String;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666976
    if-eqz p2, :cond_0

    .line 2666977
    iput-object p1, p0, LX/JGL;->e:Ljava/lang/String;

    .line 2666978
    :goto_0
    return-void

    .line 2666979
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGL;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final setVolume(LX/5pG;F)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666999
    const-string v0, "uri"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/JGF;

    invoke-direct {v1, p0, p2}, LX/JGF;-><init>(LX/JGL;F)V

    invoke-direct {p0, v0, v1}, LX/JGL;->a(Ljava/lang/String;LX/JGB;)V

    .line 2667000
    return-void
.end method
