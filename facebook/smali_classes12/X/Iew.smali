.class public LX/Iew;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2599216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2599217
    iput-object p1, p0, LX/Iew;->a:LX/0SG;

    .line 2599218
    return-void
.end method

.method public static a(LX/0QB;)LX/Iew;
    .locals 1

    .prologue
    .line 2599199
    invoke-static {p0}, LX/Iew;->b(LX/0QB;)LX/Iew;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;)Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2599200
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->m()I

    move-result v1

    if-nez v1, :cond_1

    .line 2599201
    :cond_0
    :goto_0
    return-object v0

    .line 2599202
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->l()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2599203
    invoke-virtual {p0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;

    move-result-object v1

    .line 2599204
    if-eqz v1, :cond_0

    .line 2599205
    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitItemsModel$MessengerInboxItemAttachmentModel;->n()Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;

    move-result-object v1

    .line 2599206
    invoke-virtual {v1}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->k()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2599207
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    .line 2599208
    sget-object v0, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v1}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 2599209
    invoke-virtual {v1}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/Iew;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;)Lcom/facebook/user/model/Name;

    move-result-object v0

    .line 2599210
    iput-object v0, v2, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2599211
    const/4 v0, 0x1

    .line 2599212
    iput-boolean v0, v2, LX/0XI;->z:Z

    .line 2599213
    invoke-virtual {v1}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2599214
    iput-object v0, v2, LX/0XI;->n:Ljava/lang/String;

    .line 2599215
    new-instance v0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->l()I

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;-><init>(Lcom/facebook/user/model/User;I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;)Lcom/facebook/user/model/Name;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2599188
    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;->m_()Ljava/lang/String;

    move-result-object v4

    .line 2599189
    if-eqz v4, :cond_1

    .line 2599190
    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    move-object v2, v1

    :goto_0
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;

    .line 2599191
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->n_()I

    move-result v7

    .line 2599192
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->n_()I

    move-result v8

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->a()I

    move-result v9

    add-int/2addr v8, v9

    .line 2599193
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v9

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v9, v10, :cond_0

    .line 2599194
    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    .line 2599195
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2599196
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNamePartFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v0

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->LAST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v0, v9, :cond_3

    .line 2599197
    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    goto :goto_1

    :cond_1
    move-object v2, v1

    .line 2599198
    :cond_2
    new-instance v0, Lcom/facebook/user/model/Name;

    invoke-direct {v0, v2, v1, v4}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_3
    move-object v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/Iew;
    .locals 2

    .prologue
    .line 2599186
    new-instance v1, LX/Iew;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-direct {v1, v0}, LX/Iew;-><init>(LX/0SG;)V

    .line 2599187
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel;)Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2599171
    new-instance v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    invoke-virtual {p1}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel;->a()Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel$MessengerContactsYouMayKnowModel;

    move-result-object v1

    .line 2599172
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2599173
    invoke-virtual {v1}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel$MessengerContactsYouMayKnowModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;

    .line 2599174
    new-instance v7, LX/0XI;

    invoke-direct {v7}, LX/0XI;-><init>()V

    .line 2599175
    sget-object p1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v2}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, p1, v1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 2599176
    invoke-virtual {v2}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object p1

    invoke-static {p1}, LX/Iew;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;)Lcom/facebook/user/model/Name;

    move-result-object p1

    .line 2599177
    iput-object p1, v7, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2599178
    invoke-virtual {v2}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object p1

    .line 2599179
    iput-object p1, v7, LX/0XI;->n:Ljava/lang/String;

    .line 2599180
    invoke-virtual {v7}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v7

    move-object v7, v7

    .line 2599181
    if-eqz v7, :cond_0

    invoke-virtual {v2}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->k()Z

    move-result p1

    if-nez p1, :cond_0

    .line 2599182
    new-instance p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-virtual {v2}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactYouMayKnowInfoModel;->l()I

    move-result v2

    invoke-direct {p1, v7, v2}, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;-><init>(Lcom/facebook/user/model/User;I)V

    invoke-virtual {v4, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2599183
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2599184
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2599185
    iget-object v2, p0, LX/Iew;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;-><init>(LX/0Px;J)V

    return-object v0
.end method
