.class public final LX/HS7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0am;

.field public final synthetic d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;JLjava/lang/String;LX/0am;)V
    .locals 0

    .prologue
    .line 2466550
    iput-object p1, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    iput-wide p2, p0, LX/HS7;->a:J

    iput-object p4, p0, LX/HS7;->b:Ljava/lang/String;

    iput-object p5, p0, LX/HS7;->c:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x5a9f8db0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2466551
    iget-object v0, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    iget-wide v2, p0, LX/HS7;->a:J

    invoke-virtual {v0, v2, v3}, LX/HS4;->a(J)V

    .line 2466552
    iget-object v0, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    iget-wide v2, p0, LX/HS7;->a:J

    invoke-static {v0, v2, v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->b(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;J)Landroid/content/Intent;

    move-result-object v2

    .line 2466553
    if-eqz v2, :cond_0

    iget-object v0, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->h:LX/0Uh;

    const/16 v3, 0x71

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2466554
    :cond_0
    iget-object v0, p0, LX/HS7;->b:Ljava/lang/String;

    iget-wide v2, p0, LX/HS7;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2466555
    iget-object v2, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    iget-object v2, v2, LX/HS4;->e:LX/17Y;

    iget-object v3, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2466556
    const-string v0, "uri_unhandled_report_category_name"

    const-string v3, "PageFacewebUriNotHandled"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2466557
    iget-object v0, p0, LX/HS7;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2466558
    iget-object v0, p0, LX/HS7;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HRS;

    invoke-virtual {v0}, LX/HRS;->a()V

    .line 2466559
    :cond_1
    iget-object v0, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    iget-object v0, v0, LX/HS4;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2466560
    :goto_0
    const v0, -0x4b9a1bc8

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2466561
    :cond_2
    iget-object v0, p0, LX/HS7;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2466562
    iget-object v0, p0, LX/HS7;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HRS;

    .line 2466563
    iget-object v3, v0, LX/HRS;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    const/4 v4, 0x1

    .line 2466564
    iput-boolean v4, v3, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->C:Z

    .line 2466565
    :cond_3
    iget-object v0, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    iget-object v0, v0, LX/HS4;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HS7;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
