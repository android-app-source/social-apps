.class public LX/JUe;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUg;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JUe",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JUg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699416
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2699417
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JUe;->b:LX/0Zi;

    .line 2699418
    iput-object p1, p0, LX/JUe;->a:LX/0Ot;

    .line 2699419
    return-void
.end method

.method public static a(LX/0QB;)LX/JUe;
    .locals 4

    .prologue
    .line 2699420
    const-class v1, LX/JUe;

    monitor-enter v1

    .line 2699421
    :try_start_0
    sget-object v0, LX/JUe;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699422
    sput-object v2, LX/JUe;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699423
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699424
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699425
    new-instance v3, LX/JUe;

    const/16 p0, 0x2080

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JUe;-><init>(LX/0Ot;)V

    .line 2699426
    move-object v0, v3

    .line 2699427
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699428
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699429
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699430
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2699431
    check-cast p2, LX/JUd;

    .line 2699432
    iget-object v0, p0, LX/JUe;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JUg;

    iget-object v1, p2, LX/JUd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JUd;->b:Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    iget-object v3, p2, LX/JUd;->c:LX/1Pr;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/JUg;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;LX/1Pr;)LX/1Dg;

    move-result-object v0

    .line 2699433
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2699434
    invoke-static {}, LX/1dS;->b()V

    .line 2699435
    const/4 v0, 0x0

    return-object v0
.end method
