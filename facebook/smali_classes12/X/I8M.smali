.class public LX/I8M;
.super Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;
.source ""


# instance fields
.field public j:LX/11R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Ble;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Bm1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

.field public q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field public r:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2541062
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;)V

    .line 2541063
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/I8M;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v3

    check-cast v3, LX/11R;

    const/16 v4, 0x2eb

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/Ble;->b(LX/0QB;)LX/Ble;

    move-result-object v5

    check-cast v5, LX/Ble;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v6

    check-cast v6, LX/1nQ;

    invoke-static {v0}, LX/Bm1;->a(LX/0QB;)LX/Bm1;

    move-result-object v0

    check-cast v0, LX/Bm1;

    iput-object v3, v2, LX/I8M;->j:LX/11R;

    iput-object v4, v2, LX/I8M;->k:LX/0Ot;

    iput-object v5, v2, LX/I8M;->l:LX/Ble;

    iput-object v6, v2, LX/I8M;->m:LX/1nQ;

    iput-object v0, v2, LX/I8M;->n:LX/Bm1;

    .line 2541064
    const/4 v1, -0x2

    .line 2541065
    new-instance v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-direct {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/I8M;->o:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2541066
    new-instance v0, LX/6VC;

    invoke-direct {v0, v1, v1}, LX/6VC;-><init>(II)V

    .line 2541067
    sget-object v1, LX/6VB;->SUBTITLE:LX/6VB;

    iput-object v1, v0, LX/6VC;->e:LX/6VB;

    .line 2541068
    iget-object v1, p0, LX/I8M;->o:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p0}, LX/I8M;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00a3

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTextColor(I)V

    .line 2541069
    iget-object v1, p0, LX/I8M;->o:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p0}, LX/I8M;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00a1

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 2541070
    iget-object v1, p0, LX/I8M;->o:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p0}, LX/I8M;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1453

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 2541071
    iget-object v1, p0, LX/I8M;->o:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, LX/I8M;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2541072
    new-instance v0, LX/I8L;

    invoke-direct {v0, p0}, LX/I8L;-><init>(LX/I8M;)V

    invoke-virtual {p0, v0}, LX/I8M;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2541073
    return-void
.end method

.method public static a$redex0(LX/I8M;LX/Blc;)V
    .locals 4

    .prologue
    .line 2541032
    iget-object v0, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v0, :cond_0

    .line 2541033
    :goto_0
    return-void

    .line 2541034
    :cond_0
    iget-object v0, p0, LX/I8M;->r:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v0, :cond_1

    .line 2541035
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v1, Lcom/facebook/events/common/EventActionContext;->d:Lcom/facebook/events/common/EventActionContext;

    invoke-direct {v0, v1}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;)V

    iput-object v0, p0, LX/I8M;->r:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2541036
    :cond_1
    iget-object v0, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/Event;->b(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v0

    iget-object v1, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v1

    invoke-direct {p0}, LX/I8M;->e()Z

    move-result v2

    invoke-static {v0, v1, v2}, LX/I9i;->a(ZZZ)LX/0Px;

    move-result-object v0

    .line 2541037
    iget-object v1, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, LX/I9i;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;LX/0Px;Z)LX/0Px;

    move-result-object v0

    .line 2541038
    new-instance v1, LX/Blg;

    iget-object v2, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Blg;-><init>(Ljava/lang/String;)V

    .line 2541039
    iget-object v2, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v2

    .line 2541040
    iput-object v2, v1, LX/Blg;->c:Ljava/lang/String;

    .line 2541041
    move-object v2, v1

    .line 2541042
    iget-object v3, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v3

    .line 2541043
    iput-object v3, v2, LX/Blg;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 2541044
    move-object v2, v2

    .line 2541045
    iget-object v3, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    .line 2541046
    iput-object v3, v2, LX/Blg;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 2541047
    move-object v2, v2

    .line 2541048
    iget-object v3, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    .line 2541049
    iput-object v3, v2, LX/Blg;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2541050
    move-object v2, v2

    .line 2541051
    invoke-direct {p0}, LX/I8M;->e()Z

    move-result v3

    .line 2541052
    iput-boolean v3, v2, LX/Blg;->g:Z

    .line 2541053
    move-object v2, v2

    .line 2541054
    iget-object v3, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v3}, LX/I9i;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v3

    .line 2541055
    iput-boolean v3, v2, LX/Blg;->i:Z

    .line 2541056
    move-object v2, v2

    .line 2541057
    iput-object v0, v2, LX/Blg;->h:LX/0Px;

    .line 2541058
    move-object v0, v2

    .line 2541059
    iget-object v2, p0, LX/I8M;->r:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2541060
    iput-object v2, v0, LX/Blg;->a:Lcom/facebook/events/common/EventActionContext;

    .line 2541061
    iget-object v0, p0, LX/I8M;->l:LX/Ble;

    invoke-virtual {p0}, LX/I8M;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1}, LX/Blg;->a()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v1

    invoke-virtual {v0, v2, v1, p1}, LX/Ble;->a(Landroid/content/Context;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/Blc;)V

    goto/16 :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 2540993
    iget-object v0, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Lcom/facebook/events/model/Event;

    move-result-object v0

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2540994
    iput-object p1, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    .line 2540995
    iput-object p2, p0, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2540996
    iput-object p3, p0, LX/I8M;->r:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2540997
    iget-object v0, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2540998
    iget-object v0, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    new-instance v2, Landroid/text/SpannableString;

    invoke-virtual {v1, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2540999
    iget-object v0, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    const v4, 0x328e79d9

    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 2541000
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {v3, v1, p2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_6

    .line 2541001
    :cond_0
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2541002
    :cond_1
    iget-object v0, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2541003
    iget-object v0, p0, LX/I8M;->o:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 2541004
    const/4 v3, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 2541005
    :goto_2
    move v1, v2

    .line 2541006
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 2541007
    :cond_3
    iget-object v0, p0, LX/I8M;->j:LX/11R;

    sget-object v1, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    iget-object v2, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->j()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2541008
    iget-object v1, p0, LX/I8M;->o:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2541009
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2541010
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v7}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 2541011
    iget-object v0, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel$ActorsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2541012
    iget-object v0, p0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel$ActorsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2541013
    :cond_4
    sget-object v0, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2541014
    invoke-virtual {p0}, LX/I8M;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0097

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/I8M;->setBackgroundColor(I)V

    .line 2541015
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setEnableActionButton(Z)V

    .line 2541016
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2541017
    return-void

    .line 2541018
    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto/16 :goto_0

    .line 2541019
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {v3, v1, p2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_3
    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2541020
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, p3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v4, v3, p3}, LX/15i;->j(II)I

    move-result v8

    invoke-virtual {v4, v3, p3}, LX/15i;->j(II)I

    move-result p1

    invoke-virtual {v4, v3, p2}, LX/15i;->j(II)I

    move-result v3

    add-int/2addr v3, p1

    const/16 v4, 0x21

    invoke-interface {v2, v5, v8, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_4

    .line 2541021
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_3

    .line 2541022
    :sswitch_0
    const-string v4, "plan_mall_activity"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v3, v2

    goto/16 :goto_1

    :sswitch_1
    const-string v4, "admin_plan_mall_activity"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto/16 :goto_1

    :sswitch_2
    const-string v4, "plan_user_joined"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x2

    goto/16 :goto_1

    :sswitch_3
    const-string v4, "plan_user_associated"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x3

    goto/16 :goto_1

    :sswitch_4
    const-string v4, "plan_user_declined"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x4

    goto/16 :goto_1

    :sswitch_5
    const-string v4, "plan_edited"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x5

    goto/16 :goto_1

    :sswitch_6
    const-string v4, "feedback_reaction_generic"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x6

    goto/16 :goto_1

    :sswitch_7
    const-string v4, "event_mall_comment"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x7

    goto/16 :goto_1

    :sswitch_8
    const-string v4, "event_comment_mention"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v3, 0x8

    goto/16 :goto_1

    :sswitch_9
    const-string v4, "event_mall_reply"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v3, 0x9

    goto/16 :goto_1

    .line 2541023
    :pswitch_0
    const v2, 0x7f020985

    goto/16 :goto_2

    .line 2541024
    :pswitch_1
    const v2, 0x7f020859

    goto/16 :goto_2

    .line 2541025
    :pswitch_2
    const v2, 0x7f02085d

    goto/16 :goto_2

    .line 2541026
    :pswitch_3
    const v2, 0x7f02085f

    goto/16 :goto_2

    .line 2541027
    :pswitch_4
    const v2, 0x7f020954

    goto/16 :goto_2

    .line 2541028
    :pswitch_5
    const v2, 0x7f0208fc

    goto/16 :goto_2

    .line 2541029
    :pswitch_6
    const v2, 0x7f02080a

    goto/16 :goto_2

    .line 2541030
    :pswitch_7
    const v2, 0x7f020929

    goto/16 :goto_2

    .line 2541031
    :pswitch_8
    const v2, 0x7f02080a

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6eff5cc5 -> :sswitch_6
        -0x54fe9ecc -> :sswitch_1
        -0x48e50f1c -> :sswitch_3
        -0x4240443c -> :sswitch_9
        -0x389ac79b -> :sswitch_8
        -0x186729bc -> :sswitch_0
        0x1238778c -> :sswitch_4
        0x4428c8b9 -> :sswitch_7
        0x573561ff -> :sswitch_5
        0x6c0d0227 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
