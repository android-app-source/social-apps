.class public LX/Idt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Idf;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/Idk;

.field private c:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2598072
    const-class v0, LX/Idt;

    sput-object v0, LX/Idt;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2598073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2598074
    iput-object p2, p0, LX/Idt;->c:Landroid/net/Uri;

    .line 2598075
    new-instance v0, LX/Idu;

    invoke-direct {v0, p0, p1, p2}, LX/Idu;-><init>(LX/Idf;Landroid/content/ContentResolver;Landroid/net/Uri;)V

    iput-object v0, p0, LX/Idt;->b:LX/Idk;

    .line 2598076
    return-void
.end method


# virtual methods
.method public final a(I)LX/Idk;
    .locals 1

    .prologue
    .line 2598077
    if-nez p1, :cond_0

    iget-object v0, p0, LX/Idt;->b:LX/Idk;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)LX/Idk;
    .locals 1

    .prologue
    .line 2598078
    iget-object v0, p0, LX/Idt;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Idt;->b:LX/Idk;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2598079
    iput-object v0, p0, LX/Idt;->b:LX/Idk;

    .line 2598080
    iput-object v0, p0, LX/Idt;->c:Landroid/net/Uri;

    .line 2598081
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2598082
    const/4 v0, 0x1

    return v0
.end method
