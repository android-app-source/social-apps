.class public LX/J0X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637250
    return-void
.end method

.method public static a(LX/0QB;)LX/J0X;
    .locals 1

    .prologue
    .line 2637276
    new-instance v0, LX/J0X;

    invoke-direct {v0}, LX/J0X;-><init>()V

    .line 2637277
    move-object v0, v0

    .line 2637278
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2637253
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;

    .line 2637254
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2637255
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "transfer_id"

    .line 2637256
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2637257
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637258
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637259
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "decline_payment"

    .line 2637260
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2637261
    move-object v1, v1

    .line 2637262
    const-string v2, "POST"

    .line 2637263
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2637264
    move-object v1, v1

    .line 2637265
    const-string v2, "%s/p2p_declined_transfers"

    .line 2637266
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2637267
    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2637268
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2637269
    move-object v1, v1

    .line 2637270
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2637271
    move-object v0, v1

    .line 2637272
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2637273
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637274
    move-object v0, v0

    .line 2637275
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2637251
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637252
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
