.class public LX/Ipx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;


# instance fields
.field public final irisSeqId:Ljava/lang/Long;

.field public final newReceiverStatus:Ljava/lang/Integer;

.field public final newSenderStatus:Ljava/lang/Integer;

.field public final newStatus:Ljava/lang/Integer;

.field public final timestampMs:Ljava/lang/Long;

.field public final transferFbId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0xa

    const/16 v3, 0x8

    .line 2616431
    new-instance v0, LX/1sv;

    const-string v1, "DeltaTransferStatus"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipx;->b:LX/1sv;

    .line 2616432
    new-instance v0, LX/1sw;

    const-string v1, "transferFbId"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipx;->c:LX/1sw;

    .line 2616433
    new-instance v0, LX/1sw;

    const-string v1, "timestampMs"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipx;->d:LX/1sw;

    .line 2616434
    new-instance v0, LX/1sw;

    const-string v1, "newStatus"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipx;->e:LX/1sw;

    .line 2616435
    new-instance v0, LX/1sw;

    const-string v1, "newSenderStatus"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipx;->f:LX/1sw;

    .line 2616436
    new-instance v0, LX/1sw;

    const-string v1, "newReceiverStatus"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipx;->g:LX/1sw;

    .line 2616437
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipx;->h:LX/1sw;

    .line 2616438
    sput-boolean v5, LX/Ipx;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2616423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2616424
    iput-object p1, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    .line 2616425
    iput-object p2, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    .line 2616426
    iput-object p3, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    .line 2616427
    iput-object p4, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    .line 2616428
    iput-object p5, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    .line 2616429
    iput-object p6, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    .line 2616430
    return-void
.end method

.method private static a(LX/Ipx;)V
    .locals 3

    .prologue
    .line 2616416
    iget-object v0, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/Iq1;->a:LX/1sn;

    iget-object v1, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2616417
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'newStatus\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2616418
    :cond_0
    iget-object v0, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/Iq8;->a:LX/1sn;

    iget-object v1, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2616419
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'newSenderStatus\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2616420
    :cond_1
    iget-object v0, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    sget-object v0, LX/Iq6;->a:LX/1sn;

    iget-object v1, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2616421
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'newReceiverStatus\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2616422
    :cond_2
    return-void
.end method

.method public static b(LX/1su;)LX/Ipx;
    .locals 12

    .prologue
    const/16 v11, 0xa

    const/16 v10, 0x8

    const/4 v6, 0x0

    .line 2616389
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v5, v6

    move-object v4, v6

    move-object v3, v6

    move-object v2, v6

    move-object v1, v6

    .line 2616390
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 2616391
    iget-byte v7, v0, LX/1sw;->b:B

    if-eqz v7, :cond_6

    .line 2616392
    iget-short v7, v0, LX/1sw;->c:S

    sparse-switch v7, :sswitch_data_0

    .line 2616393
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2616394
    :sswitch_0
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v11, :cond_0

    .line 2616395
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 2616396
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2616397
    :sswitch_1
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v11, :cond_1

    .line 2616398
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 2616399
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2616400
    :sswitch_2
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_2

    .line 2616401
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 2616402
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2616403
    :sswitch_3
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_3

    .line 2616404
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 2616405
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2616406
    :sswitch_4
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_4

    .line 2616407
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    .line 2616408
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2616409
    :sswitch_5
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v11, :cond_5

    .line 2616410
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto :goto_0

    .line 2616411
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616412
    :cond_6
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2616413
    new-instance v0, LX/Ipx;

    invoke-direct/range {v0 .. v6}, LX/Ipx;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2616414
    invoke-static {v0}, LX/Ipx;->a(LX/Ipx;)V

    .line 2616415
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x3e8 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2616304
    if-eqz p2, :cond_d

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2616305
    :goto_0
    if-eqz p2, :cond_e

    const-string v0, "\n"

    move-object v3, v0

    .line 2616306
    :goto_1
    if-eqz p2, :cond_f

    const-string v0, " "

    move-object v1, v0

    .line 2616307
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "DeltaTransferStatus"

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2616308
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616309
    const-string v0, "("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616310
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616311
    const/4 v0, 0x1

    .line 2616312
    iget-object v6, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    if-eqz v6, :cond_0

    .line 2616313
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616314
    const-string v0, "transferFbId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616315
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616316
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616317
    iget-object v0, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    if-nez v0, :cond_10

    .line 2616318
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v0, v2

    .line 2616319
    :cond_0
    iget-object v6, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    if-eqz v6, :cond_2

    .line 2616320
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616321
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616322
    const-string v0, "timestampMs"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616323
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616324
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616325
    iget-object v0, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    if-nez v0, :cond_11

    .line 2616326
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v0, v2

    .line 2616327
    :cond_2
    iget-object v6, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    if-eqz v6, :cond_5

    .line 2616328
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616329
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616330
    const-string v0, "newStatus"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616331
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616332
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616333
    iget-object v0, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    if-nez v0, :cond_12

    .line 2616334
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    :goto_5
    move v0, v2

    .line 2616335
    :cond_5
    iget-object v6, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    if-eqz v6, :cond_8

    .line 2616336
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616337
    :cond_6
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616338
    const-string v0, "newSenderStatus"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616339
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616340
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616341
    iget-object v0, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    if-nez v0, :cond_14

    .line 2616342
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    :goto_6
    move v0, v2

    .line 2616343
    :cond_8
    iget-object v6, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    if-eqz v6, :cond_19

    .line 2616344
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616345
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616346
    const-string v0, "newReceiverStatus"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616347
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616348
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616349
    iget-object v0, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    if-nez v0, :cond_16

    .line 2616350
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616351
    :cond_a
    :goto_7
    iget-object v0, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 2616352
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616353
    :cond_b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616354
    const-string v0, "irisSeqId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616355
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616356
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616357
    iget-object v0, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_18

    .line 2616358
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616359
    :cond_c
    :goto_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616360
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616361
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2616362
    :cond_d
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2616363
    :cond_e
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2616364
    :cond_f
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 2616365
    :cond_10
    iget-object v0, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2616366
    :cond_11
    iget-object v0, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2616367
    :cond_12
    sget-object v0, LX/Iq1;->b:Ljava/util/Map;

    iget-object v6, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2616368
    if-eqz v0, :cond_13

    .line 2616369
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616370
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616371
    :cond_13
    iget-object v6, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2616372
    if-eqz v0, :cond_4

    .line 2616373
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2616374
    :cond_14
    sget-object v0, LX/Iq8;->b:Ljava/util/Map;

    iget-object v6, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2616375
    if-eqz v0, :cond_15

    .line 2616376
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616377
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616378
    :cond_15
    iget-object v6, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2616379
    if-eqz v0, :cond_7

    .line 2616380
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2616381
    :cond_16
    sget-object v0, LX/Iq6;->b:Ljava/util/Map;

    iget-object v6, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2616382
    if-eqz v0, :cond_17

    .line 2616383
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616384
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616385
    :cond_17
    iget-object v6, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2616386
    if-eqz v0, :cond_a

    .line 2616387
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2616388
    :cond_18
    iget-object v0, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_19
    move v2, v0

    goto/16 :goto_7
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2616221
    invoke-static {p0}, LX/Ipx;->a(LX/Ipx;)V

    .line 2616222
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2616223
    iget-object v0, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2616224
    iget-object v0, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2616225
    sget-object v0, LX/Ipx;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616226
    iget-object v0, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2616227
    :cond_0
    iget-object v0, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2616228
    iget-object v0, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2616229
    sget-object v0, LX/Ipx;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616230
    iget-object v0, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2616231
    :cond_1
    iget-object v0, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2616232
    iget-object v0, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2616233
    sget-object v0, LX/Ipx;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616234
    iget-object v0, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2616235
    :cond_2
    iget-object v0, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2616236
    iget-object v0, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2616237
    sget-object v0, LX/Ipx;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616238
    iget-object v0, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2616239
    :cond_3
    iget-object v0, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2616240
    iget-object v0, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2616241
    sget-object v0, LX/Ipx;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616242
    iget-object v0, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2616243
    :cond_4
    iget-object v0, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 2616244
    iget-object v0, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 2616245
    sget-object v0, LX/Ipx;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616246
    iget-object v0, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2616247
    :cond_5
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2616248
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2616249
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2616254
    if-nez p1, :cond_1

    .line 2616255
    :cond_0
    :goto_0
    return v0

    .line 2616256
    :cond_1
    instance-of v1, p1, LX/Ipx;

    if-eqz v1, :cond_0

    .line 2616257
    check-cast p1, LX/Ipx;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2616258
    if-nez p1, :cond_3

    .line 2616259
    :cond_2
    :goto_1
    move v0, v2

    .line 2616260
    goto :goto_0

    .line 2616261
    :cond_3
    iget-object v0, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_10

    move v0, v1

    .line 2616262
    :goto_2
    iget-object v3, p1, LX/Ipx;->transferFbId:Ljava/lang/Long;

    if-eqz v3, :cond_11

    move v3, v1

    .line 2616263
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2616264
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616265
    iget-object v0, p0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipx;->transferFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616266
    :cond_5
    iget-object v0, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 2616267
    :goto_4
    iget-object v3, p1, LX/Ipx;->timestampMs:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 2616268
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2616269
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616270
    iget-object v0, p0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipx;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616271
    :cond_7
    iget-object v0, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    move v0, v1

    .line 2616272
    :goto_6
    iget-object v3, p1, LX/Ipx;->newStatus:Ljava/lang/Integer;

    if-eqz v3, :cond_15

    move v3, v1

    .line 2616273
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2616274
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616275
    iget-object v0, p0, LX/Ipx;->newStatus:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipx;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616276
    :cond_9
    iget-object v0, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    move v0, v1

    .line 2616277
    :goto_8
    iget-object v3, p1, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    if-eqz v3, :cond_17

    move v3, v1

    .line 2616278
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2616279
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616280
    iget-object v0, p0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616281
    :cond_b
    iget-object v0, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_18

    move v0, v1

    .line 2616282
    :goto_a
    iget-object v3, p1, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    if-eqz v3, :cond_19

    move v3, v1

    .line 2616283
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2616284
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616285
    iget-object v0, p0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616286
    :cond_d
    iget-object v0, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 2616287
    :goto_c
    iget-object v3, p1, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 2616288
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2616289
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616290
    iget-object v0, p0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_f
    move v2, v1

    .line 2616291
    goto/16 :goto_1

    :cond_10
    move v0, v2

    .line 2616292
    goto/16 :goto_2

    :cond_11
    move v3, v2

    .line 2616293
    goto/16 :goto_3

    :cond_12
    move v0, v2

    .line 2616294
    goto/16 :goto_4

    :cond_13
    move v3, v2

    .line 2616295
    goto/16 :goto_5

    :cond_14
    move v0, v2

    .line 2616296
    goto :goto_6

    :cond_15
    move v3, v2

    .line 2616297
    goto :goto_7

    :cond_16
    move v0, v2

    .line 2616298
    goto :goto_8

    :cond_17
    move v3, v2

    .line 2616299
    goto :goto_9

    :cond_18
    move v0, v2

    .line 2616300
    goto :goto_a

    :cond_19
    move v3, v2

    .line 2616301
    goto :goto_b

    :cond_1a
    move v0, v2

    .line 2616302
    goto :goto_c

    :cond_1b
    move v3, v2

    .line 2616303
    goto :goto_d
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2616253
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2616250
    sget-boolean v0, LX/Ipx;->a:Z

    .line 2616251
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipx;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2616252
    return-object v0
.end method
