.class public final LX/HtO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HtP;


# direct methods
.method public constructor <init>(LX/HtP;)V
    .locals 0

    .prologue
    .line 2516074
    iput-object p1, p0, LX/HtO;->a:LX/HtP;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2516075
    iget-object v0, p0, LX/HtO;->a:LX/HtP;

    .line 2516076
    iget-object v1, v0, LX/HtP;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 2516077
    iget-object v3, v0, LX/HtP;->c:LX/IC3;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0ip;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 2516078
    iget-object p0, v3, LX/IC3;->a:LX/0Zb;

    const-string p1, "minutiae_preview_fetch_failed"

    invoke-static {p1, v2, v1}, LX/IC3;->a(Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2516079
    iget-object v1, v0, LX/HtP;->i:LX/Hsu;

    if-eqz v1, :cond_0

    .line 2516080
    iget-object v1, v0, LX/HtP;->i:LX/Hsu;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/Hsu;->setVisibility(I)V

    .line 2516081
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2516082
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2516083
    iget-object v0, p0, LX/HtO;->a:LX/HtP;

    .line 2516084
    iget-object v1, v0, LX/HtP;->i:LX/Hsu;

    if-nez v1, :cond_0

    .line 2516085
    :goto_0
    return-void

    .line 2516086
    :cond_0
    if-nez p1, :cond_1

    .line 2516087
    iget-object v1, v0, LX/HtP;->i:LX/Hsu;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/Hsu;->setVisibility(I)V

    goto :goto_0

    .line 2516088
    :cond_1
    iget-object v1, v0, LX/HtP;->d:LX/HtQ;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/HtQ;->b(Ljava/util/List;)LX/HtM;

    move-result-object v1

    .line 2516089
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2516090
    iget-object v2, v0, LX/HtP;->i:LX/Hsu;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/Hsu;->setLoadingIndicatorVisibility(Z)V

    .line 2516091
    iget-object v2, v0, LX/HtP;->i:LX/Hsu;

    .line 2516092
    iget-object p0, v2, LX/Hsu;->a:Landroid/widget/FrameLayout;

    move-object v2, p0

    .line 2516093
    iget-object p0, v0, LX/HtP;->i:LX/Hsu;

    .line 2516094
    iget-object v0, p0, LX/Hsu;->a:Landroid/widget/FrameLayout;

    move-object p0, v0

    .line 2516095
    invoke-interface {v1, p1, p0}, LX/HtM;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
