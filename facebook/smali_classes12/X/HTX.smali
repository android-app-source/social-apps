.class public final LX/HTX;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final synthetic l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

.field public m:I

.field public n:Landroid/view/View;

.field public o:Landroid/widget/TextView;

.field public p:Landroid/widget/TextView;

.field public q:Landroid/widget/TextView;

.field public r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public t:Lcom/facebook/fbui/glyph/GlyphView;

.field public u:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2469548
    iput-object p1, p0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    .line 2469549
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2469550
    iput-object p2, p0, LX/HTX;->n:Landroid/view/View;

    .line 2469551
    const v0, 0x7f0d0d7c

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HTX;->o:Landroid/widget/TextView;

    .line 2469552
    const v0, 0x7f0d0d7d

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HTX;->p:Landroid/widget/TextView;

    .line 2469553
    const v0, 0x7f0d0d80

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HTX;->q:Landroid/widget/TextView;

    .line 2469554
    const v0, 0x7f0d0d7b

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/HTX;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2469555
    const v0, 0x7f0d0d7e

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/HTX;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2469556
    const v0, 0x7f0d0d7f

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/HTX;->t:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2469557
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x46f2038c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2469558
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    iget-object v2, p0, LX/HTX;->u:Ljava/lang/String;

    .line 2469559
    iput-object v2, v0, LX/89k;->b:Ljava/lang/String;

    .line 2469560
    move-object v0, v0

    .line 2469561
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v2

    .line 2469562
    iget-object v0, p0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hy;

    invoke-interface {v0, v2}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v2

    .line 2469563
    iget-object v0, p0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->j:Landroid/content/Context;

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2469564
    const v0, -0x1850f474

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
