.class public LX/IuD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final g:Ljava/lang/Object;


# instance fields
.field private final b:LX/DoY;

.field public final c:LX/2Ox;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DoJ;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/IuH;

.field private final f:LX/Dob;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2625850
    const-class v0, LX/IuD;

    sput-object v0, LX/IuD;->a:Ljava/lang/Class;

    .line 2625851
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/IuD;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/DoY;LX/2Ox;LX/0Or;LX/IuH;LX/Dob;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DoY;",
            "LX/2Ox;",
            "LX/0Or",
            "<",
            "LX/DoJ;",
            ">;",
            "LX/IuH;",
            "LX/Dob;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625844
    iput-object p1, p0, LX/IuD;->b:LX/DoY;

    .line 2625845
    iput-object p2, p0, LX/IuD;->c:LX/2Ox;

    .line 2625846
    iput-object p3, p0, LX/IuD;->d:LX/0Or;

    .line 2625847
    iput-object p4, p0, LX/IuD;->e:LX/IuH;

    .line 2625848
    iput-object p5, p0, LX/IuD;->f:LX/Dob;

    .line 2625849
    return-void
.end method

.method public static a(LX/0QB;)LX/IuD;
    .locals 13

    .prologue
    .line 2625814
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2625815
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2625816
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2625817
    if-nez v1, :cond_0

    .line 2625818
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2625819
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2625820
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2625821
    sget-object v1, LX/IuD;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2625822
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2625823
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2625824
    :cond_1
    if-nez v1, :cond_4

    .line 2625825
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2625826
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2625827
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2625828
    new-instance v7, LX/IuD;

    invoke-static {v0}, LX/DoY;->a(LX/0QB;)LX/DoY;

    move-result-object v8

    check-cast v8, LX/DoY;

    invoke-static {v0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v9

    check-cast v9, LX/2Ox;

    const/16 v10, 0x2a0c

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/IuH;->b(LX/0QB;)LX/IuH;

    move-result-object v11

    check-cast v11, LX/IuH;

    invoke-static {v0}, LX/Dob;->a(LX/0QB;)LX/Dob;

    move-result-object v12

    check-cast v12, LX/Dob;

    invoke-direct/range {v7 .. v12}, LX/IuD;-><init>(LX/DoY;LX/2Ox;LX/0Or;LX/IuH;LX/Dob;)V

    .line 2625829
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2625830
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2625831
    if-nez v1, :cond_2

    .line 2625832
    sget-object v0, LX/IuD;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuD;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2625833
    :goto_1
    if-eqz v0, :cond_3

    .line 2625834
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2625835
    :goto_3
    check-cast v0, LX/IuD;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2625836
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2625837
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2625838
    :catchall_1
    move-exception v0

    .line 2625839
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2625840
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2625841
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2625842
    :cond_2
    :try_start_8
    sget-object v0, LX/IuD;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuD;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/IuD;LX/DoK;LX/DoU;)Lcom/facebook/messaging/model/messages/Message;
    .locals 9

    .prologue
    .line 2625801
    iget-wide v0, p2, LX/DoU;->e:J

    .line 2625802
    iget-object v2, p0, LX/IuD;->e:LX/IuH;

    invoke-virtual {p1}, LX/DoK;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/IuH;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2625803
    iget-object v3, p2, LX/DoU;->f:Ljava/lang/String;

    .line 2625804
    iget-wide v4, p2, LX/DoU;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    .line 2625805
    new-instance v5, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v6, Lcom/facebook/user/model/UserKey;

    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v6, v7, v4}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-direct {v5, v6, v4}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2625806
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v4

    .line 2625807
    invoke-virtual {v4, v3}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    .line 2625808
    iput-object v3, v4, LX/6f7;->n:Ljava/lang/String;

    .line 2625809
    iput-object v2, v4, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2625810
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 2625811
    iput-wide v0, v4, LX/6f7;->c:J

    .line 2625812
    iput-object v5, v4, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2625813
    invoke-virtual {v4}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/DoK;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2625797
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 2625798
    new-instance v1, Landroid/util/JsonWriter;

    invoke-direct {v1, v0}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 2625799
    invoke-virtual {p0, v1}, LX/DoK;->a(Landroid/util/JsonWriter;)V

    .line 2625800
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)LX/DoK;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2625780
    iget-object v0, p0, LX/IuD;->e:LX/IuH;

    invoke-virtual {v0, p1}, LX/IuH;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 2625781
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/IuH;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2625782
    :goto_0
    iget-object v3, p0, LX/IuD;->b:LX/DoY;

    invoke-virtual {v3, v2, v0}, LX/DoY;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2625783
    if-nez v0, :cond_1

    .line 2625784
    sget-object v0, LX/IuD;->a:Ljava/lang/Class;

    const-string v2, "No crypto state retrieved for session %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2625785
    :goto_1
    return-object v1

    :cond_0
    move-object v0, v1

    .line 2625786
    goto :goto_0

    .line 2625787
    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 2625788
    new-instance v3, Landroid/util/JsonReader;

    invoke-direct {v3, v2}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 2625789
    iget-object v0, p0, LX/IuD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DoJ;

    .line 2625790
    iget-object v2, v0, LX/DoJ;->b:LX/DoL;

    .line 2625791
    new-instance v4, LX/DoT;

    iget-object p0, v2, LX/DoL;->a:LX/DoO;

    iget-object p1, v2, LX/DoL;->b:LX/DoP;

    iget-object v0, v2, LX/DoL;->c:LX/DoR;

    invoke-direct {v4, v3, p0, p1, v0}, LX/DoT;-><init>(Landroid/util/JsonReader;LX/DoO;LX/DoP;LX/DoR;)V

    move-object v2, v4

    .line 2625792
    move-object v0, v2

    .line 2625793
    invoke-virtual {v3}, Landroid/util/JsonReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 2625794
    goto :goto_1

    .line 2625795
    :catch_0
    move-exception v0

    .line 2625796
    sget-object v2, LX/IuD;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(LX/DoK;LX/DoU;[B[BZ)V
    .locals 6

    .prologue
    .line 2625762
    iget-object v0, p0, LX/IuD;->f:LX/Dob;

    iget-wide v2, p2, LX/DoU;->a:J

    invoke-virtual {v0, v2, v3}, LX/Dob;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2625763
    new-instance v0, LX/DoG;

    const-string v1, "Thread participant not found for pre-key message"

    invoke-direct {v0, v1}, LX/DoG;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2625764
    :cond_0
    invoke-static {p0, p1, p2}, LX/IuD;->a(LX/IuD;LX/DoK;LX/DoU;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2625765
    :try_start_0
    iget-object v0, p0, LX/IuD;->c:LX/2Ox;

    invoke-static {p1}, LX/IuD;->a(LX/DoK;)Ljava/lang/String;

    move-result-object v4

    if-eqz p5, :cond_2

    iget-object v5, p2, LX/DoU;->b:Ljava/lang/String;

    :goto_0
    move-object v2, p3

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LX/2Ox;->a(Lcom/facebook/messaging/model/messages/Message;[B[BLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2625766
    :goto_1
    if-nez p5, :cond_1

    .line 2625767
    iget-object v0, p0, LX/IuD;->c:LX/2Ox;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    iget-object v1, p2, LX/DoU;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, LX/2Ox;->a(JLjava/lang/String;)V

    .line 2625768
    :cond_1
    return-void

    .line 2625769
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 2625770
    :catch_0
    move-exception v0

    .line 2625771
    sget-object v2, LX/IuD;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(LX/DoK;Z)V
    .locals 4

    .prologue
    .line 2625772
    invoke-virtual {p1}, LX/DoK;->c()Ljava/lang/String;

    move-result-object v0

    .line 2625773
    iget-object v1, p0, LX/IuD;->e:LX/IuH;

    invoke-virtual {p1}, LX/DoK;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/IuH;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2625774
    if-eqz p2, :cond_0

    invoke-static {v0}, LX/IuH;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2625775
    :goto_0
    :try_start_0
    iget-object v2, p0, LX/IuD;->c:LX/2Ox;

    invoke-static {p1}, LX/IuD;->a(LX/DoK;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v0, v3}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2625776
    :goto_1
    return-void

    .line 2625777
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2625778
    :catch_0
    move-exception v0

    .line 2625779
    sget-object v1, LX/IuD;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
