.class public final enum LX/IyK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IyK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IyK;

.field public static final enum CART_CUSTOM_ITEM:LX/IyK;

.field public static final enum CART_ITEM:LX/IyK;

.field public static final enum SEARCH_ADD_ITEM:LX/IyK;

.field public static final enum SEARCH_ITEM:LX/IyK;


# instance fields
.field private final mSelectable:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2633207
    new-instance v0, LX/IyK;

    const-string v1, "SEARCH_ITEM"

    invoke-direct {v0, v1, v2, v3}, LX/IyK;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/IyK;->SEARCH_ITEM:LX/IyK;

    .line 2633208
    new-instance v0, LX/IyK;

    const-string v1, "SEARCH_ADD_ITEM"

    invoke-direct {v0, v1, v3, v3}, LX/IyK;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/IyK;->SEARCH_ADD_ITEM:LX/IyK;

    .line 2633209
    new-instance v0, LX/IyK;

    const-string v1, "CART_ITEM"

    invoke-direct {v0, v1, v4, v2}, LX/IyK;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/IyK;->CART_ITEM:LX/IyK;

    .line 2633210
    new-instance v0, LX/IyK;

    const-string v1, "CART_CUSTOM_ITEM"

    invoke-direct {v0, v1, v5, v2}, LX/IyK;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/IyK;->CART_CUSTOM_ITEM:LX/IyK;

    .line 2633211
    const/4 v0, 0x4

    new-array v0, v0, [LX/IyK;

    sget-object v1, LX/IyK;->SEARCH_ITEM:LX/IyK;

    aput-object v1, v0, v2

    sget-object v1, LX/IyK;->SEARCH_ADD_ITEM:LX/IyK;

    aput-object v1, v0, v3

    sget-object v1, LX/IyK;->CART_ITEM:LX/IyK;

    aput-object v1, v0, v4

    sget-object v1, LX/IyK;->CART_CUSTOM_ITEM:LX/IyK;

    aput-object v1, v0, v5

    sput-object v0, LX/IyK;->$VALUES:[LX/IyK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 2633212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2633213
    iput-boolean p3, p0, LX/IyK;->mSelectable:Z

    .line 2633214
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IyK;
    .locals 1

    .prologue
    .line 2633215
    const-class v0, LX/IyK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IyK;

    return-object v0
.end method

.method public static values()[LX/IyK;
    .locals 1

    .prologue
    .line 2633216
    sget-object v0, LX/IyK;->$VALUES:[LX/IyK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IyK;

    return-object v0
.end method


# virtual methods
.method public final isSelectable()Z
    .locals 1

    .prologue
    .line 2633217
    iget-boolean v0, p0, LX/IyK;->mSelectable:Z

    return v0
.end method
