.class public LX/JDT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/J9y;

.field private final b:LX/1Uf;

.field private final c:LX/JCa;

.field private final d:LX/3GL;


# direct methods
.method public constructor <init>(LX/J9y;LX/1Uf;LX/JCa;LX/3GL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2664080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2664081
    iput-object p1, p0, LX/JDT;->a:LX/J9y;

    .line 2664082
    iput-object p2, p0, LX/JDT;->b:LX/1Uf;

    .line 2664083
    iput-object p3, p0, LX/JDT;->c:LX/JCa;

    .line 2664084
    iput-object p4, p0, LX/JDT;->d:LX/3GL;

    .line 2664085
    return-void
.end method

.method private a(LX/0Px;)Landroid/text/Spannable;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;",
            ">;)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2664064
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2664065
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v5

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;

    .line 2664066
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2664067
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2664068
    :cond_0
    iget-object v0, p0, LX/JDT;->d:LX/3GL;

    invoke-virtual {v0, v2}, LX/3GL;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 2664069
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    .line 2664070
    const v6, 0x7f0a096d

    .line 2664071
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v11

    move v10, v5

    move v2, v5

    :goto_1
    if-ge v10, v11, :cond_2

    invoke-virtual {p1, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;

    .line 2664072
    invoke-virtual {v9}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2664073
    invoke-virtual {v9}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;->v_()Ljava/lang/String;

    move-result-object v0

    .line 2664074
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2664075
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    .line 2664076
    iget-object v0, p0, LX/JDT;->b:LX/1Uf;

    add-int v3, v2, v12

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v9}, LX/1Uf;->a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;)V

    .line 2664077
    iget-object v0, p0, LX/JDT;->d:LX/3GL;

    invoke-virtual {v0}, LX/3GL;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v12

    add-int/2addr v2, v0

    .line 2664078
    :cond_1
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_1

    .line 2664079
    :cond_2
    return-object v4
.end method

.method public static a(Lcom/facebook/resources/ui/FbTextView;I)V
    .locals 2

    .prologue
    .line 2664058
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2664059
    if-eqz v1, :cond_0

    instance-of v0, v1, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2664060
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2664061
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2664062
    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2664063
    return-void
.end method

.method public static b(LX/0QB;)LX/JDT;
    .locals 5

    .prologue
    .line 2664056
    new-instance v4, LX/JDT;

    invoke-static {p0}, LX/J9y;->a(LX/0QB;)LX/J9y;

    move-result-object v0

    check-cast v0, LX/J9y;

    invoke-static {p0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v1

    check-cast v1, LX/1Uf;

    invoke-static {p0}, LX/JCa;->a(LX/0QB;)LX/JCa;

    move-result-object v2

    check-cast v2, LX/JCa;

    invoke-static {p0}, LX/3GL;->b(LX/0QB;)LX/3GL;

    move-result-object v3

    check-cast v3, LX/3GL;

    invoke-direct {v4, v0, v1, v2, v3}, LX/JDT;-><init>(LX/J9y;LX/1Uf;LX/JCa;LX/3GL;)V

    .line 2664057
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/resources/ui/FbTextView;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/resources/ui/FbTextView;",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2664050
    invoke-direct {p0, p2}, LX/JDT;->a(LX/0Px;)Landroid/text/Spannable;

    move-result-object v0

    .line 2664051
    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 2664052
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 2664053
    iget-object v0, p0, LX/JDT;->c:LX/JCa;

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2664054
    const v0, 0x800003

    invoke-static {p1, v0}, LX/JDT;->a(Lcom/facebook/resources/ui/FbTextView;I)V

    .line 2664055
    return-void
.end method

.method public final b(Lcom/facebook/resources/ui/FbTextView;Landroid/view/View;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2664038
    invoke-virtual {p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_0

    .line 2664039
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2664040
    :goto_0
    return-void

    .line 2664041
    :cond_0
    invoke-virtual {p1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2664042
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2664043
    invoke-virtual {p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2664044
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004c

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 2664045
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2664046
    new-instance v0, LX/JDS;

    invoke-direct {v0, p0, p3}, LX/JDS;-><init>(LX/JDT;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)V

    .line 2664047
    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2664048
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2664049
    const/16 v0, 0x11

    invoke-static {p1, v0}, LX/JDT;->a(Lcom/facebook/resources/ui/FbTextView;I)V

    goto :goto_0
.end method
