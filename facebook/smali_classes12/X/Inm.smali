.class public final LX/Inm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V
    .locals 0

    .prologue
    .line 2610830
    iput-object p1, p0, LX/Inm;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2610826
    iget-object v0, p0, LX/Inm;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->z(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    .line 2610827
    iget-object v0, p0, LX/Inm;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->t:LX/03V;

    const-string v1, "EnterPaymentValueActivity"

    const-string v2, "Payment request failed to fetch"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2610828
    iget-object v0, p0, LX/Inm;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->v(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    .line 2610829
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610831
    check-cast p1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2610832
    iget-object v0, p0, LX/Inm;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->z(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    .line 2610833
    new-instance v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;-><init>()V

    .line 2610834
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2610835
    const-string v2, "payment_flow_type"

    sget-object v3, LX/5g0;->REQUEST_ACK:LX/5g0;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2610836
    const-string v2, "payment_request"

    invoke-static {v1, v2, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2610837
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2610838
    move-object v0, v0

    .line 2610839
    iget-object v1, p0, LX/Inm;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-static {v1, v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->a$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2610840
    return-void
.end method
