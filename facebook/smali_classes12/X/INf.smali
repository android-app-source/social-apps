.class public LX/INf;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/IL3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public c:Lcom/facebook/fig/footer/FigFooter;

.field public d:Lcom/facebook/fig/header/FigHeader;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2571047
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2571048
    const/4 p1, 0x0

    .line 2571049
    const v0, 0x7f030300

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2571050
    const-class v0, LX/INf;

    invoke-static {v0, p0}, LX/INf;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2571051
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/INf;->setOrientation(I)V

    .line 2571052
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/INf;->setGravity(I)V

    .line 2571053
    invoke-virtual {p0}, LX/INf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2571054
    const v1, 0x7f020c6f

    invoke-virtual {p0, v1}, LX/INf;->setBackgroundResource(I)V

    .line 2571055
    invoke-virtual {p0, p1, v0, p1, p1}, LX/INf;->setPadding(IIII)V

    .line 2571056
    const v0, 0x7f0d0a52

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/INf;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2571057
    const v0, 0x7f0d090e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/header/FigHeader;

    iput-object v0, p0, LX/INf;->d:Lcom/facebook/fig/header/FigHeader;

    .line 2571058
    iget-object v0, p0, LX/INf;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, LX/INf;->getContext()Landroid/content/Context;

    invoke-direct {v1, p1, p1}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2571059
    const v0, 0x7f0d0a53

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/footer/FigFooter;

    iput-object v0, p0, LX/INf;->c:Lcom/facebook/fig/footer/FigFooter;

    .line 2571060
    iget-object v0, p0, LX/INf;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/INe;

    invoke-direct {v1, p0}, LX/INe;-><init>(LX/INf;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 2571061
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/INf;

    new-instance v0, LX/IL3;

    const-class p0, LX/INy;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/INy;

    invoke-direct {v0, p0}, LX/IL3;-><init>(LX/INy;)V

    move-object v1, v0

    check-cast v1, LX/IL3;

    iput-object v1, p1, LX/INf;->a:LX/IL3;

    return-void
.end method
