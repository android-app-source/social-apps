.class public final LX/HKo;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HKp;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/HKp;


# direct methods
.method public constructor <init>(LX/HKp;)V
    .locals 1

    .prologue
    .line 2455117
    iput-object p1, p0, LX/HKo;->d:LX/HKp;

    .line 2455118
    move-object v0, p1

    .line 2455119
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2455120
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2455099
    const-string v0, "PagesInsightsOverviewCardHeaderUnitComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2455100
    if-ne p0, p1, :cond_1

    .line 2455101
    :cond_0
    :goto_0
    return v0

    .line 2455102
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2455103
    goto :goto_0

    .line 2455104
    :cond_3
    check-cast p1, LX/HKo;

    .line 2455105
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2455106
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2455107
    if-eq v2, v3, :cond_0

    .line 2455108
    iget-object v2, p0, LX/HKo;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HKo;->a:Ljava/lang/String;

    iget-object v3, p1, LX/HKo;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2455109
    goto :goto_0

    .line 2455110
    :cond_5
    iget-object v2, p1, LX/HKo;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2455111
    :cond_6
    iget-object v2, p0, LX/HKo;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HKo;->b:Ljava/lang/String;

    iget-object v3, p1, LX/HKo;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2455112
    goto :goto_0

    .line 2455113
    :cond_8
    iget-object v2, p1, LX/HKo;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2455114
    :cond_9
    iget-object v2, p0, LX/HKo;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/HKo;->c:Ljava/lang/String;

    iget-object v3, p1, LX/HKo;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2455115
    goto :goto_0

    .line 2455116
    :cond_a
    iget-object v2, p1, LX/HKo;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
