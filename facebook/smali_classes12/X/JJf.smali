.class public final LX/JJf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4oV;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/ScrollingAwareScrollView;

.field public final synthetic b:Lcom/facebook/react/bridge/Callback;

.field public final synthetic c:LX/JJg;


# direct methods
.method public constructor <init>(LX/JJg;Lcom/facebook/widget/ScrollingAwareScrollView;Lcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 2679686
    iput-object p1, p0, LX/JJf;->c:LX/JJg;

    iput-object p2, p0, LX/JJf;->a:Lcom/facebook/widget/ScrollingAwareScrollView;

    iput-object p3, p0, LX/JJf;->b:Lcom/facebook/react/bridge/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 5

    .prologue
    .line 2679687
    iget-object v0, p0, LX/JJf;->a:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v1, 0x7f0d2db5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2679688
    if-nez v0, :cond_0

    .line 2679689
    :goto_0
    return-void

    .line 2679690
    :cond_0
    iget-object v1, p0, LX/JJf;->a:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/ScrollingAwareScrollView;->b(LX/4oV;)V

    .line 2679691
    iget-object v1, p0, LX/JJf;->b:Lcom/facebook/react/bridge/Callback;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/JJf;->a:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v4}, Lcom/facebook/widget/ScrollingAwareScrollView;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, LX/JJf;->a:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v4}, Lcom/facebook/widget/ScrollingAwareScrollView;->getHeight()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
