.class public final enum LX/IzC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IzC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IzC;

.field public static final enum CARD_DOES_NOT_EXIST:LX/IzC;

.field public static final enum CARD_EXISTS_AND_IN_CACHE:LX/IzC;

.field public static final enum CARD_EXISTS_BUT_NOT_IN_CACHE:LX/IzC;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2634325
    new-instance v0, LX/IzC;

    const-string v1, "CARD_EXISTS_AND_IN_CACHE"

    invoke-direct {v0, v1, v2}, LX/IzC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IzC;->CARD_EXISTS_AND_IN_CACHE:LX/IzC;

    .line 2634326
    new-instance v0, LX/IzC;

    const-string v1, "CARD_DOES_NOT_EXIST"

    invoke-direct {v0, v1, v3}, LX/IzC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IzC;->CARD_DOES_NOT_EXIST:LX/IzC;

    .line 2634327
    new-instance v0, LX/IzC;

    const-string v1, "CARD_EXISTS_BUT_NOT_IN_CACHE"

    invoke-direct {v0, v1, v4}, LX/IzC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IzC;->CARD_EXISTS_BUT_NOT_IN_CACHE:LX/IzC;

    .line 2634328
    const/4 v0, 0x3

    new-array v0, v0, [LX/IzC;

    sget-object v1, LX/IzC;->CARD_EXISTS_AND_IN_CACHE:LX/IzC;

    aput-object v1, v0, v2

    sget-object v1, LX/IzC;->CARD_DOES_NOT_EXIST:LX/IzC;

    aput-object v1, v0, v3

    sget-object v1, LX/IzC;->CARD_EXISTS_BUT_NOT_IN_CACHE:LX/IzC;

    aput-object v1, v0, v4

    sput-object v0, LX/IzC;->$VALUES:[LX/IzC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2634330
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IzC;
    .locals 1

    .prologue
    .line 2634331
    const-class v0, LX/IzC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IzC;

    return-object v0
.end method

.method public static values()[LX/IzC;
    .locals 1

    .prologue
    .line 2634329
    sget-object v0, LX/IzC;->$VALUES:[LX/IzC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IzC;

    return-object v0
.end method
