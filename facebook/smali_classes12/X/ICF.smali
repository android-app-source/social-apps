.class public final LX/ICF;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ICG;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/ICG;


# direct methods
.method public constructor <init>(LX/ICG;)V
    .locals 1

    .prologue
    .line 2549085
    iput-object p1, p0, LX/ICF;->f:LX/ICG;

    .line 2549086
    move-object v0, p1

    .line 2549087
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2549088
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2549089
    const-string v0, "StoryAttachmentSaveButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2549090
    if-ne p0, p1, :cond_1

    .line 2549091
    :cond_0
    :goto_0
    return v0

    .line 2549092
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2549093
    goto :goto_0

    .line 2549094
    :cond_3
    check-cast p1, LX/ICF;

    .line 2549095
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2549096
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2549097
    if-eq v2, v3, :cond_0

    .line 2549098
    iget-object v2, p0, LX/ICF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ICF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/ICF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2549099
    goto :goto_0

    .line 2549100
    :cond_5
    iget-object v2, p1, LX/ICF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2549101
    :cond_6
    iget-object v2, p0, LX/ICF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ICF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/ICF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2549102
    goto :goto_0

    .line 2549103
    :cond_8
    iget-object v2, p1, LX/ICF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2549104
    :cond_9
    iget-object v2, p0, LX/ICF;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/ICF;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ICF;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2549105
    goto :goto_0

    .line 2549106
    :cond_b
    iget-object v2, p1, LX/ICF;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_a

    .line 2549107
    :cond_c
    iget-object v2, p0, LX/ICF;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/ICF;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ICF;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2549108
    goto :goto_0

    .line 2549109
    :cond_e
    iget-object v2, p1, LX/ICF;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_d

    .line 2549110
    :cond_f
    iget-object v2, p0, LX/ICF;->e:LX/1Po;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/ICF;->e:LX/1Po;

    iget-object v3, p1, LX/ICF;->e:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2549111
    goto :goto_0

    .line 2549112
    :cond_10
    iget-object v2, p1, LX/ICF;->e:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
