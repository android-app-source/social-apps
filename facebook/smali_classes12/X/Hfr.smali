.class public LX/Hfr;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hfp;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hfv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2492431
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hfr;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hfv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2492521
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2492522
    iput-object p1, p0, LX/Hfr;->b:LX/0Ot;

    .line 2492523
    return-void
.end method

.method public static a(LX/1De;LX/1De;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2492524
    const v0, -0x1befdfa

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Hfr;
    .locals 4

    .prologue
    .line 2492487
    const-class v1, LX/Hfr;

    monitor-enter v1

    .line 2492488
    :try_start_0
    sget-object v0, LX/Hfr;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2492489
    sput-object v2, LX/Hfr;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2492490
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2492491
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2492492
    new-instance v3, LX/Hfr;

    const/16 p0, 0x38d0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hfr;-><init>(LX/0Ot;)V

    .line 2492493
    move-object v0, v3

    .line 2492494
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2492495
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hfr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2492496
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2492497
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2492498
    check-cast p2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492499
    iget-object v0, p0, LX/Hfr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hfv;

    iget-object v2, p2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    iget-object v4, p2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v5, p2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->d:Ljava/lang/Boolean;

    move-object v1, p1

    .line 2492500
    iget-object v6, v0, LX/Hfv;->b:LX/1vg;

    invoke-virtual {v6, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v6

    const v7, 0x7f020723

    invoke-virtual {v6, v7}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    const v7, 0x7f0a00fc

    invoke-virtual {v6, v7}, LX/2xv;->j(I)LX/2xv;

    move-result-object v6

    invoke-virtual {v6}, LX/1n6;->b()LX/1dc;

    move-result-object v11

    .line 2492501
    invoke-static {v0, v1, v2, v5}, LX/Hfv;->a(LX/Hfv;LX/1De;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;Ljava/lang/Boolean;)LX/1Dh;

    move-result-object p0

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v4

    move-object v10, v5

    .line 2492502
    const/4 v5, 0x2

    .line 2492503
    invoke-static {v7}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p1

    const p2, 0x7f0a00d5

    invoke-interface {p1, p2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object p1

    const p2, 0x7f0b2349

    invoke-interface {p1, v5, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p1

    invoke-static {v7}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p2

    .line 2492504
    invoke-static {v8, v10}, LX/Hfv;->a(Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2492505
    iget-object v0, v6, LX/Hfv;->b:LX/1vg;

    invoke-virtual {v0, v7}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    const v2, 0x7f02071f

    invoke-virtual {v0, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, LX/2xv;->j(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 2492506
    :goto_0
    move-object v0, v0

    .line 2492507
    invoke-virtual {p2, v0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2}, LX/1X5;->c()LX/1Di;

    move-result-object p2

    const/4 v0, 0x6

    const v2, 0x7f0b234a

    invoke-interface {p2, v0, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object p2

    invoke-static {v7, v7}, LX/Hfr;->a(LX/1De;LX/1De;)LX/1dQ;

    move-result-object v0

    invoke-interface {p2, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p2

    invoke-interface {p1, p2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p2

    invoke-static {v7}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v0

    iget-object p1, v6, LX/Hfv;->a:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1Ad;

    const/4 v3, 0x0

    .line 2492508
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2492509
    invoke-virtual {v8}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->n()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2492510
    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    .line 2492511
    invoke-virtual {v8}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->n()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v4, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2492512
    :goto_2
    move-object v2, v2

    .line 2492513
    invoke-virtual {p1, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object p1

    invoke-virtual {p1, v9}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p1

    invoke-virtual {p1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p1

    invoke-virtual {v0, p1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object p1

    invoke-static {v7}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v0

    const v2, 0x7f0a00d6

    invoke-virtual {v0, v2}, LX/1nh;->i(I)LX/1nh;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object p1

    new-instance v0, LX/4Ab;

    invoke-direct {v0}, LX/4Ab;-><init>()V

    invoke-virtual {v7}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b235a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, LX/4Ab;->a(F)LX/4Ab;

    move-result-object v0

    invoke-virtual {v7}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v7}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b2359

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object p1

    invoke-virtual {p1}, LX/1X5;->c()LX/1Di;

    move-result-object p1

    const v0, 0x7f0b2349

    invoke-interface {p1, v5, v0}, LX/1Di;->c(II)LX/1Di;

    move-result-object p1

    const/4 v0, 0x7

    const v2, 0x7f0b234b

    invoke-interface {p1, v0, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object p1

    const v0, 0x7f0b234d

    invoke-interface {p1, v0}, LX/1Di;->i(I)LX/1Di;

    move-result-object p1

    const v0, 0x7f0b234d

    invoke-interface {p1, v0}, LX/1Di;->q(I)LX/1Di;

    move-result-object p1

    invoke-static {v7, v7}, LX/Hfr;->a(LX/1De;LX/1De;)LX/1dQ;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p1

    invoke-interface {p2, p1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p1

    invoke-static {v7}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2, v11}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2}, LX/1X5;->c()LX/1Di;

    move-result-object p2

    .line 2492514
    const v0, 0x4330aefd

    const/4 v2, 0x0

    invoke-static {v7, v0, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    move-object v0, v0

    .line 2492515
    invoke-interface {p2, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p2

    invoke-interface {p1, p2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p1

    move-object v6, p1

    .line 2492516
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v7

    const v8, 0x7f0a00e9

    invoke-virtual {v7, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, LX/1Di;->o(I)LX/1Di;

    move-result-object v7

    const/4 v8, 0x4

    invoke-interface {v7, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v7

    const/4 v8, 0x6

    const v9, 0x7f0b2349

    invoke-interface {v7, v8, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    .line 2492517
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2492518
    return-object v0

    :cond_0
    iget-object v0, v6, LX/Hfv;->b:LX/1vg;

    invoke-virtual {v0, v7}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    const v2, 0x7f02071f

    invoke-virtual {v0, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    move v2, v3

    .line 2492519
    goto/16 :goto_1

    :cond_2
    move v2, v3

    goto/16 :goto_1

    .line 2492520
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2492445
    invoke-static {}, LX/1dS;->b()V

    .line 2492446
    iget v0, p1, LX/1dQ;->b:I

    .line 2492447
    sparse-switch v0, :sswitch_data_0

    .line 2492448
    :goto_0
    return-object v3

    .line 2492449
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2492450
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 2492451
    check-cast v2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492452
    iget-object v4, p0, LX/Hfr;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Hfv;

    iget v5, v2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->e:I

    iget-object p1, v2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    .line 2492453
    invoke-virtual {p1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->q()Ljava/lang/String;

    move-result-object p2

    .line 2492454
    iget-object p0, v4, LX/Hfv;->d:LX/17Y;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {p0, v2, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    .line 2492455
    iget-object p0, v4, LX/Hfv;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {p0, p2, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2492456
    const/4 p2, 0x1

    .line 2492457
    iget-object p0, v0, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 2492458
    if-nez p0, :cond_1

    .line 2492459
    :goto_1
    iget-object p2, v4, LX/Hfv;->i:LX/HgN;

    .line 2492460
    iget-object p0, p2, LX/HgN;->a:LX/0if;

    sget-object v2, LX/0ig;->af:LX/0ih;

    const-string v4, "go_to_group"

    const/4 v1, 0x0

    invoke-static {p2, v5, p1}, LX/HgN;->c(LX/HgN;ILcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)LX/1rQ;

    move-result-object v0

    invoke-virtual {p0, v2, v4, v1, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2492461
    goto :goto_0

    .line 2492462
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2492463
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2492464
    check-cast v1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492465
    iget-object v2, p0, LX/Hfr;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Hfv;

    iget v4, v1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->e:I

    iget-object v5, v1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    iget-object v6, v1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->b:LX/Hfh;

    .line 2492466
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    const/4 p0, 0x1

    .line 2492467
    new-instance p1, LX/5OM;

    invoke-direct {p1, v7}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2492468
    invoke-virtual {p1}, LX/5OM;->c()LX/5OG;

    move-result-object p2

    .line 2492469
    iput-boolean p0, p1, LX/0ht;->w:Z

    .line 2492470
    iput-boolean p0, p1, LX/0ht;->x:Z

    .line 2492471
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {v5}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 2492472
    invoke-virtual {v5}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object p0

    .line 2492473
    :goto_2
    move-object p0, p0

    .line 2492474
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq p0, v1, :cond_0

    .line 2492475
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v1, 0x7f083716

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p0

    .line 2492476
    new-instance v1, LX/Hfs;

    invoke-direct {v1, v2, v5, v7, v4}, LX/Hfs;-><init>(LX/Hfv;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;Landroid/content/Context;I)V

    invoke-interface {p0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2492477
    :cond_0
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v1, 0x7f083717

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p2

    .line 2492478
    new-instance p0, LX/Hfu;

    invoke-direct {p0, v2, v5, v4, v6}, LX/Hfu;-><init>(LX/Hfv;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;ILX/Hfh;)V

    invoke-interface {p2, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2492479
    iget-object p2, v2, LX/Hfv;->i:LX/HgN;

    .line 2492480
    iget-object p0, p2, LX/HgN;->a:LX/0if;

    sget-object v1, LX/0ig;->af:LX/0ih;

    const-string v2, "group_more_clicked"

    const/4 v6, 0x0

    invoke-static {p2, v4, v5}, LX/HgN;->c(LX/HgN;ILcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)LX/1rQ;

    move-result-object v7

    invoke-virtual {p0, v1, v2, v6, v7}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2492481
    move-object v7, p1

    .line 2492482
    invoke-virtual {v7, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2492483
    goto/16 :goto_0

    .line 2492484
    :cond_1
    check-cast p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492485
    new-instance v2, LX/Hfq;

    iget-object v1, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->f:LX/Hfr;

    invoke-direct {v2, v1, p2}, LX/Hfq;-><init>(LX/Hfr;Z)V

    move-object p0, v2

    .line 2492486
    invoke-virtual {v0, p0}, LX/1De;->a(LX/48B;)V

    goto/16 :goto_1

    :cond_2
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x1befdfa -> :sswitch_0
        0x4330aefd -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 2492441
    check-cast p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492442
    check-cast p2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492443
    iget-object v0, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->d:Ljava/lang/Boolean;

    iput-object v0, p2, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->d:Ljava/lang/Boolean;

    .line 2492444
    return-void
.end method

.method public final c(LX/1De;)LX/Hfp;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2492433
    new-instance v1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;-><init>(LX/Hfr;)V

    .line 2492434
    sget-object v2, LX/Hfr;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Hfp;

    .line 2492435
    if-nez v2, :cond_0

    .line 2492436
    new-instance v2, LX/Hfp;

    invoke-direct {v2}, LX/Hfp;-><init>()V

    .line 2492437
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Hfp;->a$redex0(LX/Hfp;LX/1De;IILcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;)V

    .line 2492438
    move-object v1, v2

    .line 2492439
    move-object v0, v1

    .line 2492440
    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2492432
    const/4 v0, 0x1

    return v0
.end method
