.class public LX/IdZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2597028
    const-class v0, LX/IdZ;

    sput-object v0, LX/IdZ;->a:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2597029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2597030
    return-void
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 13

    .prologue
    .line 2597051
    const/4 v3, 0x1

    const/4 v12, -0x1

    .line 2597052
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-double v4, v2

    .line 2597053
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-double v6, v2

    .line 2597054
    if-ne p2, v12, :cond_3

    move v2, v3

    .line 2597055
    :goto_0
    if-ne p1, v12, :cond_4

    const/16 v4, 0x80

    .line 2597056
    :goto_1
    if-ge v4, v2, :cond_5

    .line 2597057
    :cond_0
    :goto_2
    move v1, v2

    .line 2597058
    const/16 v0, 0x8

    if-gt v1, v0, :cond_1

    .line 2597059
    const/4 v0, 0x1

    .line 2597060
    :goto_3
    if-ge v0, v1, :cond_2

    .line 2597061
    shl-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2597062
    :cond_1
    add-int/lit8 v0, v1, 0x7

    div-int/lit8 v0, v0, 0x8

    mul-int/lit8 v0, v0, 0x8

    .line 2597063
    :cond_2
    return v0

    .line 2597064
    :cond_3
    mul-double v8, v4, v6

    int-to-double v10, p2

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v2, v8

    goto :goto_0

    .line 2597065
    :cond_4
    int-to-double v8, p1

    div-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    int-to-double v8, p1

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    double-to-int v4, v4

    goto :goto_1

    .line 2597066
    :cond_5
    if-ne p2, v12, :cond_6

    if-ne p1, v12, :cond_6

    move v2, v3

    .line 2597067
    goto :goto_2

    .line 2597068
    :cond_6
    if-eq p1, v12, :cond_0

    move v2, v4

    .line 2597069
    goto :goto_2
.end method

.method public static a(IILandroid/net/Uri;Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 2597031
    if-nez p4, :cond_0

    .line 2597032
    :try_start_0
    const-string v1, "r"

    invoke-virtual {p3, p2, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    move-result-object v1

    .line 2597033
    :goto_0
    move-object p4, v1
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2597034
    :cond_0
    if-nez p4, :cond_1

    .line 2597035
    invoke-static {p4}, LX/IdZ;->a(Landroid/os/ParcelFileDescriptor;)V

    :goto_1
    return-object v0

    .line 2597036
    :cond_1
    if-nez p5, :cond_2

    :try_start_2
    new-instance p5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2597037
    :cond_2
    invoke-virtual {p4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    .line 2597038
    const/4 v2, 0x1

    iput-boolean v2, p5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2597039
    invoke-static {}, LX/IdX;->a()LX/IdX;

    move-result-object v2

    invoke-virtual {v2, v1, p5}, LX/IdX;->a(Ljava/io/FileDescriptor;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2597040
    iget-boolean v2, p5, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    if-nez v2, :cond_3

    iget v2, p5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eq v2, v3, :cond_3

    iget v2, p5, Landroid/graphics/BitmapFactory$Options;->outHeight:I
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v2, v3, :cond_4

    .line 2597041
    :cond_3
    invoke-static {p4}, LX/IdZ;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_1

    .line 2597042
    :cond_4
    :try_start_3
    invoke-static {p5, p0, p1}, LX/IdZ;->a(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v2

    iput v2, p5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 2597043
    const/4 v2, 0x0

    iput-boolean v2, p5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2597044
    const/4 v2, 0x0

    iput-boolean v2, p5, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 2597045
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, p5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 2597046
    invoke-static {}, LX/IdX;->a()LX/IdX;

    move-result-object v2

    invoke-virtual {v2, v1, p5}, LX/IdX;->a(Ljava/io/FileDescriptor;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2597047
    invoke-static {p4}, LX/IdZ;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_1

    .line 2597048
    :catch_0
    move-exception v1

    .line 2597049
    :try_start_4
    sget-object v2, LX/IdZ;->a:Ljava/lang/Class;

    const-string v3, "Got oom exception "

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2597050
    invoke-static {p4}, LX/IdZ;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {p4}, LX/IdZ;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0

    :catch_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(IILandroid/net/Uri;Landroid/content/ContentResolver;Z)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2596986
    :try_start_0
    const-string v0, "r"

    invoke-virtual {p3, p2, v0}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 2596987
    if-eqz p4, :cond_0

    .line 2596988
    :try_start_1
    invoke-static {}, LX/IdZ;->a()Landroid/graphics/BitmapFactory$Options;

    move-result-object v5

    :goto_0
    move v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 2596989
    invoke-static/range {v0 .. v5}, LX/IdZ;->a(IILandroid/net/Uri;Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2596990
    invoke-static {v4}, LX/IdZ;->a(Landroid/os/ParcelFileDescriptor;)V

    :goto_1
    return-object v0

    :catch_0
    move-object v4, v6

    :goto_2
    invoke-static {v4}, LX/IdZ;->a(Landroid/os/ParcelFileDescriptor;)V

    move-object v0, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v4, v6

    :goto_3
    invoke-static {v4}, LX/IdZ;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    goto :goto_2

    :cond_0
    move-object v5, v6

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 2596979
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 2596980
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 2596981
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 2596982
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2596983
    if-eq p0, v0, :cond_0

    .line 2596984
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-object p0, v0

    .line 2596985
    :cond_0
    :goto_0
    return-object p0

    :catch_0
    goto :goto_0
.end method

.method public static a(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZZ)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const v7, 0x3f666666    # 0.9f

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2596991
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v2, v0, p2

    .line 2596992
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v3, v0, p3

    .line 2596993
    if-nez p4, :cond_2

    if-ltz v2, :cond_0

    if-gez v3, :cond_2

    .line 2596994
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2596995
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2596996
    div-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2596997
    div-int/lit8 v3, v3, 0x2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2596998
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {p2, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-static {p3, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int/2addr v7, v1

    invoke-direct {v3, v2, v1, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2596999
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    .line 2597000
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int v2, p3, v2

    div-int/lit8 v2, v2, 0x2

    .line 2597001
    new-instance v6, Landroid/graphics/Rect;

    sub-int v7, p2, v1

    sub-int v8, p3, v2

    invoke-direct {v6, v1, v2, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2597002
    invoke-virtual {v4, p1, v3, v6, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2597003
    if-eqz p5, :cond_1

    .line 2597004
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2597005
    :cond_1
    :goto_0
    return-object v0

    .line 2597006
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 2597007
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    .line 2597008
    div-float v3, v0, v2

    .line 2597009
    int-to-float v4, p2

    int-to-float v6, p3

    div-float/2addr v4, v6

    .line 2597010
    cmpl-float v3, v3, v4

    if-lez v3, :cond_7

    .line 2597011
    int-to-float v0, p3

    div-float/2addr v0, v2

    .line 2597012
    cmpg-float v2, v0, v7

    if-ltz v2, :cond_3

    cmpl-float v2, v0, v8

    if-lez v2, :cond_4

    .line 2597013
    :cond_3
    invoke-virtual {p0, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    move-object v5, p0

    .line 2597014
    :cond_4
    :goto_1
    if-eqz v5, :cond_9

    .line 2597015
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v2, v0

    .line 2597016
    :goto_2
    if-eqz p5, :cond_5

    if-eq v2, p1, :cond_5

    .line 2597017
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2597018
    :cond_5
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int/2addr v0, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2597019
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v3, p3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2597020
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    invoke-static {v2, v0, v1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2597021
    if-eq v0, v2, :cond_1

    .line 2597022
    if-nez p5, :cond_6

    if-eq v2, p1, :cond_1

    .line 2597023
    :cond_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 2597024
    :cond_7
    int-to-float v2, p2

    div-float v0, v2, v0

    .line 2597025
    cmpg-float v2, v0, v7

    if-ltz v2, :cond_8

    cmpl-float v2, v0, v8

    if-lez v2, :cond_4

    .line 2597026
    :cond_8
    invoke-virtual {p0, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    move-object v5, p0

    goto :goto_1

    :cond_9
    move-object v2, p1

    .line 2597027
    goto :goto_2
.end method

.method public static a()Landroid/graphics/BitmapFactory$Options;
    .locals 1

    .prologue
    .line 2596977
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2596978
    return-object v0
.end method

.method private static a(Landroid/os/ParcelFileDescriptor;)V
    .locals 1

    .prologue
    .line 2596973
    if-nez p0, :cond_0

    .line 2596974
    :goto_0
    return-void

    .line 2596975
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2596976
    :catch_0
    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/camerautil/MonitoredActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 2596970
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v0

    .line 2596971
    new-instance v1, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;

    invoke-direct {v1, p0, p3, v0, p4}, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;-><init>(Lcom/facebook/messaging/camerautil/MonitoredActivity;Ljava/lang/Runnable;LX/4BY;Landroid/os/Handler;)V

    const v0, 0x7490319c

    invoke-static {v1, v0}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2596972
    return-void
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 2596966
    if-nez p0, :cond_0

    .line 2596967
    :goto_0
    return-void

    .line 2596968
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2596969
    :catch_0
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2596965
    if-eq p0, p1, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
