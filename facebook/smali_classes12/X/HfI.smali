.class public final LX/HfI;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HfJ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

.field public c:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/HfJ;


# direct methods
.method public constructor <init>(LX/HfJ;)V
    .locals 1

    .prologue
    .line 2491508
    iput-object p1, p0, LX/HfI;->d:LX/HfJ;

    .line 2491509
    move-object v0, p1

    .line 2491510
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2491511
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2491507
    const-string v0, "WorkGroupDiscoveryListItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2491512
    if-ne p0, p1, :cond_1

    .line 2491513
    :cond_0
    :goto_0
    return v0

    .line 2491514
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2491515
    goto :goto_0

    .line 2491516
    :cond_3
    check-cast p1, LX/HfI;

    .line 2491517
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2491518
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2491519
    if-eq v2, v3, :cond_0

    .line 2491520
    iget v2, p0, LX/HfI;->a:I

    iget v3, p1, LX/HfI;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2491521
    goto :goto_0

    .line 2491522
    :cond_4
    iget-object v2, p0, LX/HfI;->b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/HfI;->b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    iget-object v3, p1, LX/HfI;->b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2491523
    goto :goto_0

    .line 2491524
    :cond_6
    iget-object v2, p1, LX/HfI;->b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    if-nez v2, :cond_5

    .line 2491525
    :cond_7
    iget-object v2, p0, LX/HfI;->c:LX/2kW;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HfI;->c:LX/2kW;

    iget-object v3, p1, LX/HfI;->c:LX/2kW;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2491526
    goto :goto_0

    .line 2491527
    :cond_8
    iget-object v2, p1, LX/HfI;->c:LX/2kW;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
