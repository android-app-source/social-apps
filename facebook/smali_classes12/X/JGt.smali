.class public final LX/JGt;
.super Landroid/text/style/MetricAffectingSpan;
.source ""


# static fields
.field public static final a:LX/JGt;


# instance fields
.field public b:D

.field public c:I

.field public d:Z

.field public e:Z

.field public f:I

.field public g:I

.field public h:I

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v5, -0x1

    .line 2669437
    new-instance v1, LX/JGt;

    const-wide/high16 v2, -0x3e90000000000000L    # -1.6777216E7

    const/4 v10, 0x0

    const/4 v11, 0x1

    move v6, v5

    move v7, v5

    move v8, v4

    move v9, v4

    invoke-direct/range {v1 .. v11}, LX/JGt;-><init>(DIIIIZZLjava/lang/String;Z)V

    sput-object v1, LX/JGt;->a:LX/JGt;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2669417
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    .line 2669418
    return-void
.end method

.method public constructor <init>(DIIIIZZLjava/lang/String;Z)V
    .locals 1
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2669426
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    .line 2669427
    iput-wide p1, p0, LX/JGt;->b:D

    .line 2669428
    iput p3, p0, LX/JGt;->c:I

    .line 2669429
    iput p4, p0, LX/JGt;->f:I

    .line 2669430
    iput p5, p0, LX/JGt;->g:I

    .line 2669431
    iput p6, p0, LX/JGt;->h:I

    .line 2669432
    iput-boolean p7, p0, LX/JGt;->d:Z

    .line 2669433
    iput-boolean p8, p0, LX/JGt;->e:Z

    .line 2669434
    iput-object p9, p0, LX/JGt;->i:Ljava/lang/String;

    .line 2669435
    iput-boolean p10, p0, LX/JGt;->j:Z

    .line 2669436
    return-void
.end method

.method public static e(LX/JGt;I)I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2669438
    iget v0, p0, LX/JGt;->g:I

    if-eq v0, v2, :cond_0

    .line 2669439
    and-int/lit8 v0, p1, -0x3

    iget v1, p0, LX/JGt;->g:I

    or-int p1, v0, v1

    .line 2669440
    :cond_0
    iget v0, p0, LX/JGt;->h:I

    if-eq v0, v2, :cond_1

    .line 2669441
    and-int/lit8 v0, p1, -0x2

    iget v1, p0, LX/JGt;->h:I

    or-int p1, v0, v1

    .line 2669442
    :cond_1
    return p1
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2669419
    iget-wide v0, p0, LX/JGt;->b:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2669420
    iget-wide v0, p0, LX/JGt;->b:D

    double-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2669421
    :cond_0
    iget v0, p0, LX/JGt;->c:I

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    .line 2669422
    iget-boolean v0, p0, LX/JGt;->d:Z

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2669423
    iget-boolean v0, p0, LX/JGt;->e:Z

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setStrikeThruText(Z)V

    .line 2669424
    invoke-virtual {p0, p1}, LX/JGt;->updateMeasureState(Landroid/text/TextPaint;)V

    .line 2669425
    return-void
.end method

.method public final updateMeasureState(Landroid/text/TextPaint;)V
    .locals 9

    .prologue
    .line 2669371
    iget v0, p0, LX/JGt;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2669372
    iget v0, p0, LX/JGt;->f:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2669373
    :cond_0
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    .line 2669374
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 2669375
    :goto_0
    invoke-static {p0, v0}, LX/JGt;->e(LX/JGt;I)I

    move-result v2

    .line 2669376
    if-ne v0, v2, :cond_2

    iget-object v0, p0, LX/JGt;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2669377
    :goto_1
    return-void

    .line 2669378
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    goto :goto_0

    .line 2669379
    :cond_2
    iget-object v0, p0, LX/JGt;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2669380
    iget-object v0, p0, LX/JGt;->i:Ljava/lang/String;

    .line 2669381
    sget-object v1, LX/JH4;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/graphics/Typeface;

    .line 2669382
    if-nez v1, :cond_5

    .line 2669383
    const/4 v1, 0x4

    new-array v1, v1, [Landroid/graphics/Typeface;

    .line 2669384
    sget-object v3, LX/JH4;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2669385
    :cond_3
    sget-object v3, LX/JH4;->c:[Ljava/lang/String;

    aget-object v3, v3, v2

    .line 2669386
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x20

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "fonts/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 2669387
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    .line 2669388
    sget-object v6, LX/JH4;->d:[Ljava/lang/String;

    array-length v7, v6

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v7, :cond_6

    aget-object v8, v6, v3

    .line 2669389
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2669390
    :try_start_0
    sget-object p0, LX/JH4;->e:Landroid/content/res/AssetManager;

    invoke-static {p0, v8}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2669391
    :goto_3
    move-object v3, v3

    .line 2669392
    aput-object v3, v1, v2

    .line 2669393
    sget-object v4, LX/JH4;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v3

    .line 2669394
    :goto_4
    move-object v0, v1

    .line 2669395
    :goto_5
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_1

    .line 2669396
    :cond_4
    if-nez v1, :cond_7

    .line 2669397
    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2669398
    :goto_6
    move-object v0, v0

    .line 2669399
    goto :goto_5

    .line 2669400
    :cond_5
    aget-object v3, v1, v2

    if-eqz v3, :cond_3

    .line 2669401
    aget-object v1, v1, v2

    goto :goto_4

    .line 2669402
    :catch_0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2669403
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2669404
    :cond_6
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 2669405
    move-object v3, v3

    .line 2669406
    check-cast v3, Landroid/graphics/Typeface;

    goto :goto_3

    .line 2669407
    :cond_7
    sget-object v0, LX/JH4;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/Typeface;

    .line 2669408
    if-nez v0, :cond_9

    .line 2669409
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Typeface;

    .line 2669410
    invoke-virtual {v1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v3

    aput-object v1, v0, v3

    .line 2669411
    :cond_8
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 2669412
    aput-object v3, v0, v2

    .line 2669413
    sget-object v4, LX/JH4;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v3

    .line 2669414
    goto :goto_6

    .line 2669415
    :cond_9
    aget-object v3, v0, v2

    if-eqz v3, :cond_8

    .line 2669416
    aget-object v0, v0, v2

    goto :goto_6
.end method
