.class public final LX/Ier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ieq;


# instance fields
.field public final synthetic a:LX/Ies;


# direct methods
.method public constructor <init>(LX/Ies;)V
    .locals 0

    .prologue
    .line 2599027
    iput-object p1, p0, LX/Ier;->a:LX/Ies;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2599009
    iget-object v0, p0, LX/Ier;->a:LX/Ies;

    .line 2599010
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599011
    iget-object v2, v0, LX/Ies;->f:Ljava/util/Set;

    iget-object p0, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599012
    iget-object p1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p0, p1

    .line 2599013
    invoke-interface {v2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2599014
    if-eqz v1, :cond_0

    iget-object v2, v0, LX/Ies;->d:LX/IfB;

    if-eqz v2, :cond_0

    .line 2599015
    iget-object v2, v0, LX/Ies;->d:LX/IfB;

    invoke-interface {v2, v1}, LX/IfB;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599016
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2599022
    iget-object v0, p0, LX/Ier;->a:LX/Ies;

    .line 2599023
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599024
    if-eqz v1, :cond_0

    iget-object p0, v0, LX/Ies;->d:LX/IfB;

    if-eqz p0, :cond_0

    .line 2599025
    iget-object p0, v0, LX/Ies;->d:LX/IfB;

    invoke-interface {p0, v1}, LX/IfB;->b(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599026
    :cond_0
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2599017
    iget-object v0, p0, LX/Ier;->a:LX/Ies;

    .line 2599018
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599019
    if-eqz v1, :cond_0

    iget-object p0, v0, LX/Ies;->d:LX/IfB;

    if-eqz p0, :cond_0

    .line 2599020
    iget-object p0, v0, LX/Ies;->d:LX/IfB;

    invoke-interface {p0, v1}, LX/IfB;->c(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599021
    :cond_0
    return-void
.end method
