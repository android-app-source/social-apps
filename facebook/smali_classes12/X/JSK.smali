.class public final enum LX/JSK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JSK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JSK;

.field public static final enum LOADING:LX/JSK;

.field public static final enum PAUSED:LX/JSK;

.field public static final enum PLAYING:LX/JSK;

.field public static final enum PLAY_REQUESTED:LX/JSK;

.field public static final enum STOPPED:LX/JSK;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2695123
    new-instance v0, LX/JSK;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, LX/JSK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JSK;->STOPPED:LX/JSK;

    .line 2695124
    new-instance v0, LX/JSK;

    const-string v1, "PLAY_REQUESTED"

    invoke-direct {v0, v1, v3}, LX/JSK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JSK;->PLAY_REQUESTED:LX/JSK;

    .line 2695125
    new-instance v0, LX/JSK;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, LX/JSK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JSK;->LOADING:LX/JSK;

    .line 2695126
    new-instance v0, LX/JSK;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v5}, LX/JSK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JSK;->PLAYING:LX/JSK;

    .line 2695127
    new-instance v0, LX/JSK;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v6}, LX/JSK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JSK;->PAUSED:LX/JSK;

    .line 2695128
    const/4 v0, 0x5

    new-array v0, v0, [LX/JSK;

    sget-object v1, LX/JSK;->STOPPED:LX/JSK;

    aput-object v1, v0, v2

    sget-object v1, LX/JSK;->PLAY_REQUESTED:LX/JSK;

    aput-object v1, v0, v3

    sget-object v1, LX/JSK;->LOADING:LX/JSK;

    aput-object v1, v0, v4

    sget-object v1, LX/JSK;->PLAYING:LX/JSK;

    aput-object v1, v0, v5

    sget-object v1, LX/JSK;->PAUSED:LX/JSK;

    aput-object v1, v0, v6

    sput-object v0, LX/JSK;->$VALUES:[LX/JSK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2695129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JSK;
    .locals 1

    .prologue
    .line 2695130
    const-class v0, LX/JSK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JSK;

    return-object v0
.end method

.method public static values()[LX/JSK;
    .locals 1

    .prologue
    .line 2695131
    sget-object v0, LX/JSK;->$VALUES:[LX/JSK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JSK;

    return-object v0
.end method
