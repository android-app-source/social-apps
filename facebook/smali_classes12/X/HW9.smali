.class public LX/HW9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475702
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v8, 0x0

    .line 2475703
    const-string v2, "cover_photo_fbid"

    invoke-virtual {p1, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2475704
    const-string v4, "cover_photo_uri"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2475705
    const-string v5, "profile_id"

    invoke-virtual {p1, v5, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 2475706
    cmp-long v5, v2, v8

    if-eqz v5, :cond_1

    .line 2475707
    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2475708
    invoke-static {v2, v3, v4, v6, v7}, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->a(JLjava/lang/String;J)Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    move-result-object v0

    .line 2475709
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2475710
    goto :goto_0

    .line 2475711
    :cond_1
    cmp-long v2, v6, v8

    if-eqz v2, :cond_2

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2475712
    invoke-static {v8, v9, v4, v6, v7}, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->a(JLjava/lang/String;J)Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2475713
    goto :goto_2
.end method
