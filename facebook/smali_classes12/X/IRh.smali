.class public final LX/IRh;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V
    .locals 0

    .prologue
    .line 2576621
    iput-object p1, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iput-boolean p2, p0, LX/IRh;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2576622
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->j()V

    .line 2576623
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    .line 2576624
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v1, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0, v1}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2576625
    :cond_0
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-boolean v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aR:Z

    if-nez v0, :cond_1

    .line 2576626
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    .line 2576627
    iput-boolean v2, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aR:Z

    .line 2576628
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v0, v2}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    .line 2576629
    :goto_0
    return-void

    .line 2576630
    :cond_1
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->af:Ljava/lang/String;

    const-string v2, "Failed to fetch group header."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2576631
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2576632
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2576633
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_3

    .line 2576634
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->g()V

    .line 2576635
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->e()V

    .line 2576636
    :goto_0
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    const/4 v1, 0x0

    .line 2576637
    iput-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aR:Z

    .line 2576638
    iget-object v1, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    .line 2576639
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576640
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v1, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2576641
    iget-object v1, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    .line 2576642
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576643
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2576644
    const/4 v2, 0x0

    .line 2576645
    iget-object v3, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->V:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->T:LX/0Uh;

    const/16 v4, 0x575

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, LX/IQV;->e(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v2, v2

    .line 2576646
    if-nez v2, :cond_5

    .line 2576647
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2576648
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, v1, :cond_4

    .line 2576649
    iget-boolean v0, p0, LX/IRh;->a:Z

    if-eqz v0, :cond_2

    .line 2576650
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->c()V

    .line 2576651
    :cond_2
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    const/4 v1, 0x1

    .line 2576652
    iput-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aS:Z

    .line 2576653
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->X(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576654
    :goto_2
    return-void

    .line 2576655
    :cond_3
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->d()V

    .line 2576656
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->f()V

    goto :goto_0

    .line 2576657
    :cond_4
    iget-object v0, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->z:LX/1My;

    iget-object v1, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aX:LX/0TF;

    iget-object v2, p0, LX/IRh;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_2

    .line 2576658
    :cond_5
    iget-object v2, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->r:LX/0iA;

    const-string v3, "4327"

    const-class v4, LX/8vQ;

    invoke-virtual {v2, v3, v4}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/8vQ;

    .line 2576659
    if-eqz v2, :cond_1

    .line 2576660
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 2576661
    iput-object v3, v2, LX/8vQ;->d:Ljava/lang/String;

    .line 2576662
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    .line 2576663
    iput-object v3, v2, LX/8vQ;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2576664
    iget-object v3, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    .line 2576665
    iput-object v3, v2, LX/8vQ;->b:Landroid/view/View;

    .line 2576666
    invoke-static {v0}, LX/IQV;->d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/0Px;

    move-result-object v3

    .line 2576667
    iput-object v3, v2, LX/8vQ;->c:LX/0Px;

    .line 2576668
    invoke-static {v0}, LX/IQV;->c(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)I

    move-result v3

    .line 2576669
    iput v3, v2, LX/8vQ;->f:I

    .line 2576670
    invoke-static {v0}, LX/IQV;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v3

    .line 2576671
    iput-boolean v3, v2, LX/8vQ;->e:Z

    .line 2576672
    iget-object v2, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/15W;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v5, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v4, v5}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 2576673
    const-class v5, LX/0i1;

    const/4 v1, 0x0

    invoke-virtual {v2, v3, v4, v5, v1}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 2576674
    goto/16 :goto_1
.end method
