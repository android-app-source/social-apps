.class public LX/Iou;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IoC;


# instance fields
.field private a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612355
    iput-object p1, p0, LX/Iou;->a:LX/0Zb;

    .line 2612356
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2612357
    const-string v0, "orion_messenger_pay_params"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    .line 2612358
    const-string v1, "payment_flow_type"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, LX/5g0;

    .line 2612359
    iget-object v2, p0, LX/Iou;->a:LX/0Zb;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2612360
    iget-object v3, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 2612361
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/5fz;->i(Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2612362
    iget-object v3, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v3, v3

    .line 2612363
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->c()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    move-result-object v1

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2612364
    sget-object v3, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->p:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0, v3}, Lcom/facebook/payments/currency/CurrencyAmount;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    move v0, v3

    .line 2612365
    invoke-virtual {v1, v0}, LX/5fz;->a(Z)LX/5fz;

    move-result-object v0

    .line 2612366
    iget-object v1, v0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v0, v1

    .line 2612367
    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2612368
    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
