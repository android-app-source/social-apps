.class public LX/HL5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/17W;

.field public final c:LX/1vg;

.field private final d:LX/9XE;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17W;LX/1vg;LX/9XE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2455527
    iput-object p1, p0, LX/HL5;->a:Landroid/content/Context;

    .line 2455528
    iput-object p2, p0, LX/HL5;->b:LX/17W;

    .line 2455529
    iput-object p3, p0, LX/HL5;->c:LX/1vg;

    .line 2455530
    iput-object p4, p0, LX/HL5;->d:LX/9XE;

    .line 2455531
    return-void
.end method

.method public static a(LX/0QB;)LX/HL5;
    .locals 7

    .prologue
    .line 2455532
    const-class v1, LX/HL5;

    monitor-enter v1

    .line 2455533
    :try_start_0
    sget-object v0, LX/HL5;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2455534
    sput-object v2, LX/HL5;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2455535
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455536
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2455537
    new-instance p0, LX/HL5;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v6

    check-cast v6, LX/9XE;

    invoke-direct {p0, v3, v4, v5, v6}, LX/HL5;-><init>(Landroid/content/Context;LX/17W;LX/1vg;LX/9XE;)V

    .line 2455538
    move-object v0, p0

    .line 2455539
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2455540
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HL5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2455541
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2455542
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2455543
    iget-object v0, p0, LX/HL5;->d:LX/9XE;

    .line 2455544
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2455545
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2455546
    iget-object v1, v0, LX/9XE;->a:LX/0Zb;

    sget-object v4, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_MESSAGE_FRIEND:LX/9XI;

    invoke-static {v4, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p2, "tapped_friend_id"

    invoke-virtual {v4, p2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v1, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2455547
    iget-object v0, p0, LX/HL5;->b:LX/17W;

    iget-object v1, p0, LX/HL5;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->am:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2455548
    return-void
.end method
