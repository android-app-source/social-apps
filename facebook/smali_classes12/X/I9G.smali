.class public LX/I9G;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/I7i;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2542649
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2542650
    invoke-direct {p0}, LX/I9G;->a()V

    .line 2542651
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2542646
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2542647
    invoke-direct {p0}, LX/I9G;->a()V

    .line 2542648
    return-void
.end method

.method private a(IIILandroid/view/View$OnClickListener;)Lcom/facebook/fbui/widget/text/ImageWithTextView;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2542640
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2542641
    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 2542642
    iget-object v1, p0, LX/I9G;->a:LX/0wM;

    const v2, -0x6e685d

    invoke-virtual {v1, p3, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2542643
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setVisibility(I)V

    .line 2542644
    invoke-virtual {v0, p4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2542645
    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2542652
    const-class v0, LX/I9G;

    invoke-static {v0, p0}, LX/I9G;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2542653
    const v0, 0x7f0304ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2542654
    new-instance v0, LX/I9F;

    invoke-direct {v0, p0}, LX/I9F;-><init>(LX/I9G;)V

    .line 2542655
    const v1, 0x7f0d01af

    const v2, 0x7f081f74

    const v3, 0x7f02080f

    invoke-direct {p0, v1, v2, v3, v0}, LX/I9G;->a(IIILandroid/view/View$OnClickListener;)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2542656
    const v1, 0x7f0d01b0

    const v2, 0x7f081f75

    const v3, 0x7f0207b3

    invoke-direct {p0, v1, v2, v3, v0}, LX/I9G;->a(IIILandroid/view/View$OnClickListener;)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2542657
    iget-object v1, p0, LX/I9G;->b:LX/0ad;

    sget-short v2, LX/Blo;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2542658
    const v1, 0x7f0d01b1

    const v2, 0x7f081f76

    const v3, 0x7f02085b

    invoke-direct {p0, v1, v2, v3, v0}, LX/I9G;->a(IIILandroid/view/View$OnClickListener;)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2542659
    :cond_0
    return-void
.end method

.method private static a(LX/I9G;LX/0wM;LX/0ad;)V
    .locals 0

    .prologue
    .line 2542639
    iput-object p1, p0, LX/I9G;->a:LX/0wM;

    iput-object p2, p0, LX/I9G;->b:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/I9G;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/I9G;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0, v0, v1}, LX/I9G;->a(LX/I9G;LX/0wM;LX/0ad;)V

    return-void
.end method
