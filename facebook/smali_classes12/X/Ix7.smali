.class public final LX/Ix7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V
    .locals 0

    .prologue
    .line 2631192
    iput-object p1, p0, LX/Ix7;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2631193
    iget-object v0, p0, LX/Ix7;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2631194
    iget-object v0, p0, LX/Ix7;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2631195
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2631196
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2631197
    if-nez p1, :cond_1

    .line 2631198
    :cond_0
    :goto_0
    return-void

    .line 2631199
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2631200
    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;

    .line 2631201
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2631202
    iget-object v0, p0, LX/Ix7;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2631203
    iget-object v0, p0, LX/Ix7;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2631204
    iget-object v0, p0, LX/Ix7;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->d:LX/Iws;

    .line 2631205
    iget-object v1, v0, LX/Iws;->a:LX/0if;

    sget-object v2, LX/0ig;->S:LX/0ih;

    const-string v3, "branded_content_show_intro"

    const/4 p0, 0x0

    invoke-static {v0}, LX/Iws;->h(LX/Iws;)LX/1rQ;

    move-result-object p1

    invoke-virtual {v1, v2, v3, p0, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2631206
    goto :goto_0
.end method
