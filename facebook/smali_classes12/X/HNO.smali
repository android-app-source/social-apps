.class public LX/HNO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Uh;

.field private final f:Landroid/content/Context;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2458396
    const v0, 0x7f020908

    sput v0, LX/HNO;->a:I

    .line 2458397
    const v0, 0x7f08367b

    sput v0, LX/HNO;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Uh;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458407
    iput-object p1, p0, LX/HNO;->c:LX/0Ot;

    .line 2458408
    iput-object p2, p0, LX/HNO;->d:LX/0Ot;

    .line 2458409
    iput-object p3, p0, LX/HNO;->e:LX/0Uh;

    .line 2458410
    iput-object p4, p0, LX/HNO;->f:Landroid/content/Context;

    .line 2458411
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2458405
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->REORDER_TABS:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    sget v2, LX/HNO;->b:I

    sget v3, LX/HNO;->a:I

    iget-object v6, p0, LX/HNO;->e:LX/0Uh;

    sget v7, LX/8Dm;->j:I

    invoke-virtual {v6, v7, v5}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, LX/HNO;->g:Ljava/lang/String;

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    move v5, v4

    :cond_0
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2458404
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->REORDER_TABS:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    sget v2, LX/HNO;->b:I

    sget v3, LX/HNO;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2458399
    sget-object v0, LX/8Dq;->e:Ljava/lang/String;

    iget-object v1, p0, LX/HNO;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2458400
    iget-object v0, p0, LX/HNO;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, LX/HNO;->f:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2458401
    const-string v0, "profile_name"

    iget-object v2, p0, LX/HNO;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2458402
    iget-object v0, p0, LX/HNO;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HNO;->f:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2458403
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2458398
    const/4 v0, 0x0

    return-object v0
.end method
