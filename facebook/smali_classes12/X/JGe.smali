.class public LX/JGe;
.super LX/5rN;
.source ""


# instance fields
.field private final b:LX/JGy;

.field private final c:LX/JH2;

.field private d:Lcom/facebook/react/views/image/ReactImageManager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/5pY;


# direct methods
.method private constructor <init>(LX/5pY;Lcom/facebook/react/views/image/ReactImageManager;LX/5rq;LX/JGq;LX/5s9;)V
    .locals 1
    .param p2    # Lcom/facebook/react/views/image/ReactImageManager;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2668827
    invoke-direct {p0, p1, p3, p4, p5}, LX/5rN;-><init>(LX/5pY;LX/5rq;LX/5rl;LX/5s9;)V

    .line 2668828
    new-instance v0, LX/JGy;

    invoke-direct {v0}, LX/JGy;-><init>()V

    iput-object v0, p0, LX/JGe;->b:LX/JGy;

    .line 2668829
    iput-object p1, p0, LX/JGe;->e:LX/5pY;

    .line 2668830
    new-instance v0, LX/JH2;

    invoke-direct {v0, p4}, LX/JH2;-><init>(LX/JGq;)V

    iput-object v0, p0, LX/JGe;->c:LX/JH2;

    .line 2668831
    iput-object p2, p0, LX/JGe;->d:Lcom/facebook/react/views/image/ReactImageManager;

    .line 2668832
    return-void
.end method

.method public static a(LX/5pY;Ljava/util/List;LX/5s9;)LX/JGe;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;",
            "LX/5s9;",
            ")",
            "LX/JGe;"
        }
    .end annotation

    .prologue
    .line 2668722
    invoke-static {p1}, LX/JGe;->a(Ljava/util/List;)Lcom/facebook/react/views/image/ReactImageManager;

    move-result-object v2

    .line 2668723
    if-eqz v2, :cond_0

    .line 2668724
    iget-object v0, v2, Lcom/facebook/react/views/image/ReactImageManager;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 2668725
    if-eqz v0, :cond_0

    .line 2668726
    sput-object v0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->d:Ljava/lang/Object;

    .line 2668727
    :cond_0
    invoke-virtual {p0}, LX/5pY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2668728
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sput-object v1, LX/JGZ;->a:LX/1Uo;

    .line 2668729
    invoke-virtual {p0}, LX/5pY;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 2668730
    sput-object v0, LX/JH4;->e:Landroid/content/res/AssetManager;

    .line 2668731
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2668732
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668733
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTTextManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTTextManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668734
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTRawTextManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTRawTextManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668735
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTVirtualTextManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualTextManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668736
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImageManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImageManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668737
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTImageViewManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTImageViewManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668738
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTTextInputManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTTextInputManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668739
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTViewPagerManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTViewPagerManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668740
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668741
    new-instance v1, Lcom/facebook/catalyst/shadow/flat/RCTModalHostManager;

    invoke-direct {v1}, Lcom/facebook/catalyst/shadow/flat/RCTModalHostManager;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2668742
    new-instance v3, LX/5rq;

    invoke-direct {v3, v0}, LX/5rq;-><init>(Ljava/util/List;)V

    .line 2668743
    new-instance v0, LX/JGd;

    invoke-direct {v0, v3}, LX/JGd;-><init>(LX/5rq;)V

    .line 2668744
    new-instance v4, LX/JGq;

    invoke-direct {v4, p0, v0}, LX/JGq;-><init>(LX/5pY;LX/JGd;)V

    .line 2668745
    new-instance v0, LX/JGe;

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/JGe;-><init>(LX/5pY;Lcom/facebook/react/views/image/ReactImageManager;LX/5rq;LX/JGq;LX/5s9;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/react/uimanager/ReactShadowNode;II)Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 3

    .prologue
    .line 2668714
    if-lt p1, p2, :cond_0

    .line 2668715
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invariant failure, needs sorting! "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2668716
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;)Lcom/facebook/react/views/image/ReactImageManager;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;)",
            "Lcom/facebook/react/views/image/ReactImageManager;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2668746
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-eq v0, v1, :cond_1

    .line 2668747
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/react/views/image/ReactImageManager;

    if-eqz v2, :cond_0

    .line 2668748
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/views/image/ReactImageManager;

    .line 2668749
    :goto_1
    return-object v0

    .line 2668750
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2668751
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IZLcom/facebook/react/bridge/Callback;)V
    .locals 8

    .prologue
    .line 2668752
    invoke-virtual {p0, p1}, LX/5rN;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2668753
    invoke-virtual {v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2668754
    iget-object v1, p0, LX/JGe;->c:LX/JH2;

    invoke-virtual {v1, v0}, LX/JH2;->b(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V

    .line 2668755
    if-eqz p2, :cond_0

    .line 2668756
    invoke-super {p0, p1, p3}, LX/5rN;->b(ILcom/facebook/react/bridge/Callback;)V

    .line 2668757
    :goto_0
    return-void

    .line 2668758
    :cond_0
    invoke-super {p0, p1, p3}, LX/5rN;->a(ILcom/facebook/react/bridge/Callback;)V

    goto :goto_0

    .line 2668759
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->z()F

    move-result v4

    .line 2668760
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->A()F

    move-result v5

    .line 2668761
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v2

    .line 2668762
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v1

    move v3, v1

    .line 2668763
    :goto_1
    iget-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v1

    .line 2668764
    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2668765
    move-object v0, v0

    .line 2668766
    move-object v1, v0

    check-cast v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2668767
    invoke-virtual {v1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2668768
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    invoke-virtual {v0, v1}, LX/JH2;->b(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V

    .line 2668769
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->z()F

    move-result v6

    .line 2668770
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->A()F

    move-result v7

    .line 2668771
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    .line 2668772
    iget-object p0, v0, LX/JH2;->d:LX/JGq;

    move-object v0, p0

    .line 2668773
    iget p0, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, p0

    .line 2668774
    div-float/2addr v2, v6

    div-float/2addr v3, v7

    div-float/2addr v4, v6

    div-float/2addr v5, v7

    move v6, p2

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, LX/JGq;->a(IFFFFZLcom/facebook/react/bridge/Callback;)V

    goto :goto_0

    .line 2668775
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v0

    add-float/2addr v2, v0

    .line 2668776
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v0

    add-float/2addr v0, v3

    move v3, v0

    move-object v0, v1

    goto :goto_1
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 2

    .prologue
    .line 2668777
    iget-object v0, p0, LX/JGe;->b:LX/JGy;

    .line 2668778
    iget-object v1, v0, LX/JGy;->d:[Lcom/facebook/react/uimanager/ReactShadowNode;

    invoke-static {v0, p2}, LX/JGy;->f(LX/JGy;I)I

    move-result p0

    aput-object p1, v1, p0

    .line 2668779
    return-void
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5pC;LX/5pC;)V
    .locals 10
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const v1, 0x7fffffff

    .line 2668780
    const/4 v6, -0x1

    .line 2668781
    iget-object v0, p0, LX/JGe;->b:LX/JGy;

    .line 2668782
    iget v3, v0, LX/JGy;->b:I

    move v0, v3

    .line 2668783
    if-nez v0, :cond_0

    move v4, v1

    move v5, v1

    .line 2668784
    :goto_0
    if-nez p3, :cond_1

    move v0, v1

    move v3, v6

    move v6, v5

    move v5, v4

    move v4, v2

    move v2, v1

    .line 2668785
    :goto_1
    if-ge v0, v5, :cond_3

    .line 2668786
    invoke-interface {p2, v2}, LX/5pC;->getInt(I)I

    move-result v7

    invoke-virtual {p0, v7}, LX/5rN;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v7

    .line 2668787
    invoke-static {p1, v7, v0, v3}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;II)V

    .line 2668788
    add-int/lit8 v3, v2, 0x1

    .line 2668789
    if-ne v3, v4, :cond_2

    move v2, v3

    move v3, v0

    move v0, v1

    .line 2668790
    goto :goto_1

    .line 2668791
    :cond_0
    iget-object v0, p0, LX/JGe;->b:LX/JGy;

    invoke-virtual {v0, v2}, LX/JGy;->c(I)I

    move-result v4

    move v5, v2

    goto :goto_0

    .line 2668792
    :cond_1
    invoke-interface {p3}, LX/5pC;->size()I

    move-result v3

    .line 2668793
    invoke-interface {p3, v2}, LX/5pC;->getInt(I)I

    move-result v0

    move v8, v3

    move v3, v6

    move v6, v5

    move v5, v4

    move v4, v8

    goto :goto_1

    .line 2668794
    :cond_2
    invoke-interface {p3, v3}, LX/5pC;->getInt(I)I

    move-result v2

    move v8, v2

    move v2, v3

    move v3, v0

    move v0, v8

    .line 2668795
    goto :goto_1

    :cond_3
    if-ge v5, v0, :cond_5

    .line 2668796
    iget-object v7, p0, LX/JGe;->b:LX/JGy;

    .line 2668797
    iget-object v8, v7, LX/JGy;->d:[Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 2668798
    move v9, v6

    .line 2668799
    aget-object v8, v8, v9

    move-object v7, v8

    .line 2668800
    invoke-static {p1, v7, v5, v3}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;II)V

    .line 2668801
    add-int/lit8 v6, v6, 0x1

    .line 2668802
    iget-object v3, p0, LX/JGe;->b:LX/JGy;

    .line 2668803
    iget v7, v3, LX/JGy;->b:I

    move v3, v7

    .line 2668804
    if-ne v6, v3, :cond_4

    move v3, v5

    move v5, v1

    .line 2668805
    goto :goto_1

    .line 2668806
    :cond_4
    iget-object v3, p0, LX/JGe;->b:LX/JGy;

    invoke-virtual {v3, v6}, LX/JGy;->c(I)I

    move-result v3

    move v8, v3

    move v3, v5

    move v5, v8

    .line 2668807
    goto :goto_1

    .line 2668808
    :cond_5
    return-void
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5pC;LX/5pC;LX/5pC;)V
    .locals 11
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 2668863
    const v5, 0x7fffffff

    .line 2668864
    iget-object v0, p0, LX/JGe;->b:LX/JGy;

    const/4 v7, 0x0

    .line 2668865
    iput-object p3, v0, LX/JGy;->a:LX/5pC;

    .line 2668866
    if-nez p2, :cond_9

    .line 2668867
    invoke-static {v0, v7}, LX/JGy;->j(LX/JGy;I)V

    .line 2668868
    :cond_0
    iget-object v0, p0, LX/JGe;->b:LX/JGy;

    .line 2668869
    iget v3, v0, LX/JGy;->b:I

    move v0, v3

    .line 2668870
    add-int/lit8 v4, v0, -0x1

    .line 2668871
    if-ne v4, v2, :cond_1

    move v3, v2

    .line 2668872
    :goto_0
    if-nez p4, :cond_2

    move v0, v1

    .line 2668873
    :goto_1
    new-array v6, v0, [I

    .line 2668874
    if-lez v0, :cond_3

    .line 2668875
    invoke-static {p4}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2668876
    :goto_2
    if-ge v1, v0, :cond_3

    .line 2668877
    invoke-interface {p4, v1}, LX/5pC;->getInt(I)I

    move-result v7

    .line 2668878
    aput v7, v6, v1

    .line 2668879
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2668880
    :cond_1
    iget-object v0, p0, LX/JGe;->b:LX/JGy;

    invoke-virtual {v0, v4}, LX/JGy;->b(I)I

    move-result v3

    goto :goto_0

    .line 2668881
    :cond_2
    invoke-interface {p4}, LX/5pC;->size()I

    move-result v0

    goto :goto_1

    .line 2668882
    :cond_3
    invoke-static {v6}, Ljava/util/Arrays;->sort([I)V

    .line 2668883
    if-nez p4, :cond_4

    move v1, v2

    move v0, v5

    move v5, v4

    move v4, v3

    move v3, v2

    .line 2668884
    :goto_3
    if-le v4, v1, :cond_6

    .line 2668885
    invoke-static {p1, v4, v0}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;II)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    invoke-direct {p0, v0, v5}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 2668886
    add-int/lit8 v5, v5, -0x1

    .line 2668887
    if-ne v5, v2, :cond_5

    move v0, v2

    :goto_4
    move v8, v0

    move v0, v4

    move v4, v8

    goto :goto_3

    .line 2668888
    :cond_4
    array-length v0, v6

    add-int/lit8 v1, v0, -0x1

    .line 2668889
    aget v0, v6, v1

    move v8, v0

    move v0, v5

    move v5, v4

    move v4, v3

    move v3, v1

    move v1, v8

    goto :goto_3

    .line 2668890
    :cond_5
    iget-object v0, p0, LX/JGe;->b:LX/JGy;

    invoke-virtual {v0, v5}, LX/JGy;->b(I)I

    move-result v0

    goto :goto_4

    .line 2668891
    :cond_6
    if-le v1, v4, :cond_8

    .line 2668892
    invoke-static {p1, v1, v0}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;II)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 2668893
    add-int/lit8 v3, v3, -0x1

    .line 2668894
    if-ne v3, v2, :cond_7

    move v0, v2

    :goto_5
    move v8, v0

    move v0, v1

    move v1, v8

    goto :goto_3

    :cond_7
    aget v0, v6, v3

    goto :goto_5

    .line 2668895
    :cond_8
    return-void

    .line 2668896
    :cond_9
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v6

    .line 2668897
    add-int v3, v6, v6

    .line 2668898
    iget-object v4, v0, LX/JGy;->c:[I

    array-length v4, v4

    if-ge v4, v3, :cond_a

    .line 2668899
    new-array v3, v3, [I

    iput-object v3, v0, LX/JGy;->c:[I

    .line 2668900
    new-array v3, v6, [Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    iput-object v3, v0, LX/JGy;->d:[Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 2668901
    :cond_a
    invoke-static {v0, v6}, LX/JGy;->j(LX/JGy;I)V

    .line 2668902
    invoke-interface {p2, v7}, LX/5pC;->getInt(I)I

    move-result v3

    invoke-static {v0, v7, v7, v3}, LX/JGy;->a(LX/JGy;III)V

    .line 2668903
    const/4 v3, 0x1

    move v4, v3

    :goto_6
    if-ge v4, v6, :cond_0

    .line 2668904
    invoke-interface {p2, v4}, LX/5pC;->getInt(I)I

    move-result v7

    .line 2668905
    add-int/lit8 v3, v4, -0x1

    :goto_7
    if-ltz v3, :cond_b

    .line 2668906
    invoke-static {v0, v3}, LX/JGy;->g(LX/JGy;I)I

    move-result v8

    if-lt v8, v7, :cond_b

    .line 2668907
    add-int/lit8 v8, v3, 0x1

    invoke-static {v0, v3}, LX/JGy;->f(LX/JGy;I)I

    move-result v9

    invoke-static {v0, v3}, LX/JGy;->g(LX/JGy;I)I

    move-result v10

    invoke-static {v0, v8, v9, v10}, LX/JGy;->a(LX/JGy;III)V

    .line 2668908
    add-int/lit8 v3, v3, -0x1

    goto :goto_7

    .line 2668909
    :cond_b
    add-int/lit8 v3, v3, 0x1

    invoke-static {v0, v3, v4, v7}, LX/JGy;->a(LX/JGy;III)V

    .line 2668910
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 0

    .prologue
    .line 2668809
    invoke-direct {p0, p1, p2}, LX/JGe;->b(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 2668810
    invoke-virtual {p0, p1}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 2668811
    return-void
.end method

.method private static a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;II)V
    .locals 3

    .prologue
    .line 2668859
    if-gt p2, p3, :cond_0

    .line 2668860
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invariant failure, needs sorting! "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2668861
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 2668862
    return-void
.end method

.method private b(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 5

    .prologue
    .line 2668833
    instance-of v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2668834
    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2668835
    invoke-virtual {v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2668836
    iget-boolean v1, v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->p:Z

    move v1, v1

    .line 2668837
    if-eqz v1, :cond_2

    .line 2668838
    const/4 v3, -0x1

    move-object v2, p2

    .line 2668839
    :goto_0
    if-eqz v2, :cond_3

    .line 2668840
    instance-of v1, v2, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 2668841
    check-cast v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2668842
    invoke-virtual {v1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2668843
    iget-boolean v4, v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->p:Z

    move v4, v4

    .line 2668844
    if-eqz v4, :cond_1

    .line 2668845
    iget-object v4, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v4, v4

    .line 2668846
    if-eqz v4, :cond_1

    .line 2668847
    iget v2, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v2

    .line 2668848
    :goto_1
    iget-object v2, p0, LX/JGe;->c:LX/JH2;

    .line 2668849
    iget-object v3, v2, LX/JH2;->k:Ljava/util/ArrayList;

    .line 2668850
    iget v4, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v4, v4

    .line 2668851
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2668852
    iget-object v3, v2, LX/JH2;->l:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2668853
    :cond_0
    return-void

    .line 2668854
    :cond_1
    iget-object v1, v2, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v2, v1

    .line 2668855
    goto :goto_0

    .line 2668856
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v1

    :goto_2
    if-eq v0, v1, :cond_0

    .line 2668857
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v2

    invoke-direct {p0, v2, p1}, LX/JGe;->b(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 2668858
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_1
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 2668821
    invoke-virtual {p0, p1}, LX/5rN;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2668822
    iget-boolean v1, v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->p:Z

    move v1, v1

    .line 2668823
    if-eqz v1, :cond_0

    .line 2668824
    :goto_0
    return-void

    .line 2668825
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2668826
    iget-object v1, p0, LX/JGe;->c:LX/JH2;

    invoke-virtual {v1, v0}, LX/JH2;->b(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 3

    .prologue
    .line 2668812
    iget-object v0, p0, LX/JGe;->d:Lcom/facebook/react/views/image/ReactImageManager;

    if-eqz v0, :cond_0

    .line 2668813
    iget-object v0, p0, LX/JGe;->d:Lcom/facebook/react/views/image/ReactImageManager;

    invoke-virtual {v0}, Lcom/facebook/react/views/image/ReactImageManager;->t()LX/1Ae;

    move-result-object v0

    .line 2668814
    sput-object v0, LX/JGZ;->b:LX/1Ae;

    .line 2668815
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGe;->d:Lcom/facebook/react/views/image/ReactImageManager;

    .line 2668816
    :cond_0
    new-instance v0, Lcom/facebook/catalyst/shadow/flat/FlatRootShadowNode;

    invoke-direct {v0}, Lcom/facebook/catalyst/shadow/flat/FlatRootShadowNode;-><init>()V

    .line 2668817
    invoke-static {}, LX/5qa;->a()LX/5qa;

    move-result-object v1

    .line 2668818
    iget-object v2, p0, LX/JGe;->e:LX/5pY;

    invoke-virtual {v1, v2}, LX/5qa;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2668819
    sget-object v1, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaDirection;)V

    .line 2668820
    :cond_1
    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 2

    .prologue
    .line 2668717
    invoke-super {p0, p1}, LX/5rN;->a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 2668718
    instance-of v1, v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2668719
    :cond_0
    :goto_0
    return-object v0

    .line 2668720
    :cond_1
    invoke-virtual {p0, p1}, LX/5rN;->b(Ljava/lang/String;)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v1

    .line 2668721
    new-instance v0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;

    invoke-direct {v0, v1}, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;-><init>(Lcom/facebook/react/uimanager/ViewManager;)V

    goto :goto_0
.end method

.method public final a(IFFLcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 2668615
    invoke-direct {p0, p1}, LX/JGe;->f(I)V

    .line 2668616
    invoke-super {p0, p1, p2, p3, p4}, LX/5rN;->a(IFFLcom/facebook/react/bridge/Callback;)V

    .line 2668617
    return-void
.end method

.method public final a(IILX/5pC;)V
    .locals 2

    .prologue
    .line 2668618
    invoke-direct {p0, p1}, LX/JGe;->f(I)V

    .line 2668619
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    .line 2668620
    iget-object v1, v0, LX/JH2;->o:Ljava/util/ArrayList;

    iget-object p0, v0, LX/JH2;->d:LX/JGq;

    invoke-virtual {p0, p1, p2, p3}, LX/JGq;->b(IILX/5pC;)LX/JGp;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2668621
    return-void
.end method

.method public final a(ILX/5pC;)V
    .locals 4

    .prologue
    .line 2668622
    invoke-virtual {p0, p1}, LX/5rN;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 2668623
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2668624
    invoke-interface {p2, v0}, LX/5pC;->getInt(I)I

    move-result v2

    invoke-virtual {p0, v2}, LX/5rN;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v2

    .line 2668625
    add-int/lit8 v3, v0, -0x1

    invoke-static {v1, v2, v0, v3}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;II)V

    .line 2668626
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2668627
    :cond_0
    return-void
.end method

.method public final a(ILX/5pC;LX/5pC;LX/5pC;LX/5pC;LX/5pC;)V
    .locals 1
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2668710
    invoke-virtual {p0, p1}, LX/5rN;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 2668711
    invoke-direct {p0, v0, p2, p3, p6}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5pC;LX/5pC;LX/5pC;)V

    .line 2668712
    invoke-direct {p0, v0, p4, p5}, LX/JGe;->a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5pC;LX/5pC;)V

    .line 2668713
    return-void
.end method

.method public final a(ILX/5pC;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 2668628
    invoke-direct {p0, p1}, LX/JGe;->f(I)V

    .line 2668629
    invoke-super {p0, p1, p2, p3, p4}, LX/5rN;->a(ILX/5pC;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    .line 2668630
    return-void
.end method

.method public final a(ILcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 2668631
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/JGe;->a(IZLcom/facebook/react/bridge/Callback;)V

    .line 2668632
    return-void
.end method

.method public final a(IZ)V
    .locals 4

    .prologue
    .line 2668633
    invoke-virtual {p0, p1}, LX/5rN;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 2668634
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2668635
    iget-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v1

    .line 2668636
    goto :goto_0

    .line 2668637
    :cond_0
    iget v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v2, v1

    .line 2668638
    move-object v1, v0

    .line 2668639
    :goto_1
    instance-of v0, v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    invoke-virtual {v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2668640
    iget-object v0, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v1, v0

    .line 2668641
    goto :goto_1

    .line 2668642
    :cond_1
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    .line 2668643
    iget-object v3, v0, LX/JH2;->d:LX/JGq;

    move-object v3, v3

    .line 2668644
    if-nez v1, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v3, v0, p1, p2}, LX/5rl;->a(IIZ)V

    .line 2668645
    return-void

    .line 2668646
    :cond_2
    iget v0, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v0

    .line 2668647
    goto :goto_2
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;FF)V
    .locals 11

    .prologue
    .line 2668648
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    check-cast p1, Lcom/facebook/catalyst/shadow/flat/FlatRootShadowNode;

    const/high16 v9, 0x7f800000    # Float.POSITIVE_INFINITY

    const/high16 v7, -0x800000    # Float.NEGATIVE_INFINITY

    .line 2668649
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->z()F

    move-result v1

    .line 2668650
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->A()F

    move-result v2

    .line 2668651
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v3

    .line 2668652
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v4

    .line 2668653
    add-float v5, v3, v1

    .line 2668654
    add-float v6, v4, v2

    move-object v1, v0

    move-object v2, p1

    move v8, v7

    move v10, v9

    .line 2668655
    invoke-static/range {v1 .. v10}, LX/JH2;->a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFFFFF)Z

    move-object v1, v0

    move-object v2, p1

    .line 2668656
    invoke-static/range {v1 .. v6}, LX/JH2;->a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFF)V

    .line 2668657
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;ILX/5rC;)V
    .locals 1
    .param p3    # LX/5rC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2668658
    instance-of v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v0, :cond_2

    .line 2668659
    check-cast p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2668660
    if-eqz p3, :cond_0

    .line 2668661
    invoke-virtual {p1, p3}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->b(LX/5rC;)V

    .line 2668662
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2668663
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    invoke-virtual {v0, p1, p3}, LX/JH2;->a(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;LX/5rC;)V

    .line 2668664
    :cond_1
    :goto_0
    return-void

    .line 2668665
    :cond_2
    invoke-super {p0, p1, p2, p3}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;ILX/5rC;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V
    .locals 1

    .prologue
    .line 2668666
    instance-of v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v0, :cond_1

    .line 2668667
    check-cast p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2668668
    invoke-virtual {p1, p3}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->b(LX/5rC;)V

    .line 2668669
    invoke-virtual {p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2668670
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    invoke-virtual {v0, p1, p3}, LX/JH2;->a(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;LX/5rC;)V

    .line 2668671
    :cond_0
    :goto_0
    return-void

    .line 2668672
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 2668673
    invoke-super {p0}, LX/5rN;->b()V

    .line 2668674
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    iget-object v1, p0, LX/5rN;->a:LX/5s9;

    const/4 v3, 0x0

    .line 2668675
    iget-object v2, v0, LX/JH2;->p:LX/JGf;

    if-eqz v2, :cond_0

    .line 2668676
    iget-object v2, v0, LX/JH2;->i:Ljava/util/ArrayList;

    invoke-static {v2}, LX/JH2;->a(Ljava/util/ArrayList;)[I

    move-result-object v2

    .line 2668677
    iget-object v4, v0, LX/JH2;->i:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 2668678
    iget-object v4, v0, LX/JH2;->p:LX/JGf;

    .line 2668679
    iput-object v2, v4, LX/JGf;->b:[I

    .line 2668680
    const/4 v2, 0x0

    iput-object v2, v0, LX/JH2;->p:LX/JGf;

    .line 2668681
    :cond_0
    iget-object v2, v0, LX/JH2;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    :goto_0
    if-eq v4, v5, :cond_1

    .line 2668682
    iget-object p0, v0, LX/JH2;->d:LX/JGq;

    iget-object v2, v0, LX/JH2;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5rT;

    .line 2668683
    invoke-virtual {p0, v2}, LX/5rl;->a(LX/5rT;)V

    .line 2668684
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2668685
    :cond_1
    iget-object v2, v0, LX/JH2;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2668686
    iget-object v2, v0, LX/JH2;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    :goto_1
    if-eq v4, v5, :cond_2

    .line 2668687
    iget-object p0, v0, LX/JH2;->d:LX/JGq;

    iget-object v2, v0, LX/JH2;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5rT;

    .line 2668688
    invoke-virtual {p0, v2}, LX/5rl;->a(LX/5rT;)V

    .line 2668689
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 2668690
    :cond_2
    iget-object v2, v0, LX/JH2;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2668691
    iget-object v2, v0, LX/JH2;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_2
    if-eq v3, v4, :cond_3

    .line 2668692
    iget-object v2, v0, LX/JH2;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5r0;

    invoke-virtual {v1, v2}, LX/5s9;->a(LX/5r0;)V

    .line 2668693
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2668694
    :cond_3
    iget-object v2, v0, LX/JH2;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2668695
    iget-object v2, v0, LX/JH2;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 2668696
    iget-object v2, v0, LX/JH2;->d:LX/JGq;

    iget-object v3, v0, LX/JH2;->k:Ljava/util/ArrayList;

    iget-object v4, v0, LX/JH2;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, LX/JGq;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 2668697
    iget-object v2, v0, LX/JH2;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2668698
    iget-object v2, v0, LX/JH2;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2668699
    :cond_4
    iget-object v2, v0, LX/JH2;->d:LX/JGq;

    invoke-virtual {v2}, LX/JGq;->f()V

    .line 2668700
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2668701
    iget-object v0, p0, LX/JGe;->c:LX/JH2;

    .line 2668702
    iget-object v1, v0, LX/JH2;->k:Ljava/util/ArrayList;

    neg-int p0, p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2668703
    iget-object v1, v0, LX/JH2;->l:Ljava/util/ArrayList;

    const/4 p0, -0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2668704
    return-void
.end method

.method public final b(II)V
    .locals 0

    .prologue
    .line 2668705
    invoke-direct {p0, p1}, LX/JGe;->f(I)V

    .line 2668706
    invoke-super {p0, p1, p2}, LX/5rN;->b(II)V

    .line 2668707
    return-void
.end method

.method public final b(ILcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 2668708
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, LX/JGe;->a(IZLcom/facebook/react/bridge/Callback;)V

    .line 2668709
    return-void
.end method
