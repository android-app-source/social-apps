.class public final LX/HdN;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HdO;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

.field public final synthetic b:LX/HdO;


# direct methods
.method public constructor <init>(LX/HdO;)V
    .locals 1

    .prologue
    .line 2488272
    iput-object p1, p0, LX/HdN;->b:LX/HdO;

    .line 2488273
    move-object v0, p1

    .line 2488274
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2488275
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2488276
    const-string v0, "TopicSourceHscrollItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2488277
    if-ne p0, p1, :cond_1

    .line 2488278
    :cond_0
    :goto_0
    return v0

    .line 2488279
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2488280
    goto :goto_0

    .line 2488281
    :cond_3
    check-cast p1, LX/HdN;

    .line 2488282
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2488283
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2488284
    if-eq v2, v3, :cond_0

    .line 2488285
    iget-object v2, p0, LX/HdN;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/HdN;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    iget-object v3, p1, LX/HdN;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2488286
    goto :goto_0

    .line 2488287
    :cond_4
    iget-object v2, p1, LX/HdN;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
