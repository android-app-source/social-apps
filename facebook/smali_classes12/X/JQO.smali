.class public LX/JQO;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JQO;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Landroid/widget/TextView;

.field public c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

.field public d:Z

.field public e:Lcom/facebook/friends/ui/SmartButtonLite;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2691591
    new-instance v0, LX/JQN;

    invoke-direct {v0}, LX/JQN;-><init>()V

    sput-object v0, LX/JQO;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2691581
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 2691582
    const v0, 0x7f030951

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2691583
    const v0, 0x7f0d17dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    iput-object v0, p0, LX/JQO;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 2691584
    const v0, 0x7f0d17de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JQO;->b:Landroid/widget/TextView;

    .line 2691585
    const v0, 0x7f0d0393

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, LX/JQO;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2691586
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, LX/JQO;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-direct {v0, p1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object p1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, p1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2691587
    iget-object p1, p0, LX/JQO;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2691588
    iget-object v0, p0, LX/JQO;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    sget-object p1, LX/1vY;->PHOTO:LX/1vY;

    invoke-static {v0, p1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2691589
    iget-object v0, p0, LX/JQO;->b:Landroid/widget/TextView;

    sget-object p1, LX/1vY;->USER_NAME:LX/1vY;

    invoke-static {v0, p1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2691590
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2691580
    iget-boolean v0, p0, LX/JQO;->d:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x414ddf6e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2691568
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2691569
    const/4 v1, 0x1

    .line 2691570
    iput-boolean v1, p0, LX/JQO;->d:Z

    .line 2691571
    const/16 v1, 0x2d

    const v2, -0x5af6df4c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x51131660

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2691576
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2691577
    const/4 v1, 0x0

    .line 2691578
    iput-boolean v1, p0, LX/JQO;->d:Z

    .line 2691579
    const/16 v1, 0x2d

    const v2, -0x64be82a1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setInstallClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2691574
    iget-object v0, p0, LX/JQO;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2691575
    return-void
.end method

.method public setPhotoClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2691572
    iget-object v0, p0, LX/JQO;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2691573
    return-void
.end method
