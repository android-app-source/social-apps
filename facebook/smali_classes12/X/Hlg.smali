.class public final LX/Hlg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2498812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0nX;LX/HlZ;Z)V
    .locals 4

    .prologue
    .line 2498813
    if-eqz p2, :cond_0

    .line 2498814
    invoke-virtual {p0}, LX/0nX;->f()V

    .line 2498815
    :cond_0
    iget-object v0, p1, LX/HlZ;->a:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2498816
    const-string v0, "age_ms"

    iget-object v1, p1, LX/HlZ;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 2498817
    :cond_1
    iget-object v0, p1, LX/HlZ;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2498818
    const-string v0, "hardware_address"

    iget-object v1, p1, LX/HlZ;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498819
    :cond_2
    iget-object v0, p1, LX/HlZ;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2498820
    const-string v0, "rssi_dbm"

    iget-object v1, p1, LX/HlZ;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 2498821
    :cond_3
    iget-object v0, p1, LX/HlZ;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2498822
    const-string v0, "network_name"

    iget-object v1, p1, LX/HlZ;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498823
    :cond_4
    iget-object v0, p1, LX/HlZ;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 2498824
    const-string v0, "frequency_mhz"

    iget-object v1, p1, LX/HlZ;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 2498825
    :cond_5
    if-eqz p2, :cond_6

    .line 2498826
    invoke-virtual {p0}, LX/0nX;->g()V

    .line 2498827
    :cond_6
    return-void
.end method
