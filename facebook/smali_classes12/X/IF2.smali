.class public LX/IF2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2553533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2553534
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x2c

    invoke-static {v1}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v1

    invoke-virtual {v1}, LX/2Cb;->trimResults()LX/2Cb;

    move-result-object v1

    sget-char v2, LX/IEy;->c:C

    const-string v3, "happy"

    invoke-interface {p1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2Cb;->splitToList(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/IF2;->a:Ljava/util/Set;

    .line 2553535
    const-string v0, "(.*[^a-zA-Z]|^)([a-zA-Z]+)([^a-zA-Z]*)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/IF2;->b:Ljava/util/regex/Pattern;

    .line 2553536
    return-void
.end method
