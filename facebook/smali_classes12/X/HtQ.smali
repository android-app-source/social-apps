.class public LX/HtQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            "LX/0Ot",
            "<+",
            "LX/HtM;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HtT;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516173
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OG_COMPOSER_SIMPLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    move-object v1, p1

    move-object v3, p1

    move-object v5, p1

    move-object v7, p2

    move-object v9, p2

    invoke-static/range {v0 .. v9}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/HtQ;->a:Ljava/util/Map;

    .line 2516174
    iget-object v0, p0, LX/HtQ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/HtQ;->b:LX/0Px;

    .line 2516175
    return-void
.end method

.method public static c(LX/HtQ;Ljava/util/List;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2516176
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 2516177
    iget-object v2, p0, LX/HtQ;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2516178
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Ljava/util/List;)LX/HtM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;)",
            "LX/HtM;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2516179
    invoke-static {p0, p1}, LX/HtQ;->c(LX/HtQ;Ljava/util/List;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    move-result-object v0

    .line 2516180
    if-nez v0, :cond_0

    .line 2516181
    const/4 v0, 0x0

    .line 2516182
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/HtQ;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HtM;

    goto :goto_0
.end method
