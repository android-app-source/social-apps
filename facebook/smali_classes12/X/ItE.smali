.class public final LX/ItE;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/service/model/SendMessageParams;

.field public final synthetic b:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic c:Lcom/facebook/messaging/send/client/SendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 0

    .prologue
    .line 2622211
    iput-object p1, p0, LX/ItE;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iput-object p2, p0, LX/ItE;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    iput-object p3, p0, LX/ItE;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2622212
    iget-object v0, p0, LX/ItE;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    iget-object v1, p0, LX/ItE;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2622213
    iget-object v0, p0, LX/ItE;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, LX/ItE;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2622214
    invoke-static {v0, p1, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622215
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 2622216
    iget-object v0, p0, LX/ItE;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    iget-object v1, p0, LX/ItE;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2622217
    iget-object v0, p0, LX/ItE;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, LX/ItE;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->m(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622218
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2622219
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/ItE;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
