.class public final LX/HlO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2Ct;


# direct methods
.method public constructor <init>(LX/2Ct;)V
    .locals 0

    .prologue
    .line 2498499
    iput-object p1, p0, LX/HlO;->a:LX/2Ct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x4d7bfcd5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2498500
    const-string v1, "extra_has_explicit_place"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2498501
    iget-object v1, p0, LX/HlO;->a:LX/2Ct;

    .line 2498502
    iget-object v4, v1, LX/2Ct;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/2Fn;->c:LX/0Tn;

    iget-object v6, v1, LX/2Ct;->n:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/32 v8, 0x124f80

    add-long/2addr v6, v8

    invoke-interface {v4, v5, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 2498503
    iget-object v4, v1, LX/2Ct;->f:LX/2Cw;

    invoke-virtual {v4}, LX/2Cw;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2498504
    sget-object v4, LX/2Ct;->c:LX/2Cu;

    invoke-static {v1, v4}, LX/2Ct;->a(LX/2Ct;LX/2Cu;)V

    .line 2498505
    :cond_0
    const/16 v1, 0x27

    const v2, 0x56605345

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
