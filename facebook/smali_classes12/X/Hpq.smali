.class public LX/Hpq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/Bdg;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/Hpm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2509979
    new-instance v0, LX/Bdg;

    invoke-direct {v0}, LX/Bdg;-><init>()V

    sput-object v0, LX/Hpq;->a:LX/Bdg;

    return-void
.end method

.method public constructor <init>(LX/Hpm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2509991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2509992
    iput-object p1, p0, LX/Hpq;->b:LX/Hpm;

    .line 2509993
    return-void
.end method

.method public static a(LX/0QB;)LX/Hpq;
    .locals 4

    .prologue
    .line 2509980
    const-class v1, LX/Hpq;

    monitor-enter v1

    .line 2509981
    :try_start_0
    sget-object v0, LX/Hpq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2509982
    sput-object v2, LX/Hpq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2509983
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2509984
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2509985
    new-instance p0, LX/Hpq;

    invoke-static {v0}, LX/Hpm;->a(LX/0QB;)LX/Hpm;

    move-result-object v3

    check-cast v3, LX/Hpm;

    invoke-direct {p0, v3}, LX/Hpq;-><init>(LX/Hpm;)V

    .line 2509986
    move-object v0, p0

    .line 2509987
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2509988
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hpq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2509989
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2509990
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
