.class public final LX/IgR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/media/folder/Folder;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2601087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2601088
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2601089
    if-nez p1, :cond_0

    .line 2601090
    const/4 v0, 0x0

    .line 2601091
    :goto_0
    return-object v0

    .line 2601092
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;

    .line 2601093
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2601094
    iget-object v3, v0, Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/folder/Folder;

    .line 2601095
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2601096
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2601097
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
