.class public final LX/J6c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J3r;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 0

    .prologue
    .line 2649814
    iput-object p1, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;B)V
    .locals 0

    .prologue
    .line 2649813
    invoke-direct {p0, p1}, LX/J6c;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2649796
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    const-string v1, "FETCH_PROGRESS_DIALOG_TAG"

    invoke-static {v0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->b(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;Ljava/lang/String;)V

    .line 2649797
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2649798
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 2649799
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649800
    iput-object v3, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    .line 2649801
    :cond_0
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    .line 2649802
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 2649803
    :cond_1
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649804
    iput-object v3, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    .line 2649805
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v1, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649806
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649807
    const-string v2, "succes_data_load"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649808
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    iget-object v1, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2649809
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    .line 2649810
    invoke-static {v0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;LX/0h5;)V

    .line 2649811
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->x:LX/J6h;

    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2649812
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2649794
    iget-object v0, p0, LX/J6c;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-static {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->n(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    .line 2649795
    return-void
.end method
