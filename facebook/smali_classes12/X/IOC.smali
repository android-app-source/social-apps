.class public final LX/IOC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:LX/IOD;


# direct methods
.method public constructor <init>(LX/IOD;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 2571572
    iput-object p1, p0, LX/IOC;->b:LX/IOD;

    iput-object p2, p0, LX/IOC;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    const v0, 0x11b682b0

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2571573
    new-instance v1, LX/21A;

    invoke-direct {v1}, LX/21A;-><init>()V

    const-string v2, "community"

    .line 2571574
    iput-object v2, v1, LX/21A;->c:Ljava/lang/String;

    .line 2571575
    move-object v1, v1

    .line 2571576
    sget-object v2, LX/1Qt;->GROUPS:LX/1Qt;

    invoke-static {v2}, LX/9Ir;->a(LX/1Qt;)LX/21D;

    move-result-object v2

    .line 2571577
    iput-object v2, v1, LX/21A;->i:LX/21D;

    .line 2571578
    move-object v1, v1

    .line 2571579
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    move-object v1, v1

    .line 2571580
    new-instance v2, LX/8qL;

    invoke-direct {v2}, LX/8qL;-><init>()V

    iget-object v3, p0, LX/IOC;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2571581
    iput-object v3, v2, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2571582
    move-object v2, v2

    .line 2571583
    iget-object v3, p0, LX/IOC;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    .line 2571584
    iput-object v3, v2, LX/8qL;->d:Ljava/lang/String;

    .line 2571585
    move-object v2, v2

    .line 2571586
    iget-object v3, p0, LX/IOC;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    .line 2571587
    iput-object v3, v2, LX/8qL;->e:Ljava/lang/String;

    .line 2571588
    move-object v2, v2

    .line 2571589
    iput-boolean v6, v2, LX/8qL;->i:Z

    .line 2571590
    move-object v2, v2

    .line 2571591
    iput-boolean v4, v2, LX/8qL;->h:Z

    .line 2571592
    move-object v2, v2

    .line 2571593
    iput-boolean v4, v2, LX/8qL;->j:Z

    .line 2571594
    move-object v2, v2

    .line 2571595
    iput-object v1, v2, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2571596
    move-object v1, v2

    .line 2571597
    invoke-virtual {v1}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v1

    .line 2571598
    new-instance v2, LX/8qO;

    invoke-direct {v2}, LX/8qO;-><init>()V

    .line 2571599
    iput-boolean v4, v2, LX/8qO;->a:Z

    .line 2571600
    move-object v2, v2

    .line 2571601
    invoke-virtual {v2}, LX/8qO;->a()Lcom/facebook/ufiservices/flyout/PopoverParams;

    move-result-object v2

    .line 2571602
    iget-object v3, p0, LX/IOC;->b:LX/IOD;

    iget-object v3, v3, LX/IOD;->b:LX/1nI;

    iget-object v4, p0, LX/IOC;->b:LX/IOD;

    invoke-virtual {v4}, LX/IOD;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4, v1, v2}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;)V

    .line 2571603
    const v1, -0x2fa468eb

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
