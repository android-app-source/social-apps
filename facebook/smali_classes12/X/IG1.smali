.class public LX/IG1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field private d:I

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLInterfaces$BackgroundLocationPrivacyPickerOptionEdge;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/IFO;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;)V
    .locals 1
    .param p3    # Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2555061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555062
    invoke-static {p2}, LX/IFO;->c(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IG1;->a:Ljava/util/List;

    .line 2555063
    invoke-virtual {p1, p2}, LX/IFO;->d(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IG1;->b:LX/0Px;

    .line 2555064
    const/4 v0, -0x1

    .line 2555065
    invoke-static {p2}, LX/IFO;->o(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;

    move-result-object p1

    .line 2555066
    if-nez p1, :cond_5

    .line 2555067
    :cond_0
    :goto_0
    move v0, v0

    .line 2555068
    if-ltz v0, :cond_3

    .line 2555069
    :cond_1
    :goto_1
    move v0, v0

    .line 2555070
    iput v0, p0, LX/IG1;->c:I

    .line 2555071
    invoke-static {p2}, LX/IFO;->f(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)I

    move-result v0

    iput v0, p0, LX/IG1;->d:I

    .line 2555072
    const/4 v0, 0x0

    .line 2555073
    if-nez p3, :cond_7

    .line 2555074
    :cond_2
    :goto_2
    move-object v0, v0

    .line 2555075
    iput-object v0, p0, LX/IG1;->e:LX/0Px;

    .line 2555076
    return-void

    .line 2555077
    :cond_3
    const/4 v0, -0x1

    .line 2555078
    if-nez p2, :cond_6

    .line 2555079
    :cond_4
    :goto_3
    move v0, v0

    .line 2555080
    if-gez v0, :cond_1

    .line 2555081
    const/4 v0, 0x0

    goto :goto_1

    .line 2555082
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object p1

    .line 2555083
    if-eqz p1, :cond_0

    .line 2555084
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel$FriendsSharingLocationConnectionModel;->a()I

    move-result v0

    goto :goto_0

    .line 2555085
    :cond_6
    invoke-virtual {p2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$FriendsSharingModel;

    move-result-object p1

    .line 2555086
    if-eqz p1, :cond_4

    .line 2555087
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$FriendsSharingModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$FriendsSharingModel$FriendsSharingLocationConnectionModel;

    move-result-object p1

    .line 2555088
    if-eqz p1, :cond_4

    .line 2555089
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$FriendsSharingModel$FriendsSharingLocationConnectionModel;->a()I

    move-result v0

    goto :goto_3

    .line 2555090
    :cond_7
    invoke-virtual {p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->a()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    move-result-object p1

    .line 2555091
    if-eqz p1, :cond_2

    .line 2555092
    invoke-virtual {p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->a()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_2
.end method
