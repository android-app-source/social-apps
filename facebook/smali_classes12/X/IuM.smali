.class public final enum LX/IuM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IuM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IuM;

.field public static final enum COMPLETED:LX/IuM;

.field public static final enum FAILED_GENERATE:LX/IuM;

.field public static final enum FAILED_UPLOAD:LX/IuM;

.field public static final enum GENERATING:LX/IuM;

.field public static final enum NOT_STARTED:LX/IuM;

.field public static final enum UPLOADING:LX/IuM;


# instance fields
.field private mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2626561
    new-instance v0, LX/IuM;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v4, v4}, LX/IuM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuM;->NOT_STARTED:LX/IuM;

    .line 2626562
    new-instance v0, LX/IuM;

    const-string v1, "GENERATING"

    invoke-direct {v0, v1, v5, v5}, LX/IuM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuM;->GENERATING:LX/IuM;

    .line 2626563
    new-instance v0, LX/IuM;

    const-string v1, "UPLOADING"

    invoke-direct {v0, v1, v6, v6}, LX/IuM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuM;->UPLOADING:LX/IuM;

    .line 2626564
    new-instance v0, LX/IuM;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v7, v7}, LX/IuM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuM;->COMPLETED:LX/IuM;

    .line 2626565
    new-instance v0, LX/IuM;

    const-string v1, "FAILED_UPLOAD"

    invoke-direct {v0, v1, v8, v8}, LX/IuM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuM;->FAILED_UPLOAD:LX/IuM;

    .line 2626566
    new-instance v0, LX/IuM;

    const-string v1, "FAILED_GENERATE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/IuM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IuM;->FAILED_GENERATE:LX/IuM;

    .line 2626567
    const/4 v0, 0x6

    new-array v0, v0, [LX/IuM;

    sget-object v1, LX/IuM;->NOT_STARTED:LX/IuM;

    aput-object v1, v0, v4

    sget-object v1, LX/IuM;->GENERATING:LX/IuM;

    aput-object v1, v0, v5

    sget-object v1, LX/IuM;->UPLOADING:LX/IuM;

    aput-object v1, v0, v6

    sget-object v1, LX/IuM;->COMPLETED:LX/IuM;

    aput-object v1, v0, v7

    sget-object v1, LX/IuM;->FAILED_UPLOAD:LX/IuM;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/IuM;->FAILED_GENERATE:LX/IuM;

    aput-object v2, v0, v1

    sput-object v0, LX/IuM;->$VALUES:[LX/IuM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2626558
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2626559
    iput p3, p0, LX/IuM;->mValue:I

    .line 2626560
    return-void
.end method

.method public static from(I)LX/IuM;
    .locals 5

    .prologue
    .line 2626553
    invoke-static {}, LX/IuM;->values()[LX/IuM;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2626554
    invoke-virtual {v0}, LX/IuM;->getValue()I

    move-result v4

    if-ne p0, v4, :cond_0

    .line 2626555
    :goto_1
    return-object v0

    .line 2626556
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2626557
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/IuM;
    .locals 1

    .prologue
    .line 2626550
    const-class v0, LX/IuM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IuM;

    return-object v0
.end method

.method public static values()[LX/IuM;
    .locals 1

    .prologue
    .line 2626552
    sget-object v0, LX/IuM;->$VALUES:[LX/IuM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IuM;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2626551
    iget v0, p0, LX/IuM;->mValue:I

    return v0
.end method
