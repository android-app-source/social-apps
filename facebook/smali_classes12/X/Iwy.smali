.class public final LX/Iwy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V
    .locals 0

    .prologue
    .line 2631129
    iput-object p1, p0, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 2631131
    iget-object v1, p0, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    if-nez p1, :cond_0

    const-string v0, ""

    .line 2631132
    :goto_0
    iput-object v0, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->g:Ljava/lang/String;

    .line 2631133
    iget-object v0, p0, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->h:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2631134
    iget-object v0, p0, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->a:LX/9Wz;

    iget-object v1, p0, LX/Iwy;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->g:Ljava/lang/String;

    const/16 v2, 0x32

    const-string v3, "Page"

    new-instance v5, LX/Iwv;

    invoke-direct {v5, p0, v6}, LX/Iwv;-><init>(LX/Iwy;Ljava/lang/String;)V

    move-object v6, v4

    .line 2631135
    if-eqz v1, :cond_1

    const/4 v7, 0x1

    :goto_1
    invoke-static {v7}, LX/0PB;->checkState(Z)V

    .line 2631136
    iget-object p0, v0, LX/9Wz;->b:LX/1Ck;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "simple_search_loader_key"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v7, v0

    move-object v8, v1

    move v9, v2

    move-object v10, v4

    move-object v11, v3

    move-object v12, v6

    .line 2631137
    new-instance v1, LX/9zs;

    invoke-direct {v1}, LX/9zs;-><init>()V

    const-string v2, "query"

    invoke-virtual {v1, v2, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "count"

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after"

    invoke-virtual {v1, v2, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {v1, v2, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_picture_size"

    const/high16 v3, 0x428c0000    # 70.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2631138
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2631139
    iget-object v2, v7, LX/9Wz;->a:LX/0tX;

    .line 2631140
    new-instance v3, LX/9Ws;

    invoke-direct {v3}, LX/9Ws;-><init>()V

    move-object v3, v3

    .line 2631141
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2631142
    move-object v7, v1

    .line 2631143
    new-instance v8, LX/9Wy;

    invoke-direct {v8, v0, v5}, LX/9Wy;-><init>(LX/9Wz;LX/Iwv;)V

    invoke-virtual {p0, p1, v7, v8}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2631144
    return-void

    .line 2631145
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2631146
    :cond_1
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2631147
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2631130
    return-void
.end method
