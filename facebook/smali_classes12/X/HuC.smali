.class public final LX/HuC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HuG;


# direct methods
.method public constructor <init>(LX/HuG;)V
    .locals 0

    .prologue
    .line 2517206
    iput-object p1, p0, LX/HuC;->a:LX/HuG;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2517207
    iget-object v0, p0, LX/HuC;->a:LX/HuG;

    .line 2517208
    iget-object v1, v0, LX/HuG;->f:LX/03V;

    sget-object v2, LX/HuG;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2517209
    iget-object v0, p0, LX/HuC;->a:LX/HuG;

    const/4 v1, 0x0

    .line 2517210
    iput-object v1, v0, LX/HuG;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2517211
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 2517212
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2517213
    iget-object v2, p0, LX/HuC;->a:LX/HuG;

    if-nez p1, :cond_0

    move-object v0, v1

    :goto_0
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2517214
    iget-object v3, v2, LX/HuG;->u:LX/Hsu;

    if-nez v3, :cond_1

    .line 2517215
    :goto_1
    iget-object v0, p0, LX/HuC;->a:LX/HuG;

    .line 2517216
    iput-object v1, v0, LX/HuG;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2517217
    return-void

    .line 2517218
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2517219
    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    goto :goto_0

    .line 2517220
    :cond_1
    iget-object v3, v2, LX/HuG;->u:LX/Hsu;

    invoke-virtual {v3, v9}, LX/Hsu;->setLoadingIndicatorVisibility(Z)V

    .line 2517221
    iget-object v3, v2, LX/HuG;->u:LX/Hsu;

    .line 2517222
    iget-object v4, v3, LX/Hsu;->a:Landroid/widget/FrameLayout;

    move-object v5, v4

    .line 2517223
    iget-object v3, v2, LX/HuG;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    .line 2517224
    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0j0;

    check-cast v4, LX/0j8;

    invoke-interface {v4}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    .line 2517225
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v4

    .line 2517226
    iget-boolean v6, v2, LX/HuG;->s:Z

    if-nez v6, :cond_2

    .line 2517227
    iput-boolean v10, v2, LX/HuG;->s:Z

    .line 2517228
    iget-object v6, v2, LX/HuG;->g:LX/0gd;

    sget-object v7, LX/0ge;->CHECKIN_PREVIEW_SEEN:LX/0ge;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0j0;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v7, v3}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2517229
    :cond_2
    new-instance v3, LX/4X8;

    invoke-direct {v3}, LX/4X8;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v7

    .line 2517230
    iput-wide v7, v3, LX/4X8;->b:D

    .line 2517231
    move-object v3, v3

    .line 2517232
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v7

    .line 2517233
    iput-wide v7, v3, LX/4X8;->c:D

    .line 2517234
    move-object v3, v3

    .line 2517235
    invoke-virtual {v3}, LX/4X8;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v3

    .line 2517236
    const-string v4, "checkin_story_preview"

    invoke-static {v11, v4, v3}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLocation;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v3

    .line 2517237
    new-instance v4, LX/5eI;

    iget-object v6, v2, LX/HuG;->l:Lcom/facebook/maps/rows/MapPartDefinition;

    sget-object v7, LX/1Qm;->a:LX/1PW;

    invoke-direct {v4, v6, v7}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    .line 2517238
    iget-object v6, v2, LX/HuG;->o:LX/3BL;

    invoke-virtual {v6, v11}, LX/3BL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v6

    .line 2517239
    const v7, 0x3ff33333    # 1.9f

    invoke-static {v6, v7}, LX/3BL;->a(IF)I

    move-result v7

    .line 2517240
    new-instance v8, LX/3BY;

    invoke-direct {v8, v3, v10, v6, v7}, LX/3BY;-><init>(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;ZII)V

    invoke-virtual {v4, v8}, LX/5eI;->a(Ljava/lang/Object;)V

    .line 2517241
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v6, 0x7f030287

    invoke-virtual {v3, v6, v5, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 2517242
    const v3, 0x7f0d0930

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/maps/FbStaticMapView;

    .line 2517243
    invoke-virtual {v4, v3}, LX/5eI;->a(Landroid/view/View;)V

    .line 2517244
    new-instance v4, LX/5eI;

    iget-object v3, v2, LX/HuG;->m:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    new-instance v7, LX/HuF;

    invoke-direct {v7}, LX/HuF;-><init>()V

    invoke-direct {v4, v3, v7}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    .line 2517245
    invoke-static {v0}, LX/HuK;->a(LX/5CI;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v3

    .line 2517246
    new-instance v7, LX/31T;

    new-instance v8, LX/23u;

    invoke-direct {v8}, LX/23u;-><init>()V

    .line 2517247
    iput-object v3, v8, LX/23u;->A:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 2517248
    move-object v8, v8

    .line 2517249
    invoke-virtual {v8}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    invoke-direct {v7, v8, v3, v9}, LX/31T;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;Z)V

    invoke-virtual {v4, v7}, LX/5eI;->a(Ljava/lang/Object;)V

    .line 2517250
    const v3, 0x7f0d116c

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2517251
    invoke-virtual {v4, v3}, LX/5eI;->a(Landroid/view/View;)V

    .line 2517252
    iget-object v3, v2, LX/HuG;->h:LX/0ad;

    sget-short v4, LX/5HH;->d:S

    invoke-interface {v3, v4, v9}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2517253
    iget-object v3, v2, LX/HuG;->h:LX/0ad;

    sget-short v4, LX/5HH;->f:S

    const/4 v7, 0x0

    invoke-interface {v3, v4, v7}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2517254
    const v3, 0x7f0d0932

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2517255
    iget-object v4, v2, LX/HuG;->n:LX/0wM;

    const v7, 0x7f0207b5

    const/4 v8, -0x1

    invoke-virtual {v4, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2517256
    :goto_2
    new-instance v4, LX/HuE;

    invoke-direct {v4, v2}, LX/HuE;-><init>(LX/HuG;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2517257
    :cond_3
    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2517258
    :cond_4
    const v3, 0x7f0d0931

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    goto :goto_2
.end method
