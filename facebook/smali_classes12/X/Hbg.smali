.class public final LX/Hbg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V
    .locals 0

    .prologue
    .line 2486032
    iput-object p1, p0, LX/Hbg;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x5dfcf554

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486033
    iget-object v1, p0, LX/Hbg;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    iget-object v1, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2486034
    iget-object v1, p0, LX/Hbg;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    iget-object v1, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486035
    iget-object v2, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v3}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2486036
    iget-object v2, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b()V

    .line 2486037
    const/4 v2, 0x0

    .line 2486038
    :goto_0
    move v1, v2

    .line 2486039
    if-eqz v1, :cond_0

    .line 2486040
    iget-object v1, p0, LX/Hbg;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    iget-object v2, p0, LX/Hbg;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    iget-object v2, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486041
    iget-object v3, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v3}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2486042
    iget-object v3, p0, LX/Hbg;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    iget-object v3, v3, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486043
    iget-object v4, v3, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v4}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2486044
    iget-object v4, p0, LX/Hbg;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    iget-object v4, v4, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486045
    iget-object p0, v4, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object p0

    move-object v4, p0

    .line 2486046
    invoke-static {v1}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->e(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V

    .line 2486047
    new-instance v6, LX/4I6;

    invoke-direct {v6}, LX/4I6;-><init>()V

    .line 2486048
    const-string p0, "old_password"

    invoke-virtual {v6, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2486049
    move-object v6, v6

    .line 2486050
    const-string p0, "new_password"

    invoke-virtual {v6, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2486051
    move-object v6, v6

    .line 2486052
    const-string p0, "confirm_password"

    invoke-virtual {v6, p0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2486053
    move-object v6, v6

    .line 2486054
    iget-object p0, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->g:LX/HaZ;

    .line 2486055
    iget-object p1, p0, LX/HaZ;->b:Ljava/lang/String;

    move-object p0, p1

    .line 2486056
    const-string p1, "security_checkup_source"

    invoke-virtual {v6, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2486057
    move-object v6, v6

    .line 2486058
    new-instance p0, LX/Han;

    invoke-direct {p0}, LX/Han;-><init>()V

    move-object p0, p0

    .line 2486059
    const-string p1, "input"

    invoke-virtual {p0, p1, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2486060
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    .line 2486061
    iget-object v6, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->e:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-virtual {v6, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2486062
    new-instance p0, LX/Hbh;

    invoke-direct {p0, v1}, LX/Hbh;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V

    iget-object p1, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2486063
    :cond_0
    const v1, -0x4fa6a326

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
