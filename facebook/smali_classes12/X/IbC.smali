.class public final LX/IbC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 2592269
    const/4 v7, 0x0

    .line 2592270
    const/4 v6, 0x0

    .line 2592271
    const-wide/16 v4, 0x0

    .line 2592272
    const-wide/16 v2, 0x0

    .line 2592273
    const/4 v1, 0x0

    .line 2592274
    const/4 v0, 0x0

    .line 2592275
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 2592276
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2592277
    const/4 v0, 0x0

    .line 2592278
    :goto_0
    return v0

    .line 2592279
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_5

    .line 2592280
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 2592281
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2592282
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 2592283
    const-string v4, "formatted_minimum_fare"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2592284
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 2592285
    :cond_1
    const-string v4, "surge_confirmation_id"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2592286
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 2592287
    :cond_2
    const-string v4, "surge_multiplier"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2592288
    const/4 v0, 0x1

    .line 2592289
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 2592290
    :cond_3
    const-string v4, "surge_user_input_threshold"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2592291
    const/4 v0, 0x1

    .line 2592292
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 2592293
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2592294
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2592295
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2592296
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2592297
    if-eqz v1, :cond_6

    .line 2592298
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2592299
    :cond_6
    if-eqz v6, :cond_7

    .line 2592300
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2592301
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_8
    move-wide v8, v2

    move v10, v7

    move-wide v2, v4

    move v7, v6

    move v6, v0

    goto/16 :goto_1
.end method
