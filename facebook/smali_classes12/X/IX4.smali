.class public LX/IX4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/IX2;",
        "LX/IX3;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2585454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585455
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2585456
    check-cast p1, LX/IX2;

    .line 2585457
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2585458
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "advertising_id"

    iget-object v3, p1, LX/IX2;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2585459
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "notif_cnt"

    iget v3, p1, LX/IX2;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2585460
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "last_interval"

    iget v3, p1, LX/IX2;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2585461
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "install_notifier"

    .line 2585462
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2585463
    move-object v1, v1

    .line 2585464
    const-string v2, "POST"

    .line 2585465
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2585466
    move-object v1, v1

    .line 2585467
    const-string v2, "install_notifier"

    .line 2585468
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2585469
    move-object v1, v1

    .line 2585470
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 2585471
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2585472
    move-object v0, v1

    .line 2585473
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2585474
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2585475
    move-object v0, v0

    .line 2585476
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2585477
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2585478
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v5

    .line 2585479
    new-instance v0, LX/IX3;

    const-string v1, "should_notify"

    invoke-virtual {v5, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->F()Z

    move-result v1

    const-string v2, "should_schedule_next"

    invoke-virtual {v5, v2}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->F()Z

    move-result v2

    const-string v3, "next_interval"

    invoke-virtual {v5, v3}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-virtual {v3}, LX/0lF;->C()I

    move-result v3

    const-string v4, "title"

    invoke-virtual {v5, v4}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v4

    const-string v6, "text"

    invoke-virtual {v5, v6}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/IX3;-><init>(ZZILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
