.class public final LX/HFW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "LX/HF0;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443678
    iput-object p1, p0, LX/HFW;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2443679
    iget-object v0, p0, LX/HFW;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2443680
    iget-object v0, p0, LX/HFW;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->a:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    const-string v2, "city search failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2443681
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2443682
    check-cast p1, LX/0Px;

    .line 2443683
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2443684
    iget-object v0, p0, LX/HFW;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->f:LX/HF2;

    .line 2443685
    if-eqz p1, :cond_1

    :goto_0
    iput-object p1, v0, LX/HF2;->a:LX/0Px;

    .line 2443686
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2443687
    iget-object v0, p0, LX/HFW;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2443688
    iget-object v0, p0, LX/HFW;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->x:Landroid/widget/ScrollView;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$8$1;

    invoke-direct {v1, p0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$8$1;-><init>(LX/HFW;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 2443689
    :cond_0
    return-void

    .line 2443690
    :cond_1
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 2443691
    goto :goto_0
.end method
