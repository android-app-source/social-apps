.class public LX/IC9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/ICC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ICC",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/ICG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ICG",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/ICC;LX/ICG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2548946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2548947
    iput-object p1, p0, LX/IC9;->a:LX/ICC;

    .line 2548948
    iput-object p2, p0, LX/IC9;->b:LX/ICG;

    .line 2548949
    return-void
.end method

.method public static a(LX/0QB;)LX/IC9;
    .locals 5

    .prologue
    .line 2548950
    const-class v1, LX/IC9;

    monitor-enter v1

    .line 2548951
    :try_start_0
    sget-object v0, LX/IC9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2548952
    sput-object v2, LX/IC9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2548953
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2548954
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2548955
    new-instance p0, LX/IC9;

    invoke-static {v0}, LX/ICC;->a(LX/0QB;)LX/ICC;

    move-result-object v3

    check-cast v3, LX/ICC;

    invoke-static {v0}, LX/ICG;->a(LX/0QB;)LX/ICG;

    move-result-object v4

    check-cast v4, LX/ICG;

    invoke-direct {p0, v3, v4}, LX/IC9;-><init>(LX/ICC;LX/ICG;)V

    .line 2548956
    move-object v0, p0

    .line 2548957
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2548958
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IC9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2548959
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2548960
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
