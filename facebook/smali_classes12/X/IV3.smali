.class public final LX/IV3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;)V
    .locals 0

    .prologue
    .line 2581258
    iput-object p1, p0, LX/IV3;->a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x2471cc48

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2581259
    iget-object v0, p0, LX/IV3;->a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    if-eqz v0, :cond_0

    .line 2581260
    iget-object v0, p0, LX/IV3;->a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/IV3;->a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->C:Ljava/lang/String;

    iget-object v4, p0, LX/IV3;->a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    iget-object v4, v4, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2581261
    :cond_0
    const v0, -0x7e269b33

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
