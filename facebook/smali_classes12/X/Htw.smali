.class public LX/Htw;
.super LX/Hs6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0jD;",
        ":",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
        "DerivedData::",
        "LX/5Qz;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/Hs6;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final b:LX/Hr2;

.field private final c:Landroid/content/res/Resources;

.field private final d:Z

.field private final e:LX/Hu0;


# direct methods
.method public constructor <init>(LX/0il;LX/Hr2;Landroid/content/res/Resources;Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Hr2;",
            "Landroid/content/res/Resources;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516976
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2516977
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Htw;->a:Ljava/lang/ref/WeakReference;

    .line 2516978
    iput-object p2, p0, LX/Htw;->b:LX/Hr2;

    .line 2516979
    iput-object p3, p0, LX/Htw;->c:Landroid/content/res/Resources;

    .line 2516980
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Htw;->d:Z

    .line 2516981
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v0

    const v1, 0x7f0208a2

    .line 2516982
    iput v1, v0, LX/Htz;->a:I

    .line 2516983
    move-object v0, v0

    .line 2516984
    const v1, 0x7f0a04c3

    .line 2516985
    iput v1, v0, LX/Htz;->f:I

    .line 2516986
    move-object v1, v0

    .line 2516987
    iget-object v2, p0, LX/Htw;->c:Landroid/content/res/Resources;

    iget-boolean v0, p0, LX/Htw;->d:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0812b1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2516988
    iput-object v0, v1, LX/Htz;->b:Ljava/lang/String;

    .line 2516989
    move-object v0, v1

    .line 2516990
    invoke-virtual {p0}, LX/Htw;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2516991
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2516992
    move-object v0, v0

    .line 2516993
    iget-object v1, p0, LX/Htw;->b:LX/Hr2;

    .line 2516994
    iput-object v1, v0, LX/Htz;->e:LX/Hr2;

    .line 2516995
    move-object v0, v0

    .line 2516996
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/Htw;->e:LX/Hu0;

    .line 2516997
    return-void

    .line 2516998
    :cond_0
    const v0, 0x7f0812b0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2516970
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2516999
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2516974
    iget-object v1, p0, LX/Htw;->c:Landroid/content/res/Resources;

    iget-boolean v0, p0, LX/Htw;->d:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0812b4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0812b3

    goto :goto_0
.end method

.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2516975
    iget-object v0, p0, LX/Htw;->e:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2516973
    iget-object v0, p0, LX/Htw;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qz;

    invoke-interface {v0}, LX/5Qz;->C()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2516972
    iget-object v0, p0, LX/Htw;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jD;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2516971
    sget-object v0, LX/Hty;->TAG_PEOPLE:LX/Hty;

    return-object v0
.end method
