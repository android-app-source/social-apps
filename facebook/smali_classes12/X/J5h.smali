.class public final enum LX/J5h;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J5h;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J5h;

.field public static final enum PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_CANCEL:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_APP_ONLY:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_POSTS:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_EXPOSED:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_APP_STEP_NEXT:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_APP_STEP_PREVIOUS:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_COMPOSER_STEP_NEXT:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_COMPOSER_STEP_PREVIOUS:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_INTRO_STEP_CLOSE:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_INTRO_STEP_CONTINUE:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_INTRO_STEP_NEXT:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_PROFILE_STEP_NEXT:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_PROFILE_STEP_PREVIOUS:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_REVIEW_STEP_CLOSE:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_REVIEW_STEP_PREVIOUS:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_DROPPED:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_FAILURE:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH_FAILURE:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH_SUCCESS:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_ON_EXIT:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_QUEUED:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_SENT:LX/J5h;

.field public static final enum PRIVACY_CHECKUP_WRITE_REQUEST_SUCCESS:LX/J5h;

.field public static final enum PRIVACY_REVIEW_CANCEL:LX/J5h;

.field public static final enum PRIVACY_REVIEW_CORE_EVENT:LX/J5h;

.field public static final enum PRIVACY_REVIEW_WRITE_EXIT_ON_FAILURE:LX/J5h;

.field public static final enum PRIVACY_REVIEW_WRITE_FAILURE:LX/J5h;

.field public static final enum PRIVACY_REVIEW_WRITE_RETRY:LX/J5h;

.field public static final enum PRIVACY_REVIEW_WRITE_SENT:LX/J5h;

.field public static final enum PRIVACY_REVIEW_WRITE_SUCCESS:LX/J5h;

.field public static final enum PRIVACY_REVIEW_WRITE_TIMEOUT:LX/J5h;


# instance fields
.field public final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2648565
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_INTRO_STEP_NEXT"

    const-string v2, "privacy_checkup_intro_step_next"

    invoke-direct {v0, v1, v4, v2}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_INTRO_STEP_NEXT:LX/J5h;

    .line 2648566
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_INTRO_STEP_CONTINUE"

    const-string v2, "privacy_checkup_intro_step_continue"

    invoke-direct {v0, v1, v5, v2}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_INTRO_STEP_CONTINUE:LX/J5h;

    .line 2648567
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_INTRO_STEP_CLOSE"

    const-string v2, "privacy_checkup_intro_step_close"

    invoke-direct {v0, v1, v6, v2}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_INTRO_STEP_CLOSE:LX/J5h;

    .line 2648568
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_COMPOSER_STEP_NEXT"

    const-string v2, "privacy_checkup_composer_step_next"

    invoke-direct {v0, v1, v7, v2}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_COMPOSER_STEP_NEXT:LX/J5h;

    .line 2648569
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_COMPOSER_STEP_PREVIOUS"

    const-string v2, "privacy_checkup_composer_step_previous"

    invoke-direct {v0, v1, v8, v2}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_COMPOSER_STEP_PREVIOUS:LX/J5h;

    .line 2648570
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_PROFILE_STEP_NEXT"

    const/4 v2, 0x5

    const-string v3, "privacy_checkup_profile_step_next"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_PROFILE_STEP_NEXT:LX/J5h;

    .line 2648571
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_PROFILE_STEP_PREVIOUS"

    const/4 v2, 0x6

    const-string v3, "privacy_checkup_profile_step_previous"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_PROFILE_STEP_PREVIOUS:LX/J5h;

    .line 2648572
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_APP_STEP_NEXT"

    const/4 v2, 0x7

    const-string v3, "privacy_checkup_app_step_next"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_NEXT:LX/J5h;

    .line 2648573
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_APP_STEP_PREVIOUS"

    const/16 v2, 0x8

    const-string v3, "privacy_checkup_app_step_previous"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_PREVIOUS:LX/J5h;

    .line 2648574
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_EXPOSED"

    const/16 v2, 0x9

    const-string v3, "privacy_checkup_app_delete_dialog_exposed"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_EXPOSED:LX/J5h;

    .line 2648575
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_CANCEL"

    const/16 v2, 0xa

    const-string v3, "privacy_checkup_app_delete_dialog_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_CANCEL:LX/J5h;

    .line 2648576
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_POSTS"

    const/16 v2, 0xb

    const-string v3, "privacy_checkup_app_delete_dialog_delete_posts"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_POSTS:LX/J5h;

    .line 2648577
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_APP_ONLY"

    const/16 v2, 0xc

    const-string v3, "privacy_checkup_app_delete_dialog_delete_app_only"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_APP_ONLY:LX/J5h;

    .line 2648578
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_REVIEW_STEP_PREVIOUS"

    const/16 v2, 0xd

    const-string v3, "privacy_checkup_review_step_previous"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_REVIEW_STEP_PREVIOUS:LX/J5h;

    .line 2648579
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_REVIEW_STEP_CLOSE"

    const/16 v2, 0xe

    const-string v3, "privacy_checkup_review_step_close"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_REVIEW_STEP_CLOSE:LX/J5h;

    .line 2648580
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_QUEUED"

    const/16 v2, 0xf

    const-string v3, "privacy_checkup_write_request_queued"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_QUEUED:LX/J5h;

    .line 2648581
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_SENT"

    const/16 v2, 0x10

    const-string v3, "privacy_checkup_write_request_sent"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_SENT:LX/J5h;

    .line 2648582
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_SUCCESS"

    const/16 v2, 0x11

    const-string v3, "privacy_checkup_write_request_success"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_SUCCESS:LX/J5h;

    .line 2648583
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_FAILURE"

    const/16 v2, 0x12

    const-string v3, "privacy_checkup_write_request_failure"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FAILURE:LX/J5h;

    .line 2648584
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_DROPPED"

    const/16 v2, 0x13

    const-string v3, "privacy_checkup_write_request_dropped"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_DROPPED:LX/J5h;

    .line 2648585
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_ON_EXIT"

    const/16 v2, 0x14

    const-string v3, "privacy_checkup_write_request_on_exit"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_ON_EXIT:LX/J5h;

    .line 2648586
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH"

    const/16 v2, 0x15

    const-string v3, "privacy_checkup_write_request_flush"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH:LX/J5h;

    .line 2648587
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH_SUCCESS"

    const/16 v2, 0x16

    const-string v3, "privacy_checkup_write_request_flush_success"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH_SUCCESS:LX/J5h;

    .line 2648588
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH_FAILURE"

    const/16 v2, 0x17

    const-string v3, "privacy_checkup_write_request_flush_failure"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH_FAILURE:LX/J5h;

    .line 2648589
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_REVIEW_WRITE_SENT"

    const/16 v2, 0x18

    const-string v3, "privacy_review_write_sent"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_SENT:LX/J5h;

    .line 2648590
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_REVIEW_WRITE_SUCCESS"

    const/16 v2, 0x19

    const-string v3, "privacy_review_write_success"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_SUCCESS:LX/J5h;

    .line 2648591
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_REVIEW_WRITE_FAILURE"

    const/16 v2, 0x1a

    const-string v3, "privacy_review_write_failure"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_FAILURE:LX/J5h;

    .line 2648592
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_REVIEW_WRITE_TIMEOUT"

    const/16 v2, 0x1b

    const-string v3, "privacy_review_write_timeout"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_TIMEOUT:LX/J5h;

    .line 2648593
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_REVIEW_WRITE_RETRY"

    const/16 v2, 0x1c

    const-string v3, "privacy_review_write_retry"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_RETRY:LX/J5h;

    .line 2648594
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_REVIEW_WRITE_EXIT_ON_FAILURE"

    const/16 v2, 0x1d

    const-string v3, "privacy_review_write_exit_on_failure"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_EXIT_ON_FAILURE:LX/J5h;

    .line 2648595
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_REVIEW_CANCEL"

    const/16 v2, 0x1e

    const-string v3, "privacy_review_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_REVIEW_CANCEL:LX/J5h;

    .line 2648596
    new-instance v0, LX/J5h;

    const-string v1, "PRIVACY_REVIEW_CORE_EVENT"

    const/16 v2, 0x1f

    const-string v3, "privacy_review_core_event"

    invoke-direct {v0, v1, v2, v3}, LX/J5h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5h;->PRIVACY_REVIEW_CORE_EVENT:LX/J5h;

    .line 2648597
    const/16 v0, 0x20

    new-array v0, v0, [LX/J5h;

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_INTRO_STEP_NEXT:LX/J5h;

    aput-object v1, v0, v4

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_INTRO_STEP_CONTINUE:LX/J5h;

    aput-object v1, v0, v5

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_INTRO_STEP_CLOSE:LX/J5h;

    aput-object v1, v0, v6

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_COMPOSER_STEP_NEXT:LX/J5h;

    aput-object v1, v0, v7

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_COMPOSER_STEP_PREVIOUS:LX/J5h;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_PROFILE_STEP_NEXT:LX/J5h;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_PROFILE_STEP_PREVIOUS:LX/J5h;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_NEXT:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_PREVIOUS:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_EXPOSED:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_CANCEL:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_POSTS:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_APP_ONLY:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_REVIEW_STEP_PREVIOUS:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_REVIEW_STEP_CLOSE:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_QUEUED:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_SENT:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_SUCCESS:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FAILURE:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_DROPPED:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_ON_EXIT:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH_SUCCESS:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH_FAILURE:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/J5h;->PRIVACY_REVIEW_WRITE_SENT:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/J5h;->PRIVACY_REVIEW_WRITE_SUCCESS:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/J5h;->PRIVACY_REVIEW_WRITE_FAILURE:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/J5h;->PRIVACY_REVIEW_WRITE_TIMEOUT:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/J5h;->PRIVACY_REVIEW_WRITE_RETRY:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/J5h;->PRIVACY_REVIEW_WRITE_EXIT_ON_FAILURE:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/J5h;->PRIVACY_REVIEW_CANCEL:LX/J5h;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/J5h;->PRIVACY_REVIEW_CORE_EVENT:LX/J5h;

    aput-object v2, v0, v1

    sput-object v0, LX/J5h;->$VALUES:[LX/J5h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2648560
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2648561
    iput-object p3, p0, LX/J5h;->eventName:Ljava/lang/String;

    .line 2648562
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J5h;
    .locals 1

    .prologue
    .line 2648564
    const-class v0, LX/J5h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J5h;

    return-object v0
.end method

.method public static values()[LX/J5h;
    .locals 1

    .prologue
    .line 2648563
    sget-object v0, LX/J5h;->$VALUES:[LX/J5h;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J5h;

    return-object v0
.end method
