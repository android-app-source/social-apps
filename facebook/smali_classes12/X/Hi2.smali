.class public final LX/Hi2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 2496737
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2496738
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2496739
    if-eqz v0, :cond_0

    .line 2496740
    const-string v0, "current_region"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496741
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496742
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2496743
    if-eqz v0, :cond_5

    .line 2496744
    const-string v1, "current_region_tos_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496745
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2496746
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2496747
    if-eqz v1, :cond_1

    .line 2496748
    const-string p3, "cookies_url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496749
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496750
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2496751
    if-eqz v1, :cond_2

    .line 2496752
    const-string p3, "privacy_url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496753
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496754
    :cond_2
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2496755
    if-eqz v1, :cond_3

    .line 2496756
    const-string p3, "terms_url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496757
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496758
    :cond_3
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2496759
    if-eqz v1, :cond_4

    .line 2496760
    const-string p3, "tos_version"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496761
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496762
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2496763
    :cond_5
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2496764
    if-eqz v0, :cond_6

    .line 2496765
    const-string v0, "effective_region"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496766
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496767
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2496768
    if-eqz v0, :cond_7

    .line 2496769
    const-string v0, "tos_transition_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496770
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496771
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2496772
    return-void
.end method

.method public static a$redex0(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2496677
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2496678
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2496679
    :goto_0
    return v1

    .line 2496680
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2496681
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2496682
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2496683
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2496684
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2496685
    const-string v6, "current_region"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2496686
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2496687
    :cond_2
    const-string v6, "current_region_tos_links"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2496688
    const/4 v5, 0x0

    .line 2496689
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_d

    .line 2496690
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2496691
    :goto_2
    move v3, v5

    .line 2496692
    goto :goto_1

    .line 2496693
    :cond_3
    const-string v6, "effective_region"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2496694
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_1

    .line 2496695
    :cond_4
    const-string v6, "tos_transition_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2496696
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 2496697
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2496698
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2496699
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2496700
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2496701
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2496702
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 2496703
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2496704
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 2496705
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2496706
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2496707
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_8

    if-eqz v9, :cond_8

    .line 2496708
    const-string v10, "cookies_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2496709
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_3

    .line 2496710
    :cond_9
    const-string v10, "privacy_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2496711
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_3

    .line 2496712
    :cond_a
    const-string v10, "terms_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 2496713
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 2496714
    :cond_b
    const-string v10, "tos_version"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2496715
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    .line 2496716
    :cond_c
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2496717
    invoke-virtual {p1, v5, v8}, LX/186;->b(II)V

    .line 2496718
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v7}, LX/186;->b(II)V

    .line 2496719
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 2496720
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v3}, LX/186;->b(II)V

    .line 2496721
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_d
    move v3, v5

    move v6, v5

    move v7, v5

    move v8, v5

    goto :goto_3
.end method

.method public static a$redex0(LX/15w;)LX/15i;
    .locals 15

    .prologue
    .line 2496650
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2496651
    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 2496652
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_7

    .line 2496653
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2496654
    :goto_0
    move v1, v3

    .line 2496655
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2496656
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 2496657
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, v13, :cond_5

    .line 2496658
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 2496659
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2496660
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v12, :cond_0

    .line 2496661
    const-string v13, "expiration_time"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2496662
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    move v2, v8

    goto :goto_1

    .line 2496663
    :cond_1
    const-string v13, "granted_permissions"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 2496664
    invoke-static {p0, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 2496665
    :cond_2
    const-string v13, "signed_request"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 2496666
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2496667
    :cond_3
    const-string v13, "token"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2496668
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2496669
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2496670
    :cond_5
    const/4 v12, 0x4

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 2496671
    if-eqz v2, :cond_6

    move-object v2, v0

    .line 2496672
    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2496673
    :cond_6
    invoke-virtual {v0, v8, v11}, LX/186;->b(II)V

    .line 2496674
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2496675
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2496676
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_0

    :cond_7
    move v2, v3

    move v9, v3

    move v10, v3

    move v11, v3

    move-wide v4, v6

    goto :goto_1
.end method

.method public static a$redex0(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 2496722
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2496723
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2496724
    if-eqz v0, :cond_0

    .line 2496725
    const-string v1, "end_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496726
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496727
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2496728
    if-eqz v0, :cond_1

    .line 2496729
    const-string v1, "has_next_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496730
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2496731
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2496732
    if-eqz v0, :cond_2

    .line 2496733
    const-string v1, "start_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496734
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496735
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2496736
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2496629
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2496630
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2496631
    :goto_0
    return v1

    .line 2496632
    :cond_0
    const-string v8, "length"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2496633
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 2496634
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 2496635
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2496636
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2496637
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2496638
    const-string v8, "entity"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2496639
    invoke-static {p0, p1}, LX/GSJ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2496640
    :cond_2
    const-string v8, "offset"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2496641
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2496642
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2496643
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2496644
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2496645
    if-eqz v3, :cond_5

    .line 2496646
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 2496647
    :cond_5
    if-eqz v0, :cond_6

    .line 2496648
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 2496649
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2496614
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2496615
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2496616
    if-eqz v0, :cond_0

    .line 2496617
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496618
    invoke-static {p0, v0, p2}, LX/GSJ;->a(LX/15i;ILX/0nX;)V

    .line 2496619
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2496620
    if-eqz v0, :cond_1

    .line 2496621
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496622
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2496623
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2496624
    if-eqz v0, :cond_2

    .line 2496625
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496626
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2496627
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2496628
    return-void
.end method
