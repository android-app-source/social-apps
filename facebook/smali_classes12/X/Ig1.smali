.class public LX/Ig1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ifu;


# static fields
.field private static final a:J


# instance fields
.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final f:LX/0lC;

.field public g:LX/Ifv;

.field private h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2600414
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/Ig1;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2600415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600416
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Ig1;->h:J

    .line 2600417
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    iput-object v0, p0, LX/Ig1;->f:LX/0lC;

    .line 2600418
    return-void
.end method

.method public static b(LX/Ig1;LX/Ig7;)V
    .locals 8

    .prologue
    .line 2600419
    iget-object v0, p0, LX/Ig1;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2600420
    iget-wide v2, p0, LX/Ig1;->h:J

    sub-long v2, v0, v2

    sget-wide v4, LX/Ig1;->a:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 2600421
    :goto_0
    return-void

    .line 2600422
    :cond_0
    iput-wide v0, p0, LX/Ig1;->h:J

    .line 2600423
    iget-object v0, p0, LX/Ig1;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v0

    .line 2600424
    sget-object v1, LX/Ig2;->a:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600425
    iget-object v1, p1, LX/Ig7;->c:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2600426
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600427
    iget-object v1, p0, LX/Ig1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2600428
    invoke-static {p0, p1}, LX/Ig1;->c(LX/Ig1;LX/Ig7;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2600429
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2600430
    const-string v3, "userKey"

    .line 2600431
    iget-object v4, p1, LX/Ig7;->c:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2600432
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2600433
    iget-object v3, p1, LX/Ig7;->d:Landroid/location/Location;

    move-object v3, v3

    .line 2600434
    if-eqz v3, :cond_1

    .line 2600435
    const-string v4, "lng"

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2600436
    const-string v4, "lat"

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2600437
    :cond_1
    const-string v3, "location_title"

    .line 2600438
    iget-object v4, p1, LX/Ig7;->e:Ljava/lang/String;

    move-object v4, v4

    .line 2600439
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2600440
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2600441
    :goto_1
    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_0

    .line 2600442
    :cond_2
    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_1
.end method

.method private static c(LX/Ig1;LX/Ig7;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2600443
    if-nez p1, :cond_0

    move v0, v1

    .line 2600444
    :goto_0
    return v0

    .line 2600445
    :cond_0
    iget-object v0, p0, LX/Ig1;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2600446
    iget-object v3, p1, LX/Ig7;->c:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 2600447
    invoke-virtual {v0, v3}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 2600448
    goto :goto_0

    .line 2600449
    :cond_1
    iget-object v0, p0, LX/Ig1;->g:LX/Ifv;

    .line 2600450
    iget-object v3, p1, LX/Ig7;->c:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 2600451
    iget-object p0, v0, LX/Ifv;->h:LX/0kD;

    invoke-interface {p0, v3}, LX/0kD;->c(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    move-object v0, p0

    .line 2600452
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Ifv;)V
    .locals 13

    .prologue
    .line 2600453
    iput-object p1, p0, LX/Ig1;->g:LX/Ifv;

    .line 2600454
    iget-object v0, p0, LX/Ig1;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v0

    .line 2600455
    iget-object v1, p0, LX/Ig1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Ig2;->a:LX/0Tn;

    invoke-virtual {v2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 2600456
    iget-object v1, p0, LX/Ig1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2600457
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600458
    const/4 v5, 0x0

    .line 2600459
    iget-object v4, p0, LX/Ig1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4, v0, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2600460
    :try_start_0
    iget-object v6, p0, LX/Ig1;->f:LX/0lC;

    invoke-virtual {v6, v4}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    check-cast v4, LX/0m9;

    .line 2600461
    const-string v6, "userKey"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-virtual {v6}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v6

    .line 2600462
    iget-object v7, p0, LX/Ig1;->g:LX/Ifv;

    invoke-virtual {v7, v6}, LX/Ifv;->a(Lcom/facebook/user/model/UserKey;)LX/Ig7;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2600463
    :try_start_1
    const-string v6, "lat"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 2600464
    const-string v7, "lng"

    invoke-virtual {v4, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 2600465
    if-eqz v6, :cond_1

    .line 2600466
    new-instance v8, Landroid/location/Location;

    const-string v9, ""

    invoke-direct {v8, v9}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2600467
    invoke-virtual {v6}, LX/0lF;->E()D

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Landroid/location/Location;->setLatitude(D)V

    .line 2600468
    invoke-virtual {v7}, LX/0lF;->E()D

    move-result-wide v6

    invoke-virtual {v8, v6, v7}, Landroid/location/Location;->setLongitude(D)V

    .line 2600469
    invoke-virtual {v5, v8}, LX/Ig7;->a(Landroid/location/Location;)V

    .line 2600470
    :cond_1
    const-string v6, "location_title"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/Ig7;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v4, v5

    .line 2600471
    :goto_1
    move-object v3, v4

    .line 2600472
    invoke-static {p0, v3}, LX/Ig1;->c(LX/Ig1;LX/Ig7;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2600473
    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_0

    .line 2600474
    :cond_2
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2600475
    return-void

    .line 2600476
    :catch_0
    move-exception v4

    move-object v12, v4

    move-object v4, v5

    move-object v5, v12

    .line 2600477
    :goto_2
    iget-object v6, p0, LX/Ig1;->d:LX/03V;

    const-string v7, "Error reading object from persistent storage."

    invoke-virtual {v6, v7, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2600478
    :catch_1
    move-exception v4

    move-object v12, v4

    move-object v4, v5

    move-object v5, v12

    goto :goto_2
.end method

.method public final a(LX/Ig7;)V
    .locals 0

    .prologue
    .line 2600479
    invoke-static {p0, p1}, LX/Ig1;->b(LX/Ig1;LX/Ig7;)V

    .line 2600480
    return-void
.end method
