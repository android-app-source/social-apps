.class public LX/J94;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/J94;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652486
    return-void
.end method

.method public static a(LX/0QB;)LX/J94;
    .locals 3

    .prologue
    .line 2652473
    sget-object v0, LX/J94;->a:LX/J94;

    if-nez v0, :cond_1

    .line 2652474
    const-class v1, LX/J94;

    monitor-enter v1

    .line 2652475
    :try_start_0
    sget-object v0, LX/J94;->a:LX/J94;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2652476
    if-eqz v2, :cond_0

    .line 2652477
    :try_start_1
    new-instance v0, LX/J94;

    invoke-direct {v0}, LX/J94;-><init>()V

    .line 2652478
    move-object v0, v0

    .line 2652479
    sput-object v0, LX/J94;->a:LX/J94;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2652480
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2652481
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2652482
    :cond_1
    sget-object v0, LX/J94;->a:LX/J94;

    return-object v0

    .line 2652483
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2652484
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(ILandroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2652469
    const/4 v0, 0x0

    invoke-virtual {p2, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2652470
    const-string v1, "collectionsViewFramer_inner_view"

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2652471
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2652472
    return-object v0
.end method

.method public static a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2652468
    const-string v0, "collectionsViewFramer_inner_view"

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2652467
    const v0, 0x7f030240

    invoke-static {v0, p0, p1}, LX/J94;->a(ILandroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2652466
    const v0, 0x7f030242

    invoke-static {v0, p0, p1}, LX/J94;->a(ILandroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2652465
    const v0, 0x7f030241

    invoke-static {v0, p0, p1}, LX/J94;->a(ILandroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static final d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2652464
    const v0, 0x7f03023f

    invoke-static {v0, p0, p1}, LX/J94;->a(ILandroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
