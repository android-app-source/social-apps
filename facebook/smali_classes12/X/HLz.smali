.class public LX/HLz;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field private b:Lcom/facebook/maps/FbStaticMapView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2456544
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2456545
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "pages_multi_locations_map"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/HLz;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 2456546
    const v0, 0x7f030e95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2456547
    const v0, 0x7f0d23b5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, LX/HLz;->b:Lcom/facebook/maps/FbStaticMapView;

    .line 2456548
    return-void
.end method


# virtual methods
.method public final a(ZLjava/util/ArrayList;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;",
            "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLInterfaces$ReactionGeoRectangleFields;",
            "I",
            "Lcom/facebook/graphql/enums/GraphQLPlaceType;",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2456549
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2456550
    sget-object v0, LX/HLy;->a:[I

    invoke-virtual {p6}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 2456551
    :goto_1
    iget-object v3, p0, LX/HLz;->b:Lcom/facebook/maps/FbStaticMapView;

    sget-object v4, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    invoke-virtual {p0}, LX/HLz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v5, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v3, v4, v0, v2}, Lcom/facebook/maps/FbStaticMapView;->a(LX/0yY;LX/0gc;LX/6Zs;)V

    .line 2456552
    iget-object v0, p0, LX/HLz;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a()Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v3

    .line 2456553
    if-eqz p1, :cond_3

    .line 2456554
    iget-object v4, p0, LX/HLz;->b:Lcom/facebook/maps/FbStaticMapView;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->PLACE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne p5, v0, :cond_2

    iget-object v0, p0, LX/HLz;->b:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0}, Lcom/facebook/maps/FbStaticMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020e90

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_2
    const/high16 v2, 0x3f000000    # 0.5f

    const v5, 0x3f6e147b    # 0.93f

    invoke-virtual {v4, v0, v2, v5}, LX/3BP;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 2456555
    invoke-virtual {v3, p4}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v2

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 2456556
    if-eqz p3, :cond_0

    .line 2456557
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->d()D

    move-result-wide v4

    double-to-float v1, v4

    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->b()D

    move-result-wide v4

    double-to-float v2, v4

    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->a()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->c()D

    move-result-wide v6

    double-to-float v5, v6

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v3, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/graphics/RectF;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 2456558
    :cond_0
    :goto_3
    iget-object v0, p0, LX/HLz;->b:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, v3}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2456559
    return-void

    :cond_1
    move v0, v1

    .line 2456560
    goto/16 :goto_0

    .line 2456561
    :pswitch_0
    iget-object v0, p0, LX/HLz;->b:Lcom/facebook/maps/FbStaticMapView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    invoke-virtual {p0}, LX/HLz;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0d66

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Lcom/facebook/maps/FbStaticMapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_2
    move-object v0, v2

    .line 2456562
    goto :goto_2

    .line 2456563
    :cond_3
    const-string v0, "red"

    invoke-virtual {v3, p2, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
