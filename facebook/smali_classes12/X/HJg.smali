.class public LX/HJg;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HJg",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453141
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2453142
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HJg;->b:LX/0Zi;

    .line 2453143
    iput-object p1, p0, LX/HJg;->a:LX/0Ot;

    .line 2453144
    return-void
.end method

.method public static a(LX/0QB;)LX/HJg;
    .locals 4

    .prologue
    .line 2453145
    const-class v1, LX/HJg;

    monitor-enter v1

    .line 2453146
    :try_start_0
    sget-object v0, LX/HJg;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453147
    sput-object v2, LX/HJg;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453148
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453149
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453150
    new-instance v3, LX/HJg;

    const/16 p0, 0x2bb5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJg;-><init>(LX/0Ot;)V

    .line 2453151
    move-object v0, v3

    .line 2453152
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453153
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453154
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453155
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2453156
    check-cast p1, LX/HJf;

    .line 2453157
    iget-object v0, p0, LX/HJg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentSpec;

    iget-object v1, p1, LX/HJf;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p1, LX/HJf;->b:LX/2km;

    .line 2453158
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2453159
    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2453160
    :goto_0
    return-void

    .line 2453161
    :cond_0
    iget-object v3, v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentSpec;->c:LX/E1f;

    .line 2453162
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2453163
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    move-object v5, v2

    check-cast v5, LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    move-object v7, v2

    check-cast v7, LX/2kp;

    invoke-interface {v7}, LX/2kp;->t()LX/2jY;

    move-result-object v7

    .line 2453164
    iget-object v8, v7, LX/2jY;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2453165
    move-object v8, v2

    check-cast v8, LX/2kp;

    invoke-interface {v8}, LX/2kp;->t()LX/2jY;

    move-result-object v8

    .line 2453166
    iget-object v9, v8, LX/2jY;->b:Ljava/lang/String;

    move-object v8, v9

    .line 2453167
    iget-object v9, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 2453168
    invoke-virtual/range {v3 .. v9}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    .line 2453169
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2453170
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2453171
    invoke-interface {v2, v4, v5, v3}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2453172
    const v0, -0x4514a15c

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2453173
    check-cast p2, LX/HJf;

    .line 2453174
    iget-object v0, p0, LX/HJg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentSpec;

    iget-object v1, p2, LX/HJf;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 p2, 0x6

    const/4 p0, 0x2

    const/4 v7, 0x1

    .line 2453175
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2453176
    invoke-interface {v2}, LX/9uc;->aW()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    .line 2453177
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2453178
    invoke-interface {v3}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2453179
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0213ab

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    .line 2453180
    const v5, -0x4514a15c

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2453181
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0083

    invoke-interface {v4, p2, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x7

    const v6, 0x7f0b0083

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    .line 2453182
    iget-object v6, v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentSpec;->b:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    sget-object v1, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v6

    const v1, 0x7f0a010a

    invoke-virtual {v6, v1}, LX/1nw;->h(I)LX/1nw;

    move-result-object v6

    sget-object v1, LX/1Up;->f:LX/1Up;

    invoke-virtual {v6, v1}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v1, 0x7f0b0d65

    invoke-interface {v6, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const v1, 0x7f0b0d65

    invoke-interface {v6, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    move-object v2, v6

    .line 2453183
    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0083

    invoke-interface {v4, p2, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x1010038

    invoke-virtual {v3, v5}, LX/1ne;->o(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b0050

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2453184
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2453185
    invoke-static {}, LX/1dS;->b()V

    .line 2453186
    iget v0, p1, LX/1dQ;->b:I

    .line 2453187
    packed-switch v0, :pswitch_data_0

    .line 2453188
    :goto_0
    return-object v1

    .line 2453189
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HJg;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x4514a15c
        :pswitch_0
    .end packed-switch
.end method
