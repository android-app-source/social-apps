.class public final LX/HY6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2480124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2480123
    invoke-static {}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;->values()[Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2480122
    new-array v0, p1, [Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    return-object v0
.end method
