.class public final LX/HGs;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V
    .locals 0

    .prologue
    .line 2446691
    iput-object p1, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2446692
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    const/4 v1, 0x0

    .line 2446693
    iput-boolean v1, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->B:Z

    .line 2446694
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->p(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    .line 2446695
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchPagesAlbumsList"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2446696
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2446697
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2446698
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    .line 2446699
    iput-boolean v1, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->C:Z

    .line 2446700
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    .line 2446701
    iput-boolean v2, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->B:Z

    .line 2446702
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->p(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    .line 2446703
    if-eqz p1, :cond_0

    .line 2446704
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2446705
    if-nez v0, :cond_1

    .line 2446706
    :cond_0
    :goto_0
    return-void

    .line 2446707
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 2446708
    if-eqz v0, :cond_0

    iget-object v3, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    if-eqz v3, :cond_0

    .line 2446709
    iget-object v3, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->H:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2446710
    :goto_1
    if-eqz v1, :cond_2

    .line 2446711
    iget-object v1, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    invoke-virtual {v1}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->d()V

    .line 2446712
    :cond_2
    iget-object v1, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->a(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;)V

    .line 2446713
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2446714
    iget-object v1, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 2446715
    iput-object v2, v1, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->H:Ljava/lang/String;

    .line 2446716
    iget-object v1, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    .line 2446717
    iput-boolean v0, v1, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->I:Z

    .line 2446718
    :goto_2
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->z:LX/HME;

    if-eqz v0, :cond_0

    .line 2446719
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->z:LX/HME;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    move v1, v2

    .line 2446720
    goto :goto_1

    .line 2446721
    :cond_4
    iget-object v0, p0, LX/HGs;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    .line 2446722
    iput-boolean v2, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->I:Z

    .line 2446723
    goto :goto_2
.end method
