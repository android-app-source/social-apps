.class public LX/Iiz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iif;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605180
    iput-object p1, p0, LX/Iiz;->a:Landroid/content/Context;

    .line 2605181
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;IIIIIIII)LX/Iik;
    .locals 3

    .prologue
    .line 2605182
    new-instance v0, LX/Iil;

    iget-object v1, p0, LX/Iiz;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Iil;-><init>(Landroid/content/Context;)V

    .line 2605183
    new-instance v1, LX/Iin;

    invoke-direct {v1}, LX/Iin;-><init>()V

    move-object v1, v1

    .line 2605184
    iput-object p1, v1, LX/Iin;->a:Ljava/lang/String;

    .line 2605185
    move-object v1, v1

    .line 2605186
    iput-object p2, v1, LX/Iin;->b:Ljava/lang/String;

    .line 2605187
    move-object v1, v1

    .line 2605188
    new-instance v2, LX/Iix;

    invoke-direct {v2, p3, p4}, LX/Iix;-><init>(II)V

    .line 2605189
    iput-object v2, v1, LX/Iin;->c:LX/Iix;

    .line 2605190
    move-object v1, v1

    .line 2605191
    new-instance v2, LX/Iix;

    invoke-direct {v2, p5, p6}, LX/Iix;-><init>(II)V

    .line 2605192
    iput-object v2, v1, LX/Iin;->d:LX/Iix;

    .line 2605193
    move-object v1, v1

    .line 2605194
    new-instance v2, LX/Iix;

    invoke-direct {v2, p7, p8}, LX/Iix;-><init>(II)V

    .line 2605195
    iput-object v2, v1, LX/Iin;->e:LX/Iix;

    .line 2605196
    move-object v1, v1

    .line 2605197
    iput p9, v1, LX/Iin;->f:I

    .line 2605198
    move-object v1, v1

    .line 2605199
    iput p10, v1, LX/Iin;->g:I

    .line 2605200
    move-object v1, v1

    .line 2605201
    new-instance v2, LX/Iim;

    invoke-direct {v2, v1}, LX/Iim;-><init>(LX/Iin;)V

    move-object v1, v2

    .line 2605202
    invoke-virtual {v0, v1}, LX/Iil;->setViewParams(LX/Iim;)V

    .line 2605203
    return-object v0
.end method


# virtual methods
.method public final a(LX/Iiv;)LX/Iik;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 2605204
    sget-object v0, LX/Iiy;->a:[I

    invoke-virtual {p1}, LX/Iiv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2605205
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid PaymentAwarenessMode provided: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2605206
    :pswitch_0
    iget-object v0, p0, LX/Iiz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0839e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0839e8

    const v4, 0x7f0203ea

    const v5, 0x7f0839e9

    const v6, 0x7f0203b1

    const v7, 0x7f0839ea

    const v8, 0x7f020b36

    const v9, 0x7f080036

    const v10, 0x7f030ef8

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/Iiz;->a(Ljava/lang/String;Ljava/lang/String;IIIIIIII)LX/Iik;

    move-result-object v0

    .line 2605207
    :goto_0
    return-object v0

    .line 2605208
    :pswitch_1
    iget-object v0, p0, LX/Iiz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0839eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0839ec

    const v4, 0x7f0203ea

    const v5, 0x7f0839ed

    const v6, 0x7f0203b1

    const v7, 0x7f0839ee

    const v8, 0x7f020b36

    const v9, 0x7f080036

    const v10, 0x7f030ef8

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/Iiz;->a(Ljava/lang/String;Ljava/lang/String;IIIIIIII)LX/Iik;

    move-result-object v0

    goto :goto_0

    .line 2605209
    :pswitch_2
    iget-object v0, p0, LX/Iiz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0839e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0839e2

    const v4, 0x7f020ed0

    const v5, 0x7f0839e3

    const v6, 0x7f020b36

    const v7, 0x7f0839e4

    const v8, 0x7f0203b1

    const v9, 0x7f0839e5

    const v10, 0x7f030ef8

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/Iiz;->a(Ljava/lang/String;Ljava/lang/String;IIIIIIII)LX/Iik;

    move-result-object v0

    goto :goto_0

    .line 2605210
    :pswitch_3
    iget-object v0, p0, LX/Iiz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0839da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/Iiz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0839db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0839dc

    const v4, 0x7f02023f

    const v5, 0x7f0839dd

    const v6, 0x7f020b36

    const v7, 0x7f0839de

    const v8, 0x7f0203b1

    const v9, 0x7f0839e0

    const v10, 0x7f030de6

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/Iiz;->a(Ljava/lang/String;Ljava/lang/String;IIIIIIII)LX/Iik;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
