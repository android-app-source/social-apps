.class public final LX/Ie3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;)V
    .locals 0

    .prologue
    .line 2598273
    iput-object p1, p0, LX/Ie3;->a:Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2598270
    iget-object v0, p0, LX/Ie3;->a:Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;

    const/4 v1, 0x0

    .line 2598271
    iput-object v1, v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2598272
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2598274
    iget-object v0, p0, LX/Ie3;->a:Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;

    iget-object v1, p0, LX/Ie3;->a:Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;

    iget-object v1, v1, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2598275
    const v2, -0x1783399

    :try_start_0
    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v2}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v2

    .line 2598276
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2598277
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 2598278
    if-eqz v2, :cond_0

    .line 2598279
    iget-object p1, v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->c:LX/3MV;

    invoke-virtual {p1, v2}, LX/3MV;->a(LX/5Vu;)Lcom/facebook/user/model/User;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2598280
    :catch_0
    move-exception v2

    .line 2598281
    iget-object v3, v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->h:LX/03V;

    const-string v4, "BotComposerDataUpdater"

    const-string p1, "Encountered error when updating bots menu"

    invoke-virtual {v3, v4, p1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2598282
    :goto_1
    iget-object v0, p0, LX/Ie3;->a:Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;

    const/4 v1, 0x0

    .line 2598283
    iput-object v1, v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2598284
    return-void

    .line 2598285
    :cond_1
    :try_start_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2598286
    iget-object v3, v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->f:LX/3N0;

    invoke-virtual {v3, v2}, LX/3N0;->a(Ljava/util/List;)V

    .line 2598287
    iget-object v3, v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->b:LX/2Oi;

    invoke-virtual {v3, v2}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 2598288
    new-instance v2, Landroid/content/Intent;

    const-string v3, "bot_composer_user_data_update"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2598289
    const-string v3, "bot_composer_page_id"

    iget-object v4, v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2598290
    iget-object v3, v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->j:LX/0Xl;

    invoke-interface {v3, v2}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
