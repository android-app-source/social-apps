.class public LX/IJ7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/IJ7;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2563093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2563094
    iput-object p1, p0, LX/IJ7;->a:LX/0Zb;

    .line 2563095
    return-void
.end method

.method public static a(LX/0QB;)LX/IJ7;
    .locals 4

    .prologue
    .line 2563096
    sget-object v0, LX/IJ7;->b:LX/IJ7;

    if-nez v0, :cond_1

    .line 2563097
    const-class v1, LX/IJ7;

    monitor-enter v1

    .line 2563098
    :try_start_0
    sget-object v0, LX/IJ7;->b:LX/IJ7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2563099
    if-eqz v2, :cond_0

    .line 2563100
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2563101
    new-instance p0, LX/IJ7;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/IJ7;-><init>(LX/0Zb;)V

    .line 2563102
    move-object v0, p0

    .line 2563103
    sput-object v0, LX/IJ7;->b:LX/IJ7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2563104
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2563105
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2563106
    :cond_1
    sget-object v0, LX/IJ7;->b:LX/IJ7;

    return-object v0

    .line 2563107
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2563108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
