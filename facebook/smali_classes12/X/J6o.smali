.class public final LX/J6o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V
    .locals 0

    .prologue
    .line 2650179
    iput-object p1, p0, LX/J6o;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 9

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 2650180
    iget-object v0, p0, LX/J6o;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    iget-object v0, v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    const/16 v3, 0x40

    if-ge v0, v3, :cond_0

    .line 2650181
    iget-object v0, p0, LX/J6o;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    iget-object v0, v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->g:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2650182
    :goto_0
    iget-object v0, p0, LX/J6o;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    iget-object v0, v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2650183
    return v5

    .line 2650184
    :cond_0
    iget-object v0, p0, LX/J6o;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    iget-object v0, v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->g:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2650185
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 2650186
    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 2650187
    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1, v2}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2650188
    iget-object v1, p0, LX/J6o;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    iget-object v1, v1, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method
