.class public LX/JPm;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2690390
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2690391
    const v0, 0x7f0214bf

    iput v0, p0, LX/JPm;->b:I

    .line 2690392
    const v0, 0x7f0308a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2690393
    const v0, 0x7f0d1667

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JPm;->a:Landroid/widget/TextView;

    .line 2690394
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;II)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2690395
    iget-object v1, p0, LX/JPm;->a:Landroid/widget/TextView;

    int-to-float v2, p3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 2690396
    iget-object v1, p0, LX/JPm;->a:Landroid/widget/TextView;

    if-ne p2, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2690397
    iget-object v0, p0, LX/JPm;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 2690398
    iget-object v0, p0, LX/JPm;->a:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2690399
    iget-object v0, p0, LX/JPm;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2690400
    return-void

    .line 2690401
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
