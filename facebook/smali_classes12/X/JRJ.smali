.class public LX/JRJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/2g9;

.field public final b:LX/JRB;


# direct methods
.method public constructor <init>(LX/2g9;LX/JRB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693163
    iput-object p1, p0, LX/JRJ;->a:LX/2g9;

    .line 2693164
    iput-object p2, p0, LX/JRJ;->b:LX/JRB;

    .line 2693165
    return-void
.end method

.method public static a(LX/0QB;)LX/JRJ;
    .locals 5

    .prologue
    .line 2693166
    const-class v1, LX/JRJ;

    monitor-enter v1

    .line 2693167
    :try_start_0
    sget-object v0, LX/JRJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693168
    sput-object v2, LX/JRJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693169
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693170
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693171
    new-instance p0, LX/JRJ;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    invoke-static {v0}, LX/JRB;->a(LX/0QB;)LX/JRB;

    move-result-object v4

    check-cast v4, LX/JRB;

    invoke-direct {p0, v3, v4}, LX/JRJ;-><init>(LX/2g9;LX/JRB;)V

    .line 2693172
    move-object v0, p0

    .line 2693173
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693174
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693175
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693176
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
