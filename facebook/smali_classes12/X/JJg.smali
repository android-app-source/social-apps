.class public LX/JJg;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBShopNativeModule"
.end annotation


# instance fields
.field public a:LX/7j6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679700
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2679701
    return-void
.end method

.method public static synthetic a(LX/JJg;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2679699
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679698
    const-string v0, "FBShopNativeModule"

    return-object v0
.end method

.method public onNextScroll(Lcom/facebook/react/bridge/Callback;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679702
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d2db4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 2679703
    new-instance v1, LX/JJf;

    invoke-direct {v1, p0, v0, p1}, LX/JJf;-><init>(LX/JJg;Lcom/facebook/widget/ScrollingAwareScrollView;Lcom/facebook/react/bridge/Callback;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->a(LX/4oV;)V

    .line 2679704
    return-void
.end method

.method public openCreateShop(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679692
    iget-object v0, p0, LX/JJg;->a:LX/7j6;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/7j6;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2679693
    return-void
.end method

.method public openEditShop(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679694
    iget-object v0, p0, LX/JJg;->a:LX/7j6;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, LX/7j6;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 2679695
    return-void
.end method

.method public storefrontViewLayoutChanged(IIII)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679696
    new-instance v0, Lcom/facebook/fbreact/commerce/FBShopNativeModule$1;

    invoke-direct {v0, p0, p3, p4}, Lcom/facebook/fbreact/commerce/FBShopNativeModule$1;-><init>(LX/JJg;II)V

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 2679697
    return-void
.end method
