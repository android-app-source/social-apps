.class public final LX/IJ9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 2563112
    iput-object p1, p0, LX/IJ9;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    iput-object p2, p0, LX/IJ9;->a:Landroid/support/v4/app/DialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2563113
    iget-object v0, p0, LX/IJ9;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2563114
    iget-object v0, p0, LX/IJ9;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->x:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2563115
    sget-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->v:Ljava/lang/Class;

    const-string v1, "fail to send invite."

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2563116
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2563117
    iget-object v0, p0, LX/IJ9;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2563118
    iget-object v0, p0, LX/IJ9;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08385c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2563119
    iget-object v0, p0, LX/IJ9;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    .line 2563120
    const-class v1, LX/7g9;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7g9;

    .line 2563121
    if-eqz v1, :cond_0

    .line 2563122
    invoke-interface {v1}, LX/7g9;->mr_()V

    .line 2563123
    :cond_0
    return-void
.end method
