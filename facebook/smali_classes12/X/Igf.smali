.class public final LX/Igf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ige;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V
    .locals 0

    .prologue
    .line 2601402
    iput-object p1, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2601403
    iget-object v0, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    invoke-virtual {v0}, Lcom/facebook/widget/CountBadge;->b()V

    .line 2601404
    iget-object v0, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    invoke-virtual {v0}, LX/Igt;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    sget-object v1, LX/Igi;->SELECTED:LX/Igi;

    if-ne v0, v1, :cond_0

    .line 2601405
    iget-object v0, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    const/4 p0, 0x0

    .line 2601406
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2601407
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2601408
    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2601409
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->q:Landroid/view/ViewGroup;

    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2601410
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->o:Landroid/widget/RadioButton;

    invoke-virtual {v1, p0}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 2601411
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->u:Landroid/widget/RadioButton;

    invoke-virtual {v1, p0}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 2601412
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2601413
    invoke-static {v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->o(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    .line 2601414
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(JZ)V
    .locals 5

    .prologue
    .line 2601415
    iget-object v0, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    if-eqz v0, :cond_0

    .line 2601416
    iget-object v0, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601417
    if-nez v0, :cond_1

    .line 2601418
    invoke-direct {p0}, LX/Igf;->b()V

    .line 2601419
    :cond_0
    :goto_0
    return-void

    .line 2601420
    :cond_1
    if-eqz p3, :cond_2

    .line 2601421
    iget-object v1, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    invoke-virtual {v1}, Lcom/facebook/widget/CountBadge;->a()V

    .line 2601422
    iget-object v1, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->h:LX/6eS;

    invoke-virtual {v1, v0}, LX/6eS;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/Igb;

    iget-object v3, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-direct {v2, v3, v0}, LX/Igb;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;Lcom/facebook/ui/media/attachments/MediaResource;)V

    iget-object v0, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->b:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2601423
    :goto_1
    iget-object v0, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->q(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    goto :goto_0

    .line 2601424
    :cond_2
    iget-object v1, p0, LX/Igf;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->e:LX/FGd;

    invoke-virtual {v1, v0}, LX/FGd;->b(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2601425
    invoke-direct {p0}, LX/Igf;->b()V

    goto :goto_1
.end method
