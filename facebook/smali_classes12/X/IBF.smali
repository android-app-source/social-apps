.class public final LX/IBF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/model/Event;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/IBH;


# direct methods
.method public constructor <init>(LX/IBH;Lcom/facebook/events/model/Event;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2546729
    iput-object p1, p0, LX/IBF;->c:LX/IBH;

    iput-object p2, p0, LX/IBF;->a:Lcom/facebook/events/model/Event;

    iput-object p3, p0, LX/IBF;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2546730
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    const/4 v5, 0x0

    .line 2546731
    iget-object v0, p0, LX/IBF;->c:LX/IBH;

    iget-object v1, v0, LX/IBH;->b:LX/1Kf;

    iget-object v0, p0, LX/IBF;->c:LX/IBH;

    iget-object v2, p0, LX/IBF;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, p1, v2}, LX/IBH;->b(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/events/model/Event;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/16 v3, 0x1f6

    iget-object v0, p0, LX/IBF;->b:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v5, v2, v3, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2546732
    return-object v5
.end method
