.class public LX/INW;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

.field public b:LX/INT;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2570895
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2570896
    const/4 p1, 0x1

    const/4 v2, 0x0

    .line 2570897
    invoke-virtual {p0}, LX/INW;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0302f7

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2570898
    invoke-virtual {p0, p1}, LX/INW;->setOrientation(I)V

    .line 2570899
    invoke-virtual {p0}, LX/INW;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2570900
    const v1, 0x7f020c6f

    invoke-virtual {p0, v1}, LX/INW;->setBackgroundResource(I)V

    .line 2570901
    invoke-virtual {p0, v2, v0, v2, v2}, LX/INW;->setPadding(IIII)V

    .line 2570902
    new-instance v0, LX/INT;

    invoke-direct {v0, p0}, LX/INT;-><init>(LX/INW;)V

    iput-object v0, p0, LX/INW;->b:LX/INT;

    .line 2570903
    const v0, 0x7f0d0a36

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

    iput-object v0, p0, LX/INW;->a:Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

    .line 2570904
    iget-object v0, p0, LX/INW;->a:Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->setType(I)V

    .line 2570905
    iget-object v0, p0, LX/INW;->a:Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

    iget-object v1, p0, LX/INW;->b:LX/INT;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2570906
    return-void
.end method
