.class public LX/HQn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/HQm;

.field public final c:F

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/HQm;FLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)V
    .locals 0

    .prologue
    .line 2463986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2463987
    iput-object p1, p0, LX/HQn;->a:Ljava/lang/String;

    .line 2463988
    iput-object p2, p0, LX/HQn;->b:LX/HQm;

    .line 2463989
    iput p3, p0, LX/HQn;->c:F

    .line 2463990
    iput-object p4, p0, LX/HQn;->d:Ljava/lang/String;

    .line 2463991
    iput-object p5, p0, LX/HQn;->e:Ljava/lang/String;

    .line 2463992
    iput-object p6, p0, LX/HQn;->f:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 2463993
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2463994
    instance-of v1, p1, LX/HQn;

    if-nez v1, :cond_1

    .line 2463995
    :cond_0
    :goto_0
    return v0

    .line 2463996
    :cond_1
    check-cast p1, LX/HQn;

    .line 2463997
    iget-object v1, p0, LX/HQn;->a:Ljava/lang/String;

    iget-object v2, p1, LX/HQn;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/HQn;->b:LX/HQm;

    iget-object v2, p1, LX/HQn;->b:LX/HQm;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LX/HQn;->c:F

    iget v2, p1, LX/HQn;->c:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    iget-object v1, p0, LX/HQn;->d:Ljava/lang/String;

    iget-object v2, p1, LX/HQn;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/HQn;->e:Ljava/lang/String;

    iget-object v2, p1, LX/HQn;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/HQn;->f:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iget-object v2, p1, LX/HQn;->f:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2463998
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/HQn;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/HQn;->b:LX/HQm;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, LX/HQn;->c:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/HQn;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/HQn;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/HQn;->f:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
