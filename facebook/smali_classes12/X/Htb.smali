.class public LX/Htb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/facebook/user/model/User;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8zA;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/HtU;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/user/model/User;LX/0Ot;LX/HtU;)V
    .locals 0
    .param p2    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p4    # LX/HtU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/user/model/User;",
            "LX/0Ot",
            "<",
            "LX/8zA;",
            ">;",
            "Lcom/facebook/composer/header/ComposerMetaTextController$Listener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516513
    iput-object p1, p0, LX/Htb;->a:Landroid/content/Context;

    .line 2516514
    iput-object p2, p0, LX/Htb;->b:Lcom/facebook/user/model/User;

    .line 2516515
    iput-object p3, p0, LX/Htb;->c:LX/0Ot;

    .line 2516516
    iput-object p4, p0, LX/Htb;->d:LX/HtU;

    .line 2516517
    return-void
.end method

.method private static a(JLX/0Px;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2516506
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 2516507
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v4

    cmp-long v0, v4, p0

    if-nez v0, :cond_0

    .line 2516508
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2516509
    :goto_1
    return v0

    .line 2516510
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2516511
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    goto :goto_1
.end method

.method private static a(JLX/0Px;I)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;I)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2516499
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2516500
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v5

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {p2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 2516501
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v6

    cmp-long v1, v6, p0

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2516502
    add-int/lit8 v1, v2, 0x1

    if-ge v2, p3, :cond_0

    .line 2516503
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 2516504
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 2516505
    :cond_0
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private static b(LX/Htb;LX/0Px;Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Z)Landroid/text/SpannedString;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "Z)",
            "Landroid/text/SpannedString;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 2516453
    new-instance v0, LX/8z6;

    invoke-direct {v0}, LX/8z6;-><init>()V

    .line 2516454
    iput-boolean p6, v0, LX/8z6;->f:Z

    .line 2516455
    move-object v0, v0

    .line 2516456
    iput-boolean p6, v0, LX/8z6;->g:Z

    .line 2516457
    move-object v0, v0

    .line 2516458
    new-instance v1, LX/Hta;

    invoke-direct {v1, p0}, LX/Hta;-><init>(LX/Htb;)V

    .line 2516459
    iput-object v1, v0, LX/8z6;->h:LX/8z9;

    .line 2516460
    move-object v1, v0

    .line 2516461
    iput-object p5, v1, LX/8z6;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2516462
    iput-object p3, v1, LX/8z6;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2516463
    move-object v0, v1

    .line 2516464
    iput-object p4, v0, LX/8z6;->c:Ljava/lang/String;

    .line 2516465
    move-object v0, v0

    .line 2516466
    iput-object p2, v0, LX/8z6;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 2516467
    iget-object v0, p0, LX/Htb;->b:Lcom/facebook/user/model/User;

    .line 2516468
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2516469
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2516470
    invoke-static {v2, v3, p1}, LX/Htb;->a(JLX/0Px;)I

    move-result v4

    .line 2516471
    if-nez p2, :cond_1

    if-ne v4, v6, :cond_1

    .line 2516472
    invoke-static {v2, v3, p1, v6}, LX/Htb;->a(JLX/0Px;I)LX/0Px;

    move-result-object v0

    .line 2516473
    invoke-virtual {v1, v0}, LX/8z6;->a(LX/0Px;)LX/8z6;

    .line 2516474
    :cond_0
    :goto_0
    iput v4, v1, LX/8z6;->e:I

    .line 2516475
    new-instance v2, Landroid/text/SpannedString;

    iget-object v0, p0, LX/Htb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8zA;

    invoke-virtual {v1}, LX/8z6;->a()LX/8z5;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8zA;->a(LX/8z5;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    return-object v2

    .line 2516476
    :cond_1
    if-eqz p2, :cond_2

    if-eq v4, v5, :cond_3

    :cond_2
    if-nez p2, :cond_0

    .line 2516477
    :cond_3
    invoke-static {v2, v3, p1, v5}, LX/Htb;->a(JLX/0Px;I)LX/0Px;

    move-result-object v0

    .line 2516478
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2516479
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/8z6;->b(Ljava/lang/String;)LX/8z6;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Z)Landroid/text/SpannedString;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "Z)",
            "Landroid/text/SpannedString;"
        }
    .end annotation

    .prologue
    .line 2516480
    invoke-static/range {p0 .. p6}, LX/Htb;->b(LX/Htb;LX/0Px;Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Z)Landroid/text/SpannedString;

    move-result-object v0

    .line 2516481
    if-eqz p6, :cond_1

    .line 2516482
    :cond_0
    :goto_0
    return-object v0

    .line 2516483
    :cond_1
    invoke-virtual {v0}, Landroid/text/SpannedString;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2516484
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2516485
    if-eqz p5, :cond_4

    invoke-virtual {p5}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v0

    .line 2516486
    :goto_1
    const/16 p4, 0x21

    .line 2516487
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2516488
    if-eqz v1, :cond_3

    .line 2516489
    const-string p1, " \u2014 "

    invoke-virtual {v2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2516490
    if-eqz v0, :cond_2

    .line 2516491
    const/16 p1, 0x200c

    invoke-virtual {v2, p1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 2516492
    const-string p1, " "

    invoke-virtual {v2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2516493
    const/4 p1, 0x3

    const/4 p2, 0x4

    invoke-virtual {v2, v0, p1, p2, p4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2516494
    :cond_2
    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2516495
    :cond_3
    new-instance p1, LX/8zE;

    iget-object p2, p0, LX/Htb;->a:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0a0508

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-direct {p1, p2}, LX/8zE;-><init>(I)V

    const/4 p2, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p3

    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2516496
    new-instance p1, Landroid/text/SpannedString;

    invoke-direct {p1, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, p1

    .line 2516497
    goto :goto_0

    .line 2516498
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
