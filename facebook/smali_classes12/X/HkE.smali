.class public LX/HkE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HkB;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:LX/HkJ;

.field private final d:LX/Hk2;

.field private final e:LX/Hjs;

.field private final f:LX/Hj3;

.field private final g:I

.field public h:Z

.field public final i:Landroid/os/Handler;

.field public final j:Ljava/lang/Runnable;

.field public k:LX/HjL;

.field public l:LX/Hjx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/Hk2;LX/Hj3;LX/Hjs;ILjava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "LX/Hk2;",
            "LX/Hj3;",
            "LX/Hjs;",
            "I",
            "Ljava/util/EnumSet",
            "<",
            "LX/HjA;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/HkE;->a:Landroid/content/Context;

    iput-object p2, p0, LX/HkE;->b:Ljava/lang/String;

    iput-object p3, p0, LX/HkE;->d:LX/Hk2;

    iput-object p4, p0, LX/HkE;->f:LX/Hj3;

    iput-object p5, p0, LX/HkE;->e:LX/Hjs;

    iput p6, p0, LX/HkE;->g:I

    new-instance v0, LX/HkJ;

    invoke-direct {v0}, LX/HkJ;-><init>()V

    iput-object v0, p0, LX/HkE;->c:LX/HkJ;

    iget-object v0, p0, LX/HkE;->c:LX/HkJ;

    iput-object p0, v0, LX/HkJ;->c:LX/HkB;

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HkE;->h:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/HkE;->i:Landroid/os/Handler;

    new-instance v0, Lcom/facebook/ads/internal/i$b;

    invoke-direct {v0, p0}, Lcom/facebook/ads/internal/i$b;-><init>(LX/HkE;)V

    iput-object v0, p0, LX/HkE;->j:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    new-instance v0, LX/Hk0;

    iget-object v1, p0, LX/HkE;->a:Landroid/content/Context;

    iget-object v2, p0, LX/HkE;->b:Ljava/lang/String;

    iget-object v3, p0, LX/HkE;->f:LX/Hj3;

    iget-object v4, p0, LX/HkE;->d:LX/Hk2;

    iget-object v5, p0, LX/HkE;->e:LX/Hjs;

    iget v6, p0, LX/HkE;->g:I

    iget-object v7, p0, LX/HkE;->a:Landroid/content/Context;

    invoke-static {v7}, LX/Hj2;->a(Landroid/content/Context;)Z

    move-result v7

    invoke-direct/range {v0 .. v7}, LX/Hk0;-><init>(Landroid/content/Context;Ljava/lang/String;LX/Hj3;LX/Hk2;LX/Hjs;IZ)V

    iget-object v1, p0, LX/HkE;->c:LX/HkJ;

    iget-object v2, p0, LX/HkE;->a:Landroid/content/Context;

    invoke-static {v1}, LX/HkJ;->b(LX/HkJ;)V

    const/4 v4, 0x1

    const-string v3, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    :goto_0
    move v3, v3

    if-nez v3, :cond_0

    new-instance v3, LX/Hjr;

    sget-object v4, LX/HjN;->NETWORK_ERROR:LX/HjN;

    const-string v5, "No network connection"

    invoke-direct {v3, v4, v5}, LX/Hjr;-><init>(LX/HjN;Ljava/lang/String;)V

    invoke-static {v1, v3}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V

    :goto_1
    return-void

    :cond_0
    iput-object v0, v1, LX/HkJ;->d:LX/Hk0;

    invoke-static {v0}, LX/Hkj;->a(LX/Hk0;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, LX/Hkj;->c:Ljava/util/Map;

    invoke-static {v0}, LX/Hkj;->d(LX/Hk0;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v3, v3

    if-eqz v3, :cond_1

    invoke-static {v1, v3}, LX/HkJ;->a$redex0(LX/HkJ;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sget-object v3, LX/HjN;->LOAD_TOO_FREQUENTLY:LX/HjN;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v3

    invoke-static {v1, v3}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V

    goto :goto_1

    :cond_2
    sget-object v3, LX/HkJ;->h:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v4, Lcom/facebook/ads/internal/server/a$1;

    invoke-direct {v4, v1, v2, v0}, Lcom/facebook/ads/internal/server/a$1;-><init>(LX/HkJ;Landroid/content/Context;LX/Hk0;)V

    const v5, -0x56d00bb8

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_1

    :cond_3
    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(LX/Hjr;)V
    .locals 5

    iget-boolean v0, p0, LX/HkE;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HkE;->i:Landroid/os/Handler;

    iget-object v1, p0, LX/HkE;->j:Ljava/lang/Runnable;

    const-wide/32 v2, 0x1b7740

    const v4, 0x19375b3a

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    :cond_0
    iget-object v0, p0, LX/HkE;->k:LX/HjL;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HkE;->k:LX/HjL;

    invoke-virtual {v0, p1}, LX/HjL;->a(LX/Hjr;)V

    :cond_1
    return-void
.end method

.method public final a(LX/HkN;)V
    .locals 6

    iget-object v0, p1, LX/HkM;->a:LX/Hjx;

    move-object v2, v0

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no placement in response"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, LX/HkE;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, v2, LX/Hjx;->c:LX/Hjy;

    move-object v0, v0

    invoke-virtual {v0}, LX/Hjy;->b()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    const-wide/32 v0, 0x1b7740

    :cond_1
    iget-object v3, p0, LX/HkE;->i:Landroid/os/Handler;

    iget-object v4, p0, LX/HkE;->j:Ljava/lang/Runnable;

    const v5, -0x2efa6e12

    invoke-static {v3, v4, v0, v1, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    :cond_2
    iput-object v2, p0, LX/HkE;->l:LX/Hjx;

    iget-object v2, p0, LX/HkE;->l:LX/Hjx;

    invoke-virtual {v2}, LX/Hjx;->c()LX/Hju;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    iget-object v1, v2, LX/Hjx;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move v1, v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_4

    iget-object v0, v1, LX/Hju;->b:Ljava/lang/String;

    sget-object v4, LX/HkF;->NATIVE:LX/HkF;

    invoke-static {v0, v4}, LX/Hjb;->a(Ljava/lang/String;LX/HkF;)LX/HjT;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, LX/HjT;->a()LX/HkF;

    move-result-object v4

    sget-object v5, LX/HkF;->NATIVE:LX/HkF;

    if-ne v4, v5, :cond_3

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v5, "data"

    iget-object v1, v1, LX/Hju;->c:Lorg/json/JSONObject;

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "definition"

    iget-object v5, v2, LX/Hjx;->c:LX/Hjy;

    move-object v5, v5

    invoke-interface {v4, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, LX/Hjj;

    iget-object v1, p0, LX/HkE;->a:Landroid/content/Context;

    new-instance v5, LX/HkD;

    invoke-direct {v5, p0, v3}, LX/HkD;-><init>(LX/HkE;Ljava/util/List;)V

    invoke-virtual {v0, v1, v5, v4}, LX/Hjj;->a(Landroid/content/Context;LX/Hjq;Ljava/util/Map;)V

    :cond_3
    invoke-virtual {v2}, LX/Hjx;->c()LX/Hju;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_4
    move-object v0, v3

    iget-object v1, p0, LX/HkE;->k:LX/HjL;

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, LX/HkE;->k:LX/HjL;

    sget-object v1, LX/HjN;->NO_FILL:LX/HjN;

    const-string v2, ""

    invoke-virtual {v1, v2}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HjL;->a(LX/Hjr;)V

    :cond_5
    :goto_1
    return-void

    :cond_6
    iget-object v1, p0, LX/HkE;->k:LX/HjL;

    invoke-virtual {v1, v0}, LX/HjL;->a(Ljava/util/List;)V

    goto :goto_1
.end method
