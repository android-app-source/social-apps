.class public LX/HgN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0if;

.field public b:LX/Hfw;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2492944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2492945
    iput-object p1, p0, LX/HgN;->a:LX/0if;

    .line 2492946
    sget-object v0, LX/Hfw;->ALL:LX/Hfw;

    iput-object v0, p0, LX/HgN;->b:LX/Hfw;

    .line 2492947
    return-void
.end method

.method public static a(LX/0QB;)LX/HgN;
    .locals 4

    .prologue
    .line 2492948
    const-class v1, LX/HgN;

    monitor-enter v1

    .line 2492949
    :try_start_0
    sget-object v0, LX/HgN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2492950
    sput-object v2, LX/HgN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2492951
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2492952
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2492953
    new-instance p0, LX/HgN;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/HgN;-><init>(LX/0if;)V

    .line 2492954
    move-object v0, p0

    .line 2492955
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2492956
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HgN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2492957
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2492958
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(ILcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;)LX/1rQ;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2492959
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    .line 2492960
    invoke-virtual {p1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v3

    .line 2492961
    if-nez v3, :cond_0

    move-object v0, v2

    .line 2492962
    :goto_0
    return-object v0

    .line 2492963
    :cond_0
    const-string v0, "position"

    invoke-virtual {v2, v0, p0}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    .line 2492964
    const-string v0, "group_id"

    invoke-virtual {v3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2492965
    const-string v4, "join_status"

    invoke-virtual {v3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v4, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2492966
    invoke-virtual {v3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    const-string v4, "has_cover_photo"

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v2, v4, v0}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    .line 2492967
    const-string v0, "member_profiles_count"

    invoke-virtual {v3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->k()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel;

    move-result-object v4

    if-nez v4, :cond_3

    :goto_3
    invoke-virtual {v2, v0, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-object v0, v2

    .line 2492968
    goto :goto_0

    .line 2492969
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2492970
    goto :goto_2

    .line 2492971
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->k()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel;->a()I

    move-result v1

    goto :goto_3
.end method

.method public static c(LX/Hfw;)LX/1rQ;
    .locals 3

    .prologue
    .line 2492972
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    .line 2492973
    const-string v1, "ordering"

    invoke-virtual {p0}, LX/Hfw;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2492974
    return-object v0
.end method

.method public static c(LX/HgN;ILcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)LX/1rQ;
    .locals 4

    .prologue
    .line 2492975
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    .line 2492976
    invoke-virtual {p2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v1

    .line 2492977
    if-nez v1, :cond_0

    .line 2492978
    :goto_0
    return-object v0

    .line 2492979
    :cond_0
    const-string v2, "position"

    invoke-virtual {v0, v2, p1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    .line 2492980
    const-string v2, "ordering"

    iget-object v3, p0, LX/HgN;->b:LX/Hfw;

    invoke-virtual {v3}, LX/Hfw;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2492981
    const-string v2, "group_id"

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2492982
    const-string v2, "is_favorite"

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->j()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    .line 2492983
    const-string v2, "badge_count"

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->p()I

    move-result v1

    invoke-virtual {v0, v2, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    goto :goto_0
.end method


# virtual methods
.method public final a(ILcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2492984
    invoke-static {p0, p1, p2}, LX/HgN;->c(LX/HgN;ILcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)LX/1rQ;

    move-result-object v0

    .line 2492985
    const-string v1, "menu_option"

    invoke-virtual {v0, v1, p3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2492986
    iget-object v1, p0, LX/HgN;->a:LX/0if;

    sget-object v2, LX/0ig;->af:LX/0ih;

    const-string v3, "group_more_selected_option"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2492987
    return-void
.end method
