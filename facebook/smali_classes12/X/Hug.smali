.class public LX/Hug;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0iq;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsTagExpansionPillSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<+",
            "LX/8p2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0il;LX/8p2;)V
    .locals 2
    .param p2    # LX/8p2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/8p2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2517837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517838
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hug;->a:Ljava/lang/ref/WeakReference;

    .line 2517839
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hug;->b:Ljava/lang/ref/WeakReference;

    .line 2517840
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2517829
    iget-object v0, p0, LX/Hug;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517830
    iget-object v1, p0, LX/Hug;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8p2;

    .line 2517831
    if-nez v1, :cond_0

    .line 2517832
    :goto_0
    return-void

    :cond_0
    move-object v2, v0

    .line 2517833
    check-cast v2, LX/0ik;

    invoke-interface {v2}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2zG;

    invoke-virtual {v2}, LX/2zG;->B()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2517834
    const/4 v0, 0x0

    invoke-interface {v1, v0}, LX/8p2;->setShouldShowTagExpansionInfo(Z)V

    goto :goto_0

    .line 2517835
    :cond_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    invoke-static {v0}, LX/94d;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Z

    move-result v0

    .line 2517836
    invoke-interface {v1, v0}, LX/8p2;->setShouldShowTagExpansionInfo(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 2517826
    sget-object v0, LX/Huf;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517827
    :goto_0
    return-void

    .line 2517828
    :pswitch_0
    invoke-direct {p0}, LX/Hug;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2517824
    invoke-direct {p0}, LX/Hug;->b()V

    .line 2517825
    return-void
.end method
