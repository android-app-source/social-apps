.class public final LX/Hw7;
.super LX/2EJ;
.source ""


# instance fields
.field public final synthetic b:LX/Hw8;

.field public c:Landroid/widget/EditText;

.field public d:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(LX/Hw8;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2520123
    iput-object p1, p0, LX/Hw7;->b:LX/Hw8;

    .line 2520124
    invoke-direct {p0, p2}, LX/2EJ;-><init>(Landroid/content/Context;)V

    .line 2520125
    const/4 p2, 0x0

    .line 2520126
    iget-object v0, p0, LX/Hw7;->b:LX/Hw8;

    iget-object v0, v0, LX/Hw8;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2520127
    const p1, 0x7f031265

    invoke-virtual {v0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 2520128
    invoke-virtual {p0, p1}, LX/2EJ;->a(Landroid/view/View;)V

    .line 2520129
    invoke-virtual {p0, p2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2520130
    iget-object v0, p0, LX/Hw7;->b:LX/Hw8;

    iget-object v0, v0, LX/Hw8;->b:Landroid/content/Context;

    const p2, 0x7f08144f

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Hw7;->setTitle(Ljava/lang/CharSequence;)V

    .line 2520131
    iget-object v0, p0, LX/Hw7;->b:LX/Hw8;

    iget-object v0, v0, LX/Hw8;->b:Landroid/content/Context;

    const p2, 0x7f081453

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance p2, LX/Hw1;

    invoke-direct {p2, p0}, LX/Hw1;-><init>(LX/Hw7;)V

    invoke-virtual {p0, v0, p2}, LX/2EJ;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2520132
    new-instance v0, LX/Hw3;

    invoke-direct {v0, p0}, LX/Hw3;-><init>(LX/Hw7;)V

    invoke-virtual {p0, v0}, LX/Hw7;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 2520133
    iget-object v0, p0, LX/Hw7;->b:LX/Hw8;

    iget-object v0, v0, LX/Hw8;->b:Landroid/content/Context;

    const p2, 0x7f080017

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance p2, LX/Hw4;

    invoke-direct {p2, p0}, LX/Hw4;-><init>(LX/Hw7;)V

    invoke-virtual {p0, v0, p2}, LX/2EJ;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2520134
    const v0, 0x7f0d0e70

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LX/Hw7;->c:Landroid/widget/EditText;

    .line 2520135
    iget-object v0, p0, LX/Hw7;->c:Landroid/widget/EditText;

    new-instance p2, LX/Hw5;

    invoke-direct {p2, p0}, LX/Hw5;-><init>(LX/Hw7;)V

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2520136
    invoke-virtual {p0}, LX/Hw7;->b()V

    .line 2520137
    const v0, 0x7f0d0e71

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LX/Hw7;->d:Landroid/widget/EditText;

    .line 2520138
    iget-object v0, p0, LX/Hw7;->d:Landroid/widget/EditText;

    new-instance p1, LX/Hw6;

    invoke-direct {p1, p0}, LX/Hw6;-><init>(LX/Hw7;)V

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2520139
    invoke-virtual {p0}, LX/Hw7;->c()V

    .line 2520140
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 2520141
    iget-object v0, p0, LX/Hw7;->c:Landroid/widget/EditText;

    iget-object v1, p0, LX/Hw7;->b:LX/Hw8;

    iget-object v1, v1, LX/Hw8;->g:LX/Hvy;

    iget-object v2, p0, LX/Hw7;->b:LX/Hw8;

    iget-object v2, v2, LX/Hw8;->d:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/Hvy;->b(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2520142
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2520143
    iget-object v0, p0, LX/Hw7;->d:Landroid/widget/EditText;

    iget-object v1, p0, LX/Hw7;->b:LX/Hw8;

    iget-object v1, v1, LX/Hw8;->g:LX/Hvy;

    iget-object v2, p0, LX/Hw7;->b:LX/Hw8;

    iget-object v2, v2, LX/Hw8;->d:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/Hvy;->c(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2520144
    return-void
.end method
