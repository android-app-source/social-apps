.class public final LX/IAi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/IAk;


# direct methods
.method public constructor <init>(LX/IAk;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2546039
    iput-object p1, p0, LX/IAi;->b:LX/IAk;

    iput-object p2, p0, LX/IAi;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2546040
    iget-object v0, p0, LX/IAi;->b:LX/IAk;

    iget-object v1, p0, LX/IAi;->a:Ljava/lang/String;

    .line 2546041
    iget-object v2, v0, LX/IAk;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b145a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2546042
    new-instance v3, LX/7nI;

    invoke-direct {v3}, LX/7nI;-><init>()V

    move-object v3, v3

    .line 2546043
    const-string v4, "event_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string p0, "profile_image_size"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, p0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2546044
    iget-object v2, v0, LX/IAk;->a:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    move-object v0, v2

    .line 2546045
    return-object v0
.end method
