.class public LX/JJr;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBProfileCurationTagsEditViewEventHandler"
.end annotation


# instance fields
.field private final a:LX/JJt;


# direct methods
.method public constructor <init>(LX/JJt;LX/5pY;)V
    .locals 0
    .param p2    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679866
    invoke-direct {p0, p2}, LX/5pb;-><init>(LX/5pY;)V

    .line 2679867
    iput-object p1, p0, LX/JJr;->a:LX/JJt;

    .line 2679868
    return-void
.end method


# virtual methods
.method public didCompleteTagsEditing()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679870
    iget-object v0, p0, LX/JJr;->a:LX/JJt;

    new-instance v1, LX/JJq;

    invoke-direct {v1}, LX/JJq;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2679871
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679869
    const-string v0, "FBProfileCurationTagsEditViewEventHandler"

    return-object v0
.end method
