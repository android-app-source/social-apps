.class public LX/Iuc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/Iua;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/lang/Object;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2626964
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "chat:web"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "web"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "titan:web"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "messenger:web"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/Iuc;->a:Ljava/util/Set;

    .line 2626965
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Iuc;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2626966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2626967
    iput-object p1, p0, LX/Iuc;->b:LX/0Or;

    .line 2626968
    iput-object p2, p0, LX/Iuc;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2626969
    return-void
.end method

.method public static a(LX/0QB;)LX/Iuc;
    .locals 8

    .prologue
    .line 2626970
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2626971
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2626972
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2626973
    if-nez v1, :cond_0

    .line 2626974
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2626975
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2626976
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2626977
    sget-object v1, LX/Iuc;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2626978
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2626979
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2626980
    :cond_1
    if-nez v1, :cond_4

    .line 2626981
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2626982
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2626983
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2626984
    new-instance v7, LX/Iuc;

    const/16 v1, 0x15e8

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v7, p0, v1}, LX/Iuc;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2626985
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2626986
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2626987
    if-nez v1, :cond_2

    .line 2626988
    sget-object v0, LX/Iuc;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iuc;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2626989
    :goto_1
    if-eqz v0, :cond_3

    .line 2626990
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2626991
    :goto_3
    check-cast v0, LX/Iuc;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2626992
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2626993
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2626994
    :catchall_1
    move-exception v0

    .line 2626995
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2626996
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2626997
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2626998
    :cond_2
    :try_start_8
    sget-object v0, LX/Iuc;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iuc;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)LX/Iuf;
    .locals 6

    .prologue
    .line 2626999
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2627000
    iget-wide v2, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 2627001
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v0

    .line 2627002
    iget-object v0, p0, LX/Iuc;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, LX/2gS;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2627003
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    .line 2627004
    if-eqz v0, :cond_0

    const-string v1, "source"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/Iuc;->a:Ljava/util/Set;

    const-string v4, "source"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2627005
    iget-object v0, p0, LX/Iuc;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2gS;->b:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2627006
    :goto_0
    sget-object v0, LX/Iuf;->FORCE_SUPPRESS:LX/Iuf;

    .line 2627007
    :goto_1
    return-object v0

    .line 2627008
    :cond_0
    iget-object v0, p0, LX/Iuc;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2gS;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    .line 2627009
    :cond_1
    iget-object v0, p0, LX/Iuc;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2gS;->b:LX/0Tn;

    const-wide/16 v4, -0x1

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 2627010
    cmp-long v4, v2, v0

    if-lez v4, :cond_2

    sub-long v0, v2, v0

    const-wide/32 v2, 0x2bf20

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 2627011
    sget-object v0, LX/Iuf;->SUPPRESS:LX/Iuf;

    goto :goto_1

    .line 2627012
    :cond_2
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2627013
    const-string v0, "LastWebSentRule"

    return-object v0
.end method

.method public final c()J
    .locals 4

    .prologue
    .line 2627014
    iget-object v0, p0, LX/Iuc;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2gS;->b:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 2627015
    iget-object v0, p0, LX/Iuc;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2gS;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2627016
    return-void
.end method
