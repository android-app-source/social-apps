.class public LX/Hj5;
.super Landroid/widget/RelativeLayout;
.source ""


# instance fields
.field private final a:LX/Hl4;

.field private final b:LX/Hl6;

.field private c:Z

.field private d:Z


# virtual methods
.method public setAutoplay(Z)V
    .locals 1

    iput-boolean p1, p0, LX/Hj5;->d:Z

    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    iput-boolean p1, v0, LX/Hl6;->f:Z

    return-void
.end method

.method public setNativeAd(LX/HjF;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iput-boolean v2, p1, LX/HjF;->u:Z

    iget-boolean v0, p0, LX/Hj5;->d:Z

    iput-boolean v0, p1, LX/HjF;->v:Z

    iget-boolean v0, p0, LX/Hj5;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hj5;->a:LX/Hl4;

    invoke-virtual {v0, v4, v4}, LX/Hl4;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    const/4 v4, 0x0

    iput-object v4, v0, LX/Hl6;->c:Ljava/lang/String;

    iput-boolean v3, p0, LX/Hj5;->c:Z

    :cond_0
    invoke-virtual {p1}, LX/HjF;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Hj5;->a:LX/Hl4;

    invoke-virtual {v0, v1}, LX/Hl4;->setVisibility(I)V

    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    invoke-virtual {v0, v3}, LX/Hl6;->setVisibility(I)V

    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    invoke-virtual {p0, v0}, LX/Hj5;->bringChildToFront(Landroid/view/View;)V

    iput-boolean v2, p0, LX/Hj5;->c:Z

    :try_start_0
    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    invoke-static {p1}, LX/HjF;->m(LX/HjF;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x0

    :goto_1
    move-object v1, v1

    iput-object v1, v0, LX/Hl6;->d:Ljava/lang/String;

    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    invoke-static {p1}, LX/HjF;->m(LX/HjF;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    :goto_2
    move-object v1, v1

    invoke-virtual {v0}, LX/Hl6;->a()V

    iput-object v1, v0, LX/Hl6;->e:Ljava/lang/String;

    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    invoke-virtual {p1}, LX/HjF;->i()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Hl6;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v2, v0, LX/Hl6;->a:LX/Hl8;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v2, LX/Hl8;->d:Landroid/net/Uri;

    iget-object v4, v2, LX/Hl8;->a:LX/HlB;

    iget-object v1, v2, LX/Hl8;->c:Landroid/view/View;

    invoke-interface {v4, v1, v3}, LX/HlB;->a(Landroid/view/View;Landroid/net/Uri;)V

    iget-boolean v2, v0, LX/Hl6;->f:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/Hl6;->a:LX/Hl8;

    invoke-virtual {v2}, LX/Hl8;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_3
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :cond_2
    invoke-virtual {p1}, LX/HjF;->b()LX/Hj9;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    invoke-virtual {v0}, LX/Hl6;->a()V

    iget-object v0, p0, LX/Hj5;->b:LX/Hl6;

    invoke-virtual {v0, v1}, LX/Hl6;->setVisibility(I)V

    iget-object v0, p0, LX/Hj5;->a:LX/Hl4;

    invoke-virtual {v0, v3}, LX/Hl4;->setVisibility(I)V

    iget-object v0, p0, LX/Hj5;->a:LX/Hl4;

    invoke-virtual {p0, v0}, LX/Hj5;->bringChildToFront(Landroid/view/View;)V

    iput-boolean v2, p0, LX/Hj5;->c:Z

    new-instance v0, LX/Hkr;

    iget-object v1, p0, LX/Hj5;->a:LX/Hl4;

    invoke-direct {v0, v1}, LX/Hkr;-><init>(LX/Hl4;)V

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p1}, LX/HjF;->b()LX/Hj9;

    move-result-object v2

    iget-object v4, v2, LX/Hj9;->a:Ljava/lang/String;

    move-object v2, v4

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, LX/Hkr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    :try_start_1
    iget-object v1, p1, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v1}, LX/Hjj;->p()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    iget-object v1, p1, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v1}, LX/Hjj;->q()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
