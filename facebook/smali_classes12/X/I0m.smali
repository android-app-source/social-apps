.class public LX/I0m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HzD;


# instance fields
.field private final a:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Ljava/util/Calendar;

.field private d:Ljava/util/Calendar;

.field private e:Lcom/facebook/events/common/EventAnalyticsParams;

.field private f:LX/HzV;

.field private g:LX/I0o;

.field public h:LX/1Ck;

.field public i:LX/I0f;

.field private j:LX/I0v;

.field private k:LX/1nQ;

.field private l:I

.field private m:Z

.field private n:I

.field private o:Z

.field private p:Ljava/lang/String;

.field public q:Z

.field public r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private w:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public x:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:LX/HzH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/I0f;LX/I0o;LX/I0v;LX/1Ck;LX/1nQ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2527548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2527549
    new-instance v0, LX/I0i;

    invoke-direct {v0, p0}, LX/I0i;-><init>(LX/I0m;)V

    iput-object v0, p0, LX/I0m;->a:LX/0Vd;

    .line 2527550
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I0m;->s:Z

    .line 2527551
    iput-object p1, p0, LX/I0m;->b:Landroid/content/Context;

    .line 2527552
    iput-object p2, p0, LX/I0m;->i:LX/I0f;

    .line 2527553
    iput-object p3, p0, LX/I0m;->g:LX/I0o;

    .line 2527554
    iput-object p4, p0, LX/I0m;->j:LX/I0v;

    .line 2527555
    iput-object p5, p0, LX/I0m;->h:LX/1Ck;

    .line 2527556
    iput-object p6, p0, LX/I0m;->k:LX/1nQ;

    .line 2527557
    return-void
.end method

.method private static a(LX/I0m;Z)LX/I0t;
    .locals 12

    .prologue
    .line 2527781
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    .line 2527782
    new-instance v11, LX/I0s;

    invoke-direct {v11}, LX/I0s;-><init>()V

    .line 2527783
    iget-boolean v6, v0, LX/I0v;->h:Z

    .line 2527784
    iput-boolean v6, v11, LX/I0s;->b:Z

    .line 2527785
    iget-object v6, v0, LX/I0v;->a:LX/I0r;

    iget-object v7, v0, LX/I0v;->m:Ljava/util/HashMap;

    iget-wide v8, v0, LX/I0v;->f:J

    .line 2527786
    iget-boolean v10, v0, LX/I0v;->k:Z

    if-eqz v10, :cond_5

    iget-boolean v10, v0, LX/I0v;->j:Z

    if-eqz v10, :cond_5

    .line 2527787
    sget-object v10, LX/I0u;->LOADING_PAST_AND_FUTURE:LX/I0u;

    .line 2527788
    :goto_0
    move-object v10, v10

    .line 2527789
    invoke-virtual/range {v6 .. v11}, LX/I0r;->a(Ljava/util/HashMap;JLX/I0u;LX/I0s;)V

    .line 2527790
    new-instance v6, LX/I0t;

    iget-object v7, v11, LX/I0s;->a:LX/0Px;

    iget-boolean v8, v11, LX/I0s;->b:Z

    iget v9, v11, LX/I0s;->c:I

    invoke-direct {v6, v7, v8, v9}, LX/I0t;-><init>(LX/0Px;ZI)V

    move-object v6, v6

    .line 2527791
    move-object v1, v6

    .line 2527792
    iget v0, v1, LX/I0t;->c:I

    iput v0, p0, LX/I0m;->l:I

    .line 2527793
    iget v0, p0, LX/I0m;->l:I

    iget-object v2, v1, LX/I0t;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 2527794
    iget-object v0, v1, LX/I0t;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/I0m;->l:I

    .line 2527795
    :cond_0
    iget-object v0, p0, LX/I0m;->f:LX/HzV;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2527796
    :goto_1
    return-object v0

    .line 2527797
    :cond_1
    iget-object v0, p0, LX/I0m;->f:LX/HzV;

    invoke-virtual {v0}, LX/HzV;->a()I

    move-result v2

    .line 2527798
    iget-object v0, p0, LX/I0m;->f:LX/HzV;

    .line 2527799
    iget-object v3, v0, LX/HzV;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->i:LX/1P0;

    invoke-virtual {v0}, LX/HzV;->a()I

    move-result v4

    invoke-virtual {v3, v4}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v3

    .line 2527800
    if-nez v3, :cond_8

    const/4 v3, 0x0

    :goto_2
    move v3, v3

    .line 2527801
    iget-object v0, p0, LX/I0m;->i:LX/I0f;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    .line 2527802
    iget-object v4, p0, LX/I0m;->i:LX/I0f;

    iget-object v5, v1, LX/I0t;->a:LX/0Px;

    .line 2527803
    iput-object v5, v4, LX/I0f;->b:LX/0Px;

    .line 2527804
    invoke-virtual {v4}, LX/1OM;->notifyDataSetChanged()V

    .line 2527805
    iget-boolean v4, p0, LX/I0m;->m:Z

    if-nez v4, :cond_2

    if-gez v2, :cond_3

    .line 2527806
    :cond_2
    iget-object v0, p0, LX/I0m;->f:LX/HzV;

    iget v2, p0, LX/I0m;->l:I

    .line 2527807
    iget-object v3, v0, LX/HzV;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->g(II)V

    .line 2527808
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/I0m;->m:Z

    :goto_3
    move-object v0, v1

    .line 2527809
    goto :goto_1

    .line 2527810
    :cond_3
    iget-object v4, p0, LX/I0m;->f:LX/HzV;

    if-eqz p1, :cond_4

    iget-object v5, p0, LX/I0m;->i:LX/I0f;

    invoke-virtual {v5}, LX/1OM;->ij_()I

    move-result v5

    sub-int v0, v5, v0

    :cond_4
    add-int/2addr v0, v2

    .line 2527811
    iget-object v2, v4, LX/HzV;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->g(II)V

    .line 2527812
    goto :goto_3

    .line 2527813
    :cond_5
    iget-boolean v10, v0, LX/I0v;->j:Z

    if-eqz v10, :cond_6

    .line 2527814
    sget-object v10, LX/I0u;->LOADING_FUTURE:LX/I0u;

    goto/16 :goto_0

    .line 2527815
    :cond_6
    iget-boolean v10, v0, LX/I0v;->k:Z

    if-eqz v10, :cond_7

    .line 2527816
    sget-object v10, LX/I0u;->LOADING_PAST:LX/I0u;

    goto/16 :goto_0

    .line 2527817
    :cond_7
    sget-object v10, LX/I0u;->LOADING_COMPLETE:LX/I0u;

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    goto :goto_2
.end method

.method public static a(LX/I0m;LX/0Pz;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2527772
    iget-object v0, p0, LX/I0m;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 2527773
    :cond_0
    :goto_0
    return-void

    .line 2527774
    :cond_1
    iget-object v0, p0, LX/I0m;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2527775
    iget-object v0, p0, LX/I0m;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2527776
    invoke-static {p0, p1, v0}, LX/I0m;->a$redex0(LX/I0m;LX/0Pz;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2527777
    iput-object v4, p0, LX/I0m;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2527778
    :cond_2
    iget-object v0, p0, LX/I0m;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2527779
    iget-object v0, p0, LX/I0m;->h:LX/1Ck;

    sget-object v1, LX/I0p;->FETCH_CALENDARABLE_ITEMS:LX/I0p;

    iget-object v2, p0, LX/I0m;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I0l;

    invoke-direct {v3, p0, p1}, LX/I0l;-><init>(LX/I0m;LX/0Pz;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2527780
    iput-object v4, p0, LX/I0m;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private a(ZI)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2527747
    if-eqz p1, :cond_0

    .line 2527748
    add-int/lit8 v0, p2, 0x6

    iget-object p1, p0, LX/I0m;->i:LX/I0f;

    invoke-virtual {p1}, LX/1OM;->ij_()I

    move-result p1

    if-le v0, p1, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2527749
    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/I0m;->q:Z

    if-eqz v0, :cond_1

    .line 2527750
    :cond_0
    :goto_1
    return-void

    .line 2527751
    :cond_1
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    .line 2527752
    iget p1, v0, LX/I0v;->e:I

    iget-object p2, v0, LX/I0v;->l:LX/0Px;

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p2

    if-lt p1, p2, :cond_5

    const/4 p1, 0x1

    :goto_2
    move v0, p1

    .line 2527753
    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/I0m;->o:Z

    if-eqz v0, :cond_2

    .line 2527754
    invoke-static {p0}, LX/I0m;->e(LX/I0m;)V

    .line 2527755
    invoke-static {p0}, LX/I0m;->g(LX/I0m;)V

    goto :goto_1

    .line 2527756
    :cond_2
    iput-boolean v1, p0, LX/I0m;->q:Z

    .line 2527757
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    .line 2527758
    iput-boolean v1, v0, LX/I0v;->j:Z

    .line 2527759
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    .line 2527760
    invoke-static {v0}, LX/I0v;->e(LX/I0v;)V

    .line 2527761
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/I0v;->j:Z

    .line 2527762
    iput-boolean v2, p0, LX/I0m;->q:Z

    .line 2527763
    invoke-static {p0, v2}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    move-result-object v0

    .line 2527764
    iget-boolean v0, v0, LX/I0t;->b:Z

    if-eqz v0, :cond_0

    .line 2527765
    iget-boolean v0, p0, LX/I0m;->o:Z

    if-eqz v0, :cond_3

    .line 2527766
    invoke-static {p0}, LX/I0m;->e(LX/I0m;)V

    .line 2527767
    invoke-static {p0}, LX/I0m;->g(LX/I0m;)V

    goto :goto_1

    .line 2527768
    :cond_3
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    .line 2527769
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2527770
    invoke-virtual {v0, v1}, LX/I0v;->b(LX/0Px;)V

    .line 2527771
    invoke-static {p0, v2}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 p1, 0x0

    goto :goto_2
.end method

.method public static a$redex0(LX/I0m;LX/0Pz;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2527728
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2527729
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v3

    .line 2527730
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2527731
    :cond_0
    :goto_0
    return-void

    .line 2527732
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2527733
    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2527734
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2527735
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;->b()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;

    move-result-object v0

    .line 2527736
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2527737
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/I0m;->b(LX/I0m;Ljava/lang/String;)V

    .line 2527738
    invoke-static {p0, p1}, LX/I0m;->a(LX/I0m;LX/0Pz;)V

    goto :goto_0

    .line 2527739
    :cond_3
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2527740
    iget-object v1, p0, LX/I0m;->g:LX/I0o;

    invoke-virtual {v1, v0, p2}, LX/I0o;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2527741
    iget-object v1, p0, LX/I0m;->j:LX/I0v;

    iget-object v3, p0, LX/I0m;->d:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 2527742
    invoke-static {v1, v4, v5, v0}, LX/I0v;->d(LX/I0v;JLX/0Px;)V

    .line 2527743
    const/4 v3, 0x0

    iput-boolean v3, v1, LX/I0v;->k:Z

    .line 2527744
    iget-object v0, p0, LX/I0m;->d:Ljava/util/Calendar;

    const/4 v1, 0x6

    sget v3, LX/I0h;->a:I

    neg-int v3, v3

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->add(II)V

    .line 2527745
    iput-boolean v2, p0, LX/I0m;->r:Z

    .line 2527746
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    goto :goto_0
.end method

.method public static a$redex0(LX/I0m;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2527711
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2527712
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    .line 2527713
    if-nez v0, :cond_0

    .line 2527714
    :goto_0
    return-void

    .line 2527715
    :cond_0
    iget-object v1, p0, LX/I0m;->j:LX/I0v;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v0

    .line 2527716
    iget-object v2, v1, LX/I0v;->a:LX/I0r;

    iget-object v3, v1, LX/I0v;->m:Ljava/util/HashMap;

    const/4 v9, 0x0

    .line 2527717
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2527718
    :cond_1
    :goto_1
    iget-wide v2, v1, LX/I0v;->g:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 2527719
    iget-object v2, v1, LX/I0v;->a:LX/I0r;

    invoke-virtual {v2}, LX/I0r;->a()Ljava/util/Calendar;

    move-result-object v2

    .line 2527720
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, v1, LX/I0v;->g:J

    .line 2527721
    :cond_2
    invoke-static {v1}, LX/I0v;->e(LX/I0v;)V

    .line 2527722
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    goto :goto_0

    .line 2527723
    :cond_3
    invoke-virtual {v2}, LX/I0r;->a()Ljava/util/Calendar;

    move-result-object v6

    .line 2527724
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v2, v3, v6, v7}, LX/I0r;->a(Ljava/util/HashMap;J)Ljava/util/List;

    move-result-object v7

    .line 2527725
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/I0X;

    iget-object v6, v6, LX/I0X;->a:LX/I0Y;

    sget-object v8, LX/I0Y;->PRIVATE_INVITES_HSCROLL:LX/I0Y;

    if-ne v6, v8, :cond_4

    .line 2527726
    new-instance v6, LX/I0X;

    sget-object v8, LX/I0Y;->PRIVATE_INVITES_HSCROLL:LX/I0Y;

    invoke-direct {v6, v8, v0}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {v7, v9, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2527727
    :cond_4
    new-instance v6, LX/I0X;

    sget-object v8, LX/I0Y;->PRIVATE_INVITES_HSCROLL:LX/I0Y;

    invoke-direct {v6, v8, v0}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {v7, v9, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/I0m;
    .locals 13

    .prologue
    .line 2527700
    new-instance v0, LX/I0m;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 2527701
    new-instance v4, LX/I0f;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-direct {v4, v2, v3}, LX/I0f;-><init>(Landroid/content/Context;LX/0hB;)V

    .line 2527702
    move-object v2, v4

    .line 2527703
    check-cast v2, LX/I0f;

    .line 2527704
    new-instance v7, LX/I0o;

    const-class v8, Landroid/content/Context;

    invoke-interface {p0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v10

    check-cast v10, LX/1My;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v11

    check-cast v11, LX/0hB;

    const/16 v12, 0x1619

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct/range {v7 .. v12}, LX/I0o;-><init>(Landroid/content/Context;LX/0tX;LX/1My;LX/0hB;LX/0Or;)V

    .line 2527705
    move-object v3, v7

    .line 2527706
    check-cast v3, LX/I0o;

    .line 2527707
    new-instance v7, LX/I0v;

    invoke-static {p0}, LX/I0r;->b(LX/0QB;)LX/I0r;

    move-result-object v4

    check-cast v4, LX/I0r;

    invoke-static {p0}, LX/I0g;->b(LX/0QB;)LX/I0g;

    move-result-object v5

    check-cast v5, LX/I0g;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v6

    check-cast v6, LX/11R;

    const/16 v8, 0x161a

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct {v7, v4, v5, v6, v8}, LX/I0v;-><init>(LX/I0r;LX/I0g;LX/11R;LX/0Or;)V

    .line 2527708
    move-object v4, v7

    .line 2527709
    check-cast v4, LX/I0v;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v6

    check-cast v6, LX/1nQ;

    invoke-direct/range {v0 .. v6}, LX/I0m;-><init>(Landroid/content/Context;LX/I0f;LX/I0o;LX/I0v;LX/1Ck;LX/1nQ;)V

    .line 2527710
    return-object v0
.end method

.method public static b(LX/I0m;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 2527693
    iput-boolean v8, p0, LX/I0m;->r:Z

    .line 2527694
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    .line 2527695
    iput-boolean v8, v0, LX/I0v;->k:Z

    .line 2527696
    iget-object v1, p0, LX/I0m;->g:LX/I0o;

    const/4 v3, 0x0

    iget-object v0, p0, LX/I0m;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/16 v6, 0xe

    iget v7, p0, LX/I0m;->n:I

    move-object v2, p1

    invoke-virtual/range {v1 .. v7}, LX/I0o;->a(Ljava/lang/String;ZJII)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/I0m;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2527697
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2527698
    invoke-static {p0, v8}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    .line 2527699
    :cond_0
    return-void
.end method

.method public static b$redex0(LX/I0m;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 2527667
    iput-boolean v2, p0, LX/I0m;->q:Z

    .line 2527668
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2527669
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v3

    .line 2527670
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2527671
    :cond_0
    :goto_0
    return-void

    .line 2527672
    :cond_1
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2527673
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_2

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2527674
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2527675
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2527676
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2527677
    iget-object v1, p0, LX/I0m;->g:LX/I0o;

    invoke-virtual {v1, v0, p1}, LX/I0o;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2527678
    iget-boolean v1, p0, LX/I0m;->s:Z

    if-eqz v1, :cond_3

    .line 2527679
    iget-object v1, p0, LX/I0m;->j:LX/I0v;

    iget-object v4, p0, LX/I0m;->c:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5, v0}, LX/I0v;->a(JLX/0Px;)V

    .line 2527680
    iget-object v0, p0, LX/I0m;->y:LX/HzH;

    sget-object v1, LX/Hx7;->CALENDAR:LX/Hx7;

    invoke-virtual {v0, v1}, LX/HzH;->a(LX/Hx7;)V

    .line 2527681
    iput-boolean v2, p0, LX/I0m;->s:Z

    .line 2527682
    invoke-static {p0, v2}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    .line 2527683
    invoke-static {p0, v7}, LX/I0m;->b(LX/I0m;Ljava/lang/String;)V

    .line 2527684
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-static {p0, v0}, LX/I0m;->a(LX/I0m;LX/0Pz;)V

    .line 2527685
    :goto_2
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;->b()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;

    move-result-object v0

    .line 2527686
    if-eqz v0, :cond_4

    .line 2527687
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->b()Z

    move-result v1

    iput-boolean v1, p0, LX/I0m;->o:Z

    .line 2527688
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/I0m;->p:Ljava/lang/String;

    goto :goto_0

    .line 2527689
    :cond_3
    iget-object v1, p0, LX/I0m;->j:LX/I0v;

    invoke-virtual {v1, v0}, LX/I0v;->b(LX/0Px;)V

    .line 2527690
    invoke-static {p0, v2}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    goto :goto_2

    .line 2527691
    :cond_4
    iput-boolean v2, p0, LX/I0m;->o:Z

    .line 2527692
    iput-object v7, p0, LX/I0m;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method private static e(LX/I0m;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    .line 2527659
    iput-boolean v3, p0, LX/I0m;->q:Z

    .line 2527660
    iget-object v1, p0, LX/I0m;->j:LX/I0v;

    iget-boolean v0, p0, LX/I0m;->s:Z

    if-nez v0, :cond_0

    move v0, v3

    .line 2527661
    :goto_0
    iput-boolean v0, v1, LX/I0v;->j:Z

    .line 2527662
    iget-object v1, p0, LX/I0m;->g:LX/I0o;

    iget-boolean v0, p0, LX/I0m;->o:Z

    if-eqz v0, :cond_1

    iget-object v2, p0, LX/I0m;->p:Ljava/lang/String;

    :goto_1
    iget-object v0, p0, LX/I0m;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/16 v6, 0xe

    iget v7, p0, LX/I0m;->n:I

    invoke-virtual/range {v1 .. v7}, LX/I0o;->a(Ljava/lang/String;ZJII)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/I0m;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2527663
    invoke-static {p0, v8}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    .line 2527664
    return-void

    :cond_0
    move v0, v8

    .line 2527665
    goto :goto_0

    .line 2527666
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static g(LX/I0m;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2527650
    iget-object v0, p0, LX/I0m;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_1

    .line 2527651
    :cond_0
    :goto_0
    return-void

    .line 2527652
    :cond_1
    iget-object v0, p0, LX/I0m;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2527653
    iget-object v0, p0, LX/I0m;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2527654
    invoke-static {p0, v0}, LX/I0m;->b$redex0(LX/I0m;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2527655
    iput-object v4, p0, LX/I0m;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2527656
    :cond_2
    iget-object v0, p0, LX/I0m;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2527657
    iget-object v0, p0, LX/I0m;->h:LX/1Ck;

    sget-object v1, LX/I0p;->FETCH_CALENDARABLE_ITEMS:LX/I0p;

    iget-object v2, p0, LX/I0m;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I0k;

    invoke-direct {v3, p0}, LX/I0k;-><init>(LX/I0m;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2527658
    iput-object v4, p0, LX/I0m;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1OM;
    .locals 1

    .prologue
    .line 2527818
    iget-object v0, p0, LX/I0m;->i:LX/I0f;

    return-object v0
.end method

.method public final a(LX/HzH;)V
    .locals 0

    .prologue
    .line 2527648
    iput-object p1, p0, LX/I0m;->y:LX/HzH;

    .line 2527649
    return-void
.end method

.method public final a(LX/HzI;)V
    .locals 0

    .prologue
    .line 2527647
    return-void
.end method

.method public final a(Lcom/facebook/events/common/EventAnalyticsParams;LX/HzV;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2527600
    iput-object p1, p0, LX/I0m;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527601
    iput-object p2, p0, LX/I0m;->f:LX/HzV;

    .line 2527602
    iget-object v0, p0, LX/I0m;->i:LX/I0f;

    iget-object v1, p0, LX/I0m;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527603
    iput-object v1, v0, LX/I0f;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527604
    iget-object v0, p0, LX/I0m;->i:LX/I0f;

    .line 2527605
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2527606
    iput-object v1, v0, LX/I0f;->b:LX/0Px;

    .line 2527607
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    const/4 v6, 0x0

    .line 2527608
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2527609
    iput-object v4, v0, LX/I0v;->l:LX/0Px;

    .line 2527610
    iget-object v4, v0, LX/I0v;->m:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 2527611
    iget-object v4, v0, LX/I0v;->n:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 2527612
    iput v6, v0, LX/I0v;->e:I

    .line 2527613
    const-wide/16 v4, 0x0

    iput-wide v4, v0, LX/I0v;->g:J

    .line 2527614
    iput-boolean v6, v0, LX/I0v;->i:Z

    .line 2527615
    iput-boolean v6, v0, LX/I0v;->k:Z

    .line 2527616
    iput-boolean v6, v0, LX/I0v;->j:Z

    .line 2527617
    const/4 v0, 0x0

    iput-object v0, p0, LX/I0m;->p:Ljava/lang/String;

    .line 2527618
    iput-boolean v2, p0, LX/I0m;->o:Z

    .line 2527619
    iput-boolean v2, p0, LX/I0m;->q:Z

    .line 2527620
    iput-boolean v2, p0, LX/I0m;->r:Z

    .line 2527621
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I0m;->s:Z

    .line 2527622
    iput-boolean v2, p0, LX/I0m;->m:Z

    .line 2527623
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2527624
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2527625
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2527626
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2527627
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2527628
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, LX/I0m;->c:Ljava/util/Calendar;

    .line 2527629
    iget-object v1, p0, LX/I0m;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2527630
    const/4 v1, 0x6

    sget v2, LX/I0h;->a:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 2527631
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, LX/I0m;->d:Ljava/util/Calendar;

    .line 2527632
    iget-object v1, p0, LX/I0m;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2527633
    iget-object v0, p0, LX/I0m;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/I0m;->n:I

    .line 2527634
    invoke-static {p0}, LX/I0m;->e(LX/I0m;)V

    .line 2527635
    iget-object v0, p0, LX/I0m;->g:LX/I0o;

    iget v1, p0, LX/I0m;->n:I

    const/4 p1, 0x0

    const/4 v6, 0x1

    .line 2527636
    new-instance v2, LX/7oS;

    invoke-direct {v2}, LX/7oS;-><init>()V

    const-string v3, "profile_image_size"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "cover_image_portrait_size"

    iget-object v4, v0, LX/I0o;->f:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "cover_image_landscape_size"

    iget-object v4, v0, LX/I0o;->f:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "low_res_size"

    iget-object v4, v0, LX/I0o;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b15c8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "first_count"

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "event_state"

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "PUBLISHED"

    aput-object v5, v4, p1

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v2

    const-string v3, "filter"

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "INVITED"

    aput-object v5, v4, p1

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v2

    const-string v3, "event_ent_types"

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "PRIVATE_EVENT"

    aput-object v5, v4, p1

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v2

    .line 2527637
    invoke-static {}, LX/7oV;->g()LX/7oS;

    move-result-object v3

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2527638
    iget-object v4, v2, LX/0gW;->e:LX/0w7;

    move-object v2, v4

    .line 2527639
    invoke-virtual {v3, v2}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->d:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2527640
    iput-boolean v6, v2, LX/0zO;->p:Z

    .line 2527641
    move-object v2, v2

    .line 2527642
    iget-object v3, v0, LX/I0o;->d:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    move-object v0, v2

    .line 2527643
    iput-object v0, p0, LX/I0m;->x:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2527644
    iget-object v0, p0, LX/I0m;->g:LX/I0o;

    iget-object v1, p0, LX/I0m;->a:LX/0Vd;

    .line 2527645
    iput-object v1, v0, LX/I0o;->b:LX/0TF;

    .line 2527646
    return-void
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V
    .locals 1

    .prologue
    .line 2527596
    if-eqz p1, :cond_0

    .line 2527597
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    invoke-virtual {v0, p1}, LX/I0v;->a(LX/7oa;)V

    .line 2527598
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    .line 2527599
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2527592
    if-nez p1, :cond_0

    .line 2527593
    :goto_0
    return-void

    .line 2527594
    :cond_0
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    invoke-virtual {v0, p1}, LX/I0v;->a(Ljava/lang/String;)V

    .line 2527595
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    goto :goto_0
.end method

.method public final a(ZII)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2527577
    iget-boolean v0, p0, LX/I0m;->s:Z

    if-eqz v0, :cond_0

    .line 2527578
    :goto_0
    return-void

    .line 2527579
    :cond_0
    iget-boolean v0, p0, LX/I0m;->u:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    iget v0, p0, LX/I0m;->l:I

    if-lt p2, v0, :cond_1

    .line 2527580
    iget-object v0, p0, LX/I0m;->k:LX/1nQ;

    iget-object v1, p0, LX/I0m;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/I0m;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_DOWN_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/1nQ;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2527581
    iput-boolean v4, p0, LX/I0m;->u:Z

    .line 2527582
    :cond_1
    iget-boolean v0, p0, LX/I0m;->t:Z

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    iget v0, p0, LX/I0m;->l:I

    if-gt p2, v0, :cond_2

    .line 2527583
    iget-object v0, p0, LX/I0m;->k:LX/1nQ;

    iget-object v1, p0, LX/I0m;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/I0m;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_UP_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/1nQ;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2527584
    iput-boolean v4, p0, LX/I0m;->t:Z

    .line 2527585
    :cond_2
    invoke-direct {p0, p1, p3}, LX/I0m;->a(ZI)V

    .line 2527586
    if-nez p1, :cond_3

    .line 2527587
    add-int/lit8 v0, p2, -0x6

    if-gez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2527588
    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/I0m;->r:Z

    if-eqz v0, :cond_4

    .line 2527589
    :cond_3
    :goto_2
    goto :goto_0

    .line 2527590
    :cond_4
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/I0m;->b(LX/I0m;Ljava/lang/String;)V

    .line 2527591
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-static {p0, v0}, LX/I0m;->a(LX/I0m;LX/0Pz;)V

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2527566
    const/4 v4, 0x0

    .line 2527567
    iget-object v0, p0, LX/I0m;->x:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_1

    .line 2527568
    :cond_0
    :goto_0
    invoke-static {p0}, LX/I0m;->g(LX/I0m;)V

    .line 2527569
    return-void

    .line 2527570
    :cond_1
    iget-object v0, p0, LX/I0m;->x:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2527571
    iget-object v0, p0, LX/I0m;->x:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2527572
    invoke-static {p0, v0}, LX/I0m;->a$redex0(LX/I0m;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2527573
    iput-object v4, p0, LX/I0m;->x:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2527574
    :cond_2
    iget-object v0, p0, LX/I0m;->x:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2527575
    iget-object v0, p0, LX/I0m;->h:LX/1Ck;

    sget-object v1, LX/I0p;->FETCH_PRIVATE_UNCONNECTED_UPCOMING_EVENT:LX/I0p;

    iget-object v2, p0, LX/I0m;->x:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I0j;

    invoke-direct {v3, p0}, LX/I0j;-><init>(LX/I0m;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2527576
    iput-object v4, p0, LX/I0m;->x:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V
    .locals 1

    .prologue
    .line 2527562
    if-eqz p1, :cond_0

    .line 2527563
    iget-object v0, p0, LX/I0m;->j:LX/I0v;

    invoke-virtual {v0, p1}, LX/I0v;->a(LX/7oa;)V

    .line 2527564
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/I0m;->a(LX/I0m;Z)LX/I0t;

    .line 2527565
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2527559
    const/4 v0, 0x0

    iput-object v0, p0, LX/I0m;->f:LX/HzV;

    .line 2527560
    iget-object v0, p0, LX/I0m;->h:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2527561
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2527558
    return-void
.end method
