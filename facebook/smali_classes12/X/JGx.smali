.class public final LX/JGx;
.super Landroid/text/style/ReplacementSpan;
.source ""

# interfaces
.implements LX/JGQ;
.implements LX/JGR;


# static fields
.field private static final b:Landroid/graphics/RectF;


# instance fields
.field public c:LX/JGz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/JGr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:F

.field public f:F

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2669571
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, LX/JGx;->b:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x7fc00000    # NaNf

    .line 2669572
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/JGx;-><init>(LX/JGz;FF)V

    .line 2669573
    return-void
.end method

.method public constructor <init>(LX/JGz;FF)V
    .locals 0
    .param p1    # LX/JGz;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2669523
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 2669524
    iput-object p1, p0, LX/JGx;->c:LX/JGz;

    .line 2669525
    iput p2, p0, LX/JGx;->e:F

    .line 2669526
    iput p3, p0, LX/JGx;->f:F

    .line 2669527
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2669536
    iget-object v0, p0, LX/JGx;->c:LX/JGz;

    if-eqz v0, :cond_0

    .line 2669537
    iget-object v0, p0, LX/JGx;->c:LX/JGz;

    const/4 v2, 0x0

    .line 2669538
    iget v1, v0, LX/JGz;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/JGz;->e:I

    .line 2669539
    iget v1, v0, LX/JGz;->e:I

    if-eqz v1, :cond_1

    .line 2669540
    :goto_0
    iget-object v0, p0, LX/JGx;->c:LX/JGz;

    .line 2669541
    iget v1, v0, LX/JGz;->e:I

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2669542
    if-eqz v0, :cond_0

    .line 2669543
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGx;->d:LX/JGr;

    .line 2669544
    :cond_0
    return-void

    .line 2669545
    :cond_1
    iget-object v1, v0, LX/JGz;->c:LX/1ca;

    if-eqz v1, :cond_2

    .line 2669546
    iget-object v1, v0, LX/JGz;->c:LX/1ca;

    invoke-interface {v1}, LX/1ca;->g()Z

    .line 2669547
    iput-object v2, v0, LX/JGz;->c:LX/1ca;

    .line 2669548
    :cond_2
    iget-object v1, v0, LX/JGz;->d:LX/1FJ;

    if-eqz v1, :cond_3

    .line 2669549
    iget-object v1, v0, LX/JGz;->d:LX/1FJ;

    invoke-virtual {v1}, LX/1FJ;->close()V

    .line 2669550
    iput-object v2, v0, LX/JGz;->d:LX/1FJ;

    .line 2669551
    :cond_3
    iput-object v2, v0, LX/JGz;->b:LX/JGR;

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/JGr;)V
    .locals 3

    .prologue
    .line 2669552
    iput-object p1, p0, LX/JGx;->d:LX/JGr;

    .line 2669553
    iget-object v0, p0, LX/JGx;->c:LX/JGz;

    if-eqz v0, :cond_0

    .line 2669554
    iget-object v0, p0, LX/JGx;->c:LX/JGz;

    const/4 p1, 0x0

    const/4 v2, 0x1

    .line 2669555
    iput-object p0, v0, LX/JGz;->b:LX/JGR;

    .line 2669556
    iget v1, v0, LX/JGz;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/JGz;->e:I

    .line 2669557
    iget v1, v0, LX/JGz;->e:I

    if-eq v1, v2, :cond_1

    .line 2669558
    invoke-virtual {v0}, LX/JGz;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2669559
    if-eqz v1, :cond_0

    .line 2669560
    invoke-interface {p0}, LX/JGR;->b()V

    .line 2669561
    :cond_0
    :goto_0
    return-void

    .line 2669562
    :cond_1
    iget-object v1, v0, LX/JGz;->c:LX/1ca;

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-static {v1}, LX/0nE;->a(Z)V

    .line 2669563
    iget-object v1, v0, LX/JGz;->d:LX/1FJ;

    if-nez v1, :cond_3

    :goto_2
    invoke-static {v2}, LX/0nE;->a(Z)V

    .line 2669564
    invoke-static {}, LX/1FE;->a()LX/1FE;

    move-result-object v1

    invoke-virtual {v1}, LX/1FE;->h()LX/1HI;

    move-result-object v1

    .line 2669565
    iget-object v2, v0, LX/JGz;->a:LX/1bf;

    .line 2669566
    sget-object p1, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->d:Ljava/lang/Object;

    move-object p1, p1

    .line 2669567
    invoke-virtual {v1, v2, p1}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    iput-object v1, v0, LX/JGz;->c:LX/1ca;

    .line 2669568
    iget-object v1, v0, LX/JGz;->c:LX/1ca;

    invoke-static {}, LX/1by;->b()LX/1by;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    :cond_2
    move v1, p1

    .line 2669569
    goto :goto_1

    :cond_3
    move v2, p1

    .line 2669570
    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2669532
    iget-object v0, p0, LX/JGx;->d:LX/JGr;

    .line 2669533
    move-object v0, v0

    .line 2669534
    check-cast v0, LX/JGr;

    invoke-virtual {v0}, LX/JGr;->a()V

    .line 2669535
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2669528
    iget-object v0, p0, LX/JGx;->d:LX/JGr;

    .line 2669529
    move-object v0, v0

    .line 2669530
    check-cast v0, LX/JGr;

    invoke-virtual {v0}, LX/JGr;->a()V

    .line 2669531
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 5

    .prologue
    .line 2669516
    iget-object v0, p0, LX/JGx;->c:LX/JGz;

    if-nez v0, :cond_1

    .line 2669517
    :cond_0
    :goto_0
    return-void

    .line 2669518
    :cond_1
    iget-object v0, p0, LX/JGx;->c:LX/JGz;

    invoke-virtual {v0}, LX/JGz;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2669519
    if-eqz v0, :cond_0

    .line 2669520
    int-to-float v1, p8

    invoke-virtual {p9}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 2669521
    sget-object v2, LX/JGx;->b:Landroid/graphics/RectF;

    iget v3, p0, LX/JGx;->f:F

    sub-float v3, v1, v3

    iget v4, p0, LX/JGx;->e:F

    add-float/2addr v4, p5

    invoke-virtual {v2, p5, v3, v4, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2669522
    const/4 v1, 0x0

    sget-object v2, LX/JGx;->b:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, v1, v2, p9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2669510
    if-eqz p5, :cond_0

    .line 2669511
    iget v0, p0, LX/JGx;->f:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    neg-int v0, v0

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 2669512
    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 2669513
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 2669514
    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 2669515
    :cond_0
    iget v0, p0, LX/JGx;->e:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method
