.class public LX/IuC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DoP;
.implements LX/DoR;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/Object;


# instance fields
.field private final b:LX/0SF;

.field private final c:LX/IuE;

.field private final d:LX/IuE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2625756
    const-class v0, LX/IuC;

    sput-object v0, LX/IuC;->a:Ljava/lang/Class;

    .line 2625757
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/IuC;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/DoY;LX/2Ox;LX/0SF;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625752
    iput-object p3, p0, LX/IuC;->b:LX/0SF;

    .line 2625753
    new-instance v0, LX/IuE;

    const-string v1, "pre_keys"

    invoke-direct {v0, p1, p2, v1}, LX/IuE;-><init>(LX/DoY;LX/2Ox;Ljava/lang/String;)V

    iput-object v0, p0, LX/IuC;->c:LX/IuE;

    .line 2625754
    new-instance v0, LX/IuE;

    const-string v1, "signed_pre_keys"

    invoke-direct {v0, p1, p2, v1}, LX/IuE;-><init>(LX/DoY;LX/2Ox;Ljava/lang/String;)V

    iput-object v0, p0, LX/IuC;->d:LX/IuE;

    .line 2625755
    return-void
.end method

.method public static a(LX/0QB;)LX/IuC;
    .locals 9

    .prologue
    .line 2625722
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2625723
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2625724
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2625725
    if-nez v1, :cond_0

    .line 2625726
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2625727
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2625728
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2625729
    sget-object v1, LX/IuC;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2625730
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2625731
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2625732
    :cond_1
    if-nez v1, :cond_4

    .line 2625733
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2625734
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2625735
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2625736
    new-instance p0, LX/IuC;

    invoke-static {v0}, LX/DoY;->a(LX/0QB;)LX/DoY;

    move-result-object v1

    check-cast v1, LX/DoY;

    invoke-static {v0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v7

    check-cast v7, LX/2Ox;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SF;

    invoke-direct {p0, v1, v7, v8}, LX/IuC;-><init>(LX/DoY;LX/2Ox;LX/0SF;)V

    .line 2625737
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2625738
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2625739
    if-nez v1, :cond_2

    .line 2625740
    sget-object v0, LX/IuC;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuC;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2625741
    :goto_1
    if-eqz v0, :cond_3

    .line 2625742
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2625743
    :goto_3
    check-cast v0, LX/IuC;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2625744
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2625745
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2625746
    :catchall_1
    move-exception v0

    .line 2625747
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2625748
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2625749
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2625750
    :cond_2
    :try_start_8
    sget-object v0, LX/IuC;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuC;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)",
            "Ljava/util/List",
            "<",
            "LX/Ebk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2625715
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2625716
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 2625717
    :try_start_0
    new-instance v3, LX/Ebk;

    invoke-direct {v3, v0}, LX/Ebk;-><init>([B)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2625718
    :catch_0
    move-exception v0

    .line 2625719
    sget-object v1, LX/IuC;->a:Ljava/lang/Class;

    const-string v2, "Error retrieving all pre-keys from store"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2625720
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2625721
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final declared-synchronized a(I)LX/Ebg;
    .locals 3

    .prologue
    .line 2625707
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IuC;->c:LX/IuE;

    invoke-virtual {v0, p1}, LX/IuE;->a(I)[B

    move-result-object v0

    .line 2625708
    if-nez v0, :cond_0

    .line 2625709
    new-instance v0, LX/Eah;

    const-string v1, "Pre-key not found in store"

    invoke-direct {v0, v1}, LX/Eah;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625710
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2625711
    :cond_0
    :try_start_1
    new-instance v1, LX/Ebg;

    invoke-direct {v1, v0}, LX/Ebg;-><init>([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1

    .line 2625712
    :catch_0
    move-exception v0

    .line 2625713
    sget-object v1, LX/IuC;->a:Ljava/lang/Class;

    const-string v2, "Error retrieving pre-key from store"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2625714
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final declared-synchronized a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/Ebk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2625758
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IuC;->d:LX/IuE;

    .line 2625759
    iget-object v1, v0, LX/IuE;->a:LX/DoY;

    iget-object v2, v0, LX/IuE;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/DoY;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 2625760
    invoke-static {v0}, LX/IuC;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 2625761
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILX/Ebg;)V
    .locals 4

    .prologue
    .line 2625704
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IuC;->c:LX/IuE;

    invoke-virtual {p2}, LX/Ebg;->c()[B

    move-result-object v1

    iget-object v2, p0, LX/IuC;->b:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-virtual {v0, p1, v1, v2, v3}, LX/IuE;->a(I[BJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625705
    monitor-exit p0

    return-void

    .line 2625706
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILX/Ebk;)V
    .locals 4

    .prologue
    .line 2625701
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IuC;->d:LX/IuE;

    invoke-virtual {p2}, LX/Ebk;->d()[B

    move-result-object v1

    iget-object v2, p0, LX/IuC;->b:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-virtual {v0, p1, v1, v2, v3}, LX/IuE;->a(I[BJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625702
    monitor-exit p0

    return-void

    .line 2625703
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/Ebg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2625696
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/IuB;

    invoke-direct {v0, p0}, LX/IuB;-><init>(LX/IuC;)V

    invoke-static {p1, v0}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v0

    .line 2625697
    iget-object v1, p0, LX/IuC;->c:LX/IuE;

    iget-object v2, p0, LX/IuC;->b:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    .line 2625698
    iget-object v4, v1, LX/IuE;->b:LX/2Ox;

    iget-object p1, v1, LX/IuE;->c:Ljava/lang/String;

    invoke-virtual {v4, p1, v0, v2, v3}, LX/2Ox;->a(Ljava/lang/String;Ljava/util/Collection;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625699
    monitor-exit p0

    return-void

    .line 2625700
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 3

    .prologue
    .line 2625692
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IuC;->c:LX/IuE;

    .line 2625693
    iget-object v1, v0, LX/IuE;->b:LX/2Ox;

    iget-object v2, v0, LX/IuE;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, LX/2Ox;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625694
    monitor-exit p0

    return-void

    .line 2625695
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(I)LX/Ebk;
    .locals 3

    .prologue
    .line 2625684
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IuC;->d:LX/IuE;

    invoke-virtual {v0, p1}, LX/IuE;->a(I)[B

    move-result-object v0

    .line 2625685
    if-nez v0, :cond_0

    .line 2625686
    new-instance v0, LX/Eah;

    const-string v1, "Signed pre-key not found in store"

    invoke-direct {v0, v1}, LX/Eah;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625687
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2625688
    :cond_0
    :try_start_1
    new-instance v1, LX/Ebk;

    invoke-direct {v1, v0}, LX/Ebk;-><init>([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1

    .line 2625689
    :catch_0
    move-exception v0

    .line 2625690
    sget-object v1, LX/IuC;->a:Ljava/lang/Class;

    const-string v2, "Error retrieving signed pre-key from store"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2625691
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
