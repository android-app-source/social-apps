.class public final LX/J2U;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 2641433
    const/4 v5, 0x0

    .line 2641434
    const-wide/16 v6, 0x0

    .line 2641435
    const/4 v4, 0x0

    .line 2641436
    const-wide/16 v2, 0x0

    .line 2641437
    const/4 v1, 0x0

    .line 2641438
    const/4 v0, 0x0

    .line 2641439
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 2641440
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2641441
    const/4 v0, 0x0

    .line 2641442
    :goto_0
    return v0

    .line 2641443
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_6

    .line 2641444
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 2641445
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2641446
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 2641447
    const-string v4, "__type__"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "__typename"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2641448
    :cond_1
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 2641449
    :cond_2
    const-string v4, "creation_time"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2641450
    const/4 v0, 0x1

    .line 2641451
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 2641452
    :cond_3
    const-string v4, "id"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2641453
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 2641454
    :cond_4
    const-string v4, "updated_time"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2641455
    const/4 v0, 0x1

    .line 2641456
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 2641457
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2641458
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2641459
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2641460
    if-eqz v1, :cond_7

    .line 2641461
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2641462
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2641463
    if-eqz v6, :cond_8

    .line 2641464
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2641465
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move-wide v8, v2

    move v10, v5

    move-wide v2, v6

    move v7, v4

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 2641466
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2641467
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2641468
    if-eqz v0, :cond_0

    .line 2641469
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641470
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2641471
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2641472
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 2641473
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641474
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2641475
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2641476
    if-eqz v0, :cond_2

    .line 2641477
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641478
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2641479
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2641480
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 2641481
    const-string v2, "updated_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641482
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2641483
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2641484
    return-void
.end method
