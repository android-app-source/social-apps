.class public final LX/IJl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IJn;


# direct methods
.method public constructor <init>(LX/IJn;)V
    .locals 0

    .prologue
    .line 2564177
    iput-object p1, p0, LX/IJl;->a:LX/IJn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564178
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v5, 0x4b895c84    # 1.8004232E7f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2564179
    if-eqz p1, :cond_0

    .line 2564180
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564181
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2564182
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2564183
    :goto_2
    return-object v0

    .line 2564184
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564185
    check-cast v0, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2564186
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2564187
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564188
    check-cast v0, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2564189
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2564190
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2564191
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564192
    check-cast v0, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2564193
    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2564194
    :goto_4
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2564195
    new-instance v6, Landroid/location/Address;

    iget-object v7, p0, LX/IJl;->a:LX/IJn;

    iget-object v7, v7, LX/IJn;->d:Ljava/util/Locale;

    invoke-direct {v6, v7}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 2564196
    invoke-virtual {v5, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    .line 2564197
    invoke-virtual {v5, v4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    .line 2564198
    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 2564199
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    .line 2564200
    :cond_7
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_2
.end method
