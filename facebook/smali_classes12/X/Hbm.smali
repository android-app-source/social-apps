.class public final LX/Hbm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

.field private b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

.field private c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;


# direct methods
.method public constructor <init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;)V
    .locals 0

    .prologue
    .line 2486209
    iput-object p1, p0, LX/Hbm;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2486210
    iput-object p2, p0, LX/Hbm;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486211
    iput-object p3, p0, LX/Hbm;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486212
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2486213
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2486208
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2486185
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2486186
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2486187
    sget-object v4, LX/HaS;->NULL:LX/HaS;

    .line 2486188
    :goto_0
    move-object v0, v4

    .line 2486189
    iget-object v1, p0, LX/Hbm;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    invoke-static {v1}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->n(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2486190
    iget-object v1, p0, LX/Hbm;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    iget-object v1, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->j:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-virtual {v1}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b()V

    .line 2486191
    :goto_1
    iget-object v1, p0, LX/Hbm;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486192
    iget-object v2, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f:LX/HaS;

    move-object v1, v2

    .line 2486193
    sget-object v2, LX/HaS;->NULL:LX/HaS;

    if-eq v1, v2, :cond_0

    .line 2486194
    iget-object v1, p0, LX/Hbm;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    sget-object v2, LX/HaS;->NULL:LX/HaS;

    invoke-virtual {v1, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordStrength(LX/HaS;)V

    .line 2486195
    iget-object v1, p0, LX/Hbm;->c:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v1, v3}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordsMatch(Z)V

    .line 2486196
    :cond_0
    iget-object v1, p0, LX/Hbm;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v1, v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordStrength(LX/HaS;)V

    .line 2486197
    iget-object v0, p0, LX/Hbm;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0, v3}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setPasswordsMatch(Z)V

    .line 2486198
    return-void

    .line 2486199
    :cond_1
    iget-object v1, p0, LX/Hbm;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    iget-object v1, v1, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->j:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-virtual {v1}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->c()V

    goto :goto_1

    .line 2486200
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x6

    if-ge v4, v5, :cond_3

    .line 2486201
    sget-object v4, LX/HaS;->NULL:LX/HaS;

    goto :goto_0

    .line 2486202
    :cond_3
    invoke-static {v0}, LX/HaR;->a(Ljava/lang/String;)D

    move-result-wide v4

    .line 2486203
    const-wide v6, 0x4041800000000000L    # 35.0

    cmpg-double v6, v4, v6

    if-gez v6, :cond_4

    .line 2486204
    sget-object v4, LX/HaS;->WEAK:LX/HaS;

    goto :goto_0

    .line 2486205
    :cond_4
    const-wide/high16 v6, 0x404a000000000000L    # 52.0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_5

    .line 2486206
    sget-object v4, LX/HaS;->OK:LX/HaS;

    goto :goto_0

    .line 2486207
    :cond_5
    sget-object v4, LX/HaS;->STRONG:LX/HaS;

    goto :goto_0
.end method
