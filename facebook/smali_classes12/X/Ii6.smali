.class public LX/Ii6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final d:Landroid/content/ContentResolver;

.field public final e:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

.field public final f:LX/IgO;

.field public g:LX/6eS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2603366
    const-class v0, LX/Ii6;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ii6;->a:Ljava/lang/String;

    .line 2603367
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "_display_name"

    aput-object v1, v0, v5

    const-string v1, "mime_type"

    aput-object v1, v0, v6

    const-string v1, "datetaken"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "orientation"

    aput-object v2, v0, v1

    sput-object v0, LX/Ii6;->b:[Ljava/lang/String;

    .line 2603368
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "_display_name"

    aput-object v1, v0, v5

    const-string v1, "mime_type"

    aput-object v1, v0, v6

    const-string v1, "datetaken"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    sput-object v0, LX/Ii6;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/IgO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2603361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2603362
    iput-object p1, p0, LX/Ii6;->d:Landroid/content/ContentResolver;

    .line 2603363
    iput-object p2, p0, LX/Ii6;->e:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 2603364
    iput-object p3, p0, LX/Ii6;->f:LX/IgO;

    .line 2603365
    return-void
.end method

.method private static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2603360
    const v0, 0x7fffffff

    if-ge p0, v0, :cond_0

    const-string v0, " LIMIT %d"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2603354
    if-nez p0, :cond_0

    .line 2603355
    const/4 v0, 0x0

    .line 2603356
    :goto_0
    return-object v0

    .line 2603357
    :cond_0
    if-eqz p1, :cond_1

    .line 2603358
    const-string v0, "bucket_id = ? "

    goto :goto_0

    .line 2603359
    :cond_1
    const-string v0, "bucket_id = ? "

    goto :goto_0
.end method

.method public static a(LX/Ii6;Landroid/os/Bundle;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2603332
    const-string v0, "mediaLimit"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 2603333
    const-string v0, "folderId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2603334
    iget-object v0, p0, LX/Ii6;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/Ii6;->b:[Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v4, v3}, LX/Ii6;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, LX/Ii6;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "date_modified DESC"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, LX/Ii6;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v1, 0x0

    .line 2603335
    if-nez v2, :cond_1

    .line 2603336
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2603337
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 2603338
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2603339
    :cond_2
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2603340
    invoke-static {p0, v2}, LX/Ii6;->c(LX/Ii6;Landroid/database/Cursor;)LX/5zn;

    move-result-object v3

    .line 2603341
    if-nez v3, :cond_6

    .line 2603342
    const/4 v3, 0x0

    .line 2603343
    :goto_2
    move-object v3, v3

    .line 2603344
    if-eqz v3, :cond_2

    .line 2603345
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 2603346
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2603347
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_3
    if-eqz v2, :cond_3

    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_4
    throw v0

    :cond_4
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :cond_6
    const/4 v4, 0x5

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, LX/47e;->a(I)LX/47d;

    move-result-object v4

    .line 2603348
    iput-object v4, v3, LX/5zn;->l:LX/47d;

    .line 2603349
    move-object v3, v3

    .line 2603350
    sget-object v4, LX/2MK;->PHOTO:LX/2MK;

    .line 2603351
    iput-object v4, v3, LX/5zn;->c:LX/2MK;

    .line 2603352
    move-object v3, v3

    .line 2603353
    invoke-virtual {v3}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v3

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 2603331
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Ii6;
    .locals 5

    .prologue
    .line 2603324
    new-instance v3, LX/Ii6;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    invoke-static {p0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 2603325
    new-instance v4, LX/IgO;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v2

    check-cast v2, Landroid/content/ContentResolver;

    invoke-direct {v4, v2}, LX/IgO;-><init>(Landroid/content/ContentResolver;)V

    .line 2603326
    move-object v2, v4

    .line 2603327
    check-cast v2, LX/IgO;

    invoke-direct {v3, v0, v1, v2}, LX/Ii6;-><init>(Landroid/content/ContentResolver;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/IgO;)V

    .line 2603328
    invoke-static {p0}, LX/6eS;->b(LX/0QB;)LX/6eS;

    move-result-object v0

    check-cast v0, LX/6eS;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    .line 2603329
    iput-object v0, v3, LX/Ii6;->g:LX/6eS;

    iput-object v1, v3, LX/Ii6;->h:LX/03V;

    iput-object v2, v3, LX/Ii6;->i:LX/0ad;

    .line 2603330
    return-object v3
.end method

.method public static b(LX/Ii6;Landroid/os/Bundle;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2603301
    const-string v0, "mediaLimit"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 2603302
    const-string v0, "folderId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2603303
    iget-object v0, p0, LX/Ii6;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/Ii6;->c:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v4, v3}, LX/Ii6;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, LX/Ii6;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "date_modified DESC"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, LX/Ii6;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v1, 0x0

    .line 2603304
    if-nez v2, :cond_1

    .line 2603305
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2603306
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 2603307
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2603308
    :cond_2
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2603309
    invoke-static {p0, v2}, LX/Ii6;->c(LX/Ii6;Landroid/database/Cursor;)LX/5zn;

    move-result-object v9

    .line 2603310
    if-nez v9, :cond_6

    .line 2603311
    const/4 v9, 0x0

    .line 2603312
    :goto_2
    move-object v3, v9

    .line 2603313
    if-eqz v3, :cond_2

    .line 2603314
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 2603315
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2603316
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_3
    if-eqz v2, :cond_3

    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_4
    throw v0

    :cond_4
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 2603317
    :cond_6
    const/4 v10, 0x5

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 2603318
    sget-object v10, LX/2MK;->VIDEO:LX/2MK;

    .line 2603319
    iput-object v10, v9, LX/5zn;->c:LX/2MK;

    .line 2603320
    move-object v9, v9

    .line 2603321
    iput-wide v11, v9, LX/5zn;->i:J

    .line 2603322
    move-object v9, v9

    .line 2603323
    invoke-virtual {v9}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v9

    goto :goto_2
.end method

.method public static c(LX/Ii6;Landroid/database/Cursor;)LX/5zn;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2603280
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2603281
    if-nez v1, :cond_1

    .line 2603282
    :cond_0
    :goto_0
    return-object v0

    .line 2603283
    :cond_1
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2603284
    if-eqz v1, :cond_0

    .line 2603285
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2603286
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2603287
    if-eqz v0, :cond_2

    const-string v4, "*/"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2603288
    :cond_2
    iget-object v0, p0, LX/Ii6;->e:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 2603289
    :cond_3
    move-object v0, v0

    .line 2603290
    const/4 v4, 0x4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 2603291
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v6

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2603292
    iput-object v1, v6, LX/5zn;->b:Landroid/net/Uri;

    .line 2603293
    move-object v1, v6

    .line 2603294
    iput-object v0, v1, LX/5zn;->q:Ljava/lang/String;

    .line 2603295
    move-object v0, v1

    .line 2603296
    iput-wide v4, v0, LX/5zn;->B:J

    .line 2603297
    move-object v0, v0

    .line 2603298
    iput-wide v2, v0, LX/5zn;->h:J

    .line 2603299
    move-object v0, v0

    .line 2603300
    goto :goto_0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 2603241
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2603242
    const-string v1, "load_local_media"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2603243
    const/4 v8, 0x0

    .line 2603244
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2603245
    const-string v1, "hideVideos"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 2603246
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2603247
    const-string v2, "mediaLimit"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 2603248
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2603249
    invoke-static {p0, v1}, LX/Ii6;->a(LX/Ii6;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v1

    .line 2603250
    if-nez v0, :cond_0

    .line 2603251
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2603252
    invoke-static {p0, v0}, LX/Ii6;->b(LX/Ii6;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2603253
    :cond_0
    iget-object v0, p0, LX/Ii6;->i:LX/0ad;

    sget-short v3, LX/Ii4;->a:S

    invoke-interface {v0, v3, v8}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2603254
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2603255
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2603256
    iget-object v4, p0, LX/Ii6;->g:LX/6eS;

    invoke-virtual {v4, v0}, LX/6eS;->c(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6eR;

    move-result-object v4

    .line 2603257
    sget-object v5, LX/6eR;->VALID:LX/6eR;

    if-eq v4, v5, :cond_1

    .line 2603258
    iget-object v5, p0, LX/Ii6;->h:LX/03V;

    sget-object v6, LX/Ii6;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " local media file: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2603259
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2603260
    :cond_2
    sget-object v0, Lcom/facebook/ui/media/attachments/MediaResource;->a:Ljava/util/Comparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2603261
    if-lez v2, :cond_6

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_6

    .line 2603262
    invoke-interface {v1, v8, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 2603263
    :goto_1
    new-instance v1, Lcom/facebook/messaging/media/service/LocalMediaLoadResult;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/messaging/media/service/LocalMediaLoadResult;-><init>(Ljava/util/List;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2603264
    :goto_2
    return-object v0

    .line 2603265
    :cond_3
    const-string v1, "load_local_folders"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2603266
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2603267
    sget-object v1, Lcom/facebook/messaging/media/folder/LoadFolderParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/folder/LoadFolderParams;

    .line 2603268
    iget-object v1, p0, LX/Ii6;->f:LX/IgO;

    .line 2603269
    iput-object v0, v1, LX/IgO;->c:Lcom/facebook/messaging/media/folder/LoadFolderParams;

    .line 2603270
    new-instance p0, LX/IgL;

    invoke-direct {p0}, LX/IgL;-><init>()V

    invoke-static {v1, p0}, LX/IgO;->a(LX/IgO;LX/IgK;)V

    .line 2603271
    iget-object p0, v1, LX/IgO;->c:Lcom/facebook/messaging/media/folder/LoadFolderParams;

    iget-boolean p0, p0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->c:Z

    if-nez p0, :cond_4

    .line 2603272
    new-instance p0, LX/IgQ;

    invoke-direct {p0}, LX/IgQ;-><init>()V

    invoke-static {v1, p0}, LX/IgO;->a(LX/IgO;LX/IgK;)V

    .line 2603273
    :cond_4
    new-instance p0, Ljava/util/ArrayList;

    iget-object v0, v1, LX/IgO;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2603274
    new-instance v0, LX/IgJ;

    invoke-direct {v0}, LX/IgJ;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2603275
    invoke-static {p0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p0

    move-object p0, p0

    .line 2603276
    move-object v0, p0

    .line 2603277
    new-instance v1, Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;-><init>(Ljava/util/List;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2603278
    goto :goto_2

    .line 2603279
    :cond_5
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported operation type"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method
