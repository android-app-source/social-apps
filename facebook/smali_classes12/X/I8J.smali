.class public LX/I8J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AhN;


# instance fields
.field public a:Lcom/facebook/events/model/Event;

.field public b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/events/common/EventAnalyticsParams;

.field public d:LX/I7i;

.field public e:LX/DBC;

.field public f:LX/I81;

.field public final g:LX/DBA;

.field public final h:LX/I7w;

.field public final i:LX/I7z;

.field public final j:LX/I7n;

.field public final k:LX/DBE;

.field public final l:LX/DBH;

.field public final m:LX/I82;

.field private final n:LX/1nQ;

.field private final o:LX/Bie;

.field private final p:LX/6RZ;

.field public final q:LX/0wM;

.field private final r:LX/01T;

.field public final s:LX/B9y;

.field private final t:Landroid/view/View;

.field public final u:Lcom/facebook/content/SecureContextHelper;

.field private final v:LX/1Sa;

.field public final w:Z


# direct methods
.method public constructor <init>(Landroid/view/View;LX/DBA;LX/I7w;LX/I7z;LX/I7n;LX/DBE;LX/DBH;LX/I82;LX/1nQ;LX/Bie;LX/6RZ;LX/0wM;LX/01T;Lcom/facebook/content/SecureContextHelper;LX/B9y;LX/1Sa;Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2540920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540921
    iput-object p2, p0, LX/I8J;->g:LX/DBA;

    .line 2540922
    iput-object p3, p0, LX/I8J;->h:LX/I7w;

    .line 2540923
    iput-object p4, p0, LX/I8J;->i:LX/I7z;

    .line 2540924
    iput-object p5, p0, LX/I8J;->j:LX/I7n;

    .line 2540925
    iput-object p6, p0, LX/I8J;->k:LX/DBE;

    .line 2540926
    iput-object p7, p0, LX/I8J;->l:LX/DBH;

    .line 2540927
    iput-object p9, p0, LX/I8J;->n:LX/1nQ;

    .line 2540928
    iput-object p8, p0, LX/I8J;->m:LX/I82;

    .line 2540929
    iput-object p10, p0, LX/I8J;->o:LX/Bie;

    .line 2540930
    iput-object p11, p0, LX/I8J;->p:LX/6RZ;

    .line 2540931
    iput-object p12, p0, LX/I8J;->q:LX/0wM;

    .line 2540932
    iput-object p13, p0, LX/I8J;->r:LX/01T;

    .line 2540933
    move-object/from16 v0, p15

    iput-object v0, p0, LX/I8J;->s:LX/B9y;

    .line 2540934
    iput-object p1, p0, LX/I8J;->t:Landroid/view/View;

    .line 2540935
    move-object/from16 v0, p14

    iput-object v0, p0, LX/I8J;->u:Lcom/facebook/content/SecureContextHelper;

    .line 2540936
    move-object/from16 v0, p16

    iput-object v0, p0, LX/I8J;->v:LX/1Sa;

    .line 2540937
    invoke-virtual/range {p17 .. p17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/I8J;->w:Z

    .line 2540938
    return-void
.end method

.method public static c(LX/I8J;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2540806
    iget-object v0, p0, LX/I8J;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2540807
    iget-object v0, p0, LX/I8J;->r:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->SAVE:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2540808
    iget-object v1, v0, Lcom/facebook/events/model/Event;->F:Ljava/lang/String;

    move-object v0, v1

    .line 2540809
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2540810
    iget-object v1, v0, Lcom/facebook/events/model/Event;->G:Ljava/lang/String;

    move-object v0, v1

    .line 2540811
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 2540812
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-static {v0}, LX/I83;->fromOrdinal(I)LX/I83;

    move-result-object v0

    .line 2540813
    if-eqz v0, :cond_0

    .line 2540814
    sget-object v1, LX/I8I;->a:[I

    invoke-virtual {v0}, LX/I83;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2540815
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2540816
    :pswitch_0
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v3, p0, LX/I8J;->t:Landroid/view/View;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v0 .. v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;Z)V

    goto :goto_0

    .line 2540817
    :pswitch_1
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v3, p0, LX/I8J;->t:Landroid/view/View;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v0 .. v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;Z)V

    goto :goto_0

    .line 2540818
    :pswitch_2
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v3, p0, LX/I8J;->t:Landroid/view/View;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v0 .. v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;Z)V

    goto :goto_0

    .line 2540819
    :pswitch_3
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v4, p0, LX/I8J;->t:Landroid/view/View;

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    .line 2540820
    :pswitch_4
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    .line 2540821
    :pswitch_5
    iget-object v3, p0, LX/I8J;->h:LX/I7w;

    iget-object v4, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v7, p0, LX/I8J;->t:Landroid/view/View;

    sget-object v8, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    .line 2540822
    :pswitch_6
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    .line 2540823
    :pswitch_7
    iget-object v3, p0, LX/I8J;->h:LX/I7w;

    iget-object v4, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v7, p0, LX/I8J;->t:Landroid/view/View;

    sget-object v8, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    .line 2540824
    :pswitch_8
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    .line 2540825
    :pswitch_9
    iget-object v3, p0, LX/I8J;->h:LX/I7w;

    iget-object v4, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v7, p0, LX/I8J;->t:Landroid/view/View;

    sget-object v8, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540826
    :pswitch_a
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540827
    :pswitch_b
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540828
    :pswitch_c
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540829
    :pswitch_d
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;Z)V

    goto/16 :goto_0

    .line 2540830
    :pswitch_e
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;Z)V

    goto/16 :goto_0

    .line 2540831
    :pswitch_f
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;Z)V

    goto/16 :goto_0

    .line 2540832
    :pswitch_10
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v3, p0, LX/I8J;->t:Landroid/view/View;

    invoke-virtual {v0, v1, v2, v2, v3}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Landroid/view/View;Landroid/view/View;)V

    goto/16 :goto_0

    .line 2540833
    :pswitch_11
    iget-object v0, p0, LX/I8J;->n:LX/1nQ;

    iget-object v1, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2540834
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2540835
    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2540836
    iget-object v3, v0, LX/1nQ;->i:LX/0Zb;

    const-string v4, "event_album_click"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2540837
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2540838
    const-string v4, "mechanism"

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "event_permalink"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    iget-object v4, v0, LX/1nQ;->j:LX/0kv;

    iget-object v5, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v4, v5}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "Event"

    invoke-virtual {v3, v4}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2540839
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2540840
    iget-object v1, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2540841
    iget-object v2, v1, Lcom/facebook/events/model/Event;->aa:Ljava/lang/String;

    move-object v1, v2

    .line 2540842
    if-eqz v1, :cond_2

    .line 2540843
    sget-object v1, LX/0ax;->bs:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2540844
    iget-object v5, v4, Lcom/facebook/events/model/Event;->aa:Ljava/lang/String;

    move-object v4, v5

    .line 2540845
    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2540846
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2540847
    iget-object v1, p0, LX/I8J;->u:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2540848
    :cond_2
    goto/16 :goto_0

    .line 2540849
    :pswitch_12
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v1, v2}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0

    .line 2540850
    :pswitch_13
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v1, v2}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0

    .line 2540851
    :pswitch_14
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v1, v2}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0

    .line 2540852
    :pswitch_15
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v1, v2}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0

    .line 2540853
    :pswitch_16
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iget-object v4, p0, LX/I8J;->t:Landroid/view/View;

    invoke-virtual {v0, v1, v3, v2, v4}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Landroid/view/View;Landroid/view/View;)V

    goto/16 :goto_0

    .line 2540854
    :pswitch_17
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v1, v2}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0

    .line 2540855
    :pswitch_18
    iget-object v0, p0, LX/I8J;->h:LX/I7w;

    iget-object v1, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iget-object v4, p0, LX/I8J;->t:Landroid/view/View;

    invoke-virtual {v0, v1, v3, v2, v4}, LX/I7w;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Landroid/view/View;Landroid/view/View;)V

    goto/16 :goto_0

    .line 2540856
    :pswitch_19
    iget-object v0, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_4

    .line 2540857
    :goto_1
    goto/16 :goto_0

    .line 2540858
    :pswitch_1a
    iget-object v0, p0, LX/I8J;->i:LX/I7z;

    .line 2540859
    sget-object v1, LX/5vL;->ADD:LX/5vL;

    invoke-static {v0, v1}, LX/I7z;->a(LX/I7z;LX/5vL;)V

    .line 2540860
    iget-object v0, p0, LX/I8J;->v:LX/1Sa;

    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Sa;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2540861
    :pswitch_1b
    iget-object v0, p0, LX/I8J;->i:LX/I7z;

    .line 2540862
    sget-object v1, LX/5vL;->REMOVE:LX/5vL;

    invoke-static {v0, v1}, LX/I7z;->a(LX/I7z;LX/5vL;)V

    .line 2540863
    goto/16 :goto_0

    .line 2540864
    :pswitch_1c
    new-instance v1, LX/4mb;

    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/4mb;-><init>(Landroid/content/Context;)V

    .line 2540865
    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v2

    iget-boolean v0, p0, LX/I8J;->w:Z

    if-eqz v0, :cond_9

    const v0, 0x7f081f51

    :goto_2
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, LX/I8C;

    invoke-direct {v2, p0}, LX/I8C;-><init>(LX/I8J;)V

    invoke-virtual {v1, v0, v2}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2540866
    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f081f56

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, LX/I8D;

    invoke-direct {v2, p0}, LX/I8D;-><init>(LX/I8J;)V

    invoke-virtual {v1, v0, v2}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2540867
    invoke-virtual {v1}, LX/4mb;->show()Landroid/app/AlertDialog;

    .line 2540868
    goto/16 :goto_0

    .line 2540869
    :pswitch_1d
    iget-object v0, p0, LX/I8J;->d:LX/I7i;

    invoke-virtual {v0}, LX/I7i;->e()V

    goto/16 :goto_0

    .line 2540870
    :pswitch_1e
    iget-object v0, p0, LX/I8J;->d:LX/I7i;

    invoke-virtual {v0}, LX/I7i;->a()V

    goto/16 :goto_0

    .line 2540871
    :pswitch_1f
    invoke-virtual {p0}, LX/I8J;->b()V

    goto/16 :goto_0

    .line 2540872
    :pswitch_20
    const v6, -0x958e80

    .line 2540873
    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v0

    .line 2540874
    new-instance v1, LX/3Af;

    invoke-direct {v1, v0}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2540875
    new-instance v2, LX/7TY;

    invoke-direct {v2, v0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2540876
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2540877
    const v3, 0x7f081f18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    iget-object v4, p0, LX/I8J;->q:LX/0wM;

    const v5, 0x7f02088f

    invoke-virtual {v4, v5, v6}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v3

    new-instance v4, LX/I8A;

    invoke-direct {v4, p0}, LX/I8A;-><init>(LX/I8J;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2540878
    const v3, 0x7f081f19

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    iget-object v3, p0, LX/I8J;->q:LX/0wM;

    const v4, 0x7f0209c4

    invoke-virtual {v3, v4, v6}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v3, LX/I8B;

    invoke-direct {v3, p0}, LX/I8B;-><init>(LX/I8J;)V

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2540879
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2540880
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2540881
    goto/16 :goto_0

    .line 2540882
    :pswitch_21
    iget-object v0, p0, LX/I8J;->g:LX/DBA;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    iget-object v3, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v3, :cond_3

    iget-object v2, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v2

    .line 2540883
    :cond_3
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, LX/DBA;->a(LX/DBA;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;ZZ)V

    .line 2540884
    goto/16 :goto_0

    .line 2540885
    :pswitch_22
    iget-object v0, p0, LX/I8J;->f:LX/I81;

    invoke-virtual {v0}, LX/I81;->b()V

    goto/16 :goto_0

    .line 2540886
    :pswitch_23
    iget-object v0, p0, LX/I8J;->f:LX/I81;

    invoke-virtual {v0}, LX/I81;->a()V

    goto/16 :goto_0

    .line 2540887
    :pswitch_24
    iget-object v0, p0, LX/I8J;->f:LX/I81;

    const-string v1, "ti"

    const-string v2, "cl"

    invoke-virtual {v0, v1, v2}, LX/I81;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2540888
    :pswitch_25
    iget-object v0, p0, LX/I8J;->f:LX/I81;

    invoke-virtual {v0}, LX/I81;->c()V

    goto/16 :goto_0

    .line 2540889
    :pswitch_26
    iget-object v0, p0, LX/I8J;->o:LX/Bie;

    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/I8J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/Bie;->a(Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540890
    :pswitch_27
    iget-object v0, p0, LX/I8J;->j:LX/I7n;

    invoke-virtual {v0}, LX/I7n;->a()V

    goto/16 :goto_0

    .line 2540891
    :pswitch_28
    iget-object v0, p0, LX/I8J;->o:LX/Bie;

    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    iget-object v3, p0, LX/I8J;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->at()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/I8J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->COPY_EVENT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v0 .. v5}, LX/Bie;->a(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540892
    :pswitch_29
    iget-object v0, p0, LX/I8J;->k:LX/DBE;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1}, LX/DBE;->a(Lcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540893
    :pswitch_2a
    iget-object v0, p0, LX/I8J;->l:LX/DBH;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1}, LX/DBH;->a(Lcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540894
    :pswitch_2b
    iget-object v0, p0, LX/I8J;->l:LX/DBH;

    const-wide/16 v2, 0x0

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v2, v3, v1}, LX/DBH;->a(JLcom/facebook/events/common/ActionMechanism;)V

    goto/16 :goto_0

    .line 2540895
    :cond_4
    invoke-static {p0}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v0

    .line 2540896
    new-instance v1, LX/3Af;

    invoke-direct {v1, v0}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2540897
    new-instance v2, LX/7TY;

    invoke-direct {v2, v0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2540898
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2540899
    iget-object v0, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    sget-object v4, LX/7vK;->INVITE:LX/7vK;

    invoke-virtual {v0, v4}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2540900
    iget-boolean v0, p0, LX/I8J;->w:Z

    if-eqz v0, :cond_8

    const v0, 0x7f081f53

    :goto_3
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    const v4, 0x7f020886

    invoke-virtual {v0, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2540901
    new-instance v4, LX/I8E;

    invoke-direct {v4, p0}, LX/I8E;-><init>(LX/I8J;)V

    move-object v4, v4

    .line 2540902
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2540903
    :cond_5
    iget-object v0, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    sget-object v4, LX/7vK;->SHARE:LX/7vK;

    invoke-virtual {v0, v4}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2540904
    const v0, 0x7f081f54

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    const v4, 0x7f02080f

    invoke-virtual {v0, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2540905
    new-instance v4, LX/I8F;

    invoke-direct {v4, p0}, LX/I8F;-><init>(LX/I8J;)V

    move-object v4, v4

    .line 2540906
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2540907
    :cond_6
    iget-object v0, p0, LX/I8J;->s:LX/B9y;

    invoke-virtual {v0}, LX/B9y;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2540908
    const v0, 0x7f081f55

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v3}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    invoke-static {}, LX/10A;->a()I

    move-result v4

    invoke-virtual {v0, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2540909
    new-instance v4, LX/I8G;

    invoke-direct {v4, p0}, LX/I8G;-><init>(LX/I8J;)V

    move-object v4, v4

    .line 2540910
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2540911
    :cond_7
    const v0, 0x7f081f5f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    const v3, 0x7f0209c3

    invoke-virtual {v0, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2540912
    new-instance v3, LX/I8H;

    invoke-direct {v3, p0}, LX/I8H;-><init>(LX/I8J;)V

    move-object v3, v3

    .line 2540913
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2540914
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2540915
    invoke-virtual {v1}, LX/3Af;->show()V

    goto/16 :goto_1

    .line 2540916
    :cond_8
    const v0, 0x7f081f52

    goto/16 :goto_3

    .line 2540917
    :cond_9
    const v0, 0x7f081f50

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1c
        :pswitch_1d
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2540918
    iget-object v0, p0, LX/I8J;->e:LX/DBC;

    iget-object v1, p0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/DBC;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;)V

    .line 2540919
    return-void
.end method
