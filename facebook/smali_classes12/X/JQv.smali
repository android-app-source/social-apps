.class public final LX/JQv;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JQx;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:LX/JQk;

.field public c:Z

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/JQx;


# direct methods
.method public constructor <init>(LX/JQx;)V
    .locals 1

    .prologue
    .line 2692385
    iput-object p1, p0, LX/JQv;->e:LX/JQx;

    .line 2692386
    move-object v0, p1

    .line 2692387
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2692388
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2692389
    const-string v0, "ActiveNowHScrollItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2692390
    if-ne p0, p1, :cond_1

    .line 2692391
    :cond_0
    :goto_0
    return v0

    .line 2692392
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2692393
    goto :goto_0

    .line 2692394
    :cond_3
    check-cast p1, LX/JQv;

    .line 2692395
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2692396
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2692397
    if-eq v2, v3, :cond_0

    .line 2692398
    iget v2, p0, LX/JQv;->a:I

    iget v3, p1, LX/JQv;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2692399
    goto :goto_0

    .line 2692400
    :cond_4
    iget-object v2, p0, LX/JQv;->b:LX/JQk;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/JQv;->b:LX/JQk;

    iget-object v3, p1, LX/JQv;->b:LX/JQk;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2692401
    goto :goto_0

    .line 2692402
    :cond_6
    iget-object v2, p1, LX/JQv;->b:LX/JQk;

    if-nez v2, :cond_5

    .line 2692403
    :cond_7
    iget-boolean v2, p0, LX/JQv;->c:Z

    iget-boolean v3, p1, LX/JQv;->c:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2692404
    goto :goto_0

    .line 2692405
    :cond_8
    iget-object v2, p0, LX/JQv;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/JQv;->d:Ljava/lang/String;

    iget-object v3, p1, LX/JQv;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2692406
    goto :goto_0

    .line 2692407
    :cond_9
    iget-object v2, p1, LX/JQv;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
