.class public final LX/JJi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/JJj;


# direct methods
.method public constructor <init>(LX/JJj;)V
    .locals 0

    .prologue
    .line 2679707
    iput-object p1, p0, LX/JJi;->a:LX/JJj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x220a37a9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2679708
    const-string v0, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JJi;->a:LX/JJj;

    iget-object v0, v0, LX/JJj;->c:Ljava/lang/String;

    const-string v1, "extra_request_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2679709
    :cond_0
    const/16 v0, 0x27

    const v1, -0x4f8aee

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2679710
    :goto_0
    return-void

    .line 2679711
    :cond_1
    const-string v0, "graphql_story"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2679712
    const-string v1, "extra_result"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object v1

    .line 2679713
    new-instance v3, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v3}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2679714
    const-string v4, "isSuccess"

    sget-object v5, LX/7m7;->SUCCESS:LX/7m7;

    if-ne v1, v5, :cond_5

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v4, v1}, Lcom/facebook/react/bridge/WritableNativeMap;->putBoolean(Ljava/lang/String;Z)V

    .line 2679715
    if-eqz v0, :cond_2

    .line 2679716
    const-string v1, "storyId"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lcom/facebook/react/bridge/WritableNativeMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679717
    const-string v1, "storyCacheId"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/facebook/react/bridge/WritableNativeMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679718
    :cond_2
    iget-object v0, p0, LX/JJi;->a:LX/JJj;

    iget-object v0, v0, LX/JJj;->d:LX/5pY;

    if-eqz v0, :cond_3

    .line 2679719
    iget-object v0, p0, LX/JJi;->a:LX/JJj;

    iget-object v0, v0, LX/JJj;->d:LX/5pY;

    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v1, "onCommerceComposerPostResult"

    invoke-interface {v0, v1, v3}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2679720
    :cond_3
    iget-object v0, p0, LX/JJi;->a:LX/JJj;

    .line 2679721
    iget-object v1, v0, LX/JJj;->b:LX/0Yb;

    if-eqz v1, :cond_4

    .line 2679722
    iget-object v1, v0, LX/JJj;->b:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2679723
    :cond_4
    const v0, -0x74ae35f0

    invoke-static {v0, v2}, LX/02F;->e(II)V

    goto :goto_0

    .line 2679724
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method
