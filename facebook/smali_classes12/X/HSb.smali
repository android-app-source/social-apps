.class public final LX/HSb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HSk;


# direct methods
.method public constructor <init>(LX/HSk;)V
    .locals 0

    .prologue
    .line 2467366
    iput-object p1, p0, LX/HSb;->a:LX/HSk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2467367
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2467368
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2467369
    if-nez p1, :cond_1

    .line 2467370
    :cond_0
    :goto_0
    return-void

    .line 2467371
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2467372
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2467373
    iget-object v1, p0, LX/HSb;->a:LX/HSk;

    iget-object v1, v1, LX/HSk;->e:LX/HSM;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2467374
    iget-object v1, p0, LX/HSb;->a:LX/HSk;

    iget-object v1, v1, LX/HSk;->e:LX/HSM;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/HSM;->a(Ljava/lang/String;Lcom/facebook/events/model/Event;)V

    goto :goto_0
.end method
