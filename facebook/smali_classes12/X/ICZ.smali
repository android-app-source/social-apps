.class public final LX/ICZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ICk;

.field public final synthetic b:LX/ICb;


# direct methods
.method public constructor <init>(LX/ICb;LX/ICk;)V
    .locals 0

    .prologue
    .line 2549556
    iput-object p1, p0, LX/ICZ;->b:LX/ICb;

    iput-object p2, p0, LX/ICZ;->a:LX/ICk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2549557
    iget-object v0, p0, LX/ICZ;->b:LX/ICb;

    iget-object v0, v0, LX/ICb;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->c:LX/2dj;

    iget-object v1, p0, LX/ICZ;->b:LX/ICb;

    iget-object v1, v1, LX/ICb;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v1, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->g:Ljava/lang/String;

    iget-object v2, p0, LX/ICZ;->b:LX/ICb;

    iget-object v2, v2, LX/ICb;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v2, v2, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->n:Ljava/lang/String;

    iget-object v3, p0, LX/ICZ;->a:LX/ICk;

    invoke-virtual {v3}, LX/ICk;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/ICZ;->b:LX/ICb;

    iget-object v4, v4, LX/ICb;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v4, v4, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->m:Ljava/lang/String;

    .line 2549558
    new-instance v5, LX/85f;

    invoke-direct {v5}, LX/85f;-><init>()V

    move-object v5, v5

    .line 2549559
    new-instance v6, LX/4F3;

    invoke-direct {v6}, LX/4F3;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2549560
    const-string p0, "client_mutation_id"

    invoke-virtual {v6, p0, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549561
    move-object v6, v6

    .line 2549562
    const-string v7, "actor_id"

    invoke-virtual {v6, v7, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549563
    move-object v6, v6

    .line 2549564
    const-string v7, "newcomer_id"

    invoke-virtual {v6, v7, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549565
    move-object v6, v6

    .line 2549566
    const-string v7, "receiver_id"

    invoke-virtual {v6, v7, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549567
    move-object v6, v6

    .line 2549568
    const-string v7, "attempt_id"

    invoke-virtual {v6, v7, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549569
    move-object v6, v6

    .line 2549570
    const-string v7, "input"

    invoke-virtual {v5, v7, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2549571
    iget-object v6, v0, LX/2dj;->l:LX/2dm;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/2dm;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v6

    iget-object v7, v0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 2549572
    return-object v0
.end method
