.class public LX/Iz1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/P2pPaymentExtension;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Iyu;LX/Iyr;)V
    .locals 2
    .param p1    # LX/Iyu;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634072
    iget-object v0, p2, LX/Iyr;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/Iyr;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iys;

    iget-object v0, v0, LX/Iys;->b:LX/0Px;

    :goto_0
    move-object v0, v0

    .line 2634073
    iput-object v0, p0, LX/Iz1;->a:LX/0Px;

    .line 2634074
    return-void

    :cond_0
    iget-object v0, p2, LX/Iyr;->a:LX/0P1;

    sget-object v1, LX/Iyu;->DEFAULT:LX/Iyu;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iys;

    iget-object v0, v0, LX/Iys;->b:LX/0Px;

    goto :goto_0
.end method
