.class public LX/J4E;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field public final a:LX/1Ck;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Lcom/facebook/privacy/PrivacyOperationsClient;

.field public final d:LX/J5g;

.field public final e:LX/J4N;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2643894
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/J4E;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/1Ck;Ljava/util/concurrent/ExecutorService;Lcom/facebook/privacy/PrivacyOperationsClient;LX/J5g;LX/J4N;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2643887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643888
    iput-object p1, p0, LX/J4E;->a:LX/1Ck;

    .line 2643889
    iput-object p2, p0, LX/J4E;->b:Ljava/util/concurrent/ExecutorService;

    .line 2643890
    iput-object p3, p0, LX/J4E;->c:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 2643891
    iput-object p4, p0, LX/J4E;->d:LX/J5g;

    .line 2643892
    iput-object p5, p0, LX/J4E;->e:LX/J4N;

    .line 2643893
    return-void
.end method

.method public static a(LX/0QB;)LX/J4E;
    .locals 13

    .prologue
    .line 2643858
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2643859
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2643860
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2643861
    if-nez v1, :cond_0

    .line 2643862
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2643863
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2643864
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2643865
    sget-object v1, LX/J4E;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2643866
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2643867
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2643868
    :cond_1
    if-nez v1, :cond_4

    .line 2643869
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2643870
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2643871
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2643872
    new-instance v7, LX/J4E;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v10

    check-cast v10, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v0}, LX/J5g;->a(LX/0QB;)LX/J5g;

    move-result-object v11

    check-cast v11, LX/J5g;

    invoke-static {v0}, LX/J4N;->a(LX/0QB;)LX/J4N;

    move-result-object v12

    check-cast v12, LX/J4N;

    invoke-direct/range {v7 .. v12}, LX/J4E;-><init>(LX/1Ck;Ljava/util/concurrent/ExecutorService;Lcom/facebook/privacy/PrivacyOperationsClient;LX/J5g;LX/J4N;)V

    .line 2643873
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2643874
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2643875
    if-nez v1, :cond_2

    .line 2643876
    sget-object v0, LX/J4E;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J4E;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2643877
    :goto_1
    if-eqz v0, :cond_3

    .line 2643878
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2643879
    :goto_3
    check-cast v0, LX/J4E;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2643880
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2643881
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2643882
    :catchall_1
    move-exception v0

    .line 2643883
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2643884
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2643885
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2643886
    :cond_2
    :try_start_8
    sget-object v0, LX/J4E;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J4E;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 2643856
    iget-object v0, p0, LX/J4E;->a:LX/1Ck;

    sget-object v1, LX/J4D;->SEND_PRIVACY_EDITS:LX/J4D;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2643857
    return-void
.end method
