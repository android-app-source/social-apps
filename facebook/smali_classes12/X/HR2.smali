.class public final LX/HR2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;)V
    .locals 0

    .prologue
    .line 2464163
    iput-object p1, p0, LX/HR2;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2464164
    iget-object v0, p0, LX/HR2;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    iget-object v1, p0, LX/HR2;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    iget v1, v1, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setMaxLines(I)V

    .line 2464165
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2464166
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2464167
    iget-object v0, p0, LX/HR2;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    const/4 v1, 0x0

    .line 2464168
    iput-boolean v1, v0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    .line 2464169
    iget-object v0, p0, LX/HR2;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    iget-object v1, p0, LX/HR2;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setFadingGradient(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;I)V

    .line 2464170
    return-void
.end method
