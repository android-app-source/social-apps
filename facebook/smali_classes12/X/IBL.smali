.class public LX/IBL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/0kL;

.field public final c:LX/3iH;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/3iH;Ljava/util/concurrent/Executor;LX/0kL;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2546914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2546915
    iput-object p1, p0, LX/IBL;->c:LX/3iH;

    .line 2546916
    iput-object p2, p0, LX/IBL;->a:Ljava/util/concurrent/Executor;

    .line 2546917
    iput-object p3, p0, LX/IBL;->b:LX/0kL;

    .line 2546918
    return-void
.end method

.method public static a(LX/0QB;)LX/IBL;
    .locals 6

    .prologue
    .line 2546919
    const-class v1, LX/IBL;

    monitor-enter v1

    .line 2546920
    :try_start_0
    sget-object v0, LX/IBL;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2546921
    sput-object v2, LX/IBL;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2546922
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2546923
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2546924
    new-instance p0, LX/IBL;

    invoke-static {v0}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v3

    check-cast v3, LX/3iH;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-direct {p0, v3, v4, v5}, LX/IBL;-><init>(LX/3iH;Ljava/util/concurrent/Executor;LX/0kL;)V

    .line 2546925
    move-object v0, p0

    .line 2546926
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2546927
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IBL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2546928
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2546929
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;LX/0QK;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/model/Event;",
            "LX/0QK",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2546930
    iget-object v0, p1, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v0, v0

    .line 2546931
    if-eqz v0, :cond_1

    .line 2546932
    const/4 v0, 0x1

    move v0, v0

    .line 2546933
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2546934
    if-eqz v0, :cond_0

    .line 2546935
    iget-object v0, p0, LX/IBL;->c:LX/3iH;

    .line 2546936
    iget-object v1, p1, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v1, v1

    .line 2546937
    new-instance v2, LX/IBK;

    invoke-direct {v2, p0, p2}, LX/IBK;-><init>(LX/IBL;LX/0QK;)V

    iget-object v3, p0, LX/IBL;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1, v2, v3}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    .line 2546938
    :goto_1
    return-void

    .line 2546939
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
