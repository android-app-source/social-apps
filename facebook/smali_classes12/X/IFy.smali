.class public LX/IFy;
.super LX/0To;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0To",
        "<",
        "LX/IFy;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:LX/IFy;

.field public static final e:LX/IFy;

.field public static final f:LX/IFy;

.field public static final g:LX/IFy;

.field public static final h:LX/IFy;

.field public static final i:LX/IFy;

.field public static final j:LX/IFy;

.field public static final k:LX/IFy;

.field public static final l:LX/IFy;

.field public static final m:LX/IFy;

.field public static final n:LX/IFy;

.field public static final o:LX/IFy;

.field public static final p:LX/IFy;

.field public static final q:LX/IFy;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2555018
    new-instance v0, LX/IFy;

    const-string v1, "fetch_nearby_friends"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->d:LX/IFy;

    .line 2555019
    new-instance v0, LX/IFy;

    const-string v1, "fetch_nearby_friends_location_sharing"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->e:LX/IFy;

    .line 2555020
    new-instance v0, LX/IFy;

    const-string v1, "fetch_nearby_friends_contacts_tab"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->f:LX/IFy;

    .line 2555021
    new-instance v0, LX/IFy;

    const-string v1, "fetch_nearby_friends_highlight"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->g:LX/IFy;

    .line 2555022
    new-instance v0, LX/IFy;

    const-string v1, "fetch_location"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->h:LX/IFy;

    .line 2555023
    new-instance v0, LX/IFy;

    const-string v1, "fetch_more_in_section"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->i:LX/IFy;

    .line 2555024
    new-instance v0, LX/IFy;

    const-string v1, "pause"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->j:LX/IFy;

    .line 2555025
    new-instance v0, LX/IFy;

    const-string v1, "nearby_friends_search"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->k:LX/IFy;

    .line 2555026
    new-instance v0, LX/IFy;

    const-string v1, "save_location"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->l:LX/IFy;

    .line 2555027
    new-instance v0, LX/IFy;

    const-string v1, "turn_in"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->m:LX/IFy;

    .line 2555028
    new-instance v0, LX/IFy;

    const-string v1, "invite_"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->n:LX/IFy;

    .line 2555029
    new-instance v0, LX/IFy;

    const-string v1, "uninvite_"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->o:LX/IFy;

    .line 2555030
    new-instance v0, LX/IFy;

    const-string v1, "wrong_region_feedback"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->p:LX/IFy;

    .line 2555031
    new-instance v0, LX/IFy;

    const-string v1, "unknown_region_feedback"

    invoke-direct {v0, v1}, LX/IFy;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/IFy;->q:LX/IFy;

    return-void
.end method

.method private constructor <init>(LX/0To;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2555032
    invoke-direct {p0, p1, p2}, LX/0To;-><init>(LX/0To;Ljava/lang/String;)V

    .line 2555033
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2555034
    invoke-direct {p0, p1}, LX/0To;-><init>(Ljava/lang/String;)V

    .line 2555035
    return-void
.end method


# virtual methods
.method public final a(LX/0To;Ljava/lang/String;)LX/0To;
    .locals 1

    .prologue
    .line 2555036
    new-instance v0, LX/IFy;

    invoke-direct {v0, p1, p2}, LX/IFy;-><init>(LX/0To;Ljava/lang/String;)V

    return-object v0
.end method
