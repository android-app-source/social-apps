.class public LX/IHu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/IHu;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/IGk;

.field public final c:LX/IGi;


# direct methods
.method public constructor <init>(LX/0Or;LX/IGk;LX/IGi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/IGk;",
            "LX/IGi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2561246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2561247
    iput-object p1, p0, LX/IHu;->a:LX/0Or;

    .line 2561248
    iput-object p2, p0, LX/IHu;->b:LX/IGk;

    .line 2561249
    iput-object p3, p0, LX/IHu;->c:LX/IGi;

    .line 2561250
    return-void
.end method

.method public static a(LX/0QB;)LX/IHu;
    .locals 6

    .prologue
    .line 2561268
    sget-object v0, LX/IHu;->d:LX/IHu;

    if-nez v0, :cond_1

    .line 2561269
    const-class v1, LX/IHu;

    monitor-enter v1

    .line 2561270
    :try_start_0
    sget-object v0, LX/IHu;->d:LX/IHu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2561271
    if-eqz v2, :cond_0

    .line 2561272
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2561273
    new-instance v5, LX/IHu;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2561274
    new-instance v3, LX/IGk;

    invoke-direct {v3}, LX/IGk;-><init>()V

    .line 2561275
    move-object v3, v3

    .line 2561276
    move-object v3, v3

    .line 2561277
    check-cast v3, LX/IGk;

    .line 2561278
    new-instance v4, LX/IGi;

    invoke-direct {v4}, LX/IGi;-><init>()V

    .line 2561279
    move-object v4, v4

    .line 2561280
    move-object v4, v4

    .line 2561281
    check-cast v4, LX/IGi;

    invoke-direct {v5, p0, v3, v4}, LX/IHu;-><init>(LX/0Or;LX/IGk;LX/IGi;)V

    .line 2561282
    move-object v0, v5

    .line 2561283
    sput-object v0, LX/IHu;->d:LX/IHu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2561284
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2561285
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2561286
    :cond_1
    sget-object v0, LX/IHu;->d:LX/IHu;

    return-object v0

    .line 2561287
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2561288
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2561289
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2561290
    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2561291
    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2561251
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2561252
    const-string v1, "send_invite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2561253
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2561254
    const-string v1, "friendsNearbyInviteParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;

    .line 2561255
    iget-object v1, p0, LX/IHu;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/IHu;->b:LX/IGk;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 2561256
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2561257
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2561258
    :goto_0
    move-object v0, v0

    .line 2561259
    :goto_1
    return-object v0

    .line 2561260
    :cond_0
    const-string v1, "delete_invite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2561261
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2561262
    const-string v1, "friendsNearbyDeleteInviteParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;

    .line 2561263
    iget-object v1, p0, LX/IHu;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/IHu;->c:LX/IGi;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2561264
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2561265
    move-object v0, v0

    .line 2561266
    goto :goto_1

    .line 2561267
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
