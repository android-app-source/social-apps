.class public final LX/Hhk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/HhX;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hhl;


# direct methods
.method public constructor <init>(LX/Hhl;)V
    .locals 0

    .prologue
    .line 2496173
    iput-object p1, p0, LX/Hhk;->a:LX/Hhl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2496144
    check-cast p1, LX/HhX;

    .line 2496145
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/HhX;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2496146
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2496147
    :goto_0
    return-object v0

    .line 2496148
    :cond_1
    invoke-virtual {p1}, LX/HhX;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 2496149
    iget-object v2, p0, LX/Hhk;->a:LX/Hhl;

    iget-object v2, v2, LX/Hhl;->c:LX/1rd;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1rd;->c(I)I

    move-result v2

    .line 2496150
    invoke-static {v2}, LX/0km;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 2496151
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2496152
    invoke-virtual {p1}, LX/HhX;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HhM;

    .line 2496153
    new-instance v6, LX/4Jh;

    invoke-direct {v6}, LX/4Jh;-><init>()V

    .line 2496154
    iget-object v7, v2, LX/HhM;->c:Ljava/lang/String;

    move-object v7, v7

    .line 2496155
    const-string v8, "ip"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496156
    iget-wide v10, v2, LX/HhM;->j:J

    move-wide v8, v10

    .line 2496157
    long-to-int v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 2496158
    const-string v8, "latency"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2496159
    iget-object v7, v2, LX/HhM;->k:Ljava/lang/Throwable;

    move-object v7, v7

    .line 2496160
    if-eqz v7, :cond_2

    .line 2496161
    iget-object v7, v2, LX/HhM;->k:Ljava/lang/Throwable;

    move-object v2, v7

    .line 2496162
    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2496163
    const-string v7, "error"

    invoke-virtual {v6, v7, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496164
    :cond_2
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2496165
    :cond_3
    new-instance v2, LX/4KU;

    invoke-direct {v2}, LX/4KU;-><init>()V

    .line 2496166
    const-string v5, "network_type"

    invoke-virtual {v2, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496167
    const-string v3, "test_results"

    invoke-virtual {v2, v3, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2496168
    new-instance v3, LX/Hhb;

    invoke-direct {v3}, LX/Hhb;-><init>()V

    move-object v3, v3

    .line 2496169
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2496170
    move-object v0, v3

    .line 2496171
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2496172
    iget-object v1, p0, LX/Hhk;->a:LX/Hhl;

    iget-object v1, v1, LX/Hhl;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_0
.end method
