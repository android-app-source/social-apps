.class public final LX/Iqz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V
    .locals 0

    .prologue
    .line 2617377
    iput-object p1, p0, LX/Iqz;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x75210655

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2617369
    iget-object v1, p0, LX/Iqz;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    const/4 p1, 0x1

    .line 2617370
    iget-object p0, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result p0

    if-nez p0, :cond_1

    .line 2617371
    iget-boolean p0, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-nez p0, :cond_0

    .line 2617372
    invoke-static {v1}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->l(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    .line 2617373
    :cond_0
    iget-object p0, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/doodle/CaptionEditorView;->setEnabled(Z)V

    .line 2617374
    iget-object p0, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {p0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2617375
    iget-object p0, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->b()V

    .line 2617376
    :cond_1
    const v1, -0x7d29edb7

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
