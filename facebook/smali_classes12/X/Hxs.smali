.class public final enum LX/Hxs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hxs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hxs;

.field public static final enum FETCH_EVENT_COUNTS:LX/Hxs;

.field public static final enum FIRST_DB_FETCH:LX/Hxs;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2522892
    new-instance v0, LX/Hxs;

    const-string v1, "FIRST_DB_FETCH"

    invoke-direct {v0, v1, v2}, LX/Hxs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hxs;->FIRST_DB_FETCH:LX/Hxs;

    .line 2522893
    new-instance v0, LX/Hxs;

    const-string v1, "FETCH_EVENT_COUNTS"

    invoke-direct {v0, v1, v3}, LX/Hxs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hxs;->FETCH_EVENT_COUNTS:LX/Hxs;

    .line 2522894
    const/4 v0, 0x2

    new-array v0, v0, [LX/Hxs;

    sget-object v1, LX/Hxs;->FIRST_DB_FETCH:LX/Hxs;

    aput-object v1, v0, v2

    sget-object v1, LX/Hxs;->FETCH_EVENT_COUNTS:LX/Hxs;

    aput-object v1, v0, v3

    sput-object v0, LX/Hxs;->$VALUES:[LX/Hxs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2522895
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hxs;
    .locals 1

    .prologue
    .line 2522896
    const-class v0, LX/Hxs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hxs;

    return-object v0
.end method

.method public static values()[LX/Hxs;
    .locals 1

    .prologue
    .line 2522897
    sget-object v0, LX/Hxs;->$VALUES:[LX/Hxs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hxs;

    return-object v0
.end method
