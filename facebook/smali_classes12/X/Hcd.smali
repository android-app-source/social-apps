.class public final LX/Hcd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/2km;

.field public final synthetic b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic c:Landroid/view/View$OnClickListener;

.field public final synthetic d:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2487177
    iput-object p1, p0, LX/Hcd;->d:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    iput-object p2, p0, LX/Hcd;->a:LX/2km;

    iput-object p3, p0, LX/Hcd;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p4, p0, LX/Hcd;->c:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7bc15743

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    move-object v0, p1

    .line 2487178
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->hasSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487179
    iget-object v0, p0, LX/Hcd;->a:LX/2km;

    check-cast v0, LX/2kk;

    iget-object v2, p0, LX/Hcd;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    sget-object v3, LX/Cfc;->VIEW_COMMENT_ATTACHMENT_TAP:LX/Cfc;

    invoke-virtual {v3}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2487180
    const v0, 0x6b11f551

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2487181
    :goto_0
    return-void

    .line 2487182
    :cond_0
    iget-object v0, p0, LX/Hcd;->c:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2487183
    const v0, 0xe707b9b

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
