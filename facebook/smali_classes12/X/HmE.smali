.class public final LX/HmE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Hm4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 0

    .prologue
    .line 2500356
    iput-object p1, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/Hm4;)V
    .locals 5
    .param p1    # LX/Hm4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 2500357
    iget-object v0, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v2, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, LX/Hmf;->c(Z)V

    .line 2500358
    if-nez p1, :cond_1

    .line 2500359
    iget-object v0, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    iget-object v2, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v3, 0x7f083951

    invoke-virtual {v2, v3}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v4, 0x7f083954

    invoke-virtual {v3, v4}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2500360
    :goto_1
    return-void

    .line 2500361
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2500362
    :cond_1
    iget-object v0, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500363
    iget-object v1, v0, LX/HmV;->j:LX/Hm4;

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, LX/0Tp;->a(Z)V

    .line 2500364
    iput-object p1, v0, LX/HmV;->j:LX/Hm4;

    .line 2500365
    iget-object v0, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-static {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->l(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    goto :goto_1

    .line 2500366
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2500367
    iget-object v0, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-virtual {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2500368
    :goto_0
    return-void

    .line 2500369
    :cond_0
    iget-object v0, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Hmf;->c(Z)V

    .line 2500370
    iget-object v0, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    iget-object v1, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f083951

    invoke-virtual {v1, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HmE;->a:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v3, 0x7f083954

    invoke-virtual {v2, v3}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2500371
    const-string v0, "BeamReceiver"

    const-string v1, "Unexpected error creating socket"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2500372
    check-cast p1, LX/Hm4;

    invoke-direct {p0, p1}, LX/HmE;->a(LX/Hm4;)V

    return-void
.end method
