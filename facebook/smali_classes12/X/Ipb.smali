.class public final LX/Ipb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;)V
    .locals 0

    .prologue
    .line 2613066
    iput-object p1, p0, LX/Ipb;->a:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x171a8dc9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2613067
    iget-object v1, p0, LX/Ipb;->a:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    iget-object v1, v1, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->f:LX/Iph;

    sget-object v2, LX/Iph;->BUTTON_LEFT_SELECTED:LX/Iph;

    if-ne v1, v2, :cond_0

    .line 2613068
    iget-object v1, p0, LX/Ipb;->a:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    sget-object v2, LX/Iph;->BUTTON_LEFT_CONFIRMED:LX/Iph;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setState(LX/Iph;)V

    .line 2613069
    :goto_0
    const v1, -0x57507897

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2613070
    :cond_0
    iget-object v1, p0, LX/Ipb;->a:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    sget-object v2, LX/Iph;->BUTTON_LEFT_SELECTED:LX/Iph;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setState(LX/Iph;)V

    goto :goto_0
.end method
