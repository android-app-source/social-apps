.class public LX/IoD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IoC;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2611596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2611597
    iput-object p1, p0, LX/IoD;->a:LX/0Zb;

    .line 2611598
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2611599
    iget-object v0, p0, LX/IoD;->a:LX/0Zb;

    sget-object v1, LX/5g0;->GROUP_COMMERCE_SEND:LX/5g0;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2611600
    iget-object v2, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2611601
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5fz;->i(Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2611602
    iget-object v2, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v2, v2

    .line 2611603
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->c()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    move-result-object v1

    .line 2611604
    iget-object v2, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v1, v2

    .line 2611605
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2611606
    return-void
.end method
