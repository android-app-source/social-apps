.class public final LX/IYm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/looknow/LookNowPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/looknow/LookNowPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2588065
    iput-object p1, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 2588066
    if-eqz p1, :cond_0

    .line 2588067
    iget-object v0, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/looknow/LookNowPermalinkFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2588068
    :goto_0
    return-void

    .line 2588069
    :cond_0
    iget-object v0, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/looknow/LookNowPermalinkFragment;->d(Lcom/facebook/looknow/LookNowPermalinkFragment;)LX/0fz;

    move-result-object v0

    invoke-virtual {v0}, LX/0fz;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2588070
    iget-object v0, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/looknow/LookNowPermalinkFragment;->h:LX/IYi;

    sget-object v1, LX/IYh;->LOOK_NOW_PERMALINK_LOAD_FAILURE:LX/IYh;

    const-string v2, "no_story"

    invoke-virtual {v0, v1, v2}, LX/IYi;->a(LX/IYh;Ljava/lang/String;)V

    .line 2588071
    iget-object v0, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/looknow/LookNowPermalinkFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    const v2, 0x7f0838ab

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0

    .line 2588072
    :cond_1
    iget-object v0, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/looknow/LookNowPermalinkFragment;->h:LX/IYi;

    sget-object v1, LX/IYh;->LOOK_NOW_PERMALINK_LOAD_SUCCESS:LX/IYh;

    invoke-virtual {v0, v1}, LX/IYi;->a(LX/IYh;)V

    .line 2588073
    iget-object v0, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/looknow/LookNowPermalinkFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2588074
    iget-object v0, p0, LX/IYm;->a:Lcom/facebook/looknow/LookNowPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/looknow/LookNowPermalinkFragment;->b(Lcom/facebook/looknow/LookNowPermalinkFragment;)LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    goto :goto_0
.end method
