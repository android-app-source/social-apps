.class public LX/HRo;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/widget/LinearLayout$LayoutParams;


# instance fields
.field public b:Z

.field public c:Z

.field private d:Z

.field public e:Landroid/widget/LinearLayout;

.field public f:LX/HRn;

.field public g:Z

.field public h:Landroid/app/Activity;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2465901
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, LX/HRo;->a:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2465899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2465900
    return-void
.end method

.method public static a(LX/0QB;)LX/HRo;
    .locals 1

    .prologue
    .line 2465848
    new-instance v0, LX/HRo;

    invoke-direct {v0}, LX/HRo;-><init>()V

    .line 2465849
    move-object v0, v0

    .line 2465850
    return-object v0
.end method

.method private static b(LX/HRo;JLX/15i;ILX/0am;)V
    .locals 7
    .param p1    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/15i;",
            "I",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2465876
    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/HRo;->g:Z

    if-nez v0, :cond_1

    .line 2465877
    iget-object v1, p0, LX/HRo;->f:LX/HRn;

    check-cast v1, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    .line 2465878
    invoke-virtual/range {v1 .. v6}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b(JLX/15i;ILX/0am;)V

    .line 2465879
    :cond_0
    :goto_0
    return-void

    .line 2465880
    :cond_1
    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, LX/HRo;->g:Z

    if-eqz v0, :cond_5

    .line 2465881
    iget-boolean v0, p0, LX/HRo;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    instance-of v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;

    if-eqz v0, :cond_2

    .line 2465882
    iget-object v1, p0, LX/HRo;->f:LX/HRn;

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, LX/HRn;->a(JLX/15i;ILX/0am;)V

    goto :goto_0

    .line 2465883
    :cond_2
    iget-boolean v0, p0, LX/HRo;->c:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    instance-of v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    if-eqz v0, :cond_3

    .line 2465884
    iget-object v1, p0, LX/HRo;->f:LX/HRn;

    check-cast v1, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    .line 2465885
    invoke-virtual/range {v1 .. v6}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b(JLX/15i;ILX/0am;)V

    goto :goto_0

    .line 2465886
    :cond_3
    iget-boolean v0, p0, LX/HRo;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    instance-of v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;

    if-eqz v0, :cond_4

    .line 2465887
    iget-object v1, p0, LX/HRo;->f:LX/HRn;

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, LX/HRn;->a(JLX/15i;ILX/0am;)V

    goto :goto_0

    .line 2465888
    :cond_4
    invoke-static {p0}, LX/HRo;->d(LX/HRo;)V

    .line 2465889
    const/4 v0, 0x0

    iput-object v0, p0, LX/HRo;->f:LX/HRn;

    .line 2465890
    :cond_5
    iget-boolean v0, p0, LX/HRo;->b:Z

    if-eqz v0, :cond_7

    .line 2465891
    iget-object v0, p0, LX/HRo;->h:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030e0b

    iget-object v2, p0, LX/HRo;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;

    iput-object v0, p0, LX/HRo;->f:LX/HRn;

    .line 2465892
    :cond_6
    :goto_1
    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    if-eqz v0, :cond_0

    .line 2465893
    invoke-static {p0}, LX/HRo;->c(LX/HRo;)V

    .line 2465894
    iget-object v1, p0, LX/HRo;->f:LX/HRn;

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, LX/HRn;->a(JLX/15i;ILX/0am;)V

    goto :goto_0

    .line 2465895
    :cond_7
    iget-boolean v0, p0, LX/HRo;->c:Z

    if-eqz v0, :cond_8

    .line 2465896
    iget-object v0, p0, LX/HRo;->h:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030e09

    iget-object v2, p0, LX/HRo;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iput-object v0, p0, LX/HRo;->f:LX/HRn;

    goto :goto_1

    .line 2465897
    :cond_8
    iget-boolean v0, p0, LX/HRo;->d:Z

    if-eqz v0, :cond_6

    .line 2465898
    iget-object v0, p0, LX/HRo;->h:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030e0e

    iget-object v2, p0, LX/HRo;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;

    iput-object v0, p0, LX/HRo;->f:LX/HRn;

    goto :goto_1
.end method

.method public static c(LX/HRo;)V
    .locals 4

    .prologue
    .line 2465873
    iget-object v1, p0, LX/HRo;->e:Landroid/widget/LinearLayout;

    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, LX/HRo;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sget-object v3, LX/HRo;->a:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1, v0, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2465874
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HRo;->g:Z

    .line 2465875
    return-void
.end method

.method public static d(LX/HRo;)V
    .locals 2

    .prologue
    .line 2465870
    iget-object v1, p0, LX/HRo;->e:Landroid/widget/LinearLayout;

    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2465871
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HRo;->g:Z

    .line 2465872
    return-void
.end method


# virtual methods
.method public final a(JLX/15i;ILX/0am;)V
    .locals 5
    .param p3    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/15i;",
            "I",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 2465851
    if-nez p4, :cond_2

    .line 2465852
    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    if-eqz v0, :cond_1

    .line 2465853
    iget-boolean v0, p0, LX/HRo;->g:Z

    if-eqz v0, :cond_0

    .line 2465854
    invoke-static {p0}, LX/HRo;->d(LX/HRo;)V

    .line 2465855
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/HRo;->f:LX/HRn;

    .line 2465856
    :cond_1
    :goto_0
    return-void

    .line 2465857
    :cond_2
    iget-boolean v2, p0, LX/HRo;->b:Z

    .line 2465858
    iget-boolean v3, p0, LX/HRo;->c:Z

    .line 2465859
    invoke-virtual {p3, p4, v4}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    .line 2465860
    invoke-virtual {p3, p4, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2465861
    const/4 v4, 0x2

    invoke-virtual {p3, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, LX/HRo;->b:Z

    .line 2465862
    const/16 v0, 0x9

    invoke-virtual {p3, p4, v0}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    iput-boolean v1, p0, LX/HRo;->c:Z

    .line 2465863
    const/4 v0, 0x6

    invoke-virtual {p3, p4, v0}, LX/15i;->h(II)Z

    move-result v0

    iput-boolean v0, p0, LX/HRo;->d:Z

    .line 2465864
    iget-object v0, p0, LX/HRo;->f:LX/HRn;

    if-nez v0, :cond_6

    .line 2465865
    :cond_4
    :goto_2
    invoke-static/range {p0 .. p5}, LX/HRo;->b(LX/HRo;JLX/15i;ILX/0am;)V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 2465866
    goto :goto_1

    .line 2465867
    :cond_6
    iget-boolean v0, p0, LX/HRo;->b:Z

    if-ne v2, v0, :cond_7

    iget-boolean v0, p0, LX/HRo;->b:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, LX/HRo;->c:Z

    if-eq v3, v0, :cond_4

    .line 2465868
    :cond_7
    invoke-static {p0}, LX/HRo;->d(LX/HRo;)V

    .line 2465869
    const/4 v0, 0x0

    iput-object v0, p0, LX/HRo;->f:LX/HRn;

    goto :goto_2
.end method
