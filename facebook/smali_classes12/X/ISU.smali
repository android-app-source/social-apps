.class public LX/ISU;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/DK4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DK4",
            "<",
            "LX/DK7;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/IW5;

.field private final d:LX/8ht;

.field public final e:LX/0zG;

.field private final f:LX/0Uh;

.field private final g:LX/0ad;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/IW3;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DO3;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/88n;

.field public final m:LX/DK3;

.field public final n:LX/ISH;

.field public final o:LX/DKL;

.field public final p:Lcom/facebook/base/fragment/FbFragment;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ILp;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/DKM;

.field public t:LX/IST;

.field public u:Z

.field public v:Z

.field private w:LX/63Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2577945
    const-class v0, LX/ISU;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ISU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/IW5;LX/8ht;LX/0zG;LX/0Uh;LX/0ad;LX/0Ot;LX/IW3;LX/0Ot;LX/0Ot;LX/88n;LX/DK3;LX/ISH;Lcom/facebook/base/fragment/FbFragment;LX/DKL;LX/0Ot;LX/0Ot;)V
    .locals 2
    .param p12    # LX/ISH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/base/fragment/FbFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IW5;",
            "LX/8ht;",
            "LX/0zG;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/IW3;",
            "LX/0Ot",
            "<",
            "LX/DO3;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/88n;",
            "LX/DK3;",
            "LX/ISH;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "LX/DKL;",
            "LX/0Ot",
            "<",
            "LX/ILp;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2577984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2577985
    new-instance v1, LX/ISO;

    invoke-direct {v1, p0}, LX/ISO;-><init>(LX/ISU;)V

    iput-object v1, p0, LX/ISU;->b:LX/DK4;

    .line 2577986
    iput-object p1, p0, LX/ISU;->c:LX/IW5;

    .line 2577987
    iput-object p2, p0, LX/ISU;->d:LX/8ht;

    .line 2577988
    iput-object p3, p0, LX/ISU;->e:LX/0zG;

    .line 2577989
    iput-object p4, p0, LX/ISU;->f:LX/0Uh;

    .line 2577990
    iput-object p5, p0, LX/ISU;->g:LX/0ad;

    .line 2577991
    iput-object p6, p0, LX/ISU;->h:LX/0Ot;

    .line 2577992
    iput-object p7, p0, LX/ISU;->i:LX/IW3;

    .line 2577993
    iput-object p8, p0, LX/ISU;->j:LX/0Ot;

    .line 2577994
    iput-object p9, p0, LX/ISU;->k:LX/0Ot;

    .line 2577995
    iput-object p10, p0, LX/ISU;->l:LX/88n;

    .line 2577996
    iput-object p11, p0, LX/ISU;->m:LX/DK3;

    .line 2577997
    iput-object p12, p0, LX/ISU;->n:LX/ISH;

    .line 2577998
    iput-object p13, p0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    .line 2577999
    move-object/from16 v0, p14

    iput-object v0, p0, LX/ISU;->o:LX/DKL;

    .line 2578000
    move-object/from16 v0, p15

    iput-object v0, p0, LX/ISU;->q:LX/0Ot;

    .line 2578001
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ISU;->r:LX/0Ot;

    .line 2578002
    return-void
.end method


# virtual methods
.method public final a(LX/63S;LX/0g9;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2577978
    iget-object v0, p0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    const-class v1, LX/63U;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/63U;

    .line 2577979
    iget-object v0, p0, LX/ISU;->w:LX/63Q;

    if-nez v0, :cond_0

    if-eqz v4, :cond_0

    .line 2577980
    new-instance v0, LX/63Q;

    invoke-direct {v0}, LX/63Q;-><init>()V

    iput-object v0, p0, LX/ISU;->w:LX/63Q;

    .line 2577981
    iget-object v0, p0, LX/ISU;->w:LX/63Q;

    iget-object v1, p0, LX/ISU;->e:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/63T;

    iget-boolean v1, p0, LX/ISU;->v:Z

    if-nez v1, :cond_1

    move v6, v5

    :goto_0
    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, LX/63Q;->a(LX/63S;LX/63T;LX/0g9;LX/63U;ZZ)V

    .line 2577982
    :cond_0
    return-void

    .line 2577983
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final b(Z)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 6

    .prologue
    .line 2577973
    iget-object v0, p0, LX/ISU;->c:LX/IW5;

    .line 2577974
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2577975
    iget-object v1, p0, LX/ISU;->c:LX/IW5;

    .line 2577976
    iget-object v2, v1, LX/IW5;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2577977
    iget-object v3, p0, LX/ISU;->d:LX/8ht;

    iget-object v4, p0, LX/ISU;->f:LX/0Uh;

    iget-object v5, p0, LX/ISU;->g:LX/0ad;

    move v2, p1

    invoke-static/range {v0 .. v5}, LX/DOI;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;ZLX/8ht;LX/0Uh;LX/0ad;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2577954
    iget-object v0, p0, LX/ISU;->c:LX/IW5;

    .line 2577955
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2577956
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISU;->c:LX/IW5;

    .line 2577957
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2577958
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISU;->c:LX/IW5;

    .line 2577959
    iget-boolean v1, v0, LX/IW5;->b:Z

    move v0, v1

    .line 2577960
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/ISU;->v:Z

    if-eqz v0, :cond_1

    .line 2577961
    :cond_0
    :goto_0
    return-void

    .line 2577962
    :cond_1
    iget-object v0, p0, LX/ISU;->d:LX/8ht;

    invoke-virtual {p0, v4}, LX/ISU;->b(Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8ht;->f(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2577963
    iget-object v0, p0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    const v1, 0x7f0820b7

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/ISU;->c:LX/IW5;

    .line 2577964
    iget-object v5, v3, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v3, v5

    .line 2577965
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2577966
    iget-object v0, p0, LX/ISU;->e:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/63T;

    invoke-interface {v0, v1}, LX/63T;->setTitleHint(Ljava/lang/String;)V

    goto :goto_0

    .line 2577967
    :cond_2
    iget-object v0, p0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    const-class v1, LX/1ZF;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2577968
    if-eqz v0, :cond_0

    .line 2577969
    iget-object v1, p0, LX/ISU;->c:LX/IW5;

    .line 2577970
    iget-object v2, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v2

    .line 2577971
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2577972
    invoke-interface {v0}, LX/1ZF;->lH_()V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 2577950
    if-eqz p1, :cond_0

    sget-object v0, LX/DO3;->a:LX/0zS;

    .line 2577951
    :goto_0
    iget-object v1, p0, LX/ISU;->i:LX/IW3;

    invoke-virtual {v1, v0}, LX/IW3;->a(LX/0zS;)V

    .line 2577952
    return-void

    .line 2577953
    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2577946
    iget-boolean v0, p0, LX/ISU;->u:Z

    if-eqz v0, :cond_0

    .line 2577947
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ISU;->u:Z

    .line 2577948
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/ISU;->c(Z)V

    .line 2577949
    :cond_0
    return-void
.end method
