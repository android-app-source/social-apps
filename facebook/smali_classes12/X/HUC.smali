.class public final LX/HUC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V
    .locals 0

    .prologue
    .line 2471660
    iput-object p1, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2471641
    iget-object v0, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-static {v0, v1}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V

    .line 2471642
    iget-object v0, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    .line 2471643
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->F:Z

    .line 2471644
    iget-object v0, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchPagesPoliticalEndorsements"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2471645
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2471646
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2471647
    iget-object v0, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    .line 2471648
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->F:Z

    .line 2471649
    if-nez p1, :cond_0

    .line 2471650
    :goto_0
    return-void

    .line 2471651
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2471652
    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;

    .line 2471653
    iget-object v3, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;->j()Z

    move-result v4

    if-ne v4, v2, :cond_1

    move v1, v2

    .line 2471654
    :cond_1
    iput-boolean v1, v3, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->C:Z

    .line 2471655
    iget-object v1, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    .line 2471656
    iput-object v0, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->G:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;

    .line 2471657
    iget-object v1, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-static {v1, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;)V

    .line 2471658
    iget-object v1, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;)V

    .line 2471659
    iget-object v0, p0, LX/HUC;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-static {v0, v2}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->b(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V

    goto :goto_0
.end method
