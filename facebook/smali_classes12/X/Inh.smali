.class public LX/Inh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ing;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iok;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IpT;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Uh;

.field private e:LX/Ing;

.field private f:LX/Io2;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Iok;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/IpT;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610792
    iput-object p1, p0, LX/Inh;->a:LX/0Ot;

    .line 2610793
    iput-object p2, p0, LX/Inh;->b:LX/0Ot;

    .line 2610794
    iput-object p3, p0, LX/Inh;->c:LX/0Ot;

    .line 2610795
    iput-object p4, p0, LX/Inh;->d:LX/0Uh;

    .line 2610796
    return-void
.end method

.method private a(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)LX/Ing;
    .locals 3

    .prologue
    .line 2610782
    iget-object v0, p0, LX/Inh;->d:LX/0Uh;

    const/16 v1, 0x22b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610783
    sget-object v0, LX/Inf;->b:[I

    .line 2610784
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->z:LX/IoB;

    move-object v1, v1

    .line 2610785
    invoke-virtual {v1}, LX/IoB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2610786
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "EnterPaymentValueNewDesignSelectedFlow is not of the correct type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2610787
    :pswitch_0
    iget-object v0, p0, LX/Inh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ing;

    .line 2610788
    :goto_0
    return-object v0

    .line 2610789
    :pswitch_1
    iget-object v0, p0, LX/Inh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ing;

    goto :goto_0

    .line 2610790
    :cond_0
    iget-object v0, p0, LX/Inh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ing;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/Inh;
    .locals 5

    .prologue
    .line 2610765
    new-instance v1, LX/Inh;

    const/16 v0, 0x28d0

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v0, 0x28d4

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v0, 0x28d8

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v2, v3, v4, v0}, LX/Inh;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 2610766
    move-object v0, v1

    .line 2610767
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2610779
    iget-object v0, p0, LX/Inh;->e:LX/Ing;

    if-nez v0, :cond_0

    .line 2610780
    :goto_0
    return-void

    .line 2610781
    :cond_0
    iget-object v0, p0, LX/Inh;->e:LX/Ing;

    invoke-interface {v0}, LX/Ing;->a()V

    goto :goto_0
.end method

.method public final a(LX/Io2;)V
    .locals 0

    .prologue
    .line 2610777
    iput-object p1, p0, LX/Inh;->f:LX/Io2;

    .line 2610778
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 4

    .prologue
    .line 2610768
    const-string v0, "payment_flow_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5g0;

    .line 2610769
    sget-object v1, LX/Inf;->a:[I

    invoke-virtual {v0}, LX/5g0;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2610770
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported paymentFlowType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2610771
    :pswitch_0
    invoke-direct {p0, p2}, LX/Inh;->a(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)LX/Ing;

    move-result-object v0

    iput-object v0, p0, LX/Inh;->e:LX/Ing;

    .line 2610772
    :goto_0
    iget-object v0, p0, LX/Inh;->e:LX/Ing;

    iget-object v1, p0, LX/Inh;->f:LX/Io2;

    invoke-interface {v0, v1}, LX/Ing;->a(LX/Io2;)V

    .line 2610773
    iget-object v0, p0, LX/Inh;->e:LX/Ing;

    invoke-interface {v0, p1, p2}, LX/Ing;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2610774
    return-void

    .line 2610775
    :pswitch_1
    iget-object v0, p0, LX/Inh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ing;

    iput-object v0, p0, LX/Inh;->e:LX/Ing;

    goto :goto_0

    .line 2610776
    :pswitch_2
    iget-object v0, p0, LX/Inh;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ing;

    iput-object v0, p0, LX/Inh;->e:LX/Ing;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
