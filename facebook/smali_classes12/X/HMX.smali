.class public final LX/HMX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/HMI;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/HMI;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 2457234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457235
    iput-object p1, p0, LX/HMX;->a:LX/0QB;

    .line 2457236
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2457237
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/HMX;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2457238
    packed-switch p2, :pswitch_data_0

    .line 2457239
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2457240
    :pswitch_0
    new-instance v2, LX/HMJ;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {p1}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v6

    check-cast v6, LX/9XE;

    invoke-static {p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v7

    check-cast v7, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p1}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v8

    check-cast v8, LX/0SI;

    invoke-direct/range {v2 .. v8}, LX/HMJ;-><init>(Landroid/content/Context;LX/0kL;Landroid/content/res/Resources;LX/9XE;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0SI;)V

    .line 2457241
    move-object v0, v2

    .line 2457242
    :goto_0
    return-object v0

    .line 2457243
    :pswitch_1
    new-instance v2, LX/HML;

    const/16 v3, 0x12c4

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x51

    invoke-static {p1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf07

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x455

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, Landroid/content/Context;

    invoke-interface {p1, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-direct/range {v2 .. v7}, LX/HML;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;)V

    .line 2457244
    move-object v0, v2

    .line 2457245
    goto :goto_0

    .line 2457246
    :pswitch_2
    new-instance v2, LX/HMN;

    const/16 v3, 0x12c4

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x51

    invoke-static {p1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf07

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x36a9

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, Landroid/content/Context;

    invoke-interface {p1, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-direct/range {v2 .. v7}, LX/HMN;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;)V

    .line 2457247
    move-object v0, v2

    .line 2457248
    goto :goto_0

    .line 2457249
    :pswitch_3
    new-instance v3, LX/HMP;

    invoke-static {p1}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v0

    check-cast v0, LX/9XE;

    invoke-static {p1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {p1}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v2

    check-cast v2, LX/HDT;

    invoke-direct {v3, v0, v1, v2}, LX/HMP;-><init>(LX/9XE;Lcom/facebook/privacy/PrivacyOperationsClient;LX/HDT;)V

    .line 2457250
    move-object v0, v3

    .line 2457251
    goto :goto_0

    .line 2457252
    :pswitch_4
    new-instance v0, LX/HMQ;

    invoke-direct {v0}, LX/HMQ;-><init>()V

    .line 2457253
    move-object v0, v0

    .line 2457254
    move-object v0, v0

    .line 2457255
    goto :goto_0

    .line 2457256
    :pswitch_5
    invoke-static {p1}, LX/HMS;->b(LX/0QB;)LX/HMS;

    move-result-object v0

    goto :goto_0

    .line 2457257
    :pswitch_6
    new-instance v2, LX/HMT;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {p1}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v6

    check-cast v6, LX/9XE;

    invoke-static {p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v7

    check-cast v7, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v2 .. v8}, LX/HMT;-><init>(Landroid/content/Context;LX/0kL;Landroid/content/res/Resources;LX/9XE;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/03V;)V

    .line 2457258
    move-object v0, v2

    .line 2457259
    goto/16 :goto_0

    .line 2457260
    :pswitch_7
    new-instance v2, LX/HMV;

    const/16 v3, 0x19c6

    invoke-static {p1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x12c4

    invoke-static {p1, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf07

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1061

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {p1}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v8

    check-cast v8, LX/HDT;

    invoke-static {p1}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v9

    check-cast v9, LX/01T;

    const-class v10, Landroid/content/Context;

    invoke-interface {p1, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v11

    check-cast v11, Landroid/content/res/Resources;

    invoke-static {p1}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v12

    check-cast v12, LX/8Do;

    invoke-direct/range {v2 .. v12}, LX/HMV;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/HDT;LX/01T;Landroid/content/Context;Landroid/content/res/Resources;LX/8Do;)V

    .line 2457261
    move-object v0, v2

    .line 2457262
    goto/16 :goto_0

    .line 2457263
    :pswitch_8
    new-instance v2, LX/HMW;

    invoke-static {p1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    invoke-static {p1}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v1

    check-cast v1, LX/9XE;

    invoke-direct {v2, v0, v1}, LX/HMW;-><init>(LX/0kL;LX/9XE;)V

    .line 2457264
    move-object v0, v2

    .line 2457265
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2457266
    const/16 v0, 0x9

    return v0
.end method
