.class public final LX/ISh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final synthetic b:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 0

    .prologue
    .line 2578120
    iput-object p1, p0, LX/ISh;->b:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iput-object p2, p0, LX/ISh;->a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x10f82ff1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2578121
    iget-object v1, p0, LX/ISh;->b:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->s:LX/ISp;

    iget-object v2, p0, LX/ISh;->a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2578122
    invoke-static {v1}, LX/ISp;->d(LX/ISp;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    .line 2578123
    iget-object v6, v1, LX/ISp;->a:LX/1Kf;

    const/4 v7, 0x0

    const/16 v8, 0x6dc

    iget-object v4, v1, LX/ISp;->g:Landroid/content/Context;

    const-class p1, Landroid/app/Activity;

    invoke-static {v4, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-interface {v6, v7, v5, v8, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2578124
    iget-object v1, p0, LX/ISh;->b:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    .line 2578125
    iget-object v2, v1, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->J:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->c()V

    .line 2578126
    iget-object v2, v1, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->y:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const v4, 0x7f081bfc

    invoke-virtual {v2, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(I)V

    .line 2578127
    iget-object v2, v1, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->K:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v4, v1, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578128
    const v1, 0x58793e53

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
