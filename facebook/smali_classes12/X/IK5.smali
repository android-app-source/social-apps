.class public final LX/IK5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<",
        "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IK6;


# direct methods
.method public constructor <init>(LX/IK6;)V
    .locals 0

    .prologue
    .line 2564956
    iput-object p1, p0, LX/IK5;->a:LX/IK6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2564953
    iget-object v0, p0, LX/IK5;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    invoke-virtual {v0, p4}, LX/IK9;->b(LX/2kM;)V

    .line 2564954
    iget-object v0, p0, LX/IK5;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    sget-object v1, LX/1lD;->LOAD_FINISHED:LX/1lD;

    invoke-virtual {v0, v1}, LX/IK9;->a(LX/1lD;)V

    .line 2564955
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2564952
    return-void
.end method

.method public final bridge synthetic a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 0
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564944
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564950
    iget-object v0, p0, LX/IK5;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    invoke-virtual {v0, v1}, LX/IK9;->a(LX/1lD;)V

    .line 2564951
    return-void
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564945
    iget-object v0, p0, LX/IK5;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    iget-object v1, p0, LX/IK5;->a:LX/IK6;

    iget-object v1, v1, LX/IK6;->l:LX/2kW;

    .line 2564946
    iget-object p1, v1, LX/2kW;->o:LX/2kM;

    move-object v1, p1

    .line 2564947
    invoke-virtual {v0, v1}, LX/IK9;->b(LX/2kM;)V

    .line 2564948
    iget-object v0, p0, LX/IK5;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    sget-object v1, LX/1lD;->LOAD_FINISHED:LX/1lD;

    invoke-virtual {v0, v1}, LX/IK9;->a(LX/1lD;)V

    .line 2564949
    return-void
.end method
