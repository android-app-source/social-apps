.class public final LX/ICJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

.field public final synthetic b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V
    .locals 0

    .prologue
    .line 2549274
    iput-object p1, p0, LX/ICJ;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iput-object p2, p0, LX/ICJ;->a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x2b51b4cf

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2549275
    iget-object v1, p0, LX/ICJ;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iget-object v2, p0, LX/ICJ;->a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    invoke-static {v1, v2, p1}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->a$redex0(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;Landroid/view/View;)V

    .line 2549276
    iget-object v1, p0, LX/ICJ;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    sget-object v2, LX/ICQ;->HIDE:LX/ICQ;

    iget-object v3, p0, LX/ICJ;->a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a(LX/ICQ;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V

    .line 2549277
    iget-object v1, p0, LX/ICJ;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    sget-object v2, LX/ICQ;->HIDE:LX/ICQ;

    invoke-virtual {v1, v2}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a(LX/ICQ;)V

    .line 2549278
    const v1, -0x18d77531

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
