.class public LX/Izs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lC;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2635778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635779
    iput-object p1, p0, LX/Izs;->a:LX/0lC;

    .line 2635780
    iput-object p2, p0, LX/Izs;->b:LX/03V;

    .line 2635781
    return-void
.end method

.method public static b(LX/0QB;)LX/Izs;
    .locals 3

    .prologue
    .line 2635782
    new-instance v2, LX/Izs;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/Izs;-><init>(LX/0lC;LX/03V;)V

    .line 2635783
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2635784
    const-string v0, "deserializePlatformItem"

    const v2, -0x5583f65

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635785
    if-nez p1, :cond_0

    .line 2635786
    const v0, -0x6a5e3900

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 2635787
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/Izs;->a:LX/0lC;

    const-class v2, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    invoke-virtual {v0, p1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635788
    const v1, 0x5dd31d5

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635789
    :catch_0
    :try_start_1
    iget-object v0, p0, LX/Izs;->b:LX/03V;

    const-string v2, "DbPlatformItemSerialization"

    const-string v3, "IO Exception when reading PlatformItem from JSON string."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635790
    const v0, 0x731c01a0

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x24896923

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2635791
    const-string v0, "serializePlatformItem"

    const v1, 0x194f8786

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635792
    if-nez p1, :cond_0

    .line 2635793
    const v0, 0x399bffcf

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2635794
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/Izs;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2635795
    const v1, 0x580e82b4

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635796
    :catch_0
    move-exception v0

    .line 2635797
    :try_start_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635798
    :catchall_0
    move-exception v0

    const v1, 0x2a8e47ce

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
