.class public LX/HqE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/activity/AudienceEducatorController$ResultHandlerCallback;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public final c:LX/339;

.field public final d:LX/0kb;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/J6v;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0pZ;

.field public n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/activity/AudienceEducatorController$DataProvider;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/HqA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/339;LX/0kb;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0pZ;LX/HqT;)V
    .locals 3
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsDefaultPostPrivacyEnabled;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/educator/IsNewcomerAudienceSelectorEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/educator/IsAudienceAlignmentTuxEnabled;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/educator/IsAudienceAlignmentTuxOnlyMeEnabled;
        .end annotation
    .end param
    .param p12    # LX/HqT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/339;",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/J6v;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0pZ;",
            "Lcom/facebook/composer/activity/AudienceEducatorController$DataProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2510435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2510436
    iput-object p1, p0, LX/HqE;->c:LX/339;

    .line 2510437
    iput-object p2, p0, LX/HqE;->d:LX/0kb;

    .line 2510438
    iput-object p3, p0, LX/HqE;->e:LX/0Ot;

    .line 2510439
    iput-object p4, p0, LX/HqE;->f:LX/0Ot;

    .line 2510440
    iput-object p5, p0, LX/HqE;->g:LX/0Ot;

    .line 2510441
    iput-object p6, p0, LX/HqE;->h:LX/0Ot;

    .line 2510442
    iput-object p7, p0, LX/HqE;->i:LX/0Or;

    .line 2510443
    iput-object p8, p0, LX/HqE;->j:LX/0Or;

    .line 2510444
    iput-object p9, p0, LX/HqE;->k:LX/0Or;

    .line 2510445
    iput-object p10, p0, LX/HqE;->l:LX/0Or;

    .line 2510446
    iput-object p11, p0, LX/HqE;->m:LX/0pZ;

    .line 2510447
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p12}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    .line 2510448
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HqE;->b:Z

    .line 2510449
    new-instance v0, LX/HqD;

    invoke-direct {v0, p0}, LX/HqD;-><init>(LX/HqE;)V

    new-instance v1, LX/HqB;

    invoke-direct {v1, p0}, LX/HqB;-><init>(LX/HqE;)V

    new-instance v2, LX/HqC;

    invoke-direct {v2, p0}, LX/HqC;-><init>(LX/HqE;)V

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/HqE;->o:LX/0Px;

    .line 2510450
    return-void
.end method

.method public static a(LX/HqE;LX/8Pq;LX/HrJ;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 6

    .prologue
    .line 2510310
    iget-object v0, p0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqT;

    .line 2510311
    if-nez v0, :cond_0

    .line 2510312
    :goto_0
    return-void

    .line 2510313
    :cond_0
    iget-object v1, p0, LX/HqE;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/J6v;

    invoke-virtual {v0}, LX/HqT;->d()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    .line 2510314
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2510315
    sget-object v3, LX/J6u;->a:[I

    invoke-virtual {p1}, LX/8Pq;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2510316
    :goto_1
    move-object v2, v2

    .line 2510317
    move-object v2, v2

    .line 2510318
    const-string v0, ""

    .line 2510319
    const-string v1, "tooltip_title"

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2510320
    const-string v0, "tooltip_title"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 2510321
    :goto_2
    const-string v0, ""

    .line 2510322
    const-string v3, "tooltip_body"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2510323
    const-string v0, "tooltip_body"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2510324
    :cond_1
    iget-object v2, p2, LX/HrJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a()LX/AP3;

    move-result-object v2

    .line 2510325
    iput-object v1, v2, LX/AP3;->a:Ljava/lang/String;

    .line 2510326
    move-object v2, v2

    .line 2510327
    iput-object v0, v2, LX/AP3;->b:Ljava/lang/String;

    .line 2510328
    move-object v2, v2

    .line 2510329
    invoke-virtual {v2}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v3

    .line 2510330
    iget-object v2, p2, LX/HrJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v4, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v2, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-virtual {v2, v3}, LX/0jL;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-virtual {v2}, LX/0jL;->a()V

    .line 2510331
    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_2

    .line 2510332
    :pswitch_0
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2510333
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2510334
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    :goto_3
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2510335
    sget-object v3, LX/J6u;->b:[I

    invoke-virtual {v0}, LX/2by;->ordinal()I

    move-result p0

    aget v3, v3, p0

    packed-switch v3, :pswitch_data_1

    .line 2510336
    :goto_4
    goto :goto_1

    .line 2510337
    :pswitch_1
    sget-object v3, LX/J6u;->b:[I

    invoke-virtual {v0}, LX/2by;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2

    .line 2510338
    :goto_5
    goto/16 :goto_1

    .line 2510339
    :pswitch_2
    sget-object v3, LX/J6u;->b:[I

    invoke-virtual {v0}, LX/2by;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_3

    .line 2510340
    :goto_6
    goto/16 :goto_1

    :cond_3
    move v3, v5

    .line 2510341
    goto :goto_3

    .line 2510342
    :pswitch_3
    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const p0, 0x7f0839bb

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v5

    invoke-virtual {v3, p0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2510343
    const-string v4, "tooltip_title"

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2510344
    const-string v4, "tooltip_body"

    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0839be

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 2510345
    :pswitch_4
    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const p0, 0x7f0839ab

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v5

    invoke-virtual {v3, p0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2510346
    const-string v4, "tooltip_body"

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 2510347
    :pswitch_5
    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const p0, 0x7f0839b3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v5

    invoke-virtual {v3, p0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2510348
    const-string v4, "tooltip_body"

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 2510349
    :pswitch_6
    const-string v4, "tooltip_body"

    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0839bc

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 2510350
    :pswitch_7
    const-string v4, "tooltip_body"

    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0839ac

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 2510351
    :pswitch_8
    const-string v4, "tooltip_body"

    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0839b4

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 2510352
    :pswitch_9
    const-string v4, "tooltip_title"

    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0839bd

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2510353
    const-string v4, "tooltip_body"

    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0839be

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 2510354
    :pswitch_a
    const-string v4, "tooltip_body"

    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0839ad

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 2510355
    :pswitch_b
    const-string v4, "tooltip_body"

    iget-object v3, v1, LX/J6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const v5, 0x7f0839b5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static a(LX/HqE;LX/HrJ;LX/8Pq;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/8Pr;Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)V
    .locals 1

    .prologue
    .line 2510420
    sget-object v0, LX/8Pr;->STICKY:LX/8Pr;

    if-ne p4, v0, :cond_0

    .line 2510421
    iget-object v0, p0, LX/HqE;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-virtual {v0, p3}, Lcom/facebook/privacy/PrivacyOperationsClient;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2510422
    :goto_0
    const/4 p4, 0x0

    .line 2510423
    iget-object v0, p1, LX/HrJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0, p3}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2510424
    iget-object v0, p1, LX/HrJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a()LX/AP3;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p0

    .line 2510425
    iput-object p0, v0, LX/AP3;->c:Ljava/lang/String;

    .line 2510426
    move-object v0, v0

    .line 2510427
    iput-boolean p4, v0, LX/AP3;->g:Z

    .line 2510428
    move-object v0, v0

    .line 2510429
    iput-boolean p4, v0, LX/AP3;->f:Z

    .line 2510430
    move-object v0, v0

    .line 2510431
    invoke-virtual {v0}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object p0

    .line 2510432
    iget-object v0, p1, LX/HrJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object p2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, p2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p0}, LX/0jL;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p4}, LX/0jL;->g(Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2510433
    return-void

    .line 2510434
    :cond_0
    invoke-direct {p0, p3}, LX/HqE;->c(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/HqE;Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2510411
    invoke-static {p0}, LX/HqE;->e(LX/HqE;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2510412
    :goto_0
    return v0

    .line 2510413
    :cond_0
    iget-object v0, p0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqT;

    .line 2510414
    if-nez v0, :cond_1

    move v0, v1

    .line 2510415
    goto :goto_0

    .line 2510416
    :cond_1
    invoke-virtual {v0}, LX/HqT;->e()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 2510417
    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2510418
    iget-object p0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, p0

    .line 2510419
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    if-ne v0, p1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a$redex0(LX/HqE;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2510402
    iget-object v0, p0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqT;

    .line 2510403
    if-nez v0, :cond_0

    move v0, v1

    .line 2510404
    :goto_0
    return v0

    .line 2510405
    :cond_0
    iget-object p0, v0, LX/HqT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object p0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2510406
    iget-object v0, p0, LX/AQ9;->R:LX/ARN;

    move-object p0, v0

    .line 2510407
    move-object v0, p0

    .line 2510408
    if-eqz v0, :cond_2

    .line 2510409
    if-eqz p1, :cond_1

    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2510410
    goto :goto_0
.end method

.method private c(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 2

    .prologue
    .line 2510398
    iget-object v0, p0, LX/HqE;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-virtual {v0, p1}, Lcom/facebook/privacy/PrivacyOperationsClient;->c(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2510399
    iget-object v0, p0, LX/HqE;->c:LX/339;

    .line 2510400
    iget-object v1, v0, LX/339;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object p0, LX/2bs;->m:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v1, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2510401
    return-void
.end method

.method public static e(LX/HqE;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2510388
    iget-object v0, p0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqT;

    .line 2510389
    if-nez v0, :cond_0

    move v0, v1

    .line 2510390
    :goto_0
    return v0

    .line 2510391
    :cond_0
    invoke-virtual {v0}, LX/HqT;->e()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v2

    .line 2510392
    invoke-virtual {v0}, LX/HqT;->d()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v3

    .line 2510393
    if-eqz v2, :cond_2

    iget-object v4, v2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v4, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2510394
    iget-object p0, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, p0

    .line 2510395
    if-eqz v4, :cond_2

    iget-object v2, v2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v2, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-boolean v2, v2, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    if-nez v2, :cond_1

    iget-boolean v2, v3, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->g:Z

    if-nez v2, :cond_1

    .line 2510396
    iget-object v2, v0, LX/HqT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v2}, LX/5Qu;->c()Z

    move-result v2

    move v0, v2

    .line 2510397
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2510381
    iget-object v0, p0, LX/HqE;->c:LX/339;

    .line 2510382
    iget-boolean v1, v0, LX/339;->h:Z

    move v0, v1

    .line 2510383
    if-eqz v0, :cond_0

    .line 2510384
    iget-object v0, p0, LX/HqE;->c:LX/339;

    sget-object v1, LX/5nW;->POSTED:LX/5nW;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    .line 2510385
    iget-object v0, p0, LX/HqE;->c:LX/339;

    const/4 v1, 0x0

    .line 2510386
    iput-boolean v1, v0, LX/339;->h:Z

    .line 2510387
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2510378
    iget-object v0, p0, LX/HqE;->c:LX/339;

    const/4 v1, 0x0

    .line 2510379
    iput-boolean v1, v0, LX/339;->h:Z

    .line 2510380
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 10
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2510356
    iget-object v0, p0, LX/HqE;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/HqE;->b:Z

    if-eqz v0, :cond_1

    .line 2510357
    :cond_0
    :goto_0
    return-void

    .line 2510358
    :cond_1
    iget-object v0, p0, LX/HqE;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HrJ;

    .line 2510359
    iget-object v1, p0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HqT;

    .line 2510360
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 2510361
    invoke-virtual {v1}, LX/HqT;->d()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v1

    .line 2510362
    iget-boolean v2, v1, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->f:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/HqE;->c:LX/339;

    iget-object v3, v1, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    .line 2510363
    iget-object v4, v2, LX/339;->m:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2c0;

    .line 2510364
    if-nez v4, :cond_5

    .line 2510365
    const/4 v4, 0x0

    .line 2510366
    :goto_1
    move v2, v4

    .line 2510367
    if-eqz v2, :cond_4

    .line 2510368
    if-nez p1, :cond_3

    .line 2510369
    iget-object v0, p0, LX/HqE;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "audience_educator_controller_null_selected_privacy"

    const-string v2, "Trying to set a privacy for this person but they don\'t have one!"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2510370
    :cond_2
    :goto_2
    iget-object v0, p0, LX/HqE;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    goto :goto_0

    .line 2510371
    :cond_3
    sget-object v6, LX/8Pq;->CHOSE_OPTION_FROM_SELECTOR:LX/8Pq;

    sget-object v8, LX/8Pr;->STICKY:LX/8Pr;

    move-object v4, p0

    move-object v5, v0

    move-object v7, p1

    move-object v9, v1

    invoke-static/range {v4 .. v9}, LX/HqE;->a(LX/HqE;LX/HrJ;LX/8Pq;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/8Pr;Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)V

    .line 2510372
    iget-object v4, v1, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    invoke-virtual {v0, v4}, LX/HrJ;->a(LX/2by;)V

    .line 2510373
    iget-object v0, p0, LX/HqE;->c:LX/339;

    .line 2510374
    iget-boolean v1, v0, LX/339;->h:Z

    move v0, v1

    .line 2510375
    if-eqz v0, :cond_2

    .line 2510376
    iget-object v0, p0, LX/HqE;->c:LX/339;

    sget-object v1, LX/5nW;->CUSTOM_SELECTION:LX/5nW;

    const-string v2, "traditional_composer"

    invoke-virtual {v0, v1, v2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    goto :goto_2

    .line 2510377
    :cond_4
    iget-object v1, v1, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    invoke-virtual {v0, v1}, LX/HrJ;->a(LX/2by;)V

    goto :goto_2

    :cond_5
    invoke-interface {v4}, LX/2c0;->c()Z

    move-result v4

    goto :goto_1
.end method
