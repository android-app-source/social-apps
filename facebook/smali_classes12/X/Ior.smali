.class public LX/Ior;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/IoU;


# instance fields
.field public a:Landroid/content/res/Resources;

.field public b:LX/J1j;

.field public c:LX/IoY;

.field public final d:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field public final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/user/tiles/UserTileView;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Landroid/widget/ProgressBar;

.field public final i:Lcom/facebook/resources/ui/FbTextView;

.field private final j:Lcom/facebook/resources/ui/FbButton;

.field public final k:Landroid/widget/LinearLayout;

.field public final l:Landroid/widget/LinearLayout;

.field private final m:Lcom/facebook/resources/ui/FbTextView;

.field private final n:Lcom/facebook/resources/ui/FbTextView;

.field private final o:Lcom/facebook/messaging/payment/value/input/MemoInputView;

.field public p:LX/Io1;

.field public q:LX/J1i;

.field public r:LX/Ios;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2612329
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Ior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2612330
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2612327
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2612328
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2612307
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2612308
    const-class v0, LX/Ior;

    invoke-static {v0, p0}, LX/Ior;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2612309
    const v0, 0x7f030de3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2612310
    const v0, 0x7f0d2202

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, LX/Ior;->d:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2612311
    const v0, 0x7f0d2201

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Ior;->e:LX/0am;

    .line 2612312
    const v0, 0x7f0d21ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Ior;->f:LX/0am;

    .line 2612313
    const v0, 0x7f0d2200

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Ior;->g:LX/0am;

    .line 2612314
    const v0, 0x7f0d2204

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Ior;->h:Landroid/widget/ProgressBar;

    .line 2612315
    const v0, 0x7f0d2209

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ior;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2612316
    const v0, 0x7f0d220a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Ior;->j:Lcom/facebook/resources/ui/FbButton;

    .line 2612317
    const v0, 0x7f0d2208

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Ior;->k:Landroid/widget/LinearLayout;

    .line 2612318
    const v0, 0x7f0d2205

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Ior;->l:Landroid/widget/LinearLayout;

    .line 2612319
    const v0, 0x7f0d2206

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ior;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2612320
    const v0, 0x7f0d2207

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ior;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2612321
    const v0, 0x7f0d220b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iput-object v0, p0, LX/Ior;->o:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    .line 2612322
    iget-object v0, p0, LX/Ior;->d:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b()V

    .line 2612323
    iget-object v0, p0, LX/Ior;->d:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setLongClickable(Z)V

    .line 2612324
    iget-object v0, p0, LX/Ior;->j:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Iol;

    invoke-direct {v1, p0}, LX/Iol;-><init>(LX/Ior;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612325
    iget-object v0, p0, LX/Ior;->n:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/Iom;

    invoke-direct {v1, p0}, LX/Iom;-><init>(LX/Ior;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612326
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ior;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const-class v2, LX/J1j;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/J1j;

    invoke-static {p0}, LX/IoY;->b(LX/0QB;)LX/IoY;

    move-result-object p0

    check-cast p0, LX/IoY;

    iput-object v1, p1, LX/Ior;->a:Landroid/content/res/Resources;

    iput-object v2, p1, LX/Ior;->b:LX/J1j;

    iput-object p0, p1, LX/Ior;->c:LX/IoY;

    return-void
.end method

.method public static setHeaderVisibility(LX/Ior;I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2612300
    sparse-switch p1, :sswitch_data_0

    .line 2612301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected value for visibility "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2612302
    :sswitch_0
    iget-object v0, p0, LX/Ior;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2612303
    iget-object v0, p0, LX/Ior;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612304
    :goto_0
    return-void

    .line 2612305
    :sswitch_1
    iget-object v0, p0, LX/Ior;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2612306
    iget-object v0, p0, LX/Ior;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static setPaymentCardInfoVisibility(LX/Ior;I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2612290
    iget-object v0, p0, LX/Ior;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612291
    if-nez p1, :cond_0

    .line 2612292
    iget-object v0, p0, LX/Ior;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612293
    iget-object v0, p0, LX/Ior;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612294
    :goto_0
    return-void

    .line 2612295
    :cond_0
    iget-object v0, p0, LX/Ior;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612296
    iget-object v0, p0, LX/Ior;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612297
    iget-object v0, p0, LX/Ior;->m:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082c85

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2612298
    iget-object v0, p0, LX/Ior;->m:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f020e62

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2612299
    iget-object v0, p0, LX/Ior;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2612331
    iget-object v0, p0, LX/Ior;->q:LX/J1i;

    invoke-virtual {v0}, LX/J1i;->a()V

    .line 2612332
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 6
    .param p1    # Landroid/view/MenuItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2612288
    iget-object v0, p0, LX/Ior;->c:LX/IoY;

    iget-object v1, p0, LX/Ior;->r:LX/Ios;

    iget-object v2, v1, LX/Ios;->b:LX/0am;

    iget-object v1, p0, LX/Ior;->r:LX/Ios;

    iget-object v3, v1, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    iget-object v1, p0, LX/Ior;->r:LX/Ios;

    iget-object v4, v1, LX/Ios;->a:LX/IoT;

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/IoY;->a(Landroid/view/MenuItem;LX/0am;Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;LX/IoT;Z)V

    .line 2612289
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2612287
    return-void
.end method

.method public getImmediateFocusView()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2612286
    iget-object v0, p0, LX/Ior;->d:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    return-object v0
.end method

.method public setListener(LX/Io1;)V
    .locals 2

    .prologue
    .line 2612283
    iput-object p1, p0, LX/Ior;->p:LX/Io1;

    .line 2612284
    iget-object v0, p0, LX/Ior;->o:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    new-instance v1, LX/Ion;

    invoke-direct {v1, p0}, LX/Ion;-><init>(LX/Ior;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MemoInputView;->setListener(LX/IoG;)V

    .line 2612285
    return-void
.end method

.method public setMessengerPayViewParams(LX/Ios;)V
    .locals 4

    .prologue
    .line 2612248
    check-cast p1, LX/Ios;

    iput-object p1, p0, LX/Ior;->r:LX/Ios;

    .line 2612249
    iget-object v0, p0, LX/Ior;->q:LX/J1i;

    if-eqz v0, :cond_1

    .line 2612250
    :goto_0
    const/16 p1, 0x8

    const/4 v3, 0x0

    .line 2612251
    iget-object v0, p0, LX/Ior;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ior;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->d:Lcom/facebook/user/model/UserKey;

    if-eqz v0, :cond_0

    .line 2612252
    iget-object v0, p0, LX/Ior;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iget-object v1, p0, LX/Ior;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->d:Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/8ue;->NONE:LX/8ue;

    invoke-static {v1, v2}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2612253
    iget-object v0, p0, LX/Ior;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v3}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2612254
    :cond_0
    iget-object v0, p0, LX/Ior;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2612255
    :goto_1
    iget-object v0, p0, LX/Ior;->q:LX/J1i;

    iget-object v1, p0, LX/Ior;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612256
    iget-object v2, v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2612257
    invoke-virtual {v0, v1}, LX/J1i;->a(Ljava/lang/String;)V

    .line 2612258
    sget-object v0, LX/Ioq;->a:[I

    iget-object v1, p0, LX/Ior;->r:LX/Ios;

    .line 2612259
    iget-object v2, v1, LX/Ios;->a:LX/IoT;

    move-object v1, v2

    .line 2612260
    invoke-virtual {v1}, LX/IoT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2612261
    iget-object v0, p0, LX/Ior;->q:LX/J1i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/J1i;->a(Z)V

    .line 2612262
    :goto_2
    const/4 v3, 0x0

    const/16 v1, 0x8

    .line 2612263
    iget-object v0, p0, LX/Ior;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    if-nez v0, :cond_6

    .line 2612264
    iget-object v0, p0, LX/Ior;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612265
    iget-object v0, p0, LX/Ior;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612266
    iget-object v0, p0, LX/Ior;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612267
    :goto_3
    return-void

    .line 2612268
    :cond_1
    iget-object v0, p0, LX/Ior;->b:LX/J1j;

    new-instance v1, LX/Iop;

    invoke-direct {v1, p0}, LX/Iop;-><init>(LX/Ior;)V

    const/4 v2, 0x0

    iget-object v3, p0, LX/Ior;->r:LX/Ios;

    iget-object v3, v3, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612269
    iget-object p1, v3, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v3, p1

    .line 2612270
    invoke-virtual {v0, v1, v2, v3}, LX/J1j;->a(LX/Ioo;ZLjava/lang/String;)LX/J1i;

    move-result-object v0

    iput-object v0, p0, LX/Ior;->q:LX/J1i;

    .line 2612271
    iget-object v0, p0, LX/Ior;->q:LX/J1i;

    iget-object v1, p0, LX/Ior;->d:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0, v1}, LX/J1i;->a(Lcom/facebook/payments/p2p/ui/DollarIconEditText;)V

    goto/16 :goto_0

    .line 2612272
    :cond_2
    iget-object v0, p0, LX/Ior;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->c:Lcom/facebook/user/model/Name;

    if-nez v0, :cond_3

    .line 2612273
    invoke-static {p0, p1}, LX/Ior;->setHeaderVisibility(LX/Ior;I)V

    goto :goto_1

    .line 2612274
    :cond_3
    iget-object v0, p0, LX/Ior;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->c:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->h()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/Ior;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->c:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2612275
    :cond_4
    iget-object v0, p0, LX/Ior;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Ior;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->c:Lcom/facebook/user/model/Name;

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2612276
    invoke-static {p0, v3}, LX/Ior;->setHeaderVisibility(LX/Ior;I)V

    goto/16 :goto_1

    .line 2612277
    :cond_5
    invoke-static {p0, p1}, LX/Ior;->setHeaderVisibility(LX/Ior;I)V

    goto/16 :goto_1

    .line 2612278
    :pswitch_0
    iget-object v0, p0, LX/Ior;->q:LX/J1i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/J1i;->a(Z)V

    goto :goto_2

    .line 2612279
    :cond_6
    iget-object v0, p0, LX/Ior;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2612280
    invoke-static {p0, v1}, LX/Ior;->setPaymentCardInfoVisibility(LX/Ior;I)V

    goto :goto_3

    .line 2612281
    :cond_7
    iget-object v1, p0, LX/Ior;->i:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, LX/Ior;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    iget-object v2, p0, LX/Ior;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/p2p/model/PaymentCard;->c(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2612282
    invoke-static {p0, v3}, LX/Ior;->setPaymentCardInfoVisibility(LX/Ior;I)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
