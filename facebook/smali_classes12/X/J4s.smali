.class public final LX/J4s;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupMediaOnlyQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2644633
    const-class v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupMediaOnlyQueryModel;

    const v0, -0x4835ebde

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "FetchProfilePhotoCheckupMediaOnlyQuery"

    const-string v6, "cacfa5e9ff05b25f1e9d6229ab521931"

    const-string v7, "viewer"

    const-string v8, "10155207367866729"

    const/4 v9, 0x0

    .line 2644634
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2644635
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2644636
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2644611
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2644612
    sparse-switch v0, :sswitch_data_0

    .line 2644613
    :goto_0
    return-object p1

    .line 2644614
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2644615
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2644616
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2644617
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2644618
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2644619
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2644620
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2644621
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2644622
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2644623
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2644624
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2644625
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2644626
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2644627
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2644628
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2644629
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2644630
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2644631
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2644632
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x750cfbaa -> :sswitch_3
        -0x6a24640d -> :sswitch_12
        -0x680de62a -> :sswitch_b
        -0x6326fdb3 -> :sswitch_9
        -0x51484e72 -> :sswitch_4
        -0x4496acc9 -> :sswitch_c
        -0x421ba035 -> :sswitch_a
        -0x3c54de38 -> :sswitch_10
        -0x2c889631 -> :sswitch_f
        -0x2a0a3d40 -> :sswitch_6
        -0x1b87b280 -> :sswitch_8
        -0x12efdeb3 -> :sswitch_d
        -0x93a55fc -> :sswitch_7
        0x101fb19 -> :sswitch_2
        0xa1fa812 -> :sswitch_1
        0x214100e0 -> :sswitch_e
        0x40c8444f -> :sswitch_5
        0x73a026b5 -> :sswitch_11
        0x7c7626df -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2644607
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2644608
    :goto_1
    return v0

    .line 2644609
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2644610
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
