.class public final LX/IDV;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendlist/fragment/FriendListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V
    .locals 0

    .prologue
    .line 2550919
    iput-object p1, p0, LX/IDV;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2550908
    check-cast p1, LX/2f2;

    .line 2550909
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/IDV;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    if-nez v0, :cond_1

    .line 2550910
    :cond_0
    :goto_0
    return-void

    .line 2550911
    :cond_1
    iget-object v0, p0, LX/IDV;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    invoke-virtual {v0}, LX/IE2;->c()Ljava/util/Map;

    move-result-object v0

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IDH;

    .line 2550912
    if-eqz v0, :cond_0

    .line 2550913
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, LX/IDH;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 2550914
    iget-object v1, p0, LX/IDV;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/friendlist/fragment/FriendListFragment;->a$redex0(Lcom/facebook/friendlist/fragment/FriendListFragment;Ljava/lang/String;)V

    .line 2550915
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, LX/IDH;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2550916
    iget-object v0, p0, LX/IDV;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    const v1, -0x60614af0

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2550917
    :cond_2
    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_0

    .line 2550918
    iget-object v0, p0, LX/IDV;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    const v1, 0x2f1b3a25

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
