.class public LX/IXr;
.super Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;
.source ""

# interfaces
.implements LX/CsR;
.implements LX/Csi;


# instance fields
.field public a:Lcom/facebook/instantarticles/view/ShareBar;

.field public b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2586887
    invoke-direct {p0, p1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2586888
    invoke-direct {p0}, LX/IXr;->f()V

    .line 2586889
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2586897
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2586898
    invoke-direct {p0}, LX/IXr;->f()V

    .line 2586899
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2586894
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2586895
    invoke-direct {p0}, LX/IXr;->f()V

    .line 2586896
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 2586890
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    move-object v0, v0

    .line 2586891
    new-instance v1, LX/CqW;

    invoke-virtual {p0}, LX/IXr;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CqW;-><init>(Landroid/content/Context;)V

    .line 2586892
    iput-object v1, v0, LX/7yl;->g:LX/7yk;

    .line 2586893
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 1

    .prologue
    .line 2586867
    iget-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    if-eqz v0, :cond_0

    .line 2586868
    iget-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;->a()V

    .line 2586869
    :cond_0
    return-void
.end method

.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 2586886
    return-object p0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 2586883
    iget-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    if-eqz v0, :cond_0

    .line 2586884
    iget-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;->a(F)V

    .line 2586885
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2586900
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2586880
    iget-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    if-eqz v0, :cond_0

    .line 2586881
    iget-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;->b()V

    .line 2586882
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2586879
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2586876
    iget-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    if-eqz v0, :cond_0

    .line 2586877
    iget-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;->e()V

    .line 2586878
    :cond_0
    return-void
.end method

.method public getFragmentPager()LX/CqD;
    .locals 1

    .prologue
    .line 2586875
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x14e27592

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2586871
    invoke-super {p0}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->onAttachedToWindow()V

    .line 2586872
    const v0, 0x7f0d167d

    invoke-virtual {p0, v0}, LX/IXr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/ShareBar;

    iput-object v0, p0, LX/IXr;->a:Lcom/facebook/instantarticles/view/ShareBar;

    .line 2586873
    const v0, 0x7f0d167c

    invoke-virtual {p0, v0}, LX/IXr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    iput-object v0, p0, LX/IXr;->b:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    .line 2586874
    const/16 v0, 0x2d

    const v2, 0x48cd93c3

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setFragmentPager(LX/CqD;)V
    .locals 0

    .prologue
    .line 2586870
    return-void
.end method
