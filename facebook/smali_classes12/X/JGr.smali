.class public final LX/JGr;
.super Ljava/lang/ref/WeakReference;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ref/WeakReference",
        "<",
        "LX/JGs;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/JGs;)V
    .locals 0

    .prologue
    .line 2669092
    invoke-direct {p0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2669093
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2669094
    invoke-virtual {p0}, LX/JGr;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGs;

    .line 2669095
    if-eqz v0, :cond_0

    .line 2669096
    invoke-virtual {v0}, LX/JGs;->invalidate()V

    .line 2669097
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 2669098
    invoke-virtual {p0}, LX/JGr;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGs;

    .line 2669099
    if-nez v0, :cond_0

    .line 2669100
    :goto_0
    return-void

    .line 2669101
    :cond_0
    invoke-virtual {v0}, LX/JGs;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    .line 2669102
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2669103
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2669104
    new-instance v1, LX/K0K;

    invoke-direct {v1, p1, p2}, LX/K0K;-><init>(II)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    goto :goto_0
.end method
