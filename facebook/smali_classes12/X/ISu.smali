.class public final LX/ISu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V
    .locals 0

    .prologue
    .line 2578475
    iput-object p1, p0, LX/ISu;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x27504dd0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2578476
    const-string v0, "extra_request_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2578477
    if-nez v2, :cond_0

    .line 2578478
    sget-object v0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->t:Ljava/lang/String;

    const-string v2, "There is no composer session id"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578479
    const/16 v0, 0x27

    const v2, -0x1ba1b770

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2578480
    :goto_0
    return-void

    .line 2578481
    :cond_0
    iget-object v0, p0, LX/ISu;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CGV;

    .line 2578482
    iget-object v3, v0, LX/CGV;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2578483
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2578484
    iget-object v0, p0, LX/ISu;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->mJ_()V

    .line 2578485
    iget-object v0, p0, LX/ISu;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->v(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V

    .line 2578486
    :cond_1
    const v0, -0x371557b0    # -480578.5f

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0
.end method
