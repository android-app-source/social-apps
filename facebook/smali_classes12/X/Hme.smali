.class public final enum LX/Hme;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hme;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hme;

.field public static final enum REF_BOOKMARK:LX/Hme;


# instance fields
.field public final tag:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2500939
    new-instance v0, LX/Hme;

    const-string v1, "REF_BOOKMARK"

    const-string v2, "ref_bookmark"

    invoke-direct {v0, v1, v3, v2}, LX/Hme;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hme;->REF_BOOKMARK:LX/Hme;

    .line 2500940
    const/4 v0, 0x1

    new-array v0, v0, [LX/Hme;

    sget-object v1, LX/Hme;->REF_BOOKMARK:LX/Hme;

    aput-object v1, v0, v3

    sput-object v0, LX/Hme;->$VALUES:[LX/Hme;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2500941
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2500942
    iput-object p3, p0, LX/Hme;->tag:Ljava/lang/String;

    .line 2500943
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hme;
    .locals 1

    .prologue
    .line 2500944
    const-class v0, LX/Hme;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hme;

    return-object v0
.end method

.method public static values()[LX/Hme;
    .locals 1

    .prologue
    .line 2500945
    sget-object v0, LX/Hme;->$VALUES:[LX/Hme;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hme;

    return-object v0
.end method
