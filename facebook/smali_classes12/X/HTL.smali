.class public final LX/HTL;
.super LX/HTK;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public l:I

.field public m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/HTJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;LX/HTJ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2469402
    invoke-direct {p0, p1}, LX/HTK;-><init>(Landroid/view/View;)V

    .line 2469403
    const/4 v0, -0x1

    iput v0, p0, LX/HTL;->l:I

    .line 2469404
    iput-object v1, p0, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    .line 2469405
    iput-object v1, p0, LX/HTL;->n:LX/HTJ;

    .line 2469406
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2469407
    iput-object p2, p0, LX/HTL;->n:LX/HTJ;

    .line 2469408
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2469409
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5c2c78c9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2469410
    iget-object v1, p0, LX/HTL;->n:LX/HTJ;

    if-eqz v1, :cond_0

    .line 2469411
    iget-object v1, p0, LX/HTL;->n:LX/HTJ;

    iget-object v2, p0, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    const/4 p0, 0x0

    .line 2469412
    iget-object v5, v1, LX/HTJ;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v6, v5, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->e:LX/H6T;

    iget-object v5, v1, LX/HTJ;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;

    invoke-virtual {v5}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;

    invoke-virtual {v5}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;->l()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$RootShareStoryModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$RootShareStoryModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/H6T;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x1

    const/4 p1, 0x0

    .line 2469413
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    move p0, v1

    :goto_0
    invoke-static {p0}, LX/0Tp;->b(Z)V

    .line 2469414
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 2469415
    sget-object p0, LX/0ax;->hF:Ljava/lang/String;

    const-string v1, "0"

    const-string p1, "0"

    invoke-static {p0, v8, v5, v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2469416
    iget-object v1, v6, LX/H6T;->j:LX/17Y;

    invoke-interface {v1, v7, p0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 2469417
    const/high16 v1, 0x10000000

    invoke-virtual {p0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2469418
    iget-object v1, v6, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, p0, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2469419
    :cond_0
    const v1, -0x4f4306de

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move p0, p1

    .line 2469420
    goto :goto_0

    :cond_2
    move v1, p1

    .line 2469421
    goto :goto_1
.end method
