.class public final LX/IEK;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2551802
    const-class v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;

    const v0, 0x5d4fee18

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchSuggestionsFriendListQuery"

    const-string v6, "1f7f11b03f8cde8b91246a07e4f965f1"

    const-string v7, "user"

    const-string v8, "10155192631691729"

    const-string v9, "10155259088341729"

    .line 2551803
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2551804
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2551805
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2551806
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2551807
    sparse-switch v0, :sswitch_data_0

    .line 2551808
    :goto_0
    return-object p1

    .line 2551809
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2551810
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2551811
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2551812
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2551813
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2551814
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x76d1794f -> :sswitch_5
        -0x41b8e48f -> :sswitch_0
        -0x295975c2 -> :sswitch_2
        0xa174e6b -> :sswitch_3
        0x21beac6a -> :sswitch_1
        0x6c6f579a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2551815
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 2551816
    :goto_1
    return v0

    .line 2551817
    :pswitch_1
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 2551818
    :pswitch_4
    const/16 v0, 0x14

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2551819
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2551820
    :pswitch_6
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
