.class public final LX/IVv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bsv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bsv",
        "<",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;)V
    .locals 0

    .prologue
    .line 2583005
    iput-object p1, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)V
    .locals 2

    .prologue
    .line 2583006
    iget-object v0, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->h:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->g:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2583007
    iget-object v0, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->i:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583008
    iget-object v0, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->j:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583009
    iget-object v0, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->k:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583010
    iget-object v0, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->m:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583011
    iget-object v0, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->n:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583012
    iget-object v0, p0, LX/IVv;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->o:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583013
    return-void
.end method
