.class public final LX/Ikg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V
    .locals 0

    .prologue
    .line 2606947
    iput-object p1, p0, LX/Ikg;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x5bc235c

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2606948
    iget-object v1, p0, LX/Ikg;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    iget-object v1, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->g:LX/IkZ;

    if-eqz v1, :cond_0

    .line 2606949
    iget-object v1, p0, LX/Ikg;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    iget-object v1, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->g:LX/IkZ;

    .line 2606950
    iget-object v3, v1, LX/IkZ;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    iget-object v3, v3, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->g:LX/Ike;

    iget-object v4, v1, LX/IkZ;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 2606951
    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/CharSequence;

    const/4 p1, 0x0

    const v1, 0x7f082d45

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p0, p1

    const/4 p1, 0x1

    const v1, 0x7f082d46

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p0, p1

    .line 2606952
    new-instance p1, LX/0ju;

    invoke-direct {p1, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f082d4d

    invoke-virtual {p1, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object p1

    new-instance v1, LX/Ikd;

    invoke-direct {v1, v3}, LX/Ikd;-><init>(LX/Ike;)V

    invoke-virtual {p1, p0, v1}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0}, LX/0ju;->b()LX/2EJ;

    .line 2606953
    :cond_0
    const v1, -0x27b7346d

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
