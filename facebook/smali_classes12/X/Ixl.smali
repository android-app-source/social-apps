.class public LX/Ixl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Void:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/IxS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2632284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2632285
    return-void
.end method

.method public static a(LX/0QB;)LX/Ixl;
    .locals 4

    .prologue
    .line 2632286
    const-class v1, LX/Ixl;

    monitor-enter v1

    .line 2632287
    :try_start_0
    sget-object v0, LX/Ixl;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2632288
    sput-object v2, LX/Ixl;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2632289
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2632290
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2632291
    new-instance p0, LX/Ixl;

    invoke-direct {p0}, LX/Ixl;-><init>()V

    .line 2632292
    invoke-static {v0}, LX/IxS;->a(LX/0QB;)LX/IxS;

    move-result-object v3

    check-cast v3, LX/IxS;

    .line 2632293
    iput-object v3, p0, LX/Ixl;->a:LX/IxS;

    .line 2632294
    move-object v0, p0

    .line 2632295
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2632296
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ixl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2632297
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2632298
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
