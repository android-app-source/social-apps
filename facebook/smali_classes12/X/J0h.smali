.class public LX/J0h;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Duh;


# direct methods
.method public constructor <init>(LX/Duh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637593
    iput-object p1, p0, LX/J0h;->a:LX/Duh;

    .line 2637594
    return-void
.end method

.method public static b(LX/0QB;)LX/J0h;
    .locals 2

    .prologue
    .line 2637595
    new-instance v1, LX/J0h;

    invoke-static {p0}, LX/Duh;->a(LX/0QB;)LX/Duh;

    move-result-object v0

    check-cast v0, LX/Duh;

    invoke-direct {v1, v0}, LX/J0h;-><init>(LX/Duh;)V

    .line 2637596
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;)Lcom/facebook/payments/p2p/model/PaymentTransaction;
    .locals 7

    .prologue
    .line 2637597
    invoke-static {}, Lcom/facebook/payments/p2p/model/PaymentTransaction;->newBuilder()LX/DtJ;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2637598
    iput-object v1, v0, LX/DtJ;->a:Ljava/lang/String;

    .line 2637599
    move-object v0, v0

    .line 2637600
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/DtM;->fromString(Ljava/lang/String;)LX/DtM;

    move-result-object v1

    .line 2637601
    iput-object v1, v0, LX/DtJ;->b:LX/DtM;

    .line 2637602
    move-object v0, v0

    .line 2637603
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->kj_()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2637604
    iput-object v1, v0, LX/DtJ;->e:Ljava/lang/String;

    .line 2637605
    move-object v0, v0

    .line 2637606
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->r()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2637607
    iput-object v1, v0, LX/DtJ;->h:Ljava/lang/String;

    .line 2637608
    move-object v0, v0

    .line 2637609
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->y()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v1

    .line 2637610
    iput-object v1, v0, LX/DtJ;->k:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2637611
    move-object v0, v0

    .line 2637612
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->ki_()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2637613
    iput-object v1, v0, LX/DtJ;->g:Ljava/lang/String;

    .line 2637614
    move-object v0, v0

    .line 2637615
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2637616
    iput-object v1, v0, LX/DtJ;->n:Ljava/lang/String;

    .line 2637617
    move-object v6, v0

    .line 2637618
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2637619
    new-instance v0, Lcom/facebook/payments/p2p/model/Sender;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->x()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->d()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/payments/p2p/model/Sender;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2637620
    iput-object v0, v6, LX/DtJ;->c:Lcom/facebook/payments/p2p/model/Sender;

    .line 2637621
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2637622
    new-instance v0, Lcom/facebook/payments/p2p/model/Receiver;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->d()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/payments/p2p/model/Receiver;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2637623
    iput-object v0, v6, LX/DtJ;->d:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2637624
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2637625
    new-instance v0, Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->c()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->s()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->a()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    .line 2637626
    iput-object v0, v6, LX/DtJ;->i:Lcom/facebook/payments/p2p/model/Amount;

    .line 2637627
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2637628
    new-instance v0, Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->c()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->t()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->a()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    .line 2637629
    iput-object v0, v6, LX/DtJ;->j:Lcom/facebook/payments/p2p/model/Amount;

    .line 2637630
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->n()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->p()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2637631
    iget-object v0, p0, LX/J0h;->a:LX/Duh;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->w()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Duh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->n()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/DtQ;->fromString(Ljava/lang/String;)LX/DtQ;

    move-result-object v0

    .line 2637632
    :goto_0
    iput-object v0, v6, LX/DtJ;->f:LX/DtQ;

    .line 2637633
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->v()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2637634
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->v()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v0

    .line 2637635
    iput-object v0, v6, LX/DtJ;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2637636
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2637637
    new-instance v0, Lcom/facebook/payments/p2p/model/CommerceOrder;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;->kd_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->u()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$CommerceOrderModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/payments/p2p/model/CommerceOrder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2637638
    iput-object v0, v6, LX/DtJ;->m:Lcom/facebook/payments/p2p/model/CommerceOrder;

    .line 2637639
    :cond_6
    invoke-virtual {v6}, LX/DtJ;->o()Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v0

    return-object v0

    .line 2637640
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;->p()Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/DtQ;->fromString(Ljava/lang/String;)LX/DtQ;

    move-result-object v0

    goto :goto_0
.end method
