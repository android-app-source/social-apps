.class public final LX/IvJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/notifications/lockscreenservice/LockScreenService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;II)V
    .locals 0

    .prologue
    .line 2628297
    iput-object p1, p0, LX/IvJ;->c:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iput p2, p0, LX/IvJ;->a:I

    iput p3, p0, LX/IvJ;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    .prologue
    .line 2628298
    if-ne p3, p7, :cond_0

    if-eq p4, p8, :cond_1

    .line 2628299
    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2628300
    iget-object v0, p0, LX/IvJ;->c:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2628301
    iget-object v2, p0, LX/IvJ;->c:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v2, v2, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 2628302
    iget v2, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    iget v4, p0, LX/IvJ;->a:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2628303
    iget v2, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget v3, p0, LX/IvJ;->b:I

    sub-int/2addr v0, v3

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 2628304
    iget-object v0, p0, LX/IvJ;->c:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    new-instance v2, Landroid/view/TouchDelegate;

    iget-object v3, p0, LX/IvJ;->c:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v3, v3, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    invoke-direct {v2, v1, v3}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 2628305
    :cond_1
    return-void
.end method
