.class public final LX/JEH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 2665749
    const/16 v16, 0x0

    .line 2665750
    const-wide/16 v14, 0x0

    .line 2665751
    const/4 v13, 0x0

    .line 2665752
    const/4 v12, 0x0

    .line 2665753
    const/4 v11, 0x0

    .line 2665754
    const/4 v10, 0x0

    .line 2665755
    const/4 v9, 0x0

    .line 2665756
    const/4 v8, 0x0

    .line 2665757
    const/4 v7, 0x0

    .line 2665758
    const/4 v6, 0x0

    .line 2665759
    const/4 v5, 0x0

    .line 2665760
    const/4 v4, 0x0

    .line 2665761
    const/4 v3, 0x0

    .line 2665762
    const/4 v2, 0x0

    .line 2665763
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_11

    .line 2665764
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2665765
    const/4 v2, 0x0

    .line 2665766
    :goto_0
    return v2

    .line 2665767
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_b

    .line 2665768
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2665769
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2665770
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v19, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 2665771
    const-string v6, "__type__"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "__typename"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2665772
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 2665773
    :cond_2
    const-string v6, "created_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2665774
    const/4 v2, 0x1

    .line 2665775
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2665776
    :cond_3
    const-string v6, "creation_story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2665777
    invoke-static/range {p0 .. p1}, LX/JEB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2665778
    :cond_4
    const-string v6, "is_video_broadcast"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2665779
    const/4 v2, 0x1

    .line 2665780
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v2

    move/from16 v17, v6

    goto :goto_1

    .line 2665781
    :cond_5
    const-string v6, "live_viewer_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2665782
    const/4 v2, 0x1

    .line 2665783
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v10, v2

    move/from16 v16, v6

    goto :goto_1

    .line 2665784
    :cond_6
    const-string v6, "peak_viewer_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2665785
    const/4 v2, 0x1

    .line 2665786
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move v15, v6

    goto/16 :goto_1

    .line 2665787
    :cond_7
    const-string v6, "playable_duration_in_ms"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2665788
    const/4 v2, 0x1

    .line 2665789
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v14, v6

    goto/16 :goto_1

    .line 2665790
    :cond_8
    const-string v6, "profile_picture"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2665791
    invoke-static/range {p0 .. p1}, LX/JEC;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2665792
    :cond_9
    const-string v6, "video_insights"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2665793
    invoke-static/range {p0 .. p1}, LX/JEG;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2665794
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2665795
    :cond_b
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2665796
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2665797
    if-eqz v3, :cond_c

    .line 2665798
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2665799
    :cond_c
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2665800
    if-eqz v11, :cond_d

    .line 2665801
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2665802
    :cond_d
    if-eqz v10, :cond_e

    .line 2665803
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2665804
    :cond_e
    if-eqz v9, :cond_f

    .line 2665805
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 2665806
    :cond_f
    if-eqz v8, :cond_10

    .line 2665807
    const/4 v2, 0x6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 2665808
    :cond_10
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2665809
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2665810
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v17, v12

    move/from16 v18, v13

    move v12, v7

    move v13, v8

    move v8, v2

    move/from16 v7, v16

    move/from16 v16, v11

    move v11, v5

    move/from16 v20, v10

    move v10, v4

    move-wide v4, v14

    move v14, v9

    move/from16 v15, v20

    move v9, v3

    move v3, v6

    goto/16 :goto_1
.end method
