.class public LX/HMV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HMI;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/8Do;

.field private final f:LX/HDT;

.field private final g:LX/01T;

.field private final h:Landroid/content/Context;

.field private final i:Landroid/content/res/Resources;

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/HDT;LX/01T;Landroid/content/Context;Landroid/content/res/Resources;LX/8Do;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;",
            "LX/0ad;",
            "LX/HDT;",
            "LX/01T;",
            "Landroid/content/Context;",
            "Landroid/content/res/Resources;",
            "LX/8Do;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2457205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457206
    iput-boolean v0, p0, LX/HMV;->j:Z

    .line 2457207
    iput-boolean v0, p0, LX/HMV;->k:Z

    .line 2457208
    iput-object p1, p0, LX/HMV;->a:LX/0Ot;

    .line 2457209
    iput-object p2, p0, LX/HMV;->b:LX/0Ot;

    .line 2457210
    iput-object p3, p0, LX/HMV;->c:LX/0Ot;

    .line 2457211
    iput-object p4, p0, LX/HMV;->d:LX/0Ot;

    .line 2457212
    iput-object p10, p0, LX/HMV;->e:LX/8Do;

    .line 2457213
    iput-object p6, p0, LX/HMV;->f:LX/HDT;

    .line 2457214
    iput-object p7, p0, LX/HMV;->g:LX/01T;

    .line 2457215
    iput-object p8, p0, LX/HMV;->h:Landroid/content/Context;

    .line 2457216
    iput-object p9, p0, LX/HMV;->i:Landroid/content/res/Resources;

    .line 2457217
    return-void
.end method


# virtual methods
.method public final a()LX/4At;
    .locals 4

    .prologue
    .line 2457204
    new-instance v0, LX/4At;

    iget-object v1, p0, LX/HMV;->h:Landroid/content/Context;

    iget-object v2, p0, LX/HMV;->i:Landroid/content/res/Resources;

    const v3, 0x7f081412

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p3    # LX/8A4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/8A4;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "Landroid/content/Intent;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2457189
    const-string v0, "is_uploading_media"

    invoke-virtual {p5, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2457190
    iget-object v0, p0, LX/HMV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p5}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2457191
    iget-object v0, p0, LX/HMV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081419

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2457192
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2457193
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2457194
    :goto_0
    return-object v0

    .line 2457195
    :cond_0
    const-string v0, "publishPostParams"

    invoke-virtual {p5, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2457196
    iget-object v3, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    sget-object v4, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    sget-object v4, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    if-ne v3, v4, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    iput-boolean v1, p0, LX/HMV;->j:Z

    .line 2457197
    if-eqz p3, :cond_4

    sget-object v1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {p3, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2457198
    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    sget-object v3, LX/5Rn;->NORMAL:LX/5Rn;

    if-ne v1, v3, :cond_3

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    sget-object v1, LX/2rt;->SHARE:LX/2rt;

    if-eq v0, v1, :cond_3

    .line 2457199
    iput-boolean v2, p0, LX/HMV;->k:Z

    .line 2457200
    :cond_3
    const-string v0, "publishPostParams"

    invoke-virtual {p5, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2457201
    iget-object v1, p0, LX/HMV;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iz;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v0, p4}, LX/2iz;->b(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z

    .line 2457202
    iget-object v0, p0, LX/HMV;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iH;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/HMU;

    invoke-direct {v1, p0, p5}, LX/HMU;-><init>(LX/HMV;Landroid/content/Intent;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2457203
    :cond_4
    iget-object v0, p0, LX/HMV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p5}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 2457218
    iget-boolean v0, p0, LX/HMV;->j:Z

    if-eqz v0, :cond_0

    .line 2457219
    iget-object v0, p0, LX/HMV;->f:LX/HDT;

    new-instance v1, LX/HDb;

    invoke-direct {v1}, LX/HDb;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2457220
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2457187
    iget-object v0, p0, LX/HMV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081407

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2457188
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2457186
    iget-boolean v0, p0, LX/HMV;->k:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2457185
    const/4 v0, 0x1

    return v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457184
    const/16 v0, 0x6dc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, LX/Cfc;->WRITE_POST_TO_PAGE_TAP:LX/Cfc;

    invoke-virtual {v1}, LX/Cfc;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, LX/Cfc;->SHARE_PHOTO_TO_PAGE_TAP:LX/Cfc;

    invoke-virtual {v2}, LX/Cfc;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
