.class public final LX/JSF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JSO;

.field public final synthetic b:LX/JTY;

.field public final synthetic c:LX/JSe;

.field public final synthetic d:LX/JSG;


# direct methods
.method public constructor <init>(LX/JSG;LX/JSO;LX/JTY;LX/JSe;)V
    .locals 0

    .prologue
    .line 2694933
    iput-object p1, p0, LX/JSF;->d:LX/JSG;

    iput-object p2, p0, LX/JSF;->a:LX/JSO;

    iput-object p3, p0, LX/JSF;->b:LX/JTY;

    iput-object p4, p0, LX/JSF;->c:LX/JSe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5f809c6d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2694934
    iget-object v1, p0, LX/JSF;->a:LX/JSO;

    iget-object v1, v1, LX/JSO;->a:LX/JSK;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/JSF;->a:LX/JSO;

    iget-object v1, v1, LX/JSO;->a:LX/JSK;

    sget-object v2, LX/JSK;->PAUSED:LX/JSK;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/JSF;->a:LX/JSO;

    iget-object v1, v1, LX/JSO;->a:LX/JSK;

    sget-object v2, LX/JSK;->STOPPED:LX/JSK;

    if-ne v1, v2, :cond_1

    .line 2694935
    :cond_0
    iget-object v1, p0, LX/JSF;->a:LX/JSO;

    iget-object v2, p0, LX/JSF;->d:LX/JSG;

    iget-object v2, v2, LX/JSG;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, v1, LX/JSO;->c:J

    .line 2694936
    iget-object v1, p0, LX/JSF;->a:LX/JSO;

    sget-object v2, LX/JSK;->PLAY_REQUESTED:LX/JSK;

    iput-object v2, v1, LX/JSO;->a:LX/JSK;

    .line 2694937
    iget-object v1, p0, LX/JSF;->b:LX/JTY;

    .line 2694938
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/JTW;->TAP_TO_PLAY:LX/JTW;

    invoke-virtual {v3}, LX/JTW;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "newsfeed_music_story_view"

    .line 2694939
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2694940
    move-object v2, v2

    .line 2694941
    invoke-static {v1, v2}, LX/JTY;->a(LX/JTY;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2694942
    :cond_1
    iget-object v1, p0, LX/JSF;->d:LX/JSG;

    iget-object v1, v1, LX/JSG;->b:LX/JSR;

    iget-object v2, p0, LX/JSF;->c:LX/JSe;

    .line 2694943
    if-nez v2, :cond_2

    .line 2694944
    :goto_0
    const v1, -0x38ca5204

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2694945
    :cond_2
    iget-object v3, v1, LX/JSR;->c:LX/JTe;

    if-nez v3, :cond_3

    .line 2694946
    iget-object v3, v1, LX/JSR;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JTe;

    iput-object v3, v1, LX/JSR;->c:LX/JTe;

    .line 2694947
    :cond_3
    iget-object v3, v1, LX/JSR;->c:LX/JTe;

    .line 2694948
    iget-object p0, v2, LX/JSe;->h:Landroid/net/Uri;

    move-object p0, p0

    .line 2694949
    invoke-virtual {v3, p0, v1}, LX/JTe;->b(Landroid/net/Uri;LX/JS8;)V

    goto :goto_0
.end method
