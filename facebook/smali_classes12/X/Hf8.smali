.class public final LX/Hf8;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Hf9;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/Hf9;


# direct methods
.method public constructor <init>(LX/Hf9;)V
    .locals 1

    .prologue
    .line 2491343
    iput-object p1, p0, LX/Hf8;->c:LX/Hf9;

    .line 2491344
    move-object v0, p1

    .line 2491345
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2491346
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2491335
    if-ne p0, p1, :cond_1

    .line 2491336
    :cond_0
    :goto_0
    return v0

    .line 2491337
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2491338
    goto :goto_0

    .line 2491339
    :cond_3
    check-cast p1, LX/Hf8;

    .line 2491340
    iget-object v2, p0, LX/Hf8;->b:LX/2kW;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Hf8;->b:LX/2kW;

    iget-object v3, p1, LX/Hf8;->b:LX/2kW;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2491341
    goto :goto_0

    .line 2491342
    :cond_4
    iget-object v2, p1, LX/Hf8;->b:LX/2kW;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
