.class public final enum LX/HeU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HeU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HeU;

.field public static final enum CONNECTED:LX/HeU;

.field public static final enum NO_INTERNET:LX/HeU;

.field public static final enum OTHER_ERROR:LX/HeU;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2490424
    new-instance v0, LX/HeU;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v2}, LX/HeU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HeU;->CONNECTED:LX/HeU;

    .line 2490425
    new-instance v0, LX/HeU;

    const-string v1, "NO_INTERNET"

    invoke-direct {v0, v1, v3}, LX/HeU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HeU;->NO_INTERNET:LX/HeU;

    .line 2490426
    new-instance v0, LX/HeU;

    const-string v1, "OTHER_ERROR"

    invoke-direct {v0, v1, v4}, LX/HeU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HeU;->OTHER_ERROR:LX/HeU;

    .line 2490427
    const/4 v0, 0x3

    new-array v0, v0, [LX/HeU;

    sget-object v1, LX/HeU;->CONNECTED:LX/HeU;

    aput-object v1, v0, v2

    sget-object v1, LX/HeU;->NO_INTERNET:LX/HeU;

    aput-object v1, v0, v3

    sget-object v1, LX/HeU;->OTHER_ERROR:LX/HeU;

    aput-object v1, v0, v4

    sput-object v0, LX/HeU;->$VALUES:[LX/HeU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2490429
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HeU;
    .locals 1

    .prologue
    .line 2490430
    const-class v0, LX/HeU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HeU;

    return-object v0
.end method

.method public static values()[LX/HeU;
    .locals 1

    .prologue
    .line 2490428
    sget-object v0, LX/HeU;->$VALUES:[LX/HeU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HeU;

    return-object v0
.end method
