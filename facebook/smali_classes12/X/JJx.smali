.class public LX/JJx;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2679972
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2679973
    return-void
.end method

.method public static a(LX/0ad;LX/0kx;)Landroid/support/v4/app/Fragment;
    .locals 7
    .annotation runtime Lcom/facebook/events/annotation/EventsDashboardReactFragment;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2679974
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2679975
    const-string v1, "cacheEnabled"

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/347;->D:S

    invoke-interface {p0, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2679976
    const-string v1, "usePersistedQueries"

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/347;->H:S

    invoke-interface {p0, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2679977
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "/events"

    .line 2679978
    iput-object v2, v1, LX/98r;->a:Ljava/lang/String;

    .line 2679979
    move-object v1, v1

    .line 2679980
    const-string v2, "EventsDashboardApp"

    .line 2679981
    iput-object v2, v1, LX/98r;->c:Ljava/lang/String;

    .line 2679982
    move-object v1, v1

    .line 2679983
    const v2, 0x7f082193

    .line 2679984
    iput v2, v1, LX/98r;->d:I

    .line 2679985
    move-object v1, v1

    .line 2679986
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2679987
    move-object v1, v1

    .line 2679988
    iput-object v0, v1, LX/98r;->f:Landroid/os/Bundle;

    .line 2679989
    move-object v0, v1

    .line 2679990
    iput v6, v0, LX/98r;->h:I

    .line 2679991
    move-object v0, v0

    .line 2679992
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 2679993
    const-string v1, "extra_ref_module"

    const-string v2, "unknown"

    invoke-virtual {p1, v2}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679994
    new-instance v1, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;

    invoke-direct {v1}, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;-><init>()V

    .line 2679995
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2679996
    move-object v0, v1

    .line 2679997
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2679998
    return-void
.end method
