.class public final LX/J9i;
.super LX/1PF;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2653177
    iput-object p1, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-direct {p0, p2}, LX/1PF;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const v3, 0xbf0005

    .line 2653178
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->z:LX/1PF;

    if-ne p0, v0, :cond_0

    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->i:LX/0kb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-nez v0, :cond_1

    .line 2653179
    :cond_0
    :goto_0
    return-void

    .line 2653180
    :cond_1
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget v1, v1, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2653181
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    if-nez v0, :cond_2

    .line 2653182
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget v1, v1, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/16 v2, 0xdf

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0

    .line 2653183
    :cond_2
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->i:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-boolean v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->B:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-boolean v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->A:Z

    if-eqz v0, :cond_3

    .line 2653184
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2653185
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    invoke-virtual {v0}, LX/3tK;->getCount()I

    move-result v0

    .line 2653186
    :goto_1
    const/16 v1, 0x14

    if-ge v0, v1, :cond_3

    .line 2653187
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    const/4 v1, 0x1

    .line 2653188
    iput-boolean v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->B:Z

    .line 2653189
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    const/4 v1, 0x0

    .line 2653190
    iput-boolean v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->A:Z

    .line 2653191
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->mJ_()V

    .line 2653192
    :cond_3
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget v1, v1, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/4 v2, 0x4

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0

    .line 2653193
    :cond_4
    iget-object v0, p0, LX/J9i;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    invoke-virtual {v0}, LX/J9W;->getCount()I

    move-result v0

    goto :goto_1
.end method
