.class public final LX/IbD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    .line 2592302
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2592303
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2592304
    if-eqz v0, :cond_0

    .line 2592305
    const-string v1, "error_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592306
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592307
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2592308
    if-eqz v0, :cond_2

    .line 2592309
    const-string v1, "preset_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592310
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2592311
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2592312
    if-eqz v1, :cond_1

    .line 2592313
    const-string p3, "cost_token"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592314
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592315
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2592316
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2592317
    if-eqz v0, :cond_3

    .line 2592318
    const-string v0, "result"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592319
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592320
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2592321
    if-eqz v0, :cond_7

    .line 2592322
    const-string v1, "ride_request"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592323
    const/4 v1, 0x0

    .line 2592324
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2592325
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2592326
    if-eqz v1, :cond_4

    .line 2592327
    const-string v2, "eta_in_minutes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592328
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2592329
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2592330
    if-eqz v1, :cond_5

    .line 2592331
    const-string v2, "request_id"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592332
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592333
    :cond_5
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2592334
    if-eqz v1, :cond_6

    .line 2592335
    const-string v2, "status"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592336
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592337
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2592338
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2592339
    if-eqz v0, :cond_c

    .line 2592340
    const-string v1, "surge_estimate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592341
    const-wide/16 v7, 0x0

    .line 2592342
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2592343
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2592344
    if-eqz v3, :cond_8

    .line 2592345
    const-string v4, "formatted_minimum_fare"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592346
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592347
    :cond_8
    const/4 v3, 0x1

    invoke-virtual {p0, v0, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2592348
    if-eqz v3, :cond_9

    .line 2592349
    const-string v4, "surge_confirmation_id"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592350
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592351
    :cond_9
    const/4 v3, 0x2

    invoke-virtual {p0, v0, v3, v7, v8}, LX/15i;->a(IID)D

    move-result-wide v3

    .line 2592352
    cmpl-double v5, v3, v7

    if-eqz v5, :cond_a

    .line 2592353
    const-string v5, "surge_multiplier"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592354
    invoke-virtual {p2, v3, v4}, LX/0nX;->a(D)V

    .line 2592355
    :cond_a
    const/4 v3, 0x3

    invoke-virtual {p0, v0, v3, v7, v8}, LX/15i;->a(IID)D

    move-result-wide v3

    .line 2592356
    cmpl-double v5, v3, v7

    if-eqz v5, :cond_b

    .line 2592357
    const-string v5, "surge_user_input_threshold"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592358
    invoke-virtual {p2, v3, v4}, LX/0nX;->a(D)V

    .line 2592359
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2592360
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2592361
    return-void
.end method
