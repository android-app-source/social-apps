.class public LX/JO4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1vb;

.field public final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/1vb;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2686850
    iput-object p1, p0, LX/JO4;->a:LX/1vb;

    .line 2686851
    iput-object p2, p0, LX/JO4;->b:Landroid/content/res/Resources;

    .line 2686852
    return-void
.end method

.method public static a(LX/0QB;)LX/JO4;
    .locals 5

    .prologue
    .line 2686853
    const-class v1, LX/JO4;

    monitor-enter v1

    .line 2686854
    :try_start_0
    sget-object v0, LX/JO4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686855
    sput-object v2, LX/JO4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686856
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686857
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686858
    new-instance p0, LX/JO4;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v3

    check-cast v3, LX/1vb;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, LX/JO4;-><init>(LX/1vb;Landroid/content/res/Resources;)V

    .line 2686859
    move-object v0, p0

    .line 2686860
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686861
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JO4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686862
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686863
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
