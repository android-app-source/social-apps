.class public LX/HWK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475811
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2475812
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v0, "extra_config_action_data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CYE;

    const-string v1, "extra_action_channel_edit_action"

    invoke-static {p1, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 2475813
    new-instance v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;

    invoke-direct {v4}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;-><init>()V

    .line 2475814
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2475815
    const-string p0, "com.facebook.katana.profile.id"

    invoke-virtual {v5, p0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475816
    const-string p0, "extra_config_action_data"

    invoke-virtual {v5, p0, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2475817
    const-string p0, "extra_action_channel_edit_action"

    invoke-static {v5, p0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2475818
    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475819
    move-object v0, v4

    .line 2475820
    return-object v0
.end method
