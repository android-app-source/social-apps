.class public final LX/HWT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V
    .locals 0

    .prologue
    .line 2476097
    iput-object p1, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2476098
    iget-object v0, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    sget-object v1, LX/HWU;->ERROR:LX/HWU;

    invoke-static {v0, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;LX/HWU;)V

    .line 2476099
    iget-object v0, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v1, LX/9XA;->EVENT_PAGE_INFO_LOAD_ERROR:LX/9XA;

    iget-object v2, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-wide v2, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->A:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2476100
    iget-object v0, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "page_information_data_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2476101
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2476102
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2476103
    iget-object v0, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    sget-object v1, LX/HWU;->LOADED:LX/HWU;

    invoke-static {v0, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;LX/HWU;)V

    .line 2476104
    iget-object v0, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v1, LX/9XB;->EVENT_PAGE_INFO_LOADED:LX/9XB;

    iget-object v2, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-wide v2, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->A:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2476105
    iget-object v1, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2476106
    :goto_0
    iput-object v0, v1, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    .line 2476107
    iget-object v0, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->b()V

    .line 2476108
    iget-object v0, p0, LX/HWT;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    const/4 v1, 0x1

    .line 2476109
    iput-boolean v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->P:Z

    .line 2476110
    return-void

    .line 2476111
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2476112
    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    goto :goto_0
.end method
