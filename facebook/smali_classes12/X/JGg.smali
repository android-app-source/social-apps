.class public final LX/JGg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5rT;


# instance fields
.field public final synthetic a:LX/JGq;

.field private final b:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>(LX/JGq;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2668914
    iput-object p1, p0, LX/JGg;->a:LX/JGq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2668915
    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    .line 2668916
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 2668917
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v4, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 2668918
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2668919
    :cond_0
    iput-object v2, p0, LX/JGg;->b:Landroid/util/SparseIntArray;

    .line 2668920
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2668921
    iget-object v0, p0, LX/JGg;->a:LX/JGq;

    iget-object v0, v0, LX/JGq;->b:LX/JGd;

    iget-object v1, p0, LX/JGg;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v1}, LX/JGd;->a(Landroid/util/SparseIntArray;)V

    .line 2668922
    return-void
.end method
