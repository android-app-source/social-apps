.class public LX/IiT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/IiT;


# instance fields
.field private final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/HiI;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2604374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2604375
    new-instance v0, LX/0aq;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/IiT;->a:LX/0aq;

    .line 2604376
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2604377
    iput-object v0, p0, LX/IiT;->b:LX/0Ot;

    .line 2604378
    return-void
.end method

.method public static a(LX/0QB;)LX/IiT;
    .locals 4

    .prologue
    .line 2604379
    sget-object v0, LX/IiT;->c:LX/IiT;

    if-nez v0, :cond_1

    .line 2604380
    const-class v1, LX/IiT;

    monitor-enter v1

    .line 2604381
    :try_start_0
    sget-object v0, LX/IiT;->c:LX/IiT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2604382
    if-eqz v2, :cond_0

    .line 2604383
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2604384
    new-instance v3, LX/IiT;

    invoke-direct {v3}, LX/IiT;-><init>()V

    .line 2604385
    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2604386
    iput-object p0, v3, LX/IiT;->b:LX/0Ot;

    .line 2604387
    move-object v0, v3

    .line 2604388
    sput-object v0, LX/IiT;->c:LX/IiT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2604389
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2604390
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2604391
    :cond_1
    sget-object v0, LX/IiT;->c:LX/IiT;

    return-object v0

    .line 2604392
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2604393
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/graphics/Bitmap;)LX/HiI;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2604394
    if-nez p1, :cond_1

    const/4 v1, 0x0

    .line 2604395
    :goto_0
    if-nez v1, :cond_2

    .line 2604396
    :try_start_0
    new-instance v0, LX/HiG;

    invoke-direct {v0, p2}, LX/HiG;-><init>(Landroid/graphics/Bitmap;)V

    move-object v0, v0

    .line 2604397
    const/16 v2, 0x32

    .line 2604398
    iput v2, v0, LX/HiG;->d:I

    .line 2604399
    move-object v0, v0

    .line 2604400
    invoke-virtual {v0}, LX/HiG;->a()LX/HiI;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2604401
    :goto_1
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 2604402
    iget-object v1, p0, LX/IiT;->a:LX/0aq;

    invoke-virtual {v1, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2604403
    :cond_0
    :goto_2
    return-object v0

    .line 2604404
    :cond_1
    iget-object v0, p0, LX/IiT;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HiI;

    move-object v1, v0

    goto :goto_0

    .line 2604405
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 2604406
    iget-object v0, p0, LX/IiT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "MontagePaletteUtil"

    const-string v4, "OOM when attempting to generate palette"

    invoke-virtual {v0, v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method
