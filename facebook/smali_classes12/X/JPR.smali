.class public LX/JPR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/JPU;

.field public final b:LX/JOt;


# direct methods
.method public constructor <init>(LX/JPU;LX/JOt;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2689730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2689731
    iput-object p1, p0, LX/JPR;->a:LX/JPU;

    .line 2689732
    iput-object p2, p0, LX/JPR;->b:LX/JOt;

    .line 2689733
    return-void
.end method

.method public static a(LX/JPR;LX/1De;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1Dh;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;TE;)",
            "LX/1Dh;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2689734
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->t()LX/0Px;

    move-result-object v2

    .line 2689735
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const v3, 0x7f0b257f

    invoke-interface {v1, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v1

    const v3, 0x7f0b258c

    invoke-interface {v1, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    .line 2689736
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x3

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v1, v0

    .line 2689737
    :goto_0
    if-ge v1, v4, :cond_2

    .line 2689738
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 2689739
    if-eqz v0, :cond_1

    .line 2689740
    iget-object v5, p0, LX/JPR;->a:LX/JPU;

    const/4 v6, 0x0

    .line 2689741
    new-instance v7, LX/JPT;

    invoke-direct {v7, v5}, LX/JPT;-><init>(LX/JPU;)V

    .line 2689742
    iget-object p2, v5, LX/JPU;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JPS;

    .line 2689743
    if-nez p2, :cond_0

    .line 2689744
    new-instance p2, LX/JPS;

    invoke-direct {p2, v5}, LX/JPS;-><init>(LX/JPU;)V

    .line 2689745
    :cond_0
    invoke-static {p2, p1, v6, v6, v7}, LX/JPS;->a$redex0(LX/JPS;LX/1De;IILX/JPT;)V

    .line 2689746
    move-object v7, p2

    .line 2689747
    move-object v6, v7

    .line 2689748
    move-object v5, v6

    .line 2689749
    iget-object v6, v5, LX/JPS;->a:LX/JPT;

    iput-object p3, v6, LX/JPT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2689750
    iget-object v6, v5, LX/JPS;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2689751
    move-object v5, v5

    .line 2689752
    iget-object v6, v5, LX/JPS;->a:LX/JPT;

    iput-object v0, v6, LX/JPT;->b:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2689753
    iget-object v6, v5, LX/JPS;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2689754
    move-object v5, v5

    .line 2689755
    move-object v0, p4

    check-cast v0, LX/1Po;

    .line 2689756
    iget-object v6, v5, LX/JPS;->a:LX/JPT;

    iput-object v0, v6, LX/JPT;->c:LX/1Po;

    .line 2689757
    iget-object v6, v5, LX/JPS;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2689758
    move-object v0, v5

    .line 2689759
    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2689760
    const/4 v0, 0x2

    if-eq v1, v0, :cond_1

    .line 2689761
    invoke-static {p1}, LX/JPK;->a(LX/1De;)LX/1Di;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2689762
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2689763
    :cond_2
    return-object v3
.end method

.method public static a(LX/0QB;)LX/JPR;
    .locals 5

    .prologue
    .line 2689764
    const-class v1, LX/JPR;

    monitor-enter v1

    .line 2689765
    :try_start_0
    sget-object v0, LX/JPR;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2689766
    sput-object v2, LX/JPR;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2689767
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2689768
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2689769
    new-instance p0, LX/JPR;

    invoke-static {v0}, LX/JPU;->a(LX/0QB;)LX/JPU;

    move-result-object v3

    check-cast v3, LX/JPU;

    invoke-static {v0}, LX/JOt;->a(LX/0QB;)LX/JOt;

    move-result-object v4

    check-cast v4, LX/JOt;

    invoke-direct {p0, v3, v4}, LX/JPR;-><init>(LX/JPU;LX/JOt;)V

    .line 2689770
    move-object v0, p0

    .line 2689771
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2689772
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JPR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2689773
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2689774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
