.class public LX/JTq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2697331
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JTq;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697332
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2697333
    iput-object p1, p0, LX/JTq;->b:LX/0Ot;

    .line 2697334
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2697335
    check-cast p2, LX/JTp;

    .line 2697336
    iget-object v0, p0, LX/JTq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsItemComponentSpec;

    iget-object v1, p2, LX/JTp;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    iget-object v2, p2, LX/JTp;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v3, p2, LX/JTp;->c:LX/JTg;

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 2697337
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3c

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0f14

    invoke-interface {v5, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x2

    const/4 v7, 0x0

    .line 2697338
    invoke-static {v2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    .line 2697339
    if-eqz v6, :cond_4

    invoke-static {v6}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2697340
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 2697341
    :goto_0
    move-object v9, v6

    .line 2697342
    if-nez v9, :cond_2

    move-object v8, v7

    .line 2697343
    :goto_1
    if-nez v9, :cond_3

    move-object v6, v7

    .line 2697344
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v9

    const/4 p0, 0x1

    invoke-interface {v9, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v9

    const p0, 0x7f0b0f18

    invoke-interface {v9, p0}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v9

    const p0, 0x7f0b0f18

    invoke-interface {v9, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v9

    const p0, 0x7f020adc

    invoke-interface {v9, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v9

    .line 2697345
    const p0, 0x51d3c

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2697346
    invoke-interface {v9, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v9

    iget-object p0, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsItemComponentSpec;->b:LX/1nu;

    invoke-virtual {p0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p0

    invoke-virtual {p0, v8}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1nw;->b(Ljava/lang/String;)LX/1nw;

    move-result-object v6

    sget-object v8, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v8}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    sget-object v8, LX/1Up;->c:LX/1Up;

    invoke-virtual {v6, v8}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v6

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/1nw;->a(LX/4Ab;)LX/1nw;

    move-result-object v6

    const v8, 0x7f0203b2

    invoke-virtual {v6, v8}, LX/1nw;->h(I)LX/1nw;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v8, 0x7f0b0f19

    invoke-interface {v6, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v8, 0x7f0b0f19

    invoke-interface {v6, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    invoke-interface {v9, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v8

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->APPROXIMATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v8, v9, :cond_0

    const/16 p2, 0x24

    const/16 p0, 0x10

    const/4 v9, 0x1

    .line 2697347
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    const v8, 0x7f021089

    invoke-virtual {v7, v8}, LX/1o5;->h(I)LX/1o5;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, v9}, LX/1Di;->c(I)LX/1Di;

    move-result-object v7

    invoke-interface {v7, v9, p2}, LX/1Di;->m(II)LX/1Di;

    move-result-object v7

    const/4 v8, 0x4

    invoke-interface {v7, v8, p2}, LX/1Di;->m(II)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Di;->j(I)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Di;->j(I)LX/1Di;

    move-result-object v7

    move-object v7, v7

    .line 2697348
    :cond_0
    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    move-object v6, v6

    .line 2697349
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsItemComponentSpec;->c:LX/JTj;

    const/4 v6, 0x0

    .line 2697350
    new-instance v7, LX/JTi;

    invoke-direct {v7, v5}, LX/JTi;-><init>(LX/JTj;)V

    .line 2697351
    sget-object v8, LX/JTj;->a:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/JTh;

    .line 2697352
    if-nez v8, :cond_1

    .line 2697353
    new-instance v8, LX/JTh;

    invoke-direct {v8}, LX/JTh;-><init>()V

    .line 2697354
    :cond_1
    invoke-static {v8, p1, v6, v6, v7}, LX/JTh;->a$redex0(LX/JTh;LX/1De;IILX/JTi;)V

    .line 2697355
    move-object v7, v8

    .line 2697356
    move-object v6, v7

    .line 2697357
    move-object v5, v6

    .line 2697358
    iget-object v6, v5, LX/JTh;->a:LX/JTi;

    iput-object v1, v6, LX/JTi;->b:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 2697359
    iget-object v6, v5, LX/JTh;->d:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2697360
    move-object v5, v5

    .line 2697361
    iget-object v6, v5, LX/JTh;->a:LX/JTi;

    iput-object v2, v6, LX/JTi;->a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 2697362
    iget-object v6, v5, LX/JTh;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2697363
    move-object v5, v5

    .line 2697364
    iget-object v6, v5, LX/JTh;->a:LX/JTi;

    iput-object v3, v6, LX/JTi;->c:LX/JTg;

    .line 2697365
    move-object v5, v5

    .line 2697366
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2697367
    return-object v0

    .line 2697368
    :cond_2
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object v8, v6

    goto/16 :goto_1

    .line 2697369
    :cond_3
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLImage;->l()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    :cond_4
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2697370
    invoke-static {}, LX/1dS;->b()V

    .line 2697371
    iget v0, p1, LX/1dQ;->b:I

    .line 2697372
    packed-switch v0, :pswitch_data_0

    .line 2697373
    :goto_0
    return-object v2

    .line 2697374
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2697375
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2697376
    check-cast v1, LX/JTp;

    .line 2697377
    iget-object v3, p0, LX/JTq;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/JTp;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    iget-object p1, v1, LX/JTp;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object p2, v1, LX/JTp;->c:LX/JTg;

    .line 2697378
    if-eqz p2, :cond_0

    .line 2697379
    invoke-static {p2, v0, p1}, LX/JTg;->a(LX/JTg;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;)V

    .line 2697380
    iget-object p0, p2, LX/JTg;->c:LX/JUO;

    invoke-virtual {p0, p1, v3}, LX/JUO;->b(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2697381
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x51d3c
        :pswitch_0
    .end packed-switch
.end method
