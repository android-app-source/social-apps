.class public final LX/JQ9;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JQA;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pm;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JQA;


# direct methods
.method public constructor <init>(LX/JQA;)V
    .locals 1

    .prologue
    .line 2691094
    iput-object p1, p0, LX/JQ9;->c:LX/JQA;

    .line 2691095
    move-object v0, p1

    .line 2691096
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2691097
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2691079
    const-string v0, "InstagramPhotosFromFriendsHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2691080
    if-ne p0, p1, :cond_1

    .line 2691081
    :cond_0
    :goto_0
    return v0

    .line 2691082
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2691083
    goto :goto_0

    .line 2691084
    :cond_3
    check-cast p1, LX/JQ9;

    .line 2691085
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2691086
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2691087
    if-eq v2, v3, :cond_0

    .line 2691088
    iget-object v2, p0, LX/JQ9;->a:LX/1Pm;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JQ9;->a:LX/1Pm;

    iget-object v3, p1, LX/JQ9;->a:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2691089
    goto :goto_0

    .line 2691090
    :cond_5
    iget-object v2, p1, LX/JQ9;->a:LX/1Pm;

    if-nez v2, :cond_4

    .line 2691091
    :cond_6
    iget-object v2, p0, LX/JQ9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JQ9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JQ9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2691092
    goto :goto_0

    .line 2691093
    :cond_7
    iget-object v2, p1, LX/JQ9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
