.class public final LX/JGY;
.super LX/JGN;
.source ""


# static fields
.field public static final c:[LX/JGY;


# instance fields
.field public final d:I

.field public e:Z

.field public f:F

.field public g:F

.field public h:F

.field public i:F

.field public final j:Landroid/graphics/RectF;

.field public k:F

.field public l:Landroid/graphics/Path;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2667681
    const/4 v0, 0x0

    new-array v0, v0, [LX/JGY;

    sput-object v0, LX/JGY;->c:[LX/JGY;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2667677
    invoke-direct {p0}, LX/JGN;-><init>()V

    .line 2667678
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/JGY;->j:Landroid/graphics/RectF;

    .line 2667679
    iput p1, p0, LX/JGY;->d:I

    .line 2667680
    return-void
.end method

.method private static a(LX/JGY;F)V
    .locals 4

    .prologue
    .line 2667670
    iput p1, p0, LX/JGY;->k:F

    .line 2667671
    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 2667672
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/JGY;->l:Landroid/graphics/Path;

    .line 2667673
    iget-object v0, p0, LX/JGY;->j:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/JGN;->j()F

    move-result v1

    invoke-virtual {p0}, LX/JGN;->k()F

    move-result v2

    invoke-virtual {p0}, LX/JGN;->l()F

    move-result v3

    invoke-virtual {p0}, LX/JGN;->m()F

    move-result p1

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2667674
    iget-object v0, p0, LX/JGY;->l:Landroid/graphics/Path;

    iget-object v1, p0, LX/JGY;->j:Landroid/graphics/RectF;

    iget v2, p0, LX/JGY;->k:F

    iget v3, p0, LX/JGY;->k:F

    sget-object p1, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 2667675
    :goto_0
    return-void

    .line 2667676
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGY;->l:Landroid/graphics/Path;

    goto :goto_0
.end method

.method private static e(LX/JGY;FFFF)Z
    .locals 1

    .prologue
    .line 2667669
    iget v0, p0, LX/JGY;->f:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, LX/JGY;->g:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    iget v0, p0, LX/JGY;->h:F

    cmpl-float v0, p3, v0

    if-nez v0, :cond_0

    iget v0, p0, LX/JGY;->i:F

    cmpl-float v0, p4, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(LX/JGY;FFFF)V
    .locals 0

    .prologue
    .line 2667664
    iput p1, p0, LX/JGY;->f:F

    .line 2667665
    iput p2, p0, LX/JGY;->g:F

    .line 2667666
    iput p3, p0, LX/JGY;->h:F

    .line 2667667
    iput p4, p0, LX/JGY;->i:F

    .line 2667668
    return-void
.end method


# virtual methods
.method public final a(FFFFFFFFFFFFF)LX/JGY;
    .locals 9

    .prologue
    .line 2667639
    invoke-virtual {p0}, LX/JGN;->h()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2667640
    invoke-virtual {p0, p1, p2, p3, p4}, LX/JGN;->c(FFFF)V

    .line 2667641
    move/from16 v0, p9

    move/from16 v1, p10

    move/from16 v2, p11

    move/from16 v3, p12

    invoke-virtual {p0, v0, v1, v2, v3}, LX/JGN;->b(FFFF)V

    .line 2667642
    move/from16 v0, p13

    invoke-static {p0, v0}, LX/JGY;->a(LX/JGY;F)V

    .line 2667643
    move/from16 v0, p7

    move/from16 v1, p8

    invoke-static {p0, p5, p6, v0, v1}, LX/JGY;->f(LX/JGY;FFFF)V

    .line 2667644
    invoke-virtual {p0}, LX/JGN;->i()V

    .line 2667645
    :cond_0
    :goto_0
    return-object p0

    .line 2667646
    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, LX/JGN;->d(FFFF)Z

    move-result v6

    .line 2667647
    move/from16 v0, p9

    move/from16 v1, p10

    move/from16 v2, p11

    move/from16 v3, p12

    invoke-virtual {p0, v0, v1, v2, v3}, LX/JGN;->a(FFFF)Z

    move-result v7

    .line 2667648
    iget v4, p0, LX/JGY;->k:F

    cmpl-float v4, v4, p13

    if-nez v4, :cond_8

    const/4 v4, 0x1

    move v5, v4

    .line 2667649
    :goto_1
    move/from16 v0, p7

    move/from16 v1, p8

    invoke-static {p0, p5, p6, v0, v1}, LX/JGY;->e(LX/JGY;FFFF)Z

    move-result v8

    .line 2667650
    if-eqz v6, :cond_2

    if-eqz v7, :cond_2

    if-eqz v5, :cond_2

    if-nez v8, :cond_0

    .line 2667651
    :cond_2
    invoke-virtual {p0}, LX/JGN;->ng_()LX/JGN;

    move-result-object v4

    check-cast v4, LX/JGY;

    .line 2667652
    if-nez v6, :cond_3

    .line 2667653
    invoke-virtual {v4, p1, p2, p3, p4}, LX/JGN;->c(FFFF)V

    .line 2667654
    :cond_3
    if-nez v7, :cond_4

    .line 2667655
    move/from16 v0, p9

    move/from16 v1, p10

    move/from16 v2, p11

    move/from16 v3, p12

    invoke-virtual {v4, v0, v1, v2, v3}, LX/JGN;->b(FFFF)V

    .line 2667656
    :cond_4
    if-nez v8, :cond_5

    .line 2667657
    move/from16 v0, p7

    move/from16 v1, p8

    invoke-static {v4, p5, p6, v0, v1}, LX/JGY;->f(LX/JGY;FFFF)V

    .line 2667658
    :cond_5
    if-eqz v5, :cond_6

    if-nez v6, :cond_7

    .line 2667659
    :cond_6
    move/from16 v0, p13

    invoke-static {v4, v0}, LX/JGY;->a(LX/JGY;F)V

    .line 2667660
    :cond_7
    const/4 v5, 0x0

    iput-boolean v5, v4, LX/JGY;->e:Z

    .line 2667661
    invoke-virtual {v4}, LX/JGN;->i()V

    move-object p0, v4

    .line 2667662
    goto :goto_0

    .line 2667663
    :cond_8
    const/4 v4, 0x0

    move v5, v4

    goto :goto_1
.end method

.method public final a(LX/JGs;Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2667620
    iget-boolean v0, p0, LX/JGN;->d_:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/JGY;->k:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 2667621
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 2667622
    invoke-virtual {p0, p2}, LX/JGY;->b(Landroid/graphics/Canvas;)V

    .line 2667623
    invoke-virtual {p1, p2}, LX/JGs;->b(Landroid/graphics/Canvas;)V

    .line 2667624
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    .line 2667625
    :goto_0
    return-void

    .line 2667626
    :cond_1
    invoke-virtual {p1, p2}, LX/JGs;->b(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2667635
    iget v0, p0, LX/JGY;->k:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 2667636
    iget-object v0, p0, LX/JGY;->l:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 2667637
    :goto_0
    return-void

    .line 2667638
    :cond_0
    invoke-super {p0, p1}, LX/JGN;->b(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final c(LX/JGs;Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 2667628
    iget v0, p1, LX/JGs;->m:I

    invoke-virtual {p1, v0}, LX/JGs;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2667629
    instance-of v1, v0, LX/JGs;

    if-eqz v1, :cond_0

    const v2, -0xbbbbbc

    .line 2667630
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v4, v1

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    int-to-float v5, v1

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v6, v0

    move-object v0, p1

    move-object v1, p2

    .line 2667631
    invoke-virtual/range {v0 .. v6}, LX/JGs;->a(Landroid/graphics/Canvas;IFFFF)V

    .line 2667632
    iget v0, p1, LX/JGs;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, LX/JGs;->m:I

    .line 2667633
    return-void

    .line 2667634
    :cond_0
    const/high16 v2, -0x10000

    goto :goto_0
.end method

.method public final c(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 2667627
    return-void
.end method
