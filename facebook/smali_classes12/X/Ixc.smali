.class public LX/Ixc;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ixa;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2631963
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ixc;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2631964
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2631965
    iput-object p1, p0, LX/Ixc;->b:LX/0Ot;

    .line 2631966
    return-void
.end method

.method public static a(LX/0QB;)LX/Ixc;
    .locals 4

    .prologue
    .line 2631967
    const-class v1, LX/Ixc;

    monitor-enter v1

    .line 2631968
    :try_start_0
    sget-object v0, LX/Ixc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2631969
    sput-object v2, LX/Ixc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2631970
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2631971
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2631972
    new-instance v3, LX/Ixc;

    const/16 p0, 0x2c6d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ixc;-><init>(LX/0Ot;)V

    .line 2631973
    move-object v0, v3

    .line 2631974
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2631975
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ixc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2631976
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2631977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2631978
    check-cast p2, LX/Ixb;

    .line 2631979
    iget-object v0, p0, LX/Ixc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;

    iget-object v1, p2, LX/Ixb;->a:LX/IxU;

    const/4 p2, 0x2

    const/4 v6, 0x0

    const/4 p0, 0x0

    const/4 v7, 0x1

    .line 2631980
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v2

    sget-object v3, LX/1Up;->g:LX/1Up;

    invoke-virtual {v2, v3}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v2

    const v3, 0x7f0a00e2

    invoke-virtual {v2, v3}, LX/1up;->h(I)LX/1up;

    move-result-object v2

    const v3, 0x7f0215c4

    .line 2631981
    iget-object v4, v2, LX/1up;->a:LX/1un;

    invoke-virtual {v2, v3}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v5

    iput-object v5, v4, LX/1un;->r:LX/1dc;

    .line 2631982
    move-object v3, v2

    .line 2631983
    iget-object v2, v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-interface {v1}, LX/IxU;->l()Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    sget-object v4, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0067

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0067

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    invoke-static {v3, v4, v6, v6}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    .line 2631984
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v2

    const/16 v4, 0x8

    invoke-interface {v2, v4, p0}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v4

    .line 2631985
    invoke-interface {v1}, LX/IxU;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 2631986
    :goto_0
    if-eqz v2, :cond_0

    .line 2631987
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v5, 0x7f0b004e

    invoke-virtual {v2, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0a00d5

    invoke-virtual {v2, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v5, 0x3

    const v6, 0x7f0b0061

    invoke-interface {v2, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v5, 0x7f0b0061

    invoke-interface {v2, p0, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v5, 0x7f0b0061

    invoke-interface {v2, p2, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2631988
    :cond_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-interface {v1}, LX/IxU;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v5, 0x7f0b0052

    invoke-virtual {v2, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0a00d5

    invoke-virtual {v2, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v5, 0x7f0b0061

    invoke-interface {v2, p0, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v5, 0x7f0b0061

    invoke-interface {v2, p2, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2631989
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1}, LX/IxU;->d()Ljava/lang/String;

    move-result-object v5

    .line 2631990
    const v6, -0x55ffa238

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v5, v7, p0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v5, v6

    .line 2631991
    invoke-interface {v2, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2631992
    return-object v0

    .line 2631993
    :cond_1
    const-string v2, "/"

    invoke-interface {v1}, LX/IxU;->j()LX/0Px;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2631994
    invoke-static {}, LX/1dS;->b()V

    .line 2631995
    iget v0, p1, LX/1dQ;->b:I

    .line 2631996
    packed-switch v0, :pswitch_data_0

    .line 2631997
    :goto_0
    return-object v3

    .line 2631998
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2631999
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/String;

    .line 2632000
    iget-object p1, p0, LX/Ixc;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;

    .line 2632001
    sget-object p2, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 2632002
    iget-object p0, p1, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;->c:LX/17W;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2632003
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x55ffa238
        :pswitch_0
    .end packed-switch
.end method
