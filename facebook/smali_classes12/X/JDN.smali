.class public final LX/JDN;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/1oP;

.field public final synthetic b:Lcom/facebook/story/UpdateTimelineAppCollectionParams;

.field public final synthetic c:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V
    .locals 0

    .prologue
    .line 2663736
    iput-object p1, p0, LX/JDN;->c:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iput-object p2, p0, LX/JDN;->a:LX/1oP;

    iput-object p3, p0, LX/JDN;->b:Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2663742
    iget-object v0, p0, LX/JDN;->c:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    .line 2663743
    sget-object v1, LX/JDJ;->REQUEST_FAILED:LX/JDJ;

    iput-object v1, v0, LX/JDK;->l:LX/JDJ;

    .line 2663744
    iget-object v0, p0, LX/JDN;->c:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    iget-object v1, p0, LX/JDN;->a:LX/1oP;

    iget-object v2, p0, LX/JDN;->b:Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    .line 2663745
    iget-object v3, v2, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    move-object v2, v3

    .line 2663746
    invoke-virtual {v0, v1, v2}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a(LX/1oP;LX/5vL;)V

    .line 2663747
    iget-object v0, p0, LX/JDN;->c:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v1, p0, LX/JDN;->b:Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    .line 2663748
    iget-object v2, v1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    move-object v1, v2

    .line 2663749
    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    invoke-virtual {v2, v1}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a(LX/5vL;)V

    .line 2663750
    iget-object v0, p0, LX/JDN;->c:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->d:LX/03V;

    const-string v1, "ListCollectionItemView"

    const-string v2, "Failed to add item to Timeline Collection due to server exception."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2663751
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2663737
    iget-object v0, p0, LX/JDN;->c:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    .line 2663738
    const/4 p1, 0x0

    iput-object p1, v0, LX/JDK;->k:LX/5vL;

    .line 2663739
    sget-object p1, LX/JDJ;->REQUEST_NONE:LX/JDJ;

    iput-object p1, v0, LX/JDK;->l:LX/JDJ;

    .line 2663740
    iget-object v0, p0, LX/JDN;->c:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a()V

    .line 2663741
    return-void
.end method
