.class public final LX/INw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/INx;


# direct methods
.method public constructor <init>(LX/INx;)V
    .locals 0

    .prologue
    .line 2571307
    iput-object p1, p0, LX/INw;->a:LX/INx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x52962851

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2571302
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/INw;->a:LX/INx;

    iget-object v0, v0, LX/INx;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2571303
    const-string v2, "group_feed_id"

    iget-object v3, p0, LX/INw;->a:LX/INx;

    iget-object v3, v3, LX/INx;->p:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2571304
    const-string v2, "target_fragment"

    sget-object v3, LX/0cQ;->GROUPS_MALL_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2571305
    iget-object v2, p0, LX/INw;->a:LX/INx;

    iget-object v2, v2, LX/INx;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/INw;->a:LX/INx;

    iget-object v3, v3, LX/INx;->m:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2571306
    const v0, 0x4e5ce148    # 9.2643789E8f

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
