.class public LX/JEL;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

.field private b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2665835
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2665836
    iput-object p1, p0, LX/JEL;->a:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

    .line 2665837
    iput-object p2, p0, LX/JEL;->b:Landroid/view/View$OnClickListener;

    .line 2665838
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2665839
    if-nez p2, :cond_0

    .line 2665840
    new-instance v1, LX/JEM;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/JEM;-><init>(Landroid/content/Context;)V

    .line 2665841
    new-instance v0, LX/JEK;

    invoke-direct {v0, v1}, LX/JEK;-><init>(LX/JEM;)V

    .line 2665842
    :goto_0
    return-object v0

    .line 2665843
    :cond_0
    new-instance v1, Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;)V

    .line 2665844
    const v0, 0x7f08392e

    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2665845
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2665846
    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2665847
    const/16 v0, 0x102

    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2665848
    new-instance v0, LX/JEJ;

    invoke-direct {v0, v1}, LX/JEJ;-><init>(Lcom/facebook/fig/button/FigButton;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2665849
    instance-of v0, p1, LX/JEK;

    if-eqz v0, :cond_1

    .line 2665850
    iget-object v0, p0, LX/JEL;->a:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel$SummaryInfoModel;

    .line 2665851
    check-cast p1, LX/JEK;

    .line 2665852
    iget-object v1, p1, LX/JEK;->l:LX/JEM;

    move-object v1, v1

    .line 2665853
    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel$SummaryInfoModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel$SummaryInfoModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel$SummaryInfoModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    move-result-object v4

    iget-object v0, p0, LX/JEL;->a:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, LX/JEM;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;Z)V

    .line 2665854
    :goto_1
    return-void

    .line 2665855
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2665856
    :cond_1
    check-cast p1, LX/JEJ;

    .line 2665857
    iget-object v0, p1, LX/JEJ;->l:Lcom/facebook/fig/button/FigButton;

    move-object v0, v0

    .line 2665858
    iget-object v1, p0, LX/JEL;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2665859
    iget-object v0, p0, LX/JEL;->a:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2665860
    const/4 v0, 0x0

    .line 2665861
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2665862
    iget-object v0, p0, LX/JEL;->a:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
