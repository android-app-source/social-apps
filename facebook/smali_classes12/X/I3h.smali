.class public LX/I3h;
.super LX/CGt;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CGt",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2531698
    invoke-direct {p0, p1, p2}, LX/CGt;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2531699
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2531700
    new-instance v0, LX/I3y;

    invoke-direct {v0}, LX/I3y;-><init>()V

    move-object v0, v0

    .line 2531701
    const-string v1, "event_collection_id"

    .line 2531702
    iget-object v2, p0, LX/CGt;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2531703
    move-object v2, v2

    .line 2531704
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "maxElements"

    .line 2531705
    iget v2, p0, LX/CGt;->e:I

    move v2, v2

    .line 2531706
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "media_type"

    .line 2531707
    iget-object v2, p0, LX/CGt;->h:LX/0wF;

    move-object v2, v2

    .line 2531708
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "scale"

    .line 2531709
    iget-object v2, p0, LX/CGt;->i:LX/0wC;

    move-object v2, v2

    .line 2531710
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "final_image_width"

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "final_image_height"

    .line 2531711
    iget v2, p0, LX/CGt;->o:I

    move v2, v2

    .line 2531712
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/I3y;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2531713
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2531714
    move-object v0, v0

    .line 2531715
    iget-object v1, p0, LX/CGt;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v1

    .line 2531716
    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2531717
    return-object v0
.end method
