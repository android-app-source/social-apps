.class public LX/IUt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IQB;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public final b:LX/IRb;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/3mF;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/IRb;Lcom/facebook/content/SecureContextHelper;LX/3mF;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p2    # LX/IRb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2581135
    iput-object p1, p0, LX/IUt;->a:Landroid/content/res/Resources;

    .line 2581136
    iput-object p2, p0, LX/IUt;->b:LX/IRb;

    .line 2581137
    iput-object p3, p0, LX/IUt;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2581138
    iput-object p4, p0, LX/IUt;->d:LX/3mF;

    .line 2581139
    iput-object p5, p0, LX/IUt;->e:Ljava/util/concurrent/ExecutorService;

    .line 2581140
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2581141
    iget-object v0, p0, LX/IUt;->a:Landroid/content/res/Resources;

    const v1, 0x7f081bd0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2581142
    iget-object v0, p0, LX/IUt;->a:Landroid/content/res/Resources;

    const v1, 0x7f081bd1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2581143
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2581144
    iget-object v0, p0, LX/IUt;->a:Landroid/content/res/Resources;

    const v1, 0x7f081bd2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2581145
    new-instance v0, LX/IUr;

    invoke-direct {v0, p0}, LX/IUr;-><init>(LX/IUt;)V

    return-object v0
.end method

.method public final f()Landroid/view/View$OnClickListener;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2581146
    const/4 v0, 0x0

    return-object v0
.end method
