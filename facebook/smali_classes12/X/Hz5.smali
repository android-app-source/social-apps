.class public final LX/Hz5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V
    .locals 0

    .prologue
    .line 2525312
    iput-object p1, p0, LX/Hz5;->a:Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2525313
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2525314
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525315
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525316
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2525317
    if-eqz v0, :cond_0

    .line 2525318
    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2525319
    iget-object v1, p0, LX/Hz5;->a:Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    invoke-virtual {v1, v0}, LX/Hz2;->a(Lcom/facebook/events/model/Event;)V

    .line 2525320
    :cond_0
    return-void
.end method
