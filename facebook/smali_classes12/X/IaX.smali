.class public final LX/IaX;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2590937
    iput-object p1, p0, LX/IaX;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iput-object p2, p0, LX/IaX;->a:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2590938
    iget-object v0, p0, LX/IaX;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, p0, LX/IaX;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Ljava/lang/String;)V

    .line 2590939
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2590940
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2590941
    iget-object v0, p0, LX/IaX;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0265

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2590942
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2590943
    return-void
.end method
