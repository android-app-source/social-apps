.class public LX/I89;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/I88;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2540762
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2540763
    return-void
.end method


# virtual methods
.method public final a(LX/AhO;)LX/I88;
    .locals 12

    .prologue
    .line 2540764
    new-instance v0, LX/I88;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v3

    check-cast v3, LX/Bl6;

    const-class v1, LX/I8K;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/I8K;

    invoke-static {p0}, LX/Bla;->b(LX/0QB;)LX/Bla;

    move-result-object v5

    check-cast v5, LX/Bla;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v1, 0x15e7

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v1, 0x1b66

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, LX/I88;-><init>(LX/AhO;Landroid/content/Context;LX/Bl6;LX/I8K;LX/Bla;LX/0Uh;Ljava/lang/Boolean;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;LX/0ad;)V

    .line 2540765
    return-object v0
.end method
