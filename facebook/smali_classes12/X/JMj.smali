.class public final LX/JMj;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JMl;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JMl;


# direct methods
.method public constructor <init>(LX/JMl;)V
    .locals 1

    .prologue
    .line 2684611
    iput-object p1, p0, LX/JMj;->b:LX/JMl;

    .line 2684612
    move-object v0, p1

    .line 2684613
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2684614
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2684615
    const-string v0, "ActionYouMayTakeFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2684616
    if-ne p0, p1, :cond_1

    .line 2684617
    :cond_0
    :goto_0
    return v0

    .line 2684618
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2684619
    goto :goto_0

    .line 2684620
    :cond_3
    check-cast p1, LX/JMj;

    .line 2684621
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2684622
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2684623
    if-eq v2, v3, :cond_0

    .line 2684624
    iget-object v2, p0, LX/JMj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JMj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JMj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2684625
    goto :goto_0

    .line 2684626
    :cond_4
    iget-object v2, p1, LX/JMj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
