.class public final LX/HfE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HfF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Ljava/lang/CharSequence;

.field public c:LX/1dQ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2491450
    invoke-static {}, LX/HfF;->q()LX/HfF;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2491451
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2491452
    const-string v0, "WorkFilledButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2491453
    if-ne p0, p1, :cond_1

    .line 2491454
    :cond_0
    :goto_0
    return v0

    .line 2491455
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2491456
    goto :goto_0

    .line 2491457
    :cond_3
    check-cast p1, LX/HfE;

    .line 2491458
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2491459
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2491460
    if-eq v2, v3, :cond_0

    .line 2491461
    iget-boolean v2, p0, LX/HfE;->a:Z

    iget-boolean v3, p1, LX/HfE;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2491462
    goto :goto_0

    .line 2491463
    :cond_4
    iget-object v2, p0, LX/HfE;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/HfE;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/HfE;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2491464
    goto :goto_0

    .line 2491465
    :cond_6
    iget-object v2, p1, LX/HfE;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    .line 2491466
    :cond_7
    iget-object v2, p0, LX/HfE;->c:LX/1dQ;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HfE;->c:LX/1dQ;

    iget-object v3, p1, LX/HfE;->c:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2491467
    goto :goto_0

    .line 2491468
    :cond_8
    iget-object v2, p1, LX/HfE;->c:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
