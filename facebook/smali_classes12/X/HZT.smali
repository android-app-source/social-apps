.class public final LX/HZT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/CheckedTextView;

.field public final synthetic b:Landroid/widget/CheckedTextView;

.field public final synthetic c:LX/HZW;

.field public final synthetic d:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;Landroid/widget/CheckedTextView;Landroid/widget/CheckedTextView;LX/HZW;)V
    .locals 0

    .prologue
    .line 2482225
    iput-object p1, p0, LX/HZT;->d:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    iput-object p2, p0, LX/HZT;->a:Landroid/widget/CheckedTextView;

    iput-object p3, p0, LX/HZT;->b:Landroid/widget/CheckedTextView;

    iput-object p4, p0, LX/HZT;->c:LX/HZW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const v0, 0x75d2cd0d

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2482226
    iget-object v1, p0, LX/HZT;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 2482227
    iget-object v1, p0, LX/HZT;->b:Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 2482228
    iget-object v1, p0, LX/HZT;->d:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    const v2, 0x7f0835a9

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HZT;->d:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    iget-object v3, p0, LX/HZT;->c:LX/HZW;

    iget v3, v3, LX/HZW;->titleResId:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2482229
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/HZT;->d:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, LX/HZT;->c:LX/HZW;

    iget v3, v3, LX/HZW;->titleResId:I

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0835a4

    new-instance v3, LX/HZS;

    invoke-direct {v3, p0}, LX/HZS;-><init>(LX/HZT;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0835a5

    new-instance v3, LX/HZR;

    invoke-direct {v3, p0}, LX/HZR;-><init>(LX/HZT;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2482230
    const v1, 0x6535b28c

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
