.class public LX/JTe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/JTe;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/media/AudioManager;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/media/MediaPlayer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/JS8;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:F

.field public g:Z

.field public h:I

.field public i:Z

.field public j:LX/JTd;

.field public final k:Landroid/os/Handler;

.field public final l:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioManager;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/media/AudioManager;",
            "LX/0Ot",
            "<",
            "Landroid/media/MediaPlayer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2697045
    const/4 v0, 0x0

    iput-object v0, p0, LX/JTe;->e:Landroid/net/Uri;

    .line 2697046
    const/4 v0, 0x0

    iput v0, p0, LX/JTe;->f:F

    .line 2697047
    sget-object v0, LX/JTd;->STOPPED:LX/JTd;

    iput-object v0, p0, LX/JTe;->j:LX/JTd;

    .line 2697048
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/JTe;->k:Landroid/os/Handler;

    .line 2697049
    new-instance v0, Lcom/facebook/feedplugins/musicstory/utils/SongClipPlayer$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/musicstory/utils/SongClipPlayer$1;-><init>(LX/JTe;)V

    iput-object v0, p0, LX/JTe;->l:Ljava/lang/Runnable;

    .line 2697050
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/JTe;->a:Landroid/content/Context;

    .line 2697051
    iput-object p2, p0, LX/JTe;->b:Landroid/media/AudioManager;

    .line 2697052
    iput-object p3, p0, LX/JTe;->c:LX/0Ot;

    .line 2697053
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, LX/441;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/JTe;->d:Ljava/util/Set;

    .line 2697054
    return-void
.end method

.method public static a(LX/0QB;)LX/JTe;
    .locals 6

    .prologue
    .line 2697031
    sget-object v0, LX/JTe;->m:LX/JTe;

    if-nez v0, :cond_1

    .line 2697032
    const-class v1, LX/JTe;

    monitor-enter v1

    .line 2697033
    :try_start_0
    sget-object v0, LX/JTe;->m:LX/JTe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2697034
    if-eqz v2, :cond_0

    .line 2697035
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2697036
    new-instance v5, LX/JTe;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    const/16 p0, 0x21

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/JTe;-><init>(Landroid/content/Context;Landroid/media/AudioManager;LX/0Ot;)V

    .line 2697037
    move-object v0, v5

    .line 2697038
    sput-object v0, LX/JTe;->m:LX/JTe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697039
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2697040
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2697041
    :cond_1
    sget-object v0, LX/JTe;->m:LX/JTe;

    return-object v0

    .line 2697042
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2697043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/JTd;)V
    .locals 0

    .prologue
    .line 2697028
    iput-object p1, p0, LX/JTe;->j:LX/JTd;

    .line 2697029
    invoke-static {p0}, LX/JTe;->e(LX/JTe;)V

    .line 2697030
    return-void
.end method

.method public static a$redex0(LX/JTe;II)V
    .locals 7

    .prologue
    const/16 v6, 0x4b0

    const v5, 0x3daaaaab

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    const/high16 v1, 0x42c80000    # 100.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 2697005
    iget v0, p0, LX/JTe;->h:I

    sub-int v0, p2, v0

    if-le v0, v6, :cond_2

    iget-boolean v0, p0, LX/JTe;->g:Z

    if-eqz v0, :cond_2

    .line 2697006
    const/4 v0, 0x0

    iput v0, p0, LX/JTe;->f:F

    .line 2697007
    iget-boolean v0, p0, LX/JTe;->g:Z

    if-eqz v0, :cond_1

    .line 2697008
    invoke-virtual {p0}, LX/JTe;->c()V

    .line 2697009
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JTe;->g:Z

    .line 2697010
    :cond_0
    :goto_1
    return-void

    .line 2697011
    :cond_1
    invoke-direct {p0}, LX/JTe;->h()V

    goto :goto_0

    .line 2697012
    :cond_2
    if-lez p1, :cond_3

    if-ltz p2, :cond_3

    if-le p2, p1, :cond_4

    .line 2697013
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, LX/JTe;->f:F

    .line 2697014
    :goto_2
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    iget v1, p0, LX/JTe;->f:F

    iget v2, p0, LX/JTe;->f:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_1

    .line 2697015
    :cond_4
    if-ge p2, v6, :cond_5

    .line 2697016
    int-to-float v0, p2

    mul-float/2addr v0, v5

    .line 2697017
    sub-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sub-float v0, v4, v0

    iput v0, p0, LX/JTe;->f:F

    goto :goto_2

    .line 2697018
    :cond_5
    sub-int v0, p1, p2

    if-ge v0, v6, :cond_6

    .line 2697019
    add-int/lit16 v0, p1, -0x4b0

    sub-int v0, p2, v0

    .line 2697020
    int-to-float v0, v0

    mul-float/2addr v0, v5

    sub-float v0, v1, v0

    .line 2697021
    sub-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sub-float v0, v4, v0

    iput v0, p0, LX/JTe;->f:F

    goto :goto_2

    .line 2697022
    :cond_6
    iget-boolean v0, p0, LX/JTe;->g:Z

    if-eqz v0, :cond_7

    .line 2697023
    iget v0, p0, LX/JTe;->h:I

    sub-int v0, p2, v0

    .line 2697024
    int-to-float v0, v0

    mul-float/2addr v0, v5

    sub-float v0, v1, v0

    .line 2697025
    sub-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sub-float v0, v4, v0

    iput v0, p0, LX/JTe;->f:F

    goto :goto_2

    .line 2697026
    :cond_7
    iget v0, p0, LX/JTe;->f:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_0

    .line 2697027
    iput v4, p0, LX/JTe;->f:F

    goto :goto_2
.end method

.method public static e(LX/JTe;)V
    .locals 4

    .prologue
    .line 2697001
    iget-object v0, p0, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JS8;

    .line 2697002
    if-eqz v0, :cond_0

    .line 2697003
    iget-object v2, p0, LX/JTe;->e:Landroid/net/Uri;

    iget-object v3, p0, LX/JTe;->j:LX/JTd;

    invoke-interface {v0, v2, v3}, LX/JS8;->a(Landroid/net/Uri;LX/JTd;)V

    goto :goto_0

    .line 2697004
    :cond_1
    return-void
.end method

.method private g()V
    .locals 7

    .prologue
    .line 2696989
    const/4 v0, 0x1

    .line 2696990
    iget-object v1, p0, LX/JTe;->b:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, p0, v2, v0}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 2696991
    if-ne v1, v0, :cond_1

    :goto_0
    move v0, v0

    .line 2696992
    if-eqz v0, :cond_0

    .line 2696993
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JTe;->g:Z

    .line 2696994
    sget-object v0, LX/JTd;->PLAYING:LX/JTd;

    invoke-direct {p0, v0}, LX/JTe;->a(LX/JTd;)V

    .line 2696995
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    .line 2696996
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 2696997
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    invoke-static {p0, v1, v0}, LX/JTe;->a$redex0(LX/JTe;II)V

    .line 2696998
    iget-object v2, p0, LX/JTe;->l:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 2696999
    iget-object v2, p0, LX/JTe;->k:Landroid/os/Handler;

    iget-object v3, p0, LX/JTe;->l:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    const v6, -0xec9b079

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2697000
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 2696984
    sget-object v0, LX/JTd;->PAUSED:LX/JTd;

    invoke-direct {p0, v0}, LX/JTe;->a(LX/JTd;)V

    .line 2696985
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 2696986
    invoke-direct {p0}, LX/JTe;->k()V

    .line 2696987
    iget-object v0, p0, LX/JTe;->b:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2696988
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 2696982
    iget-object v0, p0, LX/JTe;->k:Landroid/os/Handler;

    iget-object v1, p0, LX/JTe;->l:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2696983
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2697055
    iget-boolean v0, p0, LX/JTe;->i:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 2697056
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    sub-int v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;LX/JS8;Z)V
    .locals 2

    .prologue
    .line 2696948
    iget-object v0, p0, LX/JTe;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/JTe;->j:LX/JTd;

    sget-object v1, LX/JTd;->STOPPED:LX/JTd;

    if-eq v0, v1, :cond_3

    .line 2696949
    iget-object v0, p0, LX/JTe;->j:LX/JTd;

    sget-object v1, LX/JTd;->PLAYING:LX/JTd;

    if-ne v0, v1, :cond_1

    .line 2696950
    if-nez p3, :cond_0

    .line 2696951
    invoke-direct {p0}, LX/JTe;->h()V

    .line 2696952
    :goto_0
    return-void

    .line 2696953
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JTe;->g:Z

    .line 2696954
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, LX/JTe;->h:I

    .line 2696955
    goto :goto_0

    .line 2696956
    :cond_1
    iget-object v0, p0, LX/JTe;->j:LX/JTd;

    sget-object v1, LX/JTd;->BUFFERING:LX/JTd;

    if-ne v0, v1, :cond_2

    .line 2696957
    sget-object v0, LX/JTd;->STOPPED:LX/JTd;

    invoke-direct {p0, v0}, LX/JTe;->a(LX/JTd;)V

    .line 2696958
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    goto :goto_0

    .line 2696959
    :cond_2
    invoke-direct {p0}, LX/JTe;->g()V

    goto :goto_0

    .line 2696960
    :cond_3
    invoke-virtual {p0}, LX/JTe;->c()V

    .line 2696961
    iput-object p1, p0, LX/JTe;->e:Landroid/net/Uri;

    .line 2696962
    iget-object v0, p0, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2696963
    sget-object v0, LX/JTd;->BUFFERING:LX/JTd;

    invoke-direct {p0, v0}, LX/JTe;->a(LX/JTd;)V

    .line 2696964
    :try_start_0
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    .line 2696965
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/JTe;->i:Z

    .line 2696966
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2696967
    :try_start_1
    iget-object v1, p0, LX/JTe;->a:Landroid/content/Context;

    iget-object p1, p0, LX/JTe;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2696968
    :goto_1
    :try_start_2
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2696969
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2696970
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2696971
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 2696972
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2696973
    goto :goto_0

    .line 2696974
    :catch_0
    move-exception v0

    .line 2696975
    iget-object v1, p0, LX/JTe;->e:Landroid/net/Uri;

    invoke-static {v0}, LX/1Bz;->getStackTraceAsString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 2696976
    iget-object p1, p0, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_4
    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JS8;

    .line 2696977
    if-eqz p1, :cond_4

    .line 2696978
    invoke-interface {p1, v1, v0}, LX/JS8;->a(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_2

    .line 2696979
    :cond_5
    goto/16 :goto_0

    .line 2696980
    :catch_1
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 2696981
    iget-object v1, p0, LX/JTe;->a:Landroid/content/Context;

    iget-object p1, p0, LX/JTe;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2696945
    iget-boolean v0, p0, LX/JTe;->i:Z

    if-nez v0, :cond_0

    .line 2696946
    const/4 v0, 0x0

    .line 2696947
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;LX/JS8;)V
    .locals 1

    .prologue
    .line 2696943
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/JTe;->a(Landroid/net/Uri;LX/JS8;Z)V

    .line 2696944
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2696934
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2696935
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2696936
    iget-object v0, p0, LX/JTe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 2696937
    :cond_0
    invoke-direct {p0}, LX/JTe;->k()V

    .line 2696938
    sget-object v0, LX/JTd;->STOPPED:LX/JTd;

    invoke-direct {p0, v0}, LX/JTe;->a(LX/JTd;)V

    .line 2696939
    iget-object v0, p0, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2696940
    iget-object v0, p0, LX/JTe;->b:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2696941
    iput-object v1, p0, LX/JTe;->e:Landroid/net/Uri;

    .line 2696942
    return-void
.end method

.method public final onAudioFocusChange(I)V
    .locals 2

    .prologue
    .line 2696926
    packed-switch p1, :pswitch_data_0

    .line 2696927
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2696928
    :pswitch_1
    iget-object v0, p0, LX/JTe;->j:LX/JTd;

    sget-object v1, LX/JTd;->PLAYING:LX/JTd;

    if-ne v0, v1, :cond_1

    .line 2696929
    invoke-direct {p0}, LX/JTe;->h()V

    goto :goto_0

    .line 2696930
    :cond_1
    iget-object v0, p0, LX/JTe;->j:LX/JTd;

    sget-object v1, LX/JTd;->BUFFERING:LX/JTd;

    if-ne v0, v1, :cond_0

    .line 2696931
    :pswitch_2
    invoke-virtual {p0}, LX/JTe;->c()V

    goto :goto_0

    .line 2696932
    :pswitch_3
    iget-object v0, p0, LX/JTe;->j:LX/JTd;

    sget-object v1, LX/JTd;->PAUSED:LX/JTd;

    if-ne v0, v1, :cond_0

    .line 2696933
    invoke-direct {p0}, LX/JTe;->g()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 2696924
    invoke-virtual {p0}, LX/JTe;->c()V

    .line 2696925
    return-void
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 2

    .prologue
    .line 2696918
    iget-object v0, p0, LX/JTe;->e:Landroid/net/Uri;

    .line 2696919
    iget-object v1, p0, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JS8;

    .line 2696920
    if-eqz v1, :cond_0

    .line 2696921
    invoke-interface {v1, v0, p2, p3}, LX/JS8;->a(Landroid/net/Uri;II)V

    goto :goto_0

    .line 2696922
    :cond_1
    invoke-virtual {p0}, LX/JTe;->c()V

    .line 2696923
    const/4 v0, 0x1

    return v0
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 2696915
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JTe;->i:Z

    .line 2696916
    invoke-direct {p0}, LX/JTe;->g()V

    .line 2696917
    return-void
.end method
