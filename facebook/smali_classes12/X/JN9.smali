.class public final LX/JN9;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JNA;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JNA;


# direct methods
.method public constructor <init>(LX/JNA;)V
    .locals 1

    .prologue
    .line 2685303
    iput-object p1, p0, LX/JN9;->c:LX/JNA;

    .line 2685304
    move-object v0, p1

    .line 2685305
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2685306
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2685307
    const-string v0, "EventsSuggestionHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2685308
    if-ne p0, p1, :cond_1

    .line 2685309
    :cond_0
    :goto_0
    return v0

    .line 2685310
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2685311
    goto :goto_0

    .line 2685312
    :cond_3
    check-cast p1, LX/JN9;

    .line 2685313
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2685314
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2685315
    if-eq v2, v3, :cond_0

    .line 2685316
    iget-object v2, p0, LX/JN9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JN9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JN9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2685317
    goto :goto_0

    .line 2685318
    :cond_5
    iget-object v2, p1, LX/JN9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2685319
    :cond_6
    iget-object v2, p0, LX/JN9;->b:LX/1Pn;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JN9;->b:LX/1Pn;

    iget-object v3, p1, LX/JN9;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2685320
    goto :goto_0

    .line 2685321
    :cond_7
    iget-object v2, p1, LX/JN9;->b:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
