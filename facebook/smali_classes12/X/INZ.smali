.class public LX/INZ;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DK3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Lcom/facebook/resources/ui/FbTextView;

.field public j:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public k:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public l:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public m:Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;

.field public n:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2570924
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2570925
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/INZ;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0xc

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/DK3;->a(LX/0QB;)LX/DK3;

    move-result-object v7

    check-cast v7, LX/DK3;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v3, v2, LX/INZ;->a:LX/0Or;

    iput-object v4, v2, LX/INZ;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, LX/INZ;->c:LX/0Or;

    iput-object v6, v2, LX/INZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v7, v2, LX/INZ;->e:LX/DK3;

    iput-object p1, v2, LX/INZ;->f:LX/0Zb;

    iput-object v0, v2, LX/INZ;->g:LX/0if;

    .line 2570926
    const v0, 0x7f0302fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2570927
    const v0, 0x7f0d0a40

    invoke-virtual {p0, v0}, LX/INZ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/INZ;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2570928
    const v0, 0x7f0d0a41

    invoke-virtual {p0, v0}, LX/INZ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/INZ;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2570929
    const v0, 0x7f0d0a3f

    invoke-virtual {p0, v0}, LX/INZ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;

    iput-object v0, p0, LX/INZ;->m:Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;

    .line 2570930
    const v0, 0x7f0d0a43

    invoke-virtual {p0, v0}, LX/INZ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/INZ;->j:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2570931
    const v0, 0x7f0d0a44

    invoke-virtual {p0, v0}, LX/INZ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/INZ;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2570932
    const v0, 0x7f0d0a45

    invoke-virtual {p0, v0}, LX/INZ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/INZ;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2570933
    const v0, 0x7f0d0a3e

    invoke-virtual {p0, v0}, LX/INZ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/INZ;->n:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2570934
    iget-object v0, p0, LX/INZ;->g:LX/0if;

    sget-object v1, LX/0ig;->aS:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2570935
    const/4 v2, 0x0

    .line 2570936
    invoke-virtual {p0}, LX/INZ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2570937
    const v1, 0x7f020c6f

    invoke-virtual {p0, v1}, LX/INZ;->setBackgroundResource(I)V

    .line 2570938
    invoke-virtual {p0, v2, v0, v2, v2}, LX/INZ;->setPadding(IIII)V

    .line 2570939
    return-void
.end method

.method public static a(LX/INZ;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2570940
    new-instance v0, LX/INY;

    invoke-direct {v0, p0, p3, p1, p2}, LX/INY;-><init>(LX/INZ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/INZ;Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxAnswersModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2570941
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 2570942
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {p3, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    .line 2570943
    iget-object v6, p0, LX/INZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/ILq;->c:LX/0Tn;

    invoke-virtual {v1, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-interface {v6, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2570944
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2570945
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2570946
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxAnswersModel;

    .line 2570947
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxAnswersModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxAnswersModel$CommunityNuxQuestionModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2570948
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxAnswersModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxAnswersModel$CommunityNuxQuestionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxAnswersModel$CommunityNuxQuestionModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2570949
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2570950
    :cond_3
    return-object v4
.end method

.method public static a(Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2570951
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 2570952
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2570953
    invoke-virtual {p0, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2570954
    return-void
.end method

.method public static a$redex0(LX/INZ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2570955
    iget-object v0, p0, LX/INZ;->f:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2570956
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2570957
    const-string v1, "community_nux_question"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2570958
    const-string v1, "community_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2570959
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2570960
    :cond_0
    return-void
.end method
