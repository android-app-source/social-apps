.class public LX/Ikv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/IkY;

.field public final b:LX/Ikn;

.field public final c:LX/IkW;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/Ikx;

.field public final f:Ljava/lang/String;

.field public g:LX/IZ9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "LX/Il0;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/Iku;


# direct methods
.method public constructor <init>(LX/IkY;LX/Iko;LX/Ikx;Ljava/lang/String;LX/IkW;Ljava/util/concurrent/ExecutorService;)V
    .locals 2
    .param p3    # LX/Ikx;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607227
    new-instance v0, LX/Ikt;

    invoke-direct {v0, p0}, LX/Ikt;-><init>(LX/Ikv;)V

    iput-object v0, p0, LX/Ikv;->h:LX/0Vd;

    .line 2607228
    new-instance v0, LX/Iku;

    invoke-direct {v0, p0}, LX/Iku;-><init>(LX/Ikv;)V

    iput-object v0, p0, LX/Ikv;->i:LX/Iku;

    .line 2607229
    iput-object p1, p0, LX/Ikv;->a:LX/IkY;

    .line 2607230
    iput-object p3, p0, LX/Ikv;->e:LX/Ikx;

    .line 2607231
    iput-object p4, p0, LX/Ikv;->f:Ljava/lang/String;

    .line 2607232
    iput-object p5, p0, LX/Ikv;->c:LX/IkW;

    .line 2607233
    iput-object p6, p0, LX/Ikv;->d:Ljava/util/concurrent/ExecutorService;

    .line 2607234
    iget-object v0, p0, LX/Ikv;->i:LX/Iku;

    invoke-virtual {p2, v0}, LX/Iko;->a(LX/Iku;)LX/Ikn;

    move-result-object v0

    iput-object v0, p0, LX/Ikv;->b:LX/Ikn;

    .line 2607235
    iget-object v0, p0, LX/Ikv;->e:LX/Ikx;

    iget-object v1, p0, LX/Ikv;->b:LX/Ikn;

    .line 2607236
    iget-object p1, v0, LX/Ikx;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2607237
    iget-object v0, p0, LX/Ikv;->e:LX/Ikx;

    invoke-virtual {v0}, LX/Ikx;->a()V

    .line 2607238
    iget-object v0, p0, LX/Ikv;->a:LX/IkY;

    iget-object v1, p0, LX/Ikv;->f:Ljava/lang/String;

    iget-object p1, p0, LX/Ikv;->h:LX/0Vd;

    .line 2607239
    new-instance p2, LX/HoY;

    invoke-direct {p2}, LX/HoY;-><init>()V

    move-object p2, p2

    .line 2607240
    const-string p3, "transaction_id"

    invoke-virtual {p2, p3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p2

    check-cast p2, LX/HoY;

    invoke-static {p2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p2

    .line 2607241
    sget-object p3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {p2, p3}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    .line 2607242
    iget-object p3, v0, LX/IkY;->a:LX/0tX;

    invoke-virtual {p3, p2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p2

    invoke-static {p2}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    .line 2607243
    new-instance p3, LX/IkX;

    invoke-direct {p3, v0}, LX/IkX;-><init>(LX/IkY;)V

    .line 2607244
    sget-object p0, LX/131;->INSTANCE:LX/131;

    move-object p0, p0

    .line 2607245
    invoke-static {p2, p3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    .line 2607246
    iget-object p3, v0, LX/IkY;->b:LX/1Ck;

    const-string p0, "fetch_invoice_key"

    invoke-virtual {p3, p0, p2, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2607247
    return-void
.end method

.method public static c(LX/Ikv;)V
    .locals 1

    .prologue
    .line 2607248
    iget-object v0, p0, LX/Ikv;->e:LX/Ikx;

    invoke-virtual {v0}, LX/Ikx;->b()V

    .line 2607249
    iget-object v0, p0, LX/Ikv;->g:LX/IZ9;

    if-eqz v0, :cond_0

    .line 2607250
    iget-object v0, p0, LX/Ikv;->g:LX/IZ9;

    invoke-interface {v0}, LX/IZ9;->b()V

    .line 2607251
    :cond_0
    return-void
.end method
