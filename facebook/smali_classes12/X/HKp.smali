.class public LX/HKp;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HKn;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HKq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2455121
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/HKp;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HKq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455122
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2455123
    iput-object p1, p0, LX/HKp;->b:LX/0Ot;

    .line 2455124
    return-void
.end method

.method public static a(LX/0QB;)LX/HKp;
    .locals 4

    .prologue
    .line 2455125
    const-class v1, LX/HKp;

    monitor-enter v1

    .line 2455126
    :try_start_0
    sget-object v0, LX/HKp;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2455127
    sput-object v2, LX/HKp;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2455128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2455130
    new-instance v3, LX/HKp;

    const/16 p0, 0x2bd8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HKp;-><init>(LX/0Ot;)V

    .line 2455131
    move-object v0, v3

    .line 2455132
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2455133
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2455134
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2455135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Ljava/lang/String;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Ljava/lang/String;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2455136
    const v0, -0x570a2b4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2455137
    check-cast p2, LX/HKo;

    .line 2455138
    iget-object v0, p0, LX/HKp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HKq;

    iget-object v1, p2, LX/HKo;->a:Ljava/lang/String;

    iget-object v2, p2, LX/HKo;->b:Ljava/lang/String;

    iget-object v3, p2, LX/HKo;->c:Ljava/lang/String;

    const/4 p2, 0x2

    const/high16 v7, 0x3f800000    # 1.0f

    .line 2455139
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0052

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    .line 2455140
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a010d

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    .line 2455141
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    .line 2455142
    const v7, -0x570a2b4

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v3, v8, p0

    invoke-static {p1, v7, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2455143
    invoke-interface {v6, v7}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    iget-object v8, v0, LX/HKq;->a:LX/1vg;

    invoke-virtual {v8, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v8

    const p0, 0x7f0208ed

    invoke-virtual {v8, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v8

    const p0, 0x7f0a00fc

    invoke-virtual {v8, p0}, LX/2xv;->j(I)LX/2xv;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    .line 2455144
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0213ab

    invoke-interface {v7, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v7

    const/16 v8, 0x8

    const p0, 0x7f0b0060

    invoke-interface {v7, v8, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x3

    invoke-interface {v7, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/4 p0, 0x0

    invoke-interface {v8, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    const/4 p0, 0x1

    invoke-interface {v8, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2455145
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2455146
    invoke-static {}, LX/1dS;->b()V

    .line 2455147
    iget v0, p1, LX/1dQ;->b:I

    .line 2455148
    packed-switch v0, :pswitch_data_0

    .line 2455149
    :goto_0
    return-object v3

    .line 2455150
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2455151
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/String;

    .line 2455152
    iget-object p1, p0, LX/HKp;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2455153
    new-instance p1, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const/4 v2, 0x1

    invoke-direct {p1, p0, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2455154
    invoke-virtual {p1, v0}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2455155
    sget-object p0, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {p1, p0}, LX/0ht;->a(LX/3AV;)V

    .line 2455156
    invoke-virtual {p1, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2455157
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x570a2b4
        :pswitch_0
    .end packed-switch
.end method
