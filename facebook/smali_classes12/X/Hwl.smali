.class public final LX/Hwl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# instance fields
.field public final synthetic a:LX/2JE;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/2JE;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2521330
    iput-object p1, p0, LX/Hwl;->a:LX/2JE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2521331
    iput-object p2, p0, LX/Hwl;->b:Ljava/lang/String;

    .line 2521332
    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2521333
    const/4 v5, 0x0

    .line 2521334
    const-string v0, "Set-Cookie"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v1

    .line 2521335
    if-nez v1, :cond_0

    .line 2521336
    :goto_0
    return-object v5

    .line 2521337
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2521338
    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 2521339
    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2521340
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2521341
    :cond_1
    iget-object v0, p0, LX/Hwl;->a:LX/2JE;

    iget-object v0, v0, LX/2JE;->j:LX/1Bf;

    iget-object v1, p0, LX/Hwl;->a:LX/2JE;

    iget-object v1, v1, LX/2JE;->c:Landroid/content/Context;

    iget-object v3, p0, LX/Hwl;->b:Ljava/lang/String;

    .line 2521342
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x13

    if-ge v4, p0, :cond_3

    .line 2521343
    :goto_2
    invoke-static {}, LX/048;->a()Z

    move-result v4

    .line 2521344
    if-eqz v4, :cond_2

    .line 2521345
    invoke-static {v0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result p0

    invoke-static {v0}, LX/1Bf;->d(LX/1Bf;)Landroid/os/Bundle;

    move-result-object p1

    invoke-static {v1, v3, v2, p0, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;ZLandroid/os/Bundle;)V

    .line 2521346
    :cond_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2521347
    goto :goto_0

    .line 2521348
    :cond_3
    invoke-static {v1}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 2521349
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object p0

    .line 2521350
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2521351
    invoke-virtual {p0, v3, v4}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 2521352
    :cond_4
    invoke-static {p0}, LX/048;->a(Landroid/webkit/CookieManager;)V

    goto :goto_2
.end method
