.class public LX/I7w;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0aG;

.field public b:LX/1Ck;

.field public d:Lcom/facebook/events/model/Event;

.field public e:Lcom/facebook/events/common/EventAnalyticsParams;

.field public f:Landroid/content/Context;

.field public g:LX/Bl6;

.field public h:Landroid/content/ContentResolver;

.field public i:LX/Bky;

.field public j:LX/0TD;

.field public k:LX/Bm7;

.field private l:LX/0wM;

.field private m:LX/0tX;

.field private n:LX/2l5;

.field private o:LX/BiT;

.field private final p:LX/7vW;

.field public final q:LX/7vZ;

.field public r:LX/0kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2540362
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/I7w;->c:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0aG;LX/1Ck;LX/Bl6;Landroid/content/ContentResolver;LX/Bky;LX/0wM;LX/0TD;LX/0tX;LX/Bm7;LX/2l5;LX/BiT;LX/7vW;LX/7vZ;LX/0kL;)V
    .locals 0
    .param p8    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2540393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540394
    iput-object p1, p0, LX/I7w;->f:Landroid/content/Context;

    .line 2540395
    iput-object p2, p0, LX/I7w;->a:LX/0aG;

    .line 2540396
    iput-object p9, p0, LX/I7w;->m:LX/0tX;

    .line 2540397
    iput-object p3, p0, LX/I7w;->b:LX/1Ck;

    .line 2540398
    iput-object p4, p0, LX/I7w;->g:LX/Bl6;

    .line 2540399
    iput-object p5, p0, LX/I7w;->h:Landroid/content/ContentResolver;

    .line 2540400
    iput-object p6, p0, LX/I7w;->i:LX/Bky;

    .line 2540401
    iput-object p7, p0, LX/I7w;->l:LX/0wM;

    .line 2540402
    iput-object p8, p0, LX/I7w;->j:LX/0TD;

    .line 2540403
    iput-object p10, p0, LX/I7w;->k:LX/Bm7;

    .line 2540404
    iput-object p11, p0, LX/I7w;->n:LX/2l5;

    .line 2540405
    iput-object p12, p0, LX/I7w;->o:LX/BiT;

    .line 2540406
    iput-object p13, p0, LX/I7w;->p:LX/7vW;

    .line 2540407
    iput-object p14, p0, LX/I7w;->q:LX/7vZ;

    .line 2540408
    iput-object p15, p0, LX/I7w;->r:LX/0kL;

    .line 2540409
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/BiU;)I
    .locals 2

    .prologue
    .line 2540384
    sget-object v0, LX/I7u;->b:[I

    invoke-virtual {p1}, LX/BiU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2540385
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2540386
    :pswitch_0
    const v0, 0x7f081f80

    goto :goto_0

    .line 2540387
    :pswitch_1
    const v0, 0x7f081f45

    goto :goto_0

    .line 2540388
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p0, v0, :cond_0

    .line 2540389
    const v0, 0x7f0812a1

    goto :goto_0

    .line 2540390
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p0, v0, :cond_1

    .line 2540391
    const v0, 0x7f0812a2

    goto :goto_0

    .line 2540392
    :cond_1
    const v0, 0x7f081f49

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/I7w;
    .locals 1

    .prologue
    .line 2540383
    invoke-static {p0}, LX/I7w;->b(LX/0QB;)LX/I7w;

    move-result-object v0

    return-object v0
.end method

.method private a(IZ)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2540382
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/I7w;->l:LX/0wM;

    const v1, -0xa76f01

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/I7w;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/BiU;ZZ)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2540375
    sget-object v0, LX/I7u;->b:[I

    invoke-virtual {p1}, LX/BiU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2540376
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported rsvp type for icons: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2540377
    :pswitch_0
    if-eqz p3, :cond_0

    const v0, 0x7f0209e1

    invoke-direct {p0, v0, p2}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2540378
    :goto_0
    return-object v0

    .line 2540379
    :cond_0
    const v0, 0x7f020650

    invoke-direct {p0, v0, p2}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2540380
    :pswitch_1
    if-eqz p3, :cond_1

    const v0, 0x7f0207d6

    invoke-direct {p0, v0, p2}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f020857

    invoke-direct {p0, v0, p2}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2540381
    :pswitch_2
    const v0, 0x7f020818

    invoke-direct {p0, v0, p2}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2540368
    sget-object v2, LX/I7u;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2540369
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported rsvp type for icons: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2540370
    :pswitch_0
    if-eqz p3, :cond_1

    const v2, 0x7f0207d6

    if-ne p1, p2, :cond_0

    :goto_0
    invoke-direct {p0, v2, v0}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2540371
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2540372
    goto :goto_0

    :cond_1
    const v2, 0x7f02067d

    if-ne p1, p2, :cond_2

    :goto_2
    invoke-direct {p0, v2, v0}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 2540373
    :pswitch_1
    if-eqz p3, :cond_4

    const v2, 0x7f020999

    if-ne p1, p2, :cond_3

    :goto_3
    invoke-direct {p0, v2, v0}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    const v2, 0x7f020681

    if-ne p1, p2, :cond_5

    :goto_4
    invoke-direct {p0, v2, v0}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_4

    .line 2540374
    :pswitch_2
    if-eqz p3, :cond_7

    const v2, 0x7f020818

    if-ne p1, p2, :cond_6

    :goto_5
    invoke-direct {p0, v2, v0}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_5

    :cond_7
    const v2, 0x7f020679

    if-ne p1, p2, :cond_8

    :goto_6
    invoke-direct {p0, v2, v0}, LX/I7w;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(LX/BiU;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 2

    .prologue
    .line 2540363
    sget-object v0, LX/I7u;->b:[I

    invoke-virtual {p0}, LX/BiU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2540364
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2540365
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0

    .line 2540366
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0

    .line 2540367
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2540359
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tasks-rsvpEvent:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    .line 2540360
    iget-object p0, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2540361
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;ZLX/0TF;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            "Lcom/facebook/events/common/ActionMechanism;",
            "Z",
            "LX/0TF",
            "<",
            "LX/BlI;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2540353
    new-instance v0, LX/7vC;

    iget-object v1, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    invoke-direct {v0, v1}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    .line 2540354
    iput-object p1, v0, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2540355
    move-object v0, v0

    .line 2540356
    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 2540357
    invoke-virtual/range {v0 .. v6}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/model/Event;Lcom/facebook/events/common/ActionMechanism;ZLX/0TF;Ljava/lang/String;)V

    .line 2540358
    return-void
.end method

.method public static a$redex0(LX/I7w;Lcom/facebook/events/model/Event;LX/BlI;)V
    .locals 2

    .prologue
    .line 2540349
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->H:Z

    move v0, v0

    .line 2540350
    if-nez v0, :cond_0

    .line 2540351
    :goto_0
    return-void

    .line 2540352
    :cond_0
    iget-object v0, p0, LX/I7w;->g:LX/Bl6;

    new-instance v1, LX/BlR;

    invoke-direct {v1, p1, p2}, LX/BlR;-><init>(Lcom/facebook/events/model/Event;LX/BlI;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/I7w;
    .locals 17

    .prologue
    .line 2540347
    new-instance v1, LX/I7w;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v5

    check-cast v5, LX/Bl6;

    invoke-static/range {p0 .. p0}, LX/0cd;->a(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v6

    check-cast v6, Landroid/content/ContentResolver;

    invoke-static/range {p0 .. p0}, LX/Bky;->a(LX/0QB;)LX/Bky;

    move-result-object v7

    check-cast v7, LX/Bky;

    invoke-static/range {p0 .. p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/Bm7;->a(LX/0QB;)LX/Bm7;

    move-result-object v11

    check-cast v11, LX/Bm7;

    invoke-static/range {p0 .. p0}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v12

    check-cast v12, LX/2l5;

    invoke-static/range {p0 .. p0}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v13

    check-cast v13, LX/BiT;

    invoke-static/range {p0 .. p0}, LX/7vW;->a(LX/0QB;)LX/7vW;

    move-result-object v14

    check-cast v14, LX/7vW;

    invoke-static/range {p0 .. p0}, LX/7vZ;->a(LX/0QB;)LX/7vZ;

    move-result-object v15

    check-cast v15, LX/7vZ;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v16

    check-cast v16, LX/0kL;

    invoke-direct/range {v1 .. v16}, LX/I7w;-><init>(Landroid/content/Context;LX/0aG;LX/1Ck;LX/Bl6;Landroid/content/ContentResolver;LX/Bky;LX/0wM;LX/0TD;LX/0tX;LX/Bm7;LX/2l5;LX/BiT;LX/7vW;LX/7vZ;LX/0kL;)V

    .line 2540348
    return-object v1
.end method

.method private static b(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/facebook/graphql/calls/EventGuestStatusMutationValue;
    .end annotation

    .prologue
    .line 2540410
    sget-object v0, LX/I7u;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2540411
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported guest status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2540412
    :pswitch_0
    const-string v0, "going"

    .line 2540413
    :goto_0
    return-object v0

    .line 2540414
    :pswitch_1
    const-string v0, "maybe"

    goto :goto_0

    .line 2540415
    :pswitch_2
    const-string v0, "not_going"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static c(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)I
    .locals 3

    .prologue
    .line 2540341
    sget-object v0, LX/I7u;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2540342
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported rsvp type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2540343
    :pswitch_0
    const v0, 0x7f081f45

    .line 2540344
    :goto_0
    return v0

    .line 2540345
    :pswitch_1
    const v0, 0x7f081f47

    goto :goto_0

    .line 2540346
    :pswitch_2
    const v0, 0x7f081f48

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 10
    .param p1    # LX/7oa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2540330
    if-nez p1, :cond_0

    .line 2540331
    iget-object v0, p0, LX/I7w;->p:LX/7vW;

    iget-object v1, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    .line 2540332
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2540333
    iget-object v2, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2540334
    invoke-virtual {p3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, v1, p2, v2, p0}, LX/7vW;->a(LX/7vW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2540335
    :goto_0
    return-void

    .line 2540336
    :cond_0
    iget-object v0, p0, LX/I7w;->p:LX/7vW;

    iget-object v1, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2540337
    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    .line 2540338
    invoke-static {p1, p2, v3}, LX/7vW;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v6

    .line 2540339
    invoke-interface {p1}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v4

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_1
    const/4 v9, 0x0

    move-object v3, v0

    move-object v5, p2

    move-object v7, v1

    invoke-static/range {v3 .. v9}, LX/7vW;->a(LX/7vW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2540340
    goto :goto_0

    :cond_1
    const-string v8, "unknown"

    goto :goto_1
.end method

.method public final a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 1
    .param p1    # LX/7oa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2540328
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {p0, p1, p2, v0}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2540329
    return-void
.end method

.method public final a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 10
    .param p1    # LX/7oa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2540318
    if-nez p1, :cond_0

    .line 2540319
    iget-object v0, p0, LX/I7w;->q:LX/7vZ;

    iget-object v1, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    .line 2540320
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2540321
    iget-object v2, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, p2, v2, p3}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2540322
    :goto_0
    return-void

    .line 2540323
    :cond_0
    iget-object v0, p0, LX/I7w;->q:LX/7vZ;

    iget-object v1, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2540324
    invoke-interface {p1}, LX/7oa;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v3

    .line 2540325
    invoke-static {p1, p2, v3}, LX/7vZ;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v6

    .line 2540326
    invoke-interface {p1}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v4

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_1
    const/4 v9, 0x0

    move-object v3, v0

    move-object v5, p2

    move-object v7, v1

    invoke-static/range {v3 .. v9}, LX/7vZ;->a(LX/7vZ;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2540327
    goto :goto_0

    :cond_1
    const-string v8, "unknown"

    goto :goto_1
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 9
    .param p1    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2540219
    new-instance v4, LX/6WS;

    iget-object v0, p0, LX/I7w;->f:Landroid/content/Context;

    invoke-direct {v4, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2540220
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v5

    .line 2540221
    sget-object v0, LX/I7w;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2540222
    if-ne v0, p2, :cond_0

    move v1, v2

    .line 2540223
    :goto_1
    invoke-static {v0}, LX/I7w;->c(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)I

    move-result v7

    .line 2540224
    invoke-virtual {v5, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    .line 2540225
    invoke-direct {p0, v0, p2, v3}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2540226
    invoke-virtual {v7, v2}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2540227
    invoke-virtual {v7, v1}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2540228
    new-instance v1, LX/I7p;

    invoke-direct {v1, p0, p1, v0, p5}, LX/I7p;-><init>(LX/I7w;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    invoke-virtual {v7, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_0
    move v1, v3

    .line 2540229
    goto :goto_1

    .line 2540230
    :cond_1
    if-nez p3, :cond_2

    .line 2540231
    invoke-virtual {v4, p4}, LX/0ht;->a(Landroid/view/View;)V

    .line 2540232
    :goto_2
    return-void

    .line 2540233
    :cond_2
    invoke-virtual {v4, p3}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 6
    .param p1    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2540310
    new-instance v1, LX/3Af;

    iget-object v0, p0, LX/I7w;->f:Landroid/content/Context;

    invoke-direct {v1, v0}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2540311
    new-instance v2, LX/7TY;

    iget-object v0, p0, LX/I7w;->f:Landroid/content/Context;

    invoke-direct {v2, v0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2540312
    sget-object v0, LX/I7w;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2540313
    if-eq v0, p2, :cond_0

    .line 2540314
    invoke-static {v0}, LX/I7w;->c(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)I

    move-result v4

    invoke-virtual {v2, v4}, LX/34c;->e(I)LX/3Ai;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {p0, v0, p2, v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, LX/I7r;

    invoke-direct {v5, p0, p1, v0, p3}, LX/I7r;-><init>(LX/I7w;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 2540315
    :cond_1
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2540316
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2540317
    return-void
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 10
    .param p1    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2540299
    new-instance v3, LX/3Af;

    iget-object v0, p0, LX/I7w;->f:Landroid/content/Context;

    invoke-direct {v3, v0}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2540300
    new-instance v4, LX/7TY;

    iget-object v0, p0, LX/I7w;->f:Landroid/content/Context;

    invoke-direct {v4, v0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2540301
    invoke-static {}, LX/BiT;->b()LX/0Px;

    move-result-object v5

    .line 2540302
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_1

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BiU;

    .line 2540303
    invoke-static {v0}, LX/I7w;->a(LX/BiU;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v7

    .line 2540304
    if-eq v7, p2, :cond_0

    .line 2540305
    invoke-static {p2, v0}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/BiU;)I

    move-result v8

    invoke-virtual {v4, v8}, LX/34c;->e(I)LX/3Ai;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct {p0, v0, v2, v9}, LX/I7w;->a(LX/BiU;ZZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v8, LX/I7s;

    invoke-direct {v8, p0, p1, v7}, LX/I7s;-><init>(LX/I7w;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2540306
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2540307
    :cond_1
    invoke-virtual {v3, v4}, LX/3Af;->a(LX/1OM;)V

    .line 2540308
    invoke-virtual {v3}, LX/3Af;->show()V

    .line 2540309
    return-void
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Landroid/view/View;Landroid/view/View;)V
    .locals 11
    .param p1    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2540281
    new-instance v5, LX/6WS;

    iget-object v0, p0, LX/I7w;->f:Landroid/content/Context;

    invoke-direct {v5, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2540282
    invoke-virtual {v5}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    .line 2540283
    invoke-static {}, LX/BiT;->b()LX/0Px;

    move-result-object v7

    .line 2540284
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v4, v3

    :goto_0
    if-ge v4, v8, :cond_1

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BiU;

    .line 2540285
    invoke-static {v0}, LX/I7w;->a(LX/BiU;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v9

    .line 2540286
    if-ne v9, p2, :cond_0

    move v1, v2

    .line 2540287
    :goto_1
    invoke-static {p2, v0}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/BiU;)I

    move-result v10

    .line 2540288
    invoke-virtual {v6, v10}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v10

    .line 2540289
    invoke-direct {p0, v0, v1, v3}, LX/I7w;->a(LX/BiU;ZZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v10, v0}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2540290
    invoke-virtual {v10, v2}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2540291
    invoke-virtual {v10, v1}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2540292
    new-instance v0, LX/I7q;

    invoke-direct {v0, p0, p1, v9}, LX/I7q;-><init>(LX/I7w;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    invoke-virtual {v10, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2540293
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_0
    move v1, v3

    .line 2540294
    goto :goto_1

    .line 2540295
    :cond_1
    if-nez p3, :cond_2

    .line 2540296
    invoke-virtual {v5, p4}, LX/0ht;->a(Landroid/view/View;)V

    .line 2540297
    :goto_2
    return-void

    .line 2540298
    :cond_2
    invoke-virtual {v5, p3}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 2540278
    iput-object p1, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    .line 2540279
    iput-object p2, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2540280
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;Z)V
    .locals 10
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2540264
    new-instance v6, LX/6WS;

    iget-object v0, p0, LX/I7w;->f:Landroid/content/Context;

    invoke-direct {v6, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2540265
    invoke-virtual {v6}, LX/5OM;->c()LX/5OG;

    move-result-object v7

    .line 2540266
    sget-object v0, LX/I7w;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2540267
    invoke-static {v3}, LX/I7w;->c(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)I

    move-result v0

    .line 2540268
    invoke-virtual {v7, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v9

    .line 2540269
    const/4 v0, 0x0

    invoke-direct {p0, v3, p1, v0}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2540270
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2540271
    if-ne v3, p1, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v9, v0}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2540272
    new-instance v0, LX/I7v;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, LX/I7v;-><init>(LX/I7w;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;Z)V

    invoke-virtual {v9, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 2540273
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2540274
    :cond_1
    if-nez p2, :cond_2

    .line 2540275
    invoke-virtual {v6, p3}, LX/0ht;->a(Landroid/view/View;)V

    .line 2540276
    :goto_2
    return-void

    .line 2540277
    :cond_2
    invoke-virtual {v6, p2}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;Z)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2540262
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;ZLX/0TF;Ljava/lang/String;)V

    .line 2540263
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/model/Event;Lcom/facebook/events/common/ActionMechanism;ZLX/0TF;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            "Lcom/facebook/events/model/Event;",
            "Lcom/facebook/events/common/ActionMechanism;",
            "Z",
            "LX/0TF",
            "<",
            "LX/BlI;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2540234
    iget-object v0, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 2540235
    :cond_0
    :goto_0
    return-void

    .line 2540236
    :cond_1
    new-instance v0, LX/7vC;

    iget-object v1, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    invoke-direct {v0, v1}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v5

    .line 2540237
    iget-object v0, p0, LX/I7w;->g:LX/Bl6;

    new-instance v1, LX/BlT;

    sget-object v2, LX/BlI;->SENDING:LX/BlI;

    invoke-direct {v1, v2, v5, p2}, LX/BlT;-><init>(LX/BlI;Lcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2540238
    sget-object v0, LX/BlI;->SENDING:LX/BlI;

    invoke-static {p0, v5, v0}, LX/I7w;->a$redex0(LX/I7w;Lcom/facebook/events/model/Event;LX/BlI;)V

    .line 2540239
    invoke-static {p1}, LX/I7w;->b(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;

    move-result-object v0

    .line 2540240
    iget-object v1, p0, LX/I7w;->h:Landroid/content/ContentResolver;

    iget-object v2, p0, LX/I7w;->i:LX/Bky;

    iget-object v3, p0, LX/I7w;->j:LX/0TD;

    invoke-static {v1, v2, p2, v3}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V

    .line 2540241
    new-instance v1, LX/4ES;

    invoke-direct {v1}, LX/4ES;-><init>()V

    .line 2540242
    new-instance v2, LX/4EG;

    invoke-direct {v2}, LX/4EG;-><init>()V

    .line 2540243
    iget-object v3, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 2540244
    iget-object v3, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2540245
    iget-object v3, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 2540246
    :cond_2
    new-instance v3, LX/4EG;

    invoke-direct {v3}, LX/4EG;-><init>()V

    .line 2540247
    iget-object v4, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 2540248
    invoke-virtual {p3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 2540249
    if-eqz p6, :cond_3

    .line 2540250
    invoke-virtual {v3, p6}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 2540251
    :cond_3
    new-instance v4, LX/4EL;

    invoke-direct {v4}, LX/4EL;-><init>()V

    .line 2540252
    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 2540253
    move-object v2, v4

    .line 2540254
    invoke-virtual {v1, v2}, LX/4ES;->a(LX/4EL;)LX/4ES;

    move-result-object v1

    iget-object v2, p0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    .line 2540255
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2540256
    invoke-virtual {v1, v2}, LX/4ES;->b(Ljava/lang/String;)LX/4ES;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/4ES;->c(Ljava/lang/String;)LX/4ES;

    move-result-object v0

    .line 2540257
    iget-object v1, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2540258
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ES;->a(Ljava/util/List;)LX/4ES;

    .line 2540259
    :cond_4
    invoke-static {}, LX/7uR;->e()LX/7uI;

    move-result-object v1

    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7uI;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2540260
    iget-object v1, p0, LX/I7w;->m:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2540261
    iget-object v7, p0, LX/I7w;->b:LX/1Ck;

    invoke-direct {p0, p1}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;

    move-result-object v8

    new-instance v0, LX/I7o;

    move-object v1, p0

    move-object v2, p2

    move v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, LX/I7o;-><init>(LX/I7w;Lcom/facebook/events/model/Event;ZLX/0TF;Lcom/facebook/events/model/Event;)V

    invoke-virtual {v7, v8, v6, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method
