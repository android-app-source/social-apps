.class public LX/HxP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0ad;

.field public b:Z


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2522487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2522488
    iput-object p1, p0, LX/HxP;->a:LX/0ad;

    .line 2522489
    return-void
.end method

.method public static a(LX/0QB;)LX/HxP;
    .locals 4

    .prologue
    .line 2522491
    const-class v1, LX/HxP;

    monitor-enter v1

    .line 2522492
    :try_start_0
    sget-object v0, LX/HxP;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2522493
    sput-object v2, LX/HxP;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2522494
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2522495
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2522496
    new-instance p0, LX/HxP;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/HxP;-><init>(LX/0ad;)V

    .line 2522497
    move-object v0, p0

    .line 2522498
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2522499
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HxP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2522500
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2522501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2522490
    iget-boolean v1, p0, LX/HxP;->b:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/HxP;->a:LX/0ad;

    sget-short v2, LX/347;->q:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
