.class public final enum LX/Hjt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hjt;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/Hjt;

.field public static final enum b:LX/Hjt;

.field public static final enum c:LX/Hjt;

.field public static final enum d:LX/Hjt;

.field public static final enum e:LX/Hjt;

.field public static final enum f:LX/Hjt;

.field public static final enum g:LX/Hjt;

.field public static final enum h:LX/Hjt;

.field public static final enum i:LX/Hjt;

.field public static final enum j:LX/Hjt;

.field public static final enum k:LX/Hjt;

.field public static final enum l:LX/Hjt;

.field private static final n:[LX/Hjt;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;

.field private static final synthetic q:[LX/Hjt;


# instance fields
.field private final m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    new-instance v1, LX/Hjt;

    const-string v2, "APP_AD"

    invoke-direct {v1, v2, v0, v0}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->a:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "LINK_AD"

    invoke-direct {v1, v2, v5, v5}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->b:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "APP_AD_V2"

    invoke-direct {v1, v2, v6, v6}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->c:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "LINK_AD_V2"

    invoke-direct {v1, v2, v7, v7}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->d:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "APP_ENGAGEMENT_AD"

    invoke-direct {v1, v2, v8, v8}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->e:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "AD_CHOICES"

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->f:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "JS_TRIGGER"

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v1, v2, v3, v4}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->g:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "JS_TRIGGER_NO_AUTO_IMP_LOGGING"

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v1, v2, v3, v4}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->h:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "VIDEO_AD"

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v1, v2, v3, v4}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->i:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "INLINE_VIDEO_AD"

    const/16 v3, 0x9

    const/16 v4, 0x9

    invoke-direct {v1, v2, v3, v4}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->j:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "BANNER_TO_INTERSTITIAL"

    const/16 v3, 0xa

    const/16 v4, 0xa

    invoke-direct {v1, v2, v3, v4}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->k:LX/Hjt;

    new-instance v1, LX/Hjt;

    const-string v2, "NATIVE_CLOSE_BUTTON"

    const/16 v3, 0xb

    const/16 v4, 0xb

    invoke-direct {v1, v2, v3, v4}, LX/Hjt;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/Hjt;->l:LX/Hjt;

    const/16 v1, 0xc

    new-array v1, v1, [LX/Hjt;

    sget-object v2, LX/Hjt;->a:LX/Hjt;

    aput-object v2, v1, v0

    sget-object v2, LX/Hjt;->b:LX/Hjt;

    aput-object v2, v1, v5

    sget-object v2, LX/Hjt;->c:LX/Hjt;

    aput-object v2, v1, v6

    sget-object v2, LX/Hjt;->d:LX/Hjt;

    aput-object v2, v1, v7

    sget-object v2, LX/Hjt;->e:LX/Hjt;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, LX/Hjt;->f:LX/Hjt;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, LX/Hjt;->g:LX/Hjt;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, LX/Hjt;->h:LX/Hjt;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, LX/Hjt;->i:LX/Hjt;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, LX/Hjt;->j:LX/Hjt;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, LX/Hjt;->k:LX/Hjt;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, LX/Hjt;->l:LX/Hjt;

    aput-object v3, v1, v2

    sput-object v1, LX/Hjt;->q:[LX/Hjt;

    const/4 v1, 0x5

    new-array v1, v1, [LX/Hjt;

    sget-object v2, LX/Hjt;->d:LX/Hjt;

    aput-object v2, v1, v0

    sget-object v2, LX/Hjt;->e:LX/Hjt;

    aput-object v2, v1, v5

    sget-object v2, LX/Hjt;->f:LX/Hjt;

    aput-object v2, v1, v6

    sget-object v2, LX/Hjt;->h:LX/Hjt;

    aput-object v2, v1, v7

    sget-object v2, LX/Hjt;->l:LX/Hjt;

    aput-object v2, v1, v8

    sput-object v1, LX/Hjt;->n:[LX/Hjt;

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    sget-object v2, LX/Hjt;->n:[LX/Hjt;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-virtual {v4}, LX/Hjt;->a()I

    move-result v4

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hjt;->o:Ljava/lang/String;

    const-string v0, ","

    sget-object v1, LX/Hjt;->n:[LX/Hjt;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hjt;->p:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/Hjt;->m:I

    return-void
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    goto :goto_1

    :goto_0
    return-object v0

    :goto_1
    sget-object v0, LX/Hjt;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    sget-object v0, LX/Hjt;->p:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hjt;
    .locals 1

    const-class v0, LX/Hjt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hjt;

    return-object v0
.end method

.method public static values()[LX/Hjt;
    .locals 1

    sget-object v0, LX/Hjt;->q:[LX/Hjt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hjt;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, LX/Hjt;->m:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, LX/Hjt;->m:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
