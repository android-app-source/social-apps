.class public LX/J0B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J0B;


# instance fields
.field private final a:LX/0Xl;


# direct methods
.method public constructor <init>(LX/0Xl;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636594
    iput-object p1, p0, LX/J0B;->a:LX/0Xl;

    .line 2636595
    return-void
.end method

.method public static a(LX/0QB;)LX/J0B;
    .locals 4

    .prologue
    .line 2636580
    sget-object v0, LX/J0B;->b:LX/J0B;

    if-nez v0, :cond_1

    .line 2636581
    const-class v1, LX/J0B;

    monitor-enter v1

    .line 2636582
    :try_start_0
    sget-object v0, LX/J0B;->b:LX/J0B;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2636583
    if-eqz v2, :cond_0

    .line 2636584
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2636585
    new-instance p0, LX/J0B;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-direct {p0, v3}, LX/J0B;-><init>(LX/0Xl;)V

    .line 2636586
    move-object v0, p0

    .line 2636587
    sput-object v0, LX/J0B;->b:LX/J0B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2636588
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2636589
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2636590
    :cond_1
    sget-object v0, LX/J0B;->b:LX/J0B;

    return-object v0

    .line 2636591
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2636592
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/J0B;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2636578
    iget-object v0, p0, LX/J0B;->a:LX/0Xl;

    invoke-interface {v0, p1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2636579
    return-void
.end method


# virtual methods
.method public final a(LX/DtQ;J)V
    .locals 2

    .prologue
    .line 2636572
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2636573
    const-string v1, "extra_transfer_status"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2636574
    const-string v1, "extra_transfer_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2636575
    const-string v1, "com.facebook.messaging.payment.ACTION_PAYMENT_TRANSACTION_CACHE_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2636576
    invoke-static {p0, v0}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2636577
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2636567
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2636568
    const-string v1, "extra_payment_request_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2636569
    const-string v1, "com.facebook.messaging.payment.ACTION_PAYMENT_REQUEST_CACHE_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2636570
    invoke-static {p0, v0}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2636571
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2636559
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2636560
    const-string v1, "com.facebook.messaging.payment.ACTION_PAYMENT_CARD_CACHE_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2636561
    invoke-static {p0, v0}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2636562
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2636563
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2636564
    const-string v1, "com.facebook.messaging.payment.ACTION_PAYMENT_PLATFORM_CONTEXTS_CACHE_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2636565
    invoke-static {p0, v0}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2636566
    return-void
.end method
