.class public final LX/HPb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HPd;


# direct methods
.method public constructor <init>(LX/HPd;)V
    .locals 0

    .prologue
    .line 2462236
    iput-object p1, p0, LX/HPb;->a:LX/HPd;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2462245
    iget-object v0, p0, LX/HPb;->a:LX/HPd;

    iget-object v0, v0, LX/HPd;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x130082

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2462246
    iget-object v0, p0, LX/HPb;->a:LX/HPd;

    invoke-static {v0}, LX/HPd;->e(LX/HPd;)V

    .line 2462247
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2462237
    const v2, 0x130082

    .line 2462238
    iget-object v0, p0, LX/HPb;->a:LX/HPd;

    iget-object v0, v0, LX/HPd;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x79

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 2462239
    iget-object v0, p0, LX/HPb;->a:LX/HPd;

    const/4 v1, 0x1

    .line 2462240
    iput-boolean v1, v0, LX/HPd;->h:Z

    .line 2462241
    iget-object v0, p0, LX/HPb;->a:LX/HPd;

    iget-boolean v0, v0, LX/HPd;->i:Z

    if-eqz v0, :cond_0

    .line 2462242
    iget-object v0, p0, LX/HPb;->a:LX/HPd;

    iget-object v0, v0, LX/HPd;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "OutOfOrder"

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2462243
    :cond_0
    iget-object v0, p0, LX/HPb;->a:LX/HPd;

    invoke-static {v0}, LX/HPd;->g(LX/HPd;)V

    .line 2462244
    return-void
.end method
