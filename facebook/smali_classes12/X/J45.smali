.class public LX/J45;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final m:Ljava/lang/Object;


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/1Ck;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/J5g;

.field public final e:LX/J49;

.field public final f:LX/J4N;

.field public final g:Lcom/facebook/privacy/PrivacyOperationsClient;

.field public final h:LX/03V;

.field private final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/J4K;",
            "LX/J4L;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field public l:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2643622
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/J45;->m:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/1Ck;Ljava/util/concurrent/ExecutorService;LX/J5g;LX/J49;LX/J4N;Lcom/facebook/privacy/PrivacyOperationsClient;LX/03V;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2643610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643611
    iput-object p1, p0, LX/J45;->a:LX/0SG;

    .line 2643612
    iput-object p2, p0, LX/J45;->b:LX/1Ck;

    .line 2643613
    iput-object p3, p0, LX/J45;->c:Ljava/util/concurrent/ExecutorService;

    .line 2643614
    iput-object p4, p0, LX/J45;->d:LX/J5g;

    .line 2643615
    iput-object p5, p0, LX/J45;->e:LX/J49;

    .line 2643616
    iput-object p6, p0, LX/J45;->f:LX/J4N;

    .line 2643617
    iput-object p7, p0, LX/J45;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 2643618
    iput-object p8, p0, LX/J45;->h:LX/03V;

    .line 2643619
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/J45;->i:Ljava/util/HashMap;

    .line 2643620
    invoke-static {p0}, LX/J45;->g(LX/J45;)V

    .line 2643621
    return-void
.end method

.method public static a(LX/0QB;)LX/J45;
    .locals 7

    .prologue
    .line 2643583
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2643584
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2643585
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2643586
    if-nez v1, :cond_0

    .line 2643587
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2643588
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2643589
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2643590
    sget-object v1, LX/J45;->m:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2643591
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2643592
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2643593
    :cond_1
    if-nez v1, :cond_4

    .line 2643594
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2643595
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2643596
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/J45;->b(LX/0QB;)LX/J45;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2643597
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2643598
    if-nez v1, :cond_2

    .line 2643599
    sget-object v0, LX/J45;->m:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J45;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2643600
    :goto_1
    if-eqz v0, :cond_3

    .line 2643601
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2643602
    :goto_3
    check-cast v0, LX/J45;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2643603
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2643604
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2643605
    :catchall_1
    move-exception v0

    .line 2643606
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2643607
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2643608
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2643609
    :cond_2
    :try_start_8
    sget-object v0, LX/J45;->m:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J45;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/J45;
    .locals 9

    .prologue
    .line 2643581
    new-instance v0, LX/J45;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/J5g;->a(LX/0QB;)LX/J5g;

    move-result-object v4

    check-cast v4, LX/J5g;

    invoke-static {p0}, LX/J49;->a(LX/0QB;)LX/J49;

    move-result-object v5

    check-cast v5, LX/J49;

    invoke-static {p0}, LX/J4N;->a(LX/0QB;)LX/J4N;

    move-result-object v6

    check-cast v6, LX/J4N;

    invoke-static {p0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v7

    check-cast v7, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v0 .. v8}, LX/J45;-><init>(LX/0SG;LX/1Ck;Ljava/util/concurrent/ExecutorService;LX/J5g;LX/J49;LX/J4N;Lcom/facebook/privacy/PrivacyOperationsClient;LX/03V;)V

    .line 2643582
    return-object v0
.end method

.method public static g(LX/J45;)V
    .locals 15

    .prologue
    const/4 v1, 0x0

    .line 2643570
    iget-object v0, p0, LX/J45;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2643571
    invoke-static {}, LX/J4K;->values()[LX/J4K;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2643572
    iget-object v5, p0, LX/J45;->i:Ljava/util/HashMap;

    new-instance v6, LX/J4L;

    invoke-direct {v6, v4}, LX/J4L;-><init>(LX/J4K;)V

    invoke-virtual {v5, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643573
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2643574
    :cond_0
    iput-boolean v1, p0, LX/J45;->j:Z

    .line 2643575
    iget-object v0, p0, LX/J45;->e:LX/J49;

    invoke-virtual {v0}, LX/J49;->b()V

    .line 2643576
    iget-object v11, p0, LX/J45;->a:LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v11

    const-wide/16 v13, 0x3e8

    div-long/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object v7, v11

    .line 2643577
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iput-wide v7, p0, LX/J45;->l:J

    .line 2643578
    iget-object v7, p0, LX/J45;->e:LX/J49;

    iget-wide v9, p0, LX/J45;->l:J

    .line 2643579
    iput-wide v9, v7, LX/J49;->t:J

    .line 2643580
    return-void
.end method


# virtual methods
.method public final a(LX/J4K;)V
    .locals 1

    .prologue
    .line 2643568
    const-string v0, "navigation"

    invoke-virtual {p0, p1, v0}, LX/J45;->a(LX/J4K;Ljava/lang/String;)V

    .line 2643569
    return-void
.end method

.method public final a(LX/J4K;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2643563
    sget-object v0, LX/J41;->b:[I

    invoke-virtual {p1}, LX/J4K;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2643564
    :goto_0
    return-void

    .line 2643565
    :pswitch_0
    iget-object v0, p0, LX/J45;->e:LX/J49;

    sget-object v1, LX/5nj;->COMPOSER_STEP_EXPOSED:LX/5nj;

    invoke-virtual {v0, v1, p2}, LX/J49;->a(LX/5nj;Ljava/lang/String;)V

    goto :goto_0

    .line 2643566
    :pswitch_1
    iget-object v0, p0, LX/J45;->e:LX/J49;

    sget-object v1, LX/5nj;->PROFILE_STEP_EXPOSED:LX/5nj;

    invoke-virtual {v0, v1, p2}, LX/J49;->a(LX/5nj;Ljava/lang/String;)V

    goto :goto_0

    .line 2643567
    :pswitch_2
    iget-object v0, p0, LX/J45;->e:LX/J49;

    sget-object v1, LX/5nj;->APP_STEP_EXPOSED:LX/5nj;

    invoke-virtual {v0, v1, p2}, LX/J49;->a(LX/5nj;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/J5h;)V
    .locals 1

    .prologue
    .line 2643552
    iget-object v0, p0, LX/J45;->e:LX/J49;

    invoke-virtual {v0, p1}, LX/J49;->a(LX/J5h;)V

    .line 2643553
    return-void
.end method

.method public final a(LX/J6P;Z)V
    .locals 4

    .prologue
    .line 2643561
    iget-object v0, p0, LX/J45;->b:LX/1Ck;

    sget-object v1, LX/J44;->FETCH_COMPOSER_PRIVACY_OPTIONS:LX/J44;

    new-instance v2, LX/J3u;

    invoke-direct {v2, p0, p2}, LX/J3u;-><init>(LX/J45;Z)V

    new-instance v3, LX/J42;

    invoke-direct {v3, p0, p1, p2}, LX/J42;-><init>(LX/J45;LX/J6P;Z)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2643562
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643559
    iget-object v0, p0, LX/J45;->e:LX/J49;

    sget-object v1, LX/5nj;->INTRO_STEP_EXPOSED:LX/5nj;

    invoke-virtual {v0, v1, p1}, LX/J49;->a(LX/5nj;Ljava/lang/String;)V

    .line 2643560
    return-void
.end method

.method public final b(LX/J4K;)LX/J4L;
    .locals 1

    .prologue
    .line 2643556
    iget-object v0, p0, LX/J45;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J4L;

    .line 2643557
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643558
    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2643554
    iget-object v0, p0, LX/J45;->e:LX/J49;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/J49;->a(Z)V

    .line 2643555
    return-void
.end method
