.class public LX/HlQ;
.super LX/1qk;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HlQ;


# direct methods
.method public constructor <init>(LX/1r1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2498611
    const-string v0, "BackgroundLocationReportingNewImpl"

    invoke-direct {p0, p1, v0}, LX/1qk;-><init>(LX/1r1;Ljava/lang/String;)V

    .line 2498612
    return-void
.end method

.method public static a(LX/0QB;)LX/HlQ;
    .locals 4

    .prologue
    .line 2498613
    sget-object v0, LX/HlQ;->b:LX/HlQ;

    if-nez v0, :cond_1

    .line 2498614
    const-class v1, LX/HlQ;

    monitor-enter v1

    .line 2498615
    :try_start_0
    sget-object v0, LX/HlQ;->b:LX/HlQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2498616
    if-eqz v2, :cond_0

    .line 2498617
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2498618
    new-instance p0, LX/HlQ;

    invoke-static {v0}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v3

    check-cast v3, LX/1r1;

    invoke-direct {p0, v3}, LX/HlQ;-><init>(LX/1r1;)V

    .line 2498619
    move-object v0, p0

    .line 2498620
    sput-object v0, LX/HlQ;->b:LX/HlQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2498621
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2498622
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2498623
    :cond_1
    sget-object v0, LX/HlQ;->b:LX/HlQ;

    return-object v0

    .line 2498624
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2498625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
