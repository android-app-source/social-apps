.class public LX/Ikr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/IlJ;

.field private final b:LX/IlH;

.field private final c:LX/IlC;

.field private final d:LX/IlL;

.field private final e:LX/IlF;

.field private final f:LX/IlA;

.field private final g:LX/IlE;


# direct methods
.method public constructor <init>(LX/IlJ;LX/IlH;LX/IlC;LX/IlL;LX/IlF;LX/IlA;LX/IlE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607120
    iput-object p1, p0, LX/Ikr;->a:LX/IlJ;

    .line 2607121
    iput-object p2, p0, LX/Ikr;->b:LX/IlH;

    .line 2607122
    iput-object p3, p0, LX/Ikr;->c:LX/IlC;

    .line 2607123
    iput-object p4, p0, LX/Ikr;->d:LX/IlL;

    .line 2607124
    iput-object p5, p0, LX/Ikr;->e:LX/IlF;

    .line 2607125
    iput-object p6, p0, LX/Ikr;->f:LX/IlA;

    .line 2607126
    iput-object p7, p0, LX/Ikr;->g:LX/IlE;

    .line 2607127
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/Ikp;
    .locals 4

    .prologue
    .line 2607092
    invoke-static {p2}, LX/Il1;->getItemFromViewType(I)LX/Il1;

    move-result-object v0

    .line 2607093
    sget-object v1, LX/Ikq;->a:[I

    invoke-virtual {v0}, LX/Il1;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2607094
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Item of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/Il1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not implemented"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2607095
    :pswitch_0
    new-instance v0, LX/IlI;

    invoke-direct {v0, p1}, LX/IlI;-><init>(Landroid/view/ViewGroup;)V

    .line 2607096
    move-object v0, v0

    .line 2607097
    :goto_0
    return-object v0

    .line 2607098
    :pswitch_1
    new-instance v0, LX/IlG;

    invoke-direct {v0, p1}, LX/IlG;-><init>(Landroid/view/ViewGroup;)V

    .line 2607099
    move-object v0, v0

    .line 2607100
    goto :goto_0

    .line 2607101
    :pswitch_2
    iget-object v0, p0, LX/Ikr;->e:LX/IlF;

    .line 2607102
    new-instance v2, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-direct {v2, v1, p1}, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2607103
    move-object v0, v2

    .line 2607104
    goto :goto_0

    .line 2607105
    :pswitch_3
    iget-object v0, p0, LX/Ikr;->c:LX/IlC;

    new-instance v1, LX/Il3;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Il3;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1, v1}, LX/IlC;->a(Landroid/view/ViewGroup;LX/Iky;)LX/IlB;

    move-result-object v0

    goto :goto_0

    .line 2607106
    :pswitch_4
    iget-object v0, p0, LX/Ikr;->c:LX/IlC;

    new-instance v1, LX/Il5;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Il5;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1, v1}, LX/IlC;->a(Landroid/view/ViewGroup;LX/Iky;)LX/IlB;

    move-result-object v0

    goto :goto_0

    .line 2607107
    :pswitch_5
    iget-object v0, p0, LX/Ikr;->c:LX/IlC;

    new-instance v1, LX/Il4;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Il4;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1, v1}, LX/IlC;->a(Landroid/view/ViewGroup;LX/Iky;)LX/IlB;

    move-result-object v0

    goto :goto_0

    .line 2607108
    :pswitch_6
    iget-object v0, p0, LX/Ikr;->d:LX/IlL;

    .line 2607109
    new-instance v2, LX/IlK;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-direct {v2, v1, p1}, LX/IlK;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2607110
    move-object v0, v2

    .line 2607111
    goto :goto_0

    .line 2607112
    :pswitch_7
    iget-object v0, p0, LX/Ikr;->f:LX/IlA;

    .line 2607113
    new-instance v2, LX/Il9;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-direct {v2, v1, p1}, LX/Il9;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2607114
    move-object v0, v2

    .line 2607115
    goto :goto_0

    .line 2607116
    :pswitch_8
    new-instance v0, LX/IlD;

    invoke-direct {v0, p1}, LX/IlD;-><init>(Landroid/view/ViewGroup;)V

    .line 2607117
    move-object v0, v0

    .line 2607118
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
