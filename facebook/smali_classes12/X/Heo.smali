.class public final LX/Heo;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/Hep;


# direct methods
.method public constructor <init>(LX/Hep;)V
    .locals 0

    .prologue
    .line 2490821
    iput-object p1, p0, LX/Heo;->a:LX/Hep;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 8

    .prologue
    .line 2490822
    iget-object v0, p0, LX/Heo;->a:LX/Hep;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 2490823
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    float-to-double v6, v1

    sub-double/2addr v4, v6

    iget-object v6, v0, LX/Hep;->d:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 2490824
    iget-object v5, v0, LX/Hep;->d:Landroid/view/View;

    int-to-float v6, v4

    invoke-virtual {v5, v6}, Landroid/view/View;->setTranslationY(F)V

    .line 2490825
    iget-object v5, v0, LX/Hep;->e:Landroid/view/View;

    iget v6, v0, LX/Hep;->c:I

    iget-object v7, v0, LX/Hep;->e:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    sub-int/2addr v4, v6

    int-to-float v4, v4

    invoke-virtual {v5, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 2490826
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 4

    .prologue
    .line 2490827
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 2490828
    iget-object v0, p0, LX/Heo;->a:LX/Hep;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/Hep;->setVisibility(I)V

    .line 2490829
    :cond_0
    return-void
.end method
