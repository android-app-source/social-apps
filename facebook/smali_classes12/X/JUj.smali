.class public LX/JUj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JUh;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2699641
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JUj;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JUk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699642
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2699643
    iput-object p1, p0, LX/JUj;->b:LX/0Ot;

    .line 2699644
    return-void
.end method

.method public static a(LX/0QB;)LX/JUj;
    .locals 4

    .prologue
    .line 2699645
    const-class v1, LX/JUj;

    monitor-enter v1

    .line 2699646
    :try_start_0
    sget-object v0, LX/JUj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699647
    sput-object v2, LX/JUj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699648
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699649
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699650
    new-instance v3, LX/JUj;

    const/16 p0, 0x2082

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JUj;-><init>(LX/0Ot;)V

    .line 2699651
    move-object v0, v3

    .line 2699652
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699653
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699654
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699655
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2699656
    check-cast p2, LX/JUi;

    .line 2699657
    iget-object v0, p0, LX/JUj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-wide v2, p2, LX/JUi;->a:D

    iget-wide v4, p2, LX/JUi;->b:D

    iget-object v6, p2, LX/JUi;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-object v1, p1

    .line 2699658
    new-instance v7, LX/4X8;

    invoke-direct {v7}, LX/4X8;-><init>()V

    .line 2699659
    iput-wide v2, v7, LX/4X8;->b:D

    .line 2699660
    move-object v7, v7

    .line 2699661
    iput-wide v4, v7, LX/4X8;->c:D

    .line 2699662
    move-object v7, v7

    .line 2699663
    invoke-virtual {v7}, LX/4X8;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v7

    .line 2699664
    const-string v8, "pages_multi_locations_map"

    invoke-static {v6, v8, v7}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLocation;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v7

    .line 2699665
    invoke-static {v1}, LX/Jcf;->c(LX/1De;)LX/Jcd;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/Jcd;->a(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)LX/Jcd;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, LX/Jcd;->a(Z)LX/Jcd;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v8, 0x7f0b25c1

    invoke-interface {v7, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b25c2

    invoke-interface {v7, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v7

    const v8, 0x7f021388

    invoke-interface {v7, v8}, LX/1Di;->z(I)LX/1Di;

    move-result-object v7

    const v8, 0x7f08150e

    invoke-virtual {v1, v8}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v7

    .line 2699666
    const v8, -0x56e90a46

    const/4 v9, 0x0

    invoke-static {v1, v8, v9}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v8

    move-object v8, v8

    .line 2699667
    invoke-interface {v7, v8}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 2699668
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2699669
    invoke-static {}, LX/1dS;->b()V

    .line 2699670
    iget v0, p1, LX/1dQ;->b:I

    .line 2699671
    packed-switch v0, :pswitch_data_0

    .line 2699672
    :goto_0
    return-object v2

    .line 2699673
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2699674
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2699675
    check-cast v1, LX/JUi;

    .line 2699676
    iget-object v3, p0, LX/JUj;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JUk;

    iget-object v4, v1, LX/JUi;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/JUi;->e:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object p2, v1, LX/JUi;->f:Ljava/lang/String;

    .line 2699677
    iget-object p0, v3, LX/JUk;->a:LX/JUQ;

    invoke-virtual {p0, v0, v4, p1, p2}, LX/JUQ;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Ljava/lang/String;)V

    .line 2699678
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x56e90a46
        :pswitch_0
    .end packed-switch
.end method
