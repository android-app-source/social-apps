.class public final LX/HWk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Uh;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2477018
    iput-object p1, p0, LX/HWk;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)Z
    .locals 5

    .prologue
    .line 2477019
    iget-object v0, p0, LX/HWk;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    sget-object v1, LX/HX6;->SCROLL_TO_TAB:LX/HX6;

    invoke-static {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/HX6;)V

    .line 2477020
    iget-object v0, p0, LX/HWk;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v1, "HasClickedTab"

    invoke-virtual {v0, v1}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2477021
    iget-object v0, p0, LX/HWk;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v0

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    .line 2477022
    iget-object v1, p0, LX/HWk;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v0

    .line 2477023
    :goto_0
    sget-object v2, LX/CSM;->CLICKED:LX/CSM;

    iput-object v2, v1, LX/CSN;->a:LX/CSM;

    .line 2477024
    iget-object v2, v1, LX/CSN;->b:LX/0if;

    sget-object v3, LX/0ig;->ag:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "tap_"

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p2, "position:"

    invoke-direct {p0, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, v4, p0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2477025
    const/4 v0, 0x0

    return v0

    .line 2477026
    :cond_0
    const-string v0, "NULL"

    goto :goto_0
.end method
