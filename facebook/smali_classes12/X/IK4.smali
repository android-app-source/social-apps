.class public final LX/IK4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<",
        "Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IK6;


# direct methods
.method public constructor <init>(LX/IK6;)V
    .locals 0

    .prologue
    .line 2564943
    iput-object p1, p0, LX/IK4;->a:LX/IK6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2564926
    iget-object v0, p0, LX/IK4;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    invoke-virtual {v0, p4}, LX/IK9;->a(LX/2kM;)V

    .line 2564927
    iget-object v0, p0, LX/IK4;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    sget-object v1, LX/1lD;->LOAD_FINISHED:LX/1lD;

    invoke-virtual {v0, v1}, LX/IK9;->a(LX/1lD;)V

    .line 2564928
    iget-object v0, p0, LX/IK4;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    invoke-virtual {v0, p4}, LX/IK9;->a(LX/2kM;)V

    .line 2564929
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2564930
    return-void
.end method

.method public final bridge synthetic a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 0
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564931
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564932
    iget-object v0, p0, LX/IK4;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    invoke-virtual {v0, v1}, LX/IK9;->a(LX/1lD;)V

    .line 2564933
    return-void
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564934
    iget-object v0, p0, LX/IK4;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    iget-object v1, p0, LX/IK4;->a:LX/IK6;

    iget-object v1, v1, LX/IK6;->k:LX/2kW;

    .line 2564935
    iget-object p1, v1, LX/2kW;->o:LX/2kM;

    move-object v1, p1

    .line 2564936
    invoke-virtual {v0, v1}, LX/IK9;->a(LX/2kM;)V

    .line 2564937
    iget-object v0, p0, LX/IK4;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    sget-object v1, LX/1lD;->LOAD_FINISHED:LX/1lD;

    invoke-virtual {v0, v1}, LX/IK9;->a(LX/1lD;)V

    .line 2564938
    iget-object v0, p0, LX/IK4;->a:LX/IK6;

    iget-object v0, v0, LX/IK6;->a:LX/IK9;

    invoke-virtual {v0}, LX/IK9;->d()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 2564939
    iget-object v0, p0, LX/IK4;->a:LX/IK6;

    iget-object v1, p0, LX/IK4;->a:LX/IK6;

    iget-object v1, v1, LX/IK6;->a:LX/IK9;

    .line 2564940
    iget-object p0, v1, LX/IK9;->b:Ljava/lang/String;

    move-object v1, p0

    .line 2564941
    invoke-virtual {v0, v1}, LX/IK6;->a(Ljava/lang/String;)V

    .line 2564942
    :cond_0
    return-void
.end method
