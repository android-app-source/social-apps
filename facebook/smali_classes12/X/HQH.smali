.class public final LX/HQH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9hN;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPhoto;

.field public final synthetic b:LX/74S;

.field public final synthetic c:LX/1bf;

.field public final synthetic d:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Lcom/facebook/graphql/model/GraphQLPhoto;LX/74S;LX/1bf;)V
    .locals 0

    .prologue
    .line 2463137
    iput-object p1, p0, LX/HQH;->d:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    iput-object p2, p0, LX/HQH;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object p3, p0, LX/HQH;->b:LX/74S;

    iput-object p4, p0, LX/HQH;->c:LX/1bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/9hO;
    .locals 3

    .prologue
    .line 2463138
    iget-object v0, p0, LX/HQH;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2463139
    new-instance v1, LX/9hO;

    iget-object v0, p0, LX/HQH;->b:LX/74S;

    sget-object v2, LX/74S;->PAGE_COVER_PHOTO:LX/74S;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, LX/HQH;->d:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    iget-object v0, v0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    :goto_0
    invoke-static {v0}, LX/9hP;->a(Lcom/facebook/drawee/view/DraweeView;)LX/9hP;

    move-result-object v0

    iget-object v2, p0, LX/HQH;->c:LX/1bf;

    invoke-direct {v1, v0, v2}, LX/9hO;-><init>(LX/9hP;LX/1bf;)V

    move-object v0, v1

    .line 2463140
    :goto_1
    return-object v0

    .line 2463141
    :cond_0
    iget-object v0, p0, LX/HQH;->d:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    iget-object v0, v0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 2463142
    iget-object v2, v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-object v0, v2

    .line 2463143
    goto :goto_0

    .line 2463144
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
