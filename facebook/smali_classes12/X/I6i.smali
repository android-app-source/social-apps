.class public final LX/I6i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/I6k;


# direct methods
.method public constructor <init>(LX/I6k;)V
    .locals 0

    .prologue
    .line 2537849
    iput-object p1, p0, LX/I6i;->a:LX/I6k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V
    .locals 11

    .prologue
    .line 2537850
    iget-object v0, p0, LX/I6i;->a:LX/I6k;

    iget-object v0, v0, LX/I6k;->b:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    if-eq v0, p1, :cond_0

    .line 2537851
    new-instance v0, LX/I70;

    invoke-direct {v0}, LX/I70;-><init>()V

    iget-object v1, p0, LX/I6i;->a:LX/I6k;

    iget-object v1, v1, LX/I6k;->d:Ljava/lang/String;

    .line 2537852
    iput-object v1, v0, LX/I70;->a:Ljava/lang/String;

    .line 2537853
    move-object v0, v0

    .line 2537854
    iput-object p1, v0, LX/I70;->b:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 2537855
    move-object v1, v0

    .line 2537856
    new-instance v0, LX/4Ee;

    invoke-direct {v0}, LX/4Ee;-><init>()V

    iget-object v2, p0, LX/I6i;->a:LX/I6k;

    iget-object v2, v2, LX/I6k;->d:Ljava/lang/String;

    .line 2537857
    const-string v3, "event_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2537858
    move-object v0, v0

    .line 2537859
    invoke-static {p1}, LX/I6k;->c(Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)Ljava/lang/String;

    move-result-object v2

    .line 2537860
    const-string v3, "level"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2537861
    move-object v0, v0

    .line 2537862
    new-instance v2, LX/I6y;

    invoke-direct {v2}, LX/I6y;-><init>()V

    move-object v2, v2

    .line 2537863
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/I6y;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 2537864
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2537865
    iget-object v5, v1, LX/I70;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2537866
    iget-object v7, v1, LX/I70;->b:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-virtual {v4, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2537867
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 2537868
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 2537869
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 2537870
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2537871
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2537872
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2537873
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2537874
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2537875
    new-instance v5, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;

    invoke-direct {v5, v4}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;-><init>(LX/15i;)V

    .line 2537876
    move-object v1, v5

    .line 2537877
    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 2537878
    iget-object v1, p0, LX/I6i;->a:LX/I6k;

    iget-object v1, v1, LX/I6k;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2537879
    :cond_0
    return-void
.end method
