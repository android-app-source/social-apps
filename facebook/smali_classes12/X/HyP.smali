.class public final LX/HyP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;)V
    .locals 0

    .prologue
    .line 2524187
    iput-object p1, p0, LX/HyP;->a:Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, -0x4dd7cdcf

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2524177
    iget-object v1, p0, LX/HyP;->a:Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->a:LX/1nQ;

    iget-object v2, p0, LX/HyP;->a:Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2524178
    iget-object p1, v2, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v2, p1

    .line 2524179
    invoke-virtual {v2}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v2

    invoke-virtual {v1, v2}, LX/1nQ;->a(I)V

    .line 2524180
    iget-object v1, p0, LX/HyP;->a:Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->b:LX/Hx5;

    .line 2524181
    iget-boolean v2, v1, LX/Hx5;->C:Z

    if-eq v2, v4, :cond_1

    .line 2524182
    iput-boolean v4, v1, LX/Hx5;->C:Z

    .line 2524183
    if-eqz v4, :cond_0

    .line 2524184
    invoke-static {v1}, LX/Hx5;->j(LX/Hx5;)V

    .line 2524185
    :cond_0
    const v2, 0x506b1339

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2524186
    :cond_1
    const v1, -0x7a204c93

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
