.class public LX/IkD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/IkC;

.field private final b:LX/IkE;


# direct methods
.method public constructor <init>(LX/IkC;LX/IkE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606496
    iput-object p1, p0, LX/IkD;->a:LX/IkC;

    .line 2606497
    iput-object p2, p0, LX/IkD;->b:LX/IkE;

    .line 2606498
    return-void
.end method

.method public static a(LX/0QB;)LX/IkD;
    .locals 5

    .prologue
    .line 2606499
    const-class v1, LX/IkD;

    monitor-enter v1

    .line 2606500
    :try_start_0
    sget-object v0, LX/IkD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606501
    sput-object v2, LX/IkD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606502
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606503
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606504
    new-instance p0, LX/IkD;

    invoke-static {v0}, LX/IkC;->a(LX/0QB;)LX/IkC;

    move-result-object v3

    check-cast v3, LX/IkC;

    invoke-static {v0}, LX/IkE;->a(LX/0QB;)LX/IkE;

    move-result-object v4

    check-cast v4, LX/IkE;

    invoke-direct {p0, v3, v4}, LX/IkD;-><init>(LX/IkC;LX/IkE;)V

    .line 2606505
    move-object v0, p0

    .line 2606506
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606507
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606508
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606509
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
