.class public LX/HRT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final c:LX/2nq;

.field public final d:LX/17W;

.field private final e:LX/2Yg;

.field private final f:LX/3Cm;

.field public final g:LX/3DA;

.field public final h:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2465436
    const-class v0, LX/HRT;

    sput-object v0, LX/HRT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2nq;LX/1Pq;LX/17W;LX/2Yg;LX/3Cm;LX/3DA;LX/03V;)V
    .locals 0
    .param p1    # LX/2nq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nq;",
            "TE;",
            "LX/17W;",
            "LX/2Yg;",
            "LX/3Cm;",
            "Lcom/facebook/notifications/util/NotificationStoryLauncher;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2465427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2465428
    iput-object p1, p0, LX/HRT;->c:LX/2nq;

    .line 2465429
    iput-object p2, p0, LX/HRT;->b:LX/1Pq;

    .line 2465430
    iput-object p3, p0, LX/HRT;->d:LX/17W;

    .line 2465431
    iput-object p4, p0, LX/HRT;->e:LX/2Yg;

    .line 2465432
    iput-object p5, p0, LX/HRT;->f:LX/3Cm;

    .line 2465433
    iput-object p6, p0, LX/HRT;->g:LX/3DA;

    .line 2465434
    iput-object p7, p0, LX/HRT;->h:LX/03V;

    .line 2465435
    return-void
.end method

.method public static a(LX/HRT;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2465415
    new-instance v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v1}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v2

    .line 2465416
    iput-object v2, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    .line 2465417
    move-object v1, v1

    .line 2465418
    iput-boolean v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    .line 2465419
    move-object v1, v1

    .line 2465420
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2465421
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    .line 2465422
    move-object v0, v1

    .line 2465423
    iget-object v1, p0, LX/HRT;->e:LX/2Yg;

    const-string v2, "notifications_jewel_module"

    .line 2465424
    iget-object v3, v1, LX/2Yg;->b:LX/0Zb;

    new-instance p0, Lcom/facebook/notifications/logging/NotificationsLogger$JewelNotificationEvent;

    sget-object p1, LX/3B2;->graph_notification_click:LX/3B2;

    invoke-direct {p0, v1, p1, v0, v2}, Lcom/facebook/notifications/logging/NotificationsLogger$JewelNotificationEvent;-><init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V

    invoke-interface {v3, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2465425
    return-void

    .line 2465426
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x1d1644c0

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2465400
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2465401
    iget-object v0, p0, LX/HRT;->c:LX/2nq;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2465402
    iget-object v0, p0, LX/HRT;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    iget-object v4, p0, LX/HRT;->c:LX/2nq;

    invoke-interface {v0, v4}, LX/2kk;->a(LX/2nq;)V

    .line 2465403
    if-nez v3, :cond_1

    .line 2465404
    iget-object v0, p0, LX/HRT;->h:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/HRT;->a:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "_null_notif_story"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "Null notif story in adapter"

    invoke-virtual {v0, v4, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2465405
    :cond_0
    :goto_0
    iget-object v0, p0, LX/HRT;->b:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2465406
    const v0, -0x5e4eca24

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2465407
    :cond_1
    invoke-static {p0, v3}, LX/HRT;->a(LX/HRT;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2465408
    invoke-static {v3}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 2465409
    if-eqz v0, :cond_2

    iget-object v4, p0, LX/HRT;->d:LX/17W;

    invoke-virtual {v4, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 2465410
    :goto_1
    if-nez v0, :cond_0

    .line 2465411
    iget-object v0, p0, LX/HRT;->h:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/HRT;->a:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "_launch_failed"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p1, "Could not launch notification story "

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2465412
    :cond_2
    iget-object v0, p0, LX/HRT;->g:LX/3DA;

    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 2465413
    const/4 v6, -0x1

    invoke-virtual {v0, v2, v4, v6}, LX/3DA;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;I)Z

    move-result v6

    move v0, v6

    .line 2465414
    goto :goto_1
.end method
