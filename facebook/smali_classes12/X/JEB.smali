.class public final LX/JEB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2665538
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2665539
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2665540
    :goto_0
    return v1

    .line 2665541
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2665542
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2665543
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2665544
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2665545
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2665546
    const-string v4, "actors"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2665547
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2665548
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 2665549
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2665550
    invoke-static {p0, p1}, LX/JE9;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2665551
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2665552
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2665553
    goto :goto_1

    .line 2665554
    :cond_3
    const-string v4, "message"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2665555
    invoke-static {p0, p1}, LX/JEA;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2665556
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2665557
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2665558
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2665559
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2665560
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2665561
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2665562
    if-eqz v0, :cond_1

    .line 2665563
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665564
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2665565
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2665566
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/JE9;->a(LX/15i;ILX/0nX;)V

    .line 2665567
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2665568
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2665569
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2665570
    if-eqz v0, :cond_2

    .line 2665571
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665572
    invoke-static {p0, v0, p2}, LX/JEA;->a(LX/15i;ILX/0nX;)V

    .line 2665573
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2665574
    return-void
.end method
