.class public final LX/I9F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I9G;


# direct methods
.method public constructor <init>(LX/I9G;)V
    .locals 0

    .prologue
    .line 2542619
    iput-object p1, p0, LX/I9F;->a:LX/I9G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x1669ec7a

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2542620
    iget-object v1, p0, LX/I9F;->a:LX/I9G;

    iget-object v1, v1, LX/I9G;->c:LX/I7i;

    if-nez v1, :cond_0

    .line 2542621
    const v1, 0x7a17f0ee

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2542622
    :goto_0
    return-void

    .line 2542623
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2542624
    const v2, 0x7f0d01af

    if-ne v1, v2, :cond_1

    .line 2542625
    iget-object v1, p0, LX/I9F;->a:LX/I9G;

    iget-object v1, v1, LX/I9G;->c:LX/I7i;

    invoke-virtual {v1}, LX/I7i;->a()V

    .line 2542626
    :goto_1
    const v1, -0x5654cfb4

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2542627
    :cond_1
    const v2, 0x7f0d01b0

    if-ne v1, v2, :cond_3

    .line 2542628
    iget-object v1, p0, LX/I9F;->a:LX/I9G;

    iget-object v1, v1, LX/I9G;->c:LX/I7i;

    .line 2542629
    iget-object v2, v1, LX/I7i;->k:Lcom/facebook/events/model/Event;

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    if-nez v2, :cond_6

    .line 2542630
    :cond_2
    :goto_2
    goto :goto_1

    .line 2542631
    :cond_3
    const v2, 0x7f0d01b1

    if-ne v1, v2, :cond_5

    .line 2542632
    iget-object v1, p0, LX/I9F;->a:LX/I9G;

    iget-object v1, v1, LX/I9G;->c:LX/I7i;

    .line 2542633
    iget-object v2, v1, LX/I7i;->k:Lcom/facebook/events/model/Event;

    if-eqz v2, :cond_4

    iget-object v2, v1, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    if-nez v2, :cond_7

    .line 2542634
    :cond_4
    :goto_3
    goto :goto_1

    .line 2542635
    :cond_5
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown composer id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const v1, -0x4209df0f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    throw v2

    .line 2542636
    :cond_6
    iget-object v2, v1, LX/I7i;->f:LX/IBL;

    iget-object v3, v1, LX/I7i;->k:Lcom/facebook/events/model/Event;

    new-instance v4, LX/I7f;

    invoke-direct {v4, v1}, LX/I7f;-><init>(LX/I7i;)V

    invoke-virtual {v2, v3, v4}, LX/IBL;->a(Lcom/facebook/events/model/Event;LX/0QK;)V

    goto :goto_2

    .line 2542637
    :cond_7
    iget-object v2, v1, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 2542638
    iget-object v3, v1, LX/I7i;->j:Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x9

    iget-object p0, v1, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v3, v2, v4, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_3
.end method
