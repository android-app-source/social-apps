.class public LX/IID;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/IID;


# instance fields
.field public final a:LX/0Zb;

.field public final b:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

.field public c:J

.field public d:LX/IIC;

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/common/time/AwakeTimeSinceBootClock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2561603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2561604
    sget-object v0, LX/IIC;->UNKNOWN:LX/IIC;

    iput-object v0, p0, LX/IID;->d:LX/IIC;

    .line 2561605
    iput-object p1, p0, LX/IID;->a:LX/0Zb;

    .line 2561606
    iput-object p2, p0, LX/IID;->b:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    .line 2561607
    return-void
.end method

.method public static a(LX/0QB;)LX/IID;
    .locals 5

    .prologue
    .line 2561554
    sget-object v0, LX/IID;->g:LX/IID;

    if-nez v0, :cond_1

    .line 2561555
    const-class v1, LX/IID;

    monitor-enter v1

    .line 2561556
    :try_start_0
    sget-object v0, LX/IID;->g:LX/IID;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2561557
    if-eqz v2, :cond_0

    .line 2561558
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2561559
    new-instance p0, LX/IID;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    invoke-direct {p0, v3, v4}, LX/IID;-><init>(LX/0Zb;Lcom/facebook/common/time/AwakeTimeSinceBootClock;)V

    .line 2561560
    move-object v0, p0

    .line 2561561
    sput-object v0, LX/IID;->g:LX/IID;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2561562
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2561563
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2561564
    :cond_1
    sget-object v0, LX/IID;->g:LX/IID;

    return-object v0

    .line 2561565
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2561566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 2561599
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "background_location"

    .line 2561600
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2561601
    move-object v0, v0

    .line 2561602
    const-string v1, "session_id"

    iget-wide v2, p0, LX/IID;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/IIC;)V
    .locals 3

    .prologue
    .line 2561593
    iget-object v0, p0, LX/IID;->d:LX/IIC;

    if-ne v0, p1, :cond_0

    .line 2561594
    :goto_0
    return-void

    .line 2561595
    :cond_0
    iput-object p1, p0, LX/IID;->d:LX/IIC;

    .line 2561596
    const-string v0, "friends_nearby_dashboard_impression"

    invoke-static {p0, v0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2561597
    const-string v1, "view"

    iget-object v2, p1, LX/IIC;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2561598
    iget-object v1, p0, LX/IID;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2561588
    const-string v0, "friends_nearby_dashboard_timeline"

    invoke-static {p0, v0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2561589
    const-string v1, "targetID"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2561590
    const-string v1, "useEntityCard"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2561591
    iget-object v1, p0, LX/IID;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561592
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2561584
    const-string v0, "friends_nearby_dashboard_pause_option_select"

    invoke-static {p0, v0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2561585
    const-string v1, "pause_option"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2561586
    iget-object v1, p0, LX/IID;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561587
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2561581
    const-string v0, "friends_nearby_dashboard_unpause"

    invoke-static {p0, v0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2561582
    iget-object v1, p0, LX/IID;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561583
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2561577
    const-string v0, "friends_nearby_dashboard_ping"

    invoke-static {p0, v0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2561578
    const-string v1, "targetID"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "wave_sent"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2561579
    iget-object v1, p0, LX/IID;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561580
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2561574
    const-string v0, "friends_nearby_dashboard_selfview_feedback"

    invoke-static {p0, v0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2561575
    iget-object v1, p0, LX/IID;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561576
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2561570
    const-string v0, "friends_nearby_dashboard_map_interaction"

    invoke-static {p0, v0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2561571
    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2561572
    iget-object v1, p0, LX/IID;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561573
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2561567
    const-string v0, "friends_nearby_dashboard_invite_empty"

    invoke-static {p0, v0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2561568
    iget-object v1, p0, LX/IID;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561569
    return-void
.end method
