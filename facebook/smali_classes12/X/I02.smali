.class public final enum LX/I02;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I02;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I02;

.field public static final enum RSVP_BAR_HIDDEN:LX/I02;

.field public static final enum SET_OPTIMISTIC_GUEST_STATUS:LX/I02;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2526401
    new-instance v0, LX/I02;

    const-string v1, "RSVP_BAR_HIDDEN"

    invoke-direct {v0, v1, v2}, LX/I02;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I02;->RSVP_BAR_HIDDEN:LX/I02;

    .line 2526402
    new-instance v0, LX/I02;

    const-string v1, "SET_OPTIMISTIC_GUEST_STATUS"

    invoke-direct {v0, v1, v3}, LX/I02;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I02;->SET_OPTIMISTIC_GUEST_STATUS:LX/I02;

    .line 2526403
    const/4 v0, 0x2

    new-array v0, v0, [LX/I02;

    sget-object v1, LX/I02;->RSVP_BAR_HIDDEN:LX/I02;

    aput-object v1, v0, v2

    sget-object v1, LX/I02;->SET_OPTIMISTIC_GUEST_STATUS:LX/I02;

    aput-object v1, v0, v3

    sput-object v0, LX/I02;->$VALUES:[LX/I02;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2526404
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I02;
    .locals 1

    .prologue
    .line 2526405
    const-class v0, LX/I02;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I02;

    return-object v0
.end method

.method public static values()[LX/I02;
    .locals 1

    .prologue
    .line 2526406
    sget-object v0, LX/I02;->$VALUES:[LX/I02;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I02;

    return-object v0
.end method
