.class public final LX/IXw;
.super LX/Chm;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;)V
    .locals 1

    .prologue
    .line 2587081
    iput-object p1, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    invoke-direct {p0}, LX/Chm;-><init>()V

    .line 2587082
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IXw;->b:Z

    return-void
.end method

.method private a(LX/CiX;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2587083
    iget-object v2, p1, LX/CiX;->b:LX/Cqw;

    move-object v2, v2

    .line 2587084
    iget-object v3, v2, LX/Cqw;->e:LX/Cqu;

    move-object v3, v3

    .line 2587085
    iget-object v4, v2, LX/Cqw;->f:LX/Cqt;

    move-object v4, v4

    .line 2587086
    iget-object v5, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-boolean v5, v5, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->p:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v5, v5, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->q:LX/Cqw;

    .line 2587087
    iget-object v6, v5, LX/Cqw;->e:LX/Cqu;

    move-object v5, v6

    .line 2587088
    if-ne v3, v5, :cond_0

    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->q:LX/Cqw;

    .line 2587089
    iget-object v5, v3, LX/Cqw;->f:LX/Cqt;

    move-object v3, v5

    .line 2587090
    if-eq v4, v3, :cond_2

    .line 2587091
    :cond_0
    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    .line 2587092
    iput-boolean v0, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->p:Z

    .line 2587093
    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    .line 2587094
    iput-object v2, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->q:LX/Cqw;

    .line 2587095
    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    invoke-static {v3, v2}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->b(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;LX/Cqw;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2587096
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v2}, LX/IXs;->getCurrentHeight()I

    move-result v2

    .line 2587097
    if-eqz v2, :cond_1

    .line 2587098
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v3}, LX/IXs;->getCurrentHeight()I

    move-result v3

    .line 2587099
    iput v3, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->o:I

    .line 2587100
    :cond_1
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    .line 2587101
    iput-boolean v0, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->r:Z

    .line 2587102
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    .line 2587103
    iput v1, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->w:I

    .line 2587104
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v3}, LX/IXs;->getCurrentHeight()I

    move-result v3

    .line 2587105
    invoke-static {v2, v3, v1}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a$redex0(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V

    .line 2587106
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    if-eqz v2, :cond_2

    .line 2587107
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v2}, LX/CnR;->getTop()I

    move-result v2

    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v3}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v3

    if-eq v2, v3, :cond_4

    :goto_0
    iput-boolean v0, p0, LX/IXw;->b:Z

    .line 2587108
    iget-boolean v0, p0, LX/IXw;->b:Z

    if-eqz v0, :cond_2

    .line 2587109
    iget-object v0, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v2}, LX/CnR;->getTop()I

    move-result v2

    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v3}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->c(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V

    .line 2587110
    :cond_2
    :goto_1
    iget-boolean v0, p1, LX/CiX;->d:Z

    move v0, v0

    .line 2587111
    if-eqz v0, :cond_3

    .line 2587112
    iget-object v0, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    .line 2587113
    iput-boolean v1, v0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->p:Z

    .line 2587114
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 2587115
    goto :goto_0

    .line 2587116
    :cond_5
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    .line 2587117
    iput-boolean v1, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->r:Z

    .line 2587118
    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    .line 2587119
    iput v0, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->w:I

    .line 2587120
    iget-object v0, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v2}, LX/IXs;->getCurrentHeight()I

    move-result v2

    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget v3, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->o:I

    .line 2587121
    invoke-static {v0, v2, v3}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a$redex0(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V

    .line 2587122
    iget-object v0, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/IXw;->b:Z

    if-eqz v0, :cond_2

    .line 2587123
    iget-object v0, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v2}, LX/CnR;->getBottom()I

    move-result v2

    iget-object v3, p0, LX/IXw;->a:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v3}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->c(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V

    goto :goto_1
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2587124
    check-cast p1, LX/CiX;

    invoke-direct {p0, p1}, LX/IXw;->a(LX/CiX;)V

    return-void
.end method
