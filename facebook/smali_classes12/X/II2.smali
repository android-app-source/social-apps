.class public final LX/II2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/0gc;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

.field public final synthetic d:LX/0am;

.field public final synthetic e:LX/IIB;


# direct methods
.method public constructor <init>(LX/IIB;LX/0gc;JLcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;LX/0am;)V
    .locals 1

    .prologue
    .line 2561345
    iput-object p1, p0, LX/II2;->e:LX/IIB;

    iput-object p2, p0, LX/II2;->a:LX/0gc;

    iput-wide p3, p0, LX/II2;->b:J

    iput-object p5, p0, LX/II2;->c:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    iput-object p6, p0, LX/II2;->d:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    .line 2561346
    iget-object v0, p0, LX/II2;->e:LX/IIB;

    iget-object v0, v0, LX/IIB;->f:LX/IID;

    .line 2561347
    const-string v1, "friends_nearby_dashboard_selfview_report_location"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2561348
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561349
    new-instance v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyDashboardActionButtonHandler$FeedbackConfirmDialogFragment;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyDashboardActionButtonHandler$FeedbackConfirmDialogFragment;-><init>()V

    iget-object v1, p0, LX/II2;->a:LX/0gc;

    const-string v2, "feedback_confirm_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2561350
    iget-object v0, p0, LX/II2;->e:LX/IIB;

    iget-wide v2, p0, LX/II2;->b:J

    iget-object v1, p0, LX/II2;->c:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    iget-object v4, p0, LX/II2;->d:LX/0am;

    .line 2561351
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2561352
    iget-object v5, v0, LX/IIB;->c:LX/03V;

    const-string v6, "nearby_friends_self_view_fetch_location_fail"

    const-string v7, "Failed to fetch location for feedback"

    invoke-virtual {v5, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561353
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2561354
    :cond_0
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/location/ImmutableLocation;

    .line 2561355
    new-instance v6, LX/96H;

    invoke-direct {v6}, LX/96H;-><init>()V

    move-object v7, v6

    .line 2561356
    new-instance v8, LX/4Dx;

    invoke-direct {v8}, LX/4Dx;-><init>()V

    .line 2561357
    invoke-virtual {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 2561358
    const-string v9, "region_id"

    invoke-virtual {v8, v9, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561359
    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    .line 2561360
    const-string v9, "latitude"

    invoke-virtual {v8, v9, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2561361
    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    .line 2561362
    const-string v9, "longitude"

    invoke-virtual {v8, v9, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2561363
    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v6, v9}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    .line 2561364
    const-string v9, "accuracy_meters"

    invoke-virtual {v8, v9, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2561365
    iget-object v6, v0, LX/IIB;->e:LX/0yD;

    invoke-virtual {v6, v5}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v5

    long-to-double v5, v5

    const-wide v9, 0x408f400000000000L    # 1000.0

    div-double/2addr v5, v9

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    .line 2561366
    const-string v6, "age_seconds"

    invoke-virtual {v8, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2561367
    const-string v5, "input"

    invoke-virtual {v7, v5, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2561368
    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2561369
    iget-object v6, v0, LX/IIB;->a:LX/1Ck;

    .line 2561370
    sget-object v7, LX/IFy;->p:LX/IFy;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "_"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v7

    check-cast v7, LX/IFy;

    move-object v7, v7

    .line 2561371
    iget-object v8, v0, LX/IIB;->b:LX/0tX;

    sget-object v9, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v8, v5, v9}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v8, LX/II9;

    invoke-direct {v8, v0}, LX/II9;-><init>(LX/IIB;)V

    invoke-virtual {v6, v7, v5, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method
