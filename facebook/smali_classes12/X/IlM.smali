.class public final LX/IlM;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;)V
    .locals 0

    .prologue
    .line 2607657
    iput-object p1, p0, LX/IlM;->a:Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2607658
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2607659
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2607660
    iget-object v0, p0, LX/IlM;->a:Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/6up;->a(Landroid/content/Context;)V

    .line 2607661
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2607662
    iget-object v0, p0, LX/IlM;->a:Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->r:LX/IjX;

    if-eqz v0, :cond_0

    .line 2607663
    iget-object v0, p0, LX/IlM;->a:Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->r:LX/IjX;

    .line 2607664
    new-instance p0, LX/73T;

    sget-object p1, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {p0, p1}, LX/73T;-><init>(LX/73S;)V

    .line 2607665
    iget-object p1, v0, LX/IjX;->a:LX/IjY;

    iget-object p1, p1, LX/IjY;->c:LX/IjZ;

    .line 2607666
    iget-object v0, p1, LX/73V;->a:LX/6qh;

    invoke-virtual {v0, p0}, LX/6qh;->a(LX/73T;)V

    .line 2607667
    :cond_0
    return-void
.end method
