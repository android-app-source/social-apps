.class public final LX/HeJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V
    .locals 0

    .prologue
    .line 2490045
    iput-object p1, p0, LX/HeJ;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/facebook/search/api/SearchTypeaheadResult;I)V
    .locals 11

    .prologue
    .line 2490046
    invoke-virtual {p1}, Lcom/facebook/search/api/SearchTypeaheadResult;->b()Ljava/lang/String;

    move-result-object v0

    .line 2490047
    if-eqz v0, :cond_0

    .line 2490048
    iget-object v1, p0, LX/HeJ;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v1, v1, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490049
    iget-object v2, v1, LX/He8;->d:Ljava/util/List;

    move-object v1, v2

    .line 2490050
    iget-object v2, p0, LX/HeJ;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v2, v2, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->q:LX/G5R;

    iget-object v3, p0, LX/HeJ;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v3, v3, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->u:Ljava/lang/String;

    .line 2490051
    const-string v6, "call_quick_action"

    move-object v5, v2

    move-object v7, v3

    move-object v8, p1

    move v9, p2

    move-object v10, v1

    invoke-static/range {v5 .. v10}, LX/G5R;->a(LX/G5R;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/util/List;)V

    .line 2490052
    iget-object v1, p0, LX/HeJ;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v2, p0, LX/HeJ;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tel:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2490053
    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->a$redex0(Lcom/facebook/uberbar/ui/UberbarResultsFragment;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2490054
    :goto_0
    return-void

    .line 2490055
    :cond_0
    iget-object v0, p0, LX/HeJ;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v0, v0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->p:LX/03V;

    const-string v1, "NullPointerException"

    const-string v2, "Ubersearch: Tried calling a person with no phone numbers"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
