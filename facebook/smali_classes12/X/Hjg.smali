.class public LX/Hjg;
.super LX/HjU;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/Hl3;

.field public c:LX/Hjm;

.field public d:LX/Hk8;

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/Hjk;

.field public g:Landroid/content/Context;

.field public h:J

.field public i:LX/Hkf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, LX/Hjg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hjg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LX/HjU;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/Hk8;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/ads/internal/adapters/BannerAdapterListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LX/Hjg;->g:Landroid/content/Context;

    iput-object p2, p0, LX/Hjg;->d:LX/Hk8;

    iput-object p3, p0, LX/Hjg;->e:Ljava/util/Map;

    const-string v0, "definition"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hjy;

    const/4 v6, 0x0

    const-wide/16 v1, 0x0

    iput-wide v1, p0, LX/Hjg;->h:J

    iput-object v6, p0, LX/Hjg;->i:LX/Hkf;

    iget-object v1, p0, LX/Hjg;->e:Ljava/util/Map;

    const-string v2, "data"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    invoke-static {v1}, LX/Hjl;->a(Lorg/json/JSONObject;)LX/Hjl;

    move-result-object v3

    iget-object v1, p0, LX/Hjg;->g:Landroid/content/Context;

    invoke-static {v1, v3}, LX/Hkl;->a(Landroid/content/Context;LX/HjW;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Hjg;->d:LX/Hk8;

    invoke-static {}, LX/HkC;->b()V

    iget-object v2, v1, LX/Hk8;->b:LX/HkC;

    iget-object v2, v2, LX/HkC;->d:Landroid/os/Handler;

    iget-object v3, v1, LX/Hk8;->a:Ljava/lang/Runnable;

    invoke-static {v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/HjT;->b()V

    :cond_0
    iget-object v2, v1, LX/Hk8;->b:LX/HkC;

    invoke-static {v2}, LX/HkC;->d$redex0(LX/HkC;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v1, LX/Hl3;

    iget-object v2, p0, LX/Hjg;->g:Landroid/content/Context;

    new-instance v4, LX/Hje;

    invoke-direct {v4, p0, v3}, LX/Hje;-><init>(LX/Hjg;LX/Hjl;)V

    iget v5, v0, LX/Hjy;->b:I

    move v5, v5

    invoke-direct {v1, v2, v4, v5}, LX/Hl3;-><init>(Landroid/content/Context;LX/Hje;I)V

    iput-object v1, p0, LX/Hjg;->b:LX/Hl3;

    iget-object v1, p0, LX/Hjg;->b:LX/Hl3;

    iget v2, v0, LX/Hjy;->f:I

    move v2, v2

    iget v4, v0, LX/Hjy;->g:I

    move v4, v4

    iget-object v5, v1, LX/Hl3;->b:LX/HjZ;

    iput v2, v5, LX/HjZ;->a:I

    iget-object v5, v1, LX/Hl3;->b:LX/HjZ;

    iput v4, v5, LX/HjZ;->b:I

    new-instance v1, LX/Hjm;

    iget-object v2, p0, LX/Hjg;->g:Landroid/content/Context;

    iget-object v4, p0, LX/Hjg;->b:LX/Hl3;

    new-instance v5, LX/Hjf;

    invoke-direct {v5, p0}, LX/Hjf;-><init>(LX/Hjg;)V

    invoke-direct {v1, v2, v4, v5}, LX/Hjm;-><init>(Landroid/content/Context;LX/Hl2;LX/HjD;)V

    iput-object v1, p0, LX/Hjg;->c:LX/Hjm;

    iget-object v1, p0, LX/Hjg;->c:LX/Hjm;

    iput-object v3, v1, LX/Hjm;->d:LX/Hjl;

    iget-object v1, p0, LX/Hjg;->b:LX/Hl3;

    invoke-static {}, LX/Hkp;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v4, v3, LX/Hjl;->a:Ljava/lang/String;

    move-object v3, v4

    const-string v4, "text/html"

    const-string v5, "utf-8"

    invoke-virtual/range {v1 .. v6}, LX/Hl3;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, LX/Hjg;->d:LX/Hk8;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Hjg;->d:LX/Hk8;

    iget-object v2, p0, LX/Hjg;->b:LX/Hl3;

    invoke-static {}, LX/HkC;->b()V

    iget-object v3, v1, LX/Hk8;->b:LX/HkC;

    iget-object v3, v3, LX/HkC;->d:Landroid/os/Handler;

    iget-object v4, v1, LX/Hk8;->a:Ljava/lang/Runnable;

    invoke-static {v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iget-object v3, v1, LX/Hk8;->b:LX/HkC;

    iget-object v3, v3, LX/HkC;->j:LX/HjT;

    iget-object v4, v1, LX/Hk8;->b:LX/HkC;

    iput-object p0, v4, LX/HkC;->j:LX/HjT;

    iget-object v4, v1, LX/Hk8;->b:LX/HkC;

    iput-object v2, v4, LX/HkC;->k:Landroid/view/View;

    iget-object v4, v1, LX/Hk8;->b:LX/HkC;

    iget-boolean v4, v4, LX/HkC;->i:Z

    if-nez v4, :cond_3

    iget-object v3, v1, LX/Hk8;->b:LX/HkC;

    iget-object v3, v3, LX/HkC;->a:LX/HjO;

    invoke-virtual {v3}, LX/HjO;->c()V

    :goto_1
    goto :goto_0

    :cond_3
    if-eqz v3, :cond_4

    invoke-interface {v3}, LX/HjT;->b()V

    :cond_4
    iget-object v3, v1, LX/Hk8;->b:LX/HkC;

    invoke-static {v3}, LX/HkC;->f(LX/HkC;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LX/Hjg;->b:LX/Hl3;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hjg;->b:LX/Hl3;

    invoke-static {v0}, LX/Hkp;->a(Landroid/webkit/WebView;)V

    iget-object v0, p0, LX/Hjg;->b:LX/Hl3;

    invoke-virtual {v0}, LX/Hl3;->destroy()V

    iput-object v1, p0, LX/Hjg;->b:LX/Hl3;

    :cond_0
    iget-object v0, p0, LX/Hjg;->f:LX/Hjk;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Hjg;->f:LX/Hjk;

    invoke-virtual {v0}, LX/Hjk;->b()V

    iput-object v1, p0, LX/Hjg;->f:LX/Hjk;

    :cond_1
    return-void
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, LX/Hjg;->c:LX/Hjm;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Hjg;->c:LX/Hjm;

    invoke-virtual {v0}, LX/HjX;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LX/Hjg;->f:LX/Hjk;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "mil"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, LX/Hjg;->f:LX/Hjk;

    invoke-virtual {v1, v0}, LX/Hjj;->a(Ljava/util/Map;)V

    goto :goto_0
.end method
