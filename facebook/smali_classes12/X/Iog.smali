.class public LX/Iog;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Inb;


# instance fields
.field public final a:LX/0Uh;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/03V;

.field public final d:LX/6Oo;

.field public final e:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

.field public final f:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

.field public final g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field public h:LX/Io3;

.field public i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;>;"
        }
    .end annotation
.end field

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;Ljava/util/concurrent/Executor;LX/03V;LX/6Oo;Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612109
    iput-object p1, p0, LX/Iog;->a:LX/0Uh;

    .line 2612110
    iput-object p2, p0, LX/Iog;->b:Ljava/util/concurrent/Executor;

    .line 2612111
    iput-object p3, p0, LX/Iog;->c:LX/03V;

    .line 2612112
    iput-object p4, p0, LX/Iog;->d:LX/6Oo;

    .line 2612113
    iput-object p5, p0, LX/Iog;->e:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    .line 2612114
    iput-object p6, p0, LX/Iog;->f:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    .line 2612115
    iput-object p7, p0, LX/Iog;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2612116
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2612068
    iget-object v0, p0, LX/Iog;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2612069
    iget-object v0, p0, LX/Iog;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612070
    iput-object v2, p0, LX/Iog;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612071
    :cond_0
    iget-object v0, p0, LX/Iog;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2612072
    iget-object v0, p0, LX/Iog;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612073
    iput-object v2, p0, LX/Iog;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612074
    :cond_1
    iget-object v0, p0, LX/Iog;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2612075
    iget-object v0, p0, LX/Iog;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612076
    iput-object v2, p0, LX/Iog;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612077
    :cond_2
    iget-object v0, p0, LX/Iog;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2612078
    iget-object v0, p0, LX/Iog;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612079
    iput-object v2, p0, LX/Iog;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612080
    :cond_3
    iget-object v0, p0, LX/Iog;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2612081
    iget-object v0, p0, LX/Iog;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612082
    iput-object v2, p0, LX/Iog;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612083
    :cond_4
    return-void
.end method

.method public final a(LX/Io3;)V
    .locals 0

    .prologue
    .line 2612106
    iput-object p1, p0, LX/Iog;->h:LX/Io3;

    .line 2612107
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 2

    .prologue
    .line 2612084
    iput-object p2, p0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612085
    iget-object v0, p0, LX/Iog;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2612086
    :goto_0
    iget-object v0, p0, LX/Iog;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2612087
    :goto_1
    iget-object v0, p0, LX/Iog;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2612088
    :goto_2
    iget-object v0, p0, LX/Iog;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2612089
    :goto_3
    iget-object v0, p0, LX/Iog;->a:LX/0Uh;

    const/16 v1, 0x5a5

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2612090
    :cond_0
    :goto_4
    return-void

    .line 2612091
    :cond_1
    iget-object v0, p0, LX/Iog;->f:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Iog;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612092
    iget-object v0, p0, LX/Iog;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p1, LX/Iob;

    invoke-direct {p1, p0}, LX/Iob;-><init>(LX/Iog;)V

    iget-object p2, p0, LX/Iog;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2612093
    :cond_2
    iget-object v0, p0, LX/Iog;->e:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    invoke-virtual {v0}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Iog;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612094
    iget-object v0, p0, LX/Iog;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p1, LX/Ioc;

    invoke-direct {p1, p0}, LX/Ioc;-><init>(LX/Iog;)V

    iget-object p2, p0, LX/Iog;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2612095
    :cond_3
    iget-object v0, p0, LX/Iog;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    iget-object p1, p0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612096
    iget-object p2, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object p1, p2

    .line 2612097
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0rS;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Iog;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612098
    iget-object v0, p0, LX/Iog;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Iod;

    invoke-direct {v1, p0}, LX/Iod;-><init>(LX/Iog;)V

    iget-object p1, p0, LX/Iog;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    .line 2612099
    :cond_4
    iget-object v0, p0, LX/Iog;->d:LX/6Oo;

    iget-object v1, p0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612100
    iget-object p1, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v1, p1

    .line 2612101
    sget-object p1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v0, v1, p1}, LX/6Oo;->b(Lcom/facebook/user/model/UserKey;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Iog;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612102
    iget-object v0, p0, LX/Iog;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Ioe;

    invoke-direct {v1, p0}, LX/Ioe;-><init>(LX/Iog;)V

    iget-object p1, p0, LX/Iog;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_3

    .line 2612103
    :cond_5
    iget-object v0, p0, LX/Iog;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2612104
    iget-object v0, p0, LX/Iog;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Iog;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612105
    iget-object v0, p0, LX/Iog;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Iof;

    invoke-direct {v1, p0}, LX/Iof;-><init>(LX/Iog;)V

    iget-object p1, p0, LX/Iog;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_4
.end method
