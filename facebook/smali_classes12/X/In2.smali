.class public LX/In2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2JS;

.field private final b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final c:LX/2JT;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2JS;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;LX/2JT;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsRequestEligible;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2JS;",
            "Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;",
            "LX/2JT;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610221
    iput-object p1, p0, LX/In2;->a:LX/2JS;

    .line 2610222
    iput-object p2, p0, LX/In2;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2610223
    iput-object p3, p0, LX/In2;->c:LX/2JT;

    .line 2610224
    iput-object p4, p0, LX/In2;->d:LX/0Or;

    .line 2610225
    return-void
.end method
