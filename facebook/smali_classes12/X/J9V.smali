.class public final enum LX/J9V;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J9V;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J9V;

.field public static final enum COLLECTION_HEADER:LX/J9V;

.field public static final enum CURATE_SEARCH_RESULT:LX/J9V;

.field public static final enum LOADING_INDICATOR:LX/J9V;

.field public static final NUM_VIEW_TYPES:I

.field public static final enum SECTION_HEADER:LX/J9V;

.field public static final enum SUB_ADAPTER_ITEM_BOTTOM:LX/J9V;

.field public static final enum SUB_ADAPTER_ITEM_MIDDLE:LX/J9V;

.field public static final enum SUGGESTIONS_BANNER:LX/J9V;

.field public static final enum SUGGESTIONS_TYPEAHEAD:LX/J9V;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2652830
    new-instance v0, LX/J9V;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v3}, LX/J9V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9V;->SECTION_HEADER:LX/J9V;

    .line 2652831
    new-instance v0, LX/J9V;

    const-string v1, "COLLECTION_HEADER"

    invoke-direct {v0, v1, v4}, LX/J9V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9V;->COLLECTION_HEADER:LX/J9V;

    .line 2652832
    new-instance v0, LX/J9V;

    const-string v1, "SUB_ADAPTER_ITEM_MIDDLE"

    invoke-direct {v0, v1, v5}, LX/J9V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9V;->SUB_ADAPTER_ITEM_MIDDLE:LX/J9V;

    .line 2652833
    new-instance v0, LX/J9V;

    const-string v1, "SUB_ADAPTER_ITEM_BOTTOM"

    invoke-direct {v0, v1, v6}, LX/J9V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9V;->SUB_ADAPTER_ITEM_BOTTOM:LX/J9V;

    .line 2652834
    new-instance v0, LX/J9V;

    const-string v1, "SUGGESTIONS_BANNER"

    invoke-direct {v0, v1, v7}, LX/J9V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9V;->SUGGESTIONS_BANNER:LX/J9V;

    .line 2652835
    new-instance v0, LX/J9V;

    const-string v1, "SUGGESTIONS_TYPEAHEAD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/J9V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9V;->SUGGESTIONS_TYPEAHEAD:LX/J9V;

    .line 2652836
    new-instance v0, LX/J9V;

    const-string v1, "CURATE_SEARCH_RESULT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/J9V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9V;->CURATE_SEARCH_RESULT:LX/J9V;

    .line 2652837
    new-instance v0, LX/J9V;

    const-string v1, "LOADING_INDICATOR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/J9V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9V;->LOADING_INDICATOR:LX/J9V;

    .line 2652838
    const/16 v0, 0x8

    new-array v0, v0, [LX/J9V;

    sget-object v1, LX/J9V;->SECTION_HEADER:LX/J9V;

    aput-object v1, v0, v3

    sget-object v1, LX/J9V;->COLLECTION_HEADER:LX/J9V;

    aput-object v1, v0, v4

    sget-object v1, LX/J9V;->SUB_ADAPTER_ITEM_MIDDLE:LX/J9V;

    aput-object v1, v0, v5

    sget-object v1, LX/J9V;->SUB_ADAPTER_ITEM_BOTTOM:LX/J9V;

    aput-object v1, v0, v6

    sget-object v1, LX/J9V;->SUGGESTIONS_BANNER:LX/J9V;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/J9V;->SUGGESTIONS_TYPEAHEAD:LX/J9V;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/J9V;->CURATE_SEARCH_RESULT:LX/J9V;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/J9V;->LOADING_INDICATOR:LX/J9V;

    aput-object v2, v0, v1

    sput-object v0, LX/J9V;->$VALUES:[LX/J9V;

    .line 2652839
    invoke-static {}, LX/J9V;->values()[LX/J9V;

    move-result-object v0

    array-length v0, v0

    sput v0, LX/J9V;->NUM_VIEW_TYPES:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2652842
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J9V;
    .locals 1

    .prologue
    .line 2652841
    const-class v0, LX/J9V;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J9V;

    return-object v0
.end method

.method public static values()[LX/J9V;
    .locals 1

    .prologue
    .line 2652840
    sget-object v0, LX/J9V;->$VALUES:[LX/J9V;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J9V;

    return-object v0
.end method
