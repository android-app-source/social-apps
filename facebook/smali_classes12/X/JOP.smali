.class public LX/JOP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1vb;

.field public final b:LX/1e4;

.field public final c:LX/1vg;


# direct methods
.method public constructor <init>(LX/1vb;LX/1e4;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2687458
    iput-object p1, p0, LX/JOP;->a:LX/1vb;

    .line 2687459
    iput-object p2, p0, LX/JOP;->b:LX/1e4;

    .line 2687460
    iput-object p3, p0, LX/JOP;->c:LX/1vg;

    .line 2687461
    return-void
.end method

.method public static a(LX/0QB;)LX/JOP;
    .locals 6

    .prologue
    .line 2687462
    const-class v1, LX/JOP;

    monitor-enter v1

    .line 2687463
    :try_start_0
    sget-object v0, LX/JOP;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687464
    sput-object v2, LX/JOP;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687465
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687466
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687467
    new-instance p0, LX/JOP;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v3

    check-cast v3, LX/1vb;

    invoke-static {v0}, LX/1e4;->a(LX/0QB;)LX/1e4;

    move-result-object v4

    check-cast v4, LX/1e4;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-direct {p0, v3, v4, v5}, LX/JOP;-><init>(LX/1vb;LX/1e4;LX/1vg;)V

    .line 2687468
    move-object v0, p0

    .line 2687469
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687470
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687471
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687472
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
