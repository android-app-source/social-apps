.class public LX/Ilf;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:LX/6lN;

.field public final d:LX/Duh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2607968
    const-class v0, LX/Ilf;

    sput-object v0, LX/Ilf;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/6lN;LX/Duh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607970
    iput-object p1, p0, LX/Ilf;->b:Landroid/content/res/Resources;

    .line 2607971
    iput-object p2, p0, LX/Ilf;->c:LX/6lN;

    .line 2607972
    iput-object p3, p0, LX/Ilf;->d:LX/Duh;

    .line 2607973
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/PaymentTransaction;Z)LX/DtN;
    .locals 1

    .prologue
    .line 2607974
    if-eqz p1, :cond_0

    .line 2607975
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    move-object v0, v0

    .line 2607976
    :goto_0
    return-object v0

    .line 2607977
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    move-object v0, v0

    .line 2607978
    goto :goto_0
.end method

.method public static a(LX/Ilf;Lcom/facebook/payments/p2p/model/PaymentTransaction;ZLX/Ilc;)LX/Ila;
    .locals 8

    .prologue
    .line 2607979
    invoke-static {}, LX/Ila;->newBuilder()LX/Ilb;

    move-result-object v1

    .line 2607980
    sget-object v0, LX/Ile;->b:[I

    invoke-virtual {p3}, LX/Ilc;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2607981
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 2607982
    iput-object v0, v1, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2607983
    const-string v0, ""

    .line 2607984
    iput-object v0, v1, LX/Ilb;->c:Ljava/lang/String;

    .line 2607985
    :goto_0
    iput-object p3, v1, LX/Ilb;->b:LX/Ilc;

    .line 2607986
    invoke-virtual {v1}, LX/Ilb;->d()LX/Ila;

    move-result-object v0

    return-object v0

    .line 2607987
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    .line 2607988
    iput-object v0, v1, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2607989
    move-object v0, v1

    .line 2607990
    iget-object v2, p0, LX/Ilf;->b:Landroid/content/res/Resources;

    const v3, 0x7f082c5a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2607991
    iput-object v2, v0, LX/Ilb;->c:Ljava/lang/String;

    .line 2607992
    goto :goto_0

    .line 2607993
    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    .line 2607994
    iput-object v0, v1, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2607995
    move-object v0, v1

    .line 2607996
    iget-object v2, p0, LX/Ilf;->b:Landroid/content/res/Resources;

    const v3, 0x7f082c5b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2607997
    iput-object v2, v0, LX/Ilb;->c:Ljava/lang/String;

    .line 2607998
    goto :goto_0

    .line 2607999
    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 2608000
    iput-object v0, v1, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2608001
    iget-object v0, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2608002
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 2608003
    if-eqz p2, :cond_0

    const v0, 0x7f082c41

    .line 2608004
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 2608005
    iget-object v4, p0, LX/Ilf;->b:Landroid/content/res/Resources;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, LX/Ilf;->c:LX/6lN;

    invoke-virtual {v7, v2, v3}, LX/6lN;->a(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v4, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2608006
    iput-object v0, v1, LX/Ilb;->c:Ljava/lang/String;

    .line 2608007
    goto :goto_0

    .line 2608008
    :cond_0
    const v0, 0x7f082c42

    goto :goto_1

    .line 2608009
    :cond_1
    const-string v0, ""

    .line 2608010
    iput-object v0, v1, LX/Ilb;->c:Ljava/lang/String;

    .line 2608011
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
