.class public final LX/ICi;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V
    .locals 0

    .prologue
    .line 2549649
    iput-object p1, p0, LX/ICi;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2549650
    check-cast p1, LX/2f2;

    .line 2549651
    if-nez p1, :cond_1

    .line 2549652
    :cond_0
    :goto_0
    return-void

    .line 2549653
    :cond_1
    iget-object v0, p0, LX/ICi;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->r:LX/ICw;

    iget-wide v2, p1, LX/2f2;->a:J

    .line 2549654
    iget-object v1, v0, LX/ICw;->c:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ICk;

    move-object v0, v1

    .line 2549655
    if-eqz v0, :cond_0

    .line 2549656
    invoke-virtual {v0}, LX/ICk;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v2, :cond_0

    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, LX/ICk;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 2549657
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2549658
    iput-object v1, v0, LX/ICk;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2549659
    iget-object v1, p0, LX/ICi;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v1, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->r:LX/ICw;

    .line 2549660
    iget-object v4, v1, LX/ICw;->b:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2549661
    iget-object v4, v1, LX/ICw;->c:Ljava/util/Map;

    invoke-virtual {v0}, LX/ICk;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2549662
    const v4, 0x7eb470d9

    invoke-static {v1, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2549663
    goto :goto_0
.end method
