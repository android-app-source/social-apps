.class public LX/IV1;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/3ma;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3mb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DN3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3my;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/IUy;

.field public g:Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2581220
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2581221
    const/4 v3, 0x1

    .line 2581222
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/IV1;

    invoke-static {v0}, LX/3ma;->a(LX/0QB;)LX/3ma;

    move-result-object v4

    check-cast v4, LX/3ma;

    invoke-static {v0}, LX/3mb;->a(LX/0QB;)LX/3mb;

    move-result-object v5

    check-cast v5, LX/3mb;

    invoke-static {v0}, LX/DN3;->a(LX/0QB;)LX/DN3;

    move-result-object v6

    check-cast v6, LX/DN3;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    invoke-static {v0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v0

    check-cast v0, LX/3my;

    iput-object v4, v2, LX/IV1;->a:LX/3ma;

    iput-object v5, v2, LX/IV1;->b:LX/3mb;

    iput-object v6, v2, LX/IV1;->c:LX/DN3;

    iput-object p1, v2, LX/IV1;->d:LX/0Uh;

    iput-object v0, v2, LX/IV1;->e:LX/3my;

    .line 2581223
    invoke-virtual {p0}, LX/IV1;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030832

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2581224
    invoke-virtual {p0, v3}, LX/IV1;->setOrientation(I)V

    .line 2581225
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2581226
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 2581227
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/IV1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a009a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/IV1;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2581228
    :goto_0
    new-instance v0, LX/IUy;

    invoke-direct {v0, p0}, LX/IUy;-><init>(LX/IV1;)V

    iput-object v0, p0, LX/IV1;->f:LX/IUy;

    .line 2581229
    const v0, 0x7f0d156a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

    iput-object v0, p0, LX/IV1;->g:Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

    .line 2581230
    iget-object v0, p0, LX/IV1;->g:Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->setType(I)V

    .line 2581231
    iget-object v0, p0, LX/IV1;->g:Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;

    iget-object v1, p0, LX/IV1;->f:LX/IUy;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2581232
    return-void

    .line 2581233
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/IV1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a009a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/IV1;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;)I
    .locals 1

    .prologue
    .line 2581234
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2581235
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2581236
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
