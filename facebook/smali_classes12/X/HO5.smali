.class public final LX/HO5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459538
    iput-object p1, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x111c5a17

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2459539
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2459540
    iget-object v0, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->b:LX/CY3;

    iget-object v1, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->g:Ljava/lang/String;

    const-string v3, "cta_select_list"

    invoke-virtual {v0, v1, v3}, LX/CY3;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459541
    iget-object v0, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->c:LX/0if;

    iget-object v0, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->h:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0ig;->ak:LX/0ih;

    :goto_0
    const-string v3, "select_from_cta_list"

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459542
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2459543
    iget-object v0, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->d:LX/Gze;

    iget-object v1, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v7}, LX/Gze;->a(Ljava/lang/String;Z)V

    .line 2459544
    iget-object v0, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v3, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->g:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->a:LX/CYR;

    invoke-virtual {v5, v2}, LX/CYR;->g(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v7, v4, v2}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/CYR;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2459545
    :goto_1
    const v0, -0x2570f66d

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2459546
    :cond_0
    sget-object v0, LX/0ig;->aj:LX/0ih;

    goto :goto_0

    .line 2459547
    :cond_1
    iget-object v0, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v7

    iget-object v8, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v0, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    iget-object v1, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    iget-object v3, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->g:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, LX/HO5;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->i:LX/CYE;

    invoke-static/range {v0 .. v5}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Ljava/lang/String;Ljava/lang/String;LX/CYE;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    move-result-object v0

    const-string v1, "select_to_configure_cta_tag"

    invoke-static {v7, v8, v0, v1}, LX/CYR;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;)V

    goto :goto_1
.end method
