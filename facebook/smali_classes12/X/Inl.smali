.class public final LX/Inl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V
    .locals 0

    .prologue
    .line 2610825
    iput-object p1, p0, LX/Inl;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 3

    .prologue
    .line 2610806
    invoke-static {}, LX/Inp;->values()[LX/Inp;

    move-result-object v0

    aget-object v0, v0, p1

    sget-object v1, LX/Inp;->TAB_ORION_REQUEST:LX/Inp;

    if-ne v0, v1, :cond_0

    .line 2610807
    iget-object v0, p0, LX/Inl;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->w:LX/0Zb;

    const-string v1, "p2p_initiate_request"

    const-string v2, "p2p_request"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2610808
    iget-object v2, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v1, v2

    .line 2610809
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2610810
    :cond_0
    iget-object v0, p0, LX/Inl;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->K:LX/Ino;

    invoke-virtual {v0, p1}, LX/Ino;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2610811
    if-nez v0, :cond_2

    .line 2610812
    :cond_1
    :goto_0
    return-void

    .line 2610813
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->k()V

    .line 2610814
    iget-object v1, p0, LX/Inl;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    iget-object v1, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->K:LX/Ino;

    add-int/lit8 v2, p1, 0x1

    rem-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, LX/Ino;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2610815
    if-eqz v1, :cond_1

    .line 2610816
    invoke-virtual {v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->c()Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-result-object v2

    .line 2610817
    iget-object p0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {p0, v2}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)V

    .line 2610818
    invoke-virtual {v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->d()Ljava/lang/String;

    move-result-object v2

    .line 2610819
    iget-object p0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {p0, v2}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Ljava/lang/String;)V

    .line 2610820
    iget-object v2, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2610821
    iget-object v1, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-object v2, v1

    .line 2610822
    move-object v1, v2

    .line 2610823
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v2, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)V

    .line 2610824
    goto :goto_0
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2610805
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2610804
    return-void
.end method
