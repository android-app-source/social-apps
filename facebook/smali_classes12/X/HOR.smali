.class public LX/HOR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HOQ;


# instance fields
.field public final a:LX/HOP;

.field public final b:Lcom/facebook/widget/SwitchCompat;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/SwitchCompat;)V
    .locals 2

    .prologue
    .line 2460059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2460060
    iput-object p1, p0, LX/HOR;->b:Lcom/facebook/widget/SwitchCompat;

    .line 2460061
    new-instance v0, LX/HOP;

    invoke-direct {v0}, LX/HOP;-><init>()V

    iput-object v0, p0, LX/HOR;->a:LX/HOP;

    .line 2460062
    iget-object v0, p0, LX/HOR;->b:Lcom/facebook/widget/SwitchCompat;

    iget-object v1, p0, LX/HOR;->a:LX/HOP;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2460063
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2460058
    const/4 v0, 0x0

    return v0
.end method

.method public final b()LX/CY5;
    .locals 1

    .prologue
    .line 2460057
    sget-object v0, LX/CY5;->NONE:LX/CY5;

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2460056
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2460055
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 2460052
    return-void
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2460054
    iget-object v0, p0, LX/HOR;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2460053
    iget-object v0, p0, LX/HOR;->b:Lcom/facebook/widget/SwitchCompat;

    return-object v0
.end method
