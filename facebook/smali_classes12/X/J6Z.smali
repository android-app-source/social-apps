.class public final LX/J6Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 0

    .prologue
    .line 2649772
    iput-object p1, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2649773
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2649774
    iget-object v0, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v0}, LX/J3t;->h()V

    .line 2649775
    iget-object v0, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v1, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649776
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649777
    const-string v2, "step_fail"

    iget-object v3, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v5, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v5, v5, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v6, v6, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649778
    iget-object v0, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v1, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649779
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649780
    const-string v2, "checkup_cancel"

    iget-object v3, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v5, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v5, v5, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v6, v6, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649781
    iget-object v0, p0, LX/J6Z;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->finish()V

    .line 2649782
    return-void
.end method
