.class public LX/HuA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j0;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j8;",
        "DerivedData::",
        "LX/2rg;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/ipc/composer/model/ComposerLocationInfo$SetsLocationInfo",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0wM;

.field private final e:Landroid/content/res/Resources;

.field public final f:LX/0gd;

.field private final g:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2517189
    const-class v0, LX/HuA;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/HuA;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0gd;LX/0wM;Landroid/content/res/Resources;LX/0zw;LX/0il;)V
    .locals 2
    .param p4    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gd;",
            "LX/0wM;",
            "Landroid/content/res/Resources;",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517181
    new-instance v0, LX/Hu8;

    invoke-direct {v0, p0}, LX/Hu8;-><init>(LX/HuA;)V

    iput-object v0, p0, LX/HuA;->g:Landroid/view/View$OnClickListener;

    .line 2517182
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zw;

    iput-object v0, p0, LX/HuA;->c:LX/0zw;

    .line 2517183
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HuA;->b:Ljava/lang/ref/WeakReference;

    .line 2517184
    iput-object p1, p0, LX/HuA;->f:LX/0gd;

    .line 2517185
    iput-object p2, p0, LX/HuA;->d:LX/0wM;

    .line 2517186
    iput-object p3, p0, LX/HuA;->e:Landroid/content/res/Resources;

    .line 2517187
    invoke-direct {p0}, LX/HuA;->c()V

    .line 2517188
    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    const v6, -0x6e685d

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 2517190
    iget-object v0, p0, LX/HuA;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 2517191
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rg;

    invoke-interface {v1}, LX/2rg;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j0;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->f()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j0;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->f()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 2517192
    :cond_0
    iget-object v0, p0, LX/HuA;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2517193
    :goto_0
    return-void

    .line 2517194
    :cond_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j0;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->f()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2517195
    iget-object v1, p0, LX/HuA;->c:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2517196
    iget-object v1, p0, LX/HuA;->c:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    aget-object v3, v3, v5

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2517197
    iget-object v1, p0, LX/HuA;->d:LX/0wM;

    iget-object v3, p0, LX/HuA;->e:Landroid/content/res/Resources;

    const v4, 0x7f0202fe

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3, v6}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2517198
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j0;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2517199
    iget-object v0, p0, LX/HuA;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/HuA;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2517200
    iget-object v0, p0, LX/HuA;->d:LX/0wM;

    iget-object v1, p0, LX/HuA;->e:Landroid/content/res/Resources;

    const v4, 0x7f0202f6

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2517201
    iget-object v0, p0, LX/HuA;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 2517202
    :goto_1
    iget-object v0, p0, LX/HuA;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, v3, v2, v1, v2}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2517203
    :cond_2
    iget-object v0, p0, LX/HuA;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 2517177
    sget-object v0, LX/Hu9;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517178
    :goto_0
    return-void

    .line 2517179
    :pswitch_0
    invoke-direct {p0}, LX/HuA;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2517175
    invoke-direct {p0}, LX/HuA;->c()V

    .line 2517176
    return-void
.end method
