.class public final LX/IGZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V
    .locals 0

    .prologue
    .line 2556089
    iput-object p1, p0, LX/IGZ;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x283046c5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2556090
    iget-object v1, p0, LX/IGZ;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->z:LX/IGP;

    .line 2556091
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "friends_nearby_dashboard_ping_cancel"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "background_location"

    .line 2556092
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2556093
    move-object v2, v2

    .line 2556094
    iget-object v3, v1, LX/IGP;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2556095
    iget-object v1, p0, LX/IGZ;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2556096
    iget-object v1, p0, LX/IGZ;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    .line 2556097
    new-instance v2, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    sget-object v3, LX/IGG;->STOP:LX/IGG;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p0

    invoke-direct {v2, v3, p0}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;-><init>(LX/IGG;LX/0am;)V

    move-object v2, v2

    .line 2556098
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;LX/0am;)V

    .line 2556099
    const v1, 0x37ff82db

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
