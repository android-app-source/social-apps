.class public final LX/Ioc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/p2p/model/PaymentCard;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Iog;


# direct methods
.method public constructor <init>(LX/Iog;)V
    .locals 0

    .prologue
    .line 2612044
    iput-object p1, p0, LX/Ioc;->a:LX/Iog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2612027
    iget-object v0, p0, LX/Ioc;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->c:LX/03V;

    const-string v1, "OrionMessengerPayLoader"

    const-string v2, "Failed to fetch PaymentCards for sending money."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612028
    iget-object v0, p0, LX/Ioc;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->h:LX/Io3;

    invoke-virtual {v0}, LX/Io3;->a()V

    .line 2612029
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2612030
    check-cast p1, LX/0Px;

    .line 2612031
    iget-object v0, p0, LX/Ioc;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612032
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->e:LX/0Px;

    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2612033
    :goto_0
    iget-object v0, p0, LX/Ioc;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612034
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    move-object v0, v1

    .line 2612035
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ioc;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612036
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    move-object v0, v1

    .line 2612037
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2612038
    :cond_0
    invoke-static {p1}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2612039
    invoke-static {v0}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->d(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2612040
    iget-object v1, p0, LX/Ioc;->a:LX/Iog;

    iget-object v1, v1, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(LX/0am;)V

    .line 2612041
    :cond_1
    return-void

    .line 2612042
    :cond_2
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->e:LX/0Px;

    .line 2612043
    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method
