.class public LX/IuQ;
.super LX/IuO;
.source ""


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public final n:LX/0TD;

.field private final o:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/2cD;

.field public final r:LX/2PE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2626773
    const-class v0, LX/IuQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IuQ;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/3Ec;LX/FDp;LX/2Ox;LX/2Oi;LX/2P4;LX/IuT;LX/DoY;LX/2NB;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/7V0;LX/2Ow;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2PJ;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;LX/0Or;LX/IuN;LX/2cD;LX/2PE;)V
    .locals 20
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            "LX/FDp;",
            "LX/2Ox;",
            "LX/2Oi;",
            "LX/2P4;",
            "LX/IuT;",
            "LX/DoY;",
            "LX/2NB;",
            "LX/2P0;",
            "Lcom/facebook/messaging/tincan/messenger/interfaces/PreKeyManager;",
            "LX/7V0;",
            "LX/2Ow;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/outbound/Sender;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2PJ;",
            "Lcom/facebook/messaging/tincan/messenger/interfaces/MessengerErrorGenerator;",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/IuN;",
            "LX/2cD;",
            "LX/2PE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2626765
    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p3

    move-object/from16 v8, p7

    move-object/from16 v9, p12

    move-object/from16 v10, p13

    move-object/from16 v11, p20

    move-object/from16 v12, p9

    move-object/from16 v13, p8

    move-object/from16 v14, p10

    move-object/from16 v15, p11

    move-object/from16 v16, p14

    move-object/from16 v17, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    invoke-direct/range {v2 .. v19}, LX/IuO;-><init>(LX/3Ec;LX/2Ox;LX/2Oi;LX/2P4;LX/FDp;LX/IuT;LX/7V0;LX/2Ow;LX/IuN;LX/2NB;LX/DoY;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2PJ;)V

    .line 2626766
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuQ;->n:LX/0TD;

    .line 2626767
    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuQ;->o:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    .line 2626768
    move-object/from16 v0, p19

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuQ;->p:LX/0Or;

    .line 2626769
    move-object/from16 v0, p21

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuQ;->q:LX/2cD;

    .line 2626770
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuQ;->r:LX/2PE;

    .line 2626771
    move-object/from16 v0, p0

    iget-object v2, v0, LX/IuQ;->q:LX/2cD;

    new-instance v3, LX/IuP;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/IuP;-><init>(LX/IuQ;)V

    invoke-virtual {v2, v3}, LX/2PI;->a(Ljava/lang/Object;)V

    .line 2626772
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
    .locals 3

    .prologue
    .line 2626760
    iget-object v0, p0, LX/IuO;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2626761
    :goto_0
    return-object p1

    .line 2626762
    :cond_0
    iget-object v0, p0, LX/IuO;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2626763
    iget-object v0, p0, LX/IuO;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_0

    .line 2626764
    :cond_1
    iget-object v1, p0, LX/IuQ;->o:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    iget-object v0, p0, LX/IuQ;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0806c5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 4

    .prologue
    .line 2626754
    iget-object v0, p0, LX/IuO;->a:LX/3Ec;

    .line 2626755
    iget-object v1, v0, LX/3Ec;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2626756
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2626757
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 2626758
    invoke-static {p1, p2, v1, v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    move-object v0, v1

    .line 2626759
    return-object v0
.end method

.method public final a(JLcom/facebook/user/model/User;Ljava/lang/Integer;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 3
    .param p4    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2626727
    invoke-super {p0, p1, p2, p3, p4}, LX/IuO;->a(JLcom/facebook/user/model/User;Ljava/lang/Integer;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2626728
    iget-object v1, p0, LX/IuQ;->q:LX/2cD;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2, p1, p2}, LX/2cD;->a([BJ)Z

    .line 2626729
    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)V
    .locals 9

    .prologue
    .line 2626749
    :try_start_0
    iget-object v0, p0, LX/IuO;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p0, LX/IuO;->k:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/IuO;->l:LX/2PJ;

    invoke-virtual {v4}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p2, LX/IuS;->a:[B

    iget-object v6, p2, LX/IuS;->b:[B

    iget-object v7, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    const-string v8, "UTF-8"

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLjava/lang/String;[B[B[BZ)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2626750
    :goto_0
    return-void

    .line 2626751
    :catch_0
    move-exception v0

    .line 2626752
    const-class v1, LX/IuQ;

    const-string v2, "Could not encode message ID into packet ID"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2626753
    iget-object v0, p0, LX/IuO;->g:LX/IuN;

    invoke-virtual {v0, p1}, LX/IuN;->b(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;[B)V
    .locals 11

    .prologue
    .line 2626730
    iget-object v0, p0, LX/IuQ;->r:LX/2PE;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2PE;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 2626731
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2626732
    sget-object v0, LX/IuQ;->m:Ljava/lang/String;

    const-string v1, "Secret Conversation Recipient list for sender key is empty"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2626733
    :cond_0
    return-void

    .line 2626734
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2626735
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpM;

    .line 2626736
    iget-object v1, v0, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2626737
    if-nez v1, :cond_2

    .line 2626738
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 2626739
    iget-object v5, v0, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-interface {v3, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626740
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2626741
    :cond_3
    new-instance v7, LX/Dpl;

    iget-wide v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v7, v0, p2, v3}, LX/Dpl;-><init>(Ljava/lang/Long;[BLjava/util/Map;)V

    .line 2626742
    const/4 v0, 0x0

    .line 2626743
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v9, v0

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/DpM;

    .line 2626744
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2626745
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    .line 2626746
    iget-object v0, p0, LX/IuO;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v1, p0, LX/IuO;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    iget-object v3, p0, LX/IuO;->l:LX/2PJ;

    invoke-virtual {v3}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v6, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, v6, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(JLjava/lang/String;JLjava/lang/String;LX/Dpl;[B)V

    .line 2626747
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    .line 2626748
    goto :goto_1
.end method
