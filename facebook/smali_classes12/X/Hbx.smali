.class public final LX/Hbx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EeL;


# instance fields
.field public final synthetic a:LX/Hby;


# direct methods
.method public constructor <init>(LX/Hby;)V
    .locals 0

    .prologue
    .line 2486596
    iput-object p1, p0, LX/Hbx;->a:LX/Hby;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1wl;)V
    .locals 8

    .prologue
    .line 2486597
    iget-object v0, p0, LX/Hbx;->a:LX/Hby;

    .line 2486598
    invoke-virtual {p1}, LX/1wl;->h()LX/1wh;

    move-result-object v1

    .line 2486599
    const/4 v2, 0x0

    .line 2486600
    iget-object v3, v0, LX/Hby;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/3fU;->k:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2486601
    if-nez v4, :cond_3

    .line 2486602
    :cond_0
    :goto_0
    move-object v2, v2

    .line 2486603
    if-nez v2, :cond_1

    .line 2486604
    :goto_1
    return-void

    .line 2486605
    :cond_1
    const/4 v3, 0x0

    .line 2486606
    iget-object v4, v0, LX/Hby;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/3fU;->l:LX/0Tn;

    invoke-interface {v4, v5, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2486607
    if-nez v4, :cond_4

    .line 2486608
    :goto_2
    move-object v3, v3

    .line 2486609
    const-string v4, "appupdate_install_successful"

    .line 2486610
    invoke-virtual {v2}, Lcom/facebook/appupdate/ReleaseInfo;->a()Lorg/json/JSONObject;

    move-result-object v5

    .line 2486611
    if-eqz v3, :cond_2

    .line 2486612
    const-string v6, "diff_algorithm"

    invoke-virtual {v3}, LX/Eeh;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2486613
    :cond_2
    move-object v5, v5

    .line 2486614
    invoke-virtual {v1, v4, v5}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2486615
    const-string v4, "appupdate_install_successful"

    const-string v5, "task_success"

    invoke-virtual {v1, v4, v2, v3, v5}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 2486616
    invoke-virtual {v1}, LX/1wh;->c()V

    .line 2486617
    iget-object v1, v0, LX/Hby;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3fU;->k:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/3fU;->l:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_1

    .line 2486618
    :cond_3
    :try_start_0
    new-instance v3, Lcom/facebook/appupdate/ReleaseInfo;

    invoke-direct {v3, v4}, Lcom/facebook/appupdate/ReleaseInfo;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2486619
    invoke-virtual {p1}, LX/1wl;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, LX/1wl;->g()I

    move-result v4

    iget v5, v3, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    if-ne v4, v5, :cond_0

    move-object v2, v3

    .line 2486620
    goto :goto_0

    .line 2486621
    :catch_0
    move-exception v3

    .line 2486622
    iget-object v5, v0, LX/Hby;->c:LX/03V;

    iget-object v6, v0, LX/Hby;->b:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string p0, "Could not parse ReleaseInfo from: "

    invoke-direct {v7, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2486623
    :cond_4
    :try_start_1
    invoke-static {v4}, LX/Eeh;->valueOf(Ljava/lang/String;)LX/Eeh;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_2

    .line 2486624
    :catch_1
    move-exception v5

    .line 2486625
    iget-object v6, v0, LX/Hby;->c:LX/03V;

    iget-object v7, v0, LX/Hby;->b:Ljava/lang/String;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Could not parse diff algorithm from: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2
.end method
