.class public final enum LX/Ik1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ik1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ik1;

.field public static final enum NEW:LX/Ik1;

.field public static final enum VERIFY:LX/Ik1;


# instance fields
.field private final type:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2606329
    new-instance v0, LX/Ik1;

    const-string v1, "NEW"

    const-string v2, "new"

    invoke-direct {v0, v1, v3, v2}, LX/Ik1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ik1;->NEW:LX/Ik1;

    .line 2606330
    new-instance v0, LX/Ik1;

    const-string v1, "VERIFY"

    const-string v2, "verify"

    invoke-direct {v0, v1, v4, v2}, LX/Ik1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ik1;->VERIFY:LX/Ik1;

    .line 2606331
    const/4 v0, 0x2

    new-array v0, v0, [LX/Ik1;

    sget-object v1, LX/Ik1;->NEW:LX/Ik1;

    aput-object v1, v0, v3

    sget-object v1, LX/Ik1;->VERIFY:LX/Ik1;

    aput-object v1, v0, v4

    sput-object v0, LX/Ik1;->$VALUES:[LX/Ik1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2606326
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2606327
    iput-object p3, p0, LX/Ik1;->type:Ljava/lang/String;

    .line 2606328
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Ik1;
    .locals 5

    .prologue
    .line 2606321
    invoke-static {}, LX/Ik1;->values()[LX/Ik1;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2606322
    iget-object v4, v0, LX/Ik1;->type:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2606323
    :goto_1
    return-object v0

    .line 2606324
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2606325
    :cond_1
    sget-object v0, LX/Ik1;->VERIFY:LX/Ik1;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ik1;
    .locals 1

    .prologue
    .line 2606319
    const-class v0, LX/Ik1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ik1;

    return-object v0
.end method

.method public static values()[LX/Ik1;
    .locals 1

    .prologue
    .line 2606320
    sget-object v0, LX/Ik1;->$VALUES:[LX/Ik1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ik1;

    return-object v0
.end method
