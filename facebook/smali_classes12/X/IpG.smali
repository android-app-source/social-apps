.class public final LX/IpG;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

.field public final synthetic b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V
    .locals 0

    .prologue
    .line 2612615
    iput-object p1, p0, LX/IpG;->b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;

    iput-object p2, p0, LX/IpG;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 2612616
    iget-object v0, p0, LX/IpG;->b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->g:LX/IpF;

    const-string v1, "p2p_send_fail"

    iget-object v2, p0, LX/IpG;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-virtual {v0, v1, v2}, LX/IpF;->a(Ljava/lang/String;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2612617
    iget-object v0, p0, LX/IpG;->b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;

    .line 2612618
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->k:LX/Io2;

    invoke-virtual {v1}, LX/Io2;->a()V

    .line 2612619
    const-string v1, "OrionRequestAckMessengerPaySender"

    const-string v2, "Failed to pay request"

    invoke-static {v1, v2, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2612620
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->e:LX/03V;

    const-string v2, "OrionRequestAckMessengerPaySender"

    const-string v3, "Attempted to pay a request, but received a response with an error"

    invoke-virtual {v1, v2, v3, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2612621
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 2612622
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v1, v2, :cond_0

    .line 2612623
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->f:Landroid/content/Context;

    invoke-static {v1, p1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2612624
    :goto_0
    return-void

    .line 2612625
    :cond_0
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2612626
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2612627
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2612628
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->f:Landroid/content/Context;

    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->f:Landroid/content/Context;

    const v4, 0x7f082d24

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->f:Landroid/content/Context;

    const p0, 0x7f080016

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance p0, LX/IpH;

    invoke-direct {p0, v0}, LX/IpH;-><init>(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;)V

    invoke-static {v2, v3, v1, v4, p0}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2612629
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;

    .line 2612630
    iget-object v0, p0, LX/IpG;->b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->g:LX/IpF;

    const-string v1, "p2p_send_success"

    iget-object v2, p0, LX/IpG;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-virtual {v0, v1, v2}, LX/IpF;->a(Ljava/lang/String;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2612631
    invoke-static {p1}, LX/InS;->a(Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2612632
    iget-object v0, p0, LX/IpG;->b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->h:LX/InS;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/IpG;->b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;

    iget-object v2, v2, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612633
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v2, v3

    .line 2612634
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/IpG;->b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;

    iget-object v3, v3, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->f:Landroid/content/Context;

    .line 2612635
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2612636
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2612637
    new-instance v4, Landroid/content/Intent;

    const-class p1, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;

    invoke-direct {v4, v3, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2612638
    const-string p1, "transaction_id"

    invoke-virtual {v4, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2612639
    const-string p1, "recipient_id"

    invoke-virtual {v4, p1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2612640
    move-object v4, v4

    .line 2612641
    iget-object p1, v0, LX/InS;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1, v4, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2612642
    :cond_0
    iget-object v0, p0, LX/IpG;->b:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;

    .line 2612643
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2612644
    const-string v2, "nux_follow_up_action"

    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612645
    iget-object v4, v3, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    move-object v3, v4

    .line 2612646
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2612647
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->k:LX/Io2;

    invoke-virtual {v2, v1}, LX/Io2;->a(Landroid/content/Intent;)V

    .line 2612648
    return-void
.end method
