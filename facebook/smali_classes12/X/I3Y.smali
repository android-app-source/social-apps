.class public LX/I3Y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;

.field private final c:LX/1My;

.field private final d:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "LX/7oa;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:LX/Gcz;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/1My;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2531503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2531504
    new-instance v0, LX/I3S;

    invoke-direct {v0, p0}, LX/I3S;-><init>(LX/I3Y;)V

    iput-object v0, p0, LX/I3Y;->d:LX/0TF;

    .line 2531505
    iput-object p1, p0, LX/I3Y;->a:LX/0tX;

    .line 2531506
    iput-object p2, p0, LX/I3Y;->b:LX/1Ck;

    .line 2531507
    iput-object p3, p0, LX/I3Y;->c:LX/1My;

    .line 2531508
    return-void
.end method

.method public static a$redex0(LX/I3Y;Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$SuggestedEventCut;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2531490
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_0

    invoke-virtual {v7, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oa;

    .line 2531491
    invoke-interface {v2}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v6

    .line 2531492
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2531493
    iget-object v3, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2531494
    iget-wide v9, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v9

    .line 2531495
    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2531496
    iget-object v3, p0, LX/I3Y;->c:LX/1My;

    iget-object v4, p0, LX/I3Y;->d:LX/0TF;

    invoke-interface {v2}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2531497
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2531498
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/I3Y;
    .locals 4

    .prologue
    .line 2531499
    new-instance v3, LX/I3Y;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v2

    check-cast v2, LX/1My;

    invoke-direct {v3, v0, v1, v2}, LX/I3Y;-><init>(LX/0tX;LX/1Ck;LX/1My;)V

    .line 2531500
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2531501
    iget-object v0, p0, LX/I3Y;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2531502
    return-void
.end method
