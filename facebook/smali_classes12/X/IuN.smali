.class public LX/IuN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Ox;

.field private final b:LX/IuU;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2Ow;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Ox;LX/IuU;LX/0Or;LX/2Ow;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ox;",
            "LX/IuU;",
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;",
            "LX/2Ow;",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2626568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2626569
    iput-object p1, p0, LX/IuN;->a:LX/2Ox;

    .line 2626570
    iput-object p2, p0, LX/IuN;->b:LX/IuU;

    .line 2626571
    iput-object p3, p0, LX/IuN;->c:LX/0Or;

    .line 2626572
    iput-object p4, p0, LX/IuN;->d:LX/2Ow;

    .line 2626573
    iput-object p5, p0, LX/IuN;->e:LX/0Or;

    .line 2626574
    return-void
.end method

.method private c(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 2626575
    iget-object v0, p0, LX/IuN;->b:LX/IuU;

    invoke-virtual {v0, p1}, LX/IuU;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626576
    invoke-virtual {p0, p1}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626577
    iget-object v0, p0, LX/IuN;->d:LX/2Ow;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626578
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2626579
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_DB_NEED_INITIAL_FETCH:LX/0ta;

    iget-wide v6, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    move-object v3, p1

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2626580
    iget-object v0, p0, LX/IuN;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, v1}, LX/2Oe;->b(Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2626581
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;[BLjava/lang/String;)V
    .locals 1
    .param p2    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2626582
    iget-object v0, p0, LX/IuN;->a:LX/2Ox;

    invoke-virtual {v0, p1, p2, p3}, LX/2Ox;->a(Lcom/facebook/messaging/model/messages/Message;[BLjava/lang/String;)V

    .line 2626583
    invoke-direct {p0, p1}, LX/IuN;->c(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626584
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 5

    .prologue
    .line 2626585
    new-instance v0, LX/6fO;

    invoke-direct {v0}, LX/6fO;-><init>()V

    sget-object v1, LX/6fP;->TINCAN_NONRETRYABLE:LX/6fP;

    .line 2626586
    iput-object v1, v0, LX/6fO;->a:LX/6fP;

    .line 2626587
    move-object v0, v0

    .line 2626588
    sget-object v1, LX/Doq;->a:LX/Doq;

    move-object v1, v1

    .line 2626589
    invoke-virtual {v1}, LX/Doq;->a()J

    move-result-wide v2

    .line 2626590
    iput-wide v2, v0, LX/6fO;->c:J

    .line 2626591
    move-object v1, v0

    .line 2626592
    iget-object v0, p0, LX/IuN;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0806c0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2626593
    iput-object v0, v1, LX/6fO;->b:Ljava/lang/String;

    .line 2626594
    move-object v0, v1

    .line 2626595
    invoke-virtual {v0}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    .line 2626596
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v1

    sget-object v2, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 2626597
    iput-object v2, v1, LX/6f7;->l:LX/2uW;

    .line 2626598
    move-object v1, v1

    .line 2626599
    iput-object v0, v1, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 2626600
    move-object v1, v1

    .line 2626601
    invoke-virtual {v1}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2626602
    iget-object v2, p0, LX/IuN;->a:LX/2Ox;

    iget-object v3, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/2Ox;->a(Ljava/lang/String;Lcom/facebook/messaging/model/send/SendError;)V

    .line 2626603
    invoke-direct {p0, v1}, LX/IuN;->c(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2626604
    return-void
.end method
