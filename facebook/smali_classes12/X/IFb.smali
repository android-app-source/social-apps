.class public LX/IFb;
.super LX/IFZ;
.source ""

# interfaces
.implements LX/IFR;


# instance fields
.field public a:Lcom/facebook/location/ImmutableLocation;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/location/ImmutableLocation;LX/0Uh;)V
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/location/ImmutableLocation;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2554644
    const-string v7, "aura"

    sget-object v10, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v11}, LX/IFZ;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/3OL;LX/0Uh;)V

    .line 2554645
    move-object/from16 v0, p8

    iput-object v0, p0, LX/IFb;->a:Lcom/facebook/location/ImmutableLocation;

    .line 2554646
    return-void
.end method

.method public static a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;LX/IFO;Ljava/lang/String;Ljava/lang/String;ZLX/IFc;)LX/IFb;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554647
    invoke-static {p0}, LX/IFO;->a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v8

    .line 2554648
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->e()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v0

    .line 2554649
    if-nez v0, :cond_0

    .line 2554650
    const/4 v0, 0x0

    .line 2554651
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;->d()LX/1Fb;

    move-result-object v2

    .line 2554652
    if-nez v2, :cond_1

    .line 2554653
    const/4 v3, 0x0

    .line 2554654
    :goto_1
    move-object v2, v3

    .line 2554655
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->d()Ljava/lang/String;

    move-result-object v4

    move-object v0, p5

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    invoke-virtual/range {v0 .. v8}, LX/IFc;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/location/ImmutableLocation;)LX/IFb;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_1
.end method
