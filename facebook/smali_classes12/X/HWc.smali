.class public final LX/HWc;
.super LX/CXx;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2476871
    iput-object p1, p0, LX/HWc;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-direct {p0, p2}, LX/CXx;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2476872
    check-cast p1, LX/CXw;

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2476873
    iget-object v0, p1, LX/CXw;->c:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    .line 2476874
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->j()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabCtaChannelModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->j()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabCtaChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabCtaChannelModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2476875
    :cond_0
    iget-object v0, p0, LX/HWc;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->z:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2476876
    iget-object v0, p0, LX/HWc;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->A:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-virtual {v0, v3}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->setVisibility(I)V

    .line 2476877
    :goto_0
    return-void

    .line 2476878
    :cond_1
    iget-object v0, p0, LX/HWc;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->z:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2476879
    iget-object v0, p0, LX/HWc;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->A:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->setVisibility(I)V

    goto :goto_0
.end method
