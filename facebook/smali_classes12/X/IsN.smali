.class public LX/IsN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/3MW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/3Mv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2619677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/IsO;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IsO;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/IsP;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2619678
    new-instance v0, LX/IsU;

    invoke-direct {v0}, LX/IsU;-><init>()V

    .line 2619679
    const-string v1, "search_query"

    iget-object v2, p1, LX/IsO;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "results_limit"

    iget v3, p1, LX/IsO;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "entity_types"

    iget-object v3, p1, LX/IsO;->b:Ljava/util/EnumSet;

    .line 2619680
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 2619681
    invoke-virtual {v3}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/IsR;

    .line 2619682
    sget-object v7, LX/IsQ;->a:[I

    invoke-virtual {v4}, LX/IsR;->ordinal()I

    move-result v4

    aget v4, v7, v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 2619683
    :pswitch_0
    const-string v4, "user"

    invoke-virtual {v5, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2619684
    :pswitch_1
    const-string v4, "group_thread"

    invoke-virtual {v5, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2619685
    :cond_0
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 2619686
    invoke-virtual {v1, v2, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "user_types"

    iget-object v3, p1, LX/IsO;->b:Ljava/util/EnumSet;

    .line 2619687
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2619688
    sget-object v5, LX/IsR;->CONTACTS:LX/IsR;

    invoke-virtual {v3, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2619689
    const-string v5, "CONTACT"

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2619690
    :cond_1
    sget-object v5, LX/IsR;->FRIENDS:LX/IsR;

    invoke-virtual {v3, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2619691
    const-string v5, "FRIEND"

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2619692
    :cond_2
    sget-object v5, LX/IsR;->UNCONNECTED_USERS:LX/IsR;

    invoke-virtual {v3, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2619693
    const-string v5, "NON_FRIEND_NON_CONTACT"

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2619694
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 2619695
    invoke-virtual {v1, v2, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2619696
    iget-object v1, p0, LX/IsN;->b:LX/3MW;

    invoke-virtual {v1, v0}, LX/3MW;->a(LX/0gW;)V

    .line 2619697
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    move-object v0, v0

    .line 2619698
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v2, 0xe10

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 2619699
    iget-object v1, p0, LX/IsN;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2619700
    new-instance v1, LX/IsM;

    invoke-direct {v1, p0}, LX/IsM;-><init>(LX/IsN;)V

    iget-object v2, p0, LX/IsN;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
