.class public LX/JDP;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/J9L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:I

.field public final c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2663935
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2663936
    const-class v1, LX/JDP;

    invoke-static {v1, p0}, LX/JDP;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2663937
    const/4 v1, 0x4

    move v1, v1

    .line 2663938
    iput v1, p0, LX/JDP;->b:I

    .line 2663939
    invoke-virtual {p0}, LX/JDP;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2663940
    const v2, 0x7f0302a8

    invoke-virtual {v1, v2, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/JDP;->c:Landroid/view/View;

    .line 2663941
    iget-object v1, p0, LX/JDP;->c:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, LX/JDP;->addView(Landroid/view/View;I)V

    .line 2663942
    :goto_0
    iget v1, p0, LX/JDP;->b:I

    if-ge v0, v1, :cond_0

    .line 2663943
    new-instance v1, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    invoke-direct {v1, p1}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v1}, LX/JDP;->addView(Landroid/view/View;)V

    .line 2663944
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2663945
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/JDP;->setOrientation(I)V

    .line 2663946
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/JDP;

    invoke-static {p0}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object p0

    check-cast p0, LX/J9L;

    iput-object p0, p1, LX/JDP;->a:LX/J9L;

    return-void
.end method
