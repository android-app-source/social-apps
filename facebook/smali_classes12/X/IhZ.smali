.class public LX/IhZ;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/LayoutInflater;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/Ih9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/IhO;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/media/folder/Folder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2602758
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2602759
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IhZ;->e:Ljava/util/List;

    .line 2602760
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    .line 2602761
    iget-object v0, p0, LX/IhZ;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b18b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2602762
    iget-object v1, p0, LX/IhZ;->c:Landroid/content/res/Resources;

    const v2, 0x7f0b18b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2602763
    iget-object v2, p0, LX/IhZ;->a:Landroid/view/LayoutInflater;

    const v3, 0x7f030a9c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2602764
    iget-object v3, p0, LX/IhZ;->b:LX/Ih9;

    new-instance v4, LX/1o9;

    invoke-direct {v4, v0, v1}, LX/1o9;-><init>(II)V

    .line 2602765
    new-instance p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    invoke-static {v3}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {p0, v0, v1, v2, v4}, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;-><init>(LX/1Ad;Landroid/content/res/Resources;Landroid/view/View;LX/1o9;)V

    .line 2602766
    move-object v0, p0

    .line 2602767
    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 2602768
    iget-object v0, p0, LX/IhZ;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/folder/Folder;

    .line 2602769
    check-cast p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    .line 2602770
    iput-object v0, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->u:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602771
    iget-object v1, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->u:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602772
    iget-object v2, v1, Lcom/facebook/messaging/media/folder/Folder;->c:Landroid/net/Uri;

    move-object v1, v2

    .line 2602773
    iget-object v2, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->u:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602774
    iget-object v3, v2, Lcom/facebook/messaging/media/folder/Folder;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2602775
    iget-object v3, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->u:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602776
    iget v0, v3, Lcom/facebook/messaging/media/folder/Folder;->d:I

    move v3, v0

    .line 2602777
    const/4 p2, 0x0

    .line 2602778
    if-eqz v1, :cond_0

    .line 2602779
    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->o:LX/1o9;

    .line 2602780
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 2602781
    move-object v4, v4

    .line 2602782
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    .line 2602783
    iget-object v5, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v6, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->m:LX/1Ad;

    invoke-virtual {v6}, LX/1Ad;->o()LX/1Ad;

    move-result-object v6

    iget-object v7, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    iget-object v6, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v6}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2602784
    :cond_0
    iget-object v4, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->p:Landroid/view/View;

    new-instance v5, LX/Ih8;

    invoke-direct {v5, p1}, LX/Ih8;-><init>(Lcom/facebook/messaging/media/picker/FolderItemViewHolder;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2602785
    iget-object v4, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->p:Landroid/view/View;

    iget-object v5, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->n:Landroid/content/res/Resources;

    const v6, 0x7f0f0117

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, p2

    const/4 p2, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, p2

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2602786
    iget-object v4, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->r:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2602787
    iget-object v4, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->s:Landroid/widget/TextView;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2602788
    new-instance v0, LX/IhY;

    invoke-direct {v0, p0}, LX/IhY;-><init>(LX/IhZ;)V

    .line 2602789
    iput-object v0, p1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->t:LX/IhY;

    .line 2602790
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2602791
    iget-object v0, p0, LX/IhZ;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
