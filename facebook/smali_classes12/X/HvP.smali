.class public LX/HvP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ARX;


# instance fields
.field private final a:LX/HvO;


# direct methods
.method private constructor <init>(LX/HvO;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2519064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2519065
    iput-object p1, p0, LX/HvP;->a:LX/HvO;

    .line 2519066
    return-void
.end method

.method public static b(LX/0QB;)LX/HvP;
    .locals 2

    .prologue
    .line 2519067
    new-instance v1, LX/HvP;

    const-class v0, LX/HvO;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/HvO;

    invoke-direct {v1, v0}, LX/HvP;-><init>(LX/HvO;)V

    .line 2519068
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;LX/Ar6;)LX/HvN;
    .locals 11

    .prologue
    .line 2519069
    iget-object v0, p0, LX/HvP;->a:LX/HvO;

    .line 2519070
    new-instance v1, LX/HvN;

    const-class v2, LX/2s2;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2s2;

    const-class v2, LX/Hv9;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Hv9;

    .line 2519071
    new-instance v2, LX/HvK;

    const/16 v3, 0x19c0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-direct {v2, v3, v6}, LX/HvK;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2519072
    move-object v6, v2

    .line 2519073
    check-cast v6, LX/HvK;

    const/16 v2, 0x3d7

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v2, 0x19d7

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1MD;->a(LX/0QB;)LX/1MD;

    move-result-object v9

    check-cast v9, LX/1MD;

    const/16 v2, 0x259

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v10}, LX/HvN;-><init>(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;LX/Ar6;LX/2s2;LX/Hv9;LX/HvK;LX/0Ot;LX/0Ot;LX/1MD;LX/0Ot;)V

    .line 2519074
    move-object v0, v1

    .line 2519075
    return-object v0
.end method
