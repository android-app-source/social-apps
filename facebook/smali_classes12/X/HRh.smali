.class public LX/HRh;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/HRh;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2465721
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2465722
    const-string v0, "page/{#%s}/notifications"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->FB4A_PAGES_NOTIFICATIONS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2465723
    return-void
.end method

.method public static a(LX/0QB;)LX/HRh;
    .locals 3

    .prologue
    .line 2465724
    sget-object v0, LX/HRh;->a:LX/HRh;

    if-nez v0, :cond_1

    .line 2465725
    const-class v1, LX/HRh;

    monitor-enter v1

    .line 2465726
    :try_start_0
    sget-object v0, LX/HRh;->a:LX/HRh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2465727
    if-eqz v2, :cond_0

    .line 2465728
    :try_start_1
    new-instance v0, LX/HRh;

    invoke-direct {v0}, LX/HRh;-><init>()V

    .line 2465729
    move-object v0, v0

    .line 2465730
    sput-object v0, LX/HRh;->a:LX/HRh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2465731
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2465732
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2465733
    :cond_1
    sget-object v0, LX/HRh;->a:LX/HRh;

    return-object v0

    .line 2465734
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2465735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
