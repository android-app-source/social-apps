.class public final LX/I3V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:I

.field public final synthetic d:I

.field public final synthetic e:LX/I3Y;


# direct methods
.method public constructor <init>(LX/I3Y;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 2531459
    iput-object p1, p0, LX/I3V;->e:LX/I3Y;

    iput-object p2, p0, LX/I3V;->a:Ljava/lang/String;

    iput-object p3, p0, LX/I3V;->b:Ljava/lang/String;

    iput p4, p0, LX/I3V;->c:I

    iput p5, p0, LX/I3V;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2531460
    new-instance v0, LX/7oR;

    invoke-direct {v0}, LX/7oR;-><init>()V

    move-object v0, v0

    .line 2531461
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2531462
    new-instance v1, LX/7oR;

    invoke-direct {v1}, LX/7oR;-><init>()V

    const-string v2, "cut_type"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, LX/I3V;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "end_cursor"

    iget-object v3, p0, LX/I3V;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "per_cut_count"

    iget v3, p0, LX/I3V;->c:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget v3, p0, LX/I3V;->d:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2531463
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2531464
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2531465
    iget-object v1, p0, LX/I3V;->e:LX/I3Y;

    iget-object v1, v1, LX/I3Y;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
