.class public final LX/JAt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2656913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2656914
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;
    .locals 15

    .prologue
    .line 2656915
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2656916
    iget-object v1, p0, LX/JAt;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2656917
    iget-object v2, p0, LX/JAt;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2656918
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, LX/JAt;->c:LX/15i;

    iget v5, p0, LX/JAt;->d:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v3, -0x1b2b9a67

    invoke-static {v4, v5, v3}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2656919
    iget-object v4, p0, LX/JAt;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 2656920
    iget-object v5, p0, LX/JAt;->f:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2656921
    iget-object v6, p0, LX/JAt;->g:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2656922
    iget-object v7, p0, LX/JAt;->i:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2656923
    iget-object v8, p0, LX/JAt;->j:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2656924
    iget-object v9, p0, LX/JAt;->k:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 2656925
    iget-object v10, p0, LX/JAt;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 2656926
    iget-object v11, p0, LX/JAt;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 2656927
    iget-object v12, p0, LX/JAt;->n:LX/0Px;

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 2656928
    iget-object v13, p0, LX/JAt;->o:LX/0Px;

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 2656929
    const/16 v14, 0xe

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 2656930
    const/4 v14, 0x0

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 2656931
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2656932
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2656933
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2656934
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2656935
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2656936
    const/4 v1, 0x6

    iget-boolean v2, p0, LX/JAt;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2656937
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2656938
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2656939
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2656940
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2656941
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2656942
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 2656943
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 2656944
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2656945
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2656946
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2656947
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2656948
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2656949
    new-instance v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;-><init>(LX/15i;)V

    .line 2656950
    return-object v1

    .line 2656951
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
