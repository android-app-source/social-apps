.class public final LX/IdC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ics;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 0

    .prologue
    .line 2596105
    iput-object p1, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2596106
    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->v(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596107
    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 2596108
    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    .line 2596109
    iput-object p1, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    .line 2596110
    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-boolean v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z:Z

    if-nez v0, :cond_0

    .line 2596111
    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->v(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596112
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2596113
    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-boolean v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->A(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2596114
    :cond_0
    :goto_0
    return-void

    .line 2596115
    :cond_1
    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const v2, 0x7f082d73

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/Ib6;->ORIGIN:LX/Ib6;

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    .line 2596116
    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iput-object v6, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    .line 2596117
    iget-object v1, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v4, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    .line 2596118
    invoke-static/range {v1 .. v6}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DDLjava/lang/String;)V

    .line 2596119
    iget-object v0, p0, LX/IdC;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v4, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v0, v2, v3, v4, v5}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DD)V

    goto :goto_0
.end method
