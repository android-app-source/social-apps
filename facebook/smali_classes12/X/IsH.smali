.class public final synthetic LX/IsH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2619417
    invoke-static {}, LX/IsB;->values()[LX/IsB;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/IsH;->b:[I

    :try_start_0
    sget-object v0, LX/IsH;->b:[I

    sget-object v1, LX/IsB;->TEXT_CHANGE:LX/IsB;

    invoke-virtual {v1}, LX/IsB;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, LX/IsH;->b:[I

    sget-object v1, LX/IsB;->TEXT_COLOR_CHANGE:LX/IsB;

    invoke-virtual {v1}, LX/IsB;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    :try_start_2
    sget-object v0, LX/IsH;->b:[I

    sget-object v1, LX/IsB;->BACKGROUND_COLOR_CHANGE:LX/IsB;

    invoke-virtual {v1}, LX/IsB;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 2619418
    :goto_2
    invoke-static {}, LX/Iqn;->values()[LX/Iqn;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/IsH;->a:[I

    :try_start_3
    sget-object v0, LX/IsH;->a:[I

    sget-object v1, LX/Iqn;->DYNAMIC_COLOR_CHANGED:LX/Iqn;

    invoke-virtual {v1}, LX/Iqn;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, LX/IsH;->a:[I

    sget-object v1, LX/Iqn;->VISIBILITY_CHANGED:LX/Iqn;

    invoke-virtual {v1}, LX/Iqn;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    goto :goto_4

    :catch_1
    goto :goto_3

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_0
.end method
