.class public final LX/HUn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2473816
    iput-object p1, p0, LX/HUn;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2473817
    iget-object v0, p0, LX/HUn;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->c:LX/0tX;

    iget-object v1, p0, LX/HUn;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    .line 2473818
    invoke-static {}, LX/HUu;->a()LX/HUt;

    move-result-object v2

    .line 2473819
    const-string v3, "should_fetch_videos"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "page_id"

    iget-wide v6, v1, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->i:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "video_list_id_to_fetch"

    iget-wide v6, v1, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->h:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2473820
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    move-object v1, v2

    .line 2473821
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2473822
    return-object v0
.end method
