.class public LX/Hai;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HbG;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/HbG;

.field public c:LX/HbG;

.field public d:LX/HbG;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2484328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2484329
    iput-boolean v0, p0, LX/Hai;->e:Z

    .line 2484330
    iput-boolean v0, p0, LX/Hai;->f:Z

    .line 2484331
    iput-boolean v0, p0, LX/Hai;->g:Z

    .line 2484332
    iput-boolean v0, p0, LX/Hai;->h:Z

    .line 2484333
    return-void
.end method

.method public static a(LX/0QB;)LX/Hai;
    .locals 3

    .prologue
    .line 2484334
    const-class v1, LX/Hai;

    monitor-enter v1

    .line 2484335
    :try_start_0
    sget-object v0, LX/Hai;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2484336
    sput-object v2, LX/Hai;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2484337
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2484338
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2484339
    new-instance v0, LX/Hai;

    invoke-direct {v0}, LX/Hai;-><init>()V

    .line 2484340
    move-object v0, v0

    .line 2484341
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2484342
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hai;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2484343
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2484344
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final i()Z
    .locals 2

    .prologue
    .line 2484345
    iget-object v0, p0, LX/Hai;->b:LX/HbG;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hai;->b:LX/HbG;

    .line 2484346
    iget-boolean v1, v0, LX/HbG;->e:Z

    move v0, v1

    .line 2484347
    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, LX/Hai;->c:LX/HbG;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Hai;->c:LX/HbG;

    .line 2484348
    iget-boolean v1, v0, LX/HbG;->e:Z

    move v0, v1

    .line 2484349
    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/Hai;->d:LX/HbG;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Hai;->d:LX/HbG;

    .line 2484350
    iget-boolean v1, v0, LX/HbG;->e:Z

    move v0, v1

    .line 2484351
    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
