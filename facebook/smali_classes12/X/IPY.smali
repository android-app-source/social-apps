.class public LX/IPY;
.super LX/9Bh;
.source ""


# instance fields
.field public final a:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/IPW;

.field private final c:LX/DN9;

.field public final d:LX/13B;

.field private final e:LX/0ad;

.field public final f:Landroid/content/Context;

.field private final g:Z

.field private final h:Landroid/os/Handler;

.field public i:I

.field public j:I

.field public k:Z

.field private l:Ljava/lang/Runnable;

.field public m:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/0zw;LX/DN9;Landroid/content/Context;LX/IPW;LX/13B;LX/0ad;LX/0wW;LX/4mV;)V
    .locals 3
    .param p1    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/DN9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/IPW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;",
            "LX/DN9;",
            "Landroid/content/Context;",
            "LX/IPW;",
            "LX/13B;",
            "LX/0ad;",
            "LX/0wW;",
            "Lcom/facebook/ui/animations/ViewAnimatorFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2574286
    invoke-direct {p0, p7, p8}, LX/9Bh;-><init>(LX/0wW;LX/4mV;)V

    .line 2574287
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/IPY;->h:Landroid/os/Handler;

    .line 2574288
    iput-object p1, p0, LX/IPY;->a:LX/0zw;

    .line 2574289
    iput-object p4, p0, LX/IPY;->b:LX/IPW;

    .line 2574290
    iput-object p2, p0, LX/IPY;->c:LX/DN9;

    .line 2574291
    iput-object p5, p0, LX/IPY;->d:LX/13B;

    .line 2574292
    iput-object p6, p0, LX/IPY;->e:LX/0ad;

    .line 2574293
    iput-object p3, p0, LX/IPY;->f:Landroid/content/Context;

    .line 2574294
    sget-short v0, LX/100;->ab:S

    invoke-interface {p6, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/IPY;->g:Z

    .line 2574295
    sget v0, LX/100;->ad:I

    const v1, 0x7fffffff

    invoke-interface {p6, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/IPY;->i:I

    .line 2574296
    iput v2, p0, LX/IPY;->j:I

    .line 2574297
    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 2574258
    iget-boolean v0, p0, LX/IPY;->k:Z

    if-eqz v0, :cond_0

    .line 2574259
    iget-object v0, p0, LX/IPY;->d:LX/13B;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/13B;->a(Z)V

    .line 2574260
    invoke-static {p0, p1}, LX/IPY;->d(LX/IPY;I)V

    .line 2574261
    :cond_0
    return-void
.end method

.method public static d(LX/IPY;I)V
    .locals 3

    .prologue
    .line 2574262
    invoke-virtual {p0}, LX/9Bh;->b()V

    .line 2574263
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IPY;->k:Z

    .line 2574264
    iget-object v0, p0, LX/IPY;->e:LX/0ad;

    sget v1, LX/100;->ad:I

    const v2, 0x7fffffff

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    add-int/2addr v0, p1

    iput v0, p0, LX/IPY;->i:I

    .line 2574265
    return-void
.end method

.method private f(I)LX/IPX;
    .locals 1

    .prologue
    .line 2574266
    iget v0, p0, LX/IPY;->j:I

    if-le p1, v0, :cond_0

    .line 2574267
    sget-object v0, LX/IPX;->SCROLL_DOWN:LX/IPX;

    .line 2574268
    :goto_0
    return-object v0

    .line 2574269
    :cond_0
    iget v0, p0, LX/IPY;->j:I

    if-ge p1, v0, :cond_1

    .line 2574270
    sget-object v0, LX/IPX;->SCROLL_UP:LX/IPX;

    goto :goto_0

    .line 2574271
    :cond_1
    sget-object v0, LX/IPX;->UNKNOWN:LX/IPX;

    goto :goto_0
.end method

.method public static g(LX/IPY;I)I
    .locals 1

    .prologue
    .line 2574272
    iget-object v0, p0, LX/IPY;->c:LX/DN9;

    invoke-interface {v0, p1}, LX/DN9;->w_(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 2574273
    iget-boolean v0, p0, LX/IPY;->g:Z

    if-nez v0, :cond_0

    .line 2574274
    :goto_0
    return-void

    .line 2574275
    :cond_0
    invoke-static {p0, p1}, LX/IPY;->g(LX/IPY;I)I

    move-result v0

    .line 2574276
    invoke-direct {p0, p1}, LX/IPY;->f(I)LX/IPX;

    move-result-object v1

    sget-object v2, LX/IPX;->SCROLL_UP:LX/IPX;

    if-ne v1, v2, :cond_2

    .line 2574277
    invoke-direct {p0, v0}, LX/IPY;->c(I)V

    .line 2574278
    :cond_1
    :goto_1
    iput p1, p0, LX/IPY;->j:I

    goto :goto_0

    .line 2574279
    :cond_2
    invoke-direct {p0, p1}, LX/IPY;->f(I)LX/IPX;

    move-result-object v1

    sget-object v2, LX/IPX;->SCROLL_DOWN:LX/IPX;

    if-ne v1, v2, :cond_1

    .line 2574280
    iget-object v1, p0, LX/IPY;->h:Landroid/os/Handler;

    iget-object v2, p0, LX/IPY;->l:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2574281
    new-instance v1, Lcom/facebook/groups/feed/controller/SearchAwarenessGroupsPillController$1;

    invoke-direct {v1, p0, v0}, Lcom/facebook/groups/feed/controller/SearchAwarenessGroupsPillController$1;-><init>(LX/IPY;I)V

    iput-object v1, p0, LX/IPY;->l:Ljava/lang/Runnable;

    .line 2574282
    iget-object v0, p0, LX/IPY;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/IPY;->l:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    const v4, 0x465730a3

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1
.end method

.method public final f()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2574283
    iget-object v0, p0, LX/IPY;->a:LX/0zw;

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2574284
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/IPY;->c(I)V

    .line 2574285
    return-void
.end method
