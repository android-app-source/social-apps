.class public final LX/INc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;

.field public final synthetic c:LX/INd;


# direct methods
.method public constructor <init>(LX/INd;Ljava/lang/String;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;)V
    .locals 0

    .prologue
    .line 2570968
    iput-object p1, p0, LX/INc;->c:LX/INd;

    iput-object p2, p0, LX/INc;->a:Ljava/lang/String;

    iput-object p3, p0, LX/INc;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x75f3c914

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2570969
    iget-object v0, p0, LX/INc;->c:LX/INd;

    const-string v2, "community_trending_stories_unit_top_story_clicked"

    iget-object v3, p0, LX/INc;->a:Ljava/lang/String;

    invoke-static {v0, v2, v3}, LX/INd;->a$redex0(LX/INd;Ljava/lang/String;Ljava/lang/String;)V

    .line 2570970
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    iget-object v2, p0, LX/INc;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->hD_()Ljava/lang/String;

    move-result-object v2

    .line 2570971
    iput-object v2, v0, LX/89k;->b:Ljava/lang/String;

    .line 2570972
    move-object v0, v0

    .line 2570973
    iget-object v2, p0, LX/INc;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 2570974
    iput-object v2, v0, LX/89k;->c:Ljava/lang/String;

    .line 2570975
    move-object v0, v0

    .line 2570976
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v2

    .line 2570977
    iget-object v0, p0, LX/INc;->c:LX/INd;

    iget-object v3, v0, LX/INd;->e:LX/0hy;

    iget-object v0, p0, LX/INc;->c:LX/INd;

    iget-object v0, v0, LX/INd;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-interface {v3, v0, v2}, LX/0hy;->a(Landroid/content/ComponentName;Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2570978
    iget-object v2, p0, LX/INc;->c:LX/INd;

    iget-object v2, v2, LX/INd;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/INc;->c:LX/INd;

    invoke-virtual {v3}, LX/INd;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2570979
    const v0, -0xdf11e50

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
