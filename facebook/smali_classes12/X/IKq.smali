.class public LX/IKq;
.super LX/1a1;
.source ""


# instance fields
.field public l:LX/IK6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final m:Landroid/view/View$OnClickListener;

.field public n:Lcom/facebook/fig/sectionheader/FigSectionHeader;

.field public o:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2567347
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2567348
    new-instance v0, LX/IKp;

    invoke-direct {v0, p0}, LX/IKp;-><init>(LX/IKq;)V

    iput-object v0, p0, LX/IKq;->m:Landroid/view/View$OnClickListener;

    .line 2567349
    const v0, 0x7f0d14fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    iput-object v0, p0, LX/IKq;->n:Lcom/facebook/fig/sectionheader/FigSectionHeader;

    .line 2567350
    const v0, 0x7f0d14fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/IKq;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2567351
    iget-object v0, p0, LX/IKq;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/IKq;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2567352
    return-void
.end method
