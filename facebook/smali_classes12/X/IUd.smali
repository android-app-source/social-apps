.class public final LX/IUd;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsYourPostsFeedsFragment;

.field public b:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsYourPostsFeedsFragment;LX/0gc;)V
    .locals 0

    .prologue
    .line 2580813
    iput-object p1, p0, LX/IUd;->a:Lcom/facebook/groups/feed/ui/GroupsYourPostsFeedsFragment;

    .line 2580814
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2580815
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2580812
    iget-object v0, p0, LX/IUd;->a:Lcom/facebook/groups/feed/ui/GroupsYourPostsFeedsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, LX/IUc;->values()[LX/IUc;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, LX/IUc;->getTitleId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2580816
    invoke-static {}, LX/IUc;->values()[LX/IUc;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2580817
    new-instance v1, LX/B1S;

    invoke-direct {v1}, LX/B1S;-><init>()V

    .line 2580818
    iget-object v2, p0, LX/IUd;->a:Lcom/facebook/groups/feed/ui/GroupsYourPostsFeedsFragment;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v2

    .line 2580819
    iput-object v2, v1, LX/B1S;->a:Ljava/lang/String;

    .line 2580820
    move-object v2, v1

    .line 2580821
    invoke-virtual {v0}, LX/IUc;->getGroupsFeedType()LX/B1T;

    move-result-object v3

    .line 2580822
    iput-object v3, v2, LX/B1S;->b:LX/B1T;

    .line 2580823
    new-instance v2, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v1}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    invoke-virtual {v0}, LX/IUc;->getFeedTypeName()Lcom/facebook/api/feedtype/FeedType$Name;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    .line 2580824
    iget-object v0, p0, LX/IUd;->a:Lcom/facebook/groups/feed/ui/GroupsYourPostsFeedsFragment;

    .line 2580825
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2580826
    new-instance v1, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;

    invoke-direct {v1}, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;-><init>()V

    .line 2580827
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 2580828
    const-string p0, "feed_type_arguments_key"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2580829
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2580830
    move-object v0, v1

    .line 2580831
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2580811
    invoke-static {}, LX/IUc;->values()[LX/IUc;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2580806
    iget-object v0, p0, LX/IUd;->b:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2580807
    if-eq v0, p3, :cond_0

    move-object v0, p3

    .line 2580808
    check-cast v0, Landroid/support/v4/app/Fragment;

    iput-object v0, p0, LX/IUd;->b:Landroid/support/v4/app/Fragment;

    .line 2580809
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/2s5;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2580810
    return-void
.end method
