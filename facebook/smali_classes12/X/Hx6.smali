.class public final enum LX/Hx6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hx6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hx6;

.field public static final enum BIRTHDAYS:LX/Hx6;

.field public static final enum HOSTING:LX/Hx6;

.field public static final enum INVITED:LX/Hx6;

.field public static final enum PAST:LX/Hx6;

.field public static final enum UPCOMING:LX/Hx6;


# instance fields
.field public final iconResId:I

.field public final menuStringResId:I

.field public final selectedIconResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2522003
    new-instance v0, LX/Hx6;

    const-string v1, "UPCOMING"

    const v3, 0x7f082194

    const v4, 0x7f020398

    const v5, 0x7f020399

    invoke-direct/range {v0 .. v5}, LX/Hx6;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, LX/Hx6;->UPCOMING:LX/Hx6;

    .line 2522004
    new-instance v3, LX/Hx6;

    const-string v4, "INVITED"

    const v6, 0x7f082196

    const v7, 0x7f020394

    const v8, 0x7f020395

    move v5, v9

    invoke-direct/range {v3 .. v8}, LX/Hx6;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LX/Hx6;->INVITED:LX/Hx6;

    .line 2522005
    new-instance v3, LX/Hx6;

    const-string v4, "BIRTHDAYS"

    const v6, 0x7f082197

    const v7, 0x7f020392

    const v8, 0x7f020393

    move v5, v10

    invoke-direct/range {v3 .. v8}, LX/Hx6;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    .line 2522006
    new-instance v3, LX/Hx6;

    const-string v4, "HOSTING"

    const v6, 0x7f082198

    const v7, 0x7f020392

    const v8, 0x7f020393

    move v5, v11

    invoke-direct/range {v3 .. v8}, LX/Hx6;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LX/Hx6;->HOSTING:LX/Hx6;

    .line 2522007
    new-instance v3, LX/Hx6;

    const-string v4, "PAST"

    const v6, 0x7f082195

    const v7, 0x7f020396

    const v8, 0x7f020397

    move v5, v12

    invoke-direct/range {v3 .. v8}, LX/Hx6;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LX/Hx6;->PAST:LX/Hx6;

    .line 2522008
    const/4 v0, 0x5

    new-array v0, v0, [LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    aput-object v1, v0, v2

    sget-object v1, LX/Hx6;->INVITED:LX/Hx6;

    aput-object v1, v0, v9

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    aput-object v1, v0, v10

    sget-object v1, LX/Hx6;->HOSTING:LX/Hx6;

    aput-object v1, v0, v11

    sget-object v1, LX/Hx6;->PAST:LX/Hx6;

    aput-object v1, v0, v12

    sput-object v0, LX/Hx6;->$VALUES:[LX/Hx6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 2522009
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2522010
    iput p3, p0, LX/Hx6;->menuStringResId:I

    .line 2522011
    iput p4, p0, LX/Hx6;->iconResId:I

    .line 2522012
    iput p5, p0, LX/Hx6;->selectedIconResId:I

    .line 2522013
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hx6;
    .locals 1

    .prologue
    .line 2522014
    const-class v0, LX/Hx6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hx6;

    return-object v0
.end method

.method public static values()[LX/Hx6;
    .locals 1

    .prologue
    .line 2522015
    sget-object v0, LX/Hx6;->$VALUES:[LX/Hx6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hx6;

    return-object v0
.end method
