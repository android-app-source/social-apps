.class public final LX/JOT;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JOU;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JOU;


# direct methods
.method public constructor <init>(LX/JOU;)V
    .locals 1

    .prologue
    .line 2687578
    iput-object p1, p0, LX/JOT;->c:LX/JOU;

    .line 2687579
    move-object v0, p1

    .line 2687580
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2687581
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2687582
    const-string v0, "GroupTopStoriesHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2687583
    if-ne p0, p1, :cond_1

    .line 2687584
    :cond_0
    :goto_0
    return v0

    .line 2687585
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2687586
    goto :goto_0

    .line 2687587
    :cond_3
    check-cast p1, LX/JOT;

    .line 2687588
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2687589
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2687590
    if-eq v2, v3, :cond_0

    .line 2687591
    iget-object v2, p0, LX/JOT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JOT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JOT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2687592
    goto :goto_0

    .line 2687593
    :cond_5
    iget-object v2, p1, LX/JOT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2687594
    :cond_6
    iget-object v2, p0, LX/JOT;->b:LX/1Pk;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JOT;->b:LX/1Pk;

    iget-object v3, p1, LX/JOT;->b:LX/1Pk;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2687595
    goto :goto_0

    .line 2687596
    :cond_7
    iget-object v2, p1, LX/JOT;->b:LX/1Pk;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
