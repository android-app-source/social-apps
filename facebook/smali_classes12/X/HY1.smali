.class public final LX/HY1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2QQ;


# direct methods
.method public constructor <init>(LX/2QQ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2480025
    iput-object p1, p0, LX/HY1;->b:LX/2QQ;

    iput-object p2, p0, LX/HY1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2480026
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 2480027
    :goto_0
    return-void

    .line 2480028
    :cond_0
    iget-object v0, p0, LX/HY1;->b:LX/2QQ;

    const/4 v1, 0x0

    .line 2480029
    iput-object v1, v0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2480030
    const-string v0, "PlatformWebDialogsManifest"

    const-string v1, "Error when trying to download the manifest"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2480031
    iget-object v0, p0, LX/HY1;->b:LX/2QQ;

    iget-object v0, v0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HXy;

    .line 2480032
    iget-object v2, v0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    const-string p1, "platform_webview_manifest_refresh_failed"

    invoke-static {v2, p1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V

    .line 2480033
    iget-object v2, v0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    invoke-static {v2}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->o(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V

    .line 2480034
    goto :goto_1

    .line 2480035
    :cond_1
    iget-object v0, p0, LX/HY1;->b:LX/2QQ;

    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2480036
    iput-object v1, v0, LX/2QQ;->h:Ljava/util/HashMap;

    .line 2480037
    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 2479954
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    .line 2479955
    iget-object v0, p0, LX/HY1;->b:LX/2QQ;

    .line 2479956
    iput-object v1, v0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2479957
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v2, v0

    .line 2479958
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2479959
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2479960
    :goto_0
    if-nez v0, :cond_1

    .line 2479961
    invoke-virtual {p0, v1}, LX/HY1;->onFailure(Ljava/lang/Throwable;)V

    .line 2479962
    :goto_1
    return-void

    .line 2479963
    :catch_0
    move-exception v0

    .line 2479964
    const-string v3, "PlatformWebDialogsManifest"

    const-string v4, "Unable to parse JSON response: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v0, v1

    goto :goto_0

    .line 2479965
    :cond_1
    iget-object v1, p0, LX/HY1;->b:LX/2QQ;

    .line 2479966
    const/4 v2, 0x0

    .line 2479967
    if-nez v0, :cond_6

    .line 2479968
    :goto_2
    iget-object v0, p0, LX/HY1;->b:LX/2QQ;

    iget-object v1, p0, LX/HY1;->a:Ljava/lang/String;

    const/4 v6, 0x0

    .line 2479969
    invoke-static {v0}, LX/2QQ;->e(LX/2QQ;)V

    .line 2479970
    iget-object v2, v0, LX/2QQ;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 2479971
    iget-object v3, v0, LX/2QQ;->p:LX/2QR;

    .line 2479972
    iget-boolean v5, v3, LX/2QR;->n:Z

    move v3, v5

    .line 2479973
    if-nez v3, :cond_3

    .line 2479974
    iget-object v3, v0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HXy;

    .line 2479975
    invoke-virtual {v3, v2, v6}, LX/HXy;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;Z)V

    goto :goto_4

    .line 2479976
    :cond_3
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2479977
    iget-object v3, v0, LX/2QQ;->i:Ljava/util/List;

    invoke-interface {v3, v6, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    .line 2479978
    :cond_4
    iget-object v3, v0, LX/2QQ;->i:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2479979
    :cond_5
    invoke-static {v0}, LX/2QQ;->f(LX/2QQ;)V

    .line 2479980
    goto :goto_1

    .line 2479981
    :cond_6
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 2479982
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v7

    .line 2479983
    iget-object v3, v1, LX/2QQ;->f:Ljava/util/HashMap;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 2479984
    :try_start_1
    const-string v3, "actions"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    move-object v5, v2

    .line 2479985
    :goto_5
    if-eqz v5, :cond_d

    .line 2479986
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 2479987
    invoke-virtual {v5}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v9

    .line 2479988
    :cond_7
    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2479989
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2479990
    :try_start_2
    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 2479991
    const-string v4, "result_action"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2479992
    const-string v4, "version"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2479993
    const-string v4, "url"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 2479994
    invoke-static {v12}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2479995
    invoke-virtual {v7, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 2479996
    if-nez v3, :cond_e

    .line 2479997
    new-instance v3, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-direct {v3}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;-><init>()V

    move-object v4, v3

    .line 2479998
    :goto_7
    invoke-virtual {v6, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2479999
    invoke-virtual {v4}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->b()Ljava/lang/String;

    move-result-object v13

    .line 2480000
    invoke-virtual {v4, v10}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->b(Ljava/lang/String;)V

    .line 2480001
    invoke-virtual {v4, v11}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->c(Ljava/lang/String;)V

    .line 2480002
    invoke-virtual {v4, v2}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a(Ljava/lang/String;)V

    .line 2480003
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2480004
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2480005
    const/4 v3, 0x0

    invoke-virtual {v12, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    const/16 v10, 0x2f

    if-eq v3, v10, :cond_b

    const/4 v3, 0x1

    .line 2480006
    :goto_8
    iget-object v10, v1, LX/2QQ;->l:Landroid/content/Context;

    const-string p1, "https://m.%s"

    invoke-static {v10, p1}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2480007
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2480008
    if-eqz v3, :cond_8

    .line 2480009
    const/16 v3, 0x2f

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2480010
    :cond_8
    invoke-virtual {p1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2480011
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2480012
    :cond_9
    iget-object v10, v1, LX/2QQ;->u:LX/0W9;

    invoke-virtual {v10}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v11, v10}, LX/2QQ;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2480013
    invoke-static {v13}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    invoke-virtual {v13, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2480014
    invoke-virtual {v4}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2480015
    :cond_a
    invoke-virtual {v4, v3}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_6

    .line 2480016
    :catch_1
    move-exception v3

    .line 2480017
    iget-object v4, v1, LX/2QQ;->r:LX/03V;

    const-string v10, "PlatformWebDialogsManifest"

    const-string v11, "Manifest for \'%s\' was missing required keys or was badly formed"

    invoke-static {v11, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v10, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_6

    :catch_2
    move-object v5, v2

    goto/16 :goto_5

    .line 2480018
    :cond_b
    const/4 v3, 0x0

    goto :goto_8

    .line 2480019
    :cond_c
    iget-object v2, v1, LX/2QQ;->p:LX/2QR;

    .line 2480020
    iget-boolean v3, v2, LX/2QR;->n:Z

    move v3, v3

    .line 2480021
    if-nez v3, :cond_f

    .line 2480022
    :cond_d
    :goto_9
    iput-object v6, v1, LX/2QQ;->f:Ljava/util/HashMap;

    .line 2480023
    invoke-static {v1}, LX/2QQ;->g$redex0(LX/2QQ;)V

    goto/16 :goto_2

    :cond_e
    move-object v4, v3

    goto/16 :goto_7

    .line 2480024
    :cond_f
    iget-object v3, v2, LX/2QR;->b:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;

    invoke-direct {v4, v2, v8}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;-><init>(LX/2QR;Ljava/util/Map;)V

    const v5, -0x7208a0f9

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_9
.end method
