.class public LX/HNM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field public static final b:I


# instance fields
.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CXj;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HQ9;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/content/Context;

.field public final g:Landroid/os/ParcelUuid;

.field public h:LX/6WJ;

.field public i:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2458377
    const v0, 0x7f020a0b

    sput v0, LX/HNM;->a:I

    .line 2458378
    const v0, 0x7f08367c

    sput v0, LX/HNM;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;Landroid/os/ParcelUuid;)V
    .locals 0
    .param p5    # Landroid/os/ParcelUuid;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CXj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HQ9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Landroid/content/Context;",
            "Landroid/os/ParcelUuid;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458380
    iput-object p1, p0, LX/HNM;->c:LX/0Ot;

    .line 2458381
    iput-object p2, p0, LX/HNM;->d:LX/0Ot;

    .line 2458382
    iput-object p3, p0, LX/HNM;->e:LX/0Ot;

    .line 2458383
    iput-object p4, p0, LX/HNM;->f:Landroid/content/Context;

    .line 2458384
    iput-object p5, p0, LX/HNM;->g:Landroid/os/ParcelUuid;

    .line 2458385
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2458386
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->DELETE_TAB:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    sget v2, LX/HNM;->b:I

    sget v3, LX/HNM;->a:I

    iget-object v5, p0, LX/HNM;->j:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    move v5, v4

    :goto_0
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2458387
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->DELETE_TAB:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    sget v2, LX/HNM;->b:I

    sget v3, LX/HNM;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2458388
    iget-object v0, p0, LX/HNM;->i:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HNM;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2458389
    :cond_0
    :goto_0
    return-void

    .line 2458390
    :cond_1
    iget-object v0, p0, LX/HNM;->h:LX/6WJ;

    if-nez v0, :cond_2

    .line 2458391
    new-instance v0, LX/6WI;

    iget-object v1, p0, LX/HNM;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/6WI;-><init>(Landroid/content/Context;)V

    sget v1, LX/HNM;->b:I

    invoke-virtual {v0, v1}, LX/6WI;->a(I)LX/6WI;

    move-result-object v0

    const v1, 0x7f083680

    invoke-virtual {v0, v1}, LX/6WI;->b(I)LX/6WI;

    move-result-object v0

    const v1, 0x7f083684

    new-instance v2, LX/HNK;

    invoke-direct {v2, p0}, LX/HNK;-><init>(LX/HNM;)V

    invoke-virtual {v0, v1, v2}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/HNJ;

    invoke-direct {v2, p0}, LX/HNJ;-><init>(LX/HNM;)V

    invoke-virtual {v0, v1, v2}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    invoke-virtual {v0}, LX/6WI;->a()LX/6WJ;

    move-result-object v0

    iput-object v0, p0, LX/HNM;->h:LX/6WJ;

    .line 2458392
    :cond_2
    iget-object v0, p0, LX/HNM;->h:LX/6WJ;

    invoke-virtual {v0}, LX/6WJ;->show()V

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2458393
    const/4 v0, 0x0

    return-object v0
.end method
