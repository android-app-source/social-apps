.class public LX/IDH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/83X;


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:I

.field private f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field private g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final i:Z

.field public j:I


# direct methods
.method public constructor <init>(Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2550550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2550551
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2550552
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, LX/IDH;->a:J

    .line 2550553
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, LX/IDH;->b:Ljava/lang/String;

    .line 2550554
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IDH;->c:Ljava/lang/String;

    .line 2550555
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move v0, v2

    :goto_1
    iput v0, p0, LX/IDH;->e:I

    .line 2550556
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p0, LX/IDH;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2550557
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p0, LX/IDH;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2550558
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p0, LX/IDH;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2550559
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->l()Z

    move-result v0

    iput-boolean v0, p0, LX/IDH;->i:Z

    .line 2550560
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->r()I

    move-result v0

    iput v0, p0, LX/IDH;->j:I

    .line 2550561
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->p()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    move-result-object v0

    .line 2550562
    if-nez v0, :cond_2

    .line 2550563
    iput-object v1, p0, LX/IDH;->d:Ljava/lang/String;

    .line 2550564
    :goto_2
    return-void

    .line 2550565
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2550566
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->j(II)I

    move-result v0

    goto :goto_1

    .line 2550567
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/16z;->a(Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IDH;->d:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2550549
    iget-wide v0, p0, LX/IDH;->a:J

    return-wide v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 2550538
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2550548
    iget-object v0, p0, LX/IDH;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 1

    .prologue
    .line 2550545
    iget-object v0, p0, LX/IDH;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/IDH;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2550546
    iput-object p1, p0, LX/IDH;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2550547
    return-void
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2550544
    iget-object v0, p0, LX/IDH;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2550543
    iget-object v0, p0, LX/IDH;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2550542
    iget v0, p0, LX/IDH;->e:I

    return v0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2550541
    iget-object v0, p0, LX/IDH;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final g()LX/2h7;
    .locals 1

    .prologue
    .line 2550540
    sget-object v0, LX/2h7;->FRIENDS_TAB:LX/2h7;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2550539
    const/4 v0, 0x0

    return-object v0
.end method
