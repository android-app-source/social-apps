.class public LX/Hwr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/DBC;

.field private final c:LX/0aG;

.field private final d:LX/BiW;

.field private final e:LX/1nQ;

.field private final f:LX/0hx;

.field private final g:LX/0So;

.field private final h:LX/1Ck;

.field private final i:LX/0kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2521464
    const-class v0, LX/Hwr;

    sput-object v0, LX/Hwr;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/DBC;LX/0aG;LX/BiW;LX/1nQ;LX/0hx;LX/0So;LX/1Ck;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2521465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2521466
    iput-object p1, p0, LX/Hwr;->b:LX/DBC;

    .line 2521467
    iput-object p2, p0, LX/Hwr;->c:LX/0aG;

    .line 2521468
    iput-object p3, p0, LX/Hwr;->d:LX/BiW;

    .line 2521469
    iput-object p4, p0, LX/Hwr;->e:LX/1nQ;

    .line 2521470
    iput-object p5, p0, LX/Hwr;->f:LX/0hx;

    .line 2521471
    iput-object p6, p0, LX/Hwr;->g:LX/0So;

    .line 2521472
    iput-object p7, p0, LX/Hwr;->h:LX/1Ck;

    .line 2521473
    iput-object p8, p0, LX/Hwr;->i:LX/0kL;

    .line 2521474
    return-void
.end method
