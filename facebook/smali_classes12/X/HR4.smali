.class public final LX/HR4;
.super LX/HR0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 2464177
    iput-object p1, p0, LX/HR4;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    .line 2464178
    invoke-direct {p0, p2, p3, p4}, LX/HR0;-><init>(Landroid/view/View;II)V

    .line 2464179
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HR4;->b:Z

    .line 2464180
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4

    .prologue
    .line 2464181
    invoke-super {p0, p1, p2}, LX/HR0;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 2464182
    iget-boolean v0, p0, LX/HR4;->b:Z

    if-eqz v0, :cond_0

    .line 2464183
    :goto_0
    return-void

    .line 2464184
    :cond_0
    float-to-double v0, p1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    .line 2464185
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HR4;->b:Z

    .line 2464186
    iget-object v0, p0, LX/HR4;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-static {v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->d(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;)V

    goto :goto_0

    .line 2464187
    :cond_1
    iget-object v0, p0, LX/HR4;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    iget-object v1, p0, LX/HR4;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v0, v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setFadingGradient(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;I)V

    goto :goto_0
.end method
