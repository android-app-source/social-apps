.class public LX/HN9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HN9;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458183
    iput-object p1, p0, LX/HN9;->a:LX/0Zb;

    .line 2458184
    return-void
.end method

.method public static a(LX/0QB;)LX/HN9;
    .locals 4

    .prologue
    .line 2458185
    sget-object v0, LX/HN9;->b:LX/HN9;

    if-nez v0, :cond_1

    .line 2458186
    const-class v1, LX/HN9;

    monitor-enter v1

    .line 2458187
    :try_start_0
    sget-object v0, LX/HN9;->b:LX/HN9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2458188
    if-eqz v2, :cond_0

    .line 2458189
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2458190
    new-instance p0, LX/HN9;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/HN9;-><init>(LX/0Zb;)V

    .line 2458191
    move-object v0, p0

    .line 2458192
    sput-object v0, LX/HN9;->b:LX/HN9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2458193
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2458194
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2458195
    :cond_1
    sget-object v0, LX/HN9;->b:LX/HN9;

    return-object v0

    .line 2458196
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2458197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2458198
    iget-object v0, p0, LX/HN9;->a:LX/0Zb;

    .line 2458199
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "pages_public_view"

    .line 2458200
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2458201
    move-object v1, v1

    .line 2458202
    const-string v2, "page_id"

    invoke-virtual {v1, v2, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v1, v1

    .line 2458203
    const-string v2, "tab_type"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2458204
    return-void
.end method
