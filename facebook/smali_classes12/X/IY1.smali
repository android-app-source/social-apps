.class public final LX/IY1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2587142
    iput-object p1, p0, LX/IY1;->b:Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    iput-object p2, p0, LX/IY1;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    .prologue
    .line 2587143
    iget-object v0, p0, LX/IY1;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2587144
    iget-object v0, p0, LX/IY1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget v1, LX/CoL;->M:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2587145
    return-void
.end method
