.class public final LX/JIJ;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/JIJ;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JIH;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/JIK;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2677766
    const/4 v0, 0x0

    sput-object v0, LX/JIJ;->a:LX/JIJ;

    .line 2677767
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JIJ;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2677780
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2677781
    new-instance v0, LX/JIK;

    invoke-direct {v0}, LX/JIK;-><init>()V

    iput-object v0, p0, LX/JIJ;->c:LX/JIK;

    .line 2677782
    return-void
.end method

.method public static declared-synchronized q()LX/JIJ;
    .locals 2

    .prologue
    .line 2677776
    const-class v1, LX/JIJ;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/JIJ;->a:LX/JIJ;

    if-nez v0, :cond_0

    .line 2677777
    new-instance v0, LX/JIJ;

    invoke-direct {v0}, LX/JIJ;-><init>()V

    sput-object v0, LX/JIJ;->a:LX/JIJ;

    .line 2677778
    :cond_0
    sget-object v0, LX/JIJ;->a:LX/JIJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2677779
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2677774
    invoke-static {}, LX/1dS;->b()V

    .line 2677775
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2677783
    new-instance v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    invoke-direct {v0, p1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;-><init>(Landroid/content/Context;)V

    .line 2677784
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p0, 0x7f021756

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2677785
    move-object v0, v0

    .line 2677786
    return-object v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 2677770
    check-cast p3, LX/JII;

    .line 2677771
    check-cast p2, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    iget-object v0, p3, LX/JII;->a:LX/1aZ;

    .line 2677772
    invoke-virtual {p2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2677773
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2677769
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2677768
    const/16 v0, 0xf

    return v0
.end method
