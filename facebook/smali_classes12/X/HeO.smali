.class public LX/HeO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/view/inputmethod/InputMethodManager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Landroid/view/WindowManager;

.field private d:Z

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/view/WindowManager;)V
    .locals 1

    .prologue
    .line 2490250
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, LX/HeO;-><init>(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/view/inputmethod/InputMethodManager;Landroid/view/WindowManager;)V

    .line 2490251
    return-void
.end method

.method private constructor <init>(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/view/inputmethod/InputMethodManager;Landroid/view/WindowManager;)V
    .locals 1

    .prologue
    .line 2490238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2490239
    iput-object p1, p0, LX/HeO;->a:Landroid/view/View;

    .line 2490240
    iput-object p3, p0, LX/HeO;->b:Landroid/view/inputmethod/InputMethodManager;

    .line 2490241
    iput-object p4, p0, LX/HeO;->c:Landroid/view/WindowManager;

    .line 2490242
    iget-object v0, p0, LX/HeO;->a:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2490243
    const/4 v0, 0x1

    .line 2490244
    invoke-static {p0}, LX/HeO;->c(LX/HeO;)V

    .line 2490245
    const/16 p1, 0x200

    invoke-static {p0, p1, v0}, LX/HeO;->a(LX/HeO;IZ)V

    .line 2490246
    if-eqz v0, :cond_0

    const/16 p1, 0x2710

    :goto_0
    invoke-static {p0, p1}, LX/HeO;->e(LX/HeO;I)V

    .line 2490247
    invoke-static {p0}, LX/HeO;->d(LX/HeO;)V

    .line 2490248
    return-void

    .line 2490249
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public static a(LX/HeO;IZ)V
    .locals 3

    .prologue
    .line 2490186
    invoke-static {p0}, LX/HeO;->f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2490187
    if-eqz p2, :cond_1

    iget v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/2addr v0, p1

    .line 2490188
    :goto_0
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    if-eq v2, v0, :cond_0

    .line 2490189
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2490190
    invoke-static {p0}, LX/HeO;->g(LX/HeO;)V

    .line 2490191
    :cond_0
    return-void

    .line 2490192
    :cond_1
    iget v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v0, v2

    goto :goto_0
.end method

.method public static c(LX/HeO;)V
    .locals 1

    .prologue
    .line 2490234
    iget v0, p0, LX/HeO;->e:I

    if-nez v0, :cond_0

    .line 2490235
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HeO;->f:Z

    .line 2490236
    :cond_0
    iget v0, p0, LX/HeO;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/HeO;->e:I

    .line 2490237
    return-void
.end method

.method public static d(LX/HeO;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2490229
    iget v0, p0, LX/HeO;->e:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/HeO;->e:I

    .line 2490230
    iget v0, p0, LX/HeO;->e:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/HeO;->f:Z

    if-eqz v0, :cond_0

    .line 2490231
    invoke-static {p0}, LX/HeO;->g(LX/HeO;)V

    .line 2490232
    iput-boolean v1, p0, LX/HeO;->f:Z

    .line 2490233
    :cond_0
    return-void
.end method

.method public static e(LX/HeO;I)V
    .locals 2

    .prologue
    .line 2490224
    invoke-static {p0}, LX/HeO;->f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 2490225
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    if-eq v1, p1, :cond_0

    .line 2490226
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2490227
    invoke-static {p0}, LX/HeO;->g(LX/HeO;)V

    .line 2490228
    :cond_0
    return-void
.end method

.method public static f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;
    .locals 1

    .prologue
    .line 2490223
    iget-object v0, p0, LX/HeO;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method public static g(LX/HeO;)V
    .locals 3

    .prologue
    .line 2490218
    iget v0, p0, LX/HeO;->e:I

    if-lez v0, :cond_1

    .line 2490219
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HeO;->f:Z

    .line 2490220
    :cond_0
    :goto_0
    return-void

    .line 2490221
    :cond_1
    iget-boolean v0, p0, LX/HeO;->d:Z

    if-eqz v0, :cond_0

    .line 2490222
    iget-object v0, p0, LX/HeO;->c:Landroid/view/WindowManager;

    iget-object v1, p0, LX/HeO;->a:Landroid/view/View;

    invoke-static {p0}, LX/HeO;->f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2490213
    iget-boolean v0, p0, LX/HeO;->d:Z

    if-eqz v0, :cond_0

    .line 2490214
    :goto_0
    return-void

    .line 2490215
    :cond_0
    iget-object v0, p0, LX/HeO;->c:Landroid/view/WindowManager;

    iget-object v1, p0, LX/HeO;->a:Landroid/view/View;

    invoke-static {p0}, LX/HeO;->f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2490216
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HeO;->d:Z

    .line 2490217
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/ui/appoverlay/AppOverlayWindow$1;

    invoke-direct {v1, p0}, Lcom/facebook/ui/appoverlay/AppOverlayWindow$1;-><init>(LX/HeO;)V

    const v2, 0x339416e4

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2490208
    invoke-static {p0}, LX/HeO;->c(LX/HeO;)V

    .line 2490209
    const/16 v0, 0x200

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/HeO;->a(LX/HeO;IZ)V

    .line 2490210
    invoke-static {p0, p1}, LX/HeO;->e(LX/HeO;I)V

    .line 2490211
    invoke-static {p0}, LX/HeO;->d(LX/HeO;)V

    .line 2490212
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2490198
    iget-boolean v0, p0, LX/HeO;->d:Z

    if-nez v0, :cond_0

    .line 2490199
    :goto_0
    return-void

    .line 2490200
    :cond_0
    const/16 v0, 0x8

    .line 2490201
    invoke-static {p0}, LX/HeO;->f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v1, v0

    if-ne v1, v0, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2490202
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2490203
    if-eqz v0, :cond_1

    .line 2490204
    iget-object v0, p0, LX/HeO;->b:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_1

    .line 2490205
    iget-object v0, p0, LX/HeO;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/HeO;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2490206
    :cond_1
    iget-object v0, p0, LX/HeO;->c:Landroid/view/WindowManager;

    iget-object v1, p0, LX/HeO;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 2490207
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HeO;->d:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2490193
    invoke-static {p0}, LX/HeO;->f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 2490194
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    if-eq v1, p1, :cond_0

    .line 2490195
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2490196
    invoke-static {p0}, LX/HeO;->g(LX/HeO;)V

    .line 2490197
    :cond_0
    return-void
.end method
