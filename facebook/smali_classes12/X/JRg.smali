.class public final LX/JRg;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JRh;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pm;

.field public c:I

.field public final synthetic d:LX/JRh;


# direct methods
.method public constructor <init>(LX/JRh;)V
    .locals 1

    .prologue
    .line 2693600
    iput-object p1, p0, LX/JRg;->d:LX/JRh;

    .line 2693601
    move-object v0, p1

    .line 2693602
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2693603
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2693604
    const-string v0, "MultiShareProductItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2693605
    if-ne p0, p1, :cond_1

    .line 2693606
    :cond_0
    :goto_0
    return v0

    .line 2693607
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2693608
    goto :goto_0

    .line 2693609
    :cond_3
    check-cast p1, LX/JRg;

    .line 2693610
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2693611
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2693612
    if-eq v2, v3, :cond_0

    .line 2693613
    iget-object v2, p0, LX/JRg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JRg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JRg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2693614
    goto :goto_0

    .line 2693615
    :cond_5
    iget-object v2, p1, LX/JRg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2693616
    :cond_6
    iget-object v2, p0, LX/JRg;->b:LX/1Pm;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JRg;->b:LX/1Pm;

    iget-object v3, p1, LX/JRg;->b:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2693617
    goto :goto_0

    .line 2693618
    :cond_8
    iget-object v2, p1, LX/JRg;->b:LX/1Pm;

    if-nez v2, :cond_7

    .line 2693619
    :cond_9
    iget v2, p0, LX/JRg;->c:I

    iget v3, p1, LX/JRg;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2693620
    goto :goto_0
.end method
