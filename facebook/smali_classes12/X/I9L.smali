.class public LX/I9L;
.super LX/I9K;
.source ""


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field public e:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

.field private f:Ljava/lang/String;

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Lcom/facebook/events/common/EventActionContext;

.field public j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Blc;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/2do;

.field public m:LX/I9I;


# direct methods
.method public constructor <init>(LX/2do;Landroid/content/Context;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/Blc;Z)V
    .locals 2
    .param p3    # Lcom/facebook/events/common/EventActionContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Blc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2542833
    invoke-direct {p0, p2}, LX/I9K;-><init>(Landroid/content/Context;)V

    .line 2542834
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I9L;->j:Ljava/util/Map;

    .line 2542835
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/I9L;->k:Ljava/util/Map;

    .line 2542836
    new-instance v0, LX/I9I;

    invoke-direct {v0, p0}, LX/I9I;-><init>(LX/I9L;)V

    iput-object v0, p0, LX/I9L;->m:LX/I9I;

    .line 2542837
    iput-object p1, p0, LX/I9L;->l:LX/2do;

    .line 2542838
    iget-object v0, p0, LX/I9L;->l:LX/2do;

    iget-object v1, p0, LX/I9L;->m:LX/I9I;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2542839
    iput-object p2, p0, LX/I9L;->c:Landroid/content/Context;

    .line 2542840
    iput-object p3, p0, LX/I9L;->i:Lcom/facebook/events/common/EventActionContext;

    .line 2542841
    iget-object v0, p4, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2542842
    iput-object v0, p0, LX/I9L;->f:Ljava/lang/String;

    .line 2542843
    iget-object v0, p4, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2542844
    iput-object v0, p0, LX/I9L;->d:Ljava/lang/String;

    .line 2542845
    iget-object v0, p4, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->h:LX/0Px;

    move-object v0, v0

    .line 2542846
    iput-object v0, p0, LX/I9L;->g:LX/0Px;

    .line 2542847
    iput-object p5, p0, LX/I9K;->g:LX/Blc;

    .line 2542848
    iput-boolean p6, p0, LX/I9L;->h:Z

    .line 2542849
    return-void
.end method


# virtual methods
.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 2542808
    invoke-virtual {p0, p1}, LX/I9K;->f(I)I

    move-result v0

    if-ge v0, p2, :cond_0

    .line 2542809
    invoke-virtual {p0, p1, p2}, LX/I9K;->d(II)V

    .line 2542810
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/I9K;->c(II)I

    move-result v0

    .line 2542811
    invoke-static {}, LX/I9J;->values()[LX/I9J;

    move-result-object v1

    aget-object v0, v1, v0

    .line 2542812
    if-nez p4, :cond_3

    .line 2542813
    sget-object v1, LX/I9H;->a:[I

    invoke-virtual {v0}, LX/I9J;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2542814
    const/4 v1, 0x0

    :goto_0
    move-object v9, v1

    .line 2542815
    :goto_1
    sget-object v1, LX/I9J;->CHILD:LX/I9J;

    if-ne v0, v1, :cond_1

    .line 2542816
    invoke-virtual {p0, p1, p2}, LX/I9K;->a(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/EventUser;

    .line 2542817
    iget-object v0, p0, LX/I9L;->j:Ljava/util/Map;

    .line 2542818
    iget-object v1, v2, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2542819
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/I9L;->j:Ljava/util/Map;

    .line 2542820
    iget-object v1, v2, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2542821
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blc;

    move-object v3, v0

    :goto_2
    move-object v0, v9

    .line 2542822
    check-cast v0, LX/IA4;

    iget-object v1, p0, LX/I9L;->f:Ljava/lang/String;

    .line 2542823
    iget-object v4, p0, LX/I9K;->g:LX/Blc;

    move-object v4, v4

    .line 2542824
    iget-object v5, p0, LX/I9L;->i:Lcom/facebook/events/common/EventActionContext;

    iget-boolean v6, p0, LX/I9L;->h:Z

    iget-object v7, p0, LX/I9L;->g:LX/0Px;

    iget-object v8, p0, LX/I9L;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, LX/IA4;->a(Ljava/lang/String;Lcom/facebook/events/model/EventUser;LX/Blc;LX/Blc;Lcom/facebook/events/common/EventActionContext;ZLX/0Px;Ljava/lang/String;)V

    .line 2542825
    :cond_1
    return-object v9

    .line 2542826
    :cond_2
    iget-object v0, p0, LX/I9K;->g:LX/Blc;

    move-object v3, v0

    .line 2542827
    goto :goto_2

    :cond_3
    move-object v9, p4

    goto :goto_1

    .line 2542828
    :pswitch_0
    new-instance v1, LX/IA4;

    iget-object v2, p0, LX/I9L;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/IA4;-><init>(Landroid/content/Context;)V

    .line 2542829
    iget-object v2, p0, LX/I9L;->e:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    .line 2542830
    iput-object v2, v1, LX/IA4;->y:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    .line 2542831
    goto :goto_0

    .line 2542832
    :pswitch_1
    new-instance v1, LX/I9b;

    iget-object v2, p0, LX/I9L;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/I9b;-><init>(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
