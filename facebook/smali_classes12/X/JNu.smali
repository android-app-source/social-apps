.class public LX/JNu;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/3mX",
        "<",
        "LX/JO8;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/JO7;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pb;LX/25M;LX/JO7;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/JO8;",
            ">;TE;",
            "LX/25M;",
            "LX/JO7;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686632
    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2686633
    iput-object p5, p0, LX/JNu;->c:LX/JO7;

    .line 2686634
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2686635
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2686636
    check-cast p2, LX/JO8;

    .line 2686637
    iget-object v0, p2, LX/JO8;->b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 2686638
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->X()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->m()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x285feb

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->V()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2686639
    :cond_1
    iget-object v0, p0, LX/JNu;->c:LX/JO7;

    const/4 v1, 0x0

    .line 2686640
    new-instance v2, LX/JO6;

    invoke-direct {v2, v0}, LX/JO6;-><init>(LX/JO7;)V

    .line 2686641
    iget-object v3, v0, LX/JO7;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JO5;

    .line 2686642
    if-nez v3, :cond_2

    .line 2686643
    new-instance v3, LX/JO5;

    invoke-direct {v3, v0}, LX/JO5;-><init>(LX/JO7;)V

    .line 2686644
    :cond_2
    invoke-static {v3, p1, v1, v1, v2}, LX/JO5;->a$redex0(LX/JO5;LX/1De;IILX/JO6;)V

    .line 2686645
    move-object v2, v3

    .line 2686646
    move-object v1, v2

    .line 2686647
    move-object v0, v1

    .line 2686648
    iget-object v1, p2, LX/JO8;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 2686649
    iget-object v2, v0, LX/JO5;->a:LX/JO6;

    iput-object v1, v2, LX/JO6;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 2686650
    iget-object v2, v0, LX/JO5;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2686651
    move-object v0, v0

    .line 2686652
    iget-object v1, p2, LX/JO8;->b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686653
    iget-object v2, v0, LX/JO5;->a:LX/JO6;

    iput-object v1, v2, LX/JO6;->b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686654
    iget-object v2, v0, LX/JO5;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2686655
    move-object v1, v0

    .line 2686656
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/1Pb;

    .line 2686657
    iget-object v2, v1, LX/JO5;->a:LX/JO6;

    iput-object v0, v2, LX/JO6;->d:LX/1Pb;

    .line 2686658
    iget-object v2, v1, LX/JO5;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2686659
    move-object v0, v1

    .line 2686660
    iget-object v1, p0, LX/3mX;->l:LX/3mj;

    move-object v1, v1

    .line 2686661
    iget-object v2, v0, LX/JO5;->a:LX/JO6;

    iput-object v1, v2, LX/JO6;->c:LX/3mj;

    .line 2686662
    iget-object v2, v0, LX/JO5;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2686663
    move-object v0, v0

    .line 2686664
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2686665
    :goto_0
    return-object v0

    :cond_3
    invoke-static {p1}, LX/JNs;->c(LX/1De;)LX/JNq;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2686666
    const/4 v0, 0x0

    return v0
.end method
