.class public final LX/I0q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/I0X;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2527944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2527945
    check-cast p1, LX/I0X;

    check-cast p2, LX/I0X;

    .line 2527946
    invoke-virtual {p1}, LX/I0X;->a()I

    move-result v0

    .line 2527947
    invoke-virtual {p2}, LX/I0X;->a()I

    move-result v1

    .line 2527948
    invoke-virtual {p1}, LX/I0X;->a()I

    move-result v2

    invoke-virtual {p2}, LX/I0X;->a()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 2527949
    sub-int/2addr v0, v1

    .line 2527950
    :goto_0
    return v0

    .line 2527951
    :cond_0
    iget-object v0, p1, LX/I0X;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527952
    iget-object v1, v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-object v1, v1

    .line 2527953
    iget-object v0, p2, LX/I0X;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527954
    iget-object v2, v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-object v0, v2

    .line 2527955
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v1

    invoke-interface {v1}, LX/7oa;->j()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v0

    invoke-interface {v0}, LX/7oa;->j()J

    move-result-wide v0

    sub-long v0, v2, v0

    long-to-int v0, v0

    goto :goto_0
.end method
