.class public LX/Huj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0iq;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field private a:LX/0wM;

.field private final b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/8Sa;

.field public final f:Landroid/content/Context;

.field public g:LX/0hs;

.field public h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

.field public i:LX/Hqb;


# direct methods
.method public constructor <init>(LX/0wM;LX/0Ot;LX/0il;LX/0zw;LX/Hqb;LX/8Sa;Landroid/content/Context;)V
    .locals 2
    .param p3    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Hqb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0wM;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;TServices;",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;",
            "Lcom/facebook/composer/privacy/controller/FixedPrivacyPillViewController$PillClickedListener;",
            "LX/8Sa;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517861
    iput-object p1, p0, LX/Huj;->a:LX/0wM;

    .line 2517862
    iput-object p2, p0, LX/Huj;->d:LX/0Ot;

    .line 2517863
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Huj;->c:Ljava/lang/ref/WeakReference;

    .line 2517864
    iput-object p4, p0, LX/Huj;->b:LX/0zw;

    .line 2517865
    iput-object p6, p0, LX/Huj;->e:LX/8Sa;

    .line 2517866
    iput-object p7, p0, LX/Huj;->f:Landroid/content/Context;

    .line 2517867
    iput-object p5, p0, LX/Huj;->i:LX/Hqb;

    .line 2517868
    invoke-direct {p0}, LX/Huj;->b()V

    .line 2517869
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 2517870
    iget-object v0, p0, LX/Huj;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517871
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iq;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iq;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-nez v1, :cond_2

    .line 2517872
    :cond_0
    iget-object v0, p0, LX/Huj;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2517873
    iput-object v5, p0, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 2517874
    :cond_1
    :goto_0
    return-void

    .line 2517875
    :cond_2
    iget-object v3, p0, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iq;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eq v3, v1, :cond_1

    .line 2517876
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iput-object v0, p0, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 2517877
    iget-object v0, p0, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2517878
    iget-object v0, p0, LX/Huj;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2517879
    iget-object v1, p0, LX/Huj;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b01b1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablePadding(I)V

    .line 2517880
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2517881
    iget-object v2, p0, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v2, v2, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v3, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v2, v3}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v2

    .line 2517882
    iget-object v1, p0, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 2517883
    iget-object v1, p0, LX/Huj;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-class v3, LX/Huj;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ComposerFixedPrivacyData : Label is null"

    invoke-virtual {v1, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2517884
    :cond_3
    if-nez v2, :cond_4

    .line 2517885
    iget-object v1, p0, LX/Huj;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-class v3, LX/Huj;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ComposerFixedPrivacyData : Drawable Resource is 0"

    invoke-virtual {v1, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2517886
    :cond_4
    iget-object v1, p0, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2517887
    iget-object v1, p0, LX/Huj;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-class v3, LX/Huj;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ComposerFixedPrivacyData : ToolTipText is empty or null"

    invoke-virtual {v1, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2517888
    :cond_5
    iget-object v1, p0, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2517889
    iget-object v1, p0, LX/Huj;->a:LX/0wM;

    const v3, -0x6e685d

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1, v5, v5, v5}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2517890
    new-instance v1, LX/Huh;

    invoke-direct {v1, p0, v0}, LX/Huh;-><init>(LX/Huj;Lcom/facebook/resources/ui/FbTextView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 2517891
    goto/16 :goto_1
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 2517892
    sget-object v0, LX/Hui;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517893
    :goto_0
    return-void

    .line 2517894
    :pswitch_0
    invoke-direct {p0}, LX/Huj;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2517895
    invoke-direct {p0}, LX/Huj;->b()V

    .line 2517896
    return-void
.end method
