.class public LX/J10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6sf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6sf",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/PayOverCounterMethod;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2638142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638143
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)LX/0Px;
    .locals 3

    .prologue
    .line 2638144
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/PayOverCounterMethod;

    .line 2638145
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    const-string v1, "type"

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/cards/PayOverCounterMethod;->b()LX/6zU;

    move-result-object v2

    invoke-virtual {v2}, LX/6zU;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    .line 2638146
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->NMOR_PAYMENT_METHOD:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/6zU;
    .locals 1

    .prologue
    .line 2638147
    sget-object v0, LX/6zU;->PAY_OVER_COUNTER:LX/6zU;

    return-object v0
.end method
