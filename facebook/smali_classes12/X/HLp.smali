.class public final LX/HLp;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/res/Resources;

.field public final synthetic b:Landroid/view/View$OnClickListener;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/HLq;


# direct methods
.method public constructor <init>(LX/HLq;Landroid/content/res/Resources;Landroid/view/View$OnClickListener;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2456413
    iput-object p1, p0, LX/HLp;->d:LX/HLq;

    iput-object p2, p0, LX/HLp;->a:Landroid/content/res/Resources;

    iput-object p3, p0, LX/HLp;->b:Landroid/view/View$OnClickListener;

    iput-object p4, p0, LX/HLp;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2456408
    iget-object v0, p0, LX/HLp;->d:LX/HLq;

    iget-object v0, v0, LX/HLq;->c:LX/0Uh;

    sget v1, LX/8Dm;->m:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2456409
    if-eqz v0, :cond_0

    .line 2456410
    iget-object v0, p0, LX/HLp;->b:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2456411
    :goto_0
    return-void

    .line 2456412
    :cond_0
    iget-object v0, p0, LX/HLp;->d:LX/HLq;

    iget-object v0, v0, LX/HLq;->d:Lcom/facebook/content/SecureContextHelper;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://www.instagram.com/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/HLp;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2456414
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2456415
    iget-object v0, p0, LX/HLp;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2456416
    return-void
.end method
