.class public LX/J0H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2636771
    const-class v0, LX/J0H;

    sput-object v0, LX/J0H;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636773
    return-void
.end method

.method public static a(LX/0QB;)LX/J0H;
    .locals 1

    .prologue
    .line 2636774
    new-instance v0, LX/J0H;

    invoke-direct {v0}, LX/J0H;-><init>()V

    .line 2636775
    move-object v0, v0

    .line 2636776
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2636777
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;

    .line 2636778
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2636779
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "preset_credential_id"

    .line 2636780
    iget-wide v6, p1, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->b:J

    move-wide v4, v6

    .line 2636781
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636782
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "p2p_credit_cards"

    .line 2636783
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2636784
    move-object v1, v1

    .line 2636785
    const-string v2, "POST"

    .line 2636786
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2636787
    move-object v1, v1

    .line 2636788
    const-string v2, "%d/p2p_settings"

    .line 2636789
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2636790
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2636791
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2636792
    move-object v1, v1

    .line 2636793
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2636794
    move-object v0, v1

    .line 2636795
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2636796
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2636797
    move-object v0, v0

    .line 2636798
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2636799
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2636800
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
