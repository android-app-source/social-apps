.class public final LX/IOn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IOo;


# direct methods
.method public constructor <init>(LX/IOo;)V
    .locals 0

    .prologue
    .line 2572962
    iput-object p1, p0, LX/IOn;->a:LX/IOo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x829954f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2572963
    iget-object v1, p0, LX/IOn;->a:LX/IOo;

    iget-object v1, v1, LX/IOo;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->f:LX/0ad;

    sget-short v2, LX/IOa;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2572964
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2572965
    const-string v2, "group_members"

    iget-object v3, p0, LX/IOn;->a:LX/IOo;

    iget-object v3, v3, LX/IOo;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v3, v3, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2572966
    iget-object v2, p0, LX/IOn;->a:LX/IOo;

    iget-object v2, v2, LX/IOo;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->e:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/0ax;->N:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2572967
    :goto_0
    const v1, 0x5c960595

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2572968
    :cond_0
    const-string v1, "https://m.facebook.com/groups/create/?addees=%s"

    iget-object v2, p0, LX/IOn;->a:LX/IOo;

    iget-object v2, v2, LX/IOo;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2572969
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2572970
    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2572971
    iget-object v1, p0, LX/IOn;->a:LX/IOo;

    iget-object v1, v1, LX/IOo;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
