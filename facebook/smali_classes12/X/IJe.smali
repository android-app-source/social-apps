.class public final LX/IJe;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;)V
    .locals 0

    .prologue
    .line 2563984
    iput-object p1, p0, LX/IJe;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2563985
    iget-object v0, p0, LX/IJe;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->a:LX/17W;

    iget-object v1, p0, LX/IJe;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    invoke-virtual {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2563986
    iget-object v0, p0, LX/IJe;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->b:LX/IID;

    .line 2563987
    const-string v1, "friends_nearby_dashboard_pause_learn_more_click"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2563988
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2563989
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2563990
    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2563991
    return-void
.end method
