.class public final LX/IIA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingLocationFeedbackMutationsModels$CrowdsourcingUnknownRegionClaimModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IIB;


# direct methods
.method public constructor <init>(LX/IIB;)V
    .locals 0

    .prologue
    .line 2561418
    iput-object p1, p0, LX/IIA;->a:LX/IIB;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2561419
    iget-object v0, p0, LX/IIA;->a:LX/IIB;

    iget-object v0, v0, LX/IIB;->c:LX/03V;

    const-string v1, "nearby_friends_self_view_unknown_region_feedback_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2561420
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2561421
    return-void
.end method
