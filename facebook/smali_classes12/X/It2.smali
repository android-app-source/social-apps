.class public LX/It2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0So;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/send/PendingSendQueueKey;",
            "LX/It3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 1

    .prologue
    .line 2621858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2621859
    iput-object p1, p0, LX/It2;->a:LX/0So;

    .line 2621860
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/It2;->b:Ljava/util/Map;

    .line 2621861
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;
    .locals 1

    .prologue
    .line 2621862
    iget-object v0, p0, LX/It2;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It3;

    return-object v0
.end method

.method public final a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/It3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2621863
    iget-object v0, p0, LX/It2;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;
    .locals 1

    .prologue
    .line 2621864
    iget-object v0, p0, LX/It2;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It3;

    return-object v0
.end method

.method public final c(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;
    .locals 2

    .prologue
    .line 2621865
    iget-object v0, p0, LX/It2;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It3;

    .line 2621866
    if-nez v0, :cond_0

    .line 2621867
    new-instance v0, LX/It3;

    iget-object v1, p0, LX/It2;->a:LX/0So;

    invoke-direct {v0, v1, p1}, LX/It3;-><init>(LX/0So;Lcom/facebook/messaging/model/send/PendingSendQueueKey;)V

    .line 2621868
    iget-object v1, p0, LX/It2;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2621869
    :cond_0
    return-object v0
.end method
