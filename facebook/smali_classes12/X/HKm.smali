.class public LX/HKm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1vg;


# direct methods
.method public constructor <init>(LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2455046
    iput-object p1, p0, LX/HKm;->a:LX/1vg;

    .line 2455047
    return-void
.end method

.method public static a(LX/0QB;)LX/HKm;
    .locals 4

    .prologue
    .line 2455048
    const-class v1, LX/HKm;

    monitor-enter v1

    .line 2455049
    :try_start_0
    sget-object v0, LX/HKm;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2455050
    sput-object v2, LX/HKm;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2455051
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455052
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2455053
    new-instance p0, LX/HKm;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-direct {p0, v3}, LX/HKm;-><init>(LX/1vg;)V

    .line 2455054
    move-object v0, p0

    .line 2455055
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2455056
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2455057
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2455058
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLX/0Px;)LX/1Dg;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # LX/0Px;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "LX/0Px",
            "<",
            "LX/HM9;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2455059
    if-eqz p6, :cond_5

    .line 2455060
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0058

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010c

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    .line 2455061
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010d

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    .line 2455062
    const-string v1, "-"

    invoke-virtual {p4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    .line 2455063
    if-eqz v2, :cond_0

    .line 2455064
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    .line 2455065
    :cond_0
    if-eqz p7, :cond_2

    if-nez v2, :cond_3

    const v1, 0x7f0a00d4

    move v3, v1

    .line 2455066
    :goto_0
    if-eqz v2, :cond_4

    const v1, 0x7f020a12

    .line 2455067
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    .line 2455068
    if-eqz p8, :cond_1

    .line 2455069
    new-instance v6, LX/HMA;

    move-object/from16 v0, p8

    invoke-direct {v6, p1, v0}, LX/HMA;-><init>(LX/1De;LX/0Px;)V

    .line 2455070
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v6

    invoke-virtual {v6}, LX/1n6;->b()LX/1dc;

    move-result-object v6

    invoke-interface {v2, v6}, LX/1Di;->a(LX/1dc;)LX/1Di;

    move-result-object v2

    const v6, 0x7f0b2321

    invoke-interface {v2, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v6, 0x7f0b2322

    invoke-interface {v2, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    .line 2455071
    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-static {p1, p5}, LX/HKl;->a(LX/1De;Ljava/lang/String;)LX/1dQ;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    iget-object v8, p0, LX/HKm;->a:LX/1vg;

    invoke-virtual {v8, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v8

    invoke-virtual {v8, v1}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/2xv;->j(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v1

    invoke-interface {v6, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v6, 0x7f0b0050

    invoke-virtual {v3, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    .line 2455072
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const v6, 0x7f0213ab

    invoke-interface {v3, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x2

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/16 v6, 0x8

    const v7, 0x7f0b0060

    invoke-interface {v3, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x3

    invoke-interface {v3, v6}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x2

    invoke-interface {v3, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x3

    invoke-interface {v7, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v6, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 2455073
    :goto_2
    return-object v1

    .line 2455074
    :cond_2
    if-eqz v2, :cond_3

    const v1, 0x7f0a00d4

    move v3, v1

    goto/16 :goto_0

    :cond_3
    const v1, 0x7f0a00d3

    move v3, v1

    goto/16 :goto_0

    .line 2455075
    :cond_4
    const v1, 0x7f020a17

    goto/16 :goto_1

    .line 2455076
    :cond_5
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0213ab

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/16 v2, 0x8

    const v3, 0x7f0b0060

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0836a4

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a010e

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_2
.end method
