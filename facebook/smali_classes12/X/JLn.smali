.class public LX/JLn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2681554
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 7

    .prologue
    .line 2681555
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2681556
    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2681557
    const-string v2, "route_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2681558
    const-string v3, "react_search_module"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2681559
    const-string v4, "init_props"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 2681560
    const-string v5, "requested_orientation"

    const/4 v6, -0x1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2681561
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v5

    .line 2681562
    iput-object v3, v5, LX/98r;->n:Ljava/lang/String;

    .line 2681563
    move-object v3, v5

    .line 2681564
    iput-object v1, v3, LX/98r;->a:Ljava/lang/String;

    .line 2681565
    move-object v1, v3

    .line 2681566
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2681567
    move-object v1, v1

    .line 2681568
    iput v0, v1, LX/98r;->h:I

    .line 2681569
    move-object v0, v1

    .line 2681570
    iput-object v4, v0, LX/98r;->f:Landroid/os/Bundle;

    .line 2681571
    move-object v0, v0

    .line 2681572
    const/4 v1, 0x1

    .line 2681573
    iput-boolean v1, v0, LX/98r;->m:Z

    .line 2681574
    move-object v0, v0

    .line 2681575
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 2681576
    new-instance v1, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;

    invoke-direct {v1}, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;-><init>()V

    .line 2681577
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2681578
    move-object v0, v1

    .line 2681579
    return-object v0
.end method
