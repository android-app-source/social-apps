.class public final enum LX/HjN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HjN;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AD_REQUEST_FAILED:LX/HjN;

.field public static final enum AD_REQUEST_TIMEOUT:LX/HjN;

.field public static final enum DISABLED_APP:LX/HjN;

.field public static final enum ERROR_MESSAGE:LX/HjN;

.field public static final enum INTERNAL_ERROR:LX/HjN;

.field public static final enum LOAD_TOO_FREQUENTLY:LX/HjN;

.field public static final enum NETWORK_ERROR:LX/HjN;

.field public static final enum NO_AD_PLACEMENT:LX/HjN;

.field public static final enum NO_FILL:LX/HjN;

.field public static final enum PARSER_FAILURE:LX/HjN;

.field public static final enum SERVER_ERROR:LX/HjN;

.field public static final enum START_BEFORE_INIT:LX/HjN;

.field public static final enum UNKNOWN_ERROR:LX/HjN;

.field public static final enum UNKNOWN_RESPONSE:LX/HjN;

.field private static final synthetic d:[LX/HjN;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v9, 0x1

    new-instance v0, LX/HjN;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v3, -0x1

    const-string v4, "unknown error"

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v0, LX/HjN;->UNKNOWN_ERROR:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "NETWORK_ERROR"

    const/16 v6, 0x3e8

    const-string v7, "Network Error"

    move v5, v9

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->NETWORK_ERROR:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "NO_FILL"

    const/16 v6, 0x3e9

    const-string v7, "No Fill"

    move v5, v10

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->NO_FILL:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "LOAD_TOO_FREQUENTLY"

    const/16 v6, 0x3ea

    const-string v7, "Ad was re-loaded too frequently"

    move v5, v11

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->LOAD_TOO_FREQUENTLY:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "DISABLED_APP"

    const/16 v6, 0x3ed

    const-string v7, "App is disabled from making ad requests"

    move v5, v12

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->DISABLED_APP:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "SERVER_ERROR"

    const/4 v5, 0x5

    const/16 v6, 0x7d0

    const-string v7, "Server Error"

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->SERVER_ERROR:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "INTERNAL_ERROR"

    const/4 v5, 0x6

    const/16 v6, 0x7d1

    const-string v7, "Internal Error"

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->INTERNAL_ERROR:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "START_BEFORE_INIT"

    const/4 v5, 0x7

    const/16 v6, 0x7d4

    const-string v7, "initAd must be called before startAd"

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->START_BEFORE_INIT:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "AD_REQUEST_FAILED"

    const/16 v5, 0x8

    const/16 v6, 0x457

    const-string v7, "Facebook Ads SDK request for ads failed"

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->AD_REQUEST_FAILED:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "AD_REQUEST_TIMEOUT"

    const/16 v5, 0x9

    const/16 v6, 0x458

    const-string v7, "Facebook Ads SDK request for ads timed out"

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->AD_REQUEST_TIMEOUT:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "PARSER_FAILURE"

    const/16 v5, 0xa

    const/16 v6, 0x4b1

    const-string v7, "Failed to parse Facebook Ads SDK delivery response"

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->PARSER_FAILURE:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "UNKNOWN_RESPONSE"

    const/16 v5, 0xb

    const/16 v6, 0x4b2

    const-string v7, "Unknown Facebook Ads SDK delivery response type"

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->UNKNOWN_RESPONSE:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "ERROR_MESSAGE"

    const/16 v5, 0xc

    const/16 v6, 0x4b3

    const-string v7, "Facebook Ads SDK delivery response Error message"

    move v8, v9

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->ERROR_MESSAGE:LX/HjN;

    new-instance v3, LX/HjN;

    const-string v4, "NO_AD_PLACEMENT"

    const/16 v5, 0xd

    const/16 v6, 0x516

    const-string v7, "Facebook Ads SDK returned no ad placements"

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/HjN;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v3, LX/HjN;->NO_AD_PLACEMENT:LX/HjN;

    const/16 v0, 0xe

    new-array v0, v0, [LX/HjN;

    sget-object v1, LX/HjN;->UNKNOWN_ERROR:LX/HjN;

    aput-object v1, v0, v2

    sget-object v1, LX/HjN;->NETWORK_ERROR:LX/HjN;

    aput-object v1, v0, v9

    sget-object v1, LX/HjN;->NO_FILL:LX/HjN;

    aput-object v1, v0, v10

    sget-object v1, LX/HjN;->LOAD_TOO_FREQUENTLY:LX/HjN;

    aput-object v1, v0, v11

    sget-object v1, LX/HjN;->DISABLED_APP:LX/HjN;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, LX/HjN;->SERVER_ERROR:LX/HjN;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/HjN;->INTERNAL_ERROR:LX/HjN;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/HjN;->START_BEFORE_INIT:LX/HjN;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/HjN;->AD_REQUEST_FAILED:LX/HjN;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/HjN;->AD_REQUEST_TIMEOUT:LX/HjN;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/HjN;->PARSER_FAILURE:LX/HjN;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/HjN;->UNKNOWN_RESPONSE:LX/HjN;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/HjN;->ERROR_MESSAGE:LX/HjN;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/HjN;->NO_AD_PLACEMENT:LX/HjN;

    aput-object v2, v0, v1

    sput-object v0, LX/HjN;->d:[LX/HjN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/HjN;->a:I

    iput-object p4, p0, LX/HjN;->b:Ljava/lang/String;

    iput-boolean p5, p0, LX/HjN;->c:Z

    return-void
.end method

.method public static adErrorTypeFromCode(I)LX/HjN;
    .locals 1

    sget-object v0, LX/HjN;->UNKNOWN_ERROR:LX/HjN;

    invoke-static {p0, v0}, LX/HjN;->adErrorTypeFromCode(ILX/HjN;)LX/HjN;

    move-result-object v0

    return-object v0
.end method

.method public static adErrorTypeFromCode(ILX/HjN;)LX/HjN;
    .locals 5

    invoke-static {}, LX/HjN;->values()[LX/HjN;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    invoke-virtual {v0}, LX/HjN;->getErrorCode()I

    move-result v4

    if-ne v4, p0, :cond_1

    move-object p1, v0

    :cond_0
    return-object p1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/HjN;
    .locals 1

    const-class v0, LX/HjN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HjN;

    return-object v0
.end method

.method public static values()[LX/HjN;
    .locals 1

    sget-object v0, LX/HjN;->d:[LX/HjN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HjN;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, LX/HjN;->c:Z

    return v0
.end method

.method public final getAdError(Ljava/lang/String;)LX/Hj0;
    .locals 1

    new-instance v0, LX/Hjr;

    invoke-direct {v0, p0, p1}, LX/Hjr;-><init>(LX/HjN;Ljava/lang/String;)V

    invoke-virtual {v0}, LX/Hjr;->b()LX/Hj0;

    move-result-object v0

    return-object v0
.end method

.method public final getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;
    .locals 1

    new-instance v0, LX/Hjr;

    invoke-direct {v0, p0, p1}, LX/Hjr;-><init>(LX/HjN;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getDefaultErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LX/HjN;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final getErrorCode()I
    .locals 1

    iget v0, p0, LX/HjN;->a:I

    return v0
.end method
