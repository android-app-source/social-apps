.class public LX/IXf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2586391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Clo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentSlide;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;",
            ")",
            "Lcom/facebook/richdocument/model/block/v2/RichDocumentBlocks;"
        }
    .end annotation

    .prologue
    .line 2586392
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2586393
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;

    .line 2586394
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2586395
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2586396
    :cond_0
    new-instance v0, LX/IXc;

    invoke-direct {v0, p1, p2}, LX/IXc;-><init>(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)V

    .line 2586397
    invoke-virtual {v0, v2}, LX/IXc;->a(Ljava/util/List;)LX/Clo;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/B5a;Ljava/lang/String;LX/0Ot;)LX/Clw;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5a;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;",
            ">;)",
            "LX/Clw;"
        }
    .end annotation

    .prologue
    .line 2586398
    new-instance v0, LX/CmG;

    invoke-interface {p0}, LX/B5a;->em_()LX/8Yr;

    move-result-object v1

    invoke-interface {p0}, LX/B5a;->E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, LX/CmG;-><init>(LX/8Yr;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;LX/0Ot;)V

    .line 2586399
    iput-object p1, v0, LX/CmG;->g:Ljava/lang/String;

    .line 2586400
    move-object v0, v0

    .line 2586401
    invoke-interface {p0}, LX/B5a;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    .line 2586402
    iput-object v1, v0, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586403
    move-object v0, v0

    .line 2586404
    invoke-interface {p0}, LX/B5a;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v1

    .line 2586405
    iput-object v1, v0, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2586406
    move-object v0, v0

    .line 2586407
    invoke-interface {p0}, LX/B5a;->I()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    .line 2586408
    iput-object v1, v0, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586409
    move-object v0, v0

    .line 2586410
    invoke-interface {p0}, LX/B5a;->H()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    .line 2586411
    iput-object v1, v0, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586412
    move-object v0, v0

    .line 2586413
    invoke-interface {p0}, LX/B5a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, LX/B5a;->c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v2

    invoke-interface {p0}, LX/B5a;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v0

    invoke-interface {p0}, LX/B5a;->s()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v1

    invoke-interface {p0}, LX/B5a;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v0

    invoke-interface {p0}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v0

    invoke-virtual {v0}, LX/Cm7;->b()LX/Clr;

    move-result-object v0

    check-cast v0, LX/Clw;

    return-object v0
.end method

.method public static a(LX/B5a;)LX/Cm4;
    .locals 6

    .prologue
    .line 2586414
    new-instance v0, LX/Cmf;

    invoke-interface {p0}, LX/B5a;->j()LX/8Ys;

    move-result-object v1

    invoke-interface {p0}, LX/B5a;->E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v2

    invoke-interface {p0}, LX/B5a;->J()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v3

    invoke-interface {p0}, LX/B5a;->K()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v4

    invoke-interface {p0}, LX/B5a;->L()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Cmf;-><init>(LX/8Ys;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V

    invoke-interface {p0}, LX/B5a;->D()LX/8Yr;

    move-result-object v1

    .line 2586415
    iput-object v1, v0, LX/Cmf;->b:LX/8Yr;

    .line 2586416
    move-object v0, v0

    .line 2586417
    invoke-interface {p0}, LX/B5a;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    .line 2586418
    iput-object v1, v0, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586419
    move-object v0, v0

    .line 2586420
    invoke-interface {p0}, LX/B5a;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v1

    .line 2586421
    iput-object v1, v0, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2586422
    move-object v0, v0

    .line 2586423
    invoke-interface {p0}, LX/B5a;->I()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    .line 2586424
    iput-object v1, v0, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586425
    move-object v0, v0

    .line 2586426
    invoke-interface {p0}, LX/B5a;->H()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    .line 2586427
    iput-object v1, v0, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586428
    move-object v0, v0

    .line 2586429
    invoke-interface {p0}, LX/B5a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, LX/B5a;->c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v2

    invoke-interface {p0}, LX/B5a;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v0

    invoke-interface {p0}, LX/B5a;->s()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v1

    invoke-interface {p0}, LX/B5a;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v0

    invoke-interface {p0}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v0

    invoke-virtual {v0}, LX/Cm7;->b()LX/Clr;

    move-result-object v0

    check-cast v0, LX/Cm4;

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel;ILjava/lang/String;Ljava/lang/String;ILX/8bZ;)Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2586430
    invoke-static {p0}, LX/Crt;->b(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)I

    move-result v2

    .line 2586431
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel$NodeModel;

    move-result-object v0

    .line 2586432
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel$NodeModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;

    move-result-object v3

    .line 2586433
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel$NodeModel;->b()Z

    move-result v4

    .line 2586434
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel$NodeModel;

    move-result-object v5

    if-nez v5, :cond_5

    .line 2586435
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    .line 2586436
    :goto_0
    move-object v5, v5

    .line 2586437
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel$NodeModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextTextModel;

    move-result-object v6

    if-nez v6, :cond_3

    :cond_1
    move-object v0, v1

    .line 2586438
    :goto_1
    new-instance v6, LX/CmX;

    invoke-direct {v6, v2}, LX/CmX;-><init>(I)V

    .line 2586439
    iput-object v5, v6, LX/CmX;->o:Ljava/util/List;

    .line 2586440
    move-object v2, v6

    .line 2586441
    iput-object v0, v2, LX/CmX;->p:Ljava/lang/String;

    .line 2586442
    move-object v0, v2

    .line 2586443
    invoke-static {v3}, LX/Crt;->b(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586444
    iput-object v2, v0, LX/CmX;->d:Ljava/lang/String;

    .line 2586445
    move-object v0, v0

    .line 2586446
    iput-boolean v4, v0, LX/CmX;->h:Z

    .line 2586447
    move-object v0, v0

    .line 2586448
    invoke-static {v3}, LX/Crt;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586449
    iput-object v2, v0, LX/CmX;->f:Ljava/lang/String;

    .line 2586450
    move-object v0, v0

    .line 2586451
    invoke-static {v3}, LX/Crt;->c(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586452
    iput-object v2, v0, LX/CmX;->c:Ljava/lang/String;

    .line 2586453
    move-object v0, v0

    .line 2586454
    invoke-static {v3}, LX/Crt;->g(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586455
    iput-object v2, v0, LX/CmX;->g:Ljava/lang/String;

    .line 2586456
    move-object v0, v0

    .line 2586457
    invoke-static {v3}, LX/Crt;->f(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586458
    iput-object v2, v0, LX/CmX;->m:Ljava/lang/String;

    .line 2586459
    move-object v0, v0

    .line 2586460
    invoke-static {v3}, LX/Crt;->d(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 2586461
    iput-object v2, v0, LX/CmX;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2586462
    move-object v2, v0

    .line 2586463
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2586464
    :goto_2
    iput-object v0, v2, LX/CmX;->a:Ljava/lang/String;

    .line 2586465
    move-object v0, v2

    .line 2586466
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2586467
    :cond_2
    iput-object v1, v0, LX/CmX;->b:Ljava/lang/String;

    .line 2586468
    move-object v0, v0

    .line 2586469
    iput p2, v0, LX/CmX;->i:I

    .line 2586470
    move-object v0, v0

    .line 2586471
    invoke-static {v3}, LX/Crt;->e(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v1

    .line 2586472
    iput-object v1, v0, LX/CmX;->j:Ljava/lang/String;

    .line 2586473
    move-object v0, v0

    .line 2586474
    invoke-static {v3}, LX/Crt;->h(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v1

    .line 2586475
    iput-object v1, v0, LX/CmX;->e:Ljava/lang/String;

    .line 2586476
    move-object v0, v0

    .line 2586477
    sget-object v1, LX/Cm2;->INLINE:LX/Cm2;

    .line 2586478
    iput-object v1, v0, LX/CmX;->k:LX/Cm2;

    .line 2586479
    move-object v0, v0

    .line 2586480
    iput p5, v0, LX/CmX;->n:I

    .line 2586481
    move-object v0, v0

    .line 2586482
    iput-object p3, v0, LX/CmA;->a:Ljava/lang/String;

    .line 2586483
    move-object v0, v0

    .line 2586484
    invoke-virtual {v0, p4}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v0

    invoke-static {p6, p0}, LX/Crt;->b(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)LX/Cml;

    move-result-object v1

    .line 2586485
    iput-object v1, v0, LX/Cm7;->e:LX/Cml;

    .line 2586486
    move-object v0, v0

    .line 2586487
    invoke-virtual {v0}, LX/Cm7;->b()LX/Clr;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    return-object v0

    .line 2586488
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel$NodeModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextTextModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 2586489
    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel$NodeModel;->c()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/Crt;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleModel;ILjava/lang/String;ILX/8bZ;)Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2586490
    invoke-static {p0}, LX/Crt;->b(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)I

    move-result v2

    .line 2586491
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;

    move-result-object v3

    .line 2586492
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleModel;->b()Z

    move-result v4

    .line 2586493
    if-nez p1, :cond_3

    .line 2586494
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 2586495
    :goto_0
    move-object v5, v0

    .line 2586496
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextTextModel;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2586497
    :goto_1
    new-instance v6, LX/CmX;

    invoke-direct {v6, v2}, LX/CmX;-><init>(I)V

    .line 2586498
    iput-object v5, v6, LX/CmX;->o:Ljava/util/List;

    .line 2586499
    move-object v2, v6

    .line 2586500
    iput-object v0, v2, LX/CmX;->p:Ljava/lang/String;

    .line 2586501
    move-object v0, v2

    .line 2586502
    invoke-static {v3}, LX/Crt;->b(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586503
    iput-object v2, v0, LX/CmX;->d:Ljava/lang/String;

    .line 2586504
    move-object v0, v0

    .line 2586505
    iput-boolean v4, v0, LX/CmX;->h:Z

    .line 2586506
    move-object v0, v0

    .line 2586507
    invoke-static {v3}, LX/Crt;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586508
    iput-object v2, v0, LX/CmX;->f:Ljava/lang/String;

    .line 2586509
    move-object v0, v0

    .line 2586510
    invoke-static {v3}, LX/Crt;->c(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586511
    iput-object v2, v0, LX/CmX;->c:Ljava/lang/String;

    .line 2586512
    move-object v0, v0

    .line 2586513
    invoke-static {v3}, LX/Crt;->g(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586514
    iput-object v2, v0, LX/CmX;->g:Ljava/lang/String;

    .line 2586515
    move-object v0, v0

    .line 2586516
    invoke-static {v3}, LX/Crt;->f(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v2

    .line 2586517
    iput-object v2, v0, LX/CmX;->m:Ljava/lang/String;

    .line 2586518
    move-object v0, v0

    .line 2586519
    invoke-static {v3}, LX/Crt;->d(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 2586520
    iput-object v2, v0, LX/CmX;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2586521
    move-object v2, v0

    .line 2586522
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2586523
    :goto_2
    iput-object v0, v2, LX/CmX;->a:Ljava/lang/String;

    .line 2586524
    move-object v0, v2

    .line 2586525
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2586526
    :cond_0
    iput-object v1, v0, LX/CmX;->b:Ljava/lang/String;

    .line 2586527
    move-object v0, v0

    .line 2586528
    iput p2, v0, LX/CmX;->i:I

    .line 2586529
    move-object v0, v0

    .line 2586530
    invoke-static {v3}, LX/Crt;->e(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v1

    .line 2586531
    iput-object v1, v0, LX/CmX;->j:Ljava/lang/String;

    .line 2586532
    move-object v0, v0

    .line 2586533
    invoke-static {v3}, LX/Crt;->h(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;

    move-result-object v1

    .line 2586534
    iput-object v1, v0, LX/CmX;->e:Ljava/lang/String;

    .line 2586535
    move-object v0, v0

    .line 2586536
    sget-object v1, LX/Cm2;->BOTTOM:LX/Cm2;

    .line 2586537
    iput-object v1, v0, LX/CmX;->k:LX/Cm2;

    .line 2586538
    move-object v0, v0

    .line 2586539
    iput p4, v0, LX/CmX;->n:I

    .line 2586540
    move-object v0, v0

    .line 2586541
    iput-object p3, v0, LX/CmA;->a:Ljava/lang/String;

    .line 2586542
    move-object v0, v0

    .line 2586543
    invoke-static {p5, p0}, LX/Crt;->b(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)LX/Cml;

    move-result-object v1

    .line 2586544
    iput-object v1, v0, LX/Cm7;->e:LX/Cml;

    .line 2586545
    move-object v0, v0

    .line 2586546
    invoke-virtual {v0}, LX/Cm7;->b()LX/Clr;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    return-object v0

    .line 2586547
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextTextModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2586548
    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Crt;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0
.end method
