.class public final LX/Im0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;)V
    .locals 0

    .prologue
    .line 2608691
    iput-object p1, p0, LX/Im0;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2608692
    iget-object v2, p0, LX/Im0;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    invoke-static {v2}, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->c(Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2608693
    iget-object v2, p0, LX/Im0;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    iget-object v2, v2, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->f:Landroid/view/MenuItem;

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2608694
    iget-object v0, p0, LX/Im0;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 2608695
    :goto_0
    return-void

    .line 2608696
    :cond_0
    iget-object v2, p0, LX/Im0;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    iget-object v2, v2, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->f:Landroid/view/MenuItem;

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2608697
    iget-object v2, p0, LX/Im0;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    iget-object v2, v2, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v3

    iget-object v4, p0, LX/Im0;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    iget-object v4, v4, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->g:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-static {v4}, LX/6zC;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)I

    move-result v4

    if-lt v3, v4, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2608698
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2608699
    return-void
.end method
