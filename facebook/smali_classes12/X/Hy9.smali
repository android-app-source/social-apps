.class public final enum LX/Hy9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hy9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hy9;

.field public static final enum FETCH_BIRTHDAYS:LX/Hy9;

.field public static final enum FETCH_DISCOVERY_FILTERS:LX/Hy9;

.field public static final enum FETCH_DRAFT_EVENTS:LX/Hy9;

.field public static final enum FETCH_EVENTS_FOR_DISCOVERY_SURFACE:LX/Hy9;

.field public static final enum FETCH_EVENTS_TODAY:LX/Hy9;

.field public static final enum FETCH_EVENTS_UPDATE:LX/Hy9;

.field public static final enum FETCH_EVENT_COUNTS:LX/Hy9;

.field public static final enum FETCH_HOSTING_EVENTS:LX/Hy9;

.field public static final enum FETCH_PAST_EVENTS:LX/Hy9;

.field public static final enum FETCH_PROMPTS:LX/Hy9;

.field public static final enum FETCH_SINGLE_EVENT:LX/Hy9;

.field public static final enum FETCH_TIME_FILTERS:LX/Hy9;

.field public static final enum FETCH_UPCOMING_EVENTS:LX/Hy9;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2523633
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_EVENTS_FOR_DISCOVERY_SURFACE"

    invoke-direct {v0, v1, v3}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_EVENTS_FOR_DISCOVERY_SURFACE:LX/Hy9;

    .line 2523634
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_UPCOMING_EVENTS"

    invoke-direct {v0, v1, v4}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_UPCOMING_EVENTS:LX/Hy9;

    .line 2523635
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_PAST_EVENTS"

    invoke-direct {v0, v1, v5}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_PAST_EVENTS:LX/Hy9;

    .line 2523636
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_BIRTHDAYS"

    invoke-direct {v0, v1, v6}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_BIRTHDAYS:LX/Hy9;

    .line 2523637
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_PROMPTS"

    invoke-direct {v0, v1, v7}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_PROMPTS:LX/Hy9;

    .line 2523638
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_EVENTS_UPDATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_EVENTS_UPDATE:LX/Hy9;

    .line 2523639
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_DISCOVERY_FILTERS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_DISCOVERY_FILTERS:LX/Hy9;

    .line 2523640
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_HOSTING_EVENTS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_HOSTING_EVENTS:LX/Hy9;

    .line 2523641
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_DRAFT_EVENTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_DRAFT_EVENTS:LX/Hy9;

    .line 2523642
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_TIME_FILTERS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_TIME_FILTERS:LX/Hy9;

    .line 2523643
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_EVENTS_TODAY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_EVENTS_TODAY:LX/Hy9;

    .line 2523644
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_SINGLE_EVENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_SINGLE_EVENT:LX/Hy9;

    .line 2523645
    new-instance v0, LX/Hy9;

    const-string v1, "FETCH_EVENT_COUNTS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/Hy9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hy9;->FETCH_EVENT_COUNTS:LX/Hy9;

    .line 2523646
    const/16 v0, 0xd

    new-array v0, v0, [LX/Hy9;

    sget-object v1, LX/Hy9;->FETCH_EVENTS_FOR_DISCOVERY_SURFACE:LX/Hy9;

    aput-object v1, v0, v3

    sget-object v1, LX/Hy9;->FETCH_UPCOMING_EVENTS:LX/Hy9;

    aput-object v1, v0, v4

    sget-object v1, LX/Hy9;->FETCH_PAST_EVENTS:LX/Hy9;

    aput-object v1, v0, v5

    sget-object v1, LX/Hy9;->FETCH_BIRTHDAYS:LX/Hy9;

    aput-object v1, v0, v6

    sget-object v1, LX/Hy9;->FETCH_PROMPTS:LX/Hy9;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Hy9;->FETCH_EVENTS_UPDATE:LX/Hy9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Hy9;->FETCH_DISCOVERY_FILTERS:LX/Hy9;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Hy9;->FETCH_HOSTING_EVENTS:LX/Hy9;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Hy9;->FETCH_DRAFT_EVENTS:LX/Hy9;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Hy9;->FETCH_TIME_FILTERS:LX/Hy9;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Hy9;->FETCH_EVENTS_TODAY:LX/Hy9;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Hy9;->FETCH_SINGLE_EVENT:LX/Hy9;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Hy9;->FETCH_EVENT_COUNTS:LX/Hy9;

    aput-object v2, v0, v1

    sput-object v0, LX/Hy9;->$VALUES:[LX/Hy9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2523647
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hy9;
    .locals 1

    .prologue
    .line 2523648
    const-class v0, LX/Hy9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hy9;

    return-object v0
.end method

.method public static values()[LX/Hy9;
    .locals 1

    .prologue
    .line 2523649
    sget-object v0, LX/Hy9;->$VALUES:[LX/Hy9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hy9;

    return-object v0
.end method
