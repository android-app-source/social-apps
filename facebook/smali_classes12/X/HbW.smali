.class public final LX/HbW;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/HbX;


# direct methods
.method public constructor <init>(LX/HbX;)V
    .locals 0

    .prologue
    .line 2485521
    iput-object p1, p0, LX/HbW;->a:LX/HbX;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6

    .prologue
    .line 2485522
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 2485523
    iget-object v0, p0, LX/HbW;->a:LX/HbX;

    iget-object v0, v0, LX/HbX;->a:LX/HbY;

    iget-boolean v0, v0, LX/HbY;->G:Z

    if-eqz v0, :cond_0

    .line 2485524
    iget-object v0, p0, LX/HbW;->a:LX/HbX;

    iget-object v0, v0, LX/HbX;->a:LX/HbY;

    iget-object v0, v0, LX/HbY;->x:LX/Had;

    iget-object v1, p0, LX/HbW;->a:LX/HbX;

    iget-object v1, v1, LX/HbX;->a:LX/HbY;

    iget v1, v1, LX/HbN;->n:I

    const/4 v5, 0x1

    .line 2485525
    iget-object v2, v0, LX/Had;->a:LX/Hag;

    sget-object v3, LX/Hac;->EXPAND_COMPLETED:LX/Hac;

    .line 2485526
    iput-object v3, v2, LX/Hag;->e:LX/Hac;

    .line 2485527
    iget-object v2, v0, LX/Had;->a:LX/Hag;

    .line 2485528
    iput v1, v2, LX/Hag;->h:I

    .line 2485529
    iget-object v2, v0, LX/Had;->a:LX/Hag;

    iget-object v2, v2, LX/Hag;->c:LX/Hbd;

    .line 2485530
    iput-boolean v5, v2, LX/Hbd;->c:Z

    .line 2485531
    iget-object v2, v0, LX/Had;->a:LX/Hag;

    iget-object v2, v2, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v2}, LX/Hbd;->d()V

    .line 2485532
    iget-object v2, v0, LX/Had;->a:LX/Hag;

    iget-object v2, v2, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v2, v1}, LX/HbJ;->e(I)V

    .line 2485533
    iget-object v2, v0, LX/Had;->a:LX/Hag;

    const v3, 0x7f020756

    const/16 v4, 0xc

    .line 2485534
    packed-switch v5, :pswitch_data_0

    .line 2485535
    :goto_0
    iget-object v2, v0, LX/Had;->a:LX/Hag;

    iget-object v2, v2, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v3, v0, LX/Had;->a:LX/Hag;

    iget-object v3, v3, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f083652

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2485536
    iget-object v0, p0, LX/HbW;->a:LX/HbX;

    iget-object v0, v0, LX/HbX;->a:LX/HbY;

    invoke-static {v0}, LX/HbY;->A(LX/HbY;)V

    .line 2485537
    iget-object v0, p0, LX/HbW;->a:LX/HbX;

    iget-object v0, v0, LX/HbX;->a:LX/HbY;

    iget-object v0, v0, LX/HbY;->C:LX/HaY;

    invoke-virtual {v0}, LX/HaY;->a()V

    .line 2485538
    :goto_1
    return-void

    .line 2485539
    :cond_0
    iget-object v0, p0, LX/HbW;->a:LX/HbX;

    iget-object v0, v0, LX/HbX;->a:LX/HbY;

    iget-object v0, v0, LX/HbY;->x:LX/Had;

    .line 2485540
    iget-object v1, v0, LX/Had;->a:LX/Hag;

    sget-object v2, LX/Hac;->COLLAPSE_COMPLETED:LX/Hac;

    .line 2485541
    iput-object v2, v1, LX/Hag;->e:LX/Hac;

    .line 2485542
    iget-object v1, v0, LX/Had;->a:LX/Hag;

    iget-object v1, v1, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v1}, LX/Hbd;->d()V

    .line 2485543
    goto :goto_1

    .line 2485544
    :pswitch_0
    iget-object p1, v2, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v1, v2, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    goto :goto_0

    .line 2485545
    :pswitch_1
    iget-object p1, v2, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v1, v2, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2485546
    iget-object p1, v2, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    .line 2485547
    invoke-static {p1, v4}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result p1

    .line 2485548
    iget-object v1, v2, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, p1, p1, p1, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->setPadding(IIII)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
