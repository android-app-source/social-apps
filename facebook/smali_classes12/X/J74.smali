.class public final LX/J74;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J73;


# instance fields
.field public final synthetic a:Lcom/facebook/profile/inforequest/InfoRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/profile/inforequest/InfoRequestFragment;)V
    .locals 0

    .prologue
    .line 2650487
    iput-object p1, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 2650496
    iget-object v0, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2650497
    if-eqz v0, :cond_0

    .line 2650498
    iget-object v1, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v1, v1, Lcom/facebook/profile/inforequest/InfoRequestFragment;->h:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2650499
    iget-object v1, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v1, v1, Lcom/facebook/profile/inforequest/InfoRequestFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650500
    iget-object v0, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2650501
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2650493
    const v0, 0x7f08390b

    invoke-direct {p0, v0}, LX/J74;->a(I)V

    .line 2650494
    iget-object v0, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->k:Landroid/os/Handler;

    iget-object v1, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v1, v1, Lcom/facebook/profile/inforequest/InfoRequestFragment;->l:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, -0x2171e4a6

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2650495
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2650488
    iget-object v0, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->j:Landroid/os/ParcelUuid;

    if-eqz v0, :cond_0

    .line 2650489
    iget-object v0, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->b:LX/J7A;

    new-instance v1, LX/J7C;

    iget-object v2, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v2, v2, Lcom/facebook/profile/inforequest/InfoRequestFragment;->j:Landroid/os/ParcelUuid;

    invoke-direct {v1, v2}, LX/J7C;-><init>(Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2650490
    :cond_0
    const v0, 0x7f08390a

    invoke-direct {p0, v0}, LX/J74;->a(I)V

    .line 2650491
    iget-object v0, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v0, v0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->k:Landroid/os/Handler;

    iget-object v1, p0, LX/J74;->a:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iget-object v1, v1, Lcom/facebook/profile/inforequest/InfoRequestFragment;->l:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, -0xcd59604

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2650492
    return-void
.end method
