.class public LX/JPX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JPX",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2689919
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2689920
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JPX;->b:LX/0Zi;

    .line 2689921
    iput-object p1, p0, LX/JPX;->a:LX/0Ot;

    .line 2689922
    return-void
.end method

.method public static a(LX/0QB;)LX/JPX;
    .locals 4

    .prologue
    .line 2689923
    const-class v1, LX/JPX;

    monitor-enter v1

    .line 2689924
    :try_start_0
    sget-object v0, LX/JPX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2689925
    sput-object v2, LX/JPX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2689926
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2689927
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2689928
    new-instance v3, LX/JPX;

    const/16 p0, 0x1fe2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JPX;-><init>(LX/0Ot;)V

    .line 2689929
    move-object v0, v3

    .line 2689930
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2689931
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JPX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2689932
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2689933
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2689934
    check-cast p2, LX/JPW;

    .line 2689935
    iget-object v0, p0, LX/JPX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;

    iget-object v2, p2, LX/JPW;->a:Landroid/net/Uri;

    iget-object v3, p2, LX/JPW;->b:Ljava/lang/String;

    iget-object v4, p2, LX/JPW;->c:Ljava/lang/String;

    iget-boolean v5, p2, LX/JPW;->d:Z

    iget-object v6, p2, LX/JPW;->e:Ljava/lang/String;

    move-object v1, p1

    const/4 v9, 0x2

    .line 2689936
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b2597

    invoke-interface {v7, v8}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v9}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    iget-object v8, v0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;->b:LX/1nu;

    sget-object v9, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v1, v8, v9, v2}, LX/JPK;->a(LX/1De;LX/1nu;Lcom/facebook/common/callercontext/CallerContext;Landroid/net/Uri;)LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p2

    move-object v7, v0

    move-object v8, v1

    move-object v9, v3

    move-object v10, v4

    move-object p0, v6

    move p1, v5

    const/4 v3, 0x1

    .line 2689937
    invoke-static {v8}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    invoke-static {v8}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010c

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b2595

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-static {v8}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b2596

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    iget-object v1, v7, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;->c:LX/1vg;

    invoke-static {v8, v1, p0, p1}, LX/JPK;->a(LX/1De;LX/1vg;Ljava/lang/String;Z)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f0b2581

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    move-object v7, v0

    .line 2689938
    const/4 v8, 0x7

    const v9, 0x7f0b25a2

    invoke-interface {v7, v8, v9}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    invoke-interface {p2, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x6

    const v9, 0x7f0b25a1

    invoke-interface {v7, v8, v9}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 2689939
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2689940
    invoke-static {}, LX/1dS;->b()V

    .line 2689941
    const/4 v0, 0x0

    return-object v0
.end method
