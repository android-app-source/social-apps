.class public LX/Iui;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field public final a:LX/6cy;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Sh;

.field private final d:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2627233
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Iui;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/6cy;LX/0Or;LX/0Sh;)V
    .locals 4
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6cy;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2627227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2627228
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/Iui;->d:Ljava/util/concurrent/atomic/AtomicLong;

    .line 2627229
    iput-object p1, p0, LX/Iui;->a:LX/6cy;

    .line 2627230
    iput-object p2, p0, LX/Iui;->b:LX/0Or;

    .line 2627231
    iput-object p3, p0, LX/Iui;->c:LX/0Sh;

    .line 2627232
    return-void
.end method

.method public static a(LX/0QB;)LX/Iui;
    .locals 9

    .prologue
    .line 2627198
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2627199
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2627200
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2627201
    if-nez v1, :cond_0

    .line 2627202
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2627203
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2627204
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2627205
    sget-object v1, LX/Iui;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2627206
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2627207
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2627208
    :cond_1
    if-nez v1, :cond_4

    .line 2627209
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2627210
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2627211
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2627212
    new-instance v8, LX/Iui;

    invoke-static {v0}, LX/6cy;->a(LX/0QB;)LX/6cy;

    move-result-object v1

    check-cast v1, LX/6cy;

    const/16 v7, 0x15e8

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-direct {v8, v1, p0, v7}, LX/Iui;-><init>(LX/6cy;LX/0Or;LX/0Sh;)V

    .line 2627213
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2627214
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2627215
    if-nez v1, :cond_2

    .line 2627216
    sget-object v0, LX/Iui;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iui;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2627217
    :goto_1
    if-eqz v0, :cond_3

    .line 2627218
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2627219
    :goto_3
    check-cast v0, LX/Iui;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2627220
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2627221
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2627222
    :catchall_1
    move-exception v0

    .line 2627223
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2627224
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2627225
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2627226
    :cond_2
    :try_start_8
    sget-object v0, LX/Iui;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iui;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/Iui;J)V
    .locals 5

    .prologue
    .line 2627195
    iget-object v0, p0, LX/Iui;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2627196
    iget-object v0, p0, LX/Iui;->a:LX/6cy;

    sget-object v1, LX/2gS;->f:LX/2bA;

    invoke-virtual {v0, v1, p1, p2}, LX/48u;->b(LX/0To;J)V

    .line 2627197
    :cond_0
    return-void
.end method

.method private static b(LX/Iui;J)J
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 2627189
    iget-object v0, p0, LX/Iui;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    .line 2627190
    cmp-long v2, v0, v4

    if-nez v2, :cond_0

    .line 2627191
    iget-object v0, p0, LX/Iui;->a:LX/6cy;

    sget-object v1, LX/2gS;->f:LX/2bA;

    invoke-virtual {v0, v1, v4, v5}, LX/48u;->a(LX/0To;J)J

    move-result-wide v0

    .line 2627192
    :cond_0
    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    .line 2627193
    invoke-static {p0, p1, p2}, LX/Iui;->a(LX/Iui;J)V

    .line 2627194
    :cond_1
    return-wide v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    .line 2627167
    iget-object v0, p0, LX/Iui;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 2627168
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2627169
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 2627170
    :goto_0
    return-object v0

    .line 2627171
    :cond_1
    invoke-virtual {p1, v4}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2627172
    sget-object v1, LX/2gS;->e:LX/2bA;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    .line 2627173
    iget-object v1, p0, LX/Iui;->a:LX/6cy;

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, LX/48u;->a(LX/0To;J)J

    move-result-wide v2

    .line 2627174
    cmp-long v1, v2, v6

    if-gez v1, :cond_6

    .line 2627175
    invoke-virtual {p1, v4}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    iget-wide v2, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {p0, v2, v3}, LX/Iui;->b(LX/Iui;J)J

    move-result-wide v2

    move-wide v8, v2

    .line 2627176
    :goto_1
    cmp-long v1, v8, v6

    if-gez v1, :cond_2

    .line 2627177
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 2627178
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2627179
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v5, v1

    move-wide v2, v6

    :goto_2
    if-ltz v5, :cond_4

    .line 2627180
    invoke-virtual {p1, v5}, Lcom/facebook/messaging/model/messages/MessagesCollection;->b(I)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v10

    .line 2627181
    iget-object v1, p0, LX/Iui;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v10, v1}, LX/2gS;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2627182
    iget-wide v12, v10, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v1, v12, v8

    if-lez v1, :cond_3

    .line 2627183
    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2627184
    iget-wide v10, v10, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 2627185
    :cond_3
    add-int/lit8 v1, v5, -0x1

    move v5, v1

    goto :goto_2

    .line 2627186
    :cond_4
    cmp-long v1, v2, v6

    if-lez v1, :cond_5

    .line 2627187
    iget-object v1, p0, LX/Iui;->a:LX/6cy;

    invoke-virtual {v1, v0, v2, v3}, LX/48u;->b(LX/0To;J)V

    :cond_5
    move-object v0, v4

    .line 2627188
    goto :goto_0

    :cond_6
    move-wide v8, v2

    goto :goto_1
.end method
