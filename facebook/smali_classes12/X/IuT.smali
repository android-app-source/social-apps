.class public LX/IuT;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/1Hy;


# instance fields
.field private final c:Ljava/security/SecureRandom;

.field private final d:Ljavax/crypto/Mac;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2626852
    const-class v0, LX/IuT;

    sput-object v0, LX/IuT;->a:Ljava/lang/Class;

    .line 2626853
    sget-object v0, LX/1Hy;->KEY_256:LX/1Hy;

    sput-object v0, LX/IuT;->b:LX/1Hy;

    return-void
.end method

.method public constructor <init>(LX/1Hr;)V
    .locals 4

    .prologue
    .line 2626801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2626802
    invoke-static {}, LX/1Hr;->a()Ljava/security/SecureRandom;

    move-result-object v0

    iput-object v0, p0, LX/IuT;->c:Ljava/security/SecureRandom;

    .line 2626803
    const/4 v0, 0x0

    .line 2626804
    :try_start_0
    const-string v1, "HmacSHA256"

    invoke-static {v1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2626805
    :goto_0
    iput-object v0, p0, LX/IuT;->d:Ljavax/crypto/Mac;

    .line 2626806
    return-void

    .line 2626807
    :catch_0
    move-exception v1

    .line 2626808
    sget-object v2, LX/IuT;->a:Ljava/lang/Class;

    const-string v3, "Could not create SHA256 HMAC for salamander signing"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/model/attachment/Attachment;)LX/DpA;
    .locals 10

    .prologue
    .line 2626809
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626810
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2626811
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->j:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    iget v2, p0, Lcom/facebook/messaging/model/attachment/Attachment;->f:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/model/attachment/Attachment;->k:[B

    iget-object v4, p0, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    .line 2626812
    iget-object v6, p0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v6, v6, Lcom/facebook/messaging/model/attachment/ImageData;->g:Ljava/lang/String;

    .line 2626813
    if-eqz v6, :cond_0

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v6

    :goto_0
    move-object v6, v6

    .line 2626814
    iget-object v7, p0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget v7, v7, Lcom/facebook/messaging/model/attachment/ImageData;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget v8, v8, Lcom/facebook/messaging/model/attachment/ImageData;->b:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 2626815
    new-instance v9, LX/DpI;

    invoke-direct {v9, v7, v8}, LX/DpI;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    move-object v7, v9

    .line 2626816
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/facebook/messaging/model/attachment/Attachment;->l:Ljava/lang/String;

    invoke-static/range {v0 .. v9}, LX/Dpm;->a([BLjava/lang/Long;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/String;[BLX/DpI;LX/Dpp;Ljava/lang/String;)LX/DpA;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized a(LX/IuT;ILX/DpZ;Ljava/lang/Integer;)LX/IuS;
    .locals 5
    .param p2    # LX/DpZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2626817
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/IuT;->b:LX/1Hy;

    iget v0, v0, LX/1Hy;->keyLength:I

    new-array v1, v0, [B

    .line 2626818
    iget-object v0, p0, LX/IuT;->c:Ljava/security/SecureRandom;

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 2626819
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2626820
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, v1, v0}, LX/Dpm;->a(ILX/DpZ;[BLjava/lang/Integer;)LX/DpY;

    move-result-object v0

    .line 2626821
    invoke-static {v0}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v2

    .line 2626822
    iget-object v0, p0, LX/IuT;->d:Ljavax/crypto/Mac;

    if-nez v0, :cond_1

    .line 2626823
    sget-object v0, LX/IuT;->a:Ljava/lang/Class;

    const-string v1, "Could not sign salamander - missing SHA256 HMAC"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2626824
    new-instance v0, LX/IuS;

    const/4 v1, 0x1

    new-array v1, v1, [B

    invoke-direct {v0, p0, v2, v1}, LX/IuS;-><init>(LX/IuT;[B[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2626825
    :goto_1
    monitor-exit p0

    return-object v0

    .line 2626826
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2626827
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/IuT;->d:Ljavax/crypto/Mac;

    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "HmacSHA256"

    invoke-direct {v3, v1, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v0, v3}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2626828
    :try_start_2
    new-instance v0, LX/IuS;

    iget-object v1, p0, LX/IuT;->d:Ljavax/crypto/Mac;

    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v1

    invoke-direct {v0, p0, v2, v1}, LX/IuS;-><init>(LX/IuT;[B[B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2626829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2626830
    :catch_0
    move-exception v0

    .line 2626831
    :try_start_3
    sget-object v1, LX/IuT;->a:Ljava/lang/Class;

    const-string v3, "Could not sign salamander"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2626832
    new-instance v0, LX/IuS;

    const/4 v1, 0x1

    new-array v1, v1, [B

    invoke-direct {v0, p0, v2, v1}, LX/IuS;-><init>(LX/IuT;[B[B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)LX/IuS;
    .locals 5

    .prologue
    .line 2626833
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2626834
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 2626835
    invoke-static {v0}, LX/IuT;->a(Lcom/facebook/messaging/model/attachment/Attachment;)LX/DpA;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2626836
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2626837
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 2626838
    new-instance v1, LX/DpZ;

    invoke-direct {v1}, LX/DpZ;-><init>()V

    .line 2626839
    invoke-static {v1, v2}, LX/DpZ;->b(LX/DpZ;Ljava/util/List;)V

    .line 2626840
    move-object v1, v1

    .line 2626841
    const/4 v3, 0x4

    invoke-static {p0, v3, v1, v0}, LX/IuT;->a(LX/IuT;ILX/DpZ;Ljava/lang/Integer;)LX/IuS;

    move-result-object v1

    move-object v0, v1

    .line 2626842
    return-object v0
.end method

.method public final declared-synchronized a([B[B[B)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2626843
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/IuT;->d:Ljavax/crypto/Mac;

    if-nez v1, :cond_0

    .line 2626844
    sget-object v1, LX/IuT;->a:Ljava/lang/Class;

    const-string v2, "Could not verify Salamander signature - no SHA256HMAC"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2626845
    :goto_0
    monitor-exit p0

    return v0

    .line 2626846
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/IuT;->d:Ljavax/crypto/Mac;

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "HmacSHA256"

    invoke-direct {v2, p2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2626847
    :try_start_2
    iget-object v0, p0, LX/IuT;->d:Ljavax/crypto/Mac;

    invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    .line 2626848
    invoke-static {p3, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0

    .line 2626849
    :catch_0
    move-exception v1

    .line 2626850
    sget-object v2, LX/IuT;->a:Ljava/lang/Class;

    const-string v3, "Could not verify salamander signature"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2626851
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
