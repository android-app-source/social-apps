.class public final LX/Igc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V
    .locals 0

    .prologue
    .line 2601328
    iput-object p1, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 9

    .prologue
    .line 2601329
    iget-object v0, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v1}, LX/2MK;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;Ljava/lang/String;)V

    .line 2601330
    iget-object v0, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601331
    iget-boolean v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    move v0, v1

    .line 2601332
    if-eqz v0, :cond_0

    .line 2601333
    iget-object v0, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2601334
    invoke-static {v0, v1}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->b$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;LX/0Px;)V

    .line 2601335
    :goto_0
    return-void

    .line 2601336
    :cond_0
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v1, :cond_1

    .line 2601337
    iget-object v0, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->k:LX/Ihz;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Ihz;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2601338
    :goto_1
    goto :goto_0

    .line 2601339
    :cond_1
    const/4 v7, 0x0

    .line 2601340
    invoke-static {p1}, LX/Ii2;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2601341
    iget-object v2, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    .line 2601342
    new-instance v3, LX/0ju;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v4, 0x7f082e11

    invoke-virtual {v3, v4}, LX/0ju;->a(I)LX/0ju;

    move-result-object v3

    const v4, 0x7f082e12

    invoke-virtual {v3, v4}, LX/0ju;->b(I)LX/0ju;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v3

    const v4, 0x7f080036

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    .line 2601343
    :cond_2
    :goto_2
    goto :goto_0

    .line 2601344
    :cond_3
    invoke-static {p1}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    move-result-object v0

    iget-object v1, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "photo_edit_dialog_fragment_tag"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_1

    .line 2601345
    :cond_4
    iget-object v2, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->l:LX/Ii1;

    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    const-string v4, "messenger_video_edit"

    invoke-virtual {v2, v3, v4}, LX/Ii1;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2601346
    iget-object v2, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->i:LX/Iht;

    iget-object v3, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/Igc;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    .line 2601347
    iget-object v5, v4, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v5, v5

    .line 2601348
    const-string v6, "VIDEO_EDIT"

    move-object v4, p1

    move-object v8, v7

    invoke-virtual/range {v2 .. v8}, LX/Iht;->a(Landroid/content/Context;Lcom/facebook/ui/media/attachments/MediaResource;LX/0gc;Ljava/lang/String;LX/6eE;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_2
.end method
