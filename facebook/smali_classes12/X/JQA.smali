.class public LX/JQA;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQ8;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2691098
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JQA;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JQC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691099
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2691100
    iput-object p1, p0, LX/JQA;->b:LX/0Ot;

    .line 2691101
    return-void
.end method

.method public static a(LX/0QB;)LX/JQA;
    .locals 4

    .prologue
    .line 2691102
    const-class v1, LX/JQA;

    monitor-enter v1

    .line 2691103
    :try_start_0
    sget-object v0, LX/JQA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691104
    sput-object v2, LX/JQA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691105
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691106
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691107
    new-instance v3, LX/JQA;

    const/16 p0, 0x1ff5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQA;-><init>(LX/0Ot;)V

    .line 2691108
    move-object v0, v3

    .line 2691109
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691110
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691111
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691112
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2691113
    check-cast p2, LX/JQ9;

    .line 2691114
    iget-object v0, p0, LX/JQA;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JQC;

    iget-object v1, p2, LX/JQ9;->a:LX/1Pm;

    iget-object v2, p2, LX/JQ9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691115
    new-instance v4, LX/JQB;

    invoke-direct {v4, v0, v2}, LX/JQB;-><init>(LX/JQC;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2691116
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    .line 2691117
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2691118
    check-cast v3, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 2691119
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 2691120
    move-object v5, v5

    .line 2691121
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2691122
    check-cast v3, LX/0jW;

    .line 2691123
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 2691124
    move-object v3, v5

    .line 2691125
    const/16 v5, 0x8

    .line 2691126
    iput v5, v3, LX/3mP;->b:I

    .line 2691127
    move-object v3, v3

    .line 2691128
    iput-object v4, v3, LX/3mP;->g:LX/25K;

    .line 2691129
    move-object v3, v3

    .line 2691130
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 2691131
    iget-object v3, v0, LX/JQC;->b:LX/JQ7;

    .line 2691132
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2691133
    check-cast v4, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    invoke-static {v4}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)LX/0Px;

    move-result-object v6

    move-object v4, p1

    move-object v5, v2

    move-object v7, v1

    invoke-virtual/range {v3 .. v8}, LX/JQ7;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;LX/1Pm;LX/25M;)LX/JQ6;

    move-result-object v3

    .line 2691134
    iget-object v4, v0, LX/JQC;->a:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2691135
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2691136
    invoke-static {}, LX/1dS;->b()V

    .line 2691137
    const/4 v0, 0x0

    return-object v0
.end method
