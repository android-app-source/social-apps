.class public LX/ITU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2579289
    const-class v0, LX/ITU;

    sput-object v0, LX/ITU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2579290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2579291
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Landroid/view/View;LX/ITJ;ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;LX/DPA;LX/DPp;LX/0Ot;LX/3my;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
            "Landroid/view/View;",
            "LX/ITJ;",
            "Z",
            "Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;",
            "Lcom/facebook/groups/groupactions/GroupActionsHelper;",
            "Lcom/facebook/groups/info/GroupInfoFragment$GroupLeaveActionResponder;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/3my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2579292
    new-instance v7, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v7, v1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2579293
    invoke-virtual {v7}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 2579294
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 2579295
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_2

    .line 2579296
    const v1, 0x7f081bf4

    invoke-virtual {v2, v1}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v1

    .line 2579297
    new-instance v2, LX/ITL;

    invoke-direct {v2, p2, p0}, LX/ITL;-><init>(LX/ITJ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2579298
    :cond_0
    :goto_1
    invoke-virtual {v7, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2579299
    return-void

    .line 2579300
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2579301
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_6

    .line 2579302
    if-eqz p3, :cond_4

    .line 2579303
    const v3, 0x7f081bf1

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2579304
    new-instance v4, LX/ITM;

    invoke-direct {v4, p2, p0}, LX/ITM;-><init>(LX/ITJ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2579305
    :goto_2
    if-nez v1, :cond_3

    .line 2579306
    sget-object v3, LX/ITT;->a:[I

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2579307
    :cond_3
    :goto_3
    invoke-static {p0}, LX/ITU;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2579308
    const v3, 0x7f081bf8

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2579309
    new-instance v4, LX/ITQ;

    invoke-direct {v4, p2, p0, p1}, LX/ITQ;-><init>(LX/ITJ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Landroid/view/View;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2579310
    if-eqz v1, :cond_5

    const v1, 0x7f081bf6

    .line 2579311
    :goto_4
    invoke-virtual {v2, v1}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v8

    .line 2579312
    new-instance v1, LX/ITR;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, LX/ITR;-><init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Landroid/view/View;LX/DPA;LX/DPp;LX/0Ot;)V

    invoke-interface {v8, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1

    .line 2579313
    :cond_4
    const v3, 0x7f081bf0

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2579314
    new-instance v4, LX/ITN;

    invoke-direct {v4, p2, p0}, LX/ITN;-><init>(LX/ITJ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_2

    .line 2579315
    :pswitch_0
    const v3, 0x7f081bf2

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2579316
    new-instance v4, LX/ITO;

    const/4 v5, 0x1

    invoke-direct {v4, p2, p1, v5, p0}, LX/ITO;-><init>(LX/ITJ;Landroid/view/View;ZLcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3

    .line 2579317
    :pswitch_1
    const v3, 0x7f081bf3

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2579318
    new-instance v4, LX/ITP;

    const/4 v5, 0x0

    invoke-direct {v4, p2, p1, v5, p0}, LX/ITP;-><init>(LX/ITJ;Landroid/view/View;ZLcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3

    .line 2579319
    :cond_5
    const v1, 0x7f081bf5

    goto :goto_4

    .line 2579320
    :cond_6
    sget-object v1, LX/ITU;->a:Ljava/lang/Class;

    const-string v2, "Trying to display popover menu for non-request/joined groups."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2579321
    if-nez p0, :cond_1

    .line 2579322
    :cond_0
    :goto_0
    return v0

    .line 2579323
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    .line 2579324
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2579325
    invoke-virtual {v1}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->name()Ljava/lang/String;

    move-result-object v0

    .line 2579326
    const-string v1, "REAL_WORLD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
