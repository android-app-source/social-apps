.class public final LX/HRa;
.super LX/2kg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2kg",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        "LX/3DK;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2ke;

.field public final synthetic b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;LX/2ke;)V
    .locals 0

    .prologue
    .line 2465479
    iput-object p1, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iput-object p2, p0, LX/HRa;->a:LX/2ke;

    invoke-direct {p0}, LX/2kg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/2ke;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2ke",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2465480
    iget-object v0, p0, LX/HRa;->a:LX/2ke;

    return-object v0
.end method

.method public final a(LX/2kM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2465481
    invoke-interface {p1}, LX/2kM;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 2465482
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2465483
    :cond_0
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2465484
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 2465485
    invoke-static {v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->c(LX/2nk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2465486
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    sget-object v1, LX/2kx;->LOADING:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    .line 2465487
    :cond_0
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2465488
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 2465489
    sget-object v1, LX/2nk;->INITIAL:LX/2nk;

    if-ne v0, v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2465490
    if-eqz v0, :cond_1

    .line 2465491
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/HRZ;

    invoke-direct {v2, p0}, LX/HRZ;-><init>(LX/HRa;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2465492
    :cond_0
    :goto_1
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2465493
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    iget-object v1, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/1OM;->a(II)V

    .line 2465494
    return-void

    .line 2465495
    :cond_1
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 2465496
    invoke-static {v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->c(LX/2nk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2465497
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    sget-object v1, LX/2kx;->FAILED:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2465498
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    .line 2465499
    iget-object v1, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    .line 2465500
    iget-object v0, v1, LX/2kW;->o:LX/2kM;

    move-object v1, v0

    .line 2465501
    invoke-interface {v1}, LX/2kM;->c()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2465502
    if-eqz v0, :cond_0

    .line 2465503
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2465504
    :cond_0
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2465505
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 2465506
    invoke-static {v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->c(LX/2nk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2465507
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    sget-object v1, LX/2kx;->FINISHED:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    .line 2465508
    :cond_1
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2465509
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->A:Z

    if-eqz v0, :cond_2

    .line 2465510
    iget-object v0, p0, LX/HRa;->b:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->c$redex0(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    .line 2465511
    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
