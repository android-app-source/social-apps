.class public final LX/Hiy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/google/android/gms/maps/model/LatLng;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/location/Address;

.field public final synthetic b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;


# direct methods
.method public constructor <init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Landroid/location/Address;)V
    .locals 0

    .prologue
    .line 2498068
    iput-object p1, p0, LX/Hiy;->b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iput-object p2, p0, LX/Hiy;->a:Landroid/location/Address;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/maps/model/LatLng;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498069
    iget-object v0, p0, LX/Hiy;->b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-static {v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    .line 2498070
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/Hiy;->b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->w:LX/HiW;

    if-nez v0, :cond_1

    .line 2498071
    :cond_0
    iget-object v0, p0, LX/Hiy;->b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c:LX/03V;

    sget-object v1, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->h:Ljava/lang/String;

    const-string v2, "Error getting during fetch onSuccessfulResult."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498072
    :goto_0
    return-void

    .line 2498073
    :cond_1
    iget-object v0, p0, LX/Hiy;->a:Landroid/location/Address;

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Address;->setLatitude(D)V

    .line 2498074
    iget-object v0, p0, LX/Hiy;->a:Landroid/location/Address;

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Address;->setLongitude(D)V

    .line 2498075
    iget-object v0, p0, LX/Hiy;->b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->w:LX/HiW;

    iget-object v1, p0, LX/Hiy;->a:Landroid/location/Address;

    invoke-virtual {v0, v1}, LX/HiW;->a(Landroid/location/Address;)V

    .line 2498076
    iget-object v0, p0, LX/Hiy;->b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v1, p0, LX/Hiy;->a:Landroid/location/Address;

    .line 2498077
    invoke-static {v0, v1}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->b$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Landroid/location/Address;)V

    .line 2498078
    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2498065
    iget-object v0, p0, LX/Hiy;->b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-static {v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    .line 2498066
    iget-object v0, p0, LX/Hiy;->b:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c:LX/03V;

    sget-object v1, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->h:Ljava/lang/String;

    const-string v2, "Can\'t get location from Google Place id."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2498067
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498064
    check-cast p1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {p0, p1}, LX/Hiy;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    return-void
.end method
