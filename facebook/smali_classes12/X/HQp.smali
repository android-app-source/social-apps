.class public final LX/HQp;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;)V
    .locals 0

    .prologue
    .line 2464033
    iput-object p1, p0, LX/HQp;->a:Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2464031
    iget-object v0, p0, LX/HQp;->a:Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    iget-object v0, v0, LX/HQq;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "like_chaining_fetch_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2464032
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2464017
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2464018
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2464019
    iget-object v1, p0, LX/HQp;->a:Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    iget-object v1, v1, LX/HQq;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v1, :cond_0

    .line 2464020
    iget-object v1, p0, LX/HQp;->a:Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    iget-object v1, v1, LX/HQq;->f:LX/HQo;

    iget-object v2, p0, LX/HQp;->a:Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    iget-object v2, v2, LX/HQq;->d:Ljava/lang/String;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 2464021
    iget-object p1, v1, LX/HQo;->a:Ljava/util/Map;

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464022
    iget-object v0, p0, LX/HQp;->a:Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    iget-object v0, v0, LX/HQq;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    .line 2464023
    iget-object v1, v0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v1, v1

    .line 2464024
    if-nez v1, :cond_1

    .line 2464025
    iget-object v1, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string p0, "Null adapter when onRelatedPageDataReady called"

    invoke-virtual {v1, v2, p0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2464026
    :cond_0
    :goto_0
    return-void

    .line 2464027
    :cond_1
    iget-object v1, v0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->k()V

    .line 2464028
    const-class v1, LX/HQy;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HQy;

    .line 2464029
    if-eqz v1, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-interface {v1, v2}, LX/HQy;->c(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2464030
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-interface {v1, v2}, LX/HQy;->d(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    goto :goto_0
.end method
