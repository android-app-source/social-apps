.class public final LX/HxE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HxG;


# direct methods
.method public constructor <init>(LX/HxG;)V
    .locals 0

    .prologue
    .line 2522192
    iput-object p1, p0, LX/HxE;->a:LX/HxG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x2

    const v0, 0x129b23b5

    invoke-static {v5, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2522193
    iget-object v0, p0, LX/HxE;->a:LX/HxG;

    iget-object v0, v0, LX/HxG;->b:LX/7v8;

    iget-object v2, p0, LX/HxE;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->r:Ljava/lang/String;

    .line 2522194
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v6, LX/7v7;->BIRTHDAYS_CLICK_CAMERA:LX/7v7;

    invoke-virtual {v6}, LX/7v7;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2522195
    invoke-static {v0, v4, v2}, LX/7v8;->a(LX/7v8;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 2522196
    sget-object v0, LX/21D;->EVENT:LX/21D;

    const-string v2, "eventDashboardCelebrations"

    invoke-static {v0, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v2, p0, LX/HxE;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    invoke-static {v2}, LX/HxN;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v2, p0, LX/HxE;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->c:LX/HxN;

    invoke-virtual {v2}, LX/HxN;->a()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v2, "ANDROID_EVENTS_DASHBOARD_COMPOSER"

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2522197
    new-instance v2, LX/8AA;

    sget-object v3, LX/8AB;->REACTION:LX/8AB;

    invoke-direct {v2, v3}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2522198
    iput-object v0, v2, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2522199
    move-object v0, v2

    .line 2522200
    invoke-virtual {v0}, LX/8AA;->p()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->q()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    .line 2522201
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v2

    .line 2522202
    iget-object v0, p0, LX/HxE;->a:LX/HxG;

    iget-object v0, v0, LX/HxG;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ay;

    const/16 v3, 0x6dc

    iget-object v4, p0, LX/HxE;->a:LX/HxG;

    iget-object v4, v4, LX/HxG;->j:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0, v2, v3, v4}, LX/1ay;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2522203
    const v0, 0x4658ff01

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
