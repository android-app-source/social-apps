.class public LX/HMA;
.super Landroid/graphics/drawable/ShapeDrawable;
.source ""


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/HM9;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/HM9;

.field private final c:LX/HM9;


# direct methods
.method public constructor <init>(LX/1De;LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/0Px",
            "<",
            "LX/HM9;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/high16 v4, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    const-wide/high16 v2, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .line 2456854
    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    .line 2456855
    new-instance v0, LX/HM9;

    invoke-direct {v0, v4, v5, v4, v5}, LX/HM9;-><init>(DD)V

    iput-object v0, p0, LX/HMA;->b:LX/HM9;

    .line 2456856
    new-instance v0, LX/HM9;

    invoke-direct {v0, v2, v3, v2, v3}, LX/HM9;-><init>(DD)V

    iput-object v0, p0, LX/HMA;->c:LX/HM9;

    .line 2456857
    invoke-virtual {p0}, LX/HMA;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2456858
    invoke-virtual {p0}, LX/HMA;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2456859
    invoke-virtual {p0}, LX/HMA;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2320

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2456860
    iput-object p2, p0, LX/HMA;->a:LX/0Px;

    .line 2456861
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HM9;

    .line 2456862
    iget-object v3, p0, LX/HMA;->b:LX/HM9;

    iget-wide v4, v0, LX/HM9;->a:D

    iget-object v6, p0, LX/HMA;->b:LX/HM9;

    iget-wide v6, v6, LX/HM9;->a:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    iput-wide v4, v3, LX/HM9;->a:D

    .line 2456863
    iget-object v3, p0, LX/HMA;->b:LX/HM9;

    iget-wide v4, v0, LX/HM9;->b:D

    iget-object v6, p0, LX/HMA;->b:LX/HM9;

    iget-wide v6, v6, LX/HM9;->b:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    iput-wide v4, v3, LX/HM9;->b:D

    .line 2456864
    iget-object v3, p0, LX/HMA;->c:LX/HM9;

    iget-wide v4, v0, LX/HM9;->a:D

    iget-object v6, p0, LX/HMA;->c:LX/HM9;

    iget-wide v6, v6, LX/HM9;->a:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    iput-wide v4, v3, LX/HM9;->a:D

    .line 2456865
    iget-object v3, p0, LX/HMA;->c:LX/HM9;

    iget-wide v4, v0, LX/HM9;->b:D

    iget-object v0, p0, LX/HMA;->c:LX/HM9;

    iget-wide v6, v0, LX/HM9;->b:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    iput-wide v4, v3, LX/HM9;->b:D

    .line 2456866
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2456867
    :cond_0
    return-void
.end method


# virtual methods
.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 14

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    .line 2456868
    invoke-super {p0, p1}, Landroid/graphics/drawable/ShapeDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 2456869
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 2456870
    iget-object v0, p0, LX/HMA;->b:LX/HM9;

    iget-wide v0, v0, LX/HM9;->a:D

    iget-object v3, p0, LX/HMA;->c:LX/HM9;

    iget-wide v4, v3, LX/HM9;->a:D

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HMA;->b:LX/HM9;

    iget-wide v0, v0, LX/HM9;->b:D

    iget-object v3, p0, LX/HMA;->c:LX/HM9;

    iget-wide v4, v3, LX/HM9;->b:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_2

    .line 2456871
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2456872
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2456873
    :cond_1
    new-instance v0, Landroid/graphics/drawable/shapes/PathShape;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v0, v2, v1, v3}, Landroid/graphics/drawable/shapes/PathShape;-><init>(Landroid/graphics/Path;FF)V

    invoke-virtual {p0, v0}, LX/HMA;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 2456874
    return-void

    .line 2456875
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/HMA;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2456876
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-double v4, v0

    iget-object v0, p0, LX/HMA;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HM9;

    iget-wide v6, v0, LX/HM9;->a:D

    iget-object v0, p0, LX/HMA;->b:LX/HM9;

    iget-wide v8, v0, LX/HM9;->a:D

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-object v0, p0, LX/HMA;->c:LX/HM9;

    iget-wide v6, v0, LX/HM9;->a:D

    iget-object v0, p0, LX/HMA;->b:LX/HM9;

    iget-wide v8, v0, LX/HM9;->a:D

    sub-double/2addr v6, v8

    div-double/2addr v4, v6

    .line 2456877
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-double v6, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-double v8, v0

    iget-object v0, p0, LX/HMA;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HM9;

    iget-wide v10, v0, LX/HM9;->b:D

    iget-object v0, p0, LX/HMA;->b:LX/HM9;

    iget-wide v12, v0, LX/HM9;->b:D

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    iget-object v0, p0, LX/HMA;->c:LX/HM9;

    iget-wide v10, v0, LX/HM9;->b:D

    iget-object v0, p0, LX/HMA;->b:LX/HM9;

    iget-wide v12, v0, LX/HM9;->b:D

    sub-double/2addr v10, v12

    div-double/2addr v8, v10

    sub-double/2addr v6, v8

    .line 2456878
    if-nez v1, :cond_3

    .line 2456879
    double-to-float v0, v4

    double-to-float v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2456880
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2456881
    :cond_3
    double-to-float v0, v4

    double-to-float v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_1
.end method
