.class public final LX/IJc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IJd;


# direct methods
.method public constructor <init>(LX/IJd;)V
    .locals 0

    .prologue
    .line 2563954
    iput-object p1, p0, LX/IJc;->a:LX/IJd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x7df8cfb6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2563955
    iget-object v1, p0, LX/IJc;->a:LX/IJd;

    iget-object v1, v1, LX/IJd;->d:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, p0, LX/IJc;->a:LX/IJd;

    iget-object v2, v2, LX/IJd;->c:LX/IFd;

    iget-object v3, p0, LX/IJc;->a:LX/IJd;

    .line 2563956
    invoke-virtual {v2}, LX/IFd;->g()Ljava/lang/String;

    move-result-object v8

    .line 2563957
    iget-object v5, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2563958
    const-string v6, "friends_nearby_dashboard_section_expand"

    invoke-static {v5, v6}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2563959
    const-string v7, "section_id"

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2563960
    iget-object v7, v5, LX/IID;->a:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2563961
    iget-object v5, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->t:LX/IFV;

    .line 2563962
    iget-object v6, v2, LX/IFd;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2563963
    iget-object v7, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->f:LX/1Ck;

    invoke-virtual {v5, v8, v6, v7}, LX/IFV;->a(Ljava/lang/String;Ljava/lang/String;LX/1Ck;)LX/IFU;

    move-result-object v10

    .line 2563964
    iget-object v5, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J:Ljava/util/Set;

    invoke-interface {v5, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2563965
    iget-object v5, v3, LX/IJd;->b:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2563966
    iget-object v5, v3, LX/IJd;->a:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2563967
    iget-object v5, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v5}, LX/IFX;->d()LX/0Rf;

    move-result-object v11

    new-instance v5, LX/IIf;

    move-object v6, v1

    move-object v7, v2

    move-object v9, v3

    invoke-direct/range {v5 .. v10}, LX/IIf;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IFd;Ljava/lang/String;LX/IJd;LX/IFU;)V

    .line 2563968
    if-nez v5, :cond_0

    .line 2563969
    :goto_0
    const v1, 0x258ec0ff

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2563970
    :cond_0
    new-instance v6, LX/IGq;

    invoke-direct {v6}, LX/IGq;-><init>()V

    move-object v6, v6

    .line 2563971
    const-string v7, "set_items_cursor"

    iget-object v8, v10, LX/IFU;->c:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "pic_size"

    iget-object v9, v10, LX/IFU;->f:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "set_items_fetch_count"

    const-string v9, "1000"

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "more_section_id"

    iget-object v9, v10, LX/IFU;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2563972
    iget-object v7, v10, LX/IFU;->e:LX/0tX;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2563973
    iget-object v7, v10, LX/IFU;->d:LX/1Ck;

    sget-object v8, LX/IFy;->i:LX/IFy;

    new-instance v9, LX/IFT;

    invoke-direct {v9, v10, v5, v11}, LX/IFT;-><init>(LX/IFU;LX/IIf;LX/0Rf;)V

    invoke-virtual {v7, v8, v6, v9}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
