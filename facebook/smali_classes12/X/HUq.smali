.class public final LX/HUq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HUr;


# direct methods
.method public constructor <init>(LX/HUr;)V
    .locals 0

    .prologue
    .line 2473861
    iput-object p1, p0, LX/HUq;->a:LX/HUr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x199ccb73

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473862
    iget-object v1, p0, LX/HUq;->a:LX/HUr;

    iget-object v1, v1, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->d:LX/17W;

    iget-object v2, p0, LX/HUq;->a:LX/HUr;

    iget-object v2, v2, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->aX:Ljava/lang/String;

    iget-object v4, p0, LX/HUq;->a:LX/HUr;

    iget-object v4, v4, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-wide v4, v4, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2473863
    iget-object v1, p0, LX/HUq;->a:LX/HUr;

    iget-object v1, v1, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->e:LX/9XE;

    iget-object v2, p0, LX/HUq;->a:LX/HUr;

    iget-object v2, v2, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-wide v2, v2, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->i:J

    const-string v4, "VideoPlaylistFragment"

    .line 2473864
    iget-object v5, v1, LX/9XE;->a:LX/0Zb;

    sget-object p0, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_CARD_HEADER:LX/9XI;

    invoke-static {p0, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "navigation_source"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v5, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2473865
    const v1, 0x67422dab

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
