.class public LX/IKl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/IK9;


# direct methods
.method public constructor <init>(LX/IK9;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2567292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2567293
    iput-object p1, p0, LX/IKl;->a:LX/IK9;

    .line 2567294
    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2567295
    iget v0, p1, LX/3DR;->e:I

    move v0, v0

    .line 2567296
    new-instance v1, LX/IKA;

    invoke-direct {v1}, LX/IKA;-><init>()V

    move-object v1, v1

    .line 2567297
    const-string v2, "group_id"

    iget-object v3, p0, LX/IKl;->a:LX/IK9;

    .line 2567298
    iget-object p0, v3, LX/IK9;->b:Ljava/lang/String;

    move-object v3, p0

    .line 2567299
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2567300
    const-string v2, "channels_first"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2567301
    return-object v1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2567302
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567303
    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2567304
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2567305
    if-eqz v2, :cond_2

    .line 2567306
    const v0, 0x50d7aba7

    invoke-static {v1, v2, v7, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2567307
    const-class v6, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    invoke-virtual {v5, v4, v7, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2567308
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2567309
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 2567310
    :cond_1
    new-instance v0, LX/5Mb;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4}, LX/15i;->g(II)I

    move-result v2

    invoke-direct {v0, v3, v1, v2}, LX/5Mb;-><init>(LX/0Px;LX/15i;I)V

    .line 2567311
    :goto_2
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method
