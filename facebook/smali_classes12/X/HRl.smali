.class public final enum LX/HRl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HRl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HRl;

.field public static final enum WEEKLY_LIKE:LX/HRl;

.field public static final enum WEEKLY_POST_REACH:LX/HRl;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2465752
    new-instance v0, LX/HRl;

    const-string v1, "WEEKLY_LIKE"

    invoke-direct {v0, v1, v2}, LX/HRl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HRl;->WEEKLY_LIKE:LX/HRl;

    .line 2465753
    new-instance v0, LX/HRl;

    const-string v1, "WEEKLY_POST_REACH"

    invoke-direct {v0, v1, v3}, LX/HRl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HRl;->WEEKLY_POST_REACH:LX/HRl;

    .line 2465754
    const/4 v0, 0x2

    new-array v0, v0, [LX/HRl;

    sget-object v1, LX/HRl;->WEEKLY_LIKE:LX/HRl;

    aput-object v1, v0, v2

    sget-object v1, LX/HRl;->WEEKLY_POST_REACH:LX/HRl;

    aput-object v1, v0, v3

    sput-object v0, LX/HRl;->$VALUES:[LX/HRl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2465749
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HRl;
    .locals 1

    .prologue
    .line 2465751
    const-class v0, LX/HRl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HRl;

    return-object v0
.end method

.method public static values()[LX/HRl;
    .locals 1

    .prologue
    .line 2465750
    sget-object v0, LX/HRl;->$VALUES:[LX/HRl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HRl;

    return-object v0
.end method
