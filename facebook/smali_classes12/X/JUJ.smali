.class public LX/JUJ;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements Landroid/view/ViewStub$OnInflateListener;
.implements LX/2eZ;


# instance fields
.field public a:Z

.field private final b:Lcom/facebook/drawee/view/DraweeView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/LinearLayout;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/view/ViewStub;

.field public h:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

.field private final i:Landroid/view/View;

.field private final j:Lcom/facebook/maps/FbStaticMapView;

.field private final k:Landroid/view/ViewStub;

.field private l:Lcom/facebook/maps/FbMapViewDelegate;

.field private final m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final n:Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2698055
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 2698056
    const v0, 0x7f030715

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2698057
    const v0, 0x7f0d12e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, LX/JUJ;->b:Lcom/facebook/drawee/view/DraweeView;

    .line 2698058
    const v0, 0x7f0d12e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, LX/JUJ;->j:Lcom/facebook/maps/FbStaticMapView;

    .line 2698059
    const v0, 0x7f0d12e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/JUJ;->k:Landroid/view/ViewStub;

    .line 2698060
    const v0, 0x7f0d12e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/JUJ;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2698061
    const v0, 0x7f0d12ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;

    iput-object v0, p0, LX/JUJ;->n:Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;

    .line 2698062
    const v0, 0x7f0d12ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JUJ;->c:Landroid/widget/TextView;

    .line 2698063
    const v0, 0x7f0d12ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JUJ;->d:Landroid/widget/TextView;

    .line 2698064
    const v0, 0x7f0d12eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/JUJ;->e:Landroid/widget/LinearLayout;

    .line 2698065
    const v0, 0x7f0d12ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/JUJ;->f:Landroid/widget/ImageView;

    .line 2698066
    const v0, 0x7f0d12ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/JUJ;->g:Landroid/view/ViewStub;

    .line 2698067
    const v0, 0x7f0d12e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JUJ;->i:Landroid/view/View;

    .line 2698068
    iget-object v0, p0, LX/JUJ;->c:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2698069
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2698070
    new-instance v1, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v2

    .line 2698071
    iput-object v2, v1, LX/1Uo;->u:LX/4Ab;

    .line 2698072
    move-object v1, v1

    .line 2698073
    iput-object v0, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2698074
    move-object v0, v1

    .line 2698075
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2698076
    iget-object v1, p0, LX/JUJ;->b:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2698077
    iget-object v0, p0, LX/JUJ;->k:Landroid/view/ViewStub;

    invoke-virtual {v0, p0}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    .line 2698078
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;ILcom/facebook/common/callercontext/CallerContext;)V
    .locals 4
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2698044
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2698045
    :cond_0
    iget-object v0, p0, LX/JUJ;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2698046
    iget-object v0, p0, LX/JUJ;->n:Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->setVisibility(I)V

    .line 2698047
    :goto_0
    return-void

    .line 2698048
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2698049
    iget-object v1, p0, LX/JUJ;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2698050
    iget-object v0, p0, LX/JUJ;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2698051
    iget-object v0, p0, LX/JUJ;->n:Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->setVisibility(I)V

    goto :goto_0

    .line 2698052
    :cond_2
    iget-object v0, p0, LX/JUJ;->n:Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->a(LX/0Px;I)V

    .line 2698053
    iget-object v0, p0, LX/JUJ;->n:Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;

    invoke-virtual {v0, v3}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->setVisibility(I)V

    .line 2698054
    iget-object v0, p0, LX/JUJ;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(LX/3OL;LX/DHx;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 2698014
    if-nez p2, :cond_1

    .line 2698015
    iget-object v0, p0, LX/JUJ;->h:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    if-eqz v0, :cond_0

    .line 2698016
    iget-object v0, p0, LX/JUJ;->h:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setVisibility(I)V

    .line 2698017
    :cond_0
    :goto_0
    return-void

    .line 2698018
    :cond_1
    iget-object v0, p0, LX/JUJ;->h:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    if-nez v0, :cond_2

    .line 2698019
    iget-object v0, p0, LX/JUJ;->g:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iput-object v0, p0, LX/JUJ;->h:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    .line 2698020
    :cond_2
    iget-object v0, p0, LX/JUJ;->h:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setVisibility(I)V

    .line 2698021
    iget-object v0, p0, LX/JUJ;->h:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    invoke-virtual {v0, p1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setWaveState(LX/3OL;)V

    .line 2698022
    iget-object v0, p0, LX/JUJ;->h:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    .line 2698023
    iput-object p2, v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    .line 2698024
    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, p3, v0, v1}, LX/JUJ;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2698038
    if-nez p2, :cond_0

    .line 2698039
    iget-object v0, p0, LX/JUJ;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2698040
    :goto_0
    return-void

    .line 2698041
    :cond_0
    iget-object v0, p0, LX/JUJ;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2698042
    iget-object v0, p0, LX/JUJ;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2698043
    iget-object v0, p0, LX/JUJ;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2698037
    iget-boolean v0, p0, LX/JUJ;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2c620fda

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2698033
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2698034
    const/4 v1, 0x1

    .line 2698035
    iput-boolean v1, p0, LX/JUJ;->a:Z

    .line 2698036
    const/16 v1, 0x2d

    const v2, -0x4f7b5893

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6dbf56cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2698029
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2698030
    const/4 v1, 0x0

    .line 2698031
    iput-boolean v1, p0, LX/JUJ;->a:Z

    .line 2698032
    const/16 v1, 0x2d

    const v2, -0x6bc1133e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInflate(Landroid/view/ViewStub;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2698027
    check-cast p2, Lcom/facebook/maps/FbMapViewDelegate;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 2698028
    return-void
.end method

.method public setCenterCircleOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2698025
    iget-object v0, p0, LX/JUJ;->b:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2698026
    return-void
.end method

.method public setCenterImage(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2698079
    iget-object v0, p0, LX/JUJ;->b:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2698080
    iget-object v1, p0, LX/JUJ;->i:Landroid/view/View;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2698081
    return-void

    .line 2698082
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setExtraText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2697985
    iget-object v0, p0, LX/JUJ;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2697986
    return-void
.end method

.method public setNameText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2697987
    iget-object v0, p0, LX/JUJ;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2697988
    iget-object v0, p0, LX/JUJ;->b:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2697989
    return-void
.end method

.method public setOnLocationClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2697990
    iget-object v0, p0, LX/JUJ;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697991
    return-void
.end method

.method public setOnNameClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2697992
    iget-object v0, p0, LX/JUJ;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697993
    return-void
.end method

.method public setOnNameLocationSectionClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2697994
    iget-object v0, p0, LX/JUJ;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697995
    return-void
.end method

.method public setPageCoverMap(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V
    .locals 2
    .param p1    # Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2697996
    if-nez p1, :cond_0

    .line 2697997
    iget-object v0, p0, LX/JUJ;->j:Lcom/facebook/maps/FbStaticMapView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 2697998
    :goto_0
    return-void

    .line 2697999
    :cond_0
    iget-object v0, p0, LX/JUJ;->j:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, p1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2698000
    iget-object v0, p0, LX/JUJ;->j:Lcom/facebook/maps/FbStaticMapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPageCoverOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2698001
    iget-object v0, p0, LX/JUJ;->j:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, p1}, Lcom/facebook/maps/FbStaticMapView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2698002
    return-void
.end method

.method public setPulseMapReadyCallback(LX/6Zz;)V
    .locals 2

    .prologue
    .line 2698003
    if-nez p1, :cond_1

    .line 2698004
    iget-object v0, p0, LX/JUJ;->l:Lcom/facebook/maps/FbMapViewDelegate;

    if-eqz v0, :cond_0

    .line 2698005
    iget-object v0, p0, LX/JUJ;->l:Lcom/facebook/maps/FbMapViewDelegate;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbMapViewDelegate;->setVisibility(I)V

    .line 2698006
    :cond_0
    :goto_0
    return-void

    .line 2698007
    :cond_1
    iget-object v0, p0, LX/JUJ;->l:Lcom/facebook/maps/FbMapViewDelegate;

    if-nez v0, :cond_2

    .line 2698008
    iget-object v0, p0, LX/JUJ;->k:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, LX/JUJ;->l:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2698009
    :cond_2
    iget-object v0, p0, LX/JUJ;->l:Lcom/facebook/maps/FbMapViewDelegate;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbMapViewDelegate;->setVisibility(I)V

    .line 2698010
    iget-object v0, p0, LX/JUJ;->l:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p1}, LX/6Zn;->a(LX/6Zz;)V

    goto :goto_0
.end method

.method public setPulseProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2698011
    iget-object v0, p0, LX/JUJ;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2698012
    iget-object v0, p0, LX/JUJ;->n:Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2698013
    return-void
.end method
