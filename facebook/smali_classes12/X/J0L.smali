.class public LX/J0L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/70U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/70U",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636867
    return-void
.end method


# virtual methods
.method public final a()LX/6zQ;
    .locals 1

    .prologue
    .line 2636868
    sget-object v0, LX/6zQ;->NEW_PAY_OVER_COUNTER:LX/6zQ;

    return-object v0
.end method

.method public final a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
    .locals 2

    .prologue
    .line 2636869
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2636870
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2636871
    invoke-static {v0}, LX/6zQ;->forValue(Ljava/lang/String;)LX/6zQ;

    move-result-object v0

    sget-object v1, LX/6zQ;->NEW_PAY_OVER_COUNTER:LX/6zQ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2636872
    const-string v0, "provider"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2636873
    new-instance v1, Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;

    invoke-direct {v1, v0}, Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;-><init>(Ljava/lang/String;)V

    return-object v1

    .line 2636874
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
