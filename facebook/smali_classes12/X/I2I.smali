.class public LX/I2I;
.super LX/A8Z;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/A8Z",
        "<",
        "LX/A8X;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/I2F;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/I2B;

.field public c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/I2H;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;LX/I2B;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/I2F;",
            ">;",
            "LX/I2B;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2530118
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/A8Z;-><init>(LX/0Px;Z)V

    .line 2530119
    iput-object p1, p0, LX/I2I;->a:LX/0Px;

    .line 2530120
    iput-object p2, p0, LX/I2I;->b:LX/I2B;

    .line 2530121
    new-instance v0, Ljava/util/HashMap;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/I2I;->c:Ljava/util/HashMap;

    .line 2530122
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2530123
    iget-object v2, p0, LX/I2I;->c:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2F;

    .line 2530124
    iget-object v3, v0, LX/I2F;->l:LX/I2H;

    move-object v0, v3

    .line 2530125
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530126
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2530127
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;ZLX/I2H;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;Z",
            "LX/I2H;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2530128
    iget-object v0, p0, LX/I2I;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2530129
    iget-object v0, p0, LX/I2I;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2530130
    iget-object v1, p0, LX/I2I;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2F;

    .line 2530131
    iget-object v1, v0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2530132
    iget-object v1, v0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2530133
    invoke-static {v0}, LX/I2F;->g(LX/I2F;)V

    .line 2530134
    iput-boolean p2, v0, LX/I2F;->k:Z

    .line 2530135
    invoke-static {v0}, LX/I2F;->e(LX/I2F;)V

    .line 2530136
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2530137
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2530138
    :cond_0
    return-void
.end method
