.class public final LX/HYu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HYm;


# instance fields
.field public final synthetic a:LX/HYv;


# direct methods
.method public constructor <init>(LX/HYv;)V
    .locals 0

    .prologue
    .line 2481091
    iput-object p1, p0, LX/HYu;->a:LX/HYv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2481092
    iget-object v0, p0, LX/HYu;->a:LX/HYv;

    iget-object v0, v0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2481093
    iget-object v1, p0, LX/HYu;->a:LX/HYv;

    iget-object v3, v1, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HYi;

    invoke-virtual {v3, v1}, Lcom/facebook/registration/model/SimpleRegFormData;->e(LX/HYi;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2481094
    iget-object v1, p0, LX/HYu;->a:LX/HYv;

    iget-object v2, v1, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HYi;

    .line 2481095
    iput-object v1, v2, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    .line 2481096
    iget-object v1, p0, LX/HYu;->a:LX/HYv;

    iget-object v1, v1, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2481097
    iget-object v2, v1, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    move-object v1, v2

    .line 2481098
    sget-object v2, LX/HYi;->UNKNOWN:LX/HYi;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, LX/HYu;->a:LX/HYv;

    iget-object v1, v1, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v2, LX/HYi;->UNKNOWN:LX/HYi;

    invoke-virtual {v1, v2}, Lcom/facebook/registration/model/SimpleRegFormData;->e(LX/HYi;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2481099
    iget-object v1, p0, LX/HYu;->a:LX/HYv;

    iget-object v1, v1, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v2, LX/HYi;->UNKNOWN:LX/HYi;

    invoke-virtual {v1, v2}, Lcom/facebook/registration/model/SimpleRegFormData;->b(LX/HYi;)V

    .line 2481100
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HYm;

    invoke-interface {v0}, LX/HYm;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2481101
    :goto_0
    return-object v0

    .line 2481102
    :cond_2
    iget-object v0, p0, LX/HYu;->a:LX/HYv;

    iget-object v0, v0, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v1, LX/HYi;->UNKNOWN:LX/HYi;

    .line 2481103
    iput-object v1, v0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    .line 2481104
    iget-object v0, p0, LX/HYu;->a:LX/HYv;

    iget-object v0, v0, LX/HYv;->h:Ljava/util/LinkedHashMap;

    sget-object v1, LX/HYi;->UNKNOWN:LX/HYi;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HYm;

    invoke-interface {v0}, LX/HYm;->a()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
