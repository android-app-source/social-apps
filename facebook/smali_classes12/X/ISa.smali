.class public final LX/ISa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V
    .locals 0

    .prologue
    .line 2578067
    iput-object p1, p0, LX/ISa;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    .line 2578068
    iget-object v0, p0, LX/ISa;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/ISa;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2578069
    const-string v3, "groups_seeds_composer_create_album"

    invoke-static {v3}, LX/ISn;->a(Ljava/lang/String;)V

    .line 2578070
    sget-object v3, LX/ISn;->c:LX/9at;

    sget-object v4, LX/9au;->COMPOSER:LX/9au;

    const/4 v5, 0x0

    new-instance v6, LX/89I;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    sget-object v9, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v6, v7, v8, v9}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v6}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/content/Intent;

    move-result-object v3

    .line 2578071
    sget-object v4, LX/ISn;->a:Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x7c7

    invoke-interface {v4, v3, v5, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2578072
    const/4 v0, 0x1

    return v0
.end method
