.class public interface abstract LX/JBK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JAX;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Forwarder;
    processor = "com.facebook.dracula.transformer.Transformer"
    to = "CollectionWithItemsAndSuggestions$"
.end annotation


# virtual methods
.method public abstract b()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract j()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract mW_()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract o()Z
.end method

.method public abstract p()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
