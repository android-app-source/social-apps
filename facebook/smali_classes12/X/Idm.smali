.class public abstract LX/Idm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Idf;


# static fields
.field private static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final j:Ljava/util/regex/Pattern;


# instance fields
.field public a:Landroid/content/ContentResolver;

.field public b:I

.field public c:Landroid/net/Uri;

.field public d:Landroid/database/Cursor;

.field public e:Ljava/lang/String;

.field public f:Landroid/net/Uri;

.field public g:Z

.field private final i:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Integer;",
            "LX/Idl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2597900
    const-class v0, LX/Idm;

    sput-object v0, LX/Idm;->h:Ljava/lang/Class;

    .line 2597901
    const-string v0, "(.*)/\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/Idm;->j:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2597902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2597903
    new-instance v0, LX/0aq;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/Idm;->i:LX/0aq;

    .line 2597904
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Idm;->g:Z

    .line 2597905
    iput p3, p0, LX/Idm;->b:I

    .line 2597906
    iput-object p2, p0, LX/Idm;->c:Landroid/net/Uri;

    .line 2597907
    iput-object p4, p0, LX/Idm;->e:Ljava/lang/String;

    .line 2597908
    iput-object p1, p0, LX/Idm;->a:Landroid/content/ContentResolver;

    .line 2597909
    invoke-virtual {p0}, LX/Idm;->d()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    .line 2597910
    iget-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 2597911
    sget-object v0, LX/Idm;->h:Ljava/lang/Class;

    const-string v1, "createCursor returns null."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2597912
    :cond_0
    iget-object v0, p0, LX/Idm;->i:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->a()V

    .line 2597913
    return-void
.end method

.method private c(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    .line 2597914
    iget-object v0, p0, LX/Idm;->c:Landroid/net/Uri;

    .line 2597915
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/IdZ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/IdZ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/IdZ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2597916
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 2597917
    sget-object v2, LX/Idm;->j:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 2597918
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    move-object v1, v1

    .line 2597919
    invoke-static {v0, v1}, LX/IdZ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 2597930
    monitor-enter p0

    .line 2597931
    :try_start_0
    iget-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    .line 2597932
    :goto_0
    return-object v0

    .line 2597933
    :cond_0
    iget-boolean v0, p0, LX/Idm;->g:Z

    if-eqz v0, :cond_1

    .line 2597934
    iget-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    .line 2597935
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Idm;->g:Z

    .line 2597936
    :cond_1
    iget-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    monitor-exit p0

    goto :goto_0

    .line 2597937
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(I)LX/Idk;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2597920
    iget-object v0, p0, LX/Idm;->i:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idl;

    .line 2597921
    if-nez v0, :cond_0

    .line 2597922
    invoke-direct {p0}, LX/Idm;->f()Landroid/database/Cursor;

    move-result-object v0

    .line 2597923
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2597924
    :cond_0
    :goto_0
    return-object v0

    .line 2597925
    :cond_1
    monitor-enter p0

    .line 2597926
    :try_start_0
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v0}, LX/Idm;->a(Landroid/database/Cursor;)LX/Idl;

    move-result-object v0

    .line 2597927
    :goto_1
    iget-object v1, p0, LX/Idm;->i:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2597928
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    .line 2597929
    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;)LX/Idk;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2597882
    invoke-direct {p0, p1}, LX/Idm;->c(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2597883
    :cond_0
    :goto_0
    return-object v0

    .line 2597884
    :cond_1
    :try_start_0
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2597885
    invoke-direct {p0}, LX/Idm;->f()Landroid/database/Cursor;

    move-result-object v4

    .line 2597886
    if-eqz v4, :cond_0

    .line 2597887
    monitor-enter p0

    .line 2597888
    const/4 v5, -0x1

    :try_start_1
    invoke-interface {v4, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2597889
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2597890
    invoke-virtual {p0, v4}, LX/Idm;->b(Landroid/database/Cursor;)J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-nez v5, :cond_3

    .line 2597891
    iget-object v0, p0, LX/Idm;->i:LX/0aq;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idl;

    .line 2597892
    if-nez v0, :cond_2

    .line 2597893
    invoke-virtual {p0, v4}, LX/Idm;->a(Landroid/database/Cursor;)LX/Idl;

    move-result-object v0

    .line 2597894
    iget-object v2, p0, LX/Idm;->i:LX/0aq;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2597895
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 2597896
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2597897
    :catch_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    goto :goto_0

    .line 2597898
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2597899
    :cond_4
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public abstract a(Landroid/database/Cursor;)LX/Idl;
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 2597862
    :try_start_0
    iget-object v0, p0, LX/Idm;->c:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 2597863
    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    sget-object v0, LX/Idm;->h:Ljava/lang/Class;

    const-string v1, "id mismatch"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2597864
    :cond_0
    iget-object v0, p0, LX/Idm;->c:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2597865
    :goto_0
    return-object v0

    :catch_0
    iget-object v0, p0, LX/Idm;->c:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2597872
    :try_start_0
    iget-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    if-nez v0, :cond_1
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2597873
    :goto_0
    iput-object v3, p0, LX/Idm;->a:Landroid/content/ContentResolver;

    .line 2597874
    iget-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 2597875
    iget-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2597876
    iput-object v3, p0, LX/Idm;->d:Landroid/database/Cursor;

    .line 2597877
    :cond_0
    return-void

    .line 2597878
    :catch_0
    move-exception v0

    .line 2597879
    sget-object v1, LX/Idm;->h:Ljava/lang/Class;

    const-string v2, "Caught exception while deactivating cursor."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2597880
    :cond_1
    iget-object v0, p0, LX/Idm;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 2597881
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Idm;->g:Z

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2597866
    invoke-direct {p0}, LX/Idm;->f()Landroid/database/Cursor;

    move-result-object v0

    .line 2597867
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2597868
    :goto_0
    return v0

    .line 2597869
    :cond_0
    monitor-enter p0

    .line 2597870
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    monitor-exit p0

    goto :goto_0

    .line 2597871
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract b(Landroid/database/Cursor;)J
.end method

.method public abstract d()Landroid/database/Cursor;
.end method
