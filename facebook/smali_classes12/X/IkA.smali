.class public LX/IkA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/concurrent/Executor;

.field private b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;LX/03V;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606467
    iput-object p1, p0, LX/IkA;->a:Ljava/util/concurrent/Executor;

    .line 2606468
    iput-object p2, p0, LX/IkA;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2606469
    iput-object p3, p0, LX/IkA;->c:LX/03V;

    .line 2606470
    return-void
.end method

.method public static b(LX/0QB;)LX/IkA;
    .locals 4

    .prologue
    .line 2606471
    new-instance v3, LX/IkA;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v0, v1, v2}, LX/IkA;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;LX/03V;)V

    .line 2606472
    return-object v3
.end method
