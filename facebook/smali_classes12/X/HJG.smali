.class public final LX/HJG;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HJH;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public b:LX/HLF;

.field public c:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/HJH;


# direct methods
.method public constructor <init>(LX/HJH;)V
    .locals 1

    .prologue
    .line 2452307
    iput-object p1, p0, LX/HJG;->d:LX/HJH;

    .line 2452308
    move-object v0, p1

    .line 2452309
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2452310
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2452311
    const-string v0, "PageFriendsCityActivityComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2452312
    if-ne p0, p1, :cond_1

    .line 2452313
    :cond_0
    :goto_0
    return v0

    .line 2452314
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2452315
    goto :goto_0

    .line 2452316
    :cond_3
    check-cast p1, LX/HJG;

    .line 2452317
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2452318
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2452319
    if-eq v2, v3, :cond_0

    .line 2452320
    iget-object v2, p0, LX/HJG;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HJG;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HJG;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2452321
    goto :goto_0

    .line 2452322
    :cond_5
    iget-object v2, p1, LX/HJG;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_4

    .line 2452323
    :cond_6
    iget-object v2, p0, LX/HJG;->b:LX/HLF;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HJG;->b:LX/HLF;

    iget-object v3, p1, LX/HJG;->b:LX/HLF;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2452324
    goto :goto_0

    .line 2452325
    :cond_8
    iget-object v2, p1, LX/HJG;->b:LX/HLF;

    if-nez v2, :cond_7

    .line 2452326
    :cond_9
    iget-object v2, p0, LX/HJG;->c:LX/2km;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/HJG;->c:LX/2km;

    iget-object v3, p1, LX/HJG;->c:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2452327
    goto :goto_0

    .line 2452328
    :cond_a
    iget-object v2, p1, LX/HJG;->c:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
