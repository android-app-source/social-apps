.class public final LX/IjS;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/IjT;


# direct methods
.method public constructor <init>(LX/IjT;)V
    .locals 0

    .prologue
    .line 2605741
    iput-object p1, p0, LX/IjS;->a:LX/IjT;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2605742
    sget-object v0, LX/IjT;->a:Ljava/lang/Class;

    const-string v1, "Card failed to set to be primary"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2605743
    iget-object v0, p0, LX/IjS;->a:LX/IjT;

    iget-object v0, v0, LX/IjT;->e:LX/03V;

    const-string v1, "P2P_PAYMENT_CARD_SET_PRIMARY_FAILED"

    const-string v2, "Attempted to set a paymentcard as primary, but received a response with an error"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2605744
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2605745
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v0, v1, :cond_0

    .line 2605746
    iget-object v0, p0, LX/IjS;->a:LX/IjT;

    iget-object v0, v0, LX/IjT;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2605747
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2605748
    iget-object v0, p0, LX/IjS;->a:LX/IjT;

    iget-object v0, v0, LX/IjT;->h:LX/6qh;

    if-eqz v0, :cond_0

    .line 2605749
    new-instance v0, LX/73T;

    sget-object v1, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v0, v1}, LX/73T;-><init>(LX/73S;)V

    .line 2605750
    iget-object v1, p0, LX/IjS;->a:LX/IjT;

    iget-object v1, v1, LX/IjT;->h:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(LX/73T;)V

    .line 2605751
    :cond_0
    return-void
.end method
