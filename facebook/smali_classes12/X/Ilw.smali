.class public final LX/Ilw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;)V
    .locals 0

    .prologue
    .line 2608565
    iput-object p1, p0, LX/Ilw;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xec75701

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2608566
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "https://m.facebook.com/help/messenger-app/369959656515129"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 2608567
    iget-object v2, p0, LX/Ilw;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;

    iget-object v2, v2, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Ilw;->a:Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2608568
    const v1, -0x73146037

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
