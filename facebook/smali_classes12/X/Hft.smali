.class public final LX/Hft;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupLeaveCoreMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hfu;


# direct methods
.method public constructor <init>(LX/Hfu;)V
    .locals 0

    .prologue
    .line 2492529
    iput-object p1, p0, LX/Hft;->a:LX/Hfu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2492530
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2492531
    iget-object v0, p0, LX/Hft;->a:LX/Hfu;

    iget-object v0, v0, LX/Hfu;->c:LX/Hfh;

    .line 2492532
    sget-object p0, LX/Hfk;->a:[I

    iget-object p1, v0, LX/Hfh;->a:LX/Hfw;

    invoke-virtual {p1}, LX/Hfw;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 2492533
    :goto_0
    return-void

    .line 2492534
    :pswitch_0
    iget-object p0, v0, LX/Hfh;->b:LX/95R;

    iget-object p1, v0, LX/Hfh;->c:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    invoke-static {p0, p1}, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->b(LX/95R;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)V

    goto :goto_0

    .line 2492535
    :pswitch_1
    iget-object p0, v0, LX/Hfh;->d:LX/95R;

    iget-object p1, v0, LX/Hfh;->c:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    invoke-static {p0, p1}, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->b(LX/95R;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)V

    goto :goto_0

    .line 2492536
    :pswitch_2
    iget-object p0, v0, LX/Hfh;->e:LX/95R;

    iget-object p1, v0, LX/Hfh;->c:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    invoke-static {p0, p1}, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->b(LX/95R;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)V

    goto :goto_0

    .line 2492537
    :pswitch_3
    iget-object p0, v0, LX/Hfh;->f:LX/95R;

    iget-object p1, v0, LX/Hfh;->c:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    invoke-static {p0, p1}, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->b(LX/95R;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
