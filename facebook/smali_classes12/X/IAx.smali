.class public final LX/IAx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;)V
    .locals 0

    .prologue
    .line 2546307
    iput-object p1, p0, LX/IAx;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2546308
    iget-object v1, p0, LX/IAx;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    iget-object v0, p0, LX/IAx;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2546309
    iget-object v2, v1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->e:Ljava/util/Set;

    .line 2546310
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2546311
    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2546312
    iget-object v2, v1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->e:Ljava/util/Set;

    .line 2546313
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2546314
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2546315
    :goto_0
    iget-object v2, v1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->j:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    const/16 p2, 0x8

    const/4 v4, 0x0

    .line 2546316
    iget-object v3, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    invoke-virtual {v3}, LX/IB5;->d()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result p0

    .line 2546317
    iput p0, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->j:I

    .line 2546318
    if-lez p0, :cond_1

    const/4 v3, 0x1

    :goto_1
    iput-boolean v3, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->i:Z

    .line 2546319
    iget-object v3, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->g:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    iget-boolean p1, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->i:Z

    .line 2546320
    iput-boolean p1, v3, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 2546321
    iget-object v3, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->f:LX/1ZF;

    iget-object p1, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->g:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v3, p1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2546322
    rsub-int/lit8 v3, p0, 0x32

    .line 2546323
    if-ltz v3, :cond_2

    const/16 p1, 0x14

    if-gt v3, p1, :cond_2

    .line 2546324
    iget-object p1, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2546325
    iget-object p1, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2546326
    iget-object v4, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2546327
    if-nez v3, :cond_3

    .line 2546328
    const p1, 0x7f081f7f

    invoke-virtual {v2, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2546329
    :goto_2
    move-object p1, p1

    .line 2546330
    invoke-virtual {v4, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2546331
    iget-object v4, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2546332
    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 2546333
    if-gtz v3, :cond_4

    const p1, 0x7f0a0698

    .line 2546334
    :goto_3
    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    move v3, p1

    .line 2546335
    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundColor(I)V

    .line 2546336
    :goto_4
    iget-object v3, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->f:LX/1ZF;

    invoke-static {v2, p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->a(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2546337
    iget-boolean v2, v0, Lcom/facebook/events/model/EventUser;->k:Z

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_5
    iput-boolean v2, v0, Lcom/facebook/events/model/EventUser;->k:Z

    .line 2546338
    iget-object v2, v1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->k:LX/IAt;

    const v3, 0x24131c99

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2546339
    return-void

    .line 2546340
    :cond_0
    iget-object v2, v1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->e:Ljava/util/Set;

    .line 2546341
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2546342
    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v3, v4

    .line 2546343
    goto :goto_1

    .line 2546344
    :cond_2
    iget-object v3, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, p2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2546345
    iget-object v3, v2, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_4

    :cond_3
    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0f00e6

    const/4 p4, 0x1

    new-array p4, p4, [Ljava/lang/Object;

    const/4 p5, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, p4, p5

    invoke-virtual {p1, p2, v3, p4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 2546346
    :cond_4
    const p1, 0x7f0a0699

    goto :goto_3

    .line 2546347
    :cond_5
    const/4 v2, 0x0

    goto :goto_5
.end method
