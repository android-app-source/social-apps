.class public final LX/I50;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V
    .locals 0

    .prologue
    .line 2534687
    iput-object p1, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 6

    .prologue
    .line 2534688
    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-boolean v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->L:Z

    if-nez v0, :cond_1

    .line 2534689
    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-static {v0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->d$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    .line 2534690
    :cond_0
    :goto_0
    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v1, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    .line 2534691
    iput v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->C:I

    .line 2534692
    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    .line 2534693
    iput p1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    .line 2534694
    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    const/4 v1, 0x0

    .line 2534695
    iput-boolean v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->L:Z

    .line 2534696
    return-void

    .line 2534697
    :cond_1
    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->e:LX/1nQ;

    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    .line 2534698
    iget-object v3, v1, LX/1nQ;->i:LX/0Zb;

    const-string v4, "selected_time_filter"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2534699
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2534700
    const-string v4, "event_discovery"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "time_token"

    invoke-virtual {v3, v4, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "event_suggestion_token"

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2534701
    :cond_2
    iget-object v0, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget-object v1, p0, LX/I50;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    invoke-virtual {v0, v1}, LX/0gF;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    .line 2534702
    if-eqz v0, :cond_0

    .line 2534703
    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->P()V

    goto :goto_0
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2534704
    return-void
.end method
