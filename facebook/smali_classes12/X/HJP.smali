.class public final LX/HJP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic b:LX/HL6;

.field public final synthetic c:LX/2km;

.field public final synthetic d:LX/HLF;

.field public final synthetic e:Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HL6;LX/2km;LX/HLF;)V
    .locals 0

    .prologue
    .line 2452668
    iput-object p1, p0, LX/HJP;->e:Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;

    iput-object p2, p0, LX/HJP;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p3, p0, LX/HJP;->b:LX/HL6;

    iput-object p4, p0, LX/HJP;->c:LX/2km;

    iput-object p5, p0, LX/HJP;->d:LX/HLF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2452657
    iget-object v0, p0, LX/HJP;->e:Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;

    iget-object v0, v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->d:LX/9XE;

    iget-object v1, p0, LX/HJP;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452658
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v2

    .line 2452659
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/HJP;->b:LX/HL6;

    iget-object v1, v1, LX/HL6;->a:Ljava/lang/String;

    .line 2452660
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    sget-object v6, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_ERROR:LX/9XA;

    invoke-static {v6, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "tapped_friend_id"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2452661
    iget-object v0, p0, LX/HJP;->c:LX/2km;

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/HJP;->d:LX/HLF;

    iget-object v2, p0, LX/HJP;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    .line 2452662
    iput-boolean v4, v0, LX/HLG;->a:Z

    .line 2452663
    iget-object v1, p0, LX/HJP;->c:LX/2km;

    check-cast v1, LX/1Pr;

    iget-object v2, p0, LX/HJP;->d:LX/HLF;

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2452664
    iget-object v0, p0, LX/HJP;->c:LX/2km;

    check-cast v0, LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/HJP;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452665
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v3

    .line 2452666
    aput-object v2, v1, v4

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2452667
    return-void
.end method
