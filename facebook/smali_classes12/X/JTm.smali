.class public final LX/JTm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JTn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1SX;

.field public c:LX/2ei;

.field public final synthetic d:LX/JTn;


# direct methods
.method public constructor <init>(LX/JTn;)V
    .locals 1

    .prologue
    .line 2697229
    iput-object p1, p0, LX/JTm;->d:LX/JTn;

    .line 2697230
    move-object v0, p1

    .line 2697231
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2697232
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2697211
    const-string v0, "FriendsLocationsHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2697212
    if-ne p0, p1, :cond_1

    .line 2697213
    :cond_0
    :goto_0
    return v0

    .line 2697214
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2697215
    goto :goto_0

    .line 2697216
    :cond_3
    check-cast p1, LX/JTm;

    .line 2697217
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2697218
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2697219
    if-eq v2, v3, :cond_0

    .line 2697220
    iget-object v2, p0, LX/JTm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JTm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JTm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2697221
    goto :goto_0

    .line 2697222
    :cond_5
    iget-object v2, p1, LX/JTm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2697223
    :cond_6
    iget-object v2, p0, LX/JTm;->b:LX/1SX;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JTm;->b:LX/1SX;

    iget-object v3, p1, LX/JTm;->b:LX/1SX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2697224
    goto :goto_0

    .line 2697225
    :cond_8
    iget-object v2, p1, LX/JTm;->b:LX/1SX;

    if-nez v2, :cond_7

    .line 2697226
    :cond_9
    iget-object v2, p0, LX/JTm;->c:LX/2ei;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JTm;->c:LX/2ei;

    iget-object v3, p1, LX/JTm;->c:LX/2ei;

    invoke-virtual {v2, v3}, LX/2ei;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2697227
    goto :goto_0

    .line 2697228
    :cond_a
    iget-object v2, p1, LX/JTm;->c:LX/2ei;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
