.class public final LX/HxC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HxG;


# direct methods
.method public constructor <init>(LX/HxG;)V
    .locals 0

    .prologue
    .line 2522176
    iput-object p1, p0, LX/HxC;->a:LX/HxG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x1abf8702

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522177
    iget-object v1, p0, LX/HxC;->a:LX/HxG;

    iget-object v1, v1, LX/HxG;->c:LX/HxN;

    iget-object v2, p0, LX/HxC;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    iget-object v3, p0, LX/HxC;->a:LX/HxG;

    iget-boolean v3, v3, LX/HxG;->p:Z

    iget-object v4, p0, LX/HxC;->a:LX/HxG;

    iget-object v4, v4, LX/HxG;->r:Ljava/lang/String;

    iget-object v5, p0, LX/HxC;->a:LX/HxG;

    iget-object v5, v5, LX/HxG;->j:Lcom/facebook/base/fragment/FbFragment;

    .line 2522178
    sget-object v7, LX/21D;->EVENT:LX/21D;

    const-string v8, "eventDashboardCelebrations"

    invoke-static {v7, v8}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    invoke-static {v2}, LX/HxN;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    invoke-virtual {v1}, LX/HxN;->a()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    const-string v8, "ANDROID_EVENTS_DASHBOARD_COMPOSER"

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v7

    .line 2522179
    iget-object v8, v1, LX/HxN;->b:LX/7v8;

    invoke-virtual {v8, v3, v4}, LX/7v8;->a(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2522180
    iget-object p0, v1, LX/HxN;->a:LX/1Kf;

    const/16 p1, 0x6dc

    invoke-interface {p0, v8, v7, p1, v5}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2522181
    const v1, -0x2e0fd4e6

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
