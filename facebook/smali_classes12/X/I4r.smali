.class public final LX/I4r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;)V
    .locals 0

    .prologue
    .line 2534536
    iput-object p1, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x348829d0    # -1.6242224E7f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2534537
    iget-object v1, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->q:LX/I4m;

    .line 2534538
    iget-object v3, v2, LX/I4m;->c:Ljava/util/HashSet;

    move-object v2, v3

    .line 2534539
    iput-object v2, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->s:Ljava/util/HashSet;

    .line 2534540
    iget-object v1, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->q:LX/I4m;

    .line 2534541
    iget-object v3, v2, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, v3

    .line 2534542
    iput-object v2, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534543
    iget-object v1, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->x:LX/I4y;

    iget-object v2, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->s:Ljava/util/HashSet;

    iget-object v3, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v3, v3, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iget-object v4, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget v4, v4, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->v:I

    .line 2534544
    iget-object v6, v1, LX/I4y;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    .line 2534545
    iput-object v2, v6, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    .line 2534546
    iget-object v6, v1, LX/I4y;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    .line 2534547
    iput-object v3, v6, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534548
    iget-object v6, v1, LX/I4y;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    .line 2534549
    iput v4, v6, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    .line 2534550
    iget-object v6, v1, LX/I4y;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v6, v6, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget-object v7, v1, LX/I4y;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v7, v7, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-static {}, LX/H4F;->values()[LX/H4F;

    move-result-object v8

    iget-object v9, v1, LX/I4y;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget v9, v9, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    aget-object v8, v8, v9

    invoke-virtual {v8}, LX/H4F;->getValue()I

    move-result v8

    new-instance v9, Ljava/util/ArrayList;

    iget-object v10, v1, LX/I4y;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v10, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2534551
    iput-object v7, v6, LX/I5K;->c:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534552
    iput v8, v6, LX/I5K;->d:I

    .line 2534553
    iput-object v9, v6, LX/I5K;->h:Ljava/util/ArrayList;

    .line 2534554
    const/4 v10, 0x0

    move v11, v10

    :goto_0
    invoke-virtual {v6}, LX/0gG;->b()I

    move-result v10

    if-ge v11, v10, :cond_1

    .line 2534555
    invoke-virtual {v6, v11}, LX/0gF;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    .line 2534556
    if-eqz v10, :cond_0

    .line 2534557
    iput-object v7, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->t:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534558
    iput v8, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->u:I

    .line 2534559
    iput-object v9, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->w:Ljava/util/List;

    .line 2534560
    invoke-static {v10}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->Q(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;)V

    .line 2534561
    iget-object v12, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->r:Ljava/lang/String;

    iget-object p1, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->s:Ljava/lang/String;

    invoke-static {v12, p1, v7, v8, v9}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;ILjava/util/List;)LX/0m9;

    move-result-object v12

    iput-object v12, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->n:LX/0m9;

    .line 2534562
    iget-object v12, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->i:LX/I5M;

    const-string p1, "ANDROID_EVENT_DISCOVER_EVENT_LIST"

    iget-object v2, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->j:LX/I5L;

    iget-object v3, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->n:LX/0m9;

    iget-object v4, v10, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->q:Ljava/lang/String;

    invoke-virtual {v12, p1, v2, v3, v4}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    .line 2534563
    :cond_0
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_0

    .line 2534564
    :cond_1
    iget-object v6, v1, LX/I4y;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-static {v6}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->m(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    .line 2534565
    iget-object v1, p0, LX/I4r;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2534566
    const v1, -0xc2cf2ec

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
