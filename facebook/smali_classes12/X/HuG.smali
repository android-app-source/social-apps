.class public LX/HuG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/HsK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j0;",
        ":",
        "LX/0j8;",
        "DerivedData::",
        "LX/5RE;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/ipc/composer/model/ComposerLocationInfo$SetsLocationInfo",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/HsK;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/0jK;

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final e:LX/HqR;

.field public final f:LX/03V;

.field public final g:LX/0gd;

.field public final h:LX/0ad;

.field public final i:LX/0tX;

.field private final j:LX/0rq;

.field public final k:Landroid/content/res/Resources;

.field public final l:Lcom/facebook/maps/rows/MapPartDefinition;

.field public final m:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

.field public final n:LX/0wM;

.field public final o:LX/3BL;

.field private final p:Ljava/util/concurrent/Executor;

.field private final q:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final r:Landroid/view/View$OnClickListener;

.field public s:Z

.field public t:Z

.field public u:LX/Hsu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2517337
    const-class v0, LX/HuG;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HuG;->a:Ljava/lang/String;

    .line 2517338
    const-class v0, LX/HuG;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/HuG;->b:LX/0jK;

    .line 2517339
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/HuG;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/HqR;LX/03V;LX/0gd;LX/0ad;Lcom/facebook/maps/rows/MapPartDefinition;LX/0tX;LX/0rq;Landroid/content/res/Resources;Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;LX/0wM;LX/3BL;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/HqR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/composer/location/feedattachment/CheckinPreviewAttachment$Callback;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0gd;",
            "LX/0ad;",
            "Lcom/facebook/maps/rows/MapPartDefinition;",
            "LX/0tX;",
            "LX/0rq;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;",
            "LX/0wM;",
            "LX/3BL;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517321
    new-instance v0, LX/HuC;

    invoke-direct {v0, p0}, LX/HuC;-><init>(LX/HuG;)V

    iput-object v0, p0, LX/HuG;->q:LX/0TF;

    .line 2517322
    new-instance v0, LX/HuD;

    invoke-direct {v0, p0}, LX/HuD;-><init>(LX/HuG;)V

    iput-object v0, p0, LX/HuG;->r:Landroid/view/View$OnClickListener;

    .line 2517323
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HuG;->d:Ljava/lang/ref/WeakReference;

    .line 2517324
    iput-object p2, p0, LX/HuG;->e:LX/HqR;

    .line 2517325
    iput-object p3, p0, LX/HuG;->f:LX/03V;

    .line 2517326
    iput-object p4, p0, LX/HuG;->g:LX/0gd;

    .line 2517327
    iput-object p5, p0, LX/HuG;->h:LX/0ad;

    .line 2517328
    iput-object p6, p0, LX/HuG;->l:Lcom/facebook/maps/rows/MapPartDefinition;

    .line 2517329
    iput-object p7, p0, LX/HuG;->i:LX/0tX;

    .line 2517330
    iput-object p8, p0, LX/HuG;->j:LX/0rq;

    .line 2517331
    iput-object p9, p0, LX/HuG;->k:Landroid/content/res/Resources;

    .line 2517332
    iput-object p10, p0, LX/HuG;->m:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    .line 2517333
    iput-object p11, p0, LX/HuG;->n:LX/0wM;

    .line 2517334
    iput-object p12, p0, LX/HuG;->o:LX/3BL;

    .line 2517335
    iput-object p13, p0, LX/HuG;->p:Ljava/util/concurrent/Executor;

    .line 2517336
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2517311
    iput-object p1, p0, LX/HuG;->w:Ljava/lang/String;

    .line 2517312
    iget-object v0, p0, LX/HuG;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517313
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j0;

    check-cast v0, LX/0j8;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 2517314
    new-instance v1, LX/HuI;

    invoke-direct {v1}, LX/HuI;-><init>()V

    move-object v1, v1

    .line 2517315
    const-string v2, "page_id"

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/HuG;->k:Landroid/content/res/Resources;

    const p1, 0x7f0b0b53

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2517316
    iget-object v0, p0, LX/HuG;->i:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    move-object v0, v0

    .line 2517317
    iput-object v0, p0, LX/HuG;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2517318
    iget-object v0, p0, LX/HuG;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, LX/HuG;->q:LX/0TF;

    iget-object v2, p0, LX/HuG;->p:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2517319
    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2517309
    iget-object v0, p0, LX/HuG;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517310
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j0;

    check-cast v0, LX/0j8;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 2517340
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2517302
    new-instance v0, LX/Hsu;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Hsu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/HuG;->u:LX/Hsu;

    .line 2517303
    iget-object v0, p0, LX/HuG;->u:LX/Hsu;

    invoke-virtual {v0, v2}, LX/Hsu;->setLoadingIndicatorVisibility(Z)V

    .line 2517304
    iget-object v0, p0, LX/HuG;->u:LX/Hsu;

    invoke-virtual {v0, v2}, LX/Hsu;->setShowRemoveButton(Z)V

    .line 2517305
    iget-object v0, p0, LX/HuG;->u:LX/Hsu;

    iget-object v1, p0, LX/HuG;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/Hsu;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2517306
    iget-object v0, p0, LX/HuG;->u:LX/Hsu;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2517307
    invoke-direct {p0}, LX/HuG;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/HuG;->a(Ljava/lang/String;)V

    .line 2517308
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2517297
    iget-object v0, p0, LX/HuG;->u:LX/Hsu;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/HuG;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2517298
    :cond_0
    :goto_0
    return-void

    .line 2517299
    :cond_1
    invoke-direct {p0}, LX/HuG;->e()Ljava/lang/String;

    move-result-object v0

    .line 2517300
    iget-object v1, p0, LX/HuG;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2517301
    invoke-direct {p0, v0}, LX/HuG;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2517287
    iget-object v0, p0, LX/HuG;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517288
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v1, LX/5RF;->CHECKIN:LX/5RF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2517289
    iget-object v0, p0, LX/HuG;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2517290
    iget-object v0, p0, LX/HuG;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2517291
    iput-object v2, p0, LX/HuG;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2517292
    :cond_0
    iget-object v0, p0, LX/HuG;->u:LX/Hsu;

    .line 2517293
    move-object v0, v0

    .line 2517294
    check-cast v0, LX/Hsu;

    invoke-virtual {v0, v2}, LX/Hsu;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2517295
    iput-object v2, p0, LX/HuG;->u:LX/Hsu;

    .line 2517296
    return-void
.end method
