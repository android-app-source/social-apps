.class public final LX/ISd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V
    .locals 0

    .prologue
    .line 2578090
    iput-object p1, p0, LX/ISd;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2578091
    iget-object v0, p0, LX/ISd;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/ISd;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2578092
    const-string v2, "groups_seeds_composer_create_file"

    invoke-static {v2}, LX/ISn;->a(Ljava/lang/String;)V

    .line 2578093
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    sget-object v2, LX/ISn;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    invoke-virtual {p0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 2578094
    const-string p0, "group_feed_id"

    invoke-virtual {v2, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2578095
    const-string p0, "groups_launch_file_selector"

    const/4 p1, 0x1

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2578096
    const-string p0, "target_fragment"

    sget-object p1, LX/0cQ;->GROUP_FILES_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2578097
    sget-object p0, LX/ISn;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2578098
    const/4 v0, 0x1

    return v0
.end method
