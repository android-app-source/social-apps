.class public final LX/IPa;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/IPc;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/IPb;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x7

    .line 2574343
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2574344
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "section"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "recyclerConfiguration"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "emptyComponent"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "loadingComponent"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "isOpen"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "clickListener"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "recyclerEventsController"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/IPa;->b:[Ljava/lang/String;

    .line 2574345
    iput v3, p0, LX/IPa;->c:I

    .line 2574346
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/IPa;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/IPa;LX/1De;IILX/IPb;)V
    .locals 1

    .prologue
    .line 2574302
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2574303
    iput-object p4, p0, LX/IPa;->a:LX/IPb;

    .line 2574304
    iget-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2574305
    return-void
.end method


# virtual methods
.method public final a(LX/1X1;)LX/IPa;
    .locals 2

    .prologue
    .line 2574339
    iget-object v1, p0, LX/IPa;->a:LX/IPb;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, v1, LX/IPb;->c:LX/1X1;

    .line 2574340
    iget-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2574341
    return-object p0

    .line 2574342
    :cond_0
    invoke-virtual {p1}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/5K7;)LX/IPa;
    .locals 2

    .prologue
    .line 2574336
    iget-object v0, p0, LX/IPa;->a:LX/IPb;

    iput-object p1, v0, LX/IPb;->g:LX/5K7;

    .line 2574337
    iget-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2574338
    return-object p0
.end method

.method public final a(LX/BcO;)LX/IPa;
    .locals 2

    .prologue
    .line 2574333
    iget-object v0, p0, LX/IPa;->a:LX/IPb;

    iput-object p1, v0, LX/IPb;->a:LX/BcO;

    .line 2574334
    iget-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2574335
    return-object p0
.end method

.method public final a(LX/Bdb;)LX/IPa;
    .locals 2

    .prologue
    .line 2574330
    iget-object v0, p0, LX/IPa;->a:LX/IPb;

    iput-object p1, v0, LX/IPb;->b:LX/Bdb;

    .line 2574331
    iget-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2574332
    return-object p0
.end method

.method public final a(LX/IPe;)LX/IPa;
    .locals 2

    .prologue
    .line 2574327
    iget-object v0, p0, LX/IPa;->a:LX/IPb;

    iput-object p1, v0, LX/IPb;->f:LX/IPe;

    .line 2574328
    iget-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2574329
    return-object p0
.end method

.method public final a(Z)LX/IPa;
    .locals 2

    .prologue
    .line 2574324
    iget-object v0, p0, LX/IPa;->a:LX/IPb;

    iput-boolean p1, v0, LX/IPb;->e:Z

    .line 2574325
    iget-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2574326
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2574320
    invoke-super {p0}, LX/1X5;->a()V

    .line 2574321
    const/4 v0, 0x0

    iput-object v0, p0, LX/IPa;->a:LX/IPb;

    .line 2574322
    sget-object v0, LX/IPc;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2574323
    return-void
.end method

.method public final b(LX/1X1;)LX/IPa;
    .locals 2

    .prologue
    .line 2574316
    iget-object v1, p0, LX/IPa;->a:LX/IPb;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, v1, LX/IPb;->d:LX/1X1;

    .line 2574317
    iget-object v0, p0, LX/IPa;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2574318
    return-object p0

    .line 2574319
    :cond_0
    invoke-virtual {p1}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/IPc;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2574306
    iget-object v1, p0, LX/IPa;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/IPa;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/IPa;->c:I

    if-ge v1, v2, :cond_2

    .line 2574307
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2574308
    :goto_0
    iget v2, p0, LX/IPa;->c:I

    if-ge v0, v2, :cond_1

    .line 2574309
    iget-object v2, p0, LX/IPa;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2574310
    iget-object v2, p0, LX/IPa;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2574311
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2574312
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2574313
    :cond_2
    iget-object v0, p0, LX/IPa;->a:LX/IPb;

    .line 2574314
    invoke-virtual {p0}, LX/IPa;->a()V

    .line 2574315
    return-object v0
.end method
