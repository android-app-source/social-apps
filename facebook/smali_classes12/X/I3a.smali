.class public final LX/I3a;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I3b;


# direct methods
.method public constructor <init>(LX/I3b;)V
    .locals 0

    .prologue
    .line 2531523
    iput-object p1, p0, LX/I3a;->a:LX/I3b;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2531524
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2531525
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2531526
    if-eqz p1, :cond_8

    .line 2531527
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531528
    if-eqz v0, :cond_8

    .line 2531529
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531530
    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2531531
    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_6

    .line 2531532
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531533
    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3949fc71

    invoke-static {v2, v0, v1, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2531534
    iget-object v1, p0, LX/I3a;->a:LX/I3b;

    iget-object v1, v1, LX/I3b;->a:LX/Hym;

    if-eqz v0, :cond_9

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2531535
    if-eqz v0, :cond_6

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v2

    if-nez v2, :cond_6

    .line 2531536
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2531537
    if-eqz v0, :cond_6

    .line 2531538
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object p0

    :cond_0
    :goto_2
    invoke-interface {p0}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p0}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2531539
    const-class p1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {v3, v2, v5, p1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    .line 2531540
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2531541
    invoke-virtual {v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;->j()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2531542
    if-eqz v3, :cond_1

    move v3, v4

    :goto_3
    if-eqz v3, :cond_4

    .line 2531543
    invoke-virtual {v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;->j()LX/1vs;

    move-result-object v3

    iget-object p1, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2531544
    invoke-virtual {p1, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    :goto_4
    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2531545
    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    :cond_1
    move v3, v5

    .line 2531546
    goto :goto_3

    :cond_2
    move v3, v5

    goto :goto_3

    :cond_3
    move v3, v5

    goto :goto_4

    :cond_4
    move v3, v5

    goto :goto_4

    .line 2531547
    :cond_5
    iget-object v2, v1, LX/Hym;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2531548
    iget-object v4, v2, LX/Hz2;->j:LX/I2Z;

    .line 2531549
    iput-object v3, v4, LX/I2Z;->x:LX/0Px;

    .line 2531550
    invoke-static {v4}, LX/I2Z;->b(LX/I2Z;)V

    .line 2531551
    invoke-static {v2}, LX/Hz2;->k(LX/Hz2;)V

    .line 2531552
    :cond_6
    return-void

    :cond_7
    move v0, v1

    .line 2531553
    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_0

    .line 2531554
    :cond_9
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_1
.end method
