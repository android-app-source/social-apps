.class public LX/IhD;
.super LX/1a1;
.source ""


# instance fields
.field private final l:LX/Ihg;

.field public final m:LX/Ihk;

.field private final n:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

.field private final o:LX/Ihq;

.field private final p:LX/IhC;

.field public q:Z

.field public r:LX/IhV;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/Ihj;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/ui/media/attachments/MediaResource;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ihh;LX/Ihl;LX/Ihp;LX/Ihr;Landroid/view/View;)V
    .locals 2
    .param p5    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2602293
    invoke-direct {p0, p5}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2602294
    new-instance v0, LX/IhC;

    invoke-direct {v0, p0}, LX/IhC;-><init>(LX/IhD;)V

    iput-object v0, p0, LX/IhD;->p:LX/IhC;

    .line 2602295
    new-instance v0, LX/Ihg;

    invoke-direct {v0, p5}, LX/Ihg;-><init>(Landroid/view/View;)V

    .line 2602296
    move-object v0, v0

    .line 2602297
    iput-object v0, p0, LX/IhD;->l:LX/Ihg;

    .line 2602298
    new-instance v1, LX/Ihk;

    invoke-direct {v1, p5}, LX/Ihk;-><init>(Landroid/view/View;)V

    .line 2602299
    invoke-static {p2}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 2602300
    iput-object v0, v1, LX/Ihk;->a:Landroid/content/res/Resources;

    .line 2602301
    move-object v0, v1

    .line 2602302
    iput-object v0, p0, LX/IhD;->m:LX/Ihk;

    .line 2602303
    new-instance p2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    invoke-direct {p2, p5}, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;-><init>(Landroid/view/View;)V

    .line 2602304
    invoke-static {p3}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {p3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p3}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object p1

    check-cast p1, LX/11S;

    .line 2602305
    iput-object v0, p2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->e:LX/1Ad;

    iput-object v1, p2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->f:Landroid/content/res/Resources;

    iput-object p1, p2, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->g:LX/11S;

    .line 2602306
    move-object v0, p2

    .line 2602307
    iput-object v0, p0, LX/IhD;->n:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    .line 2602308
    new-instance v1, LX/Ihq;

    invoke-direct {v1, p5}, LX/Ihq;-><init>(Landroid/view/View;)V

    .line 2602309
    invoke-static {p4}, LX/8tH;->a(LX/0QB;)LX/8tH;

    move-result-object v0

    check-cast v0, LX/8tH;

    .line 2602310
    iput-object v0, v1, LX/Ihq;->a:LX/8tH;

    .line 2602311
    move-object v0, v1

    .line 2602312
    iput-object v0, p0, LX/IhD;->o:LX/Ihq;

    .line 2602313
    return-void
.end method

.method private static c(I)I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2602373
    packed-switch p0, :pswitch_data_0

    .line 2602374
    const/16 v0, 0x33

    :goto_0
    return v0

    .line 2602375
    :pswitch_0
    const/16 v0, 0x55

    goto :goto_0

    .line 2602376
    :pswitch_1
    const/16 v0, 0x53

    goto :goto_0

    .line 2602377
    :pswitch_2
    const/16 v0, 0x35

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/Ihj;)V
    .locals 3

    .prologue
    .line 2602362
    iput-object p1, p0, LX/IhD;->s:LX/Ihj;

    .line 2602363
    iget-object v0, p0, LX/IhD;->n:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-boolean v1, p1, LX/Ihj;->b:Z

    .line 2602364
    iput-boolean v1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->i:Z

    .line 2602365
    iget-object v0, p0, LX/IhD;->l:LX/Ihg;

    iget-boolean v1, p1, LX/Ihj;->c:Z

    .line 2602366
    iput-boolean v1, v0, LX/Ihg;->b:Z

    .line 2602367
    invoke-static {v0}, LX/Ihg;->a(LX/Ihg;)V

    .line 2602368
    iget-object v0, p0, LX/IhD;->m:LX/Ihk;

    iget v1, p1, LX/Ihj;->e:I

    invoke-static {v1}, LX/IhD;->c(I)I

    move-result v1

    .line 2602369
    iget-object v2, v0, LX/Ihk;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2602370
    iget-object v0, p0, LX/IhD;->o:LX/Ihq;

    iget v1, p1, LX/Ihj;->f:I

    invoke-static {v1}, LX/IhD;->c(I)I

    move-result v1

    .line 2602371
    iget-object v2, v0, LX/Ihq;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2602372
    return-void
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 8

    .prologue
    .line 2602331
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2602332
    iput-object p1, p0, LX/IhD;->t:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602333
    iget-object v0, p0, LX/IhD;->l:LX/Ihg;

    iget-object v1, p0, LX/IhD;->p:LX/IhC;

    .line 2602334
    iput-object v1, v0, LX/Ihg;->d:LX/IhC;

    .line 2602335
    iget-object v0, p0, LX/IhD;->n:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-object v1, p0, LX/IhD;->p:LX/IhC;

    .line 2602336
    iput-object v1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->k:LX/IhC;

    .line 2602337
    iget-object v0, p0, LX/IhD;->n:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2602338
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v5, LX/2MK;->PHOTO:LX/2MK;

    if-eq v2, v5, :cond_1

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v5, LX/2MK;->VIDEO:LX/2MK;

    if-ne v2, v5, :cond_4

    :cond_1
    move v2, v4

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2602339
    iput-object p1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602340
    iget-object v2, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->g:LX/11S;

    sget-object v5, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    iget-wide v6, p1, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    invoke-interface {v2, v5, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v5

    .line 2602341
    iget-object v6, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->f:Landroid/content/res/Resources;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->PHOTO:LX/2MK;

    if-ne v2, v7, :cond_5

    const v2, 0x7f082e05

    :goto_2
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v5, v4, v3

    invoke-virtual {v6, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2602342
    iget-object v4, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2602343
    iget-object v2, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->m:LX/1o9;

    if-nez v2, :cond_2

    .line 2602344
    iget-object v2, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->f:Landroid/content/res/Resources;

    const v4, 0x7f0b1f00

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2602345
    new-instance v4, LX/1o9;

    invoke-direct {v4, v2, v2}, LX/1o9;-><init>(II)V

    iput-object v4, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->m:LX/1o9;

    .line 2602346
    :cond_2
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    iget-object v4, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->m:LX/1o9;

    .line 2602347
    iput-object v4, v2, LX/1bX;->c:LX/1o9;

    .line 2602348
    move-object v2, v2

    .line 2602349
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    .line 2602350
    iget-object v5, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->e:LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->o()LX/1Ad;

    move-result-object v2

    sget-object v6, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    iget-object v6, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->d:LX/Iho;

    invoke-virtual {v2, v6}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    iget-object v4, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2602351
    iget-object v2, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2602352
    iget-object v2, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->a:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->d()V

    .line 2602353
    iget-object v0, p0, LX/IhD;->o:LX/Ihq;

    .line 2602354
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->VIDEO:LX/2MK;

    if-eq v2, v3, :cond_6

    .line 2602355
    iget-object v2, v0, LX/Ihq;->b:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2602356
    :goto_3
    return-void

    .line 2602357
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    move v2, v3

    .line 2602358
    goto/16 :goto_1

    .line 2602359
    :cond_5
    const v2, 0x7f082e06

    goto/16 :goto_2

    .line 2602360
    :cond_6
    iget-object v2, v0, LX/Ihq;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2602361
    iget-object v2, v0, LX/Ihq;->b:Landroid/widget/TextView;

    iget-object v3, v0, LX/Ihq;->a:LX/8tH;

    iget-wide v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-virtual {v3, v4, v5}, LX/8tH;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 2602314
    iget-object v0, p0, LX/IhD;->s:LX/Ihj;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IhD;->s:LX/Ihj;

    iget-boolean v0, v0, LX/Ihj;->d:Z

    if-nez v0, :cond_1

    .line 2602315
    :cond_0
    :goto_0
    return-void

    .line 2602316
    :cond_1
    iput-boolean p1, p0, LX/IhD;->q:Z

    .line 2602317
    iget-object v0, p0, LX/IhD;->n:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    .line 2602318
    iget-boolean v1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->j:Z

    if-eq v1, p1, :cond_2

    iget-boolean v1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->i:Z

    if-eqz v1, :cond_3

    .line 2602319
    :cond_2
    :goto_1
    iget-object v0, p0, LX/IhD;->m:LX/Ihk;

    .line 2602320
    iput-boolean p1, v0, LX/Ihk;->c:Z

    .line 2602321
    iget-boolean v1, v0, LX/Ihk;->c:Z

    if-eqz v1, :cond_5

    .line 2602322
    iget-object v1, v0, LX/Ihk;->b:Landroid/widget/ImageView;

    iget-object v2, v0, LX/Ihk;->a:Landroid/content/res/Resources;

    const v3, 0x7f02196c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2602323
    :goto_2
    iget-object v0, p0, LX/IhD;->l:LX/Ihg;

    .line 2602324
    iput-boolean p1, v0, LX/Ihg;->c:Z

    .line 2602325
    invoke-static {v0}, LX/Ihg;->a(LX/Ihg;)V

    .line 2602326
    goto :goto_0

    .line 2602327
    :cond_3
    iput-boolean p1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->j:Z

    .line 2602328
    iget-object v1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2602329
    iget-object v3, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->c:Landroid/animation/ValueAnimator;

    iget-boolean v1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->j:Z

    if-eqz v1, :cond_4

    const-wide/16 v1, 0x64

    :goto_3
    invoke-virtual {v3, v1, v2}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    goto :goto_1

    :cond_4
    const-wide/16 v1, 0x0

    goto :goto_3

    .line 2602330
    :cond_5
    iget-object v1, v0, LX/Ihk;->b:Landroid/widget/ImageView;

    iget-object v2, v0, LX/Ihk;->a:Landroid/content/res/Resources;

    const v3, 0x7f02196d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method
