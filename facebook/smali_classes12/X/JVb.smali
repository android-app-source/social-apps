.class public final LX/JVb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9hN;


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field private c:LX/1bf;

.field private d:Lcom/facebook/widget/CustomViewPager;

.field private e:LX/1qa;


# direct methods
.method private constructor <init>(Landroid/widget/ImageView;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1bf;Lcom/facebook/widget/CustomViewPager;LX/1qa;)V
    .locals 0

    .prologue
    .line 2701120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2701121
    iput-object p1, p0, LX/JVb;->a:Landroid/widget/ImageView;

    .line 2701122
    iput-object p2, p0, LX/JVb;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2701123
    iput-object p3, p0, LX/JVb;->c:LX/1bf;

    .line 2701124
    iput-object p4, p0, LX/JVb;->d:Lcom/facebook/widget/CustomViewPager;

    .line 2701125
    iput-object p5, p0, LX/JVb;->e:LX/1qa;

    .line 2701126
    return-void
.end method

.method public synthetic constructor <init>(Landroid/widget/ImageView;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1bf;Lcom/facebook/widget/CustomViewPager;LX/1qa;B)V
    .locals 0

    .prologue
    .line 2701100
    invoke-direct/range {p0 .. p5}, LX/JVb;-><init>(Landroid/widget/ImageView;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1bf;Lcom/facebook/widget/CustomViewPager;LX/1qa;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/9hO;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2701101
    iget-object v0, p0, LX/JVb;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/JVb;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2701102
    new-instance v4, LX/9hO;

    iget-object v0, p0, LX/JVb;->a:Landroid/widget/ImageView;

    invoke-static {v0}, LX/9hP;->a(Landroid/widget/ImageView;)LX/9hP;

    move-result-object v0

    iget-object v1, p0, LX/JVb;->c:LX/1bf;

    invoke-direct {v4, v0, v1}, LX/9hO;-><init>(LX/9hP;LX/1bf;)V

    .line 2701103
    :cond_0
    :goto_0
    return-object v4

    .line 2701104
    :cond_1
    const/4 v0, 0x0

    move v3, v0

    move-object v2, v4

    move-object v0, v4

    :goto_1
    iget-object v1, p0, LX/JVb;->d:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomViewPager;->getChildCount()I

    move-result v1

    if-ge v3, v1, :cond_3

    .line 2701105
    iget-object v0, p0, LX/JVb;->d:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/JVe;

    .line 2701106
    if-eqz v0, :cond_5

    .line 2701107
    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, LX/JVe;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2701108
    if-eqz v1, :cond_5

    .line 2701109
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2701110
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2701111
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2701112
    :cond_2
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_1

    :cond_3
    move-object v1, v2

    .line 2701113
    :cond_4
    iget-object v2, p0, LX/JVb;->d:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomViewPager;->getChildCount()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 2701114
    iget-object v2, v0, LX/JVe;->a:LX/JVd;

    move-object v0, v2

    .line 2701115
    iget-object v0, v0, LX/JVd;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2701116
    if-eqz v0, :cond_0

    .line 2701117
    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 2701118
    invoke-static {v1}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v1

    .line 2701119
    new-instance v4, LX/9hO;

    invoke-static {v0}, LX/9hP;->a(Landroid/widget/ImageView;)LX/9hP;

    move-result-object v0

    invoke-direct {v4, v0, v1}, LX/9hO;-><init>(LX/9hP;LX/1bf;)V

    goto :goto_0

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method
