.class public final LX/HOD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HOE;


# direct methods
.method public constructor <init>(LX/HOE;)V
    .locals 0

    .prologue
    .line 2459759
    iput-object p1, p0, LX/HOD;->a:LX/HOE;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2459766
    iget-object v0, p0, LX/HOD;->a:LX/HOE;

    iget-object v0, v0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2459767
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2459760
    iget-object v0, p0, LX/HOD;->a:LX/HOE;

    iget-object v0, v0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-object v1, p0, LX/HOD;->a:LX/HOE;

    iget-object v1, v1, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-wide v2, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->h:J

    iget-object v1, p0, LX/HOD;->a:LX/HOE;

    iget-object v1, v1, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->i:LX/CYE;

    .line 2459761
    iget-object v4, v1, LX/CYE;->mActionType:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v1, v4

    .line 2459762
    iget-object v4, p0, LX/HOD;->a:LX/HOE;

    iget-object v4, v4, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    .line 2459763
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v7, LX/9X7;->EDIT_COMPLETE_REPLACE_ACTION:LX/9X7;

    invoke-virtual {v7}, LX/9X7;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "old_action"

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "new_action"

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2459764
    iget-object v0, p0, LX/HOD;->a:LX/HOE;

    iget-object v0, v0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->k(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;)V

    .line 2459765
    return-void
.end method
