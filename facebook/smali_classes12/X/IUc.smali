.class public final enum LX/IUc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IUc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IUc;

.field public static final enum AVAILABLE:LX/IUc;

.field public static final enum EXPIRED:LX/IUc;

.field public static final enum SOLD:LX/IUc;


# instance fields
.field private final mFeedTypeName:Lcom/facebook/api/feedtype/FeedType$Name;

.field private final mGroupsFeedType:LX/B1T;

.field private final mTitleId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2580792
    new-instance v0, LX/IUc;

    const-string v1, "AVAILABLE"

    const v3, 0x7f081bdd

    sget-object v4, Lcom/facebook/api/feedtype/FeedType$Name;->m:Lcom/facebook/api/feedtype/FeedType$Name;

    sget-object v5, LX/B1T;->YourAvailableForSalePosts:LX/B1T;

    invoke-direct/range {v0 .. v5}, LX/IUc;-><init>(Ljava/lang/String;IILcom/facebook/api/feedtype/FeedType$Name;LX/B1T;)V

    sput-object v0, LX/IUc;->AVAILABLE:LX/IUc;

    .line 2580793
    new-instance v3, LX/IUc;

    const-string v4, "SOLD"

    const v6, 0x7f081bde

    sget-object v7, Lcom/facebook/api/feedtype/FeedType$Name;->n:Lcom/facebook/api/feedtype/FeedType$Name;

    sget-object v8, LX/B1T;->YourSoldForSalePosts:LX/B1T;

    move v5, v9

    invoke-direct/range {v3 .. v8}, LX/IUc;-><init>(Ljava/lang/String;IILcom/facebook/api/feedtype/FeedType$Name;LX/B1T;)V

    sput-object v3, LX/IUc;->SOLD:LX/IUc;

    .line 2580794
    new-instance v3, LX/IUc;

    const-string v4, "EXPIRED"

    const v6, 0x7f081be2

    sget-object v7, Lcom/facebook/api/feedtype/FeedType$Name;->o:Lcom/facebook/api/feedtype/FeedType$Name;

    sget-object v8, LX/B1T;->YourExpiredForSalePosts:LX/B1T;

    move v5, v10

    invoke-direct/range {v3 .. v8}, LX/IUc;-><init>(Ljava/lang/String;IILcom/facebook/api/feedtype/FeedType$Name;LX/B1T;)V

    sput-object v3, LX/IUc;->EXPIRED:LX/IUc;

    .line 2580795
    const/4 v0, 0x3

    new-array v0, v0, [LX/IUc;

    sget-object v1, LX/IUc;->AVAILABLE:LX/IUc;

    aput-object v1, v0, v2

    sget-object v1, LX/IUc;->SOLD:LX/IUc;

    aput-object v1, v0, v9

    sget-object v1, LX/IUc;->EXPIRED:LX/IUc;

    aput-object v1, v0, v10

    sput-object v0, LX/IUc;->$VALUES:[LX/IUc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/facebook/api/feedtype/FeedType$Name;LX/B1T;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/api/feedtype/FeedType$Name;",
            "LX/B1T;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2580796
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2580797
    iput p3, p0, LX/IUc;->mTitleId:I

    .line 2580798
    iput-object p4, p0, LX/IUc;->mFeedTypeName:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 2580799
    iput-object p5, p0, LX/IUc;->mGroupsFeedType:LX/B1T;

    .line 2580800
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IUc;
    .locals 1

    .prologue
    .line 2580801
    const-class v0, LX/IUc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IUc;

    return-object v0
.end method

.method public static values()[LX/IUc;
    .locals 1

    .prologue
    .line 2580802
    sget-object v0, LX/IUc;->$VALUES:[LX/IUc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IUc;

    return-object v0
.end method


# virtual methods
.method public final getFeedTypeName()Lcom/facebook/api/feedtype/FeedType$Name;
    .locals 1

    .prologue
    .line 2580803
    iget-object v0, p0, LX/IUc;->mFeedTypeName:Lcom/facebook/api/feedtype/FeedType$Name;

    return-object v0
.end method

.method public final getGroupsFeedType()LX/B1T;
    .locals 1

    .prologue
    .line 2580804
    iget-object v0, p0, LX/IUc;->mGroupsFeedType:LX/B1T;

    return-object v0
.end method

.method public final getTitleId()I
    .locals 1

    .prologue
    .line 2580805
    iget v0, p0, LX/IUc;->mTitleId:I

    return v0
.end method
