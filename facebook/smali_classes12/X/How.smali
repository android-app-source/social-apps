.class public LX/How;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1fX;

.field private final c:LX/0Sh;

.field private final d:LX/Hoy;

.field private final e:LX/Hp2;

.field private final f:LX/0ad;

.field private final g:Ljava/lang/Object;

.field private final h:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mQueueLock"
    .end annotation
.end field

.field private volatile i:LX/Hov;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2507107
    const-class v0, LX/How;

    sput-object v0, LX/How;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1fX;LX/0Sh;LX/Hoy;LX/Hp2;LX/0ad;)V
    .locals 1
    .param p1    # LX/1fX;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2507108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2507109
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/How;->g:Ljava/lang/Object;

    .line 2507110
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/How;->h:Ljava/util/Queue;

    .line 2507111
    sget-object v0, LX/Hov;->CLOSED:LX/Hov;

    iput-object v0, p0, LX/How;->i:LX/Hov;

    .line 2507112
    iput-object p1, p0, LX/How;->b:LX/1fX;

    .line 2507113
    iput-object p2, p0, LX/How;->c:LX/0Sh;

    .line 2507114
    iput-object p3, p0, LX/How;->d:LX/Hoy;

    .line 2507115
    iput-object p4, p0, LX/How;->e:LX/Hp2;

    .line 2507116
    iput-object p5, p0, LX/How;->f:LX/0ad;

    .line 2507117
    return-void
.end method
