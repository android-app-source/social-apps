.class public LX/I6B;
.super LX/0rn;
.source ""


# instance fields
.field private final d:LX/0w9;

.field private final e:LX/0tI;

.field private final f:LX/A5q;

.field private final g:LX/0sU;

.field private final h:LX/0sX;

.field private final i:LX/0tQ;


# direct methods
.method public constructor <init>(LX/0tQ;LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0w9;LX/0tI;LX/A5q;LX/0sU;LX/0sX;LX/0ad;LX/0sZ;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2537374
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p12

    move-object/from16 v8, p13

    invoke-direct/range {v1 .. v8}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 2537375
    iput-object p1, p0, LX/I6B;->i:LX/0tQ;

    .line 2537376
    move-object/from16 v0, p7

    iput-object v0, p0, LX/I6B;->d:LX/0w9;

    .line 2537377
    move-object/from16 v0, p8

    iput-object v0, p0, LX/I6B;->e:LX/0tI;

    .line 2537378
    move-object/from16 v0, p9

    iput-object v0, p0, LX/I6B;->f:LX/A5q;

    .line 2537379
    move-object/from16 v0, p10

    iput-object v0, p0, LX/I6B;->g:LX/0sU;

    .line 2537380
    move-object/from16 v0, p11

    iput-object v0, p0, LX/I6B;->h:LX/0sX;

    .line 2537381
    return-void
.end method

.method public static b(LX/0QB;)LX/I6B;
    .locals 14

    .prologue
    .line 2537396
    new-instance v0, LX/I6B;

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v1

    check-cast v1, LX/0tQ;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v2

    check-cast v2, LX/0sO;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0Yl;->b(LX/0QB;)LX/0Yl;

    move-result-object v4

    check-cast v4, LX/0Yl;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static {p0}, LX/0w9;->a(LX/0QB;)LX/0w9;

    move-result-object v7

    check-cast v7, LX/0w9;

    invoke-static {p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v8

    check-cast v8, LX/0tI;

    invoke-static {p0}, LX/A5q;->a(LX/0QB;)LX/A5q;

    move-result-object v9

    check-cast v9, LX/A5q;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v10

    check-cast v10, LX/0sU;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v11

    check-cast v11, LX/0sX;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {p0}, LX/0sZ;->b(LX/0QB;)LX/0sZ;

    move-result-object v13

    check-cast v13, LX/0sZ;

    invoke-direct/range {v0 .. v13}, LX/I6B;-><init>(LX/0tQ;LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0w9;LX/0tI;LX/A5q;LX/0sU;LX/0sX;LX/0ad;LX/0sZ;)V

    .line 2537397
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/15w;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 3

    .prologue
    .line 2537382
    invoke-super {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/15w;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    .line 2537383
    new-instance v1, LX/0uq;

    invoke-direct {v1}, LX/0uq;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/0rn;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 2537384
    iput-object v2, v1, LX/0uq;->d:LX/0Px;

    .line 2537385
    move-object v1, v1

    .line 2537386
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2537387
    iput-object v2, v1, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2537388
    move-object v1, v1

    .line 2537389
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j()Ljava/lang/String;

    move-result-object v2

    .line 2537390
    iput-object v2, v1, LX/0uq;->c:Ljava/lang/String;

    .line 2537391
    move-object v1, v1

    .line 2537392
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->a()I

    move-result v0

    .line 2537393
    iput v0, v1, LX/0uq;->b:I

    .line 2537394
    move-object v0, v1

    .line 2537395
    invoke-virtual {v0}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 1

    .prologue
    .line 2537371
    invoke-super {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    .line 2537372
    invoke-virtual {p0, v0}, LX/0rn;->a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V

    .line 2537373
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2537398
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 2537399
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 2537400
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2537347
    const/4 v0, 0x2

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2537348
    const-string v0, "event_stories"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2537370
    const-string v0, "EventsFeedNetworkTime"

    return-object v0
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 2537349
    const v0, 0xa007f

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2537350
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 2537351
    new-instance v0, LX/I65;

    invoke-direct {v0}, LX/I65;-><init>()V

    move-object v1, v0

    .line 2537352
    invoke-static {v1}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 2537353
    iget-object v0, p0, LX/I6B;->d:LX/0w9;

    invoke-virtual {v0, v1}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 2537354
    const-string v0, "before"

    const-string v2, "after"

    invoke-static {v1, p1, v0, v2}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2537355
    iget-object v0, p0, LX/I6B;->d:LX/0w9;

    invoke-virtual {v0, v1}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 2537356
    invoke-static {v1}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 2537357
    iget-object v0, p0, LX/I6B;->e:LX/0tI;

    invoke-virtual {v0, v1}, LX/0tI;->a(LX/0gW;)V

    .line 2537358
    iget-object v0, p0, LX/I6B;->g:LX/0sU;

    invoke-virtual {v0, v1}, LX/0sU;->a(LX/0gW;)V

    .line 2537359
    const-string v0, "action_location"

    sget-object v2, LX/0wD;->EVENT:LX/0wD;

    invoke-virtual {v2}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2537360
    const-string v0, "automatic_photo_captioning_enabled"

    iget-object v2, p0, LX/I6B;->h:LX/0sX;

    invoke-virtual {v2}, LX/0sX;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2537361
    const-string v0, "enable_download"

    iget-object v2, p0, LX/I6B;->i:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2537362
    if-eqz p1, :cond_0

    .line 2537363
    const-string v2, "event_id"

    .line 2537364
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 2537365
    iget-object v3, v0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v3

    .line 2537366
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "first"

    .line 2537367
    iget v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v3, v3

    .line 2537368
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2537369
    :cond_0
    return-object v1
.end method
