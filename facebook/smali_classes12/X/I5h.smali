.class public LX/I5h;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            "Lcom/facebook/events/feed/data/EventFeedStories;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/concurrent/Executor;

.field public c:LX/0aG;

.field public d:I

.field public e:Lcom/facebook/api/feedtype/FeedType;

.field public f:Lcom/facebook/api/feed/FeedFetchContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2536026
    new-instance v0, LX/I5g;

    invoke-direct {v0}, LX/I5g;-><init>()V

    sput-object v0, LX/I5h;->a:LX/0QK;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0aG;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2536030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2536031
    iput-object p1, p0, LX/I5h;->b:Ljava/util/concurrent/Executor;

    .line 2536032
    iput-object p2, p0, LX/I5h;->c:LX/0aG;

    .line 2536033
    return-void
.end method

.method public static a(LX/0QB;)LX/I5h;
    .locals 3

    .prologue
    .line 2536027
    new-instance v2, LX/I5h;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-direct {v2, v0, v1}, LX/I5h;-><init>(Ljava/util/concurrent/ExecutorService;LX/0aG;)V

    .line 2536028
    move-object v0, v2

    .line 2536029
    return-object v0
.end method
