.class public final LX/JJC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;LX/4BY;)V
    .locals 0

    .prologue
    .line 2678993
    iput-object p1, p0, LX/JJC;->b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iput-object p2, p0, LX/JJC;->a:LX/4BY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2678987
    iget-object v0, p0, LX/JJC;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2678988
    iget-object v0, p0, LX/JJC;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2678989
    iget-object v0, p0, LX/JJC;->b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/JJC;->b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    const v2, 0x7f083a70

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2678990
    iget-object v0, p0, LX/JJC;->b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2678991
    iget-object v0, p0, LX/JJC;->b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2678992
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2678994
    iget-object v0, p0, LX/JJC;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2678995
    iget-object v0, p0, LX/JJC;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2678996
    iget-object v0, p0, LX/JJC;->b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    iget-object v2, p0, LX/JJC;->b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2678997
    iget-object v0, p0, LX/JJC;->b:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2678998
    :cond_0
    return-void
.end method
