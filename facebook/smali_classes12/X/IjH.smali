.class public LX/IjH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6yT;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final c:Ljava/util/concurrent/Executor;

.field public d:Ljava/lang/String;

.field private e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/6qh;

.field public final g:LX/6wv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605569
    new-instance v0, LX/IjF;

    invoke-direct {v0, p0}, LX/IjF;-><init>(LX/IjH;)V

    iput-object v0, p0, LX/IjH;->g:LX/6wv;

    .line 2605570
    iput-object p1, p0, LX/IjH;->a:Landroid/content/Context;

    .line 2605571
    iput-object p2, p0, LX/IjH;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2605572
    iput-object p3, p0, LX/IjH;->c:Ljava/util/concurrent/Executor;

    .line 2605573
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2605536
    iget-object v0, p0, LX/IjH;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2605537
    iget-object v0, p0, LX/IjH;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2605538
    const/4 v0, 0x0

    iput-object v0, p0, LX/IjH;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2605539
    :cond_0
    return-void
.end method

.method public static b(LX/IjH;)V
    .locals 2

    .prologue
    .line 2605563
    const/4 v0, 0x0

    iput-object v0, p0, LX/IjH;->d:Ljava/lang/String;

    .line 2605564
    iget-object v0, p0, LX/IjH;->f:LX/6qh;

    if-eqz v0, :cond_0

    .line 2605565
    new-instance v0, LX/73T;

    sget-object v1, LX/73S;->RESET:LX/73S;

    invoke-direct {v0, v1}, LX/73T;-><init>(LX/73S;)V

    .line 2605566
    iget-object v1, p0, LX/IjH;->f:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(LX/73T;)V

    .line 2605567
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x6

    .line 2605542
    check-cast p1, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v0, p1, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    if-eqz v0, :cond_0

    .line 2605543
    const/4 v0, 0x0

    .line 2605544
    :goto_0
    return-object v0

    .line 2605545
    :cond_0
    invoke-static {p2}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2605546
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v2, :cond_1

    .line 2605547
    invoke-direct {p0}, LX/IjH;->a()V

    .line 2605548
    iget-object v0, p0, LX/IjH;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2605549
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2605550
    iget-object v1, p0, LX/IjH;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2605551
    iget-object v0, p0, LX/IjH;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2605552
    :cond_2
    invoke-direct {p0}, LX/IjH;->a()V

    .line 2605553
    iget-object v1, p0, LX/IjH;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2605554
    new-instance v2, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;

    invoke-direct {v2, v0}, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;-><init>(Ljava/lang/String;)V

    .line 2605555
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2605556
    sget-object p2, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->a:Ljava/lang/String;

    invoke-virtual {p1, p2, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2605557
    const-string v2, "validate_payment_card_bin"

    invoke-static {v1, p1, v2}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2605558
    new-instance p1, LX/Izz;

    invoke-direct {p1, v1}, LX/Izz;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    invoke-static {v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v1, v2

    .line 2605559
    iput-object v1, p0, LX/IjH;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2605560
    iput-object v0, p0, LX/IjH;->d:Ljava/lang/String;

    .line 2605561
    iget-object v0, p0, LX/IjH;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/IjG;

    invoke-direct {v1, p0}, LX/IjG;-><init>(LX/IjH;)V

    iget-object v2, p0, LX/IjH;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2605562
    iget-object v0, p0, LX/IjH;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 2605540
    iput-object p1, p0, LX/IjH;->f:LX/6qh;

    .line 2605541
    return-void
.end method
