.class public final LX/HnH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

.field private b:Landroid/widget/LinearLayout;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/LinearLayout;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V
    .locals 1

    .prologue
    .line 2501569
    iput-object p1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501570
    const v0, 0x7f0d06de

    invoke-virtual {p1, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HnH;->b:Landroid/widget/LinearLayout;

    .line 2501571
    const v0, 0x7f0d06e0

    invoke-virtual {p1, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HnH;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2501572
    const v0, 0x7f0d06e1

    invoke-virtual {p1, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HnH;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2501573
    const v0, 0x7f0d06e2

    invoke-virtual {p1, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HnH;->c:Landroid/widget/LinearLayout;

    .line 2501574
    const v0, 0x7f0d06e6

    invoke-virtual {p1, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HnH;->d:Landroid/widget/LinearLayout;

    .line 2501575
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2501562
    invoke-direct {p0}, LX/HnH;->h()V

    .line 2501563
    iget-object v0, p0, LX/HnH;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06d2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2501564
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2501565
    iget-object v0, p0, LX/HnH;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06df

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2501566
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2501567
    iget-object v0, p0, LX/HnH;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2501568
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 2501550
    invoke-direct {p0}, LX/HnH;->h()V

    .line 2501551
    iget-object v0, p0, LX/HnH;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06e3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2501552
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2501553
    iget-object v0, p0, LX/HnH;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06e4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2501554
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2501555
    if-nez p2, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2501556
    iget-object v0, p0, LX/HnH;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06e5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2501557
    if-eqz p3, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2501558
    new-instance v1, LX/HnG;

    invoke-direct {v1, p0}, LX/HnG;-><init>(LX/HnH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2501559
    iget-object v0, p0, LX/HnH;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2501560
    return-void

    :cond_1
    move v1, v3

    .line 2501561
    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2501544
    iget-object v0, p0, LX/HnH;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2501545
    iget-object v0, p0, LX/HnH;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2501546
    iget-object v0, p0, LX/HnH;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2501547
    iget-object v0, p0, LX/HnH;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2501548
    iget-object v0, p0, LX/HnH;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2501549
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2501540
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501541
    sget-object v1, LX/HnJ;->CREATE_HOTSPOT_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501542
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v1, 0x7f08395d

    invoke-virtual {v0, v1}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f08395b

    invoke-virtual {v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/HnH;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2501543
    return-void
.end method

.method public final a(LX/Hmp;)V
    .locals 5

    .prologue
    .line 2501531
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501532
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "beamTransactionId"

    .line 2501533
    iget-object v3, p1, LX/Hmp;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2501534
    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 2501535
    sget-object v2, LX/HnJ;->TRANSFERRING_APK_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v2, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501536
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v1, 0x7f08395c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v4, v4, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->D:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501537
    iget-object p1, v4, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v4, p1

    .line 2501538
    iget-object v4, v4, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f08395b

    invoke-virtual {v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/HnH;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2501539
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/Hmh;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2501576
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501577
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "reasons"

    invoke-virtual {p2}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 2501578
    sget-object v2, LX/HnJ;->NOTHING_TO_SEND_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v2, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501579
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v1, 0x7f083969

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v3, v3, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->D:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501580
    iget-object p2, v3, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v3, p2

    .line 2501581
    iget-object v3, v3, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, v4}, LX/HnH;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2501582
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501526
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501527
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "subtitle"

    invoke-virtual {v1, v2, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "retry"

    invoke-virtual {v1, v2, p3}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v1

    .line 2501528
    sget-object v2, LX/HnJ;->ERROR_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v2, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501529
    invoke-direct {p0, p1, p2, p3}, LX/HnH;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2501530
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2501519
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501520
    sget-object v1, LX/HnJ;->WAITING_FOR_CONNECTION_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501521
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v1, 0x7f083961

    invoke-virtual {v0, v1}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f083960

    invoke-virtual {v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/HnH;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2501522
    iget-object v0, p0, LX/HnH;->f:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->z:LX/Hm0;

    invoke-virtual {v1}, LX/Hm0;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2501523
    iget-object v0, p0, LX/HnH;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2501524
    iget-object v0, p0, LX/HnH;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2501525
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2501515
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501516
    sget-object v1, LX/HnJ;->DETERMINING_COMPATIBILITY_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501517
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v1, 0x7f08395e

    invoke-virtual {v0, v1}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f08395b

    invoke-virtual {v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/HnH;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2501518
    return-void
.end method

.method public final d()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2501499
    iget-object v0, p0, LX/HnH;->d:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06e8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2501500
    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    iget-object v2, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->v:LX/Hmj;

    invoke-virtual {v2}, LX/Hmj;->g()Z

    move-result v2

    .line 2501501
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "showDataSavings"

    invoke-virtual {v3, v4, v2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v3

    .line 2501502
    sget-object v4, LX/HnJ;->SUCCESSFUL_TRANSFER_SCREEN_OPENED:LX/HnJ;

    invoke-static {v1, v4, v3}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501503
    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->v:LX/Hmj;

    invoke-virtual {v1}, LX/Hmj;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2501504
    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f083958

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v4, v4, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->D:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501505
    iget-object v5, v4, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v4, v5

    .line 2501506
    iget-object v4, v4, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v8

    iget-object v4, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v4, v4, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->C:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501507
    iget-object v5, v4, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v4, v5

    .line 2501508
    iget-object v4, v4, Lcom/facebook/beam/protocol/BeamPackageInfo;->mApkSize:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v6, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->w:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v2, v3}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2501509
    :goto_0
    invoke-direct {p0}, LX/HnH;->h()V

    .line 2501510
    iget-object v0, p0, LX/HnH;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2501511
    return-void

    .line 2501512
    :cond_0
    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f083959

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v4, v4, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->D:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501513
    iget-object v5, v4, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v4, v5

    .line 2501514
    iget-object v4, v4, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v8

    invoke-virtual {v1, v2, v3}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2501495
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501496
    sget-object v1, LX/HnJ;->UNEXPECTED_ERROR_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501497
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v1, 0x7f083971

    invoke-virtual {v0, v1}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f083972

    invoke-virtual {v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, LX/HnH;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2501498
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 2501491
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501492
    sget-object v1, LX/HnJ;->CREATE_HOTSPOT_ERROR_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501493
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v1, 0x7f083973

    invoke-virtual {v0, v1}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f083972

    invoke-virtual {v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, LX/HnH;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2501494
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2501487
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501488
    sget-object v1, LX/HnJ;->CONNECT_TIMEOUT_EXCEPTION_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501489
    iget-object v0, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v1, 0x7f083974

    invoke-virtual {v0, v1}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HnH;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f083975

    invoke-virtual {v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, LX/HnH;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2501490
    return-void
.end method
