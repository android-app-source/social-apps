.class public final LX/IVz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/IW3;


# direct methods
.method public constructor <init>(LX/IW3;Z)V
    .locals 0

    .prologue
    .line 2583286
    iput-object p1, p0, LX/IVz;->b:LX/IW3;

    iput-boolean p2, p0, LX/IVz;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2583287
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->c:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->j()V

    .line 2583288
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->f:LX/IW5;

    .line 2583289
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2583290
    if-eqz v0, :cond_0

    .line 2583291
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->f:LX/IW5;

    invoke-virtual {v0}, LX/IW5;->i()V

    .line 2583292
    :cond_0
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-boolean v0, v0, LX/IW3;->m:Z

    if-nez v0, :cond_1

    .line 2583293
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    const/4 v1, 0x1

    .line 2583294
    iput-boolean v1, v0, LX/IW3;->m:Z

    .line 2583295
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    sget-object v1, LX/DO3;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/IW3;->a(LX/0zS;)V

    .line 2583296
    :goto_0
    return-void

    .line 2583297
    :cond_1
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->i:LX/03V;

    sget-object v1, LX/IW3;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch group header."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2583298
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x1

    .line 2583299
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2583300
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_1

    .line 2583301
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->c:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->g()V

    .line 2583302
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->c:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->e()V

    .line 2583303
    :goto_0
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    const/4 v1, 0x0

    .line 2583304
    iput-boolean v1, v0, LX/IW3;->m:Z

    .line 2583305
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 2583306
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, v1, :cond_2

    .line 2583307
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    .line 2583308
    iput-boolean v3, v0, LX/IW3;->n:Z

    .line 2583309
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v1, v0, LX/IW3;->f:LX/IW5;

    .line 2583310
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583311
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    sget-object v2, LX/IW2;->SUCCESS_STALE:LX/IW2;

    invoke-virtual {v1, v0, v2}, LX/IW5;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IW2;)V

    .line 2583312
    iget-boolean v0, p0, LX/IVz;->a:Z

    if-eqz v0, :cond_0

    .line 2583313
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->c:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->c()V

    .line 2583314
    :cond_0
    :goto_1
    iget-object v1, p0, LX/IVz;->b:LX/IW3;

    .line 2583315
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583316
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-static {v1, v0, v2}, LX/IW3;->a$redex0(LX/IW3;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/util/concurrent/atomic/AtomicInteger;)V

    .line 2583317
    return-void

    .line 2583318
    :cond_1
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->c:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->d()V

    .line 2583319
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->c:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->f()V

    goto :goto_0

    .line 2583320
    :cond_2
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v1, v0, LX/IW3;->f:LX/IW5;

    .line 2583321
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583322
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    sget-object v2, LX/IW2;->SUCCESS:LX/IW2;

    invoke-virtual {v1, v0, v2}, LX/IW5;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IW2;)V

    .line 2583323
    iget-object v0, p0, LX/IVz;->b:LX/IW3;

    iget-object v0, v0, LX/IW3;->g:LX/1My;

    iget-object v1, p0, LX/IVz;->b:LX/IW3;

    iget-object v1, v1, LX/IW3;->o:LX/0TF;

    iget-object v2, p0, LX/IVz;->b:LX/IW3;

    iget-object v2, v2, LX/IW3;->f:LX/IW5;

    .line 2583324
    iget-object v4, v2, LX/IW5;->c:Ljava/lang/String;

    move-object v2, v4

    .line 2583325
    invoke-virtual {v0, v1, v2, p1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_1
.end method
