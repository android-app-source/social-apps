.class public LX/JMM;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2683746
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2683747
    new-instance v0, Lcom/facebook/fbreactcomponents/stickers/StickerKeyboardWrapper$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbreactcomponents/stickers/StickerKeyboardWrapper$1;-><init>(LX/JMM;)V

    iput-object v0, p0, LX/JMM;->b:Ljava/lang/Runnable;

    .line 2683748
    new-instance v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-direct {v0, p1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/JMM;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 2683749
    iget-object v0, p0, LX/JMM;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    sget-object v1, LX/4m4;->COMMENTS:LX/4m4;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->setInterface(LX/4m4;)V

    .line 2683750
    iget-object v0, p0, LX/JMM;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-virtual {p0, v0}, LX/JMM;->addView(Landroid/view/View;)V

    .line 2683751
    return-void
.end method


# virtual methods
.method public final requestLayout()V
    .locals 1

    .prologue
    .line 2683752
    invoke-super {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 2683753
    iget-object v0, p0, LX/JMM;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/JMM;->post(Ljava/lang/Runnable;)Z

    .line 2683754
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2

    .prologue
    .line 2683755
    iget-object v0, p0, LX/JMM;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 2683756
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->g:Lcom/facebook/messaging/tabbedpager/TabbedPager;

    new-instance p0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2683757
    return-void
.end method
