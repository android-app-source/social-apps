.class public final LX/IFT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyMoreInSectionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IIf;

.field public final synthetic b:LX/0Rf;

.field public final synthetic c:LX/IFU;


# direct methods
.method public constructor <init>(LX/IFU;LX/IIf;LX/0Rf;)V
    .locals 0

    .prologue
    .line 2554169
    iput-object p1, p0, LX/IFT;->c:LX/IFU;

    iput-object p2, p0, LX/IFT;->a:LX/IIf;

    iput-object p3, p0, LX/IFT;->b:LX/0Rf;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2554170
    sget-object v0, LX/IFU;->a:Ljava/lang/Class;

    const-string v1, "failed to get more section data"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2554171
    iget-object v0, p0, LX/IFT;->c:LX/IFU;

    iget-object v0, v0, LX/IFU;->g:LX/03V;

    const-string v1, "friends_nearby_more_section_data_load_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2554172
    iget-object v0, p0, LX/IFT;->a:LX/IIf;

    .line 2554173
    iget-object v1, v0, LX/IIf;->c:LX/IJd;

    invoke-virtual {v1}, LX/IJd;->b()V

    .line 2554174
    iget-object v1, v0, LX/IIf;->e:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J:Ljava/util/Set;

    iget-object p0, v0, LX/IIf;->d:LX/IFU;

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2554175
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2554176
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2554177
    if-nez p1, :cond_0

    .line 2554178
    :goto_0
    return-void

    .line 2554179
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2554180
    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyMoreInSectionQueryModel;

    .line 2554181
    iget-object v1, p0, LX/IFT;->a:LX/IIf;

    iget-object v2, p0, LX/IFT;->c:LX/IFU;

    iget-object v3, p0, LX/IFT;->b:LX/0Rf;

    .line 2554182
    if-nez v0, :cond_1

    .line 2554183
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2554184
    :goto_1
    move-object v0, v4

    .line 2554185
    iget-object v2, v1, LX/IIf;->a:LX/IFd;

    invoke-virtual {v2, v0}, LX/IFd;->a(LX/0Px;)V

    .line 2554186
    iget-object v2, v1, LX/IIf;->a:LX/IFd;

    const/4 v3, 0x0

    .line 2554187
    iput-object v3, v2, LX/IFd;->b:Ljava/lang/String;

    .line 2554188
    iget-object v2, v1, LX/IIf;->e:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    iget-object v3, v1, LX/IIf;->b:Ljava/lang/String;

    iget-object v4, v1, LX/IIf;->a:LX/IFd;

    .line 2554189
    iget-object v5, v2, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v5}, LX/0Sh;->a()V

    .line 2554190
    iget-object v6, v2, LX/IFX;->m:LX/IG4;

    iget-object v7, v2, LX/IFX;->n:LX/IFz;

    .line 2554191
    invoke-virtual {v6, v3, v4}, LX/IG4;->a(Ljava/lang/String;LX/IFd;)V

    .line 2554192
    invoke-static {v4}, LX/IG5;->a(LX/IFd;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/IFz;->a(Ljava/util/Map;)V

    .line 2554193
    invoke-static {v2}, LX/IFX;->o(LX/IFX;)V

    .line 2554194
    invoke-static {v2}, LX/IFX;->t(LX/IFX;)V

    .line 2554195
    iget-object v2, v1, LX/IIf;->c:LX/IJd;

    invoke-virtual {v2}, LX/IJd;->b()V

    .line 2554196
    iget-object v2, v1, LX/IIf;->e:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J:Ljava/util/Set;

    iget-object v3, v1, LX/IIf;->d:LX/IFU;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2554197
    goto :goto_0

    .line 2554198
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyMoreInSectionQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyMoreInSectionQueryModel$SetItemsModel;

    move-result-object v4

    .line 2554199
    if-nez v4, :cond_2

    .line 2554200
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2554201
    goto :goto_1

    .line 2554202
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyMoreInSectionQueryModel$SetItemsModel;->a()LX/0Px;

    move-result-object v6

    .line 2554203
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2554204
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v8, :cond_4

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;

    .line 2554205
    if-eqz v4, :cond_3

    .line 2554206
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->d()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v9

    .line 2554207
    if-eqz v9, :cond_3

    .line 2554208
    invoke-virtual {v9}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v9

    .line 2554209
    if-eqz v9, :cond_3

    .line 2554210
    invoke-virtual {v9}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v9

    .line 2554211
    iget-object p0, v2, LX/IFU;->b:Ljava/lang/String;

    iget-object p1, v2, LX/IFU;->h:LX/IFa;

    invoke-static {v4, p0, v9, p1}, LX/IFZ;->a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;Ljava/lang/String;ZLX/IFa;)LX/IFZ;

    move-result-object v4

    .line 2554212
    if-eqz v4, :cond_3

    .line 2554213
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554214
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 2554215
    :cond_4
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto/16 :goto_1
.end method
