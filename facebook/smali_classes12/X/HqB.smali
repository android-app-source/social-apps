.class public final LX/HqB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HqA;


# instance fields
.field public final synthetic a:LX/HqE;


# direct methods
.method public constructor <init>(LX/HqE;)V
    .locals 0

    .prologue
    .line 2510287
    iput-object p1, p0, LX/HqB;->a:LX/HqE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2510282
    iget-object v0, p0, LX/HqB;->a:LX/HqE;

    invoke-static {v0, v1}, LX/HqE;->a$redex0(LX/HqE;Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2510283
    :goto_0
    return v0

    .line 2510284
    :cond_0
    iget-object v0, p0, LX/HqB;->a:LX/HqE;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-static {v0, v2}, LX/HqE;->a$redex0(LX/HqE;Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/HqB;->a:LX/HqE;

    iget-object v0, v0, LX/HqE;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2510285
    goto :goto_0

    .line 2510286
    :cond_1
    iget-object v0, p0, LX/HqB;->a:LX/HqE;

    iget-object v0, v0, LX/HqE;->c:LX/339;

    invoke-virtual {p0}, LX/HqB;->b()LX/2by;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/339;->b(LX/2by;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HqB;->a:LX/HqE;

    iget-object v0, v0, LX/HqE;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b()LX/2by;
    .locals 1

    .prologue
    .line 2510281
    sget-object v0, LX/2by;->AUDIENCE_ALIGNMENT_EDUCATOR:LX/2by;

    return-object v0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 2510280
    const-class v0, Lcom/facebook/privacy/educator/AudienceEducatorActivity;

    return-object v0
.end method
