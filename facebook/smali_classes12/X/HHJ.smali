.class public final LX/HHJ;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2447472
    const-class v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;

    const v0, -0xea80c6c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PagePostedPhotosQuery"

    const-string v6, "b9302fc60581db866c4d4a66b093573e"

    const-string v7, "page"

    const-string v8, "10155207368731729"

    const-string v9, "10155259089561729"

    .line 2447473
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2447474
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2447475
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2447476
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2447477
    sparse-switch v0, :sswitch_data_0

    .line 2447478
    :goto_0
    return-object p1

    .line 2447479
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2447480
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2447481
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2447482
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2447483
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2447484
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2447485
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2447486
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2447487
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2447488
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2447489
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2447490
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2447491
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2447492
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2447493
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2447494
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2447495
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2447496
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2447497
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 2447498
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 2447499
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 2447500
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 2447501
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 2447502
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 2447503
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 2447504
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 2447505
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 2447506
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 2447507
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6e3ba572 -> :sswitch_2
        -0x6a24640d -> :sswitch_1a
        -0x680de62a -> :sswitch_c
        -0x6326fdb3 -> :sswitch_8
        -0x5305c081 -> :sswitch_4
        -0x4496acc9 -> :sswitch_d
        -0x421ba035 -> :sswitch_b
        -0x41a91745 -> :sswitch_19
        -0x3c54de38 -> :sswitch_13
        -0x3be4a271 -> :sswitch_9
        -0x3b85b241 -> :sswitch_1c
        -0x36dc9999 -> :sswitch_17
        -0x30b65c8f -> :sswitch_6
        -0x2fe52f35 -> :sswitch_18
        -0x2c889631 -> :sswitch_12
        -0x2a63cba2 -> :sswitch_a
        -0x1b87b280 -> :sswitch_7
        -0x12efdeb3 -> :sswitch_e
        0x58705dc -> :sswitch_1
        0x5a7510f -> :sswitch_5
        0xa1fa812 -> :sswitch_0
        0x214100e0 -> :sswitch_f
        0x2d446f86 -> :sswitch_11
        0x3052e0ff -> :sswitch_3
        0x518e5e8f -> :sswitch_16
        0x53ffd947 -> :sswitch_14
        0x617a8767 -> :sswitch_10
        0x73a026b5 -> :sswitch_15
        0x7506f93c -> :sswitch_1b
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2447508
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2447509
    :goto_1
    return v0

    .line 2447510
    :sswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "19"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "28"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 2447511
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2447512
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2447513
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2447514
    :pswitch_3
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x33 -> :sswitch_0
        0x36 -> :sswitch_1
        0x628 -> :sswitch_2
        0x646 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
