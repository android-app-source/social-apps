.class public final enum LX/Hkk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hkk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/Hkk;

.field public static final enum b:LX/Hkk;

.field public static final enum c:LX/Hkk;

.field private static final synthetic d:[LX/Hkk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LX/Hkk;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/Hkk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hkk;->a:LX/Hkk;

    new-instance v0, LX/Hkk;

    const-string v1, "INSTALLED"

    invoke-direct {v0, v1, v3}, LX/Hkk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hkk;->b:LX/Hkk;

    new-instance v0, LX/Hkk;

    const-string v1, "NOT_INSTALLED"

    invoke-direct {v0, v1, v4}, LX/Hkk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hkk;->c:LX/Hkk;

    const/4 v0, 0x3

    new-array v0, v0, [LX/Hkk;

    sget-object v1, LX/Hkk;->a:LX/Hkk;

    aput-object v1, v0, v2

    sget-object v1, LX/Hkk;->b:LX/Hkk;

    aput-object v1, v0, v3

    sget-object v1, LX/Hkk;->c:LX/Hkk;

    aput-object v1, v0, v4

    sput-object v0, LX/Hkk;->d:[LX/Hkk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/Hkk;
    .locals 1

    invoke-static {p0}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/Hkk;->a:LX/Hkk;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hkk;->valueOf(Ljava/lang/String;)LX/Hkk;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    sget-object v0, LX/Hkk;->a:LX/Hkk;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hkk;
    .locals 1

    const-class v0, LX/Hkk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hkk;

    return-object v0
.end method

.method public static values()[LX/Hkk;
    .locals 1

    sget-object v0, LX/Hkk;->d:[LX/Hkk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hkk;

    return-object v0
.end method
