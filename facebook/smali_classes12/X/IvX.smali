.class public final LX/IvX;
.super LX/0xh;
.source ""


# instance fields
.field public a:I

.field public final synthetic b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 2

    .prologue
    .line 2628386
    iput-object p1, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-direct {p0}, LX/0xh;-><init>()V

    .line 2628387
    iget-object v0, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-virtual {v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0a5f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/IvX;->a:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;B)V
    .locals 0

    .prologue
    .line 2628388
    invoke-direct {p0, p1}, LX/IvX;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 12

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 2628389
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v10, v0

    .line 2628390
    float-to-double v0, v10

    iget v6, p0, LX/IvX;->a:I

    int-to-double v8, v6

    move-wide v6, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v11, v0

    .line 2628391
    float-to-double v0, v10

    const-wide v6, 0x3fe6666660000000L    # 0.699999988079071

    move-wide v8, v4

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 2628392
    iget-object v1, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    invoke-virtual {v1, v11}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 2628393
    iget-object v1, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->G:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2628394
    iget-object v0, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-boolean v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ah:Z

    if-eqz v0, :cond_0

    .line 2628395
    iget-object v0, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->K:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 2628396
    iget-object v0, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v11, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    float-to-double v0, v10

    iget-object v6, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget v6, v6, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ag:I

    neg-int v6, v6

    int-to-double v6, v6

    move-wide v8, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 2628397
    :cond_0
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2628398
    iget-object v0, p0, LX/IvX;->b:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    const/4 v1, 0x0

    .line 2628399
    iput-boolean v1, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ah:Z

    .line 2628400
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 2628401
    invoke-virtual {p1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2628402
    :cond_0
    return-void
.end method
