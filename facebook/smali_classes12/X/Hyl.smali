.class public final LX/Hyl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524502
    iput-object p1, p0, LX/Hyl;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2524503
    iget-object v0, p0, LX/Hyl;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->i:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    iget-object v2, p0, LX/Hyl;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->am:LX/0Vd;

    iget-object v3, p0, LX/Hyl;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aj:LX/0m9;

    const-string v4, "event_discovery_dashboard"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    .line 2524504
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2524505
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2524506
    if-eqz p1, :cond_0

    .line 2524507
    iget-object v0, p0, LX/Hyl;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aj:LX/0m9;

    const-string v1, "lat_lon"

    invoke-static {p1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a(Lcom/facebook/location/ImmutableLocation;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2524508
    :cond_0
    iget-object v0, p0, LX/Hyl;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->i:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    iget-object v2, p0, LX/Hyl;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->am:LX/0Vd;

    iget-object v3, p0, LX/Hyl;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aj:LX/0m9;

    const-string v4, "event_discovery_dashboard"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    .line 2524509
    return-void
.end method
