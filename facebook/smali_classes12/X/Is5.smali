.class public final LX/Is5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/StickerPackAdapter;I)V
    .locals 0

    .prologue
    .line 2619316
    iput-object p1, p0, LX/Is5;->b:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    iput p2, p0, LX/Is5;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2f049234

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2619298
    iget-object v0, p0, LX/Is5;->b:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->b:LX/IqQ;

    if-eqz v0, :cond_0

    .line 2619299
    iget-object v0, p0, LX/Is5;->b:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    iget-object v2, v0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->b:LX/IqQ;

    iget-object v0, p0, LX/Is5;->b:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->a:Ljava/util/List;

    iget v3, p0, LX/Is5;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 2619300
    iget-object v3, v2, LX/IqQ;->a:LX/IqX;

    .line 2619301
    sget-object p0, LX/IqV;->STICKERLIST:LX/IqV;

    invoke-virtual {v3, p0}, LX/IqX;->setStateAndVisibilities(LX/IqV;)V

    .line 2619302
    iget-object p0, v3, LX/IqX;->h:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    .line 2619303
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    if-ne v0, v2, :cond_1

    .line 2619304
    :cond_0
    :goto_0
    const v0, 0x75f579c1

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2619305
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    .line 2619306
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->f(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)Z

    move-result v2

    .line 2619307
    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 2619308
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2619309
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    if-eqz v3, :cond_0

    .line 2619310
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->d(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)V

    .line 2619311
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, LX/1OM;->i_(I)V

    .line 2619312
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->f(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)Z

    move-result v3

    if-eq v3, v2, :cond_2

    .line 2619313
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0

    .line 2619314
    :cond_2
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->f(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2619315
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, LX/1OM;->i_(I)V

    goto :goto_0
.end method
