.class public abstract LX/I9K;
.super LX/3Tf;
.source ""

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field public f:[I

.field public g:LX/Blc;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/IA6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2542757
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2542758
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/I9K;->d:Ljava/util/Map;

    .line 2542759
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2542760
    iput-object v0, p0, LX/I9K;->h:LX/0Px;

    .line 2542761
    iput-object p1, p0, LX/I9K;->c:Landroid/content/Context;

    .line 2542762
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2542806
    sget-object v0, LX/IAr;->HEADER:LX/IAr;

    invoke-virtual {v0}, LX/IAr;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2542796
    if-eqz p2, :cond_0

    .line 2542797
    check-cast p2, Landroid/view/ViewGroup;

    .line 2542798
    :goto_0
    invoke-virtual {p0, p1}, LX/I9K;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 2542799
    const v1, 0x7f0d0f4c

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2542800
    invoke-interface {v0}, LX/621;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2542801
    invoke-interface {v0}, LX/621;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2542802
    return-object p2

    .line 2542803
    :cond_0
    iget-object v0, p0, LX/I9K;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2542804
    const v1, 0x7f030588

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object p2, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2542805
    goto :goto_1
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2542795
    iget-object v0, p0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IA6;

    invoke-virtual {v0}, LX/622;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/IA6;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2542784
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2542785
    new-instance v3, Ljava/util/IdentityHashMap;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/IdentityHashMap;-><init>(I)V

    .line 2542786
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IA6;

    .line 2542787
    invoke-virtual {v3, v0}, Ljava/util/IdentityHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2542788
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2542789
    invoke-virtual {v3, v0, v0}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2542790
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2542791
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/I9K;->h:LX/0Px;

    .line 2542792
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/I9K;->f:[I

    .line 2542793
    const v0, 0x2056e8a7

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2542794
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2542780
    iget-boolean v0, p0, LX/I9K;->e:Z

    if-eq v0, p1, :cond_0

    .line 2542781
    iput-boolean p1, p0, LX/I9K;->e:Z

    .line 2542782
    const v0, -0x49835385

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2542783
    :cond_0
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 2542779
    const/4 v0, 0x0

    return v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2542807
    iget-object v0, p0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2542778
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2542777
    iget-object v0, p0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 2542770
    iget-object v0, p0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 2542771
    invoke-interface {v0}, LX/621;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2542772
    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2542773
    :goto_0
    iget-boolean v1, p0, LX/I9K;->e:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_0

    .line 2542774
    add-int/lit8 v0, v0, 0x1

    .line 2542775
    :cond_0
    return v0

    .line 2542776
    :cond_1
    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 1

    .prologue
    .line 2542767
    iget-boolean v0, p0, LX/I9K;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IA6;

    invoke-virtual {v0}, LX/622;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2542768
    sget-object v0, LX/IAr;->LOADING:LX/IAr;

    invoke-virtual {v0}, LX/IAr;->ordinal()I

    move-result v0

    .line 2542769
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/IAr;->CHILD:LX/IAr;

    invoke-virtual {v0}, LX/IAr;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 2542765
    iget-object v0, p0, LX/I9K;->f:[I

    aput p2, v0, p1

    .line 2542766
    return-void
.end method

.method public final f(I)I
    .locals 1

    .prologue
    .line 2542764
    iget-object v0, p0, LX/I9K;->f:[I

    aget v0, v0, p1

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2542763
    invoke-static {}, LX/IAr;->values()[LX/IAr;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
