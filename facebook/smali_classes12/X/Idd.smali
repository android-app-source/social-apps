.class public LX/Idd;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/view/View;

.field public b:Z

.field public c:Z

.field public d:Landroid/graphics/Rect;

.field public e:Landroid/graphics/RectF;

.field public f:Landroid/graphics/Matrix;

.field private h:LX/Idc;

.field public i:Landroid/graphics/RectF;

.field public j:Z

.field public k:F

.field private l:Z

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:Landroid/graphics/drawable/Drawable;

.field public o:Landroid/graphics/drawable/Drawable;

.field private final p:Landroid/graphics/Paint;

.field private final q:Landroid/graphics/Paint;

.field private final r:Landroid/graphics/Paint;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2597683
    const-class v0, LX/Idd;

    sput-object v0, LX/Idd;->g:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2597673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2597674
    sget-object v0, LX/Idc;->None:LX/Idc;

    iput-object v0, p0, LX/Idd;->h:LX/Idc;

    .line 2597675
    iput-boolean v1, p0, LX/Idd;->j:Z

    .line 2597676
    iput-boolean v1, p0, LX/Idd;->l:Z

    .line 2597677
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Idd;->p:Landroid/graphics/Paint;

    .line 2597678
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Idd;->q:Landroid/graphics/Paint;

    .line 2597679
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Idd;->r:Landroid/graphics/Paint;

    .line 2597680
    iput-object p1, p0, LX/Idd;->a:Landroid/view/View;

    .line 2597681
    iget-object v0, p0, LX/Idd;->a:Landroid/view/View;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 2597682
    return-void
.end method

.method public static e(LX/Idd;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 2597670
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2597671
    iget-object v1, p0, LX/Idd;->f:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 2597672
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method


# virtual methods
.method public final a(FF)I
    .locals 10

    .prologue
    const/16 v4, 0x20

    const/4 v2, 0x0

    const/4 v9, 0x0

    const/4 v1, 0x1

    const/high16 v8, 0x41a00000    # 20.0f

    .line 2597639
    invoke-static {p0}, LX/Idd;->e(LX/Idd;)Landroid/graphics/Rect;

    move-result-object v5

    .line 2597640
    iget-boolean v0, p0, LX/Idd;->l:Z

    if-eqz v0, :cond_5

    .line 2597641
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    .line 2597642
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    sub-float v2, p2, v2

    .line 2597643
    mul-float v3, v0, v0

    mul-float v5, v2, v2

    add-float/2addr v3, v5

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v3, v6

    .line 2597644
    iget-object v5, p0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    .line 2597645
    sub-int v6, v3, v5

    .line 2597646
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v6, v8

    if-gtz v6, :cond_4

    .line 2597647
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    .line 2597648
    cmpg-float v0, v2, v9

    if-gez v0, :cond_1

    .line 2597649
    const/16 v1, 0x8

    .line 2597650
    :cond_0
    :goto_0
    return v1

    .line 2597651
    :cond_1
    const/16 v1, 0x10

    goto :goto_0

    .line 2597652
    :cond_2
    cmpg-float v0, v0, v9

    if-gez v0, :cond_3

    .line 2597653
    const/4 v1, 0x2

    goto :goto_0

    .line 2597654
    :cond_3
    const/4 v1, 0x4

    goto :goto_0

    .line 2597655
    :cond_4
    if-ge v3, v5, :cond_0

    move v1, v4

    .line 2597656
    goto :goto_0

    .line 2597657
    :cond_5
    iget v0, v5, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float/2addr v0, v8

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_9

    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    add-float/2addr v0, v8

    cmpg-float v0, p2, v0

    if-gez v0, :cond_9

    move v0, v1

    .line 2597658
    :goto_1
    iget v3, v5, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    sub-float/2addr v3, v8

    cmpl-float v3, p1, v3

    if-ltz v3, :cond_6

    iget v3, v5, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    add-float/2addr v3, v8

    cmpg-float v3, p1, v3

    if-gez v3, :cond_6

    move v2, v1

    .line 2597659
    :cond_6
    iget v3, v5, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    sub-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v8

    if-gez v3, :cond_c

    if-eqz v0, :cond_c

    .line 2597660
    const/4 v3, 0x3

    .line 2597661
    :goto_2
    iget v6, v5, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    sub-float/2addr v6, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v8

    if-gez v6, :cond_7

    if-eqz v0, :cond_7

    .line 2597662
    or-int/lit8 v3, v3, 0x4

    .line 2597663
    :cond_7
    iget v0, v5, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v8

    if-gez v0, :cond_8

    if-eqz v2, :cond_8

    .line 2597664
    or-int/lit8 v3, v3, 0x8

    .line 2597665
    :cond_8
    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v8

    if-gez v0, :cond_b

    if-eqz v2, :cond_b

    .line 2597666
    or-int/lit8 v3, v3, 0x10

    move v0, v3

    .line 2597667
    :goto_3
    if-ne v0, v1, :cond_a

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_a

    move v1, v4

    .line 2597668
    goto :goto_0

    :cond_9
    move v0, v2

    .line 2597669
    goto :goto_1

    :cond_a
    move v1, v0

    goto/16 :goto_0

    :cond_b
    move v0, v3

    goto :goto_3

    :cond_c
    move v3, v1

    goto :goto_2
.end method

.method public final a(LX/Idc;)V
    .locals 1

    .prologue
    .line 2597563
    iget-object v0, p0, LX/Idd;->h:LX/Idc;

    if-eq p1, v0, :cond_0

    .line 2597564
    iput-object p1, p0, LX/Idd;->h:LX/Idc;

    .line 2597565
    iget-object v0, p0, LX/Idd;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 2597566
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 2597589
    iget-boolean v0, p0, LX/Idd;->c:Z

    if-eqz v0, :cond_1

    .line 2597590
    :cond_0
    :goto_0
    return-void

    .line 2597591
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2597592
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 2597593
    iget-boolean v0, p0, LX/Idd;->b:Z

    move v0, v0

    .line 2597594
    if-nez v0, :cond_2

    .line 2597595
    iget-object v0, p0, LX/Idd;->r:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2597596
    iget-object v0, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget-object v1, p0, LX/Idd;->r:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 2597597
    :cond_2
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 2597598
    iget-object v0, p0, LX/Idd;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2597599
    iget-boolean v0, p0, LX/Idd;->l:Z

    if-eqz v0, :cond_3

    .line 2597600
    iget-object v0, p0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    .line 2597601
    iget-object v3, p0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    .line 2597602
    iget-object v4, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    div-float v5, v0, v6

    add-float/2addr v4, v5

    iget-object v5, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    div-float/2addr v3, v6

    add-float/2addr v3, v5

    div-float/2addr v0, v6

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v4, v3, v0, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 2597603
    iget-object v0, p0, LX/Idd;->r:Landroid/graphics/Paint;

    const v3, -0x10fb2a

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2597604
    :goto_1
    sget-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 2597605
    iget-boolean v0, p0, LX/Idd;->b:Z

    move v0, v0

    .line 2597606
    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Idd;->p:Landroid/graphics/Paint;

    :goto_2
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2597607
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2597608
    iget-object v0, p0, LX/Idd;->r:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2597609
    iget-object v0, p0, LX/Idd;->h:LX/Idc;

    sget-object v1, LX/Idc;->Grow:LX/Idc;

    if-ne v0, v1, :cond_0

    .line 2597610
    iget-boolean v0, p0, LX/Idd;->l:Z

    if-eqz v0, :cond_5

    .line 2597611
    iget-object v0, p0, LX/Idd;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 2597612
    iget-object v1, p0, LX/Idd;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 2597613
    const-wide v2, 0x3fe921fb54442d18L    # 0.7853981633974483

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    iget-object v4, p0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    .line 2597614
    iget-object v3, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    add-int/2addr v3, v2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    .line 2597615
    iget-object v3, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    sub-int v2, v3, v2

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    .line 2597616
    iget-object v2, p0, LX/Idd;->o:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, LX/Idd;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, LX/Idd;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2597617
    iget-object v0, p0, LX/Idd;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 2597618
    :cond_3
    new-instance v0, Landroid/graphics/RectF;

    iget-object v3, p0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-direct {v0, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v0, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 2597619
    iget-object v0, p0, LX/Idd;->r:Landroid/graphics/Paint;

    iget-object v3, p0, LX/Idd;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a019a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_1

    .line 2597620
    :cond_4
    iget-object v0, p0, LX/Idd;->q:Landroid/graphics/Paint;

    goto/16 :goto_2

    .line 2597621
    :cond_5
    iget-object v0, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v0, 0x1

    .line 2597622
    iget-object v1, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v1, v1, 0x1

    .line 2597623
    iget-object v2, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v2, v2, 0x4

    .line 2597624
    iget-object v3, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, 0x3

    .line 2597625
    iget-object v4, p0, LX/Idd;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 2597626
    iget-object v5, p0, LX/Idd;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    .line 2597627
    iget-object v6, p0, LX/Idd;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    .line 2597628
    iget-object v7, p0, LX/Idd;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    .line 2597629
    iget-object v8, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    iget-object v10, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    .line 2597630
    iget-object v9, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iget-object v10, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget-object v11, p0, LX/Idd;->d:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    .line 2597631
    iget-object v10, p0, LX/Idd;->m:Landroid/graphics/drawable/Drawable;

    sub-int v11, v0, v4

    sub-int v12, v9, v5

    add-int/2addr v0, v4

    add-int v13, v9, v5

    invoke-virtual {v10, v11, v12, v0, v13}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2597632
    iget-object v0, p0, LX/Idd;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2597633
    iget-object v0, p0, LX/Idd;->m:Landroid/graphics/drawable/Drawable;

    sub-int v10, v1, v4

    sub-int v11, v9, v5

    add-int/2addr v1, v4

    add-int v4, v9, v5

    invoke-virtual {v0, v10, v11, v1, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2597634
    iget-object v0, p0, LX/Idd;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2597635
    iget-object v0, p0, LX/Idd;->n:Landroid/graphics/drawable/Drawable;

    sub-int v1, v8, v7

    sub-int v4, v2, v6

    add-int v5, v8, v7

    add-int/2addr v2, v6

    invoke-virtual {v0, v1, v4, v5, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2597636
    iget-object v0, p0, LX/Idd;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2597637
    iget-object v0, p0, LX/Idd;->n:Landroid/graphics/drawable/Drawable;

    sub-int v1, v8, v7

    sub-int v2, v3, v6

    add-int v4, v8, v7

    add-int/2addr v3, v6

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2597638
    iget-object v0, p0, LX/Idd;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public final a(Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/RectF;ZZ)V
    .locals 5

    .prologue
    const/16 v4, 0x7d

    const/4 v0, 0x1

    const/16 v3, 0x32

    .line 2597570
    if-eqz p4, :cond_0

    move p5, v0

    .line 2597571
    :cond_0
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v1, p0, LX/Idd;->f:Landroid/graphics/Matrix;

    .line 2597572
    iput-object p3, p0, LX/Idd;->e:Landroid/graphics/RectF;

    .line 2597573
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iput-object v1, p0, LX/Idd;->i:Landroid/graphics/RectF;

    .line 2597574
    iput-boolean p5, p0, LX/Idd;->j:Z

    .line 2597575
    iput-boolean p4, p0, LX/Idd;->l:Z

    .line 2597576
    iget-object v1, p0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    iput v1, p0, LX/Idd;->k:F

    .line 2597577
    invoke-static {p0}, LX/Idd;->e(LX/Idd;)Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, LX/Idd;->d:Landroid/graphics/Rect;

    .line 2597578
    iget-object v1, p0, LX/Idd;->p:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2597579
    iget-object v1, p0, LX/Idd;->q:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2597580
    iget-object v1, p0, LX/Idd;->r:Landroid/graphics/Paint;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2597581
    iget-object v1, p0, LX/Idd;->r:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2597582
    iget-object v1, p0, LX/Idd;->r:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2597583
    sget-object v0, LX/Idc;->None:LX/Idc;

    iput-object v0, p0, LX/Idd;->h:LX/Idc;

    .line 2597584
    iget-object v0, p0, LX/Idd;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2597585
    const v1, 0x7f0211e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/Idd;->m:Landroid/graphics/drawable/Drawable;

    .line 2597586
    const v1, 0x7f0211e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/Idd;->n:Landroid/graphics/drawable/Drawable;

    .line 2597587
    const v1, 0x7f02125b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/Idd;->o:Landroid/graphics/drawable/Drawable;

    .line 2597588
    return-void
.end method

.method public final b()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 2597569
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget-object v3, p0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget-object v4, p0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2597567
    invoke-static {p0}, LX/Idd;->e(LX/Idd;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, LX/Idd;->d:Landroid/graphics/Rect;

    .line 2597568
    return-void
.end method
