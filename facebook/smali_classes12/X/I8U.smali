.class public final enum LX/I8U;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I8U;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I8U;

.field public static final enum ACTIVITY_ROW:LX/I8U;

.field public static final enum NULL_STATE:LX/I8U;

.field public static final enum PROGRESS_BAR_ROW:LX/I8U;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2541555
    new-instance v0, LX/I8U;

    const-string v1, "ACTIVITY_ROW"

    invoke-direct {v0, v1, v2}, LX/I8U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8U;->ACTIVITY_ROW:LX/I8U;

    .line 2541556
    new-instance v0, LX/I8U;

    const-string v1, "PROGRESS_BAR_ROW"

    invoke-direct {v0, v1, v3}, LX/I8U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8U;->PROGRESS_BAR_ROW:LX/I8U;

    .line 2541557
    new-instance v0, LX/I8U;

    const-string v1, "NULL_STATE"

    invoke-direct {v0, v1, v4}, LX/I8U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8U;->NULL_STATE:LX/I8U;

    .line 2541558
    const/4 v0, 0x3

    new-array v0, v0, [LX/I8U;

    sget-object v1, LX/I8U;->ACTIVITY_ROW:LX/I8U;

    aput-object v1, v0, v2

    sget-object v1, LX/I8U;->PROGRESS_BAR_ROW:LX/I8U;

    aput-object v1, v0, v3

    sget-object v1, LX/I8U;->NULL_STATE:LX/I8U;

    aput-object v1, v0, v4

    sput-object v0, LX/I8U;->$VALUES:[LX/I8U;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2541559
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I8U;
    .locals 1

    .prologue
    .line 2541560
    const-class v0, LX/I8U;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I8U;

    return-object v0
.end method

.method public static values()[LX/I8U;
    .locals 1

    .prologue
    .line 2541561
    sget-object v0, LX/I8U;->$VALUES:[LX/I8U;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I8U;

    return-object v0
.end method
