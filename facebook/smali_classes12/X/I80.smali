.class public LX/I80;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "LX/I83;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "LX/I83;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "LX/I83;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field public d:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2540462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540463
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/I80;->a:LX/0Pz;

    .line 2540464
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/I80;->b:LX/0Pz;

    .line 2540465
    iput p1, p0, LX/I80;->c:I

    .line 2540466
    return-void
.end method


# virtual methods
.method public final a(LX/I83;)V
    .locals 1

    .prologue
    .line 2540467
    iget-object v0, p0, LX/I80;->a:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2540468
    iget v0, p0, LX/I80;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/I80;->d:I

    .line 2540469
    return-void
.end method

.method public final a(LX/I83;LX/I83;)V
    .locals 2

    .prologue
    .line 2540470
    iget v0, p0, LX/I80;->d:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/I80;->c:I

    if-ge v0, v1, :cond_0

    .line 2540471
    invoke-virtual {p0, p1}, LX/I80;->a(LX/I83;)V

    .line 2540472
    :goto_0
    return-void

    .line 2540473
    :cond_0
    invoke-virtual {p0, p2}, LX/I80;->b(LX/I83;)V

    goto :goto_0
.end method

.method public final b(LX/I83;)V
    .locals 1

    .prologue
    .line 2540474
    iget-object v0, p0, LX/I80;->b:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2540475
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/I83;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2540476
    iget-object v0, p0, LX/I80;->a:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iget-object v1, p0, LX/I80;->b:LX/0Pz;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
