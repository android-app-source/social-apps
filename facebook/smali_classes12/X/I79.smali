.class public final LX/I79;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538640
    iput-object p1, p0, LX/I79;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 2538630
    iget-object v0, p0, LX/I79;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-boolean v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->av:Z

    if-eqz v0, :cond_0

    .line 2538631
    iget-object v0, p0, LX/I79;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->au:I

    if-nez v0, :cond_1

    .line 2538632
    iget-object v0, p0, LX/I79;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->at:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    .line 2538633
    :cond_0
    :goto_0
    iget-object v0, p0, LX/I79;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    .line 2538634
    iput p2, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->au:I

    .line 2538635
    return-void

    .line 2538636
    :cond_1
    if-nez p2, :cond_0

    iget-object v0, p0, LX/I79;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->at:LX/195;

    .line 2538637
    iget-boolean p1, v0, LX/195;->n:Z

    move v0, p1

    .line 2538638
    if-eqz v0, :cond_0

    .line 2538639
    iget-object v0, p0, LX/I79;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->at:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 2538608
    iget-object v0, p0, LX/I79;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    .line 2538609
    const/4 p1, 0x0

    .line 2538610
    iget-object p0, v0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    .line 2538611
    iget-object p4, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object p0, p4

    .line 2538612
    sget-object p4, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p0, p4, :cond_1

    iget-object p0, v0, LX/I8k;->F:LX/IBq;

    sget-object p4, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {p0, p4}, LX/IBq;->b(LX/IBr;)Z

    move-result p0

    if-eqz p0, :cond_1

    move p0, p1

    .line 2538613
    :goto_0
    move p0, p0

    .line 2538614
    if-eqz p0, :cond_0

    .line 2538615
    iget-object p0, v0, LX/I8k;->w:LX/I5f;

    .line 2538616
    iget-boolean p1, p0, LX/I5f;->q:Z

    if-nez p1, :cond_0

    .line 2538617
    iget-object p1, p0, LX/I5f;->f:LX/1Ck;

    sget-object p4, LX/I5e;->LOAD_NEXT_PAGE:LX/I5e;

    iget-object v0, p0, LX/I5f;->i:Ljava/util/concurrent/Callable;

    iget-object p2, p0, LX/I5f;->j:LX/0Vd;

    invoke-virtual {p1, p4, v0, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2538618
    :cond_0
    return-void

    .line 2538619
    :cond_1
    invoke-static {v0}, LX/I8k;->p(LX/I8k;)Z

    move-result p0

    if-nez p0, :cond_2

    iget-object p0, v0, LX/I8k;->w:LX/I5f;

    .line 2538620
    iget-boolean p4, p0, LX/I5f;->q:Z

    move p0, p4

    .line 2538621
    if-eqz p0, :cond_3

    :cond_2
    move p0, p1

    .line 2538622
    goto :goto_0

    .line 2538623
    :cond_3
    add-int p0, p2, p3

    add-int/lit8 p0, p0, -0x1

    .line 2538624
    if-gtz p3, :cond_4

    move p0, p1

    .line 2538625
    goto :goto_0

    .line 2538626
    :cond_4
    iget-object p4, v0, LX/I8k;->x:LX/I8N;

    if-eqz p4, :cond_5

    iget-object p4, v0, LX/I8k;->x:LX/I8N;

    iget-object p4, p4, LX/I8N;->a:Ljava/lang/Object;

    if-nez p4, :cond_6

    :cond_5
    move p0, p1

    .line 2538627
    goto :goto_0

    .line 2538628
    :cond_6
    iget-object p4, v0, LX/I8k;->x:LX/I8N;

    invoke-virtual {p4, p2, p0}, LX/I8N;->a(II)Landroid/util/Pair;

    move-result-object p0

    .line 2538629
    if-eqz p0, :cond_7

    iget-object p0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    add-int/lit8 p0, p0, 0x5

    iget-object p4, v0, LX/I8k;->x:LX/I8N;

    invoke-virtual {p4}, LX/I8N;->a()LX/1Cw;

    move-result-object p4

    invoke-interface {p4}, LX/1Cw;->getCount()I

    move-result p4

    if-le p0, p4, :cond_7

    const/4 p0, 0x1

    goto :goto_0

    :cond_7
    move p0, p1

    goto :goto_0
.end method
