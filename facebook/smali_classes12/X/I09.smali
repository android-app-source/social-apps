.class public LX/I09;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/I09;


# instance fields
.field public final a:LX/0Yj;

.field public final b:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2526579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2526580
    iput-object p1, p0, LX/I09;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2526581
    new-instance v0, LX/0Yj;

    const v1, 0x60013

    const-string v2, "EventsBirthdaysTTI"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "event_birthdays"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 2526582
    iput-boolean v4, v0, LX/0Yj;->n:Z

    .line 2526583
    move-object v0, v0

    .line 2526584
    iput-object v0, p0, LX/I09;->a:LX/0Yj;

    .line 2526585
    return-void
.end method

.method public static a(LX/0QB;)LX/I09;
    .locals 4

    .prologue
    .line 2526586
    sget-object v0, LX/I09;->c:LX/I09;

    if-nez v0, :cond_1

    .line 2526587
    const-class v1, LX/I09;

    monitor-enter v1

    .line 2526588
    :try_start_0
    sget-object v0, LX/I09;->c:LX/I09;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2526589
    if-eqz v2, :cond_0

    .line 2526590
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2526591
    new-instance p0, LX/I09;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/I09;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 2526592
    move-object v0, p0

    .line 2526593
    sput-object v0, LX/I09;->c:LX/I09;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2526594
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2526595
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2526596
    :cond_1
    sget-object v0, LX/I09;->c:LX/I09;

    return-object v0

    .line 2526597
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2526598
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
