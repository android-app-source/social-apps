.class public LX/IC1;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/IC1;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Uh;)V
    .locals 10
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2548092
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2548093
    iput-object p1, p0, LX/IC1;->a:LX/0Or;

    .line 2548094
    iput-object p2, p0, LX/IC1;->b:LX/0Uh;

    .line 2548095
    sget-object v0, LX/0ax;->iV:Ljava/lang/String;

    const-string v1, "{job_opening_id}"

    const-string v2, "{source}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "JobApplicationRoute"

    .line 2548096
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548097
    move-object v1, v1

    .line 2548098
    iput v5, v1, LX/98r;->h:I

    .line 2548099
    move-object v1, v1

    .line 2548100
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548101
    sget-object v0, LX/0ax;->iQ:Ljava/lang/String;

    const-string v1, "{job_opening_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "JobApplicationRoute"

    .line 2548102
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548103
    move-object v1, v1

    .line 2548104
    iput v5, v1, LX/98r;->h:I

    .line 2548105
    move-object v1, v1

    .line 2548106
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548107
    sget-object v0, LX/0ax;->iT:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "JobSearchRoute"

    .line 2548108
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548109
    move-object v1, v1

    .line 2548110
    iput v5, v1, LX/98r;->h:I

    .line 2548111
    move-object v1, v1

    .line 2548112
    const v2, 0x7f08310b

    .line 2548113
    iput v2, v1, LX/98r;->d:I

    .line 2548114
    move-object v1, v1

    .line 2548115
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548116
    sget-object v0, LX/0ax;->iS:Ljava/lang/String;

    const-string v1, "{job_opening_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "JobApplicationFormRoute"

    .line 2548117
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548118
    move-object v1, v1

    .line 2548119
    iput v5, v1, LX/98r;->h:I

    .line 2548120
    move-object v1, v1

    .line 2548121
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548122
    sget-object v0, LX/0ax;->fC:Ljava/lang/String;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "{campaign_id}"

    aput-object v2, v1, v6

    const-string v2, "{campaign_type}"

    aput-object v2, v1, v5

    const-string v2, "{editor_type}"

    aput-object v2, v1, v7

    const-string v2, "{initial_source}"

    aput-object v2, v1, v8

    const-string v2, "{direct_source}"

    aput-object v2, v1, v9

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "GoodwillVideoEditorRoute"

    .line 2548123
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548124
    move-object v1, v1

    .line 2548125
    iput v5, v1, LX/98r;->h:I

    .line 2548126
    move-object v1, v1

    .line 2548127
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548128
    sget-object v0, LX/0ax;->fv:Ljava/lang/String;

    const-string v1, "{crisisID}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "CrisisRoute"

    .line 2548129
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548130
    move-object v1, v1

    .line 2548131
    const v2, 0x7f083806

    .line 2548132
    iput v2, v1, LX/98r;->d:I

    .line 2548133
    move-object v1, v1

    .line 2548134
    iput-boolean v5, v1, LX/98r;->e:Z

    .line 2548135
    move-object v1, v1

    .line 2548136
    iput v5, v1, LX/98r;->h:I

    .line 2548137
    move-object v1, v1

    .line 2548138
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548139
    sget-object v0, LX/0ax;->fw:Ljava/lang/String;

    const-string v1, "{targetURI}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "CrisisHubShellRoute"

    .line 2548140
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548141
    move-object v1, v1

    .line 2548142
    const v2, 0x7f083806

    .line 2548143
    iput v2, v1, LX/98r;->d:I

    .line 2548144
    move-object v1, v1

    .line 2548145
    iput-boolean v5, v1, LX/98r;->e:Z

    .line 2548146
    move-object v1, v1

    .line 2548147
    iput v5, v1, LX/98r;->h:I

    .line 2548148
    move-object v1, v1

    .line 2548149
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548150
    sget-object v0, LX/0ax;->n:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "AdsManagerCampaignGroupInsightsRoute"

    .line 2548151
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548152
    move-object v1, v1

    .line 2548153
    const v2, 0x7f080aa6

    .line 2548154
    iput v2, v1, LX/98r;->d:I

    .line 2548155
    move-object v1, v1

    .line 2548156
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548157
    sget-object v0, LX/0ax;->p:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "AdsManagerImagePickerRoute"

    .line 2548158
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548159
    move-object v1, v1

    .line 2548160
    const v2, 0x7f080b07

    .line 2548161
    iput v2, v1, LX/98r;->d:I

    .line 2548162
    move-object v1, v1

    .line 2548163
    iput v5, v1, LX/98r;->h:I

    .line 2548164
    move-object v1, v1

    .line 2548165
    const-string v2, "AMAImagePickerFinishSelection"

    .line 2548166
    iput-object v2, v1, LX/98r;->i:Ljava/lang/String;

    .line 2548167
    move-object v1, v1

    .line 2548168
    const v2, 0x7f080ac0

    .line 2548169
    iput v2, v1, LX/98r;->k:I

    .line 2548170
    move-object v1, v1

    .line 2548171
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548172
    sget-object v0, LX/0ax;->x:Ljava/lang/String;

    const-string v1, "{product_id}"

    const-string v2, "{page_set_id}"

    const-string v3, "{ad_id}"

    const-string v4, "{ref_id}"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "RetailProductRoute"

    .line 2548173
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548174
    move-object v1, v1

    .line 2548175
    iput v5, v1, LX/98r;->h:I

    .line 2548176
    move-object v1, v1

    .line 2548177
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548178
    sget-object v0, LX/0ax;->t:Ljava/lang/String;

    const-string v1, "{entryPoint}"

    const-string v2, "{targetURI}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "AMAShellRoute"

    .line 2548179
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548180
    move-object v1, v1

    .line 2548181
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548182
    move-object v1, v1

    .line 2548183
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548184
    sget-object v0, LX/0ax;->u:Ljava/lang/String;

    const-string v1, "{account}"

    const-string v2, "{isCheckout 1}"

    const-string v3, "{dismissOnCompletion 0}"

    const-string v4, "{contextID 0}"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "AdsPaymentsCreditCardRoute"

    .line 2548185
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548186
    move-object v1, v1

    .line 2548187
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548188
    move-object v1, v1

    .line 2548189
    const v2, 0x7f082810

    .line 2548190
    iput v2, v1, LX/98r;->d:I

    .line 2548191
    move-object v1, v1

    .line 2548192
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548193
    sget-object v0, LX/0ax;->v:Ljava/lang/String;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "{account}"

    aput-object v2, v1, v6

    const-string v2, "{page}"

    aput-object v2, v1, v5

    const-string v2, "{boostMessage unknown}"

    aput-object v2, v1, v7

    const-string v2, "{boostImageURI unknown}"

    aput-object v2, v1, v8

    const-string v2, "{boostResultType}"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "{boostResultLowerBound}"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "{boostResultUpperBound}"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "{boostDuration}"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "{budgetAmount}"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "{budgetCurrency}"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "{budgetType}"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "{credentialID 0}"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "{cachedCscToken 0}"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "{contextID 0}"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "AdsPaymentsCheckoutRoute"

    .line 2548194
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548195
    move-object v1, v1

    .line 2548196
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548197
    move-object v1, v1

    .line 2548198
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->c(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548199
    sget-object v0, LX/0ax;->w:Ljava/lang/String;

    const-string v1, "{account}"

    const-string v2, "{campaignGroupID 0}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "AdsPaymentsCheckoutCampaignReceiptRoute"

    .line 2548200
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548201
    move-object v1, v1

    .line 2548202
    const v2, 0x7f080ab7

    .line 2548203
    iput v2, v1, LX/98r;->d:I

    .line 2548204
    move-object v1, v1

    .line 2548205
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548206
    move-object v1, v1

    .line 2548207
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548208
    sget-object v0, LX/0ax;->fx:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "PokesDashboardRoute"

    .line 2548209
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548210
    move-object v1, v1

    .line 2548211
    const v2, 0x7f083813

    .line 2548212
    iput v2, v1, LX/98r;->d:I

    .line 2548213
    move-object v1, v1

    .line 2548214
    iput-boolean v5, v1, LX/98r;->e:Z

    .line 2548215
    move-object v1, v1

    .line 2548216
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548217
    sget-object v0, LX/0ax;->di:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ProfileEditRoute"

    .line 2548218
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548219
    move-object v1, v1

    .line 2548220
    const v2, 0x7f0815ca

    .line 2548221
    iput v2, v1, LX/98r;->d:I

    .line 2548222
    move-object v1, v1

    .line 2548223
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548224
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2548225
    const-string v1, "surface"

    const-string v2, "profile_curation_tags_edit_view"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2548226
    sget-object v1, LX/0ax;->dj:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v2

    const-string v3, "ProfileEditTagsRoute"

    .line 2548227
    iput-object v3, v2, LX/98r;->b:Ljava/lang/String;

    .line 2548228
    move-object v2, v2

    .line 2548229
    const v3, 0x7f0815cb

    .line 2548230
    iput v3, v2, LX/98r;->d:I

    .line 2548231
    move-object v2, v2

    .line 2548232
    iput-object v0, v2, LX/98r;->f:Landroid/os/Bundle;

    .line 2548233
    move-object v0, v2

    .line 2548234
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v1, v0}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548235
    sget-object v0, LX/0ax;->dA:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "PrivacySettingsRoute"

    .line 2548236
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548237
    move-object v1, v1

    .line 2548238
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548239
    move-object v1, v1

    .line 2548240
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548241
    sget-object v0, LX/0ax;->dB:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "PrivacySettingsRedesignRoute"

    .line 2548242
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548243
    move-object v1, v1

    .line 2548244
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548245
    move-object v1, v1

    .line 2548246
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548247
    sget-object v0, LX/0ax;->dD:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "FollowPrivacyOptionsRoute"

    .line 2548248
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548249
    move-object v1, v1

    .line 2548250
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548251
    move-object v1, v1

    .line 2548252
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548253
    sget-object v0, LX/0ax;->dF:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "LimitOldPostsPrivacyRoute"

    .line 2548254
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548255
    move-object v1, v1

    .line 2548256
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548257
    move-object v1, v1

    .line 2548258
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548259
    sget-object v0, LX/0ax;->dG:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "SearchByEmailPrivacyOptionsRoute"

    .line 2548260
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548261
    move-object v1, v1

    .line 2548262
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548263
    move-object v1, v1

    .line 2548264
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548265
    sget-object v0, LX/0ax;->dH:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "SearchByPhonePrivacyOptionsRoute"

    .line 2548266
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548267
    move-object v1, v1

    .line 2548268
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548269
    move-object v1, v1

    .line 2548270
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548271
    sget-object v0, LX/0ax;->dE:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "FriendRequestPrivacyOptionsRoute"

    .line 2548272
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548273
    move-object v1, v1

    .line 2548274
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548275
    move-object v1, v1

    .line 2548276
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548277
    sget-object v0, LX/0ax;->dC:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ComposerPrivacyOptionsRoute"

    .line 2548278
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548279
    move-object v1, v1

    .line 2548280
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548281
    move-object v1, v1

    .line 2548282
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548283
    sget-object v0, LX/0ax;->dI:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "EnablePublicPrivacyOptionForMinorsRoute"

    .line 2548284
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548285
    move-object v1, v1

    .line 2548286
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548287
    move-object v1, v1

    .line 2548288
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548289
    sget-object v0, LX/0ax;->dJ:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "FriendsListPrivacyOptionsRoute"

    .line 2548290
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548291
    move-object v1, v1

    .line 2548292
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548293
    move-object v1, v1

    .line 2548294
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548295
    sget-object v0, LX/0ax;->dK:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "AllowPublicSearchRoute"

    .line 2548296
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548297
    move-object v1, v1

    .line 2548298
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548299
    move-object v1, v1

    .line 2548300
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548301
    sget-object v0, LX/0ax;->dL:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "AllowPublicSearchRedesignRoute"

    .line 2548302
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548303
    move-object v1, v1

    .line 2548304
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548305
    move-object v1, v1

    .line 2548306
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548307
    sget-object v0, LX/0ax;->dM:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ComposerPrivacyRedirectRoute"

    .line 2548308
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548309
    move-object v1, v1

    .line 2548310
    iput-boolean v6, v1, LX/98r;->e:Z

    .line 2548311
    move-object v1, v1

    .line 2548312
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548313
    sget-object v0, LX/0ax;->hz:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "SampleIntegrationAppRoute"

    .line 2548314
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548315
    move-object v1, v1

    .line 2548316
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548317
    sget-object v0, LX/0ax;->hA:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "SimpleStoryAppRoute"

    .line 2548318
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548319
    move-object v1, v1

    .line 2548320
    const v2, 0x7f0814c7

    .line 2548321
    iput v2, v1, LX/98r;->d:I

    .line 2548322
    move-object v1, v1

    .line 2548323
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548324
    iget-object v0, p0, LX/IC1;->b:LX/0Uh;

    const/16 v1, 0x311

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2548325
    sget-object v0, LX/0ax;->fW:Ljava/lang/String;

    const-string v1, "{refID 0}"

    const-string v2, "{refType unknown}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "B2CSearch"

    .line 2548326
    iput-object v2, v1, LX/98r;->n:Ljava/lang/String;

    .line 2548327
    move-object v1, v1

    .line 2548328
    const-string v2, "ShopsFeedRoute"

    .line 2548329
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548330
    move-object v1, v1

    .line 2548331
    iput v5, v1, LX/98r;->h:I

    .line 2548332
    move-object v1, v1

    .line 2548333
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, LX/0cQ;->REACT_FRAGMENT_WITH_MARKETPLACE_SEARCH:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;I)V

    .line 2548334
    :goto_0
    sget-object v0, LX/0ax;->fX:Ljava/lang/String;

    const-string v1, "{refID 0}"

    const-string v2, "{refType unknown}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ShopsFeedCategoriesListRoute"

    .line 2548335
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548336
    move-object v1, v1

    .line 2548337
    iput v5, v1, LX/98r;->h:I

    .line 2548338
    move-object v1, v1

    .line 2548339
    const v2, 0x7f0814c7

    .line 2548340
    iput v2, v1, LX/98r;->d:I

    .line 2548341
    move-object v1, v1

    .line 2548342
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548343
    sget-object v0, LX/0ax;->fY:Ljava/lang/String;

    const-string v1, "{refID 0}"

    const-string v2, "{refType unknown}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ShopsFeedUnfinishedPurchaseListRoute"

    .line 2548344
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548345
    move-object v1, v1

    .line 2548346
    iput v5, v1, LX/98r;->h:I

    .line 2548347
    move-object v1, v1

    .line 2548348
    const v2, 0x7f0814c8

    .line 2548349
    iput v2, v1, LX/98r;->d:I

    .line 2548350
    move-object v1, v1

    .line 2548351
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548352
    sget-object v0, LX/0ax;->fQ:Ljava/lang/String;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "{productID}"

    aput-object v2, v1, v6

    const-string v2, "{refID 0}"

    aput-object v2, v1, v5

    const-string v2, "{refType unknown}"

    aput-object v2, v1, v7

    const-string v2, "{imageSizing 0}"

    aput-object v2, v1, v8

    const-string v2, "{contextID 0}"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "{styleType none}"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ShopsProductDetailsRoute"

    .line 2548353
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548354
    move-object v1, v1

    .line 2548355
    iput v5, v1, LX/98r;->h:I

    .line 2548356
    move-object v1, v1

    .line 2548357
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548358
    sget-object v0, LX/0ax;->fZ:Ljava/lang/String;

    const-string v1, "{refID 0}"

    const-string v2, "{refType unknown}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ShopsMerchantOnboardingTermsRoute"

    .line 2548359
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548360
    move-object v1, v1

    .line 2548361
    iput v5, v1, LX/98r;->h:I

    .line 2548362
    move-object v1, v1

    .line 2548363
    const v2, 0x7f0814c7

    .line 2548364
    iput v2, v1, LX/98r;->d:I

    .line 2548365
    move-object v1, v1

    .line 2548366
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548367
    sget-object v0, LX/0ax;->ga:Ljava/lang/String;

    const-string v1, "{refID 0}"

    const-string v2, "{refType unknown}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ShopsMerchantOnboardingShopTypeSelectionRoute"

    .line 2548368
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548369
    move-object v1, v1

    .line 2548370
    iput v5, v1, LX/98r;->h:I

    .line 2548371
    move-object v1, v1

    .line 2548372
    const v2, 0x7f0814c7

    .line 2548373
    iput v2, v1, LX/98r;->d:I

    .line 2548374
    move-object v1, v1

    .line 2548375
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548376
    sget-object v0, LX/0ax;->gb:Ljava/lang/String;

    const-string v1, "{refID 0}"

    const-string v2, "{refType unknown}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ShopsMerchantOnboardingCurrencySelectionRoute"

    .line 2548377
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548378
    move-object v1, v1

    .line 2548379
    iput v5, v1, LX/98r;->h:I

    .line 2548380
    move-object v1, v1

    .line 2548381
    const v2, 0x7f0814c7

    .line 2548382
    iput v2, v1, LX/98r;->d:I

    .line 2548383
    move-object v1, v1

    .line 2548384
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548385
    sget-object v0, LX/0ax;->gc:Ljava/lang/String;

    const-string v1, "{refID 0}"

    const-string v2, "{refType unknown}"

    const-string v3, "{categoryID 0}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ProductsFeedDealsRoute"

    .line 2548386
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548387
    move-object v1, v1

    .line 2548388
    iput v5, v1, LX/98r;->h:I

    .line 2548389
    move-object v1, v1

    .line 2548390
    const v2, 0x7f0814c9

    .line 2548391
    iput v2, v1, LX/98r;->d:I

    .line 2548392
    move-object v1, v1

    .line 2548393
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548394
    sget-object v0, LX/0ax;->gh:Ljava/lang/String;

    const-string v1, "{referralSurface bookmark}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "GroupCommerceBookmarkRoute"

    .line 2548395
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548396
    move-object v1, v1

    .line 2548397
    iput v5, v1, LX/98r;->h:I

    .line 2548398
    move-object v1, v1

    .line 2548399
    const v2, 0x7f082f1c

    .line 2548400
    iput v2, v1, LX/98r;->d:I

    .line 2548401
    move-object v1, v1

    .line 2548402
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548403
    sget-object v0, LX/0ax;->gj:Ljava/lang/String;

    const-string v1, "{referralSurface bookmark}"

    const-string v2, "{categoryId 0}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "GroupCommerceBookmarkCategoryRoute"

    .line 2548404
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548405
    move-object v1, v1

    .line 2548406
    iput v5, v1, LX/98r;->h:I

    .line 2548407
    move-object v1, v1

    .line 2548408
    const v2, 0x7f082f1c

    .line 2548409
    iput v2, v1, LX/98r;->d:I

    .line 2548410
    move-object v1, v1

    .line 2548411
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548412
    sget-object v0, LX/0ax;->gk:Ljava/lang/String;

    const-string v1, "{referralSurface bookmark}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "GroupCommerceBookmarkCategoryFeedRoute"

    .line 2548413
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548414
    move-object v1, v1

    .line 2548415
    iput v5, v1, LX/98r;->h:I

    .line 2548416
    move-object v1, v1

    .line 2548417
    const v2, 0x7f082f1c

    .line 2548418
    iput v2, v1, LX/98r;->d:I

    .line 2548419
    move-object v1, v1

    .line 2548420
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548421
    sget-object v0, LX/0ax;->gi:Ljava/lang/String;

    const-string v1, "{groupCommerceProductItemID 0}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "GroupCommerceMessageSellerRoute"

    .line 2548422
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548423
    move-object v1, v1

    .line 2548424
    iput v5, v1, LX/98r;->h:I

    .line 2548425
    move-object v1, v1

    .line 2548426
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548427
    sget-object v0, LX/0ax;->fk:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "DeviceRequestsRoute"

    .line 2548428
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548429
    move-object v1, v1

    .line 2548430
    iput v5, v1, LX/98r;->h:I

    .line 2548431
    move-object v1, v1

    .line 2548432
    const v2, 0x7f082ac5

    .line 2548433
    iput v2, v1, LX/98r;->d:I

    .line 2548434
    move-object v1, v1

    .line 2548435
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548436
    sget-object v0, LX/0ax;->fy:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "PageServiceAddEditRoute"

    .line 2548437
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548438
    move-object v1, v1

    .line 2548439
    const v2, 0x7f081812    # 1.8089999E38f

    .line 2548440
    iput v2, v1, LX/98r;->d:I

    .line 2548441
    move-object v1, v1

    .line 2548442
    const v2, 0x7f081789

    .line 2548443
    iput v2, v1, LX/98r;->k:I

    .line 2548444
    move-object v1, v1

    .line 2548445
    const-string v2, "PageServiceAddEditSaveSelection"

    .line 2548446
    iput-object v2, v1, LX/98r;->i:Ljava/lang/String;

    .line 2548447
    move-object v1, v1

    .line 2548448
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548449
    sget-object v0, LX/0ax;->hL:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceHomeRoute"

    .line 2548450
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548451
    move-object v1, v1

    .line 2548452
    iput v5, v1, LX/98r;->h:I

    .line 2548453
    move-object v1, v1

    .line 2548454
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548455
    sget-object v0, LX/0ax;->hK:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "LoyaltyHomeRoute"

    .line 2548456
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548457
    move-object v1, v1

    .line 2548458
    const v2, 0x7f083807

    .line 2548459
    iput v2, v1, LX/98r;->d:I

    .line 2548460
    move-object v1, v1

    .line 2548461
    iput v5, v1, LX/98r;->h:I

    .line 2548462
    move-object v1, v1

    .line 2548463
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548464
    sget-object v0, LX/0ax;->hZ:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceActivityRoute"

    .line 2548465
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548466
    move-object v1, v1

    .line 2548467
    const v2, 0x7f083808

    .line 2548468
    iput v2, v1, LX/98r;->d:I

    .line 2548469
    move-object v1, v1

    .line 2548470
    iput v5, v1, LX/98r;->h:I

    .line 2548471
    move-object v1, v1

    .line 2548472
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548473
    sget-object v0, LX/0ax;->hN:Ljava/lang/String;

    const-string v1, "{tabIndex}"

    const-string v2, "{referralSurface}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "LocalSellHomeRoute"

    .line 2548474
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548475
    move-object v1, v1

    .line 2548476
    iput v5, v1, LX/98r;->h:I

    .line 2548477
    move-object v1, v1

    .line 2548478
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548479
    sget-object v0, LX/0ax;->in:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceDraftItemsRoute"

    .line 2548480
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548481
    move-object v1, v1

    .line 2548482
    const v2, 0x7f08380f

    .line 2548483
    iput v2, v1, LX/98r;->d:I

    .line 2548484
    move-object v1, v1

    .line 2548485
    iput v5, v1, LX/98r;->h:I

    .line 2548486
    move-object v1, v1

    .line 2548487
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548488
    sget-object v0, LX/0ax;->hP:Ljava/lang/String;

    const-string v1, "{assetIDs}"

    const-string v2, "{defaultCategoryID}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceComposerRoute"

    .line 2548489
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548490
    move-object v1, v1

    .line 2548491
    iput v5, v1, LX/98r;->h:I

    .line 2548492
    move-object v1, v1

    .line 2548493
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548494
    sget-object v0, LX/0ax;->hO:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceAudienceListRoute"

    .line 2548495
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548496
    move-object v1, v1

    .line 2548497
    iput v5, v1, LX/98r;->h:I

    .line 2548498
    move-object v1, v1

    .line 2548499
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548500
    sget-object v0, LX/0ax;->hR:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplacePhotoChooserComposerRoute"

    .line 2548501
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548502
    move-object v1, v1

    .line 2548503
    iput v5, v1, LX/98r;->h:I

    .line 2548504
    move-object v1, v1

    .line 2548505
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548506
    sget-object v0, LX/0ax;->hS:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceCrossPostRoute"

    .line 2548507
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548508
    move-object v1, v1

    .line 2548509
    iput v5, v1, LX/98r;->h:I

    .line 2548510
    move-object v1, v1

    .line 2548511
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548512
    sget-object v0, LX/0ax;->hQ:Ljava/lang/String;

    const-string v1, "{storyID}"

    const-string v2, "{referralSurface}"

    const-string v3, "{isDraft}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceEditComposerRoute"

    .line 2548513
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548514
    move-object v1, v1

    .line 2548515
    iput v5, v1, LX/98r;->h:I

    .line 2548516
    move-object v1, v1

    .line 2548517
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548518
    sget-object v0, LX/0ax;->hT:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceForSaleGroupsHomeRoute"

    .line 2548519
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548520
    move-object v1, v1

    .line 2548521
    iput v5, v1, LX/98r;->h:I

    .line 2548522
    move-object v1, v1

    .line 2548523
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548524
    sget-object v0, LX/0ax;->hV:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceProductDetailsRoute"

    .line 2548525
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548526
    move-object v1, v1

    .line 2548527
    iput v5, v1, LX/98r;->h:I

    .line 2548528
    move-object v1, v1

    .line 2548529
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548530
    sget-object v0, LX/0ax;->hW:Ljava/lang/String;

    const-string v1, "{id}"

    const-string v2, "{referralSurface}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceProductDetailsRoute"

    .line 2548531
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548532
    move-object v1, v1

    .line 2548533
    iput v5, v1, LX/98r;->h:I

    .line 2548534
    move-object v1, v1

    .line 2548535
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548536
    sget-object v0, LX/0ax;->hX:Ljava/lang/String;

    const-string v1, "{post_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceProductDetailsFromPostIDRoute"

    .line 2548537
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548538
    move-object v1, v1

    .line 2548539
    iput v5, v1, LX/98r;->h:I

    .line 2548540
    move-object v1, v1

    .line 2548541
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548542
    sget-object v0, LX/0ax;->hY:Ljava/lang/String;

    const-string v1, "{id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceProfileRoute"

    .line 2548543
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548544
    move-object v1, v1

    .line 2548545
    iput v5, v1, LX/98r;->h:I

    .line 2548546
    move-object v1, v1

    .line 2548547
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548548
    sget-object v0, LX/0ax;->ia:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceMessageRoute"

    .line 2548549
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548550
    move-object v1, v1

    .line 2548551
    iput v5, v1, LX/98r;->h:I

    .line 2548552
    move-object v1, v1

    .line 2548553
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548554
    sget-object v0, LX/0ax;->ih:Ljava/lang/String;

    const-string v1, "{threadID}"

    const-string v2, "{referralSurface}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceMessageRoute"

    .line 2548555
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548556
    move-object v1, v1

    .line 2548557
    iput v5, v1, LX/98r;->h:I

    .line 2548558
    move-object v1, v1

    .line 2548559
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548560
    sget-object v0, LX/0ax;->ie:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceSellerCentralItemDetailsRoute"

    .line 2548561
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548562
    move-object v1, v1

    .line 2548563
    iput v5, v1, LX/98r;->h:I

    .line 2548564
    move-object v1, v1

    .line 2548565
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548566
    sget-object v0, LX/0ax;->if:Ljava/lang/String;

    const-string v1, "{forSaleItemID}"

    const-string v2, "{productTitle}"

    const-string v3, "{referralSurface}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceSellerCentralItemDetailsRoute"

    .line 2548567
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548568
    move-object v1, v1

    .line 2548569
    iput v5, v1, LX/98r;->h:I

    .line 2548570
    move-object v1, v1

    .line 2548571
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548572
    sget-object v0, LX/0ax;->ic:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceProductMessageThreadsRoute"

    .line 2548573
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548574
    move-object v1, v1

    .line 2548575
    iput v5, v1, LX/98r;->h:I

    .line 2548576
    move-object v1, v1

    .line 2548577
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548578
    sget-object v0, LX/0ax;->id:Ljava/lang/String;

    const-string v1, "{productID}"

    const-string v2, "{productTitle}"

    const-string v3, "{referralSurface}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceProductMessageThreadsRoute"

    .line 2548579
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548580
    move-object v1, v1

    .line 2548581
    iput v5, v1, LX/98r;->h:I

    .line 2548582
    move-object v1, v1

    .line 2548583
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548584
    sget-object v0, LX/0ax;->it:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceNotificationsRoute"

    .line 2548585
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548586
    move-object v1, v1

    .line 2548587
    const v2, 0x7f083810

    .line 2548588
    iput v2, v1, LX/98r;->d:I

    .line 2548589
    move-object v1, v1

    .line 2548590
    iput v5, v1, LX/98r;->h:I

    .line 2548591
    move-object v1, v1

    .line 2548592
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548593
    sget-object v0, LX/0ax;->iu:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceNotificationSettingsRoute"

    .line 2548594
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548595
    move-object v1, v1

    .line 2548596
    const v2, 0x7f083810

    .line 2548597
    iput v2, v1, LX/98r;->d:I

    .line 2548598
    move-object v1, v1

    .line 2548599
    iput v5, v1, LX/98r;->h:I

    .line 2548600
    move-object v1, v1

    .line 2548601
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548602
    sget-object v0, LX/0ax;->ii:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceInitialMessageRoute"

    .line 2548603
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548604
    move-object v1, v1

    .line 2548605
    iput v5, v1, LX/98r;->h:I

    .line 2548606
    move-object v1, v1

    .line 2548607
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548608
    sget-object v0, LX/0ax;->ij:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceLocationRoute"

    .line 2548609
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548610
    move-object v1, v1

    .line 2548611
    iput v5, v1, LX/98r;->h:I

    .line 2548612
    move-object v1, v1

    .line 2548613
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548614
    sget-object v0, LX/0ax;->ik:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceSearch"

    .line 2548615
    iput-object v2, v1, LX/98r;->n:Ljava/lang/String;

    .line 2548616
    move-object v1, v1

    .line 2548617
    const-string v2, "MarketplaceSearchRoute"

    .line 2548618
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548619
    move-object v1, v1

    .line 2548620
    iput v5, v1, LX/98r;->h:I

    .line 2548621
    move-object v1, v1

    .line 2548622
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, LX/0cQ;->REACT_FRAGMENT_WITH_MARKETPLACE_SEARCH:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;I)V

    .line 2548623
    sget-object v0, LX/0ax;->il:Ljava/lang/String;

    const-string v1, "{module MarketplaceSearch}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "SearchTypeaheadResultsRoute"

    .line 2548624
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548625
    move-object v1, v1

    .line 2548626
    iput v5, v1, LX/98r;->h:I

    .line 2548627
    move-object v1, v1

    .line 2548628
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548629
    sget-object v0, LX/0ax;->im:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceSearch"

    .line 2548630
    iput-object v2, v1, LX/98r;->n:Ljava/lang/String;

    .line 2548631
    move-object v1, v1

    .line 2548632
    const-string v2, "MarketplaceCategorySearchRoute"

    .line 2548633
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548634
    move-object v1, v1

    .line 2548635
    iput v5, v1, LX/98r;->h:I

    .line 2548636
    move-object v1, v1

    .line 2548637
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, LX/0cQ;->REACT_FRAGMENT_WITH_MARKETPLACE_SEARCH:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;I)V

    .line 2548638
    sget-object v0, LX/0ax;->ip:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceSavedSearchResultsRoute"

    .line 2548639
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548640
    move-object v1, v1

    .line 2548641
    iput v5, v1, LX/98r;->h:I

    .line 2548642
    move-object v1, v1

    .line 2548643
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548644
    sget-object v0, LX/0ax;->ht:Ljava/lang/String;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "{formID}"

    aput-object v2, v1, v6

    const-string v2, "{adID}"

    aput-object v2, v1, v5

    const-string v2, "{app}"

    aput-object v2, v1, v7

    const-string v2, "{mediaID}"

    aput-object v2, v1, v8

    const-string v2, "{actorID}"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "{trackingToken}"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "LeadGenRoute"

    .line 2548645
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548646
    move-object v1, v1

    .line 2548647
    iput v5, v1, LX/98r;->h:I

    .line 2548648
    move-object v1, v1

    .line 2548649
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548650
    sget-object v0, LX/0ax;->io:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceSavedItemsRoute"

    .line 2548651
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548652
    move-object v1, v1

    .line 2548653
    const v2, 0x7f081a8e

    .line 2548654
    iput v2, v1, LX/98r;->d:I

    .line 2548655
    move-object v1, v1

    .line 2548656
    iput v5, v1, LX/98r;->h:I

    .line 2548657
    move-object v1, v1

    .line 2548658
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548659
    sget-object v0, LX/0ax;->iq:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceCategoryMenuRoute"

    .line 2548660
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548661
    move-object v1, v1

    .line 2548662
    const v2, 0x7f08380d

    .line 2548663
    iput v2, v1, LX/98r;->d:I

    .line 2548664
    move-object v1, v1

    .line 2548665
    iput v5, v1, LX/98r;->h:I

    .line 2548666
    move-object v1, v1

    .line 2548667
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548668
    sget-object v0, LX/0ax;->ir:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceYourItemsRoute"

    .line 2548669
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548670
    move-object v1, v1

    .line 2548671
    const v2, 0x7f08380e

    .line 2548672
    iput v2, v1, LX/98r;->d:I

    .line 2548673
    move-object v1, v1

    .line 2548674
    iput v5, v1, LX/98r;->h:I

    .line 2548675
    move-object v1, v1

    .line 2548676
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548677
    sget-object v0, LX/0ax;->is:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "MarketplaceSellerCentralInactiveItemsRoute"

    .line 2548678
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548679
    move-object v1, v1

    .line 2548680
    const v2, 0x7f083811

    .line 2548681
    iput v2, v1, LX/98r;->d:I

    .line 2548682
    move-object v1, v1

    .line 2548683
    iput v5, v1, LX/98r;->h:I

    .line 2548684
    move-object v1, v1

    .line 2548685
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548686
    sget-object v0, LX/0ax;->iJ:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "PIVHelpRoute"

    .line 2548687
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548688
    move-object v1, v1

    .line 2548689
    iput v5, v1, LX/98r;->h:I

    .line 2548690
    move-object v1, v1

    .line 2548691
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548692
    sget-object v0, LX/0ax;->iK:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const v2, 0x7f083814

    .line 2548693
    iput v2, v1, LX/98r;->d:I

    .line 2548694
    move-object v1, v1

    .line 2548695
    const-string v2, "SinboxListRoute"

    .line 2548696
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548697
    move-object v1, v1

    .line 2548698
    iput v5, v1, LX/98r;->h:I

    .line 2548699
    move-object v1, v1

    .line 2548700
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548701
    sget-object v0, LX/0ax;->iL:Ljava/lang/String;

    const-string v1, "{id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const v2, 0x7f083814

    .line 2548702
    iput v2, v1, LX/98r;->d:I

    .line 2548703
    move-object v1, v1

    .line 2548704
    const-string v2, "SinboxItemRoute"

    .line 2548705
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548706
    move-object v1, v1

    .line 2548707
    iput v5, v1, LX/98r;->h:I

    .line 2548708
    move-object v1, v1

    .line 2548709
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548710
    sget-object v0, LX/0ax;->iM:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "CompassionResourceRoute"

    .line 2548711
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548712
    move-object v1, v1

    .line 2548713
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->c(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548714
    sget-object v0, LX/0ax;->iN:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "OCFilterSettingsRoute"

    .line 2548715
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548716
    move-object v1, v1

    .line 2548717
    const v2, 0x7f083812

    .line 2548718
    iput v2, v1, LX/98r;->d:I

    .line 2548719
    move-object v1, v1

    .line 2548720
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548721
    sget-object v0, LX/0ax;->gd:Ljava/lang/String;

    const-string v1, "{group_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "CommerceInventoryRoute"

    .line 2548722
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548723
    move-object v1, v1

    .line 2548724
    const v2, 0x7f082f14

    .line 2548725
    iput v2, v1, LX/98r;->d:I

    .line 2548726
    move-object v1, v1

    .line 2548727
    iput v5, v1, LX/98r;->h:I

    .line 2548728
    move-object v1, v1

    .line 2548729
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548730
    sget-object v0, LX/0ax;->ge:Ljava/lang/String;

    const-string v1, "{story_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "CommerceInventoryCrossPostRoute"

    .line 2548731
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548732
    move-object v1, v1

    .line 2548733
    const v2, 0x7f082f15

    .line 2548734
    iput v2, v1, LX/98r;->d:I

    .line 2548735
    move-object v1, v1

    .line 2548736
    iput v5, v1, LX/98r;->h:I

    .line 2548737
    move-object v1, v1

    .line 2548738
    const v2, 0x7f082f16

    .line 2548739
    iput v2, v1, LX/98r;->k:I

    .line 2548740
    move-object v1, v1

    .line 2548741
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548742
    sget-object v0, LX/0ax;->gf:Ljava/lang/String;

    const-string v1, "{story_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "CommerceInventoryCommentsRoute"

    .line 2548743
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548744
    move-object v1, v1

    .line 2548745
    const v2, 0x7f082f17

    .line 2548746
    iput v2, v1, LX/98r;->d:I

    .line 2548747
    move-object v1, v1

    .line 2548748
    iput v5, v1, LX/98r;->h:I

    .line 2548749
    move-object v1, v1

    .line 2548750
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548751
    sget-object v0, LX/0ax;->gg:Ljava/lang/String;

    const-string v1, "{id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "CommerceSellerProfileRoute"

    .line 2548752
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548753
    move-object v1, v1

    .line 2548754
    iput v5, v1, LX/98r;->h:I

    .line 2548755
    move-object v1, v1

    .line 2548756
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548757
    sget-object v0, LX/0ax;->hq:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "InstantArticlesRoute"

    .line 2548758
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548759
    move-object v1, v1

    .line 2548760
    iput v5, v1, LX/98r;->h:I

    .line 2548761
    move-object v1, v1

    .line 2548762
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548763
    sget-object v0, LX/0ax;->iY:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ConstituentNUXRoute"

    .line 2548764
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548765
    move-object v1, v1

    .line 2548766
    iput v5, v1, LX/98r;->h:I

    .line 2548767
    move-object v1, v1

    .line 2548768
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548769
    sget-object v0, LX/0ax;->s:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "BoostAnotherPostPickerRoute"

    .line 2548770
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548771
    move-object v1, v1

    .line 2548772
    const v2, 0x7f080bb5

    .line 2548773
    iput v2, v1, LX/98r;->d:I

    .line 2548774
    move-object v1, v1

    .line 2548775
    iput v5, v1, LX/98r;->h:I

    .line 2548776
    move-object v1, v1

    .line 2548777
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548778
    sget-object v0, LX/0ax;->em:Ljava/lang/String;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "QuicksilverHomeRoute"

    .line 2548779
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548780
    move-object v1, v1

    .line 2548781
    const v2, 0x7f08237d

    .line 2548782
    iput v2, v1, LX/98r;->d:I

    .line 2548783
    move-object v1, v1

    .line 2548784
    iput v5, v1, LX/98r;->h:I

    .line 2548785
    move-object v1, v1

    .line 2548786
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2548787
    return-void

    .line 2548788
    :cond_0
    sget-object v0, LX/0ax;->fW:Ljava/lang/String;

    const-string v1, "{refID 0}"

    const-string v2, "{refType unknown}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    const-string v2, "ShopsFeedRoute"

    .line 2548789
    iput-object v2, v1, LX/98r;->b:Ljava/lang/String;

    .line 2548790
    move-object v1, v1

    .line 2548791
    iput v5, v1, LX/98r;->h:I

    .line 2548792
    move-object v1, v1

    .line 2548793
    const v2, 0x7f0814c7

    .line 2548794
    iput v2, v1, LX/98r;->d:I

    .line 2548795
    move-object v1, v1

    .line 2548796
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/IC1;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/IC1;
    .locals 5

    .prologue
    .line 2548066
    sget-object v0, LX/IC1;->c:LX/IC1;

    if-nez v0, :cond_1

    .line 2548067
    const-class v1, LX/IC1;

    monitor-enter v1

    .line 2548068
    :try_start_0
    sget-object v0, LX/IC1;->c:LX/IC1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2548069
    if-eqz v2, :cond_0

    .line 2548070
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2548071
    new-instance v4, LX/IC1;

    const/16 v3, 0x148e

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, p0, v3}, LX/IC1;-><init>(LX/0Or;LX/0Uh;)V

    .line 2548072
    move-object v0, v4

    .line 2548073
    sput-object v0, LX/IC1;->c:LX/IC1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2548074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2548075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2548076
    :cond_1
    sget-object v0, LX/IC1;->c:LX/IC1;

    return-object v0

    .line 2548077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2548078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2548079
    const-class v0, Lcom/facebook/base/activity/ReactFragmentActivity;

    sget-object v1, LX/0cQ;->FB_REACT_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2548080
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;I)V
    .locals 1

    .prologue
    .line 2548081
    const-class v0, Lcom/facebook/base/activity/ReactFragmentActivity;

    invoke-virtual {p0, p1, v0, p3, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2548082
    return-void
.end method

.method private b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2548083
    const-class v0, Lcom/facebook/fbreact/fragment/ReactActivity;

    invoke-virtual {p0, p1, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2548084
    return-void
.end method

.method private c(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2548085
    const-class v0, Lcom/facebook/fbreact/fragment/TransparentReactActivity;

    invoke-virtual {p0, p1, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2548086
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2548087
    invoke-super {p0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2548088
    if-eqz v0, :cond_0

    sget-object v1, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2548089
    const-string v1, "uri"

    sget-object v2, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2548090
    :cond_0
    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2548091
    iget-object v0, p0, LX/IC1;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
