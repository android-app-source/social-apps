.class public final LX/JA1;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2653777
    const-class v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    const v0, -0x318cb55a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "AboutSectionsQuery"

    const-string v6, "42d31bd352b1974fa076cf8b729df5f4"

    const-string v7, "user"

    const-string v8, "10155069962891729"

    const-string v9, "10155259086426729"

    const-string v0, "profile_field_sections_end_cursor"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2653778
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2653779
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2653780
    sparse-switch v0, :sswitch_data_0

    .line 2653781
    :goto_0
    return-object p1

    .line 2653782
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2653783
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2653784
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2653785
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2653786
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2653787
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x41b8e48f -> :sswitch_3
        -0x41a91745 -> :sswitch_4
        -0xc5365bd -> :sswitch_1
        -0x1a334ba -> :sswitch_2
        0x24991595 -> :sswitch_0
        0x291d8de0 -> :sswitch_5
    .end sparse-switch
.end method
