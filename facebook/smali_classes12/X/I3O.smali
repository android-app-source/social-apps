.class public final LX/I3O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2531310
    iput-object p1, p0, LX/I3O;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2531311
    iget-object v0, p0, LX/I3O;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->i:LX/I3J;

    invoke-virtual {v0, p3}, LX/I3J;->a(I)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2531312
    if-eqz v0, :cond_0

    .line 2531313
    iget-object v1, p0, LX/I3O;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->b:LX/1nQ;

    .line 2531314
    iget-object v2, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2531315
    iget-object v2, p0, LX/I3O;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    invoke-virtual {v2}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/I3O;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531316
    iget-object p0, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v3, p0

    .line 2531317
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    invoke-virtual {v1, v0, v2, p3, v3}, LX/1nQ;->b(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2531318
    :cond_0
    return-void
.end method
