.class public LX/I49;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B5a;


# instance fields
.field public final a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;)V
    .locals 0

    .prologue
    .line 2533215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2533216
    iput-object p1, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    .line 2533217
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533218
    const/4 v0, 0x0

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533219
    const/4 v0, 0x0

    return-object v0
.end method

.method public final C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533220
    const/4 v0, 0x0

    return-object v0
.end method

.method public final D()LX/8Yr;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533221
    const/4 v0, 0x0

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533222
    iget-object v0, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->k()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v0

    return-object v0
.end method

.method public final F()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$RelatedArticleObjsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533223
    const/4 v0, 0x0

    return-object v0
.end method

.method public final G()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533224
    const/4 v0, 0x0

    return-object v0
.end method

.method public final H()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533225
    const/4 v0, 0x0

    return-object v0
.end method

.method public final I()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533234
    const/4 v0, 0x0

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533226
    const/4 v0, 0x0

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533227
    const/4 v0, 0x0

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533228
    const/4 v0, 0x0

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533229
    const/4 v0, 0x0

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533230
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533194
    iget-object v0, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533231
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533232
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533233
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533213
    const/4 v0, 0x0

    return-object v0
.end method

.method public final em_()LX/8Yr;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533214
    iget-object v0, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->em_()LX/8Yr;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/7oa;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533193
    iget-object v0, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->d()LX/7oa;

    move-result-object v0

    return-object v0
.end method

.method public final iU_()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533195
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()LX/8Ys;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533196
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533197
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533198
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2533199
    const/4 v0, 0x0

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2533200
    const/4 v0, 0x0

    return v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533201
    iget-object v0, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->c()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v0

    return-object v0
.end method

.method public final p()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533202
    const/4 v0, 0x0

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2533203
    const/4 v0, 0x0

    return v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533204
    iget-object v0, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533205
    iget-object v0, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->mE_()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533206
    const/4 v0, 0x0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533207
    iget-object v0, p0, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemsModel$ListElementsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533208
    const/4 v0, 0x0

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533209
    const/4 v0, 0x0

    return-object v0
.end method

.method public final x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533210
    const/4 v0, 0x0

    return-object v0
.end method

.method public final y()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2533211
    const/4 v0, 0x0

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533212
    const/4 v0, 0x0

    return-object v0
.end method
