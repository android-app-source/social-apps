.class public final LX/HSz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field private final a:LX/FQY;


# direct methods
.method public constructor <init>(LX/FQY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2468208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2468209
    iput-object p1, p0, LX/HSz;->a:LX/FQY;

    .line 2468210
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2468211
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2468212
    iget-object v2, p0, LX/HSz;->a:LX/FQY;

    invoke-virtual {v2, v0, v1}, LX/FQY;->a(J)Landroid/content/Intent;

    move-result-object v0

    .line 2468213
    if-eqz v0, :cond_0

    .line 2468214
    const-string v1, "popup_state"

    sget-object v2, LX/8A0;->MESSAGES:LX/8A0;

    invoke-virtual {v2}, LX/8A0;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2468215
    const-string v1, "CommsHubConstants_extra_tab_name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2468216
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2468217
    const-string v2, "CommsHubConstants_extra_tab_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2468218
    :cond_0
    return-object v0
.end method
