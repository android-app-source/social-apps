.class public final LX/IP3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;)V
    .locals 0

    .prologue
    .line 2573669
    iput-object p1, p0, LX/IP3;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0xd340029

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2573670
    check-cast p1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    .line 2573671
    iget-object v1, p1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    move-object v1, v1

    .line 2573672
    iget-object v2, p0, LX/IP3;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    .line 2573673
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 2573674
    :cond_0
    :goto_0
    const v1, -0x36990856

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2573675
    :cond_1
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    iget-object v4, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->e:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    invoke-virtual {p0, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    .line 2573676
    const-string p0, "group_feed_id"

    invoke-virtual {v1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2573677
    const-string p0, "target_fragment"

    sget-object p1, LX/0cQ;->GROUPS_MALL_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v4, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2573678
    iget-object p0, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v2}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {p0, v4, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
