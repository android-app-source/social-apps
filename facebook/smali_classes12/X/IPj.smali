.class public LX/IPj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IPr;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/IPc;

.field public final d:LX/0if;

.field private final e:LX/0ad;

.field public f:LX/1De;

.field public g:LX/BcP;

.field public h:Landroid/view/ViewStub;

.field public i:Lcom/facebook/components/ComponentView;

.field public j:Z

.field public k:I

.field private l:Landroid/animation/ValueAnimator;

.field private m:Landroid/animation/ValueAnimator;

.field private n:LX/1rs;

.field public o:Z

.field public p:LX/1dV;

.field public q:LX/Bdb;

.field public r:LX/1X1;

.field public s:LX/1X1;

.field public t:LX/IPe;

.field public u:LX/5K7;

.field public v:I

.field private w:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/IPc;LX/0if;LX/0ad;Landroid/view/ViewStub;I)V
    .locals 1
    .param p6    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/IPr;",
            ">;",
            "LX/IPc;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/0ad;",
            "Landroid/view/ViewStub;",
            "I)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2574528
    const/4 v0, -0x1

    iput v0, p0, LX/IPj;->v:I

    .line 2574529
    iput-object p6, p0, LX/IPj;->h:Landroid/view/ViewStub;

    .line 2574530
    iput-object p1, p0, LX/IPj;->a:Landroid/content/Context;

    .line 2574531
    iput-object p2, p0, LX/IPj;->b:LX/0Ot;

    .line 2574532
    iput-object p3, p0, LX/IPj;->c:LX/IPc;

    .line 2574533
    iput-object p4, p0, LX/IPj;->d:LX/0if;

    .line 2574534
    iput-object p5, p0, LX/IPj;->e:LX/0ad;

    .line 2574535
    iput p7, p0, LX/IPj;->w:I

    .line 2574536
    iget-object v0, p0, LX/IPj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b240d

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/IPj;->k:I

    .line 2574537
    new-instance v0, LX/1De;

    iget-object p1, p0, LX/IPj;->a:Landroid/content/Context;

    invoke-direct {v0, p1}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/IPj;->f:LX/1De;

    .line 2574538
    new-instance v0, LX/BcP;

    iget-object p1, p0, LX/IPj;->a:Landroid/content/Context;

    invoke-direct {v0, p1}, LX/BcP;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/IPj;->g:LX/BcP;

    .line 2574539
    iget-object v0, p0, LX/IPj;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, LX/IPj;->i:Lcom/facebook/components/ComponentView;

    .line 2574540
    new-instance v0, LX/IPe;

    invoke-direct {v0, p0}, LX/IPe;-><init>(LX/IPj;)V

    iput-object v0, p0, LX/IPj;->t:LX/IPe;

    .line 2574541
    const/4 p1, 0x0

    .line 2574542
    new-instance v0, LX/Bdg;

    invoke-direct {v0, p1, p1}, LX/Bdg;-><init>(IZ)V

    move-object v0, v0

    .line 2574543
    iput-object v0, p0, LX/IPj;->q:LX/Bdb;

    .line 2574544
    iget-object v0, p0, LX/IPj;->f:LX/1De;

    invoke-static {v0}, LX/BdR;->c(LX/1De;)LX/BdP;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    iput-object v0, p0, LX/IPj;->r:LX/1X1;

    .line 2574545
    iget-object v0, p0, LX/IPj;->f:LX/1De;

    invoke-static {v0}, LX/BdR;->c(LX/1De;)LX/BdP;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    iput-object v0, p0, LX/IPj;->s:LX/1X1;

    .line 2574546
    new-instance v0, LX/5K7;

    invoke-direct {v0}, LX/5K7;-><init>()V

    iput-object v0, p0, LX/IPj;->u:LX/5K7;

    .line 2574547
    iget-object v0, p0, LX/IPj;->f:LX/1De;

    iget-object p1, p0, LX/IPj;->c:LX/IPc;

    iget-object p2, p0, LX/IPj;->f:LX/1De;

    invoke-virtual {p1, p2}, LX/IPc;->c(LX/1De;)LX/IPa;

    move-result-object p1

    iget-object p2, p0, LX/IPj;->u:LX/5K7;

    invoke-virtual {p1, p2}, LX/IPa;->a(LX/5K7;)LX/IPa;

    move-result-object p1

    iget-object p2, p0, LX/IPj;->g:LX/BcP;

    invoke-static {p0, p2}, LX/IPj;->a(LX/IPj;LX/BcP;)LX/BcO;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/IPa;->a(LX/BcO;)LX/IPa;

    move-result-object p1

    iget-object p2, p0, LX/IPj;->q:LX/Bdb;

    invoke-virtual {p1, p2}, LX/IPa;->a(LX/Bdb;)LX/IPa;

    move-result-object p1

    iget-object p2, p0, LX/IPj;->t:LX/IPe;

    invoke-virtual {p1, p2}, LX/IPa;->a(LX/IPe;)LX/IPa;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, LX/IPa;->a(Z)LX/IPa;

    move-result-object p1

    iget-object p2, p0, LX/IPj;->s:LX/1X1;

    invoke-virtual {p1, p2}, LX/IPa;->a(LX/1X1;)LX/IPa;

    move-result-object p1

    iget-object p2, p0, LX/IPj;->r:LX/1X1;

    invoke-virtual {p1, p2}, LX/IPa;->b(LX/1X1;)LX/IPa;

    move-result-object p1

    invoke-static {v0, p1}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    iput-object v0, p0, LX/IPj;->p:LX/1dV;

    .line 2574548
    iget-object v0, p0, LX/IPj;->i:Lcom/facebook/components/ComponentView;

    iget-object p1, p0, LX/IPj;->p:LX/1dV;

    invoke-virtual {v0, p1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2574549
    iget-object v0, p0, LX/IPj;->i:Lcom/facebook/components/ComponentView;

    iget p1, p0, LX/IPj;->k:I

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Lcom/facebook/components/ComponentView;->setTranslationY(F)V

    .line 2574550
    iget-object v0, p0, LX/IPj;->d:LX/0if;

    sget-object p1, LX/0ig;->aL:LX/0ih;

    invoke-virtual {v0, p1}, LX/0if;->a(LX/0ih;)V

    .line 2574551
    return-void
.end method

.method public static a(LX/IPj;LX/BcP;)LX/BcO;
    .locals 3

    .prologue
    .line 2574502
    iget-object v0, p0, LX/IPj;->n:LX/1rs;

    if-nez v0, :cond_0

    .line 2574503
    new-instance v0, LX/IPi;

    invoke-direct {v0, p0}, LX/IPi;-><init>(LX/IPj;)V

    iput-object v0, p0, LX/IPj;->n:LX/1rs;

    .line 2574504
    :cond_0
    iget-object v0, p0, LX/IPj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IPr;

    .line 2574505
    new-instance v1, LX/IPp;

    invoke-direct {v1, v0}, LX/IPp;-><init>(LX/IPr;)V

    .line 2574506
    sget-object v2, LX/IPr;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IPo;

    .line 2574507
    if-nez v2, :cond_1

    .line 2574508
    new-instance v2, LX/IPo;

    invoke-direct {v2}, LX/IPo;-><init>()V

    .line 2574509
    :cond_1
    iput-object v1, v2, LX/BcN;->a:LX/BcO;

    .line 2574510
    iput-object v1, v2, LX/IPo;->a:LX/IPp;

    .line 2574511
    iget-object v0, v2, LX/IPo;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2574512
    move-object v1, v2

    .line 2574513
    move-object v0, v1

    .line 2574514
    iget-object v1, p0, LX/IPj;->n:LX/1rs;

    .line 2574515
    iget-object v2, v0, LX/IPo;->a:LX/IPp;

    iput-object v1, v2, LX/IPp;->c:LX/1rs;

    .line 2574516
    iget-object v2, v0, LX/IPo;->d:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Ljava/util/BitSet;->set(I)V

    .line 2574517
    move-object v0, v0

    .line 2574518
    const/16 v1, 0xa

    .line 2574519
    iget-object v2, v0, LX/IPo;->a:LX/IPp;

    iput v1, v2, LX/IPp;->d:I

    .line 2574520
    iget-object v2, v0, LX/IPo;->d:Ljava/util/BitSet;

    const/4 p1, 0x1

    invoke-virtual {v2, p1}, Ljava/util/BitSet;->set(I)V

    .line 2574521
    move-object v0, v0

    .line 2574522
    invoke-static {p0}, LX/IPj;->f(LX/IPj;)Z

    move-result v1

    .line 2574523
    iget-object v2, v0, LX/IPo;->a:LX/IPp;

    iput-boolean v1, v2, LX/IPp;->e:Z

    .line 2574524
    iget-object v2, v0, LX/IPo;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2574525
    move-object v0, v0

    .line 2574526
    invoke-virtual {v0}, LX/IPo;->b()LX/BcO;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/IPj;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2574474
    iget-object v0, p0, LX/IPj;->p:LX/1dV;

    if-nez v0, :cond_1

    .line 2574475
    :cond_0
    :goto_0
    return-void

    .line 2574476
    :cond_1
    iget-object v0, p0, LX/IPj;->p:LX/1dV;

    iget-object v1, p0, LX/IPj;->c:LX/IPc;

    iget-object v2, p0, LX/IPj;->f:LX/1De;

    invoke-virtual {v1, v2}, LX/IPc;->c(LX/1De;)LX/IPa;

    move-result-object v1

    iget-object v2, p0, LX/IPj;->u:LX/5K7;

    invoke-virtual {v1, v2}, LX/IPa;->a(LX/5K7;)LX/IPa;

    move-result-object v1

    iget-object v2, p0, LX/IPj;->g:LX/BcP;

    invoke-static {p0, v2}, LX/IPj;->a(LX/IPj;LX/BcP;)LX/BcO;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/IPa;->a(LX/BcO;)LX/IPa;

    move-result-object v1

    iget-object v2, p0, LX/IPj;->q:LX/Bdb;

    invoke-virtual {v1, v2}, LX/IPa;->a(LX/Bdb;)LX/IPa;

    move-result-object v1

    iget-object v2, p0, LX/IPj;->t:LX/IPe;

    invoke-virtual {v1, v2}, LX/IPa;->a(LX/IPe;)LX/IPa;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/IPa;->a(Z)LX/IPa;

    move-result-object v1

    iget-object v2, p0, LX/IPj;->s:LX/1X1;

    invoke-virtual {v1, v2}, LX/IPa;->a(LX/1X1;)LX/IPa;

    move-result-object v1

    iget-object v2, p0, LX/IPj;->r:LX/1X1;

    invoke-virtual {v1, v2}, LX/IPa;->b(LX/1X1;)LX/IPa;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1dV;->a(LX/1X1;)V

    .line 2574477
    if-eqz p1, :cond_0

    iget v0, p0, LX/IPj;->w:I

    if-lez v0, :cond_0

    .line 2574478
    iget-object v0, p0, LX/IPj;->u:LX/5K7;

    iget v1, p0, LX/IPj;->w:I

    invoke-virtual {v0, v1, v3}, LX/5K7;->a(IZ)V

    .line 2574479
    iput v3, p0, LX/IPj;->w:I

    goto :goto_0
.end method

.method public static c$redex0(LX/IPj;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 2574493
    iget-object v0, p0, LX/IPj;->l:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 2574494
    const/4 v0, 0x2

    new-array v0, v0, [I

    iget v1, p0, LX/IPj;->k:I

    aput v1, v0, v2

    aput v2, v0, v4

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/IPj;->l:Landroid/animation/ValueAnimator;

    .line 2574495
    iget-object v0, p0, LX/IPj;->l:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2574496
    iget-object v0, p0, LX/IPj;->l:Landroid/animation/ValueAnimator;

    new-instance v1, LX/IPf;

    invoke-direct {v1, p0}, LX/IPf;-><init>(LX/IPj;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2574497
    :cond_0
    iget-object v0, p0, LX/IPj;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2574498
    iput-boolean v4, p0, LX/IPj;->j:Z

    .line 2574499
    invoke-static {p0, v4}, LX/IPj;->a(LX/IPj;Z)V

    .line 2574500
    iget-object v0, p0, LX/IPj;->d:LX/0if;

    sget-object v1, LX/0ig;->aL:LX/0ih;

    const-string v2, "reveal_drawer"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2574501
    return-void
.end method

.method public static d(LX/IPj;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2574552
    iget-object v0, p0, LX/IPj;->m:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 2574553
    const/4 v0, 0x2

    new-array v0, v0, [I

    aput v4, v0, v4

    const/4 v1, 0x1

    iget v2, p0, LX/IPj;->k:I

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/IPj;->m:Landroid/animation/ValueAnimator;

    .line 2574554
    iget-object v0, p0, LX/IPj;->m:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2574555
    iget-object v0, p0, LX/IPj;->m:Landroid/animation/ValueAnimator;

    new-instance v1, LX/IPg;

    invoke-direct {v1, p0}, LX/IPg;-><init>(LX/IPj;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2574556
    iget-object v0, p0, LX/IPj;->m:Landroid/animation/ValueAnimator;

    new-instance v1, LX/IPh;

    invoke-direct {v1, p0}, LX/IPh;-><init>(LX/IPj;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2574557
    :cond_0
    iget-object v0, p0, LX/IPj;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2574558
    iput-boolean v4, p0, LX/IPj;->j:Z

    .line 2574559
    iget-object v0, p0, LX/IPj;->d:LX/0if;

    sget-object v1, LX/0ig;->aL:LX/0ih;

    const-string v2, "hide_drawer"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2574560
    return-void
.end method

.method public static f(LX/IPj;)Z
    .locals 3

    .prologue
    .line 2574492
    iget-object v0, p0, LX/IPj;->e:LX/0ad;

    sget-short v1, LX/9Lj;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2574487
    iget-object v0, p0, LX/IPj;->i:Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_0

    .line 2574488
    iget-object v0, p0, LX/IPj;->i:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->l()V

    .line 2574489
    const/4 v0, 0x0

    iput-object v0, p0, LX/IPj;->i:Lcom/facebook/components/ComponentView;

    .line 2574490
    :cond_0
    iget-object v0, p0, LX/IPj;->d:LX/0if;

    sget-object v1, LX/0ig;->aL:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2574491
    return-void
.end method

.method public final a(III)V
    .locals 2

    .prologue
    .line 2574480
    iget v0, p0, LX/IPj;->v:I

    if-gez v0, :cond_1

    .line 2574481
    :cond_0
    :goto_0
    return-void

    .line 2574482
    :cond_1
    iget-boolean v0, p0, LX/IPj;->o:Z

    if-nez v0, :cond_0

    .line 2574483
    add-int v0, p1, p2

    iget v1, p0, LX/IPj;->v:I

    add-int/2addr v1, p3

    if-lt v0, v1, :cond_2

    iget-boolean v0, p0, LX/IPj;->j:Z

    if-nez v0, :cond_2

    .line 2574484
    invoke-static {p0}, LX/IPj;->c$redex0(LX/IPj;)V

    goto :goto_0

    .line 2574485
    :cond_2
    add-int v0, p1, p2

    iget v1, p0, LX/IPj;->v:I

    add-int/2addr v1, p3

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, LX/IPj;->j:Z

    if-eqz v0, :cond_0

    .line 2574486
    invoke-static {p0}, LX/IPj;->d(LX/IPj;)V

    goto :goto_0
.end method
