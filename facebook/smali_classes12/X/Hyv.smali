.class public final LX/Hyv;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HyY;

.field public final synthetic b:LX/Hyx;


# direct methods
.method public constructor <init>(LX/Hyx;LX/HyY;)V
    .locals 0

    .prologue
    .line 2525000
    iput-object p1, p0, LX/Hyv;->b:LX/Hyx;

    iput-object p2, p0, LX/Hyv;->a:LX/HyY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2525001
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2525002
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525003
    if-nez p1, :cond_1

    .line 2525004
    :cond_0
    :goto_0
    return-void

    .line 2525005
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525006
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel;

    .line 2525007
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2525008
    iget-object v1, p0, LX/Hyv;->a:LX/HyY;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel;->a()LX/0Px;

    move-result-object v0

    .line 2525009
    iget-object v2, v1, LX/HyY;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    iget-object p0, v1, LX/HyY;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object p0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    .line 2525010
    iget-object p1, v2, LX/Hz2;->g:LX/I2o;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2525011
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2525012
    :cond_2
    :goto_1
    goto :goto_0

    .line 2525013
    :cond_3
    iget-object v1, v2, LX/Hz2;->j:LX/I2Z;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;

    .line 2525014
    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    .line 2525015
    iget-object v0, v1, LX/I2Z;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2525016
    invoke-static {v1}, LX/I2Z;->b(LX/I2Z;)V

    .line 2525017
    :cond_4
    invoke-static {v2}, LX/Hz2;->k(LX/Hz2;)V

    goto :goto_1
.end method
