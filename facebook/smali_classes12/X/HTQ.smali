.class public final LX/HTQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/HTS;


# direct methods
.method public constructor <init>(LX/HTS;)V
    .locals 0

    .prologue
    .line 2469497
    iput-object p1, p0, LX/HTQ;->a:LX/HTS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 2469498
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2469499
    iget-object v0, p0, LX/HTQ;->a:LX/HTS;

    iget-object v0, v0, LX/HTS;->a:LX/HTX;

    iget-object v0, v0, LX/HTX;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2469500
    iget-object v0, p0, LX/HTQ;->a:LX/HTS;

    iget-object v0, v0, LX/HTS;->a:LX/HTX;

    iget-object v0, v0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->g:Ljava/util/ArrayList;

    iget-object v1, p0, LX/HTQ;->a:LX/HTS;

    iget-object v1, v1, LX/HTS;->a:LX/HTX;

    iget v1, v1, LX/HTX;->m:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2469501
    iget-object v0, p0, LX/HTQ;->a:LX/HTS;

    iget-object v0, v0, LX/HTS;->a:LX/HTX;

    iget-object v0, v0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v1, p0, LX/HTQ;->a:LX/HTS;

    iget-object v1, v1, LX/HTS;->a:LX/HTX;

    iget v1, v1, LX/HTX;->m:I

    invoke-virtual {v0, v1}, LX/1OM;->d(I)V

    .line 2469502
    iget-object v0, p0, LX/HTQ;->a:LX/HTS;

    iget-object v0, v0, LX/HTS;->a:LX/HTX;

    iget-object v0, v0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v1, p0, LX/HTQ;->a:LX/HTS;

    iget-object v1, v1, LX/HTS;->a:LX/HTX;

    iget v1, v1, LX/HTX;->m:I

    iget-object v2, p0, LX/HTQ;->a:LX/HTS;

    iget-object v2, v2, LX/HTS;->a:LX/HTX;

    iget-object v2, v2, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1OM;->a(II)V

    .line 2469503
    iget-object v0, p0, LX/HTQ;->a:LX/HTS;

    iget-object v0, v0, LX/HTS;->a:LX/HTX;

    .line 2469504
    new-instance v1, LX/HTU;

    invoke-direct {v1, v0}, LX/HTU;-><init>(LX/HTX;)V

    .line 2469505
    new-instance v2, LX/HTW;

    invoke-direct {v2, v0}, LX/HTW;-><init>(LX/HTX;)V

    .line 2469506
    iget-object v3, v0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->d:LX/1Ck;

    const-string v4, "removePoliticalEndorsement"

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object p2, v0, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object p2, p2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->i:Ljava/lang/String;

    aput-object p2, p0, p1

    invoke-static {v4, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2469507
    return-void
.end method
