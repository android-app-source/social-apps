.class public final LX/Hqf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 0

    .prologue
    .line 2510691
    iput-object p1, p0, LX/Hqf;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 2510684
    iget-object v0, p0, LX/Hqf;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_TEXT_READY:LX/0ge;

    iget-object v0, p0, LX/Hqf;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2510685
    iget-object v0, p0, LX/Hqf;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2510686
    iget-object v1, v0, LX/1Kj;->d:LX/11i;

    sget-object v2, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2510687
    if-eqz v1, :cond_0

    .line 2510688
    const-string v2, "ComposerDrawPhase"

    const v3, 0xc38530f

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2510689
    :cond_0
    iget-object v0, p0, LX/Hqf;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2510690
    return-void
.end method
