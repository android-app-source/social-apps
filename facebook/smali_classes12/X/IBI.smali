.class public LX/IBI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/I6a;
.implements LX/IB8;


# instance fields
.field private final a:LX/1Pz;

.field private final b:LX/1Q0;

.field private final c:LX/1QL;

.field private final d:LX/IBH;

.field private final e:LX/1Q2;

.field private final f:LX/1Q3;

.field private final g:LX/I6T;

.field private final h:LX/1Q4;

.field private final i:LX/1QM;

.field private final j:LX/1QP;

.field private final k:LX/1Q7;

.field private final l:LX/I6e;

.field private final m:LX/1QQ;

.field private final n:LX/1QB;

.field private final o:LX/1QR;

.field private final p:LX/1QD;

.field private final q:LX/1QF;

.field private final r:LX/1QG;

.field private final s:LX/99Q;

.field private final t:LX/1PU;

.field private final u:LX/1QK;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;LX/1Pz;LX/1Q0;LX/1Q1;LX/IBH;LX/1Q2;LX/1Q3;LX/I6U;LX/1Q4;LX/1Q5;LX/1Q6;LX/1Q7;LX/I6f;LX/1QA;LX/1QB;LX/1QC;LX/1QD;LX/1QF;LX/1QG;LX/99Q;LX/1QI;LX/1QK;)V
    .locals 2
    .param p1    # Lcom/facebook/events/permalink/EventPermalinkFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2546833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2546834
    iput-object p7, p0, LX/IBI;->a:LX/1Pz;

    .line 2546835
    iput-object p8, p0, LX/IBI;->b:LX/1Q0;

    .line 2546836
    invoke-virtual {p9, p0}, LX/1Q1;->a(LX/1Po;)LX/1QL;

    move-result-object v1

    iput-object v1, p0, LX/IBI;->c:LX/1QL;

    .line 2546837
    iput-object p10, p0, LX/IBI;->d:LX/IBH;

    .line 2546838
    iput-object p11, p0, LX/IBI;->e:LX/1Q2;

    .line 2546839
    iput-object p12, p0, LX/IBI;->f:LX/1Q3;

    .line 2546840
    invoke-virtual {p13, p1}, LX/I6U;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;)LX/I6T;

    move-result-object v1

    iput-object v1, p0, LX/IBI;->g:LX/I6T;

    .line 2546841
    move-object/from16 v0, p14

    iput-object v0, p0, LX/IBI;->h:LX/1Q4;

    .line 2546842
    move-object/from16 v0, p15

    invoke-virtual {v0, p2}, LX/1Q5;->a(Ljava/lang/String;)LX/1QM;

    move-result-object v1

    iput-object v1, p0, LX/IBI;->i:LX/1QM;

    .line 2546843
    invoke-static {p3}, LX/1Q6;->a(Landroid/content/Context;)LX/1QP;

    move-result-object v1

    iput-object v1, p0, LX/IBI;->j:LX/1QP;

    .line 2546844
    move-object/from16 v0, p17

    iput-object v0, p0, LX/IBI;->k:LX/1Q7;

    .line 2546845
    move-object/from16 v0, p18

    invoke-virtual {v0, p0, p0}, LX/I6f;->a(LX/5Oi;LX/1Pf;)LX/I6e;

    move-result-object v1

    iput-object v1, p0, LX/IBI;->l:LX/I6e;

    .line 2546846
    invoke-static {p4}, LX/1QA;->a(LX/1PT;)LX/1QQ;

    move-result-object v1

    iput-object v1, p0, LX/IBI;->m:LX/1QQ;

    .line 2546847
    move-object/from16 v0, p20

    iput-object v0, p0, LX/IBI;->n:LX/1QB;

    .line 2546848
    invoke-static {p5}, LX/1QC;->a(Ljava/lang/Runnable;)LX/1QR;

    move-result-object v1

    iput-object v1, p0, LX/IBI;->o:LX/1QR;

    .line 2546849
    move-object/from16 v0, p22

    iput-object v0, p0, LX/IBI;->p:LX/1QD;

    .line 2546850
    move-object/from16 v0, p23

    iput-object v0, p0, LX/IBI;->q:LX/1QF;

    .line 2546851
    move-object/from16 v0, p24

    iput-object v0, p0, LX/IBI;->r:LX/1QG;

    .line 2546852
    move-object/from16 v0, p25

    iput-object v0, p0, LX/IBI;->s:LX/99Q;

    .line 2546853
    move-object/from16 v0, p26

    invoke-virtual {v0, p6}, LX/1QI;->a(LX/1PY;)LX/1PU;

    move-result-object v1

    iput-object v1, p0, LX/IBI;->t:LX/1PU;

    .line 2546854
    move-object/from16 v0, p27

    iput-object v0, p0, LX/IBI;->u:LX/1QK;

    .line 2546855
    return-void
.end method


# virtual methods
.method public final a()LX/1Q9;
    .locals 1

    .prologue
    .line 2546856
    iget-object v0, p0, LX/IBI;->k:LX/1Q7;

    invoke-virtual {v0}, LX/1Q7;->a()LX/1Q9;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;
    .locals 6

    .prologue
    .line 2546857
    iget-object v0, p0, LX/IBI;->b:LX/1Q0;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Q0;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 2546858
    iget-object v0, p0, LX/IBI;->q:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2546859
    iget-object v0, p0, LX/IBI;->q:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1R6;)V
    .locals 1

    .prologue
    .line 2546860
    iget-object v0, p0, LX/IBI;->o:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a(LX/1R6;)V

    .line 2546861
    return-void
.end method

.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 2546862
    iget-object v0, p0, LX/IBI;->s:LX/99Q;

    invoke-virtual {v0, p1}, LX/99Q;->a(LX/1Rb;)V

    .line 2546863
    return-void
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2546864
    iget-object v0, p0, LX/IBI;->n:LX/1QB;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1QB;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2546865
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2546866
    iget-object v0, p0, LX/IBI;->s:LX/99Q;

    invoke-virtual {v0, p1, p2}, LX/99Q;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2546867
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2546868
    iget-object v0, p0, LX/IBI;->s:LX/99Q;

    invoke-virtual {v0, p1, p2, p3}, LX/99Q;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2546869
    return-void
.end method

.method public final a(LX/22C;)V
    .locals 1

    .prologue
    .line 2546870
    iget-object v0, p0, LX/IBI;->h:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->a(LX/22C;)V

    .line 2546871
    return-void
.end method

.method public final a(LX/34p;)V
    .locals 1

    .prologue
    .line 2546872
    iget-object v0, p0, LX/IBI;->h:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->a(LX/34p;)V

    .line 2546873
    return-void
.end method

.method public final a(LX/5Oj;)V
    .locals 1

    .prologue
    .line 2546874
    iget-object v0, p0, LX/IBI;->t:LX/1PU;

    invoke-virtual {v0, p1}, LX/1PU;->a(LX/5Oj;)V

    .line 2546875
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/events/model/Event;)V
    .locals 1

    .prologue
    .line 2546878
    iget-object v0, p0, LX/IBI;->d:LX/IBH;

    invoke-virtual {v0, p1, p2}, LX/IBH;->a(Landroid/content/Context;Lcom/facebook/events/model/Event;)V

    .line 2546879
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2546876
    iget-object v0, p0, LX/IBI;->r:LX/1QG;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1QG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2546877
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2546899
    iget-object v0, p0, LX/IBI;->c:LX/1QL;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1QL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2546900
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2546897
    iget-object v0, p0, LX/IBI;->g:LX/I6T;

    invoke-virtual {v0, p1, p2}, LX/I6T;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 2546898
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2546895
    iget-object v0, p0, LX/IBI;->e:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2546896
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2546893
    iget-object v0, p0, LX/IBI;->e:LX/1Q2;

    invoke-virtual {v0, p1, p2}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 2546894
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2546891
    iget-object v0, p0, LX/IBI;->n:LX/1QB;

    invoke-virtual {v0, p1}, LX/1QB;->a(Ljava/lang/String;)V

    .line 2546892
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2546889
    iget-object v0, p0, LX/IBI;->a:LX/1Pz;

    invoke-virtual {v0, p1, p2}, LX/1Pz;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2546890
    return-void
.end method

.method public final a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 2546887
    iget-object v0, p0, LX/IBI;->o:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2546888
    return-void
.end method

.method public final a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2546885
    iget-object v0, p0, LX/IBI;->o:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Ljava/lang/Object;)V

    .line 2546886
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 2546884
    iget-object v0, p0, LX/IBI;->q:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 2546882
    iget-object v0, p0, LX/IBI;->o:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->b(LX/1R6;)V

    .line 2546883
    return-void
.end method

.method public final b(LX/22C;)V
    .locals 1

    .prologue
    .line 2546880
    iget-object v0, p0, LX/IBI;->h:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->b(LX/22C;)V

    .line 2546881
    return-void
.end method

.method public final b(LX/5Oj;)V
    .locals 1

    .prologue
    .line 2546829
    iget-object v0, p0, LX/IBI;->t:LX/1PU;

    invoke-virtual {v0, p1}, LX/1PU;->b(LX/5Oj;)V

    .line 2546830
    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/events/model/Event;)V
    .locals 1

    .prologue
    .line 2546831
    iget-object v0, p0, LX/IBI;->d:LX/IBH;

    invoke-virtual {v0, p1, p2}, LX/IBH;->b(Landroid/content/Context;Lcom/facebook/events/model/Event;)V

    .line 2546832
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2546792
    iget-object v0, p0, LX/IBI;->g:LX/I6T;

    invoke-virtual {v0, p1, p2}, LX/I6T;->b(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 2546793
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2546808
    iget-object v0, p0, LX/IBI;->e:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2546809
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2546806
    iget-object v0, p0, LX/IBI;->q:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 2546807
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2546804
    iget-object v0, p0, LX/IBI;->a:LX/1Pz;

    invoke-virtual {v0, p1, p2}, LX/1Pz;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2546805
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2546802
    iget-object v0, p0, LX/IBI;->p:LX/1QD;

    invoke-virtual {v0, p1}, LX/1QD;->b(Z)V

    .line 2546803
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2546801
    iget-object v0, p0, LX/IBI;->m:LX/1QQ;

    invoke-virtual {v0}, LX/1QQ;->c()LX/1PT;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 2546800
    iget-object v0, p0, LX/IBI;->e:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 2546799
    iget-object v0, p0, LX/IBI;->l:LX/I6e;

    invoke-virtual {v0}, LX/I6e;->e()LX/1SX;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2546798
    iget-object v0, p0, LX/IBI;->r:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsModule()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "analyticsModule"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2546797
    iget-object v0, p0, LX/IBI;->i:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getAnalyticsModule()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2546796
    iget-object v0, p0, LX/IBI;->j:LX/1QP;

    invoke-virtual {v0}, LX/1QP;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getFontFoundry()LX/1QO;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2546794
    iget-object v0, p0, LX/IBI;->i:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getFontFoundry()LX/1QO;

    move-result-object v0

    return-object v0
.end method

.method public getNavigator()LX/5KM;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "navigator"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2546795
    iget-object v0, p0, LX/IBI;->i:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getNavigator()LX/5KM;

    move-result-object v0

    return-object v0
.end method

.method public getTraitCollection()LX/1QN;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "traitCollection"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2546811
    iget-object v0, p0, LX/IBI;->i:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getTraitCollection()LX/1QN;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2546812
    iget-object v0, p0, LX/IBI;->r:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2546813
    iget-object v0, p0, LX/IBI;->r:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->i()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2546810
    iget-object v0, p0, LX/IBI;->r:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 2546814
    iget-object v0, p0, LX/IBI;->o:LX/1QR;

    invoke-virtual {v0}, LX/1QR;->iN_()V

    .line 2546815
    return-void
.end method

.method public final iO_()Z
    .locals 1

    .prologue
    .line 2546816
    iget-object v0, p0, LX/IBI;->p:LX/1QD;

    invoke-virtual {v0}, LX/1QD;->iO_()Z

    move-result v0

    return v0
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2546817
    iget-object v0, p0, LX/IBI;->r:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->j()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2546818
    iget-object v0, p0, LX/IBI;->r:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->k()V

    .line 2546819
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2546820
    iget-object v0, p0, LX/IBI;->s:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->l()Z

    move-result v0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 2546821
    iget-object v0, p0, LX/IBI;->s:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final m_(Z)V
    .locals 1

    .prologue
    .line 2546822
    iget-object v0, p0, LX/IBI;->o:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->m_(Z)V

    .line 2546823
    return-void
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 2546824
    iget-object v0, p0, LX/IBI;->s:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2546825
    iget-object v0, p0, LX/IBI;->s:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->p()V

    .line 2546826
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2546827
    iget-object v0, p0, LX/IBI;->s:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->q()Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2546828
    iget-object v0, p0, LX/IBI;->u:LX/1QK;

    invoke-virtual {v0}, LX/1QK;->r()Z

    move-result v0

    return v0
.end method
