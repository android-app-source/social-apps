.class public final enum LX/IIC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IIC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IIC;

.field public static final enum ERROR:LX/IIC;

.field public static final enum FRIENDSLIST:LX/IIC;

.field public static final enum INVITE:LX/IIC;

.field public static final enum LOCATION_DISABLED:LX/IIC;

.field public static final enum LOCATION_REQUIRED_NOT_WORKING:LX/IIC;

.field public static final enum PAUSE:LX/IIC;

.field public static final enum UNKNOWN:LX/IIC;

.field public static final enum UPSELL:LX/IIC;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2561540
    new-instance v0, LX/IIC;

    const-string v1, "INVITE"

    const-string v2, "invite"

    invoke-direct {v0, v1, v4, v2}, LX/IIC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IIC;->INVITE:LX/IIC;

    .line 2561541
    new-instance v0, LX/IIC;

    const-string v1, "LOCATION_DISABLED"

    const-string v2, "location_instruction"

    invoke-direct {v0, v1, v5, v2}, LX/IIC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IIC;->LOCATION_DISABLED:LX/IIC;

    .line 2561542
    new-instance v0, LX/IIC;

    const-string v1, "LOCATION_REQUIRED_NOT_WORKING"

    const-string v2, "location_required_not_working"

    invoke-direct {v0, v1, v6, v2}, LX/IIC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IIC;->LOCATION_REQUIRED_NOT_WORKING:LX/IIC;

    .line 2561543
    new-instance v0, LX/IIC;

    const-string v1, "FRIENDSLIST"

    const-string v2, "friendslist"

    invoke-direct {v0, v1, v7, v2}, LX/IIC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IIC;->FRIENDSLIST:LX/IIC;

    .line 2561544
    new-instance v0, LX/IIC;

    const-string v1, "UPSELL"

    const-string v2, "upsell"

    invoke-direct {v0, v1, v8, v2}, LX/IIC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IIC;->UPSELL:LX/IIC;

    .line 2561545
    new-instance v0, LX/IIC;

    const-string v1, "PAUSE"

    const/4 v2, 0x5

    const-string v3, "pause"

    invoke-direct {v0, v1, v2, v3}, LX/IIC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IIC;->PAUSE:LX/IIC;

    .line 2561546
    new-instance v0, LX/IIC;

    const-string v1, "ERROR"

    const/4 v2, 0x6

    const-string v3, "error"

    invoke-direct {v0, v1, v2, v3}, LX/IIC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IIC;->ERROR:LX/IIC;

    .line 2561547
    new-instance v0, LX/IIC;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x7

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/IIC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IIC;->UNKNOWN:LX/IIC;

    .line 2561548
    const/16 v0, 0x8

    new-array v0, v0, [LX/IIC;

    sget-object v1, LX/IIC;->INVITE:LX/IIC;

    aput-object v1, v0, v4

    sget-object v1, LX/IIC;->LOCATION_DISABLED:LX/IIC;

    aput-object v1, v0, v5

    sget-object v1, LX/IIC;->LOCATION_REQUIRED_NOT_WORKING:LX/IIC;

    aput-object v1, v0, v6

    sget-object v1, LX/IIC;->FRIENDSLIST:LX/IIC;

    aput-object v1, v0, v7

    sget-object v1, LX/IIC;->UPSELL:LX/IIC;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/IIC;->PAUSE:LX/IIC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IIC;->ERROR:LX/IIC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IIC;->UNKNOWN:LX/IIC;

    aput-object v2, v0, v1

    sput-object v0, LX/IIC;->$VALUES:[LX/IIC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2561550
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2561551
    iput-object p3, p0, LX/IIC;->name:Ljava/lang/String;

    .line 2561552
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IIC;
    .locals 1

    .prologue
    .line 2561553
    const-class v0, LX/IIC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IIC;

    return-object v0
.end method

.method public static values()[LX/IIC;
    .locals 1

    .prologue
    .line 2561549
    sget-object v0, LX/IIC;->$VALUES:[LX/IIC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IIC;

    return-object v0
.end method
