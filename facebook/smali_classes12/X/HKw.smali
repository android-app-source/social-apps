.class public final LX/HKw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

.field public final c:I

.field public final d:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public final e:Landroid/view/View$OnClickListener;

.field public final f:Z


# direct methods
.method public constructor <init>(ZLjava/util/ArrayList;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;Landroid/view/View$OnClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;",
            "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLInterfaces$ReactionGeoRectangleFields;",
            "I",
            "Lcom/facebook/graphql/enums/GraphQLPlaceType;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2455357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2455358
    iput-boolean p1, p0, LX/HKw;->f:Z

    .line 2455359
    iput-object p2, p0, LX/HKw;->a:Ljava/util/ArrayList;

    .line 2455360
    iput-object p3, p0, LX/HKw;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    .line 2455361
    iput p4, p0, LX/HKw;->c:I

    .line 2455362
    iput-object p5, p0, LX/HKw;->d:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2455363
    iput-object p6, p0, LX/HKw;->e:Landroid/view/View$OnClickListener;

    .line 2455364
    return-void
.end method
