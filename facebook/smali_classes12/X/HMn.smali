.class public final LX/HMn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/services/PagesServicesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 0

    .prologue
    .line 2457600
    iput-object p1, p0, LX/HMn;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x1c57f57c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457601
    iget-object v1, p0, LX/HMn;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    sget-object v2, LX/0ig;->bd:LX/0ih;

    const-string v3, "tap_later"

    const-string v4, "on_service_list"

    invoke-virtual {v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2457602
    const-string v1, "admin_publish_services_edit_flow"

    iget-object v2, p0, LX/HMn;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/services/PagesServicesFragment;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2457603
    iget-object v1, p0, LX/HMn;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2457604
    :goto_0
    const v1, 0x4a0ed0cb    # 2339890.8f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2457605
    :cond_0
    iget-object v1, p0, LX/HMn;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->o:LX/CYT;

    iget-object v2, p0, LX/HMn;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v2, v2, LX/HN5;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/CYT;->d(Ljava/lang/String;)V

    .line 2457606
    iget-object v1, p0, LX/HMn;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method
