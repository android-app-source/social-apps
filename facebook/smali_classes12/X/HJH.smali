.class public LX/HJH;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HJL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HJH",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HJL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452329
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2452330
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HJH;->b:LX/0Zi;

    .line 2452331
    iput-object p1, p0, LX/HJH;->a:LX/0Ot;

    .line 2452332
    return-void
.end method

.method public static a(LX/0QB;)LX/HJH;
    .locals 4

    .prologue
    .line 2452333
    const-class v1, LX/HJH;

    monitor-enter v1

    .line 2452334
    :try_start_0
    sget-object v0, LX/HJH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452335
    sput-object v2, LX/HJH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452336
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452337
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452338
    new-instance v3, LX/HJH;

    const/16 p0, 0x2ba9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJH;-><init>(LX/0Ot;)V

    .line 2452339
    move-object v0, v3

    .line 2452340
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452341
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452342
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452343
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2452344
    check-cast p2, LX/HJG;

    .line 2452345
    iget-object v0, p0, LX/HJH;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HJL;

    iget-object v1, p2, LX/HJG;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p2, LX/HJG;->b:LX/HLF;

    iget-object v3, p2, LX/HJG;->c:LX/2km;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/HJL;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/2km;)LX/1Dg;

    move-result-object v0

    .line 2452346
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2452347
    invoke-static {}, LX/1dS;->b()V

    .line 2452348
    const/4 v0, 0x0

    return-object v0
.end method
