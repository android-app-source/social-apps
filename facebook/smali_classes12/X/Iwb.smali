.class public LX/Iwb;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/Iwc;",
        "LX/Iwa;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Iwb;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2630728
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2630729
    return-void
.end method

.method public static a(LX/0QB;)LX/Iwb;
    .locals 3

    .prologue
    .line 2630730
    sget-object v0, LX/Iwb;->a:LX/Iwb;

    if-nez v0, :cond_1

    .line 2630731
    const-class v1, LX/Iwb;

    monitor-enter v1

    .line 2630732
    :try_start_0
    sget-object v0, LX/Iwb;->a:LX/Iwb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2630733
    if-eqz v2, :cond_0

    .line 2630734
    :try_start_1
    new-instance v0, LX/Iwb;

    invoke-direct {v0}, LX/Iwb;-><init>()V

    .line 2630735
    move-object v0, v0

    .line 2630736
    sput-object v0, LX/Iwb;->a:LX/Iwb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2630737
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2630738
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2630739
    :cond_1
    sget-object v0, LX/Iwb;->a:LX/Iwb;

    return-object v0

    .line 2630740
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2630741
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
