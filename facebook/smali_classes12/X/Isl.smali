.class public LX/Isl;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Isk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private final b:Landroid/view/LayoutInflater;

.field public c:Z

.field public d:LX/Ism;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Ish;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2621266
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2621267
    new-instance v0, LX/Isj;

    invoke-direct {v0, p0}, LX/Isj;-><init>(LX/Isl;)V

    iput-object v0, p0, LX/Isl;->a:Landroid/view/View$OnClickListener;

    .line 2621268
    iput-boolean v1, p0, LX/Isl;->c:Z

    .line 2621269
    iput-object p1, p0, LX/Isl;->b:Landroid/view/LayoutInflater;

    .line 2621270
    invoke-virtual {p0, v1}, LX/1OM;->a(Z)V

    .line 2621271
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2621272
    iget-object v0, p0, LX/Isl;->e:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2621273
    iget-object v0, p0, LX/Isl;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ish;

    invoke-virtual {v0}, LX/Ish;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2621274
    iget-object v0, p0, LX/Isl;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030cd8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2621275
    new-instance v1, LX/Isk;

    invoke-direct {v1, v0}, LX/Isk;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2621276
    check-cast p1, LX/Isk;

    .line 2621277
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;

    .line 2621278
    iget-object v1, p0, LX/Isl;->e:LX/0Px;

    if-eqz v1, :cond_1

    .line 2621279
    iget-object v1, p0, LX/Isl;->e:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ish;

    .line 2621280
    iget-object v2, v1, LX/Ish;->a:Lcom/facebook/user/model/User;

    move-object v2, v2

    .line 2621281
    if-eqz v2, :cond_3

    .line 2621282
    iget-object v2, v1, LX/Ish;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2621283
    if-eqz v2, :cond_2

    .line 2621284
    iget-object v2, v1, LX/Ish;->a:Lcom/facebook/user/model/User;

    move-object v2, v2

    .line 2621285
    iget-object v3, v1, LX/Ish;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2621286
    invoke-virtual {v0, v2, v3}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a(Lcom/facebook/user/model/User;Ljava/lang/String;)V

    .line 2621287
    :cond_0
    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->setTag(Ljava/lang/Object;)V

    .line 2621288
    iput p2, v0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->f:I

    .line 2621289
    iget-object v1, p0, LX/Isl;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2621290
    iget-boolean v1, p0, LX/Isl;->c:Z

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->setSingleLine(Z)V

    .line 2621291
    :cond_1
    return-void

    .line 2621292
    :cond_2
    iget-object v2, v1, LX/Ish;->a:Lcom/facebook/user/model/User;

    move-object v2, v2

    .line 2621293
    invoke-virtual {v0, v2}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->setUser(Lcom/facebook/user/model/User;)V

    goto :goto_0

    .line 2621294
    :cond_3
    iget-object v2, v1, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v2

    .line 2621295
    if-eqz v2, :cond_0

    .line 2621296
    iget-object v2, v1, LX/Ish;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v2, v2

    .line 2621297
    invoke-virtual {v0, v2}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->setThreadSummary(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2621298
    iget-object v0, p0, LX/Isl;->e:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Isl;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
