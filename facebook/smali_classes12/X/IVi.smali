.class public final synthetic LX/IVi;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2582223
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->values()[Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/IVi;->b:[I

    :try_start_0
    sget-object v0, LX/IVi;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_0
    :try_start_1
    sget-object v0, LX/IVi;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->CLOSED:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    :goto_1
    :try_start_2
    sget-object v0, LX/IVi;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->SECRET:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    .line 2582224
    :goto_2
    invoke-static {}, LX/IVk;->values()[LX/IVk;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/IVi;->a:[I

    :try_start_3
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->PINNED_POST:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_3
    :try_start_4
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->FRIENDS_IN_GROUP:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    :goto_4
    :try_start_5
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->PRIVACY:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    :goto_5
    :try_start_6
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->ABOUT:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    :goto_6
    :try_start_7
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->UPCOMING_EVENT:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_7
    :try_start_8
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->PENDING_POST:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    :goto_8
    :try_start_9
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->YOUR_SALE_POSTS:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_9
    :try_start_a
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->MEMBER_REQUESTS:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_a
    :try_start_b
    sget-object v0, LX/IVi;->a:[I

    sget-object v1, LX/IVk;->REPORTED_POST:LX/IVk;

    invoke-virtual {v1}, LX/IVk;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_b
    return-void

    :catch_0
    goto :goto_b

    :catch_1
    goto :goto_a

    :catch_2
    goto :goto_9

    :catch_3
    goto :goto_8

    :catch_4
    goto :goto_7

    :catch_5
    goto :goto_6

    :catch_6
    goto :goto_5

    :catch_7
    goto :goto_4

    :catch_8
    goto :goto_3

    :catch_9
    goto :goto_2

    :catch_a
    goto/16 :goto_1

    :catch_b
    goto/16 :goto_0
.end method
