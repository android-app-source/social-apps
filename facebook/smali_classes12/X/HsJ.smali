.class public final LX/HsJ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/HsI;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HsL;


# direct methods
.method public constructor <init>(LX/HsL;)V
    .locals 0

    .prologue
    .line 2514225
    iput-object p1, p0, LX/HsJ;->a:LX/HsL;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2514226
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2514227
    check-cast p1, LX/HsI;

    .line 2514228
    iget-object v0, p0, LX/HsJ;->a:LX/HsL;

    iget-object v0, v0, LX/HsL;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514229
    iget-object v1, p0, LX/HsJ;->a:LX/HsL;

    iget-object v2, p1, LX/HsI;->c:LX/0Px;

    .line 2514230
    iget-object v3, v1, LX/HsL;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    .line 2514231
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    invoke-interface {v3}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2514232
    const/4 v3, 0x0

    move v5, v3

    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_1

    .line 2514233
    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HsC;

    .line 2514234
    iget v4, v3, LX/HsC;->d:I

    move v4, v4

    .line 2514235
    const/4 v7, -0x1

    if-eq v4, v7, :cond_0

    .line 2514236
    iget v4, v3, LX/HsC;->d:I

    move v4, v4

    .line 2514237
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2514238
    iget-object v7, v3, LX/HsC;->c:Lcom/facebook/composer/attachments/ComposerAttachment;

    move-object v7, v7

    .line 2514239
    if-ne v4, v7, :cond_0

    .line 2514240
    iget v7, v3, LX/HsC;->d:I

    move v7, v7

    .line 2514241
    invoke-static {v4}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v8

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v4

    .line 2514242
    iget-object v1, v3, LX/HsC;->b:Landroid/net/Uri;

    move-object v3, v1

    .line 2514243
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setDisplayUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 2514244
    iput-object v3, v8, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 2514245
    move-object v3, v8

    .line 2514246
    invoke-virtual {v3}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    invoke-virtual {v6, v7, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2514247
    :cond_0
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 2514248
    :cond_1
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 2514249
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1, v2}, LX/HsL;->b(LX/0Px;LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2514250
    iget-object v1, p0, LX/HsJ;->a:LX/HsL;

    .line 2514251
    iput-object v2, v1, LX/HsL;->j:LX/0Px;

    .line 2514252
    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    const-class v1, LX/HsL;

    invoke-static {v1}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, v2}, LX/0jL;->c(LX/0Px;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2514253
    :cond_2
    iget-object v0, p0, LX/HsJ;->a:LX/HsL;

    const/4 v3, 0x0

    .line 2514254
    iget-object v1, p1, LX/HsI;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    new-array v4, v1, [LX/1aZ;

    move v2, v3

    .line 2514255
    :goto_1
    iget-object v1, p1, LX/HsI;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 2514256
    iget-object v1, p1, LX/HsI;->c:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HsC;

    .line 2514257
    iget-object v5, v1, LX/HsC;->a:LX/1aZ;

    move-object v1, v5

    .line 2514258
    aput-object v1, v4, v2

    .line 2514259
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2514260
    :cond_3
    iget-object v1, v0, LX/HsL;->l:Landroid/view/ViewGroup;

    const-string v2, "bindCollage() should never be called before bind()."

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2514261
    iget-object v1, v0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    if-nez v1, :cond_4

    .line 2514262
    iget-object v1, v0, LX/HsL;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03030e

    iget-object v5, v0, LX/HsL;->l:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    iput-object v1, v0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    .line 2514263
    iget-object v1, v0, LX/HsL;->l:Landroid/view/ViewGroup;

    iget-object v2, v0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2514264
    :cond_4
    iget-object v1, v0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    invoke-virtual {v1}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->a()V

    .line 2514265
    iget-object v1, v0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    iget-object v2, p1, LX/HsI;->b:LX/26O;

    invoke-virtual {v1, v2, v4}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->a(LX/26O;[LX/1aZ;)V

    .line 2514266
    iget-object v1, v0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    iget-object v2, p1, LX/HsI;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    array-length v3, v4

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->setInvisiblePhotoCount(I)V

    .line 2514267
    iget-object v1, v0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    iget-object v2, v0, LX/HsL;->i:LX/HsF;

    .line 2514268
    iput-object v2, v1, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->d:LX/HsF;

    .line 2514269
    invoke-static {v0}, LX/HsL;->e(LX/HsL;)V

    .line 2514270
    return-void
.end method
