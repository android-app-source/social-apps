.class public LX/JJ0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fx7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Fx7",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLInterfaces$DiscoveryPhotoProtileItemFields;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JJ0;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2678793
    return-void
.end method

.method public static a(Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLInterfaces$DiscoveryPhotoProtileSectionFields;",
            ")",
            "LX/0Px",
            "<+",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLInterfaces$DiscoveryPhotoProtileItemFields;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2678794
    invoke-virtual {p0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel$ProfileTileItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel$ProfileTileItemsModel;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JJ0;
    .locals 3

    .prologue
    .line 2678795
    sget-object v0, LX/JJ0;->a:LX/JJ0;

    if-nez v0, :cond_1

    .line 2678796
    const-class v1, LX/JJ0;

    monitor-enter v1

    .line 2678797
    :try_start_0
    sget-object v0, LX/JJ0;->a:LX/JJ0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2678798
    if-eqz v2, :cond_0

    .line 2678799
    :try_start_1
    new-instance v0, LX/JJ0;

    invoke-direct {v0}, LX/JJ0;-><init>()V

    .line 2678800
    move-object v0, v0

    .line 2678801
    sput-object v0, LX/JJ0;->a:LX/JJ0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2678802
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2678803
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2678804
    :cond_1
    sget-object v0, LX/JJ0;->a:LX/JJ0;

    return-object v0

    .line 2678805
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2678806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;I)LX/5vo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLInterfaces$DiscoveryPhotoProtileItemFields;",
            ">;I)",
            "LX/5vo;"
        }
    .end annotation

    .prologue
    .line 2678807
    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->b()LX/G1S;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0Px;I)LX/5vn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLInterfaces$DiscoveryPhotoProtileItemFields;",
            ">;I)",
            "LX/5vn;"
        }
    .end annotation

    .prologue
    .line 2678808
    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileItemFieldsModel;->a()LX/5vn;

    move-result-object v0

    return-object v0
.end method
