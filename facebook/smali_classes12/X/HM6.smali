.class public final enum LX/HM6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HM6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HM6;

.field public static final enum HORIZONTAL:LX/HM6;

.field public static final enum ONE_BIG_SIDE_SMALL_GRID:LX/HM6;

.field public static final enum UNSUPPORTED:LX/HM6;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2456738
    new-instance v0, LX/HM6;

    const-string v1, "UNSUPPORTED"

    invoke-direct {v0, v1, v2}, LX/HM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HM6;->UNSUPPORTED:LX/HM6;

    .line 2456739
    new-instance v0, LX/HM6;

    const-string v1, "ONE_BIG_SIDE_SMALL_GRID"

    invoke-direct {v0, v1, v3}, LX/HM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HM6;->ONE_BIG_SIDE_SMALL_GRID:LX/HM6;

    .line 2456740
    new-instance v0, LX/HM6;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v4}, LX/HM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HM6;->HORIZONTAL:LX/HM6;

    .line 2456741
    const/4 v0, 0x3

    new-array v0, v0, [LX/HM6;

    sget-object v1, LX/HM6;->UNSUPPORTED:LX/HM6;

    aput-object v1, v0, v2

    sget-object v1, LX/HM6;->ONE_BIG_SIDE_SMALL_GRID:LX/HM6;

    aput-object v1, v0, v3

    sget-object v1, LX/HM6;->HORIZONTAL:LX/HM6;

    aput-object v1, v0, v4

    sput-object v0, LX/HM6;->$VALUES:[LX/HM6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2456742
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromPhotoCount(I)LX/HM6;
    .locals 1

    .prologue
    const/4 v0, 0x3

    .line 2456743
    if-ne p0, v0, :cond_0

    .line 2456744
    sget-object v0, LX/HM6;->ONE_BIG_SIDE_SMALL_GRID:LX/HM6;

    .line 2456745
    :goto_0
    return-object v0

    .line 2456746
    :cond_0
    if-lez p0, :cond_1

    if-ge p0, v0, :cond_1

    .line 2456747
    sget-object v0, LX/HM6;->HORIZONTAL:LX/HM6;

    goto :goto_0

    .line 2456748
    :cond_1
    sget-object v0, LX/HM6;->UNSUPPORTED:LX/HM6;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/HM6;
    .locals 1

    .prologue
    .line 2456749
    const-class v0, LX/HM6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HM6;

    return-object v0
.end method

.method public static values()[LX/HM6;
    .locals 1

    .prologue
    .line 2456750
    sget-object v0, LX/HM6;->$VALUES:[LX/HM6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HM6;

    return-object v0
.end method
