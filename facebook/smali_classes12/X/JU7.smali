.class public final LX/JU7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JUH;

.field public final synthetic b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;LX/JUH;)V
    .locals 0

    .prologue
    .line 2697719
    iput-object p1, p0, LX/JU7;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    iput-object p2, p0, LX/JU7;->a:LX/JUH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x5ab3ee1b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697720
    iget-object v1, p0, LX/JU7;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    iget-object v2, p0, LX/JU7;->a:LX/JUH;

    .line 2697721
    iget-object v3, v2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->p()Ljava/lang/String;

    move-result-object v3

    .line 2697722
    new-instance v5, LX/89k;

    invoke-direct {v5}, LX/89k;-><init>()V

    .line 2697723
    iput-object v3, v5, LX/89k;->b:Ljava/lang/String;

    .line 2697724
    move-object v3, v5

    .line 2697725
    invoke-virtual {v3}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v3

    .line 2697726
    iget-object v5, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->e:LX/0hy;

    invoke-interface {v5, v3}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v3

    .line 2697727
    iget-object v5, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v5, v3, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2697728
    iget-object v1, p0, LX/JU7;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->d:LX/JUO;

    iget-object v2, p0, LX/JU7;->a:LX/JUH;

    iget-object v2, v2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v3, p0, LX/JU7;->a:LX/JUH;

    iget-object v3, v3, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v1, v2, v3}, LX/JUO;->a(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2697729
    const v1, 0x39397fe2

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
