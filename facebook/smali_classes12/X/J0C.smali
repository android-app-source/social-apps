.class public LX/J0C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;",
        "Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2636596
    const-class v0, LX/J0C;

    sput-object v0, LX/J0C;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636598
    return-void
.end method

.method public static a(LX/0QB;)LX/J0C;
    .locals 1

    .prologue
    .line 2636599
    new-instance v0, LX/J0C;

    invoke-direct {v0}, LX/J0C;-><init>()V

    .line 2636600
    move-object v0, v0

    .line 2636601
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2636602
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;

    .line 2636603
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2636604
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "creditCardNumber"

    .line 2636605
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2636606
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636607
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "month"

    .line 2636608
    iget v3, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->b:I

    move v3, v3

    .line 2636609
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636610
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "year"

    .line 2636611
    iget v3, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->c:I

    move v3, v3

    .line 2636612
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636613
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "csc"

    .line 2636614
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2636615
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636616
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "zip"

    .line 2636617
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2636618
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636619
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "product_type"

    .line 2636620
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2636621
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636622
    iget-object v1, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->h:Ljava/lang/String;

    move-object v1, v1

    .line 2636623
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2636624
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "product_id"

    .line 2636625
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2636626
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636627
    :cond_0
    const-string v1, "/%d/p2p_credit_cards"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2636628
    iget-object v4, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->f:Ljava/lang/String;

    move-object v4, v4

    .line 2636629
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, LX/73t;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/14O;

    move-result-object v1

    const-string v2, "p2p_credit_cards"

    .line 2636630
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2636631
    move-object v1, v1

    .line 2636632
    const-string v2, "POST"

    .line 2636633
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2636634
    move-object v1, v1

    .line 2636635
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2636636
    move-object v0, v1

    .line 2636637
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2636638
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2636639
    move-object v0, v0

    .line 2636640
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2636641
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2636642
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2636643
    const-class v1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;

    .line 2636644
    return-object v0
.end method
