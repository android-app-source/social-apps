.class public LX/Ieu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private a:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2599094
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Ieu;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2599095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2599096
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ieu;->b:Ljava/util/Map;

    .line 2599097
    return-void
.end method

.method public static a(LX/0QB;)LX/Ieu;
    .locals 7

    .prologue
    .line 2599098
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2599099
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2599100
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2599101
    if-nez v1, :cond_0

    .line 2599102
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2599103
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2599104
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2599105
    sget-object v1, LX/Ieu;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2599106
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2599107
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2599108
    :cond_1
    if-nez v1, :cond_4

    .line 2599109
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2599110
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2599111
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 2599112
    new-instance v0, LX/Ieu;

    invoke-direct {v0}, LX/Ieu;-><init>()V

    .line 2599113
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2599114
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2599115
    if-nez v1, :cond_2

    .line 2599116
    sget-object v0, LX/Ieu;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ieu;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2599117
    :goto_1
    if-eqz v0, :cond_3

    .line 2599118
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2599119
    :goto_3
    check-cast v0, LX/Ieu;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2599120
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2599121
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2599122
    :catchall_1
    move-exception v0

    .line 2599123
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2599124
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2599125
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2599126
    :cond_2
    :try_start_8
    sget-object v0, LX/Ieu;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ieu;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/Ieu;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;
    .locals 4
    .param p0    # LX/Ieu;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2599127
    iget-object v0, p0, LX/Ieu;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2599128
    iget-object v0, p0, LX/Ieu;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    iget-object v0, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    .line 2599129
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599130
    iget-object p0, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599131
    iget-object p1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p0, p1

    .line 2599132
    invoke-static {p0, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2599133
    :goto_1
    move-object v0, v1

    .line 2599134
    :goto_2
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 2599135
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2599136
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2599137
    invoke-static {p0, p1, p2}, LX/Ieu;->b(LX/Ieu;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-result-object v2

    .line 2599138
    iget-object v0, p0, LX/Ieu;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2599139
    iget-object v4, p0, LX/Ieu;->b:Ljava/util/Map;

    iget-object v1, p0, LX/Ieu;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    .line 2599140
    if-eqz v1, :cond_1

    new-instance v5, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    iget-object v6, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    .line 2599141
    new-instance v7, LX/Iet;

    invoke-direct {v7, p0, p2}, LX/Iet;-><init>(LX/Ieu;Ljava/lang/String;)V

    invoke-static {v6, v7}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v7

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v7

    move-object v6, v7

    .line 2599142
    iget-wide v7, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->b:J

    invoke-direct {v5, v6, v7, v8}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;-><init>(LX/0Px;J)V

    :goto_1
    move-object v1, v5

    .line 2599143
    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2599144
    :cond_0
    return-object v2

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2599145
    iget-object v0, p0, LX/Ieu;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599146
    iget-object v0, p0, LX/Ieu;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2599147
    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 2599148
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/Ieu;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2599149
    monitor-exit p0

    return-void

    .line 2599150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 2599151
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/Ieu;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599152
    iget-object v0, p0, LX/Ieu;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ieu;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    iget-object v0, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
