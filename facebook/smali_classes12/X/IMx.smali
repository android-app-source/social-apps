.class public final LX/IMx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7HP;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7HP",
        "<",
        "LX/IN5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

.field private b:LX/IN9;

.field private c:LX/DSo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DSo",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;LX/IN9;LX/DSo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IN9;",
            "LX/DSo",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2570320
    iput-object p1, p0, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2570321
    iput-object p2, p0, LX/IMx;->b:LX/IN9;

    .line 2570322
    iput-object p3, p0, LX/IMx;->c:LX/DSo;

    .line 2570323
    return-void
.end method


# virtual methods
.method public final a(LX/7Hi;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "LX/IN5;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2570324
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v1, v0

    .line 2570325
    iget-object v0, p0, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2570326
    :goto_0
    new-instance v2, LX/IMw;

    invoke-direct {v2, p0, v0}, LX/IMw;-><init>(LX/IMx;Ljava/lang/String;)V

    .line 2570327
    iget-object v3, p0, LX/IMx;->b:LX/IN9;

    .line 2570328
    iget-object v4, v1, LX/7Hc;->b:LX/0Px;

    move-object v1, v4

    .line 2570329
    iget-object v4, p0, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-boolean v4, v4, Lcom/facebook/groups/community/search/CommunitySearchFragment;->p:Z

    .line 2570330
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2570331
    sget-object v6, LX/INA;->ALL_GROUPS:LX/INA;

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2570332
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v5, v3

    move-object v7, v1

    move v8, v4

    move-object v9, v0

    move-object v10, v2

    invoke-static/range {v5 .. v10}, LX/IN9;->a(LX/IN9;LX/0Px;LX/0Px;ZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2570333
    iget-object v0, p0, LX/IMx;->c:LX/DSo;

    const v1, 0x47a71537

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2570334
    return-void

    .line 2570335
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
