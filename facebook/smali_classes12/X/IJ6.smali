.class public LX/IJ6;
.super LX/8RF;
.source ""


# instance fields
.field public a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2563089
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/8RF;-><init>(Z)V

    .line 2563090
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2563091
    iput-object v0, p0, LX/IJ6;->a:LX/0Rf;

    .line 2563092
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/8QL;Z)V
    .locals 5

    .prologue
    .line 2563081
    check-cast p2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2563082
    iget-object v0, p2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 2563083
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    .line 2563084
    iget-object v1, p0, LX/IJ6;->a:LX/0Rf;

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2563085
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083868

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2563086
    new-instance v1, Lcom/facebook/friendsnearby/ui/InvitedSimpleUserToken;

    invoke-direct {v1, p2, v0}, Lcom/facebook/friendsnearby/ui/InvitedSimpleUserToken;-><init>(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Ljava/lang/String;)V

    invoke-super {p0, p1, v1, p3}, LX/8RF;->a(Landroid/view/View;LX/8QL;Z)V

    .line 2563087
    :goto_0
    return-void

    .line 2563088
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/8RF;->a(Landroid/view/View;LX/8QL;Z)V

    goto :goto_0
.end method
