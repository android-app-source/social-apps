.class public LX/I5E;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/location/FbLocationOperationParams;

.field private c:LX/0i4;

.field public d:LX/1sS;

.field public e:LX/1Ck;

.field public f:LX/H4I;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2535212
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, LX/I5E;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0i4;LX/0Or;LX/1Ck;LX/H4I;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0i4;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "LX/1Ck;",
            "LX/H4I;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2535202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2535203
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    const-wide/16 v2, 0x4e20

    .line 2535204
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2535205
    move-object v0, v0

    .line 2535206
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    iput-object v0, p0, LX/I5E;->b:Lcom/facebook/location/FbLocationOperationParams;

    .line 2535207
    iput-object p1, p0, LX/I5E;->c:LX/0i4;

    .line 2535208
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sS;

    iput-object v0, p0, LX/I5E;->d:LX/1sS;

    .line 2535209
    iput-object p3, p0, LX/I5E;->e:LX/1Ck;

    .line 2535210
    iput-object p4, p0, LX/I5E;->f:LX/H4I;

    .line 2535211
    return-void
.end method

.method public static a(LX/0QB;)LX/I5E;
    .locals 1

    .prologue
    .line 2535213
    invoke-static {p0}, LX/I5E;->b(LX/0QB;)LX/I5E;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/I5E;
    .locals 5

    .prologue
    .line 2535200
    new-instance v3, LX/I5E;

    const-class v0, LX/0i4;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/0i4;

    const/16 v1, 0xc81

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/H4I;->b(LX/0QB;)LX/H4I;

    move-result-object v2

    check-cast v2, LX/H4I;

    invoke-direct {v3, v0, v4, v1, v2}, LX/I5E;-><init>(LX/0i4;LX/0Or;LX/1Ck;LX/H4I;)V

    .line 2535201
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/app/Activity;LX/0Vd;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/0Vd",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2535198
    iget-object v0, p0, LX/I5E;->c:LX/0i4;

    invoke-virtual {v0, p1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    sget-object v1, LX/I5E;->a:[Ljava/lang/String;

    new-instance v2, LX/I5D;

    invoke-direct {v2, p0, p3, p2}, LX/I5D;-><init>(LX/I5E;Lcom/facebook/common/callercontext/CallerContext;LX/0Vd;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2535199
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2535196
    iget-object v0, p0, LX/I5E;->f:LX/H4I;

    new-instance v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

    invoke-direct {v1, p1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, p2}, LX/H4I;->a(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;LX/0TF;)V

    .line 2535197
    return-void
.end method
