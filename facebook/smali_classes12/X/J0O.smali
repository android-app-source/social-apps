.class public LX/J0O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;",
        "Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;


# direct methods
.method public constructor <init>(LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636983
    iput-object p1, p0, LX/J0O;->a:LX/0dC;

    .line 2636984
    return-void
.end method

.method public static a(LX/0QB;)LX/J0O;
    .locals 2

    .prologue
    .line 2636985
    new-instance v1, LX/J0O;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v0

    check-cast v0, LX/0dC;

    invoke-direct {v1, v0}, LX/J0O;-><init>(LX/0dC;)V

    .line 2636986
    move-object v0, v1

    .line 2636987
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2636943
    check-cast p1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;

    .line 2636944
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2636945
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "amount"

    .line 2636946
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v3, v3

    .line 2636947
    iget-object v4, v3, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v3, v4

    .line 2636948
    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636949
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_credential"

    .line 2636950
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2636951
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636952
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pin"

    .line 2636953
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2636954
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636955
    iget-object v1, p1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2636956
    if-eqz v1, :cond_0

    .line 2636957
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "touchid_nonce"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636958
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/J0O;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636959
    :cond_0
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "external_request_id"

    .line 2636960
    iget-wide v6, p1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->f:J

    move-wide v4, v6

    .line 2636961
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636962
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "item_id"

    .line 2636963
    iget-wide v6, p1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->g:J

    move-wide v4, v6

    .line 2636964
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636965
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "money_penny_place_order"

    .line 2636966
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2636967
    move-object v1, v1

    .line 2636968
    const-string v2, "POST"

    .line 2636969
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2636970
    move-object v1, v1

    .line 2636971
    const-string v2, "/%s/%s"

    .line 2636972
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2636973
    const-string v4, "moneypenny_payments"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2636974
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2636975
    move-object v1, v1

    .line 2636976
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2636977
    move-object v0, v1

    .line 2636978
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2636979
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2636980
    move-object v0, v0

    .line 2636981
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2636940
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2636941
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2636942
    const-class v1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;

    return-object v0
.end method
