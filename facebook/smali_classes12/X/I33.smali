.class public LX/I33;
.super LX/Gco;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2531002
    invoke-direct {p0}, LX/Gco;-><init>()V

    .line 2531003
    iput-object p1, p0, LX/I33;->a:LX/0tX;

    .line 2531004
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/events/model/Event;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531005
    const v0, 0x7f0821cd

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 3

    .prologue
    .line 2531006
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    const-string v1, "MOBILE_SUBSCRIPTIONS_DASHBOARD"

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    .line 2531007
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    move-result-object v0

    .line 2531008
    new-instance v1, LX/4Jc;

    invoke-direct {v1}, LX/4Jc;-><init>()V

    .line 2531009
    iget-object v2, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2531010
    const-string p1, "event_id"

    invoke-virtual {v1, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531011
    move-object v1, v1

    .line 2531012
    const-string v2, "HIDE"

    .line 2531013
    const-string p1, "negative_action_type"

    invoke-virtual {v1, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531014
    move-object v1, v1

    .line 2531015
    const-string v2, "context"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2531016
    move-object v0, v1

    .line 2531017
    new-instance v1, LX/7uO;

    invoke-direct {v1}, LX/7uO;-><init>()V

    move-object v1, v1

    .line 2531018
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7uO;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2531019
    iget-object v1, p0, LX/I33;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2531020
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2531021
    const/4 v0, 0x0

    return v0
.end method
