.class public LX/IN4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/IN4;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0TD;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/search/api/GraphSearchQuery;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0tX;LX/0TD;)V
    .locals 1
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2570529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2570530
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    iput-object v0, p0, LX/IN4;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2570531
    iput-object p2, p0, LX/IN4;->a:LX/0tX;

    .line 2570532
    const v0, 0x7f0b2008

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IN4;->c:Ljava/lang/String;

    .line 2570533
    iput-object p3, p0, LX/IN4;->b:LX/0TD;

    .line 2570534
    return-void
.end method

.method public static a(LX/0QB;)LX/IN4;
    .locals 6

    .prologue
    .line 2570535
    sget-object v0, LX/IN4;->f:LX/IN4;

    if-nez v0, :cond_1

    .line 2570536
    const-class v1, LX/IN4;

    monitor-enter v1

    .line 2570537
    :try_start_0
    sget-object v0, LX/IN4;->f:LX/IN4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2570538
    if-eqz v2, :cond_0

    .line 2570539
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2570540
    new-instance p0, LX/IN4;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-direct {p0, v3, v4, v5}, LX/IN4;-><init>(Landroid/content/res/Resources;LX/0tX;LX/0TD;)V

    .line 2570541
    move-object v0, p0

    .line 2570542
    sput-object v0, LX/IN4;->f:LX/IN4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2570543
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2570544
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2570545
    :cond_1
    sget-object v0, LX/IN4;->f:LX/IN4;

    return-object v0

    .line 2570546
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2570547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
