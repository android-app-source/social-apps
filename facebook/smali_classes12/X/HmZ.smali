.class public LX/HmZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HardcodedIPAddressUse"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2500831
    new-instance v0, LX/HmX;

    invoke-direct {v0}, LX/HmX;-><init>()V

    sput-object v0, LX/HmZ;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2500832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;)LX/0h5;
    .locals 2

    .prologue
    .line 2500833
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2500834
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2500835
    const v1, 0x7f08393a

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2500836
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 2500837
    new-instance v1, LX/HmY;

    invoke-direct {v1, p0}, LX/HmY;-><init>(Landroid/app/Activity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2500838
    return-object v0
.end method

.method public static a()LX/Hm4;
    .locals 3

    .prologue
    .line 2500839
    const-string v0, "192.168.43.1"

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 2500840
    new-instance v1, Ljava/net/Socket;

    const/16 v2, 0x6219

    invoke-direct {v1, v0, v2}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    .line 2500841
    invoke-virtual {v1}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    .line 2500842
    new-instance v0, LX/Hm4;

    invoke-direct {v0, v1}, LX/Hm4;-><init>(Ljava/net/Socket;)V

    return-object v0
.end method
