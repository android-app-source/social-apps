.class public LX/IXn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/view/ViewPager;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/animation/ValueAnimator;

.field public final c:Landroid/animation/ValueAnimator;

.field public d:I


# direct methods
.method public constructor <init>(Landroid/support/v4/view/ViewPager;)V
    .locals 1

    .prologue
    .line 2586608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2586609
    const/4 v0, 0x0

    iput v0, p0, LX/IXn;->d:I

    .line 2586610
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/IXn;->a:Ljava/lang/ref/WeakReference;

    .line 2586611
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, LX/IXn;->b:Landroid/animation/ValueAnimator;

    .line 2586612
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, LX/IXn;->c:Landroid/animation/ValueAnimator;

    .line 2586613
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 2586614
    iget-object v0, p0, LX/IXn;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2586615
    :goto_0
    return-void

    .line 2586616
    :cond_0
    iget-object v0, p0, LX/IXn;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v6

    .line 2586617
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    const-wide/16 v4, 0xc8

    move-object v1, p0

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2586618
    iget-object v0, v1, LX/IXn;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2586619
    if-eqz v0, :cond_1

    iget-object v7, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2586620
    :cond_1
    :goto_1
    goto :goto_0

    .line 2586621
    :cond_2
    iget-object v7, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v7, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2586622
    iget-object v7, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    new-array v8, v11, [I

    aput v9, v8, v9

    aput v6, v8, v10

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 2586623
    iget-object v7, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v7, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2586624
    iget-object v7, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 2586625
    iget-object v7, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 2586626
    iget-object v7, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    new-instance v8, LX/IXj;

    invoke-direct {v8, v1, v0}, LX/IXj;-><init>(LX/IXn;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2586627
    iget-object v7, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    new-instance v8, LX/IXk;

    invoke-direct {v8, v1, v0}, LX/IXk;-><init>(LX/IXn;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2586628
    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v7, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2586629
    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    new-array v8, v11, [I

    aput v9, v8, v9

    aput v6, v8, v10

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 2586630
    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v7, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2586631
    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v7, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2586632
    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 2586633
    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 2586634
    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    new-instance v8, LX/IXl;

    invoke-direct {v8, v1, v0}, LX/IXl;-><init>(LX/IXn;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2586635
    iget-object v7, v1, LX/IXn;->c:Landroid/animation/ValueAnimator;

    new-instance v8, LX/IXm;

    invoke-direct {v8, v1, v0}, LX/IXm;-><init>(LX/IXn;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2586636
    iget-object v0, v1, LX/IXn;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1
.end method
