.class public LX/IBj;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IBo;

.field public c:LX/IBg;

.field public d:LX/IBi;

.field public e:LX/IBn;

.field public f:LX/IBZ;

.field public g:LX/IBY;

.field public h:LX/IBh;

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 6

    .prologue
    .line 2547535
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2547536
    iput-boolean p2, p0, LX/IBj;->i:Z

    .line 2547537
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2547538
    const-class v0, LX/IBj;

    invoke-static {v0, p0}, LX/IBj;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2547539
    invoke-virtual {p0, v3}, LX/IBj;->setOrientation(I)V

    .line 2547540
    invoke-virtual {p0}, LX/IBj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2547541
    const v0, 0x7f0a00d5

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/IBj;->setBackgroundColor(I)V

    .line 2547542
    iget-boolean v0, p0, LX/IBj;->i:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2547543
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2547544
    const v0, 0x7f0a0598

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2547545
    iget-boolean v0, p0, LX/IBj;->i:Z

    if-nez v0, :cond_0

    .line 2547546
    invoke-virtual {p0}, LX/IBj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1444

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b_(II)V

    .line 2547547
    :cond_0
    const/4 p2, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 2547548
    invoke-virtual {p0}, LX/IBj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2547549
    const v1, 0x7f0b1442

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2547550
    const v2, 0x7f0b1443

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2547551
    new-instance v0, LX/IBo;

    invoke-direct {v0, p1}, LX/IBo;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, v1, v5}, LX/IBj;->a(LX/IBj;Landroid/widget/TextView;II)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, LX/IBo;

    iput-object v0, p0, LX/IBj;->b:LX/IBo;

    .line 2547552
    iget-object v0, p0, LX/IBj;->b:LX/IBo;

    invoke-static {p0, v2}, LX/IBj;->d(LX/IBj;I)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, LX/IBj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2547553
    new-instance v0, LX/IBg;

    invoke-direct {v0, p1}, LX/IBg;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, v1, p2}, LX/IBj;->a(LX/IBj;Landroid/widget/TextView;II)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, LX/IBg;

    iput-object v0, p0, LX/IBj;->c:LX/IBg;

    .line 2547554
    iget-object v0, p0, LX/IBj;->c:LX/IBg;

    invoke-static {p0, v2}, LX/IBj;->d(LX/IBj;I)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, LX/IBj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2547555
    new-instance v0, LX/IBi;

    invoke-direct {v0, p1}, LX/IBi;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, v1, p2}, LX/IBj;->a(LX/IBj;Landroid/widget/TextView;II)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, LX/IBi;

    iput-object v0, p0, LX/IBj;->d:LX/IBi;

    .line 2547556
    iget-object v0, p0, LX/IBj;->d:LX/IBi;

    invoke-static {p0, v2}, LX/IBj;->d(LX/IBj;I)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, LX/IBj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2547557
    new-instance v0, LX/IBn;

    invoke-direct {v0, p1}, LX/IBn;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, v1, v5}, LX/IBj;->a(LX/IBj;Landroid/widget/TextView;II)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, LX/IBn;

    iput-object v0, p0, LX/IBj;->e:LX/IBn;

    .line 2547558
    iget-object v0, p0, LX/IBj;->e:LX/IBn;

    invoke-static {p0, v2}, LX/IBj;->d(LX/IBj;I)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, LX/IBj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2547559
    new-instance v0, LX/IBZ;

    invoke-direct {v0, p1}, LX/IBZ;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, v1, v4}, LX/IBj;->a(LX/IBj;Landroid/widget/TextView;II)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, LX/IBZ;

    iput-object v0, p0, LX/IBj;->f:LX/IBZ;

    .line 2547560
    iget-object v0, p0, LX/IBj;->f:LX/IBZ;

    invoke-static {p0, v2}, LX/IBj;->d(LX/IBj;I)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, LX/IBj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2547561
    new-instance v0, LX/IBY;

    invoke-direct {v0, p1}, LX/IBY;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, v1, v4}, LX/IBj;->a(LX/IBj;Landroid/widget/TextView;II)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, LX/IBY;

    iput-object v0, p0, LX/IBj;->g:LX/IBY;

    .line 2547562
    iget-object v0, p0, LX/IBj;->g:LX/IBY;

    invoke-static {p0, v2}, LX/IBj;->d(LX/IBj;I)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, LX/IBj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2547563
    new-instance v0, LX/IBh;

    invoke-direct {v0, p1}, LX/IBh;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, v1, v4}, LX/IBj;->a(LX/IBj;Landroid/widget/TextView;II)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, LX/IBh;

    iput-object v0, p0, LX/IBj;->h:LX/IBh;

    .line 2547564
    iget-object v0, p0, LX/IBj;->h:LX/IBh;

    invoke-static {p0, v2}, LX/IBj;->d(LX/IBj;I)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/IBj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2547565
    return-void

    .line 2547566
    :cond_1
    const/4 v0, 0x2

    goto/16 :goto_0
.end method

.method public static a(LX/IBj;Landroid/widget/TextView;II)Landroid/widget/TextView;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/TextView;",
            ">(TT;II)TT;"
        }
    .end annotation

    .prologue
    .line 2547525
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 2547526
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2547527
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 2547528
    iget-boolean v0, p0, LX/IBj;->i:Z

    if-eqz v0, :cond_0

    .line 2547529
    const/16 v0, 0x30

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2547530
    invoke-virtual {p0}, LX/IBj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2547531
    const v1, 0x7f0b145e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0b0069

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x7f0b006d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2547532
    const/4 v0, 0x0

    const v1, 0x3f99999a    # 1.2f

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 2547533
    :goto_0
    return-object p1

    .line 2547534
    :cond_0
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method public static a(LX/IBj;Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 2
    .param p2    # Lcom/facebook/events/model/Event;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Landroid/view/View;",
            ":",
            "LX/IBX;",
            ">(TS;",
            "Lcom/facebook/events/model/Event;",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$FetchEventPermalinkFragment;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2547520
    move-object v0, p1

    check-cast v0, LX/IBX;

    invoke-interface {v0, p2, p3}, LX/IBX;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2547521
    check-cast v0, LX/IBX;

    iget-boolean v1, p0, LX/IBj;->i:Z

    invoke-interface {v0, p2, p3, p4, v1}, LX/IBX;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;Z)V

    .line 2547522
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2547523
    :goto_0
    return-void

    .line 2547524
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IBj;

    invoke-static {p0}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object p0

    check-cast p0, LX/BiT;

    iput-object p0, p1, LX/IBj;->a:LX/BiT;

    return-void
.end method

.method public static d(LX/IBj;I)Landroid/widget/LinearLayout$LayoutParams;
    .locals 2

    .prologue
    .line 2547515
    invoke-virtual {p0}, LX/IBj;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    .line 2547516
    invoke-static {v1, p1}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 2547517
    iget-boolean v0, p0, LX/IBj;->i:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x30

    :goto_0
    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2547518
    return-object v1

    .line 2547519
    :cond_0
    const/16 v0, 0x10

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2547513
    invoke-virtual {p0}, LX/IBj;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    .prologue
    .line 2547514
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method
