.class public LX/JQx;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQw;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2692430
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JQx;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692485
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2692486
    iput-object p1, p0, LX/JQx;->b:LX/0Ot;

    .line 2692487
    return-void
.end method

.method public static a(LX/0QB;)LX/JQx;
    .locals 4

    .prologue
    .line 2692474
    const-class v1, LX/JQx;

    monitor-enter v1

    .line 2692475
    :try_start_0
    sget-object v0, LX/JQx;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692476
    sput-object v2, LX/JQx;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692477
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692478
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692479
    new-instance v3, LX/JQx;

    const/16 p0, 0x2018

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQx;-><init>(LX/0Ot;)V

    .line 2692480
    move-object v0, v3

    .line 2692481
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692482
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692483
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692484
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2692473
    const v0, -0x4250c229

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2692452
    check-cast p2, LX/JQv;

    .line 2692453
    iget-object v0, p0, LX/JQx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;

    iget v1, p2, LX/JQv;->a:I

    iget-object v2, p2, LX/JQv;->b:LX/JQk;

    iget-boolean v3, p2, LX/JQv;->c:Z

    .line 2692454
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    .line 2692455
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v6

    iget-object v4, v0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    .line 2692456
    iget-object p0, v2, LX/JQk;->a:Landroid/net/Uri;

    move-object p0, p0

    .line 2692457
    invoke-static {p0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object p2

    new-instance v0, LX/1o9;

    invoke-direct {v0, v1, v1}, LX/1o9;-><init>(II)V

    .line 2692458
    iput-object v0, p2, LX/1bX;->c:LX/1o9;

    .line 2692459
    move-object p2, p2

    .line 2692460
    invoke-static {}, LX/1bd;->a()LX/1bd;

    move-result-object v0

    .line 2692461
    iput-object v0, p2, LX/1bX;->d:LX/1bd;

    .line 2692462
    move-object p2, p2

    .line 2692463
    invoke-virtual {p2}, LX/1bX;->n()LX/1bf;

    move-result-object p2

    move-object p0, p2

    .line 2692464
    invoke-virtual {v4, p0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    sget-object p0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    sget-object v6, LX/1Up;->g:LX/1Up;

    invoke-virtual {v4, v6}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v4

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v1}, LX/1Di;->g(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v1}, LX/1Di;->o(I)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2692465
    if-eqz v3, :cond_0

    .line 2692466
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v6, 0x7f020052

    invoke-virtual {v4, v6}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v6, 0x1

    invoke-interface {v4, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    const/4 v6, 0x2

    const p0, 0x7f0b25ad

    invoke-interface {v4, v6, p0}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    const/4 v6, 0x3

    const p0, 0x7f0b25ad

    invoke-interface {v4, v6, p0}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2692467
    :cond_0
    iget-object v4, v2, LX/JQk;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2692468
    invoke-interface {v5, v4}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    .line 2692469
    const v4, -0x4250c229

    const/4 v6, 0x0

    invoke-static {p1, v4, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2692470
    invoke-interface {v5, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    .line 2692471
    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2692472
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2692431
    invoke-static {}, LX/1dS;->b()V

    .line 2692432
    iget v0, p1, LX/1dQ;->b:I

    .line 2692433
    packed-switch v0, :pswitch_data_0

    .line 2692434
    :goto_0
    return-object v2

    .line 2692435
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2692436
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2692437
    check-cast v1, LX/JQv;

    .line 2692438
    iget-object v3, p0, LX/JQx;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;

    iget-object v4, v1, LX/JQv;->b:LX/JQk;

    iget-object p1, v1, LX/JQv;->d:Ljava/lang/String;

    .line 2692439
    iget-object p2, v3, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;->c:LX/JRB;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    .line 2692440
    iget-object v1, v4, LX/JQk;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2692441
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "messenger_active_now_feed_unit_user_photo_tap"

    invoke-direct {v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2692442
    const-string v3, "tracking"

    invoke-virtual {v0, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2692443
    const-string v3, "installed_state"

    iget-object v4, p2, LX/JRB;->b:LX/23i;

    invoke-virtual {v4}, LX/23i;->f()Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2692444
    const-string v3, "tapped_userid"

    invoke-virtual {v0, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2692445
    const-string v4, "presence_enabled"

    iget-object v3, p2, LX/JRB;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3CA;

    invoke-virtual {v3}, LX/3CA;->shouldShowPresence()Z

    move-result v3

    invoke-virtual {v0, v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2692446
    const-string v3, "messenger_feed_units"

    .line 2692447
    iput-object v3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2692448
    iget-object v3, p2, LX/JRB;->c:LX/0Zb;

    invoke-interface {v3, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2692449
    sget-object v3, LX/0ax;->am:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2692450
    iget-object v0, p2, LX/JRB;->a:LX/17W;

    invoke-virtual {v0, p0, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2692451
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x4250c229
        :pswitch_0
    .end packed-switch
.end method
