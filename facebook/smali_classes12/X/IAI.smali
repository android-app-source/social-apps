.class public LX/IAI;
.super Landroid/text/style/ImageSpan;
.source ""


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .prologue
    .line 2545294
    invoke-direct {p0, p1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 2545295
    iput p2, p0, LX/IAI;->a:I

    .line 2545296
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 3

    .prologue
    .line 2545297
    invoke-virtual {p0}, LX/IAI;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2545298
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2545299
    iget v1, p0, LX/IAI;->a:I

    add-int/2addr v1, p7

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, p5, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2545300
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2545301
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2545302
    return-void
.end method
