.class public LX/Hl3;
.super LX/Hl2;
.source ""


# instance fields
.field public final a:LX/Hje;

.field public b:LX/HjZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Hje;I)V
    .locals 5

    const/4 v2, 0x0

    invoke-direct {p0, p1}, LX/Hl2;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, LX/Hl3;->a:LX/Hje;

    new-instance v0, LX/Hl0;

    invoke-direct {v0, p0}, LX/Hl0;-><init>(LX/Hl3;)V

    invoke-virtual {p0, v0}, LX/Hl3;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    invoke-virtual {p0}, LX/Hl3;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {p0}, LX/Hl3;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v1, v3, :cond_0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    :goto_0
    invoke-virtual {p0, v2}, LX/Hl3;->setHorizontalScrollBarEnabled(Z)V

    invoke-virtual {p0, v2}, LX/Hl3;->setHorizontalScrollbarOverlay(Z)V

    invoke-virtual {p0, v2}, LX/Hl3;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {p0, v2}, LX/Hl3;->setVerticalScrollbarOverlay(Z)V

    new-instance v0, LX/Hl1;

    invoke-direct {v0, p0}, LX/Hl1;-><init>(LX/Hl3;)V

    const-string v1, "AdControl"

    invoke-virtual {p0, v0, v1}, LX/Hl3;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, LX/HjZ;

    invoke-virtual {p0}, LX/Hl3;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/Hkz;

    invoke-direct {v2, p0, p2}, LX/Hkz;-><init>(LX/Hl3;LX/Hje;)V

    invoke-direct {v0, v1, p0, p3, v2}, LX/HjZ;-><init>(Landroid/content/Context;Landroid/view/View;ILX/Hj7;)V

    iput-object v0, p0, LX/Hl3;->b:LX/HjZ;

    return-void

    :cond_0
    :try_start_0
    const-class v1, Landroid/webkit/WebSettings;

    const-string v3, "setMixedContentMode"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v4

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final destroy()V
    .locals 1

    iget-object v0, p0, LX/Hl3;->b:LX/HjZ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hl3;->b:LX/HjZ;

    invoke-virtual {v0}, LX/HjZ;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, LX/Hl3;->b:LX/HjZ;

    :cond_0
    invoke-static {p0}, LX/Hkp;->a(Landroid/webkit/WebView;)V

    invoke-super {p0}, LX/Hl2;->destroy()V

    return-void
.end method

.method public final onWindowVisibilityChanged(I)V
    .locals 3

    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x7811bd53

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    invoke-super {p0, p1}, LX/Hl2;->onWindowVisibilityChanged(I)V

    iget-object v1, p0, LX/Hl3;->a:LX/Hje;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Hl3;->a:LX/Hje;

    invoke-virtual {v1, p1}, LX/Hje;->a(I)V

    :cond_0
    if-nez p1, :cond_2

    iget-object v1, p0, LX/Hl3;->b:LX/HjZ;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Hl3;->b:LX/HjZ;

    invoke-virtual {v1}, LX/HjZ;->a()V

    :cond_1
    :goto_0
    const v1, 0x528a436d

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void

    :cond_2
    const/16 v1, 0x8

    if-ne p1, v1, :cond_1

    iget-object v1, p0, LX/Hl3;->b:LX/HjZ;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Hl3;->b:LX/HjZ;

    invoke-virtual {v1}, LX/HjZ;->b()V

    goto :goto_0
.end method
