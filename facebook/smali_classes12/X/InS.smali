.class public LX/InS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610683
    iput-object p1, p0, LX/InS;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2610684
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;)Z
    .locals 2

    .prologue
    .line 2610681
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUIRE_VERIFICATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UNDER_MANUAL_REVIEW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/InS;
    .locals 2

    .prologue
    .line 2610679
    new-instance v1, LX/InS;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v1, v0}, LX/InS;-><init>(Lcom/facebook/content/SecureContextHelper;)V

    .line 2610680
    return-object v1
.end method
