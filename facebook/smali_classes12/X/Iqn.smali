.class public final enum LX/Iqn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iqn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iqn;

.field public static final enum ASSET_LOADED:LX/Iqn;

.field public static final enum DYNAMIC_COLOR_CHANGED:LX/Iqn;

.field public static final enum FLIP:LX/Iqn;

.field public static final enum ROTATE:LX/Iqn;

.field public static final enum SCALE:LX/Iqn;

.field public static final enum TRANSLATE:LX/Iqn;

.field public static final enum VISIBILITY_CHANGED:LX/Iqn;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2617260
    new-instance v0, LX/Iqn;

    const-string v1, "TRANSLATE"

    invoke-direct {v0, v1, v3}, LX/Iqn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqn;->TRANSLATE:LX/Iqn;

    .line 2617261
    new-instance v0, LX/Iqn;

    const-string v1, "SCALE"

    invoke-direct {v0, v1, v4}, LX/Iqn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqn;->SCALE:LX/Iqn;

    .line 2617262
    new-instance v0, LX/Iqn;

    const-string v1, "ROTATE"

    invoke-direct {v0, v1, v5}, LX/Iqn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqn;->ROTATE:LX/Iqn;

    .line 2617263
    new-instance v0, LX/Iqn;

    const-string v1, "FLIP"

    invoke-direct {v0, v1, v6}, LX/Iqn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqn;->FLIP:LX/Iqn;

    .line 2617264
    new-instance v0, LX/Iqn;

    const-string v1, "VISIBILITY_CHANGED"

    invoke-direct {v0, v1, v7}, LX/Iqn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqn;->VISIBILITY_CHANGED:LX/Iqn;

    .line 2617265
    new-instance v0, LX/Iqn;

    const-string v1, "ASSET_LOADED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Iqn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqn;->ASSET_LOADED:LX/Iqn;

    .line 2617266
    new-instance v0, LX/Iqn;

    const-string v1, "DYNAMIC_COLOR_CHANGED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Iqn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqn;->DYNAMIC_COLOR_CHANGED:LX/Iqn;

    .line 2617267
    const/4 v0, 0x7

    new-array v0, v0, [LX/Iqn;

    sget-object v1, LX/Iqn;->TRANSLATE:LX/Iqn;

    aput-object v1, v0, v3

    sget-object v1, LX/Iqn;->SCALE:LX/Iqn;

    aput-object v1, v0, v4

    sget-object v1, LX/Iqn;->ROTATE:LX/Iqn;

    aput-object v1, v0, v5

    sget-object v1, LX/Iqn;->FLIP:LX/Iqn;

    aput-object v1, v0, v6

    sget-object v1, LX/Iqn;->VISIBILITY_CHANGED:LX/Iqn;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Iqn;->ASSET_LOADED:LX/Iqn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Iqn;->DYNAMIC_COLOR_CHANGED:LX/Iqn;

    aput-object v2, v0, v1

    sput-object v0, LX/Iqn;->$VALUES:[LX/Iqn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2617268
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iqn;
    .locals 1

    .prologue
    .line 2617269
    const-class v0, LX/Iqn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iqn;

    return-object v0
.end method

.method public static values()[LX/Iqn;
    .locals 1

    .prologue
    .line 2617270
    sget-object v0, LX/Iqn;->$VALUES:[LX/Iqn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iqn;

    return-object v0
.end method
