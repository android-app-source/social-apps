.class public final LX/IgV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;)V
    .locals 0

    .prologue
    .line 2601204
    iput-object p1, p0, LX/IgV;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;B)V
    .locals 0

    .prologue
    .line 2601203
    invoke-direct {p0, p1}, LX/IgV;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2601197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2601198
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2601199
    const-string v2, "extra_media_items"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2601200
    iget-object v0, p0, LX/IgV;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2601201
    iget-object v0, p0, LX/IgV;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;

    invoke-virtual {v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->finish()V

    .line 2601202
    return-void
.end method
