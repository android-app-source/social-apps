.class public LX/Iux;
.super LX/3pF;
.source ""


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/neko/util/AppCardFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:LX/Iv7;

.field private final c:LX/Iuu;

.field private final d:LX/Iuu;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HjF;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field private g:Z

.field public h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/0gc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2627471
    invoke-direct {p0, p1}, LX/3pF;-><init>(LX/0gc;)V

    .line 2627472
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Iux;->a:Ljava/util/HashMap;

    .line 2627473
    new-instance v0, LX/Iuv;

    invoke-direct {v0, p0}, LX/Iuv;-><init>(LX/Iux;)V

    iput-object v0, p0, LX/Iux;->c:LX/Iuu;

    .line 2627474
    new-instance v0, LX/Iuw;

    invoke-direct {v0, p0}, LX/Iuw;-><init>(LX/Iux;)V

    iput-object v0, p0, LX/Iux;->d:LX/Iuu;

    .line 2627475
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Iux;->e:Ljava/util/List;

    .line 2627476
    iput v1, p0, LX/Iux;->f:I

    .line 2627477
    sget-object v0, LX/Iv7;->Loading:LX/Iv7;

    iput-object v0, p0, LX/Iux;->b:LX/Iv7;

    .line 2627478
    iput-boolean v1, p0, LX/Iux;->g:Z

    .line 2627479
    return-void
.end method

.method public static a$redex0(LX/Iux;LX/Iv6;ZI)V
    .locals 2

    .prologue
    .line 2627507
    iget-object v0, p0, LX/Iux;->b:LX/Iv7;

    sget-object v1, LX/Iv7;->Loading:LX/Iv7;

    if-ne v0, v1, :cond_1

    .line 2627508
    sget-object v0, LX/Iv7;->Loading:LX/Iv7;

    iget-boolean v1, p0, LX/Iux;->g:Z

    invoke-virtual {p1, v0, p2, v1}, LX/Iur;->a(LX/Iv7;ZZ)V

    .line 2627509
    :cond_0
    :goto_0
    return-void

    .line 2627510
    :cond_1
    iget-object v0, p0, LX/Iux;->b:LX/Iv7;

    sget-object v1, LX/Iv7;->Error:LX/Iv7;

    if-ne v0, v1, :cond_0

    .line 2627511
    if-nez p3, :cond_2

    .line 2627512
    sget-object v0, LX/Iv7;->Error:LX/Iv7;

    iget-boolean v1, p0, LX/Iux;->g:Z

    invoke-virtual {p1, v0, p2, v1}, LX/Iur;->a(LX/Iv7;ZZ)V

    .line 2627513
    iget-object v0, p0, LX/Iux;->h:Landroid/view/View$OnClickListener;

    .line 2627514
    iget-object v1, p1, LX/Iv6;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2627515
    goto :goto_0

    .line 2627516
    :cond_2
    sget-object v0, LX/Iv7;->Loading:LX/Iv7;

    iget-boolean v1, p0, LX/Iux;->g:Z

    invoke-virtual {p1, v0, p2, v1}, LX/Iur;->a(LX/Iv7;ZZ)V

    .line 2627517
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/Iv6;->setShowLoading(Z)V

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 2627518
    iget-object v0, p0, LX/Iux;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2627519
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/neko/util/AppCardFragment;

    .line 2627520
    if-eqz v1, :cond_0

    .line 2627521
    iget-object v3, v1, Lcom/facebook/neko/util/AppCardFragment;->a:LX/Iur;

    move-object v3, v3

    .line 2627522
    instance-of v3, v3, LX/Iv6;

    if-eqz v3, :cond_0

    .line 2627523
    iget-object v3, v1, Lcom/facebook/neko/util/AppCardFragment;->a:LX/Iur;

    move-object v1, v3

    .line 2627524
    check-cast v1, LX/Iv6;

    const/4 v3, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v1, v3, v0}, LX/Iux;->a$redex0(LX/Iux;LX/Iv6;ZI)V

    goto :goto_0

    .line 2627525
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2627537
    const/4 v0, -0x2

    return v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2627526
    new-instance v1, Lcom/facebook/neko/util/AppCardFragment;

    invoke-direct {v1}, Lcom/facebook/neko/util/AppCardFragment;-><init>()V

    .line 2627527
    iput p1, v1, Lcom/facebook/neko/util/AppCardFragment;->d:I

    .line 2627528
    iget-object v0, p0, LX/Iux;->b:LX/Iv7;

    sget-object v2, LX/Iv7;->Apps:LX/Iv7;

    if-eq v0, v2, :cond_0

    .line 2627529
    iget-object v0, p0, LX/Iux;->c:LX/Iuu;

    .line 2627530
    iput-object v0, v1, Lcom/facebook/neko/util/AppCardFragment;->c:LX/Iuu;

    .line 2627531
    :goto_0
    return-object v1

    .line 2627532
    :cond_0
    iget-object v0, p0, LX/Iux;->d:LX/Iuu;

    .line 2627533
    iput-object v0, v1, Lcom/facebook/neko/util/AppCardFragment;->c:LX/Iuu;

    .line 2627534
    iget-object v0, p0, LX/Iux;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HjF;

    .line 2627535
    iput-object v0, v1, Lcom/facebook/neko/util/AppCardFragment;->b:LX/HjF;

    .line 2627536
    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2627503
    invoke-super {p0, p1, p2}, LX/3pF;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/neko/util/AppCardFragment;

    .line 2627504
    iget-object v1, p0, LX/Iux;->a:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2627505
    return-object v0
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 2627506
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2627498
    iget-object v0, p0, LX/Iux;->a:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2627499
    iget-object v0, p0, LX/Iux;->b:LX/Iv7;

    sget-object v1, LX/Iv7;->Apps:LX/Iv7;

    if-ne v0, v1, :cond_0

    .line 2627500
    iget-object v0, p0, LX/Iux;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HjF;

    invoke-virtual {v0}, LX/HjF;->l()V

    .line 2627501
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/3pF;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2627502
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2627490
    iget-object v0, p0, LX/Iux;->b:LX/Iv7;

    .line 2627491
    sget-object v1, LX/Iv7;->Error:LX/Iv7;

    iput-object v1, p0, LX/Iux;->b:LX/Iv7;

    .line 2627492
    iput-boolean p1, p0, LX/Iux;->g:Z

    .line 2627493
    sget-object v1, LX/Iv7;->Loading:LX/Iv7;

    if-ne v0, v1, :cond_0

    .line 2627494
    invoke-direct {p0}, LX/Iux;->e()V

    .line 2627495
    :goto_0
    return-void

    .line 2627496
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, LX/Iux;->f:I

    .line 2627497
    invoke-virtual {p0}, LX/0gG;->kV_()V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2627489
    iget v0, p0, LX/Iux;->f:I

    return v0
.end method

.method public final d(I)F
    .locals 1

    .prologue
    .line 2627488
    const v0, 0x3f4ccccd    # 0.8f

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2627481
    iget-object v0, p0, LX/Iux;->b:LX/Iv7;

    .line 2627482
    sget-object v1, LX/Iv7;->Loading:LX/Iv7;

    iput-object v1, p0, LX/Iux;->b:LX/Iv7;

    .line 2627483
    sget-object v1, LX/Iv7;->Error:LX/Iv7;

    if-ne v0, v1, :cond_0

    .line 2627484
    invoke-direct {p0}, LX/Iux;->e()V

    .line 2627485
    :goto_0
    return-void

    .line 2627486
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, LX/Iux;->f:I

    .line 2627487
    invoke-virtual {p0}, LX/0gG;->kV_()V

    goto :goto_0
.end method

.method public final lf_()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 2627480
    const/4 v0, 0x0

    return-object v0
.end method
