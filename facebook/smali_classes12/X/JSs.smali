.class public LX/JSs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/animation/Animation$AnimationListener;

.field public final b:Landroid/view/animation/AnimationSet;

.field public final c:LX/JSP;

.field public d:LX/JSt;

.field public e:LX/JSw;

.field public f:LX/JSu;

.field public g:Z


# direct methods
.method public constructor <init>(LX/JSP;)V
    .locals 8

    .prologue
    .line 2696106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696107
    new-instance v0, LX/JSr;

    invoke-direct {v0, p0}, LX/JSr;-><init>(LX/JSs;)V

    iput-object v0, p0, LX/JSs;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 2696108
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, LX/JSs;->b:Landroid/view/animation/AnimationSet;

    .line 2696109
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSP;

    iput-object v0, p0, LX/JSs;->c:LX/JSP;

    .line 2696110
    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 2696111
    new-instance v2, LX/JSt;

    iget-object v3, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {v3}, LX/JSP;->getProgressView()Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    move-result-object v3

    invoke-direct {v2, v3}, LX/JSt;-><init>(Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;)V

    iput-object v2, p0, LX/JSs;->d:LX/JSt;

    .line 2696112
    new-instance v2, LX/JSw;

    iget-object v3, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {v3}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object v3

    sget-object v4, LX/JSv;->SQUARE_TO_CIRCLE:LX/JSv;

    invoke-direct {v2, v3, v4}, LX/JSw;-><init>(Lcom/facebook/feedplugins/musicstory/animations/VinylView;LX/JSv;)V

    .line 2696113
    const-wide/16 v4, 0x15e

    invoke-virtual {v2, v4, v5}, LX/JSw;->setDuration(J)V

    .line 2696114
    iget-object v3, p0, LX/JSs;->b:Landroid/view/animation/AnimationSet;

    invoke-virtual {v3, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2696115
    new-instance v2, LX/JSu;

    iget-object v3, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {v3}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object v3

    invoke-direct {v2, v3}, LX/JSu;-><init>(Lcom/facebook/feedplugins/musicstory/animations/VinylView;)V

    iput-object v2, p0, LX/JSs;->f:LX/JSu;

    .line 2696116
    iget-object v2, p0, LX/JSs;->f:LX/JSu;

    invoke-virtual {v2, v7}, LX/JSu;->setFillEnabled(Z)V

    .line 2696117
    iget-object v2, p0, LX/JSs;->f:LX/JSu;

    invoke-virtual {v2, v7}, LX/JSu;->setFillAfter(Z)V

    .line 2696118
    iget-object v2, p0, LX/JSs;->f:LX/JSu;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, LX/JSu;->setStartOffset(J)V

    .line 2696119
    iget-object v2, p0, LX/JSs;->f:LX/JSu;

    invoke-virtual {v2, v6}, LX/JSu;->setRepeatCount(I)V

    .line 2696120
    iget-object v2, p0, LX/JSs;->f:LX/JSu;

    invoke-virtual {v2, v6}, LX/JSu;->setRepeatMode(I)V

    .line 2696121
    iget-object v2, p0, LX/JSs;->b:Landroid/view/animation/AnimationSet;

    iget-object v3, p0, LX/JSs;->f:LX/JSu;

    invoke-virtual {v2, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2696122
    new-instance v2, LX/JSw;

    iget-object v3, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {v3}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object v3

    sget-object v4, LX/JSv;->CIRCLE_TO_SQUARE:LX/JSv;

    invoke-direct {v2, v3, v4}, LX/JSw;-><init>(Lcom/facebook/feedplugins/musicstory/animations/VinylView;LX/JSv;)V

    iput-object v2, p0, LX/JSs;->e:LX/JSw;

    .line 2696123
    iget-object v2, p0, LX/JSs;->e:LX/JSw;

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v4, v5}, LX/JSw;->setDuration(J)V

    .line 2696124
    iget-object v2, p0, LX/JSs;->e:LX/JSw;

    iget-object v3, p0, LX/JSs;->a:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v3}, LX/JSw;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2696125
    return-void
.end method

.method public static e(LX/JSs;)V
    .locals 2

    .prologue
    .line 2696126
    iget-object v0, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {v0}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2696127
    :goto_0
    return-void

    .line 2696128
    :cond_0
    iget-object v0, p0, LX/JSs;->e:LX/JSw;

    invoke-virtual {v0}, LX/JSw;->reset()V

    .line 2696129
    iget-object v0, p0, LX/JSs;->f:LX/JSu;

    .line 2696130
    const/4 v1, 0x0

    iput v1, v0, LX/JSu;->d:F

    .line 2696131
    iget-object v0, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {v0}, LX/JSP;->getPlayButtonDrawable()Landroid/graphics/drawable/TransitionDrawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 2696132
    iget-object v0, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {v0}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->clearAnimation()V

    .line 2696133
    iget-object v0, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {v0}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object v0

    iget-object v1, p0, LX/JSs;->e:LX/JSw;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 2696134
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JSs;->g:Z

    .line 2696135
    iget-object v0, p0, LX/JSs;->d:LX/JSt;

    invoke-virtual {v0}, LX/JSt;->b()V

    .line 2696136
    invoke-static {p0}, LX/JSs;->e(LX/JSs;)V

    .line 2696137
    return-void
.end method
