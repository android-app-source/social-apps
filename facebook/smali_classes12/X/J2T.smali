.class public final LX/J2T;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2641401
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2641402
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2641403
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2641404
    invoke-static {p0, p1}, LX/J2T;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2641405
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2641406
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2641407
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2641408
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2641409
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/J2T;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2641410
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2641411
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2641412
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2641413
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2641414
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2641415
    :goto_0
    return v1

    .line 2641416
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2641417
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2641418
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2641419
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2641420
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2641421
    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2641422
    invoke-static {p0, p1}, LX/J2S;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2641423
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2641424
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2641425
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2641426
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2641427
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2641428
    if-eqz v0, :cond_0

    .line 2641429
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641430
    invoke-static {p0, v0, p2}, LX/J2S;->a(LX/15i;ILX/0nX;)V

    .line 2641431
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2641432
    return-void
.end method
