.class public LX/Hla;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/instagram/common/json/annotation/JsonType;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/HlX;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/Double;

.field public f:Ljava/lang/Float;

.field public g:Ljava/lang/Float;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Integer;

.field public k:LX/HlY;

.field public l:LX/HlV;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2498752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498753
    iput-object v0, p0, LX/Hla;->a:Ljava/lang/String;

    .line 2498754
    iput-object v0, p0, LX/Hla;->b:LX/HlX;

    .line 2498755
    iput-object v0, p0, LX/Hla;->c:Ljava/lang/Long;

    .line 2498756
    iput-object v0, p0, LX/Hla;->d:Ljava/lang/Float;

    .line 2498757
    iput-object v0, p0, LX/Hla;->e:Ljava/lang/Double;

    .line 2498758
    iput-object v0, p0, LX/Hla;->f:Ljava/lang/Float;

    .line 2498759
    iput-object v0, p0, LX/Hla;->g:Ljava/lang/Float;

    .line 2498760
    iput-object v0, p0, LX/Hla;->h:Ljava/lang/Long;

    .line 2498761
    iput-object v0, p0, LX/Hla;->i:Ljava/lang/Long;

    .line 2498762
    iput-object v0, p0, LX/Hla;->j:Ljava/lang/Integer;

    .line 2498763
    iput-object v0, p0, LX/Hla;->k:LX/HlY;

    .line 2498764
    iput-object v0, p0, LX/Hla;->l:LX/HlV;

    .line 2498765
    return-void
.end method

.method public constructor <init>(LX/HlW;)V
    .locals 1

    .prologue
    .line 2498766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498767
    iget-object v0, p1, LX/HlW;->a:Ljava/lang/String;

    iput-object v0, p0, LX/Hla;->a:Ljava/lang/String;

    .line 2498768
    iget-object v0, p1, LX/HlW;->b:LX/HlX;

    iput-object v0, p0, LX/Hla;->b:LX/HlX;

    .line 2498769
    iget-object v0, p1, LX/HlW;->c:Ljava/lang/Long;

    iput-object v0, p0, LX/Hla;->c:Ljava/lang/Long;

    .line 2498770
    iget-object v0, p1, LX/HlW;->d:Ljava/lang/Float;

    iput-object v0, p0, LX/Hla;->d:Ljava/lang/Float;

    .line 2498771
    iget-object v0, p1, LX/HlW;->e:Ljava/lang/Double;

    iput-object v0, p0, LX/Hla;->e:Ljava/lang/Double;

    .line 2498772
    iget-object v0, p1, LX/HlW;->f:Ljava/lang/Float;

    iput-object v0, p0, LX/Hla;->f:Ljava/lang/Float;

    .line 2498773
    iget-object v0, p1, LX/HlW;->g:Ljava/lang/Float;

    iput-object v0, p0, LX/Hla;->g:Ljava/lang/Float;

    .line 2498774
    iget-object v0, p1, LX/HlW;->h:Ljava/lang/Long;

    iput-object v0, p0, LX/Hla;->h:Ljava/lang/Long;

    .line 2498775
    iget-object v0, p1, LX/HlW;->i:Ljava/lang/Long;

    iput-object v0, p0, LX/Hla;->i:Ljava/lang/Long;

    .line 2498776
    iget-object v0, p1, LX/HlW;->j:Ljava/lang/Integer;

    iput-object v0, p0, LX/Hla;->j:Ljava/lang/Integer;

    .line 2498777
    iget-object v0, p1, LX/HlW;->k:LX/HlY;

    iput-object v0, p0, LX/Hla;->k:LX/HlY;

    .line 2498778
    iget-object v0, p1, LX/HlW;->l:LX/HlV;

    iput-object v0, p0, LX/Hla;->l:LX/HlV;

    .line 2498779
    return-void
.end method
