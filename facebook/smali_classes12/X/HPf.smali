.class public LX/HPf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field public final b:LX/0rq;

.field public final c:LX/0se;

.field public final d:LX/0hB;

.field public final e:LX/00H;

.field public final f:LX/7H7;

.field public final g:Landroid/content/res/Resources;

.field private final h:LX/0Sg;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/2U1;

.field public final k:LX/0sX;

.field public final l:LX/CK5;


# direct methods
.method public constructor <init>(LX/0tX;LX/0rq;LX/0se;LX/0hB;LX/00H;LX/7H7;Landroid/content/res/Resources;LX/0Sg;LX/0Ot;LX/2U1;LX/0sX;LX/CK5;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0rq;",
            "LX/0se;",
            "LX/0hB;",
            "LX/00H;",
            "LX/7H7;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/2U1;",
            "LX/0sX;",
            "LX/CK5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2462339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2462340
    iput-object p1, p0, LX/HPf;->a:LX/0tX;

    .line 2462341
    iput-object p2, p0, LX/HPf;->b:LX/0rq;

    .line 2462342
    iput-object p4, p0, LX/HPf;->d:LX/0hB;

    .line 2462343
    iput-object p3, p0, LX/HPf;->c:LX/0se;

    .line 2462344
    iput-object p5, p0, LX/HPf;->e:LX/00H;

    .line 2462345
    iput-object p6, p0, LX/HPf;->f:LX/7H7;

    .line 2462346
    iput-object p7, p0, LX/HPf;->g:Landroid/content/res/Resources;

    .line 2462347
    iput-object p8, p0, LX/HPf;->h:LX/0Sg;

    .line 2462348
    iput-object p9, p0, LX/HPf;->i:LX/0Ot;

    .line 2462349
    iput-object p10, p0, LX/HPf;->j:LX/2U1;

    .line 2462350
    iput-object p11, p0, LX/HPf;->k:LX/0sX;

    .line 2462351
    iput-object p12, p0, LX/HPf;->l:LX/CK5;

    .line 2462352
    return-void
.end method

.method public static b(LX/0QB;)LX/HPf;
    .locals 13

    .prologue
    .line 2462353
    new-instance v0, LX/HPf;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    const-class v5, LX/00H;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/00H;

    invoke-static {p0}, LX/7H7;->a(LX/0QB;)LX/7H7;

    move-result-object v6

    check-cast v6, LX/7H7;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v8

    check-cast v8, LX/0Sg;

    const/16 v9, 0xac0

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v10

    check-cast v10, LX/2U1;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v11

    check-cast v11, LX/0sX;

    invoke-static {p0}, LX/CK5;->a(LX/0QB;)LX/CK5;

    move-result-object v12

    check-cast v12, LX/CK5;

    invoke-direct/range {v0 .. v12}, LX/HPf;-><init>(LX/0tX;LX/0rq;LX/0se;LX/0hB;LX/00H;LX/7H7;Landroid/content/res/Resources;LX/0Sg;LX/0Ot;LX/2U1;LX/0sX;LX/CK5;)V

    .line 2462354
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Long;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "LX/0zS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2462355
    iget-object v0, p0, LX/HPf;->a:LX/0tX;

    invoke-virtual {p0, p1, p2}, LX/HPf;->b(Ljava/lang/Long;LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2462356
    iget-object v1, p0, LX/HPf;->h:LX/0Sg;

    invoke-virtual {v1, v0}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2462357
    return-object v0
.end method

.method public final b(Ljava/lang/Long;LX/0zS;)LX/0zO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "LX/0zS;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2462358
    new-instance v0, LX/9Yq;

    invoke-direct {v0}, LX/9Yq;-><init>()V

    move-object v0, v0

    .line 2462359
    const-string v1, "page_id"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2462360
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2462361
    const-string v1, "profile_image_size"

    iget-object v4, p0, LX/HPf;->g:Landroid/content/res/Resources;

    const v5, 0x7f0b0e91

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "context_items_row_limit"

    iget-object v5, p0, LX/HPf;->g:Landroid/content/res/Resources;

    const p1, 0x7f0c003f

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "context_item_icon_size"

    iget-object v5, p0, LX/HPf;->g:Landroid/content/res/Resources;

    const p1, 0x7f0b0d14

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v4, "context_items_source"

    const-string v5, "newsfeed"

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "context_items_source_id"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "profile_pic_as_cover_size"

    iget-object v5, p0, LX/HPf;->d:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->c()I

    move-result v5

    iget-object p1, p0, LX/HPf;->d:LX/0hB;

    invoke-virtual {p1}, LX/0hB;->d()I

    move-result p1

    invoke-static {v5, p1}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "is_pma"

    iget-object v1, p0, LX/HPf;->e:LX/00H;

    .line 2462362
    iget-object p1, v1, LX/00H;->j:LX/01T;

    move-object v1, p1

    .line 2462363
    sget-object p1, LX/01T;->PAA:LX/01T;

    if-ne v1, p1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "cover_image_high_res_size"

    iget-object v5, p0, LX/HPf;->f:LX/7H7;

    invoke-virtual {v5}, LX/7H7;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "should_redirect"

    iget-object v1, p0, LX/HPf;->e:LX/00H;

    .line 2462364
    iget-object p1, v1, LX/00H;->j:LX/01T;

    move-object v1, p1

    .line 2462365
    sget-object p1, LX/01T;->FB4A:LX/01T;

    if-ne v1, p1, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "is_ride_share_enabled"

    iget-object v5, p0, LX/HPf;->l:LX/CK5;

    invoke-virtual {v5}, LX/CK5;->a()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v4

    const-string v5, "should_fetch_page_username"

    iget-object v1, p0, LX/HPf;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    sget p1, LX/FQe;->f:I

    invoke-virtual {v1, p1, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "automatic_photo_captioning_enabled"

    iget-object v4, p0, LX/HPf;->k:LX/0sX;

    invoke-virtual {v4}, LX/0sX;->a()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "cta_icon_size"

    iget-object v4, p0, LX/HPf;->g:Landroid/content/res/Resources;

    const v5, 0x7f0b0f8e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v3, "cta_icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2462366
    iget-object v1, p0, LX/HPf;->c:LX/0se;

    iget-object v3, p0, LX/HPf;->b:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->c()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2462367
    iput-boolean v2, v0, LX/0gW;->l:Z

    .line 2462368
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/32 v2, 0x24ea00

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    const-string v2, "PageHeaderTag"

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 2462369
    iput-object v2, v1, LX/0zO;->d:Ljava/util/Set;

    .line 2462370
    move-object v1, v1

    .line 2462371
    new-instance v2, LX/HPe;

    invoke-direct {v2, v0}, LX/HPe;-><init>(LX/9Yq;)V

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zT;)LX/0zO;

    move-result-object v0

    return-object v0

    :cond_0
    move v1, v3

    .line 2462372
    goto/16 :goto_0

    :cond_1
    move v1, v3

    goto/16 :goto_1
.end method
