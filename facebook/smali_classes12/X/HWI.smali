.class public LX/HWI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/9XE;


# direct methods
.method public constructor <init>(LX/9XE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475781
    iput-object p1, p0, LX/HWI;->a:LX/9XE;

    .line 2475782
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 9

    .prologue
    .line 2475783
    const-string v0, "page_video_list_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2475784
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2475785
    iget-object v1, p0, LX/HWI;->a:LX/9XE;

    const-string v4, "pages_navigation_source"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2475786
    iget-object v5, v1, LX/9XE;->a:LX/0Zb;

    sget-object p0, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_PLAYLIST_HEADER:LX/9XI;

    invoke-static {p0, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "playlist_id"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "navigation_source"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v5, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2475787
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2475788
    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2475789
    new-instance v5, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-direct {v5}, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;-><init>()V

    .line 2475790
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2475791
    const-string v7, "video_list_id"

    invoke-virtual {v6, v7, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475792
    const-string v7, "page_id"

    invoke-virtual {v6, v7, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475793
    invoke-virtual {v5, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475794
    move-object v0, v5

    .line 2475795
    return-object v0

    .line 2475796
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method
