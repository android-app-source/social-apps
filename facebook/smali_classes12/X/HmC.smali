.class public final LX/HmC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2500344
    iput-object p1, p0, LX/HmC;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iput-object p2, p0, LX/HmC;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2500346
    iget-object v0, p0, LX/HmC;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Hmf;->b(Z)V

    .line 2500347
    iget-object v0, p0, LX/HmC;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-static {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->b(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    .line 2500348
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2500349
    iget-object v0, p0, LX/HmC;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-virtual {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2500350
    :goto_0
    return-void

    .line 2500351
    :cond_0
    iget-object v0, p0, LX/HmC;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    invoke-virtual {v0, v6}, LX/Hmf;->b(Z)V

    .line 2500352
    iget-object v0, p0, LX/HmC;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    iget-object v1, p0, LX/HmC;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f083951

    invoke-virtual {v1, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HmC;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v3, 0x7f083953

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, LX/HmC;->a:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v7}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2500353
    const-string v0, "BeamReceiver"

    const-string v1, "Unexpected error connecting to hotspot"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2500345
    invoke-direct {p0}, LX/HmC;->a()V

    return-void
.end method
