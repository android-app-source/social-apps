.class public final enum LX/Hov;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hov;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hov;

.field public static final enum CLOSED:LX/Hov;

.field public static final enum OPEN:LX/Hov;

.field public static final enum RECORDING:LX/Hov;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2507102
    new-instance v0, LX/Hov;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v2}, LX/Hov;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hov;->CLOSED:LX/Hov;

    .line 2507103
    new-instance v0, LX/Hov;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v3}, LX/Hov;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hov;->OPEN:LX/Hov;

    .line 2507104
    new-instance v0, LX/Hov;

    const-string v1, "RECORDING"

    invoke-direct {v0, v1, v4}, LX/Hov;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hov;->RECORDING:LX/Hov;

    .line 2507105
    const/4 v0, 0x3

    new-array v0, v0, [LX/Hov;

    sget-object v1, LX/Hov;->CLOSED:LX/Hov;

    aput-object v1, v0, v2

    sget-object v1, LX/Hov;->OPEN:LX/Hov;

    aput-object v1, v0, v3

    sget-object v1, LX/Hov;->RECORDING:LX/Hov;

    aput-object v1, v0, v4

    sput-object v0, LX/Hov;->$VALUES:[LX/Hov;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2507106
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hov;
    .locals 1

    .prologue
    .line 2507101
    const-class v0, LX/Hov;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hov;

    return-object v0
.end method

.method public static values()[LX/Hov;
    .locals 1

    .prologue
    .line 2507100
    sget-object v0, LX/Hov;->$VALUES:[LX/Hov;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hov;

    return-object v0
.end method
