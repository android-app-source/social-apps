.class public final LX/Hn7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;)V
    .locals 0

    .prologue
    .line 2501354
    iput-object p1, p0, LX/Hn7;->a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x35b7a11f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2501342
    iget-object v1, p0, LX/Hn7;->a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->p:LX/HnK;

    iget-object v2, p0, LX/Hn7;->a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    iget-object v2, v2, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->r:LX/Hmj;

    invoke-virtual {v2}, LX/Hmj;->f()Z

    move-result v2

    iget-object v3, p0, LX/Hn7;->a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    iget-object v3, v3, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->q:LX/14G;

    invoke-virtual {v3}, LX/14G;->b()LX/0am;

    move-result-object v3

    .line 2501343
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string v6, "forceMobileDataOffBeamConfig"

    invoke-virtual {v5, v6, v2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v5

    const-string v6, "isMobileDataEnabledPresent"

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result p1

    invoke-virtual {v5, v6, p1}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v6

    .line 2501344
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2501345
    const-string p1, "isMobileDataEnabled"

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v6, p1, v5}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    .line 2501346
    :cond_0
    sget-object v5, LX/HnJ;->MOBILE_DATA_WARNING_SCREEN_CLICKED:LX/HnJ;

    invoke-static {v1, v5, v6}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501347
    iget-object v1, p0, LX/Hn7;->a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    invoke-virtual {v1}, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2501348
    iget-object v1, p0, LX/Hn7;->a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    invoke-virtual {v1}, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->b()V

    .line 2501349
    const v1, -0x56ee878d

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2501350
    :goto_0
    return-void

    .line 2501351
    :cond_1
    iget-object v1, p0, LX/Hn7;->a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->setResult(ILandroid/content/Intent;)V

    .line 2501352
    iget-object v1, p0, LX/Hn7;->a:Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    invoke-virtual {v1}, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->finish()V

    .line 2501353
    const v1, -0x7544e40

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
