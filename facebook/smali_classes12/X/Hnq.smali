.class public final LX/Hnq;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2503857
    const-class v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    const v0, -0x43c5a1ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "ElectionHubVideoStoriesQuery"

    const-string v6, "3a6f127b0e7b8718bae77a9c40383ad6"

    const-string v7, "election_hub"

    const-string v8, "10155261854591729"

    const/4 v9, 0x0

    .line 2503858
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2503859
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2503860
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2503831
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2503832
    sparse-switch v0, :sswitch_data_0

    .line 2503833
    :goto_0
    return-object p1

    .line 2503834
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2503835
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2503836
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2503837
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2503838
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2503839
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2503840
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2503841
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2503842
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2503843
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2503844
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2503845
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2503846
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2503847
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2503848
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2503849
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2503850
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2503851
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2503852
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 2503853
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 2503854
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 2503855
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 2503856
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_12
        -0x680de62a -> :sswitch_9
        -0x6326fdb3 -> :sswitch_8
        -0x57984ae8 -> :sswitch_16
        -0x5305c081 -> :sswitch_1
        -0x513764de -> :sswitch_13
        -0x4496acc9 -> :sswitch_a
        -0x41a91745 -> :sswitch_f
        -0x41143822 -> :sswitch_4
        -0x3c54de38 -> :sswitch_d
        -0x3b85b241 -> :sswitch_15
        -0x25a646c8 -> :sswitch_5
        -0x1b87b280 -> :sswitch_7
        -0x15db59af -> :sswitch_14
        -0x12efdeb3 -> :sswitch_b
        -0x3e446ed -> :sswitch_6
        -0x12603b3 -> :sswitch_11
        0x58705dc -> :sswitch_0
        0xa1fa812 -> :sswitch_3
        0x214100e0 -> :sswitch_c
        0x291d8de0 -> :sswitch_10
        0x51b3404b -> :sswitch_2
        0x73a026b5 -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2503820
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2503821
    :goto_1
    return v0

    .line 2503822
    :sswitch_0
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "13"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "17"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "19"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "20"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "21"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "22"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    .line 2503823
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2503824
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2503825
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2503826
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2503827
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2503828
    :pswitch_5
    const-string v0, "mobile"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2503829
    :pswitch_6
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2503830
    :pswitch_7
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x34 -> :sswitch_0
        0x35 -> :sswitch_1
        0x622 -> :sswitch_2
        0x626 -> :sswitch_3
        0x628 -> :sswitch_4
        0x63e -> :sswitch_5
        0x63f -> :sswitch_6
        0x640 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
