.class public final enum LX/HPm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HPm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HPm;

.field public static final enum DEFAULT:LX/HPm;

.field public static final enum FORCED_BY_USER:LX/HPm;

.field public static final enum FORCED_SERVER_AFTER_CACHE_HIT:LX/HPm;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2462405
    new-instance v0, LX/HPm;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/HPm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HPm;->DEFAULT:LX/HPm;

    .line 2462406
    new-instance v0, LX/HPm;

    const-string v1, "FORCED_BY_USER"

    invoke-direct {v0, v1, v3}, LX/HPm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HPm;->FORCED_BY_USER:LX/HPm;

    .line 2462407
    new-instance v0, LX/HPm;

    const-string v1, "FORCED_SERVER_AFTER_CACHE_HIT"

    invoke-direct {v0, v1, v4}, LX/HPm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HPm;->FORCED_SERVER_AFTER_CACHE_HIT:LX/HPm;

    .line 2462408
    const/4 v0, 0x3

    new-array v0, v0, [LX/HPm;

    sget-object v1, LX/HPm;->DEFAULT:LX/HPm;

    aput-object v1, v0, v2

    sget-object v1, LX/HPm;->FORCED_BY_USER:LX/HPm;

    aput-object v1, v0, v3

    sget-object v1, LX/HPm;->FORCED_SERVER_AFTER_CACHE_HIT:LX/HPm;

    aput-object v1, v0, v4

    sput-object v0, LX/HPm;->$VALUES:[LX/HPm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2462409
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HPm;
    .locals 1

    .prologue
    .line 2462410
    const-class v0, LX/HPm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HPm;

    return-object v0
.end method

.method public static values()[LX/HPm;
    .locals 1

    .prologue
    .line 2462411
    sget-object v0, LX/HPm;->$VALUES:[LX/HPm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HPm;

    return-object v0
.end method
