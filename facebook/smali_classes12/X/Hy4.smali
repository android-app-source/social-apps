.class public LX/Hy4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/listview/BetterListView;

.field public b:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public c:Z

.field public final d:LX/Hx6;

.field public final e:Lcom/facebook/events/dashboard/EventsDashboardFragment;


# direct methods
.method public constructor <init>(LX/Hx6;Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 0

    .prologue
    .line 2523532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2523533
    iput-object p1, p0, LX/Hy4;->d:LX/Hx6;

    .line 2523534
    iput-object p2, p0, LX/Hy4;->e:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2523535
    return-void
.end method

.method public static c(LX/Hy4;Landroid/view/ViewGroup;)Lcom/facebook/widget/FbSwipeRefreshLayout;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2523536
    iget-object v0, p0, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2523537
    iget-object v0, p0, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2523538
    :goto_0
    return-object v0

    .line 2523539
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2523540
    const v1, 0x7f03055b

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2523541
    iget-object v0, p0, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x7f0a00d1

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 2523542
    iget-object v0, p0, LX/Hy4;->e:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    if-eqz v0, :cond_1

    .line 2523543
    iget-object v0, p0, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const v1, 0x7f0d0eff

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, LX/Hy4;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 2523544
    iget-object v0, p0, LX/Hy4;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2523545
    iget-object v0, p0, LX/Hy4;->e:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v1, p0, LX/Hy4;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget-object v3, p0, LX/Hy4;->d:LX/Hx6;

    .line 2523546
    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    .line 2523547
    iput-object v2, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2523548
    sget-object p1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v3, p1, :cond_2

    .line 2523549
    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->q(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523550
    :goto_1
    iget-object p1, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    invoke-virtual {p1, v3}, LX/HxQ;->a(LX/Hx6;)V

    .line 2523551
    sget-object p1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v3, p1, :cond_1

    .line 2523552
    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->n(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523553
    :cond_1
    iget-object v0, p0, LX/Hy4;->b:Lcom/facebook/widget/FbSwipeRefreshLayout;

    goto :goto_0

    .line 2523554
    :cond_2
    iget-object p1, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    invoke-static {v0, p1}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a(Lcom/facebook/events/dashboard/EventsDashboardFragment;Lcom/facebook/widget/listview/BetterListView;)V

    goto :goto_1
.end method
