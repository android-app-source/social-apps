.class public LX/IV9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/ITK;

.field public final b:LX/3my;

.field public final c:LX/2g9;

.field public final d:LX/IQY;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B1P;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/ITK;LX/3my;LX/2g9;LX/IQY;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ITK;",
            "LX/3my;",
            "LX/2g9;",
            "LX/IQY;",
            "LX/0Ot",
            "<",
            "LX/B1P;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2581451
    iput-object p1, p0, LX/IV9;->a:LX/ITK;

    .line 2581452
    iput-object p2, p0, LX/IV9;->b:LX/3my;

    .line 2581453
    iput-object p3, p0, LX/IV9;->c:LX/2g9;

    .line 2581454
    iput-object p4, p0, LX/IV9;->d:LX/IQY;

    .line 2581455
    iput-object p5, p0, LX/IV9;->e:LX/0Ot;

    .line 2581456
    iput-object p6, p0, LX/IV9;->f:LX/0Ot;

    .line 2581457
    return-void
.end method

.method public static a(LX/0QB;)LX/IV9;
    .locals 10

    .prologue
    .line 2581458
    const-class v1, LX/IV9;

    monitor-enter v1

    .line 2581459
    :try_start_0
    sget-object v0, LX/IV9;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581460
    sput-object v2, LX/IV9;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581461
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581462
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581463
    new-instance v3, LX/IV9;

    const-class v4, LX/ITK;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/ITK;

    invoke-static {v0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v5

    check-cast v5, LX/3my;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v6

    check-cast v6, LX/2g9;

    invoke-static {v0}, LX/IQY;->b(LX/0QB;)LX/IQY;

    move-result-object v7

    check-cast v7, LX/IQY;

    const/16 v8, 0x2410

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xac0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/IV9;-><init>(LX/ITK;LX/3my;LX/2g9;LX/IQY;LX/0Ot;LX/0Ot;)V

    .line 2581464
    move-object v0, v3

    .line 2581465
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581466
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IV9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581467
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581468
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
