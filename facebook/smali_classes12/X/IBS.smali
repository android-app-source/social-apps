.class public LX/IBS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0hB;

.field public final b:LX/0TD;

.field private final c:LX/0tX;

.field public final d:LX/0ad;

.field private final e:LX/0sX;


# direct methods
.method public constructor <init>(LX/0hB;LX/0TD;LX/0tX;LX/0ad;LX/0sX;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2547159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2547160
    iput-object p1, p0, LX/IBS;->a:LX/0hB;

    .line 2547161
    iput-object p2, p0, LX/IBS;->b:LX/0TD;

    .line 2547162
    iput-object p3, p0, LX/IBS;->c:LX/0tX;

    .line 2547163
    iput-object p4, p0, LX/IBS;->d:LX/0ad;

    .line 2547164
    iput-object p5, p0, LX/IBS;->e:LX/0sX;

    .line 2547165
    return-void
.end method

.method public static a(LX/0QB;)LX/IBS;
    .locals 1

    .prologue
    .line 2547189
    invoke-static {p0}, LX/IBS;->b(LX/0QB;)LX/IBS;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/IBS;
    .locals 6

    .prologue
    .line 2547190
    new-instance v0, LX/IBS;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v5

    check-cast v5, LX/0sX;

    invoke-direct/range {v0 .. v5}, LX/IBS;-><init>(LX/0hB;LX/0TD;LX/0tX;LX/0ad;LX/0sX;)V

    .line 2547191
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2547166
    new-instance v0, LX/7oL;

    invoke-direct {v0}, LX/7oL;-><init>()V

    .line 2547167
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2547168
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0f74

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2547169
    const-string v2, "profile_image_size"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2547170
    const-string v1, "cover_image_portrait_size"

    iget-object v2, p0, LX/IBS;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2547171
    const-string v1, "cover_image_landscape_size"

    iget-object v2, p0, LX/IBS;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2547172
    const-string v1, "automatic_photo_captioning_enabled"

    iget-object v2, p0, LX/IBS;->e:LX/0sX;

    invoke-virtual {v2}, LX/0sX;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2547173
    const-string v1, "ticket_client_capability"

    const/4 p2, 0x0

    .line 2547174
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2547175
    const-string v3, "FREE_TICKETS"

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2547176
    iget-object v3, p0, LX/IBS;->d:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget-short p1, LX/347;->h:S

    invoke-interface {v3, v4, v5, p1, p2}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2547177
    const-string v3, "MULTI_TIER_TICKETING"

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2547178
    :cond_0
    iget-object v3, p0, LX/IBS;->d:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget-short p1, LX/347;->i:S

    invoke-interface {v3, v4, v5, p1, p2}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2547179
    const-string v3, "REGISTRATIONS"

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2547180
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2547181
    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2547182
    invoke-static {}, LX/7oV;->c()LX/7oL;

    move-result-object v1

    .line 2547183
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const/4 v2, 0x1

    .line 2547184
    iput-boolean v2, v1, LX/0zO;->p:Z

    .line 2547185
    move-object v1, v1

    .line 2547186
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 2547187
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 2547188
    iget-object v1, p0, LX/IBS;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
