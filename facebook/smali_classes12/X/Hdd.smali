.class public LX/Hdd;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mX",
        "<",
        "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/HdO;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pm;LX/25M;LX/HdO;)V
    .locals 0
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;",
            ">;",
            "LX/1Pm;",
            "LX/25M;",
            "LX/HdO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2488875
    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2488876
    iput-object p5, p0, LX/Hdd;->c:LX/HdO;

    .line 2488877
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2488859
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 3

    .prologue
    .line 2488861
    check-cast p2, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    .line 2488862
    iget-object v0, p0, LX/Hdd;->c:LX/HdO;

    const/4 v1, 0x0

    .line 2488863
    new-instance v2, LX/HdN;

    invoke-direct {v2, v0}, LX/HdN;-><init>(LX/HdO;)V

    .line 2488864
    sget-object p0, LX/HdO;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HdM;

    .line 2488865
    if-nez p0, :cond_0

    .line 2488866
    new-instance p0, LX/HdM;

    invoke-direct {p0}, LX/HdM;-><init>()V

    .line 2488867
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HdM;->a$redex0(LX/HdM;LX/1De;IILX/HdN;)V

    .line 2488868
    move-object v2, p0

    .line 2488869
    move-object v1, v2

    .line 2488870
    move-object v0, v1

    .line 2488871
    iget-object v1, v0, LX/HdM;->a:LX/HdN;

    iput-object p2, v1, LX/HdN;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    .line 2488872
    iget-object v1, v0, LX/HdM;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2488873
    move-object v0, v0

    .line 2488874
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2488860
    const/4 v0, 0x1

    return v0
.end method
