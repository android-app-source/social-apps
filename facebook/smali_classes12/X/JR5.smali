.class public LX/JR5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/23i;


# direct methods
.method public constructor <init>(LX/23i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692740
    iput-object p1, p0, LX/JR5;->a:LX/23i;

    .line 2692741
    return-void
.end method

.method public static a(LX/0QB;)LX/JR5;
    .locals 4

    .prologue
    .line 2692742
    const-class v1, LX/JR5;

    monitor-enter v1

    .line 2692743
    :try_start_0
    sget-object v0, LX/JR5;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692744
    sput-object v2, LX/JR5;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692745
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692746
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692747
    new-instance p0, LX/JR5;

    invoke-static {v0}, LX/23i;->a(LX/0QB;)LX/23i;

    move-result-object v3

    check-cast v3, LX/23i;

    invoke-direct {p0, v3}, LX/JR5;-><init>(LX/23i;)V

    .line 2692748
    move-object v0, p0

    .line 2692749
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692750
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JR5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692751
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692752
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
