.class public LX/Iis;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iio;


# instance fields
.field private a:LX/0Or;
    .annotation runtime Lcom/facebook/payments/p2p/config/HasUserAddedCredentialInP2pPaymentsBefore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/HasUserAddedCredentialInP2pPaymentsBefore;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605116
    iput-object p1, p0, LX/Iis;->a:LX/0Or;

    .line 2605117
    return-void
.end method


# virtual methods
.method public final a()LX/0Tn;
    .locals 1

    .prologue
    .line 2605114
    sget-object v0, LX/Iiw;->d:LX/0Tn;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2605119
    iget-object v0, p0, LX/Iis;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2605118
    const/4 v0, 0x2

    return v0
.end method
