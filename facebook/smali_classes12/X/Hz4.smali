.class public final LX/Hz4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V
    .locals 0

    .prologue
    .line 2525303
    iput-object p1, p0, LX/Hz4;->a:Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2525304
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2525305
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525306
    if-nez p1, :cond_1

    .line 2525307
    :cond_0
    :goto_0
    return-void

    .line 2525308
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525309
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2525310
    iget-object v1, p0, LX/Hz4;->a:Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2525311
    iget-object v1, p0, LX/Hz4;->a:Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Hz2;->a(Lcom/facebook/events/model/Event;)V

    goto :goto_0
.end method
