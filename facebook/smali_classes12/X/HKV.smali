.class public LX/HKV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/HKQ;


# direct methods
.method public constructor <init>(LX/HKQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2454520
    iput-object p1, p0, LX/HKV;->a:LX/HKQ;

    .line 2454521
    return-void
.end method

.method private static a(LX/HKV;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/util/List;)LX/1Dg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "Ljava/util/List",
            "<",
            "LX/HKU;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 2454522
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0a0097

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-interface {v0, v1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v7

    invoke-interface {p4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HKU;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/HKV;->a(LX/HKV;LX/1De;ZLX/HKU;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;

    move-result-object v0

    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v6, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0e45

    invoke-interface {v0, v8, v1}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v7

    invoke-interface {p4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HKU;

    move-object v0, p0

    move-object v1, p1

    move v2, v8

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/HKV;->a(LX/HKV;LX/1De;ZLX/HKU;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0e47

    invoke-interface {v0, v8, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x3

    const v2, 0x7f0b0e46

    invoke-interface {v0, v1, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {p4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HKU;

    move-object v0, p0

    move-object v1, p1

    move v2, v8

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/HKV;->a(LX/HKV;LX/1De;ZLX/HKU;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0e47

    invoke-interface {v0, v8, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v6, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1De;)LX/1Di;
    .locals 2

    .prologue
    .line 2454523
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/HKV;LX/1De;ZLX/HKU;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Z",
            "LX/HKU;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 2454524
    iget-object v0, p0, LX/HKV;->a:LX/HKQ;

    const/4 v1, 0x0

    .line 2454525
    new-instance v2, LX/HKP;

    invoke-direct {v2, v0}, LX/HKP;-><init>(LX/HKQ;)V

    .line 2454526
    iget-object p0, v0, LX/HKQ;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HKO;

    .line 2454527
    if-nez p0, :cond_0

    .line 2454528
    new-instance p0, LX/HKO;

    invoke-direct {p0, v0}, LX/HKO;-><init>(LX/HKQ;)V

    .line 2454529
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HKO;->a$redex0(LX/HKO;LX/1De;IILX/HKP;)V

    .line 2454530
    move-object v2, p0

    .line 2454531
    move-object v1, v2

    .line 2454532
    move-object v0, v1

    .line 2454533
    iget-object v1, v0, LX/HKO;->a:LX/HKP;

    iput-object p4, v1, LX/HKP;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2454534
    iget-object v1, v0, LX/HKO;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454535
    move-object v0, v0

    .line 2454536
    iget-object v1, v0, LX/HKO;->a:LX/HKP;

    iput-boolean p2, v1, LX/HKP;->b:Z

    .line 2454537
    iget-object v1, v0, LX/HKO;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454538
    move-object v0, v0

    .line 2454539
    iget-object v1, v0, LX/HKO;->a:LX/HKP;

    iput-object p3, v1, LX/HKP;->a:LX/HKU;

    .line 2454540
    iget-object v1, v0, LX/HKO;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454541
    move-object v0, v0

    .line 2454542
    iget-object v1, v0, LX/HKO;->a:LX/HKP;

    iput-object p5, v1, LX/HKP;->d:LX/2km;

    .line 2454543
    iget-object v1, v0, LX/HKO;->e:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454544
    move-object v0, v0

    .line 2454545
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/HKV;
    .locals 4

    .prologue
    .line 2454546
    const-class v1, LX/HKV;

    monitor-enter v1

    .line 2454547
    :try_start_0
    sget-object v0, LX/HKV;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454548
    sput-object v2, LX/HKV;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454549
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454550
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454551
    new-instance p0, LX/HKV;

    invoke-static {v0}, LX/HKQ;->a(LX/0QB;)LX/HKQ;

    move-result-object v3

    check-cast v3, LX/HKQ;

    invoke-direct {p0, v3}, LX/HKV;-><init>(LX/HKQ;)V

    .line 2454552
    move-object v0, p0

    .line 2454553
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454554
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454555
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454556
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;I)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "I)",
            "Ljava/util/List",
            "<",
            "LX/HKU;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2454557
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2454558
    invoke-interface {v0}, LX/9uc;->bU()LX/0Px;

    move-result-object v3

    .line 2454559
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2454560
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2454561
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v6, v0, [J

    move v2, v1

    .line 2454562
    :goto_0
    if-ge v2, v4, :cond_0

    .line 2454563
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v6, v2

    .line 2454564
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2454565
    :cond_0
    :goto_1
    if-ge v1, v4, :cond_1

    .line 2454566
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v2

    .line 2454567
    new-instance v7, LX/HKU;

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    invoke-interface {v0}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    invoke-interface {v0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v2, v8, v0, v6}, LX/HKU;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[J)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2454568
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2454569
    :cond_1
    return-object v5
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Dg;
    .locals 11
    .param p2    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x1

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 2454570
    const/4 v0, 0x3

    invoke-static {p2, v0}, LX/HKV;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;I)Ljava/util/List;

    move-result-object v7

    .line 2454571
    if-eqz v7, :cond_0

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2454572
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v10, :cond_1

    .line 2454573
    invoke-static {p0, p1, p2, p3, v7}, LX/HKV;->a(LX/HKV;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/util/List;)LX/1Dg;

    move-result-object v0

    .line 2454574
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 2454575
    goto :goto_0

    .line 2454576
    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0a0097

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v8

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HKU;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/HKV;->a(LX/HKV;LX/1De;ZLX/HKU;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v10, :cond_2

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HKU;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/HKV;->a(LX/HKV;LX/1De;ZLX/HKU;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    :goto_2
    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/HKV;->a(LX/1De;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {p1}, LX/HKV;->a(LX/1De;)LX/1Di;

    move-result-object v0

    goto :goto_2
.end method
