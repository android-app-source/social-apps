.class public final LX/HPS;
.super LX/CgK;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V
    .locals 0

    .prologue
    .line 2461828
    iput-object p1, p0, LX/HPS;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-direct {p0}, LX/CgK;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 14

    .prologue
    .line 2461829
    check-cast p1, LX/CgJ;

    .line 2461830
    iget-object v0, p0, LX/HPS;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    .line 2461831
    iget-object v1, p1, LX/CgJ;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2461832
    iget-object v2, p1, LX/CgJ;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2461833
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2461834
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v9

    .line 2461835
    new-instance v3, LX/4Vp;

    invoke-direct {v3}, LX/4Vp;-><init>()V

    .line 2461836
    iput-object v2, v3, LX/4Vp;->m:Ljava/lang/String;

    .line 2461837
    move-object v3, v3

    .line 2461838
    invoke-virtual {v3}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v4

    .line 2461839
    iget-object v3, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->A:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8I0;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v9, v8}, LX/8I0;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 2461840
    const-string v3, "extra_album_selected"

    invoke-static {v10, v3, v4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2461841
    const-string v3, "extra_photo_tab_mode_params"

    sget-object v4, LX/5SD;->VIEWING_MODE:LX/5SD;

    iget-object v8, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2461842
    iget-object v11, v8, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v8, v11

    .line 2461843
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v4, v11, v12}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    move-result-object v4

    invoke-virtual {v10, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2461844
    const-string v3, "is_page"

    invoke-virtual {v10, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2461845
    const-string v3, "owner_id"

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-virtual {v10, v3, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2461846
    const-string v3, "pick_hc_pic"

    invoke-virtual {v10, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2461847
    const-string v3, "pick_pic_lite"

    invoke-virtual {v10, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2461848
    const-string v3, "disable_adding_photos_to_albums"

    invoke-virtual {v10, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2461849
    iget-object v3, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->z:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2U1;

    invoke-virtual {v3, v1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8Dk;

    .line 2461850
    if-eqz v3, :cond_0

    .line 2461851
    iget-object v4, v3, LX/8Dk;->b:LX/0am;

    move-object v4, v4

    .line 2461852
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2461853
    :cond_0
    iget-object v3, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->B:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v10, v9}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2461854
    :goto_0
    return-void

    .line 2461855
    :cond_1
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2461856
    iget-object v4, v3, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v4, v4

    .line 2461857
    invoke-virtual {v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    move v8, v7

    .line 2461858
    :goto_1
    if-ge v8, v13, :cond_2

    invoke-virtual {v12, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2461859
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2461860
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    .line 2461861
    :cond_2
    iget-object v4, v3, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v4, v4

    .line 2461862
    invoke-virtual {v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    .line 2461863
    if-eqz v4, :cond_6

    move v4, v6

    .line 2461864
    :goto_2
    if-eqz v4, :cond_7

    .line 2461865
    iget-object v4, v3, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v4, v4

    .line 2461866
    invoke-virtual {v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v4

    iget-object v8, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2461867
    invoke-virtual {v8, v4, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 2461868
    :goto_3
    iget-object v8, v3, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v8, v8

    .line 2461869
    invoke-virtual {v8}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_8

    .line 2461870
    :goto_4
    if-eqz v6, :cond_3

    .line 2461871
    iget-object v5, v3, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v3, v5

    .line 2461872
    invoke-virtual {v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v5

    .line 2461873
    :cond_3
    new-instance v3, LX/89I;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    sget-object v6, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v3, v7, v8, v6}, LX/89I;-><init>(JLX/2rw;)V

    .line 2461874
    iput-object v5, v3, LX/89I;->c:Ljava/lang/String;

    .line 2461875
    move-object v3, v3

    .line 2461876
    iput-object v4, v3, LX/89I;->d:Ljava/lang/String;

    .line 2461877
    move-object v3, v3

    .line 2461878
    invoke-virtual {v3}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    .line 2461879
    invoke-static {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Q(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    .line 2461880
    if-eqz v4, :cond_5

    .line 2461881
    const-string v5, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v10, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2461882
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 2461883
    const-string v4, "extra_pages_admin_permissions"

    invoke-virtual {v10, v4, v11}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2461884
    :cond_4
    const-string v4, "extra_composer_target_data"

    invoke-virtual {v10, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2461885
    :cond_5
    iget-object v3, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->B:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v10, v9}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_6
    move v4, v7

    .line 2461886
    goto :goto_2

    :cond_7
    move-object v4, v5

    .line 2461887
    goto :goto_3

    :cond_8
    move v6, v7

    .line 2461888
    goto :goto_4
.end method
