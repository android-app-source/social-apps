.class public final LX/HWq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2477094
    iput-object p1, p0, LX/HWq;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2477095
    iget-object v0, p0, LX/HWq;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477096
    iget-object v1, v0, LX/HX9;->b:LX/03R;

    move-object v0, v1

    .line 2477097
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2477098
    :goto_0
    return-object v4

    .line 2477099
    :cond_0
    iget-object v0, p0, LX/HWq;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->y:LX/HES;

    iget-object v1, p0, LX/HWq;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477100
    iget-wide v5, v1, LX/HX9;->a:J

    move-wide v2, v5

    .line 2477101
    new-instance v1, LX/HEY;

    invoke-direct {v1}, LX/HEY;-><init>()V

    move-object v1, v1

    .line 2477102
    const-string v5, "page_id"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/HEY;

    .line 2477103
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v5, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2477104
    iget-object v5, v0, LX/HES;->a:LX/0tX;

    invoke-virtual {v5, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2477105
    new-instance v5, LX/HER;

    invoke-direct {v5, v0, v2, v3}, LX/HER;-><init>(LX/HES;J)V

    iget-object v6, v0, LX/HES;->b:Ljava/util/concurrent/Executor;

    invoke-static {v1, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2477106
    goto :goto_0
.end method
