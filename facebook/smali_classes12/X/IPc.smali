.class public LX/IPc;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IPa;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IPd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2574386
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/IPc;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IPd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574387
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2574388
    iput-object p1, p0, LX/IPc;->b:LX/0Ot;

    .line 2574389
    return-void
.end method

.method public static a(LX/0QB;)LX/IPc;
    .locals 4

    .prologue
    .line 2574390
    const-class v1, LX/IPc;

    monitor-enter v1

    .line 2574391
    :try_start_0
    sget-object v0, LX/IPc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2574392
    sput-object v2, LX/IPc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2574393
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2574394
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2574395
    new-instance v3, LX/IPc;

    const/16 p0, 0x23f7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IPc;-><init>(LX/0Ot;)V

    .line 2574396
    move-object v0, v3

    .line 2574397
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2574398
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IPc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574399
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2574400
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2574401
    check-cast p2, LX/IPb;

    .line 2574402
    iget-object v0, p0, LX/IPc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IPd;

    iget-object v2, p2, LX/IPb;->a:LX/BcO;

    iget-object v3, p2, LX/IPb;->b:LX/Bdb;

    iget-object v4, p2, LX/IPb;->c:LX/1X1;

    iget-object v5, p2, LX/IPb;->d:LX/1X1;

    iget-boolean v6, p2, LX/IPb;->e:Z

    iget-object v7, p2, LX/IPb;->f:LX/IPe;

    iget-object v8, p2, LX/IPb;->g:LX/5K7;

    move-object v1, p1

    const/4 p1, 0x2

    const/4 p2, 0x1

    .line 2574403
    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v9

    const v10, 0x7f0a011c

    invoke-virtual {v9, v10}, LX/25Q;->i(I)LX/25Q;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    invoke-interface {v9, p2}, LX/1Di;->o(I)LX/1Di;

    move-result-object v9

    const/4 v10, 0x4

    invoke-interface {v9, v10}, LX/1Di;->b(I)LX/1Di;

    move-result-object v11

    .line 2574404
    if-eqz v6, :cond_0

    const v9, 0x7f0207e2

    move v10, v9

    .line 2574405
    :goto_0
    if-eqz v6, :cond_1

    invoke-static {v1}, LX/Bdq;->c(LX/1De;)LX/Bdl;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/Bdl;->a(LX/5K7;)LX/Bdl;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/Bdl;->a(LX/BcO;)LX/Bdl;

    move-result-object v9

    invoke-virtual {v9, v4}, LX/Bdl;->b(LX/1X1;)LX/Bdl;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/Bdl;->a(LX/1X1;)LX/Bdl;

    move-result-object v9

    invoke-virtual {v9, v3}, LX/Bdl;->a(LX/Bdb;)LX/Bdl;

    move-result-object v9

    invoke-virtual {v9, p2}, LX/Bdl;->a(Z)LX/Bdl;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const v12, 0x7f0b240d

    invoke-interface {v9, v12}, LX/1Di;->q(I)LX/1Di;

    move-result-object v9

    .line 2574406
    :goto_1
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v12

    const p0, 0x7f0b2408

    invoke-interface {v12, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v12

    invoke-interface {v12, v11}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v11

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v12

    invoke-interface {v12, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v12

    invoke-interface {v12, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v12

    const/4 p0, 0x3

    invoke-interface {v12, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v12

    const p0, 0x7f0b240a

    invoke-interface {v12, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v12

    const/4 p0, 0x6

    const p1, 0x7f0b240b

    invoke-interface {v12, p0, p1}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v12

    .line 2574407
    const p0, 0x39ac1652

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v7, p1, v2

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2574408
    invoke-interface {v12, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v12

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p1, 0x7f0b2409

    invoke-virtual {p0, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p1, 0x7f0838a7

    invoke-virtual {p0, p1}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object p0

    invoke-interface {v12, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v12

    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    iget-object p1, v0, LX/IPd;->a:LX/1vg;

    invoke-virtual {p1, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p1

    invoke-virtual {p1, v10}, LX/2xv;->h(I)LX/2xv;

    move-result-object v10

    const p1, 0x7f0a00e6

    invoke-virtual {v10, p1}, LX/2xv;->j(I)LX/2xv;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const p0, 0x7f0b240c

    invoke-interface {v10, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v10

    const p0, 0x7f0b240c

    invoke-interface {v10, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v10

    invoke-interface {v12, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    invoke-interface {v11, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9}, LX/1Di;->k()LX/1Dg;

    move-result-object v9

    move-object v0, v9

    .line 2574409
    return-object v0

    .line 2574410
    :cond_0
    const v9, 0x7f0207f0

    move v10, v9

    goto/16 :goto_0

    .line 2574411
    :cond_1
    const/4 v9, 0x0

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2574412
    invoke-static {}, LX/1dS;->b()V

    .line 2574413
    iget v0, p1, LX/1dQ;->b:I

    .line 2574414
    packed-switch v0, :pswitch_data_0

    .line 2574415
    :goto_0
    return-object v2

    .line 2574416
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, LX/IPe;

    .line 2574417
    iget-object p1, p0, LX/IPc;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2574418
    if-eqz v0, :cond_0

    .line 2574419
    iget-object p1, v0, LX/IPe;->a:LX/IPj;

    const/4 p0, 0x1

    .line 2574420
    iput-boolean p0, p1, LX/IPj;->o:Z

    .line 2574421
    iget-object p1, v0, LX/IPe;->a:LX/IPj;

    iget-boolean p1, p1, LX/IPj;->j:Z

    if-nez p1, :cond_1

    .line 2574422
    iget-object p1, v0, LX/IPe;->a:LX/IPj;

    invoke-static {p1}, LX/IPj;->c$redex0(LX/IPj;)V

    .line 2574423
    :cond_0
    :goto_1
    goto :goto_0

    .line 2574424
    :cond_1
    iget-object p1, v0, LX/IPe;->a:LX/IPj;

    invoke-static {p1}, LX/IPj;->d(LX/IPj;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x39ac1652
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/IPa;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2574425
    new-instance v1, LX/IPb;

    invoke-direct {v1, p0}, LX/IPb;-><init>(LX/IPc;)V

    .line 2574426
    sget-object v2, LX/IPc;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IPa;

    .line 2574427
    if-nez v2, :cond_0

    .line 2574428
    new-instance v2, LX/IPa;

    invoke-direct {v2}, LX/IPa;-><init>()V

    .line 2574429
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/IPa;->a$redex0(LX/IPa;LX/1De;IILX/IPb;)V

    .line 2574430
    move-object v1, v2

    .line 2574431
    move-object v0, v1

    .line 2574432
    return-object v0
.end method
