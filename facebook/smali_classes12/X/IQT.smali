.class public LX/IQT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0iA;

.field public final b:LX/IW5;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/view/View;

.field public final e:Landroid/view/ViewGroup;

.field public final f:Lcom/facebook/base/fragment/FbFragment;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hf;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation
.end field

.field public final j:LX/0Or;
    .annotation runtime Lcom/facebook/groups/feed/ui/IsGroupCommercePurposeModalEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Uh;

.field public final l:LX/ISU;

.field private m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

.field public n:Z


# direct methods
.method public constructor <init>(LX/0iA;LX/IW5;LX/0Ot;Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/base/fragment/FbFragment;LX/ISU;LX/0Ot;LX/0Ot;Ljava/lang/Boolean;LX/0Or;LX/0Uh;)V
    .locals 0
    .param p4    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/base/fragment/FbFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/ISU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/groups/feed/ui/IsGroupCommercePurposeModalEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0iA;",
            "LX/IW5;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "LX/ISU;",
            "LX/0Ot",
            "<",
            "LX/2hf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2575224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2575225
    iput-object p1, p0, LX/IQT;->a:LX/0iA;

    .line 2575226
    iput-object p2, p0, LX/IQT;->b:LX/IW5;

    .line 2575227
    iput-object p3, p0, LX/IQT;->c:LX/0Ot;

    .line 2575228
    iput-object p4, p0, LX/IQT;->d:Landroid/view/View;

    .line 2575229
    iput-object p5, p0, LX/IQT;->e:Landroid/view/ViewGroup;

    .line 2575230
    iput-object p6, p0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    .line 2575231
    iput-object p7, p0, LX/IQT;->l:LX/ISU;

    .line 2575232
    iput-object p8, p0, LX/IQT;->g:LX/0Ot;

    .line 2575233
    iput-object p9, p0, LX/IQT;->h:LX/0Ot;

    .line 2575234
    iput-object p10, p0, LX/IQT;->i:Ljava/lang/Boolean;

    .line 2575235
    iput-object p12, p0, LX/IQT;->k:LX/0Uh;

    .line 2575236
    iput-object p11, p0, LX/IQT;->j:LX/0Or;

    .line 2575237
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2575254
    const/4 v1, 0x0

    .line 2575255
    iget-object v0, p0, LX/IQT;->a:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/1wS;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/1wS;

    .line 2575256
    if-eqz v0, :cond_5

    .line 2575257
    iget-object v2, p0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 2575258
    if-nez v2, :cond_4

    move v0, v1

    .line 2575259
    :goto_0
    move v0, v0

    .line 2575260
    if-eqz v0, :cond_0

    .line 2575261
    :goto_1
    return-void

    .line 2575262
    :cond_0
    const/4 v0, 0x0

    .line 2575263
    iget-object v1, p0, LX/IQT;->a:LX/0iA;

    sget-object v2, LX/77s;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/77s;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/77s;

    .line 2575264
    if-nez v1, :cond_6

    .line 2575265
    const/4 v1, 0x0

    .line 2575266
    :goto_2
    move-object v1, v1

    .line 2575267
    if-eqz v1, :cond_1

    iget-object v2, p0, LX/IQT;->m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/IQT;->m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v2, v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2575268
    iget-object v0, p0, LX/IQT;->m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->r()V

    goto :goto_1

    .line 2575269
    :cond_1
    if-eqz v1, :cond_2

    .line 2575270
    iget-object v0, p0, LX/IQT;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hf;

    invoke-virtual {v0, v1}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object v0

    .line 2575271
    :cond_2
    instance-of v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v1, :cond_3

    .line 2575272
    check-cast v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    iput-object v0, p0, LX/IQT;->m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    .line 2575273
    iget-object v0, p0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d159e

    iget-object v2, p0, LX/IQT;->m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2575274
    iget-object v0, p0, LX/IQT;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 2575275
    :cond_3
    invoke-virtual {p0}, LX/IQT;->b()V

    goto :goto_1

    .line 2575276
    :cond_4
    iget-object v1, p0, LX/IQT;->a:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/1wS;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2575277
    iget-object v0, p0, LX/IQT;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2575278
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 2575279
    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2575248
    iget-object v0, p0, LX/IQT;->m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v0, :cond_0

    .line 2575249
    iget-object v0, p0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/IQT;->m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2575250
    const/4 v0, 0x0

    iput-object v0, p0, LX/IQT;->m:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    .line 2575251
    :cond_0
    iget-object v0, p0, LX/IQT;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2575252
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2575253
    iget-object v0, p0, LX/IQT;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IQT;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2575241
    invoke-virtual {p0}, LX/IQT;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2575242
    new-instance v0, LX/31Y;

    iget-object v1, p0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2575243
    const v1, 0x7f081baa

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    .line 2575244
    const v1, 0x7f081ba9

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    .line 2575245
    const v1, 0x7f081ba8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2575246
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2575247
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2575238
    iget-object v0, p0, LX/IQT;->b:LX/IW5;

    .line 2575239
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2575240
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
