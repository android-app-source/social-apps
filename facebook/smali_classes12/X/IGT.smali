.class public final LX/IGT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V
    .locals 0

    .prologue
    .line 2556057
    iput-object p1, p0, LX/IGT;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2556052
    iget-object v0, p0, LX/IGT;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x300009

    const-string v2, "FriendsNearbyPingDelete"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2556053
    sget-object v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->m:Ljava/lang/Class;

    const-string v1, "Could not delete location ping"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2556054
    iget-object v0, p0, LX/IGT;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2556055
    iget-object v0, p0, LX/IGT;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->w:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2556056
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2556037
    iget-object v0, p0, LX/IGT;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x300009

    const-string v2, "FriendsNearbyPingDelete"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2556038
    iget-object v0, p0, LX/IGT;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2556039
    iget-object v0, p0, LX/IGT;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->E:LX/IHy;

    if-eqz v0, :cond_0

    .line 2556040
    iget-object v0, p0, LX/IGT;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->E:LX/IHy;

    .line 2556041
    iget-object v1, v0, LX/IHy;->a:LX/IFX;

    iget-object v2, v0, LX/IHy;->b:Ljava/lang/String;

    .line 2556042
    iget-object p0, v1, LX/IFX;->d:LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->a()V

    .line 2556043
    iget-object p0, v1, LX/IFX;->l:LX/IG2;

    .line 2556044
    iget-object p1, p0, LX/IG2;->b:Ljava/util/HashMap;

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/IFf;

    .line 2556045
    if-nez p1, :cond_1

    .line 2556046
    :goto_0
    const/4 p0, 0x0

    invoke-static {v1, v2, p0}, LX/IFX;->a(LX/IFX;Ljava/lang/String;Z)V

    .line 2556047
    invoke-static {v1}, LX/IFX;->o(LX/IFX;)V

    .line 2556048
    invoke-static {v1, v2}, LX/IFX;->g(LX/IFX;Ljava/lang/String;)V

    .line 2556049
    :cond_0
    return-void

    .line 2556050
    :cond_1
    iget-object v0, p0, LX/IG2;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2556051
    iget-object p1, p0, LX/IG2;->b:Ljava/util/HashMap;

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
