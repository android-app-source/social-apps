.class public final LX/Ht8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HtB;


# direct methods
.method public constructor <init>(LX/HtB;)V
    .locals 0

    .prologue
    .line 2515395
    iput-object p1, p0, LX/Ht8;->a:LX/HtB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x55338b1e

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2515396
    iget-object v1, p0, LX/Ht8;->a:LX/HtB;

    .line 2515397
    iget-object v3, v1, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    .line 2515398
    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v4, LX/0j5;

    invoke-interface {v4}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v4, LX/0j5;

    invoke-interface {v4}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v4, :cond_0

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v4, LX/0j5;

    invoke-interface {v4}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 2515399
    :goto_0
    iget-object v6, v1, LX/HtB;->g:LX/0gd;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v4, LX/0j0;

    invoke-interface {v4}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v4, LX/0j3;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v4

    .line 2515400
    sget-object p1, LX/0ge;->COMPOSER_REMOVE_LINK_ATTACHMENT:LX/0ge;

    invoke-static {p1, p0}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object p1

    invoke-virtual {p1, v4}, LX/324;->l(Z)LX/324;

    move-result-object p1

    invoke-virtual {p1, v5}, LX/324;->r(Ljava/lang/String;)LX/324;

    move-result-object p1

    .line 2515401
    iget-object v1, p1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p1, v1

    .line 2515402
    iget-object v1, v6, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2515403
    invoke-static {v3}, LX/Hsz;->a(LX/0il;)V

    .line 2515404
    const v1, -0x6a654054

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2515405
    :cond_0
    const/4 v4, 0x0

    move-object v5, v4

    goto :goto_0
.end method
