.class public final enum LX/IJG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IJG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IJG;

.field public static final enum CHILD:LX/IJG;

.field public static final enum CHILD_MORE:LX/IJG;

.field public static final enum FAKE_HEADER:LX/IJG;

.field public static final enum HEADER:LX/IJG;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2563279
    new-instance v0, LX/IJG;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/IJG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IJG;->HEADER:LX/IJG;

    .line 2563280
    new-instance v0, LX/IJG;

    const-string v1, "FAKE_HEADER"

    invoke-direct {v0, v1, v3}, LX/IJG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IJG;->FAKE_HEADER:LX/IJG;

    .line 2563281
    new-instance v0, LX/IJG;

    const-string v1, "CHILD"

    invoke-direct {v0, v1, v4}, LX/IJG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IJG;->CHILD:LX/IJG;

    .line 2563282
    new-instance v0, LX/IJG;

    const-string v1, "CHILD_MORE"

    invoke-direct {v0, v1, v5}, LX/IJG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IJG;->CHILD_MORE:LX/IJG;

    .line 2563283
    const/4 v0, 0x4

    new-array v0, v0, [LX/IJG;

    sget-object v1, LX/IJG;->HEADER:LX/IJG;

    aput-object v1, v0, v2

    sget-object v1, LX/IJG;->FAKE_HEADER:LX/IJG;

    aput-object v1, v0, v3

    sget-object v1, LX/IJG;->CHILD:LX/IJG;

    aput-object v1, v0, v4

    sget-object v1, LX/IJG;->CHILD_MORE:LX/IJG;

    aput-object v1, v0, v5

    sput-object v0, LX/IJG;->$VALUES:[LX/IJG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2563278
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IJG;
    .locals 1

    .prologue
    .line 2563276
    const-class v0, LX/IJG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IJG;

    return-object v0
.end method

.method public static values()[LX/IJG;
    .locals 1

    .prologue
    .line 2563277
    sget-object v0, LX/IJG;->$VALUES:[LX/IJG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IJG;

    return-object v0
.end method
