.class public LX/Irv;
.super LX/IqZ;
.source ""


# instance fields
.field public final b:Lcom/facebook/messaging/doodle/ColourIndicator;

.field private final c:Lcom/facebook/messaging/doodle/ColourPicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2619081
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Irv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2619082
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2619083
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Irv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2619084
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2619085
    invoke-direct {p0, p1, p2, p3}, LX/IqZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2619086
    const v0, 0x7f03043a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2619087
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Irv;->setClipChildren(Z)V

    .line 2619088
    const v0, 0x7f0d0cd1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourIndicator;

    iput-object v0, p0, LX/Irv;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    .line 2619089
    const v0, 0x7f0d0cd2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourPicker;

    iput-object v0, p0, LX/Irv;->c:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 2619090
    iget-object v0, p0, LX/Irv;->c:Lcom/facebook/messaging/doodle/ColourPicker;

    new-instance v1, LX/Iru;

    invoke-direct {v1, p0}, LX/Iru;-><init>(LX/Irv;)V

    .line 2619091
    iput-object v1, v0, Lcom/facebook/messaging/doodle/ColourPicker;->c:LX/8CF;

    .line 2619092
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2619093
    iget-object v0, p0, LX/Irv;->c:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/doodle/ColourPicker;->setVisibility(I)V

    .line 2619094
    iget-object v0, p0, LX/Irv;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setVisibility(I)V

    .line 2619095
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2619096
    iget-object v0, p0, LX/Irv;->c:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/doodle/ColourPicker;->setVisibility(I)V

    .line 2619097
    iget-object v0, p0, LX/Irv;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setVisibility(I)V

    .line 2619098
    return-void
.end method
