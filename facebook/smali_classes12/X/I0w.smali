.class public final LX/I0w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public final synthetic b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 0

    .prologue
    .line 2528241
    iput-object p1, p0, LX/I0w;->b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iput-object p2, p0, LX/I0w;->a:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x23e99329

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2528238
    iget-object v1, p0, LX/I0w;->b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iget-object v2, p0, LX/I0w;->a:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-static {v1, v2}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->setPrivateRsvpButtonColor(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    .line 2528239
    iget-object v1, p0, LX/I0w;->b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a:LX/I7w;

    iget-object v2, p0, LX/I0w;->b:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iget-object v2, v2, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->k:LX/7oa;

    iget-object v3, p0, LX/I0w;->a:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_CALENDAR_TAB_INVITATION:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v1, v2, v3, v4}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2528240
    const v1, -0x71895e91

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
