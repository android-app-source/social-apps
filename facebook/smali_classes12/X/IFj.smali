.class public final LX/IFj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IIN;

.field public final synthetic b:LX/IFl;


# direct methods
.method public constructor <init>(LX/IFl;LX/IIN;)V
    .locals 0

    .prologue
    .line 2554771
    iput-object p1, p0, LX/IFj;->b:LX/IFl;

    iput-object p2, p0, LX/IFj;->a:LX/IIN;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2554772
    iget-object v0, p0, LX/IFj;->b:LX/IFl;

    sget-object v1, LX/IFk;->ERROR:LX/IFk;

    .line 2554773
    iput-object v1, v0, LX/IFl;->h:LX/IFk;

    .line 2554774
    iget-object v0, p0, LX/IFj;->a:LX/IIN;

    if-eqz v0, :cond_0

    .line 2554775
    iget-object v0, p0, LX/IFj;->a:LX/IIN;

    invoke-virtual {v0}, LX/IIN;->a()V

    .line 2554776
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2554777
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2554778
    if-nez p1, :cond_1

    .line 2554779
    :cond_0
    :goto_0
    return-void

    .line 2554780
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2554781
    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;

    .line 2554782
    iget-object v1, p0, LX/IFj;->b:LX/IFl;

    invoke-static {v1, v0}, LX/IFl;->b(LX/IFl;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;)LX/IFd;

    move-result-object v1

    .line 2554783
    iget-object v2, p0, LX/IFj;->b:LX/IFl;

    iget-object v3, p0, LX/IFj;->b:LX/IFl;

    invoke-static {v3, v0}, LX/IFl;->b(LX/IFl;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;)Ljava/lang/String;

    move-result-object v0

    .line 2554784
    iput-object v0, v2, LX/IFl;->i:Ljava/lang/String;

    .line 2554785
    iget-object v0, p0, LX/IFj;->b:LX/IFl;

    iget-object v0, v0, LX/IFl;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2554786
    iget-object v0, p0, LX/IFj;->b:LX/IFl;

    sget-object v2, LX/IFk;->COMPLETE:LX/IFk;

    .line 2554787
    iput-object v2, v0, LX/IFl;->h:LX/IFk;

    .line 2554788
    :goto_1
    iget-object v0, p0, LX/IFj;->a:LX/IIN;

    if-eqz v0, :cond_0

    .line 2554789
    iget-object v0, p0, LX/IFj;->a:LX/IIN;

    .line 2554790
    invoke-static {v0, v1}, LX/IIN;->c(LX/IIN;LX/IFd;)V

    .line 2554791
    goto :goto_0

    .line 2554792
    :cond_2
    iget-object v0, p0, LX/IFj;->b:LX/IFl;

    sget-object v2, LX/IFk;->HAS_MORE:LX/IFk;

    .line 2554793
    iput-object v2, v0, LX/IFl;->h:LX/IFk;

    .line 2554794
    goto :goto_1
.end method
