.class public LX/IVC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IVA;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IVD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2581510
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/IVC;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IVD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581511
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2581512
    iput-object p1, p0, LX/IVC;->b:LX/0Ot;

    .line 2581513
    return-void
.end method

.method public static a(LX/0QB;)LX/IVC;
    .locals 4

    .prologue
    .line 2581514
    const-class v1, LX/IVC;

    monitor-enter v1

    .line 2581515
    :try_start_0
    sget-object v0, LX/IVC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581516
    sput-object v2, LX/IVC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581517
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581518
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581519
    new-instance v3, LX/IVC;

    const/16 p0, 0x2423

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IVC;-><init>(LX/0Ot;)V

    .line 2581520
    move-object v0, v3

    .line 2581521
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581522
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581523
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581524
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2581525
    check-cast p2, LX/IVB;

    .line 2581526
    iget-object v0, p0, LX/IVC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IVD;

    iget-object v1, p2, LX/IVB;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p2, LX/IVB;->b:LX/IRb;

    const/4 p2, 0x5

    const/16 p0, 0x8

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2581527
    iget-object v3, v0, LX/IVD;->b:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f02089c

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const v4, -0x686869

    invoke-virtual {v3, v4}, LX/2xv;->i(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 2581528
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    .line 2581529
    invoke-static {v1}, LX/IQY;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2581530
    iget-object v5, v0, LX/IVD;->c:LX/IQY;

    invoke-virtual {v5, v1, v2}, LX/IQY;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)LX/IQB;

    move-result-object v5

    .line 2581531
    if-nez v5, :cond_0

    .line 2581532
    const/4 v3, 0x0

    .line 2581533
    :goto_0
    move-object v0, v3

    .line 2581534
    return-object v0

    .line 2581535
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v10}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v7, 0x7

    const/16 v8, 0xe

    invoke-interface {v3, v7, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    const/4 v7, 0x6

    invoke-interface {v3, v7, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0, p0}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-interface {v5}, LX/IQB;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b0050

    invoke-virtual {v7, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v8, 0x7f0a00f7

    invoke-virtual {v7, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/16 v8, 0x14

    invoke-interface {v7, p2, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-interface {v5}, LX/IQB;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v7, 0x7f0b0050

    invoke-virtual {v5, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v7, 0x7f0a00fb

    invoke-virtual {v5, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/16 v7, 0x14

    invoke-interface {v5, p2, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    invoke-interface {v6, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2581536
    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v10}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v9}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0a00d5

    invoke-interface {v3, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x438

    invoke-interface {v4, v5}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a00fe

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/IVD;->a:LX/IV8;

    const/4 v5, 0x0

    .line 2581537
    new-instance v6, LX/IV7;

    invoke-direct {v6, v4}, LX/IV7;-><init>(LX/IV8;)V

    .line 2581538
    sget-object v7, LX/IV8;->a:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/IV6;

    .line 2581539
    if-nez v7, :cond_2

    .line 2581540
    new-instance v7, LX/IV6;

    invoke-direct {v7}, LX/IV6;-><init>()V

    .line 2581541
    :cond_2
    invoke-static {v7, p1, v5, v5, v6}, LX/IV6;->a$redex0(LX/IV6;LX/1De;IILX/IV7;)V

    .line 2581542
    move-object v6, v7

    .line 2581543
    move-object v5, v6

    .line 2581544
    move-object v4, v5

    .line 2581545
    iget-object v5, v4, LX/IV6;->a:LX/IV7;

    iput-object v1, v5, LX/IV7;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2581546
    iget-object v5, v4, LX/IV6;->d:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2581547
    move-object v4, v4

    .line 2581548
    iget-object v5, v4, LX/IV6;->a:LX/IV7;

    iput-object v2, v5, LX/IV7;->b:LX/IRb;

    .line 2581549
    iget-object v5, v4, LX/IV6;->d:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2581550
    move-object v4, v4

    .line 2581551
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x438

    invoke-interface {v4, v5}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a00fe

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2581552
    invoke-static {}, LX/1dS;->b()V

    .line 2581553
    const/4 v0, 0x0

    return-object v0
.end method
