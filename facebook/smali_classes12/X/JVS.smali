.class public LX/JVS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JVQ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JVT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2700790
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JVS;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JVT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700838
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2700839
    iput-object p1, p0, LX/JVS;->b:LX/0Ot;

    .line 2700840
    return-void
.end method

.method public static a(LX/0QB;)LX/JVS;
    .locals 4

    .prologue
    .line 2700827
    const-class v1, LX/JVS;

    monitor-enter v1

    .line 2700828
    :try_start_0
    sget-object v0, LX/JVS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700829
    sput-object v2, LX/JVS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700830
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700831
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700832
    new-instance v3, LX/JVS;

    const/16 p0, 0x2096

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JVS;-><init>(LX/0Ot;)V

    .line 2700833
    move-object v0, v3

    .line 2700834
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700835
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700836
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700837
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2700826
    const v0, -0x726966ab

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2700806
    check-cast p2, LX/JVR;

    .line 2700807
    iget-object v0, p0, LX/JVS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JVT;

    iget-object v1, p2, LX/JVR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700808
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2700809
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2700810
    const/4 p0, 0x1

    const/4 p2, 0x0

    .line 2700811
    iget-object v1, v0, LX/JVT;->a:LX/17S;

    invoke-virtual {v1}, LX/17S;->g()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2700812
    invoke-static {v2}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/JVT;->a:LX/17S;

    invoke-virtual {v1}, LX/17S;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2700813
    :cond_0
    :goto_0
    move p0, p0

    .line 2700814
    if-nez p0, :cond_1

    .line 2700815
    const/4 v2, 0x0

    .line 2700816
    :goto_1
    move-object v0, v2

    .line 2700817
    return-object v0

    .line 2700818
    :cond_1
    iget-object p0, v0, LX/JVT;->a:LX/17S;

    invoke-static {v0, v2}, LX/JVT;->b(LX/JVT;Lcom/facebook/graphql/model/GraphQLStory;)LX/99r;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/17S;->b(LX/99r;)V

    .line 2700819
    iget-object v2, v0, LX/JVT;->a:LX/17S;

    invoke-virtual {v2}, LX/17S;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f0810e5

    .line 2700820
    :goto_2
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0b0050

    invoke-virtual {v2, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0a00d2

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const p0, 0x7f020791

    invoke-interface {v2, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    .line 2700821
    const p0, -0x726966ab

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2700822
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const/4 p0, 0x6

    const p2, 0x7f0b0917

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x7

    const p2, 0x7f0b006c

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_1

    .line 2700823
    :cond_2
    const v2, 0x7f0810dc

    goto :goto_2

    :cond_3
    move p0, p2

    .line 2700824
    goto :goto_0

    .line 2700825
    :cond_4
    invoke-static {v2}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, LX/JVT;->a:LX/17S;

    invoke-virtual {v1}, LX/17S;->a()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, v0, LX/JVT;->a:LX/17S;

    invoke-virtual {v1}, LX/17S;->f()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_5
    move p0, p2

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2700791
    invoke-static {}, LX/1dS;->b()V

    .line 2700792
    iget v0, p1, LX/1dQ;->b:I

    .line 2700793
    packed-switch v0, :pswitch_data_0

    .line 2700794
    :goto_0
    return-object v2

    .line 2700795
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2700796
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2700797
    check-cast v1, LX/JVR;

    .line 2700798
    iget-object v3, p0, LX/JVS;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JVT;

    iget-object v4, v1, LX/JVR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700799
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 2700800
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2700801
    invoke-static {v3, p1}, LX/JVT;->b(LX/JVT;Lcom/facebook/graphql/model/GraphQLStory;)LX/99r;

    move-result-object p2

    .line 2700802
    iget-object p0, v3, LX/JVT;->a:LX/17S;

    invoke-virtual {p0}, LX/17S;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2700803
    iget-object p1, v3, LX/JVT;->a:LX/17S;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p1, p0, p2}, LX/17S;->a(Landroid/content/Context;LX/99r;)V

    .line 2700804
    :goto_1
    goto :goto_0

    .line 2700805
    :cond_0
    iget-object p0, v3, LX/JVT;->a:LX/17S;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p2, p1}, LX/17S;->a(Landroid/content/Context;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x726966ab
        :pswitch_0
    .end packed-switch
.end method
