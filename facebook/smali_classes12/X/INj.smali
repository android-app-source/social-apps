.class public final LX/INj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic b:LX/INs;


# direct methods
.method public constructor <init>(LX/INs;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 0

    .prologue
    .line 2571105
    iput-object p1, p0, LX/INj;->b:LX/INs;

    iput-object p2, p0, LX/INj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x676d7b2b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2571106
    iget-object v0, p0, LX/INj;->b:LX/INs;

    const-string v2, "group_email_verification_change"

    iget-object v3, p0, LX/INj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v3}, LX/INs;->d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/INs;->a$redex0(LX/INs;Ljava/lang/String;Ljava/lang/String;)V

    .line 2571107
    iget-object v0, p0, LX/INj;->b:LX/INs;

    iget-object v2, v0, LX/INs;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/INj;->b:LX/INs;

    iget-object v3, p0, LX/INj;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0, v3}, LX/INs;->f(LX/INs;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/content/Intent;

    move-result-object v3

    const/16 v4, 0x7ad

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2571108
    const v0, 0x728a5250

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
