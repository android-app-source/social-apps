.class public LX/JNY;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2686067
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JNY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2686068
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2686069
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2686070
    const v0, 0x7f030646

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2686071
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2686072
    iget-boolean v0, p0, LX/JNY;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x650f6f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2686073
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2686074
    const/4 v1, 0x1

    .line 2686075
    iput-boolean v1, p0, LX/JNY;->a:Z

    .line 2686076
    const/16 v1, 0x2d

    const v2, 0x5d7040db

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7d816fef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2686077
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2686078
    const/4 v1, 0x0

    .line 2686079
    iput-boolean v1, p0, LX/JNY;->a:Z

    .line 2686080
    const/16 v1, 0x2d

    const v2, 0x63b43307

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
