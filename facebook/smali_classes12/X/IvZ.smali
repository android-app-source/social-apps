.class public final LX/IvZ;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 0

    .prologue
    .line 2628433
    iput-object p1, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;B)V
    .locals 0

    .prologue
    .line 2628407
    invoke-direct {p0, p1}, LX/IvZ;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 10

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 2628420
    iget-object v0, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->J:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 2628421
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    .line 2628422
    iget-object v1, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-boolean v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ai:Z

    if-eqz v1, :cond_2

    .line 2628423
    iget-object v1, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setTranslationX(F)V

    .line 2628424
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 2628425
    iget-object v1, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-boolean v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ai:Z

    if-eqz v1, :cond_3

    .line 2628426
    float-to-double v0, v0

    iget-object v4, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget v4, v4, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->M:F

    float-to-double v4, v4

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 2628427
    :goto_1
    iget-object v1, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    if-eqz v1, :cond_1

    .line 2628428
    iget-object v1, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->setAlpha(F)V

    .line 2628429
    :cond_1
    return-void

    .line 2628430
    :cond_2
    iget-object v1, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-boolean v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->aj:Z

    if-eqz v1, :cond_0

    .line 2628431
    iget-object v1, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setTranslationY(F)V

    goto :goto_0

    .line 2628432
    :cond_3
    float-to-double v0, v0

    iget-object v4, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget v4, v4, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->N:F

    float-to-double v4, v4

    const-wide/high16 v8, 0x3fd0000000000000L    # 0.25

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_1
.end method

.method public final b(LX/0wd;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2628408
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 2628409
    iget-object v0, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-boolean v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ai:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->M:F

    .line 2628410
    :goto_0
    float-to-double v0, v0

    cmpl-double v0, v2, v0

    if-ltz v0, :cond_1

    .line 2628411
    iget-object v0, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    const/4 v1, 0x1

    .line 2628412
    invoke-static {v0, v1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    .line 2628413
    :goto_1
    return-void

    .line 2628414
    :cond_0
    iget-object v0, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->N:F

    goto :goto_0

    .line 2628415
    :cond_1
    iget-object v0, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628416
    iput-boolean v4, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ai:Z

    .line 2628417
    iget-object v0, p0, LX/IvZ;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628418
    iput-boolean v4, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->aj:Z

    .line 2628419
    goto :goto_1
.end method
