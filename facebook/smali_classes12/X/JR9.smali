.class public final LX/JR9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/JR3;

.field public final synthetic b:LX/1Pm;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:LX/JRA;


# direct methods
.method public constructor <init>(LX/JRA;LX/JR3;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 2692834
    iput-object p1, p0, LX/JR9;->d:LX/JRA;

    iput-object p2, p0, LX/JR9;->a:LX/JR3;

    iput-object p3, p0, LX/JR9;->b:LX/1Pm;

    iput-object p4, p0, LX/JR9;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2692835
    iget-object v0, p0, LX/JR9;->a:LX/JR3;

    iget-object v1, p0, LX/JR9;->d:LX/JRA;

    iget-object v1, v1, LX/JRA;->a:LX/JQo;

    invoke-virtual {v1}, LX/JQo;->a()Z

    move-result v1

    .line 2692836
    iput-boolean v1, v0, LX/JR3;->a:Z

    .line 2692837
    iget-object v0, p0, LX/JR9;->a:LX/JR3;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 2692838
    iput-wide v2, v0, LX/JR3;->b:J

    .line 2692839
    iget-object v0, p0, LX/JR9;->a:LX/JR3;

    .line 2692840
    iput p2, v0, LX/JR3;->d:I

    .line 2692841
    iget-object v0, p0, LX/JR9;->a:LX/JR3;

    .line 2692842
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2692843
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2692844
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2692845
    new-instance v5, LX/JQk;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2692846
    iget-object p2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p2, p2

    .line 2692847
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v6, p2, v1}, LX/JQk;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2692848
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2692849
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2692850
    iput-object v1, v0, LX/JR3;->c:LX/0Px;

    .line 2692851
    iget-object v0, p0, LX/JR9;->b:LX/1Pm;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/JR9;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2692852
    return-void
.end method
