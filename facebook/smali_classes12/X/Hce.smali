.class public final LX/Hce;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8qq;


# instance fields
.field public final synthetic a:Landroid/view/View$OnClickListener;

.field public final synthetic b:LX/2km;

.field public final synthetic c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic f:LX/HcX;

.field public final synthetic g:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;Landroid/view/View$OnClickListener;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/model/GraphQLComment;Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;)V
    .locals 0

    .prologue
    .line 2487210
    iput-object p1, p0, LX/Hce;->g:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    iput-object p2, p0, LX/Hce;->a:Landroid/view/View$OnClickListener;

    iput-object p3, p0, LX/Hce;->b:LX/2km;

    iput-object p4, p0, LX/Hce;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p5, p0, LX/Hce;->d:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p6, p0, LX/Hce;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p7, p0, LX/Hce;->f:LX/HcX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2487211
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2487212
    iget-object v0, p0, LX/Hce;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2487213
    return-void
.end method

.method public final a(Landroid/view/View;LX/1zt;)V
    .locals 0

    .prologue
    .line 2487209
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2487184
    iget-object v0, p0, LX/Hce;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2487185
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 2487194
    iget-object v0, p0, LX/Hce;->b:LX/2km;

    check-cast v0, LX/2kl;

    invoke-interface {v0}, LX/2kl;->md_()LX/2jc;

    move-result-object v0

    iget-object v1, p0, LX/Hce;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    sget-object v2, LX/Cfc;->LIKE_COMMENT_TAP:LX/Cfc;

    invoke-interface {v0, v1, v2}, LX/2jc;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/Cfc;)V

    .line 2487195
    iget-object v0, p0, LX/Hce;->g:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->e:LX/9Cd;

    iget-object v1, p0, LX/Hce;->d:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/Hce;->d:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->E()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/9Cd;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2487196
    iget-object v1, p0, LX/Hce;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v0, p0, LX/Hce;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2487197
    iget-object v0, p0, LX/Hce;->f:LX/HcX;

    iget-object v1, p0, LX/Hce;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 2487198
    iput-object v1, v0, LX/HcX;->b:LX/03R;

    .line 2487199
    invoke-static {p1}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->b(Landroid/view/View;)LX/Hcl;

    move-result-object v7

    .line 2487200
    if-eqz v7, :cond_0

    .line 2487201
    iget-object v0, v7, LX/Hcl;->a:Lcom/facebook/feedback/ui/rows/views/CommentActionsView;

    move-object v0, v0

    .line 2487202
    if-nez v0, :cond_2

    .line 2487203
    :cond_0
    :goto_1
    return-void

    .line 2487204
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2487205
    :cond_2
    iget-object v0, v7, LX/Hcl;->a:Lcom/facebook/feedback/ui/rows/views/CommentActionsView;

    move-object v8, v0

    .line 2487206
    iget-object v0, p0, LX/Hce;->g:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    iget-object v1, p0, LX/Hce;->d:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/Hce;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {v1, v2}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLComment;Ljava/util/concurrent/atomic/AtomicBoolean;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    iget-object v2, p0, LX/Hce;->a:Landroid/view/View$OnClickListener;

    iget-object v3, p0, LX/Hce;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v4, p0, LX/Hce;->b:LX/2km;

    iget-object v5, p0, LX/Hce;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v6, p0, LX/Hce;->f:LX/HcX;

    .line 2487207
    invoke-static/range {v0 .. v6}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->a$redex0(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View$OnClickListener;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;)Ljava/util/List;

    move-result-object v9

    move-object v0, v9

    .line 2487208
    invoke-virtual {v7}, LX/Hcl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/8qs;->a(Ljava/util/List;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->setMetadataText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2487192
    iget-object v0, p0, LX/Hce;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2487193
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2487189
    iget-object v0, p0, LX/Hce;->b:LX/2km;

    check-cast v0, LX/2kl;

    invoke-interface {v0}, LX/2kl;->md_()LX/2jc;

    move-result-object v0

    iget-object v1, p0, LX/Hce;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    sget-object v2, LX/Cfc;->REPLY_COMMMENT_TAP:LX/Cfc;

    invoke-interface {v0, v1, v2}, LX/2jc;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/Cfc;)V

    .line 2487190
    iget-object v0, p0, LX/Hce;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2487191
    return-void
.end method

.method public final e(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2487186
    iget-object v0, p0, LX/Hce;->b:LX/2km;

    check-cast v0, LX/2kl;

    invoke-interface {v0}, LX/2kl;->md_()LX/2jc;

    move-result-object v0

    iget-object v1, p0, LX/Hce;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    sget-object v2, LX/Cfc;->SHARE_COMMENT_TAP:LX/Cfc;

    invoke-interface {v0, v1, v2}, LX/2jc;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/Cfc;)V

    .line 2487187
    iget-object v0, p0, LX/Hce;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2487188
    return-void
.end method
