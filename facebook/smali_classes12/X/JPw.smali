.class public final LX/JPw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/JPt;

.field public b:Z

.field public final c:J

.field public final d:Landroid/net/Uri;

.field public final e:Landroid/net/Uri;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/StringBuilder;

.field public final h:Ljava/lang/String;

.field public final i:Landroid/view/View$OnClickListener;

.field public final j:Landroid/graphics/PointF;

.field public final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/44w",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;",
            "Landroid/view/View$OnClickListener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JPt;ZJLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/StringBuilder;Landroid/view/View$OnClickListener;Landroid/graphics/PointF;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JPt;",
            "ZJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/StringBuilder;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/graphics/PointF;",
            "LX/0Px",
            "<",
            "LX/44w",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;",
            "Landroid/view/View$OnClickListener;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2690598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2690599
    iput-object p1, p0, LX/JPw;->a:LX/JPt;

    .line 2690600
    iput-boolean p2, p0, LX/JPw;->b:Z

    .line 2690601
    iput-wide p3, p0, LX/JPw;->c:J

    .line 2690602
    iput-object p5, p0, LX/JPw;->f:Ljava/lang/String;

    .line 2690603
    iput-object p6, p0, LX/JPw;->h:Ljava/lang/String;

    .line 2690604
    iput-object p7, p0, LX/JPw;->d:Landroid/net/Uri;

    .line 2690605
    iput-object p8, p0, LX/JPw;->e:Landroid/net/Uri;

    .line 2690606
    iput-object p9, p0, LX/JPw;->g:Ljava/lang/StringBuilder;

    .line 2690607
    iput-object p10, p0, LX/JPw;->i:Landroid/view/View$OnClickListener;

    .line 2690608
    iput-object p11, p0, LX/JPw;->j:Landroid/graphics/PointF;

    .line 2690609
    iput-object p12, p0, LX/JPw;->k:LX/0Px;

    .line 2690610
    return-void
.end method
