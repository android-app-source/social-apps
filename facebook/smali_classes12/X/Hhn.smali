.class public LX/Hhn;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:LX/HhW;

.field public final b:LX/0kL;

.field private final c:Landroid/preference/Preference$OnPreferenceClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/HhW;LX/0kL;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2496184
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2496185
    new-instance v0, LX/Hhm;

    invoke-direct {v0, p0}, LX/Hhm;-><init>(LX/Hhn;)V

    iput-object v0, p0, LX/Hhn;->c:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 2496186
    iput-object p2, p0, LX/Hhn;->a:LX/HhW;

    .line 2496187
    iput-object p3, p0, LX/Hhn;->b:LX/0kL;

    .line 2496188
    const v0, 0x7f083674

    invoke-virtual {p0, v0}, LX/Hhn;->setTitle(I)V

    .line 2496189
    iget-object v0, p0, LX/Hhn;->c:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {p0, v0}, LX/Hhn;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2496190
    return-void
.end method

.method public static a(LX/0QB;)LX/Hhn;
    .locals 4

    .prologue
    .line 2496191
    new-instance v3, LX/Hhn;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/HhW;->a(LX/0QB;)LX/HhW;

    move-result-object v1

    check-cast v1, LX/HhW;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-direct {v3, v0, v1, v2}, LX/Hhn;-><init>(Landroid/content/Context;LX/HhW;LX/0kL;)V

    .line 2496192
    move-object v0, v3

    .line 2496193
    return-object v0
.end method
