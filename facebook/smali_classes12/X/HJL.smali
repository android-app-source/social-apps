.class public LX/HJL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/17W;

.field private final c:LX/3mL;

.field private final d:LX/1DR;

.field public final e:LX/1nG;

.field private final f:LX/3AZ;

.field private final g:LX/HL4;

.field private final h:LX/HL8;

.field private final i:LX/HLE;

.field private final j:LX/HIQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17W;LX/3mL;LX/1DR;LX/1nG;LX/3AZ;LX/HL4;LX/HL8;LX/HLE;LX/HIQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2452449
    iput-object p1, p0, LX/HJL;->a:Landroid/content/Context;

    .line 2452450
    iput-object p2, p0, LX/HJL;->b:LX/17W;

    .line 2452451
    iput-object p3, p0, LX/HJL;->c:LX/3mL;

    .line 2452452
    iput-object p4, p0, LX/HJL;->d:LX/1DR;

    .line 2452453
    iput-object p5, p0, LX/HJL;->e:LX/1nG;

    .line 2452454
    iput-object p6, p0, LX/HJL;->f:LX/3AZ;

    .line 2452455
    iput-object p7, p0, LX/HJL;->g:LX/HL4;

    .line 2452456
    iput-object p8, p0, LX/HJL;->h:LX/HL8;

    .line 2452457
    iput-object p9, p0, LX/HJL;->i:LX/HLE;

    .line 2452458
    iput-object p10, p0, LX/HJL;->j:LX/HIQ;

    .line 2452459
    return-void
.end method

.method private static a(LX/HJL;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/2km;LX/0Px;)LX/1Di;
    .locals 4
    .param p1    # LX/1De;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/HLF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HLF;",
            "TE;",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$HScrollPageCardFields;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 2452460
    check-cast p4, LX/1Pr;

    invoke-interface {p4, p3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    .line 2452461
    const/4 v1, 0x0

    .line 2452462
    invoke-virtual {v0}, LX/HLG;->a()I

    move-result v2

    if-ltz v2, :cond_0

    invoke-virtual {v0}, LX/HLG;->a()I

    move-result v2

    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 2452463
    invoke-virtual {v0}, LX/HLG;->a()I

    move-result v0

    .line 2452464
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/16 v2, 0x8

    const v3, 0x7f0b163c

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, LX/HJL;->f:LX/3AZ;

    invoke-virtual {v2, p1}, LX/3AZ;->c(LX/1De;)LX/3Aa;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/3Aa;->b(Z)LX/3Aa;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/3Aa;->a(Landroid/text/TextUtils$TruncateAt;)LX/3Aa;

    move-result-object v2

    new-instance v3, LX/HJI;

    invoke-direct {v3, p0}, LX/HJI;-><init>(LX/HJL;)V

    .line 2452465
    iget-object p0, v2, LX/3Aa;->a:LX/8yo;

    iput-object v3, p0, LX/8yo;->b:LX/HJI;

    .line 2452466
    move-object v2, v2

    .line 2452467
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2452468
    invoke-interface {v3}, LX/9uc;->m()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->b()LX/3Ab;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3Aa;->a(LX/3Ab;)LX/3Aa;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/3Aa;->h(I)LX/3Aa;

    move-result-object v0

    const v2, 0x7f0b004e

    invoke-virtual {v0, v2}, LX/3Aa;->n(I)LX/3Aa;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/HJL;
    .locals 14

    .prologue
    .line 2452469
    const-class v1, LX/HJL;

    monitor-enter v1

    .line 2452470
    :try_start_0
    sget-object v0, LX/HJL;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452471
    sput-object v2, LX/HJL;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452472
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452473
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452474
    new-instance v3, LX/HJL;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v6

    check-cast v6, LX/3mL;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v7

    check-cast v7, LX/1DR;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v8

    check-cast v8, LX/1nG;

    invoke-static {v0}, LX/3AZ;->a(LX/0QB;)LX/3AZ;

    move-result-object v9

    check-cast v9, LX/3AZ;

    invoke-static {v0}, LX/HL4;->a(LX/0QB;)LX/HL4;

    move-result-object v10

    check-cast v10, LX/HL4;

    const-class v11, LX/HL8;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/HL8;

    const-class v12, LX/HLE;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/HLE;

    invoke-static {v0}, LX/HIQ;->a(LX/0QB;)LX/HIQ;

    move-result-object v13

    check-cast v13, LX/HIQ;

    invoke-direct/range {v3 .. v13}, LX/HJL;-><init>(Landroid/content/Context;LX/17W;LX/3mL;LX/1DR;LX/1nG;LX/3AZ;LX/HL4;LX/HL8;LX/HLE;LX/HIQ;)V

    .line 2452475
    move-object v0, v3

    .line 2452476
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452477
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452478
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452479
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/HJL;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/2km;)LX/HL7;
    .locals 7
    .param p1    # LX/1De;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/HLF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HLF;",
            "TE;)",
            "LX/HL7;"
        }
    .end annotation

    .prologue
    .line 2452480
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2452481
    const/4 v0, 0x0

    move v1, v0

    .line 2452482
    :goto_0
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452483
    invoke-interface {v0}, LX/9uc;->aE()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2452484
    new-instance v3, LX/HL6;

    .line 2452485
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452486
    invoke-interface {v0}, LX/9uc;->aE()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 2452487
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452488
    invoke-interface {v0}, LX/9uc;->aE()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v4, v0, v1}, LX/HL6;-><init>(Ljava/lang/String;Landroid/net/Uri;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2452489
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2452490
    :cond_0
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 2452491
    move-object v0, p4

    .line 2452492
    check-cast v0, LX/1Pr;

    invoke-interface {v0, p3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    iget-object v0, v0, LX/HLG;->d:LX/25L;

    .line 2452493
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b2328

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 2452494
    iput v3, v1, LX/3mP;->b:I

    .line 2452495
    move-object v1, v1

    .line 2452496
    iput-object v0, v1, LX/3mP;->d:LX/25L;

    .line 2452497
    move-object v0, v1

    .line 2452498
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2452499
    invoke-static {v1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v1

    .line 2452500
    iput-object v1, v0, LX/3mP;->e:LX/0jW;

    .line 2452501
    move-object v1, v0

    .line 2452502
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x4

    if-ge v0, v3, :cond_1

    const/4 v0, 0x1

    .line 2452503
    :goto_1
    iput-boolean v0, v1, LX/3mP;->a:Z

    .line 2452504
    move-object v0, v1

    .line 2452505
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v4

    .line 2452506
    iget-object v0, p0, LX/HJL;->h:LX/HL8;

    move-object v1, p1

    move-object v3, p4

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, LX/HL8;->a(Landroid/content/Context;LX/0Px;Ljava/lang/Object;LX/25M;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;)LX/HL7;

    move-result-object v0

    return-object v0

    .line 2452507
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(LX/HJL;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/2km;LX/0Px;)LX/HLD;
    .locals 6
    .param p1    # LX/1De;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/HLF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HLF;",
            "TE;",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$HScrollPageCardFields;",
            ">;)",
            "LX/HLD;"
        }
    .end annotation

    .prologue
    .line 2452508
    move-object v0, p4

    check-cast v0, LX/1Pr;

    invoke-interface {v0, p3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    iget-object v0, v0, LX/HLG;->e:LX/25L;

    .line 2452509
    new-instance v1, LX/HJJ;

    invoke-direct {v1, p0, p4, p3, p2}, LX/HJJ;-><init>(LX/HJL;LX/2km;LX/HLF;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2452510
    new-instance v2, LX/HJK;

    invoke-direct {v2, p0, p4, p3, p2}, LX/HJK;-><init>(LX/HJL;LX/2km;LX/HLF;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2452511
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b232a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    .line 2452512
    iput v4, v3, LX/3mP;->b:I

    .line 2452513
    move-object v3, v3

    .line 2452514
    iput-object v1, v3, LX/3mP;->g:LX/25K;

    .line 2452515
    move-object v1, v3

    .line 2452516
    iput-object v0, v1, LX/3mP;->d:LX/25L;

    .line 2452517
    move-object v0, v1

    .line 2452518
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2452519
    invoke-static {v1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v1

    .line 2452520
    iput-object v1, v0, LX/3mP;->e:LX/0jW;

    .line 2452521
    move-object v0, v0

    .line 2452522
    iput-object v2, v0, LX/3mP;->f:LX/HJK;

    .line 2452523
    move-object v0, v0

    .line 2452524
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v4

    .line 2452525
    iget-object v0, p0, LX/HJL;->i:LX/HLE;

    move-object v1, p1

    move-object v2, p5

    move-object v3, p4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, LX/HLE;->a(Landroid/content/Context;LX/0Px;Ljava/lang/Object;LX/25M;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/HLD;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/2km;)LX/1Dg;
    .locals 10
    .param p2    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/HLF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HLF;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2452526
    move-object v0, p4

    check-cast v0, LX/1Pr;

    invoke-interface {v0, p3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    .line 2452527
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2452528
    invoke-interface {v1}, LX/9uc;->aE()LX/0Px;

    move-result-object v1

    iget v2, v0, LX/HLG;->f:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 2452529
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2452530
    invoke-interface {v1}, LX/9uc;->aE()LX/0Px;

    move-result-object v1

    iget v2, v0, LX/HLG;->f:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;->e()Ljava/lang/String;

    move-result-object v8

    .line 2452531
    invoke-static {p0, p1, p2, p3, p4}, LX/HJL;->b(LX/HJL;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/2km;)LX/HL7;

    move-result-object v1

    .line 2452532
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0097

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    const v4, 0x7f0b163c

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    iget-object v3, p0, LX/HJL;->c:LX/3mL;

    invoke-virtual {v3, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    .line 2452533
    iget-boolean v0, v0, LX/HLG;->a:Z

    if-nez v0, :cond_3

    .line 2452534
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2452535
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452536
    invoke-interface {v0}, LX/9uc;->m()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v6

    .line 2452537
    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;

    .line 2452538
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2452539
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2452540
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 2452541
    invoke-static/range {v0 .. v5}, LX/HJL;->a(LX/HJL;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/2km;LX/0Px;)LX/1Di;

    move-result-object v0

    invoke-interface {v9, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2452542
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2452543
    iget-object v0, p0, LX/HJL;->d:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b163c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 2452544
    iget-object v0, p0, LX/HJL;->j:LX/HIQ;

    invoke-virtual {v0, p1}, LX/HIQ;->c(LX/1De;)LX/HIO;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/HIO;->a(LX/2km;)LX/HIO;

    move-result-object v2

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-virtual {v2, v0}, LX/HIO;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/HIO;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/HIO;->h(I)LX/HIO;

    move-result-object v0

    invoke-interface {v9, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2452545
    :goto_1
    iget-object v0, p0, LX/HJL;->g:LX/HL4;

    const/4 v1, 0x0

    .line 2452546
    new-instance v2, LX/HL3;

    invoke-direct {v2, v0}, LX/HL3;-><init>(LX/HL4;)V

    .line 2452547
    iget-object v3, v0, LX/HL4;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HL2;

    .line 2452548
    if-nez v3, :cond_1

    .line 2452549
    new-instance v3, LX/HL2;

    invoke-direct {v3, v0}, LX/HL2;-><init>(LX/HL4;)V

    .line 2452550
    :cond_1
    invoke-static {v3, p1, v1, v1, v2}, LX/HL2;->a$redex0(LX/HL2;LX/1De;IILX/HL3;)V

    .line 2452551
    move-object v2, v3

    .line 2452552
    move-object v1, v2

    .line 2452553
    move-object v0, v1

    .line 2452554
    iget-object v1, v0, LX/HL2;->a:LX/HL3;

    iput-object v7, v1, LX/HL3;->b:Ljava/lang/String;

    .line 2452555
    iget-object v1, v0, LX/HL2;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2452556
    move-object v0, v0

    .line 2452557
    iget-object v1, v0, LX/HL2;->a:LX/HL3;

    iput-object v8, v1, LX/HL3;->a:Ljava/lang/String;

    .line 2452558
    iget-object v1, v0, LX/HL2;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2452559
    move-object v0, v0

    .line 2452560
    iget-object v1, v0, LX/HL2;->a:LX/HL3;

    iput-object p2, v1, LX/HL3;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452561
    iget-object v1, v0, LX/HL2;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2452562
    move-object v0, v0

    .line 2452563
    invoke-interface {v9, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 2452564
    invoke-static/range {v0 .. v5}, LX/HJL;->b(LX/HJL;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;LX/2km;LX/0Px;)LX/HLD;

    move-result-object v0

    .line 2452565
    iget-object v1, p0, LX/HJL;->c:LX/3mL;

    invoke-virtual {v1, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    invoke-interface {v9, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    goto :goto_1

    .line 2452566
    :cond_3
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x103005b

    invoke-static {p1, v1, v2}, LX/5Jt;->a(LX/1De;II)LX/5Jr;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b231e

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b163c

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b231e

    invoke-interface {v1, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    move-object v0, v0

    .line 2452567
    invoke-interface {v9, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto/16 :goto_1
.end method
