.class public LX/JVE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JVC;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JVG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2700376
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JVE;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JVG;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700377
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2700378
    iput-object p1, p0, LX/JVE;->b:LX/0Ot;

    .line 2700379
    return-void
.end method

.method public static a(LX/0QB;)LX/JVE;
    .locals 4

    .prologue
    .line 2700380
    const-class v1, LX/JVE;

    monitor-enter v1

    .line 2700381
    :try_start_0
    sget-object v0, LX/JVE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700382
    sput-object v2, LX/JVE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700383
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700384
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700385
    new-instance v3, LX/JVE;

    const/16 p0, 0x2090

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JVE;-><init>(LX/0Ot;)V

    .line 2700386
    move-object v0, v3

    .line 2700387
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700388
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700389
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700390
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2700391
    check-cast p2, LX/JVD;

    .line 2700392
    iget-object v0, p0, LX/JVE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JVG;

    iget-object v1, p2, LX/JVD;->a:LX/1Pf;

    iget-object v2, p2, LX/JVD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700393
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2700394
    move-object v4, v3

    check-cast v4, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    .line 2700395
    new-instance v3, LX/JVF;

    invoke-direct {v3, v0, v4}, LX/JVF;-><init>(LX/JVG;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)V

    .line 2700396
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v6

    .line 2700397
    iput-object v6, v5, LX/3mP;->d:LX/25L;

    .line 2700398
    move-object v5, v5

    .line 2700399
    iput-object v4, v5, LX/3mP;->e:LX/0jW;

    .line 2700400
    move-object v5, v5

    .line 2700401
    const/16 v6, 0x8

    .line 2700402
    iput v6, v5, LX/3mP;->b:I

    .line 2700403
    move-object v5, v5

    .line 2700404
    iput-object v2, v5, LX/3mP;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700405
    move-object v5, v5

    .line 2700406
    iput-object v3, v5, LX/3mP;->g:LX/25K;

    .line 2700407
    move-object v3, v5

    .line 2700408
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 2700409
    iget-object v3, v0, LX/JVG;->b:LX/JV6;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;->a()LX/0Px;

    move-result-object v5

    move-object v4, p1

    move-object v6, v2

    move-object v7, v1

    invoke-virtual/range {v3 .. v8}, LX/JV6;->a(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;LX/25M;)LX/JV5;

    move-result-object v3

    .line 2700410
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JVG;->a:LX/3mL;

    invoke-virtual {v5, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2700411
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2700412
    invoke-static {}, LX/1dS;->b()V

    .line 2700413
    const/4 v0, 0x0

    return-object v0
.end method
