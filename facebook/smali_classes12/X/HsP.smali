.class public final synthetic LX/HsP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2514389
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->values()[Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/HsP;->a:[I

    :try_start_0
    sget-object v0, LX/HsP;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->MARRIED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, LX/HsP;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ENGAGED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v0, LX/HsP;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->OTHER:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v0, LX/HsP;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v0, LX/HsP;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->STARTED_JOB:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, LX/HsP;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->GRADUATED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    goto :goto_5

    :catch_1
    goto :goto_4

    :catch_2
    goto :goto_3

    :catch_3
    goto :goto_2

    :catch_4
    goto :goto_1

    :catch_5
    goto :goto_0
.end method
