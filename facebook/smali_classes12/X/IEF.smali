.class public final LX/IEF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DHs;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DHs;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DHs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2551724
    sget-object v0, LX/DHs;->ALL_FRIENDS:LX/DHs;

    sget-object v1, LX/DHs;->RECENTLY_ADDED_FRIENDS:LX/DHs;

    sget-object v2, LX/DHs;->FRIENDS_WITH_NEW_POSTS:LX/DHs;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IEF;->a:LX/0Px;

    .line 2551725
    sget-object v0, LX/DHs;->ALL_FRIENDS:LX/DHs;

    sget-object v1, LX/DHs;->RECENTLY_ADDED_FRIENDS:LX/DHs;

    sget-object v2, LX/DHs;->FRIENDS_WITH_NEW_POSTS:LX/DHs;

    sget-object v3, LX/DHs;->PYMK:LX/DHs;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IEF;->b:LX/0Px;

    .line 2551726
    sget-object v0, LX/DHs;->SUGGESTIONS:LX/DHs;

    sget-object v1, LX/DHs;->MUTUAL_FRIENDS:LX/DHs;

    sget-object v2, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IEF;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2551727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
