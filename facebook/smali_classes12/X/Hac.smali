.class public final enum LX/Hac;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hac;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hac;

.field public static final enum COLLAPSE_COMPLETED:LX/Hac;

.field public static final enum COLLAPSE_STARTED:LX/Hac;

.field public static final enum EXPAND_COMPLETED:LX/Hac;

.field public static final enum EXPAND_STARTED:LX/Hac;

.field public static final enum SCROLLING:LX/Hac;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2484165
    new-instance v0, LX/Hac;

    const-string v1, "EXPAND_STARTED"

    invoke-direct {v0, v1, v2}, LX/Hac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hac;->EXPAND_STARTED:LX/Hac;

    .line 2484166
    new-instance v0, LX/Hac;

    const-string v1, "EXPAND_COMPLETED"

    invoke-direct {v0, v1, v3}, LX/Hac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hac;->EXPAND_COMPLETED:LX/Hac;

    .line 2484167
    new-instance v0, LX/Hac;

    const-string v1, "COLLAPSE_STARTED"

    invoke-direct {v0, v1, v4}, LX/Hac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hac;->COLLAPSE_STARTED:LX/Hac;

    .line 2484168
    new-instance v0, LX/Hac;

    const-string v1, "COLLAPSE_COMPLETED"

    invoke-direct {v0, v1, v5}, LX/Hac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hac;->COLLAPSE_COMPLETED:LX/Hac;

    .line 2484169
    new-instance v0, LX/Hac;

    const-string v1, "SCROLLING"

    invoke-direct {v0, v1, v6}, LX/Hac;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hac;->SCROLLING:LX/Hac;

    .line 2484170
    const/4 v0, 0x5

    new-array v0, v0, [LX/Hac;

    sget-object v1, LX/Hac;->EXPAND_STARTED:LX/Hac;

    aput-object v1, v0, v2

    sget-object v1, LX/Hac;->EXPAND_COMPLETED:LX/Hac;

    aput-object v1, v0, v3

    sget-object v1, LX/Hac;->COLLAPSE_STARTED:LX/Hac;

    aput-object v1, v0, v4

    sget-object v1, LX/Hac;->COLLAPSE_COMPLETED:LX/Hac;

    aput-object v1, v0, v5

    sget-object v1, LX/Hac;->SCROLLING:LX/Hac;

    aput-object v1, v0, v6

    sput-object v0, LX/Hac;->$VALUES:[LX/Hac;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2484164
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hac;
    .locals 1

    .prologue
    .line 2484172
    const-class v0, LX/Hac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hac;

    return-object v0
.end method

.method public static values()[LX/Hac;
    .locals 1

    .prologue
    .line 2484171
    sget-object v0, LX/Hac;->$VALUES:[LX/Hac;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hac;

    return-object v0
.end method
