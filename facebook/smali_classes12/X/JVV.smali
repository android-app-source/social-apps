.class public final LX/JVV;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStory;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/3mj;

.field public final synthetic d:Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;)V
    .locals 1

    .prologue
    .line 2700879
    iput-object p1, p0, LX/JVV;->d:Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;

    .line 2700880
    move-object v0, p1

    .line 2700881
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2700882
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2700883
    const-string v0, "PhotoChainingItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2700884
    if-ne p0, p1, :cond_1

    .line 2700885
    :cond_0
    :goto_0
    return v0

    .line 2700886
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2700887
    goto :goto_0

    .line 2700888
    :cond_3
    check-cast p1, LX/JVV;

    .line 2700889
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2700890
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2700891
    if-eq v2, v3, :cond_0

    .line 2700892
    iget-object v2, p0, LX/JVV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JVV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p1, LX/JVV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2700893
    goto :goto_0

    .line 2700894
    :cond_5
    iget-object v2, p1, LX/JVV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_4

    .line 2700895
    :cond_6
    iget-object v2, p0, LX/JVV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JVV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JVV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2700896
    goto :goto_0

    .line 2700897
    :cond_8
    iget-object v2, p1, LX/JVV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2700898
    :cond_9
    iget-object v2, p0, LX/JVV;->c:LX/3mj;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JVV;->c:LX/3mj;

    iget-object v3, p1, LX/JVV;->c:LX/3mj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2700899
    goto :goto_0

    .line 2700900
    :cond_a
    iget-object v2, p1, LX/JVV;->c:LX/3mj;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
