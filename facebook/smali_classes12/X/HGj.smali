.class public LX/HGj;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/support/v7/widget/RecyclerView;

.field private c:Landroid/view/LayoutInflater;

.field public d:Z

.field private final e:LX/E93;

.field public final f:LX/Dvb;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 2446489
    sget v0, LX/Dva;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget v1, LX/Dva;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, LX/Dva;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v3, LX/Dva;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v4, LX/Dva;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget v5, LX/Dva;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Integer;

    const/4 v7, 0x0

    sget v8, LX/Dva;->g:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget v8, LX/Dva;->h:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/HGj;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;LX/E93;LX/Dvb;Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2446490
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2446491
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HGj;->d:Z

    .line 2446492
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2446493
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2446494
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2446495
    iput-object p1, p0, LX/HGj;->b:Landroid/support/v7/widget/RecyclerView;

    .line 2446496
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/HGj;->c:Landroid/view/LayoutInflater;

    .line 2446497
    iput-object p2, p0, LX/HGj;->e:LX/E93;

    .line 2446498
    iput-object p3, p0, LX/HGj;->f:LX/Dvb;

    .line 2446499
    iput-object p5, p0, LX/HGj;->g:Landroid/view/View;

    .line 2446500
    iput-object p4, p0, LX/HGj;->h:Landroid/view/View;

    .line 2446501
    return-void
.end method

.method private f()I
    .locals 1

    .prologue
    .line 2446504
    iget-object v0, p0, LX/HGj;->f:LX/Dvb;

    invoke-virtual {v0}, LX/Dvb;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HGj;->f:LX/Dvb;

    invoke-virtual {v0}, LX/Dvb;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()I
    .locals 1

    .prologue
    .line 2446502
    invoke-direct {p0}, LX/HGj;->f()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static h(LX/HGj;)I
    .locals 1

    .prologue
    .line 2446503
    iget-boolean v0, p0, LX/HGj;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()I
    .locals 1

    .prologue
    .line 2446487
    iget-object v0, p0, LX/HGj;->e:LX/E93;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    return v0
.end method

.method private j()I
    .locals 2

    .prologue
    .line 2446488
    invoke-direct {p0}, LX/HGj;->i()I

    move-result v0

    invoke-direct {p0}, LX/HGj;->g()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static k(LX/HGj;)I
    .locals 2

    .prologue
    .line 2446486
    invoke-direct {p0}, LX/HGj;->j()I

    move-result v0

    invoke-direct {p0}, LX/HGj;->f()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2446474
    sget v0, LX/HGi;->a:I

    if-ne p2, v0, :cond_0

    .line 2446475
    iget-object v0, p0, LX/HGj;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030e9a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2446476
    new-instance v0, LX/HGf;

    invoke-direct {v0, v1}, LX/HGf;-><init>(Landroid/view/View;)V

    .line 2446477
    :goto_0
    return-object v0

    .line 2446478
    :cond_0
    sget v0, LX/HGi;->c:I

    if-ne p2, v0, :cond_1

    .line 2446479
    new-instance v0, LX/HGh;

    iget-object v1, p0, LX/HGj;->g:Landroid/view/View;

    invoke-direct {v0, v1}, LX/HGh;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2446480
    :cond_1
    sget v0, LX/HGi;->b:I

    if-ne p2, v0, :cond_2

    .line 2446481
    new-instance v0, LX/HGh;

    iget-object v1, p0, LX/HGj;->h:Landroid/view/View;

    invoke-direct {v0, v1}, LX/HGh;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2446482
    :cond_2
    sget-object v0, LX/HGj;->a:LX/0Rf;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 2446483
    if-eqz v0, :cond_3

    .line 2446484
    new-instance v0, LX/HGg;

    iget-object v1, p0, LX/HGj;->f:LX/Dvb;

    invoke-virtual {v1, p1, p2}, LX/Dvb;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HGg;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2446485
    :cond_3
    iget-object v0, p0, LX/HGj;->e:LX/E93;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1OD;)V
    .locals 1

    .prologue
    .line 2446471
    invoke-super {p0, p1}, LX/1OM;->a(LX/1OD;)V

    .line 2446472
    iget-object v0, p0, LX/HGj;->e:LX/E93;

    invoke-virtual {v0, p1}, LX/1OM;->a(LX/1OD;)V

    .line 2446473
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2446464
    invoke-direct {p0}, LX/HGj;->i()I

    move-result v0

    if-ge p2, v0, :cond_1

    .line 2446465
    iget-object v0, p0, LX/HGj;->e:LX/E93;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(LX/1a1;I)V

    .line 2446466
    :cond_0
    :goto_0
    return-void

    .line 2446467
    :cond_1
    invoke-direct {p0}, LX/HGj;->j()I

    move-result v0

    if-lt p2, v0, :cond_0

    .line 2446468
    invoke-static {p0}, LX/HGj;->k(LX/HGj;)I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 2446469
    invoke-direct {p0}, LX/HGj;->j()I

    move-result v0

    sub-int v0, p2, v0

    .line 2446470
    iget-object v1, p0, LX/HGj;->f:LX/Dvb;

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    iget-object v3, p0, LX/HGj;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0, v2, v3}, LX/Dvb;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_0
.end method

.method public final b(LX/1OD;)V
    .locals 1

    .prologue
    .line 2446461
    invoke-super {p0, p1}, LX/1OM;->b(LX/1OD;)V

    .line 2446462
    iget-object v0, p0, LX/HGj;->e:LX/E93;

    invoke-virtual {v0, p1}, LX/1OM;->b(LX/1OD;)V

    .line 2446463
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2446449
    invoke-direct {p0}, LX/HGj;->i()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2446450
    iget-object v0, p0, LX/HGj;->e:LX/E93;

    invoke-virtual {v0, p1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2446451
    :goto_0
    return v0

    .line 2446452
    :cond_0
    invoke-direct {p0}, LX/HGj;->j()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2446453
    sget v0, LX/HGi;->a:I

    goto :goto_0

    .line 2446454
    :cond_1
    invoke-static {p0}, LX/HGj;->k(LX/HGj;)I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 2446455
    invoke-direct {p0}, LX/HGj;->j()I

    move-result v0

    sub-int v0, p1, v0

    .line 2446456
    iget-object v1, p0, LX/HGj;->f:LX/Dvb;

    invoke-virtual {v1, v0}, LX/Dvb;->b(I)I

    move-result v0

    goto :goto_0

    .line 2446457
    :cond_2
    invoke-static {p0}, LX/HGj;->k(LX/HGj;)I

    move-result v0

    invoke-static {p0}, LX/HGj;->h(LX/HGj;)I

    move-result v1

    add-int/2addr v0, v1

    move v0, v0

    .line 2446458
    if-ge p1, v0, :cond_3

    .line 2446459
    sget v0, LX/HGi;->b:I

    goto :goto_0

    .line 2446460
    :cond_3
    sget v0, LX/HGi;->c:I

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2446448
    iget-object v0, p0, LX/HGj;->e:LX/E93;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    invoke-direct {p0}, LX/HGj;->g()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0}, LX/HGj;->f()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p0}, LX/HGj;->h(LX/HGj;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
