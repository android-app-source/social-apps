.class public final LX/IVQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/IVR;


# direct methods
.method public constructor <init>(LX/IVR;)V
    .locals 0

    .prologue
    .line 2581913
    iput-object p1, p0, LX/IVQ;->a:LX/IVR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2581914
    const-class v0, LX/IVR;

    const-string v1, "Failure while fetching more substories : "

    invoke-static {v0, v1, p1}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2581915
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2581916
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 2581917
    iget-object v0, p0, LX/IVQ;->a:LX/IVR;

    iget-object v0, v0, LX/IVR;->b:LX/0bH;

    new-instance v1, LX/1Ne;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v1, p1}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2581918
    :cond_0
    return-void
.end method
