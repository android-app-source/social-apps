.class public LX/Izq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Izq;


# instance fields
.field private final a:LX/03V;

.field private final b:LX/IzO;


# direct methods
.method public constructor <init>(LX/03V;LX/IzO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2635689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635690
    iput-object p1, p0, LX/Izq;->a:LX/03V;

    .line 2635691
    iput-object p2, p0, LX/Izq;->b:LX/IzO;

    .line 2635692
    return-void
.end method

.method public static a(LX/0QB;)LX/Izq;
    .locals 5

    .prologue
    .line 2635693
    sget-object v0, LX/Izq;->c:LX/Izq;

    if-nez v0, :cond_1

    .line 2635694
    const-class v1, LX/Izq;

    monitor-enter v1

    .line 2635695
    :try_start_0
    sget-object v0, LX/Izq;->c:LX/Izq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2635696
    if-eqz v2, :cond_0

    .line 2635697
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2635698
    new-instance p0, LX/Izq;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v4

    check-cast v4, LX/IzO;

    invoke-direct {p0, v3, v4}, LX/Izq;-><init>(LX/03V;LX/IzO;)V

    .line 2635699
    move-object v0, p0

    .line 2635700
    sput-object v0, LX/Izq;->c:LX/Izq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635701
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2635702
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635703
    :cond_1
    sget-object v0, LX/Izq;->c:LX/Izq;

    return-object v0

    .line 2635704
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2635705
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/03R;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 2635706
    const-string v0, "getRecipientEligibility"

    const v1, -0x1eaa5bb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635707
    :try_start_0
    iget-object v0, p0, LX/Izq;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2635708
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v3, LX/IzW;->b:LX/0U1;

    .line 2635709
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635710
    aput-object v3, v2, v1

    .line 2635711
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/IzW;->a:LX/0U1;

    .line 2635712
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635713
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2635714
    const-string v1, "recipient_eligibility"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2635715
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v8, :cond_0

    .line 2635716
    iget-object v0, p0, LX/Izq;->a:LX/03V;

    const-string v2, "DbRecipientEligibilityHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RecipientEligibilityTable table should only have one row for a given recipient IDbut it has "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635717
    sget-object v0, LX/03R;->UNSET:LX/03R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635718
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635719
    const v1, 0x8d77590

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 2635720
    :cond_0
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 2635721
    sget-object v0, LX/03R;->UNSET:LX/03R;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2635722
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2635723
    const v1, -0x4801d7eb

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635724
    :cond_1
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2635725
    sget-object v0, LX/IzW;->b:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2635726
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 2635727
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2635728
    const v1, 0x7ca97b72

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635729
    :catchall_0
    move-exception v0

    :try_start_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2635730
    :catchall_1
    move-exception v0

    const v1, -0x23da723f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 2635731
    const-string v0, "setRecipientEligibility"

    const v1, -0x327ed8c3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635732
    :try_start_0
    iget-object v0, p0, LX/Izq;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635733
    const v0, -0x769290e4

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635734
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2635735
    sget-object v2, LX/IzW;->a:LX/0U1;

    .line 2635736
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635737
    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635738
    sget-object v2, LX/IzW;->b:LX/0U1;

    .line 2635739
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635740
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635741
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/IzW;->a:LX/0U1;

    .line 2635742
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635743
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2635744
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 2635745
    const-string v4, "recipient_eligibility"

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2635746
    if-nez v2, :cond_0

    .line 2635747
    const-string v2, "recipient_eligibility"

    const/4 v3, 0x0

    const v4, 0x6911b679

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x67c25311

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2635748
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2635749
    const v0, -0x25ce3fde

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2635750
    :goto_0
    const v0, 0x46fc8b04

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635751
    return-void

    .line 2635752
    :catch_0
    move-exception v0

    .line 2635753
    :try_start_3
    iget-object v2, p0, LX/Izq;->a:LX/03V;

    const-string v3, "DbRecipientEligibilityHandler"

    const-string v4, "A SQLException occurred when trying to insert into the database"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635754
    const v0, 0x5a176d8d

    :try_start_4
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2635755
    :catchall_0
    move-exception v0

    const v1, 0x48a877bf

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2635756
    :catchall_1
    move-exception v0

    const v2, 0x1b97545d

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
