.class public final LX/HJb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HJc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public b:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/HJc;


# direct methods
.method public constructor <init>(LX/HJc;)V
    .locals 1

    .prologue
    .line 2452984
    iput-object p1, p0, LX/HJb;->c:LX/HJc;

    .line 2452985
    move-object v0, p1

    .line 2452986
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2452987
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2452988
    const-string v0, "PageInfoWriteFirstReviewUnitComponentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2452989
    if-ne p0, p1, :cond_1

    .line 2452990
    :cond_0
    :goto_0
    return v0

    .line 2452991
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2452992
    goto :goto_0

    .line 2452993
    :cond_3
    check-cast p1, LX/HJb;

    .line 2452994
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2452995
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2452996
    if-eq v2, v3, :cond_0

    .line 2452997
    iget-object v2, p0, LX/HJb;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HJb;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HJb;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2452998
    goto :goto_0

    .line 2452999
    :cond_5
    iget-object v2, p1, LX/HJb;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_4

    .line 2453000
    :cond_6
    iget-object v2, p0, LX/HJb;->b:LX/2km;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/HJb;->b:LX/2km;

    iget-object v3, p1, LX/HJb;->b:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2453001
    goto :goto_0

    .line 2453002
    :cond_7
    iget-object v2, p1, LX/HJb;->b:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
