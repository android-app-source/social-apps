.class public final LX/ILC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;",
        ">;",
        "LX/ILO;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ILF;


# direct methods
.method public constructor <init>(LX/ILF;)V
    .locals 0

    .prologue
    .line 2567735
    iput-object p1, p0, LX/ILC;->a:LX/ILF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2567721
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2567722
    if-eqz p1, :cond_0

    .line 2567723
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567724
    if-eqz v0, :cond_0

    .line 2567725
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567726
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2567727
    :cond_0
    const/4 v0, 0x0

    .line 2567728
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, LX/ILO;

    .line 2567729
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567730
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;->a()LX/0Px;

    move-result-object v2

    .line 2567731
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567732
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2567733
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567734
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, LX/ILO;-><init>(LX/0Px;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0
.end method
