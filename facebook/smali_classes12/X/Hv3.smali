.class public LX/Hv3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsKeyboardUp;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
        "DerivedData::",
        "LX/5RE;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field public final b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Hqq;

.field public final d:LX/IF2;

.field public final e:LX/0ad;

.field public final f:LX/IF0;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0il;LX/0zw;LX/Hqq;LX/IF2;LX/IF1;LX/0ad;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Hqq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;",
            "Lcom/facebook/composer/stickerpost/upsell/ComposerStickerUpsellController$StickerUpsellPickerLaunchDelegate;",
            "LX/IF2;",
            "LX/IF1;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2518212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518213
    iput-object p1, p0, LX/Hv3;->a:LX/0il;

    .line 2518214
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zw;

    iput-object v0, p0, LX/Hv3;->b:LX/0zw;

    .line 2518215
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hqq;

    iput-object v0, p0, LX/Hv3;->c:LX/Hqq;

    .line 2518216
    iput-object p4, p0, LX/Hv3;->d:LX/IF2;

    .line 2518217
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2518218
    new-instance p2, LX/IF0;

    invoke-static {p5}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    invoke-direct {p2, v0, p1}, LX/IF0;-><init>(Ljava/lang/String;LX/0Zb;)V

    .line 2518219
    move-object v0, p2

    .line 2518220
    iput-object v0, p0, LX/Hv3;->f:LX/IF0;

    .line 2518221
    iput-object p6, p0, LX/Hv3;->e:LX/0ad;

    .line 2518222
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 5

    .prologue
    .line 2518223
    sget-object v0, LX/Hv2;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2518224
    :goto_0
    return-void

    .line 2518225
    :pswitch_0
    const/4 v1, 0x0

    .line 2518226
    iget-object v0, p0, LX/Hv3;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isInlineSproutsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hv3;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2518227
    :cond_0
    iget-object v0, p0, LX/Hv3;->a:LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v2, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    if-eq v0, v2, :cond_1

    iget-object v0, p0, LX/Hv3;->a:LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v2, LX/5RF;->STICKER:LX/5RF;

    if-ne v0, v2, :cond_6

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2518228
    if-eqz v0, :cond_5

    .line 2518229
    iget-object v2, p0, LX/Hv3;->d:LX/IF2;

    iget-object v0, p0, LX/Hv3;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j2;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    .line 2518230
    iget-object v3, v2, LX/IF2;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 2518231
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 2518232
    const/4 p1, 0x2

    invoke-virtual {v3, p1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    sget-object p1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 2518233
    iget-object p1, v2, LX/IF2;->a:Ljava/util/Set;

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 2518234
    :goto_2
    move-object v0, v3

    .line 2518235
    iput-object v0, p0, LX/Hv3;->g:Ljava/lang/String;

    .line 2518236
    iget-object v0, p0, LX/Hv3;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 2518237
    :goto_3
    move v0, v0

    .line 2518238
    if-eqz v0, :cond_3

    .line 2518239
    iget-object v0, p0, LX/Hv3;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2518240
    iget-object v0, p0, LX/Hv3;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d2d9f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2518241
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0814b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2518242
    iget-object v2, p0, LX/Hv3;->e:LX/0ad;

    sget-char v3, LX/IEy;->b:C

    invoke-interface {v2, v3, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2518243
    :cond_2
    iget-object v0, p0, LX/Hv3;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2518244
    iget-object v0, p0, LX/Hv3;->f:LX/IF0;

    iget-object v1, p0, LX/Hv3;->g:Ljava/lang/String;

    .line 2518245
    iget-object v2, v0, LX/IF0;->a:LX/0Zb;

    sget-object v3, LX/IEz;->STICKER_UPSELL_BUTTON_IMPRESSION:LX/IEz;

    invoke-static {v0, v3}, LX/IF0;->a(LX/IF0;LX/IEz;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "sticker_tag"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2518246
    iget-object v0, p0, LX/Hv3;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/Hv1;

    invoke-direct {v1, p0}, LX/Hv1;-><init>(LX/Hv3;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2518247
    :cond_3
    iget-object v0, p0, LX/Hv3;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 2518248
    goto :goto_3

    :cond_5
    move v0, v1

    .line 2518249
    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    move-object v3, v4

    .line 2518250
    goto :goto_2

    :cond_8
    move-object v3, v4

    .line 2518251
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2518252
    return-void
.end method
