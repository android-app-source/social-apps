.class public final LX/J63;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V
    .locals 0

    .prologue
    .line 2649223
    iput-object p1, p0, LX/J63;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    .line 2649225
    add-int v0, p2, p3

    add-int/lit8 v0, v0, 0x8

    if-le v0, p4, :cond_0

    iget-object v0, p0, LX/J63;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-boolean v0, v0, LX/J4L;->k:Z

    if-eqz v0, :cond_0

    .line 2649226
    iget-object v0, p0, LX/J63;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->t:LX/J4E;

    iget-object v1, p0, LX/J63;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->B:Ljava/lang/String;

    const/16 v2, 0xf

    iget-object v3, p0, LX/J63;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v3, v3, LX/J4L;->j:Ljava/lang/String;

    iget-object v4, p0, LX/J63;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v4, v4, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->O:LX/0Vd;

    .line 2649227
    iget-object p0, v0, LX/J4E;->a:LX/1Ck;

    sget-object p1, LX/J4D;->FETCH_REVIEW_DATA:LX/J4D;

    new-instance p2, LX/J4B;

    invoke-direct {p2, v0, v1, v3, v2}, LX/J4B;-><init>(LX/J4E;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, p1, p2, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2649228
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2649224
    return-void
.end method
