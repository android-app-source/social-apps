.class public LX/JFz;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Random;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2666635
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, LX/JFz;->a:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2666633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2666634
    return-void
.end method

.method public static a(IIIZZ[CLjava/util/Random;)Ljava/lang/String;
    .locals 8

    .prologue
    const v7, 0xdc00

    const v6, 0xd800

    const/16 v5, 0x80

    .line 2666643
    if-nez p0, :cond_0

    .line 2666644
    const-string v0, ""

    .line 2666645
    :goto_0
    return-object v0

    .line 2666646
    :cond_0
    if-gez p0, :cond_1

    .line 2666647
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested random string length "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is less than 0."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2666648
    :cond_1
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    .line 2666649
    const/16 p2, 0x7b

    .line 2666650
    const/16 p1, 0x20

    .line 2666651
    if-nez p3, :cond_2

    if-nez p4, :cond_2

    .line 2666652
    const/4 p1, 0x0

    .line 2666653
    const p2, 0x7fffffff

    .line 2666654
    :cond_2
    new-array v2, p0, [C

    .line 2666655
    sub-int v3, p2, p1

    .line 2666656
    :goto_1
    add-int/lit8 v1, p0, -0x1

    if-eqz p0, :cond_b

    .line 2666657
    if-nez p5, :cond_6

    .line 2666658
    invoke-virtual {p6, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/2addr v0, p1

    int-to-char v0, v0

    .line 2666659
    :goto_2
    if-eqz p3, :cond_3

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v4

    if-nez v4, :cond_5

    :cond_3
    if-eqz p4, :cond_4

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    if-nez p3, :cond_a

    if-nez p4, :cond_a

    .line 2666660
    :cond_5
    if-lt v0, v7, :cond_7

    const v4, 0xdfff

    if-gt v0, v4, :cond_7

    .line 2666661
    if-eqz v1, :cond_a

    .line 2666662
    aput-char v0, v2, v1

    .line 2666663
    add-int/lit8 p0, v1, -0x1

    .line 2666664
    invoke-virtual {p6, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/2addr v0, v6

    int-to-char v0, v0

    aput-char v0, v2, p0

    goto :goto_1

    .line 2666665
    :cond_6
    invoke-virtual {p6, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/2addr v0, p1

    aget-char v0, p5, v0

    goto :goto_2

    .line 2666666
    :cond_7
    if-lt v0, v6, :cond_8

    const v4, 0xdb7f

    if-gt v0, v4, :cond_8

    .line 2666667
    if-eqz v1, :cond_a

    .line 2666668
    invoke-virtual {p6, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/2addr v4, v7

    int-to-char v4, v4

    aput-char v4, v2, v1

    .line 2666669
    add-int/lit8 p0, v1, -0x1

    .line 2666670
    aput-char v0, v2, p0

    goto :goto_1

    .line 2666671
    :cond_8
    const v4, 0xdb80

    if-lt v0, v4, :cond_9

    const v4, 0xdbff

    if-le v0, v4, :cond_a

    .line 2666672
    :cond_9
    aput-char v0, v2, v1

    move p0, v1

    goto :goto_1

    .line 2666673
    :cond_a
    add-int/lit8 p0, v1, 0x1

    .line 2666674
    goto :goto_1

    .line 2666675
    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    goto/16 :goto_0
.end method

.method public static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2666636
    if-nez p1, :cond_0

    .line 2666637
    const/4 v5, 0x0

    sget-object v6, LX/JFz;->a:Ljava/util/Random;

    move v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-static/range {v0 .. v6}, LX/JFz;->a(IIIZZ[CLjava/util/Random;)Ljava/lang/String;

    move-result-object v0

    .line 2666638
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    const/4 v8, 0x0

    .line 2666639
    if-nez v0, :cond_1

    .line 2666640
    const/4 v12, 0x0

    sget-object v13, LX/JFz;->a:Ljava/util/Random;

    move v7, p0

    move v9, v8

    move v10, v8

    move v11, v8

    invoke-static/range {v7 .. v13}, LX/JFz;->a(IIIZZ[CLjava/util/Random;)Ljava/lang/String;

    move-result-object v7

    .line 2666641
    :goto_1
    move-object v0, v7

    .line 2666642
    goto :goto_0

    :cond_1
    array-length v9, v0

    sget-object v13, LX/JFz;->a:Ljava/util/Random;

    move v7, p0

    move v10, v8

    move v11, v8

    move-object v12, v0

    invoke-static/range {v7 .. v13}, LX/JFz;->a(IIIZZ[CLjava/util/Random;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1
.end method
