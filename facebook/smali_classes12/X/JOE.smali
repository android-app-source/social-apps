.class public LX/JOE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2687117
    return-void
.end method

.method public static a(LX/0QB;)LX/JOE;
    .locals 3

    .prologue
    .line 2687118
    const-class v1, LX/JOE;

    monitor-enter v1

    .line 2687119
    :try_start_0
    sget-object v0, LX/JOE;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687120
    sput-object v2, LX/JOE;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687121
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687122
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2687123
    new-instance v0, LX/JOE;

    invoke-direct {v0}, LX/JOE;-><init>()V

    .line 2687124
    move-object v0, v0

    .line 2687125
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687126
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687127
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687128
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;)Lcom/facebook/greetingcards/model/GreetingCard$Slide;
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2687129
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2687130
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2687131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidePhotosConnection;->a()LX/0Px;

    move-result-object v5

    .line 2687132
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 2687133
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_4

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2687134
    const/4 v2, 0x1

    if-ne v6, v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2687135
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v1

    :goto_1
    invoke-static {v8, v9, v0}, Lcom/facebook/greetingcards/model/CardPhoto;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/model/CardPhoto;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2687136
    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2687137
    :cond_1
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v10

    double-to-float v10, v10

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v12

    double-to-float v0, v12

    invoke-direct {v2, v10, v0}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v0, v2

    goto :goto_1

    .line 2687138
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2687139
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    if-nez v2, :cond_3

    move-object v0, v1

    :goto_3
    invoke-static {v8, v9, v0}, Lcom/facebook/greetingcards/model/CardPhoto;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/model/CardPhoto;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    :cond_3
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v10

    double-to-float v10, v10

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v12

    double-to-float v0, v12

    invoke-direct {v2, v10, v0}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v0, v2

    goto :goto_3

    .line 2687140
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2687141
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2687142
    :goto_5
    new-instance v2, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    return-object v2

    .line 2687143
    :cond_5
    const-string v0, ""

    goto :goto_4

    .line 2687144
    :cond_6
    const-string v1, ""

    goto :goto_5
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLGreetingCard;)Lcom/facebook/greetingcards/model/GreetingCard;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2687145
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2687146
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->m()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2687147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->m()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move-object v1, v3

    move-object v2, v3

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_0

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;

    .line 2687148
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;->k()Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    move-result-object v7

    .line 2687149
    sget-object v8, LX/JOD;->a:[I

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;->ordinal()I

    move-result v7

    aget v7, v8, v7

    packed-switch v7, :pswitch_data_0

    move-object v0, v1

    move-object v1, v2

    .line 2687150
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2687151
    :pswitch_0
    invoke-static {v0}, LX/JOE;->a(Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v0

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    .line 2687152
    goto :goto_1

    .line 2687153
    :pswitch_1
    invoke-static {v0}, LX/JOE;->a(Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v0, v1

    move-object v1, v2

    .line 2687154
    goto :goto_1

    .line 2687155
    :pswitch_2
    invoke-static {v0}, LX/JOE;->a(Lcom/facebook/graphql/model/GraphQLGreetingCardSlide;)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v0

    move-object v1, v2

    goto :goto_1

    :cond_0
    move-object v3, v1

    move-object v1, v2

    .line 2687156
    :goto_2
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->k()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/greetingcards/model/GreetingCard;-><init>(Lcom/facebook/greetingcards/model/GreetingCard$Slide;LX/0Px;Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v1, v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
