.class public LX/J9L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J9L;


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    .line 2652718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652719
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    .line 2652720
    if-ne v0, v2, :cond_0

    .line 2652721
    const/4 v0, 0x5

    iput v0, p0, LX/J9L;->a:I

    .line 2652722
    :goto_0
    return-void

    .line 2652723
    :cond_0
    if-ne v0, v1, :cond_1

    .line 2652724
    iput v2, p0, LX/J9L;->a:I

    goto :goto_0

    .line 2652725
    :cond_1
    iput v1, p0, LX/J9L;->a:I

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/J9L;
    .locals 4

    .prologue
    .line 2652726
    sget-object v0, LX/J9L;->b:LX/J9L;

    if-nez v0, :cond_1

    .line 2652727
    const-class v1, LX/J9L;

    monitor-enter v1

    .line 2652728
    :try_start_0
    sget-object v0, LX/J9L;->b:LX/J9L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2652729
    if-eqz v2, :cond_0

    .line 2652730
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2652731
    new-instance p0, LX/J9L;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/J9L;-><init>(Landroid/content/res/Resources;)V

    .line 2652732
    move-object v0, p0

    .line 2652733
    sput-object v0, LX/J9L;->b:LX/J9L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2652734
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2652735
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2652736
    :cond_1
    sget-object v0, LX/J9L;->b:LX/J9L;

    return-object v0

    .line 2652737
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2652738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final h()I
    .locals 3

    .prologue
    .line 2652739
    const/4 v0, 0x6

    move v0, v0

    .line 2652740
    const/4 v1, 0x6

    move v1, v1

    .line 2652741
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2652742
    const/4 v1, 0x4

    move v1, v1

    .line 2652743
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2652744
    const/4 v1, 0x2

    move v1, v1

    .line 2652745
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2652746
    const/4 v1, 0x2

    move v1, v1

    .line 2652747
    iget v2, p0, LX/J9L;->a:I

    move v2, v2

    .line 2652748
    mul-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2652749
    return v0
.end method
