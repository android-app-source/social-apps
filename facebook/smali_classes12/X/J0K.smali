.class public LX/J0K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/70U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/70U",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636845
    return-void
.end method


# virtual methods
.method public final a()LX/6zQ;
    .locals 1

    .prologue
    .line 2636846
    sget-object v0, LX/6zQ;->NEW_NET_BANKING:LX/6zQ;

    return-object v0
.end method

.method public final a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
    .locals 6

    .prologue
    .line 2636847
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2636848
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2636849
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2636850
    invoke-static {v0}, LX/6zQ;->forValue(Ljava/lang/String;)LX/6zQ;

    move-result-object v0

    sget-object v3, LX/6zQ;->NEW_NET_BANKING:LX/6zQ;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2636851
    const-string v0, "provider"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 2636852
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2636853
    const-string v0, "bank_info"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2636854
    const-string v0, "bank_info"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2636855
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636856
    invoke-virtual {v0}, LX/0lF;->h()Z

    move-result v5

    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2636857
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2636858
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2636859
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2636860
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2636861
    new-instance v2, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;

    invoke-direct {v2, v0}, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;-><init>(LX/0lF;)V

    .line 2636862
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    :cond_0
    move v0, v2

    .line 2636863
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2636864
    goto :goto_1

    .line 2636865
    :cond_2
    new-instance v0, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;-><init>(Ljava/lang/String;LX/0Px;)V

    return-object v0
.end method
