.class public final LX/HfF;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/HfF;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HfD;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/HfG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2491469
    const/4 v0, 0x0

    sput-object v0, LX/HfF;->a:LX/HfF;

    .line 2491470
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/HfF;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2491471
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2491472
    new-instance v0, LX/HfG;

    invoke-direct {v0}, LX/HfG;-><init>()V

    iput-object v0, p0, LX/HfF;->c:LX/HfG;

    .line 2491473
    return-void
.end method

.method public static declared-synchronized q()LX/HfF;
    .locals 2

    .prologue
    .line 2491474
    const-class v1, LX/HfF;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/HfF;->a:LX/HfF;

    if-nez v0, :cond_0

    .line 2491475
    new-instance v0, LX/HfF;

    invoke-direct {v0}, LX/HfF;-><init>()V

    sput-object v0, LX/HfF;->a:LX/HfF;

    .line 2491476
    :cond_0
    sget-object v0, LX/HfF;->a:LX/HfF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2491477
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2491478
    check-cast p2, LX/HfE;

    .line 2491479
    iget-boolean v0, p2, LX/HfE;->a:Z

    iget-object v1, p2, LX/HfE;->b:Ljava/lang/CharSequence;

    iget-object v2, p2, LX/HfE;->c:LX/1dQ;

    .line 2491480
    const/4 p0, 0x0

    if-eqz v0, :cond_0

    const v3, 0x7f0e0bb6

    :goto_0
    invoke-static {p1, p0, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    if-eqz v0, :cond_1

    const v3, 0x7f020b18

    :goto_1
    invoke-interface {p0, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    const/4 p0, 0x6

    const p2, 0x7f0b2366

    invoke-interface {v3, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 p0, 0x7

    const p2, 0x7f0b2367

    invoke-interface {v3, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2491481
    return-object v0

    :cond_0
    const v3, 0x7f0e0bb5

    goto :goto_0

    :cond_1
    const v3, 0x7f0219de

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2491482
    invoke-static {}, LX/1dS;->b()V

    .line 2491483
    const/4 v0, 0x0

    return-object v0
.end method
