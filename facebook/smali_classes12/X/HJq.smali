.class public final LX/HJq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

.field public final c:I

.field public final d:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public final e:Z

.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/util/ArrayList;Ljava/lang/String;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLInterfaces$ReactionGeoRectangleFields;",
            "I",
            "Lcom/facebook/graphql/enums/GraphQLPlaceType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2453349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2453350
    iput-boolean p1, p0, LX/HJq;->e:Z

    .line 2453351
    iput-object p2, p0, LX/HJq;->a:Ljava/util/ArrayList;

    .line 2453352
    iput-object p3, p0, LX/HJq;->f:Ljava/lang/String;

    .line 2453353
    iput-object p4, p0, LX/HJq;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    .line 2453354
    iput p5, p0, LX/HJq;->c:I

    .line 2453355
    iput-object p6, p0, LX/HJq;->d:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2453356
    return-void
.end method
