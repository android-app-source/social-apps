.class public final LX/IdI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 0

    .prologue
    .line 2596202
    iput-object p1, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x50e39bc5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2596152
    iget-object v0, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    .line 2596153
    iget-object v2, v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    move-object v0, v2

    .line 2596154
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v2}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->D(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596155
    iget-object v4, v2, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v4

    .line 2596156
    if-nez v2, :cond_1

    .line 2596157
    :cond_0
    const v0, 0x40253a82

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2596158
    :goto_0
    return-void

    .line 2596159
    :cond_1
    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    const-string v3, "click_request_ride_button"

    invoke-virtual {v2, v3}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2596160
    new-instance v2, LX/IcK;

    invoke-direct {v2}, LX/IcK;-><init>()V

    iget-object v3, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596161
    iget-object v4, v3, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v3, v4

    .line 2596162
    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 2596163
    iput-object v3, v2, LX/IcK;->b:Ljava/lang/String;

    .line 2596164
    move-object v2, v2

    .line 2596165
    iget-object v3, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596166
    iget-object v4, v3, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2596167
    iput-object v3, v2, LX/IcK;->c:Ljava/lang/String;

    .line 2596168
    move-object v2, v2

    .line 2596169
    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2596170
    iput-object v3, v2, LX/IcK;->d:Ljava/lang/String;

    .line 2596171
    move-object v2, v2

    .line 2596172
    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 2596173
    iput-object v3, v2, LX/IcK;->e:Ljava/lang/String;

    .line 2596174
    move-object v2, v2

    .line 2596175
    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->l()I

    move-result v0

    .line 2596176
    iput v0, v2, LX/IcK;->a:I

    .line 2596177
    move-object v0, v2

    .line 2596178
    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    .line 2596179
    iput-object v2, v0, LX/IcK;->f:Landroid/location/Location;

    .line 2596180
    move-object v0, v0

    .line 2596181
    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    .line 2596182
    iput-object v2, v0, LX/IcK;->g:Landroid/location/Location;

    .line 2596183
    move-object v0, v0

    .line 2596184
    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->c:Ljava/lang/String;

    .line 2596185
    iput-object v2, v0, LX/IcK;->i:Ljava/lang/String;

    .line 2596186
    move-object v0, v0

    .line 2596187
    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->c:Ljava/lang/String;

    .line 2596188
    iput-object v2, v0, LX/IcK;->k:Ljava/lang/String;

    .line 2596189
    move-object v0, v0

    .line 2596190
    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    .line 2596191
    iput-object v2, v0, LX/IcK;->h:Ljava/lang/String;

    .line 2596192
    move-object v0, v0

    .line 2596193
    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    .line 2596194
    iput-object v2, v0, LX/IcK;->j:Ljava/lang/String;

    .line 2596195
    move-object v2, v0

    .line 2596196
    iget-object v0, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->L:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->L:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a()Ljava/lang/String;

    move-result-object v0

    .line 2596197
    :goto_1
    iput-object v0, v2, LX/IcK;->l:Ljava/lang/String;

    .line 2596198
    move-object v0, v2

    .line 2596199
    iget-object v2, p0, LX/IdI;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->e:LX/Ic4;

    invoke-virtual {v2, v0}, LX/Ic4;->a(LX/IcK;)V

    .line 2596200
    const v0, -0x41be67e5

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2596201
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
