.class public final LX/Hh6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;)V
    .locals 0

    .prologue
    .line 2494853
    iput-object p1, p0, LX/Hh6;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v0, 0x1

    const v1, -0x44418415

    invoke-static {v11, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 2494854
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 2494855
    new-instance v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 2494856
    iget-object v1, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2494857
    iget-object v2, p0, LX/Hh6;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    const v3, 0x7f080e08

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    .line 2494858
    iget-object v4, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2494859
    iget-object v5, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->f:Ljava/lang/String;

    move-object v5, v5

    .line 2494860
    iget-object v6, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2494861
    iget-object v7, p0, LX/Hh6;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    const v9, 0x7f080e09

    invoke-virtual {v7, v9}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2494862
    iget-object v9, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->o:Ljava/lang/String;

    move-object v8, v9

    .line 2494863
    sget-object v9, LX/6Y4;->CARRIER_MANAGER:LX/6Y4;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Y4;)V

    .line 2494864
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/Hh6;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2494865
    const-string v2, "promo_data_model"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2494866
    iget-object v0, p0, LX/Hh6;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->c:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Hh6;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2494867
    const v0, 0x1bdbfe5a

    invoke-static {v11, v11, v0, v10}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
