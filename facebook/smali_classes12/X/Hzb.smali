.class public final LX/Hzb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hxg;

.field public final synthetic b:LX/Hze;


# direct methods
.method public constructor <init>(LX/Hze;LX/Hxg;)V
    .locals 0

    .prologue
    .line 2525794
    iput-object p1, p0, LX/Hzb;->b:LX/Hze;

    iput-object p2, p0, LX/Hzb;->a:LX/Hxg;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2525769
    iget-object v0, p0, LX/Hzb;->a:LX/Hxg;

    if-eqz v0, :cond_0

    .line 2525770
    iget-object v0, p0, LX/Hzb;->a:LX/Hxg;

    invoke-interface {v0}, LX/Hxg;->a()V

    .line 2525771
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2525772
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2525773
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525774
    if-nez v0, :cond_1

    .line 2525775
    iget-object v0, p0, LX/Hzb;->a:LX/Hxg;

    if-eqz v0, :cond_0

    .line 2525776
    iget-object v0, p0, LX/Hzb;->a:LX/Hxg;

    invoke-interface {v0}, LX/Hxg;->a()V

    .line 2525777
    :cond_0
    :goto_0
    return-void

    .line 2525778
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525779
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;

    move-result-object v2

    .line 2525780
    if-eqz v2, :cond_0

    .line 2525781
    iget-object v0, p0, LX/Hzb;->b:LX/Hze;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-static {v0, v3}, LX/Hze;->a$redex0(LX/Hze;LX/0Px;)V

    .line 2525782
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2525783
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7oa;

    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2525784
    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2525785
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    .line 2525786
    iget-object v4, p0, LX/Hzb;->b:LX/Hze;

    if-nez v3, :cond_2

    .line 2525787
    :goto_2
    iput-object v1, v4, LX/Hze;->m:Ljava/lang/String;

    .line 2525788
    iget-object v4, p0, LX/Hzb;->b:LX/Hze;

    if-eqz v3, :cond_3

    invoke-interface {v3}, LX/0ut;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 2525789
    :goto_3
    iput-boolean v1, v4, LX/Hze;->o:Z

    .line 2525790
    iget-object v1, p0, LX/Hzb;->a:LX/Hxg;

    if-eqz v1, :cond_0

    .line 2525791
    iget-object v1, p0, LX/Hzb;->a:LX/Hxg;

    iget-object v3, p0, LX/Hzb;->b:LX/Hze;

    iget-boolean v3, v3, LX/Hze;->o:Z

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-interface {v1, v3, v2, v0}, LX/Hxg;->a(ZILjava/lang/Long;)V

    goto :goto_0

    .line 2525792
    :cond_2
    invoke-interface {v3}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2525793
    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
