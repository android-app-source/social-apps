.class public LX/Idi;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final d:Landroid/net/Uri;

.field private static final e:Landroid/net/Uri;

.field private static final f:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2597717
    const-class v0, LX/Idi;

    sput-object v0, LX/Idi;->c:Ljava/lang/Class;

    .line 2597718
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, LX/Idi;->d:Landroid/net/Uri;

    .line 2597719
    sget-object v0, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, LX/Idi;->e:Landroid/net/Uri;

    .line 2597720
    const-string v0, "content://media/external/video/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/Idi;->f:Landroid/net/Uri;

    .line 2597721
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM/Camera"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2597722
    sput-object v0, LX/Idi;->a:Ljava/lang/String;

    .line 2597723
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2597724
    sput-object v0, LX/Idi;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2597725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2597726
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;LX/Ide;IILjava/lang/String;)LX/Idf;
    .locals 1

    .prologue
    .line 2597727
    new-instance v0, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;

    invoke-direct {v0}, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;-><init>()V

    .line 2597728
    iput-object p1, v0, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->a:LX/Ide;

    .line 2597729
    iput p2, v0, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->b:I

    .line 2597730
    iput p3, v0, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->c:I

    .line 2597731
    iput-object p4, v0, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->d:Ljava/lang/String;

    .line 2597732
    move-object v0, v0

    .line 2597733
    invoke-static {p0, v0}, LX/Idi;->a(Landroid/content/ContentResolver;Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;)LX/Idf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;)LX/Idf;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2597734
    iget-object v6, p1, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->a:LX/Ide;

    .line 2597735
    iget v7, p1, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->b:I

    .line 2597736
    iget v4, p1, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->c:I

    .line 2597737
    iget-object v5, p1, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->d:Ljava/lang/String;

    .line 2597738
    iget-object v1, p1, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->e:Landroid/net/Uri;

    .line 2597739
    iget-boolean v0, p1, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->f:Z

    .line 2597740
    if-nez v0, :cond_0

    if-nez p0, :cond_1

    .line 2597741
    :cond_0
    new-instance v0, LX/Idg;

    invoke-direct {v0}, LX/Idg;-><init>()V

    .line 2597742
    :goto_0
    return-object v0

    .line 2597743
    :cond_1
    if-eqz v1, :cond_2

    .line 2597744
    new-instance v0, LX/Idt;

    invoke-direct {v0, p0, v1}, LX/Idt;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    .line 2597745
    :cond_2
    invoke-static {v9}, LX/Idi;->a(Z)Z

    move-result v0

    .line 2597746
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2597747
    if-eqz v0, :cond_3

    sget-object v0, LX/Ide;->INTERNAL:LX/Ide;

    if-eq v6, v0, :cond_3

    .line 2597748
    and-int/lit8 v0, v7, 0x1

    if-eqz v0, :cond_3

    .line 2597749
    new-instance v0, LX/Ido;

    sget-object v2, LX/Idi;->d:Landroid/net/Uri;

    sget-object v3, LX/Idi;->e:Landroid/net/Uri;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/Ido;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/net/Uri;ILjava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2597750
    :cond_3
    sget-object v0, LX/Ide;->INTERNAL:LX/Ide;

    if-eq v6, v0, :cond_4

    sget-object v0, LX/Ide;->ALL:LX/Ide;

    if-ne v6, v0, :cond_5

    .line 2597751
    :cond_4
    and-int/lit8 v0, v7, 0x1

    if-eqz v0, :cond_5

    .line 2597752
    new-instance v0, LX/Ido;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Landroid/provider/MediaStore$Images$Thumbnails;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/Ido;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/net/Uri;ILjava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2597753
    :cond_5
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2597754
    :cond_6
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2597755
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idm;

    .line 2597756
    invoke-virtual {v0}, LX/Idm;->b()I

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 2597757
    if-eqz v2, :cond_6

    .line 2597758
    invoke-virtual {v0}, LX/Idm;->a()V

    .line 2597759
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 2597760
    :cond_7
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    .line 2597761
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idm;

    goto :goto_0

    .line 2597762
    :cond_8
    new-instance v1, LX/Ids;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LX/Idf;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Idf;

    invoke-direct {v1, v0, v4}, LX/Ids;-><init>([LX/Idf;I)V

    move-object v0, v1

    .line 2597763
    goto/16 :goto_0

    :cond_9
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;JLandroid/location/Location;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;[B[I)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 2597764
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2597765
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2597766
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 2597767
    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p5, p6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597768
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2597769
    if-eqz p7, :cond_2

    .line 2597770
    :try_start_1
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x4b

    invoke-virtual {p7, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2597771
    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, p9, v3
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2597772
    :goto_0
    invoke-static {v2}, LX/IdZ;->a(Ljava/io/Closeable;)V

    .line 2597773
    new-instance v0, Landroid/content/ContentValues;

    const/4 v2, 0x7

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 2597774
    const-string v2, "title"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597775
    const-string v2, "_display_name"

    invoke-virtual {v0, v2, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597776
    const-string v2, "datetaken"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2597777
    const-string v2, "mime_type"

    const-string v3, "image/jpeg"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597778
    const-string v2, "orientation"

    aget v3, p9, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2597779
    const-string v2, "_data"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597780
    if-eqz p4, :cond_1

    .line 2597781
    const-string v1, "latitude"

    invoke-virtual {p4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2597782
    const-string v1, "longitude"

    invoke-virtual {p4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2597783
    :cond_1
    sget-object v1, LX/Idi;->d:Landroid/net/Uri;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2597784
    :cond_2
    :try_start_2
    invoke-virtual {v2, p8}, Ljava/io/OutputStream;->write([B)V

    .line 2597785
    const/4 v3, 0x0

    invoke-static {v1}, LX/Idi;->b(Ljava/lang/String;)I

    move-result v4

    aput v4, p9, v3
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2597786
    :catch_0
    move-exception v1

    .line 2597787
    :goto_2
    :try_start_3
    sget-object v3, LX/Idi;->c:Ljava/lang/Class;

    const-string v4, ""

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2597788
    invoke-static {v2}, LX/IdZ;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 2597789
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 2597790
    :goto_3
    :try_start_4
    sget-object v3, LX/Idi;->c:Ljava/lang/Class;

    const-string v4, ""

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2597791
    invoke-static {v2}, LX/IdZ;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    invoke-static {v2}, LX/IdZ;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    .line 2597792
    :catch_2
    move-exception v1

    goto :goto_3

    .line 2597793
    :catch_3
    move-exception v1

    move-object v2, v0

    goto :goto_2
.end method

.method public static a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2597794
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 2597795
    const-string v2, "mounted"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2597796
    if-eqz p0, :cond_1

    .line 2597797
    const/4 v0, 0x0

    .line 2597798
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/DCIM"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2597799
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2597800
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result p0

    if-nez p0, :cond_4

    .line 2597801
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2597802
    :cond_0
    :goto_0
    move v0, v0

    .line 2597803
    :cond_1
    :goto_1
    return v0

    .line 2597804
    :cond_2
    if-nez p0, :cond_3

    const-string v2, "mounted_ro"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2597805
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2597806
    :cond_4
    new-instance v2, Ljava/io/File;

    const-string p0, ".probe"

    invoke-direct {v2, v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597807
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2597808
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 2597809
    :cond_5
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2597810
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2597811
    const/4 v0, 0x1

    goto :goto_0

    .line 2597812
    :catch_0
    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 2597813
    const/4 v0, 0x0

    .line 2597814
    const/4 v2, 0x0

    .line 2597815
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2597816
    :goto_0
    if-eqz v1, :cond_0

    .line 2597817
    const-string v2, "Orientation"

    invoke-virtual {v1, v2, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v1

    .line 2597818
    if-eq v1, v5, :cond_0

    .line 2597819
    packed-switch v1, :pswitch_data_0

    .line 2597820
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 2597821
    :catch_0
    move-exception v1

    .line 2597822
    sget-object v3, LX/Idi;->c:Ljava/lang/Class;

    const-string v4, "cannot read exif"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_0

    .line 2597823
    :pswitch_1
    const/16 v0, 0x5a

    .line 2597824
    goto :goto_1

    .line 2597825
    :pswitch_2
    const/16 v0, 0xb4

    .line 2597826
    goto :goto_1

    .line 2597827
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
