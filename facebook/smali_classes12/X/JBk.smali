.class public final LX/JBk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2660404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v13, 0x0

    const/4 v2, 0x0

    .line 2660405
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2660406
    iget-object v1, p0, LX/JBk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2660407
    iget-object v3, p0, LX/JBk;->b:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2660408
    iget-object v5, p0, LX/JBk;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2660409
    iget-object v6, p0, LX/JBk;->d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2660410
    iget-object v7, p0, LX/JBk;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2660411
    iget-object v8, p0, LX/JBk;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2660412
    iget-object v9, p0, LX/JBk;->g:LX/0Px;

    invoke-virtual {v0, v9}, LX/186;->c(Ljava/util/List;)I

    move-result v9

    .line 2660413
    iget-object v10, p0, LX/JBk;->i:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2660414
    iget-object v11, p0, LX/JBk;->j:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2660415
    const/16 v12, 0xa

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 2660416
    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 2660417
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 2660418
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2660419
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2660420
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2660421
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2660422
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2660423
    const/4 v1, 0x7

    iget-boolean v3, p0, LX/JBk;->h:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 2660424
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2660425
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2660426
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2660427
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2660428
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2660429
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2660430
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2660431
    new-instance v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    invoke-direct {v1, v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;-><init>(LX/15i;)V

    .line 2660432
    return-object v1
.end method
