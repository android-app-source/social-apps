.class public LX/IQJ;
.super LX/DNC;
.source ""


# instance fields
.field private final a:LX/DOp;

.field private final b:LX/IRJ;

.field private final c:LX/DNR;

.field private final d:LX/DNJ;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/IVs;

.field private final g:Landroid/content/Context;

.field public h:LX/1Qq;


# direct methods
.method public constructor <init>(LX/DNR;LX/DNJ;LX/DOp;LX/IRJ;LX/0Ot;LX/IVs;Landroid/content/Context;)V
    .locals 0
    .param p1    # LX/DNR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/DNJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DNR;",
            "LX/DNJ;",
            "LX/DOp;",
            "LX/IRJ;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;",
            ">;",
            "LX/IVs;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2575115
    invoke-direct {p0}, LX/DNC;-><init>()V

    .line 2575116
    iput-object p1, p0, LX/IQJ;->c:LX/DNR;

    .line 2575117
    iput-object p2, p0, LX/IQJ;->d:LX/DNJ;

    .line 2575118
    iput-object p3, p0, LX/IQJ;->a:LX/DOp;

    .line 2575119
    iput-object p4, p0, LX/IQJ;->b:LX/IRJ;

    .line 2575120
    iput-object p5, p0, LX/IQJ;->e:LX/0Ot;

    .line 2575121
    iput-object p6, p0, LX/IQJ;->f:LX/IVs;

    .line 2575122
    iput-object p7, p0, LX/IQJ;->g:Landroid/content/Context;

    .line 2575123
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2575124
    iget-object v0, p0, LX/IQJ;->b:LX/IRJ;

    sget-object v1, LX/IRH;->NORMAL:LX/IRH;

    iget-object v2, p0, LX/IQJ;->g:Landroid/content/Context;

    .line 2575125
    sget-object v3, LX/DOq;->a:LX/DOq;

    move-object v3, v3

    .line 2575126
    new-instance v4, Lcom/facebook/groups/feed/ui/CommunityTrendingStoriesFeedControllerResponder$1;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/CommunityTrendingStoriesFeedControllerResponder$1;-><init>(LX/IQJ;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/1Qq;
    .locals 1

    .prologue
    .line 2575127
    iget-object v0, p0, LX/IQJ;->h:LX/1Qq;

    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2575128
    iget-object v0, p0, LX/IQJ;->a:LX/DOp;

    iget-object v1, p0, LX/IQJ;->f:LX/IVs;

    iget-object v2, p0, LX/IQJ;->e:LX/0Ot;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, LX/IQJ;->c:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/IQJ;->h:LX/1Qq;

    .line 2575129
    iget-object v0, p0, LX/IQJ;->h:LX/1Qq;

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2575130
    iget-object v0, p0, LX/IQJ;->d:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2575131
    return-void
.end method
