.class public final LX/HGo;
.super LX/HGm;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public l:LX/HH1;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/HH1;)V
    .locals 2

    .prologue
    .line 2446608
    invoke-direct {p0, p1}, LX/HGm;-><init>(Landroid/view/View;)V

    .line 2446609
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2446610
    iput-object p2, p0, LX/HGo;->l:LX/HH1;

    .line 2446611
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2446612
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2446613
    iget-object v1, p0, LX/HGm;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2446614
    iget-object v1, p0, LX/HGm;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const p1, 0x7f020090

    invoke-virtual {v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 2446615
    iget-object v1, p0, LX/HGm;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0811d4

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2446616
    iget-object v1, p0, LX/HGm;->q:Landroid/widget/TextView;

    const-string p1, ""

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2446617
    iget-object v1, p0, LX/HGm;->r:Landroid/widget/TextView;

    const-string p1, ""

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2446618
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x191cc28a

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2446619
    iget-object v1, p0, LX/HGo;->l:LX/HH1;

    .line 2446620
    iget-object v3, v1, LX/HH1;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->q:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9XE;

    iget-object v4, v1, LX/HH1;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    const-string v4, "redesign_albums_list"

    invoke-virtual {v3, v5, v6, v4}, LX/9XE;->f(JLjava/lang/String;)V

    .line 2446621
    iget-object v3, v1, LX/HH1;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->k:LX/0Uh;

    const/16 v4, 0x41b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 2446622
    if-eqz v3, :cond_1

    iget-object v3, v1, LX/HH1;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    .line 2446623
    new-instance v7, Landroid/content/Intent;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v8

    const-class v9, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2446624
    const-string v8, "com.facebook.katana.profile.id"

    iget-object v9, v3, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-virtual {v7, v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2446625
    move-object v3, v7

    .line 2446626
    move-object v4, v3

    .line 2446627
    :goto_0
    if-eqz v4, :cond_0

    .line 2446628
    iget-object v3, v1, LX/HH1;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->o:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    iget-object v5, v1, LX/HH1;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2446629
    :cond_0
    const v1, -0x2cd76102

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2446630
    :cond_1
    iget-object v3, v1, LX/HH1;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v1, LX/HH1;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->G:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2446631
    invoke-static {v3}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->l(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    .line 2446632
    if-eqz v6, :cond_2

    .line 2446633
    iget-boolean v5, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v5, v5

    .line 2446634
    if-eqz v5, :cond_2

    .line 2446635
    iget-object v5, v3, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->l:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9at;

    sget-object v7, LX/9au;->ALBUMSTAB:LX/9au;

    invoke-virtual {v5, v7, v6, v4}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/content/Intent;

    move-result-object v5

    .line 2446636
    :goto_1
    move-object v3, v5

    .line 2446637
    move-object v4, v3

    goto :goto_0

    .line 2446638
    :cond_2
    iget-object v5, v3, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    const-string v6, "getCreateAlbumIntent"

    const-string v7, "not page context"

    invoke-virtual {v5, v6, v7}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2446639
    const/4 v5, 0x0

    goto :goto_1
.end method
