.class public LX/HN7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;)V
    .locals 1
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458171
    iput-object p1, p0, LX/HN7;->a:LX/0Ot;

    .line 2458172
    iput-object p2, p0, LX/HN7;->b:LX/0Ot;

    .line 2458173
    iput-object p3, p0, LX/HN7;->c:LX/0Ot;

    .line 2458174
    iput-object p4, p0, LX/HN7;->d:LX/0Ot;

    .line 2458175
    iput-object p5, p0, LX/HN7;->e:Ljava/lang/String;

    .line 2458176
    iget-object v0, p0, LX/HN7;->e:Ljava/lang/String;

    invoke-static {v0}, LX/HN7;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2458177
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2458169
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "published"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "staging"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLjava/lang/String;LX/HMl;)V
    .locals 9

    .prologue
    .line 2458155
    iget-object v3, p0, LX/HN7;->e:Ljava/lang/String;

    .line 2458156
    invoke-static {p3}, LX/HN7;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2458157
    :cond_0
    :goto_0
    return-void

    .line 2458158
    :cond_1
    iget-object v0, p0, LX/HN7;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/1Ck;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "update_services_data_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, LX/HN7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    .line 2458159
    new-instance v1, LX/9a7;

    invoke-direct {v1}, LX/9a7;-><init>()V

    move-object v1, v1

    .line 2458160
    new-instance v2, LX/4Hz;

    invoke-direct {v2}, LX/4Hz;-><init>()V

    .line 2458161
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 2458162
    const-string v5, "page_id"

    invoke-virtual {v2, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458163
    const-string v4, "published"

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2458164
    const-string v4, "PUBLISHED"

    invoke-virtual {v2, v4}, LX/4Hz;->b(Ljava/lang/String;)LX/4Hz;

    .line 2458165
    :goto_1
    const-string v4, "input"

    invoke-virtual {v1, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2458166
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    move-object v1, v1

    .line 2458167
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    new-instance v0, LX/HN6;

    move-object v1, p0

    move-object v2, p4

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, LX/HN6;-><init>(LX/HN7;LX/HMl;Ljava/lang/String;J)V

    invoke-virtual {v6, v7, v8, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2458168
    :cond_2
    const-string v4, "STAGING"

    invoke-virtual {v2, v4}, LX/4Hz;->b(Ljava/lang/String;)LX/4Hz;

    goto :goto_1
.end method
