.class public final LX/IiO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2604211
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2604212
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2604213
    if-eqz v0, :cond_0

    .line 2604214
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604215
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2604216
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2604217
    if-eqz v0, :cond_1

    .line 2604218
    const-string v1, "message_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604219
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604220
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2604221
    if-eqz v0, :cond_2

    .line 2604222
    const-string v1, "message_sender"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604223
    invoke-static {p0, v0, p2, p3}, LX/IiN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2604224
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2604225
    if-eqz v0, :cond_5

    .line 2604226
    const-string v1, "message_thread_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604227
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2604228
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2604229
    if-eqz v1, :cond_3

    .line 2604230
    const-string p3, "other_user_id"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604231
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604232
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2604233
    if-eqz v1, :cond_4

    .line 2604234
    const-string p3, "thread_fbid"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604235
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604236
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2604237
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2604238
    if-eqz v0, :cond_6

    .line 2604239
    const-string v1, "snippet"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604240
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604241
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2604242
    if-eqz v0, :cond_7

    .line 2604243
    const-string v1, "timestamp_precise"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604244
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604245
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2604246
    return-void
.end method
