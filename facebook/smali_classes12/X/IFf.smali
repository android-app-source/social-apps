.class public LX/IFf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IFR;


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

.field public e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2554696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554697
    iput-object p1, p0, LX/IFf;->d:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    .line 2554698
    iput-object p2, p0, LX/IFf;->a:Landroid/net/Uri;

    .line 2554699
    iput-object p3, p0, LX/IFf;->b:Ljava/lang/String;

    .line 2554700
    iput-object p4, p0, LX/IFf;->e:Ljava/lang/String;

    .line 2554701
    iput-object p5, p0, LX/IFf;->c:Ljava/lang/String;

    .line 2554702
    return-void
.end method

.method public static a(Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/IFf;
    .locals 6

    .prologue
    .line 2554695
    new-instance v0, LX/IFf;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/IFf;-><init>(Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2554694
    const v0, 0x7f020b9a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554693
    iget-object v0, p0, LX/IFf;->d:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554692
    iget-object v0, p0, LX/IFf;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/3OL;)V
    .locals 0

    .prologue
    .line 2554691
    return-void
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 2554690
    return-void
.end method

.method public final b(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2554689
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554688
    iget-object v0, p0, LX/IFf;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554687
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554703
    iget-object v0, p0, LX/IFf;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554677
    const-string v0, ""

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554678
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2554679
    const v0, 0x7f083843

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/IFf;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554680
    const-string v0, "aura"

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2554681
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2554682
    const/4 v0, 0x0

    return v0
.end method

.method public final h()LX/3OL;
    .locals 1

    .prologue
    .line 2554683
    sget-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 2554684
    const/4 v0, 0x1

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2554676
    const/4 v0, 0x1

    return v0
.end method

.method public final k()LX/6VG;
    .locals 1

    .prologue
    .line 2554686
    sget-object v0, LX/6VG;->NONE:LX/6VG;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554685
    iget-object v0, p0, LX/IFf;->c:Ljava/lang/String;

    return-object v0
.end method
