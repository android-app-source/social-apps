.class public final LX/JRE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JRF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JRF;


# direct methods
.method public constructor <init>(LX/JRF;)V
    .locals 1

    .prologue
    .line 2692935
    iput-object p1, p0, LX/JRE;->c:LX/JRF;

    .line 2692936
    move-object v0, p1

    .line 2692937
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2692938
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2692920
    const-string v0, "MessengerGenericPromotionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2692921
    if-ne p0, p1, :cond_1

    .line 2692922
    :cond_0
    :goto_0
    return v0

    .line 2692923
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2692924
    goto :goto_0

    .line 2692925
    :cond_3
    check-cast p1, LX/JRE;

    .line 2692926
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2692927
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2692928
    if-eq v2, v3, :cond_0

    .line 2692929
    iget-object v2, p0, LX/JRE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JRE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JRE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2692930
    goto :goto_0

    .line 2692931
    :cond_5
    iget-object v2, p1, LX/JRE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2692932
    :cond_6
    iget-object v2, p0, LX/JRE;->b:LX/1Pk;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JRE;->b:LX/1Pk;

    iget-object v3, p1, LX/JRE;->b:LX/1Pk;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2692933
    goto :goto_0

    .line 2692934
    :cond_7
    iget-object v2, p1, LX/JRE;->b:LX/1Pk;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
