.class public LX/HKB;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HKB",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453876
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2453877
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HKB;->b:LX/0Zi;

    .line 2453878
    iput-object p1, p0, LX/HKB;->a:LX/0Ot;

    .line 2453879
    return-void
.end method

.method public static a(LX/0QB;)LX/HKB;
    .locals 4

    .prologue
    .line 2453880
    const-class v1, LX/HKB;

    monitor-enter v1

    .line 2453881
    :try_start_0
    sget-object v0, LX/HKB;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453882
    sput-object v2, LX/HKB;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453883
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453884
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453885
    new-instance v3, LX/HKB;

    const/16 p0, 0x2bc2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HKB;-><init>(LX/0Ot;)V

    .line 2453886
    move-object v0, v3

    .line 2453887
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453888
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453889
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453890
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2453891
    const v0, -0x789fb1cf

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2453858
    check-cast p2, LX/HKA;

    .line 2453859
    iget-object v0, p0, LX/HKB;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;

    iget-object v1, p2, LX/HKA;->a:LX/HKJ;

    const/4 v8, 0x3

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 2453860
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    .line 2453861
    const v3, -0x789fb1cf

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2453862
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f02008a

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0e43

    invoke-interface {v3, v8, v4}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v3

    iget-object v4, v1, LX/HKJ;->b:Ljava/lang/String;

    const/high16 p0, 0x3fa00000    # 1.25f

    .line 2453863
    if-nez v4, :cond_0

    .line 2453864
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const v9, 0x7f020089

    invoke-interface {v5, v9}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    iget-object v9, v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->b:LX/1nu;

    invoke-virtual {v9, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v9

    invoke-virtual {v9, p0}, LX/1nw;->c(F)LX/1nw;

    move-result-object v9

    sget-object p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v9, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v9

    const/4 p0, 0x0

    invoke-virtual {v9, p0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v9

    const p0, 0x7f020626

    invoke-virtual {v9, p0}, LX/1nw;->h(I)LX/1nw;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const/16 p0, 0x8

    const p2, 0x7f0b0e3c

    invoke-interface {v9, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v9

    invoke-interface {v5, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    .line 2453865
    :goto_0
    move-object v4, v5

    .line 2453866
    const/4 v5, 0x4

    invoke-interface {v4, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0e44

    invoke-interface {v4, v7, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0e44

    invoke-interface {v4, v8, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    iget-object v4, v1, LX/HKJ;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a0099

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004f

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    iget-object v4, v1, LX/HKJ;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00a3

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2453867
    return-object v0

    :cond_0
    iget-object v5, v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->b:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1nw;->c(F)LX/1nw;

    move-result-object v5

    sget-object v9, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v9}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v5

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v5, v9}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2453868
    invoke-static {}, LX/1dS;->b()V

    .line 2453869
    iget v0, p1, LX/1dQ;->b:I

    .line 2453870
    packed-switch v0, :pswitch_data_0

    .line 2453871
    :goto_0
    return-object v1

    .line 2453872
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2453873
    check-cast v0, LX/HKA;

    .line 2453874
    iget-object v2, p0, LX/HKB;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;

    iget-object v3, v0, LX/HKA;->a:LX/HKJ;

    iget-object p1, v0, LX/HKA;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object p2, v0, LX/HKA;->c:LX/2km;

    invoke-virtual {v2, v3, p1, p2}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->onClick(LX/HKJ;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)V

    .line 2453875
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x789fb1cf
        :pswitch_0
    .end packed-switch
.end method
