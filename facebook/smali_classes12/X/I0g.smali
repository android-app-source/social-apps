.class public LX/I0g;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/Calendar;

.field private b:Ljava/util/Calendar;

.field private c:Landroid/content/Context;

.field private d:LX/11R;

.field private e:Ljava/text/SimpleDateFormat;


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/11R;LX/0Or;LX/0Or;)V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/11R;",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2527500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2527501
    iput-object p1, p0, LX/I0g;->c:Landroid/content/Context;

    .line 2527502
    iput-object p2, p0, LX/I0g;->d:LX/11R;

    .line 2527503
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE, d MMM"

    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/I0g;->e:Ljava/text/SimpleDateFormat;

    .line 2527504
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/I0g;->a:Ljava/util/Calendar;

    .line 2527505
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/I0g;->b:Ljava/util/Calendar;

    .line 2527506
    return-void
.end method

.method public static b(LX/0QB;)LX/I0g;
    .locals 5

    .prologue
    .line 2527507
    new-instance v2, LX/I0g;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11R;

    const/16 v3, 0x1617

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x161a

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v2, v0, v1, v3, v4}, LX/I0g;-><init>(Landroid/content/Context;LX/11R;LX/0Or;LX/0Or;)V

    .line 2527508
    return-object v2
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 2527509
    iget-object v0, p0, LX/I0g;->d:LX/11R;

    invoke-virtual {v0, p1, p2}, LX/11R;->a(J)J

    move-result-wide v0

    .line 2527510
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2527511
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    .line 2527512
    iget-object v0, p0, LX/I0g;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082191

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2527513
    :cond_0
    :goto_0
    iget-object v0, p0, LX/I0g;->e:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2527514
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2527515
    :cond_1
    const-wide/16 v4, 0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2527516
    iget-object v0, p0, LX/I0g;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082192

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final a(JJ)Z
    .locals 5

    .prologue
    const/4 v3, 0x6

    const/4 v0, 0x1

    .line 2527517
    iget-object v1, p0, LX/I0g;->a:Ljava/util/Calendar;

    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2527518
    iget-object v1, p0, LX/I0g;->b:Ljava/util/Calendar;

    invoke-virtual {v1, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2527519
    iget-object v1, p0, LX/I0g;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, LX/I0g;->b:Ljava/util/Calendar;

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/I0g;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, LX/I0g;->b:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
