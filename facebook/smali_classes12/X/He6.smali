.class public final enum LX/He6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/He6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/He6;

.field public static final enum COMPLETED:LX/He6;

.field public static final enum NO_QUERY:LX/He6;

.field public static final enum ONGOING:LX/He6;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2489635
    new-instance v0, LX/He6;

    const-string v1, "NO_QUERY"

    invoke-direct {v0, v1, v2}, LX/He6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/He6;->NO_QUERY:LX/He6;

    .line 2489636
    new-instance v0, LX/He6;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v3}, LX/He6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/He6;->COMPLETED:LX/He6;

    .line 2489637
    new-instance v0, LX/He6;

    const-string v1, "ONGOING"

    invoke-direct {v0, v1, v4}, LX/He6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/He6;->ONGOING:LX/He6;

    .line 2489638
    const/4 v0, 0x3

    new-array v0, v0, [LX/He6;

    sget-object v1, LX/He6;->NO_QUERY:LX/He6;

    aput-object v1, v0, v2

    sget-object v1, LX/He6;->COMPLETED:LX/He6;

    aput-object v1, v0, v3

    sget-object v1, LX/He6;->ONGOING:LX/He6;

    aput-object v1, v0, v4

    sput-object v0, LX/He6;->$VALUES:[LX/He6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2489640
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/He6;
    .locals 1

    .prologue
    .line 2489641
    const-class v0, LX/He6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/He6;

    return-object v0
.end method

.method public static values()[LX/He6;
    .locals 1

    .prologue
    .line 2489639
    sget-object v0, LX/He6;->$VALUES:[LX/He6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/He6;

    return-object v0
.end method
