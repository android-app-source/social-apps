.class public abstract LX/J8p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public b:Z

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1

    .prologue
    .line 2652190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652191
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/J8p;->c:Ljava/util/Set;

    .line 2652192
    iput-object p1, p0, LX/J8p;->a:LX/0Zb;

    .line 2652193
    return-void
.end method

.method public static a(LX/9lP;)LX/9lQ;
    .locals 2

    .prologue
    .line 2652156
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2652157
    iget-object v1, p0, LX/9lP;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v1

    .line 2652158
    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2652159
    sget-object v0, LX/9lQ;->FRIEND:LX/9lQ;

    .line 2652160
    :goto_0
    return-object v0

    .line 2652161
    :cond_0
    invoke-virtual {p0}, LX/9lP;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2652162
    sget-object v0, LX/9lQ;->SELF:LX/9lQ;

    goto :goto_0

    .line 2652163
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2652164
    iget-object v1, p0, LX/9lP;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v1, v1

    .line 2652165
    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2652166
    sget-object v0, LX/9lQ;->SUBSCRIBED_TO:LX/9lQ;

    goto :goto_0

    .line 2652167
    :cond_2
    sget-object v0, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/9lQ;)V
    .locals 3

    .prologue
    .line 2652180
    iget-boolean v0, p0, LX/J8p;->b:Z

    if-eqz v0, :cond_0

    .line 2652181
    :goto_0
    return-void

    .line 2652182
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "view"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/J8p;->b()Ljava/lang/String;

    move-result-object v1

    .line 2652183
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2652184
    move-object v0, v0

    .line 2652185
    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2652186
    sget-object v1, LX/9lQ;->UNDEFINED:LX/9lQ;

    if-eq p2, v1, :cond_1

    .line 2652187
    const-string v1, "relationship_type"

    invoke-virtual {p2}, LX/9lQ;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2652188
    :cond_1
    iget-object v1, p0, LX/J8p;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2652189
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J8p;->b:Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/9lQ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2652168
    iget-object v0, p0, LX/J8p;->c:Ljava/util/Set;

    invoke-interface {v0, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2652169
    :goto_0
    return-void

    .line 2652170
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "view_collection"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/J8p;->b()Ljava/lang/String;

    move-result-object v1

    .line 2652171
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2652172
    move-object v0, v0

    .line 2652173
    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2652174
    sget-object v1, LX/9lQ;->UNDEFINED:LX/9lQ;

    if-eq p2, v1, :cond_1

    .line 2652175
    const-string v1, "relationship_type"

    invoke-virtual {p2}, LX/9lQ;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2652176
    :cond_1
    const-string v1, "active_app_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2652177
    const-string v1, "active_collection_id"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2652178
    iget-object v1, p0, LX/J8p;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2652179
    iget-object v0, p0, LX/J8p;->c:Ljava/util/Set;

    invoke-interface {v0, p4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public abstract b()Ljava/lang/String;
.end method
