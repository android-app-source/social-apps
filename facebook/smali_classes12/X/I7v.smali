.class public final LX/I7v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/I7w;

.field private final b:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field private final c:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field private d:Lcom/facebook/events/common/ActionMechanism;

.field private final e:Z


# direct methods
.method public constructor <init>(LX/I7w;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;Z)V
    .locals 0

    .prologue
    .line 2540210
    iput-object p1, p0, LX/I7v;->a:LX/I7w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540211
    iput-object p2, p0, LX/I7v;->b:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2540212
    iput-object p3, p0, LX/I7v;->c:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2540213
    iput-object p4, p0, LX/I7v;->d:Lcom/facebook/events/common/ActionMechanism;

    .line 2540214
    iput-boolean p5, p0, LX/I7v;->e:Z

    .line 2540215
    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 2540216
    iget-object v0, p0, LX/I7v;->b:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v1, p0, LX/I7v;->c:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v0, v1, :cond_0

    .line 2540217
    iget-object v0, p0, LX/I7v;->a:LX/I7w;

    iget-object v1, p0, LX/I7v;->c:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v2, p0, LX/I7v;->d:Lcom/facebook/events/common/ActionMechanism;

    iget-boolean v3, p0, LX/I7v;->e:Z

    invoke-virtual {v0, v1, v2, v3}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;Z)V

    .line 2540218
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
