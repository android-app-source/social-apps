.class public final LX/ITe;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Lcom/facebook/components/ComponentView;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic b:LX/ITj;


# direct methods
.method public constructor <init>(LX/ITj;LX/DML;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 0

    .prologue
    .line 2579459
    iput-object p1, p0, LX/ITe;->b:LX/ITj;

    iput-object p3, p0, LX/ITe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2579460
    check-cast p1, Lcom/facebook/components/ComponentView;

    .line 2579461
    invoke-virtual {p1}, Lcom/facebook/components/ComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/1De;

    iget-object v1, p0, LX/ITe;->b:LX/ITj;

    iget-object v2, v1, LX/ITj;->k:LX/IVC;

    invoke-virtual {p1}, Lcom/facebook/components/ComponentView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, LX/1De;

    const/4 v3, 0x0

    .line 2579462
    new-instance v4, LX/IVB;

    invoke-direct {v4, v2}, LX/IVB;-><init>(LX/IVC;)V

    .line 2579463
    sget-object v5, LX/IVC;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/IVA;

    .line 2579464
    if-nez v5, :cond_0

    .line 2579465
    new-instance v5, LX/IVA;

    invoke-direct {v5}, LX/IVA;-><init>()V

    .line 2579466
    :cond_0
    invoke-static {v5, v1, v3, v3, v4}, LX/IVA;->a$redex0(LX/IVA;LX/1De;IILX/IVB;)V

    .line 2579467
    move-object v4, v5

    .line 2579468
    move-object v3, v4

    .line 2579469
    move-object v1, v3

    .line 2579470
    iget-object v2, p0, LX/ITe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2579471
    iget-object v3, v1, LX/IVA;->a:LX/IVB;

    iput-object v2, v3, LX/IVB;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2579472
    iget-object v3, v1, LX/IVA;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2579473
    move-object v1, v1

    .line 2579474
    iget-object v2, p0, LX/ITe;->b:LX/ITj;

    iget-object v2, v2, LX/ITj;->b:LX/IRb;

    .line 2579475
    iget-object v3, v1, LX/IVA;->a:LX/IVB;

    iput-object v2, v3, LX/IVB;->b:LX/IRb;

    .line 2579476
    iget-object v3, v1, LX/IVA;->d:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2579477
    move-object v1, v1

    .line 2579478
    invoke-static {v0, v1}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 2579479
    invoke-virtual {p1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2579480
    return-void
.end method
