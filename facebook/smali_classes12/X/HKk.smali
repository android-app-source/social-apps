.class public final LX/HKk;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HKl;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/HM9;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic h:LX/HKl;


# direct methods
.method public constructor <init>(LX/HKl;)V
    .locals 1

    .prologue
    .line 2454982
    iput-object p1, p0, LX/HKk;->h:LX/HKl;

    .line 2454983
    move-object v0, p1

    .line 2454984
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2454985
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2454986
    const-string v0, "PagesInsightsMetricWithChartUnitComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2454987
    if-ne p0, p1, :cond_1

    .line 2454988
    :cond_0
    :goto_0
    return v0

    .line 2454989
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2454990
    goto :goto_0

    .line 2454991
    :cond_3
    check-cast p1, LX/HKk;

    .line 2454992
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2454993
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2454994
    if-eq v2, v3, :cond_0

    .line 2454995
    iget-object v2, p0, LX/HKk;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HKk;->a:Ljava/lang/String;

    iget-object v3, p1, LX/HKk;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2454996
    goto :goto_0

    .line 2454997
    :cond_5
    iget-object v2, p1, LX/HKk;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2454998
    :cond_6
    iget-object v2, p0, LX/HKk;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HKk;->b:Ljava/lang/String;

    iget-object v3, p1, LX/HKk;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2454999
    goto :goto_0

    .line 2455000
    :cond_8
    iget-object v2, p1, LX/HKk;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2455001
    :cond_9
    iget-object v2, p0, LX/HKk;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/HKk;->c:Ljava/lang/String;

    iget-object v3, p1, LX/HKk;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2455002
    goto :goto_0

    .line 2455003
    :cond_b
    iget-object v2, p1, LX/HKk;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2455004
    :cond_c
    iget-object v2, p0, LX/HKk;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/HKk;->d:Ljava/lang/String;

    iget-object v3, p1, LX/HKk;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2455005
    goto :goto_0

    .line 2455006
    :cond_e
    iget-object v2, p1, LX/HKk;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2455007
    :cond_f
    iget-boolean v2, p0, LX/HKk;->e:Z

    iget-boolean v3, p1, LX/HKk;->e:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2455008
    goto :goto_0

    .line 2455009
    :cond_10
    iget-boolean v2, p0, LX/HKk;->f:Z

    iget-boolean v3, p1, LX/HKk;->f:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 2455010
    goto :goto_0

    .line 2455011
    :cond_11
    iget-object v2, p0, LX/HKk;->g:LX/0Px;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/HKk;->g:LX/0Px;

    iget-object v3, p1, LX/HKk;->g:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2455012
    goto/16 :goto_0

    .line 2455013
    :cond_12
    iget-object v2, p1, LX/HKk;->g:LX/0Px;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
