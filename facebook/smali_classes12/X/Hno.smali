.class public final LX/Hno;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2503758
    const-class v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubNTTabQueryModel;

    const v0, 0x2e13ae51

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "ElectionHubNTTabQuery"

    const-string v6, "7fb04bc59ae30866af690b4b40b24aef"

    const-string v7, "election_hub"

    const-string v8, "10155255658626729"

    const-string v9, "10155259086596729"

    .line 2503759
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2503760
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2503761
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2503762
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2503763
    sparse-switch v0, :sswitch_data_0

    .line 2503764
    :goto_0
    return-object p1

    .line 2503765
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2503766
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2503767
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2503768
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2503769
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2503770
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2503771
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2503772
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2503773
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2503774
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2503775
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2503776
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_b
        -0x680de62a -> :sswitch_4
        -0x6326fdb3 -> :sswitch_9
        -0x4496acc9 -> :sswitch_7
        -0x25a646c8 -> :sswitch_3
        -0x1b87b280 -> :sswitch_6
        -0x14283bca -> :sswitch_1
        -0x12efdeb3 -> :sswitch_5
        0xa1fa812 -> :sswitch_2
        0x214100e0 -> :sswitch_8
        0x47261325 -> :sswitch_0
        0x73a026b5 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2503777
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2503778
    :goto_1
    return v0

    .line 2503779
    :pswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2503780
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
