.class public final LX/IBd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field public final synthetic a:LX/IBg;


# direct methods
.method public constructor <init>(LX/IBg;)V
    .locals 0

    .prologue
    .line 2547342
    iput-object p1, p0, LX/IBd;->a:LX/IBg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 5

    .prologue
    .line 2547343
    iget-object v0, p0, LX/IBd;->a:LX/IBg;

    iget-object v0, v0, LX/IBg;->c:LX/1nQ;

    const-string v1, "event_location_summary_cancel"

    iget-object v2, p0, LX/IBd;->a:LX/IBg;

    iget-object v2, v2, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547344
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2547345
    iget-object v3, p0, LX/IBd;->a:LX/IBg;

    iget-object v3, v3, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547346
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v3, v4

    .line 2547347
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    iget-object v4, p0, LX/IBd;->a:LX/IBg;

    iget-object v4, v4, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547348
    iget-object p0, v4, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v4, p0

    .line 2547349
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1nQ;->c(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2547350
    return-void
.end method
