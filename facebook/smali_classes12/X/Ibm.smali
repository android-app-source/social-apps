.class public final LX/Ibm;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/business/ride/gating/IsRideServiceComposerEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HiN;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ic6;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IJn;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ib2;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2594783
    const-class v0, LX/Ibm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ibm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2594771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2594772
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2594773
    iput-object v0, p0, LX/Ibm;->c:LX/0Ot;

    .line 2594774
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2594775
    iput-object v0, p0, LX/Ibm;->d:LX/0Ot;

    .line 2594776
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2594777
    iput-object v0, p0, LX/Ibm;->e:LX/0Ot;

    .line 2594778
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2594779
    iput-object v0, p0, LX/Ibm;->f:LX/0Ot;

    .line 2594780
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2594781
    iput-object v0, p0, LX/Ibm;->g:LX/0Ot;

    .line 2594782
    return-void
.end method

.method public static a(LX/0QB;)LX/Ibm;
    .locals 10

    .prologue
    .line 2594763
    new-instance v0, LX/Ibm;

    invoke-direct {v0}, LX/Ibm;-><init>()V

    .line 2594764
    const/16 v1, 0x14ff

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x167a

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x27c5

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x26c0

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2316

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x26b4

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    .line 2594765
    iput-object v1, v0, LX/Ibm;->b:LX/0Or;

    iput-object v2, v0, LX/Ibm;->c:LX/0Ot;

    iput-object v3, v0, LX/Ibm;->d:LX/0Ot;

    iput-object v4, v0, LX/Ibm;->e:LX/0Ot;

    iput-object v5, v0, LX/Ibm;->f:LX/0Ot;

    iput-object v6, v0, LX/Ibm;->g:LX/0Ot;

    iput-object v7, v0, LX/Ibm;->h:Ljava/util/concurrent/ExecutorService;

    iput-object v8, v0, LX/Ibm;->i:LX/03V;

    iput-object v9, v0, LX/Ibm;->j:LX/0Uh;

    .line 2594766
    move-object v0, v0

    .line 2594767
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2594768
    iget-object v0, p0, LX/Ibm;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ibm;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Ibm;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2594769
    :cond_0
    :goto_0
    return-void

    .line 2594770
    :cond_1
    iget-object v0, p0, LX/Ibm;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0
.end method
