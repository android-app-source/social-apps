.class public final LX/JVg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

.field public final synthetic d:Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)V
    .locals 0

    .prologue
    .line 2701286
    iput-object p1, p0, LX/JVg;->d:Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    iput-object p2, p0, LX/JVg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/JVg;->b:Ljava/lang/String;

    iput-object p4, p0, LX/JVg;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1855ef1f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2701287
    iget-object v1, p0, LX/JVg;->d:Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    iget-object v2, p0, LX/JVg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/JVg;->b:Ljava/lang/String;

    .line 2701288
    const v5, 0x7f0d0083

    invoke-virtual {p1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 2701289
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2701290
    iget-object v5, v1, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    sget-object v6, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->a:Ljava/lang/String;

    const-string v7, "Null url passed when logging IX organic share click"

    invoke-virtual {v5, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2701291
    :goto_0
    iget-object v1, p0, LX/JVg;->d:Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->k:LX/3i4;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/JVg;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    invoke-virtual {v1, v2, v3}, LX/3i4;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)V

    .line 2701292
    const v1, -0x32bf0c8b

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2701293
    :cond_0
    invoke-static {v2}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v7

    .line 2701294
    invoke-static {v2}, LX/1WF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 2701295
    if-eqz v5, :cond_2

    invoke-static {v5}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    :goto_1
    move-object v8, v5

    .line 2701296
    const-string v9, "native_newsfeed"

    .line 2701297
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v5

    .line 2701298
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v6}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v11

    move-object v6, v3

    invoke-static/range {v6 .. v11}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2701299
    invoke-static {v5}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2701300
    invoke-static {v5, p1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 2701301
    :cond_1
    iget-object v6, v1, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->o:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method
