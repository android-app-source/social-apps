.class public final LX/Io6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/p2p/model/PaymentCard;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Io9;


# direct methods
.method public constructor <init>(LX/Io9;)V
    .locals 0

    .prologue
    .line 2611538
    iput-object p1, p0, LX/Io6;->a:LX/Io9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2611523
    iget-object v0, p0, LX/Io6;->a:LX/Io9;

    iget-object v0, v0, LX/Io9;->e:LX/03V;

    sget-object v1, LX/Io9;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch PaymentCards for sending money."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2611524
    iget-object v0, p0, LX/Io6;->a:LX/Io9;

    iget-object v0, v0, LX/Io9;->b:LX/Iab;

    if-eqz v0, :cond_0

    .line 2611525
    iget-object v0, p0, LX/Io6;->a:LX/Io9;

    iget-object v0, v0, LX/Io9;->b:LX/Iab;

    .line 2611526
    iget-object v1, v0, LX/Iab;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Z)V

    .line 2611527
    iget-object v1, v0, LX/Iab;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->h:LX/03V;

    const-string v2, "BusinessCreateAccountFragment"

    const-string p0, "Failed to fetch payment cards"

    invoke-static {v2, p0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/03V;->a(LX/0VG;)V

    .line 2611528
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2611529
    check-cast p1, LX/0Px;

    .line 2611530
    iget-object v0, p0, LX/Io6;->a:LX/Io9;

    iget-object v0, v0, LX/Io9;->b:LX/Iab;

    if-eqz v0, :cond_0

    .line 2611531
    iget-object v0, p0, LX/Io6;->a:LX/Io9;

    iget-object v0, v0, LX/Io9;->b:LX/Iab;

    .line 2611532
    iget-object v1, v0, LX/Iab;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    const/4 p0, 0x0

    invoke-static {v1, p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Z)V

    .line 2611533
    iget-object v1, v0, LX/Iab;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    .line 2611534
    iput-object p1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->l:LX/0Px;

    .line 2611535
    iget-object v1, v0, LX/Iab;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-nez v1, :cond_0

    .line 2611536
    iget-object p0, v0, LX/Iab;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-static {p0, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/payments/p2p/model/PaymentCard;)V

    .line 2611537
    :cond_0
    return-void
.end method
