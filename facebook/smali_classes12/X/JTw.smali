.class public final LX/JTw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:LX/JUH;

.field public final synthetic b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;LX/JUH;)V
    .locals 0

    .prologue
    .line 2697466
    iput-object p1, p0, LX/JTw;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    iput-object p2, p0, LX/JTw;->a:LX/JUH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 11

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    .line 2697467
    invoke-virtual {p1}, LX/6al;->a()V

    .line 2697468
    iget-object v0, p0, LX/JTw;->a:LX/JUH;

    iget-object v0, v0, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    .line 2697469
    iget-object v1, p0, LX/JTw;->a:LX/JUH;

    iget-object v1, v1, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->o()D

    move-result-wide v2

    .line 2697470
    invoke-static {v0, v2, v3}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLLocation;D)LX/697;

    move-result-object v1

    iget-object v4, p0, LX/JTw;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0f17

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v1, v4}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v1

    .line 2697471
    invoke-virtual {p1, v1}, LX/6al;->a(LX/6aM;)V

    .line 2697472
    iget-object v1, p1, LX/6al;->a:LX/680;

    move-object v1, v1

    .line 2697473
    invoke-static {v1, v0, v2, v3}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->b(LX/680;Lcom/facebook/graphql/model/GraphQLLocation;D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2697474
    new-instance v1, LX/699;

    invoke-direct {v1}, LX/699;-><init>()V

    new-instance v4, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2697475
    iput-object v4, v1, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2697476
    move-object v1, v1

    .line 2697477
    iget-object v4, p0, LX/JTw;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    .line 2697478
    iget-object v5, p1, LX/6al;->a:LX/680;

    move-object v5, v5

    .line 2697479
    invoke-static {v4, v5, v0, v2, v3}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->c(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;LX/680;Lcom/facebook/graphql/model/GraphQLLocation;D)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, LX/690;->a(Landroid/graphics/Bitmap;)LX/68w;

    move-result-object v0

    .line 2697480
    iput-object v0, v1, LX/699;->c:LX/68w;

    .line 2697481
    move-object v0, v1

    .line 2697482
    invoke-virtual {v0, v10, v10}, LX/699;->a(FF)LX/699;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/6al;->a(LX/699;)LX/6ax;

    .line 2697483
    :cond_0
    new-instance v0, LX/JTv;

    invoke-direct {v0, p0}, LX/JTv;-><init>(LX/JTw;)V

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6Zu;)V

    .line 2697484
    return-void
.end method
