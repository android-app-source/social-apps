.class public LX/Hsf;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hse;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hsg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2514764
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hsf;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hsg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514765
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2514766
    iput-object p1, p0, LX/Hsf;->b:LX/0Ot;

    .line 2514767
    return-void
.end method

.method public static a(LX/0QB;)LX/Hsf;
    .locals 4

    .prologue
    .line 2514777
    const-class v1, LX/Hsf;

    monitor-enter v1

    .line 2514778
    :try_start_0
    sget-object v0, LX/Hsf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2514779
    sput-object v2, LX/Hsf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2514780
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2514781
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2514782
    new-instance v3, LX/Hsf;

    const/16 p0, 0x197b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hsf;-><init>(LX/0Ot;)V

    .line 2514783
    move-object v0, v3

    .line 2514784
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2514785
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hsf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2514786
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2514787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2514768
    invoke-static {}, LX/1dS;->b()V

    .line 2514769
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/16 v0, 0x1e

    const v1, -0x553b9c0c

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2514770
    check-cast p6, LX/Hsd;

    .line 2514771
    iget-object v0, p0, LX/Hsf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hsg;

    iget-object v4, p6, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    iget-object v5, p6, LX/Hsd;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move v1, p3

    move v2, p4

    move-object v3, p5

    .line 2514772
    iget-object p0, v0, LX/Hsg;->a:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-virtual {p0, v5, v4}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/attachments/angora/AngoraAttachmentView;)V

    .line 2514773
    invoke-virtual {v4, v1, v2}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->measure(II)V

    .line 2514774
    invoke-virtual {v4}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->getMeasuredWidth()I

    move-result p0

    iput p0, v3, LX/1no;->a:I

    .line 2514775
    invoke-virtual {v4}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->getMeasuredHeight()I

    move-result p0

    iput p0, v3, LX/1no;->b:I

    .line 2514776
    const/16 v0, 0x1f

    const v1, 0x1f6e8585

    invoke-static {v7, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 2514757
    check-cast p1, LX/Hsd;

    .line 2514758
    check-cast p2, LX/Hsd;

    .line 2514759
    iget-object v0, p1, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    iput-object v0, p2, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    .line 2514760
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2514761
    iget-object v0, p0, LX/Hsf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2514762
    new-instance v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-direct {v0, p1}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2514763
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2514740
    const/4 v0, 0x1

    return v0
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 2514741
    check-cast p2, LX/Hsd;

    .line 2514742
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2514743
    iget-object v0, p0, LX/Hsf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2514744
    new-instance v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-direct {v0, p1}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;)V

    .line 2514745
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2514746
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2514747
    check-cast v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    iput-object v0, p2, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    .line 2514748
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2514749
    return-void
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 2514750
    check-cast p3, LX/Hsd;

    .line 2514751
    iget-object v0, p0, LX/Hsf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hsg;

    check-cast p2, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    iget-object v1, p3, LX/Hsd;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2514752
    iget-object p0, v0, LX/Hsg;->a:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-virtual {p0, v1, p2}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/attachments/angora/AngoraAttachmentView;)V

    .line 2514753
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2514754
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2514755
    const/16 v0, 0xf

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2514756
    const/4 v0, 0x1

    return v0
.end method
