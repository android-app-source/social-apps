.class public LX/HyA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/HyA;


# instance fields
.field private final a:LX/0Yj;

.field private final b:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final c:LX/0id;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0id;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2523659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2523660
    iput-object p1, p0, LX/HyA;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2523661
    iput-object p2, p0, LX/HyA;->c:LX/0id;

    .line 2523662
    new-instance v0, LX/0Yj;

    const v1, 0x60012

    const-string v2, "EventsDashboardFragment"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "event_dashboard"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 2523663
    iput-boolean v4, v0, LX/0Yj;->n:Z

    .line 2523664
    move-object v0, v0

    .line 2523665
    iput-object v0, p0, LX/HyA;->a:LX/0Yj;

    .line 2523666
    return-void
.end method

.method public static a(LX/0QB;)LX/HyA;
    .locals 5

    .prologue
    .line 2523667
    sget-object v0, LX/HyA;->d:LX/HyA;

    if-nez v0, :cond_1

    .line 2523668
    const-class v1, LX/HyA;

    monitor-enter v1

    .line 2523669
    :try_start_0
    sget-object v0, LX/HyA;->d:LX/HyA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2523670
    if-eqz v2, :cond_0

    .line 2523671
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2523672
    new-instance p0, LX/HyA;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v4

    check-cast v4, LX/0id;

    invoke-direct {p0, v3, v4}, LX/HyA;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0id;)V

    .line 2523673
    move-object v0, p0

    .line 2523674
    sput-object v0, LX/HyA;->d:LX/HyA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2523675
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2523676
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2523677
    :cond_1
    sget-object v0, LX/HyA;->d:LX/HyA;

    return-object v0

    .line 2523678
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2523679
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2523657
    iget-object v0, p0, LX/HyA;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/HyA;->a:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2523658
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2523680
    if-eqz p1, :cond_0

    .line 2523681
    iget-object v0, p0, LX/HyA;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x60012

    const-string v2, "EventsDashboardFragment"

    const/4 v3, 0x0

    const-string v4, "load_type"

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2523682
    :goto_0
    iget-object v0, p0, LX/HyA;->c:LX/0id;

    const-string v1, "LoadEventsDashboard"

    invoke-virtual {v0, v1, p1}, LX/0id;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523683
    return-void

    .line 2523684
    :cond_0
    iget-object v0, p0, LX/HyA;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/HyA;->a:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2523654
    iget-object v0, p0, LX/HyA;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/HyA;->a:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 2523655
    iget-object v0, p0, LX/HyA;->c:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 2523656
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2523650
    iget-object v0, p0, LX/HyA;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000e

    const-string v2, "EventsDashboardChangeFilter"

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2523651
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2523652
    iget-object v0, p0, LX/HyA;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000e

    const-string v2, "EventsDashboardChangeFilter"

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2523653
    return-void
.end method
