.class public final LX/I5G;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I4u;

.field public final synthetic b:LX/I5J;


# direct methods
.method public constructor <init>(LX/I5J;LX/I4u;)V
    .locals 0

    .prologue
    .line 2535218
    iput-object p1, p0, LX/I5G;->b:LX/I5J;

    iput-object p2, p0, LX/I5G;->a:LX/I4u;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2535219
    iget-object v0, p0, LX/I5G;->a:LX/I4u;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I4u;->a(Ljava/util/List;)V

    .line 2535220
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2535221
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2535222
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535223
    if-eqz v0, :cond_0

    .line 2535224
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535225
    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2535226
    iget-object v1, p0, LX/I5G;->a:LX/I4u;

    .line 2535227
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535228
    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/I4u;->a(Ljava/util/List;)V

    .line 2535229
    :cond_0
    return-void
.end method
