.class public LX/IVP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/IVV;

.field public final c:LX/IVJ;

.field public final d:LX/1g8;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2581906
    const-class v0, LX/IVP;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IVP;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/IVV;LX/IVJ;LX/1g8;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IVV;",
            "LX/IVJ;",
            "LX/1g8;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2581908
    iput-object p1, p0, LX/IVP;->b:LX/IVV;

    .line 2581909
    iput-object p2, p0, LX/IVP;->c:LX/IVJ;

    .line 2581910
    iput-object p3, p0, LX/IVP;->d:LX/1g8;

    .line 2581911
    iput-object p4, p0, LX/IVP;->e:LX/0Ot;

    .line 2581912
    return-void
.end method

.method public static a(LX/0QB;)LX/IVP;
    .locals 7

    .prologue
    .line 2581895
    const-class v1, LX/IVP;

    monitor-enter v1

    .line 2581896
    :try_start_0
    sget-object v0, LX/IVP;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581897
    sput-object v2, LX/IVP;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581898
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581899
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581900
    new-instance v6, LX/IVP;

    invoke-static {v0}, LX/IVV;->a(LX/0QB;)LX/IVV;

    move-result-object v3

    check-cast v3, LX/IVV;

    invoke-static {v0}, LX/IVJ;->a(LX/0QB;)LX/IVJ;

    move-result-object v4

    check-cast v4, LX/IVJ;

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v5

    check-cast v5, LX/1g8;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/IVP;-><init>(LX/IVV;LX/IVJ;LX/1g8;LX/0Ot;)V

    .line 2581901
    move-object v0, v6

    .line 2581902
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581903
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581904
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2581884
    const/4 v1, 0x0

    .line 2581885
    if-nez p0, :cond_1

    move-object v0, v1

    .line 2581886
    :goto_0
    move-object v0, v0

    .line 2581887
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2581888
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    .line 2581889
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2581890
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2581891
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    .line 2581892
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 2581893
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2581894
    goto :goto_0
.end method
