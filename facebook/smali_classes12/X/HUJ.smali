.class public final LX/HUJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;)V
    .locals 0

    .prologue
    .line 2471942
    iput-object p1, p0, LX/HUJ;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2471943
    const/16 v4, 0x64

    .line 2471944
    new-instance v0, LX/HUM;

    invoke-direct {v0}, LX/HUM;-><init>()V

    move-object v0, v0

    .line 2471945
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2471946
    new-instance v1, LX/HUM;

    invoke-direct {v1}, LX/HUM;-><init>()V

    const-string v2, "page_id"

    iget-object v3, p0, LX/HUJ;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->n:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "num_issue_views"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "num_issue_opinions"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "media_type"

    sget-object v3, LX/0wF;->IMAGEWEBP:LX/0wF;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    .line 2471947
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2471948
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2471949
    iget-object v1, p0, LX/HUJ;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
