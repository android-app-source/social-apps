.class public final LX/IO1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/IN0;

.field public final synthetic c:LX/IO3;


# direct methods
.method public constructor <init>(LX/IO3;Ljava/lang/String;LX/IN0;)V
    .locals 0

    .prologue
    .line 2571340
    iput-object p1, p0, LX/IO1;->c:LX/IO3;

    iput-object p2, p0, LX/IO1;->a:Ljava/lang/String;

    iput-object p3, p0, LX/IO1;->b:LX/IN0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x4f9d1f07

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2571341
    iget-object v0, p0, LX/IO1;->c:LX/IO3;

    iget-boolean v0, v0, LX/IO3;->n:Z

    if-eqz v0, :cond_0

    .line 2571342
    iget-object v0, p0, LX/IO1;->c:LX/IO3;

    iget-object v0, v0, LX/IO3;->b:LX/3mF;

    iget-object v2, p0, LX/IO1;->c:LX/IO3;

    iget-object v2, v2, LX/IO3;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/IO1;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2571343
    :goto_0
    iget-object v2, p0, LX/IO1;->c:LX/IO3;

    iget-object v2, v2, LX/IO3;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2571344
    iget-object v3, p0, LX/IO1;->c:LX/IO3;

    iget-object v3, v3, LX/IO3;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-static {v3}, LX/9Nh;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;)LX/9Nh;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v4

    .line 2571345
    sget-object v5, LX/IO2;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2571346
    :goto_1
    move-object v4, v4

    .line 2571347
    iput-object v4, v3, LX/9Nh;->j:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2571348
    move-object v3, v3

    .line 2571349
    invoke-virtual {v3}, LX/9Nh;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    move-result-object v3

    .line 2571350
    iget-object v4, p0, LX/IO1;->c:LX/IO3;

    .line 2571351
    iput-object v3, v4, LX/IO3;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2571352
    iget-object v4, p0, LX/IO1;->c:LX/IO3;

    iget-boolean v4, v4, LX/IO3;->n:Z

    .line 2571353
    iget-object v5, p0, LX/IO1;->c:LX/IO3;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v6

    invoke-static {v5, v6}, LX/IO3;->setJoinButtonState(LX/IO3;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    .line 2571354
    new-instance v5, LX/IO0;

    invoke-direct {v5, p0, v3, v4, v2}, LX/IO0;-><init>(LX/IO1;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;ZLcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;)V

    iget-object v2, p0, LX/IO1;->c:LX/IO3;

    iget-object v2, v2, LX/IO3;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v5, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2571355
    const v0, -0x5280b9a2

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2571356
    :cond_0
    iget-object v0, p0, LX/IO1;->c:LX/IO3;

    iget-object v0, v0, LX/IO3;->b:LX/3mF;

    iget-object v2, p0, LX/IO1;->c:LX/IO3;

    iget-object v2, v2, LX/IO3;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/IO1;->a:Ljava/lang/String;

    const-string v4, "ALLOW_READD"

    invoke-virtual {v0, v2, v3, v4}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2571357
    :pswitch_0
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1

    .line 2571358
    :pswitch_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1

    .line 2571359
    :pswitch_2
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1

    .line 2571360
    :pswitch_3
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
