.class public LX/Irg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/LayoutInflater;

.field public final b:Landroid/view/ViewGroup;

.field private final c:LX/0Zk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zk",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Zk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zk",
            "<",
            "Lcom/facebook/messaging/photos/editing/LayerEditText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    const/16 v1, 0x14

    .line 2618559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2618560
    new-instance v0, LX/0Zj;

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    iput-object v0, p0, LX/Irg;->c:LX/0Zk;

    .line 2618561
    new-instance v0, LX/0Zj;

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    iput-object v0, p0, LX/Irg;->d:LX/0Zk;

    .line 2618562
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/Irg;->b:Landroid/view/ViewGroup;

    .line 2618563
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/Irg;->a:Landroid/view/LayoutInflater;

    .line 2618564
    return-void
.end method

.method private static a(LX/0Zk;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Zk",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 2618570
    :try_start_0
    invoke-interface {p0, p1}, LX/0Zk;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2618571
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 2618572
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2618573
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    .line 2618574
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    .line 2618575
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 2618576
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2618577
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 2618578
    instance-of v0, p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_1

    .line 2618579
    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2618580
    invoke-virtual {p1, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2618581
    iget-object v0, p0, LX/Irg;->c:LX/0Zk;

    invoke-static {v0, p1}, LX/Irg;->a(LX/0Zk;Ljava/lang/Object;)V

    .line 2618582
    :cond_0
    :goto_0
    return-void

    .line 2618583
    :cond_1
    instance-of v0, p1, Lcom/facebook/messaging/photos/editing/LayerEditText;

    if-eqz v0, :cond_0

    .line 2618584
    check-cast p1, Lcom/facebook/messaging/photos/editing/LayerEditText;

    .line 2618585
    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2618586
    invoke-virtual {p1, v3}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setHintTextColor(I)V

    .line 2618587
    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2618588
    invoke-virtual {p1, v3}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setTextColor(I)V

    .line 2618589
    const/4 v0, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2618590
    invoke-static {p1, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2618591
    iget-object v0, p0, LX/Irg;->d:LX/0Zk;

    invoke-static {v0, p1}, LX/Irg;->a(LX/0Zk;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2618565
    iget-object v0, p0, LX/Irg;->c:LX/0Zk;

    invoke-interface {v0}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2618566
    if-nez v0, :cond_0

    .line 2618567
    iget-object v0, p0, LX/Irg;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f0313c2

    iget-object v3, p0, LX/Irg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2618568
    :goto_0
    return-object v0

    .line 2618569
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0
.end method
