.class public LX/IuF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/IuF;


# instance fields
.field public final b:LX/1Hr;

.field public c:Ljava/security/SecureRandom;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2625860
    const-class v0, LX/IuF;

    sput-object v0, LX/IuF;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Hr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625862
    iput-object p1, p0, LX/IuF;->b:LX/1Hr;

    .line 2625863
    return-void
.end method

.method public static a(LX/0QB;)LX/IuF;
    .locals 4

    .prologue
    .line 2625864
    sget-object v0, LX/IuF;->d:LX/IuF;

    if-nez v0, :cond_1

    .line 2625865
    const-class v1, LX/IuF;

    monitor-enter v1

    .line 2625866
    :try_start_0
    sget-object v0, LX/IuF;->d:LX/IuF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2625867
    if-eqz v2, :cond_0

    .line 2625868
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2625869
    new-instance p0, LX/IuF;

    invoke-static {v0}, LX/1Hr;->a(LX/0QB;)LX/1Hr;

    move-result-object v3

    check-cast v3, LX/1Hr;

    invoke-direct {p0, v3}, LX/IuF;-><init>(LX/1Hr;)V

    .line 2625870
    move-object v0, p0

    .line 2625871
    sput-object v0, LX/IuF;->d:LX/IuF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2625872
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2625873
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2625874
    :cond_1
    sget-object v0, LX/IuF;->d:LX/IuF;

    return-object v0

    .line 2625875
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2625876
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()[B
    .locals 2

    .prologue
    .line 2625877
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 2625878
    iget-object v1, p0, LX/IuF;->c:Ljava/security/SecureRandom;

    if-nez v1, :cond_0

    .line 2625879
    invoke-static {}, LX/1Hr;->a()Ljava/security/SecureRandom;

    move-result-object v1

    iput-object v1, p0, LX/IuF;->c:Ljava/security/SecureRandom;

    .line 2625880
    :cond_0
    iget-object v1, p0, LX/IuF;->c:Ljava/security/SecureRandom;

    move-object v1, v1

    .line 2625881
    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 2625882
    return-object v0
.end method
