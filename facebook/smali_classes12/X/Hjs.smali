.class public final enum LX/Hjs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hjs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/Hjs;

.field public static final enum b:LX/Hjs;

.field private static final synthetic d:[LX/Hjs;


# instance fields
.field private final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LX/Hjs;

    const-string v1, "ADS"

    invoke-direct {v0, v1, v2, v2}, LX/Hjs;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hjs;->a:LX/Hjs;

    new-instance v0, LX/Hjs;

    const-string v1, "APP_OF_THE_DAY"

    invoke-direct {v0, v1, v3, v3}, LX/Hjs;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hjs;->b:LX/Hjs;

    const/4 v0, 0x2

    new-array v0, v0, [LX/Hjs;

    sget-object v1, LX/Hjs;->a:LX/Hjs;

    aput-object v1, v0, v2

    sget-object v1, LX/Hjs;->b:LX/Hjs;

    aput-object v1, v0, v3

    sput-object v0, LX/Hjs;->d:[LX/Hjs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/Hjs;->c:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hjs;
    .locals 1

    const-class v0, LX/Hjs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hjs;

    return-object v0
.end method

.method public static values()[LX/Hjs;
    .locals 1

    sget-object v0, LX/Hjs;->d:[LX/Hjs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hjs;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, LX/Hjs;->c:I

    return v0
.end method
