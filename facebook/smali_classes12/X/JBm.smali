.class public final LX/JBm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2660659
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2660660
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2660661
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2660662
    invoke-static {p0, p1}, LX/JBm;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2660663
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2660664
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2660653
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2660654
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2660655
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/JBm;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2660656
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2660657
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2660658
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2660556
    const/4 v10, 0x0

    .line 2660557
    const/4 v9, 0x0

    .line 2660558
    const/4 v8, 0x0

    .line 2660559
    const/4 v7, 0x0

    .line 2660560
    const/4 v6, 0x0

    .line 2660561
    const/4 v5, 0x0

    .line 2660562
    const/4 v4, 0x0

    .line 2660563
    const/4 v3, 0x0

    .line 2660564
    const/4 v2, 0x0

    .line 2660565
    const/4 v1, 0x0

    .line 2660566
    const/4 v0, 0x0

    .line 2660567
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 2660568
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2660569
    const/4 v0, 0x0

    .line 2660570
    :goto_0
    return v0

    .line 2660571
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2660572
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 2660573
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2660574
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2660575
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2660576
    const-string v12, "curation_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2660577
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2660578
    :cond_2
    const-string v12, "eligibleItemsSuggestions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2660579
    invoke-static {p0, p1}, LX/JBB;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2660580
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2660581
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2660582
    :cond_4
    const-string v12, "items"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2660583
    invoke-static {p0, p1}, LX/JB6;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2660584
    :cond_5
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2660585
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2660586
    :cond_6
    const-string v12, "rating_title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2660587
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2660588
    :cond_7
    const-string v12, "style_list"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2660589
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2660590
    :cond_8
    const-string v12, "supports_suggestions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2660591
    const/4 v0, 0x1

    .line 2660592
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    goto/16 :goto_1

    .line 2660593
    :cond_9
    const-string v12, "tracking"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 2660594
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 2660595
    :cond_a
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2660596
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 2660597
    :cond_b
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2660598
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 2660599
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2660600
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 2660601
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 2660602
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 2660603
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 2660604
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 2660605
    if-eqz v0, :cond_c

    .line 2660606
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2660607
    :cond_c
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2660608
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2660609
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 2660610
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2660611
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2660612
    if-eqz v0, :cond_0

    .line 2660613
    const-string v1, "curation_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660614
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2660615
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2660616
    if-eqz v0, :cond_1

    .line 2660617
    const-string v1, "eligibleItemsSuggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660618
    invoke-static {p0, v0, p2, p3}, LX/JBB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2660619
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2660620
    if-eqz v0, :cond_2

    .line 2660621
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660622
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2660623
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2660624
    if-eqz v0, :cond_3

    .line 2660625
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660626
    invoke-static {p0, v0, p2, p3}, LX/JB6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2660627
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2660628
    if-eqz v0, :cond_4

    .line 2660629
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660630
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2660631
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2660632
    if-eqz v0, :cond_5

    .line 2660633
    const-string v1, "rating_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660634
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2660635
    :cond_5
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2660636
    if-eqz v0, :cond_6

    .line 2660637
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660638
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2660639
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2660640
    if-eqz v0, :cond_7

    .line 2660641
    const-string v1, "supports_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660642
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2660643
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2660644
    if-eqz v0, :cond_8

    .line 2660645
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660646
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2660647
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2660648
    if-eqz v0, :cond_9

    .line 2660649
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2660650
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2660651
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2660652
    return-void
.end method
