.class public LX/I5f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0c5;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/IBQ;

.field public final c:LX/0fz;

.field public final d:LX/0kb;

.field public final e:LX/0Xw;

.field public final f:LX/1Ck;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/I5d;

.field public final i:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/feed/data/EventFeedStories;",
            ">;>;"
        }
    .end annotation
.end field

.field public final j:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/events/feed/data/EventFeedStories;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/feed/data/EventFeedStories;",
            ">;>;"
        }
    .end annotation
.end field

.field public final l:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/events/feed/data/EventFeedStories;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/I5h;

.field public n:LX/I7I;

.field public o:LX/I5b;

.field public p:LX/I5b;

.field public q:Z

.field public r:I

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2536011
    const-class v0, LX/I5f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/I5f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/IBQ;LX/1Ck;LX/0fz;LX/0kb;LX/0Xw;LX/0Ot;)V
    .locals 1
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IBQ;",
            "LX/1Ck;",
            "LX/0fz;",
            "LX/0kb;",
            "LX/0Xw;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2535997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2535998
    const/4 v0, 0x0

    iput v0, p0, LX/I5f;->r:I

    .line 2535999
    iput-object p1, p0, LX/I5f;->b:LX/IBQ;

    .line 2536000
    iput-object p3, p0, LX/I5f;->c:LX/0fz;

    .line 2536001
    iput-object p2, p0, LX/I5f;->f:LX/1Ck;

    .line 2536002
    iput-object p4, p0, LX/I5f;->d:LX/0kb;

    .line 2536003
    iput-object p5, p0, LX/I5f;->e:LX/0Xw;

    .line 2536004
    iput-object p6, p0, LX/I5f;->g:LX/0Ot;

    .line 2536005
    new-instance v0, LX/I5W;

    invoke-direct {v0, p0}, LX/I5W;-><init>(LX/I5f;)V

    iput-object v0, p0, LX/I5f;->i:Ljava/util/concurrent/Callable;

    .line 2536006
    new-instance v0, LX/I5X;

    invoke-direct {v0, p0}, LX/I5X;-><init>(LX/I5f;)V

    iput-object v0, p0, LX/I5f;->j:LX/0Vd;

    .line 2536007
    new-instance v0, LX/I5Y;

    invoke-direct {v0, p0}, LX/I5Y;-><init>(LX/I5f;)V

    iput-object v0, p0, LX/I5f;->k:Ljava/util/concurrent/Callable;

    .line 2536008
    new-instance v0, LX/I5Z;

    invoke-direct {v0, p0}, LX/I5Z;-><init>(LX/I5f;)V

    iput-object v0, p0, LX/I5f;->l:LX/0Vd;

    .line 2536009
    new-instance v0, LX/I5d;

    invoke-direct {v0, p0}, LX/I5d;-><init>(LX/I5f;)V

    iput-object v0, p0, LX/I5f;->h:LX/I5d;

    .line 2536010
    return-void
.end method

.method public static a(LX/0QB;)LX/I5f;
    .locals 1

    .prologue
    .line 2535996
    invoke-static {p0}, LX/I5f;->b(LX/0QB;)LX/I5f;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/I5f;
    .locals 7

    .prologue
    .line 2535994
    new-instance v0, LX/I5f;

    invoke-static {p0}, LX/IBQ;->a(LX/0QB;)LX/IBQ;

    move-result-object v1

    check-cast v1, LX/IBQ;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/0fz;->b(LX/0QB;)LX/0fz;

    move-result-object v3

    check-cast v3, LX/0fz;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {p0}, LX/29m;->a(LX/0QB;)LX/0Xw;

    move-result-object v5

    check-cast v5, LX/0Xw;

    const/16 v6, 0x1430

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/I5f;-><init>(LX/IBQ;LX/1Ck;LX/0fz;LX/0kb;LX/0Xw;LX/0Ot;)V

    .line 2535995
    return-object v0
.end method

.method public static j(LX/I5f;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/feed/data/EventFeedStories;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2535964
    iget-object v0, p0, LX/I5f;->p:LX/I5b;

    invoke-virtual {v0}, LX/I5a;->a()V

    .line 2535965
    iget v0, p0, LX/I5f;->r:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "cold_start_cursor"

    .line 2535966
    :goto_0
    iget-object v1, p0, LX/I5f;->m:LX/I5h;

    .line 2535967
    iget-boolean v2, p0, LX/I5f;->s:Z

    if-eqz v2, :cond_1

    .line 2535968
    sget-object v2, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    .line 2535969
    :goto_1
    move-object v2, v2

    .line 2535970
    new-instance v3, LX/0rT;

    invoke-direct {v3}, LX/0rT;-><init>()V

    iget v4, v1, LX/I5h;->d:I

    .line 2535971
    iput v4, v3, LX/0rT;->c:I

    .line 2535972
    move-object v3, v3

    .line 2535973
    iget-object v4, v1, LX/I5h;->e:Lcom/facebook/api/feedtype/FeedType;

    .line 2535974
    iput-object v4, v3, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 2535975
    move-object v3, v3

    .line 2535976
    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2535977
    iput-object v4, v3, LX/0rT;->a:LX/0rS;

    .line 2535978
    move-object v3, v3

    .line 2535979
    iget-object v4, v1, LX/I5h;->f:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2535980
    iput-object v4, v3, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2535981
    move-object v3, v3

    .line 2535982
    invoke-virtual {v3, v2}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v3

    .line 2535983
    iput-object v0, v3, LX/0rT;->g:Ljava/lang/String;

    .line 2535984
    move-object v3, v3

    .line 2535985
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2535986
    const-string v5, "fetchFeedParams"

    invoke-virtual {v3}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2535987
    iget-object v3, v1, LX/I5h;->c:LX/0aG;

    const-string v5, "feed_fetch_news_feed_before"

    const p0, 0x7f1b6c50    # 2.0659273E38f

    invoke-static {v3, v5, v4, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v3

    .line 2535988
    sget-object v4, LX/I5h;->a:LX/0QK;

    iget-object v5, v1, LX/I5h;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2535989
    return-object v0

    .line 2535990
    :cond_0
    iget-object v0, p0, LX/I5f;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2535991
    :cond_1
    iget v2, p0, LX/I5f;->r:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 2535992
    sget-object v2, LX/0gf;->INITIALIZATION:LX/0gf;

    goto :goto_1

    .line 2535993
    :cond_2
    sget-object v2, LX/0gf;->SCROLLING:LX/0gf;

    goto :goto_1
.end method

.method public static l(LX/I5f;)V
    .locals 3

    .prologue
    .line 2536012
    iget-object v0, p0, LX/I5f;->f:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2536013
    iget-object v0, p0, LX/I5f;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/events/feed/data/EventFeedPager$5;

    invoke-direct {v1, p0}, Lcom/facebook/events/feed/data/EventFeedPager$5;-><init>(LX/I5f;)V

    const v2, -0x5a5dd868

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2536014
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/I5f;->q:Z

    .line 2536015
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/feed/data/EventFeedStories;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2535942
    iget-object v0, p0, LX/I5f;->o:LX/I5b;

    invoke-virtual {v0}, LX/I5a;->a()V

    .line 2535943
    iget v0, p0, LX/I5f;->r:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 2535944
    :goto_0
    iget-object v1, p0, LX/I5f;->m:LX/I5h;

    iget-object v2, p0, LX/I5f;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->s()Ljava/lang/String;

    move-result-object v2

    .line 2535945
    new-instance v3, LX/0rT;

    invoke-direct {v3}, LX/0rT;-><init>()V

    iget v4, v1, LX/I5h;->d:I

    .line 2535946
    iput v4, v3, LX/0rT;->c:I

    .line 2535947
    move-object v3, v3

    .line 2535948
    iget-object v4, v1, LX/I5h;->e:Lcom/facebook/api/feedtype/FeedType;

    .line 2535949
    iput-object v4, v3, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 2535950
    move-object v3, v3

    .line 2535951
    iput-object v0, v3, LX/0rT;->a:LX/0rS;

    .line 2535952
    move-object v3, v3

    .line 2535953
    iget-object v4, v1, LX/I5h;->f:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2535954
    iput-object v4, v3, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2535955
    move-object v3, v3

    .line 2535956
    iput-object v2, v3, LX/0rT;->f:Ljava/lang/String;

    .line 2535957
    move-object v3, v3

    .line 2535958
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2535959
    const-string v5, "fetchFeedParams"

    invoke-virtual {v3}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2535960
    iget-object v3, v1, LX/I5h;->c:LX/0aG;

    const-string v5, "feed_fetch_news_feed_after"

    const p0, 0xc6e581e

    invoke-static {v3, v5, v4, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v3

    .line 2535961
    sget-object v4, LX/I5h;->a:LX/0QK;

    iget-object v5, v1, LX/I5h;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2535962
    return-object v0

    .line 2535963
    :cond_0
    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 2535939
    iput-boolean p1, p0, LX/I5f;->s:Z

    .line 2535940
    iget-object v0, p0, LX/I5f;->f:LX/1Ck;

    sget-object v1, LX/I5e;->REFRESH_FEED:LX/I5e;

    iget-object v2, p0, LX/I5f;->k:Ljava/util/concurrent/Callable;

    iget-object v3, p0, LX/I5f;->l:LX/0Vd;

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2535941
    return-void
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 2535937
    invoke-static {p0}, LX/I5f;->l(LX/I5f;)V

    .line 2535938
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2535936
    iget v0, p0, LX/I5f;->r:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2535935
    const-class v0, LX/I5f;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "totalStories"

    iget-object v2, p0, LX/I5f;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "freshStories"

    iget-object v2, p0, LX/I5f;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->w()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "mHasReachedEndOfFeed"

    iget-boolean v2, p0, LX/I5f;->q:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
