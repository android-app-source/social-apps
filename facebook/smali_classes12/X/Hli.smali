.class public LX/Hli;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Hli;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/0aU;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0aU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2498829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498830
    iput-object p1, p0, LX/Hli;->a:Landroid/content/Context;

    .line 2498831
    iput-object p2, p0, LX/Hli;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2498832
    iput-object p3, p0, LX/Hli;->c:LX/0aU;

    .line 2498833
    return-void
.end method

.method public static a(LX/0QB;)LX/Hli;
    .locals 6

    .prologue
    .line 2498834
    sget-object v0, LX/Hli;->d:LX/Hli;

    if-nez v0, :cond_1

    .line 2498835
    const-class v1, LX/Hli;

    monitor-enter v1

    .line 2498836
    :try_start_0
    sget-object v0, LX/Hli;->d:LX/Hli;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2498837
    if-eqz v2, :cond_0

    .line 2498838
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2498839
    new-instance p0, LX/Hli;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v5

    check-cast v5, LX/0aU;

    invoke-direct {p0, v3, v4, v5}, LX/Hli;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0aU;)V

    .line 2498840
    move-object v0, p0

    .line 2498841
    sput-object v0, LX/Hli;->d:LX/Hli;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2498842
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2498843
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2498844
    :cond_1
    sget-object v0, LX/Hli;->d:LX/Hli;

    return-object v0

    .line 2498845
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2498846
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/location/LocationSignalDataPackage;Z)V
    .locals 3

    .prologue
    .line 2498847
    iget-object v0, p0, LX/Hli;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Hli;->a:Landroid/content/Context;

    iget-object v2, p0, LX/Hli;->c:LX/0aU;

    invoke-static {v1, v2, p1, p2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a(Landroid/content/Context;LX/0aU;Lcom/facebook/location/LocationSignalDataPackage;Z)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, LX/Hli;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 2498848
    return-void
.end method
