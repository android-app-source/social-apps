.class public LX/HVp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/E1d;

.field private final b:Landroid/content/Context;

.field private final c:LX/17W;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E1d;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475404
    iput-object p1, p0, LX/HVp;->b:Landroid/content/Context;

    .line 2475405
    iput-object p2, p0, LX/HVp;->a:LX/E1d;

    .line 2475406
    iput-object p3, p0, LX/HVp;->c:LX/17W;

    .line 2475407
    return-void
.end method


# virtual methods
.method public onClick(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 6

    .prologue
    .line 2475408
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v0

    .line 2475409
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2475410
    iget-object v2, p0, LX/HVp;->a:LX/E1d;

    iget-wide v4, p2, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-virtual {v2, v0, v4, v5, v1}, LX/E1d;->a(Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;JLjava/lang/String;)LX/2jY;

    .line 2475411
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2475412
    const-string v3, "reaction_session_id"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475413
    const-string v1, "source_name"

    iget-object v3, p2, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475414
    const-string v1, "page_context_item_type"

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475415
    const-string v0, "com.facebook.katana.profile.id"

    iget-wide v4, p2, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475416
    sget-object v0, LX/0ax;->aO:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p2, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2475417
    iget-object v1, p0, LX/HVp;->c:LX/17W;

    iget-object v3, p0, LX/HVp;->b:Landroid/content/Context;

    invoke-virtual {v1, v3, v0, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2475418
    return-void
.end method
