.class public LX/HGe;
.super LX/Dwa;
.source ""


# static fields
.field public static final t:Ljava/lang/String;


# instance fields
.field public u:LX/HGd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2446365
    const-class v0, LX/HGe;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HGe;->t:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/DwK;LX/Dw8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dux;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DvH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dy7;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/DwK;",
            "LX/Dw8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2446366
    invoke-direct/range {p0 .. p10}, LX/Dwa;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/DwK;LX/Dw8;)V

    .line 2446367
    iput-object p7, p0, LX/HGe;->v:LX/0Ot;

    .line 2446368
    return-void
.end method

.method public static a(Landroid/view/View;)LX/DxP;
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2446369
    if-eqz p0, :cond_0

    instance-of v0, p0, LX/DxP;

    if-nez v0, :cond_1

    .line 2446370
    :cond_0
    new-instance v0, LX/HGc;

    const-string v1, "Original detail view is not PandoraAlbumPermalinkDetailsView"

    invoke-direct {v0, v1}, LX/HGc;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2446371
    :cond_1
    check-cast p0, LX/DxP;

    .line 2446372
    const v0, 0x7f0d0510

    invoke-virtual {p0, v0}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2446373
    if-nez v1, :cond_2

    .line 2446374
    new-instance v0, LX/HGc;

    const-string v1, "PandoraAlbumPermalinkDetailsView layout mismatch"

    invoke-direct {v0, v1}, LX/HGc;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2446375
    :cond_2
    const v0, 0x7f0d2464

    invoke-virtual {p0, v0}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2446376
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_4

    .line 2446377
    :cond_3
    new-instance v0, LX/HGc;

    const-string v1, "PandoraAlbumPermalinkDetailsView layout mismatch"

    invoke-direct {v0, v1}, LX/HGc;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2446378
    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2446379
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2446380
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2446381
    return-object p0
.end method

.method public static a(LX/HGe;Lcom/facebook/graphql/model/GraphQLAlbum;LX/HGb;)V
    .locals 10

    .prologue
    .line 2446382
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->G()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2446383
    if-eqz v0, :cond_2

    .line 2446384
    const/4 v2, 0x0

    .line 2446385
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    .line 2446386
    :goto_1
    if-eqz v1, :cond_5

    const-string v1, ""

    .line 2446387
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v3

    if-nez v3, :cond_6

    .line 2446388
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->z()J

    move-result-wide v3

    .line 2446389
    iget-object v5, p2, LX/HGb;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2446390
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, LX/HGb;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v2, v5}, LX/HGb;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \u00b7 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, LX/HGb;->b:LX/11R;

    .line 2446391
    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-nez v6, :cond_7

    .line 2446392
    const-string v6, ""

    .line 2446393
    :goto_4
    move-object v2, v6

    .line 2446394
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \u00b7 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2446395
    iget-object v2, p2, LX/HGb;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2446396
    iget-object v0, p2, LX/HGb;->c:Landroid/widget/ImageView;

    move-object v0, v0

    .line 2446397
    const/4 v2, 0x0

    .line 2446398
    iget-object v1, p0, LX/HGe;->u:LX/HGd;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/HGe;->u:LX/HGd;

    invoke-interface {v1}, LX/HGd;->a()Z

    move-result v1

    if-nez v1, :cond_8

    .line 2446399
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 2446400
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2446401
    :goto_5
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/HGb;->setVisibility(I)V

    .line 2446402
    :goto_6
    return-void

    .line 2446403
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, LX/HGb;->setVisibility(I)V

    goto :goto_6

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 2446404
    goto :goto_1

    .line 2446405
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2446406
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v2

    goto :goto_3

    :cond_7
    sget-object v6, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v3

    invoke-virtual {v2, v6, v8, v9}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 2446407
    :cond_8
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 2446408
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_9

    .line 2446409
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2446410
    :cond_9
    iget-object v1, p0, LX/HGe;->u:LX/HGd;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5
.end method

.method private c(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2446411
    check-cast p1, Landroid/widget/LinearLayout;

    .line 2446412
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/HGb;

    .line 2446413
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 2446414
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/DxP;

    .line 2446415
    invoke-super {p0, v1, p2}, LX/Dwa;->a(Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    .line 2446416
    :goto_0
    iget-object v1, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    move-object v1, v1

    .line 2446417
    invoke-static {p0, v1, v0}, LX/HGe;->a(LX/HGe;Lcom/facebook/graphql/model/GraphQLAlbum;LX/HGb;)V

    .line 2446418
    return-void

    .line 2446419
    :cond_0
    iget-object v1, p0, LX/HGe;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    sget-object v2, LX/HGe;->t:Ljava/lang/String;

    const-string v3, "PandoraAlbumPermalinkDetailsView is not integrated"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2446420
    if-nez p1, :cond_0

    .line 2446421
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2446422
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2446423
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2446424
    new-instance v3, LX/HGb;

    invoke-direct {v3, v0}, LX/HGb;-><init>(Landroid/content/Context;)V

    .line 2446425
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2446426
    invoke-super {p0, p1, p2}, LX/Dwa;->a(Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 2446427
    :try_start_0
    invoke-static {v0}, LX/HGe;->a(Landroid/view/View;)LX/DxP;

    move-result-object v0

    .line 2446428
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
    :try_end_0
    .catch LX/HGc; {:try_start_0 .. :try_end_0} :catch_0

    .line 2446429
    :goto_0
    iget-object v0, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    move-object v0, v0

    .line 2446430
    invoke-static {p0, v0, v3}, LX/HGe;->a(LX/HGe;Lcom/facebook/graphql/model/GraphQLAlbum;LX/HGb;)V

    .line 2446431
    move-object p1, v2

    .line 2446432
    :goto_1
    return-object p1

    .line 2446433
    :cond_0
    invoke-direct {p0, p1, p2}, LX/HGe;->c(Landroid/view/View;Landroid/view/View;)V

    goto :goto_1

    .line 2446434
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2446435
    iget-object v0, p0, LX/HGe;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v4, LX/HGe;->t:Ljava/lang/String;

    invoke-virtual {v1}, LX/HGc;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
