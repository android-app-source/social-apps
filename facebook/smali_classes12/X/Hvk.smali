.class public LX/Hvk;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Hvj;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2519767
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2519768
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/Be2;Ljava/lang/Object;)LX/Hvj;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsKeyboardUp;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j2;",
            ":",
            "LX/0j8;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j9;",
            ":",
            "LX/0j6;",
            ":",
            "LX/0ip;",
            "DerivedData::",
            "LX/5RE;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(",
            "Landroid/view/View;",
            "LX/Be2;",
            "TServices;)",
            "LX/Hvj",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2519769
    new-instance v0, LX/Hvj;

    move-object/from16 v3, p3

    check-cast v3, LX/0il;

    const-class v1, LX/Hvb;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Hvb;

    invoke-static {p0}, LX/87Y;->b(LX/0QB;)LX/87Y;

    move-result-object v5

    check-cast v5, LX/87Y;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/IF5;->b(LX/0QB;)LX/IF5;

    move-result-object v7

    check-cast v7, LX/IF5;

    invoke-static {p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v8

    check-cast v8, LX/0tO;

    invoke-static {p0}, LX/CEp;->b(LX/0QB;)LX/CEp;

    move-result-object v9

    check-cast v9, LX/CEp;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v11

    check-cast v11, LX/0hB;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {p0}, LX/IF6;->a(LX/0QB;)LX/IF6;

    move-result-object v13

    check-cast v13, LX/IF6;

    move-object v1, p1

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v13}, LX/Hvj;-><init>(Landroid/view/View;LX/Be2;LX/0il;LX/Hvb;LX/87Y;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/IF5;LX/0tO;LX/CEp;Ljava/util/concurrent/ExecutorService;LX/0hB;LX/0Uh;LX/IF6;)V

    .line 2519770
    return-object v0
.end method
