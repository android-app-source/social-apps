.class public LX/Hmc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Hmc;


# instance fields
.field private final a:LX/13Q;


# direct methods
.method public constructor <init>(LX/13Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2500889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2500890
    iput-object p1, p0, LX/Hmc;->a:LX/13Q;

    .line 2500891
    return-void
.end method

.method public static a(LX/0QB;)LX/Hmc;
    .locals 4

    .prologue
    .line 2500892
    sget-object v0, LX/Hmc;->b:LX/Hmc;

    if-nez v0, :cond_1

    .line 2500893
    const-class v1, LX/Hmc;

    monitor-enter v1

    .line 2500894
    :try_start_0
    sget-object v0, LX/Hmc;->b:LX/Hmc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2500895
    if-eqz v2, :cond_0

    .line 2500896
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2500897
    new-instance p0, LX/Hmc;

    invoke-static {v0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v3

    check-cast v3, LX/13Q;

    invoke-direct {p0, v3}, LX/Hmc;-><init>(LX/13Q;)V

    .line 2500898
    move-object v0, p0

    .line 2500899
    sput-object v0, LX/Hmc;->b:LX/Hmc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2500900
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2500901
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2500902
    :cond_1
    sget-object v0, LX/Hmc;->b:LX/Hmc;

    return-object v0

    .line 2500903
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2500904
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Hmc;LX/Hmb;)V
    .locals 3

    .prologue
    .line 2500905
    iget-object v0, p0, LX/Hmc;->a:LX/13Q;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "beam_receiver_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    .line 2500906
    return-void
.end method
