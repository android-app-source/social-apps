.class public final LX/JDy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JDz;


# direct methods
.method public constructor <init>(LX/JDz;)V
    .locals 0

    .prologue
    .line 2664895
    iput-object p1, p0, LX/JDy;->a:LX/JDz;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2664896
    iget-object v0, p0, LX/JDy;->a:LX/JDz;

    iget-object v0, v0, LX/JDz;->d:LX/JDx;

    invoke-interface {v0}, LX/JDx;->a()V

    .line 2664897
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2664898
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2664899
    if-eqz p1, :cond_0

    .line 2664900
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2664901
    if-nez v0, :cond_1

    .line 2664902
    :cond_0
    :goto_0
    return-void

    .line 2664903
    :cond_1
    iget-object v0, p0, LX/JDy;->a:LX/JDz;

    iget-object v1, v0, LX/JDz;->d:LX/JDx;

    .line 2664904
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2664905
    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;

    invoke-interface {v1, v0}, LX/JDx;->a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;)V

    goto :goto_0
.end method
