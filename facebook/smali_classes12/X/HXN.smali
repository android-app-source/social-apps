.class public LX/HXN;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:Ljava/lang/String;

.field public final e:LX/01T;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Ljava/lang/String;JLjava/lang/String;LX/01T;Ljava/lang/String;ZZZ)V
    .locals 1

    .prologue
    .line 2479025
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2479026
    iput-object p1, p0, LX/HXN;->a:Landroid/view/LayoutInflater;

    .line 2479027
    iput-object p2, p0, LX/HXN;->b:Ljava/lang/String;

    .line 2479028
    iput-wide p3, p0, LX/HXN;->c:J

    .line 2479029
    iput-object p5, p0, LX/HXN;->d:Ljava/lang/String;

    .line 2479030
    iput-object p6, p0, LX/HXN;->e:LX/01T;

    .line 2479031
    iput-object p7, p0, LX/HXN;->f:Ljava/lang/String;

    .line 2479032
    iput-boolean p8, p0, LX/HXN;->g:Z

    .line 2479033
    iput-boolean p9, p0, LX/HXN;->h:Z

    .line 2479034
    iput-boolean p10, p0, LX/HXN;->i:Z

    .line 2479035
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2479022
    sget-object v1, LX/HXM;->PUBLISHER_BAR:LX/HXM;

    invoke-virtual {v1}, LX/HXM;->ordinal()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 2479023
    iget-object v1, p0, LX/HXN;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f030e76

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2479024
    :cond_0
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 8

    .prologue
    .line 2479018
    sget-object v0, LX/HXM;->PUBLISHER_BAR:LX/HXM;

    invoke-virtual {v0}, LX/HXM;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_0

    .line 2479019
    const v0, 0x7f0d2365

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;

    .line 2479020
    iget-wide v2, p0, LX/HXN;->c:J

    iget-object v4, p0, LX/HXN;->d:Ljava/lang/String;

    iget-object v5, p0, LX/HXN;->f:Ljava/lang/String;

    iget-boolean v6, p0, LX/HXN;->h:Z

    iget-object v7, p0, LX/HXN;->b:Ljava/lang/String;

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2479021
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2479016
    iget-object v0, p0, LX/HXN;->e:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, LX/HXN;->i:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/HXN;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2479017
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2479013
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2479014
    const/4 v0, 0x0

    return-object v0

    .line 2479015
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2479010
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2479011
    int-to-long v0, p1

    return-wide v0

    .line 2479012
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2479006
    if-nez p1, :cond_0

    .line 2479007
    sget-object v0, LX/HXM;->PUBLISHER_BAR:LX/HXM;

    invoke-virtual {v0}, LX/HXM;->ordinal()I

    move-result v0

    .line 2479008
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HXM;->UNKNOWN:LX/HXM;

    invoke-virtual {v0}, LX/HXM;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2479009
    invoke-static {}, LX/HXM;->values()[LX/HXM;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
