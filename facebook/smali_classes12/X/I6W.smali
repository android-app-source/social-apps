.class public LX/I6W;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0qV;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0SG;LX/0qV;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2537674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2537675
    iput-object p1, p0, LX/I6W;->a:LX/0SG;

    .line 2537676
    iput-object p2, p0, LX/I6W;->b:LX/0qV;

    .line 2537677
    iput-object p3, p0, LX/I6W;->c:Landroid/content/res/Resources;

    .line 2537678
    return-void
.end method

.method private static a(JLcom/facebook/graphql/model/GraphQLStory;)LX/23u;
    .locals 3

    .prologue
    .line 2537671
    invoke-static {p2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 2537672
    iput-wide p0, v0, LX/23u;->G:J

    .line 2537673
    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2537666
    invoke-static {p1, v2}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 2537667
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2537668
    new-instance v0, Lcom/facebook/api/feed/FeedFetchContext;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feed/FeedFetchContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2537669
    iget-object v1, p0, LX/I6W;->b:LX/0qV;

    invoke-virtual {v1, p1, v0}, LX/0qV;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/api/feed/FeedFetchContext;)V

    .line 2537670
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/I6W;
    .locals 4

    .prologue
    .line 2537620
    new-instance v3, LX/I6W;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0qV;->a(LX/0QB;)LX/0qV;

    move-result-object v1

    check-cast v1, LX/0qV;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1, v2}, LX/I6W;-><init>(LX/0SG;LX/0qV;Landroid/content/res/Resources;)V

    .line 2537621
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2537641
    invoke-virtual {p1}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2537642
    :cond_0
    const/4 v0, 0x0

    .line 2537643
    :goto_0
    return-object v0

    .line 2537644
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;

    .line 2537645
    iget-object v1, p0, LX/I6W;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v2, v3, v1}, LX/I6W;->a(JLcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    iget-object v3, p0, LX/I6W;->c:Landroid/content/res/Resources;

    const v4, 0x7f083804

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2537646
    iput-object v3, v2, LX/173;->f:Ljava/lang/String;

    .line 2537647
    move-object v2, v2

    .line 2537648
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 2537649
    iput-object v2, v1, LX/23u;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2537650
    move-object v1, v1

    .line 2537651
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2537652
    invoke-direct {p0, v1}, LX/I6W;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2537653
    new-instance v2, LX/1u8;

    invoke-direct {v2}, LX/1u8;-><init>()V

    .line 2537654
    iput-object v1, v2, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 2537655
    move-object v2, v2

    .line 2537656
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 2537657
    iput-object v1, v2, LX/1u8;->d:Ljava/lang/String;

    .line 2537658
    move-object v1, v2

    .line 2537659
    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2537660
    iput-object v2, v1, LX/1u8;->i:Ljava/lang/String;

    .line 2537661
    move-object v1, v1

    .line 2537662
    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2537663
    iput-object v0, v1, LX/1u8;->c:Ljava/lang/String;

    .line 2537664
    move-object v0, v1

    .line 2537665
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2537622
    invoke-virtual {p1}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;->j()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;->j()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2537623
    :cond_0
    const/4 v0, 0x0

    .line 2537624
    :goto_0
    return-object v0

    .line 2537625
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;->j()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel$EdgesModel;

    .line 2537626
    iget-object v1, p0, LX/I6W;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v2, v3, v1}, LX/I6W;->a(JLcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2537627
    invoke-direct {p0, v1}, LX/I6W;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2537628
    new-instance v2, LX/1u8;

    invoke-direct {v2}, LX/1u8;-><init>()V

    .line 2537629
    iput-object v1, v2, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 2537630
    move-object v2, v2

    .line 2537631
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 2537632
    iput-object v1, v2, LX/1u8;->d:Ljava/lang/String;

    .line 2537633
    move-object v1, v2

    .line 2537634
    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel$EdgesModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2537635
    iput-object v2, v1, LX/1u8;->i:Ljava/lang/String;

    .line 2537636
    move-object v1, v1

    .line 2537637
    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventStoriesModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2537638
    iput-object v0, v1, LX/1u8;->c:Ljava/lang/String;

    .line 2537639
    move-object v0, v1

    .line 2537640
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    goto :goto_0
.end method
