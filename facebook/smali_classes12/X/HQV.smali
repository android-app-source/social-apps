.class public final LX/HQV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/graphics/PointF;

.field public final c:[Ljava/lang/String;

.field public final d:LX/HQU;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:Z

.field public final h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/PointF;[Ljava/lang/String;LX/HQU;Ljava/lang/String;ZZLandroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2463655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2463656
    iput-object p1, p0, LX/HQV;->a:Ljava/lang/String;

    .line 2463657
    iput-object p2, p0, LX/HQV;->b:Landroid/graphics/PointF;

    .line 2463658
    iput-object p3, p0, LX/HQV;->c:[Ljava/lang/String;

    .line 2463659
    iput-object p4, p0, LX/HQV;->d:LX/HQU;

    .line 2463660
    iput-object p5, p0, LX/HQV;->e:Ljava/lang/String;

    .line 2463661
    iput-boolean p6, p0, LX/HQV;->f:Z

    .line 2463662
    iput-boolean p7, p0, LX/HQV;->g:Z

    .line 2463663
    iput-object p8, p0, LX/HQV;->h:Landroid/view/View$OnClickListener;

    .line 2463664
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2463665
    if-ne p0, p1, :cond_1

    .line 2463666
    :cond_0
    :goto_0
    return v2

    .line 2463667
    :cond_1
    instance-of v0, p1, LX/HQV;

    if-nez v0, :cond_2

    move v2, v1

    .line 2463668
    goto :goto_0

    .line 2463669
    :cond_2
    check-cast p1, LX/HQV;

    .line 2463670
    iget-object v0, p0, LX/HQV;->h:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_3

    iget-object v0, p1, LX/HQV;->h:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_4

    :cond_3
    iget-object v0, p0, LX/HQV;->h:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_6

    iget-object v0, p1, LX/HQV;->h:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_6

    :cond_4
    move v0, v2

    .line 2463671
    :goto_1
    iget-object v3, p0, LX/HQV;->a:Ljava/lang/String;

    iget-object v4, p1, LX/HQV;->a:Ljava/lang/String;

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, LX/HQV;->b:Landroid/graphics/PointF;

    iget-object v4, p1, LX/HQV;->b:Landroid/graphics/PointF;

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, LX/HQV;->c:[Ljava/lang/String;

    iget-object v4, p1, LX/HQV;->c:[Ljava/lang/String;

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, LX/HQV;->d:LX/HQU;

    iget-object v4, p1, LX/HQV;->d:LX/HQU;

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, LX/HQV;->e:Ljava/lang/String;

    iget-object v4, p1, LX/HQV;->e:Ljava/lang/String;

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-boolean v3, p0, LX/HQV;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-boolean v4, p1, LX/HQV;->f:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v0, :cond_0

    :cond_5
    move v2, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2463672
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/HQV;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/HQV;->b:Landroid/graphics/PointF;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/HQV;->c:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/HQV;->d:LX/HQU;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/HQV;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, LX/HQV;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
