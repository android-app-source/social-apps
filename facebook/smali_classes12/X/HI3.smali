.class public final LX/HI3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 2449827
    const/4 v12, 0x0

    .line 2449828
    const/4 v11, 0x0

    .line 2449829
    const/4 v10, 0x0

    .line 2449830
    const/4 v9, 0x0

    .line 2449831
    const/4 v8, 0x0

    .line 2449832
    const/4 v7, 0x0

    .line 2449833
    const/4 v6, 0x0

    .line 2449834
    const/4 v5, 0x0

    .line 2449835
    const/4 v4, 0x0

    .line 2449836
    const/4 v3, 0x0

    .line 2449837
    const/4 v2, 0x0

    .line 2449838
    const/4 v1, 0x0

    .line 2449839
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 2449840
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2449841
    const/4 v1, 0x0

    .line 2449842
    :goto_0
    return v1

    .line 2449843
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2449844
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 2449845
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 2449846
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2449847
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 2449848
    const-string v14, "all_draft_posts"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 2449849
    invoke-static/range {p0 .. p1}, LX/HHx;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 2449850
    :cond_2
    const-string v14, "all_scheduled_posts"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 2449851
    invoke-static/range {p0 .. p1}, LX/HHy;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 2449852
    :cond_3
    const-string v14, "boosted_local_awareness_promotion_status_description"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 2449853
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2449854
    :cond_4
    const-string v14, "boosted_local_awareness_promotions"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 2449855
    invoke-static/range {p0 .. p1}, LX/HHz;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2449856
    :cond_5
    const-string v14, "boosted_page_like_promotion_info"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 2449857
    invoke-static/range {p0 .. p1}, LX/HI0;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2449858
    :cond_6
    const-string v14, "boosted_page_like_promotion_status_description"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 2449859
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2449860
    :cond_7
    const-string v14, "can_viewer_promote_for_page_likes"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 2449861
    const/4 v2, 0x1

    .line 2449862
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 2449863
    :cond_8
    const-string v14, "can_viewer_promote_local_awareness"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 2449864
    const/4 v1, 0x1

    .line 2449865
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v5

    goto/16 :goto_1

    .line 2449866
    :cond_9
    const-string v14, "page_contact_us_leads"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 2449867
    invoke-static/range {p0 .. p1}, LX/HI1;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2449868
    :cond_a
    const-string v14, "page_insights_summary"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 2449869
    invoke-static/range {p0 .. p1}, LX/HI2;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2449870
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 2449871
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 2449872
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 2449873
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 2449874
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 2449875
    const/4 v9, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 2449876
    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2449877
    if-eqz v2, :cond_c

    .line 2449878
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->a(IZ)V

    .line 2449879
    :cond_c
    if-eqz v1, :cond_d

    .line 2449880
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->a(IZ)V

    .line 2449881
    :cond_d
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2449882
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2449883
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 2449884
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2449885
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2449886
    if-eqz v0, :cond_1

    .line 2449887
    const-string v1, "all_draft_posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449888
    const/4 v1, 0x0

    .line 2449889
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2449890
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2449891
    if-eqz v1, :cond_0

    .line 2449892
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449893
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2449894
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2449895
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2449896
    if-eqz v0, :cond_3

    .line 2449897
    const-string v1, "all_scheduled_posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449898
    const/4 v1, 0x0

    .line 2449899
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2449900
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2449901
    if-eqz v1, :cond_2

    .line 2449902
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449903
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2449904
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2449905
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2449906
    if-eqz v0, :cond_4

    .line 2449907
    const-string v1, "boosted_local_awareness_promotion_status_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449908
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449909
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2449910
    if-eqz v0, :cond_8

    .line 2449911
    const-string v1, "boosted_local_awareness_promotions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449912
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2449913
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2449914
    if-eqz v1, :cond_7

    .line 2449915
    const-string v2, "nodes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449916
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2449917
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 2449918
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    const/4 p3, 0x0

    .line 2449919
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2449920
    invoke-virtual {p0, v3, p3}, LX/15i;->g(II)I

    move-result v0

    .line 2449921
    if-eqz v0, :cond_5

    .line 2449922
    const-string v0, "boosting_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449923
    invoke-virtual {p0, v3, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449924
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2449925
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2449926
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2449927
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2449928
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2449929
    if-eqz v0, :cond_12

    .line 2449930
    const-string v1, "boosted_page_like_promotion_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449931
    const/4 v5, 0x5

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 2449932
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2449933
    invoke-virtual {p0, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2449934
    if-eqz v2, :cond_9

    .line 2449935
    const-string v2, "boosting_status"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449936
    invoke-virtual {p0, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449937
    :cond_9
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2449938
    if-eqz v2, :cond_a

    .line 2449939
    const-string v3, "budget"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449940
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449941
    :cond_a
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2449942
    if-eqz v2, :cond_b

    .line 2449943
    const-string v3, "has_editable_promotion"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449944
    invoke-virtual {p2, v2}, LX/0nX;->a(Z)V

    .line 2449945
    :cond_b
    const/4 v2, 0x3

    invoke-virtual {p0, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 2449946
    if-eqz v2, :cond_c

    .line 2449947
    const-string v3, "kpi"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449948
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 2449949
    :cond_c
    const/4 v2, 0x4

    invoke-virtual {p0, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 2449950
    if-eqz v2, :cond_d

    .line 2449951
    const-string v3, "reach"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449952
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 2449953
    :cond_d
    invoke-virtual {p0, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2449954
    if-eqz v2, :cond_e

    .line 2449955
    const-string v2, "reset_period"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449956
    invoke-virtual {p0, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449957
    :cond_e
    const/4 v2, 0x6

    invoke-virtual {p0, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2449958
    if-eqz v2, :cond_f

    .line 2449959
    const-string v3, "spent"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449960
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449961
    :cond_f
    const/4 v2, 0x7

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2449962
    cmp-long v4, v2, v6

    if-eqz v4, :cond_10

    .line 2449963
    const-string v4, "start_time"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449964
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(J)V

    .line 2449965
    :cond_10
    const/16 v2, 0x8

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2449966
    cmp-long v4, v2, v6

    if-eqz v4, :cond_11

    .line 2449967
    const-string v4, "stop_time"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449968
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(J)V

    .line 2449969
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2449970
    :cond_12
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2449971
    if-eqz v0, :cond_13

    .line 2449972
    const-string v1, "boosted_page_like_promotion_status_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449973
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449974
    :cond_13
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2449975
    if-eqz v0, :cond_14

    .line 2449976
    const-string v1, "can_viewer_promote_for_page_likes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449977
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2449978
    :cond_14
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2449979
    if-eqz v0, :cond_15

    .line 2449980
    const-string v1, "can_viewer_promote_local_awareness"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449981
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2449982
    :cond_15
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2449983
    if-eqz v0, :cond_17

    .line 2449984
    const-string v1, "page_contact_us_leads"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449985
    const/4 v1, 0x0

    .line 2449986
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2449987
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2449988
    if-eqz v1, :cond_16

    .line 2449989
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449990
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2449991
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2449992
    :cond_17
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2449993
    if-eqz v0, :cond_1a

    .line 2449994
    const-string v1, "page_insights_summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449995
    const/4 v3, 0x0

    .line 2449996
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2449997
    invoke-virtual {p0, v0, v3, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2449998
    if-eqz v1, :cond_18

    .line 2449999
    const-string v2, "weekly_new_likes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450000
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2450001
    :cond_18
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2450002
    if-eqz v1, :cond_19

    .line 2450003
    const-string v2, "weekly_post_reach"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450004
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2450005
    :cond_19
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2450006
    :cond_1a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2450007
    return-void
.end method
