.class public LX/Irs;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2619057
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2619058
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/photos/editing/LayerGroupLayout;LX/Ird;)Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;
    .locals 8

    .prologue
    .line 2619059
    new-instance v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {p0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v3

    check-cast v3, LX/1zC;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    const/16 v1, 0x277e

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v7

    check-cast v7, LX/0wW;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;-><init>(Lcom/facebook/messaging/photos/editing/LayerGroupLayout;LX/Ird;LX/1zC;LX/1Ad;LX/0Ot;LX/0TD;LX/0wW;)V

    .line 2619060
    const-class v1, LX/Iqm;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Iqm;

    const-class v2, LX/Is2;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Is2;

    .line 2619061
    iput-object v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->l:LX/Iqm;

    iput-object v2, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->m:LX/Is2;

    .line 2619062
    return-object v0
.end method
