.class public final LX/HTI;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V
    .locals 0

    .prologue
    .line 2469300
    iput-object p1, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2469270
    iget-object v0, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a$redex0(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;Z)V

    .line 2469271
    iget-object v0, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchPagesOffersList"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2469272
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2469273
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2469274
    iget-object v0, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    .line 2469275
    iput-boolean v2, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->k:Z

    .line 2469276
    iget-object v0, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-static {v0, v3}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a$redex0(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;Z)V

    .line 2469277
    if-nez p1, :cond_1

    .line 2469278
    :cond_0
    :goto_0
    return-void

    .line 2469279
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2469280
    check-cast v0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;

    .line 2469281
    iget-object v1, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->m:Ljava/lang/String;

    if-nez v1, :cond_6

    move v1, v2

    .line 2469282
    :goto_1
    if-eqz v1, :cond_2

    .line 2469283
    iget-object v1, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->h:Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;

    .line 2469284
    iget-object v4, v1, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2469285
    :cond_2
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;->j()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2469286
    iget-object v1, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->h:Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;->j()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;

    move-result-object v4

    .line 2469287
    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 2469288
    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v5, 0x0

    move v6, v5

    :goto_2
    if-ge v6, v8, :cond_4

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    .line 2469289
    invoke-virtual {v5}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->l()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {v5}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {v5}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    .line 2469290
    iget-object p1, v1, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->b:Ljava/util/List;

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2469291
    :cond_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    .line 2469292
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;->j()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_5

    .line 2469293
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;->j()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v5, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-virtual {v4, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2469294
    iput-object v1, v5, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->m:Ljava/lang/String;

    .line 2469295
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;->j()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    .line 2469296
    iput-boolean v0, v3, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->n:Z

    .line 2469297
    :cond_5
    iget-object v0, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->i:LX/HME;

    if-eqz v0, :cond_0

    .line 2469298
    iget-object v0, p0, LX/HTI;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->i:LX/HME;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 2469299
    goto/16 :goto_1
.end method
