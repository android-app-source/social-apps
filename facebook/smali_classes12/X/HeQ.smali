.class public abstract LX/HeQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0l1;

.field public final b:LX/0Zb;

.field public final c:LX/0hx;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/HeR;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/content/Context;

.field private f:Landroid/view/LayoutInflater;

.field private g:Landroid/widget/PopupWindow;

.field public h:Z

.field public i:Z

.field private j:I

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2490254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2490255
    iput-object v1, p0, LX/HeQ;->e:Landroid/content/Context;

    .line 2490256
    iput-object v1, p0, LX/HeQ;->f:Landroid/view/LayoutInflater;

    .line 2490257
    iput-object v1, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;

    .line 2490258
    iput-boolean v0, p0, LX/HeQ;->h:Z

    .line 2490259
    iput-boolean v0, p0, LX/HeQ;->i:Z

    .line 2490260
    iput v0, p0, LX/HeQ;->j:I

    .line 2490261
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HeQ;->d:Ljava/util/ArrayList;

    .line 2490262
    iput-object p1, p0, LX/HeQ;->e:Landroid/content/Context;

    .line 2490263
    iput-object p2, p0, LX/HeQ;->f:Landroid/view/LayoutInflater;

    .line 2490264
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 2490265
    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iput-object v0, p0, LX/HeQ;->b:LX/0Zb;

    .line 2490266
    invoke-static {v1}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v0

    check-cast v0, LX/0hx;

    iput-object v0, p0, LX/HeQ;->c:LX/0hx;

    .line 2490267
    invoke-static {v1}, LX/0l1;->a(LX/0QB;)LX/0l1;

    move-result-object v0

    check-cast v0, LX/0l1;

    iput-object v0, p0, LX/HeQ;->a:LX/0l1;

    .line 2490268
    return-void
.end method

.method private e()V
    .locals 8

    .prologue
    .line 2490269
    iget-object v0, p0, LX/HeQ;->a:LX/0l1;

    const/16 p0, 0x3eb

    const/16 v7, 0x3ea

    const/16 v6, 0x3e9

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2490270
    sget-object v1, LX/03R;->YES:LX/03R;

    iget-object v4, v0, LX/0l1;->o:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    if-ne v1, v4, :cond_a

    move v1, v2

    .line 2490271
    :goto_0
    iget-object v4, v0, LX/0l1;->i:LX/0l2;

    invoke-virtual {v4}, LX/0l2;->a()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2490272
    iget-object v3, v0, LX/0l1;->b:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 2490273
    :goto_1
    iget-object v3, v0, LX/0l1;->j:LX/0jh;

    iget-object v4, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/facebook/common/activitylistener/annotations/HideSettingsFromOptionsMenu;

    invoke-virtual {v3, v4, v5}, LX/0jh;->a(Ljava/lang/reflect/AnnotatedElement;Ljava/lang/Class;)Z

    move-result v3

    .line 2490274
    iget-object v4, v0, LX/0l1;->b:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, v0, LX/0l1;->e:LX/0WJ;

    invoke-virtual {v4}, LX/0WJ;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, LX/0l1;->d:Landroid/app/Activity;

    instance-of v4, v4, Lcom/facebook/katana/settings/activity/SettingsActivity;

    if-nez v4, :cond_0

    if-nez v3, :cond_0

    .line 2490275
    const v4, 0x7f083ad0

    const v5, 0x7f021443

    invoke-virtual {v0, v6, v4, v5}, LX/0l1;->a(III)V

    .line 2490276
    :cond_0
    iget-object v4, v0, LX/0l1;->b:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v4, v0, LX/0l1;->e:LX/0WJ;

    invoke-virtual {v4}, LX/0WJ;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v3, :cond_1

    if-nez v2, :cond_1

    .line 2490277
    const v3, 0x7f083206

    const v4, 0x7f021441

    invoke-virtual {v0, p0, v3, v4}, LX/0l1;->a(III)V

    .line 2490278
    :cond_1
    iget-object v3, v0, LX/0l1;->b:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    .line 2490279
    const v3, 0x7f0800cb

    const v4, 0x7f021440

    invoke-virtual {v0, v7, v3, v4}, LX/0l1;->a(III)V

    .line 2490280
    :cond_2
    iget-object v3, v0, LX/0l1;->b:Ljava/util/Map;

    const/16 v4, 0x3f1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    .line 2490281
    const/16 v3, 0x3f1

    const v4, 0x7f083676

    const v5, 0x1080039

    invoke-virtual {v0, v3, v4, v5}, LX/0l1;->a(III)V

    .line 2490282
    :cond_3
    iget-object v3, v0, LX/0l1;->b:Ljava/util/Map;

    const/16 v4, 0x3ec

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_4

    if-eqz v1, :cond_4

    if-nez v2, :cond_4

    .line 2490283
    const/16 v3, 0x3ec

    const v4, 0x7f083b01

    const v5, 0x1080038

    invoke-virtual {v0, v3, v4, v5}, LX/0l1;->a(III)V

    .line 2490284
    :cond_4
    iget-object v3, v0, LX/0l1;->b:Ljava/util/Map;

    const/16 v4, 0x3ed

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_5

    iget-object v3, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0l1;->j(LX/0l1;Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v0, LX/0l1;->e:LX/0WJ;

    invoke-virtual {v3}, LX/0WJ;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v0, LX/0l1;->d:Landroid/app/Activity;

    instance-of v3, v3, LX/6G5;

    if-nez v3, :cond_5

    if-nez v2, :cond_5

    .line 2490285
    const/16 v3, 0x3ed

    const v4, 0x7f0818ee

    const v5, 0x7f02143f

    invoke-virtual {v0, v3, v4, v5}, LX/0l1;->a(III)V

    .line 2490286
    :cond_5
    iget-object v3, v0, LX/0l1;->b:Ljava/util/Map;

    const/16 v4, 0x3ef

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_6

    if-eqz v1, :cond_6

    if-nez v2, :cond_6

    .line 2490287
    const/16 v1, 0x3ef

    const v2, 0x7f0832ce

    const v3, 0x1080042

    invoke-virtual {v0, v1, v2, v3}, LX/0l1;->a(III)V

    .line 2490288
    :cond_6
    iget-object v1, v0, LX/0l1;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2490289
    iget-object v1, v0, LX/0l1;->d:Landroid/app/Activity;

    instance-of v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    if-eqz v1, :cond_7

    .line 2490290
    iget-object v1, v0, LX/0l1;->d:Landroid/app/Activity;

    check-cast v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->lW_()V

    .line 2490291
    :cond_7
    iget-object v1, v0, LX/0l1;->d:Landroid/app/Activity;

    instance-of v1, v1, LX/CIz;

    if-eqz v1, :cond_8

    .line 2490292
    iget-object v1, v0, LX/0l1;->d:Landroid/app/Activity;

    check-cast v1, LX/CIz;

    .line 2490293
    invoke-interface {v1}, LX/CIz;->a()Ljava/util/List;

    move-result-object v2

    .line 2490294
    if-nez v2, :cond_c

    .line 2490295
    :cond_8
    :goto_2
    iget-object v1, v0, LX/0l1;->a:LX/HeQ;

    if-eqz v1, :cond_9

    .line 2490296
    iget-object v1, v0, LX/0l1;->a:LX/HeQ;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, LX/0l1;->b:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2}, LX/HeQ;->a(Ljava/util/ArrayList;)V

    .line 2490297
    :cond_9
    return-void

    :cond_a
    move v1, v3

    .line 2490298
    goto/16 :goto_0

    :cond_b
    move v2, v3

    goto/16 :goto_1

    .line 2490299
    :cond_c
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2490300
    iget-object v3, v0, LX/0l1;->b:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_d
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2490301
    const/16 v6, 0x3e9

    if-lt v3, v6, :cond_e

    const/16 v6, 0x3f1

    if-le v3, v6, :cond_d

    .line 2490302
    :cond_e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2490303
    :cond_f
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2490304
    iget-object v5, v0, LX/0l1;->b:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2490305
    goto :goto_4

    .line 2490306
    :cond_10
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_11
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CIy;

    .line 2490307
    iget v3, v2, LX/CIy;->c:I

    if-lez v3, :cond_12

    iget v3, v2, LX/CIy;->c:I

    .line 2490308
    :goto_6
    iget v5, v2, LX/CIy;->a:I

    iget v6, v2, LX/CIy;->b:I

    invoke-virtual {v0, v5, v6, v3}, LX/0l1;->a(III)V

    .line 2490309
    iget v3, v2, LX/CIy;->a:I

    iget-boolean v5, v2, LX/CIy;->d:Z

    invoke-virtual {v0, v3, v5}, LX/0l1;->a(IZ)V

    .line 2490310
    iget-object v3, v2, LX/CIy;->e:Ljava/lang/String;

    if-eqz v3, :cond_11

    .line 2490311
    iget-object v3, v0, LX/0l1;->c:Ljava/util/Map;

    iget v5, v2, LX/CIy;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v2, v2, LX/CIy;->e:Ljava/lang/String;

    invoke-interface {v3, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 2490312
    :cond_12
    const v3, 0x7f021443

    goto :goto_6

    .line 2490313
    :cond_13
    goto/16 :goto_2
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(LX/HeR;)V
.end method

.method public final declared-synchronized a(Landroid/view/View;ZZ)V
    .locals 13

    .prologue
    .line 2490314
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/HeQ;->e()V

    .line 2490315
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/HeQ;->h:Z

    .line 2490316
    iput-boolean p2, p0, LX/HeQ;->i:Z

    .line 2490317
    iget-object v1, p0, LX/HeQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 2490318
    if-lez v9, :cond_0

    iget-object v1, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 2490319
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2490320
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/HeQ;->e:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 2490321
    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_4

    const/4 v1, 0x1

    move v2, v1

    .line 2490322
    :goto_1
    iget-object v1, p0, LX/HeQ;->l:Landroid/view/View;

    if-nez v1, :cond_2

    .line 2490323
    new-instance v1, Landroid/view/View;

    iget-object v4, p0, LX/HeQ;->e:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/HeQ;->l:Landroid/view/View;

    .line 2490324
    iget-object v1, p0, LX/HeQ;->l:Landroid/view/View;

    iget-object v4, p0, LX/HeQ;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x106000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2490325
    iget-object v1, p0, LX/HeQ;->l:Landroid/view/View;

    new-instance v4, LX/2z8;

    invoke-direct {v4, p0}, LX/2z8;-><init>(LX/HeQ;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2490326
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    iget-object v4, p0, LX/HeQ;->l:Landroid/view/View;

    new-instance v5, Landroid/view/WindowManager$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2490327
    :cond_2
    iget-object v1, p0, LX/HeQ;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 2490328
    iget-object v1, p0, LX/HeQ;->f:Landroid/view/LayoutInflater;

    const v4, 0x7f030ac5

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/HeQ;->k:Landroid/view/View;

    .line 2490329
    new-instance v1, Landroid/widget/PopupWindow;

    iget-object v4, p0, LX/HeQ;->k:Landroid/view/View;

    const/4 v5, -0x1

    const/4 v6, -0x2

    const/4 v7, 0x0

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v1, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;

    .line 2490330
    if-eqz p3, :cond_3

    .line 2490331
    iget-object v1, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;

    const v4, 0x1030056

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 2490332
    :cond_3
    iget-object v1, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 2490333
    iget-object v1, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;

    const/16 v3, 0x50

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, p1, v3, v4, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 2490334
    if-eqz v2, :cond_5

    const/4 v1, 0x6

    move v8, v1

    .line 2490335
    :goto_2
    if-eqz v2, :cond_6

    const/4 v1, 0x1

    .line 2490336
    :goto_3
    div-int v3, v9, v8

    rem-int v2, v9, v8

    if-nez v2, :cond_7

    const/4 v2, 0x0

    :goto_4
    add-int/2addr v2, v3

    iput v2, p0, LX/HeQ;->j:I

    .line 2490337
    if-eqz p2, :cond_c

    .line 2490338
    iget v1, p0, LX/HeQ;->j:I

    move v7, v1

    .line 2490339
    :goto_5
    iget-object v1, p0, LX/HeQ;->k:Landroid/view/View;

    const v2, 0x7f0d1b80

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableLayout;

    .line 2490340
    invoke-virtual {v1}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 2490341
    const/4 v2, 0x0

    move v6, v2

    :goto_6
    if-ge v6, v7, :cond_b

    .line 2490342
    new-instance v10, Landroid/widget/TableRow;

    iget-object v2, p0, LX/HeQ;->e:Landroid/content/Context;

    invoke-direct {v10, v2}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 2490343
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    invoke-virtual {v10, v2}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2490344
    const/4 v2, 0x0

    move v5, v2

    :goto_7
    if-ge v5, v8, :cond_a

    .line 2490345
    mul-int v2, v6, v8

    add-int/2addr v2, v5

    if-ge v2, v9, :cond_a

    .line 2490346
    add-int/lit8 v2, v7, -0x1

    if-ne v6, v2, :cond_8

    add-int/lit8 v2, v8, -0x1

    if-ne v5, v2, :cond_8

    mul-int v2, v7, v8

    if-le v9, v2, :cond_8

    if-nez p2, :cond_8

    .line 2490347
    new-instance v2, LX/HeR;

    invoke-direct {v2}, LX/HeR;-><init>()V

    .line 2490348
    iget-object v3, p0, LX/HeQ;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f083675

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/HeR;->a(Ljava/lang/String;)V

    .line 2490349
    const/16 v3, 0x3ee

    invoke-virtual {v2, v3}, LX/HeR;->b(I)V

    .line 2490350
    const v3, 0x7f020ecc

    invoke-virtual {v2, v3}, LX/HeR;->a(I)V

    move-object v4, v2

    .line 2490351
    :goto_8
    iget-object v2, p0, LX/HeQ;->f:Landroid/view/LayoutInflater;

    const v3, 0x7f030ac6

    const/4 v11, 0x0

    invoke-virtual {v2, v3, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 2490352
    const v2, 0x7f0d1b82

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2490353
    invoke-virtual {v4}, LX/HeR;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490354
    const v3, 0x7f0d1b81

    invoke-virtual {v11, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 2490355
    invoke-virtual {v4}, LX/HeR;->b()I

    move-result v12

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2490356
    invoke-virtual {v4}, LX/HeR;->d()Z

    move-result v12

    if-nez v12, :cond_9

    .line 2490357
    const/16 v4, 0x4b

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 2490358
    iget-object v3, p0, LX/HeQ;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a08dd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2490359
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Landroid/view/View;->setClickable(Z)V

    .line 2490360
    :goto_9
    invoke-virtual {v10, v11}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 2490361
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_7

    .line 2490362
    :cond_4
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_1

    .line 2490363
    :cond_5
    const/4 v1, 0x3

    move v8, v1

    goto/16 :goto_2

    .line 2490364
    :cond_6
    const/4 v1, 0x2

    goto/16 :goto_3

    .line 2490365
    :cond_7
    const/4 v2, 0x1

    goto/16 :goto_4

    .line 2490366
    :cond_8
    iget-object v2, p0, LX/HeQ;->d:Ljava/util/ArrayList;

    mul-int v3, v6, v8

    add-int/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HeR;

    move-object v4, v2

    goto :goto_8

    .line 2490367
    :cond_9
    new-instance v2, LX/2z9;

    invoke-direct {v2, p0, v4}, LX/2z9;-><init>(LX/HeQ;LX/HeR;)V

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_9

    .line 2490368
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 2490369
    :cond_a
    :try_start_2
    invoke-virtual {v1, v10}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 2490370
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_6

    .line 2490371
    :cond_b
    iget-object v1, p0, LX/HeQ;->e:Landroid/content/Context;

    invoke-static {v1}, LX/14o;->a(Landroid/content/Context;)LX/0gh;

    move-result-object v1

    invoke-virtual {p0}, LX/HeQ;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0gh;->a(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_c
    move v7, v1

    goto/16 :goto_5
.end method

.method public final declared-synchronized a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/HeR;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2490372
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/HeQ;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 2490373
    :goto_0
    monitor-exit p0

    return-void

    .line 2490374
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/HeQ;->d:Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2490375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 2490376
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/HeQ;->h:Z

    .line 2490377
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HeQ;->i:Z

    .line 2490378
    iget-object v0, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 2490379
    iget-object v0, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 2490380
    const/4 v0, 0x0

    iput-object v0, p0, LX/HeQ;->g:Landroid/widget/PopupWindow;

    .line 2490381
    iget-object v0, p0, LX/HeQ;->e:Landroid/content/Context;

    invoke-static {v0}, LX/14o;->a(Landroid/content/Context;)LX/0gh;

    move-result-object v0

    invoke-virtual {p0}, LX/HeQ;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gh;->d(Ljava/lang/String;)V

    .line 2490382
    :cond_0
    iget-object v0, p0, LX/HeQ;->l:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2490383
    iget-object v0, p0, LX/HeQ;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/HeQ;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2490384
    const/4 v0, 0x0

    iput-object v0, p0, LX/HeQ;->l:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2490385
    :cond_1
    monitor-exit p0

    return-void

    .line 2490386
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
