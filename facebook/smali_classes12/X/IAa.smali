.class public final LX/IAa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;)V
    .locals 0

    .prologue
    .line 2545842
    iput-object p1, p0, LX/IAa;->a:Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x5691feba

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2545843
    iget-object v1, p0, LX/IAa;->a:Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;

    iget-object v1, v1, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->n:LX/DBC;

    iget-object v2, p0, LX/IAa;->a:Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;

    iget-object v2, v2, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->l:Lcom/facebook/events/model/Event;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->COPY_EVENT_INVITES:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/IAa;->a:Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;

    iget-object v4, v4, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->m:Ljava/lang/String;

    .line 2545844
    if-nez v2, :cond_1

    .line 2545845
    :cond_0
    :goto_0
    const v1, -0x62130a09

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2545846
    :cond_1
    sget-object v6, LX/Blb;->FACEBOOK:LX/Blb;

    invoke-static {v1, v2, v3, v6}, LX/DBC;->b(LX/DBC;Lcom/facebook/events/model/Event;Ljava/lang/String;LX/Blb;)Landroid/content/Intent;

    move-result-object v7

    .line 2545847
    if-eqz v7, :cond_0

    .line 2545848
    const-string v6, "extra_original_event_id"

    invoke-virtual {v7, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2545849
    const-string v6, "extra_cohost_ids"

    .line 2545850
    iget-object v8, v2, Lcom/facebook/events/model/Event;->q:LX/0Px;

    move-object v8, v8

    .line 2545851
    invoke-virtual {v7, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2545852
    iget-object v8, v1, LX/DBC;->f:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x1f5

    iget-object v6, v1, LX/DBC;->c:Landroid/content/Context;

    const-class p1, Landroid/app/Activity;

    invoke-static {v6, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    invoke-interface {v8, v7, p0, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method
