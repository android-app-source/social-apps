.class public final LX/ICf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/ICn;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V
    .locals 0

    .prologue
    .line 2549616
    iput-object p1, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2549617
    iget-object v0, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->t:Z

    if-eqz v0, :cond_0

    .line 2549618
    iget-object v0, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->f:Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;

    iget-object v1, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v1, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->n:Ljava/lang/String;

    iget-object v2, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget v2, v2, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->l:I

    iget-object v3, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v3, v3, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/ICe;

    invoke-direct {v4, p0}, LX/ICe;-><init>(LX/ICf;)V

    .line 2549619
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->b(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;Ljava/lang/String;ILjava/lang/String;)LX/0zO;

    move-result-object v6

    .line 2549620
    invoke-static {v0}, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->c(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;)LX/0QK;

    move-result-object v7

    .line 2549621
    iget-object v5, v0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1My;

    new-instance v8, LX/ICl;

    invoke-direct {v8, v0, v4, v7}, LX/ICl;-><init>(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;LX/0TF;LX/0QK;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 2549622
    iget-object v10, v6, LX/0zO;->m:LX/0gW;

    move-object v10, v10

    .line 2549623
    iget-object p0, v10, LX/0gW;->f:Ljava/lang/String;

    move-object v10, p0

    .line 2549624
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->e:I

    add-int/lit8 p0, v10, 0x1

    iput p0, v0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->e:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v6, v8, v9}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v7, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 2549625
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->f:Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;

    iget-object v1, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v1, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->n:Ljava/lang/String;

    iget-object v2, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget v2, v2, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->l:I

    iget-object v3, p0, LX/ICf;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v3, v3, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2549626
    iget-object v4, v0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->b(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;Ljava/lang/String;ILjava/lang/String;)LX/0zO;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-static {v0}, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->c(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;)LX/0QK;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2549627
    goto :goto_0
.end method
