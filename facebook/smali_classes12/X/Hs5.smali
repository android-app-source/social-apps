.class public final LX/Hs5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

.field private b:LX/0lB;

.field private c:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;LX/0lB;LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lB;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2513979
    iput-object p1, p0, LX/Hs5;->a:Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2513980
    iput-object p2, p0, LX/Hs5;->b:LX/0lB;

    .line 2513981
    iput-object p3, p0, LX/Hs5;->c:LX/0QK;

    .line 2513982
    return-void
.end method


# virtual methods
.method public selectAlbum(Lcom/facebook/java2js/JSValue;)V
    .locals 3
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        mode = .enum LX/5SV;->METHOD:LX/5SV;
    .end annotation

    .prologue
    .line 2513983
    :try_start_0
    iget-object v0, p0, LX/Hs5;->b:LX/0lB;

    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->toJSON()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2513984
    iget-object v1, p0, LX/Hs5;->c:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2513985
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
