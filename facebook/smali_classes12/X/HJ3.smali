.class public final LX/HJ3;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Landroid/view/View;

.field public final synthetic d:LX/HJ4;


# direct methods
.method public constructor <init>(LX/HJ4;ZLandroid/content/Context;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2451993
    iput-object p1, p0, LX/HJ3;->d:LX/HJ4;

    iput-boolean p2, p0, LX/HJ3;->a:Z

    iput-object p3, p0, LX/HJ3;->b:Landroid/content/Context;

    iput-object p4, p0, LX/HJ3;->c:Landroid/view/View;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2452006
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2451994
    iget-boolean v0, p0, LX/HJ3;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HJ3;->b:Landroid/content/Context;

    const v1, 0x7f081788

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2451995
    :goto_0
    iget-object v0, p0, LX/HJ3;->c:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2451996
    iget-object v0, p0, LX/HJ3;->d:LX/HJ4;

    iget-object v0, v0, LX/HJ4;->a:LX/2km;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v1

    iget-object v0, p0, LX/HJ3;->d:LX/HJ4;

    iget-object v0, v0, LX/HJ4;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2451997
    iget-object v2, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2451998
    iget-object v0, p0, LX/HJ3;->d:LX/HJ4;

    iget-object v0, v0, LX/HJ4;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2451999
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2452000
    iget-object v0, p0, LX/HJ3;->d:LX/HJ4;

    iget-object v0, v0, LX/HJ4;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452001
    iget-object v4, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v4

    .line 2452002
    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Cfl;

    iget-object v0, p0, LX/HJ3;->d:LX/HJ4;

    iget-object v0, v0, LX/HJ4;->b:LX/9uc;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v6

    iget-boolean v0, p0, LX/HJ3;->a:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/Cfc;->UNSAVE_PAGE_TAP:LX/Cfc;

    :goto_1
    invoke-direct {v5, v6, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;)V

    invoke-virtual {v1, v2, v3, v4, v5}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2452003
    return-void

    .line 2452004
    :cond_0
    iget-object v0, p0, LX/HJ3;->b:Landroid/content/Context;

    const v1, 0x7f08178a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2452005
    :cond_1
    sget-object v0, LX/Cfc;->SAVE_PAGE_TAP:LX/Cfc;

    goto :goto_1
.end method
