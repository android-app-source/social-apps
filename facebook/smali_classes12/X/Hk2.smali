.class public final enum LX/Hk2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hk2;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/Hk2;

.field public static final enum b:LX/Hk2;

.field public static final enum c:LX/Hk2;

.field public static final enum d:LX/Hk2;

.field public static final enum e:LX/Hk2;

.field public static final enum f:LX/Hk2;

.field public static final enum g:LX/Hk2;

.field public static final enum h:LX/Hk2;

.field public static final enum i:LX/Hk2;

.field public static final enum j:LX/Hk2;

.field public static final enum k:LX/Hk2;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic m:[LX/Hk2;


# instance fields
.field private final l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x7

    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x0

    new-instance v0, LX/Hk2;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->a:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "WEBVIEW_BANNER_LEGACY"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v5}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->b:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "WEBVIEW_BANNER_50"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->c:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "WEBVIEW_BANNER_90"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v7}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->d:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "WEBVIEW_BANNER_250"

    invoke-direct {v0, v1, v5, v8}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->e:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "WEBVIEW_INTERSTITIAL_UNKNOWN"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v6, v2}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->f:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "WEBVIEW_INTERSTITIAL_HORIZONTAL"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v7, v2}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->g:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "WEBVIEW_INTERSTITIAL_VERTICAL"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v8, v2}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->h:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "WEBVIEW_INTERSTITIAL_TABLET"

    const/16 v2, 0x8

    const/16 v3, 0x67

    invoke-direct {v0, v1, v2, v3}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->i:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "NATIVE_UNKNOWN"

    const/16 v2, 0x9

    const/16 v3, 0xc8

    invoke-direct {v0, v1, v2, v3}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->j:LX/Hk2;

    new-instance v0, LX/Hk2;

    const-string v1, "NATIVE_250"

    const/16 v2, 0xa

    const/16 v3, 0xc9

    invoke-direct {v0, v1, v2, v3}, LX/Hk2;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hk2;->k:LX/Hk2;

    const/16 v0, 0xb

    new-array v0, v0, [LX/Hk2;

    sget-object v1, LX/Hk2;->a:LX/Hk2;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    sget-object v2, LX/Hk2;->b:LX/Hk2;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/Hk2;->c:LX/Hk2;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/Hk2;->d:LX/Hk2;

    aput-object v2, v0, v1

    sget-object v1, LX/Hk2;->e:LX/Hk2;

    aput-object v1, v0, v5

    sget-object v1, LX/Hk2;->f:LX/Hk2;

    aput-object v1, v0, v6

    sget-object v1, LX/Hk2;->g:LX/Hk2;

    aput-object v1, v0, v7

    sget-object v1, LX/Hk2;->h:LX/Hk2;

    aput-object v1, v0, v8

    const/16 v1, 0x8

    sget-object v2, LX/Hk2;->i:LX/Hk2;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Hk2;->j:LX/Hk2;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Hk2;->k:LX/Hk2;

    aput-object v2, v0, v1

    sput-object v0, LX/Hk2;->m:[LX/Hk2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/Hk2;->l:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hk2;
    .locals 1

    const-class v0, LX/Hk2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hk2;

    return-object v0
.end method

.method public static values()[LX/Hk2;
    .locals 1

    sget-object v0, LX/Hk2;->m:[LX/Hk2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hk2;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, LX/Hk2;->l:I

    return v0
.end method
