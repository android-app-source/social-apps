.class public final enum LX/I0b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I0b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I0b;

.field public static final enum MULTI_DATE_DURING:LX/I0b;

.field public static final enum MULTI_DATE_END:LX/I0b;

.field public static final enum MULTI_DATE_START:LX/I0b;

.field public static final enum SINGLE:LX/I0b;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2527416
    new-instance v0, LX/I0b;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v2}, LX/I0b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0b;->SINGLE:LX/I0b;

    .line 2527417
    new-instance v0, LX/I0b;

    const-string v1, "MULTI_DATE_START"

    invoke-direct {v0, v1, v3}, LX/I0b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0b;->MULTI_DATE_START:LX/I0b;

    .line 2527418
    new-instance v0, LX/I0b;

    const-string v1, "MULTI_DATE_DURING"

    invoke-direct {v0, v1, v4}, LX/I0b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0b;->MULTI_DATE_DURING:LX/I0b;

    .line 2527419
    new-instance v0, LX/I0b;

    const-string v1, "MULTI_DATE_END"

    invoke-direct {v0, v1, v5}, LX/I0b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0b;->MULTI_DATE_END:LX/I0b;

    .line 2527420
    const/4 v0, 0x4

    new-array v0, v0, [LX/I0b;

    sget-object v1, LX/I0b;->SINGLE:LX/I0b;

    aput-object v1, v0, v2

    sget-object v1, LX/I0b;->MULTI_DATE_START:LX/I0b;

    aput-object v1, v0, v3

    sget-object v1, LX/I0b;->MULTI_DATE_DURING:LX/I0b;

    aput-object v1, v0, v4

    sget-object v1, LX/I0b;->MULTI_DATE_END:LX/I0b;

    aput-object v1, v0, v5

    sput-object v0, LX/I0b;->$VALUES:[LX/I0b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2527421
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I0b;
    .locals 1

    .prologue
    .line 2527422
    const-class v0, LX/I0b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I0b;

    return-object v0
.end method

.method public static values()[LX/I0b;
    .locals 1

    .prologue
    .line 2527423
    sget-object v0, LX/I0b;->$VALUES:[LX/I0b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I0b;

    return-object v0
.end method
