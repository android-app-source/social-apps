.class public final LX/HNi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;)V
    .locals 0

    .prologue
    .line 2458776
    iput-object p1, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x1f0aaa80

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2458777
    iget-object v0, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;->d:LX/0Uh;

    const/16 v2, 0x5ba

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2458778
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2458779
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2458780
    const-string v3, "page_id"

    iget-object v4, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    .line 2458781
    iget-object v5, v4, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v5

    .line 2458782
    const-string v5, "arg_page_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458783
    const-string v3, "on_save_service"

    const-string v4, "show_services_list"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458784
    const-string v3, "init_props"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2458785
    iget-object v0, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v3, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/0ax;->fy:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2458786
    iget-object v0, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;->c:LX/0if;

    sget-object v2, LX/0ig;->bd:LX/0ih;

    const-string v3, "tap_add_services"

    const-string v4, "on_add_service_nux"

    invoke-virtual {v0, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2458787
    :goto_0
    const v0, -0x2ced8885

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2458788
    :cond_0
    iget-object v0, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/8Dq;->I:Ljava/lang/String;

    iget-object v4, p0, LX/HNi;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    .line 2458789
    iget-object v5, v4, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v5

    .line 2458790
    const-string v5, "arg_page_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method
