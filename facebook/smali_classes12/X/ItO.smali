.class public final LX/ItO;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic b:Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 0

    .prologue
    .line 2623124
    iput-object p1, p0, LX/ItO;->b:Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    iput-object p2, p0, LX/ItO;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 14

    .prologue
    .line 2623125
    iget-object v0, p0, LX/ItO;->b:Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    iget-object v1, p0, LX/ItO;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2623126
    iget-object v2, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2623127
    iget-object v4, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623128
    iget-object v2, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->o:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ItQ;

    .line 2623129
    invoke-static {v0, v2}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->b(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;LX/ItQ;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2623130
    :try_start_0
    iget-object v3, v2, LX/ItQ;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623131
    new-instance v10, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v11, "send_to_pending_thread_failure"

    invoke-direct {v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2623132
    const-string v11, "pending_thread_id"

    iget-wide v12, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    invoke-virtual {v10, v11, v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2623133
    if-eqz p1, :cond_0

    .line 2623134
    const-string v11, "error_message"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2623135
    :cond_0
    iget-object v11, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->g:LX/0Zb;

    invoke-interface {v11, v10}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2623136
    invoke-virtual {v2}, LX/ItQ;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/messages/Message;

    .line 2623137
    iget-object v5, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->d:LX/0Sh;

    invoke-virtual {v5}, LX/0Sh;->a()V

    .line 2623138
    iget-object v5, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->q:Ljava/util/Map;

    iget-object v6, v3, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/common/util/concurrent/SettableFuture;

    .line 2623139
    if-eqz v5, :cond_1

    .line 2623140
    invoke-virtual {v5, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch LX/It4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2623141
    :cond_1
    goto :goto_0

    .line 2623142
    :catch_0
    move-exception v2

    .line 2623143
    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->a(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;Lcom/facebook/messaging/model/messages/Message;LX/It4;)V

    .line 2623144
    :goto_1
    return-void

    .line 2623145
    :cond_2
    :try_start_1
    iget-object v3, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->e:LX/It6;

    iget-object v2, v2, LX/ItQ;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623146
    invoke-static {v3, v2}, LX/It6;->c(LX/It6;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/It5;

    move-result-object v4

    .line 2623147
    iget-boolean v5, v4, LX/It5;->d:Z

    move v5, v5

    .line 2623148
    if-eqz v5, :cond_3

    .line 2623149
    iget-object v5, v4, LX/It5;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v5}, LX/It4;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/It4;

    .line 2623150
    :cond_3
    const/4 v6, 0x1

    .line 2623151
    iget-boolean v5, v4, LX/It5;->d:Z

    if-nez v5, :cond_5

    move v5, v6

    :goto_2
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 2623152
    iput-boolean v6, v4, LX/It5;->d:Z

    .line 2623153
    iget-object v4, v3, LX/It6;->b:LX/2Ow;

    .line 2623154
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 2623155
    sget-object v6, LX/0aY;->h:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2623156
    const-string v6, "pending_thread_key"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2623157
    invoke-static {v4, v5}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 2623158
    iget-object v2, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ItB;

    invoke-virtual {v2, v1}, LX/ItB;->c(Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_1
    .catch LX/It4; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2623159
    :cond_4
    iget-object v5, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->p:Ljava/util/List;

    iget-object v3, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v6, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager$4;

    invoke-direct {v6, v0, v4, v2}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager$4;-><init>(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/ItQ;)V

    const-wide/16 v8, 0x7530

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v6, v8, v9, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2623160
    :cond_5
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 9

    .prologue
    .line 2623161
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2623162
    iget-object v1, p0, LX/ItO;->b:Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    iget-object v2, p0, LX/ItO;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2623163
    iget-object p1, v0, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, p1

    .line 2623164
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623165
    iget-object v3, v1, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->d:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V
    :try_end_0
    .catch LX/4BK; {:try_start_0 .. :try_end_0} :catch_1

    .line 2623166
    :try_start_1
    iget-object v3, v1, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->o:Ljava/util/Map;

    iget-object v4, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ItQ;

    .line 2623167
    iget-object v4, v1, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->e:LX/It6;

    iget-object v5, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623168
    invoke-static {v4, v5}, LX/It6;->c(LX/It6;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/It5;

    move-result-object v6

    .line 2623169
    iget-boolean v7, v6, LX/It5;->d:Z

    move v7, v7

    .line 2623170
    if-eqz v7, :cond_0

    .line 2623171
    iget-object v7, v6, LX/It5;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v7}, LX/It4;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/It4;

    .line 2623172
    :cond_0
    iget-object v7, v6, LX/It5;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v7, v7

    .line 2623173
    if-eqz v7, :cond_1

    .line 2623174
    iget-object v8, v6, LX/It5;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v8, v7}, LX/It4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/It4;

    .line 2623175
    :cond_1
    iget-boolean v7, v6, LX/It5;->d:Z

    if-nez v7, :cond_5

    const/4 v7, 0x1

    :goto_0
    invoke-static {v7}, LX/0PB;->checkState(Z)V

    .line 2623176
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623177
    iput-object v0, v6, LX/It5;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623178
    iget-object v6, v4, LX/It6;->b:LX/2Ow;

    .line 2623179
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 2623180
    sget-object v8, LX/0aY;->g:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2623181
    const-string v8, "pending_thread_key"

    invoke-virtual {v7, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2623182
    const-string v8, "server_thread_key"

    invoke-virtual {v7, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2623183
    invoke-static {v6, v7}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 2623184
    invoke-virtual {v3}, LX/ItQ;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/messages/Message;

    .line 2623185
    invoke-virtual {v3}, LX/ItQ;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    if-ne v4, v5, :cond_4

    const/4 v5, 0x1

    .line 2623186
    :goto_2
    iget-object v7, v1, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->d:LX/0Sh;

    invoke-virtual {v7}, LX/0Sh;->a()V

    .line 2623187
    iget-object v7, v1, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->q:Ljava/util/Map;

    iget-object v8, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/common/util/concurrent/SettableFuture;

    .line 2623188
    if-eqz v7, :cond_2

    .line 2623189
    new-instance v8, LX/ItR;

    invoke-direct {v8, v4, v0, v5}, LX/ItR;-><init>(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V

    const p1, 0x3cfb39fb

    invoke-static {v7, v8, p1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_1
    .catch LX/It4; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/4BK; {:try_start_1 .. :try_end_1} :catch_1

    .line 2623190
    :cond_2
    :try_start_2
    goto :goto_1

    .line 2623191
    :catch_0
    move-exception v3

    .line 2623192
    invoke-static {v1, v2, v3}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->a(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;Lcom/facebook/messaging/model/messages/Message;LX/It4;)V
    :try_end_2
    .catch LX/4BK; {:try_start_2 .. :try_end_2} :catch_1

    .line 2623193
    :cond_3
    :goto_3
    return-void

    .line 2623194
    :catch_1
    move-exception v0

    .line 2623195
    invoke-virtual {p0, v0}, LX/ItO;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 2623196
    :cond_4
    :try_start_3
    const/4 v5, 0x0

    goto :goto_2
    :try_end_3
    .catch LX/4BK; {:try_start_3 .. :try_end_3} :catch_1

    .line 2623197
    :cond_5
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2623198
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/ItO;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
