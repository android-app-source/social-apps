.class public LX/HTP;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/fbui/facepile/FacepileView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Lcom/facebook/widget/CustomLinearLayout;

.field public e:Lcom/facebook/fbui/glyph/GlyphView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2469495
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/HTP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2469496
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2469485
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2469486
    const v0, 0x7f030e51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2469487
    const v0, 0x7f0d22e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/HTP;->d:Lcom/facebook/widget/CustomLinearLayout;

    .line 2469488
    const v0, 0x7f0d22ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/HTP;->a:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2469489
    const v0, 0x7f0d22eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HTP;->b:Landroid/widget/TextView;

    .line 2469490
    const v0, 0x7f0d22e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HTP;->c:Landroid/widget/TextView;

    .line 2469491
    const v0, 0x7f0d22ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/HTP;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2469492
    const v0, 0x7f0d22ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HTP;->f:Landroid/widget/TextView;

    .line 2469493
    const v0, 0x7f0d22ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HTP;->g:Landroid/widget/TextView;

    .line 2469494
    return-void
.end method
