.class public final enum LX/IQW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IQW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IQW;

.field public static final enum ADDED_MEMBER:LX/IQW;

.field public static final enum INVITED_MEMBER:LX/IQW;

.field public static final enum NOTIF_SETTINGS:LX/IQW;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2575319
    new-instance v0, LX/IQW;

    const-string v1, "NOTIF_SETTINGS"

    invoke-direct {v0, v1, v2}, LX/IQW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IQW;->NOTIF_SETTINGS:LX/IQW;

    .line 2575320
    new-instance v0, LX/IQW;

    const-string v1, "INVITED_MEMBER"

    invoke-direct {v0, v1, v3}, LX/IQW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IQW;->INVITED_MEMBER:LX/IQW;

    .line 2575321
    new-instance v0, LX/IQW;

    const-string v1, "ADDED_MEMBER"

    invoke-direct {v0, v1, v4}, LX/IQW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IQW;->ADDED_MEMBER:LX/IQW;

    .line 2575322
    const/4 v0, 0x3

    new-array v0, v0, [LX/IQW;

    sget-object v1, LX/IQW;->NOTIF_SETTINGS:LX/IQW;

    aput-object v1, v0, v2

    sget-object v1, LX/IQW;->INVITED_MEMBER:LX/IQW;

    aput-object v1, v0, v3

    sget-object v1, LX/IQW;->ADDED_MEMBER:LX/IQW;

    aput-object v1, v0, v4

    sput-object v0, LX/IQW;->$VALUES:[LX/IQW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2575316
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IQW;
    .locals 1

    .prologue
    .line 2575318
    const-class v0, LX/IQW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IQW;

    return-object v0
.end method

.method public static values()[LX/IQW;
    .locals 1

    .prologue
    .line 2575317
    sget-object v0, LX/IQW;->$VALUES:[LX/IQW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IQW;

    return-object v0
.end method
