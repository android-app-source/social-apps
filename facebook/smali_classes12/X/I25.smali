.class public final LX/I25;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I26;


# direct methods
.method public constructor <init>(LX/I26;)V
    .locals 0

    .prologue
    .line 2529710
    iput-object p1, p0, LX/I25;->a:LX/I26;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2529716
    iget-object v0, p0, LX/I25;->a:LX/I26;

    iget-object v0, v0, LX/I26;->j:LX/HzI;

    invoke-virtual {v0}, LX/HzI;->a()V

    .line 2529717
    iget-object v0, p0, LX/I25;->a:LX/I26;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/I26;->c$redex0(LX/I26;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529718
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2529711
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529712
    iget-object v0, p0, LX/I25;->a:LX/I26;

    iget-object v0, v0, LX/I26;->i:LX/HzH;

    sget-object v1, LX/Hx7;->HOSTING:LX/Hx7;

    invoke-virtual {v0, v1}, LX/HzH;->a(LX/Hx7;)V

    .line 2529713
    iget-object v0, p0, LX/I25;->a:LX/I26;

    iget-object v0, v0, LX/I26;->j:LX/HzI;

    invoke-virtual {v0}, LX/HzI;->a()V

    .line 2529714
    iget-object v0, p0, LX/I25;->a:LX/I26;

    invoke-static {v0, p1}, LX/I26;->c$redex0(LX/I26;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529715
    return-void
.end method
