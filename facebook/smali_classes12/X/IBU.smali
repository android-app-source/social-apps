.class public final LX/IBU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Bt7;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic d:Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;LX/Bt7;LX/1Pn;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2547198
    iput-object p1, p0, LX/IBU;->d:Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;

    iput-object p2, p0, LX/IBU;->a:LX/Bt7;

    iput-object p3, p0, LX/IBU;->b:LX/1Pn;

    iput-object p4, p0, LX/IBU;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const v0, 0x37688ddd

    invoke-static {v5, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2547199
    iget-object v0, p0, LX/IBU;->a:LX/Bt7;

    .line 2547200
    iput-boolean v2, v0, LX/Bt7;->a:Z

    .line 2547201
    iget-object v0, p0, LX/IBU;->b:LX/1Pn;

    check-cast v0, LX/1Pq;

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/IBU;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2547202
    iget-object v0, p0, LX/IBU;->d:Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;

    iget-object v2, p0, LX/IBU;->b:LX/1Pn;

    .line 2547203
    iget-object v3, v0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->b:LX/0Zb;

    const-string v4, "android_event_permalink_details_toggle_expansion"

    const/4 v6, 0x0

    invoke-interface {v3, v4, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2547204
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v3, v2

    .line 2547205
    check-cast v3, LX/2kn;

    invoke-interface {v3}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v3

    .line 2547206
    if-eqz v3, :cond_0

    .line 2547207
    const-string v6, "ref_module"

    iget-object p0, v3, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->a:Ljava/lang/String;

    invoke-virtual {v4, v6, p0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string p0, "ref_mechanism"

    iget-object p1, v3, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->b:Ljava/lang/String;

    invoke-virtual {v6, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string p0, "mechanism"

    iget-object v3, v3, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v6, p0, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2547208
    :cond_0
    const-string v6, "source_module"

    move-object v3, v2

    check-cast v3, LX/2kp;

    invoke-interface {v3}, LX/2kp;->t()LX/2jY;

    move-result-object v3

    .line 2547209
    iget-object p0, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, p0

    .line 2547210
    invoke-virtual {v4, v6, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v6, "surface"

    move-object v3, v2

    check-cast v3, LX/2kp;

    invoke-interface {v3}, LX/2kp;->t()LX/2jY;

    move-result-object v3

    .line 2547211
    iget-object p0, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, p0

    .line 2547212
    invoke-virtual {v4, v6, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "source_entity_id"

    check-cast v2, LX/2kp;

    invoke-interface {v2}, LX/2kp;->t()LX/2jY;

    move-result-object v6

    invoke-virtual {v6}, LX/2jY;->u()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2547213
    :cond_1
    const v0, 0x40fdad3e

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
