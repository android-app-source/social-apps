.class public final LX/HYt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HYm;


# instance fields
.field public final synthetic a:LX/HYv;


# direct methods
.method public constructor <init>(LX/HYv;)V
    .locals 0

    .prologue
    .line 2481084
    iput-object p1, p0, LX/HYt;->a:LX/HYv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2481085
    iget-object v0, p0, LX/HYt;->a:LX/HYv;

    iget-object v0, v0, LX/HYv;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v1, :cond_0

    .line 2481086
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationAdditionalEmailFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v0}, LX/HZ1;->b()LX/HZ1;

    move-result-object v0

    invoke-virtual {v0}, LX/HZ1;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2481087
    :goto_0
    return-object v0

    .line 2481088
    :cond_0
    iget-object v0, p0, LX/HYt;->a:LX/HYv;

    iget-object v0, v0, LX/HYv;->g:LX/HaQ;

    invoke-virtual {v0}, LX/HaQ;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2481089
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v0}, LX/HZ1;->c()LX/HZ1;

    move-result-object v0

    invoke-virtual {v0}, LX/HZ1;->a()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 2481090
    :cond_1
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v0}, LX/HZ1;->c()LX/HZ1;

    move-result-object v0

    invoke-virtual {v0}, LX/HZ1;->a()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
