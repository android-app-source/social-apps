.class public LX/IzB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2634316
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/IzB;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634283
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/IzB;->a:LX/0am;

    .line 2634284
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2634285
    iput-object v0, p0, LX/IzB;->b:LX/0Px;

    .line 2634286
    return-void
.end method

.method public static a(LX/0QB;)LX/IzB;
    .locals 7

    .prologue
    .line 2634287
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2634288
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2634289
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2634290
    if-nez v1, :cond_0

    .line 2634291
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2634292
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2634293
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2634294
    sget-object v1, LX/IzB;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2634295
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2634296
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2634297
    :cond_1
    if-nez v1, :cond_4

    .line 2634298
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2634299
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2634300
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 2634301
    new-instance v0, LX/IzB;

    invoke-direct {v0}, LX/IzB;-><init>()V

    .line 2634302
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2634303
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2634304
    if-nez v1, :cond_2

    .line 2634305
    sget-object v0, LX/IzB;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IzB;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2634306
    :goto_1
    if-eqz v0, :cond_3

    .line 2634307
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2634308
    :goto_3
    check-cast v0, LX/IzB;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2634309
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2634310
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2634311
    :catchall_1
    move-exception v0

    .line 2634312
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2634313
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2634314
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2634315
    :cond_2
    :try_start_8
    sget-object v0, LX/IzB;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IzB;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static d(LX/IzB;J)LX/0am;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2634317
    iget-object v0, p0, LX/IzB;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/IzB;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2634318
    iget-wide v7, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v4, v7

    .line 2634319
    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 2634320
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2634321
    :goto_1
    return-object v0

    .line 2634322
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2634323
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_1
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 2634231
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/IzB;->a:LX/0am;

    .line 2634232
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2634233
    iput-object v0, p0, LX/IzB;->b:LX/0Px;

    .line 2634234
    invoke-virtual {p0}, LX/IzB;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2634235
    monitor-exit p0

    return-void

    .line 2634236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2634324
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IzB;->c:LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)LX/0am;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2634269
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IzB;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2634270
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2634271
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2634272
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/IzB;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2634273
    iget-wide v7, v1, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v4, v7

    .line 2634274
    cmp-long v4, v4, p1

    if-nez v4, :cond_1

    .line 2634275
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 2634276
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2634277
    :cond_2
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 2634278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2634279
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/IzB;->a:LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2634280
    monitor-exit p0

    return-void

    .line 2634281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 1

    .prologue
    .line 2634266
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/IzB;->c:LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2634267
    monitor-exit p0

    return-void

    .line 2634268
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2634265
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IzB;->a:LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(J)V
    .locals 11

    .prologue
    .line 2634251
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IzB;->c:LX/0am;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IzB;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IzB;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2634252
    iget-wide v9, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v0, v9

    .line 2634253
    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 2634254
    invoke-virtual {p0}, LX/IzB;->c()V

    .line 2634255
    :cond_0
    iget-object v0, p0, LX/IzB;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2634256
    :goto_0
    monitor-exit p0

    return-void

    .line 2634257
    :cond_1
    :try_start_1
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2634258
    iget-object v0, p0, LX/IzB;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2634259
    iget-wide v9, v1, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v6, v9

    .line 2634260
    cmp-long v5, v6, p1

    if-eqz v5, :cond_2

    .line 2634261
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2634262
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2634263
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/IzB;->a:LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2634264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 4

    .prologue
    .line 2634242
    monitor-enter p0

    .line 2634243
    :try_start_0
    iget-wide v2, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v0, v2

    .line 2634244
    invoke-static {p0, v0, v1}, LX/IzB;->d(LX/IzB;J)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2634245
    :goto_0
    monitor-exit p0

    return-void

    .line 2634246
    :cond_0
    :try_start_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2634247
    iget-object v1, p0, LX/IzB;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2634248
    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2634249
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IzB;->b:LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2634250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 2634239
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/IzB;->c:LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2634240
    monitor-exit p0

    return-void

    .line 2634241
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 2634237
    invoke-direct {p0}, LX/IzB;->d()V

    .line 2634238
    return-void
.end method
