.class public final LX/ILo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ILc;


# instance fields
.field public final synthetic a:LX/4At;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;LX/4At;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2568331
    iput-object p1, p0, LX/ILo;->c:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iput-object p2, p0, LX/ILo;->a:LX/4At;

    iput-object p3, p0, LX/ILo;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2568321
    iget-object v0, p0, LX/ILo;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2568322
    iget-object v0, p0, LX/ILo;->c:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    const-string v1, "group_email_submit_for_verification"

    iget-object v2, p0, LX/ILo;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->a$redex0(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2568323
    iget-object v0, p0, LX/ILo;->c:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->f:LX/0Uh;

    const/16 v1, 0x39

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2568324
    iget-object v0, p0, LX/ILo;->c:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->t:LX/IL2;

    iget-object v1, p0, LX/ILo;->c:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/IL2;->b(Ljava/lang/String;)V

    .line 2568325
    :goto_0
    return-void

    .line 2568326
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2568327
    const-string v1, "submitted_email"

    iget-object v2, p0, LX/ILo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2568328
    iget-object v1, p0, LX/ILo;->c:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2568329
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2568330
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public final a(LX/IMp;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2568314
    iget-object v0, p0, LX/ILo;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2568315
    new-instance v0, LX/0ju;

    iget-object v2, p0, LX/ILo;->c:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v2, p1, LX/IMp;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v2, p1, LX/IMp;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v2

    .line 2568316
    iget v0, p1, LX/IMp;->c:I

    const v3, 0x14ff8b

    if-ne v0, v3, :cond_0

    .line 2568317
    new-instance v0, LX/ILn;

    invoke-direct {v0, p0}, LX/ILn;-><init>(LX/ILo;)V

    .line 2568318
    const v3, 0x7f080017

    invoke-virtual {v2, v3, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2568319
    :goto_0
    const v1, 0x7f080016

    invoke-virtual {v2, v1, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2568320
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
