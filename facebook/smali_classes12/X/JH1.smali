.class public final LX/JH1;
.super Landroid/text/style/CharacterStyle;
.source ""


# static fields
.field public static final a:LX/JH1;


# instance fields
.field public b:F

.field public c:F

.field public d:F

.field public e:I

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2671987
    new-instance v0, LX/JH1;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v5}, LX/JH1;-><init>(FFFIZ)V

    sput-object v0, LX/JH1;->a:LX/JH1;

    return-void
.end method

.method public constructor <init>(FFFIZ)V
    .locals 0

    .prologue
    .line 2671988
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    .line 2671989
    iput p1, p0, LX/JH1;->b:F

    .line 2671990
    iput p2, p0, LX/JH1;->c:F

    .line 2671991
    iput p3, p0, LX/JH1;->d:F

    .line 2671992
    iput p4, p0, LX/JH1;->e:I

    .line 2671993
    iput-boolean p5, p0, LX/JH1;->f:Z

    .line 2671994
    return-void
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 4

    .prologue
    .line 2671985
    iget v0, p0, LX/JH1;->d:F

    iget v1, p0, LX/JH1;->b:F

    iget v2, p0, LX/JH1;->c:F

    iget v3, p0, LX/JH1;->e:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 2671986
    return-void
.end method
