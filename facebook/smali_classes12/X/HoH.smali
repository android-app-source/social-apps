.class public final enum LX/HoH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HoH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HoH;

.field public static final enum FIRST_FETCH:LX/HoH;

.field public static final enum HEAD_LOAD:LX/HoH;

.field public static final enum TAIL_LOAD:LX/HoH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2505551
    new-instance v0, LX/HoH;

    const-string v1, "FIRST_FETCH"

    invoke-direct {v0, v1, v2}, LX/HoH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HoH;->FIRST_FETCH:LX/HoH;

    .line 2505552
    new-instance v0, LX/HoH;

    const-string v1, "HEAD_LOAD"

    invoke-direct {v0, v1, v3}, LX/HoH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HoH;->HEAD_LOAD:LX/HoH;

    .line 2505553
    new-instance v0, LX/HoH;

    const-string v1, "TAIL_LOAD"

    invoke-direct {v0, v1, v4}, LX/HoH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HoH;->TAIL_LOAD:LX/HoH;

    .line 2505554
    const/4 v0, 0x3

    new-array v0, v0, [LX/HoH;

    sget-object v1, LX/HoH;->FIRST_FETCH:LX/HoH;

    aput-object v1, v0, v2

    sget-object v1, LX/HoH;->HEAD_LOAD:LX/HoH;

    aput-object v1, v0, v3

    sget-object v1, LX/HoH;->TAIL_LOAD:LX/HoH;

    aput-object v1, v0, v4

    sput-object v0, LX/HoH;->$VALUES:[LX/HoH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2505555
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HoH;
    .locals 1

    .prologue
    .line 2505556
    const-class v0, LX/HoH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HoH;

    return-object v0
.end method

.method public static values()[LX/HoH;
    .locals 1

    .prologue
    .line 2505557
    sget-object v0, LX/HoH;->$VALUES:[LX/HoH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HoH;

    return-object v0
.end method
