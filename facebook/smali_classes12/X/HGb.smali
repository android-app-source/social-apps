.class public LX/HGb;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/11R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/ImageView;

.field public d:Lcom/facebook/widget/text/BetterTextView;

.field public e:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2446335
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2446336
    const-class v0, LX/HGb;

    invoke-static {v0, p0}, LX/HGb;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2446337
    const v0, 0x7f030e1c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2446338
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/HGb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2446339
    const v0, 0x7f0d2282

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2446340
    const v1, 0x7f0d2284

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/HGb;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2446341
    iget-boolean v1, v0, Lcom/facebook/widget/text/BetterTextView;->f:Z

    move v1, v1

    .line 2446342
    if-eqz v1, :cond_0

    .line 2446343
    invoke-static {v0}, LX/HGb;->a(Lcom/facebook/widget/text/BetterTextView;)V

    .line 2446344
    iget-object v0, p0, LX/HGb;->e:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setGravity(I)V

    .line 2446345
    :cond_0
    const v0, 0x7f0d2283

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/HGb;->c:Landroid/widget/ImageView;

    .line 2446346
    const v0, 0x7f0d2285

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HGb;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2446347
    iget-object v0, p0, LX/HGb;->a:LX/0wM;

    const v1, 0x7f0207df

    invoke-virtual {p0}, LX/HGb;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0a04a8

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/4 p1, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2446348
    iget-object v1, p0, LX/HGb;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2446349
    return-void
.end method

.method public static a(ILandroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2446350
    if-nez p0, :cond_0

    .line 2446351
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0811d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2446352
    :goto_0
    return-object v0

    .line 2446353
    :cond_0
    if-lez p0, :cond_1

    .line 2446354
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0085

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2446355
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid album size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/facebook/widget/text/BetterTextView;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2446356
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2446357
    const/4 v1, 0x0

    aget-object v1, v0, v1

    if-nez v1, :cond_0

    aget-object v1, v0, v3

    if-nez v1, :cond_0

    aget-object v1, v0, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    aget-object v1, v0, v1

    if-eqz v1, :cond_1

    .line 2446358
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Album detail title prefix layout mismatch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2446359
    :cond_1
    aget-object v0, v0, v4

    .line 2446360
    invoke-static {v0, v3}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 2446361
    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2446362
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/HGb;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object p0

    check-cast p0, LX/11R;

    iput-object v1, p1, LX/HGb;->a:LX/0wM;

    iput-object p0, p1, LX/HGb;->b:LX/11R;

    return-void
.end method
