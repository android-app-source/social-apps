.class public LX/Iml;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:LX/Imc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/ImY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/ImX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Ima;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/ImZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/ImW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Imb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/ImV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final j:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/payment/thread/PaymentBubbleDetailsView;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2610163
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Iml;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610164
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2610161
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Iml;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2610144
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610145
    const-class v0, LX/Iml;

    invoke-static {v0, p0}, LX/Iml;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610146
    const v0, 0x7f030d68

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2610147
    iget-object v0, p0, LX/Iml;->i:LX/0Zb;

    const-string v1, "p2p_receive"

    const-string v2, "p2p_payment_bubble_rendered"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2610148
    const v0, 0x7f0d2119    # 1.87593E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Iml;->j:LX/4ob;

    .line 2610149
    const v0, 0x7f0d211a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Iml;->k:LX/4ob;

    .line 2610150
    const v0, 0x7f0d211b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Iml;->l:LX/4ob;

    .line 2610151
    const v0, 0x7f0d211c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Iml;->m:LX/4ob;

    .line 2610152
    const v0, 0x7f0d211d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Iml;->n:LX/4ob;

    .line 2610153
    const v0, 0x7f0d211e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Iml;->o:LX/4ob;

    .line 2610154
    const v0, 0x7f0d211f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Iml;->p:LX/4ob;

    .line 2610155
    const v0, 0x7f0d2120

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/Iml;->q:LX/4ob;

    .line 2610156
    return-void
.end method

.method private static a(LX/Iml;LX/Imc;LX/ImY;LX/ImX;LX/Ima;LX/ImZ;LX/ImW;LX/Imb;LX/ImV;LX/0Zb;)V
    .locals 0

    .prologue
    .line 2610160
    iput-object p1, p0, LX/Iml;->a:LX/Imc;

    iput-object p2, p0, LX/Iml;->b:LX/ImY;

    iput-object p3, p0, LX/Iml;->c:LX/ImX;

    iput-object p4, p0, LX/Iml;->d:LX/Ima;

    iput-object p5, p0, LX/Iml;->e:LX/ImZ;

    iput-object p6, p0, LX/Iml;->f:LX/ImW;

    iput-object p7, p0, LX/Iml;->g:LX/Imb;

    iput-object p8, p0, LX/Iml;->h:LX/ImV;

    iput-object p9, p0, LX/Iml;->i:LX/0Zb;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Iml;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, LX/Iml;

    new-instance v1, LX/Imc;

    const/16 v2, 0x28b3

    invoke-static {v9, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x28bb

    invoke-static {v9, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/Imc;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v1, v1

    check-cast v1, LX/Imc;

    new-instance v2, LX/ImY;

    const/16 v3, 0x28b0

    invoke-static {v9, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x28b7

    invoke-static {v9, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/ImY;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v2, v2

    check-cast v2, LX/ImY;

    new-instance v3, LX/ImX;

    const/16 v4, 0x28af

    invoke-static {v9, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x28b6

    invoke-static {v9, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/ImX;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v3, v3

    check-cast v3, LX/ImX;

    new-instance v4, LX/Ima;

    const/16 v5, 0x28b2

    invoke-static {v9, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x28b9

    invoke-static {v9, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-direct {v4, v5, v6}, LX/Ima;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v4, v4

    check-cast v4, LX/Ima;

    new-instance v5, LX/ImZ;

    const/16 v6, 0x28b1

    invoke-static {v9, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x28b8

    invoke-static {v9, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct {v5, v6, v7}, LX/ImZ;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v5, v5

    check-cast v5, LX/ImZ;

    new-instance v6, LX/ImW;

    const/16 v7, 0x28ae

    invoke-static {v9, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x28b5

    invoke-static {v9, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct {v6, v7, v8}, LX/ImW;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v6, v6

    check-cast v6, LX/ImW;

    new-instance v7, LX/Imb;

    const/16 v8, 0x28a8

    invoke-static {v9, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 p0, 0x28ba

    invoke-static {v9, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v7, v8, p0}, LX/Imb;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v7, v7

    check-cast v7, LX/Imb;

    new-instance v8, LX/ImV;

    const/16 p0, 0x28ad

    invoke-static {v9, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p1, 0x28b4

    invoke-static {v9, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-direct {v8, p0, p1}, LX/ImV;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v8, v8

    check-cast v8, LX/ImV;

    invoke-static {v9}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static/range {v0 .. v9}, LX/Iml;->a(LX/Iml;LX/Imc;LX/ImY;LX/ImX;LX/Ima;LX/ImZ;LX/ImW;LX/Imb;LX/ImV;LX/0Zb;)V

    return-void
.end method


# virtual methods
.method public setPaymentsAnimatingItemInfo(LX/JEV;)V
    .locals 1

    .prologue
    .line 2610157
    iget-object v0, p0, LX/Iml;->o:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610158
    iget-object v0, p0, LX/Iml;->o:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/thread/PaymentBubbleDetailsView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->setItemInfo(LX/JEV;)V

    .line 2610159
    :cond_0
    return-void
.end method
