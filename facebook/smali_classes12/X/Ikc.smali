.class public final LX/Ikc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IZ9;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

.field private final b:LX/IZ9;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;LX/IZ9;)V
    .locals 0
    .param p2    # LX/IZ9;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606802
    iput-object p1, p0, LX/Ikc;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606803
    iput-object p2, p0, LX/Ikc;->b:LX/IZ9;

    .line 2606804
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2606805
    iget-object v0, p0, LX/Ikc;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    const/4 v1, 0x1

    .line 2606806
    iput-boolean v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->n:Z

    .line 2606807
    iget-object v0, p0, LX/Ikc;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 2606808
    iget-object v0, p0, LX/Ikc;->b:LX/IZ9;

    if-eqz v0, :cond_0

    .line 2606809
    iget-object v0, p0, LX/Ikc;->b:LX/IZ9;

    invoke-interface {v0}, LX/IZ9;->a()V

    .line 2606810
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2606811
    iget-object v0, p0, LX/Ikc;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    const/4 v1, 0x0

    .line 2606812
    iput-boolean v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->n:Z

    .line 2606813
    iget-object v0, p0, LX/Ikc;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 2606814
    iget-object v0, p0, LX/Ikc;->b:LX/IZ9;

    if-eqz v0, :cond_0

    .line 2606815
    iget-object v0, p0, LX/Ikc;->b:LX/IZ9;

    invoke-interface {v0}, LX/IZ9;->b()V

    .line 2606816
    :cond_0
    return-void
.end method
