.class public final LX/HNg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

.field public final synthetic c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

.field public final synthetic d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;Ljava/lang/String;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;)V
    .locals 0

    .prologue
    .line 2458705
    iput-object p1, p0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iput-object p2, p0, LX/HNg;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HNg;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iput-object p4, p0, LX/HNg;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x2

    const v0, 0x4b71965a    # 1.5832666E7f

    invoke-static {v8, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2458706
    iget-object v0, p0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->a(Z)V

    .line 2458707
    iget-object v0, p0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->b:LX/HNd;

    iget-object v1, p0, LX/HNg;->a:Ljava/lang/String;

    iget-object v2, p0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    .line 2458708
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v3

    .line 2458709
    const-string v3, "arg_config_action_data"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, LX/CYE;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iget-object v4, p0, LX/HNg;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iget-object v5, p0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    .line 2458710
    iget-object v7, v5, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v7

    .line 2458711
    const-string v7, "arg_page_admin_cta"

    invoke-static {v5, v7}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-virtual/range {v0 .. v5}, LX/HNd;->a(Ljava/lang/String;LX/CYE;Ljava/util/Map;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)LX/HNc;

    move-result-object v0

    .line 2458712
    new-instance v1, LX/HNf;

    invoke-direct {v1, p0}, LX/HNf;-><init>(LX/HNg;)V

    .line 2458713
    iput-object v1, v0, LX/HNc;->d:LX/HNb;

    .line 2458714
    invoke-virtual {v0}, LX/HNc;->a()V

    .line 2458715
    iget-object v0, p0, LX/HNg;->d:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->bd:LX/0ih;

    const-string v2, "tap_enable_now"

    const-string v3, "on_enable_message_nux"

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2458716
    const v0, 0x68489c4f

    invoke-static {v8, v8, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
