.class public final LX/JO6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JO7;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

.field public b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

.field public c:LX/3mj;

.field public d:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/JO7;


# direct methods
.method public constructor <init>(LX/JO7;)V
    .locals 1

    .prologue
    .line 2686907
    iput-object p1, p0, LX/JO6;->e:LX/JO7;

    .line 2686908
    move-object v0, p1

    .line 2686909
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2686910
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2686886
    const-string v0, "ConnectWithFacebookPageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2686887
    if-ne p0, p1, :cond_1

    .line 2686888
    :cond_0
    :goto_0
    return v0

    .line 2686889
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2686890
    goto :goto_0

    .line 2686891
    :cond_3
    check-cast p1, LX/JO6;

    .line 2686892
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2686893
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2686894
    if-eq v2, v3, :cond_0

    .line 2686895
    iget-object v2, p0, LX/JO6;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JO6;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object v3, p1, LX/JO6;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2686896
    goto :goto_0

    .line 2686897
    :cond_5
    iget-object v2, p1, LX/JO6;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-nez v2, :cond_4

    .line 2686898
    :cond_6
    iget-object v2, p0, LX/JO6;->b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JO6;->b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget-object v3, p1, LX/JO6;->b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2686899
    goto :goto_0

    .line 2686900
    :cond_8
    iget-object v2, p1, LX/JO6;->b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    if-nez v2, :cond_7

    .line 2686901
    :cond_9
    iget-object v2, p0, LX/JO6;->c:LX/3mj;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JO6;->c:LX/3mj;

    iget-object v3, p1, LX/JO6;->c:LX/3mj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2686902
    goto :goto_0

    .line 2686903
    :cond_b
    iget-object v2, p1, LX/JO6;->c:LX/3mj;

    if-nez v2, :cond_a

    .line 2686904
    :cond_c
    iget-object v2, p0, LX/JO6;->d:LX/1Pb;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JO6;->d:LX/1Pb;

    iget-object v3, p1, LX/JO6;->d:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2686905
    goto :goto_0

    .line 2686906
    :cond_d
    iget-object v2, p1, LX/JO6;->d:LX/1Pb;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
