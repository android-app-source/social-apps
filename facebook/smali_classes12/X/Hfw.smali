.class public final enum LX/Hfw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hfw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hfw;

.field public static final enum ALL:LX/Hfw;

.field public static final enum FAVORITED:LX/Hfw;

.field public static final enum RECENT:LX/Hfw;

.field public static final enum TOP:LX/Hfw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2492569
    new-instance v0, LX/Hfw;

    const-string v1, "RECENT"

    invoke-direct {v0, v1, v2}, LX/Hfw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hfw;->RECENT:LX/Hfw;

    new-instance v0, LX/Hfw;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, LX/Hfw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hfw;->ALL:LX/Hfw;

    new-instance v0, LX/Hfw;

    const-string v1, "FAVORITED"

    invoke-direct {v0, v1, v4}, LX/Hfw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hfw;->FAVORITED:LX/Hfw;

    new-instance v0, LX/Hfw;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v5}, LX/Hfw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hfw;->TOP:LX/Hfw;

    .line 2492570
    const/4 v0, 0x4

    new-array v0, v0, [LX/Hfw;

    sget-object v1, LX/Hfw;->RECENT:LX/Hfw;

    aput-object v1, v0, v2

    sget-object v1, LX/Hfw;->ALL:LX/Hfw;

    aput-object v1, v0, v3

    sget-object v1, LX/Hfw;->FAVORITED:LX/Hfw;

    aput-object v1, v0, v4

    sget-object v1, LX/Hfw;->TOP:LX/Hfw;

    aput-object v1, v0, v5

    sput-object v0, LX/Hfw;->$VALUES:[LX/Hfw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2492571
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hfw;
    .locals 1

    .prologue
    .line 2492572
    const-class v0, LX/Hfw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hfw;

    return-object v0
.end method

.method public static values()[LX/Hfw;
    .locals 1

    .prologue
    .line 2492573
    sget-object v0, LX/Hfw;->$VALUES:[LX/Hfw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hfw;

    return-object v0
.end method
