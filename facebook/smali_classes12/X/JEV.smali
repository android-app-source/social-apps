.class public LX/JEV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/3Nm;",
            ">;"
        }
    .end annotation
.end field

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2666079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2666080
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/JEV;->a:Ljava/util/Set;

    .line 2666081
    iput v1, p0, LX/JEV;->b:F

    .line 2666082
    iput v1, p0, LX/JEV;->c:F

    .line 2666083
    iput v1, p0, LX/JEV;->d:F

    .line 2666084
    iput v1, p0, LX/JEV;->e:F

    .line 2666085
    const/4 v0, 0x0

    iput v0, p0, LX/JEV;->f:I

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2666076
    iget-object v0, p0, LX/JEV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Nm;

    .line 2666077
    invoke-virtual {v0}, LX/3Nm;->a()V

    goto :goto_0

    .line 2666078
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/3Nm;)V
    .locals 1

    .prologue
    .line 2666074
    iget-object v0, p0, LX/JEV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2666075
    return-void
.end method

.method public final b(LX/3Nm;)V
    .locals 1

    .prologue
    .line 2666072
    iget-object v0, p0, LX/JEV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2666073
    return-void
.end method

.method public setAlpha(F)V
    .locals 0

    .prologue
    .line 2666069
    iput p1, p0, LX/JEV;->e:F

    .line 2666070
    invoke-direct {p0}, LX/JEV;->f()V

    .line 2666071
    return-void
.end method

.method public setAnimationOffset(F)V
    .locals 1

    .prologue
    .line 2666064
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2666065
    iput p1, p0, LX/JEV;->b:F

    .line 2666066
    invoke-direct {p0}, LX/JEV;->f()V

    .line 2666067
    return-void

    .line 2666068
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScaleX(F)V
    .locals 0

    .prologue
    .line 2666061
    iput p1, p0, LX/JEV;->c:F

    .line 2666062
    invoke-direct {p0}, LX/JEV;->f()V

    .line 2666063
    return-void
.end method

.method public setScaleY(F)V
    .locals 0

    .prologue
    .line 2666058
    iput p1, p0, LX/JEV;->d:F

    .line 2666059
    invoke-direct {p0}, LX/JEV;->f()V

    .line 2666060
    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    .prologue
    .line 2666055
    iput p1, p0, LX/JEV;->f:I

    .line 2666056
    invoke-direct {p0}, LX/JEV;->f()V

    .line 2666057
    return-void
.end method
