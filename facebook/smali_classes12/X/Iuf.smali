.class public final enum LX/Iuf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iuf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iuf;

.field public static final enum BUZZ:LX/Iuf;

.field public static final enum FORCE_BUZZ:LX/Iuf;

.field public static final enum FORCE_SILENT:LX/Iuf;

.field public static final enum FORCE_SUPPRESS:LX/Iuf;

.field public static final enum SILENT:LX/Iuf;

.field public static final enum SUPPRESS:LX/Iuf;

.field public static final enum UNSET:LX/Iuf;


# instance fields
.field public final name:Ljava/lang/String;

.field public final priority:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2627029
    new-instance v0, LX/Iuf;

    const-string v1, "UNSET"

    const/4 v2, -0x1

    const-string v3, "unset"

    invoke-direct {v0, v1, v5, v2, v3}, LX/Iuf;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Iuf;->UNSET:LX/Iuf;

    .line 2627030
    new-instance v0, LX/Iuf;

    const-string v1, "BUZZ"

    const-string v2, "buzz"

    invoke-direct {v0, v1, v6, v5, v2}, LX/Iuf;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    .line 2627031
    new-instance v0, LX/Iuf;

    const-string v1, "SILENT"

    const-string v2, "silent"

    invoke-direct {v0, v1, v7, v6, v2}, LX/Iuf;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Iuf;->SILENT:LX/Iuf;

    .line 2627032
    new-instance v0, LX/Iuf;

    const-string v1, "SUPPRESS"

    const-string v2, "supress"

    invoke-direct {v0, v1, v8, v7, v2}, LX/Iuf;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Iuf;->SUPPRESS:LX/Iuf;

    .line 2627033
    new-instance v0, LX/Iuf;

    const-string v1, "FORCE_BUZZ"

    const-string v2, "f_buzz"

    invoke-direct {v0, v1, v9, v8, v2}, LX/Iuf;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Iuf;->FORCE_BUZZ:LX/Iuf;

    .line 2627034
    new-instance v0, LX/Iuf;

    const-string v1, "FORCE_SILENT"

    const/4 v2, 0x5

    const-string v3, "f_silent"

    invoke-direct {v0, v1, v2, v9, v3}, LX/Iuf;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Iuf;->FORCE_SILENT:LX/Iuf;

    .line 2627035
    new-instance v0, LX/Iuf;

    const-string v1, "FORCE_SUPPRESS"

    const/4 v2, 0x6

    const/4 v3, 0x5

    const-string v4, "f_suppress"

    invoke-direct {v0, v1, v2, v3, v4}, LX/Iuf;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Iuf;->FORCE_SUPPRESS:LX/Iuf;

    .line 2627036
    const/4 v0, 0x7

    new-array v0, v0, [LX/Iuf;

    sget-object v1, LX/Iuf;->UNSET:LX/Iuf;

    aput-object v1, v0, v5

    sget-object v1, LX/Iuf;->BUZZ:LX/Iuf;

    aput-object v1, v0, v6

    sget-object v1, LX/Iuf;->SILENT:LX/Iuf;

    aput-object v1, v0, v7

    sget-object v1, LX/Iuf;->SUPPRESS:LX/Iuf;

    aput-object v1, v0, v8

    sget-object v1, LX/Iuf;->FORCE_BUZZ:LX/Iuf;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/Iuf;->FORCE_SILENT:LX/Iuf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Iuf;->FORCE_SUPPRESS:LX/Iuf;

    aput-object v2, v0, v1

    sput-object v0, LX/Iuf;->$VALUES:[LX/Iuf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2627037
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2627038
    iput p3, p0, LX/Iuf;->priority:I

    .line 2627039
    iput-object p4, p0, LX/Iuf;->name:Ljava/lang/String;

    .line 2627040
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iuf;
    .locals 1

    .prologue
    .line 2627041
    const-class v0, LX/Iuf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iuf;

    return-object v0
.end method

.method public static values()[LX/Iuf;
    .locals 1

    .prologue
    .line 2627042
    sget-object v0, LX/Iuf;->$VALUES:[LX/Iuf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iuf;

    return-object v0
.end method
