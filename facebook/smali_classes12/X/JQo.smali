.class public LX/JQo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static l:LX/0Xm;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Kl;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2CH;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public e:LX/3Lb;

.field public f:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<",
            "Ljava/lang/Void;",
            "LX/3MZ;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/3Mg;

.field public h:Z

.field public i:LX/JR9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0dN;

.field public k:LX/JR1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2692223
    const-class v0, LX/JQo;

    sput-object v0, LX/JQo;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2CH;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/3Kl;",
            ">;",
            "Lcom/facebook/presence/PresenceManager;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692244
    const/4 v0, 0x0

    iput-object v0, p0, LX/JQo;->i:LX/JR9;

    .line 2692245
    iput-object p1, p0, LX/JQo;->b:LX/0Or;

    .line 2692246
    iput-object p2, p0, LX/JQo;->c:LX/2CH;

    .line 2692247
    iput-object p3, p0, LX/JQo;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2692248
    new-instance v0, LX/JQl;

    invoke-direct {v0, p0}, LX/JQl;-><init>(LX/JQo;)V

    iput-object v0, p0, LX/JQo;->g:LX/3Mg;

    .line 2692249
    iget-object v0, p0, LX/JQo;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kl;

    invoke-virtual {v0}, LX/3Kl;->b()LX/3Lb;

    move-result-object v0

    iput-object v0, p0, LX/JQo;->e:LX/3Lb;

    .line 2692250
    new-instance v0, LX/JQm;

    invoke-direct {v0, p0}, LX/JQm;-><init>(LX/JQo;)V

    iput-object v0, p0, LX/JQo;->f:LX/3Mb;

    .line 2692251
    new-instance v0, LX/JQn;

    invoke-direct {v0, p0}, LX/JQn;-><init>(LX/JQo;)V

    iput-object v0, p0, LX/JQo;->j:LX/0dN;

    .line 2692252
    return-void
.end method

.method public static a(LX/0QB;)LX/JQo;
    .locals 6

    .prologue
    .line 2692232
    const-class v1, LX/JQo;

    monitor-enter v1

    .line 2692233
    :try_start_0
    sget-object v0, LX/JQo;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692234
    sput-object v2, LX/JQo;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692235
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692236
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692237
    new-instance v5, LX/JQo;

    const/16 v3, 0xd09

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v3

    check-cast v3, LX/2CH;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v5, p0, v3, v4}, LX/JQo;-><init>(LX/0Or;LX/2CH;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2692238
    move-object v0, v5

    .line 2692239
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692240
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692241
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692242
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b$redex0(LX/JQo;)V
    .locals 2

    .prologue
    .line 2692253
    iget-boolean v0, p0, LX/JQo;->h:Z

    if-eqz v0, :cond_1

    .line 2692254
    :cond_0
    :goto_0
    return-void

    .line 2692255
    :cond_1
    iget-object v0, p0, LX/JQo;->c:LX/2CH;

    .line 2692256
    iget-object v1, v0, LX/2CH;->K:LX/2AL;

    move-object v0, v1

    .line 2692257
    sget-object v1, LX/2AL;->TP_FULL_LIST_RECEIVED:LX/2AL;

    if-ne v0, v1, :cond_0

    .line 2692258
    iget-object v0, p0, LX/JQo;->e:LX/3Lb;

    invoke-virtual {v0}, LX/3Lb;->a()V

    goto :goto_0
.end method

.method public static c(LX/JQo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2692225
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JQo;->h:Z

    .line 2692226
    iget-object v0, p0, LX/JQo;->e:LX/3Lb;

    .line 2692227
    iput-object v2, v0, LX/3Lb;->B:LX/3Mb;

    .line 2692228
    iget-object v0, p0, LX/JQo;->c:LX/2CH;

    invoke-virtual {v0, p0}, LX/2CH;->b(Ljava/lang/Object;)V

    .line 2692229
    iget-object v0, p0, LX/JQo;->c:LX/2CH;

    iget-object v1, p0, LX/JQo;->g:LX/3Mg;

    invoke-virtual {v0, v1}, LX/2CH;->b(LX/3Mg;)V

    .line 2692230
    iput-object v2, p0, LX/JQo;->i:LX/JR9;

    .line 2692231
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2692224
    iget-object v0, p0, LX/JQo;->c:LX/2CH;

    invoke-virtual {v0}, LX/2CH;->e()Z

    move-result v0

    return v0
.end method
