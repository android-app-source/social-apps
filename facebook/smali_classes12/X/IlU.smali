.class public LX/IlU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/DtL;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/payments/p2p/model/Amount;

.field public final d:LX/Ila;

.field public final e:Z


# direct methods
.method public constructor <init>(LX/IlV;)V
    .locals 1

    .prologue
    .line 2607817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607818
    iget-object v0, p1, LX/IlV;->a:LX/DtL;

    move-object v0, v0

    .line 2607819
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607820
    iget-object v0, p1, LX/IlV;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2607821
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607822
    iget-object v0, p1, LX/IlV;->c:Lcom/facebook/payments/p2p/model/Amount;

    move-object v0, v0

    .line 2607823
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607824
    iget-object v0, p1, LX/IlV;->d:LX/Ila;

    move-object v0, v0

    .line 2607825
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607826
    iget-object v0, p1, LX/IlV;->e:Ljava/lang/Boolean;

    move-object v0, v0

    .line 2607827
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607828
    iget-object v0, p1, LX/IlV;->a:LX/DtL;

    move-object v0, v0

    .line 2607829
    iput-object v0, p0, LX/IlU;->a:LX/DtL;

    .line 2607830
    iget-object v0, p1, LX/IlV;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2607831
    iput-object v0, p0, LX/IlU;->b:Ljava/lang/String;

    .line 2607832
    iget-object v0, p1, LX/IlV;->c:Lcom/facebook/payments/p2p/model/Amount;

    move-object v0, v0

    .line 2607833
    iput-object v0, p0, LX/IlU;->c:Lcom/facebook/payments/p2p/model/Amount;

    .line 2607834
    iget-object v0, p1, LX/IlV;->d:LX/Ila;

    move-object v0, v0

    .line 2607835
    iput-object v0, p0, LX/IlU;->d:LX/Ila;

    .line 2607836
    iget-object v0, p1, LX/IlV;->e:Ljava/lang/Boolean;

    move-object v0, v0

    .line 2607837
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/IlU;->e:Z

    .line 2607838
    return-void
.end method

.method public static newBuilder()LX/IlV;
    .locals 1

    .prologue
    .line 2607839
    new-instance v0, LX/IlV;

    invoke-direct {v0}, LX/IlV;-><init>()V

    return-object v0
.end method
