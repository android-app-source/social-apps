.class public final LX/J6w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;)V
    .locals 0

    .prologue
    .line 2650405
    iput-object p1, p0, LX/J6w;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x60f3f078    # -2.9659E-20f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650406
    iget-object v1, p0, LX/J6w;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    iget-object v1, v1, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->p:LX/Hrh;

    .line 2650407
    iget-object v3, v1, LX/Hrh;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    invoke-virtual {v3}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2650408
    iget-object v3, v1, LX/Hrh;->d:LX/Hrk;

    iget-object v3, v3, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object v3, v3, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mFirstSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2650409
    iget-object v4, v1, LX/Hrh;->d:LX/Hrk;

    iget-object v4, v4, LX/Hrk;->i:LX/8Ps;

    sget-object v5, LX/5nc;->CHOSE_FIRST_OPTION:LX/5nc;

    iget-object p0, v1, LX/Hrh;->d:LX/Hrk;

    iget-object p0, p0, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object p0, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const-string p1, "traditional_composer"

    invoke-virtual {v4, v5, p0, v3, p1}, LX/8Ps;->a(LX/5nc;LX/1oU;LX/1oU;Ljava/lang/String;)V

    .line 2650410
    iget-object v4, v1, LX/Hrh;->d:LX/Hrk;

    iget-object v5, v1, LX/Hrh;->b:LX/HqL;

    iget-object p0, v1, LX/Hrh;->c:LX/HqM;

    invoke-static {v4, v3, v5, p0}, LX/Hrk;->a$redex0(LX/Hrk;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/HqL;LX/HqM;)V

    .line 2650411
    const v1, 0x1a406ffb

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
