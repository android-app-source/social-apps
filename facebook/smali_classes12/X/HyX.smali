.class public final LX/HyX;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524304
    iput-object p1, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    .line 2524305
    iget-object v0, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    .line 2524306
    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v2, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v1, v2, :cond_2

    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    .line 2524307
    sget-object v3, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v2, v3, :cond_3

    iget-object v3, v1, LX/Hyx;->e:Ljava/lang/String;

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    move v1, v3

    .line 2524308
    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getLastVisiblePosition()I

    move-result v1

    add-int/lit8 v1, v1, 0x6

    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v2}, LX/1OM;->ij_()I

    move-result v2

    if-le v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2524309
    if-eqz v0, :cond_0

    .line 2524310
    iget-object v0, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    const/4 v2, 0x1

    .line 2524311
    iput-boolean v2, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    .line 2524312
    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v1, v2}, LX/Hz2;->b(Z)V

    .line 2524313
    invoke-static {v0, v2}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->c(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Z)V

    .line 2524314
    :goto_2
    return-void

    .line 2524315
    :cond_0
    iget-object v0, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->A(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    .line 2524316
    if-lez p3, :cond_1

    iget-object v0, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-boolean v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ac:Z

    if-nez v0, :cond_1

    .line 2524317
    iget-object v0, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->l:LX/1nQ;

    iget-object v1, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->T:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->T:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1nQ;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2524318
    iget-object v0, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    const/4 v1, 0x1

    .line 2524319
    iput-boolean v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ac:Z

    .line 2524320
    :cond_1
    iget-object v0, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    iget-object v1, p0, LX/HyX;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->x:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 2524321
    invoke-static {v0, v2, v3}, LX/2ja;->g(LX/2ja;J)V

    .line 2524322
    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method
