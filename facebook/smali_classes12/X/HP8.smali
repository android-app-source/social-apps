.class public final LX/HP8;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/BPT;

.field public final synthetic b:LX/HP9;


# direct methods
.method public constructor <init>(LX/HP9;LX/BPT;)V
    .locals 0

    .prologue
    .line 2461315
    iput-object p1, p0, LX/HP8;->b:LX/HP9;

    iput-object p2, p0, LX/HP8;->a:LX/BPT;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 2461316
    iget-object v0, p0, LX/HP8;->b:LX/HP9;

    iget-object v0, v0, LX/HP9;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->k:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/HP8;->b:LX/HP9;

    iget-object v2, v2, LX/HP9;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2461317
    iget-object v0, p0, LX/HP8;->b:LX/HP9;

    iget-object v0, v0, LX/HP9;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Cannot delete post: storyId = %s, cacheId = %s, legacyApiStoryId = %s"

    iget-object v3, p0, LX/HP8;->a:LX/BPT;

    iget-object v3, v3, LX/BPT;->c:Ljava/lang/String;

    iget-object v4, p0, LX/HP8;->a:LX/BPT;

    iget-object v4, v4, LX/BPT;->b:Ljava/lang/String;

    iget-object v5, p0, LX/HP8;->a:LX/BPT;

    iget-object v5, v5, LX/BPT;->a:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461318
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2461319
    new-instance v1, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    iget-object v0, p0, LX/HP8;->a:LX/BPT;

    iget-object v0, v0, LX/BPT;->a:Ljava/lang/String;

    iget-object v2, p0, LX/HP8;->a:LX/BPT;

    iget-object v2, v2, LX/BPT;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/HP8;->a:LX/BPT;

    iget-object v3, v3, LX/BPT;->c:Ljava/lang/String;

    sget-object v4, LX/55G;->LOCAL_ONLY:LX/55G;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/facebook/api/feed/DeleteStoryMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/55G;)V

    .line 2461320
    iget-object v0, p0, LX/HP8;->b:LX/HP9;

    iget-object v0, v0, LX/HP9;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/99v;

    invoke-virtual {v0, v1}, LX/99v;->a(Lcom/facebook/api/feed/DeleteStoryMethod$Params;)V

    .line 2461321
    return-void
.end method
