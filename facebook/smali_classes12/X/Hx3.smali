.class public final enum LX/Hx3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hx3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hx3;

.field public static final enum BIRTHDAYS:LX/Hx3;

.field public static final enum BOTTOM_PADDING:LX/Hx3;

.field public static final enum DASHBOARD_FILTER:LX/Hx3;

.field public static final enum EVENT:LX/Hx3;

.field public static final enum SUBSCRIPTIONS:LX/Hx3;

.field public static final enum SUGGESTIONS:LX/Hx3;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2521536
    new-instance v0, LX/Hx3;

    const-string v1, "DASHBOARD_FILTER"

    invoke-direct {v0, v1, v3}, LX/Hx3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx3;->DASHBOARD_FILTER:LX/Hx3;

    .line 2521537
    new-instance v0, LX/Hx3;

    const-string v1, "EVENT"

    invoke-direct {v0, v1, v4}, LX/Hx3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx3;->EVENT:LX/Hx3;

    .line 2521538
    new-instance v0, LX/Hx3;

    const-string v1, "BIRTHDAYS"

    invoke-direct {v0, v1, v5}, LX/Hx3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx3;->BIRTHDAYS:LX/Hx3;

    .line 2521539
    new-instance v0, LX/Hx3;

    const-string v1, "SUGGESTIONS"

    invoke-direct {v0, v1, v6}, LX/Hx3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx3;->SUGGESTIONS:LX/Hx3;

    .line 2521540
    new-instance v0, LX/Hx3;

    const-string v1, "SUBSCRIPTIONS"

    invoke-direct {v0, v1, v7}, LX/Hx3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx3;->SUBSCRIPTIONS:LX/Hx3;

    .line 2521541
    new-instance v0, LX/Hx3;

    const-string v1, "BOTTOM_PADDING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Hx3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx3;->BOTTOM_PADDING:LX/Hx3;

    .line 2521542
    const/4 v0, 0x6

    new-array v0, v0, [LX/Hx3;

    sget-object v1, LX/Hx3;->DASHBOARD_FILTER:LX/Hx3;

    aput-object v1, v0, v3

    sget-object v1, LX/Hx3;->EVENT:LX/Hx3;

    aput-object v1, v0, v4

    sget-object v1, LX/Hx3;->BIRTHDAYS:LX/Hx3;

    aput-object v1, v0, v5

    sget-object v1, LX/Hx3;->SUGGESTIONS:LX/Hx3;

    aput-object v1, v0, v6

    sget-object v1, LX/Hx3;->SUBSCRIPTIONS:LX/Hx3;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Hx3;->BOTTOM_PADDING:LX/Hx3;

    aput-object v2, v0, v1

    sput-object v0, LX/Hx3;->$VALUES:[LX/Hx3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2521543
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hx3;
    .locals 1

    .prologue
    .line 2521544
    const-class v0, LX/Hx3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hx3;

    return-object v0
.end method

.method public static values()[LX/Hx3;
    .locals 1

    .prologue
    .line 2521545
    sget-object v0, LX/Hx3;->$VALUES:[LX/Hx3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hx3;

    return-object v0
.end method
