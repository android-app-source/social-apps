.class public LX/Hry;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/9bY;

.field public final b:LX/0gd;

.field public final c:LX/9at;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Landroid/content/Context;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hs2;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final h:LX/Hrp;

.field public i:LX/Hrx;

.field public j:LX/Hs1;

.field public final k:LX/Hro;

.field public final l:Landroid/support/v4/app/Fragment;

.field private final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0dk;

.field private final o:LX/Hs4;

.field private final p:LX/0W3;

.field public q:Z

.field public r:LX/0ht;

.field public s:Z

.field public t:LX/23P;


# direct methods
.method public constructor <init>(LX/Hrp;LX/Hro;Landroid/support/v4/app/Fragment;Lcom/facebook/auth/viewercontext/ViewerContext;LX/9bY;LX/0gd;LX/9at;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/23P;LX/0Ot;LX/0Or;LX/0dk;LX/Hs4;LX/0W3;)V
    .locals 2
    .param p1    # LX/Hrp;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hro;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/album/controller/AlbumSelectorController$AlbumSelectorCallback;",
            "Lcom/facebook/composer/album/controller/AlbumSelectorController$DataProvider;",
            "Landroid/support/v4/app/Fragment;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/9bY;",
            "LX/0gd;",
            "LX/9at;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/content/Context;",
            "LX/23P;",
            "LX/0Ot",
            "<",
            "LX/Hs2;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0dk;",
            "LX/Hs4;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2513749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2513750
    iput-object p1, p0, LX/Hry;->h:LX/Hrp;

    .line 2513751
    iput-object p2, p0, LX/Hry;->k:LX/Hro;

    .line 2513752
    iput-object p3, p0, LX/Hry;->l:Landroid/support/v4/app/Fragment;

    .line 2513753
    iput-object p4, p0, LX/Hry;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2513754
    iput-object p12, p0, LX/Hry;->m:LX/0Or;

    .line 2513755
    iput-object p13, p0, LX/Hry;->n:LX/0dk;

    .line 2513756
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Hry;->o:LX/Hs4;

    .line 2513757
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Hry;->p:LX/0W3;

    .line 2513758
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Hry;->q:Z

    .line 2513759
    iput-object p5, p0, LX/Hry;->a:LX/9bY;

    .line 2513760
    iput-object p6, p0, LX/Hry;->b:LX/0gd;

    .line 2513761
    iput-object p7, p0, LX/Hry;->c:LX/9at;

    .line 2513762
    iput-object p8, p0, LX/Hry;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2513763
    iput-object p9, p0, LX/Hry;->e:Landroid/content/Context;

    .line 2513764
    iput-object p11, p0, LX/Hry;->f:LX/0Ot;

    .line 2513765
    iput-object p10, p0, LX/Hry;->t:LX/23P;

    .line 2513766
    return-void
.end method

.method private static a(LX/Hry;Landroid/widget/ProgressBar;Landroid/widget/ListView;)V
    .locals 6

    .prologue
    .line 2513826
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2513827
    iget-object v0, p0, LX/Hry;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hs2;

    iget-object v1, p0, LX/Hry;->k:LX/Hro;

    invoke-virtual {v1}, LX/Hro;->c()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v2, p0, LX/Hry;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Hry;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2513828
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2513829
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_0
    new-instance v4, LX/Hrw;

    invoke-direct {v4, p0, p1}, LX/Hrw;-><init>(LX/Hry;Landroid/widget/ProgressBar;)V

    iget-object v3, p0, LX/Hry;->k:LX/Hro;

    invoke-virtual {v3}, LX/Hro;->d()Z

    move-result v5

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/Hs2;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/Long;Landroid/widget/ListView;LX/Hrw;Z)LX/Hs1;

    move-result-object v0

    iput-object v0, p0, LX/Hry;->j:LX/Hs1;

    .line 2513830
    return-void

    .line 2513831
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static b(LX/Hry;Landroid/view/View;)V
    .locals 11

    .prologue
    .line 2513770
    const v0, 0x7f0d051c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 2513771
    iget-object v1, p0, LX/Hry;->p:LX/0W3;

    sget-wide v2, LX/0X5;->ji:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2513772
    invoke-virtual {v0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2513773
    new-instance v3, LX/1De;

    invoke-direct {v3, v1}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2513774
    iget-object v2, p0, LX/Hry;->n:LX/0dk;

    invoke-virtual {v2}, LX/0dk;->a()Lcom/facebook/java2js/JSContext;

    move-result-object v4

    .line 2513775
    new-instance v5, Lcom/facebook/components/ComponentView;

    invoke-direct {v5, v1}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    .line 2513776
    iget-object v1, p0, LX/Hry;->k:LX/Hro;

    invoke-virtual {v1}, LX/Hro;->c()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    .line 2513777
    iget-object v1, p0, LX/Hry;->k:LX/Hro;

    invoke-virtual {v1}, LX/Hro;->b()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v7

    .line 2513778
    iget-object v1, p0, LX/Hry;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Hry;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2513779
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2513780
    move-object v2, v1

    .line 2513781
    :goto_0
    if-eqz v6, :cond_2

    iget-object v1, v6, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v1}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2513782
    :goto_1
    iget-object v6, p0, LX/Hry;->a:LX/9bY;

    .line 2513783
    iget-object v8, p0, LX/Hry;->o:LX/Hs4;

    const/4 v9, 0x0

    .line 2513784
    new-instance v10, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    invoke-direct {v10, v8}, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;-><init>(LX/Hs4;)V

    .line 2513785
    sget-object p1, LX/Hs4;->a:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/Hs3;

    .line 2513786
    if-nez p1, :cond_0

    .line 2513787
    new-instance p1, LX/Hs3;

    invoke-direct {p1}, LX/Hs3;-><init>()V

    .line 2513788
    :cond_0
    invoke-static {p1, v3, v9, v9, v10}, LX/Hs3;->a$redex0(LX/Hs3;LX/1De;IILcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;)V

    .line 2513789
    move-object v10, p1

    .line 2513790
    move-object v9, v10

    .line 2513791
    move-object v8, v9

    .line 2513792
    iget-object v9, v8, LX/Hs3;->a:Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    iput-object v2, v9, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->b:Ljava/lang/String;

    .line 2513793
    iget-object v9, v8, LX/Hs3;->d:Ljava/util/BitSet;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 2513794
    move-object v2, v8

    .line 2513795
    iget-object v8, v2, LX/Hs3;->a:Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    iput-object v1, v8, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->c:Ljava/lang/String;

    .line 2513796
    iget-object v8, v2, LX/Hs3;->d:Ljava/util/BitSet;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 2513797
    move-object v2, v2

    .line 2513798
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    .line 2513799
    :goto_2
    iget-object v7, v2, LX/Hs3;->a:Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    iput-object v1, v7, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->d:Ljava/lang/String;

    .line 2513800
    iget-object v7, v2, LX/Hs3;->d:Ljava/util/BitSet;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 2513801
    move-object v1, v2

    .line 2513802
    iget-object v2, p0, LX/Hry;->k:LX/Hro;

    invoke-virtual {v2}, LX/Hro;->a()Ljava/lang/String;

    move-result-object v2

    .line 2513803
    iget-object v7, v1, LX/Hs3;->a:Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    iput-object v2, v7, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->e:Ljava/lang/String;

    .line 2513804
    iget-object v7, v1, LX/Hs3;->d:Ljava/util/BitSet;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 2513805
    move-object v1, v1

    .line 2513806
    iget-object v2, p0, LX/Hry;->k:LX/Hro;

    invoke-virtual {v2}, LX/Hro;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2513807
    iget-object v7, v1, LX/Hs3;->a:Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    iput-object v2, v7, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->f:Ljava/lang/Boolean;

    .line 2513808
    iget-object v7, v1, LX/Hs3;->d:Ljava/util/BitSet;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 2513809
    move-object v1, v1

    .line 2513810
    new-instance v2, LX/Hrt;

    invoke-direct {v2, p0, v6}, LX/Hrt;-><init>(LX/Hry;LX/9bY;)V

    .line 2513811
    iget-object v6, v1, LX/Hs3;->a:Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    iput-object v2, v6, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->g:LX/0QK;

    .line 2513812
    iget-object v6, v1, LX/Hs3;->d:Ljava/util/BitSet;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2513813
    move-object v1, v1

    .line 2513814
    iget-object v2, v1, LX/Hs3;->a:Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    iput-object v4, v2, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->h:Lcom/facebook/java2js/JSContext;

    .line 2513815
    iget-object v2, v1, LX/Hs3;->d:Ljava/util/BitSet;

    const/4 v6, 0x6

    invoke-virtual {v2, v6}, Ljava/util/BitSet;->set(I)V

    .line 2513816
    move-object v1, v1

    .line 2513817
    invoke-static {v3, v1}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v1

    invoke-virtual {v1}, LX/1me;->b()LX/1dV;

    move-result-object v1

    .line 2513818
    invoke-virtual {v5, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2513819
    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {v1, v0, v5}, LX/478;->b(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    .line 2513820
    :goto_3
    return-void

    .line 2513821
    :cond_1
    iget-object v1, p0, LX/Hry;->m:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    goto/16 :goto_0

    .line 2513822
    :cond_2
    const-string v1, "unknown"

    goto/16 :goto_1

    .line 2513823
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 2513824
    :cond_4
    const v1, 0x7f0d051d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 2513825
    invoke-static {p0, v1, v0}, LX/Hry;->a(LX/Hry;Landroid/widget/ProgressBar;Landroid/widget/ListView;)V

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2513767
    iget-boolean v0, p0, LX/Hry;->q:Z

    if-nez v0, :cond_0

    .line 2513768
    :goto_0
    return-void

    .line 2513769
    :cond_0
    iget-object v0, p0, LX/Hry;->a:LX/9bY;

    iget-object v1, p0, LX/Hry;->i:LX/Hrx;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method
