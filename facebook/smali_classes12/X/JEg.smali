.class public final LX/JEg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/4pL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JEn;

.field public final synthetic b:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;LX/JEn;)V
    .locals 0

    .prologue
    .line 2666338
    iput-object p1, p0, LX/JEg;->b:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    iput-object p2, p0, LX/JEg;->a:LX/JEn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/4pL;)V
    .locals 6
    .param p1    # LX/4pL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2666339
    if-nez p1, :cond_0

    .line 2666340
    iget-object v0, p0, LX/JEg;->b:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->b(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;Ljava/lang/String;)V

    .line 2666341
    :goto_0
    return-void

    .line 2666342
    :cond_0
    iget-object v0, p1, LX/4pL;->a:Ljava/lang/String;

    move-object v1, v0

    .line 2666343
    iget-object v0, p1, LX/4pL;->b:Ljava/lang/String;

    move-object v2, v0

    .line 2666344
    iget-object v0, p1, LX/4pL;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2666345
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v0, ""

    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2666346
    iget-object v3, p0, LX/JEg;->b:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    iget-object v3, v3, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/0dQ;->s:LX/0Tn;

    invoke-interface {v3, v4, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    sget-object v4, LX/0dQ;->t:LX/0Tn;

    invoke-interface {v3, v4, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    sget-object v4, LX/0dQ;->u:LX/0Tn;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2666347
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2666348
    iget-object v3, p0, LX/JEg;->a:LX/JEn;

    iget-object v4, p0, LX/JEg;->b:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    invoke-virtual {v4, v1, v2}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v3, LX/JEn;->d:Ljava/lang/CharSequence;

    .line 2666349
    iget-object v2, p0, LX/JEg;->a:LX/JEn;

    iput-object v0, v2, LX/JEn;->e:Landroid/net/Uri;

    .line 2666350
    :cond_2
    iget-object v0, p0, LX/JEg;->b:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    invoke-static {v0, v1}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->b(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2666351
    iget-object v0, p0, LX/JEg;->b:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    invoke-virtual {v0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->finish()V

    .line 2666352
    iget-object v0, p0, LX/JEg;->b:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->b(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;Ljava/lang/String;)V

    .line 2666353
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2666354
    check-cast p1, LX/4pL;

    invoke-direct {p0, p1}, LX/JEg;->a(LX/4pL;)V

    return-void
.end method
