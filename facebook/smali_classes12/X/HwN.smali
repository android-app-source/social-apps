.class public final LX/HwN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2520640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520641
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2520642
    iput-object v0, p0, LX/HwN;->a:LX/0Px;

    .line 2520643
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2520644
    iput-object v0, p0, LX/HwN;->c:LX/0P1;

    .line 2520645
    const-string v0, ""

    iput-object v0, p0, LX/HwN;->o:Ljava/lang/String;

    .line 2520646
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;)V
    .locals 1

    .prologue
    .line 2520580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520581
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2520582
    instance-of v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    if-eqz v0, :cond_0

    .line 2520583
    check-cast p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520584
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    iput-object v0, p0, LX/HwN;->a:LX/0Px;

    .line 2520585
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, LX/HwN;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2520586
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    iput-object v0, p0, LX/HwN;->c:LX/0P1;

    .line 2520587
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    iput-boolean v0, p0, LX/HwN;->d:Z

    .line 2520588
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    iput-boolean v0, p0, LX/HwN;->e:Z

    .line 2520589
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    iput-boolean v0, p0, LX/HwN;->f:Z

    .line 2520590
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    iput-boolean v0, p0, LX/HwN;->g:Z

    .line 2520591
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    iput-boolean v0, p0, LX/HwN;->h:Z

    .line 2520592
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    iput-boolean v0, p0, LX/HwN;->i:Z

    .line 2520593
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    iput-boolean v0, p0, LX/HwN;->j:Z

    .line 2520594
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    iput-boolean v0, p0, LX/HwN;->k:Z

    .line 2520595
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    iput-boolean v0, p0, LX/HwN;->l:Z

    .line 2520596
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    iput-boolean v0, p0, LX/HwN;->m:Z

    .line 2520597
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, LX/HwN;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2520598
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    iput-object v0, p0, LX/HwN;->o:Ljava/lang/String;

    .line 2520599
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    iput-boolean v0, p0, LX/HwN;->p:Z

    .line 2520600
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, LX/HwN;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2520601
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iput-object v0, p0, LX/HwN;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2520602
    :goto_0
    return-void

    .line 2520603
    :cond_0
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    move-object v0, v0

    .line 2520604
    iput-object v0, p0, LX/HwN;->a:LX/0Px;

    .line 2520605
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v0

    .line 2520606
    iput-object v0, p0, LX/HwN;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2520607
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    move-object v0, v0

    .line 2520608
    iput-object v0, p0, LX/HwN;->c:LX/0P1;

    .line 2520609
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    move v0, v0

    .line 2520610
    iput-boolean v0, p0, LX/HwN;->d:Z

    .line 2520611
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    move v0, v0

    .line 2520612
    iput-boolean v0, p0, LX/HwN;->e:Z

    .line 2520613
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    move v0, v0

    .line 2520614
    iput-boolean v0, p0, LX/HwN;->f:Z

    .line 2520615
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    move v0, v0

    .line 2520616
    iput-boolean v0, p0, LX/HwN;->g:Z

    .line 2520617
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    move v0, v0

    .line 2520618
    iput-boolean v0, p0, LX/HwN;->h:Z

    .line 2520619
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    move v0, v0

    .line 2520620
    iput-boolean v0, p0, LX/HwN;->i:Z

    .line 2520621
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    move v0, v0

    .line 2520622
    iput-boolean v0, p0, LX/HwN;->j:Z

    .line 2520623
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    move v0, v0

    .line 2520624
    iput-boolean v0, p0, LX/HwN;->k:Z

    .line 2520625
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    move v0, v0

    .line 2520626
    iput-boolean v0, p0, LX/HwN;->l:Z

    .line 2520627
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    move v0, v0

    .line 2520628
    iput-boolean v0, p0, LX/HwN;->m:Z

    .line 2520629
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-object v0, v0

    .line 2520630
    iput-object v0, p0, LX/HwN;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2520631
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    move-object v0, v0

    .line 2520632
    iput-object v0, p0, LX/HwN;->o:Ljava/lang/String;

    .line 2520633
    iget-boolean v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    move v0, v0

    .line 2520634
    iput-boolean v0, p0, LX/HwN;->p:Z

    .line 2520635
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v0, v0

    .line 2520636
    iput-object v0, p0, LX/HwN;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2520637
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    move-object v0, v0

    .line 2520638
    iput-object v0, p0, LX/HwN;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;
    .locals 2

    .prologue
    .line 2520639
    new-instance v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    invoke-direct {v0, p0}, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;-><init>(LX/HwN;)V

    return-object v0
.end method
