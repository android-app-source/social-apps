.class public LX/J5g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/J5g;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0se;

.field public final c:LX/0tG;

.field public final d:LX/0tI;


# direct methods
.method public constructor <init>(LX/0tX;LX/0se;LX/0tG;LX/0tI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2648533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2648534
    iput-object p1, p0, LX/J5g;->a:LX/0tX;

    .line 2648535
    iput-object p2, p0, LX/J5g;->b:LX/0se;

    .line 2648536
    iput-object p3, p0, LX/J5g;->c:LX/0tG;

    .line 2648537
    iput-object p4, p0, LX/J5g;->d:LX/0tI;

    .line 2648538
    return-void
.end method

.method public static a(LX/0QB;)LX/J5g;
    .locals 7

    .prologue
    .line 2648539
    sget-object v0, LX/J5g;->e:LX/J5g;

    if-nez v0, :cond_1

    .line 2648540
    const-class v1, LX/J5g;

    monitor-enter v1

    .line 2648541
    :try_start_0
    sget-object v0, LX/J5g;->e:LX/J5g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2648542
    if-eqz v2, :cond_0

    .line 2648543
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2648544
    new-instance p0, LX/J5g;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v4

    check-cast v4, LX/0se;

    invoke-static {v0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v5

    check-cast v5, LX/0tG;

    invoke-static {v0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v6

    check-cast v6, LX/0tI;

    invoke-direct {p0, v3, v4, v5, v6}, LX/J5g;-><init>(LX/0tX;LX/0se;LX/0tG;LX/0tI;)V

    .line 2648545
    move-object v0, p0

    .line 2648546
    sput-object v0, LX/J5g;->e:LX/J5g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2648547
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2648548
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2648549
    :cond_1
    sget-object v0, LX/J5g;->e:LX/J5g;

    return-object v0

    .line 2648550
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2648551
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2648552
    invoke-static {}, LX/J5A;->d()LX/J56;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2648553
    new-instance v1, LX/J56;

    invoke-direct {v1}, LX/J56;-><init>()V

    const-string v2, "max_num_steps"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "privacy_review_type"

    invoke-virtual {v1, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "review_id"

    invoke-virtual {v1, v2, p5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "item_count"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after"

    invoke-virtual {v1, v2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    .line 2648554
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2648555
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2648556
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    .line 2648557
    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 2648558
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 2648559
    iget-object v1, p0, LX/J5g;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
