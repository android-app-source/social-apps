.class public final LX/Iz3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/p2p/P2pPaymentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/p2p/P2pPaymentFragment;)V
    .locals 0

    .prologue
    .line 2634077
    iput-object p1, p0, LX/Iz3;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Boolean;)V
    .locals 5
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2634078
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2634079
    iget-object v0, p0, LX/Iz3;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->i:LX/Iz1;

    .line 2634080
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2634081
    iget-object v1, v0, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v1, v0, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Iz9;

    .line 2634082
    goto :goto_1

    .line 2634083
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2634084
    :cond_0
    move-object v0, v3

    .line 2634085
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2634086
    iget-object v2, p0, LX/Iz3;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v2, v2, Lcom/facebook/payments/p2p/P2pPaymentFragment;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 2634087
    :cond_1
    iget-object v0, p0, LX/Iz3;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2634088
    iget-object v0, p0, LX/Iz3;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->o:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2634089
    :goto_3
    return-void

    .line 2634090
    :cond_2
    iget-object v0, p0, LX/Iz3;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->l:LX/Iyv;

    invoke-interface {v0}, LX/Iyv;->b()V

    goto :goto_3
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2634091
    iget-object v0, p0, LX/Iz3;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->l:LX/Iyv;

    invoke-interface {v0}, LX/Iyv;->b()V

    .line 2634092
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2634093
    check-cast p1, Ljava/lang/Boolean;

    invoke-direct {p0, p1}, LX/Iz3;->a(Ljava/lang/Boolean;)V

    return-void
.end method
