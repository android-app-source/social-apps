.class public final LX/HyW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hww;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524265
    iput-object p1, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Hx6;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2524266
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    if-ne p1, v0, :cond_1

    .line 2524267
    :cond_0
    :goto_0
    return-void

    .line 2524268
    :cond_1
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-boolean v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->L:Z

    if-eqz v0, :cond_2

    .line 2524269
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    invoke-virtual {v0}, LX/HyO;->b()V

    .line 2524270
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    .line 2524271
    iput-boolean v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->K:Z

    .line 2524272
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    .line 2524273
    iput-boolean v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->L:Z

    .line 2524274
    :cond_2
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->m:LX/HyA;

    invoke-virtual {p1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/HyA;->b(Ljava/lang/String;)V

    .line 2524275
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->l:LX/1nQ;

    invoke-virtual {p1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1nQ;->c(Ljava/lang/String;)V

    .line 2524276
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    .line 2524277
    iput-object p1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    .line 2524278
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    .line 2524279
    iput-boolean v1, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->O:Z

    .line 2524280
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->C:Landroid/view/View;

    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v3, LX/Hx6;->PAST:LX/Hx6;

    if-ne v0, v3, :cond_5

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2524281
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v2, LX/Hx6;->INVITED:LX/Hx6;

    if-ne v0, v2, :cond_3

    .line 2524282
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-virtual {v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->c()V

    .line 2524283
    :cond_3
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    invoke-virtual {v0}, LX/Hyx;->a()V

    .line 2524284
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    .line 2524285
    iput-object p1, v0, LX/Hz2;->c:LX/Hx6;

    .line 2524286
    iget-object v2, v0, LX/Hz2;->g:LX/I2o;

    invoke-interface {v2, p1}, LX/I2k;->a(LX/Hx6;)V

    .line 2524287
    iget-object v2, v0, LX/Hz2;->j:LX/I2Z;

    if-eqz v2, :cond_4

    .line 2524288
    iget-object v2, v0, LX/Hz2;->j:LX/I2Z;

    .line 2524289
    iput-object p1, v2, LX/I2Z;->n:LX/Hx6;

    .line 2524290
    :cond_4
    iget-object v2, v0, LX/Hz2;->g:LX/I2o;

    invoke-interface {v2}, LX/1Pq;->iN_()V

    .line 2524291
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v2, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v0, v2, :cond_7

    .line 2524292
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, p1}, LX/Hz2;->b(LX/Hx6;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2524293
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, p1}, LX/Hz2;->c(LX/Hx6;)V

    .line 2524294
    :goto_2
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->c(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Z)V

    .line 2524295
    :goto_3
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-boolean v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->M:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, p1}, LX/Hz2;->d(LX/Hx6;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2524296
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 2524297
    goto :goto_1

    .line 2524298
    :cond_6
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->H:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/Hz2;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_2

    .line 2524299
    :cond_7
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, p1}, LX/Hz2;->b(LX/Hx6;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2524300
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, p1}, LX/Hz2;->c(LX/Hx6;)V

    .line 2524301
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Z)V

    .line 2524302
    :goto_4
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-virtual {v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b()V

    goto :goto_3

    .line 2524303
    :cond_8
    iget-object v0, p0, LX/HyW;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->u$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    goto :goto_4
.end method
