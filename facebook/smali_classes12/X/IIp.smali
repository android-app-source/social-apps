.class public final LX/IIp;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562118
    iput-object p1, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 2562112
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->I(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562113
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562114
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562115
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->M(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562116
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562117
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 2562110
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const v2, 0x7f080024

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->b(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/String;)V

    .line 2562111
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 2562119
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x0

    .line 2562120
    iput-boolean v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562121
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aD:LX/IIm;

    .line 2562122
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562123
    if-eqz v2, :cond_0

    .line 2562124
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562125
    :goto_0
    return-void

    .line 2562126
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562127
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 2562102
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->c(LX/IG6;)V

    .line 2562103
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aH:LX/IIm;

    .line 2562104
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562105
    if-eqz v2, :cond_0

    .line 2562106
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562107
    :goto_0
    return-void

    .line 2562108
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562109
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 2562094
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->c(LX/IG6;)V

    .line 2562095
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aF:LX/IIm;

    .line 2562096
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562097
    if-eqz v2, :cond_0

    .line 2562098
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562099
    :goto_0
    return-void

    .line 2562100
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562101
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 2562076
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->c(LX/IG6;)V

    .line 2562077
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->l:LX/IG1;

    .line 2562078
    iget-object v1, v0, LX/IG1;->e:LX/0Px;

    move-object v0, v1

    .line 2562079
    if-nez v0, :cond_0

    .line 2562080
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aH:LX/IIm;

    .line 2562081
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562082
    if-eqz v2, :cond_1

    .line 2562083
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562084
    :goto_0
    return-void

    .line 2562085
    :cond_0
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aG:LX/IIm;

    .line 2562086
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562087
    if-eqz v2, :cond_2

    .line 2562088
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562089
    :goto_1
    goto :goto_0

    .line 2562090
    :cond_1
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562091
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0

    .line 2562092
    :cond_2
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562093
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_1
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 2562068
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->c(LX/IG6;)V

    .line 2562069
    iget-object v0, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIp;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aI:LX/IIm;

    .line 2562070
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562071
    if-eqz v2, :cond_0

    .line 2562072
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562073
    :goto_0
    return-void

    .line 2562074
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562075
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method
