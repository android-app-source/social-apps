.class public final enum LX/JTd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JTd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JTd;

.field public static final enum BUFFERING:LX/JTd;

.field public static final enum PAUSED:LX/JTd;

.field public static final enum PLAYING:LX/JTd;

.field public static final enum STOPPED:LX/JTd;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2696907
    new-instance v0, LX/JTd;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v2}, LX/JTd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTd;->PLAYING:LX/JTd;

    .line 2696908
    new-instance v0, LX/JTd;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, LX/JTd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTd;->PAUSED:LX/JTd;

    .line 2696909
    new-instance v0, LX/JTd;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v4}, LX/JTd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTd;->STOPPED:LX/JTd;

    .line 2696910
    new-instance v0, LX/JTd;

    const-string v1, "BUFFERING"

    invoke-direct {v0, v1, v5}, LX/JTd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTd;->BUFFERING:LX/JTd;

    .line 2696911
    const/4 v0, 0x4

    new-array v0, v0, [LX/JTd;

    sget-object v1, LX/JTd;->PLAYING:LX/JTd;

    aput-object v1, v0, v2

    sget-object v1, LX/JTd;->PAUSED:LX/JTd;

    aput-object v1, v0, v3

    sget-object v1, LX/JTd;->STOPPED:LX/JTd;

    aput-object v1, v0, v4

    sget-object v1, LX/JTd;->BUFFERING:LX/JTd;

    aput-object v1, v0, v5

    sput-object v0, LX/JTd;->$VALUES:[LX/JTd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2696912
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JTd;
    .locals 1

    .prologue
    .line 2696913
    const-class v0, LX/JTd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JTd;

    return-object v0
.end method

.method public static values()[LX/JTd;
    .locals 1

    .prologue
    .line 2696914
    sget-object v0, LX/JTd;->$VALUES:[LX/JTd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JTd;

    return-object v0
.end method
