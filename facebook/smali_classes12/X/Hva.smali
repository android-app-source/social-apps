.class public LX/Hva;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HvZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1c9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1c9",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/87Y;

.field public final c:LX/0tO;

.field public d:I

.field public e:LX/Hvf;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/HvV;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(LX/1c9;LX/87Y;LX/0tO;)V
    .locals 1
    .param p1    # LX/1c9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1c9",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;",
            "LX/87Y;",
            "LX/0tO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2519318
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2519319
    iput v0, p0, LX/Hva;->d:I

    .line 2519320
    iput v0, p0, LX/Hva;->g:I

    .line 2519321
    iput v0, p0, LX/Hva;->h:I

    .line 2519322
    iput-object p1, p0, LX/Hva;->a:LX/1c9;

    .line 2519323
    iput-object p2, p0, LX/Hva;->b:LX/87Y;

    .line 2519324
    iput-object p3, p0, LX/Hva;->c:LX/0tO;

    .line 2519325
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2519363
    iget-object v0, p0, LX/Hva;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2519364
    new-instance v0, LX/HvZ;

    new-instance v1, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, LX/HvZ;-><init>(LX/Hva;Landroid/view/View;)V

    .line 2519365
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/HvZ;

    new-instance v1, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, LX/HvZ;-><init>(LX/Hva;Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(ILX/IF4;)V
    .locals 5

    .prologue
    .line 2519340
    iget-object v0, p0, LX/Hva;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2519341
    iget v0, p0, LX/Hva;->d:I

    if-eq v0, p1, :cond_0

    .line 2519342
    iget v0, p0, LX/Hva;->d:I

    iput v0, p0, LX/Hva;->h:I

    .line 2519343
    iput p1, p0, LX/Hva;->d:I

    .line 2519344
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2519345
    iget-object v0, p0, LX/Hva;->f:LX/HvV;

    if-eqz v0, :cond_0

    .line 2519346
    iget-object v1, p0, LX/Hva;->f:LX/HvV;

    iget-object v0, p0, LX/Hva;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    iget-object v0, p0, LX/Hva;->b:LX/87Y;

    invoke-virtual {v0, p1}, LX/87Y;->a(I)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0, p1, p2}, LX/HvV;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;ILX/IF4;)V

    .line 2519347
    :cond_0
    :goto_1
    return-void

    .line 2519348
    :cond_1
    iget v0, p0, LX/Hva;->d:I

    if-ne v0, p1, :cond_5

    .line 2519349
    sget-object v0, LX/IF4;->AUTO_DEFAULT:LX/IF4;

    if-eq p2, v0, :cond_2

    sget-object v0, LX/IF4;->DRAFT_RECOVERY:LX/IF4;

    if-eq p2, v0, :cond_2

    sget-object v0, LX/IF4;->STICKY_STYLES:LX/IF4;

    if-ne p2, v0, :cond_8

    :cond_2
    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2519350
    if-nez v0, :cond_5

    .line 2519351
    :cond_3
    :goto_3
    goto :goto_1

    .line 2519352
    :cond_4
    iget-object v0, p0, LX/Hva;->a:LX/1c9;

    invoke-virtual {v0, p1}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    goto :goto_0

    .line 2519353
    :cond_5
    iget v0, p0, LX/Hva;->d:I

    iput v0, p0, LX/Hva;->h:I

    .line 2519354
    iput p1, p0, LX/Hva;->d:I

    .line 2519355
    iget-object v0, p0, LX/Hva;->e:LX/Hvf;

    if-eqz v0, :cond_6

    .line 2519356
    iget-object v0, p0, LX/Hva;->e:LX/Hvf;

    iget v1, p0, LX/Hva;->h:I

    iget v2, p0, LX/Hva;->d:I

    .line 2519357
    iget-object v3, v0, LX/Hvf;->a:LX/Hvj;

    const/4 v4, 0x1

    invoke-static {v3, v1, v2, v4}, LX/Hvj;->a$redex0(LX/Hvj;IIZ)V

    .line 2519358
    :cond_6
    iget-object v0, p0, LX/Hva;->f:LX/HvV;

    if-eqz v0, :cond_3

    .line 2519359
    iget-object v1, p0, LX/Hva;->f:LX/HvV;

    iget-object v0, p0, LX/Hva;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    iget-object v0, p0, LX/Hva;->b:LX/87Y;

    invoke-virtual {v0, p1}, LX/87Y;->a(I)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0, p1, p2}, LX/HvV;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;ILX/IF4;)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, LX/Hva;->a:LX/1c9;

    invoke-virtual {v0, p1}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(LX/1a1;)V
    .locals 2

    .prologue
    .line 2519360
    check-cast p1, LX/HvZ;

    .line 2519361
    iget-object v0, p1, LX/HvZ;->m:LX/Hvl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/Hvl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2519362
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2519329
    check-cast p1, LX/HvZ;

    .line 2519330
    const/4 p0, 0x1

    .line 2519331
    iget-object v0, p1, LX/HvZ;->l:LX/Hva;

    iget-object v0, v0, LX/Hva;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    if-ne v0, p0, :cond_1

    iget-object v0, p1, LX/HvZ;->l:LX/Hva;

    iget-object v0, v0, LX/Hva;->b:LX/87Y;

    invoke-virtual {v0, p2}, LX/87Y;->a(I)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    .line 2519332
    :goto_0
    iget-object v1, p1, LX/HvZ;->m:LX/Hvl;

    invoke-interface {v1, v0}, LX/Hvl;->setTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V

    .line 2519333
    iget-object v0, p1, LX/HvZ;->l:LX/Hva;

    iget v0, v0, LX/Hva;->d:I

    if-ne p2, v0, :cond_2

    .line 2519334
    iget-object v0, p1, LX/HvZ;->l:LX/Hva;

    iget-object v0, v0, LX/Hva;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    if-ne v0, p0, :cond_0

    .line 2519335
    iget-object v0, p1, LX/HvZ;->m:LX/Hvl;

    invoke-interface {v0, p0}, LX/Hvl;->setSelected(Z)V

    .line 2519336
    :cond_0
    :goto_1
    iget-object v0, p1, LX/HvZ;->m:LX/Hvl;

    new-instance v1, LX/HvY;

    invoke-direct {v1, p1, p2}, LX/HvY;-><init>(LX/HvZ;I)V

    invoke-interface {v0, v1}, LX/Hvl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2519337
    return-void

    .line 2519338
    :cond_1
    iget-object v0, p1, LX/HvZ;->l:LX/Hva;

    iget-object v0, v0, LX/Hva;->a:LX/1c9;

    invoke-virtual {v0, p2}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    goto :goto_0

    .line 2519339
    :cond_2
    iget-object v0, p1, LX/HvZ;->m:LX/Hvl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/Hvl;->setSelected(Z)V

    goto :goto_1
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2519326
    iget-object v0, p0, LX/Hva;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2519327
    iget-object v0, p0, LX/Hva;->b:LX/87Y;

    invoke-virtual {v0}, LX/87Y;->b()I

    move-result v0

    .line 2519328
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Hva;->a:LX/1c9;

    invoke-virtual {v0}, LX/1c9;->size()I

    move-result v0

    goto :goto_0
.end method
