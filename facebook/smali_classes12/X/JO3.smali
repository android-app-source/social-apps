.class public LX/JO3;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JO4;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JO3",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JO4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686845
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2686846
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JO3;->b:LX/0Zi;

    .line 2686847
    iput-object p1, p0, LX/JO3;->a:LX/0Ot;

    .line 2686848
    return-void
.end method

.method public static a(LX/0QB;)LX/JO3;
    .locals 4

    .prologue
    .line 2686822
    const-class v1, LX/JO3;

    monitor-enter v1

    .line 2686823
    :try_start_0
    sget-object v0, LX/JO3;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686824
    sput-object v2, LX/JO3;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686825
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686826
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686827
    new-instance v3, LX/JO3;

    const/16 p0, 0x1f45

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JO3;-><init>(LX/0Ot;)V

    .line 2686828
    move-object v0, v3

    .line 2686829
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686830
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JO3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686831
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686832
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2686833
    check-cast p2, LX/JO2;

    .line 2686834
    iget-object v0, p0, LX/JO3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JO4;

    iget-object v1, p2, LX/JO2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JO2;->b:LX/1Pq;

    const/4 v4, 0x2

    .line 2686835
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x24

    invoke-interface {v3, v4}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v4

    .line 2686836
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2686837
    check-cast v3, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 2686838
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-nez v5, :cond_0

    iget-object v5, v0, LX/JO4;->b:Landroid/content/res/Resources;

    const p2, 0x7f083a7e

    invoke-virtual {v5, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {p0, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b0052

    invoke-virtual {v5, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0a00e1

    invoke-virtual {v5, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b254f

    invoke-virtual {v5, p0}, LX/1ne;->s(I)LX/1ne;

    move-result-object v5

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v5, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v5, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    const/4 p0, 0x0

    const p2, 0x7f0b0060

    invoke-interface {v5, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 p0, 0x2

    const p2, 0x7f0b0060

    invoke-interface {v5, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    move-object v3, v5

    .line 2686839
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2686840
    iget-object v4, v0, LX/JO4;->a:LX/1vb;

    invoke-virtual {v4, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v4

    sget-object v5, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-virtual {v4, v5}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v4

    check-cast v2, LX/1Pk;

    invoke-interface {v2}, LX/1Pk;->e()LX/1SX;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 2686841
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2686842
    return-object v0

    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2686843
    invoke-static {}, LX/1dS;->b()V

    .line 2686844
    const/4 v0, 0x0

    return-object v0
.end method
