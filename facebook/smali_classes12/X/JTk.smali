.class public LX/JTk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1vg;


# direct methods
.method public constructor <init>(LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2697164
    iput-object p1, p0, LX/JTk;->a:LX/1vg;

    .line 2697165
    return-void
.end method

.method public static a(LX/0QB;)LX/JTk;
    .locals 4

    .prologue
    .line 2697166
    const-class v1, LX/JTk;

    monitor-enter v1

    .line 2697167
    :try_start_0
    sget-object v0, LX/JTk;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697168
    sput-object v2, LX/JTk;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697169
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697170
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697171
    new-instance p0, LX/JTk;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-direct {p0, v3}, LX/JTk;-><init>(LX/1vg;)V

    .line 2697172
    move-object v0, p0

    .line 2697173
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697174
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JTk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697175
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697176
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/JTk;LX/1De;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;)LX/1Di;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2697177
    invoke-static {p2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 2697178
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x6

    const/16 v4, 0x8

    invoke-interface {v2, v3, v4}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/JTj;->d(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v3, 0x7f0b004e

    invoke-virtual {v0, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v3, 0x7f0a0428

    invoke-virtual {v0, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v3, 0x7

    const/4 v4, 0x3

    invoke-interface {v0, v3, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    const v3, 0x7f020a8d

    invoke-interface {v0, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    invoke-static {p1}, LX/JTj;->d(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-interface {v0, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/JTj;->d(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    const/16 v7, 0x10

    .line 2697179
    iget-object v3, p0, LX/JTk;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v4

    .line 2697180
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->APPROXIMATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v4, p0, :cond_2

    .line 2697181
    const p0, 0x7f02093c

    .line 2697182
    :goto_1
    move v4, p0

    .line 2697183
    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const v4, 0x7f0a0590

    invoke-virtual {v3, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 2697184
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Di;->j(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x5

    const/4 v7, 0x4

    invoke-interface {v3, v4, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 2697185
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-nez v4, :cond_1

    :goto_2
    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b004e

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0a015d

    invoke-virtual {v1, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2697186
    :cond_2
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v4, p0, :cond_3

    .line 2697187
    const p0, 0x7f020964

    goto :goto_1

    .line 2697188
    :cond_3
    const p0, 0x7f0208d3

    goto :goto_1
.end method
