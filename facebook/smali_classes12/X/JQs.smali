.class public LX/JQs;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mX",
        "<",
        "LX/JQk;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/JQx;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/25M;LX/1Pm;ILjava/lang/String;ZLX/JQx;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/JQk;",
            ">;",
            "LX/25M;",
            "LX/1Pm;",
            "I",
            "Ljava/lang/String;",
            "Z",
            "LX/JQx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692325
    invoke-direct {p0, p1, p2, p4, p3}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2692326
    iput-object p8, p0, LX/JQs;->c:LX/JQx;

    .line 2692327
    iput p5, p0, LX/JQs;->d:I

    .line 2692328
    iput-object p6, p0, LX/JQs;->e:Ljava/lang/String;

    .line 2692329
    iput-boolean p7, p0, LX/JQs;->f:Z

    .line 2692330
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2692331
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2692332
    check-cast p2, LX/JQk;

    .line 2692333
    iget-object v0, p0, LX/JQs;->c:LX/JQx;

    const/4 v1, 0x0

    .line 2692334
    new-instance v2, LX/JQv;

    invoke-direct {v2, v0}, LX/JQv;-><init>(LX/JQx;)V

    .line 2692335
    sget-object v3, LX/JQx;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JQw;

    .line 2692336
    if-nez v3, :cond_0

    .line 2692337
    new-instance v3, LX/JQw;

    invoke-direct {v3}, LX/JQw;-><init>()V

    .line 2692338
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JQw;->a$redex0(LX/JQw;LX/1De;IILX/JQv;)V

    .line 2692339
    move-object v2, v3

    .line 2692340
    move-object v1, v2

    .line 2692341
    move-object v0, v1

    .line 2692342
    iget v1, p0, LX/JQs;->d:I

    .line 2692343
    iget-object v2, v0, LX/JQw;->a:LX/JQv;

    iput v1, v2, LX/JQv;->a:I

    .line 2692344
    iget-object v2, v0, LX/JQw;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2692345
    move-object v0, v0

    .line 2692346
    iget-object v1, v0, LX/JQw;->a:LX/JQv;

    iput-object p2, v1, LX/JQv;->b:LX/JQk;

    .line 2692347
    iget-object v1, v0, LX/JQw;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2692348
    move-object v0, v0

    .line 2692349
    iget-object v1, p0, LX/JQs;->e:Ljava/lang/String;

    .line 2692350
    iget-object v2, v0, LX/JQw;->a:LX/JQv;

    iput-object v1, v2, LX/JQv;->d:Ljava/lang/String;

    .line 2692351
    iget-object v2, v0, LX/JQw;->d:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2692352
    move-object v0, v0

    .line 2692353
    iget-boolean v1, p0, LX/JQs;->f:Z

    .line 2692354
    iget-object v2, v0, LX/JQw;->a:LX/JQv;

    iput-boolean v1, v2, LX/JQv;->c:Z

    .line 2692355
    iget-object v2, v0, LX/JQw;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2692356
    move-object v0, v0

    .line 2692357
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2692358
    const/4 v0, 0x0

    return v0
.end method
