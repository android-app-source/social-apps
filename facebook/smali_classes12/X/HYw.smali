.class public final LX/HYw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/42q;


# instance fields
.field public final synthetic a:Lcom/facebook/registration/controller/RegistrationFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/controller/RegistrationFragmentController;)V
    .locals 0

    .prologue
    .line 2481196
    iput-object p1, p0, LX/HYw;->a:Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2481197
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2481198
    invoke-static {v0}, LX/HYj;->valueOfKey(Ljava/lang/String;)LX/HYj;

    move-result-object v0

    .line 2481199
    iget-object v1, p0, LX/HYw;->a:Lcom/facebook/registration/controller/RegistrationFragmentController;

    iget-object v2, p0, LX/HYw;->a:Lcom/facebook/registration/controller/RegistrationFragmentController;

    iget-object v2, v2, Lcom/facebook/registration/controller/RegistrationFragmentController;->b:LX/HYv;

    invoke-virtual {v2, v0}, LX/HYv;->a(LX/HYj;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/content/Intent;)V

    .line 2481200
    iget-object v1, p0, LX/HYw;->a:Lcom/facebook/registration/controller/RegistrationFragmentController;

    iget-object v1, v1, Lcom/facebook/registration/controller/RegistrationFragmentController;->d:LX/HZt;

    .line 2481201
    if-nez v0, :cond_2

    .line 2481202
    const-string v2, "null"

    .line 2481203
    :goto_0
    invoke-static {v1, v2}, LX/HZt;->i(LX/HZt;Ljava/lang/String;)V

    .line 2481204
    sget-object v2, LX/HYj;->PHONE_ACQUIRED:LX/HYj;

    if-eq v0, v2, :cond_0

    sget-object v2, LX/HYj;->EMAIL_ACQUIRED:LX/HYj;

    if-ne v0, v2, :cond_1

    .line 2481205
    :cond_0
    const-string v2, "contactpoint_acquired"

    invoke-static {v1, v2}, LX/HZt;->i(LX/HZt;Ljava/lang/String;)V

    .line 2481206
    :cond_1
    return-void

    .line 2481207
    :cond_2
    invoke-virtual {v0}, LX/HYj;->name()Ljava/lang/String;

    move-result-object v2

    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, p0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
