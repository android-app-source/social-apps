.class public LX/Hag;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public b:Landroid/content/Context;

.field public c:LX/Hbd;

.field public d:LX/HbJ;

.field public e:LX/Hac;

.field public f:Lcom/facebook/fbui/glyph/GlyphButton;

.field public g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

.field public h:I

.field public i:I

.field public j:LX/HbK;

.field public k:LX/HaZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Lcom/facebook/fbui/glyph/GlyphButton;LX/HbK;LX/HaZ;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/fbui/glyph/GlyphButton;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2484318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2484319
    iput-object p1, p0, LX/Hag;->b:Landroid/content/Context;

    .line 2484320
    iput-object p3, p0, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2484321
    iput-object p4, p0, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2484322
    iput-object p2, p0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    .line 2484323
    iput-object p5, p0, LX/Hag;->j:LX/HbK;

    .line 2484324
    iput-object p6, p0, LX/Hag;->k:LX/HaZ;

    .line 2484325
    return-void
.end method

.method public static a(LX/Hag;I)LX/HbN;
    .locals 1

    .prologue
    .line 2484300
    iget-object v0, p0, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v0, p1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2484301
    if-nez v0, :cond_0

    .line 2484302
    const/4 v0, 0x0

    .line 2484303
    :goto_0
    return-object v0

    .line 2484304
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HbN;

    goto :goto_0
.end method

.method public static h(LX/Hag;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2484310
    iget-object v0, p0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 2484311
    iget-object v0, p0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 2484312
    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2484313
    :cond_2
    iget-object v0, p0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2484314
    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->h(II)Z

    move-result v0

    goto :goto_1

    .line 2484315
    :cond_3
    iget-object v0, p0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2484316
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v1

    goto :goto_2

    :cond_4
    move v1, v2

    .line 2484317
    goto :goto_2
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 2484305
    iget v0, p0, LX/Hag;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2484306
    iget v0, p0, LX/Hag;->h:I

    invoke-static {p0, v0}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v0

    .line 2484307
    instance-of v1, v0, LX/HbY;

    if-eqz v1, :cond_0

    .line 2484308
    check-cast v0, LX/HbY;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/HbY;->b(Z)V

    .line 2484309
    :cond_0
    return-void
.end method
