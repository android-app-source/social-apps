.class public final LX/JGs;
.super Landroid/view/ViewGroup;
.source ""

# interfaces
.implements LX/5qf;
.implements LX/5qg;
.implements LX/5r7;
.implements LX/5rA;
.implements LX/5rB;


# static fields
.field public static b:Landroid/graphics/Paint;

.field public static c:Landroid/graphics/Paint;

.field public static d:Landroid/graphics/Paint;

.field public static e:Landroid/graphics/Paint;

.field public static f:Landroid/graphics/Rect;

.field public static final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/JGs;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Landroid/graphics/Rect;

.field public static final u:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field public i:LX/JGr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:[LX/JGM;

.field private k:[LX/JGQ;

.field public l:[LX/JGu;

.field public m:I

.field private n:Z

.field public o:Z

.field public p:Z

.field public q:Landroid/graphics/drawable/Drawable;

.field public r:LX/5r3;

.field private s:J

.field private t:LX/5qd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/JGS;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2669241
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/JGs;->g:Ljava/util/ArrayList;

    .line 2669242
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/JGs;->h:Landroid/graphics/Rect;

    .line 2669243
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, LX/JGs;->u:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2669244
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 2669245
    sget-object v0, LX/JGM;->b:[LX/JGM;

    iput-object v0, p0, LX/JGs;->j:[LX/JGM;

    .line 2669246
    sget-object v0, LX/JGQ;->a:[LX/JGQ;

    iput-object v0, p0, LX/JGs;->k:[LX/JGQ;

    .line 2669247
    sget-object v0, LX/JGu;->a:[LX/JGu;

    iput-object v0, p0, LX/JGs;->l:[LX/JGu;

    .line 2669248
    iput v1, p0, LX/JGs;->m:I

    .line 2669249
    iput-boolean v1, p0, LX/JGs;->n:Z

    .line 2669250
    iput-boolean v1, p0, LX/JGs;->o:Z

    .line 2669251
    iput-boolean v1, p0, LX/JGs;->p:Z

    .line 2669252
    sget-object v0, LX/5r3;->AUTO:LX/5r3;

    iput-object v0, p0, LX/JGs;->r:LX/5r3;

    .line 2669253
    invoke-virtual {p0, v1}, LX/JGs;->setClipChildren(Z)V

    .line 2669254
    return-void
.end method

.method private static a(F)I
    .locals 1

    .prologue
    .line 2669255
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static a(LX/JGs;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2669256
    invoke-virtual {p0, p1}, LX/JGs;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2669257
    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0}, LX/JGs;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Canvas;FFFFLandroid/graphics/Paint;II)V
    .locals 7

    .prologue
    .line 2669258
    int-to-float v4, p6

    int-to-float v5, p6

    int-to-float v6, p7

    move-object v0, p0

    move-object v1, p5

    move v2, p1

    move v3, p2

    invoke-static/range {v0 .. v6}, LX/JGs;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFFF)V

    .line 2669259
    int-to-float v4, p6

    neg-int v0, p6

    int-to-float v5, v0

    int-to-float v6, p7

    move-object v0, p0

    move-object v1, p5

    move v2, p1

    move v3, p4

    invoke-static/range {v0 .. v6}, LX/JGs;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFFF)V

    .line 2669260
    neg-int v0, p6

    int-to-float v4, v0

    int-to-float v5, p6

    int-to-float v6, p7

    move-object v0, p0

    move-object v1, p5

    move v2, p3

    move v3, p2

    invoke-static/range {v0 .. v6}, LX/JGs;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFFF)V

    .line 2669261
    neg-int v0, p6

    int-to-float v4, v0

    neg-int v0, p6

    int-to-float v5, v0

    int-to-float v6, p7

    move-object v0, p0

    move-object v1, p5

    move v2, p3

    move v3, p4

    invoke-static/range {v0 .. v6}, LX/JGs;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFFF)V

    .line 2669262
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFF)V
    .locals 6

    .prologue
    .line 2669263
    cmpl-float v0, p2, p4

    if-eqz v0, :cond_0

    cmpl-float v0, p3, p5

    if-eqz v0, :cond_0

    .line 2669264
    cmpl-float v0, p2, p4

    if-lez v0, :cond_2

    move v3, p2

    move v1, p4

    .line 2669265
    :goto_0
    cmpl-float v0, p3, p5

    if-lez v0, :cond_1

    move v4, p3

    move v2, p5

    :goto_1
    move-object v0, p0

    move-object v5, p1

    .line 2669266
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2669267
    :cond_0
    return-void

    :cond_1
    move v4, p5

    move v2, p3

    goto :goto_1

    :cond_2
    move v3, p4

    move v1, p2

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFFF)V
    .locals 6

    .prologue
    .line 2669268
    add-float v4, p2, p4

    invoke-static {p5}, LX/JGs;->a(F)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p6

    add-float v5, p3, v0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, LX/JGs;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFF)V

    .line 2669269
    invoke-static {p4}, LX/JGs;->a(F)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p6

    add-float v4, p2, v0

    add-float v5, p3, p5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, LX/JGs;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFF)V

    .line 2669270
    return-void
.end method

.method public static b(LX/JGs;I)I
    .locals 2

    .prologue
    .line 2669271
    invoke-virtual {p0}, LX/JGs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 2669272
    int-to-float v1, p1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private b([LX/JGQ;)V
    .locals 4

    .prologue
    .line 2669273
    array-length v0, p1

    .line 2669274
    if-nez v0, :cond_1

    .line 2669275
    :cond_0
    return-void

    .line 2669276
    :cond_1
    iget-object v0, p0, LX/JGs;->i:LX/JGr;

    if-nez v0, :cond_2

    .line 2669277
    new-instance v0, LX/JGr;

    invoke-direct {v0, p0}, LX/JGr;-><init>(LX/JGs;)V

    iput-object v0, p0, LX/JGs;->i:LX/JGr;

    .line 2669278
    :cond_2
    iget-object v0, p0, LX/JGs;->i:LX/JGr;

    move-object v1, v0

    .line 2669279
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 2669280
    invoke-interface {v3, v1}, LX/JGQ;->a(LX/JGr;)V

    .line 2669281
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private c(FF)LX/JGu;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2669282
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    if-eqz v0, :cond_1

    .line 2669283
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-virtual {v0, p1, p2}, LX/JGS;->b(FF)LX/JGu;

    move-result-object v0

    .line 2669284
    :cond_0
    :goto_0
    return-object v0

    .line 2669285
    :cond_1
    iget-object v0, p0, LX/JGs;->l:[LX/JGu;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    .line 2669286
    iget-object v0, p0, LX/JGs;->l:[LX/JGu;

    aget-object v0, v0, v1

    .line 2669287
    iget-boolean v2, v0, LX/JGu;->d:Z

    if-eqz v2, :cond_2

    .line 2669288
    invoke-virtual {v0, p1, p2}, LX/JGu;->a(FF)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2669289
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 2669290
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c([LX/JGQ;)V
    .locals 3

    .prologue
    .line 2669291
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p0, v0

    .line 2669292
    invoke-interface {v2}, LX/JGQ;->a()V

    .line 2669293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2669294
    :cond_0
    return-void
.end method

.method private static e(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2669360
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2669361
    if-eqz v0, :cond_0

    .line 2669362
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot add view "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to FlatViewGroup while it has a parent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2669363
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 2

    .prologue
    .line 2669295
    iget-object v0, p0, LX/JGs;->r:LX/5r3;

    sget-object v1, LX/5r3;->NONE:LX/5r3;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "TouchTargetHelper should not allow calling this method when pointer events are NONE"

    invoke-static {v0, v1}, LX/5pd;->a(ZLjava/lang/String;)V

    .line 2669296
    iget-object v0, p0, LX/JGs;->r:LX/5r3;

    sget-object v1, LX/5r3;->BOX_ONLY:LX/5r3;

    if-eq v0, v1, :cond_1

    .line 2669297
    invoke-direct {p0, p1, p2}, LX/JGs;->c(FF)LX/JGu;

    move-result-object v0

    .line 2669298
    if-eqz v0, :cond_1

    .line 2669299
    invoke-virtual {v0, p1, p2}, LX/JGu;->b(FF)I

    move-result v0

    .line 2669300
    :goto_1
    return v0

    .line 2669301
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2669302
    :cond_1
    invoke-virtual {p0}, LX/JGs;->getId()I

    move-result v0

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2669356
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    if-nez v0, :cond_1

    .line 2669357
    :cond_0
    :goto_0
    return-void

    .line 2669358
    :cond_1
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-virtual {v0}, LX/JGS;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2669359
    invoke-virtual {p0}, LX/JGs;->invalidate()V

    goto :goto_0
.end method

.method public final a(LX/JGd;[I[I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2669337
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    if-eqz v0, :cond_1

    .line 2669338
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-virtual {v0, p1, p2, p3}, LX/JGS;->a(LX/JGd;[I[I)V

    .line 2669339
    :cond_0
    invoke-virtual {p0}, LX/JGs;->invalidate()V

    .line 2669340
    return-void

    .line 2669341
    :cond_1
    array-length v2, p2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_3

    aget v3, p2, v0

    .line 2669342
    if-lez v3, :cond_2

    .line 2669343
    invoke-virtual {p1, v3}, LX/JGd;->d(I)Landroid/view/View;

    move-result-object v3

    .line 2669344
    invoke-static {v3}, LX/JGs;->e(Landroid/view/View;)V

    .line 2669345
    invoke-virtual {p0, v3}, LX/JGs;->c(Landroid/view/View;)V

    .line 2669346
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2669347
    :cond_2
    neg-int v3, v3

    invoke-virtual {p1, v3}, LX/JGd;->d(I)Landroid/view/View;

    move-result-object v3

    .line 2669348
    invoke-static {v3}, LX/JGs;->e(Landroid/view/View;)V

    .line 2669349
    invoke-virtual {p0, v3}, LX/JGs;->d(Landroid/view/View;)V

    goto :goto_1

    .line 2669350
    :cond_3
    array-length v2, p3

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_0

    aget v3, p3, v0

    .line 2669351
    invoke-virtual {p1, v3}, LX/JGd;->d(I)Landroid/view/View;

    move-result-object v3

    .line 2669352
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 2669353
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to remove view not owned by FlatViewGroup"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2669354
    :cond_4
    invoke-virtual {p0, v3, v1}, LX/JGs;->removeDetachedView(Landroid/view/View;Z)V

    .line 2669355
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public final a(Landroid/graphics/Canvas;IFFFF)V
    .locals 8

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 2669332
    sget-object v0, LX/JGs;->d:Landroid/graphics/Paint;

    sget-object v1, LX/JGs;->d:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    const/high16 v2, -0x1000000

    and-int/2addr v1, v2

    const v2, 0xffffff

    and-int/2addr v2, p2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2669333
    sget-object v0, LX/JGs;->d:Landroid/graphics/Paint;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2669334
    sub-float v3, p5, v4

    sub-float v4, p6, v4

    sget-object v5, LX/JGs;->d:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p3

    move v2, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2669335
    sget-object v5, LX/JGs;->e:Landroid/graphics/Paint;

    const/16 v0, 0x8

    invoke-static {p0, v0}, LX/JGs;->b(LX/JGs;I)I

    move-result v6

    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/JGs;->b(LX/JGs;I)I

    move-result v7

    move-object v0, p1

    move v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    invoke-static/range {v0 .. v7}, LX/JGs;->a(Landroid/graphics/Canvas;FFFFLandroid/graphics/Paint;II)V

    .line 2669336
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 2669328
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    if-nez v0, :cond_0

    .line 2669329
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to get the clipping rect for a non-clipping FlatViewGroup"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2669330
    :cond_0
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-virtual {v0, p1}, LX/JGS;->a(Landroid/graphics/Rect;)V

    .line 2669331
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2669326
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {p0, v0}, LX/JGs;->a(LX/JGs;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, LX/JGs;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 2669327
    return-void
.end method

.method public final a([LX/JGM;Landroid/util/SparseIntArray;[F[FZ)V
    .locals 6

    .prologue
    .line 2669323
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGS;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/JGS;->a([LX/JGM;Landroid/util/SparseIntArray;[F[FZ)V

    .line 2669324
    invoke-virtual {p0}, LX/JGs;->invalidate()V

    .line 2669325
    return-void
.end method

.method public final a([LX/JGQ;)V
    .locals 1

    .prologue
    .line 2669318
    iget-boolean v0, p0, LX/JGs;->n:Z

    if-eqz v0, :cond_0

    .line 2669319
    invoke-direct {p0, p1}, LX/JGs;->b([LX/JGQ;)V

    .line 2669320
    iget-object v0, p0, LX/JGs;->k:[LX/JGQ;

    invoke-static {v0}, LX/JGs;->c([LX/JGQ;)V

    .line 2669321
    :cond_0
    iput-object p1, p0, LX/JGs;->k:[LX/JGQ;

    .line 2669322
    return-void
.end method

.method public final a([LX/JGu;[F[F)V
    .locals 1

    .prologue
    .line 2669315
    iput-object p1, p0, LX/JGs;->l:[LX/JGu;

    .line 2669316
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGS;

    invoke-virtual {v0, p1, p2, p3}, LX/JGS;->a([LX/JGu;[F[F)V

    .line 2669317
    return-void
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2669305
    iget v0, p0, LX/JGs;->m:I

    invoke-virtual {p0, v0}, LX/JGs;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2669306
    instance-of v1, v0, LX/JGs;

    if-eqz v1, :cond_0

    .line 2669307
    invoke-virtual {p0}, LX/JGs;->getDrawingTime()J

    move-result-wide v2

    invoke-super {p0, p1, v0, v2, v3}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 2669308
    :goto_0
    iget v0, p0, LX/JGs;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/JGs;->m:I

    .line 2669309
    return-void

    .line 2669310
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    .line 2669311
    sget-object v1, LX/JGs;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 2669312
    sget-object v1, LX/JGs;->h:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 2669313
    invoke-virtual {p0}, LX/JGs;->getDrawingTime()J

    move-result-wide v2

    invoke-super {p0, p1, v0, v2, v3}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 2669314
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2669303
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/JGs;->removeDetachedView(Landroid/view/View;Z)V

    .line 2669304
    return-void
.end method

.method public final b(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2669230
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {p0, v0}, LX/JGs;->a(LX/JGs;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LX/JGs;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2669231
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 2669232
    invoke-virtual {p0}, LX/JGs;->getRemoveClippedSubviews()Z

    move-result v0

    .line 2669233
    if-ne p1, v0, :cond_0

    .line 2669234
    :goto_0
    return-void

    .line 2669235
    :cond_0
    if-eqz v0, :cond_1

    .line 2669236
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to transition FlatViewGroup from clipping to non-clipping state"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2669237
    :cond_1
    iget-object v0, p0, LX/JGs;->j:[LX/JGM;

    .line 2669238
    new-instance v1, LX/JH5;

    invoke-direct {v1, p0, v0}, LX/JH5;-><init>(LX/JGs;[LX/JGM;)V

    move-object v0, v1

    .line 2669239
    iput-object v0, p0, LX/JGs;->v:LX/JGS;

    .line 2669240
    sget-object v0, LX/JGM;->b:[LX/JGM;

    iput-object v0, p0, LX/JGs;->j:[LX/JGM;

    goto :goto_0
.end method

.method public final b(FF)Z
    .locals 3

    .prologue
    .line 2669106
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    if-eqz v0, :cond_2

    .line 2669107
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-virtual {v0, p1, p2}, LX/JGS;->c(FF)LX/JGu;

    move-result-object v0

    .line 2669108
    :cond_0
    :goto_0
    move-object v0, v0

    .line 2669109
    if-eqz v0, :cond_1

    iget-boolean v0, v0, LX/JGu;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2669110
    :cond_2
    iget-object v0, p0, LX/JGs;->l:[LX/JGu;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_3

    .line 2669111
    iget-object v0, p0, LX/JGs;->l:[LX/JGu;

    aget-object v0, v0, v1

    .line 2669112
    invoke-virtual {v0, p1, p2}, LX/JGu;->a(FF)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2669113
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 2669114
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2669175
    const/4 v0, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {p0, v1}, LX/JGs;->a(LX/JGs;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v0, v1, v2}, LX/JGs;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 2669176
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2669173
    const/4 v0, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {p0, v1}, LX/JGs;->a(LX/JGs;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, LX/JGs;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2669174
    return-void
.end method

.method public final detachAllViewsFromParent()V
    .locals 0

    .prologue
    .line 2669171
    invoke-super {p0}, Landroid/view/ViewGroup;->detachAllViewsFromParent()V

    .line 2669172
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2669126
    iput-boolean v1, p0, LX/JGs;->a:Z

    .line 2669127
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2669128
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    if-eqz v0, :cond_1

    .line 2669129
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-virtual {v0, p1}, LX/JGS;->a(Landroid/graphics/Canvas;)V

    .line 2669130
    :cond_0
    iget v0, p0, LX/JGs;->m:I

    invoke-virtual {p0}, LX/JGs;->getChildCount()I

    move-result v2

    if-eq v0, v2, :cond_2

    .line 2669131
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Did not draw all children: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/JGs;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/JGs;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2669132
    :cond_1
    iget-object v2, p0, LX/JGs;->j:[LX/JGM;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2669133
    invoke-virtual {v4, p0, p1}, LX/JGM;->a(LX/JGs;Landroid/graphics/Canvas;)V

    .line 2669134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2669135
    :cond_2
    iput v1, p0, LX/JGs;->m:I

    .line 2669136
    iget-boolean v0, p0, LX/JGs;->a:Z

    if-eqz v0, :cond_9

    .line 2669137
    const/16 v2, 0xc8

    .line 2669138
    sget-object v0, LX/JGs;->b:Landroid/graphics/Paint;

    if-nez v0, :cond_3

    .line 2669139
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 2669140
    sput-object v0, LX/JGs;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 2669141
    sget-object v0, LX/JGs;->b:Landroid/graphics/Paint;

    const/16 v1, 0x9

    invoke-static {p0, v1}, LX/JGs;->b(LX/JGs;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2669142
    sget-object v0, LX/JGs;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2669143
    sget-object v0, LX/JGs;->b:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2669144
    sget-object v0, LX/JGs;->b:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2669145
    :cond_3
    sget-object v0, LX/JGs;->c:Landroid/graphics/Paint;

    if-nez v0, :cond_4

    .line 2669146
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 2669147
    sput-object v0, LX/JGs;->c:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2669148
    sget-object v0, LX/JGs;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2669149
    sget-object v0, LX/JGs;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2669150
    :cond_4
    sget-object v0, LX/JGs;->d:Landroid/graphics/Paint;

    if-nez v0, :cond_5

    .line 2669151
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 2669152
    sput-object v0, LX/JGs;->d:Landroid/graphics/Paint;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2669153
    sget-object v0, LX/JGs;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2669154
    :cond_5
    sget-object v0, LX/JGs;->e:Landroid/graphics/Paint;

    if-nez v0, :cond_6

    .line 2669155
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 2669156
    sput-object v0, LX/JGs;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2669157
    sget-object v0, LX/JGs;->e:Landroid/graphics/Paint;

    const/16 v1, 0x3f

    const/16 v2, 0x7f

    const/16 v3, 0xff

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2669158
    sget-object v0, LX/JGs;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2669159
    :cond_6
    sget-object v0, LX/JGs;->f:Landroid/graphics/Rect;

    if-nez v0, :cond_7

    .line 2669160
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/JGs;->f:Landroid/graphics/Rect;

    .line 2669161
    :cond_7
    const/4 v1, 0x0

    .line 2669162
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    if-eqz v0, :cond_b

    .line 2669163
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    invoke-virtual {v0, p1}, LX/JGS;->b(Landroid/graphics/Canvas;)V

    .line 2669164
    :cond_8
    iput v1, p0, LX/JGs;->m:I

    .line 2669165
    :cond_9
    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    .line 2669166
    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2669167
    :cond_a
    return-void

    .line 2669168
    :cond_b
    iget-object v2, p0, LX/JGs;->j:[LX/JGM;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_8

    aget-object v4, v2, v0

    .line 2669169
    invoke-virtual {v4, p0, p1}, LX/JGM;->b(LX/JGs;Landroid/graphics/Canvas;)V

    .line 2669170
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final dispatchDrawableHotspotChanged(FF)V
    .locals 1

    .prologue
    .line 2669122
    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2669123
    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 2669124
    invoke-virtual {p0}, LX/JGs;->invalidate()V

    .line 2669125
    :cond_0
    return-void
.end method

.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 1

    .prologue
    .line 2669121
    const/4 v0, 0x0

    return v0
.end method

.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 2669117
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 2669118
    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2669119
    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/JGs;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 2669120
    :cond_0
    return-void
.end method

.method public final getHitSlopRect()Landroid/graphics/Rect;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2669116
    iget-object v0, p0, LX/JGs;->w:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getPointerEvents()LX/5r3;
    .locals 1

    .prologue
    .line 2669115
    iget-object v0, p0, LX/JGs;->r:LX/5r3;

    return-object v0
.end method

.method public final getRemoveClippedSubviews()Z
    .locals 1

    .prologue
    .line 2669105
    iget-object v0, p0, LX/JGs;->v:LX/JGS;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 2669181
    iget-boolean v0, p0, LX/JGs;->p:Z

    return v0
.end method

.method public final invalidate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2669182
    invoke-virtual {p0}, LX/JGs;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, LX/JGs;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v2, v2, v0, v1}, LX/JGs;->invalidate(IIII)V

    .line 2669183
    return-void
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 2669177
    invoke-super {p0}, Landroid/view/ViewGroup;->jumpDrawablesToCurrentState()V

    .line 2669178
    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2669179
    iget-object v0, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 2669180
    :cond_0
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5b190a55

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2669184
    iget-boolean v1, p0, LX/JGs;->n:Z

    if-eqz v1, :cond_0

    .line 2669185
    const/16 v1, 0x2d

    const v2, 0x309eadc0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2669186
    :goto_0
    return-void

    .line 2669187
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/JGs;->n:Z

    .line 2669188
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 2669189
    iget-object v1, p0, LX/JGs;->k:[LX/JGQ;

    invoke-direct {p0, v1}, LX/JGs;->b([LX/JGQ;)V

    .line 2669190
    invoke-virtual {p0}, LX/JGs;->a()V

    .line 2669191
    const v1, -0x1b254eb0

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x19f48b14

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2669192
    iget-boolean v1, p0, LX/JGs;->n:Z

    if-nez v1, :cond_0

    .line 2669193
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Double detach"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2d

    const v3, 0x1cbe2b47

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1

    .line 2669194
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/JGs;->n:Z

    .line 2669195
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 2669196
    iget-object v1, p0, LX/JGs;->k:[LX/JGQ;

    invoke-static {v1}, LX/JGs;->c([LX/JGQ;)V

    .line 2669197
    const v1, -0x3f59c311

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2669198
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    .line 2669199
    iget-wide v4, p0, LX/JGs;->s:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 2669200
    iput-wide v2, p0, LX/JGs;->s:J

    .line 2669201
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/JGs;->b(FF)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2669202
    :cond_0
    :goto_0
    return v0

    .line 2669203
    :cond_1
    iget-object v1, p0, LX/JGs;->t:LX/5qd;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JGs;->t:LX/5qd;

    invoke-interface {v1, p0, p1}, LX/5qd;->a(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2669204
    :cond_2
    iget-object v1, p0, LX/JGs;->r:LX/5r3;

    sget-object v2, LX/5r3;->NONE:LX/5r3;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/JGs;->r:LX/5r3;

    sget-object v2, LX/5r3;->BOX_ONLY:LX/5r3;

    if-eq v1, v2, :cond_0

    .line 2669205
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 2669206
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, 0xb748912

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2669207
    iget-object v1, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 2669208
    iget-object v1, p0, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2, v2, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2669209
    invoke-virtual {p0}, LX/JGs;->invalidate()V

    .line 2669210
    :cond_0
    invoke-virtual {p0}, LX/JGs;->a()V

    .line 2669211
    const/16 v1, 0x2d

    const v2, -0x27bbf34f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x2

    const v2, 0xb526d11

    invoke-static {v5, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2669212
    iget-object v3, p0, LX/JGs;->r:LX/5r3;

    sget-object v4, LX/5r3;->NONE:LX/5r3;

    if-ne v3, v4, :cond_0

    .line 2669213
    const v1, -0x397ea882

    invoke-static {v5, v5, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2669214
    :goto_0
    return v0

    .line 2669215
    :cond_0
    iget-object v3, p0, LX/JGs;->r:LX/5r3;

    sget-object v4, LX/5r3;->BOX_NONE:LX/5r3;

    if-ne v3, v4, :cond_1

    .line 2669216
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v3, v4}, LX/JGs;->c(FF)LX/JGu;

    move-result-object v3

    .line 2669217
    if-nez v3, :cond_1

    .line 2669218
    const v1, -0x469c78df

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 2669219
    :cond_1
    const v0, 0x7e3c5f3f

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0
.end method

.method public final removeAllViewsInLayout()V
    .locals 1

    .prologue
    .line 2669220
    sget-object v0, LX/JGM;->b:[LX/JGM;

    iput-object v0, p0, LX/JGs;->j:[LX/JGM;

    .line 2669221
    invoke-super {p0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 2669222
    return-void
.end method

.method public final requestLayout()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 2669223
    iget-boolean v0, p0, LX/JGs;->o:Z

    if-eqz v0, :cond_0

    .line 2669224
    :goto_0
    return-void

    .line 2669225
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JGs;->o:Z

    .line 2669226
    sget-object v0, LX/JGs;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final setOnInterceptTouchEventListener(LX/5qd;)V
    .locals 0

    .prologue
    .line 2669227
    iput-object p1, p0, LX/JGs;->t:LX/5qd;

    .line 2669228
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 2669229
    const/4 v0, 0x1

    return v0
.end method
