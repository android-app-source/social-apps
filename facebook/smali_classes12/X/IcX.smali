.class public LX/IcX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Ibn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IcJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6aE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/6ax;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/content/Context;

.field public h:Lcom/facebook/maps/FbMapViewDelegate;

.field public i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

.field private j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

.field public k:Landroid/os/Handler;

.field public final l:Ljava/lang/String;

.field private final m:LX/699;

.field public final n:LX/03V;

.field public final o:LX/IcT;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;Landroid/os/Handler;Lcom/facebook/maps/FbMapViewDelegate;Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;Ljava/lang/String;)V
    .locals 1
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/maps/FbMapViewDelegate;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2595589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2595590
    new-instance v0, LX/699;

    invoke-direct {v0}, LX/699;-><init>()V

    iput-object v0, p0, LX/IcX;->m:LX/699;

    .line 2595591
    new-instance v0, LX/IcT;

    invoke-direct {v0, p0}, LX/IcT;-><init>(LX/IcX;)V

    iput-object v0, p0, LX/IcX;->o:LX/IcT;

    .line 2595592
    new-instance v0, LX/IcV;

    invoke-direct {v0, p0}, LX/IcV;-><init>(LX/IcX;)V

    iput-object v0, p0, LX/IcX;->f:LX/0TF;

    .line 2595593
    iput-object p1, p0, LX/IcX;->g:Landroid/content/Context;

    .line 2595594
    iput-object p2, p0, LX/IcX;->n:LX/03V;

    .line 2595595
    iput-object p4, p0, LX/IcX;->h:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2595596
    iput-object p5, p0, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    .line 2595597
    iput-object p6, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    .line 2595598
    iput-object p3, p0, LX/IcX;->k:Landroid/os/Handler;

    .line 2595599
    iput-object p7, p0, LX/IcX;->l:Ljava/lang/String;

    .line 2595600
    return-void
.end method

.method public static synthetic a(LX/IcX;LX/6al;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2595601
    iget-object v7, p0, LX/IcX;->k:Landroid/os/Handler;

    new-instance v0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p5

    move-object v4, p1

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;-><init>(LX/IcX;Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;LX/6al;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;)V

    const v1, -0x258f4d31

    invoke-static {v7, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2595602
    return-void
.end method

.method public static a$redex0(LX/IcX;LX/6al;ILcom/facebook/android/maps/model/LatLng;)LX/6ax;
    .locals 2

    .prologue
    .line 2595603
    iget-object v0, p0, LX/IcX;->m:LX/699;

    invoke-static {p2}, LX/690;->a(I)LX/68w;

    move-result-object v1

    .line 2595604
    iput-object v1, v0, LX/699;->c:LX/68w;

    .line 2595605
    move-object v0, v0

    .line 2595606
    iput-object p3, v0, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2595607
    move-object v0, v0

    .line 2595608
    invoke-virtual {p1, v0}, LX/6al;->a(LX/699;)LX/6ax;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(LX/IcX;LX/6al;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2595609
    const-wide/16 v6, 0x0

    .line 2595610
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v0

    .line 2595611
    invoke-virtual {p1}, LX/6al;->d()Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2595612
    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p1}, LX/6al;->d()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, LX/6al;->d()Landroid/location/Location;

    move-result-object v4

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2595613
    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p5}, LX/IcX;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2595614
    invoke-virtual {v0, p2}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2595615
    :cond_1
    if-eqz p3, :cond_2

    .line 2595616
    invoke-virtual {v0, p3}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2595617
    :cond_2
    if-eqz p4, :cond_4

    invoke-static {p5}, LX/IcX;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-wide v2, p4, Lcom/facebook/android/maps/model/LatLng;->a:D

    cmpl-double v1, v2, v6

    if-nez v1, :cond_3

    iget-wide v2, p4, Lcom/facebook/android/maps/model/LatLng;->b:D

    cmpl-double v1, v2, v6

    if-eqz v1, :cond_4

    .line 2595618
    :cond_3
    invoke-virtual {v0, p4}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2595619
    :cond_4
    invoke-virtual {v0}, LX/696;->a()LX/697;

    move-result-object v0

    iget-object v1, p0, LX/IcX;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b12f4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6aM;)V

    .line 2595620
    return-void
.end method

.method public static b(LX/IcX;Ljava/lang/String;)V
    .locals 3
    .param p0    # LX/IcX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595621
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLRideStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRideStatus;

    move-result-object v0

    .line 2595622
    sget-object v1, LX/IcW;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLRideStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2595623
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->c()V

    .line 2595624
    :goto_0
    return-void

    .line 2595625
    :pswitch_0
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->a()V

    .line 2595626
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    iget-object v1, p0, LX/IcX;->g:Landroid/content/Context;

    const v2, 0x7f082d96

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->setDialogText(Ljava/lang/String;)V

    goto :goto_0

    .line 2595627
    :pswitch_1
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->b()V

    .line 2595628
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    iget-object v1, p0, LX/IcX;->g:Landroid/content/Context;

    const v2, 0x7f082d97

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->setDialogText(Ljava/lang/String;)V

    goto :goto_0

    .line 2595629
    :pswitch_2
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->b()V

    .line 2595630
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    iget-object v1, p0, LX/IcX;->g:Landroid/content/Context;

    const v2, 0x7f082d98

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->setDialogText(Ljava/lang/String;)V

    goto :goto_0

    .line 2595631
    :pswitch_3
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->a()V

    .line 2595632
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    iget-object v1, p0, LX/IcX;->g:Landroid/content/Context;

    const v2, 0x7f082d99

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->setDialogText(Ljava/lang/String;)V

    goto :goto_0

    .line 2595633
    :pswitch_4
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->a()V

    .line 2595634
    iget-object v0, p0, LX/IcX;->j:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    iget-object v1, p0, LX/IcX;->g:Landroid/content/Context;

    const v2, 0x7f082d9a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->setDialogText(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;)Z
    .locals 6

    .prologue
    const-wide v4, 0x3f2a36e2eb1c432dL    # 2.0E-4

    .line 2595635
    iget-wide v0, p0, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpg-double v0, v0, v4

    if-gez v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpg-double v0, v0, v4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595636
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLRideStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRideStatus;

    move-result-object v0

    .line 2595637
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideStatus;->DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLRideStatus;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
