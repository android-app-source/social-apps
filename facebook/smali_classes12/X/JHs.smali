.class public LX/JHs;
.super LX/3LH;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2677190
    invoke-direct {p0}, LX/3LH;-><init>()V

    .line 2677191
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2677192
    iput-object v0, p0, LX/JHs;->b:LX/0Px;

    .line 2677193
    iput-object p1, p0, LX/JHs;->a:Landroid/content/Context;

    .line 2677194
    return-void
.end method

.method private a(I)LX/3OO;
    .locals 1

    .prologue
    .line 2677195
    iget-object v0, p0, LX/JHs;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OO;

    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2677186
    iput-object p1, p0, LX/JHs;->b:LX/0Px;

    .line 2677187
    const v0, -0x327b46c6    # -2.7834144E8f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2677188
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2677189
    iget-object v0, p0, LX/JHs;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2677185
    invoke-direct {p0, p1}, LX/JHs;->a(I)LX/3OO;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2677184
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2677179
    check-cast p2, LX/Jho;

    .line 2677180
    if-nez p2, :cond_0

    .line 2677181
    new-instance p2, LX/Jho;

    iget-object v0, p0, LX/JHs;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jho;-><init>(Landroid/content/Context;)V

    .line 2677182
    :cond_0
    invoke-direct {p0, p1}, LX/JHs;->a(I)LX/3OO;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/Jho;->setContactRow(LX/3OO;)V

    .line 2677183
    return-object p2
.end method
