.class public LX/HvX;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/composer/textstyle/ComposerRichTextController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2519303
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2519304
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/Be2;LX/Hvj;Landroid/graphics/Rect;Landroid/view/ViewGroup$LayoutParams;)Lcom/facebook/composer/textstyle/ComposerRichTextController;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j2;",
            ":",
            "LX/0j9;",
            "DerivedData::",
            "LX/5RE;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSpec$SetsRichTextStyle",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "LX/Be2;",
            "LX/Hvj;",
            "Landroid/graphics/Rect;",
            "Landroid/view/ViewGroup$LayoutParams;",
            ")",
            "Lcom/facebook/composer/textstyle/ComposerRichTextController",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2519305
    new-instance v0, Lcom/facebook/composer/textstyle/ComposerRichTextController;

    move-object v1, p1

    check-cast v1, LX/0il;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p0}, LX/IF5;->b(LX/0QB;)LX/IF5;

    move-result-object v9

    check-cast v9, LX/IF5;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v10

    check-cast v10, LX/1Ad;

    invoke-static {p0}, LX/1qZ;->b(LX/0QB;)LX/1Uo;

    move-result-object v11

    check-cast v11, LX/1Uo;

    invoke-static {p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v12

    check-cast v12, LX/0tO;

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v12}, Lcom/facebook/composer/textstyle/ComposerRichTextController;-><init>(LX/0il;LX/Be2;LX/Hvj;Landroid/graphics/Rect;Landroid/view/ViewGroup$LayoutParams;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/IF5;LX/1Ad;LX/1Uo;LX/0tO;)V

    .line 2519306
    return-object v0
.end method
