.class public LX/J3h;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/J3h;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2643218
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2643219
    sget-object v0, LX/0ax;->dQ:Ljava/lang/String;

    const-string v1, "{source unknown}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2643220
    sget-object v0, LX/0ax;->dR:Ljava/lang/String;

    const-string v1, "{checkup_type}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2643221
    sget-object v0, LX/0ax;->dS:Ljava/lang/String;

    const-string v1, "{checkup_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2643222
    sget-object v0, LX/0ax;->dT:Ljava/lang/String;

    const-string v1, "{checkup_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/fbreact/fragment/ReactActivity;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v2

    const-string v3, "PrivacyCheckupRoute"

    .line 2643223
    iput-object v3, v2, LX/98r;->b:Ljava/lang/String;

    .line 2643224
    move-object v2, v2

    .line 2643225
    const/4 v3, 0x1

    .line 2643226
    iput v3, v2, LX/98r;->h:I

    .line 2643227
    move-object v2, v2

    .line 2643228
    invoke-virtual {v2}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2643229
    sget-object v0, LX/0ax;->dU:Ljava/lang/String;

    const-string v1, "{source unknown}"

    const-string v2, "{checkup_type PROFILE_PHOTO_CHECKUP}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2643230
    return-void
.end method

.method public static a(LX/0QB;)LX/J3h;
    .locals 3

    .prologue
    .line 2643231
    sget-object v0, LX/J3h;->a:LX/J3h;

    if-nez v0, :cond_1

    .line 2643232
    const-class v1, LX/J3h;

    monitor-enter v1

    .line 2643233
    :try_start_0
    sget-object v0, LX/J3h;->a:LX/J3h;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2643234
    if-eqz v2, :cond_0

    .line 2643235
    :try_start_1
    new-instance v0, LX/J3h;

    invoke-direct {v0}, LX/J3h;-><init>()V

    .line 2643236
    move-object v0, v0

    .line 2643237
    sput-object v0, LX/J3h;->a:LX/J3h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2643238
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2643239
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2643240
    :cond_1
    sget-object v0, LX/J3h;->a:LX/J3h;

    return-object v0

    .line 2643241
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2643242
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2643243
    invoke-super {p0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2643244
    if-eqz v0, :cond_0

    sget-object v1, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2643245
    const-string v1, "uri"

    sget-object v2, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2643246
    :cond_0
    return-object v0
.end method
