.class public final LX/JR6;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JR8;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JR7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JR8",
            "<TE;>.MessengerActiveNowUpdateComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JR8;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JR8;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2692753
    iput-object p1, p0, LX/JR6;->b:LX/JR8;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2692754
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JR6;->c:[Ljava/lang/String;

    .line 2692755
    iput v3, p0, LX/JR6;->d:I

    .line 2692756
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JR6;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JR6;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JR6;LX/1De;IILX/JR7;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JR8",
            "<TE;>.MessengerActiveNowUpdateComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2692767
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2692768
    iput-object p4, p0, LX/JR6;->a:LX/JR7;

    .line 2692769
    iget-object v0, p0, LX/JR6;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2692770
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2692771
    invoke-super {p0}, LX/1X5;->a()V

    .line 2692772
    const/4 v0, 0x0

    iput-object v0, p0, LX/JR6;->a:LX/JR7;

    .line 2692773
    iget-object v0, p0, LX/JR6;->b:LX/JR8;

    iget-object v0, v0, LX/JR8;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2692774
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JR8;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2692757
    iget-object v1, p0, LX/JR6;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JR6;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JR6;->d:I

    if-ge v1, v2, :cond_2

    .line 2692758
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2692759
    :goto_0
    iget v2, p0, LX/JR6;->d:I

    if-ge v0, v2, :cond_1

    .line 2692760
    iget-object v2, p0, LX/JR6;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2692761
    iget-object v2, p0, LX/JR6;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2692762
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2692763
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2692764
    :cond_2
    iget-object v0, p0, LX/JR6;->a:LX/JR7;

    .line 2692765
    invoke-virtual {p0}, LX/JR6;->a()V

    .line 2692766
    return-object v0
.end method
