.class public final LX/HRQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;Z)V
    .locals 0

    .prologue
    .line 2465158
    iput-object p1, p0, LX/HRQ;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iput-boolean p2, p0, LX/HRQ;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2465159
    iget-object v0, p0, LX/HRQ;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    .line 2465160
    invoke-static {}, LX/HHu;->a()LX/HHt;

    move-result-object v4

    const-string v5, "page_id"

    iget-wide v6, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/HHt;

    move-object v0, v4

    .line 2465161
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2465162
    iget-boolean v1, p0, LX/HRQ;->a:Z

    if-eqz v1, :cond_1

    .line 2465163
    iget-object v1, p0, LX/HRQ;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->o:LX/16I;

    invoke-virtual {v1}, LX/16I;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HRQ;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->E:Z

    if-eqz v1, :cond_2

    .line 2465164
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 2465165
    :cond_0
    :goto_0
    new-instance v1, LX/HRi;

    iget-object v2, p0, LX/HRQ;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-wide v2, v2, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    invoke-direct {v1, v2, v3}, LX/HRi;-><init>(J)V

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zT;)LX/0zO;

    .line 2465166
    :cond_1
    iget-object v1, p0, LX/HRQ;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->g:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0

    .line 2465167
    :cond_2
    iget-object v1, p0, LX/HRQ;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->E:Z

    if-eqz v1, :cond_0

    .line 2465168
    sget-object v1, LX/0zS;->b:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_0
.end method
