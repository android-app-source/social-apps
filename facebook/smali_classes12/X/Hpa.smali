.class public interface abstract LX/Hpa;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract getFontFoundry()LX/1QO;
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
    .end annotation
.end method

.method public abstract getImageDownloader()LX/1Ad;
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "imageDownloader"
    .end annotation
.end method

.method public abstract getScenePath()Lcom/facebook/common/callercontext/CallerContext;
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "scenePath"
    .end annotation
.end method

.method public abstract getSessionId()Ljava/lang/String;
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "sessionId"
    .end annotation
.end method
