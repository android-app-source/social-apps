.class public final LX/J4m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupMediaOnlyQueryModel;",
        ">;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/J4j;


# direct methods
.method public constructor <init>(LX/J4j;)V
    .locals 0

    .prologue
    .line 2644471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2644472
    iput-object p1, p0, LX/J4m;->a:LX/J4j;

    .line 2644473
    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2644474
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2644475
    if-eqz p1, :cond_0

    .line 2644476
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2644477
    if-nez v0, :cond_1

    .line 2644478
    :cond_0
    :goto_0
    return-object v2

    .line 2644479
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2644480
    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupMediaOnlyQueryModel;

    .line 2644481
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupMediaOnlyQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupMediaOnlyQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2644482
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupMediaOnlyQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    move-result-object v0

    iget-object v1, p0, LX/J4m;->a:LX/J4j;

    invoke-static {v0, v1}, LX/J4p;->b(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;LX/J4j;)V

    goto :goto_0
.end method
