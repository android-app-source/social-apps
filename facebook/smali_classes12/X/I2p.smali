.class public LX/I2p;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/I2o;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530861
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2530862
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1Jg;LX/1PY;Landroid/content/Context;LX/0o8;LX/2ja;LX/2jY;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/I2Y;)LX/I2o;
    .locals 44

    .prologue
    .line 2530863
    new-instance v1, LX/I2o;

    const-class v2, LX/3UA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/3UA;

    invoke-static/range {p0 .. p0}, LX/1Pz;->a(LX/0QB;)LX/1Pz;

    move-result-object v17

    check-cast v17, LX/1Pz;

    invoke-static/range {p0 .. p0}, LX/1Q0;->a(LX/0QB;)LX/1Q0;

    move-result-object v18

    check-cast v18, LX/1Q0;

    const-class v2, LX/1Q1;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/1Q1;

    invoke-static/range {p0 .. p0}, LX/1Q2;->a(LX/0QB;)LX/1Q2;

    move-result-object v20

    check-cast v20, LX/1Q2;

    invoke-static/range {p0 .. p0}, LX/1Q3;->a(LX/0QB;)LX/1Q3;

    move-result-object v21

    check-cast v21, LX/1Q3;

    invoke-static/range {p0 .. p0}, LX/1Q4;->a(LX/0QB;)LX/1Q4;

    move-result-object v22

    check-cast v22, LX/1Q4;

    const-class v2, LX/1Q5;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/1Q5;

    const-class v2, LX/1Q6;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/1Q6;

    invoke-static/range {p0 .. p0}, LX/1Q7;->a(LX/0QB;)LX/1Q7;

    move-result-object v25

    check-cast v25, LX/1Q7;

    const-class v2, LX/1QA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/1QA;

    const-class v2, LX/1QC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/1QC;

    invoke-static/range {p0 .. p0}, LX/1QD;->a(LX/0QB;)LX/1QD;

    move-result-object v28

    check-cast v28, LX/1QD;

    invoke-static/range {p0 .. p0}, LX/1QB;->a(LX/0QB;)LX/1QB;

    move-result-object v29

    check-cast v29, LX/1QB;

    const-class v2, LX/1QE;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v30

    check-cast v30, LX/1QE;

    invoke-static/range {p0 .. p0}, LX/1QF;->a(LX/0QB;)LX/1QF;

    move-result-object v31

    check-cast v31, LX/1QF;

    invoke-static/range {p0 .. p0}, LX/1QG;->a(LX/0QB;)LX/1QG;

    move-result-object v32

    check-cast v32, LX/1QG;

    const-class v2, LX/1QH;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v33

    check-cast v33, LX/1QH;

    const-class v2, LX/1QI;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v34

    check-cast v34, LX/1QI;

    const-class v2, LX/3Tp;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v35

    check-cast v35, LX/3Tp;

    const-class v2, LX/I2x;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v36

    check-cast v36, LX/I2x;

    const-class v2, LX/I2r;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v37

    check-cast v37, LX/I2r;

    invoke-static/range {p0 .. p0}, LX/I2s;->a(LX/0QB;)LX/I2s;

    move-result-object v38

    check-cast v38, LX/I2s;

    invoke-static/range {p0 .. p0}, LX/I2v;->a(LX/0QB;)LX/I2v;

    move-result-object v39

    check-cast v39, LX/I2v;

    const-class v2, LX/I2u;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v40

    check-cast v40, LX/I2u;

    const-class v2, LX/2kq;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v41

    check-cast v41, LX/2kq;

    const-class v2, LX/I2z;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v42

    check-cast v42, LX/I2z;

    invoke-static/range {p0 .. p0}, LX/1QK;->a(LX/0QB;)LX/1QK;

    move-result-object v43

    check-cast v43, LX/1QK;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v1 .. v43}, LX/I2o;-><init>(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1Jg;LX/1PY;Landroid/content/Context;LX/0o8;LX/2ja;LX/2jY;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/I2Y;LX/3UA;LX/1Pz;LX/1Q0;LX/1Q1;LX/1Q2;LX/1Q3;LX/1Q4;LX/1Q5;LX/1Q6;LX/1Q7;LX/1QA;LX/1QC;LX/1QD;LX/1QB;LX/1QE;LX/1QF;LX/1QG;LX/1QH;LX/1QI;LX/3Tp;LX/I2x;LX/I2r;LX/I2s;LX/I2v;LX/I2u;LX/2kq;LX/I2z;LX/1QK;)V

    .line 2530864
    return-object v1
.end method
