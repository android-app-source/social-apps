.class public LX/JMK;
.super LX/5ui;
.source ""


# instance fields
.field private final a:LX/1Ae;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9nR;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field private d:LX/5s9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:[F

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Ae;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2683529
    invoke-direct {p0, p1}, LX/5ui;-><init>(Landroid/content/Context;)V

    .line 2683530
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, LX/JMK;->e:[F

    .line 2683531
    iput-boolean v1, p0, LX/JMK;->f:Z

    .line 2683532
    iput-object p2, p0, LX/JMK;->a:LX/1Ae;

    .line 2683533
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/JMK;->b:Ljava/util/List;

    .line 2683534
    new-instance v0, LX/JMI;

    invoke-direct {v0, p0}, LX/JMI;-><init>(LX/JMK;)V

    invoke-virtual {p0, v0}, LX/5ui;->setTapListener(Landroid/view/GestureDetector$SimpleOnGestureListener;)V

    .line 2683535
    return-void
.end method

.method private static a(Landroid/net/Uri;)LX/1bf;
    .locals 2

    .prologue
    .line 2683525
    invoke-static {p0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    const/4 v1, 0x1

    .line 2683526
    iput-boolean v1, v0, LX/1bX;->g:Z

    .line 2683527
    move-object v0, v0

    .line 2683528
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public static getEventDispatcher(LX/JMK;)LX/5s9;
    .locals 2

    .prologue
    .line 2683520
    iget-object v0, p0, LX/JMK;->d:LX/5s9;

    if-nez v0, :cond_0

    .line 2683521
    invoke-virtual {p0}, LX/JMK;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2683522
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2683523
    iput-object v0, p0, LX/JMK;->d:LX/5s9;

    .line 2683524
    :cond_0
    iget-object v0, p0, LX/JMK;->d:LX/5s9;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)V
    .locals 6

    .prologue
    .line 2683536
    invoke-super {p0, p1}, LX/5ui;->a(Landroid/graphics/Matrix;)V

    .line 2683537
    iget-boolean v0, p0, LX/JMK;->f:Z

    if-eqz v0, :cond_0

    .line 2683538
    iget-object v0, p0, LX/JMK;->e:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 2683539
    invoke-virtual {p0}, LX/JMK;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    .line 2683540
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2683541
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2683542
    invoke-virtual {p0}, LX/JMK;->getId()I

    move-result v1

    iget-object v2, p0, LX/JMK;->e:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, LX/JMK;->e:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, LX/JMK;->e:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    invoke-static {v1, v2, v3, v4}, LX/JML;->a(IFFF)LX/JML;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2683543
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 2683497
    iget-boolean v0, p0, LX/JMK;->c:Z

    if-nez v0, :cond_1

    .line 2683498
    :cond_0
    :goto_0
    return-void

    .line 2683499
    :cond_1
    invoke-virtual {p0}, LX/JMK;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/JMK;->getHeight()I

    move-result v1

    iget-object v3, p0, LX/JMK;->b:Ljava/util/List;

    invoke-static {v0, v1, v3}, LX/K0R;->a(IILjava/util/List;)LX/K0Q;

    move-result-object v0

    .line 2683500
    iget-object v1, v0, LX/K0Q;->a:LX/9nR;

    move-object v3, v1

    .line 2683501
    iget-object v1, v0, LX/K0Q;->b:LX/9nR;

    move-object v1, v1

    .line 2683502
    if-eqz v3, :cond_0

    .line 2683503
    invoke-virtual {p0}, LX/JMK;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/JMK;->getHeight()I

    move-result v4

    iget-object v5, p0, LX/JMK;->b:Ljava/util/List;

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v0, v4, v5, v6, v7}, LX/K0R;->a(IILjava/util/List;D)LX/K0Q;

    move-result-object v0

    .line 2683504
    iget-object v4, v0, LX/K0Q;->a:LX/9nR;

    move-object v0, v4

    .line 2683505
    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9nR;

    .line 2683506
    invoke-virtual {v3}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, LX/JMK;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v4

    .line 2683507
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/JMK;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    .line 2683508
    :goto_1
    iget-object v5, p0, LX/JMK;->a:LX/1Ae;

    invoke-virtual {v5, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    new-instance v5, LX/JMJ;

    .line 2683509
    iget-object v6, v3, LX/9nR;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2683510
    invoke-direct {v5, p0, v6}, LX/JMJ;-><init>(LX/JMK;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ae;->h()LX/1bp;

    move-result-object v1

    .line 2683511
    iget-object v5, v0, LX/9nR;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2683512
    iget-object v6, v3, LX/9nR;->b:Ljava/lang/String;

    move-object v3, v6

    .line 2683513
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2683514
    :goto_2
    invoke-virtual {p0, v1, v2}, LX/5ui;->a(LX/1aZ;LX/1aZ;)V

    .line 2683515
    iput-boolean v8, p0, LX/JMK;->c:Z

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 2683516
    goto :goto_1

    .line 2683517
    :cond_3
    iget-object v2, p0, LX/JMK;->a:LX/1Ae;

    invoke-virtual {v0}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, LX/JMK;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v2

    new-instance v3, LX/JMJ;

    .line 2683518
    iget-object v4, v0, LX/9nR;->b:Ljava/lang/String;

    move-object v0, v4

    .line 2683519
    invoke-direct {v3, p0, v0}, LX/JMJ;-><init>(LX/JMK;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ae;->h()LX/1bp;

    move-result-object v2

    goto :goto_2
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const/16 v2, 0x2c

    const v3, 0x2fba9ede

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2683491
    invoke-super {p0, p1, p2, p3, p4}, LX/5ui;->onSizeChanged(IIII)V

    .line 2683492
    if-lez p1, :cond_1

    if-lez p2, :cond_1

    .line 2683493
    iget-boolean v2, p0, LX/JMK;->c:Z

    if-nez v2, :cond_0

    iget-object v2, p0, LX/JMK;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v0, :cond_2

    :cond_0
    :goto_0
    iput-boolean v0, p0, LX/JMK;->c:Z

    .line 2683494
    invoke-virtual {p0}, LX/JMK;->c()V

    .line 2683495
    :cond_1
    const v0, -0x264d2bf9

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 2683496
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, -0x44dd4ba6

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2683484
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    .line 2683485
    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-eq v2, v4, :cond_1

    :cond_0
    if-nez v1, :cond_2

    iget-object v2, p0, LX/JMK;->e:[F

    aget v2, v2, v6

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 2683486
    :cond_1
    invoke-static {p0, p1}, LX/5sB;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2683487
    iput-boolean v5, p0, LX/JMK;->f:Z

    .line 2683488
    :cond_2
    if-ne v1, v5, :cond_3

    .line 2683489
    iput-boolean v6, p0, LX/JMK;->f:Z

    .line 2683490
    :cond_3
    invoke-super {p0, p1}, LX/5ui;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x1d3240c7

    invoke-static {v4, v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
