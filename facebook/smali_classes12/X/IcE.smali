.class public final enum LX/IcE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IcE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IcE;

.field public static final enum GET_RIDE_PROMO_SHARE:LX/IcE;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2595332
    new-instance v0, LX/IcE;

    const-string v1, "GET_RIDE_PROMO_SHARE"

    invoke-direct {v0, v1, v2}, LX/IcE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IcE;->GET_RIDE_PROMO_SHARE:LX/IcE;

    .line 2595333
    const/4 v0, 0x1

    new-array v0, v0, [LX/IcE;

    sget-object v1, LX/IcE;->GET_RIDE_PROMO_SHARE:LX/IcE;

    aput-object v1, v0, v2

    sput-object v0, LX/IcE;->$VALUES:[LX/IcE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2595336
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IcE;
    .locals 1

    .prologue
    .line 2595335
    const-class v0, LX/IcE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IcE;

    return-object v0
.end method

.method public static values()[LX/IcE;
    .locals 1

    .prologue
    .line 2595334
    sget-object v0, LX/IcE;->$VALUES:[LX/IcE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IcE;

    return-object v0
.end method
