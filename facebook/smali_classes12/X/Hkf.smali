.class public final enum LX/Hkf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hkf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/Hkf;

.field public static final enum b:LX/Hkf;

.field public static final enum c:LX/Hkf;

.field public static final enum d:LX/Hkf;

.field public static final enum e:LX/Hkf;

.field private static final synthetic g:[LX/Hkf;


# instance fields
.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LX/Hkf;

    const-string v1, "OPEN_STORE"

    invoke-direct {v0, v1, v2, v2}, LX/Hkf;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hkf;->a:LX/Hkf;

    new-instance v0, LX/Hkf;

    const-string v1, "OPEN_LINK"

    invoke-direct {v0, v1, v3, v3}, LX/Hkf;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hkf;->b:LX/Hkf;

    new-instance v0, LX/Hkf;

    const-string v1, "XOUT"

    invoke-direct {v0, v1, v4, v4}, LX/Hkf;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hkf;->c:LX/Hkf;

    new-instance v0, LX/Hkf;

    const-string v1, "OPEN_URL"

    invoke-direct {v0, v1, v5, v5}, LX/Hkf;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hkf;->d:LX/Hkf;

    new-instance v0, LX/Hkf;

    const-string v1, "SHOW_INTERSTITIAL"

    invoke-direct {v0, v1, v6, v6}, LX/Hkf;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hkf;->e:LX/Hkf;

    const/4 v0, 0x5

    new-array v0, v0, [LX/Hkf;

    sget-object v1, LX/Hkf;->a:LX/Hkf;

    aput-object v1, v0, v2

    sget-object v1, LX/Hkf;->b:LX/Hkf;

    aput-object v1, v0, v3

    sget-object v1, LX/Hkf;->c:LX/Hkf;

    aput-object v1, v0, v4

    sget-object v1, LX/Hkf;->d:LX/Hkf;

    aput-object v1, v0, v5

    sget-object v1, LX/Hkf;->e:LX/Hkf;

    aput-object v1, v0, v6

    sput-object v0, LX/Hkf;->g:[LX/Hkf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/Hkf;->f:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hkf;
    .locals 1

    const-class v0, LX/Hkf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hkf;

    return-object v0
.end method

.method public static values()[LX/Hkf;
    .locals 1

    sget-object v0, LX/Hkf;->g:[LX/Hkf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hkf;

    return-object v0
.end method
