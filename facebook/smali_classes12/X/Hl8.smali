.class public LX/Hl8;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/HlB;

.field private b:LX/HlD;

.field public c:Landroid/view/View;

.field public d:Landroid/net/Uri;

.field private e:Landroid/content/Context;


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, LX/Hl8;->a:LX/HlB;

    invoke-interface {v0}, LX/HlB;->start()V

    return-void
.end method

.method public getCurrentPosition()I
    .locals 1

    iget-object v0, p0, LX/Hl8;->a:LX/HlB;

    invoke-interface {v0}, LX/HlB;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getPlaceholderView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, LX/Hl8;->c:Landroid/view/View;

    return-object v0
.end method

.method public getVideoImplType()LX/HlD;
    .locals 1

    iget-object v0, p0, LX/Hl8;->b:LX/HlD;

    return-object v0
.end method

.method public setFrameVideoViewListener(LX/Hl9;)V
    .locals 1

    iget-object v0, p0, LX/Hl8;->a:LX/HlB;

    invoke-interface {v0, p1}, LX/HlB;->setFrameVideoViewListener(LX/Hl9;)V

    return-void
.end method

.method public setVideoImpl(LX/HlD;)V
    .locals 3

    invoke-virtual {p0}, LX/Hl8;->removeAllViews()V

    sget-object v0, LX/HlD;->a:LX/HlD;

    if-ne p1, v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    sget-object p1, LX/HlD;->b:LX/HlD;

    :cond_0
    iput-object p1, p0, LX/Hl8;->b:LX/HlD;

    sget-object v0, LX/Hl7;->a:[I

    invoke-virtual {p1}, LX/HlD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, LX/Hl8;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/Hl8;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, LX/Hl8;->a()V

    return-void

    :pswitch_0
    new-instance v0, LX/HlC;

    iget-object v1, p0, LX/Hl8;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/HlC;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, LX/Hl8;->c:Landroid/view/View;

    iget-object v2, p0, LX/Hl8;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/HlC;->a(Landroid/view/View;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, LX/Hl8;->addView(Landroid/view/View;)V

    iput-object v0, p0, LX/Hl8;->a:LX/HlB;

    goto :goto_0

    :pswitch_1
    new-instance v0, LX/HlE;

    iget-object v1, p0, LX/Hl8;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/HlE;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, LX/Hl8;->c:Landroid/view/View;

    iget-object v2, p0, LX/Hl8;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/HlE;->a(Landroid/view/View;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, LX/Hl8;->addView(Landroid/view/View;)V

    iput-object v0, p0, LX/Hl8;->a:LX/HlB;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
