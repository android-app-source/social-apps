.class public LX/Huz;
.super LX/Hs6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsSlideshowVideoSupported;",
        ":",
        "LX/5RE;",
        "Services::",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/Hs6;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final b:LX/Hr2;

.field private final c:LX/0ad;

.field private final d:Landroid/content/res/Resources;

.field private final e:LX/Hu0;


# direct methods
.method public constructor <init>(LX/0ik;LX/Hr2;LX/0ad;Landroid/content/res/Resources;)V
    .locals 3
    .param p1    # LX/0ik;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Hr2;",
            "LX/0ad;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2518180
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2518181
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Huz;->a:Ljava/lang/ref/WeakReference;

    .line 2518182
    iput-object p2, p0, LX/Huz;->b:LX/Hr2;

    .line 2518183
    iput-object p3, p0, LX/Huz;->c:LX/0ad;

    .line 2518184
    iput-object p4, p0, LX/Huz;->d:Landroid/content/res/Resources;

    .line 2518185
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v0

    const v1, 0x7f020874

    .line 2518186
    iput v1, v0, LX/Htz;->a:I

    .line 2518187
    move-object v0, v0

    .line 2518188
    const v1, 0x7f0a097b

    .line 2518189
    iput v1, v0, LX/Htz;->f:I

    .line 2518190
    move-object v0, v0

    .line 2518191
    iget-object v1, p0, LX/Huz;->d:Landroid/content/res/Resources;

    const v2, 0x7f083984

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2518192
    iput-object v1, v0, LX/Htz;->b:Ljava/lang/String;

    .line 2518193
    move-object v0, v0

    .line 2518194
    invoke-virtual {p0}, LX/Huz;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2518195
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2518196
    move-object v0, v0

    .line 2518197
    iget-object v1, p0, LX/Huz;->b:LX/Hr2;

    .line 2518198
    iput-object v1, v0, LX/Htz;->e:LX/Hr2;

    .line 2518199
    move-object v0, v0

    .line 2518200
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/Huz;->e:LX/Hu0;

    .line 2518201
    return-void
.end method


# virtual methods
.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2518202
    iget-object v0, p0, LX/Huz;->e:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2518177
    iget-object v0, p0, LX/Huz;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ik;

    .line 2518178
    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2zG;

    check-cast v1, LX/5RE;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    .line 2518179
    sget-object v3, LX/5RF;->SLIDESHOW:LX/5RF;

    if-eq v1, v3, :cond_0

    sget-object v3, LX/5RF;->CHECKIN:LX/5RF;

    if-eq v1, v3, :cond_0

    sget-object v3, LX/5RF;->MINUTIAE:LX/5RF;

    if-eq v1, v3, :cond_0

    sget-object v3, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    if-ne v1, v3, :cond_1

    :cond_0
    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Huz;->c:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/1EB;->P:S

    invoke-interface {v0, v1, v3, v2}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2518175
    iget-object v0, p0, LX/Huz;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ik;

    .line 2518176
    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v1, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2518174
    sget-object v0, LX/Hty;->SLIDESHOW:LX/Hty;

    return-object v0
.end method
