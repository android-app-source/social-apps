.class public LX/HMf;
.super Lcom/facebook/fig/listitem/FigListItem;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2457492
    invoke-direct {p0, p1}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 2457493
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2457481
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2457482
    invoke-virtual {p0}, LX/HMf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0ecc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2457483
    invoke-virtual {p0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleTextAppearenceType(I)V

    .line 2457484
    invoke-virtual {p0, p2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2457485
    invoke-virtual {p0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setMetaTextAppearenceType(I)V

    .line 2457486
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2457487
    invoke-virtual {p0, p3}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2457488
    :goto_0
    const v0, 0x7f0213a3

    invoke-virtual {p0, v0}, LX/HMf;->setBackgroundResource(I)V

    .line 2457489
    invoke-virtual {p0, p4}, LX/HMf;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2457490
    return-void

    .line 2457491
    :cond_0
    const v0, 0x7f0817e5

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(I)V

    goto :goto_0
.end method
