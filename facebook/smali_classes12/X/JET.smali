.class public LX/JET;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public b:Lcom/facebook/video/insight/view/VideoInsightInsightPartView;

.field public c:Lcom/facebook/video/insight/view/VideoInsightInfoView;

.field public d:Lcom/facebook/video/insight/view/VideoInsightVideoPartView;

.field public e:Lcom/facebook/fbui/glyph/GlyphView;

.field public f:Lcom/facebook/common/callercontext/CallerContext;

.field public g:LX/11S;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2666042
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JET;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2666043
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2666044
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/JET;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2666045
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2666033
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2666034
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2666035
    const v1, 0x7f0315a7

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2666036
    const v0, 0x7f0d30d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/JET;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2666037
    const v0, 0x7f0d30d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;

    iput-object v0, p0, LX/JET;->b:Lcom/facebook/video/insight/view/VideoInsightInsightPartView;

    .line 2666038
    const v0, 0x7f0d30d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;

    iput-object v0, p0, LX/JET;->d:Lcom/facebook/video/insight/view/VideoInsightVideoPartView;

    .line 2666039
    const v0, 0x7f0d30d6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/view/VideoInsightInfoView;

    iput-object v0, p0, LX/JET;->c:Lcom/facebook/video/insight/view/VideoInsightInfoView;

    .line 2666040
    const v0, 0x7f0d30d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/JET;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2666041
    return-void
.end method

.method public static a(LX/JET;Z)V
    .locals 2

    .prologue
    .line 2666025
    if-eqz p1, :cond_0

    .line 2666026
    iget-object v0, p0, LX/JET;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2666027
    :goto_0
    if-eqz p1, :cond_1

    const/16 v0, 0x8

    .line 2666028
    :goto_1
    iget-object v1, p0, LX/JET;->b:Lcom/facebook/video/insight/view/VideoInsightInsightPartView;

    invoke-virtual {v1, v0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->setVisibility(I)V

    .line 2666029
    iget-object v1, p0, LX/JET;->d:Lcom/facebook/video/insight/view/VideoInsightVideoPartView;

    invoke-virtual {v1, v0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->setVisibility(I)V

    .line 2666030
    return-void

    .line 2666031
    :cond_0
    iget-object v0, p0, LX/JET;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0

    .line 2666032
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
