.class public LX/J1x;
.super LX/6E7;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/ui/PrimaryCtaButtonView;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2639597
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 2639598
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/J1x;->a:Ljava/util/List;

    .line 2639599
    const v0, 0x7f031195

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2639600
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/J1x;->setOrientation(I)V

    .line 2639601
    const v0, 0x7f0d2948

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/J1x;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2639602
    return-void
.end method


# virtual methods
.method public final a(LX/J2r;LX/J29;)V
    .locals 7

    .prologue
    .line 2639608
    iget-object v0, p1, LX/J2r;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p1, LX/J2r;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2639609
    iget-object v0, p0, LX/J1x;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setVisibility(I)V

    .line 2639610
    :goto_0
    const/4 v3, 0x0

    .line 2639611
    iget-object v0, p0, LX/J1x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    .line 2639612
    invoke-virtual {p0, v0}, LX/J1x;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 2639613
    :cond_0
    iget-object v0, p0, LX/J1x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2639614
    iget-object v0, p1, LX/J2r;->c:LX/J2e;

    move-object v0, v0

    .line 2639615
    if-nez v0, :cond_5

    .line 2639616
    :cond_1
    return-void

    .line 2639617
    :cond_2
    iget-object v0, p0, LX/J1x;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setVisibility(I)V

    .line 2639618
    iget-object v0, p1, LX/J2r;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 2639619
    iget-object v0, p0, LX/J1x;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->b()V

    .line 2639620
    :goto_2
    iget-object v0, p1, LX/J2r;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 2639621
    iget-object v0, p0, LX/J1x;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->c()V

    goto :goto_0

    .line 2639622
    :cond_3
    iget-object v0, p0, LX/J1x;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v1, p1, LX/J2r;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2639623
    :cond_4
    iget-object v0, p0, LX/J1x;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v1, p1, LX/J2r;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2639624
    :cond_5
    iget-object v0, p1, LX/J2r;->c:LX/J2e;

    move-object v0, v0

    .line 2639625
    iget-object v4, v0, LX/J2e;->c:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J2n;

    .line 2639626
    invoke-virtual {p0}, LX/J1x;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v6, 0x7f031194

    invoke-virtual {v1, v6, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    .line 2639627
    iget-object v6, v0, LX/J2n;->b:Ljava/lang/String;

    invoke-virtual {v1, v6}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    .line 2639628
    invoke-virtual {v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b()V

    .line 2639629
    invoke-virtual {v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->f()V

    .line 2639630
    new-instance v6, LX/J1w;

    invoke-direct {v6, p0, p2, v0}, LX/J1w;-><init>(LX/J1x;LX/J29;LX/J2n;)V

    invoke-virtual {v1, v6}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2639631
    iget-object v0, p0, LX/J1x;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2639632
    invoke-virtual {p0, v1}, LX/J1x;->addView(Landroid/view/View;)V

    .line 2639633
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3
.end method

.method public final c(I)Landroid/view/View;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 2639603
    iget-object v0, p0, LX/J1x;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2639604
    const v0, 0x7f0d2949

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2639605
    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2639606
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/J1x;->c:Landroid/view/View;

    .line 2639607
    :cond_0
    iget-object v0, p0, LX/J1x;->c:Landroid/view/View;

    return-object v0
.end method
