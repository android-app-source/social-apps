.class public final LX/JS2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:LX/162;

.field public final synthetic h:Landroid/net/Uri;

.field public final synthetic i:Ljava/lang/String;

.field public final synthetic j:LX/JS5;


# direct methods
.method public constructor <init>(LX/JS5;ZLandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2694627
    iput-object p1, p0, LX/JS2;->j:LX/JS5;

    iput-boolean p2, p0, LX/JS2;->a:Z

    iput-object p3, p0, LX/JS2;->b:Landroid/content/Context;

    iput-object p4, p0, LX/JS2;->c:Ljava/lang/String;

    iput-object p5, p0, LX/JS2;->d:Ljava/lang/String;

    iput-object p6, p0, LX/JS2;->e:Ljava/lang/String;

    iput-object p7, p0, LX/JS2;->f:Ljava/lang/String;

    iput-object p8, p0, LX/JS2;->g:LX/162;

    iput-object p9, p0, LX/JS2;->h:Landroid/net/Uri;

    iput-object p10, p0, LX/JS2;->i:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 2694602
    iget-boolean v0, p0, LX/JS2;->a:Z

    if-eqz v0, :cond_1

    .line 2694603
    iget-object v0, p0, LX/JS2;->j:LX/JS5;

    iget-object v1, p0, LX/JS2;->b:Landroid/content/Context;

    iget-object v2, p0, LX/JS2;->c:Ljava/lang/String;

    iget-object v3, p0, LX/JS2;->d:Ljava/lang/String;

    iget-object v4, p0, LX/JS2;->e:Ljava/lang/String;

    iget-object v5, p0, LX/JS2;->f:Ljava/lang/String;

    iget-object v6, p0, LX/JS2;->g:LX/162;

    .line 2694604
    iget-object v7, v0, LX/JS5;->e:LX/17d;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v7, v1, p0}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v7

    .line 2694605
    if-nez v7, :cond_4

    .line 2694606
    :cond_0
    :goto_0
    return-void

    .line 2694607
    :cond_1
    iget-object v0, p0, LX/JS2;->h:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 2694608
    iget-object v0, p0, LX/JS2;->j:LX/JS5;

    iget-object v0, v0, LX/JS5;->e:LX/17d;

    iget-object v1, p0, LX/JS2;->b:Landroid/content/Context;

    iget-object v2, p0, LX/JS2;->h:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/content/pm/PackageManager;)Landroid/content/Intent;

    move-result-object v0

    .line 2694609
    if-eqz v0, :cond_0

    .line 2694610
    iget-object v1, p0, LX/JS2;->j:LX/JS5;

    iget-object v1, v1, LX/JS5;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/JS2;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2694611
    :cond_2
    iget-object v0, p0, LX/JS2;->j:LX/JS5;

    iget-object v1, p0, LX/JS2;->b:Landroid/content/Context;

    iget-object v2, p0, LX/JS2;->i:Ljava/lang/String;

    iget-object v3, p0, LX/JS2;->d:Ljava/lang/String;

    iget-object v4, p0, LX/JS2;->e:Ljava/lang/String;

    iget-object v5, p0, LX/JS2;->g:LX/162;

    iget-object v6, p0, LX/JS2;->f:Ljava/lang/String;

    .line 2694612
    const/4 v10, 0x0

    .line 2694613
    iget-object v7, v0, LX/JS5;->d:LX/17T;

    .line 2694614
    sget-object v8, LX/17T;->a:Landroid/net/Uri;

    const-string v9, "foo"

    invoke-static {v8, v9}, LX/17T;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 2694615
    invoke-static {v7, v8}, LX/17T;->a(LX/17T;Landroid/content/Intent;)Landroid/content/pm/ActivityInfo;

    move-result-object v8

    if-eqz v8, :cond_6

    const/4 v8, 0x1

    :goto_1
    move v7, v8

    .line 2694616
    if-eqz v7, :cond_5

    .line 2694617
    iget-object v7, v0, LX/JS5;->b:LX/JRz;

    .line 2694618
    iget-object v8, v7, LX/JRz;->a:LX/0Zb;

    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v10, "music_preview_install_native"

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v10, "og_song_id"

    invoke-virtual {v9, v10, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "og_object_id"

    invoke-virtual {v9, v10, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "tracking_codes"

    invoke-virtual {v9, v10, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "provider_name"

    invoke-virtual {v9, v10, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-interface {v8, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2694619
    iget-object v7, v0, LX/JS5;->d:LX/17T;

    invoke-virtual {v7, v1, v2}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2694620
    :cond_3
    :goto_2
    goto :goto_0

    .line 2694621
    :cond_4
    iget-object p0, v0, LX/JS5;->b:LX/JRz;

    .line 2694622
    iget-object p1, p0, LX/JRz;->a:LX/0Zb;

    new-instance p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "music_preview_open_native"

    invoke-direct {p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "og_song_id"

    invoke-virtual {p2, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    const-string v2, "og_object_id"

    invoke-virtual {p2, v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    const-string v2, "tracking_codes"

    invoke-virtual {p2, v2, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    const-string v2, "provider_name"

    invoke-virtual {p2, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p1, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2694623
    iget-object p0, v0, LX/JS5;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v7, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2694624
    :cond_5
    iget-object v7, v0, LX/JS5;->e:LX/17d;

    const-string v8, "https://play.google.com/store/apps/details?id=%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v2, v9, v11

    invoke-static {v8, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    move-object v8, v1

    move-object v11, v10

    move-object v12, v10

    invoke-virtual/range {v7 .. v12}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/content/pm/PackageManager;)Landroid/content/Intent;

    move-result-object v7

    .line 2694625
    if-eqz v7, :cond_3

    .line 2694626
    iget-object v8, v0, LX/JS5;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v8, v7, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2

    :cond_6
    const/4 v8, 0x0

    goto :goto_1
.end method
