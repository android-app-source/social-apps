.class public LX/I08;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/1Ck;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2526565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2526566
    iput-object p1, p0, LX/I08;->a:LX/0tX;

    .line 2526567
    iput-object p2, p0, LX/I08;->b:LX/1Ck;

    .line 2526568
    iput-object p3, p0, LX/I08;->c:Landroid/content/res/Resources;

    .line 2526569
    return-void
.end method

.method public static a(LX/0QB;)LX/I08;
    .locals 1

    .prologue
    .line 2526570
    invoke-static {p0}, LX/I08;->b(LX/0QB;)LX/I08;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/I08;
    .locals 4

    .prologue
    .line 2526571
    new-instance v3, LX/I08;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1, v2}, LX/I08;-><init>(LX/0tX;LX/1Ck;Landroid/content/res/Resources;)V

    .line 2526572
    return-object v3
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/String;Ljava/util/GregorianCalendar;LX/Hxk;)V
    .locals 6

    .prologue
    .line 2526573
    iget-object v0, p0, LX/I08;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b1552

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2526574
    new-instance v0, LX/I06;

    move-object v1, p0

    move v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/I06;-><init>(LX/I08;IILjava/lang/String;Ljava/util/GregorianCalendar;)V

    .line 2526575
    new-instance v1, LX/I07;

    invoke-direct {v1, p0, p5, p2}, LX/I07;-><init>(LX/I08;LX/Hxk;Ljava/lang/String;)V

    .line 2526576
    iget-object v2, p0, LX/I08;->b:LX/1Ck;

    const-string v3, "fetch_birthdays_task"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2526577
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2526578
    iget-object v0, p0, LX/I08;->b:LX/1Ck;

    const-string v1, "fetch_birthdays_task"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
