.class public final LX/II3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/0gc;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

.field public final synthetic d:LX/IIB;


# direct methods
.method public constructor <init>(LX/IIB;LX/0gc;JLcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;)V
    .locals 1

    .prologue
    .line 2561372
    iput-object p1, p0, LX/II3;->d:LX/IIB;

    iput-object p2, p0, LX/II3;->a:LX/0gc;

    iput-wide p3, p0, LX/II3;->b:J

    iput-object p5, p0, LX/II3;->c:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 2561373
    iget-object v0, p0, LX/II3;->d:LX/IIB;

    iget-object v0, v0, LX/IIB;->f:LX/IID;

    .line 2561374
    const-string v1, "friends_nearby_dashboard_selfview_report_name"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2561375
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561376
    new-instance v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyDashboardActionButtonHandler$FeedbackConfirmDialogFragment;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyDashboardActionButtonHandler$FeedbackConfirmDialogFragment;-><init>()V

    iget-object v1, p0, LX/II3;->a:LX/0gc;

    const-string v2, "feedback_confirm_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2561377
    iget-object v0, p0, LX/II3;->d:LX/IIB;

    iget-wide v2, p0, LX/II3;->b:J

    iget-object v1, p0, LX/II3;->c:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    invoke-static {v0, v2, v3, v1}, LX/IIB;->a$redex0(LX/IIB;JLcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;)V

    .line 2561378
    const/4 v0, 0x1

    return v0
.end method
