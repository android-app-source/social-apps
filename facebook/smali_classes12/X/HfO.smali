.class public LX/HfO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HfM;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HfR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2491774
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/HfO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HfR;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2491771
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2491772
    iput-object p1, p0, LX/HfO;->b:LX/0Ot;

    .line 2491773
    return-void
.end method

.method public static a(LX/0QB;)LX/HfO;
    .locals 4

    .prologue
    .line 2491760
    const-class v1, LX/HfO;

    monitor-enter v1

    .line 2491761
    :try_start_0
    sget-object v0, LX/HfO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2491762
    sput-object v2, LX/HfO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2491763
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2491764
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2491765
    new-instance v3, LX/HfO;

    const/16 p0, 0x38ca

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HfO;-><init>(LX/0Ot;)V

    .line 2491766
    move-object v0, v3

    .line 2491767
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2491768
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HfO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2491769
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2491770
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2491759
    const v0, 0x5e62dab3

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2491739
    invoke-static {}, LX/1dS;->b()V

    .line 2491740
    iget v0, p1, LX/1dQ;->b:I

    .line 2491741
    packed-switch v0, :pswitch_data_0

    .line 2491742
    :goto_0
    return-object v2

    .line 2491743
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2491744
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2491745
    check-cast v1, LX/HfN;

    .line 2491746
    iget-object v3, p0, LX/HfO;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HfR;

    iget v4, v1, LX/HfN;->a:I

    iget-object v5, v1, LX/HfN;->b:Ljava/lang/String;

    iget-object v6, v1, LX/HfN;->c:LX/2kW;

    .line 2491747
    iget-object p1, v3, LX/HfR;->b:Lcom/facebook/work/groupstab/SuggestededGroupBlacklister;

    new-instance p2, LX/HfQ;

    invoke-direct {p2, v3, v6, v5}, LX/HfQ;-><init>(LX/HfR;LX/2kW;Ljava/lang/String;)V

    .line 2491748
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2491749
    const-string v1, "blacklistGroupsYouShouldJoinParamsKey"

    invoke-virtual {p0, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2491750
    iget-object v1, p1, Lcom/facebook/work/groupstab/SuggestededGroupBlacklister;->b:LX/1Ck;

    sget-object v0, LX/HfC;->BLACKLIST_SUGGESTED_GROUP:LX/HfC;

    new-instance v6, LX/HfB;

    invoke-direct {v6, p1, p0}, LX/HfB;-><init>(Lcom/facebook/work/groupstab/SuggestededGroupBlacklister;Landroid/os/Bundle;)V

    invoke-virtual {v1, v0, v6, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2491751
    iget-object p1, v3, LX/HfR;->d:LX/HgN;

    .line 2491752
    iget-object p2, p1, LX/HgN;->a:LX/0if;

    sget-object p0, LX/0ig;->af:LX/0ih;

    const-string v1, "suggestion_blacklisted"

    const/4 v3, 0x0

    .line 2491753
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    .line 2491754
    const-string p1, "position"

    invoke-virtual {v0, p1, v4}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    .line 2491755
    const-string p1, "group_id"

    invoke-virtual {v0, p1, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2491756
    move-object v0, v0

    .line 2491757
    invoke-virtual {p2, p0, v1, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2491758
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5e62dab3
        :pswitch_0
    .end packed-switch
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2491731
    iget-object v0, p0, LX/HfO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HfR;

    .line 2491732
    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v1

    .line 2491733
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v1}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x3

    invoke-interface {v1, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x2

    invoke-interface {v1, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x1

    invoke-interface {v1, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/16 p0, 0x8

    const p3, 0x7f0b2361

    invoke-interface {v1, p0, p3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x0

    const p3, 0x7f0e0bb7

    invoke-static {p1, p0, p3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p0

    const p3, 0x7f08371d

    invoke-virtual {p0, p3}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/4 p3, 0x6

    const p4, 0x7f0b2363

    invoke-interface {p0, p3, p4}, LX/1Di;->g(II)LX/1Di;

    move-result-object p0

    const/4 p3, 0x7

    const p4, 0x7f0b2362

    invoke-interface {p0, p3, p4}, LX/1Di;->g(II)LX/1Di;

    move-result-object p0

    const p3, 0x7f0203e3

    invoke-interface {p0, p3}, LX/1Di;->x(I)LX/1Di;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2491734
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    iget-object p3, v0, LX/HfR;->a:LX/1vg;

    invoke-virtual {p3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p3

    const p4, 0x7f02081c

    invoke-virtual {p3, p4}, LX/2xv;->h(I)LX/2xv;

    move-result-object p3

    const p4, 0x7f0a00d5

    invoke-virtual {p3, p4}, LX/2xv;->j(I)LX/2xv;

    move-result-object p3

    invoke-virtual {p3}, LX/1n6;->b()LX/1dc;

    move-result-object p3

    invoke-virtual {p0, p3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 2491735
    const p3, 0x5e62dab3

    const/4 p4, 0x0

    invoke-static {p1, p3, p4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p3

    move-object p3, p3

    .line 2491736
    invoke-interface {p0, p3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    const p3, 0x7f0203e3

    invoke-interface {p0, p3}, LX/1Di;->x(I)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object p0, p0

    .line 2491737
    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2491738
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2491730
    const/4 v0, 0x1

    return v0
.end method
