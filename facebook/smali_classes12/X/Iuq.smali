.class public LX/Iuq;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Iuq;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2627385
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2627386
    sget-object v0, LX/0ax;->el:Ljava/lang/String;

    const-class v1, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2627387
    return-void
.end method

.method public static a(LX/0QB;)LX/Iuq;
    .locals 3

    .prologue
    .line 2627388
    sget-object v0, LX/Iuq;->a:LX/Iuq;

    if-nez v0, :cond_1

    .line 2627389
    const-class v1, LX/Iuq;

    monitor-enter v1

    .line 2627390
    :try_start_0
    sget-object v0, LX/Iuq;->a:LX/Iuq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2627391
    if-eqz v2, :cond_0

    .line 2627392
    :try_start_1
    new-instance v0, LX/Iuq;

    invoke-direct {v0}, LX/Iuq;-><init>()V

    .line 2627393
    move-object v0, v0

    .line 2627394
    sput-object v0, LX/Iuq;->a:LX/Iuq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2627395
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2627396
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2627397
    :cond_1
    sget-object v0, LX/Iuq;->a:LX/Iuq;

    return-object v0

    .line 2627398
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2627399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
