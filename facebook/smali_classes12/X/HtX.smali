.class public final LX/HtX;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/header/ComposerHeaderViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/header/ComposerHeaderViewController;)V
    .locals 0

    .prologue
    .line 2516298
    iput-object p1, p0, LX/HtX;->a:Lcom/facebook/composer/header/ComposerHeaderViewController;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2516292
    iget-object v0, p0, LX/HtX;->a:Lcom/facebook/composer/header/ComposerHeaderViewController;

    iget-object v0, v0, Lcom/facebook/composer/header/ComposerHeaderViewController;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hr0;

    invoke-interface {v0}, LX/Hr0;->a()V

    .line 2516293
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2516294
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2516295
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2516296
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2516297
    return-void
.end method
