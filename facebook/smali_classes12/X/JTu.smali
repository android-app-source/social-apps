.class public final LX/JTu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2eJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2eJ",
        "<",
        "LX/JUH;",
        "TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;LX/0Px;)V
    .locals 0

    .prologue
    .line 2697420
    iput-object p1, p0, LX/JTu;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    iput-object p2, p0, LX/JTu;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    iput-object p3, p0, LX/JTu;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2697409
    iget-object v0, p0, LX/JTu;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/1RC;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1RC",
            "<",
            "LX/JUH;",
            "*-TE;*>;"
        }
    .end annotation

    .prologue
    .line 2697419
    iget-object v0, p0, LX/JTu;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->PULSE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/JTu;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->g:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/JTu;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->f:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2697418
    new-instance v1, LX/JUH;

    iget-object v2, p0, LX/JTu;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    iget-object v0, p0, LX/JTu;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-direct {v1, v2, v0}, LX/JUH;-><init>(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;)V

    return-object v1
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 2697410
    iget-object v0, p0, LX/JTu;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->e:LX/1LV;

    iget-object v1, p0, LX/JTu;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 2697411
    iget-object v0, p0, LX/JTu;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    iget-object v1, p0, LX/JTu;->b:LX/0Px;

    invoke-static {v0, v1, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 2697412
    iget-object v0, p0, LX/JTu;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->h:LX/JUO;

    iget-object v0, p0, LX/JTu;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v2, p0, LX/JTu;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 2697413
    sget-object v3, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SCROLL:LX/6VK;

    invoke-static {v1, v3, v0, v2}, LX/JUO;->a(LX/JUO;LX/6VK;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2697414
    invoke-virtual {p0}, LX/JTu;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt p1, v0, :cond_0

    .line 2697415
    iget-object v0, p0, LX/JTu;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->h:LX/JUO;

    iget-object v0, p0, LX/JTu;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v2, p0, LX/JTu;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 2697416
    sget-object v3, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SCROLL_NEAR_END:LX/6VK;

    invoke-static {v1, v3, v0, v2}, LX/JUO;->a(LX/JUO;LX/6VK;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2697417
    :cond_0
    return-void
.end method
