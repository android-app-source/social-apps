.class public final LX/HaP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/growth/model/DeviceOwnerData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/HaQ;


# direct methods
.method public constructor <init>(LX/HaQ;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2483755
    iput-object p1, p0, LX/HaP;->b:LX/HaQ;

    iput-object p2, p0, LX/HaP;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2483756
    iget-object v0, p0, LX/HaP;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0xf5cb2ad

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2483757
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2483758
    check-cast p1, Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2483759
    if-eqz p1, :cond_0

    .line 2483760
    iget-object v0, p0, LX/HaP;->b:LX/HaQ;

    iget-object v0, v0, LX/HaQ;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0, p1}, Lcom/facebook/registration/model/SimpleRegFormData;->a(Lcom/facebook/growth/model/DeviceOwnerData;)V

    .line 2483761
    :cond_0
    iget-object v0, p0, LX/HaP;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x1a638104

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2483762
    return-void
.end method
