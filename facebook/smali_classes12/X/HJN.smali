.class public final LX/HJN;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HJO;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/HL6;

.field public b:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public d:LX/HLF;

.field public final synthetic e:LX/HJO;


# direct methods
.method public constructor <init>(LX/HJO;)V
    .locals 1

    .prologue
    .line 2452590
    iput-object p1, p0, LX/HJN;->e:LX/HJO;

    .line 2452591
    move-object v0, p1

    .line 2452592
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2452593
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2452594
    const-string v0, "PageFriendsCityActivityFacepileComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2452595
    if-ne p0, p1, :cond_1

    .line 2452596
    :cond_0
    :goto_0
    return v0

    .line 2452597
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2452598
    goto :goto_0

    .line 2452599
    :cond_3
    check-cast p1, LX/HJN;

    .line 2452600
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2452601
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2452602
    if-eq v2, v3, :cond_0

    .line 2452603
    iget-object v2, p0, LX/HJN;->a:LX/HL6;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HJN;->a:LX/HL6;

    iget-object v3, p1, LX/HJN;->a:LX/HL6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2452604
    goto :goto_0

    .line 2452605
    :cond_5
    iget-object v2, p1, LX/HJN;->a:LX/HL6;

    if-nez v2, :cond_4

    .line 2452606
    :cond_6
    iget-object v2, p0, LX/HJN;->b:LX/2km;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HJN;->b:LX/2km;

    iget-object v3, p1, LX/HJN;->b:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2452607
    goto :goto_0

    .line 2452608
    :cond_8
    iget-object v2, p1, LX/HJN;->b:LX/2km;

    if-nez v2, :cond_7

    .line 2452609
    :cond_9
    iget-object v2, p0, LX/HJN;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/HJN;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HJN;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2452610
    goto :goto_0

    .line 2452611
    :cond_b
    iget-object v2, p1, LX/HJN;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_a

    .line 2452612
    :cond_c
    iget-object v2, p0, LX/HJN;->d:LX/HLF;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/HJN;->d:LX/HLF;

    iget-object v3, p1, LX/HJN;->d:LX/HLF;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2452613
    goto :goto_0

    .line 2452614
    :cond_d
    iget-object v2, p1, LX/HJN;->d:LX/HLF;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
