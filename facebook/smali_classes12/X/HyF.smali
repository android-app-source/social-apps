.class public final LX/HyF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V
    .locals 0

    .prologue
    .line 2523748
    iput-object p1, p0, LX/HyF;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 3

    .prologue
    .line 2523770
    iget-object v0, p0, LX/HyF;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a:LX/I7w;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    const/4 v2, 0x1

    invoke-virtual {v0, p2, v1, v2}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;Z)V

    .line 2523771
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 6

    .prologue
    .line 2523749
    iget-object v0, p0, LX/HyF;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a:LX/I7w;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    const/4 v3, 0x0

    .line 2523750
    iget-object v4, v0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    .line 2523751
    new-instance v2, LX/7vC;

    iget-object v5, v0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    invoke-direct {v2, v5}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    .line 2523752
    iput-boolean v3, v2, LX/7vC;->H:Z

    .line 2523753
    move-object v2, v2

    .line 2523754
    iput-object p2, v2, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2523755
    move-object v2, v2

    .line 2523756
    invoke-static {p2}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v5

    .line 2523757
    iput-object v5, v2, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2523758
    move-object v2, v2

    .line 2523759
    invoke-virtual {v2}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v5

    .line 2523760
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v2, :cond_1

    const/4 v2, 0x1

    .line 2523761
    :goto_0
    if-eqz v2, :cond_0

    .line 2523762
    iget-object p0, v0, LX/I7w;->g:LX/Bl6;

    new-instance p1, LX/BlE;

    invoke-direct {p1, v5, v3}, LX/BlE;-><init>(Lcom/facebook/events/model/Event;Z)V

    invoke-virtual {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2523763
    :cond_0
    iget-object v3, v0, LX/I7w;->h:Landroid/content/ContentResolver;

    iget-object p0, v0, LX/I7w;->i:LX/Bky;

    iget-object p1, v0, LX/I7w;->j:LX/0TD;

    invoke-static {v3, p0, v5, p1}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V

    .line 2523764
    iget-object v3, v0, LX/I7w;->q:LX/7vZ;

    iget-object p0, v0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    .line 2523765
    iget-object p1, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object p0, p1

    .line 2523766
    iget-object p1, v0, LX/I7w;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v3, p0, p2, p1, v1}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2523767
    iget-object p0, v0, LX/I7w;->b:LX/1Ck;

    new-instance p1, LX/I7t;

    invoke-direct {p1, v0, v2, v5, v4}, LX/I7t;-><init>(LX/I7w;ZLcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;)V

    invoke-virtual {p0, v0, v3, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2523768
    return-void

    :cond_1
    move v2, v3

    .line 2523769
    goto :goto_0
.end method
