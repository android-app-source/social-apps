.class public LX/JJS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2679519
    iput-object p1, p0, LX/JJS;->a:LX/0tX;

    .line 2679520
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2679521
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2679522
    new-instance v1, LX/4If;

    invoke-direct {v1}, LX/4If;-><init>()V

    .line 2679523
    const-string v2, "canonical_name"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679524
    move-object v1, v1

    .line 2679525
    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2679526
    const-string p1, "service"

    invoke-virtual {v1, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2679527
    move-object v1, v1

    .line 2679528
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2679529
    new-instance v1, LX/4Ii;

    invoke-direct {v1}, LX/4Ii;-><init>()V

    .line 2679530
    const-string v2, "external_links"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2679531
    new-instance v0, LX/5yD;

    invoke-direct {v0}, LX/5yD;-><init>()V

    move-object v0, v0

    .line 2679532
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2679533
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2679534
    iget-object v1, p0, LX/JJS;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
