.class public final enum LX/Iyy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iyy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iyy;

.field public static final enum CANCELLED:LX/Iyy;

.field public static final enum FAILURE:LX/Iyy;

.field public static final enum SUCCESS:LX/Iyy;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2634057
    new-instance v0, LX/Iyy;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/Iyy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iyy;->SUCCESS:LX/Iyy;

    .line 2634058
    new-instance v0, LX/Iyy;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v3}, LX/Iyy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iyy;->FAILURE:LX/Iyy;

    .line 2634059
    new-instance v0, LX/Iyy;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v4}, LX/Iyy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iyy;->CANCELLED:LX/Iyy;

    .line 2634060
    const/4 v0, 0x3

    new-array v0, v0, [LX/Iyy;

    sget-object v1, LX/Iyy;->SUCCESS:LX/Iyy;

    aput-object v1, v0, v2

    sget-object v1, LX/Iyy;->FAILURE:LX/Iyy;

    aput-object v1, v0, v3

    sget-object v1, LX/Iyy;->CANCELLED:LX/Iyy;

    aput-object v1, v0, v4

    sput-object v0, LX/Iyy;->$VALUES:[LX/Iyy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2634056
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iyy;
    .locals 1

    .prologue
    .line 2634054
    const-class v0, LX/Iyy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iyy;

    return-object v0
.end method

.method public static values()[LX/Iyy;
    .locals 1

    .prologue
    .line 2634055
    sget-object v0, LX/Iyy;->$VALUES:[LX/Iyy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iyy;

    return-object v0
.end method
