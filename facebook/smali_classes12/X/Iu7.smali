.class public final LX/Iu7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2625360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625361
    iput-object p1, p0, LX/Iu7;->a:Ljava/lang/String;

    .line 2625362
    iput-object p2, p0, LX/Iu7;->b:Landroid/net/Uri;

    .line 2625363
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2625365
    if-ne p1, p0, :cond_1

    .line 2625366
    :cond_0
    :goto_0
    return v0

    .line 2625367
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2625368
    goto :goto_0

    .line 2625369
    :cond_3
    check-cast p1, LX/Iu7;

    .line 2625370
    iget-object v2, p0, LX/Iu7;->a:Ljava/lang/String;

    iget-object v3, p1, LX/Iu7;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Iu7;->b:Landroid/net/Uri;

    iget-object v3, p1, LX/Iu7;->b:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2625364
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/Iu7;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/Iu7;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
