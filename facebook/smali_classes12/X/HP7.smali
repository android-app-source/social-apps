.class public final LX/HP7;
.super LX/CgD;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V
    .locals 0

    .prologue
    .line 2461309
    iput-object p1, p0, LX/HP7;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-direct {p0}, LX/CgD;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2461294
    check-cast p1, LX/CgC;

    .line 2461295
    iget-object v0, p0, LX/HP7;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    const-class v1, LX/HQv;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HQv;

    .line 2461296
    iget-object v1, p0, LX/HP7;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    const-class v2, LX/HQy;

    invoke-virtual {v1, v2}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HQy;

    .line 2461297
    iget-object v2, p1, LX/CgC;->c:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v2, v2

    .line 2461298
    if-eqz v0, :cond_1

    invoke-interface {v0, v2}, LX/HQv;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2461299
    invoke-interface {v0, v2}, LX/HQv;->b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2461300
    :cond_0
    :goto_0
    return-void

    .line 2461301
    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1, v2}, LX/HQy;->c(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2461302
    invoke-interface {v1, v2}, LX/HQy;->d(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    goto :goto_0

    .line 2461303
    :cond_2
    iget-object v0, p1, LX/CgC;->d:LX/Cfl;

    move-object v0, v0

    .line 2461304
    if-eqz v0, :cond_0

    .line 2461305
    iget-object v0, p0, LX/HP7;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tx;

    .line 2461306
    iget-object v1, p1, LX/CgC;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2461307
    iget-object v2, p1, LX/CgC;->d:LX/Cfl;

    move-object v2, v2

    .line 2461308
    iget-object v3, p0, LX/HP7;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v4, p0, LX/HP7;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/3Tx;->a(Ljava/lang/String;LX/Cfl;LX/0o8;Landroid/content/Context;)V

    goto :goto_0
.end method
