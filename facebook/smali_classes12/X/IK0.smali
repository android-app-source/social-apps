.class public final LX/IK0;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IK1;


# direct methods
.method public constructor <init>(LX/IK1;)V
    .locals 0

    .prologue
    .line 2564783
    iput-object p1, p0, LX/IK0;->a:LX/IK1;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2564784
    sget-object v0, LX/IK1;->a:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2564785
    iget-object v0, p0, LX/IK0;->a:LX/IK1;

    invoke-static {v0}, LX/IK1;->a(LX/IK1;)V

    .line 2564786
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2564787
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2564788
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564789
    if-eqz v0, :cond_0

    .line 2564790
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564791
    check-cast v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->a()Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2564792
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564793
    check-cast v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->a()Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2564794
    :cond_0
    sget-object v0, LX/IK1;->a:Ljava/lang/Class;

    const-string v1, "Unknown room creation failure"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2564795
    iget-object v0, p0, LX/IK0;->a:LX/IK1;

    invoke-static {v0}, LX/IK1;->a(LX/IK1;)V

    .line 2564796
    :goto_0
    return-void

    .line 2564797
    :cond_1
    iget-object v1, p0, LX/IK0;->a:LX/IK1;

    .line 2564798
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564799
    check-cast v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->a()Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;->k()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    move-result-object v0

    .line 2564800
    iget-object p0, v1, LX/IK1;->d:LX/IK8;

    invoke-virtual {p0, v0}, LX/IK8;->a(Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;)V

    .line 2564801
    goto :goto_0
.end method
