.class public final LX/JV3;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JV4;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pf;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JV4;


# direct methods
.method public constructor <init>(LX/JV4;)V
    .locals 1

    .prologue
    .line 2700097
    iput-object p1, p0, LX/JV3;->c:LX/JV4;

    .line 2700098
    move-object v0, p1

    .line 2700099
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2700100
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2700101
    const-string v0, "ProductsDealsForYouComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2700102
    if-ne p0, p1, :cond_1

    .line 2700103
    :cond_0
    :goto_0
    return v0

    .line 2700104
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2700105
    goto :goto_0

    .line 2700106
    :cond_3
    check-cast p1, LX/JV3;

    .line 2700107
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2700108
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2700109
    if-eq v2, v3, :cond_0

    .line 2700110
    iget-object v2, p0, LX/JV3;->a:LX/1Pf;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JV3;->a:LX/1Pf;

    iget-object v3, p1, LX/JV3;->a:LX/1Pf;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2700111
    goto :goto_0

    .line 2700112
    :cond_5
    iget-object v2, p1, LX/JV3;->a:LX/1Pf;

    if-nez v2, :cond_4

    .line 2700113
    :cond_6
    iget-object v2, p0, LX/JV3;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JV3;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JV3;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2700114
    goto :goto_0

    .line 2700115
    :cond_7
    iget-object v2, p1, LX/JV3;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
