.class public final enum LX/J3T;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J3T;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J3T;

.field public static final enum ACTION_BUTTONS:LX/J3T;

.field public static final enum AMOUNT_CUSTOM_LABEL:LX/J3T;

.field public static final enum AMOUNT_SHIPPING:LX/J3T;

.field public static final enum AMOUNT_TAX:LX/J3T;

.field public static final enum AMOUNT_TOTAL:LX/J3T;

.field public static final enum CURRENCY:LX/J3T;

.field public static final enum DIALOG_BASED_PROGRESS:LX/J3T;

.field public static final enum EMAIL:LX/J3T;

.field public static final enum ENTITY_DESCRIPTION:LX/J3T;

.field public static final enum ENTITY_IMAGE:LX/J3T;

.field public static final enum ENTITY_SUBTITLE:LX/J3T;

.field public static final enum ENTITY_TITLE:LX/J3T;

.field public static final enum HEADER_ACTION_BUTTONS:LX/J3T;

.field public static final enum HEADER_CALL_TO_ACTION:LX/J3T;

.field public static final enum HEADER_ENTITY:LX/J3T;

.field public static final enum HEADER_ITEM_INFO:LX/J3T;

.field public static final enum HEADER_PRICE_TABLE:LX/J3T;

.field public static final enum HEADER_PURCHASE_INFO:LX/J3T;

.field public static final enum HEADER_TITLE_BAR:LX/J3T;

.field public static final enum ITEM_IMAGE:LX/J3T;

.field public static final enum ITEM_INFO:LX/J3T;

.field public static final enum ITEM_SUBTITLE:LX/J3T;

.field public static final enum ITEM_SUB_SUBTITLE:LX/J3T;

.field public static final enum ITEM_SUB_SUB_SUBTITLE:LX/J3T;

.field public static final enum ITEM_TITLE:LX/J3T;

.field public static final enum MAILING_ADDRESS:LX/J3T;

.field public static final enum MERCHANT_NAME:LX/J3T;

.field public static final enum NAME:LX/J3T;

.field public static final enum NO_CHARGE:LX/J3T;

.field public static final enum PAYMENT_METHOD:LX/J3T;

.field public static final enum PAYMENT_PROCESSOR_NAME:LX/J3T;

.field public static final enum PAY_BUTTON_TEXT:LX/J3T;

.field public static final enum PHONE_NUMBER:LX/J3T;

.field public static final enum PLAIN_TEXT:LX/J3T;

.field public static final enum PRICE_TABLE:LX/J3T;

.field public static final enum SHIPPING_OPTION:LX/J3T;

.field public static final enum USE_APP_ICON:LX/J3T;

.field public static final enum USE_AUTHENTICATION:LX/J3T;

.field public static final enum USE_ENTITY:LX/J3T;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2642940
    new-instance v0, LX/J3T;

    const-string v1, "NO_CHARGE"

    const-string v2, "noCharge"

    invoke-direct {v0, v1, v4, v2}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->NO_CHARGE:LX/J3T;

    .line 2642941
    new-instance v0, LX/J3T;

    const-string v1, "HEADER_TITLE_BAR"

    const-string v2, "Title Bar"

    invoke-direct {v0, v1, v5, v2}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->HEADER_TITLE_BAR:LX/J3T;

    .line 2642942
    new-instance v0, LX/J3T;

    const-string v1, "USE_APP_ICON"

    const-string v2, "useAppIcon"

    invoke-direct {v0, v1, v6, v2}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->USE_APP_ICON:LX/J3T;

    .line 2642943
    new-instance v0, LX/J3T;

    const-string v1, "HEADER_ENTITY"

    const-string v2, "Entity"

    invoke-direct {v0, v1, v7, v2}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->HEADER_ENTITY:LX/J3T;

    .line 2642944
    new-instance v0, LX/J3T;

    const-string v1, "USE_ENTITY"

    const-string v2, "entity"

    invoke-direct {v0, v1, v8, v2}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->USE_ENTITY:LX/J3T;

    .line 2642945
    new-instance v0, LX/J3T;

    const-string v1, "ENTITY_IMAGE"

    const/4 v2, 0x5

    const-string v3, "entityImage"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ENTITY_IMAGE:LX/J3T;

    .line 2642946
    new-instance v0, LX/J3T;

    const-string v1, "ENTITY_TITLE"

    const/4 v2, 0x6

    const-string v3, "entityTitle"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ENTITY_TITLE:LX/J3T;

    .line 2642947
    new-instance v0, LX/J3T;

    const-string v1, "ENTITY_SUBTITLE"

    const/4 v2, 0x7

    const-string v3, "entitySubtitle"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ENTITY_SUBTITLE:LX/J3T;

    .line 2642948
    new-instance v0, LX/J3T;

    const-string v1, "ENTITY_DESCRIPTION"

    const/16 v2, 0x8

    const-string v3, "entityDescription"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ENTITY_DESCRIPTION:LX/J3T;

    .line 2642949
    new-instance v0, LX/J3T;

    const-string v1, "HEADER_ITEM_INFO"

    const/16 v2, 0x9

    const-string v3, "Single Item Purchase Review"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->HEADER_ITEM_INFO:LX/J3T;

    .line 2642950
    new-instance v0, LX/J3T;

    const-string v1, "ITEM_INFO"

    const/16 v2, 0xa

    const-string v3, "itemInfo"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ITEM_INFO:LX/J3T;

    .line 2642951
    new-instance v0, LX/J3T;

    const-string v1, "ITEM_IMAGE"

    const/16 v2, 0xb

    const-string v3, "itemImageUrl"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ITEM_IMAGE:LX/J3T;

    .line 2642952
    new-instance v0, LX/J3T;

    const-string v1, "ITEM_TITLE"

    const/16 v2, 0xc

    const-string v3, "itemTitle"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ITEM_TITLE:LX/J3T;

    .line 2642953
    new-instance v0, LX/J3T;

    const-string v1, "ITEM_SUBTITLE"

    const/16 v2, 0xd

    const-string v3, "itemSubtitle"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ITEM_SUBTITLE:LX/J3T;

    .line 2642954
    new-instance v0, LX/J3T;

    const-string v1, "ITEM_SUB_SUBTITLE"

    const/16 v2, 0xe

    const-string v3, "itemSubSubtitle"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ITEM_SUB_SUBTITLE:LX/J3T;

    .line 2642955
    new-instance v0, LX/J3T;

    const-string v1, "ITEM_SUB_SUB_SUBTITLE"

    const/16 v2, 0xf

    const-string v3, "itemSubSubSubtitle"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ITEM_SUB_SUB_SUBTITLE:LX/J3T;

    .line 2642956
    new-instance v0, LX/J3T;

    const-string v1, "HEADER_PRICE_TABLE"

    const/16 v2, 0x10

    const-string v3, "Price Table"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->HEADER_PRICE_TABLE:LX/J3T;

    .line 2642957
    new-instance v0, LX/J3T;

    const-string v1, "PRICE_TABLE"

    const/16 v2, 0x11

    const-string v3, "priceTable"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->PRICE_TABLE:LX/J3T;

    .line 2642958
    new-instance v0, LX/J3T;

    const-string v1, "CURRENCY"

    const/16 v2, 0x12

    const-string v3, "currency"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->CURRENCY:LX/J3T;

    .line 2642959
    new-instance v0, LX/J3T;

    const-string v1, "AMOUNT_CUSTOM_LABEL"

    const/16 v2, 0x13

    const-string v3, "2x Movie Tickets"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->AMOUNT_CUSTOM_LABEL:LX/J3T;

    .line 2642960
    new-instance v0, LX/J3T;

    const-string v1, "AMOUNT_TAX"

    const/16 v2, 0x14

    const-string v3, "tax"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->AMOUNT_TAX:LX/J3T;

    .line 2642961
    new-instance v0, LX/J3T;

    const-string v1, "AMOUNT_SHIPPING"

    const/16 v2, 0x15

    const-string v3, "shipping"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->AMOUNT_SHIPPING:LX/J3T;

    .line 2642962
    new-instance v0, LX/J3T;

    const-string v1, "AMOUNT_TOTAL"

    const/16 v2, 0x16

    const-string v3, "total"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->AMOUNT_TOTAL:LX/J3T;

    .line 2642963
    new-instance v0, LX/J3T;

    const-string v1, "HEADER_PURCHASE_INFO"

    const/16 v2, 0x17

    const-string v3, "Purchase Info"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->HEADER_PURCHASE_INFO:LX/J3T;

    .line 2642964
    new-instance v0, LX/J3T;

    const-string v1, "NAME"

    const/16 v2, 0x18

    const-string v3, "name"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->NAME:LX/J3T;

    .line 2642965
    new-instance v0, LX/J3T;

    const-string v1, "EMAIL"

    const/16 v2, 0x19

    const-string v3, "email"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->EMAIL:LX/J3T;

    .line 2642966
    new-instance v0, LX/J3T;

    const-string v1, "PHONE_NUMBER"

    const/16 v2, 0x1a

    const-string v3, "phoneNumber"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->PHONE_NUMBER:LX/J3T;

    .line 2642967
    new-instance v0, LX/J3T;

    const-string v1, "MAILING_ADDRESS"

    const/16 v2, 0x1b

    const-string v3, "shippingAddress"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->MAILING_ADDRESS:LX/J3T;

    .line 2642968
    new-instance v0, LX/J3T;

    const-string v1, "SHIPPING_OPTION"

    const/16 v2, 0x1c

    const-string v3, "shippingMethod"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->SHIPPING_OPTION:LX/J3T;

    .line 2642969
    new-instance v0, LX/J3T;

    const-string v1, "PAYMENT_METHOD"

    const/16 v2, 0x1d

    const-string v3, "paymentMethod"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->PAYMENT_METHOD:LX/J3T;

    .line 2642970
    new-instance v0, LX/J3T;

    const-string v1, "USE_AUTHENTICATION"

    const/16 v2, 0x1e

    const-string v3, "useAuthentication"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->USE_AUTHENTICATION:LX/J3T;

    .line 2642971
    new-instance v0, LX/J3T;

    const-string v1, "PLAIN_TEXT"

    const/16 v2, 0x1f

    const-string v3, "plainText"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->PLAIN_TEXT:LX/J3T;

    .line 2642972
    new-instance v0, LX/J3T;

    const-string v1, "HEADER_CALL_TO_ACTION"

    const/16 v2, 0x20

    const-string v3, "Call to Action"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->HEADER_CALL_TO_ACTION:LX/J3T;

    .line 2642973
    new-instance v0, LX/J3T;

    const-string v1, "PAYMENT_PROCESSOR_NAME"

    const/16 v2, 0x21

    const-string v3, "paymentProcessorName"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->PAYMENT_PROCESSOR_NAME:LX/J3T;

    .line 2642974
    new-instance v0, LX/J3T;

    const-string v1, "MERCHANT_NAME"

    const/16 v2, 0x22

    const-string v3, "merchantName"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->MERCHANT_NAME:LX/J3T;

    .line 2642975
    new-instance v0, LX/J3T;

    const-string v1, "DIALOG_BASED_PROGRESS"

    const/16 v2, 0x23

    const-string v3, "useDialogBasedProgress"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->DIALOG_BASED_PROGRESS:LX/J3T;

    .line 2642976
    new-instance v0, LX/J3T;

    const-string v1, "PAY_BUTTON_TEXT"

    const/16 v2, 0x24

    const-string v3, "payButtonText"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->PAY_BUTTON_TEXT:LX/J3T;

    .line 2642977
    new-instance v0, LX/J3T;

    const-string v1, "HEADER_ACTION_BUTTONS"

    const/16 v2, 0x25

    const-string v3, "Button Actions"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->HEADER_ACTION_BUTTONS:LX/J3T;

    .line 2642978
    new-instance v0, LX/J3T;

    const-string v1, "ACTION_BUTTONS"

    const/16 v2, 0x26

    const-string v3, "Action Buttons Container"

    invoke-direct {v0, v1, v2, v3}, LX/J3T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J3T;->ACTION_BUTTONS:LX/J3T;

    .line 2642979
    const/16 v0, 0x27

    new-array v0, v0, [LX/J3T;

    sget-object v1, LX/J3T;->NO_CHARGE:LX/J3T;

    aput-object v1, v0, v4

    sget-object v1, LX/J3T;->HEADER_TITLE_BAR:LX/J3T;

    aput-object v1, v0, v5

    sget-object v1, LX/J3T;->USE_APP_ICON:LX/J3T;

    aput-object v1, v0, v6

    sget-object v1, LX/J3T;->HEADER_ENTITY:LX/J3T;

    aput-object v1, v0, v7

    sget-object v1, LX/J3T;->USE_ENTITY:LX/J3T;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/J3T;->ENTITY_IMAGE:LX/J3T;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/J3T;->ENTITY_TITLE:LX/J3T;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/J3T;->ENTITY_SUBTITLE:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/J3T;->ENTITY_DESCRIPTION:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/J3T;->HEADER_ITEM_INFO:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/J3T;->ITEM_INFO:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/J3T;->ITEM_IMAGE:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/J3T;->ITEM_TITLE:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/J3T;->ITEM_SUBTITLE:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/J3T;->ITEM_SUB_SUBTITLE:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/J3T;->ITEM_SUB_SUB_SUBTITLE:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/J3T;->HEADER_PRICE_TABLE:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/J3T;->PRICE_TABLE:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/J3T;->CURRENCY:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/J3T;->AMOUNT_CUSTOM_LABEL:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/J3T;->AMOUNT_TAX:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/J3T;->AMOUNT_SHIPPING:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/J3T;->AMOUNT_TOTAL:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/J3T;->HEADER_PURCHASE_INFO:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/J3T;->NAME:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/J3T;->EMAIL:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/J3T;->PHONE_NUMBER:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/J3T;->MAILING_ADDRESS:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/J3T;->SHIPPING_OPTION:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/J3T;->PAYMENT_METHOD:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/J3T;->USE_AUTHENTICATION:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/J3T;->PLAIN_TEXT:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/J3T;->HEADER_CALL_TO_ACTION:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/J3T;->PAYMENT_PROCESSOR_NAME:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/J3T;->MERCHANT_NAME:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/J3T;->DIALOG_BASED_PROGRESS:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/J3T;->PAY_BUTTON_TEXT:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/J3T;->HEADER_ACTION_BUTTONS:LX/J3T;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/J3T;->ACTION_BUTTONS:LX/J3T;

    aput-object v2, v0, v1

    sput-object v0, LX/J3T;->$VALUES:[LX/J3T;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2642980
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2642981
    iput-object p3, p0, LX/J3T;->mValue:Ljava/lang/String;

    .line 2642982
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J3T;
    .locals 1

    .prologue
    .line 2642983
    const-class v0, LX/J3T;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J3T;

    return-object v0
.end method

.method public static values()[LX/J3T;
    .locals 1

    .prologue
    .line 2642984
    sget-object v0, LX/J3T;->$VALUES:[LX/J3T;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J3T;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2642985
    invoke-virtual {p0}, LX/J3T;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2642986
    iget-object v0, p0, LX/J3T;->mValue:Ljava/lang/String;

    return-object v0
.end method
