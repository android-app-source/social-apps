.class public LX/Hsl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hsj;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hsm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2514949
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hsl;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hsm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514950
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2514951
    iput-object p1, p0, LX/Hsl;->b:LX/0Ot;

    .line 2514952
    return-void
.end method

.method public static a(LX/0QB;)LX/Hsl;
    .locals 4

    .prologue
    .line 2514953
    const-class v1, LX/Hsl;

    monitor-enter v1

    .line 2514954
    :try_start_0
    sget-object v0, LX/Hsl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2514955
    sput-object v2, LX/Hsl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2514956
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2514957
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2514958
    new-instance v3, LX/Hsl;

    const/16 p0, 0x197e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hsl;-><init>(LX/0Ot;)V

    .line 2514959
    move-object v0, v3

    .line 2514960
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2514961
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hsl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2514962
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2514963
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2514964
    check-cast p2, LX/Hsk;

    .line 2514965
    iget-object v0, p0, LX/Hsl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hsm;

    iget-object v1, p2, LX/Hsk;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v2, p2, LX/Hsk;->b:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    iget-object v3, p2, LX/Hsk;->c:LX/HtA;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/Hsm;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/HtA;)LX/1Dg;

    move-result-object v0

    .line 2514966
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2514967
    invoke-static {}, LX/1dS;->b()V

    .line 2514968
    const/4 v0, 0x0

    return-object v0
.end method
