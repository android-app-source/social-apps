.class public LX/Htq;
.super LX/Hs6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
        ":",
        "LX/0ip;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsMinutiaeSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/Hs6;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final b:LX/Hr2;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/Hu0;


# direct methods
.method public constructor <init>(LX/0il;LX/Hr2;Landroid/content/res/Resources;)V
    .locals 3
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Hr2;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516828
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2516829
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Htq;->a:Ljava/lang/ref/WeakReference;

    .line 2516830
    iput-object p2, p0, LX/Htq;->b:LX/Hr2;

    .line 2516831
    iput-object p3, p0, LX/Htq;->c:Landroid/content/res/Resources;

    .line 2516832
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v0

    const v1, 0x7f02086c

    .line 2516833
    iput v1, v0, LX/Htz;->a:I

    .line 2516834
    move-object v0, v0

    .line 2516835
    const v1, 0x7f0a04c2

    .line 2516836
    iput v1, v0, LX/Htz;->f:I

    .line 2516837
    move-object v0, v0

    .line 2516838
    iget-object v1, p0, LX/Htq;->c:Landroid/content/res/Resources;

    const v2, 0x7f0812ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2516839
    iput-object v1, v0, LX/Htz;->b:Ljava/lang/String;

    .line 2516840
    move-object v0, v0

    .line 2516841
    invoke-virtual {p0}, LX/Htq;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2516842
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2516843
    move-object v0, v0

    .line 2516844
    iget-object v1, p0, LX/Htq;->b:LX/Hr2;

    .line 2516845
    iput-object v1, v0, LX/Htz;->e:LX/Hr2;

    .line 2516846
    move-object v0, v0

    .line 2516847
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/Htq;->d:LX/Hu0;

    .line 2516848
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2516827
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2516821
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2516826
    iget-object v0, p0, LX/Htq;->c:Landroid/content/res/Resources;

    const v1, 0x7f0812b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2516825
    iget-object v0, p0, LX/Htq;->d:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2516824
    iget-object v0, p0, LX/Htq;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->p()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2516823
    iget-object v0, p0, LX/Htq;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0ip;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2516822
    sget-object v0, LX/Hty;->MINUTIAE:LX/Hty;

    return-object v0
.end method
