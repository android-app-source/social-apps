.class public final LX/HKL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2km;

.field public final synthetic c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/util/ArrayList;

.field public final synthetic f:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:Ljava/lang/String;

.field public final synthetic i:Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;Ljava/lang/String;LX/2km;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Ljava/util/ArrayList;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2454290
    iput-object p1, p0, LX/HKL;->i:Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;

    iput-object p2, p0, LX/HKL;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HKL;->b:LX/2km;

    iput-object p4, p0, LX/HKL;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p5, p0, LX/HKL;->d:Ljava/lang/String;

    iput-object p6, p0, LX/HKL;->e:Ljava/util/ArrayList;

    iput-object p7, p0, LX/HKL;->f:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object p8, p0, LX/HKL;->g:Ljava/lang/String;

    iput-object p9, p0, LX/HKL;->h:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x0

    const v0, -0x63497694

    invoke-static {v7, v8, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2454291
    new-instance v0, LX/4Vp;

    invoke-direct {v0}, LX/4Vp;-><init>()V

    iget-object v2, p0, LX/HKL;->a:Ljava/lang/String;

    .line 2454292
    iput-object v2, v0, LX/4Vp;->m:Ljava/lang/String;

    .line 2454293
    move-object v0, v0

    .line 2454294
    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    .line 2454295
    iget-object v0, p0, LX/HKL;->i:Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;

    iget-object v3, v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->c:LX/8I0;

    iget-object v0, p0, LX/HKL;->b:LX/2km;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/8I0;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2454296
    const-string v3, "extra_album_selected"

    invoke-static {v0, v3, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2454297
    const-string v2, "extra_photo_tab_mode_params"

    sget-object v3, LX/5SD;->VIEWING_MODE:LX/5SD;

    iget-object v4, p0, LX/HKL;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2454298
    iget-object v5, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2454299
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2454300
    const-string v2, "is_page"

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2454301
    const-string v2, "owner_id"

    iget-object v3, p0, LX/HKL;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2454302
    const-string v2, "pick_hc_pic"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2454303
    const-string v2, "pick_pic_lite"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2454304
    const-string v2, "disable_adding_photos_to_albums"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2454305
    iget-object v2, p0, LX/HKL;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2454306
    iget-boolean v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v2, v3

    .line 2454307
    if-eqz v2, :cond_1

    .line 2454308
    const-string v2, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object v3, p0, LX/HKL;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2454309
    iget-object v2, p0, LX/HKL;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2454310
    const-string v2, "extra_pages_admin_permissions"

    iget-object v3, p0, LX/HKL;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2454311
    :cond_0
    const-string v2, "extra_composer_target_data"

    iget-object v3, p0, LX/HKL;->f:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2454312
    :cond_1
    new-instance v2, LX/Cfl;

    iget-object v3, p0, LX/HKL;->d:Ljava/lang/String;

    sget-object v4, LX/Cfc;->ALBUM_TAP:LX/Cfc;

    invoke-direct {v2, v3, v4, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    .line 2454313
    iget-object v0, p0, LX/HKL;->b:LX/2km;

    iget-object v3, p0, LX/HKL;->g:Ljava/lang/String;

    iget-object v4, p0, LX/HKL;->h:Ljava/lang/String;

    invoke-interface {v0, v3, v4, v2}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2454314
    const v0, -0x400e3489

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
