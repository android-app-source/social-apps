.class public final enum LX/IoT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IoT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IoT;

.field public static final enum CARD_VERIFY:LX/IoT;

.field public static final enum CHECK_AMOUNT:LX/IoT;

.field public static final enum CHECK_AUTHENTICATION:LX/IoT;

.field public static final enum CHECK_RECIPIENT_ELIGIBILITY:LX/IoT;

.field public static final enum PREPARE_PAYMENT:LX/IoT;

.field public static final enum PROCESSING_CARD_VERIFY:LX/IoT;

.field public static final enum PROCESSING_CHECK_AMOUNT:LX/IoT;

.field public static final enum PROCESSING_CHECK_AUTHENTICATION:LX/IoT;

.field public static final enum PROCESSING_CHECK_RECIPIENT_ELIGIBILITY:LX/IoT;

.field public static final enum PROCESSING_SEND_PAYMENT:LX/IoT;

.field public static final enum SEND_PAYMENT:LX/IoT;


# instance fields
.field private mIsForOrionRequest:Z

.field private mIsForOrionSend:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2611864
    new-instance v0, LX/IoT;

    const-string v1, "PREPARE_PAYMENT"

    invoke-direct {v0, v1, v4, v3, v3}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->PREPARE_PAYMENT:LX/IoT;

    .line 2611865
    new-instance v0, LX/IoT;

    const-string v1, "CHECK_RECIPIENT_ELIGIBILITY"

    invoke-direct {v0, v1, v3, v3, v3}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->CHECK_RECIPIENT_ELIGIBILITY:LX/IoT;

    .line 2611866
    new-instance v0, LX/IoT;

    const-string v1, "PROCESSING_CHECK_RECIPIENT_ELIGIBILITY"

    invoke-direct {v0, v1, v5, v3, v3}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->PROCESSING_CHECK_RECIPIENT_ELIGIBILITY:LX/IoT;

    .line 2611867
    new-instance v0, LX/IoT;

    const-string v1, "CARD_VERIFY"

    invoke-direct {v0, v1, v6, v3, v4}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->CARD_VERIFY:LX/IoT;

    .line 2611868
    new-instance v0, LX/IoT;

    const-string v1, "PROCESSING_CARD_VERIFY"

    invoke-direct {v0, v1, v7, v3, v4}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->PROCESSING_CARD_VERIFY:LX/IoT;

    .line 2611869
    new-instance v0, LX/IoT;

    const-string v1, "CHECK_AMOUNT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->CHECK_AMOUNT:LX/IoT;

    .line 2611870
    new-instance v0, LX/IoT;

    const-string v1, "PROCESSING_CHECK_AMOUNT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->PROCESSING_CHECK_AMOUNT:LX/IoT;

    .line 2611871
    new-instance v0, LX/IoT;

    const-string v1, "CHECK_AUTHENTICATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->CHECK_AUTHENTICATION:LX/IoT;

    .line 2611872
    new-instance v0, LX/IoT;

    const-string v1, "PROCESSING_CHECK_AUTHENTICATION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->PROCESSING_CHECK_AUTHENTICATION:LX/IoT;

    .line 2611873
    new-instance v0, LX/IoT;

    const-string v1, "SEND_PAYMENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3, v3}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->SEND_PAYMENT:LX/IoT;

    .line 2611874
    new-instance v0, LX/IoT;

    const-string v1, "PROCESSING_SEND_PAYMENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3, v3}, LX/IoT;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/IoT;->PROCESSING_SEND_PAYMENT:LX/IoT;

    .line 2611875
    const/16 v0, 0xb

    new-array v0, v0, [LX/IoT;

    sget-object v1, LX/IoT;->PREPARE_PAYMENT:LX/IoT;

    aput-object v1, v0, v4

    sget-object v1, LX/IoT;->CHECK_RECIPIENT_ELIGIBILITY:LX/IoT;

    aput-object v1, v0, v3

    sget-object v1, LX/IoT;->PROCESSING_CHECK_RECIPIENT_ELIGIBILITY:LX/IoT;

    aput-object v1, v0, v5

    sget-object v1, LX/IoT;->CARD_VERIFY:LX/IoT;

    aput-object v1, v0, v6

    sget-object v1, LX/IoT;->PROCESSING_CARD_VERIFY:LX/IoT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/IoT;->CHECK_AMOUNT:LX/IoT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IoT;->PROCESSING_CHECK_AMOUNT:LX/IoT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IoT;->CHECK_AUTHENTICATION:LX/IoT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/IoT;->PROCESSING_CHECK_AUTHENTICATION:LX/IoT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/IoT;->SEND_PAYMENT:LX/IoT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/IoT;->PROCESSING_SEND_PAYMENT:LX/IoT;

    aput-object v2, v0, v1

    sput-object v0, LX/IoT;->$VALUES:[LX/IoT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 2611876
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2611877
    iput-boolean p3, p0, LX/IoT;->mIsForOrionSend:Z

    .line 2611878
    iput-boolean p4, p0, LX/IoT;->mIsForOrionRequest:Z

    .line 2611879
    return-void
.end method

.method private static isValidStateForEnterPaymentValueNewDesignSelectedFlow(LX/IoT;LX/IoB;)Z
    .locals 1

    .prologue
    .line 2611880
    sget-object v0, LX/IoB;->REQUEST_MONEY:LX/IoB;

    if-ne p1, v0, :cond_0

    .line 2611881
    iget-boolean v0, p0, LX/IoT;->mIsForOrionRequest:Z

    .line 2611882
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, LX/IoT;->mIsForOrionSend:Z

    goto :goto_0
.end method

.method private static isValidStateForType(LX/IoT;LX/5g0;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)Z
    .locals 2

    .prologue
    .line 2611883
    sget-object v0, LX/IoS;->a:[I

    invoke-virtual {p1}, LX/5g0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2611884
    iget-boolean v0, p0, LX/IoT;->mIsForOrionSend:Z

    :goto_0
    return v0

    .line 2611885
    :pswitch_0
    iget-object v0, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->z:LX/IoB;

    move-object v0, v0

    .line 2611886
    invoke-static {p0, v0}, LX/IoT;->isValidStateForEnterPaymentValueNewDesignSelectedFlow(LX/IoT;LX/IoB;)Z

    move-result v0

    goto :goto_0

    .line 2611887
    :pswitch_1
    iget-boolean v0, p0, LX/IoT;->mIsForOrionRequest:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/IoT;
    .locals 1

    .prologue
    .line 2611888
    const-class v0, LX/IoT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IoT;

    return-object v0
.end method

.method public static values()[LX/IoT;
    .locals 1

    .prologue
    .line 2611889
    sget-object v0, LX/IoT;->$VALUES:[LX/IoT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IoT;

    return-object v0
.end method


# virtual methods
.method public final next(LX/5g0;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)LX/IoT;
    .locals 3

    .prologue
    .line 2611890
    invoke-virtual {p0}, LX/IoT;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-static {}, LX/IoT;->values()[LX/IoT;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 2611891
    invoke-static {}, LX/IoT;->values()[LX/IoT;

    move-result-object v1

    aget-object v1, v1, v0

    .line 2611892
    invoke-static {v1, p1, p2}, LX/IoT;->isValidStateForType(LX/IoT;LX/5g0;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2611893
    return-object v1

    .line 2611894
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2611895
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reaches the end of the state machine without findinga valid state with PaymentFlowType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
