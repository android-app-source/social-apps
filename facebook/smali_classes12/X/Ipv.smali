.class public LX/Ipv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final irisSeqId:Ljava/lang/Long;

.field public final protectedProfileIdsAdded:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final protectedProfileIdsRemoved:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final unprotectedProfileIdsAdded:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final unprotectedProfileIdsRemoved:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0xf

    .line 2615547
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPinProtectedThread"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipv;->b:LX/1sv;

    .line 2615548
    new-instance v0, LX/1sw;

    const-string v1, "protectedProfileIdsAdded"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipv;->c:LX/1sw;

    .line 2615549
    new-instance v0, LX/1sw;

    const-string v1, "protectedProfileIdsRemoved"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipv;->d:LX/1sw;

    .line 2615550
    new-instance v0, LX/1sw;

    const-string v1, "unprotectedProfileIdsAdded"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipv;->e:LX/1sw;

    .line 2615551
    new-instance v0, LX/1sw;

    const-string v1, "unprotectedProfileIdsRemoved"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipv;->f:LX/1sw;

    .line 2615552
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0xa

    const/16 v3, 0x3e8

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipv;->g:LX/1sw;

    .line 2615553
    sput-boolean v4, LX/Ipv;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2615738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2615739
    iput-object p1, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    .line 2615740
    iput-object p2, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    .line 2615741
    iput-object p3, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    .line 2615742
    iput-object p4, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    .line 2615743
    iput-object p5, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    .line 2615744
    return-void
.end method

.method public static b(LX/1su;)LX/Ipv;
    .locals 11

    .prologue
    const/16 v10, 0xf

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 2615691
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v4, v5

    move-object v3, v5

    move-object v2, v5

    move-object v1, v5

    .line 2615692
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 2615693
    iget-byte v7, v0, LX/1sw;->b:B

    if-eqz v7, :cond_a

    .line 2615694
    iget-short v7, v0, LX/1sw;->c:S

    sparse-switch v7, :sswitch_data_0

    .line 2615695
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2615696
    :sswitch_0
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_2

    .line 2615697
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v7

    .line 2615698
    new-instance v1, Ljava/util/ArrayList;

    iget v0, v7, LX/1u3;->b:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v6

    .line 2615699
    :goto_1
    iget v8, v7, LX/1u3;->b:I

    if-gez v8, :cond_1

    invoke-static {}, LX/1su;->t()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2615700
    :goto_2
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2615701
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2615702
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2615703
    :cond_1
    iget v8, v7, LX/1u3;->b:I

    if-ge v0, v8, :cond_0

    goto :goto_2

    .line 2615704
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2615705
    :sswitch_1
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_4

    .line 2615706
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v7

    .line 2615707
    new-instance v2, Ljava/util/ArrayList;

    iget v0, v7, LX/1u3;->b:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v6

    .line 2615708
    :goto_3
    iget v8, v7, LX/1u3;->b:I

    if-gez v8, :cond_3

    invoke-static {}, LX/1su;->t()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2615709
    :goto_4
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2615710
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2615711
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2615712
    :cond_3
    iget v8, v7, LX/1u3;->b:I

    if-ge v0, v8, :cond_0

    goto :goto_4

    .line 2615713
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2615714
    :sswitch_2
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_6

    .line 2615715
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v7

    .line 2615716
    new-instance v3, Ljava/util/ArrayList;

    iget v0, v7, LX/1u3;->b:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v6

    .line 2615717
    :goto_5
    iget v8, v7, LX/1u3;->b:I

    if-gez v8, :cond_5

    invoke-static {}, LX/1su;->t()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2615718
    :goto_6
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2615719
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2615720
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2615721
    :cond_5
    iget v8, v7, LX/1u3;->b:I

    if-ge v0, v8, :cond_0

    goto :goto_6

    .line 2615722
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2615723
    :sswitch_3
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_8

    .line 2615724
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v7

    .line 2615725
    new-instance v4, Ljava/util/ArrayList;

    iget v0, v7, LX/1u3;->b:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v6

    .line 2615726
    :goto_7
    iget v8, v7, LX/1u3;->b:I

    if-gez v8, :cond_7

    invoke-static {}, LX/1su;->t()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2615727
    :goto_8
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2615728
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2615729
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 2615730
    :cond_7
    iget v8, v7, LX/1u3;->b:I

    if-ge v0, v8, :cond_0

    goto :goto_8

    .line 2615731
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2615732
    :sswitch_4
    iget-byte v7, v0, LX/1sw;->b:B

    const/16 v8, 0xa

    if-ne v7, v8, :cond_9

    .line 2615733
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto/16 :goto_0

    .line 2615734
    :cond_9
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2615735
    :cond_a
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2615736
    new-instance v0, LX/Ipv;

    invoke-direct/range {v0 .. v5}, LX/Ipv;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;)V

    .line 2615737
    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x3e8 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2615633
    if-eqz p2, :cond_8

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2615634
    :goto_0
    if-eqz p2, :cond_9

    const-string v0, "\n"

    move-object v3, v0

    .line 2615635
    :goto_1
    if-eqz p2, :cond_a

    const-string v0, " "

    .line 2615636
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaPinProtectedThread"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2615637
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615638
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615639
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615640
    const/4 v1, 0x1

    .line 2615641
    iget-object v6, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 2615642
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615643
    const-string v1, "protectedProfileIdsAdded"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615644
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615645
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615646
    iget-object v1, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    if-nez v1, :cond_b

    .line 2615647
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 2615648
    :cond_0
    iget-object v6, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v6, :cond_2

    .line 2615649
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615650
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615651
    const-string v1, "protectedProfileIdsRemoved"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615652
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615653
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615654
    iget-object v1, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    if-nez v1, :cond_c

    .line 2615655
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 2615656
    :cond_2
    iget-object v6, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    if-eqz v6, :cond_4

    .line 2615657
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615658
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615659
    const-string v1, "unprotectedProfileIdsAdded"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615660
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615661
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615662
    iget-object v1, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    if-nez v1, :cond_d

    .line 2615663
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 2615664
    :cond_4
    iget-object v6, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v6, :cond_10

    .line 2615665
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615666
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615667
    const-string v1, "unprotectedProfileIdsRemoved"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615668
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615669
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615670
    iget-object v1, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    if-nez v1, :cond_e

    .line 2615671
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615672
    :goto_6
    iget-object v1, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 2615673
    if-nez v2, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615674
    :cond_6
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615675
    const-string v1, "irisSeqId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615676
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615677
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615678
    iget-object v0, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_f

    .line 2615679
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615680
    :cond_7
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615681
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615682
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2615683
    :cond_8
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2615684
    :cond_9
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2615685
    :cond_a
    const-string v0, ""

    goto/16 :goto_2

    .line 2615686
    :cond_b
    iget-object v1, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2615687
    :cond_c
    iget-object v1, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2615688
    :cond_d
    iget-object v1, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2615689
    :cond_e
    iget-object v1, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2615690
    :cond_f
    iget-object v0, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_10
    move v2, v1

    goto/16 :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 2615601
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2615602
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2615603
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2615604
    sget-object v0, LX/Ipv;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615605
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v4, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2615606
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2615607
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_0

    .line 2615608
    :cond_0
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2615609
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2615610
    sget-object v0, LX/Ipv;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615611
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v4, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2615612
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2615613
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_1

    .line 2615614
    :cond_1
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2615615
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2615616
    sget-object v0, LX/Ipv;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615617
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v4, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2615618
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2615619
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_2

    .line 2615620
    :cond_2
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 2615621
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 2615622
    sget-object v0, LX/Ipv;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615623
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v4, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2615624
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2615625
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_3

    .line 2615626
    :cond_3
    iget-object v0, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2615627
    iget-object v0, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2615628
    sget-object v0, LX/Ipv;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615629
    iget-object v0, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615630
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2615631
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2615632
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2615558
    if-nez p1, :cond_1

    .line 2615559
    :cond_0
    :goto_0
    return v0

    .line 2615560
    :cond_1
    instance-of v1, p1, LX/Ipv;

    if-eqz v1, :cond_0

    .line 2615561
    check-cast p1, LX/Ipv;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2615562
    if-nez p1, :cond_3

    .line 2615563
    :cond_2
    :goto_1
    move v0, v2

    .line 2615564
    goto :goto_0

    .line 2615565
    :cond_3
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2615566
    :goto_2
    iget-object v3, p1, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2615567
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2615568
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615569
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    iget-object v3, p1, LX/Ipv;->protectedProfileIdsAdded:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615570
    :cond_5
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v0, :cond_10

    move v0, v1

    .line 2615571
    :goto_4
    iget-object v3, p1, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v3, :cond_11

    move v3, v1

    .line 2615572
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2615573
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615574
    iget-object v0, p0, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    iget-object v3, p1, LX/Ipv;->protectedProfileIdsRemoved:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615575
    :cond_7
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    if-eqz v0, :cond_12

    move v0, v1

    .line 2615576
    :goto_6
    iget-object v3, p1, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    if-eqz v3, :cond_13

    move v3, v1

    .line 2615577
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2615578
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615579
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    iget-object v3, p1, LX/Ipv;->unprotectedProfileIdsAdded:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615580
    :cond_9
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v0, :cond_14

    move v0, v1

    .line 2615581
    :goto_8
    iget-object v3, p1, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    if-eqz v3, :cond_15

    move v3, v1

    .line 2615582
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2615583
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615584
    iget-object v0, p0, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    iget-object v3, p1, LX/Ipv;->unprotectedProfileIdsRemoved:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615585
    :cond_b
    iget-object v0, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 2615586
    :goto_a
    iget-object v3, p1, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 2615587
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2615588
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615589
    iget-object v0, p0, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipv;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_d
    move v2, v1

    .line 2615590
    goto/16 :goto_1

    :cond_e
    move v0, v2

    .line 2615591
    goto/16 :goto_2

    :cond_f
    move v3, v2

    .line 2615592
    goto/16 :goto_3

    :cond_10
    move v0, v2

    .line 2615593
    goto :goto_4

    :cond_11
    move v3, v2

    .line 2615594
    goto :goto_5

    :cond_12
    move v0, v2

    .line 2615595
    goto :goto_6

    :cond_13
    move v3, v2

    .line 2615596
    goto :goto_7

    :cond_14
    move v0, v2

    .line 2615597
    goto :goto_8

    :cond_15
    move v3, v2

    .line 2615598
    goto :goto_9

    :cond_16
    move v0, v2

    .line 2615599
    goto :goto_a

    :cond_17
    move v3, v2

    .line 2615600
    goto :goto_b
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2615557
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2615554
    sget-boolean v0, LX/Ipv;->a:Z

    .line 2615555
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipv;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2615556
    return-object v0
.end method
