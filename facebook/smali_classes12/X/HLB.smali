.class public final LX/HLB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HIR;

.field public final synthetic b:LX/HLC;


# direct methods
.method public constructor <init>(LX/HLC;LX/HIR;)V
    .locals 0

    .prologue
    .line 2455637
    iput-object p1, p0, LX/HLB;->b:LX/HLC;

    iput-object p2, p0, LX/HLB;->a:LX/HIR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2455627
    iget-object v0, p0, LX/HLB;->b:LX/HLC;

    iget-object v1, v0, LX/HLC;->a:LX/HLD;

    iget-object v2, p0, LX/HLB;->a:LX/HIR;

    iget-object v0, p0, LX/HLB;->a:LX/HIR;

    iget-boolean v0, v0, LX/HIR;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, LX/HLD;->a$redex0(LX/HLD;LX/HIR;Z)V

    .line 2455628
    return-void

    .line 2455629
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 2455638
    iget-object v0, p0, LX/HLB;->b:LX/HLC;

    iget-object v0, v0, LX/HLC;->a:LX/HLD;

    iget-object v0, v0, LX/HLD;->d:LX/9XE;

    iget-object v1, p0, LX/HLB;->b:LX/HLC;

    iget-object v1, v1, LX/HLC;->a:LX/HLD;

    iget-object v1, v1, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455639
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v2

    .line 2455640
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/HLB;->a:LX/HIR;

    iget-object v1, v1, LX/HIR;->a:Ljava/lang/String;

    iget-object v4, p0, LX/HLB;->a:LX/HIR;

    iget-boolean v4, v4, LX/HIR;->d:Z

    .line 2455641
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    sget-object p1, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_ERROR:LX/9XA;

    invoke-static {p1, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "does_viewer_like"

    invoke-virtual {p1, p2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "tapped_page_id"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v5, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2455642
    iget-object v0, p0, LX/HLB;->b:LX/HLC;

    iget-object v0, v0, LX/HLC;->a:LX/HLD;

    iget-object v1, p0, LX/HLB;->a:LX/HIR;

    iget-object v2, p0, LX/HLB;->a:LX/HIR;

    iget-boolean v2, v2, LX/HIR;->d:Z

    invoke-static {v0, v1, v2}, LX/HLD;->a$redex0(LX/HLD;LX/HIR;Z)V

    .line 2455643
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2455632
    iget-object v0, p0, LX/HLB;->b:LX/HLC;

    iget-object v0, v0, LX/HLC;->a:LX/HLD;

    iget-object v0, v0, LX/HLD;->d:LX/9XE;

    iget-object v1, p0, LX/HLB;->b:LX/HLC;

    iget-object v1, v1, LX/HLC;->a:LX/HLD;

    iget-object v1, v1, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455633
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v2

    .line 2455634
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/HLB;->a:LX/HIR;

    iget-object v1, v1, LX/HIR;->a:Ljava/lang/String;

    iget-object v4, p0, LX/HLB;->a:LX/HIR;

    iget-boolean v4, v4, LX/HIR;->d:Z

    .line 2455635
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    sget-object p0, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_SUCCESS:LX/9XB;

    invoke-static {p0, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "does_viewer_like"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "tapped_page_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v5, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2455636
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2455630
    iget-object v0, p0, LX/HLB;->b:LX/HLC;

    iget-object v0, v0, LX/HLC;->a:LX/HLD;

    iget-object v1, p0, LX/HLB;->a:LX/HIR;

    iget-object v2, p0, LX/HLB;->a:LX/HIR;

    iget-boolean v2, v2, LX/HIR;->d:Z

    invoke-static {v0, v1, v2}, LX/HLD;->a$redex0(LX/HLD;LX/HIR;Z)V

    .line 2455631
    return-void
.end method
