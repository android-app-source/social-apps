.class public final LX/Hxn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1jv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1jv",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 0

    .prologue
    .line 2522853
    iput-object p1, p0, LX/Hxn;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)LX/0k9;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0k9",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2522849
    invoke-static {}, LX/Hx6;->values()[LX/Hx6;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2522850
    iget-object v1, p0, LX/Hxn;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-static {v1, v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hx6;)Landroid/net/Uri;

    move-result-object v2

    .line 2522851
    iget-object v1, p0, LX/Hxn;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-static {v1, v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->b(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hx6;)Ljava/lang/String;

    move-result-object v6

    .line 2522852
    new-instance v0, LX/HxH;

    iget-object v1, p0, LX/Hxn;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, LX/HxH;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2522836
    iget-object v0, p0, LX/Hxn;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2522837
    iput v2, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->M:I

    .line 2522838
    iget-object v0, p0, LX/Hxn;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    .line 2522839
    sget-object v1, LX/0Re;->a:LX/0Re;

    move-object v1, v1

    .line 2522840
    invoke-virtual {v0, v3, v1, v3, v2}, LX/Hx5;->a(Landroid/database/Cursor;LX/0Rf;LX/Hx6;Z)V

    .line 2522841
    return-void
.end method

.method public final a(LX/0k9;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2522842
    check-cast p2, Landroid/database/Cursor;

    .line 2522843
    invoke-static {}, LX/Hx6;->values()[LX/Hx6;

    move-result-object v0

    .line 2522844
    iget v1, p1, LX/0k9;->m:I

    move v1, v1

    .line 2522845
    aget-object v0, v0, v1

    .line 2522846
    iget-object v1, p0, LX/Hxn;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-static {v1, p2, v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Landroid/database/Cursor;LX/Hx6;)V

    .line 2522847
    iget-object v0, p0, LX/Hxn;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->t(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2522848
    return-void
.end method
