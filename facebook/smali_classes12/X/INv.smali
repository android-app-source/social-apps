.class public LX/INv;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 1

    .prologue
    .line 2571290
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2571291
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->s()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/INv;->a:LX/0Px;

    .line 2571292
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2571293
    new-instance v0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;-><init>(Landroid/content/Context;)V

    .line 2571294
    new-instance v1, LX/INu;

    invoke-direct {v1, v0}, LX/INu;-><init>(Lcom/facebook/groups/community/views/CommunityForSalePostItemView;)V

    .line 2571295
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2571296
    iget-object v0, p0, LX/INv;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel;

    move-object v0, v0

    .line 2571297
    if-nez v0, :cond_0

    .line 2571298
    :goto_0
    return-void

    .line 2571299
    :cond_0
    check-cast p1, LX/INu;

    .line 2571300
    iget-object v1, p1, LX/INu;->l:Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel;)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2571301
    iget-object v0, p0, LX/INv;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
