.class public final LX/HWM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

.field public final synthetic b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;)V
    .locals 0

    .prologue
    .line 2475827
    iput-object p1, p0, LX/HWM;->b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    iput-object p2, p0, LX/HWM;->a:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x2f516c55

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2475828
    iget-object v0, p0, LX/HWM;->b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->c:Lcom/facebook/content/SecureContextHelper;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    iget-object v0, p0, LX/HWM;->a:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->y()LX/0Px;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v0, p0, LX/HWM;->b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2475829
    const v0, -0x236b94c1

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
