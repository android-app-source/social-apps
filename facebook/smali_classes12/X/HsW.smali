.class public final LX/HsW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;)V
    .locals 0

    .prologue
    .line 2514575
    iput-object p1, p0, LX/HsW;->a:Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 2514565
    if-eqz p2, :cond_0

    .line 2514566
    iget-object v0, p0, LX/HsW;->a:Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;

    .line 2514567
    const/4 p0, 0x1

    iput-boolean p0, v0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->e:Z

    .line 2514568
    iget-object p0, v0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->c:Landroid/widget/LinearLayout;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2514569
    :goto_0
    return-void

    .line 2514570
    :cond_0
    iget-object v0, p0, LX/HsW;->a:Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;

    const/4 p1, 0x0

    .line 2514571
    iput-boolean p1, v0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->e:Z

    .line 2514572
    iget-object p0, v0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2514573
    iget-object p0, v0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->d:Landroid/widget/TextView;

    const p1, 0x7f08397b

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2514574
    goto :goto_0
.end method
