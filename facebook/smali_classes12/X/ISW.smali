.class public final LX/ISW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IRb;

.field public final synthetic b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;LX/IRb;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;I)V
    .locals 0

    .prologue
    .line 2578015
    iput-object p1, p0, LX/ISW;->d:Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    iput-object p2, p0, LX/ISW;->a:LX/IRb;

    iput-object p3, p0, LX/ISW;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iput p4, p0, LX/ISW;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x6219a4cc

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2578007
    iget-object v1, p0, LX/ISW;->d:Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    iget-object v2, p0, LX/ISW;->d:Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a:LX/ITK;

    iget-object v3, p0, LX/ISW;->a:LX/IRb;

    invoke-virtual {v2, v3}, LX/ITK;->a(LX/IRb;)LX/ITJ;

    move-result-object v2

    .line 2578008
    iput-object v2, v1, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->d:LX/ITJ;

    .line 2578009
    iget-object v1, p0, LX/ISW;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2578010
    iget-object v1, p0, LX/ISW;->d:Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->d:LX/ITJ;

    iget-object v2, p0, LX/ISW;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1, v2}, LX/ITJ;->d(LX/9N6;)V

    .line 2578011
    iget-object v1, p0, LX/ISW;->d:Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->e:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081c1e    # 1.80921E38f

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2578012
    :goto_0
    const v1, 0x7c5d604c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2578013
    :cond_0
    iget-object v1, p0, LX/ISW;->d:Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->d:LX/ITJ;

    iget-object v2, p0, LX/ISW;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1, v2}, LX/ITJ;->e(LX/9N6;)V

    .line 2578014
    iget-object v1, p0, LX/ISW;->d:Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->e:Lcom/facebook/resources/ui/FbButton;

    iget v2, p0, LX/ISW;->c:I

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    goto :goto_0
.end method
