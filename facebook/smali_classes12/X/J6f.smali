.class public final LX/J6f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)V
    .locals 0

    .prologue
    .line 2649945
    iput-object p1, p0, LX/J6f;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    .line 2649946
    add-int v0, p2, p3

    add-int/lit8 v0, v0, 0x8

    if-le v0, p4, :cond_0

    iget-object v0, p0, LX/J6f;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget-object v1, p0, LX/J6f;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    .line 2649947
    iget v2, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    move v1, v2

    .line 2649948
    invoke-virtual {v0, v1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v0

    iget-boolean v0, v0, LX/J4L;->k:Z

    if-nez v0, :cond_1

    .line 2649949
    :cond_0
    :goto_0
    return-void

    .line 2649950
    :cond_1
    iget-object v0, p0, LX/J6f;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    new-instance v1, LX/J6e;

    invoke-direct {v1, p0}, LX/J6e;-><init>(LX/J6f;)V

    iget-object v2, p0, LX/J6f;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    .line 2649951
    iget-object v3, v2, LX/J3t;->i:Ljava/lang/String;

    move-object v2, v3

    .line 2649952
    iget-object v3, p0, LX/J6f;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget-object v4, p0, LX/J6f;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    .line 2649953
    iget p0, v4, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    move v4, p0

    .line 2649954
    invoke-virtual {v3, v4}, LX/J3t;->a(I)LX/J4L;

    move-result-object v3

    iget-object v3, v3, LX/J4L;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/J3t;->a(LX/J3r;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2649955
    return-void
.end method
