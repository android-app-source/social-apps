.class public final LX/IjR;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public final synthetic b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

.field public final synthetic c:LX/IjT;


# direct methods
.method public constructor <init>(LX/IjT;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V
    .locals 0

    .prologue
    .line 2605740
    iput-object p1, p0, LX/IjR;->c:LX/IjT;

    iput-object p2, p0, LX/IjR;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object p3, p0, LX/IjR;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2605731
    iget-object v0, p0, LX/IjR;->c:LX/IjT;

    iget-object v1, p0, LX/IjR;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iget-object v2, p0, LX/IjR;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 2605732
    const-class v3, LX/2Oo;

    invoke-static {p1, v3}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v3

    check-cast v3, LX/2Oo;

    .line 2605733
    if-nez v3, :cond_0

    .line 2605734
    iget-object v3, v0, LX/IjT;->c:LX/6ye;

    invoke-virtual {v3, p1, v1, v2}, LX/6ye;->b(Ljava/lang/Throwable;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V

    .line 2605735
    :goto_0
    return-void

    .line 2605736
    :cond_0
    iget-object v4, v0, LX/IjT;->c:LX/6ye;

    invoke-virtual {v4}, LX/6ye;->a()Ljava/lang/String;

    move-result-object v4

    .line 2605737
    invoke-virtual {v3}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v5

    const/16 p0, 0x274a

    if-ne v5, p0, :cond_1

    .line 2605738
    iget-object v4, v0, LX/IjT;->b:Landroid/content/Context;

    const v5, 0x7f082ca9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2605739
    :cond_1
    iget-object v5, v0, LX/IjT;->c:LX/6ye;

    invoke-virtual {v5, v3, v1, v2, v4}, LX/6ye;->a(LX/2Oo;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2605729
    iget-object v0, p0, LX/IjR;->c:LX/IjT;

    iget-object v0, v0, LX/IjT;->c:LX/6ye;

    iget-object v1, p0, LX/IjR;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iget-object v2, p0, LX/IjR;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-virtual {v0, v1, v2}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V

    .line 2605730
    return-void
.end method
