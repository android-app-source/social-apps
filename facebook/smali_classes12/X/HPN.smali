.class public final LX/HPN;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Z)V
    .locals 0

    .prologue
    .line 2461779
    iput-object p1, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iput-object p2, p0, LX/HPN;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iput-boolean p3, p0, LX/HPN;->b:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2461780
    iget-object v0, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->P(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    .line 2461781
    iget-object v0, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchPageMetaData"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2461782
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2461783
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2461784
    if-eqz p1, :cond_0

    .line 2461785
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2461786
    if-nez v0, :cond_1

    .line 2461787
    :cond_0
    iget-object v0, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->P(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    .line 2461788
    :goto_0
    return-void

    .line 2461789
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;

    .line 2461790
    iget-object v1, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v1, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a$redex0(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;)V

    .line 2461791
    iget-object v0, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->p(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2461792
    iget-object v0, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->s:LX/3iH;

    iget-object v1, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2461793
    iget-object v1, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->v:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pages_photos_reaction_fetch_viewer_context"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/HPM;

    invoke-direct {v3, p0}, LX/HPM;-><init>(LX/HPN;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2461794
    :cond_2
    iget-object v0, p0, LX/HPN;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, p0, LX/HPN;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-boolean v2, p0, LX/HPN;->b:Z

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a$redex0(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Z)V

    goto :goto_0
.end method
