.class public final LX/Ikh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V
    .locals 0

    .prologue
    .line 2606954
    iput-object p1, p0, LX/Ikh;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x220ab24f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2606955
    iget-object v1, p0, LX/Ikh;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    .line 2606956
    const/4 v3, 0x0

    iput-object v3, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2606957
    iget-object v3, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    const/16 p1, 0x8

    .line 2606958
    iget-object p0, v3, LX/Ikl;->i:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2606959
    iget-object p0, v3, LX/Ikl;->j:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2606960
    iget-object p0, v3, LX/Ikl;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2606961
    iget-object v3, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->g:LX/IkZ;

    if-eqz v3, :cond_0

    .line 2606962
    iget-object v3, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->g:LX/IkZ;

    .line 2606963
    iget-object p0, v3, LX/IkZ;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 2606964
    :cond_0
    const v1, -0x27267a03

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
