.class public final LX/JQh;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JQi;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/JQj;

.field public final synthetic b:LX/JQi;


# direct methods
.method public constructor <init>(LX/JQi;)V
    .locals 1

    .prologue
    .line 2692098
    iput-object p1, p0, LX/JQh;->b:LX/JQi;

    .line 2692099
    move-object v0, p1

    .line 2692100
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2692101
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2692097
    const-string v0, "JobSearchOpeningComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2692086
    if-ne p0, p1, :cond_1

    .line 2692087
    :cond_0
    :goto_0
    return v0

    .line 2692088
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2692089
    goto :goto_0

    .line 2692090
    :cond_3
    check-cast p1, LX/JQh;

    .line 2692091
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2692092
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2692093
    if-eq v2, v3, :cond_0

    .line 2692094
    iget-object v2, p0, LX/JQh;->a:LX/JQj;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JQh;->a:LX/JQj;

    iget-object v3, p1, LX/JQh;->a:LX/JQj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2692095
    goto :goto_0

    .line 2692096
    :cond_4
    iget-object v2, p1, LX/JQh;->a:LX/JQj;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
