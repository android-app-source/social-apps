.class public LX/J0Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637024
    return-void
.end method

.method public static a(LX/0QB;)LX/J0Q;
    .locals 1

    .prologue
    .line 2637025
    new-instance v0, LX/J0Q;

    invoke-direct {v0}, LX/J0Q;-><init>()V

    .line 2637026
    move-object v0, v0

    .line 2637027
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2637028
    check-cast p1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;

    .line 2637029
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2637030
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "amount"

    .line 2637031
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2637032
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637033
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "offline_threading_id"

    .line 2637034
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2637035
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637036
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "memo_text"

    .line 2637037
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2637038
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637039
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "group_thread_id"

    .line 2637040
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2637041
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637042
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "theme_id"

    .line 2637043
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2637044
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637045
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "platform_context_id"

    .line 2637046
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2637047
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637048
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637049
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "create_payment_request"

    .line 2637050
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2637051
    move-object v1, v1

    .line 2637052
    const-string v2, "POST"

    .line 2637053
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2637054
    move-object v1, v1

    .line 2637055
    const-string v2, "/%s/p2p_payment_requests"

    .line 2637056
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2637057
    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2637058
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2637059
    move-object v1, v1

    .line 2637060
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2637061
    move-object v0, v1

    .line 2637062
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2637063
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637064
    move-object v0, v0

    .line 2637065
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2637066
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637067
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
