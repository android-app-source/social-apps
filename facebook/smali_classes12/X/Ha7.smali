.class public final enum LX/Ha7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ha7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ha7;

.field public static final enum CREATE_FINISH_REGISTRATION_NOTIFICATION:LX/Ha7;

.field public static final enum OPEN_REGISTRATION_FLOW:LX/Ha7;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2483369
    new-instance v0, LX/Ha7;

    const-string v1, "CREATE_FINISH_REGISTRATION_NOTIFICATION"

    invoke-direct {v0, v1, v2}, LX/Ha7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ha7;->CREATE_FINISH_REGISTRATION_NOTIFICATION:LX/Ha7;

    .line 2483370
    new-instance v0, LX/Ha7;

    const-string v1, "OPEN_REGISTRATION_FLOW"

    invoke-direct {v0, v1, v3}, LX/Ha7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ha7;->OPEN_REGISTRATION_FLOW:LX/Ha7;

    .line 2483371
    const/4 v0, 0x2

    new-array v0, v0, [LX/Ha7;

    sget-object v1, LX/Ha7;->CREATE_FINISH_REGISTRATION_NOTIFICATION:LX/Ha7;

    aput-object v1, v0, v2

    sget-object v1, LX/Ha7;->OPEN_REGISTRATION_FLOW:LX/Ha7;

    aput-object v1, v0, v3

    sput-object v0, LX/Ha7;->$VALUES:[LX/Ha7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2483368
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ha7;
    .locals 1

    .prologue
    .line 2483367
    const-class v0, LX/Ha7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ha7;

    return-object v0
.end method

.method public static values()[LX/Ha7;
    .locals 1

    .prologue
    .line 2483366
    sget-object v0, LX/Ha7;->$VALUES:[LX/Ha7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ha7;

    return-object v0
.end method
