.class public LX/Iyi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field private final b:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final c:LX/0tX;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0tX;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2633751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633752
    iput-object p1, p0, LX/Iyi;->a:Ljava/util/concurrent/Executor;

    .line 2633753
    iput-object p2, p0, LX/Iyi;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2633754
    iput-object p3, p0, LX/Iyi;->c:LX/0tX;

    .line 2633755
    return-void
.end method

.method public static b(LX/0QB;)LX/Iyi;
    .locals 4

    .prologue
    .line 2633756
    new-instance v3, LX/Iyi;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-direct {v3, v0, v1, v2}, LX/Iyi;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0tX;)V

    .line 2633757
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/6xh;Ljava/lang/String;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/PaymentInvoiceActionIdentifier;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/6xh;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/4I7;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/invoice/protocol/graphql/InvoiceMutationsModels$PaymentInvoiceMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2633758
    new-instance v0, LX/4IC;

    invoke-direct {v0}, LX/4IC;-><init>()V

    .line 2633759
    iget-object v1, p0, LX/Iyi;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2633760
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2633761
    const-string v2, "actor_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2633762
    const-string v1, "product_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2633763
    invoke-virtual {p2}, LX/6xh;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2633764
    const-string v2, "product_type"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2633765
    const-string v1, "action_identifier"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2633766
    const-string v1, "action_data"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2633767
    new-instance v1, LX/Iyk;

    invoke-direct {v1}, LX/Iyk;-><init>()V

    move-object v1, v1

    .line 2633768
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Iyk;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2633769
    iget-object v1, p0, LX/Iyi;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2633770
    return-object v0
.end method
