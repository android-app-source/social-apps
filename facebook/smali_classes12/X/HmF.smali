.class public abstract LX/HmF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/HmU;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 0

    .prologue
    .line 2500430
    iput-object p1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;B)V
    .locals 0

    .prologue
    .line 2500429
    invoke-direct {p0, p1}, LX/HmF;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    return-void
.end method

.method private b(LX/HmU;)V
    .locals 6
    .param p1    # LX/HmU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2500373
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-virtual {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2500374
    :goto_0
    return-void

    .line 2500375
    :cond_0
    if-nez p1, :cond_1

    .line 2500376
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-static {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    goto :goto_0

    .line 2500377
    :cond_1
    sget-object v0, LX/HmA;->b:[I

    invoke-virtual {p1}, LX/HmU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2500378
    invoke-virtual {p0, p1}, LX/HmF;->a(LX/HmU;)V

    goto :goto_0

    .line 2500379
    :pswitch_0
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    iget-object v1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f083951

    invoke-virtual {v1, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500380
    iget-object v3, v2, LX/HmV;->s:Ljava/lang/String;

    move-object v2, v3

    .line 2500381
    invoke-virtual {v0, v1, v2, v4}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2500382
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    iget-object v1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500383
    iget-object v2, v1, LX/HmV;->s:Ljava/lang/String;

    move-object v1, v2

    .line 2500384
    invoke-virtual {v0, v1}, LX/Hmf;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 2500385
    :pswitch_1
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500386
    iget-object v1, v0, LX/HmV;->n:LX/Hm3;

    move-object v0, v1

    .line 2500387
    if-nez v0, :cond_2

    .line 2500388
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-static {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    goto :goto_0

    .line 2500389
    :cond_2
    sget-object v1, LX/HmA;->a:[I

    invoke-virtual {v0}, LX/Hm3;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 2500390
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-static {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    goto :goto_0

    .line 2500391
    :pswitch_2
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    iget-object v1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f083952

    invoke-virtual {v1, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500392
    iget-object v3, v2, LX/HmV;->m:Ljava/lang/String;

    move-object v2, v3

    .line 2500393
    invoke-virtual {v0, v1, v2, v4}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2500394
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    iget-object v1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500395
    iget-object v2, v1, LX/HmV;->m:Ljava/lang/String;

    move-object v1, v2

    .line 2500396
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "senderErrorMessage"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 2500397
    iget-object v3, v0, LX/Hmf;->a:LX/0if;

    sget-object v4, LX/0ig;->aX:LX/0ih;

    sget-object p0, LX/Hmd;->SENDER_ERROR_SCREEN_OPENED:LX/Hmd;

    iget-object p0, p0, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-virtual {v3, v4, p0, p1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500398
    goto/16 :goto_0

    .line 2500399
    :pswitch_3
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    iget-object v1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f083952

    invoke-virtual {v1, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500400
    iget-object v4, v2, LX/HmV;->m:Ljava/lang/String;

    move-object v2, v4

    .line 2500401
    invoke-virtual {v0, v1, v2, v3}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2500402
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    iget-object v1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500403
    iget-object v2, v1, LX/HmV;->m:Ljava/lang/String;

    move-object v1, v2

    .line 2500404
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "senderErrorMessage"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 2500405
    iget-object v3, v0, LX/Hmf;->a:LX/0if;

    sget-object v4, LX/0ig;->aX:LX/0ih;

    sget-object p0, LX/Hmd;->INCOMPATIBLE_VERSION_SCREEN_OPENED:LX/Hmd;

    iget-object p0, p0, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-virtual {v3, v4, p0, p1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500406
    goto/16 :goto_0

    .line 2500407
    :pswitch_4
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    iget-object v1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f083952

    invoke-virtual {v1, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500408
    iget-object v4, v2, LX/HmV;->m:Ljava/lang/String;

    move-object v2, v4

    .line 2500409
    invoke-virtual {v0, v1, v2, v3}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2500410
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    iget-object v1, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500411
    iget-object v2, v1, LX/HmV;->o:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-object v1, v2

    .line 2500412
    iget-object v2, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500413
    iget-object v3, v2, LX/HmV;->m:Ljava/lang/String;

    move-object v2, v3

    .line 2500414
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "senderErrorMessage"

    invoke-virtual {v3, v4, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "senderUserId"

    .line 2500415
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v5, v5

    .line 2500416
    iget-object v5, v5, Lcom/facebook/beam/protocol/BeamUserInfo;->mUserId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "senderDeviceBrand"

    .line 2500417
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v5, v5

    .line 2500418
    iget-object v5, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceBrand:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "senderDeviceModel"

    .line 2500419
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v5, v5

    .line 2500420
    iget-object v5, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceModel:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "senderApkVersion"

    .line 2500421
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v5, v5

    .line 2500422
    iget v5, v5, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionCode:I

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v3

    .line 2500423
    iget-object v4, v0, LX/Hmf;->a:LX/0if;

    sget-object v5, LX/0ig;->aX:LX/0ih;

    sget-object p0, LX/Hmd;->NOTHING_TO_SEND_SCREEN_OPENED:LX/Hmd;

    iget-object p0, p0, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-virtual {v4, v5, p0, p1, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500424
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public abstract a(LX/HmU;)V
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2500426
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-virtual {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2500427
    :goto_0
    return-void

    .line 2500428
    :cond_0
    iget-object v0, p0, LX/HmF;->b:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-static {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    goto :goto_0
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2500425
    check-cast p1, LX/HmU;

    invoke-direct {p0, p1}, LX/HmF;->b(LX/HmU;)V

    return-void
.end method
