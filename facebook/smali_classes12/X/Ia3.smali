.class public LX/Ia3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field private final b:LX/0tX;

.field private final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2590540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2590541
    iput-object p1, p0, LX/Ia3;->a:LX/03V;

    .line 2590542
    iput-object p2, p0, LX/Ia3;->b:LX/0tX;

    .line 2590543
    iput-object p3, p0, LX/Ia3;->c:LX/1Ck;

    .line 2590544
    return-void
.end method

.method public static b(LX/0QB;)LX/Ia3;
    .locals 4

    .prologue
    .line 2590538
    new-instance v3, LX/Ia3;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-direct {v3, v0, v1, v2}, LX/Ia3;-><init>(LX/03V;LX/0tX;LX/1Ck;)V

    .line 2590539
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2590528
    iget-object v0, p0, LX/Ia3;->c:LX/1Ck;

    const-string v1, "SendConfirmationTask"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2590529
    return-void
.end method

.method public final a(Ljava/lang/String;LX/Ia2;)V
    .locals 4

    .prologue
    .line 2590530
    new-instance v0, LX/IZZ;

    invoke-direct {v0}, LX/IZZ;-><init>()V

    move-object v0, v0

    .line 2590531
    new-instance v1, LX/4JL;

    invoke-direct {v1}, LX/4JL;-><init>()V

    .line 2590532
    const-string v2, "phone_number"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590533
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2590534
    iget-object v1, p0, LX/Ia3;->c:LX/1Ck;

    const-string v2, "SendConfirmationTask"

    iget-object v3, p0, LX/Ia3;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2590535
    new-instance v3, LX/Ia1;

    invoke-direct {v3, p0, p2}, LX/Ia1;-><init>(LX/Ia3;LX/Ia2;)V

    move-object v3, v3

    .line 2590536
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2590537
    return-void
.end method
