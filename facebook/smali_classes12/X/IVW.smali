.class public LX/IVW;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mW",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/IVg;

.field private final d:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/IVg;LX/IVS;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;LX/25M;)V
    .locals 7
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/1Pn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IVg;",
            "LX/IVS;",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/25M;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582031
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2582032
    iget-object v0, p4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582033
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    .line 2582034
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2582035
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2582036
    :goto_0
    move-object v2, v0

    .line 2582037
    iget-object v0, p4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582038
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2582039
    new-instance v4, LX/IVR;

    invoke-static {p2}, LX/AjG;->a(LX/0QB;)LX/AjG;

    move-result-object v1

    check-cast v1, LX/AjG;

    invoke-static {p2}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    invoke-direct {v4, v1, v3, v0}, LX/IVR;-><init>(LX/AjG;LX/0bH;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2582040
    move-object v3, v4

    .line 2582041
    move-object v4, p5

    check-cast v4, LX/1Pq;

    move-object v0, p0

    move-object v1, p3

    move-object v5, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 2582042
    iput-object p1, p0, LX/IVW;->c:LX/IVg;

    .line 2582043
    iput-object p5, p0, LX/IVW;->d:LX/1Pn;

    .line 2582044
    return-void

    .line 2582045
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2582046
    invoke-static {v0}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2582047
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2582048
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2582049
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2582010
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2582012
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2582013
    iget-object v0, p0, LX/IVW;->c:LX/IVg;

    const/4 v1, 0x0

    .line 2582014
    new-instance v2, LX/IVf;

    invoke-direct {v2, v0}, LX/IVf;-><init>(LX/IVg;)V

    .line 2582015
    iget-object v3, v0, LX/IVg;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IVe;

    .line 2582016
    if-nez v3, :cond_0

    .line 2582017
    new-instance v3, LX/IVe;

    invoke-direct {v3, v0}, LX/IVe;-><init>(LX/IVg;)V

    .line 2582018
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/IVe;->a$redex0(LX/IVe;LX/1De;IILX/IVf;)V

    .line 2582019
    move-object v2, v3

    .line 2582020
    move-object v1, v2

    .line 2582021
    move-object v0, v1

    .line 2582022
    iget-object v1, p0, LX/IVW;->d:LX/1Pn;

    .line 2582023
    iget-object v2, v0, LX/IVe;->a:LX/IVf;

    iput-object v1, v2, LX/IVf;->b:LX/1Pn;

    .line 2582024
    iget-object v2, v0, LX/IVe;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2582025
    move-object v0, v0

    .line 2582026
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2582027
    iget-object v2, v0, LX/IVe;->a:LX/IVf;

    iput-object v1, v2, LX/IVf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582028
    iget-object v2, v0, LX/IVe;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2582029
    move-object v0, v0

    .line 2582030
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2582011
    const/4 v0, 0x0

    return v0
.end method
