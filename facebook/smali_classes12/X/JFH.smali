.class public LX/JFH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JEt;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2wX;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)LX/2wg;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/maps/model/LatLngBounds;",
            "Lcom/google/android/gms/location/places/AutocompleteFilter;",
            ")",
            "LX/2wg",
            "<",
            "LX/JEs;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/JFG;

    sget-object v2, LX/JF3;->c:LX/2vs;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/JFG;-><init>(LX/JFH;LX/2vs;LX/2wX;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(LX/2wX;[Ljava/lang/String;)LX/2wg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "[",
            "Ljava/lang/String;",
            ")",
            "LX/2wg",
            "<",
            "LX/JEv;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/1ol;->b(Z)V

    new-instance v0, LX/JFE;

    sget-object v1, LX/JF3;->c:LX/2vs;

    invoke-direct {v0, p0, v1, p1, p2}, LX/JFE;-><init>(LX/JFH;LX/2vs;LX/2wX;[Ljava/lang/String;)V

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
