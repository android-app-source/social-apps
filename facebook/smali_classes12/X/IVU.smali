.class public final LX/IVU;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/IVV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/IVV;


# direct methods
.method public constructor <init>(LX/IVV;)V
    .locals 1

    .prologue
    .line 2581951
    iput-object p1, p0, LX/IVU;->c:LX/IVV;

    .line 2581952
    move-object v0, p1

    .line 2581953
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2581954
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2581955
    const-string v0, "GroupsSubStoriesHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2581956
    if-ne p0, p1, :cond_1

    .line 2581957
    :cond_0
    :goto_0
    return v0

    .line 2581958
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2581959
    goto :goto_0

    .line 2581960
    :cond_3
    check-cast p1, LX/IVU;

    .line 2581961
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2581962
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2581963
    if-eq v2, v3, :cond_0

    .line 2581964
    iget-object v2, p0, LX/IVU;->a:LX/1Pn;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/IVU;->a:LX/1Pn;

    iget-object v3, p1, LX/IVU;->a:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2581965
    goto :goto_0

    .line 2581966
    :cond_5
    iget-object v2, p1, LX/IVU;->a:LX/1Pn;

    if-nez v2, :cond_4

    .line 2581967
    :cond_6
    iget-object v2, p0, LX/IVU;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/IVU;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/IVU;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2581968
    goto :goto_0

    .line 2581969
    :cond_7
    iget-object v2, p1, LX/IVU;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
