.class public final enum LX/HUm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HUm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HUm;

.field public static final enum ALL_VIDEOS:LX/HUm;

.field public static final enum LOADING_FOOTER:LX/HUm;

.field public static final enum NO_VIDEOS_MESSAGE:LX/HUm;

.field public static final enum VIDEOS_HEADER:LX/HUm;

.field public static final enum VIDEO_ITEM:LX/HUm;

.field public static final enum VIDEO_LIST:LX/HUm;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2473653
    new-instance v0, LX/HUm;

    const-string v1, "ALL_VIDEOS"

    invoke-direct {v0, v1, v3}, LX/HUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUm;->ALL_VIDEOS:LX/HUm;

    .line 2473654
    new-instance v0, LX/HUm;

    const-string v1, "VIDEO_LIST"

    invoke-direct {v0, v1, v4}, LX/HUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUm;->VIDEO_LIST:LX/HUm;

    .line 2473655
    new-instance v0, LX/HUm;

    const-string v1, "VIDEOS_HEADER"

    invoke-direct {v0, v1, v5}, LX/HUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUm;->VIDEOS_HEADER:LX/HUm;

    .line 2473656
    new-instance v0, LX/HUm;

    const-string v1, "VIDEO_ITEM"

    invoke-direct {v0, v1, v6}, LX/HUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUm;->VIDEO_ITEM:LX/HUm;

    .line 2473657
    new-instance v0, LX/HUm;

    const-string v1, "LOADING_FOOTER"

    invoke-direct {v0, v1, v7}, LX/HUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUm;->LOADING_FOOTER:LX/HUm;

    .line 2473658
    new-instance v0, LX/HUm;

    const-string v1, "NO_VIDEOS_MESSAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/HUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUm;->NO_VIDEOS_MESSAGE:LX/HUm;

    .line 2473659
    const/4 v0, 0x6

    new-array v0, v0, [LX/HUm;

    sget-object v1, LX/HUm;->ALL_VIDEOS:LX/HUm;

    aput-object v1, v0, v3

    sget-object v1, LX/HUm;->VIDEO_LIST:LX/HUm;

    aput-object v1, v0, v4

    sget-object v1, LX/HUm;->VIDEOS_HEADER:LX/HUm;

    aput-object v1, v0, v5

    sget-object v1, LX/HUm;->VIDEO_ITEM:LX/HUm;

    aput-object v1, v0, v6

    sget-object v1, LX/HUm;->LOADING_FOOTER:LX/HUm;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/HUm;->NO_VIDEOS_MESSAGE:LX/HUm;

    aput-object v2, v0, v1

    sput-object v0, LX/HUm;->$VALUES:[LX/HUm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2473660
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HUm;
    .locals 1

    .prologue
    .line 2473661
    const-class v0, LX/HUm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HUm;

    return-object v0
.end method

.method public static values()[LX/HUm;
    .locals 1

    .prologue
    .line 2473662
    sget-object v0, LX/HUm;->$VALUES:[LX/HUm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HUm;

    return-object v0
.end method
