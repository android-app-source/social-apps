.class public LX/HJz;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HK2;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HJz",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HK2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453552
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2453553
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HJz;->b:LX/0Zi;

    .line 2453554
    iput-object p1, p0, LX/HJz;->a:LX/0Ot;

    .line 2453555
    return-void
.end method

.method public static a(LX/0QB;)LX/HJz;
    .locals 4

    .prologue
    .line 2453556
    const-class v1, LX/HJz;

    monitor-enter v1

    .line 2453557
    :try_start_0
    sget-object v0, LX/HJz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453558
    sput-object v2, LX/HJz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453559
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453560
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453561
    new-instance v3, LX/HJz;

    const/16 p0, 0x2bbc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJz;-><init>(LX/0Ot;)V

    .line 2453562
    move-object v0, v3

    .line 2453563
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453564
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453565
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2453567
    check-cast p2, LX/HJy;

    .line 2453568
    iget-object v0, p0, LX/HJz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HK2;

    iget-object v1, p2, LX/HJy;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p2, LX/HJy;->b:LX/2km;

    .line 2453569
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 p2, 0x0

    .line 2453570
    iget-object v4, v0, LX/HK2;->a:LX/HJu;

    invoke-virtual {v4, p1}, LX/HJu;->c(LX/1De;)LX/HJs;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/HJs;->a(Z)LX/HJs;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/HJs;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/HJs;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/HJs;->a(LX/2km;)LX/HJs;

    move-result-object v4

    .line 2453571
    new-instance v5, LX/HK1;

    invoke-direct {v5, v0, p1, v1, v2}, LX/HK1;-><init>(LX/HK2;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)V

    .line 2453572
    invoke-static {p1}, LX/8yw;->c(LX/1De;)LX/8yu;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/8yu;->a(LX/1X5;)LX/8yu;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/8yu;->a(LX/0QR;)LX/8yu;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/8yu;->h(I)LX/8yu;

    move-result-object v4

    move-object v4, v4

    .line 2453573
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2453574
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2453575
    invoke-static {}, LX/1dS;->b()V

    .line 2453576
    const/4 v0, 0x0

    return-object v0
.end method
