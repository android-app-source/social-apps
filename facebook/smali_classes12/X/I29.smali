.class public LX/I29;
.super Lcom/facebook/fig/listitem/FigListItem;
.source ""


# instance fields
.field public j:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Blh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/DBA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/events/model/Event;

.field public n:Lcom/facebook/events/common/EventAnalyticsParams;

.field public o:Landroid/view/View$OnClickListener;

.field public p:Lcom/facebook/events/common/ActionMechanism;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p2    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/ActionType;
        .end annotation
    .end param

    .prologue
    .line 2529855
    invoke-direct {p0, p1, p2}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    .line 2529856
    const-class p1, LX/I29;

    invoke-static {p1, p0}, LX/I29;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2529857
    new-instance p1, LX/I27;

    invoke-direct {p1, p0}, LX/I27;-><init>(LX/I29;)V

    move-object p1, p1

    .line 2529858
    invoke-virtual {p0, p1}, LX/I29;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2529859
    new-instance p1, LX/I28;

    invoke-direct {p1, p0}, LX/I28;-><init>(LX/I29;)V

    move-object p1, p1

    .line 2529860
    iput-object p1, p0, LX/I29;->o:Landroid/view/View$OnClickListener;

    .line 2529861
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/I29;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v1

    check-cast v1, LX/6RZ;

    invoke-static {p0}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v2

    check-cast v2, LX/Blh;

    invoke-static {p0}, LX/DBA;->b(LX/0QB;)LX/DBA;

    move-result-object p0

    check-cast p0, LX/DBA;

    iput-object v1, p1, LX/I29;->j:LX/6RZ;

    iput-object v2, p1, LX/I29;->k:LX/Blh;

    iput-object p0, p1, LX/I29;->l:LX/DBA;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 13
    .param p1    # Lcom/facebook/events/model/Event;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 2529862
    if-nez p1, :cond_0

    .line 2529863
    :goto_0
    return-void

    .line 2529864
    :cond_0
    iput-object p1, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529865
    iput-object p2, p0, LX/I29;->n:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529866
    iget-object v0, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529867
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->z:Z

    move v0, v1

    .line 2529868
    if-eqz v0, :cond_1

    .line 2529869
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_DRAFT_EVENT:Lcom/facebook/events/common/ActionMechanism;

    iput-object v0, p0, LX/I29;->p:Lcom/facebook/events/common/ActionMechanism;

    .line 2529870
    :goto_1
    iget-object v0, p0, LX/I29;->l:LX/DBA;

    iget-object v1, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I29;->n:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, LX/DBA;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2529871
    iget-object v0, p1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2529872
    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2529873
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleMaxLines(I)V

    .line 2529874
    iget-object v0, p0, LX/I29;->j:LX/6RZ;

    .line 2529875
    iget-boolean v1, p1, Lcom/facebook/events/model/Event;->N:Z

    move v1, v1

    .line 2529876
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/6RZ;->b(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2529877
    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2529878
    invoke-virtual {p0, v4}, Lcom/facebook/fig/listitem/FigListItem;->setBodyMaxLines(I)V

    .line 2529879
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2529880
    iget-object v5, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529881
    iget-boolean v6, v5, Lcom/facebook/events/model/Event;->z:Z

    move v5, v6

    .line 2529882
    if-eqz v5, :cond_5

    .line 2529883
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529884
    iget-wide v11, v6, Lcom/facebook/events/model/Event;->A:J

    move-wide v7, v11

    .line 2529885
    invoke-virtual {v5, v7, v8}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v5

    invoke-static {v5, v6}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v5

    .line 2529886
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 2529887
    invoke-static {v5}, LX/5O7;->a(Ljava/util/Date;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v5, v6}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2529888
    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-static {v5, v7, v8}, LX/5O7;->a(Ljava/util/Date;J)Ljava/lang/String;

    move-result-object v5

    .line 2529889
    invoke-virtual {p0}, LX/I29;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0837de

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v5, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2529890
    :goto_2
    move-object v0, v5

    .line 2529891
    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2529892
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    move-object v0, v0

    .line 2529893
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2529894
    invoke-virtual {p0}, LX/I29;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2529895
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->z:Z

    move v0, v0

    .line 2529896
    if-eqz v0, :cond_3

    .line 2529897
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2529898
    invoke-virtual {p0}, LX/I29;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082f35

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2529899
    iget-object v0, p0, LX/I29;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2529900
    :cond_1
    iget-object v0, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->av()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2529901
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_PAST_EVENT:Lcom/facebook/events/common/ActionMechanism;

    iput-object v0, p0, LX/I29;->p:Lcom/facebook/events/common/ActionMechanism;

    goto/16 :goto_1

    .line 2529902
    :cond_2
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_UPCOMING_EVENT:Lcom/facebook/events/common/ActionMechanism;

    iput-object v0, p0, LX/I29;->p:Lcom/facebook/events/common/ActionMechanism;

    goto/16 :goto_1

    .line 2529903
    :cond_3
    const/4 v0, 0x0

    .line 2529904
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2529905
    goto/16 :goto_0

    .line 2529906
    :cond_4
    invoke-virtual {p0}, LX/I29;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0837df

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 2529907
    :cond_5
    iget-object v5, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529908
    iget v6, v5, Lcom/facebook/events/model/Event;->ae:I

    move v5, v6

    .line 2529909
    if-nez v5, :cond_6

    .line 2529910
    const-string v5, ""

    goto :goto_2

    .line 2529911
    :cond_6
    iget-object v5, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    invoke-virtual {v5}, Lcom/facebook/events/model/Event;->av()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2529912
    invoke-virtual {p0}, LX/I29;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0176

    iget-object v7, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529913
    iget v8, v7, Lcom/facebook/events/model/Event;->ae:I

    move v7, v8

    .line 2529914
    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529915
    iget v11, v9, Lcom/facebook/events/model/Event;->ae:I

    move v9, v11

    .line 2529916
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 2529917
    :cond_7
    invoke-virtual {p0}, LX/I29;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0178

    iget-object v7, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529918
    iget v8, v7, Lcom/facebook/events/model/Event;->ae:I

    move v7, v8

    .line 2529919
    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, LX/I29;->m:Lcom/facebook/events/model/Event;

    .line 2529920
    iget v11, v9, Lcom/facebook/events/model/Event;->ae:I

    move v9, v11

    .line 2529921
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2
.end method
