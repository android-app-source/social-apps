.class public final LX/HIG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 2450784
    const/16 v22, 0x0

    .line 2450785
    const/16 v21, 0x0

    .line 2450786
    const/16 v20, 0x0

    .line 2450787
    const/16 v19, 0x0

    .line 2450788
    const/16 v18, 0x0

    .line 2450789
    const/16 v17, 0x0

    .line 2450790
    const/16 v16, 0x0

    .line 2450791
    const/4 v15, 0x0

    .line 2450792
    const/4 v14, 0x0

    .line 2450793
    const/4 v13, 0x0

    .line 2450794
    const/4 v12, 0x0

    .line 2450795
    const/4 v11, 0x0

    .line 2450796
    const/4 v10, 0x0

    .line 2450797
    const/4 v9, 0x0

    .line 2450798
    const/4 v8, 0x0

    .line 2450799
    const/4 v7, 0x0

    .line 2450800
    const/4 v6, 0x0

    .line 2450801
    const/4 v5, 0x0

    .line 2450802
    const/4 v4, 0x0

    .line 2450803
    const/4 v3, 0x0

    .line 2450804
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    .line 2450805
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2450806
    const/4 v3, 0x0

    .line 2450807
    :goto_0
    return v3

    .line 2450808
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2450809
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_10

    .line 2450810
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v23

    .line 2450811
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2450812
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    if-eqz v23, :cond_1

    .line 2450813
    const-string v24, "__type__"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    const-string v24, "__typename"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 2450814
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v22

    goto :goto_1

    .line 2450815
    :cond_3
    const-string v24, "admin_info"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 2450816
    invoke-static/range {p0 .. p1}, LX/HIA;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 2450817
    :cond_4
    const-string v24, "commerce_store"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 2450818
    invoke-static/range {p0 .. p1}, LX/HIB;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 2450819
    :cond_5
    const-string v24, "ig_presence_account_info"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 2450820
    invoke-static/range {p0 .. p1}, LX/HIC;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 2450821
    :cond_6
    const-string v24, "instant_articles_enabled"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 2450822
    const/4 v8, 0x1

    .line 2450823
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto :goto_1

    .line 2450824
    :cond_7
    const-string v24, "menu_info"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 2450825
    invoke-static/range {p0 .. p1}, LX/HID;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 2450826
    :cond_8
    const-string v24, "page_call_to_action"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 2450827
    invoke-static/range {p0 .. p1}, LX/HIE;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 2450828
    :cond_9
    const-string v24, "preferred_merchant_settings"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 2450829
    invoke-static/range {p0 .. p1}, LX/HIF;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 2450830
    :cond_a
    const-string v24, "should_show_recent_activity_entry_point"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 2450831
    const/4 v7, 0x1

    .line 2450832
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 2450833
    :cond_b
    const-string v24, "should_show_recent_checkins_entry_point"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 2450834
    const/4 v6, 0x1

    .line 2450835
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 2450836
    :cond_c
    const-string v24, "should_show_recent_mentions_entry_point"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 2450837
    const/4 v5, 0x1

    .line 2450838
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 2450839
    :cond_d
    const-string v24, "should_show_recent_shares_entry_point"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 2450840
    const/4 v4, 0x1

    .line 2450841
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 2450842
    :cond_e
    const-string v24, "should_show_reviews_on_profile"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 2450843
    const/4 v3, 0x1

    .line 2450844
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 2450845
    :cond_f
    const-string v24, "viewer_profile_permissions"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 2450846
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2450847
    :cond_10
    const/16 v23, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2450848
    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450849
    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450850
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450851
    const/16 v20, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450852
    if-eqz v8, :cond_11

    .line 2450853
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 2450854
    :cond_11
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 2450855
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 2450856
    const/4 v8, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 2450857
    if-eqz v7, :cond_12

    .line 2450858
    const/16 v7, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->a(IZ)V

    .line 2450859
    :cond_12
    if-eqz v6, :cond_13

    .line 2450860
    const/16 v6, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->a(IZ)V

    .line 2450861
    :cond_13
    if-eqz v5, :cond_14

    .line 2450862
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->a(IZ)V

    .line 2450863
    :cond_14
    if-eqz v4, :cond_15

    .line 2450864
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(IZ)V

    .line 2450865
    :cond_15
    if-eqz v3, :cond_16

    .line 2450866
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 2450867
    :cond_16
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2450868
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
