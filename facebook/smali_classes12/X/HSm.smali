.class public final LX/HSm;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2467478
    const-class v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    const v0, -0x6fa539e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "PageNewlyAddedEventsQuery"

    const-string v6, "b48101420e35f5c1a4ae8c7a7e6e1b45"

    const-string v7, "nodes"

    const-string v8, "10155207561566729"

    const/4 v9, 0x0

    .line 2467479
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2467480
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2467481
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2467482
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2467483
    sparse-switch v0, :sswitch_data_0

    .line 2467484
    :goto_0
    return-object p1

    .line 2467485
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2467486
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2467487
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2467488
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2467489
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2467490
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2467491
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2467492
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2467493
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2467494
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2467495
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_7
        0x180aba4 -> :sswitch_9
        0x1e45b93 -> :sswitch_3
        0x291d8de0 -> :sswitch_a
        0x3052e0ff -> :sswitch_0
        0x4b46b5f1 -> :sswitch_1
        0x4c6d50cb -> :sswitch_4
        0x5f424068 -> :sswitch_8
        0x61bc9553 -> :sswitch_5
        0x6d2645b9 -> :sswitch_2
        0x73a026b5 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2467496
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2467497
    :goto_1
    return v0

    .line 2467498
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2467499
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 2467500
    new-instance v0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQL$PageNewlyAddedEventsQueryString$1;

    const-class v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQL$PageNewlyAddedEventsQueryString$1;-><init>(LX/HSm;Ljava/lang/Class;)V

    return-object v0
.end method
