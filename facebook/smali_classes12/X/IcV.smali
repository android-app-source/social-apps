.class public final LX/IcV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IcX;


# direct methods
.method public constructor <init>(LX/IcX;)V
    .locals 0

    .prologue
    .line 2595560
    iput-object p1, p0, LX/IcV;->a:LX/IcX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2595561
    iget-object v0, p0, LX/IcV;->a:LX/IcX;

    iget-object v0, v0, LX/IcX;->n:LX/03V;

    const-string v1, "RideCurrentLocationController"

    const-string v2, "GraphQL live map GraphQL subscription failed."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595562
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595563
    check-cast p1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;

    .line 2595564
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/IcV;->a:LX/IcX;

    iget-object v0, v0, LX/IcX;->h:Lcom/facebook/maps/FbMapViewDelegate;

    if-nez v0, :cond_1

    .line 2595565
    :cond_0
    :goto_0
    return-void

    .line 2595566
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2595567
    iget-object v2, p0, LX/IcV;->a:LX/IcX;

    iget-object v2, v2, LX/IcX;->h:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v3, LX/IcU;

    invoke-direct {v3, p0, v0, v1}, LX/IcU;-><init>(LX/IcV;ILX/15i;)V

    invoke-virtual {v2, v3}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595568
    iget-object v2, p0, LX/IcV;->a:LX/IcX;

    iget-object v2, v2, LX/IcX;->k:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$2$2;

    invoke-direct {v3, p0, v1, v0}, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$2$2;-><init>(LX/IcV;LX/15i;I)V

    const v0, -0x3c6d5ad1

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 2595569
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
