.class public LX/JMh;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/1a7;


# instance fields
.field private final a:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

.field private final b:Lcom/facebook/video/player/RichVideoPlayer;

.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2684528
    const v0, 0x7f03151f

    invoke-direct {p0, p1, v0}, LX/JMh;-><init>(Landroid/content/Context;I)V

    .line 2684529
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 2684518
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2684519
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2684520
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/JMh;->setOrientation(I)V

    .line 2684521
    const v0, 0x7f0d16ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JMh;->c:Landroid/widget/TextView;

    .line 2684522
    const v0, 0x7f0d2fae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iput-object v0, p0, LX/JMh;->a:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 2684523
    iget-object v0, p0, LX/JMh;->a:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iput-object v0, p0, LX/JMh;->b:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2684524
    iget-object v0, p0, LX/JMh;->b:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2684525
    new-instance v1, LX/JMg;

    invoke-direct {v1, p0}, LX/JMg;-><init>(LX/JMh;)V

    move-object v1, v1

    .line 2684526
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2684527
    return-void
.end method


# virtual methods
.method public final cr_()Z
    .locals 1

    .prologue
    .line 2684530
    const/4 v0, 0x1

    return v0
.end method

.method public getBottomTextView()Landroid/widget/TextView;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2684517
    iget-object v0, p0, LX/JMh;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method public setBottomText(Ljava/lang/CharSequence;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2684515
    iget-object v0, p0, LX/JMh;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2684516
    return-void
.end method
