.class public LX/I3o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B5X;


# instance fields
.field private final a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/I48;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;)V
    .locals 6

    .prologue
    .line 2531765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2531766
    iput-object p1, p0, LX/I3o;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;

    .line 2531767
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2531768
    iget-object v0, p0, LX/I3o;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;

    .line 2531769
    new-instance v5, LX/I48;

    invoke-direct {v5, v0}, LX/I48;-><init>(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2531770
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2531771
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/I3o;->b:LX/0Px;

    .line 2531772
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/B5b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2531773
    iget-object v0, p0, LX/I3o;->b:LX/0Px;

    return-object v0
.end method
