.class public LX/JOq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JOq",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2688299
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2688300
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JOq;->b:LX/0Zi;

    .line 2688301
    iput-object p1, p0, LX/JOq;->a:LX/0Ot;

    .line 2688302
    return-void
.end method

.method public static a(LX/0QB;)LX/JOq;
    .locals 4

    .prologue
    .line 2688303
    const-class v1, LX/JOq;

    monitor-enter v1

    .line 2688304
    :try_start_0
    sget-object v0, LX/JOq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688305
    sput-object v2, LX/JOq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688306
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688307
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688308
    new-instance v3, LX/JOq;

    const/16 p0, 0x1fce

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JOq;-><init>(LX/0Ot;)V

    .line 2688309
    move-object v0, v3

    .line 2688310
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688311
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688312
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688313
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2688314
    check-cast p2, LX/JOo;

    .line 2688315
    iget-object v0, p0, LX/JOq;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;

    iget-object v1, p2, LX/JOo;->c:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;)LX/1Dg;

    move-result-object v0

    .line 2688316
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2688317
    invoke-static {}, LX/1dS;->b()V

    .line 2688318
    iget v0, p1, LX/1dQ;->b:I

    .line 2688319
    packed-switch v0, :pswitch_data_0

    .line 2688320
    :goto_0
    return-object v2

    .line 2688321
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2688322
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2688323
    check-cast v1, LX/JOo;

    .line 2688324
    iget-object v3, p0, LX/JOq;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;

    iget-object v4, v1, LX/JOo;->c:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    iget-object v5, v1, LX/JOo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/JOo;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688325
    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->d:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->m()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, p0, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2688326
    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->f:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JPN;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->x()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, v5, p1, p0}, LX/JPN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2688327
    :goto_1
    goto :goto_0

    .line 2688328
    :cond_0
    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->f:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JPN;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string v1, "campaign id: "

    invoke-direct {p0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string v1, " action uri can\'t be handled: "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, v5, p1, p0}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x37e8c4b5
        :pswitch_0
    .end packed-switch
.end method
