.class public final LX/JBh;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2660375
    const-class v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    const v0, 0x38514256

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "CollectionItemsPageQuery"

    const-string v6, "836afbb2225066ebaa0b7c308b942342"

    const-string v7, "node"

    const-string v8, "10155207561596729"

    const-string v9, "10155259086566729"

    .line 2660376
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2660377
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2660378
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2660384
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2660385
    sparse-switch v0, :sswitch_data_0

    .line 2660386
    :goto_0
    return-object p1

    .line 2660387
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2660388
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2660389
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2660390
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2660391
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2660392
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2660393
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2660394
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2660395
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2660396
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2660397
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2660398
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2660399
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_9
        -0x3b73c98f -> :sswitch_2
        -0x30f329a4 -> :sswitch_1
        -0x17fb7f63 -> :sswitch_7
        -0x17e5f441 -> :sswitch_3
        -0x14c64f74 -> :sswitch_b
        -0x9ac82a1 -> :sswitch_8
        0x180aba4 -> :sswitch_5
        0x24991595 -> :sswitch_6
        0x291d8de0 -> :sswitch_a
        0x3052e0ff -> :sswitch_0
        0x4b13ca50 -> :sswitch_c
        0x5f424068 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2660379
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2660380
    :goto_1
    return v0

    .line 2660381
    :sswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 2660382
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2660383
    :pswitch_1
    const/16 v0, 0x12

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x38 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
