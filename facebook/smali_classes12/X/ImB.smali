.class public LX/ImB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final q:Ljava/lang/Object;


# instance fields
.field public final a:LX/6og;

.field private final b:LX/IzJ;

.field public final c:LX/2JS;

.field public final d:LX/Izd;

.field public final e:LX/Izl;

.field public final f:LX/Izk;

.field public final g:LX/IzG;

.field public final h:LX/6qC;

.field private final i:LX/J0G;

.field private final j:LX/J0c;

.field public final k:LX/J0e;

.field public final l:LX/J0T;

.field private final m:LX/J0U;

.field public final n:LX/J0b;

.field public final o:LX/J0B;

.field public final p:LX/18V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2608997
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImB;->q:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/6og;LX/IzJ;LX/2JS;LX/Izd;LX/Izl;LX/Izk;LX/IzG;LX/6qC;LX/J0G;LX/J0c;LX/J0e;LX/J0T;LX/J0U;LX/J0b;LX/J0B;LX/18V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2608998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2608999
    iput-object p1, p0, LX/ImB;->a:LX/6og;

    .line 2609000
    iput-object p2, p0, LX/ImB;->b:LX/IzJ;

    .line 2609001
    iput-object p3, p0, LX/ImB;->c:LX/2JS;

    .line 2609002
    iput-object p4, p0, LX/ImB;->d:LX/Izd;

    .line 2609003
    iput-object p7, p0, LX/ImB;->g:LX/IzG;

    .line 2609004
    iput-object p5, p0, LX/ImB;->e:LX/Izl;

    .line 2609005
    iput-object p6, p0, LX/ImB;->f:LX/Izk;

    .line 2609006
    iput-object p8, p0, LX/ImB;->h:LX/6qC;

    .line 2609007
    iput-object p9, p0, LX/ImB;->i:LX/J0G;

    .line 2609008
    iput-object p10, p0, LX/ImB;->j:LX/J0c;

    .line 2609009
    iput-object p11, p0, LX/ImB;->k:LX/J0e;

    .line 2609010
    iput-object p12, p0, LX/ImB;->l:LX/J0T;

    .line 2609011
    iput-object p13, p0, LX/ImB;->m:LX/J0U;

    .line 2609012
    iput-object p14, p0, LX/ImB;->n:LX/J0b;

    .line 2609013
    move-object/from16 v0, p15

    iput-object v0, p0, LX/ImB;->o:LX/J0B;

    .line 2609014
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ImB;->p:LX/18V;

    .line 2609015
    return-void
.end method

.method public static a(LX/0QB;)LX/ImB;
    .locals 7

    .prologue
    .line 2609016
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609017
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609018
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609019
    if-nez v1, :cond_0

    .line 2609020
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609021
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609022
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609023
    sget-object v1, LX/ImB;->q:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609024
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609025
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609026
    :cond_1
    if-nez v1, :cond_4

    .line 2609027
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609028
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609029
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/ImB;->b(LX/0QB;)LX/ImB;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2609030
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609031
    if-nez v1, :cond_2

    .line 2609032
    sget-object v0, LX/ImB;->q:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImB;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609033
    :goto_1
    if-eqz v0, :cond_3

    .line 2609034
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609035
    :goto_3
    check-cast v0, LX/ImB;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609036
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609037
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609038
    :catchall_1
    move-exception v0

    .line 2609039
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609040
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609041
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609042
    :cond_2
    :try_start_8
    sget-object v0, LX/ImB;->q:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImB;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/ImB;
    .locals 17

    .prologue
    .line 2609043
    new-instance v0, LX/ImB;

    invoke-static/range {p0 .. p0}, LX/6og;->a(LX/0QB;)LX/6og;

    move-result-object v1

    check-cast v1, LX/6og;

    invoke-static/range {p0 .. p0}, LX/IzJ;->a(LX/0QB;)LX/IzJ;

    move-result-object v2

    check-cast v2, LX/IzJ;

    invoke-static/range {p0 .. p0}, LX/2JS;->a(LX/0QB;)LX/2JS;

    move-result-object v3

    check-cast v3, LX/2JS;

    invoke-static/range {p0 .. p0}, LX/Izd;->a(LX/0QB;)LX/Izd;

    move-result-object v4

    check-cast v4, LX/Izd;

    invoke-static/range {p0 .. p0}, LX/Izl;->a(LX/0QB;)LX/Izl;

    move-result-object v5

    check-cast v5, LX/Izl;

    invoke-static/range {p0 .. p0}, LX/Izk;->a(LX/0QB;)LX/Izk;

    move-result-object v6

    check-cast v6, LX/Izk;

    invoke-static/range {p0 .. p0}, LX/IzG;->a(LX/0QB;)LX/IzG;

    move-result-object v7

    check-cast v7, LX/IzG;

    invoke-static/range {p0 .. p0}, LX/6qC;->a(LX/0QB;)LX/6qC;

    move-result-object v8

    check-cast v8, LX/6qC;

    invoke-static/range {p0 .. p0}, LX/J0G;->a(LX/0QB;)LX/J0G;

    move-result-object v9

    check-cast v9, LX/J0G;

    invoke-static/range {p0 .. p0}, LX/J0c;->a(LX/0QB;)LX/J0c;

    move-result-object v10

    check-cast v10, LX/J0c;

    invoke-static/range {p0 .. p0}, LX/J0e;->a(LX/0QB;)LX/J0e;

    move-result-object v11

    check-cast v11, LX/J0e;

    invoke-static/range {p0 .. p0}, LX/J0T;->a(LX/0QB;)LX/J0T;

    move-result-object v12

    check-cast v12, LX/J0T;

    invoke-static/range {p0 .. p0}, LX/J0U;->a(LX/0QB;)LX/J0U;

    move-result-object v13

    check-cast v13, LX/J0U;

    invoke-static/range {p0 .. p0}, LX/J0b;->a(LX/0QB;)LX/J0b;

    move-result-object v14

    check-cast v14, LX/J0b;

    invoke-static/range {p0 .. p0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v15

    check-cast v15, LX/J0B;

    invoke-static/range {p0 .. p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v16

    check-cast v16, LX/18V;

    invoke-direct/range {v0 .. v16}, LX/ImB;-><init>(LX/6og;LX/IzJ;LX/2JS;LX/Izd;LX/Izl;LX/Izk;LX/IzG;LX/6qC;LX/J0G;LX/J0c;LX/J0e;LX/J0T;LX/J0U;LX/J0b;LX/J0B;LX/18V;)V

    .line 2609044
    return-object v0
.end method

.method public static c(LX/ImB;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2609045
    iget-object v0, p0, LX/ImB;->p:LX/18V;

    iget-object v1, p0, LX/ImB;->m:LX/J0U;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;

    .line 2609046
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2609047
    iget-object v0, p0, LX/ImB;->p:LX/18V;

    iget-object v1, p0, LX/ImB;->j:LX/J0c;

    new-instance v2, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;

    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-direct {v2, p1, v3}, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;-><init>(Ljava/lang/String;LX/0rS;)V

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2609048
    iget-object v1, p0, LX/ImB;->e:LX/Izl;

    invoke-virtual {v1, v0}, LX/Izl;->b(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2609049
    iget-object v1, p0, LX/ImB;->b:LX/IzJ;

    invoke-virtual {v1, v0}, LX/IzJ;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2609050
    iget-object v1, p0, LX/ImB;->o:LX/J0B;

    .line 2609051
    iget-object v2, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v2, v2

    .line 2609052
    iget-object v3, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2609053
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/J0B;->a(LX/DtQ;J)V

    .line 2609054
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2609055
    iget-object v0, p0, LX/ImB;->p:LX/18V;

    iget-object v1, p0, LX/ImB;->i:LX/J0G;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    .line 2609056
    iget-object v1, v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->b:Lcom/facebook/payments/p2p/model/PaymentCard;

    move-object v1, v1

    .line 2609057
    if-eqz v1, :cond_0

    .line 2609058
    iget-object v1, p0, LX/ImB;->d:LX/Izd;

    .line 2609059
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->b:Lcom/facebook/payments/p2p/model/PaymentCard;

    move-object v2, v2

    .line 2609060
    invoke-virtual {v1, v2}, LX/Izd;->a(Lcom/facebook/payments/p2p/model/PaymentCard;)V

    .line 2609061
    :goto_0
    iget-object v1, p0, LX/ImB;->d:LX/Izd;

    .line 2609062
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->c:LX/0Px;

    move-object v2, v2

    .line 2609063
    invoke-virtual {v1, v2}, LX/Izd;->a(LX/0Px;)V

    .line 2609064
    iget-object v1, p0, LX/ImB;->o:LX/J0B;

    invoke-virtual {v1}, LX/J0B;->c()V

    .line 2609065
    return-void

    .line 2609066
    :cond_0
    iget-object v1, p0, LX/ImB;->d:LX/Izd;

    invoke-virtual {v1}, LX/Izd;->a()V

    goto :goto_0
.end method
