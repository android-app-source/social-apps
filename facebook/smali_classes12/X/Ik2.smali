.class public LX/Ik2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/payments/p2p/model/PaymentCard;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/support/v4/app/Fragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Z

.field public final e:LX/5g0;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/Ik1;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Z


# direct methods
.method public constructor <init>(LX/Ik0;)V
    .locals 1

    .prologue
    .line 2606332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606333
    iget-object v0, p1, LX/Ik0;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    iput-object v0, p0, LX/Ik2;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606334
    iget-object v0, p1, LX/Ik0;->b:LX/0Px;

    iput-object v0, p0, LX/Ik2;->b:LX/0Px;

    .line 2606335
    iget-object v0, p1, LX/Ik0;->c:Landroid/support/v4/app/Fragment;

    iput-object v0, p0, LX/Ik2;->c:Landroid/support/v4/app/Fragment;

    .line 2606336
    iget-boolean v0, p1, LX/Ik0;->d:Z

    iput-boolean v0, p0, LX/Ik2;->d:Z

    .line 2606337
    iget-object v0, p1, LX/Ik0;->e:LX/5g0;

    iput-object v0, p0, LX/Ik2;->e:LX/5g0;

    .line 2606338
    iget-object v0, p1, LX/Ik0;->f:Ljava/lang/String;

    iput-object v0, p0, LX/Ik2;->f:Ljava/lang/String;

    .line 2606339
    iget-object v0, p1, LX/Ik0;->g:Ljava/lang/String;

    iput-object v0, p0, LX/Ik2;->g:Ljava/lang/String;

    .line 2606340
    iget-object v0, p1, LX/Ik0;->h:LX/Ik1;

    iput-object v0, p0, LX/Ik2;->h:LX/Ik1;

    .line 2606341
    iget-object v0, p1, LX/Ik0;->i:Ljava/lang/String;

    iput-object v0, p0, LX/Ik2;->i:Ljava/lang/String;

    .line 2606342
    iget-object v0, p1, LX/Ik0;->j:Ljava/lang/String;

    iput-object v0, p0, LX/Ik2;->j:Ljava/lang/String;

    .line 2606343
    iget-boolean v0, p1, LX/Ik0;->k:Z

    iput-boolean v0, p0, LX/Ik2;->k:Z

    .line 2606344
    return-void
.end method

.method public static newBuilder()LX/Ik0;
    .locals 1

    .prologue
    .line 2606345
    new-instance v0, LX/Ik0;

    invoke-direct {v0}, LX/Ik0;-><init>()V

    return-object v0
.end method
