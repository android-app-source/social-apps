.class public final LX/Hrq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Hrr;


# direct methods
.method public constructor <init>(LX/Hrr;)V
    .locals 0

    .prologue
    .line 2513679
    iput-object p1, p0, LX/Hrq;->a:LX/Hrr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x759639c1

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2513680
    iget-object v1, p0, LX/Hrq;->a:LX/Hrr;

    iget-object v1, v1, LX/Hrr;->e:LX/Hqs;

    .line 2513681
    iget-object v3, v1, LX/Hqs;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2513682
    iget-object v4, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v5, LX/2rw;->PAGE:LX/2rw;

    if-ne v4, v5, :cond_0

    iget-object v4, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    .line 2513683
    :goto_0
    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v7}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v7}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    iget-object v8, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v8}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v8}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v8

    iget-object v9, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    .line 2513684
    invoke-static {v9}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v10

    invoke-interface {v10}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v10

    .line 2513685
    invoke-static {v10}, LX/7kq;->j(LX/0Px;)Z

    move-result v9

    if-nez v9, :cond_2

    const/4 v9, 0x1

    :goto_1
    move v10, v9

    .line 2513686
    move v9, v10

    .line 2513687
    if-nez v9, :cond_1

    const/4 v9, 0x1

    :goto_2
    invoke-static/range {v4 .. v9}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/graphql/model/GraphQLAlbum;Z)Landroid/content/Intent;

    move-result-object v4

    .line 2513688
    const/16 v5, 0xd

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2513689
    const v1, -0x3ff78518

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2513690
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 2513691
    :cond_1
    const/4 v9, 0x0

    goto :goto_2

    :cond_2
    const/4 v9, 0x0

    goto :goto_1
.end method
