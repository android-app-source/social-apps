.class public final LX/Ii9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Landroid/graphics/drawable/RotateDrawable;

.field public final synthetic b:Landroid/graphics/drawable/RotateDrawable;

.field public final synthetic c:Lcom/facebook/messaging/media/ui/MediaFabView;

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/ui/MediaFabView;Landroid/graphics/drawable/RotateDrawable;Landroid/graphics/drawable/RotateDrawable;)V
    .locals 2

    .prologue
    .line 2603375
    iput-object p1, p0, LX/Ii9;->c:Lcom/facebook/messaging/media/ui/MediaFabView;

    iput-object p2, p0, LX/Ii9;->a:Landroid/graphics/drawable/RotateDrawable;

    iput-object p3, p0, LX/Ii9;->b:Landroid/graphics/drawable/RotateDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2603376
    iget-object v0, p0, LX/Ii9;->c:Lcom/facebook/messaging/media/ui/MediaFabView;

    invoke-virtual {v0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x106000b

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/Ii9;->d:I

    .line 2603377
    iget-object v0, p0, LX/Ii9;->c:Lcom/facebook/messaging/media/ui/MediaFabView;

    invoke-virtual {v0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a019a

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/Ii9;->e:I

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    const v4, 0x461c4000    # 10000.0f

    const/high16 v3, 0x437f0000    # 255.0f

    .line 2603378
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2603379
    iget-object v1, p0, LX/Ii9;->a:Landroid/graphics/drawable/RotateDrawable;

    mul-float v2, v0, v4

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/RotateDrawable;->setLevel(I)Z

    .line 2603380
    iget-object v1, p0, LX/Ii9;->b:Landroid/graphics/drawable/RotateDrawable;

    mul-float v2, v0, v4

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/RotateDrawable;->setLevel(I)Z

    .line 2603381
    iget-object v1, p0, LX/Ii9;->a:Landroid/graphics/drawable/RotateDrawable;

    mul-float v2, v0, v3

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/RotateDrawable;->setAlpha(I)V

    .line 2603382
    iget-object v1, p0, LX/Ii9;->b:Landroid/graphics/drawable/RotateDrawable;

    const/high16 v2, -0x3c810000    # -255.0f

    mul-float/2addr v2, v0

    add-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/RotateDrawable;->setAlpha(I)V

    .line 2603383
    iget v1, p0, LX/Ii9;->d:I

    iget v2, p0, LX/Ii9;->e:I

    invoke-static {v0, v1, v2}, LX/6Uc;->a(FII)I

    move-result v0

    .line 2603384
    iget-object v1, p0, LX/Ii9;->c:Lcom/facebook/messaging/media/ui/MediaFabView;

    iget-object v1, v1, Lcom/facebook/messaging/media/ui/MediaFabView;->d:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/fab/FabView;->setFillColor(I)V

    .line 2603385
    return-void
.end method
