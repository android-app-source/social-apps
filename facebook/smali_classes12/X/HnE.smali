.class public final LX/HnE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V
    .locals 0

    .prologue
    .line 2501471
    iput-object p1, p0, LX/HnE;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2501472
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2501473
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2501474
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2501475
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2501476
    goto :goto_0

    .line 2501477
    :cond_1
    iget-object v0, p0, LX/HnE;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->y:LX/Hmz;

    iget-object v1, p0, LX/HnE;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->A:LX/Hmp;

    .line 2501478
    iget-object p0, v0, LX/Hmz;->a:LX/0TD;

    new-instance p1, LX/Hmv;

    invoke-direct {p1, v0, v1}, LX/Hmv;-><init>(LX/Hmz;LX/Hmp;)V

    invoke-interface {p0, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v0, p0

    .line 2501479
    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501480
    check-cast p1, Ljava/lang/Boolean;

    invoke-direct {p0, p1}, LX/HnE;->a(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
