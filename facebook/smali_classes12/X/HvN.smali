.class public LX/HvN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ik;
.implements LX/0il;
.implements LX/0im;
.implements LX/0in;


# static fields
.field private static final a:LX/0jK;


# instance fields
.field public final b:LX/0jJ;

.field private final c:LX/2zG;

.field private final d:LX/B5j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B5j",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/AQ9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1MC;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HvL;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1MD;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2519028
    const-class v0, LX/HvN;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/HvN;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;LX/Ar6;LX/2s2;LX/Hv9;LX/HvK;LX/0Ot;LX/0Ot;LX/1MD;LX/0Ot;)V
    .locals 2
    .param p1    # Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Ar6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/system/api/ComposerSystemData;",
            "LX/Ar6;",
            "LX/2s2;",
            "LX/Hv9;",
            "LX/HvK;",
            "LX/0Ot",
            "<",
            "LX/1MC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HvL;",
            ">;",
            "LX/1MD;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2519037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2519038
    instance-of v0, p1, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2519039
    check-cast p1, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    .line 2519040
    iget-object v0, p1, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v0, v0

    .line 2519041
    invoke-virtual {p3, v0, p0}, LX/2s2;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0in;)LX/0jJ;

    move-result-object v0

    iput-object v0, p0, LX/HvN;->b:LX/0jJ;

    .line 2519042
    invoke-virtual {p4, p0, p0}, LX/Hv9;->a(LX/0il;LX/0in;)LX/2zG;

    move-result-object v0

    iput-object v0, p0, LX/HvN;->c:LX/2zG;

    .line 2519043
    new-instance v0, LX/B5j;

    invoke-direct {v0, p0, p0, p0, p2}, LX/B5j;-><init>(LX/0il;LX/0ik;LX/0im;LX/Ar6;)V

    move-object v0, v0

    .line 2519044
    iput-object v0, p0, LX/HvN;->d:LX/B5j;

    .line 2519045
    iget-object v0, p0, LX/HvN;->d:LX/B5j;

    .line 2519046
    iget-object v1, p1, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2519047
    invoke-virtual {p5, v0, v1}, LX/HvK;->a(LX/B5j;Ljava/lang/String;)LX/AQ9;

    move-result-object v0

    iput-object v0, p0, LX/HvN;->e:LX/AQ9;

    .line 2519048
    iget-object v0, p0, LX/HvN;->b:LX/0jJ;

    sget-object v1, LX/HvN;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->b(LX/0jK;)LX/HvD;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0jL;->j(Z)LX/0jL;

    move-result-object v0

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2519049
    iget-object v0, p0, LX/HvN;->e:LX/AQ9;

    invoke-virtual {p0, v0}, LX/HvN;->a(LX/0iK;)V

    .line 2519050
    iput-object p6, p0, LX/HvN;->f:LX/0Ot;

    .line 2519051
    iput-object p7, p0, LX/HvN;->g:LX/0Ot;

    .line 2519052
    iput-object p8, p0, LX/HvN;->h:LX/1MD;

    .line 2519053
    iput-object p9, p0, LX/HvN;->i:LX/0Ot;

    .line 2519054
    invoke-direct {p0}, LX/HvN;->h()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, LX/HvN;->h()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {p8, v0, v1}, LX/1MD;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 2519055
    return-void
.end method

.method private h()Lcom/facebook/composer/system/model/ComposerModelImpl;
    .locals 1

    .prologue
    .line 2519034
    iget-object v0, p0, LX/HvN;->b:LX/0jJ;

    .line 2519035
    iget-object p0, v0, LX/0jJ;->h:Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v0, p0

    .line 2519036
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2519033
    iget-object v0, p0, LX/HvN;->c:LX/2zG;

    return-object v0
.end method

.method public final a(LX/0iK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0iK",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2519029
    iget-object v0, p0, LX/HvN;->b:LX/0jJ;

    .line 2519030
    iget-object p0, v0, LX/0jJ;->e:LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->a()V

    .line 2519031
    iget-object p0, v0, LX/0jJ;->f:Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2519032
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 3

    .prologue
    .line 2519056
    iget-object v0, p0, LX/HvN;->b:LX/0jJ;

    .line 2519057
    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    if-eq p1, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string p0, " is not supported, use mutations to make and broadcast changes to the model"

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2519058
    iget-object v1, v0, LX/0jJ;->f:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iK;

    .line 2519059
    invoke-interface {v1, p1}, LX/0iK;->a(LX/5L2;)V

    goto :goto_1

    .line 2519060
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 2519061
    :cond_1
    return-void
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2518985
    invoke-direct {p0}, LX/HvN;->h()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0jJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerMutator",
            "<",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2518986
    iget-object v0, p0, LX/HvN;->b:LX/0jJ;

    return-object v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2518987
    iget-object v0, p0, LX/HvN;->e:LX/AQ9;

    return-object v0
.end method

.method public final e()V
    .locals 15

    .prologue
    .line 2518988
    :try_start_0
    iget-object v0, p0, LX/HvN;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HvL;

    iget-object v1, p0, LX/HvN;->b:LX/0jJ;

    .line 2518989
    iget-object v2, v1, LX/0jJ;->h:Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v1, v2

    .line 2518990
    iget-object v2, p0, LX/HvN;->e:LX/AQ9;

    invoke-virtual {v2}, LX/AQ9;->a()LX/B5f;

    move-result-object v2

    .line 2518991
    iget-object v3, v2, LX/B5f;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2518992
    const/4 v5, 0x0

    .line 2518993
    iget-object v4, v0, LX/HvL;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/75Q;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAttachments()LX/0Px;

    move-result-object v6

    invoke-static {v6}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/facebook/photos/tagging/store/TagStoreCopy;->a(LX/75Q;LX/0Px;)Lcom/facebook/photos/tagging/store/TagStoreCopy;

    move-result-object v7

    .line 2518994
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAttachments()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    :goto_0
    if-ge v6, v9, :cond_0

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2518995
    invoke-virtual {v4, v7}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Lcom/facebook/photos/tagging/store/TagStoreCopy;)V

    .line 2518996
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 2518997
    :cond_0
    iget-object v4, v0, LX/HvL;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/75Q;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v6

    invoke-static {v6}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/facebook/photos/tagging/store/TagStoreCopy;->a(LX/75Q;LX/0Px;)Lcom/facebook/photos/tagging/store/TagStoreCopy;

    move-result-object v6

    .line 2518998
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    :goto_1
    if-ge v5, v8, :cond_1

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2518999
    invoke-virtual {v4, v6}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Lcom/facebook/photos/tagging/store/TagStoreCopy;)V

    .line 2519000
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 2519001
    :cond_1
    new-instance v4, LX/HvJ;

    invoke-direct {v4}, LX/HvJ;-><init>()V

    .line 2519002
    iput-object v1, v4, LX/HvJ;->c:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 2519003
    move-object v5, v4

    .line 2519004
    iget-object v4, v0, LX/HvL;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v6

    .line 2519005
    iput-wide v6, v5, LX/HvJ;->b:J

    .line 2519006
    move-object v4, v5

    .line 2519007
    iput-object v2, v4, LX/HvJ;->d:Ljava/lang/String;

    .line 2519008
    move-object v4, v4

    .line 2519009
    invoke-virtual {v4}, LX/HvJ;->a()Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    move-result-object v4

    move-object v1, v4

    .line 2519010
    iget-object v0, p0, LX/HvN;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MC;

    .line 2519011
    if-nez v1, :cond_2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2519012
    :goto_2
    return-void

    .line 2519013
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2519014
    iget-object v0, p0, LX/HvN;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "composer_save_session_failed"

    const-string v3, "Failed to save composer session"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2519015
    :cond_2
    iget-object v5, v0, LX/1MC;->c:LX/0gd;

    sget-object v6, LX/0ge;->SAVE_SESSION:LX/0ge;

    iget-object v4, v1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSessionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2519016
    iget-object v4, v0, LX/1MC;->b:LX/1MD;

    invoke-virtual {v4, v1}, LX/1MD;->a(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    .line 2519017
    iget-object v4, v0, LX/1MC;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    .line 2519018
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2519019
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 2519020
    const-string v7, "saveSession"

    invoke-virtual {v9, v7, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2519021
    iget-object v13, v4, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->d:LX/1Ck;

    const-string v14, "save_session"

    iget-object v7, v4, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->c:LX/0aG;

    const-string v8, "composer_save_session"

    sget-object v10, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v11, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v12, 0x58e39175

    invoke-static/range {v7 .. v12}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v7

    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    new-instance v8, LX/HvI;

    invoke-direct {v8, v4, v1}, LX/HvI;-><init>(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    invoke-virtual {v13, v14, v7, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2519022
    goto :goto_2
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 2519023
    iget-object v0, p0, LX/HvN;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MC;

    invoke-direct {p0}, LX/HvN;->h()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 2519024
    iget-object v2, v0, LX/1MC;->c:LX/0gd;

    sget-object v3, LX/0ge;->DISCARD_SESSION:LX/0ge;

    invoke-virtual {v2, v3, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2519025
    iget-object v2, v0, LX/1MC;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    invoke-virtual {v2}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b()V

    .line 2519026
    iget-object v0, p0, LX/HvN;->h:LX/1MD;

    invoke-direct {p0}, LX/HvN;->h()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1MD;->a(Ljava/lang/String;)V

    .line 2519027
    return-void
.end method
