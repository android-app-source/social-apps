.class public LX/Ild;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IlO;


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:LX/Ilf;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;LX/Ilf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607913
    iput-object p1, p0, LX/Ild;->a:Landroid/view/LayoutInflater;

    .line 2607914
    iput-object p2, p0, LX/Ild;->b:LX/Ilf;

    .line 2607915
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)LX/IlT;
    .locals 5

    .prologue
    .line 2607916
    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;

    if-nez v0, :cond_1

    .line 2607917
    :cond_0
    iget-object v0, p0, LX/Ild;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f03155d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2607918
    :goto_0
    check-cast v0, LX/IlT;

    .line 2607919
    iget-object v1, p0, LX/Ild;->b:LX/Ilf;

    .line 2607920
    instance-of v2, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2607921
    check-cast p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2607922
    iget-object v2, v1, LX/Ilf;->d:LX/Duh;

    invoke-virtual {v2, p1}, LX/Duh;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    .line 2607923
    :goto_1
    invoke-static {}, LX/IlU;->newBuilder()LX/IlV;

    move-result-object v3

    sget-object v4, LX/DtL;->ORION:LX/DtL;

    .line 2607924
    iput-object v4, v3, LX/IlV;->a:LX/DtL;

    .line 2607925
    move-object v3, v3

    .line 2607926
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2607927
    iput-object v4, v3, LX/IlV;->e:Ljava/lang/Boolean;

    .line 2607928
    move-object v3, v3

    .line 2607929
    invoke-static {p1, v2}, LX/Ilf;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;Z)LX/DtN;

    move-result-object v4

    invoke-interface {v4}, LX/DtN;->c()Ljava/lang/String;

    move-result-object v4

    .line 2607930
    iput-object v4, v3, LX/IlV;->b:Ljava/lang/String;

    .line 2607931
    move-object v3, v3

    .line 2607932
    iget-object v4, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    move-object v4, v4

    .line 2607933
    iput-object v4, v3, LX/IlV;->c:Lcom/facebook/payments/p2p/model/Amount;

    .line 2607934
    move-object v3, v3

    .line 2607935
    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 2607936
    iget-object v4, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v4, v4

    .line 2607937
    sget-object p0, LX/Ile;->a:[I

    invoke-virtual {v4}, LX/DtQ;->ordinal()I

    move-result v4

    aget v4, p0, v4

    packed-switch v4, :pswitch_data_0

    .line 2607938
    invoke-static {}, LX/Ila;->newBuilder()LX/Ilb;

    move-result-object v4

    sget-object p0, LX/Ilc;->PENDING:LX/Ilc;

    .line 2607939
    iput-object p0, v4, LX/Ilb;->b:LX/Ilc;

    .line 2607940
    move-object v4, v4

    .line 2607941
    sget-object p0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    .line 2607942
    iput-object p0, v4, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2607943
    move-object v4, v4

    .line 2607944
    const-string p0, ""

    .line 2607945
    iput-object p0, v4, LX/Ilb;->c:Ljava/lang/String;

    .line 2607946
    move-object v4, v4

    .line 2607947
    invoke-virtual {v4}, LX/Ilb;->d()LX/Ila;

    move-result-object v4

    :goto_2
    move-object v4, v4

    .line 2607948
    iput-object v4, v3, LX/IlV;->d:LX/Ila;

    .line 2607949
    move-object v3, v3

    .line 2607950
    invoke-static {}, LX/Ilm;->newBuilder()LX/Iln;

    move-result-object v4

    invoke-static {p1, v2}, LX/Ilf;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;Z)LX/DtN;

    move-result-object v2

    .line 2607951
    iput-object v2, v4, LX/Iln;->a:LX/DtN;

    .line 2607952
    move-object v2, v4

    .line 2607953
    invoke-virtual {v3}, LX/IlV;->f()LX/IlU;

    move-result-object v3

    .line 2607954
    iput-object v3, v2, LX/Iln;->b:LX/IlU;

    .line 2607955
    move-object v2, v2

    .line 2607956
    invoke-virtual {v2}, LX/Iln;->c()LX/Ilm;

    move-result-object v2

    move-object v1, v2

    .line 2607957
    invoke-interface {v0, v1}, LX/IlT;->setMessengerPayHistoryItemViewParams(LX/IlW;)V

    .line 2607958
    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0

    .line 2607959
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 2607960
    :pswitch_0
    sget-object v4, LX/Ilc;->PENDING:LX/Ilc;

    invoke-static {v1, p1, p2, v4}, LX/Ilf;->a(LX/Ilf;Lcom/facebook/payments/p2p/model/PaymentTransaction;ZLX/Ilc;)LX/Ila;

    move-result-object v4

    goto :goto_2

    .line 2607961
    :pswitch_1
    sget-object v4, LX/Ilc;->COMPLETED:LX/Ilc;

    invoke-static {v1, p1, p2, v4}, LX/Ilf;->a(LX/Ilf;Lcom/facebook/payments/p2p/model/PaymentTransaction;ZLX/Ilc;)LX/Ila;

    move-result-object v4

    goto :goto_2

    .line 2607962
    :pswitch_2
    sget-object v4, LX/Ilc;->CANCELED:LX/Ilc;

    invoke-static {v1, p1, p2, v4}, LX/Ilf;->a(LX/Ilf;Lcom/facebook/payments/p2p/model/PaymentTransaction;ZLX/Ilc;)LX/Ila;

    move-result-object v4

    goto :goto_2

    .line 2607963
    :pswitch_3
    sget-object v4, LX/Ilc;->COMPLETED:LX/Ilc;

    invoke-static {v1, p1, p3, v4}, LX/Ilf;->a(LX/Ilf;Lcom/facebook/payments/p2p/model/PaymentTransaction;ZLX/Ilc;)LX/Ila;

    move-result-object v4

    goto :goto_2

    .line 2607964
    :pswitch_4
    sget-object v4, LX/Ilc;->CANCELED:LX/Ilc;

    invoke-static {v1, p1, p3, v4}, LX/Ilf;->a(LX/Ilf;Lcom/facebook/payments/p2p/model/PaymentTransaction;ZLX/Ilc;)LX/Ila;

    move-result-object v4

    goto :goto_2

    .line 2607965
    :pswitch_5
    sget-object v4, LX/Ilc;->PENDING:LX/Ilc;

    invoke-static {v1, p1, p3, v4}, LX/Ilf;->a(LX/Ilf;Lcom/facebook/payments/p2p/model/PaymentTransaction;ZLX/Ilc;)LX/Ila;

    move-result-object v4

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method
