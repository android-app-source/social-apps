.class public LX/IxT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/Ixc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/IxH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2631743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2631744
    return-void
.end method

.method public static a(LX/0QB;)LX/IxT;
    .locals 5

    .prologue
    .line 2631745
    const-class v1, LX/IxT;

    monitor-enter v1

    .line 2631746
    :try_start_0
    sget-object v0, LX/IxT;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2631747
    sput-object v2, LX/IxT;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2631748
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2631749
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2631750
    new-instance p0, LX/IxT;

    invoke-direct {p0}, LX/IxT;-><init>()V

    .line 2631751
    invoke-static {v0}, LX/Ixc;->a(LX/0QB;)LX/Ixc;

    move-result-object v3

    check-cast v3, LX/Ixc;

    invoke-static {v0}, LX/IxH;->a(LX/0QB;)LX/IxH;

    move-result-object v4

    check-cast v4, LX/IxH;

    .line 2631752
    iput-object v3, p0, LX/IxT;->a:LX/Ixc;

    iput-object v4, p0, LX/IxT;->b:LX/IxH;

    .line 2631753
    move-object v0, p0

    .line 2631754
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2631755
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IxT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2631756
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2631757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
