.class public final LX/Hpw;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Hpx;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BcO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2510134
    invoke-static {}, LX/Hpx;->d()LX/Hpx;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2510135
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2510136
    if-ne p0, p1, :cond_1

    .line 2510137
    :cond_0
    :goto_0
    return v0

    .line 2510138
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2510139
    goto :goto_0

    .line 2510140
    :cond_3
    check-cast p1, LX/Hpw;

    .line 2510141
    iget-object v2, p0, LX/Hpw;->b:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Hpw;->b:Ljava/util/List;

    iget-object v3, p1, LX/Hpw;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2510142
    goto :goto_0

    .line 2510143
    :cond_4
    iget-object v2, p1, LX/Hpw;->b:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
