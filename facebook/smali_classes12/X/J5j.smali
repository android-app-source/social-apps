.class public final enum LX/J5j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J5j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J5j;

.field public static final enum PROFILE_PHOTO_CHECKUP:LX/J5j;


# instance fields
.field public final reviewType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2648606
    new-instance v0, LX/J5j;

    const-string v1, "PROFILE_PHOTO_CHECKUP"

    const-string v2, "fb4a_profile_photo_checkup"

    invoke-direct {v0, v1, v3, v2}, LX/J5j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5j;->PROFILE_PHOTO_CHECKUP:LX/J5j;

    .line 2648607
    const/4 v0, 0x1

    new-array v0, v0, [LX/J5j;

    sget-object v1, LX/J5j;->PROFILE_PHOTO_CHECKUP:LX/J5j;

    aput-object v1, v0, v3

    sput-object v0, LX/J5j;->$VALUES:[LX/J5j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2648608
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2648609
    iput-object p3, p0, LX/J5j;->reviewType:Ljava/lang/String;

    .line 2648610
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J5j;
    .locals 1

    .prologue
    .line 2648611
    const-class v0, LX/J5j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J5j;

    return-object v0
.end method

.method public static values()[LX/J5j;
    .locals 1

    .prologue
    .line 2648612
    sget-object v0, LX/J5j;->$VALUES:[LX/J5j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J5j;

    return-object v0
.end method
