.class public LX/I4m;
.super Landroid/widget/BaseExpandableListAdapter;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/I4l;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/I4o;

.field public g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field private i:I

.field private j:I

.field public final k:Landroid/content/Context;

.field private final l:LX/0ad;

.field public final m:LX/0wM;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/HashSet;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;LX/I4o;Landroid/content/Context;LX/0ad;LX/0wM;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/util/HashSet;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/I4o;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;",
            "Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment$OnChooseLocationClickedListener;",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2534508
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 2534509
    iput-object p1, p0, LX/I4m;->b:Ljava/util/List;

    .line 2534510
    iput-object p2, p0, LX/I4m;->c:Ljava/util/HashSet;

    .line 2534511
    iput-object p3, p0, LX/I4m;->d:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2534512
    iput-object p4, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534513
    iput-object p5, p0, LX/I4m;->f:LX/I4o;

    .line 2534514
    iput-object p6, p0, LX/I4m;->k:Landroid/content/Context;

    .line 2534515
    iput-object p7, p0, LX/I4m;->l:LX/0ad;

    .line 2534516
    iput-object p8, p0, LX/I4m;->m:LX/0wM;

    .line 2534517
    iget-object v0, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/I4m;->h:I

    .line 2534518
    iget-object v0, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/I4m;->i:I

    .line 2534519
    iget-object v0, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/I4m;->j:I

    .line 2534520
    invoke-direct {p0}, LX/I4m;->c()V

    .line 2534521
    return-void
.end method

.method private a(II)LX/I4k;
    .locals 1

    .prologue
    .line 2534507
    invoke-direct {p0, p1}, LX/I4m;->b(I)LX/I4l;

    move-result-object v0

    iget-object v0, v0, LX/I4l;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I4k;

    return-object v0
.end method

.method private a(LX/I4j;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2534501
    sget-object v0, LX/I4i;->a:[I

    invoke-virtual {p1}, LX/I4j;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2534502
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "checkbox type is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2534503
    :pswitch_0
    iget-object v0, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0207d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2534504
    :goto_0
    return-object v0

    .line 2534505
    :pswitch_1
    iget-object v0, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0209a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2534506
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/util/HashSet;)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2534498
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2534499
    :cond_0
    const/4 v0, 0x0

    .line 2534500
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017a

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/I4m;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)Ljava/util/List;
    .locals 10
    .param p0    # LX/I4m;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/I4k;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 2534480
    if-nez p1, :cond_0

    .line 2534481
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 2534482
    :goto_0
    return-object v0

    .line 2534483
    :cond_0
    iget-object v0, p1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    move-object v9, v0

    .line 2534484
    new-instance v8, Ljava/util/ArrayList;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v6, v7

    .line 2534485
    :goto_1
    const/4 v0, 0x3

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v6, v0, :cond_4

    .line 2534486
    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 2534487
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2534488
    if-eqz p2, :cond_1

    .line 2534489
    iget-object v0, p2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2534490
    if-nez v0, :cond_3

    :cond_1
    move v3, v7

    .line 2534491
    :goto_2
    new-instance v0, LX/I4k;

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    sget-object v4, LX/I4j;->SINGLE_SELECTION:LX/I4j;

    invoke-direct/range {v0 .. v5}, LX/I4k;-><init>(Ljava/lang/String;Ljava/lang/String;ZLX/I4j;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2534492
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 2534493
    :cond_3
    iget-object v0, p2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2534494
    invoke-virtual {v2}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_2

    .line 2534495
    :cond_4
    new-instance v0, LX/I4k;

    const v1, 0x7f0d0210

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0837f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, LX/I4j;->NO_SELECTION:LX/I4j;

    move v3, v7

    invoke-direct/range {v0 .. v5}, LX/I4k;-><init>(Ljava/lang/String;Ljava/lang/String;ZLX/I4j;Ljava/lang/String;)V

    .line 2534496
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v8

    .line 2534497
    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/util/HashSet;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/I4k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2534473
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2534474
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;

    .line 2534475
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2534476
    if-nez p1, :cond_1

    const/4 v3, 0x0

    .line 2534477
    :goto_1
    new-instance v0, LX/I4k;

    invoke-virtual {v5}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;->k()Ljava/lang/String;

    move-result-object v2

    sget-object v4, LX/I4j;->MULTIPLE_SELECTION:LX/I4j;

    invoke-virtual {v5}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/I4k;-><init>(Ljava/lang/String;Ljava/lang/String;ZLX/I4j;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2534478
    :cond_1
    invoke-virtual {v5}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_1

    .line 2534479
    :cond_2
    return-object v6
.end method

.method private b(I)LX/I4l;
    .locals 1

    .prologue
    .line 2534472
    iget-object v0, p0, LX/I4m;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I4l;

    return-object v0
.end method

.method private c()V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v1, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2534459
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/I4m;->a:Ljava/util/List;

    .line 2534460
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/I4m;->g:Ljava/util/HashMap;

    .line 2534461
    new-instance v0, LX/I4l;

    const v2, 0x7f0d020e

    iget-object v1, p0, LX/I4m;->b:Ljava/util/List;

    iget-object v3, p0, LX/I4m;->c:Ljava/util/HashSet;

    invoke-static {v1, v3}, LX/I4m;->a(Ljava/util/List;Ljava/util/HashSet;)Ljava/util/List;

    move-result-object v3

    const v4, 0x7f0209e1

    const v5, 0x7f0837f4

    iget-object v1, p0, LX/I4m;->k:Landroid/content/Context;

    iget-object v6, p0, LX/I4m;->c:Ljava/util/HashSet;

    invoke-static {v1, v6}, LX/I4m;->a(Landroid/content/Context;Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, LX/I4m;->c:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    move v7, v8

    :goto_0
    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/I4l;-><init>(LX/I4m;ILjava/util/List;IILjava/lang/String;Z)V

    .line 2534462
    iget-object v1, p0, LX/I4m;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2534463
    iget-object v0, p0, LX/I4m;->g:Ljava/util/HashMap;

    const v1, 0x7f0d020e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/I4m;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2534464
    new-instance v0, LX/I4l;

    const v2, 0x7f0d020f

    iget-object v1, p0, LX/I4m;->d:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    iget-object v3, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-static {p0, v1, v3}, LX/I4m;->a(LX/I4m;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)Ljava/util/List;

    move-result-object v3

    const v4, 0x7f020965

    const v5, 0x7f0837f5

    iget-object v1, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534465
    iget-object v6, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    move-object v6, v6

    .line 2534466
    :goto_1
    iget-object v1, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    if-eqz v1, :cond_2

    move v7, v8

    :goto_2
    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/I4l;-><init>(LX/I4m;ILjava/util/List;IILjava/lang/String;Z)V

    .line 2534467
    iget-object v1, p0, LX/I4m;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2534468
    iget-object v0, p0, LX/I4m;->g:Ljava/util/HashMap;

    const v1, 0x7f0d020f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/I4m;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2534469
    return-void

    :cond_0
    move v7, v9

    .line 2534470
    goto :goto_0

    .line 2534471
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    :cond_2
    move v7, v9

    goto :goto_2
.end method


# virtual methods
.method public final a(ILX/I4k;)V
    .locals 5
    .param p2    # LX/I4k;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2534423
    invoke-direct {p0, p1}, LX/I4m;->b(I)LX/I4l;

    move-result-object v1

    .line 2534424
    iget v0, v1, LX/I4l;->a:I

    const v2, 0x7f0d020e

    if-ne v0, v2, :cond_4

    .line 2534425
    if-nez p2, :cond_3

    .line 2534426
    iget-object v0, p0, LX/I4m;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 2534427
    :goto_0
    iget-object v0, p0, LX/I4m;->c:Ljava/util/HashSet;

    const/4 v3, 0x0

    .line 2534428
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v1, LX/I4l;->f:Z

    .line 2534429
    iget-object v2, v1, LX/I4l;->g:LX/I4m;

    iget-object v2, v2, LX/I4m;->k:Landroid/content/Context;

    invoke-static {v2, v0}, LX/I4m;->a(Landroid/content/Context;Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/I4l;->e:Ljava/lang/String;

    .line 2534430
    iget-object v2, v1, LX/I4l;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/I4k;

    .line 2534431
    if-nez v0, :cond_1

    move p1, v3

    :goto_3
    iput-boolean p1, v2, LX/I4k;->c:Z

    goto :goto_2

    :cond_0
    move v2, v3

    .line 2534432
    goto :goto_1

    .line 2534433
    :cond_1
    iget-object p1, v2, LX/I4k;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_3

    .line 2534434
    :cond_2
    :goto_4
    invoke-virtual {p0}, LX/I4m;->notifyDataSetChanged()V

    .line 2534435
    return-void

    .line 2534436
    :cond_3
    iget-object v0, p0, LX/I4m;->c:Ljava/util/HashSet;

    iget-object v2, p2, LX/I4k;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2534437
    iget-object v0, p0, LX/I4m;->c:Ljava/util/HashSet;

    iget-object v2, p2, LX/I4k;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2534438
    :goto_5
    goto :goto_0

    .line 2534439
    :cond_4
    iget v0, v1, LX/I4l;->a:I

    const v2, 0x7f0d020f

    if-ne v0, v2, :cond_2

    .line 2534440
    if-nez p2, :cond_a

    const/4 v0, 0x0

    :goto_6
    iput-object v0, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534441
    iget-object v0, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    if-eqz v0, :cond_5

    .line 2534442
    iget-object v0, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    const/4 v2, 0x0

    .line 2534443
    iput-boolean v2, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2534444
    iget-object v0, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iget-object v2, p2, LX/I4k;->a:Ljava/lang/String;

    .line 2534445
    iput-object v2, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2534446
    iget-object v0, p0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iget-object v2, p2, LX/I4k;->b:Ljava/lang/String;

    .line 2534447
    iput-object v2, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2534448
    :cond_5
    const/4 v2, 0x0

    .line 2534449
    if-eqz p2, :cond_6

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, v1, LX/I4l;->f:Z

    .line 2534450
    if-nez p2, :cond_7

    const-string v0, ""

    :goto_8
    iput-object v0, v1, LX/I4l;->e:Ljava/lang/String;

    .line 2534451
    iget-object v0, v1, LX/I4l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I4k;

    .line 2534452
    if-nez p2, :cond_8

    move v3, v2

    :goto_a
    iput-boolean v3, v0, LX/I4k;->c:Z

    goto :goto_9

    :cond_6
    move v0, v2

    .line 2534453
    goto :goto_7

    .line 2534454
    :cond_7
    iget-object v0, p2, LX/I4k;->b:Ljava/lang/String;

    goto :goto_8

    .line 2534455
    :cond_8
    iget-object v3, v0, LX/I4k;->a:Ljava/lang/String;

    iget-object p1, p2, LX/I4k;->a:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_a

    .line 2534456
    :cond_9
    goto :goto_4

    .line 2534457
    :cond_a
    new-instance v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    sget-object v2, LX/FOw;->OKAY:LX/FOw;

    invoke-direct {v0, v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;-><init>(LX/FOw;)V

    goto :goto_6

    .line 2534458
    :cond_b
    iget-object v0, p0, LX/I4m;->c:Ljava/util/HashSet;

    iget-object v2, p2, LX/I4k;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_5
.end method

.method public final synthetic getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2534522
    invoke-direct {p0, p1, p2}, LX/I4m;->a(II)LX/I4k;

    move-result-object v0

    return-object v0
.end method

.method public final getChildId(II)J
    .locals 2

    .prologue
    .line 2534422
    invoke-direct {p0, p1, p2}, LX/I4m;->a(II)LX/I4k;

    move-result-object v0

    iget-object v0, v0, LX/I4k;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2534386
    invoke-direct {p0, p1, p2}, LX/I4m;->a(II)LX/I4k;

    move-result-object v1

    .line 2534387
    if-nez p4, :cond_2

    .line 2534388
    new-instance v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iget-object v2, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;-><init>(Landroid/content/Context;)V

    .line 2534389
    :goto_0
    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2534390
    iget-object v2, v1, LX/I4k;->d:LX/I4j;

    invoke-direct {p0, v2}, LX/I4m;->a(LX/I4j;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2534391
    iget-object v2, v1, LX/I4k;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2534392
    const v2, 0x7f0e0bcd

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2534393
    iget-boolean v2, v1, LX/I4k;->c:Z

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2534394
    iget-object v2, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setBackgroundColor(I)V

    .line 2534395
    iget-object v2, v1, LX/I4k;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/I4m;->l:LX/0ad;

    sget-short v3, LX/347;->J:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2534396
    iget-object v2, v1, LX/I4k;->e:Ljava/lang/String;

    .line 2534397
    iget-object v3, p0, LX/I4m;->m:LX/0wM;

    .line 2534398
    const/4 v4, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v4, :pswitch_data_0

    .line 2534399
    const v4, 0x7f0209e2

    :goto_2
    move v4, v4

    .line 2534400
    const v5, -0x423e37

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    move-object v2, v3

    .line 2534401
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2534402
    :cond_1
    iget v2, p0, LX/I4m;->h:I

    iget v3, p0, LX/I4m;->j:I

    iget v4, p0, LX/I4m;->i:I

    iget v5, p0, LX/I4m;->j:I

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setPadding(IIII)V

    .line 2534403
    new-instance v2, LX/I4h;

    invoke-direct {v2, p0, v1, p1}, LX/I4h;-><init>(LX/I4m;LX/I4k;I)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534404
    return-object v0

    :cond_2
    move-object v0, p4

    goto :goto_0

    .line 2534405
    :sswitch_0
    const-string v5, "cocktail"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x0

    goto :goto_1

    :sswitch_1
    const-string v5, "fork-knife"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_1

    :sswitch_2
    const-string v5, "music"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x2

    goto :goto_1

    :sswitch_3
    const-string v5, "film-projector"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x3

    goto :goto_1

    :sswitch_4
    const-string v5, "book"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x4

    goto :goto_1

    :sswitch_5
    const-string v5, "ribbon"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x5

    goto :goto_1

    :sswitch_6
    const-string v5, "face-very-happy"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x6

    goto :goto_1

    :sswitch_7
    const-string v5, "group"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x7

    goto :goto_1

    :sswitch_8
    const-string v5, "games"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v4, 0x8

    goto/16 :goto_1

    :sswitch_9
    const-string v5, "hands-praying"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v4, 0x9

    goto/16 :goto_1

    :sswitch_a
    const-string v5, "shopping-bag"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v4, 0xa

    goto/16 :goto_1

    :sswitch_b
    const-string v5, "health"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v4, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string v5, "house"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v4, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v5, "network"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v4, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string v5, "running"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v4, 0xe

    goto/16 :goto_1

    :sswitch_f
    const-string v5, "arts"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v4, 0xf

    goto/16 :goto_1

    .line 2534406
    :pswitch_0
    const v4, 0x7f020805

    goto/16 :goto_2

    .line 2534407
    :pswitch_1
    const v4, 0x7f020885

    goto/16 :goto_2

    .line 2534408
    :pswitch_2
    const v4, 0x7f020939

    goto/16 :goto_2

    .line 2534409
    :pswitch_3
    const v4, 0x7f020875

    goto/16 :goto_2

    .line 2534410
    :pswitch_4
    const v4, 0x7f020780

    goto/16 :goto_2

    .line 2534411
    :pswitch_5
    const v4, 0x7f0209b5

    goto/16 :goto_2

    .line 2534412
    :pswitch_6
    const v4, 0x7f02086b

    goto/16 :goto_2

    .line 2534413
    :pswitch_7
    const v4, 0x7f0208bf

    goto/16 :goto_2

    .line 2534414
    :pswitch_8
    const v4, 0x7f0208aa

    goto/16 :goto_2

    .line 2534415
    :pswitch_9
    const v4, 0x7f0208c7

    goto/16 :goto_2

    .line 2534416
    :pswitch_a
    const v4, 0x7f0209ca

    goto/16 :goto_2

    .line 2534417
    :pswitch_b
    const v4, 0x7f0208cc

    goto/16 :goto_2

    .line 2534418
    :pswitch_c
    const v4, 0x7f0208d4

    goto/16 :goto_2

    .line 2534419
    :pswitch_d
    const v4, 0x7f02093f

    goto/16 :goto_2

    .line 2534420
    :pswitch_e
    const v4, 0x7f0209b9

    goto/16 :goto_2

    .line 2534421
    :pswitch_f
    const v4, 0x7f020768

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7cf1bf07 -> :sswitch_6
        -0x5e1911dd -> :sswitch_a
        -0x48cafda4 -> :sswitch_b
        -0x3781db2a -> :sswitch_5
        -0x350e27dc -> :sswitch_0
        0x2dd270 -> :sswitch_f
        0x2e3ae9 -> :sswitch_4
        0xa3f0ff -> :sswitch_9
        0x5d932c1 -> :sswitch_8
        0x5e0f67f -> :sswitch_7
        0x5edc720 -> :sswitch_c
        0x636ee25 -> :sswitch_2
        0x3200b9b3 -> :sswitch_3
        0x505171da -> :sswitch_1
        0x5c6f15bf -> :sswitch_e
        0x6de15a2e -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public final getChildrenCount(I)I
    .locals 1

    .prologue
    .line 2534385
    invoke-direct {p0, p1}, LX/I4m;->b(I)LX/I4l;

    move-result-object v0

    iget-object v0, v0, LX/I4l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2534384
    invoke-direct {p0, p1}, LX/I4m;->b(I)LX/I4l;

    move-result-object v0

    return-object v0
.end method

.method public final getGroupCount()I
    .locals 1

    .prologue
    .line 2534383
    iget-object v0, p0, LX/I4m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getGroupId(I)J
    .locals 2

    .prologue
    .line 2534382
    invoke-direct {p0, p1}, LX/I4m;->b(I)LX/I4l;

    move-result-object v0

    iget v0, v0, LX/I4l;->a:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2534369
    invoke-direct {p0, p1}, LX/I4m;->b(I)LX/I4l;

    move-result-object v2

    .line 2534370
    if-nez p3, :cond_0

    .line 2534371
    new-instance v1, LX/I4t;

    iget-object v0, p0, LX/I4m;->k:Landroid/content/Context;

    invoke-direct {v1, v0}, LX/I4t;-><init>(Landroid/content/Context;)V

    :goto_0
    move-object v0, v1

    .line 2534372
    check-cast v0, LX/I4t;

    .line 2534373
    iget-object p0, v0, LX/I4t;->j:Lcom/facebook/fbui/glyph/GlyphView;

    iget p1, v2, LX/I4l;->c:I

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2534374
    iget-object p1, v0, LX/I4t;->j:Lcom/facebook/fbui/glyph/GlyphView;

    iget-boolean p0, v2, LX/I4l;->f:Z

    if-eqz p0, :cond_1

    const p0, -0xbd984e

    :goto_1
    invoke-virtual {p1, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2534375
    iget-object p0, v0, LX/I4t;->k:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, v2, LX/I4l;->d:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534376
    iget-object p0, v2, LX/I4l;->e:Ljava/lang/String;

    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 2534377
    iget-object p0, v0, LX/I4t;->l:Lcom/facebook/resources/ui/FbTextView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2534378
    iget-object p0, v0, LX/I4t;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, v2, LX/I4l;->e:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534379
    :goto_2
    return-object v1

    :cond_0
    move-object v1, p3

    goto :goto_0

    .line 2534380
    :cond_1
    const p0, -0x413d37

    goto :goto_1

    .line 2534381
    :cond_2
    iget-object p0, v0, LX/I4t;->l:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2534368
    const/4 v0, 0x1

    return v0
.end method

.method public final isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 2534367
    const/4 v0, 0x0

    return v0
.end method
