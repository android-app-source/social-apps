.class public final LX/HFp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/os/Bundle;

.field public final synthetic b:J

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Lcom/facebook/share/model/ComposerAppAttribution;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2444200
    iput-object p1, p0, LX/HFp;->g:Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    iput-object p2, p0, LX/HFp;->a:Landroid/os/Bundle;

    iput-wide p3, p0, LX/HFp;->b:J

    iput-object p5, p0, LX/HFp;->c:Ljava/lang/String;

    iput-object p6, p0, LX/HFp;->d:Ljava/lang/String;

    iput-object p7, p0, LX/HFp;->e:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object p8, p0, LX/HFp;->f:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2444201
    iget-object v0, p0, LX/HFp;->g:Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->e:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->j:Ljava/lang/String;

    const-string v2, "fetch vc failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2444202
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2444203
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2444204
    if-eqz p1, :cond_0

    .line 2444205
    iget-object v0, p0, LX/HFp;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2444206
    iget-object v0, p0, LX/HFp;->g:Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQX;

    .line 2444207
    iget-object v2, v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 2444208
    iget-object v3, v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2444209
    invoke-virtual {v0, v2, v3}, LX/BQX;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 2444210
    iget-object v0, p0, LX/HFp;->g:Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQc;

    iget-wide v2, p0, LX/HFp;->b:J

    iget-object v4, p0, LX/HFp;->c:Ljava/lang/String;

    iget-object v5, p0, LX/HFp;->d:Ljava/lang/String;

    iget-object v6, p0, LX/HFp;->e:Lcom/facebook/share/model/ComposerAppAttribution;

    iget-object v7, p0, LX/HFp;->f:Ljava/lang/String;

    move-object v8, p1

    invoke-virtual/range {v0 .. v8}, LX/BQc;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2444211
    :cond_0
    return-void
.end method
