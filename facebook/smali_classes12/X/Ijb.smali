.class public LX/Ijb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6yL;


# instance fields
.field private a:LX/6qh;

.field private final b:LX/6ye;


# direct methods
.method public constructor <init>(LX/6ye;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605927
    iput-object p1, p0, LX/Ijb;->b:LX/6ye;

    .line 2605928
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 2605929
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2605930
    const-string v1, "cvv_code"

    iget-object v2, p2, LX/6y8;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2605931
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2605932
    const-string v2, "extra_activity_result_data"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2605933
    new-instance v0, LX/73T;

    sget-object v2, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v0, v2, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 2605934
    iget-object v1, p0, LX/Ijb;->a:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(LX/73T;)V

    .line 2605935
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 2605936
    iget-object v0, p0, LX/Ijb;->b:LX/6ye;

    invoke-virtual {v0, p1, p2}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6qh;)V
    .locals 1

    .prologue
    .line 2605937
    iput-object p1, p0, LX/Ijb;->a:LX/6qh;

    .line 2605938
    iget-object v0, p0, LX/Ijb;->b:LX/6ye;

    invoke-virtual {v0, p1}, LX/6ye;->a(LX/6qh;)V

    .line 2605939
    return-void
.end method
