.class public final LX/Ir3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V
    .locals 0

    .prologue
    .line 2617395
    iput-object p1, p0, LX/Ir3;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2617396
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_3

    const/4 v2, 0x4

    if-ne p2, v2, :cond_3

    .line 2617397
    iget-object v2, p0, LX/Ir3;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/Ir3;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    const/4 p1, 0x1

    const/4 v3, 0x0

    .line 2617398
    sget-object p2, LX/IrO;->a:[I

    iget-object p3, v2, LX/IrP;->w:LX/Iqe;

    invoke-virtual {p3}, LX/Iqe;->ordinal()I

    move-result p3

    aget p2, p2, p3

    packed-switch p2, :pswitch_data_0

    .line 2617399
    :cond_0
    :goto_0
    :pswitch_0
    move v2, v3

    .line 2617400
    if-eqz v2, :cond_1

    .line 2617401
    :goto_1
    return v0

    .line 2617402
    :cond_1
    iget-object v2, p0, LX/Ir3;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-static {v2}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->m(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2617403
    iget-object v1, p0, LX/Ir3;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-static {v1}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->n(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2617404
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2617405
    goto :goto_1

    .line 2617406
    :pswitch_1
    iget-object p2, v2, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {p2}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2617407
    iget-object v3, v2, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v3}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->b()V

    move v3, p1

    .line 2617408
    goto :goto_0

    .line 2617409
    :pswitch_2
    const/4 p1, 0x1

    const/4 v3, 0x0

    .line 2617410
    iget-object p2, v2, LX/IrP;->s:LX/IqW;

    if-nez p2, :cond_5

    .line 2617411
    :cond_4
    :goto_2
    move v3, v3

    .line 2617412
    goto :goto_0

    .line 2617413
    :pswitch_3
    sget-object v3, LX/Iqe;->IDLE:LX/Iqe;

    invoke-static {v2, v3}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    move v3, p1

    .line 2617414
    goto :goto_0

    :pswitch_4
    move v3, p1

    .line 2617415
    goto :goto_0

    .line 2617416
    :cond_5
    invoke-static {v2}, LX/IrP;->o(LX/IrP;)Z

    move-result p2

    .line 2617417
    if-eqz p2, :cond_6

    iget-object p3, v2, LX/IrP;->s:LX/IqW;

    invoke-interface {p3}, LX/IqW;->a()Z

    move-result p3

    if-eqz p3, :cond_6

    move v3, p1

    .line 2617418
    goto :goto_2

    .line 2617419
    :cond_6
    if-eqz p2, :cond_4

    .line 2617420
    sget-object v3, LX/Iqe;->IDLE:LX/Iqe;

    invoke-static {v2, v3}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    move v3, p1

    .line 2617421
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
