.class public LX/IUk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IQB;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/IRb;

.field private final c:Landroid/content/res/Resources;

.field public final d:LX/3mF;

.field private final e:LX/0Sh;

.field private final f:LX/0Zb;

.field public g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field private h:Landroid/net/Uri;

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2580993
    const-class v0, LX/IUk;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IUk;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/IRb;LX/J7Y;Landroid/content/res/Resources;LX/0Sh;LX/3mF;LX/0Zb;)V
    .locals 2
    .param p1    # LX/IRb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2581010
    iput-object p1, p0, LX/IUk;->b:LX/IRb;

    .line 2581011
    iput-object p4, p0, LX/IUk;->e:LX/0Sh;

    .line 2581012
    iput-object p5, p0, LX/IUk;->d:LX/3mF;

    .line 2581013
    new-instance v0, LX/J7W;

    invoke-direct {v0}, LX/J7W;-><init>()V

    const-string v1, "invitedMemberMegaphone"

    .line 2581014
    iput-object v1, v0, LX/J7W;->a:Ljava/lang/String;

    .line 2581015
    move-object v0, v0

    .line 2581016
    const-string v1, "Groups"

    .line 2581017
    iput-object v1, v0, LX/J7W;->b:Ljava/lang/String;

    .line 2581018
    move-object v0, v0

    .line 2581019
    const-string v1, "png"

    .line 2581020
    iput-object v1, v0, LX/J7W;->c:Ljava/lang/String;

    .line 2581021
    move-object v0, v0

    .line 2581022
    new-instance v1, LX/J7X;

    invoke-direct {v1, v0}, LX/J7X;-><init>(LX/J7W;)V

    move-object v0, v1

    .line 2581023
    iget-object v1, p2, LX/J7Y;->a:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 2581024
    new-instance p1, Ljava/lang/StringBuilder;

    const-string p4, "https://lookaside.facebook.com/assets/"

    invoke-direct {p1, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2581025
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {v0, p4}, LX/J7X;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p4

    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p4

    if-nez p4, :cond_0

    .line 2581026
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J7X;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2581027
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2581028
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/IUk;->h:Landroid/net/Uri;

    .line 2581029
    iput-object p3, p0, LX/IUk;->c:Landroid/content/res/Resources;

    .line 2581030
    iput-object p6, p0, LX/IUk;->f:LX/0Zb;

    .line 2581031
    return-void

    .line 2581032
    :cond_0
    iget-object p4, v0, LX/J7X;->b:Ljava/lang/String;

    move-object p4, p4

    .line 2581033
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2581034
    const-string p4, "/"

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2581035
    iget-object p4, v0, LX/J7X;->a:Ljava/lang/String;

    move-object p4, p4

    .line 2581036
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2581037
    const-string p4, "@"

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2581038
    const/16 p4, 0x78

    if-gt v1, p4, :cond_1

    .line 2581039
    const-string p4, "0.75x"

    .line 2581040
    :goto_1
    move-object v1, p4

    .line 2581041
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2581042
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2581043
    iget-object v1, v0, LX/J7X;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2581044
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2581045
    :cond_1
    const/16 p4, 0xa0

    if-gt v1, p4, :cond_2

    .line 2581046
    const-string p4, "1x"

    goto :goto_1

    .line 2581047
    :cond_2
    const/16 p4, 0xf0

    if-gt v1, p4, :cond_3

    .line 2581048
    const-string p4, "1.5x"

    goto :goto_1

    .line 2581049
    :cond_3
    const/16 p4, 0x140

    if-gt v1, p4, :cond_4

    .line 2581050
    const-string p4, "2x"

    goto :goto_1

    .line 2581051
    :cond_4
    const/16 p4, 0x1e0

    if-gt v1, p4, :cond_5

    .line 2581052
    const-string p4, "3x"

    goto :goto_1

    .line 2581053
    :cond_5
    const/16 p4, 0x280

    if-gt v1, p4, :cond_6

    .line 2581054
    const-string p4, "4x"

    goto :goto_1

    .line 2581055
    :cond_6
    const-string p4, "4x"

    goto :goto_1
.end method

.method public static a$redex0(LX/IUk;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2

    .prologue
    .line 2581060
    iget-object v0, p0, LX/IUk;->b:LX/IRb;

    invoke-interface {v0, p1, p2}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2581061
    iget-object v0, p0, LX/IUk;->e:LX/0Sh;

    new-instance v1, LX/IUj;

    invoke-direct {v1, p0, p2, p1}, LX/IUj;-><init>(LX/IUk;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v0, p3, v1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2581062
    return-void
.end method

.method public static g(LX/IUk;)Z
    .locals 2

    .prologue
    .line 2581056
    iget-object v0, p0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel$InviterModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2581057
    :cond_0
    sget-object v0, LX/IUk;->a:Ljava/lang/String;

    const-string v1, "Invitation model is null"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2581058
    const/4 v0, 0x0

    .line 2581059
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2581000
    iget-boolean v0, p0, LX/IUk;->i:Z

    if-eqz v0, :cond_0

    .line 2581001
    iget-object v0, p0, LX/IUk;->c:Landroid/content/res/Resources;

    const v1, 0x7f081c0e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel$InviterModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel$InviterModel;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2581002
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IUk;->c:Landroid/content/res/Resources;

    const v1, 0x7f081c0d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2581003
    iget-boolean v0, p0, LX/IUk;->i:Z

    if-eqz v0, :cond_0

    .line 2581004
    iget-object v0, p0, LX/IUk;->c:Landroid/content/res/Resources;

    const v1, 0x7f081c10

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2581005
    :goto_0
    return-object v0

    .line 2581006
    :cond_0
    invoke-static {p0}, LX/IUk;->g(LX/IUk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2581007
    iget-object v0, p0, LX/IUk;->c:Landroid/content/res/Resources;

    const v1, 0x7f081c11

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel$InviterModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel$InviterModel;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2581008
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2580997
    iget-boolean v0, p0, LX/IUk;->i:Z

    if-eqz v0, :cond_0

    .line 2580998
    iget-object v0, p0, LX/IUk;->c:Landroid/content/res/Resources;

    const v1, 0x7f081c16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2580999
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IUk;->c:Landroid/content/res/Resources;

    const v1, 0x7f081c14

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2580996
    iget-object v0, p0, LX/IUk;->c:Landroid/content/res/Resources;

    const v1, 0x7f081c12

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2580995
    new-instance v0, LX/IUh;

    invoke-direct {v0, p0}, LX/IUh;-><init>(LX/IUk;)V

    return-object v0
.end method

.method public final f()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2580994
    new-instance v0, LX/IUi;

    invoke-direct {v0, p0}, LX/IUi;-><init>(LX/IUk;)V

    return-object v0
.end method
