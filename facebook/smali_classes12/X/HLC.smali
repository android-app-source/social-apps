.class public final LX/HLC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HIU;


# instance fields
.field public final synthetic a:LX/HLD;


# direct methods
.method public constructor <init>(LX/HLD;)V
    .locals 0

    .prologue
    .line 2455644
    iput-object p1, p0, LX/HLC;->a:LX/HLD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/HIR;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 2455645
    iget-object v0, p0, LX/HLC;->a:LX/HLD;

    iget-object v0, v0, LX/HLD;->d:LX/9XE;

    iget-object v1, p0, LX/HLC;->a:LX/HLD;

    iget-object v1, v1, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455646
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v2

    .line 2455647
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v1, p1, LX/HIR;->a:Ljava/lang/String;

    iget-boolean v2, p1, LX/HIR;->d:Z

    .line 2455648
    iget-object v6, v0, LX/9XE;->a:LX/0Zb;

    sget-object v7, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_LIKE:LX/9XI;

    invoke-static {v7, v4, v5}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v9, "does_viewer_like"

    invoke-virtual {v7, v9, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v9, "tapped_page_id"

    invoke-virtual {v7, v9, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2455649
    iget-object v0, p0, LX/HLC;->a:LX/HLD;

    iget-object v0, v0, LX/HLD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/961;

    iget-object v1, p1, LX/HIR;->a:Ljava/lang/String;

    iget-boolean v2, p1, LX/HIR;->d:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v4, "pages_public_view"

    const-string v6, "city_hub_social_module"

    new-instance v9, LX/HLB;

    invoke-direct {v9, p0, p1}, LX/HLB;-><init>(LX/HLC;LX/HIR;)V

    move-object v5, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v9}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    .line 2455650
    return-void

    :cond_0
    move v2, v8

    .line 2455651
    goto :goto_0
.end method
