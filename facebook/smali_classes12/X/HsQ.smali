.class public final enum LX/HsQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HsQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HsQ;

.field public static final enum DATE:LX/HsQ;

.field public static final enum EDUCATION_PERIOD:LX/HsQ;

.field public static final enum WORK_PERIOD:LX/HsQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2514390
    new-instance v0, LX/HsQ;

    const-string v1, "DATE"

    invoke-direct {v0, v1, v2}, LX/HsQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HsQ;->DATE:LX/HsQ;

    .line 2514391
    new-instance v0, LX/HsQ;

    const-string v1, "WORK_PERIOD"

    invoke-direct {v0, v1, v3}, LX/HsQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HsQ;->WORK_PERIOD:LX/HsQ;

    .line 2514392
    new-instance v0, LX/HsQ;

    const-string v1, "EDUCATION_PERIOD"

    invoke-direct {v0, v1, v4}, LX/HsQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HsQ;->EDUCATION_PERIOD:LX/HsQ;

    .line 2514393
    const/4 v0, 0x3

    new-array v0, v0, [LX/HsQ;

    sget-object v1, LX/HsQ;->DATE:LX/HsQ;

    aput-object v1, v0, v2

    sget-object v1, LX/HsQ;->WORK_PERIOD:LX/HsQ;

    aput-object v1, v0, v3

    sget-object v1, LX/HsQ;->EDUCATION_PERIOD:LX/HsQ;

    aput-object v1, v0, v4

    sput-object v0, LX/HsQ;->$VALUES:[LX/HsQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2514394
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HsQ;
    .locals 1

    .prologue
    .line 2514395
    const-class v0, LX/HsQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HsQ;

    return-object v0
.end method

.method public static values()[LX/HsQ;
    .locals 1

    .prologue
    .line 2514396
    sget-object v0, LX/HsQ;->$VALUES:[LX/HsQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HsQ;

    return-object v0
.end method
