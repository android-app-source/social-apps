.class public final LX/JF7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/android/gms/maps/model/LatLng;

.field public e:F

.field public f:Lcom/google/android/gms/maps/model/LatLngBounds;

.field public g:Landroid/net/Uri;

.field public h:Z

.field public i:F

.field public j:I

.field public k:J

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LX/JF7;->a:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/location/places/internal/PlaceEntity;
    .locals 30

    new-instance v5, Lcom/google/android/gms/location/places/internal/PlaceEntity;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/JF7;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/JF7;->l:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, LX/JF7;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/JF7;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/JF7;->n:Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, LX/JF7;->o:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/JF7;->d:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/JF7;->e:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/JF7;->f:Lcom/google/android/gms/maps/model/LatLngBounds;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/JF7;->g:Landroid/net/Uri;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/JF7;->h:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/JF7;->i:F

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/JF7;->j:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/JF7;->k:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v4, v0, LX/JF7;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/JF7;->m:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/JF7;->n:Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/JF7;->o:Ljava/util/List;

    move-object/from16 v29, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v29

    invoke-static {v4, v0, v1, v2, v3}, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/location/places/internal/PlaceLocalization;

    move-result-object v26

    invoke-direct/range {v5 .. v26}, Lcom/google/android/gms/location/places/internal/PlaceEntity;-><init>(ILjava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/maps/model/LatLng;FLcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Landroid/net/Uri;ZFIJLcom/google/android/gms/location/places/internal/PlaceLocalization;)V

    return-object v5
.end method
