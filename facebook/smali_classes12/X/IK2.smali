.class public LX/IK2;
.super LX/1OM;
.source ""

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Ljava/util/Observer;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/IKw;

.field private final c:LX/IKu;

.field private final d:LX/IKo;

.field private final e:LX/IKr;

.field private final f:LX/IK9;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/IKw;LX/IKu;LX/IKo;LX/IKr;LX/IK9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2564912
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2564913
    iput-object p1, p0, LX/IK2;->a:Landroid/content/res/Resources;

    .line 2564914
    iput-object p2, p0, LX/IK2;->b:LX/IKw;

    .line 2564915
    iput-object p3, p0, LX/IK2;->c:LX/IKu;

    .line 2564916
    iput-object p4, p0, LX/IK2;->d:LX/IKo;

    .line 2564917
    iput-object p5, p0, LX/IK2;->e:LX/IKr;

    .line 2564918
    iput-object p6, p0, LX/IK2;->f:LX/IK9;

    .line 2564919
    iget-object v0, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v0, p0}, LX/IK9;->addObserver(Ljava/util/Observer;)V

    .line 2564920
    return-void
.end method

.method public static a(LX/0QB;)LX/IK2;
    .locals 10

    .prologue
    .line 2564807
    const-class v1, LX/IK2;

    monitor-enter v1

    .line 2564808
    :try_start_0
    sget-object v0, LX/IK2;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2564809
    sput-object v2, LX/IK2;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2564810
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2564811
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2564812
    new-instance v3, LX/IK2;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    const-class v5, LX/IKw;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/IKw;

    const-class v6, LX/IKu;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/IKu;

    const-class v7, LX/IKo;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/IKo;

    const-class v8, LX/IKr;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/IKr;

    invoke-static {v0}, LX/IK9;->a(LX/0QB;)LX/IK9;

    move-result-object v9

    check-cast v9, LX/IK9;

    invoke-direct/range {v3 .. v9}, LX/IK2;-><init>(Landroid/content/res/Resources;LX/IKw;LX/IKu;LX/IKo;LX/IKr;LX/IK9;)V

    .line 2564813
    move-object v0, v3

    .line 2564814
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2564815
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IK2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2564816
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2564817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private d()I
    .locals 2

    .prologue
    .line 2564909
    iget-object v0, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v0}, LX/IK9;->g()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 2564910
    :goto_0
    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, LX/IK2;->e()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 2564911
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()I
    .locals 1

    .prologue
    .line 2564906
    iget-object v0, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v0}, LX/IK9;->d()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 2564907
    :goto_0
    add-int/lit8 v0, v0, 0x1

    return v0

    .line 2564908
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/IK2;)I
    .locals 2

    .prologue
    .line 2564905
    invoke-direct {p0}, LX/IK2;->e()I

    move-result v0

    iget-object v1, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v1}, LX/IK9;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2564881
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2564882
    packed-switch p2, :pswitch_data_0

    .line 2564883
    :pswitch_0
    const v1, 0x7f0307fa

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2564884
    iget-object v1, p0, LX/IK2;->b:LX/IKw;

    .line 2564885
    new-instance p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;

    invoke-static {v1}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v2

    check-cast v2, LX/11S;

    const/16 p1, 0x2377

    invoke-static {v1, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-direct {p0, v0, v2, p1}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;-><init>(Landroid/view/View;LX/11S;LX/0Ot;)V

    .line 2564886
    move-object v0, p0

    .line 2564887
    :goto_0
    return-object v0

    .line 2564888
    :pswitch_1
    const v1, 0x7f0307f8

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2564889
    iget-object v1, p0, LX/IK2;->d:LX/IKo;

    .line 2564890
    new-instance p1, LX/IKn;

    invoke-static {v1}, LX/IK6;->a(LX/0QB;)LX/IK6;

    move-result-object v2

    check-cast v2, LX/IK6;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    invoke-direct {p1, v0, v2, p0}, LX/IKn;-><init>(Landroid/view/View;LX/IK6;LX/0wM;)V

    .line 2564891
    move-object v0, p1

    .line 2564892
    goto :goto_0

    .line 2564893
    :pswitch_2
    const v1, 0x7f0307f9

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2564894
    iget-object v1, p0, LX/IK2;->e:LX/IKr;

    .line 2564895
    new-instance p0, LX/IKq;

    invoke-direct {p0, v0}, LX/IKq;-><init>(Landroid/view/View;)V

    .line 2564896
    invoke-static {v1}, LX/IK6;->a(LX/0QB;)LX/IK6;

    move-result-object v2

    check-cast v2, LX/IK6;

    .line 2564897
    iput-object v2, p0, LX/IKq;->l:LX/IK6;

    .line 2564898
    move-object v0, p0

    .line 2564899
    goto :goto_0

    .line 2564900
    :pswitch_3
    const v1, 0x7f030282

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2564901
    iget-object v1, p0, LX/IK2;->c:LX/IKu;

    .line 2564902
    new-instance p0, LX/IKt;

    invoke-static {v1}, LX/IK6;->a(LX/0QB;)LX/IK6;

    move-result-object v2

    check-cast v2, LX/IK6;

    invoke-direct {p0, v0, v2}, LX/IKt;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;LX/IK6;)V

    .line 2564903
    move-object v0, p0

    .line 2564904
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2564832
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2564833
    :goto_0
    return-void

    .line 2564834
    :pswitch_0
    check-cast p1, LX/IKn;

    .line 2564835
    iget-object v0, p0, LX/IK2;->f:LX/IK9;

    .line 2564836
    iget-object v1, p1, LX/IKn;->l:Lcom/facebook/fig/listitem/FigListItem;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2564837
    iget-object v1, p1, LX/IKn;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 2564838
    iget p0, v1, Lcom/facebook/fig/listitem/FigListItem;->j:I

    move v1, p0

    .line 2564839
    invoke-static {v1}, LX/6WK;->d(I)I

    move-result v1

    .line 2564840
    iget-object p0, p1, LX/IKn;->l:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2564841
    iget-object p0, v0, LX/IK9;->a:LX/0WJ;

    invoke-virtual {p0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object p0

    move-object p0, p0

    .line 2564842
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object p0

    invoke-virtual {p0, v1}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    .line 2564843
    iget-object p0, p1, LX/IKn;->l:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2564844
    iget-object v1, p1, LX/IKn;->n:LX/IK6;

    iget-object p0, p1, LX/IKn;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 2564845
    iget-boolean p2, v1, LX/IK6;->m:Z

    if-eqz p2, :cond_1

    .line 2564846
    :goto_1
    goto :goto_0

    .line 2564847
    :pswitch_1
    check-cast p1, LX/IKq;

    .line 2564848
    invoke-static {p0}, LX/IK2;->f(LX/IK2;)I

    move-result v0

    if-ne p2, v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2564849
    if-eqz v0, :cond_4

    .line 2564850
    iget-object v1, p1, LX/IKq;->n:Lcom/facebook/fig/sectionheader/FigSectionHeader;

    const p0, 0x7f08389c

    invoke-virtual {v1, p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(I)V

    .line 2564851
    iget-object v1, p1, LX/IKq;->o:Lcom/facebook/resources/ui/FbTextView;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2564852
    :goto_3
    goto :goto_0

    .line 2564853
    :pswitch_2
    check-cast p1, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;

    .line 2564854
    invoke-static {p0}, LX/IK2;->f(LX/IK2;)I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 2564855
    iget-object v0, p0, LX/IK2;->f:LX/IK9;

    .line 2564856
    iget-object v1, v0, LX/IK9;->c:LX/2kM;

    move-object v0, v1

    .line 2564857
    invoke-direct {p0}, LX/IK2;->e()I

    move-result v1

    sub-int v1, p2, v1

    invoke-interface {v0, v1}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;

    invoke-virtual {p1, v0}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->a(Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;)V

    goto :goto_0

    .line 2564858
    :cond_0
    invoke-direct {p0}, LX/IK2;->d()I

    move-result v0

    sub-int v0, p2, v0

    iget-object v1, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v1}, LX/IK9;->d()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 2564859
    iget-object v1, p0, LX/IK2;->f:LX/IK9;

    .line 2564860
    iget-object p0, v1, LX/IK9;->d:LX/2kM;

    move-object v1, p0

    .line 2564861
    invoke-interface {v1, v0}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    invoke-virtual {p1, v0}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->a(Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;)V

    goto/16 :goto_0

    .line 2564862
    :pswitch_3
    check-cast p1, LX/IKt;

    .line 2564863
    iget-object v0, p0, LX/IK2;->f:LX/IK9;

    .line 2564864
    iget-object v1, v0, LX/IK9;->f:LX/1lD;

    move-object v0, v1

    .line 2564865
    sget-object v1, LX/IKs;->a:[I

    invoke-virtual {v0}, LX/1lD;->ordinal()I

    move-result p0

    aget v1, v1, p0

    packed-switch v1, :pswitch_data_1

    .line 2564866
    iget-object v1, p1, LX/IKt;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2564867
    :goto_4
    goto/16 :goto_0

    .line 2564868
    :cond_1
    iget-object p2, v1, LX/IK6;->j:LX/0iA;

    new-instance p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ROOMS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {p1, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v0, LX/IK3;

    invoke-virtual {p2, p1, v0}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object p2

    check-cast p2, LX/IK3;

    .line 2564869
    if-eqz p2, :cond_2

    .line 2564870
    new-instance p2, LX/0hs;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x2

    invoke-direct {p2, p1, v0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2564871
    const p1, 0x7f0838a5

    invoke-virtual {p2, p1}, LX/0hs;->a(I)V

    .line 2564872
    const p1, 0x7f0838a6

    invoke-virtual {p2, p1}, LX/0hs;->b(I)V

    .line 2564873
    sget-object p1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {p2, p1}, LX/0ht;->a(LX/3AV;)V

    .line 2564874
    invoke-virtual {p2, p0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2564875
    iget-object p2, v1, LX/IK6;->j:LX/0iA;

    invoke-virtual {p2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object p2

    const-string p1, "4559"

    invoke-virtual {p2, p1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2564876
    :cond_2
    const/4 p2, 0x1

    iput-boolean p2, v1, LX/IK6;->m:Z

    goto/16 :goto_1

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2564877
    :cond_4
    iget-object v1, p1, LX/IKq;->n:Lcom/facebook/fig/sectionheader/FigSectionHeader;

    const p0, 0x7f08389b

    invoke-virtual {v1, p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(I)V

    .line 2564878
    iget-object v1, p1, LX/IKq;->o:Lcom/facebook/resources/ui/FbTextView;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2564879
    :pswitch_4
    iget-object v1, p1, LX/IKt;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_4

    .line 2564880
    :pswitch_5
    iget-object v1, p1, LX/IKt;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 p0, 0x0

    invoke-virtual {v1, p1, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1DI;Ljava/lang/Runnable;)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2564821
    iget-object v0, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v0}, LX/IK9;->g()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 2564822
    :goto_0
    iget-object v3, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v3}, LX/IK9;->d()I

    move-result v3

    if-lez v3, :cond_1

    move v3, v1

    .line 2564823
    :goto_1
    if-nez p1, :cond_2

    .line 2564824
    :goto_2
    return v2

    :cond_0
    move v0, v2

    .line 2564825
    goto :goto_0

    :cond_1
    move v3, v2

    .line 2564826
    goto :goto_1

    .line 2564827
    :cond_2
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_3

    .line 2564828
    const/4 v2, 0x3

    goto :goto_2

    .line 2564829
    :cond_3
    if-eqz v3, :cond_4

    if-eq p1, v1, :cond_5

    :cond_4
    if-eqz v0, :cond_6

    invoke-static {p0}, LX/IK2;->f(LX/IK2;)I

    move-result v0

    if-ne p1, v0, :cond_6

    :cond_5
    move v2, v1

    .line 2564830
    goto :goto_2

    .line 2564831
    :cond_6
    const/4 v2, 0x2

    goto :goto_2
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2564820
    iget-object v0, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v0}, LX/IK9;->d()I

    move-result v0

    iget-object v1, p0, LX/IK2;->f:LX/IK9;

    invoke-virtual {v1}, LX/IK9;->g()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0}, LX/IK2;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2564818
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2564819
    return-void
.end method
