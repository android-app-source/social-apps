.class public final LX/JGE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JGB;


# instance fields
.field public final synthetic a:LX/5pG;

.field public final synthetic b:Lcom/facebook/react/bridge/Callback;

.field public final synthetic c:LX/JGL;


# direct methods
.method public constructor <init>(LX/JGL;LX/5pG;Lcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 2666907
    iput-object p1, p0, LX/JGE;->c:LX/JGL;

    iput-object p2, p0, LX/JGE;->a:LX/5pG;

    iput-object p3, p0, LX/JGE;->b:Lcom/facebook/react/bridge/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/JG9;)V
    .locals 4

    .prologue
    .line 2666908
    :try_start_0
    invoke-virtual {p1}, LX/JG9;->b()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2666909
    iget-object v0, p0, LX/JGE;->b:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2666910
    return-void

    .line 2666911
    :catch_0
    move-exception v0

    .line 2666912
    new-instance v1, LX/5p9;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not play audio: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/JGE;->a:LX/5pG;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/5p9;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
