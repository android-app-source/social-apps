.class public final LX/Igd;
.super LX/6LW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6LW",
        "<",
        "Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;",
        "LX/0Px",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V
    .locals 0

    .prologue
    .line 2601401
    iput-object p1, p0, LX/Igd;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-direct {p0}, LX/6LW;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2601353
    check-cast p2, LX/0Px;

    .line 2601354
    iget-object v0, p0, LX/Igd;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    .line 2601355
    const/4 v3, 0x0

    .line 2601356
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2601357
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601358
    iget-object v5, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    iget-wide v7, v1, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2601359
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2601360
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    if-eqz v1, :cond_2

    .line 2601361
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    .line 2601362
    iget-object v2, v1, LX/Igt;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    move-object v1, v2

    .line 2601363
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2601364
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 2601365
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2601366
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    invoke-virtual {v1, v7, v8}, LX/Igt;->a(J)V

    .line 2601367
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2601368
    :cond_2
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->D:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 2601369
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2601370
    iget-object v4, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v4, v1}, LX/Igt;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    goto :goto_2

    .line 2601371
    :cond_4
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->v:LX/Ign;

    iget-object v2, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    .line 2601372
    iget-object v4, v1, LX/Ign;->j:LX/Igt;

    if-eqz v4, :cond_5

    .line 2601373
    iget-object v4, v1, LX/Ign;->j:LX/Igt;

    .line 2601374
    iget-object v5, v4, LX/Igt;->b:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2601375
    :cond_5
    iput-object v2, v1, LX/Ign;->j:LX/Igt;

    .line 2601376
    iget-object v4, v1, LX/Ign;->j:LX/Igt;

    invoke-virtual {v4, v1}, LX/Igt;->a(LX/Ige;)V

    .line 2601377
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->v:LX/Ign;

    new-instance v2, LX/Igc;

    invoke-direct {v2, v0}, LX/Igc;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    .line 2601378
    iput-object v2, v1, LX/Ign;->i:LX/Igc;

    .line 2601379
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->v:LX/Ign;

    iget-object v2, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2601380
    sget-object v4, Lcom/facebook/ui/media/attachments/MediaResource;->a:Ljava/util/Comparator;

    invoke-static {v4}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1sm;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v1, LX/Ign;->l:Ljava/util/List;

    .line 2601381
    iget-object v4, v1, LX/Ign;->l:Ljava/util/List;

    iget-object v5, v1, LX/Ign;->a:LX/0Rl;

    invoke-static {v4, v5}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-static {v4}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v1, LX/Ign;->m:Ljava/util/List;

    .line 2601382
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2601383
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2601384
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2601385
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2601386
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2601387
    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2601388
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2601389
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2601390
    const v2, 0x7f0d1b17

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2601391
    :cond_6
    :goto_3
    invoke-static {v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->o(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    .line 2601392
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->v:LX/Ign;

    iget-object v2, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    invoke-virtual {v1, v2}, LX/Ign;->a(LX/Igi;)V

    .line 2601393
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/CountBadge;->setVisibility(I)V

    .line 2601394
    return-void

    .line 2601395
    :cond_7
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2601396
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601397
    iget-boolean v2, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    move v1, v2

    .line 2601398
    if-nez v1, :cond_6

    .line 2601399
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2601400
    const v2, 0x7f0d1b17

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2601349
    iget-object v0, p0, LX/Igd;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    const/4 v1, 0x1

    .line 2601350
    new-instance p1, LX/0ju;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {p1, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    if-eqz v1, :cond_0

    const p0, 0x7f082dfa

    :goto_0
    invoke-virtual {p1, p0}, LX/0ju;->a(I)LX/0ju;

    move-result-object p0

    const p1, 0x7f082dfc

    invoke-virtual {p0, p1}, LX/0ju;->b(I)LX/0ju;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object p0

    const p1, 0x7f080036

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0}, LX/0ju;->b()LX/2EJ;

    .line 2601351
    return-void

    .line 2601352
    :cond_0
    const p0, 0x7f082dfb

    goto :goto_0
.end method
