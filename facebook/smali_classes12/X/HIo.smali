.class public final LX/HIo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/9uc;

.field public final synthetic c:LX/2km;

.field public final synthetic d:Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;Ljava/lang/String;LX/9uc;LX/2km;)V
    .locals 0

    .prologue
    .line 2451782
    iput-object p1, p0, LX/HIo;->d:Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;

    iput-object p2, p0, LX/HIo;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HIo;->b:LX/9uc;

    iput-object p4, p0, LX/HIo;->c:LX/2km;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/4 v2, 0x1

    const v3, -0x6f44f490

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2451783
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/HIo;->a:Ljava/lang/String;

    iget-object v0, p0, LX/HIo;->b:LX/9uc;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HIo;->b:LX/9uc;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->F()LX/1k1;

    move-result-object v0

    invoke-interface {v0}, LX/1k1;->a()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_0
    iget-object v2, p0, LX/HIo;->b:LX/9uc;

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/HIo;->b:LX/9uc;

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->F()LX/1k1;

    move-result-object v2

    invoke-interface {v2}, LX/1k1;->b()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    :goto_1
    const-string v6, "pages_local_card_get_directions_request_ride"

    invoke-static {v5, v0, v2, v1, v6}, LX/CK5;->a(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 2451784
    iget-object v0, p0, LX/HIo;->d:Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v2, LX/9XI;->EVENT_TAPPED_REQUEST_RIDE:LX/9XI;

    iget-object v4, p0, LX/HIo;->b:LX/9uc;

    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, LX/9XE;->a(LX/9X2;J)V

    .line 2451785
    iget-object v0, p0, LX/HIo;->d:Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;

    iget-object v2, v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/HIo;->c:LX/2km;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2451786
    const v0, -0x45c5f9c6

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    :cond_0
    move-object v0, v1

    .line 2451787
    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1
.end method
