.class public LX/JN3;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public final c:Lcom/facebook/events/widget/eventcard/EventsCardView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2685129
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JN3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2685130
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2685122
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2685123
    new-instance v0, Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-direct {v0, p1}, Lcom/facebook/events/widget/eventcard/EventsCardView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    .line 2685124
    iget-object v0, p0, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {p0}, LX/JN3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020a3f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2685125
    iget-object v0, p0, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {p0, v0}, LX/JN3;->addView(Landroid/view/View;)V

    .line 2685126
    iget-object v0, p0, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2685127
    iget-object v0, p0, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getSocialContextTextView()Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, LX/1vY;->SOCIAL_CONTEXT:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2685128
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2685121
    iget-boolean v0, p0, LX/JN3;->a:Z

    return v0
.end method

.method public getEventActionView()Lcom/facebook/events/widget/eventcard/EventActionButtonView;
    .locals 1

    .prologue
    .line 2685112
    iget-object v0, p0, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x39cd76c7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2685117
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2685118
    const/4 v1, 0x1

    .line 2685119
    iput-boolean v1, p0, LX/JN3;->a:Z

    .line 2685120
    const/16 v1, 0x2d

    const v2, -0x6fd625cb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6a9c0a54

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2685113
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2685114
    const/4 v1, 0x0

    .line 2685115
    iput-boolean v1, p0, LX/JN3;->a:Z

    .line 2685116
    const/16 v1, 0x2d

    const v2, -0xca4ec3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
