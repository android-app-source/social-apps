.class public final LX/Hew;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Hex;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Hex;)V
    .locals 1

    .prologue
    .line 2490961
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2490962
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hew;->a:Ljava/lang/ref/WeakReference;

    .line 2490963
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 2490964
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2490965
    :goto_0
    return-void

    .line 2490966
    :pswitch_0
    iget-object v0, p0, LX/Hew;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hex;

    const/4 v5, 0x0

    .line 2490967
    iget-object v1, v0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    .line 2490968
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v1, v2

    .line 2490969
    if-nez v1, :cond_0

    .line 2490970
    iget-object v1, v0, LX/Hex;->f:LX/Hew;

    iget v2, v0, LX/Hex;->h:I

    int-to-long v3, v2

    invoke-virtual {v1, v5, v3, v4}, LX/Hew;->sendEmptyMessageDelayed(IJ)Z

    .line 2490971
    :goto_1
    goto :goto_0

    .line 2490972
    :cond_0
    iget v2, v0, LX/Hex;->i:I

    if-gtz v2, :cond_1

    .line 2490973
    invoke-virtual {v1}, LX/2pb;->k()I

    move-result v2

    iput v2, v0, LX/Hex;->i:I

    .line 2490974
    :cond_1
    invoke-virtual {v1}, LX/2pb;->h()I

    move-result v1

    .line 2490975
    int-to-float v1, v1

    iget v2, v0, LX/Hex;->i:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 2490976
    const/high16 v2, 0x43fa0000    # 500.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 2490977
    iget-object v2, v0, LX/Hex;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 2490978
    iget-object v1, v0, LX/Hex;->f:LX/Hew;

    iget v2, v0, LX/Hex;->h:I

    int-to-long v3, v2

    invoke-virtual {v1, v5, v3, v4}, LX/Hew;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
