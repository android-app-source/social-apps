.class public LX/IgJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/messaging/media/folder/Folder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2600954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 2600955
    check-cast p1, Lcom/facebook/messaging/media/folder/Folder;

    check-cast p2, Lcom/facebook/messaging/media/folder/Folder;

    .line 2600956
    iget-wide v4, p1, Lcom/facebook/messaging/media/folder/Folder;->e:J

    move-wide v0, v4

    .line 2600957
    iget-wide v4, p2, Lcom/facebook/messaging/media/folder/Folder;->e:J

    move-wide v2, v4

    .line 2600958
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2600959
    iget-object v0, p1, Lcom/facebook/messaging/media/folder/Folder;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2600960
    iget-object v1, p2, Lcom/facebook/messaging/media/folder/Folder;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2600961
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 2600962
    :goto_0
    return v0

    .line 2600963
    :cond_0
    iget-wide v4, p1, Lcom/facebook/messaging/media/folder/Folder;->e:J

    move-wide v0, v4

    .line 2600964
    iget-wide v4, p2, Lcom/facebook/messaging/media/folder/Folder;->e:J

    move-wide v2, v4

    .line 2600965
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
