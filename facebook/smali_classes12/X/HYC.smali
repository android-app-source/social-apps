.class public final LX/HYC;
.super LX/HYB;
.source ""


# instance fields
.field private a:LX/4hz;


# direct methods
.method public constructor <init>(LX/4hz;Lorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 2480309
    invoke-direct {p0, p2}, LX/HYB;-><init>(Lorg/json/JSONObject;)V

    .line 2480310
    iput-object p1, p0, LX/HYC;->a:LX/4hz;

    .line 2480311
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V
    .locals 4

    .prologue
    .line 2480312
    iget-object v0, p0, LX/HYB;->a:Lorg/json/JSONObject;

    move-object v0, v0

    .line 2480313
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2480314
    iget-object v1, p0, LX/HYC;->a:LX/4hz;

    invoke-virtual {v1, v0}, LX/4hz;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v2

    .line 2480315
    const-string v0, "error"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2480316
    if-nez v0, :cond_2

    const/4 v1, 0x1

    .line 2480317
    :goto_0
    if-eqz v1, :cond_0

    .line 2480318
    const-string v0, "results"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2480319
    if-nez v0, :cond_0

    .line 2480320
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2480321
    :cond_0
    const/4 v2, -0x1

    .line 2480322
    const-string v3, "platform_webview_finished"

    .line 2480323
    if-nez v1, :cond_1

    .line 2480324
    const/4 v2, 0x0

    .line 2480325
    :cond_1
    invoke-static {p1, v3}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V

    .line 2480326
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2480327
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2480328
    invoke-static {p1, v2, v3}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;ILandroid/content/Intent;)V

    .line 2480329
    const/4 v0, 0x0

    invoke-virtual {p2, p0, v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a(LX/HYB;Landroid/os/Bundle;)V

    .line 2480330
    return-void

    .line 2480331
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
