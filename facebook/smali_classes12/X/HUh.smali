.class public final LX/HUh;
.super LX/1Cv;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V
    .locals 0

    .prologue
    .line 2473529
    iput-object p1, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {p0}, LX/1Cv;-><init>()V

    return-void
.end method

.method private a(I)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;
    .locals 2

    .prologue
    .line 2473468
    iget-object v0, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ge p1, v0, :cond_0

    if-lez p1, :cond_0

    .line 2473469
    iget-object v0, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2473470
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2473520
    iget-object v0, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2473521
    const v0, 0x7f0b0060

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2473522
    sget-object v0, LX/HUm;->VIDEOS_HEADER:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2473523
    iget-object v0, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->p:Landroid/view/LayoutInflater;

    const v4, 0x7f0315e1

    invoke-virtual {v0, v4, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2473524
    const v1, 0x7f0b0060

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2473525
    :goto_0
    const v2, 0x7f0a0048

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2473526
    invoke-virtual {v0, v3, v1, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 2473527
    return-object v0

    .line 2473528
    :cond_0
    new-instance v0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;

    iget-object v2, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2473479
    iget-object v0, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2473480
    sget-object v1, LX/HUm;->VIDEOS_HEADER:LX/HUm;

    invoke-virtual {v1}, LX/HUm;->ordinal()I

    move-result v1

    if-ne p4, v1, :cond_1

    .line 2473481
    check-cast p3, Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2473482
    const v1, 0x7f08180a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2473483
    const v1, 0x7f0f00b0

    iget-object v2, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget v2, v2, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->v:I

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "%,d"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget v6, v6, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->v:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2473484
    :cond_0
    :goto_0
    return-void

    .line 2473485
    :cond_1
    sget-object v0, LX/HUm;->VIDEO_ITEM:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_0

    .line 2473486
    check-cast p2, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2473487
    invoke-virtual {p0}, LX/HUh;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, LX/HUh;->a(I)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    if-ne p2, v0, :cond_2

    .line 2473488
    iget-object v0, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-static {v0, v7}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->e$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473489
    :cond_2
    check-cast p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;

    .line 2473490
    iput-object p2, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->l:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2473491
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3fe38e39

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2473492
    new-instance v0, LX/1Uo;

    invoke-virtual {p3}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2473493
    iget-object v1, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2473494
    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2473495
    :goto_1
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2473496
    iget-object v1, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2473497
    new-instance v0, LX/HV5;

    invoke-direct {v0, p3, p2}, LX/HV5;-><init>(Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    invoke-virtual {p3, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2473498
    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->K()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->a(LX/174;)Ljava/lang/String;

    move-result-object v0

    .line 2473499
    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->v()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->a(LX/174;)Ljava/lang/String;

    move-result-object v1

    .line 2473500
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2473501
    iget-object v2, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2473502
    :goto_2
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2473503
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2473504
    :cond_3
    if-eqz p2, :cond_4

    .line 2473505
    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->x()I

    move-result v0

    .line 2473506
    iget-object v1, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p3}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00b1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2473507
    const/16 v6, 0x3e8

    if-ge v0, v6, :cond_7

    .line 2473508
    const-string v6, "%,d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v7, p0

    invoke-static {v6, v7}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2473509
    :goto_3
    move-object v6, v6

    .line 2473510
    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2473511
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->y()I

    move-result v1

    .line 2473512
    div-int/lit16 v2, v1, 0x3e8

    .line 2473513
    div-int/lit8 v3, v2, 0x3c

    .line 2473514
    rem-int/lit8 v2, v2, 0x3c

    .line 2473515
    const-string v4, "%d:%02d"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4, v3, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2473516
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2473517
    :cond_4
    goto/16 :goto_0

    .line 2473518
    :cond_5
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_1

    .line 2473519
    :cond_6
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->j:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_7
    iget-object v6, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->b:LX/154;

    invoke-virtual {v6, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_3
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2473530
    iget-object v0, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2473478
    invoke-direct {p0, p1}, LX/HUh;->a(I)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2473475
    iget-object v0, p0, LX/HUh;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ge p1, v0, :cond_0

    .line 2473476
    int-to-long v0, p1

    .line 2473477
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2473472
    if-nez p1, :cond_0

    .line 2473473
    sget-object v0, LX/HUm;->VIDEOS_HEADER:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    .line 2473474
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HUm;->VIDEO_ITEM:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2473471
    invoke-static {}, LX/HUm;->values()[LX/HUm;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
