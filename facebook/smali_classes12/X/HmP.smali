.class public final LX/HmP;
.super LX/HmN;
.source ""


# instance fields
.field public final synthetic a:LX/HmV;


# direct methods
.method public constructor <init>(LX/HmV;)V
    .locals 1

    .prologue
    .line 2500632
    iput-object p1, p0, LX/HmP;->a:LX/HmV;

    invoke-direct {p0, p1}, LX/HmN;-><init>(LX/HmV;)V

    return-void
.end method


# virtual methods
.method public final a()LX/HmU;
    .locals 3

    .prologue
    .line 2500633
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->j:LX/Hm4;

    iget-object v1, p0, LX/HmP;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->q:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-virtual {v0, v1}, LX/Hm4;->a(Lcom/facebook/beam/protocol/BeamPreflightInfo;)V

    .line 2500634
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v0}, LX/Hm4;->d()LX/Hm3;

    move-result-object v0

    .line 2500635
    if-eqz v0, :cond_0

    .line 2500636
    sget-object v1, LX/HmT;->a:[I

    invoke-virtual {v0}, LX/Hm3;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2500637
    :cond_0
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    invoke-static {v0}, LX/HmV;->j(LX/HmV;)V

    .line 2500638
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->k:LX/HmU;

    :goto_0
    return-object v0

    .line 2500639
    :pswitch_0
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    invoke-static {v0}, LX/HmV;->k(LX/HmV;)V

    .line 2500640
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->k:LX/HmU;

    goto :goto_0

    .line 2500641
    :pswitch_1
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v1, p0, LX/HmP;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v1}, LX/Hm4;->e()Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-result-object v1

    .line 2500642
    iput-object v1, v0, LX/HmV;->o:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2500643
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->o:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    if-nez v0, :cond_1

    .line 2500644
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->f:LX/Hmc;

    .line 2500645
    sget-object v1, LX/Hmb;->READ_PREFLIGHT_EXCEPTION:LX/Hmb;

    invoke-static {v0, v1}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500646
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v1, p0, LX/HmP;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->a:Landroid/content/Context;

    const v2, 0x7f083946

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/HmV;->a$redex0(LX/HmV;Ljava/lang/String;Z)V

    .line 2500647
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->k:LX/HmU;

    goto :goto_0

    .line 2500648
    :cond_1
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->o:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-virtual {v0}, LX/Hm1;->a()Ljava/lang/String;

    .line 2500649
    iget-object v0, p0, LX/HmP;->a:LX/HmV;

    sget-object v1, LX/HmU;->WAIT_RECEIVE_INTEND_TO_SEND:LX/HmU;

    .line 2500650
    iput-object v1, v0, LX/HmV;->k:LX/HmU;

    move-object v0, v1

    .line 2500651
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
