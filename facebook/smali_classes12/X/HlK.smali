.class public LX/HlK;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2498364
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/HlK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2498365
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498366
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/HlK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2498367
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498368
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2498369
    const v0, 0x7f030abe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2498370
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/HlK;->setOrientation(I)V

    .line 2498371
    invoke-virtual {p0}, LX/HlK;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020a3d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/HlK;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2498372
    const v0, 0x7f0d1b76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/HlK;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2498373
    const v0, 0x7f0d1b77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HlK;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2498374
    new-instance v0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02187f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v2}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 2498375
    new-instance v1, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2498376
    iput-object v0, v1, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 2498377
    move-object v0, v1

    .line 2498378
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2498379
    iget-object v1, p0, LX/HlK;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2498380
    return-void
.end method
