.class public final LX/JMQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "LX/JMS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JMT;


# direct methods
.method public constructor <init>(LX/JMT;)V
    .locals 0

    .prologue
    .line 2683919
    iput-object p1, p0, LX/JMQ;->a:LX/JMT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2683920
    new-instance v1, LX/JMS;

    invoke-direct {v1}, LX/JMS;-><init>()V

    .line 2683921
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 2683922
    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    :goto_0
    iput-object v0, v1, LX/JMS;->c:[B

    .line 2683923
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    iput-object v0, v1, LX/JMS;->b:[Lorg/apache/http/Header;

    .line 2683924
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    iput v0, v1, LX/JMS;->a:I

    .line 2683925
    return-object v1

    .line 2683926
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
