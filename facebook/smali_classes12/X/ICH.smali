.class public final LX/ICH;
.super LX/2fo;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic b:LX/1Po;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic e:LX/ICI;


# direct methods
.method public constructor <init>(LX/ICI;LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;LX/0Ot;Lcom/facebook/graphql/model/GraphQLNode;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 21

    .prologue
    .line 2549153
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/ICH;->e:LX/ICI;

    move-object/from16 v0, p20

    move-object/from16 v1, p0

    iput-object v0, v1, LX/ICH;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object/from16 v0, p21

    move-object/from16 v1, p0

    iput-object v0, v1, LX/ICH;->b:LX/1Po;

    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, LX/ICH;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    invoke-direct/range {v2 .. v20}, LX/2fo;-><init>(LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 2549154
    iget-object v0, p0, LX/ICH;->e:LX/ICI;

    iget-object v1, v0, LX/ICI;->b:LX/3iV;

    iget-object v0, p0, LX/ICH;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/3iV;->c(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 2549155
    iget-object v0, p0, LX/ICH;->b:LX/1Po;

    check-cast v0, LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/ICH;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2549156
    return-void

    .line 2549157
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0
.end method
