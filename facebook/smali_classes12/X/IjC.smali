.class public LX/IjC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6yL;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/6ye;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field public f:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6ye;LX/0Or;Ljava/util/concurrent/Executor;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/6ye;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605444
    iput-object p1, p0, LX/IjC;->a:Landroid/content/Context;

    .line 2605445
    iput-object p2, p0, LX/IjC;->b:LX/6ye;

    .line 2605446
    iput-object p3, p0, LX/IjC;->c:LX/0Or;

    .line 2605447
    iput-object p4, p0, LX/IjC;->d:Ljava/util/concurrent/Executor;

    .line 2605448
    iput-object p5, p0, LX/IjC;->e:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2605449
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9

    .prologue
    .line 2605450
    iget-object v0, p0, LX/IjC;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2605451
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2605452
    :goto_0
    return-object v0

    .line 2605453
    :cond_0
    iget-object v0, p0, LX/IjC;->e:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iget-object v1, p2, LX/6y8;->a:Ljava/lang/String;

    iget v2, p2, LX/6y8;->c:I

    iget v3, p2, LX/6y8;->d:I

    iget-object v4, p2, LX/6y8;->e:Ljava/lang/String;

    iget-object v5, p2, LX/6y8;->f:Ljava/lang/String;

    iget-object v6, p0, LX/IjC;->c:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    .line 2605454
    iget-object v7, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2605455
    move-object v7, p1

    check-cast v7, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v7, v7, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    if-eqz v7, :cond_1

    const-string v7, "messenger_commerce"

    :goto_1
    move-object v8, p1

    check-cast v8, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-object v8, v8, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->b:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2605456
    new-instance v1, LX/IjB;

    invoke-direct {v1, p0, p1, p2}, LX/IjB;-><init>(LX/IjC;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)V

    iget-object v2, p0, LX/IjC;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2605457
    :cond_1
    const-string v7, "p2p"

    goto :goto_1
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 2605458
    iget-object v0, p0, LX/IjC;->b:LX/6ye;

    invoke-virtual {v0, p1, p2}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6qh;)V
    .locals 2

    .prologue
    .line 2605459
    iput-object p1, p0, LX/IjC;->f:LX/6qh;

    .line 2605460
    iget-object v0, p0, LX/IjC;->b:LX/6ye;

    iget-object v1, p0, LX/IjC;->f:LX/6qh;

    invoke-virtual {v0, v1}, LX/6ye;->a(LX/6qh;)V

    .line 2605461
    return-void
.end method
