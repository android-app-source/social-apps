.class public LX/IBv;
.super LX/398;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile d:LX/IBv;


# instance fields
.field private b:LX/01T;

.field private c:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2547865
    const-string v0, "event/<p$1>"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IBv;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/01T;LX/0ad;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2547866
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2547867
    iput-object p1, p0, LX/IBv;->b:LX/01T;

    .line 2547868
    iput-object p2, p0, LX/IBv;->c:LX/0ad;

    .line 2547869
    const-string v0, "event/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "event_id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->EVENTS_PERMALINK_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2547870
    return-void
.end method

.method public static a(LX/0QB;)LX/IBv;
    .locals 5

    .prologue
    .line 2547871
    sget-object v0, LX/IBv;->d:LX/IBv;

    if-nez v0, :cond_1

    .line 2547872
    const-class v1, LX/IBv;

    monitor-enter v1

    .line 2547873
    :try_start_0
    sget-object v0, LX/IBv;->d:LX/IBv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2547874
    if-eqz v2, :cond_0

    .line 2547875
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2547876
    new-instance p0, LX/IBv;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/IBv;-><init>(LX/01T;LX/0ad;)V

    .line 2547877
    move-object v0, p0

    .line 2547878
    sput-object v0, LX/IBv;->d:LX/IBv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2547879
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2547880
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2547881
    :cond_1
    sget-object v0, LX/IBv;->d:LX/IBv;

    return-object v0

    .line 2547882
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2547883
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2547884
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2547885
    iget-object v0, p0, LX/IBv;->b:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
