.class public LX/JMG;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2683340
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2683341
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    .line 2683342
    invoke-virtual {p0}, LX/JMG;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2683343
    instance-of v1, v0, LX/5pX;

    if-eqz v1, :cond_0

    .line 2683344
    check-cast v0, LX/5pX;

    .line 2683345
    invoke-virtual {v0}, LX/5pX;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2683346
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2683347
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2683348
    new-instance v1, LX/JMH;

    invoke-virtual {p0}, LX/JMG;->getId()I

    move-result v2

    iget-object v3, p0, LX/JMG;->a:Ljava/lang/String;

    iget-boolean v4, p0, LX/JMG;->b:Z

    invoke-direct {v1, v2, v3, p1, v4}, LX/JMH;-><init>(ILjava/lang/String;ZZ)V

    invoke-virtual {v0, v1}, LX/5s9;->a(LX/5r0;)V

    .line 2683349
    :cond_0
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x25732f8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2683350
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 2683351
    const/4 v1, 0x1

    invoke-direct {p0, v1}, LX/JMG;->a(Z)V

    .line 2683352
    const/16 v1, 0x2d

    const v2, 0x515e8016

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x762f3e39

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2683353
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 2683354
    const/4 v1, 0x0

    invoke-direct {p0, v1}, LX/JMG;->a(Z)V

    .line 2683355
    const/16 v1, 0x2d

    const v2, 0x65c25ec6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
