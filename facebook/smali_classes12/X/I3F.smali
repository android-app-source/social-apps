.class public LX/I3F;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0hB;

.field private final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1My;

.field private final e:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:LX/Gcz;


# direct methods
.method public constructor <init>(LX/0tX;LX/0hB;LX/1Ck;LX/1My;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2531188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2531189
    new-instance v0, LX/I3C;

    invoke-direct {v0, p0}, LX/I3C;-><init>(LX/I3F;)V

    iput-object v0, p0, LX/I3F;->e:LX/0TF;

    .line 2531190
    iput-object p1, p0, LX/I3F;->a:LX/0tX;

    .line 2531191
    iput-object p2, p0, LX/I3F;->b:LX/0hB;

    .line 2531192
    iput-object p3, p0, LX/I3F;->c:LX/1Ck;

    .line 2531193
    iput-object p4, p0, LX/I3F;->d:LX/1My;

    .line 2531194
    return-void
.end method

.method public static a$redex0(LX/I3F;Ljava/util/List;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2531195
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 2531196
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2531197
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2531198
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2531199
    iget-object v3, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2531200
    iget-wide v7, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v7

    .line 2531201
    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2531202
    iget-object v3, p0, LX/I3F;->d:LX/1My;

    iget-object v4, p0, LX/I3F;->e:LX/0TF;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0

    .line 2531203
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/I3F;
    .locals 5

    .prologue
    .line 2531204
    new-instance v4, LX/I3F;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v3

    check-cast v3, LX/1My;

    invoke-direct {v4, v0, v1, v2, v3}, LX/I3F;-><init>(LX/0tX;LX/0hB;LX/1Ck;LX/1My;)V

    .line 2531205
    return-object v4
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2531206
    iget-object v0, p0, LX/I3F;->c:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2531207
    return-void
.end method

.method public final a(IILjava/lang/String;LX/Hxd;)V
    .locals 4
    .param p4    # LX/Hxd;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2531208
    new-instance v0, LX/I3D;

    invoke-direct {v0, p0, p3, p2, p1}, LX/I3D;-><init>(LX/I3F;Ljava/lang/String;II)V

    .line 2531209
    new-instance v1, LX/I3E;

    invoke-direct {v1, p0, p4}, LX/I3E;-><init>(LX/I3F;LX/Hxd;)V

    .line 2531210
    iget-object v2, p0, LX/I3F;->c:LX/1Ck;

    const-string v3, "FetchSubscriptions"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2531211
    return-void
.end method
