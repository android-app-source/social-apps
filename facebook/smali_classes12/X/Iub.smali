.class public LX/Iub;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/Iua;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Long;

.field private static final b:Ljava/lang/Long;

.field private static final f:Ljava/lang/Object;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0ad;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/Iue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2626932
    const-wide/32 v0, 0xea60

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, LX/Iub;->a:Ljava/lang/Long;

    .line 2626933
    const-wide/32 v0, 0x493e0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, LX/Iub;->b:Ljava/lang/Long;

    .line 2626934
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Iub;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0ad;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2626927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2626928
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Iub;->e:Ljava/util/Map;

    .line 2626929
    iput-object p1, p0, LX/Iub;->c:LX/0Or;

    .line 2626930
    iput-object p2, p0, LX/Iub;->d:LX/0ad;

    .line 2626931
    return-void
.end method

.method public static a(LX/0QB;)LX/Iub;
    .locals 8

    .prologue
    .line 2626935
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2626936
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2626937
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2626938
    if-nez v1, :cond_0

    .line 2626939
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2626940
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2626941
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2626942
    sget-object v1, LX/Iub;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2626943
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2626944
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2626945
    :cond_1
    if-nez v1, :cond_4

    .line 2626946
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2626947
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2626948
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2626949
    new-instance v7, LX/Iub;

    const/16 v1, 0x15e8

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v7, p0, v1}, LX/Iub;-><init>(LX/0Or;LX/0ad;)V

    .line 2626950
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2626951
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2626952
    if-nez v1, :cond_2

    .line 2626953
    sget-object v0, LX/Iub;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iub;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2626954
    :goto_1
    if-eqz v0, :cond_3

    .line 2626955
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2626956
    :goto_3
    check-cast v0, LX/Iub;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2626957
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2626958
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2626959
    :catchall_1
    move-exception v0

    .line 2626960
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2626961
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2626962
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2626963
    :cond_2
    :try_start_8
    sget-object v0, LX/Iub;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iub;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)LX/Iuf;
    .locals 8

    .prologue
    .line 2626895
    iget-object v0, p0, LX/Iub;->d:LX/0ad;

    sget v1, LX/IuZ;->a:I

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v1

    .line 2626896
    if-gtz v1, :cond_0

    .line 2626897
    sget-object v0, LX/Iuf;->UNSET:LX/Iuf;

    .line 2626898
    :goto_0
    return-object v0

    .line 2626899
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v0

    .line 2626900
    iget-object v0, p0, LX/Iub;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, LX/2gS;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2626901
    sget-object v0, LX/Iuf;->FORCE_SUPPRESS:LX/Iuf;

    goto :goto_0

    .line 2626902
    :cond_1
    iget-object v0, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626903
    if-nez v0, :cond_2

    .line 2626904
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto :goto_0

    .line 2626905
    :cond_2
    iget-object v3, p0, LX/Iub;->e:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2626906
    iget-object v3, p0, LX/Iub;->e:Ljava/util/Map;

    new-instance v4, LX/Iue;

    invoke-direct {v4, v1}, LX/Iue;-><init>(I)V

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626907
    :cond_3
    iget-object v3, p0, LX/Iub;->e:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iue;

    .line 2626908
    iget-wide v2, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 2626909
    iget-object v4, v0, LX/Iue;->b:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    iget v5, v0, LX/Iue;->a:I

    if-ne v4, v5, :cond_4

    .line 2626910
    iget-object v4, v0, LX/Iue;->b:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 2626911
    :cond_4
    iget-object v4, v0, LX/Iue;->b:Ljava/util/Queue;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2626912
    invoke-static {p1}, LX/2gS;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)J

    move-result-wide v4

    int-to-long v6, v1

    cmp-long v1, v4, v6

    if-gez v1, :cond_5

    .line 2626913
    const/4 v1, 0x0

    .line 2626914
    iput-object v1, v0, LX/Iue;->c:Ljava/lang/Long;

    .line 2626915
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto :goto_0

    .line 2626916
    :cond_5
    iget-object v1, v0, LX/Iue;->c:Ljava/lang/Long;

    move-object v1, v1

    .line 2626917
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    sget-object v1, LX/Iub;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_6

    .line 2626918
    sget-object v0, LX/Iuf;->SILENT:LX/Iuf;

    goto :goto_0

    .line 2626919
    :cond_6
    iget-object v1, v0, LX/Iue;->b:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    iget v4, v0, LX/Iue;->a:I

    if-ge v1, v4, :cond_8

    .line 2626920
    const/4 v1, 0x0

    .line 2626921
    :goto_1
    move-object v1, v1

    .line 2626922
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    sget-object v1, LX/Iub;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_7

    .line 2626923
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2626924
    iput-object v1, v0, LX/Iue;->c:Ljava/lang/Long;

    .line 2626925
    sget-object v0, LX/Iuf;->SILENT:LX/Iuf;

    goto/16 :goto_0

    .line 2626926
    :cond_7
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto/16 :goto_0

    :cond_8
    iget-object v1, v0, LX/Iue;->b:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2626894
    const-string v0, "FrequencyRule"

    return-object v0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2626892
    iget-object v0, p0, LX/Iub;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2626893
    return-void
.end method
