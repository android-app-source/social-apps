.class public final LX/I06;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/util/GregorianCalendar;

.field public final synthetic e:LX/I08;


# direct methods
.method public constructor <init>(LX/I08;IILjava/lang/String;Ljava/util/GregorianCalendar;)V
    .locals 0

    .prologue
    .line 2526531
    iput-object p1, p0, LX/I06;->e:LX/I08;

    iput p2, p0, LX/I06;->a:I

    iput p3, p0, LX/I06;->b:I

    iput-object p4, p0, LX/I06;->c:Ljava/lang/String;

    iput-object p5, p0, LX/I06;->d:Ljava/util/GregorianCalendar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2526532
    new-instance v0, LX/7oN;

    invoke-direct {v0}, LX/7oN;-><init>()V

    move-object v0, v0

    .line 2526533
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2526534
    iget v2, p0, LX/I06;->a:I

    iget v3, p0, LX/I06;->b:I

    iget-object v4, p0, LX/I06;->c:Ljava/lang/String;

    iget-object v5, p0, LX/I06;->d:Ljava/util/GregorianCalendar;

    .line 2526535
    new-instance v6, LX/7oN;

    invoke-direct {v6}, LX/7oN;-><init>()V

    .line 2526536
    const-string v7, "profile_image_size"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "first_count"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "end_cursor"

    invoke-virtual {v7, v8, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "start_month"

    const/4 v1, 0x2

    invoke-virtual {v5, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "start_day_of_month"

    const/4 v1, 0x5

    invoke-virtual {v5, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2526537
    move-object v6, v6

    .line 2526538
    move-object v1, v6

    .line 2526539
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2526540
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2526541
    iget-object v1, p0, LX/I06;->e:LX/I08;

    iget-object v1, v1, LX/I08;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
