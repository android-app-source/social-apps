.class public LX/JIw;
.super LX/Emg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Emg",
        "<",
        "LX/JI3;",
        ">;",
        "Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryCardAdapter$Listener;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EoZ;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G0q;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BA0;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/EpL;

.field public i:LX/Epu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/JIs;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

.field public final n:LX/EnL;

.field public final o:LX/Emj;

.field public p:LX/JIr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/1De;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Z

.field public t:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/EnL;LX/Emj;)V
    .locals 1
    .param p1    # Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/EnL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678678
    invoke-direct {p0}, LX/Emg;-><init>()V

    .line 2678679
    new-instance v0, LX/JIt;

    invoke-direct {v0, p0}, LX/JIt;-><init>(LX/JIw;)V

    iput-object v0, p0, LX/JIw;->h:LX/EpL;

    .line 2678680
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2678681
    iput-object v0, p0, LX/JIw;->l:LX/0Ot;

    .line 2678682
    iput-object p1, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678683
    iput-object p2, p0, LX/JIw;->n:LX/EnL;

    .line 2678684
    iput-object p3, p0, LX/JIw;->o:LX/Emj;

    .line 2678685
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fig/mediagrid/FigMediaGrid;ILX/0Px;LX/Fx7;LX/1aZ;Landroid/graphics/Rect;LX/Emo;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/fig/mediagrid/FigMediaGrid;",
            "I",
            "LX/0Px",
            "<+TT;>;",
            "LX/Fx7",
            "<TT;>;",
            "LX/1aZ;",
            "Landroid/graphics/Rect;",
            "LX/Emo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2678655
    invoke-interface {p5}, LX/1aZ;->c()LX/1aY;

    move-result-object v0

    instance-of v0, v0, LX/1af;

    if-nez v0, :cond_0

    .line 2678656
    iget-object v0, p0, LX/JIw;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 2678657
    const-string v1, "Expected media grid binding logic to use GenericDraweeHierarchy, got %s"

    invoke-interface {p5}, LX/1aZ;->c()LX/1aY;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2678658
    const-string v2, "discovery_card_photo_click_wrong_hierarchy"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2678659
    :goto_0
    return-void

    .line 2678660
    :cond_0
    iget-object v0, p0, LX/JIw;->o:LX/Emj;

    iget-object v1, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iget-object v5, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-object/from16 v1, p7

    invoke-interface/range {v0 .. v5}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2678661
    invoke-interface {p4, p3, p2}, LX/Fx7;->a(LX/0Px;I)LX/5vo;

    move-result-object v4

    .line 2678662
    invoke-interface {p4, p3, p2}, LX/Fx7;->b(LX/0Px;I)LX/5vn;

    move-result-object v5

    .line 2678663
    iget-object v0, p0, LX/JIw;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G0q;

    .line 2678664
    if-nez v5, :cond_2

    .line 2678665
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2678666
    invoke-static {}, LX/G0r;->b()LX/5vn;

    move-result-object v5

    .line 2678667
    invoke-interface {p5}, LX/1aZ;->c()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    move-object v1, p1

    move-object v3, p6

    invoke-virtual/range {v0 .. v7}, LX/G0q;->a(Landroid/view/View;LX/1af;Landroid/graphics/Rect;LX/1U8;LX/5vn;LX/0Px;Ljava/util/List;)V

    goto :goto_0

    .line 2678668
    :cond_1
    iget-object v0, p0, LX/JIw;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 2678669
    const-string v1, "Null collage layout, so expected exactly 1 photo, but got %s"

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2678670
    const-string v2, "discovery_card_photo_click_null_collage_layout"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2678671
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2678672
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2678673
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 2678674
    const/4 v2, 0x1

    new-array v2, v2, [LX/1U8;

    const/4 v6, 0x0

    invoke-interface {p4, p3, v1}, LX/Fx7;->a(LX/0Px;I)LX/5vo;

    move-result-object v8

    aput-object v8, v2, v6

    invoke-virtual {v3, v2}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 2678675
    const/4 v2, 0x1

    new-array v2, v2, [LX/5vn;

    const/4 v6, 0x0

    invoke-interface {p4, p3, v1}, LX/Fx7;->b(LX/0Px;I)LX/5vn;

    move-result-object v8

    aput-object v8, v2, v6

    invoke-virtual {v7, v2}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 2678676
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2678677
    :cond_3
    invoke-interface {p5}, LX/1aZ;->c()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    move-object v1, p1

    move-object v3, p6

    invoke-virtual/range {v0 .. v7}, LX/G0q;->a(Landroid/view/View;LX/1af;Landroid/graphics/Rect;LX/1U8;LX/5vn;LX/0Px;Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 15

    .prologue
    .line 2678686
    invoke-virtual {p0}, LX/Eme;->a()LX/0am;

    move-result-object v0

    .line 2678687
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2678688
    :goto_0
    return-void

    .line 2678689
    :cond_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JI3;

    .line 2678690
    iget-object v1, p0, LX/JIw;->p:LX/JIr;

    if-nez v1, :cond_2

    .line 2678691
    iget-object v1, p0, LX/JIw;->j:LX/JIs;

    .line 2678692
    iget-object v2, p0, LX/JIw;->q:LX/1De;

    if-nez v2, :cond_1

    .line 2678693
    new-instance v2, LX/1De;

    iget-object v3, p0, LX/JIw;->k:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/JIw;->q:LX/1De;

    .line 2678694
    :cond_1
    iget-object v2, p0, LX/JIw;->q:LX/1De;

    move-object v2, v2

    .line 2678695
    iget-object v3, p0, LX/JIw;->n:LX/EnL;

    iget-object v4, p0, LX/JIw;->o:LX/Emj;

    .line 2678696
    new-instance v5, LX/JIr;

    invoke-direct {v5, v2, v3, v4, p0}, LX/JIr;-><init>(LX/1De;LX/EnL;LX/Emj;LX/JIw;)V

    .line 2678697
    const/16 v6, 0x1ad0

    invoke-static {v1, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1ad1

    invoke-static {v1, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x1ad4

    invoke-static {v1, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x1ad3

    invoke-static {v1, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x1ad5

    invoke-static {v1, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x3686

    invoke-static {v1, v11}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v1}, LX/JIF;->a(LX/0QB;)LX/JIF;

    move-result-object v12

    check-cast v12, LX/JIF;

    invoke-static {v1}, LX/JIC;->a(LX/0QB;)LX/JIC;

    move-result-object v13

    check-cast v13, LX/JIC;

    const-class v14, LX/EpO;

    invoke-interface {v1, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/EpO;

    .line 2678698
    iput-object v6, v5, LX/JIr;->a:LX/0Or;

    iput-object v7, v5, LX/JIr;->b:LX/0Or;

    iput-object v8, v5, LX/JIr;->c:LX/0Or;

    iput-object v9, v5, LX/JIr;->d:LX/0Or;

    iput-object v10, v5, LX/JIr;->e:LX/0Or;

    iput-object v11, v5, LX/JIr;->f:LX/0Or;

    iput-object v12, v5, LX/JIr;->g:LX/JIF;

    iput-object v13, v5, LX/JIr;->h:LX/JIC;

    iput-object v14, v5, LX/JIr;->i:LX/EpO;

    .line 2678699
    move-object v1, v5

    .line 2678700
    iput-object v1, p0, LX/JIw;->p:LX/JIr;

    .line 2678701
    :cond_2
    iget-object v1, p0, LX/JIw;->p:LX/JIr;

    move-object v1, v1

    .line 2678702
    iget-object v2, v0, LX/JI3;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v2, v2

    .line 2678703
    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v2, v3

    .line 2678704
    if-eq v2, v1, :cond_3

    .line 2678705
    iget-object v2, v0, LX/JI3;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v2

    .line 2678706
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2678707
    :cond_3
    iget-object v0, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iget-object v2, p0, LX/JIw;->t:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, LX/JIw;->u:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    .line 2678708
    iput-object v0, v1, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678709
    iput-object v2, v1, LX/JIr;->p:Landroid/graphics/drawable/Drawable;

    .line 2678710
    iput-object v3, v1, LX/JIr;->q:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    .line 2678711
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2678712
    iget-boolean v0, p0, LX/JIw;->s:Z

    if-eqz v0, :cond_5

    .line 2678713
    :cond_4
    :goto_1
    goto/16 :goto_0

    .line 2678714
    :cond_5
    iget-object v0, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel$PhotoModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2678715
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JIw;->s:Z

    .line 2678716
    iget-object v0, p0, LX/JIw;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BA0;

    .line 2678717
    iget-object v1, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->G()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel$PhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel$PhotoModel;->c()Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v1, v2}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2678718
    iget-object v0, p0, LX/JIw;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    .line 2678719
    new-instance v2, LX/JIv;

    invoke-direct {v2, p0}, LX/JIv;-><init>(LX/JIw;)V

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2678652
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iput-object p1, p0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678653
    invoke-virtual {p0}, LX/JIw;->b()V

    .line 2678654
    return-void
.end method
