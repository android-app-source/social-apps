.class public LX/Ipu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final irisSeqId:Ljava/lang/Long;

.field public final paymentsProtected:Ljava/lang/Boolean;

.field public final pinFbId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 2615448
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPinCode"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipu;->b:LX/1sv;

    .line 2615449
    new-instance v0, LX/1sw;

    const-string v1, "pinFbId"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipu;->c:LX/1sw;

    .line 2615450
    new-instance v0, LX/1sw;

    const-string v1, "paymentsProtected"

    invoke-direct {v0, v1, v2, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipu;->d:LX/1sw;

    .line 2615451
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipu;->e:LX/1sw;

    .line 2615452
    sput-boolean v3, LX/Ipu;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2615453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2615454
    iput-object p1, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    .line 2615455
    iput-object p2, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    .line 2615456
    iput-object p3, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    .line 2615457
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2615458
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2615459
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v3, v0

    .line 2615460
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    .line 2615461
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaPinCode"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2615462
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615463
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615464
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615465
    const/4 v1, 0x1

    .line 2615466
    iget-object v6, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    if-eqz v6, :cond_0

    .line 2615467
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615468
    const-string v1, "pinFbId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615469
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615470
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615471
    iget-object v1, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    if-nez v1, :cond_7

    .line 2615472
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 2615473
    :cond_0
    iget-object v6, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    if-eqz v6, :cond_a

    .line 2615474
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615475
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615476
    const-string v1, "paymentsProtected"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615477
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615478
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615479
    iget-object v1, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    if-nez v1, :cond_8

    .line 2615480
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615481
    :goto_4
    iget-object v1, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2615482
    if-nez v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615483
    :cond_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615484
    const-string v1, "irisSeqId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615485
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615486
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615487
    iget-object v0, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_9

    .line 2615488
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615489
    :cond_3
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615490
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615491
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2615492
    :cond_4
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2615493
    :cond_5
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2615494
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 2615495
    :cond_7
    iget-object v1, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2615496
    :cond_8
    iget-object v1, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 2615497
    :cond_9
    iget-object v0, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_a
    move v2, v1

    goto/16 :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2615498
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2615499
    iget-object v0, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2615500
    iget-object v0, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2615501
    sget-object v0, LX/Ipu;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615502
    iget-object v0, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615503
    :cond_0
    iget-object v0, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 2615504
    iget-object v0, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 2615505
    sget-object v0, LX/Ipu;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615506
    iget-object v0, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2615507
    :cond_1
    iget-object v0, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2615508
    iget-object v0, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2615509
    sget-object v0, LX/Ipu;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2615510
    iget-object v0, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2615511
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2615512
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2615513
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2615514
    if-nez p1, :cond_1

    .line 2615515
    :cond_0
    :goto_0
    return v0

    .line 2615516
    :cond_1
    instance-of v1, p1, LX/Ipu;

    if-eqz v1, :cond_0

    .line 2615517
    check-cast p1, LX/Ipu;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2615518
    if-nez p1, :cond_3

    .line 2615519
    :cond_2
    :goto_1
    move v0, v2

    .line 2615520
    goto :goto_0

    .line 2615521
    :cond_3
    iget-object v0, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2615522
    :goto_2
    iget-object v3, p1, LX/Ipu;->pinFbId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2615523
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2615524
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615525
    iget-object v0, p0, LX/Ipu;->pinFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipu;->pinFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615526
    :cond_5
    iget-object v0, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    move v0, v1

    .line 2615527
    :goto_4
    iget-object v3, p1, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    if-eqz v3, :cond_d

    move v3, v1

    .line 2615528
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2615529
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615530
    iget-object v0, p0, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Ipu;->paymentsProtected:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2615531
    :cond_7
    iget-object v0, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2615532
    :goto_6
    iget-object v3, p1, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2615533
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2615534
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2615535
    iget-object v0, p0, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipu;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 2615536
    goto :goto_1

    :cond_a
    move v0, v2

    .line 2615537
    goto :goto_2

    :cond_b
    move v3, v2

    .line 2615538
    goto :goto_3

    :cond_c
    move v0, v2

    .line 2615539
    goto :goto_4

    :cond_d
    move v3, v2

    .line 2615540
    goto :goto_5

    :cond_e
    move v0, v2

    .line 2615541
    goto :goto_6

    :cond_f
    move v3, v2

    .line 2615542
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2615543
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2615544
    sget-boolean v0, LX/Ipu;->a:Z

    .line 2615545
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipu;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2615546
    return-object v0
.end method
