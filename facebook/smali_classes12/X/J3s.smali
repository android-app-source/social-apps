.class public final enum LX/J3s;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J3s;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J3s;

.field public static final enum FETCH_GENERIC_PRIVACY_REVIEW_INFO:LX/J3s;

.field public static final enum SEND_PRIVACY_EDITS:LX/J3s;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2643311
    new-instance v0, LX/J3s;

    const-string v1, "FETCH_GENERIC_PRIVACY_REVIEW_INFO"

    invoke-direct {v0, v1, v2}, LX/J3s;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J3s;->FETCH_GENERIC_PRIVACY_REVIEW_INFO:LX/J3s;

    .line 2643312
    new-instance v0, LX/J3s;

    const-string v1, "SEND_PRIVACY_EDITS"

    invoke-direct {v0, v1, v3}, LX/J3s;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J3s;->SEND_PRIVACY_EDITS:LX/J3s;

    .line 2643313
    const/4 v0, 0x2

    new-array v0, v0, [LX/J3s;

    sget-object v1, LX/J3s;->FETCH_GENERIC_PRIVACY_REVIEW_INFO:LX/J3s;

    aput-object v1, v0, v2

    sget-object v1, LX/J3s;->SEND_PRIVACY_EDITS:LX/J3s;

    aput-object v1, v0, v3

    sput-object v0, LX/J3s;->$VALUES:[LX/J3s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2643316
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J3s;
    .locals 1

    .prologue
    .line 2643315
    const-class v0, LX/J3s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J3s;

    return-object v0
.end method

.method public static values()[LX/J3s;
    .locals 1

    .prologue
    .line 2643314
    sget-object v0, LX/J3s;->$VALUES:[LX/J3s;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J3s;

    return-object v0
.end method
