.class public final LX/Hh5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

.field public final synthetic b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;)V
    .locals 0

    .prologue
    .line 2494842
    iput-object p1, p0, LX/Hh5;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iput-object p2, p0, LX/Hh5;->a:Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2494843
    iget-object v0, p0, LX/Hh5;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->i:LX/03V;

    sget-object v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a:Ljava/lang/String;

    const-string v2, "Error creating application shortcut"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2494844
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2494845
    check-cast p1, Ljava/lang/Boolean;

    .line 2494846
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2494847
    iget-object v0, p0, LX/Hh5;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->j:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "carrier_manager_shortcut_created"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Hh5;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-virtual {v2}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a()Ljava/lang/String;

    move-result-object v2

    .line 2494848
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2494849
    move-object v1, v1

    .line 2494850
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2494851
    iget-object v0, p0, LX/Hh5;->b:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->d:LX/0Sh;

    new-instance v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment$5$1;

    invoke-direct {v1, p0}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment$5$1;-><init>(LX/Hh5;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2494852
    :cond_0
    return-void
.end method
