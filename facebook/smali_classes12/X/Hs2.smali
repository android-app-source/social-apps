.class public LX/Hs2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:J

.field private final b:Landroid/content/Context;

.field private final c:LX/0Sh;

.field private final d:LX/9bq;

.field private final e:LX/9b2;

.field private final f:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final g:LX/03V;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;LX/0Sh;LX/9bq;LX/9b2;Lcom/facebook/performancelogger/PerformanceLogger;LX/03V;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2513880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2513881
    iput-object p2, p0, LX/Hs2;->b:Landroid/content/Context;

    .line 2513882
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/Hs2;->a:J

    .line 2513883
    iput-object p3, p0, LX/Hs2;->c:LX/0Sh;

    .line 2513884
    iput-object p4, p0, LX/Hs2;->d:LX/9bq;

    .line 2513885
    iput-object p5, p0, LX/Hs2;->e:LX/9b2;

    .line 2513886
    iput-object p6, p0, LX/Hs2;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2513887
    iput-object p7, p0, LX/Hs2;->g:LX/03V;

    .line 2513888
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/Long;Landroid/widget/ListView;LX/Hrw;Z)LX/Hs1;
    .locals 12

    .prologue
    .line 2513889
    new-instance v0, LX/Hs1;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, LX/Hs2;->b:Landroid/content/Context;

    iget-object v5, p0, LX/Hs2;->c:LX/0Sh;

    iget-object v6, p0, LX/Hs2;->d:LX/9bq;

    iget-object v7, p0, LX/Hs2;->e:LX/9b2;

    iget-object v9, p0, LX/Hs2;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v10, p0, LX/Hs2;->g:LX/03V;

    move-object v1, p1

    move-object v4, p3

    move-object/from16 v8, p4

    move/from16 v11, p5

    invoke-direct/range {v0 .. v11}, LX/Hs1;-><init>(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/Long;Landroid/content/Context;Landroid/widget/ListView;LX/0Sh;LX/9bq;LX/9b2;LX/Hrw;Lcom/facebook/performancelogger/PerformanceLogger;LX/03V;Z)V

    return-object v0

    :cond_0
    iget-wide v2, p0, LX/Hs2;->a:J

    goto :goto_0
.end method
