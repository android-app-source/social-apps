.class public LX/JRI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JRG;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JRJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2693103
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JRI;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JRJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693104
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2693105
    iput-object p1, p0, LX/JRI;->b:LX/0Ot;

    .line 2693106
    return-void
.end method

.method public static a(LX/0QB;)LX/JRI;
    .locals 4

    .prologue
    .line 2693107
    const-class v1, LX/JRI;

    monitor-enter v1

    .line 2693108
    :try_start_0
    sget-object v0, LX/JRI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693109
    sput-object v2, LX/JRI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693110
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693111
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693112
    new-instance v3, LX/JRI;

    const/16 p0, 0x2025

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JRI;-><init>(LX/0Ot;)V

    .line 2693113
    move-object v0, v3

    .line 2693114
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693115
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693116
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693117
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2693118
    check-cast p2, LX/JRH;

    .line 2693119
    iget-object v0, p0, LX/JRI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JRJ;

    iget-object v1, p2, LX/JRH;->a:Ljava/lang/String;

    iget-object v2, p2, LX/JRH;->b:Ljava/lang/String;

    const/4 v4, 0x2

    .line 2693120
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    const v5, 0x7f0b25a9

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b010f

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    .line 2693121
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f020eef

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b004e

    invoke-virtual {v5, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0a015d

    invoke-virtual {v5, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 p0, 0x4

    const p2, 0x7f0b25a9

    invoke-interface {v5, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v5, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JRJ;->a:LX/2g9;

    invoke-virtual {v5, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v5

    const/16 p0, 0x101

    invoke-virtual {v5, p0}, LX/2gA;->h(I)LX/2gA;

    move-result-object v5

    const p0, 0x7f083a83

    invoke-virtual {v5, p0}, LX/2gA;->j(I)LX/2gA;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 p0, 0x3

    invoke-interface {v5, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    .line 2693122
    const p0, -0x15c2a0b6

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2693123
    invoke-interface {v5, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2693124
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2693125
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2693126
    invoke-static {}, LX/1dS;->b()V

    .line 2693127
    iget v0, p1, LX/1dQ;->b:I

    .line 2693128
    packed-switch v0, :pswitch_data_0

    .line 2693129
    :goto_0
    return-object v2

    .line 2693130
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2693131
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2693132
    check-cast v1, LX/JRH;

    .line 2693133
    iget-object v3, p0, LX/JRI;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JRJ;

    iget-object p1, v1, LX/JRH;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693134
    iget-object p0, v3, LX/JRJ;->b:LX/JRB;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2693135
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 2693136
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    .line 2693137
    const/4 v3, 0x0

    .line 2693138
    instance-of v0, p2, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    if-eqz v0, :cond_2

    .line 2693139
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "messenger_generic_feed_unit_action_button_tap"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2693140
    const-string v0, "tracking"

    check-cast p2, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2693141
    const-string v0, "installed_state"

    iget-object p1, p0, LX/JRB;->b:LX/23i;

    invoke-virtual {p1}, LX/23i;->f()Z

    move-result p1

    invoke-virtual {v3, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2693142
    :cond_0
    :goto_1
    move-object v3, v3

    .line 2693143
    if-eqz v3, :cond_1

    .line 2693144
    const-string v0, "messenger_feed_units"

    .line 2693145
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2693146
    iget-object v0, p0, LX/JRB;->c:LX/0Zb;

    invoke-interface {v0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2693147
    :cond_1
    iget-object v3, p0, LX/JRB;->a:LX/17W;

    sget-object v0, LX/0ax;->ae:Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2693148
    goto :goto_0

    .line 2693149
    :cond_2
    instance-of v0, p2, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    if-eqz v0, :cond_0

    .line 2693150
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "messenger_active_now_feed_unit_action_button_tap"

    invoke-direct {v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2693151
    const-string v3, "tracking"

    check-cast p2, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2693152
    const-string v3, "installed_state"

    iget-object p1, p0, LX/JRB;->b:LX/23i;

    invoke-virtual {p1}, LX/23i;->f()Z

    move-result p1

    invoke-virtual {v0, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2693153
    const-string p1, "presence_enabled"

    iget-object v3, p0, LX/JRB;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3CA;

    invoke-virtual {v3}, LX/3CA;->shouldShowPresence()Z

    move-result v3

    invoke-virtual {v0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v3, v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x15c2a0b6
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/JRG;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2693154
    new-instance v1, LX/JRH;

    invoke-direct {v1, p0}, LX/JRH;-><init>(LX/JRI;)V

    .line 2693155
    sget-object v2, LX/JRI;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JRG;

    .line 2693156
    if-nez v2, :cond_0

    .line 2693157
    new-instance v2, LX/JRG;

    invoke-direct {v2}, LX/JRG;-><init>()V

    .line 2693158
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/JRG;->a$redex0(LX/JRG;LX/1De;IILX/JRH;)V

    .line 2693159
    move-object v1, v2

    .line 2693160
    move-object v0, v1

    .line 2693161
    return-object v0
.end method
