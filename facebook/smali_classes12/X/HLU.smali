.class public final LX/HLU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/HLX;


# direct methods
.method public constructor <init>(LX/HLX;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2455890
    iput-object p1, p0, LX/HLU;->c:LX/HLX;

    iput-object p2, p0, LX/HLU;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HLU;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2455891
    iget-object v0, p0, LX/HLU;->c:LX/HLX;

    iget-object v1, p0, LX/HLU;->a:Ljava/lang/String;

    iget-object v2, p0, LX/HLU;->b:Ljava/lang/String;

    .line 2455892
    new-instance v3, LX/HLb;

    invoke-direct {v3}, LX/HLb;-><init>()V

    move-object v3, v3

    .line 2455893
    const-string v4, "city_page_id"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2455894
    const-string v4, "cover_image_high_res_size"

    iget-object p0, v0, LX/HLX;->a:LX/7H7;

    invoke-virtual {p0}, LX/7H7;->a()I

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2455895
    const-string v4, "friend_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2455896
    const-string v4, "media_type"

    iget-object p0, v0, LX/HLX;->b:LX/0rq;

    invoke-virtual {p0}, LX/0rq;->c()LX/0wF;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2455897
    const-string v4, "size_style"

    invoke-static {}, LX/0rq;->d()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2455898
    iget-object v4, v0, LX/HLX;->c:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object p0, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, p0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    sget-object p0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v3, p0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2455899
    new-instance v4, LX/HLW;

    invoke-direct {v4}, LX/HLW;-><init>()V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object p0

    invoke-static {v3, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2455900
    return-object v0
.end method
