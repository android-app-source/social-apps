.class public final LX/HI6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 2450077
    const/16 v24, 0x0

    .line 2450078
    const/16 v23, 0x0

    .line 2450079
    const/16 v22, 0x0

    .line 2450080
    const/16 v21, 0x0

    .line 2450081
    const/16 v20, 0x0

    .line 2450082
    const/16 v19, 0x0

    .line 2450083
    const/16 v18, 0x0

    .line 2450084
    const/16 v17, 0x0

    .line 2450085
    const/16 v16, 0x0

    .line 2450086
    const/4 v15, 0x0

    .line 2450087
    const/4 v14, 0x0

    .line 2450088
    const/4 v13, 0x0

    .line 2450089
    const/4 v12, 0x0

    .line 2450090
    const/4 v11, 0x0

    .line 2450091
    const/4 v10, 0x0

    .line 2450092
    const/4 v9, 0x0

    .line 2450093
    const/4 v8, 0x0

    .line 2450094
    const/4 v7, 0x0

    .line 2450095
    const/4 v6, 0x0

    .line 2450096
    const/4 v5, 0x0

    .line 2450097
    const/4 v4, 0x0

    .line 2450098
    const/4 v3, 0x0

    .line 2450099
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    .line 2450100
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2450101
    const/4 v3, 0x0

    .line 2450102
    :goto_0
    return v3

    .line 2450103
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2450104
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_11

    .line 2450105
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v25

    .line 2450106
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2450107
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    if-eqz v25, :cond_1

    .line 2450108
    const-string v26, "__type__"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_2

    const-string v26, "__typename"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 2450109
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v24

    goto :goto_1

    .line 2450110
    :cond_3
    const-string v26, "activity_admin_info"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 2450111
    invoke-static/range {p0 .. p1}, LX/HI3;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 2450112
    :cond_4
    const-string v26, "activity_feeds"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 2450113
    invoke-static/range {p0 .. p1}, LX/HI4;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 2450114
    :cond_5
    const-string v26, "admin_info"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 2450115
    invoke-static/range {p0 .. p1}, LX/HRI;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 2450116
    :cond_6
    const-string v26, "comms_hub_config"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 2450117
    invoke-static/range {p0 .. p1}, LX/HRJ;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 2450118
    :cond_7
    const-string v26, "id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 2450119
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 2450120
    :cond_8
    const-string v26, "insights_badge_count"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 2450121
    const/4 v9, 0x1

    .line 2450122
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto/16 :goto_1

    .line 2450123
    :cond_9
    const-string v26, "is_published"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 2450124
    const/4 v8, 0x1

    .line 2450125
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 2450126
    :cond_a
    const-string v26, "page_call_to_action"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 2450127
    invoke-static/range {p0 .. p1}, LX/HI5;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 2450128
    :cond_b
    const-string v26, "page_likers"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_c

    .line 2450129
    invoke-static/range {p0 .. p1}, LX/HRK;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 2450130
    :cond_c
    const-string v26, "should_show_recent_activity_entry_point"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_d

    .line 2450131
    const/4 v7, 0x1

    .line 2450132
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 2450133
    :cond_d
    const-string v26, "should_show_recent_checkins_entry_point"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_e

    .line 2450134
    const/4 v6, 0x1

    .line 2450135
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 2450136
    :cond_e
    const-string v26, "should_show_recent_mentions_entry_point"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 2450137
    const/4 v5, 0x1

    .line 2450138
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 2450139
    :cond_f
    const-string v26, "should_show_recent_reviews_entry_point"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_10

    .line 2450140
    const/4 v4, 0x1

    .line 2450141
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 2450142
    :cond_10
    const-string v26, "should_show_recent_shares_entry_point"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 2450143
    const/4 v3, 0x1

    .line 2450144
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 2450145
    :cond_11
    const/16 v25, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2450146
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450147
    const/16 v24, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450148
    const/16 v23, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450149
    const/16 v22, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450150
    const/16 v21, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450151
    const/16 v20, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2450152
    if-eqz v9, :cond_12

    .line 2450153
    const/4 v9, 0x6

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v9, v1, v2}, LX/186;->a(III)V

    .line 2450154
    :cond_12
    if-eqz v8, :cond_13

    .line 2450155
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 2450156
    :cond_13
    const/16 v8, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 2450157
    const/16 v8, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 2450158
    if-eqz v7, :cond_14

    .line 2450159
    const/16 v7, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->a(IZ)V

    .line 2450160
    :cond_14
    if-eqz v6, :cond_15

    .line 2450161
    const/16 v6, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->a(IZ)V

    .line 2450162
    :cond_15
    if-eqz v5, :cond_16

    .line 2450163
    const/16 v5, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->a(IZ)V

    .line 2450164
    :cond_16
    if-eqz v4, :cond_17

    .line 2450165
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(IZ)V

    .line 2450166
    :cond_17
    if-eqz v3, :cond_18

    .line 2450167
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 2450168
    :cond_18
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
