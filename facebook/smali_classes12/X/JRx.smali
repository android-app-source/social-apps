.class public final LX/JRx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/musicpreview/MusicButton;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicpreview/MusicButton;)V
    .locals 0

    .prologue
    .line 2694431
    iput-object p1, p0, LX/JRx;->a:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 2694423
    iget-object v0, p0, LX/JRx;->a:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    iget-object v0, v0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2694424
    if-eqz v0, :cond_0

    .line 2694425
    iget-object v1, p0, LX/JRx;->a:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    iget-object v1, v1, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2694426
    :cond_0
    iget-object v0, p0, LX/JRx;->a:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/JRx;->a:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    iget-object v1, v1, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2694427
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 2694428
    iget-object v0, p0, LX/JRx;->a:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2694429
    :goto_0
    return-void

    .line 2694430
    :cond_1
    iget-object v0, p0, LX/JRx;->a:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
