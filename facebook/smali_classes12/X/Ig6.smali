.class public LX/Ig6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ifs;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ifr;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ift;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/user/model/UserKey;

.field public final e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public f:J

.field private g:LX/0SG;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0SG;Lcom/facebook/user/model/UserKey;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2600563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600564
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ig6;->a:LX/0mY;

    .line 2600565
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ig6;->b:LX/0mY;

    .line 2600566
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ig6;->c:LX/0mY;

    .line 2600567
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Ig6;->f:J

    .line 2600568
    iput-object v2, p0, LX/Ig6;->h:Ljava/lang/String;

    .line 2600569
    iput-object v2, p0, LX/Ig6;->i:Ljava/lang/String;

    .line 2600570
    iput-object p2, p0, LX/Ig6;->d:Lcom/facebook/user/model/UserKey;

    .line 2600571
    iput-object p3, p0, LX/Ig6;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2600572
    iput-object p1, p0, LX/Ig6;->g:LX/0SG;

    .line 2600573
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 2600574
    iget-wide v0, p0, LX/Ig6;->f:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 2600575
    :goto_0
    return-void

    .line 2600576
    :cond_0
    iput-wide p1, p0, LX/Ig6;->f:J

    .line 2600577
    iget-object v0, p0, LX/Ig6;->a:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object v2

    .line 2600578
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2600579
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifs;

    invoke-interface {v0, p0}, LX/Ifs;->c(LX/Ig6;)V

    .line 2600580
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2600581
    :cond_1
    iget-object v0, p0, LX/Ig6;->a:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600582
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2600583
    iget-object v0, p0, LX/Ig6;->h:Ljava/lang/String;

    invoke-static {v0, p1}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2600584
    :goto_0
    return-void

    .line 2600585
    :cond_0
    iput-object p1, p0, LX/Ig6;->h:Ljava/lang/String;

    .line 2600586
    iget-object v0, p0, LX/Ig6;->c:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object p1

    .line 2600587
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2600588
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ift;

    invoke-interface {v0, p0}, LX/Ift;->a(LX/Ig6;)V

    .line 2600589
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2600590
    :cond_1
    iget-object v0, p0, LX/Ig6;->c:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600591
    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 2600592
    iget-wide v0, p0, LX/Ig6;->f:J

    iget-object v2, p0, LX/Ig6;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2600593
    iget-object v0, p0, LX/Ig6;->i:Ljava/lang/String;

    invoke-static {v0, p1}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2600594
    :goto_0
    return-void

    .line 2600595
    :cond_0
    iput-object p1, p0, LX/Ig6;->i:Ljava/lang/String;

    .line 2600596
    iget-object v0, p0, LX/Ig6;->b:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object p1

    .line 2600597
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2600598
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifr;

    invoke-interface {v0, p0}, LX/Ifr;->b(LX/Ig6;)V

    .line 2600599
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2600600
    :cond_1
    iget-object v0, p0, LX/Ig6;->b:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600601
    goto :goto_0
.end method
