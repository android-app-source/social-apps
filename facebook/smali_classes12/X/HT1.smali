.class public LX/HT1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CSJ;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/AEo;

.field private final c:LX/2U1;

.field private final d:LX/0sh;

.field private final e:LX/3iH;

.field private final f:LX/0SI;

.field private final g:LX/0ad;

.field private final h:LX/13Q;


# direct methods
.method public constructor <init>(LX/AEo;LX/0Or;LX/2U1;LX/0sh;LX/3iH;LX/0SI;LX/0ad;LX/13Q;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/saved/gating/annotations/IsSavedOfflineToastEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AEo;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2U1;",
            "LX/0sh;",
            "LX/3iH;",
            "LX/0SI;",
            "LX/0ad;",
            "LX/13Q;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2468282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2468283
    iput-object p1, p0, LX/HT1;->b:LX/AEo;

    .line 2468284
    iput-object p2, p0, LX/HT1;->a:LX/0Or;

    .line 2468285
    iput-object p3, p0, LX/HT1;->c:LX/2U1;

    .line 2468286
    iput-object p4, p0, LX/HT1;->d:LX/0sh;

    .line 2468287
    iput-object p5, p0, LX/HT1;->e:LX/3iH;

    .line 2468288
    iput-object p6, p0, LX/HT1;->f:LX/0SI;

    .line 2468289
    iput-object p7, p0, LX/HT1;->g:LX/0ad;

    .line 2468290
    iput-object p8, p0, LX/HT1;->h:LX/13Q;

    .line 2468291
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 2468280
    const-string v0, "target_fragment"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2468281
    sget-object v1, LX/0cQ;->NATIVE_PAGES_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2468292
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2468293
    :goto_0
    return v0

    .line 2468294
    :cond_0
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2468295
    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    move v0, v1

    .line 2468296
    goto :goto_0

    .line 2468297
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2468298
    iget-object v0, p0, LX/HT1;->c:LX/2U1;

    invoke-virtual {v0, v3}, LX/2U1;->c(Ljava/lang/String;)LX/8Dk;

    move-result-object v0

    .line 2468299
    if-nez v0, :cond_2

    move v0, v1

    .line 2468300
    goto :goto_0

    .line 2468301
    :cond_2
    iget-object v6, v0, LX/8Dk;->b:LX/0am;

    move-object v0, v6

    .line 2468302
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v6

    if-nez v6, :cond_3

    .line 2468303
    iget-object v0, p0, LX/HT1;->h:LX/13Q;

    const-string v2, "spotty_ads_pages_fail_no_access_token"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2468304
    goto :goto_0

    .line 2468305
    :cond_3
    iget-object v6, p0, LX/HT1;->e:LX/3iH;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v3, v0}, LX/3iH;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 2468306
    invoke-static {}, LX/HHu;->a()LX/HHt;

    move-result-object v0

    const-string v6, "page_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/HHt;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2468307
    new-instance v6, LX/HRi;

    invoke-direct {v6, v4, v5}, LX/HRi;-><init>(J)V

    invoke-virtual {v0, v6}, LX/0zO;->a(LX/0zT;)LX/0zO;

    .line 2468308
    iget-object v4, p0, LX/HT1;->f:LX/0SI;

    invoke-interface {v4, v3}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    .line 2468309
    iget-object v3, p0, LX/HT1;->d:LX/0sh;

    invoke-virtual {v3, v0}, LX/0sh;->d(LX/0zO;)Z

    move-result v3

    .line 2468310
    iget-object v4, p0, LX/HT1;->f:LX/0SI;

    invoke-interface {v4}, LX/0SI;->f()V

    .line 2468311
    if-nez v3, :cond_4

    iget-object v3, p0, LX/HT1;->d:LX/0sh;

    invoke-virtual {v3, v0}, LX/0sh;->d(LX/0zO;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2468312
    iget-object v0, p0, LX/HT1;->h:LX/13Q;

    const-string v2, "spotty_ads_pages_fail_not_cached"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2468313
    goto :goto_0

    .line 2468314
    :cond_4
    iget-object v0, p0, LX/HT1;->g:LX/0ad;

    sget-short v3, LX/8Dn;->j:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2468315
    iget-object v0, p0, LX/HT1;->h:LX/13Q;

    const-string v1, "spotty_ads_pages_success"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2468316
    goto/16 :goto_0

    .line 2468317
    :cond_5
    iget-object v0, p0, LX/HT1;->h:LX/13Q;

    const-string v2, "spotty_ads_pages_fail_qe_check"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2468318
    goto/16 :goto_0
.end method

.method public final c(Landroid/content/Intent;)LX/AEn;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2468274
    iget-object v0, p0, LX/HT1;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2468275
    :cond_0
    :goto_0
    return-object v1

    .line 2468276
    :cond_1
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2468277
    iget-object v0, p0, LX/HT1;->b:LX/AEo;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2468278
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v4, v3

    .line 2468279
    move-object v3, v1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, LX/AEo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;

    move-result-object v1

    goto :goto_0
.end method
