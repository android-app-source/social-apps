.class public LX/Ilg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IlO;


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:LX/Ilj;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;LX/Ilj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2608012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2608013
    iput-object p1, p0, LX/Ilg;->a:Landroid/view/LayoutInflater;

    .line 2608014
    iput-object p2, p0, LX/Ilg;->b:LX/Ilj;

    .line 2608015
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)LX/IlT;
    .locals 6

    .prologue
    .line 2608016
    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;

    if-nez v0, :cond_1

    .line 2608017
    :cond_0
    iget-object v0, p0, LX/Ilg;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f03155d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2608018
    :goto_0
    check-cast v0, LX/IlT;

    .line 2608019
    iget-object v1, p0, LX/Ilg;->b:LX/Ilj;

    .line 2608020
    instance-of v2, p1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2608021
    check-cast p1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2608022
    iget-object v2, v1, LX/Ilj;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2608023
    iget-object v2, v1, LX/Ilj;->d:LX/03V;

    sget-object v3, LX/Ilj;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "null ViewerContextUser found when creating params for payment request"

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2608024
    const/4 v2, 0x0

    .line 2608025
    :goto_1
    move-object v1, v2

    .line 2608026
    invoke-interface {v0, v1}, LX/IlT;->setMessengerPayHistoryItemViewParams(LX/IlW;)V

    .line 2608027
    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0

    .line 2608028
    :cond_2
    iget-object v2, v1, LX/Ilj;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2608029
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2608030
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 2608031
    invoke-static {}, LX/IlU;->newBuilder()LX/IlV;

    move-result-object v2

    sget-object v4, LX/DtL;->ORION:LX/DtL;

    .line 2608032
    iput-object v4, v2, LX/IlV;->a:LX/DtL;

    .line 2608033
    move-object v4, v2

    .line 2608034
    if-nez v3, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2608035
    iput-object v2, v4, LX/IlV;->e:Ljava/lang/Boolean;

    .line 2608036
    move-object v2, v4

    .line 2608037
    invoke-static {p1, v3}, LX/Ilj;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;Z)LX/DtN;

    move-result-object v4

    invoke-interface {v4}, LX/DtN;->c()Ljava/lang/String;

    move-result-object v4

    .line 2608038
    iput-object v4, v2, LX/IlV;->b:Ljava/lang/String;

    .line 2608039
    move-object v2, v2

    .line 2608040
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v4

    .line 2608041
    new-instance v5, Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->c()I

    move-result p2

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->a()I

    move-result p3

    invoke-direct {v5, p0, p2, p3}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    move-object v4, v5

    .line 2608042
    iput-object v4, v2, LX/IlV;->c:Lcom/facebook/payments/p2p/model/Amount;

    .line 2608043
    move-object v2, v2

    .line 2608044
    sget-object v4, LX/Ili;->a:[I

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->kh_()Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2608045
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->kh_()Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 2608046
    invoke-static {}, LX/Ila;->newBuilder()LX/Ilb;

    move-result-object v4

    sget-object v5, LX/Ilc;->PENDING:LX/Ilc;

    .line 2608047
    iput-object v5, v4, LX/Ilb;->b:LX/Ilc;

    .line 2608048
    move-object v4, v4

    .line 2608049
    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    .line 2608050
    iput-object v5, v4, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2608051
    move-object v4, v4

    .line 2608052
    const-string v5, ""

    .line 2608053
    iput-object v5, v4, LX/Ilb;->c:Ljava/lang/String;

    .line 2608054
    move-object v4, v4

    .line 2608055
    invoke-virtual {v4}, LX/Ilb;->d()LX/Ila;

    move-result-object v4

    :goto_4
    move-object v4, v4

    .line 2608056
    iput-object v4, v2, LX/IlV;->d:LX/Ila;

    .line 2608057
    move-object v2, v2

    .line 2608058
    invoke-static {}, LX/Ilm;->newBuilder()LX/Iln;

    move-result-object v4

    invoke-static {p1, v3}, LX/Ilj;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;Z)LX/DtN;

    move-result-object v3

    .line 2608059
    iput-object v3, v4, LX/Iln;->a:LX/DtN;

    .line 2608060
    move-object v3, v4

    .line 2608061
    invoke-virtual {v2}, LX/IlV;->f()LX/IlU;

    move-result-object v2

    .line 2608062
    iput-object v2, v3, LX/Iln;->b:LX/IlU;

    .line 2608063
    move-object v2, v3

    .line 2608064
    invoke-virtual {v2}, LX/Iln;->c()LX/Ilm;

    move-result-object v2

    goto/16 :goto_1

    .line 2608065
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 2608066
    :pswitch_0
    invoke-static {}, LX/Ila;->newBuilder()LX/Ilb;

    move-result-object v4

    sget-object v5, LX/Ilc;->CANCELED:LX/Ilc;

    .line 2608067
    iput-object v5, v4, LX/Ilb;->b:LX/Ilc;

    .line 2608068
    move-object v4, v4

    .line 2608069
    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    .line 2608070
    iput-object v5, v4, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2608071
    move-object v4, v4

    .line 2608072
    iget-object v5, v1, LX/Ilj;->c:Landroid/content/res/Resources;

    const p0, 0x7f082c48

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2608073
    iput-object v5, v4, LX/Ilb;->c:Ljava/lang/String;

    .line 2608074
    move-object v4, v4

    .line 2608075
    invoke-virtual {v4}, LX/Ilb;->d()LX/Ila;

    move-result-object v4

    goto :goto_4

    .line 2608076
    :pswitch_1
    invoke-static {}, LX/Ila;->newBuilder()LX/Ilb;

    move-result-object v4

    sget-object v5, LX/Ilc;->CANCELED:LX/Ilc;

    .line 2608077
    iput-object v5, v4, LX/Ilb;->b:LX/Ilc;

    .line 2608078
    move-object v4, v4

    .line 2608079
    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    .line 2608080
    iput-object v5, v4, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2608081
    move-object v4, v4

    .line 2608082
    iget-object v5, v1, LX/Ilj;->c:Landroid/content/res/Resources;

    const p0, 0x7f082c47

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2608083
    iput-object v5, v4, LX/Ilb;->c:Ljava/lang/String;

    .line 2608084
    move-object v4, v4

    .line 2608085
    invoke-virtual {v4}, LX/Ilb;->d()LX/Ila;

    move-result-object v4

    goto :goto_4

    .line 2608086
    :pswitch_2
    invoke-static {}, LX/Ila;->newBuilder()LX/Ilb;

    move-result-object v4

    sget-object v5, LX/Ilc;->COMPLETED:LX/Ilc;

    .line 2608087
    iput-object v5, v4, LX/Ilb;->b:LX/Ilc;

    .line 2608088
    move-object v4, v4

    .line 2608089
    sget-object v5, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 2608090
    iput-object v5, v4, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2608091
    move-object v4, v4

    .line 2608092
    iget-object v5, v1, LX/Ilj;->c:Landroid/content/res/Resources;

    const p0, 0x7f082c49

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2608093
    iput-object v5, v4, LX/Ilb;->c:Ljava/lang/String;

    .line 2608094
    move-object v4, v4

    .line 2608095
    invoke-virtual {v4}, LX/Ilb;->d()LX/Ila;

    move-result-object v4

    goto :goto_4

    .line 2608096
    :pswitch_3
    iget-object v4, v1, LX/Ilj;->d:LX/03V;

    sget-object v5, LX/Ilj;->a:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    const-string p0, "A TRANSFER_COMPLETED request is seen by the user."

    invoke-virtual {v4, v5, p0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
