.class public LX/HfW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HfW;


# instance fields
.field private a:LX/3mF;


# direct methods
.method public constructor <init>(LX/3mF;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2491980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2491981
    iput-object p1, p0, LX/HfW;->a:LX/3mF;

    .line 2491982
    return-void
.end method

.method public static a(LX/0QB;)LX/HfW;
    .locals 4

    .prologue
    .line 2491967
    sget-object v0, LX/HfW;->b:LX/HfW;

    if-nez v0, :cond_1

    .line 2491968
    const-class v1, LX/HfW;

    monitor-enter v1

    .line 2491969
    :try_start_0
    sget-object v0, LX/HfW;->b:LX/HfW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2491970
    if-eqz v2, :cond_0

    .line 2491971
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2491972
    new-instance p0, LX/HfW;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v3

    check-cast v3, LX/3mF;

    invoke-direct {p0, v3}, LX/HfW;-><init>(LX/3mF;)V

    .line 2491973
    move-object v0, p0

    .line 2491974
    sput-object v0, LX/HfW;->b:LX/HfW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2491975
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2491976
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2491977
    :cond_1
    sget-object v0, LX/HfW;->b:LX/HfW;

    return-object v0

    .line 2491978
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2491979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;
    .locals 1
    .param p0    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel;",
            ">;)",
            "Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;"
        }
    .end annotation

    .prologue
    .line 2491948
    if-eqz p0, :cond_0

    .line 2491949
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2491950
    if-eqz v0, :cond_0

    .line 2491951
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2491952
    check-cast v0, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel;->a()Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2491953
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2491954
    check-cast v0, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel;->a()Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel;->a()Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;

    move-result-object v0

    .line 2491955
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2491962
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;->a()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    .line 2491963
    :goto_1
    return v0

    .line 2491964
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2491965
    if-nez v2, :cond_2

    move v2, v1

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2491966
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGroupJoinState;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2491956
    sget-object v0, LX/HfV;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2491957
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad group join state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2491958
    :pswitch_0
    iget-object v0, p0, LX/HfW;->a:LX/3mF;

    const-string v1, "work_mobile_groups_tab_gysj"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, p2, v1, v2}, LX/3mF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2491959
    :goto_0
    return-object v0

    .line 2491960
    :pswitch_1
    iget-object v0, p0, LX/HfW;->a:LX/3mF;

    const-string v1, "work_mobile_groups_tab_gysj"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, p2, v1, v2}, LX/3mF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2491961
    :pswitch_2
    iget-object v0, p0, LX/HfW;->a:LX/3mF;

    const-string v1, "work_mobile_groups_tab_gysj"

    const-string v2, "ALLOW_READD"

    invoke-virtual {v0, p2, v1, v2}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
