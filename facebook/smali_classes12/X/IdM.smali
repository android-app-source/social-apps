.class public final LX/IdM;
.super Lcom/facebook/widget/text/BetterButton;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

.field public b:I

.field public c:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;Landroid/content/Context;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2596771
    iput-object p1, p0, LX/IdM;->a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    .line 2596772
    invoke-direct {p0, p2}, Lcom/facebook/widget/text/BetterButton;-><init>(Landroid/content/Context;)V

    .line 2596773
    invoke-direct {p0}, LX/IdM;->d()V

    .line 2596774
    new-instance v0, LX/IdL;

    invoke-direct {v0, p0, p1}, LX/IdL;-><init>(LX/IdM;Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;)V

    invoke-virtual {p0, v0}, LX/IdM;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2596775
    return-void
.end method

.method private a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;Z)Landroid/text/SpannableStringBuilder;
    .locals 5
    .param p1    # Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2596761
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2596762
    if-nez p1, :cond_0

    move-object v0, v1

    .line 2596763
    :goto_0
    return-object v0

    .line 2596764
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2596765
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->l()I

    move-result v0

    if-lez v0, :cond_1

    .line 2596766
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->l()I

    move-result v0

    invoke-direct {p0, v1, v0, p2}, LX/IdM;->a(Landroid/text/SpannableStringBuilder;IZ)V

    .line 2596767
    :cond_1
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, LX/IdM;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz p2, :cond_2

    const v0, 0x7f0e0aa2

    :goto_1
    invoke-direct {v2, v3, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2596768
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {v1, v2, v4, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v1

    .line 2596769
    goto :goto_0

    .line 2596770
    :cond_2
    const v0, 0x7f0e0aa1

    goto :goto_1
.end method

.method private a(Landroid/text/SpannableStringBuilder;IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2596751
    invoke-virtual {p0}, LX/IdM;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020fef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2596752
    if-eqz p3, :cond_0

    const v0, 0x7f0a0811

    .line 2596753
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0}, LX/IdM;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2596754
    invoke-virtual {p0}, LX/IdM;->getTextSize()F

    move-result v0

    float-to-int v0, v0

    .line 2596755
    invoke-virtual {v1, v4, v4, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2596756
    const-string v0, "  "

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2596757
    new-instance v0, LX/34T;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2596758
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\u00d7"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2596759
    return-void

    .line 2596760
    :cond_0
    const v0, 0x7f0a0812

    goto :goto_0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2596744
    invoke-virtual {p0}, LX/IdM;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1ed5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 2596745
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2596746
    invoke-virtual {v1, v0, v4, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2596747
    invoke-virtual {p0, v1}, LX/IdM;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2596748
    invoke-virtual {p0, v4}, LX/IdM;->setAllCaps(Z)V

    .line 2596749
    invoke-virtual {p0}, LX/IdM;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/IdM;->setBackgroundColor(I)V

    .line 2596750
    return-void
.end method


# virtual methods
.method public final a(ILcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;)V
    .locals 0

    .prologue
    .line 2596740
    iput p1, p0, LX/IdM;->b:I

    .line 2596741
    iput-object p2, p0, LX/IdM;->c:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    .line 2596742
    invoke-virtual {p0}, LX/IdM;->c()V

    .line 2596743
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2596738
    iget-object v0, p0, LX/IdM;->c:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/IdM;->a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;Z)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/IdM;->setText(Ljava/lang/CharSequence;)V

    .line 2596739
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2596736
    iget-object v0, p0, LX/IdM;->c:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/IdM;->a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;Z)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/IdM;->setText(Ljava/lang/CharSequence;)V

    .line 2596737
    return-void
.end method
