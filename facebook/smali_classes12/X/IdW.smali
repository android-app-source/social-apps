.class public final LX/IdW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/IdV;

.field public b:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2596896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2596897
    sget-object v0, LX/IdV;->ALLOW:LX/IdV;

    iput-object v0, p0, LX/IdW;->a:LX/IdV;

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2596889
    iget-object v0, p0, LX/IdW;->a:LX/IdV;

    sget-object v1, LX/IdV;->CANCEL:LX/IdV;

    if-ne v0, v1, :cond_0

    .line 2596890
    const-string v0, "Cancel"

    .line 2596891
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "thread state = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", options = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/IdW;->b:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2596892
    return-object v0

    .line 2596893
    :cond_0
    iget-object v0, p0, LX/IdW;->a:LX/IdV;

    sget-object v1, LX/IdV;->ALLOW:LX/IdV;

    if-ne v0, v1, :cond_1

    .line 2596894
    const-string v0, "Allow"

    goto :goto_0

    .line 2596895
    :cond_1
    const-string v0, "?"

    goto :goto_0
.end method
