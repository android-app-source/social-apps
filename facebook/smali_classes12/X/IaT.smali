.class public final LX/IaT;
.super LX/0T0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V
    .locals 0

    .prologue
    .line 2590923
    iput-object p1, p0, LX/IaT;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-direct {p0}, LX/0T0;-><init>()V

    return-void
.end method


# virtual methods
.method public final i(Landroid/app/Activity;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2590917
    iget-object v1, p0, LX/IaT;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-boolean v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->t:Z

    if-nez v1, :cond_0

    .line 2590918
    iget-object v1, p0, LX/IaT;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    .line 2590919
    iput-boolean v0, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->t:Z

    .line 2590920
    iget-object v1, p0, LX/IaT;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    .line 2590921
    new-instance v2, LX/31Y;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v2, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const p0, 0x7f083a51

    invoke-virtual {v2, p0}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const p0, 0x7f083a52

    invoke-virtual {v2, p0}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f083a54

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/IaW;

    invoke-direct {p1, v1}, LX/IaW;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v2, p0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f080022

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/IaV;

    invoke-direct {p1, v1}, LX/IaV;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v2, p0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    .line 2590922
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
