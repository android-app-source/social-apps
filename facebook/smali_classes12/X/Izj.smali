.class public LX/Izj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/Izj;


# instance fields
.field private final b:LX/IzO;

.field private final c:LX/03V;

.field public final d:LX/Izf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2635153
    const-class v0, LX/Izj;

    sput-object v0, LX/Izj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/IzO;LX/03V;LX/Izf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2635263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635264
    iput-object p1, p0, LX/Izj;->b:LX/IzO;

    .line 2635265
    iput-object p2, p0, LX/Izj;->c:LX/03V;

    .line 2635266
    iput-object p3, p0, LX/Izj;->d:LX/Izf;

    .line 2635267
    return-void
.end method

.method public static a(LX/0QB;)LX/Izj;
    .locals 6

    .prologue
    .line 2635250
    sget-object v0, LX/Izj;->e:LX/Izj;

    if-nez v0, :cond_1

    .line 2635251
    const-class v1, LX/Izj;

    monitor-enter v1

    .line 2635252
    :try_start_0
    sget-object v0, LX/Izj;->e:LX/Izj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2635253
    if-eqz v2, :cond_0

    .line 2635254
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2635255
    new-instance p0, LX/Izj;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v3

    check-cast v3, LX/IzO;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/Izf;->a(LX/0QB;)LX/Izf;

    move-result-object v5

    check-cast v5, LX/Izf;

    invoke-direct {p0, v3, v4, v5}, LX/Izj;-><init>(LX/IzO;LX/03V;LX/Izf;)V

    .line 2635256
    move-object v0, p0

    .line 2635257
    sput-object v0, LX/Izj;->e:LX/Izj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635258
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2635259
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635260
    :cond_1
    sget-object v0, LX/Izj;->e:LX/Izj;

    return-object v0

    .line 2635261
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2635262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static d(LX/Izj;J)V
    .locals 5

    .prologue
    .line 2635230
    const-string v0, "addPaymentCardId"

    const v1, 0x11bb1d67

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635231
    :try_start_0
    iget-object v0, p0, LX/Izj;->d:LX/Izf;

    invoke-virtual {v0}, LX/Izf;->a()LX/0Px;

    move-result-object v0

    .line 2635232
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2635233
    const v0, -0x246273f8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635234
    :goto_0
    return-void

    .line 2635235
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Izj;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635236
    const v0, 0x65b2854e

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635237
    :try_start_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2635238
    sget-object v2, LX/IzQ;->a:LX/0U1;

    .line 2635239
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635240
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2635241
    const-string v2, "payment_card_ids"

    const/4 v3, 0x0

    const v4, -0x47bb791f

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x3db1a340

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2635242
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635243
    const v0, 0x162c574d

    :try_start_3
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2635244
    :goto_1
    const v0, -0x28ba1a09

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635245
    :catch_0
    move-exception v0

    .line 2635246
    :try_start_4
    iget-object v2, p0, LX/Izj;->c:LX/03V;

    const-string v3, "DbInsertPaymentCardIdsHandler"

    const-string v4, "A SQLException occurred when trying to insert into the database"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2635247
    const v0, 0x2006e340

    :try_start_5
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 2635248
    :catchall_0
    move-exception v0

    const v1, 0x26816bdc

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2635249
    :catchall_1
    move-exception v0

    const v2, -0x4fcaaca3

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2635219
    const-string v0, "deleteAllCardIds"

    const v1, 0x60f81f8e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635220
    :try_start_0
    iget-object v0, p0, LX/Izj;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635221
    const v0, -0x6018deb

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2635222
    :try_start_1
    invoke-virtual {p0}, LX/Izj;->b()V

    .line 2635223
    const-string v0, "payment_card_ids"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2635224
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635225
    const v0, -0x4bec514c

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635226
    const v0, 0x7060237b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635227
    return-void

    .line 2635228
    :catchall_0
    move-exception v0

    const v2, 0x4cadeeac    # 9.1190624E7f

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635229
    :catchall_1
    move-exception v0

    const v1, -0x5cc3f358

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 2635200
    const-string v0, "insertOrReplacePrimaryPaymentCardId"

    const v1, -0x451486fd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635201
    :try_start_0
    iget-object v0, p0, LX/Izj;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635202
    const v0, 0x744fcaf8

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635203
    :try_start_1
    invoke-static {p0, p1, p2}, LX/Izj;->d(LX/Izj;J)V

    .line 2635204
    const-string v0, "primary_payment_card_id"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2635205
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2635206
    sget-object v2, LX/IzR;->a:LX/0U1;

    .line 2635207
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635208
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2635209
    const-string v2, "primary_payment_card_id"

    const/4 v3, 0x0

    const v4, 0x65f3ea3d

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x46682aac

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2635210
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2635211
    const v0, -0x1dc00d55

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2635212
    :goto_0
    const v0, -0x10208c97

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635213
    return-void

    .line 2635214
    :catch_0
    move-exception v0

    .line 2635215
    :try_start_3
    iget-object v2, p0, LX/Izj;->c:LX/03V;

    const-string v3, "DbInsertPaymentCardIdsHandler"

    const-string v4, "A SQLException occurred when trying to insert into the database"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635216
    const v0, -0xb2148d9

    :try_start_4
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2635217
    :catchall_0
    move-exception v0

    const v1, -0x2f0d3d87

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2635218
    :catchall_1
    move-exception v0

    const v2, 0x18d93954

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2635184
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2635185
    const-string v0, "insertPaymentCardIds (%d paymentCardIds)"

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0x5162c2a4

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 2635186
    :try_start_0
    iget-object v0, p0, LX/Izj;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2635187
    const v0, 0x15dfe2ff

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635188
    :try_start_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2635189
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, LX/Izj;->d(LX/Izj;J)V

    .line 2635190
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2635191
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2635192
    const v0, -0x23d9eedc

    :try_start_2
    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2635193
    :goto_1
    const v0, -0x3706cf43

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635194
    return-void

    .line 2635195
    :catch_0
    move-exception v0

    .line 2635196
    :try_start_3
    iget-object v1, p0, LX/Izj;->c:LX/03V;

    const-string v3, "DbInsertPaymentCardIdsHandler"

    const-string v4, "A SQLException occurred when trying to insert into the database"

    invoke-virtual {v1, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635197
    const v0, 0xec6a380

    :try_start_4
    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 2635198
    :catchall_0
    move-exception v0

    const v1, -0x671d4173

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2635199
    :catchall_1
    move-exception v0

    const v1, 0x1d8a11fb

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2635174
    const-string v0, "clearPrimaryCardIdStatus"

    const v1, 0x7078a03d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635175
    :try_start_0
    iget-object v0, p0, LX/Izj;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635176
    const v0, -0x281f0960

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2635177
    :try_start_1
    const-string v0, "primary_payment_card_id"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2635178
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635179
    const v0, 0x4349d894

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635180
    const v0, 0x7d7b6c10

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635181
    return-void

    .line 2635182
    :catchall_0
    move-exception v0

    const v2, 0x41bfe70b

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635183
    :catchall_1
    move-exception v0

    const v1, -0x1c28ae93

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(J)V
    .locals 10

    .prologue
    .line 2635154
    const-string v0, "deletePaymentCardId"

    const v1, -0x4b50f90f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635155
    :try_start_0
    iget-object v0, p0, LX/Izj;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635156
    const v0, -0x3354b209    # -8.981292E7f

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2635157
    :try_start_1
    const/4 v5, 0x0

    .line 2635158
    iget-object v6, p0, LX/Izj;->d:LX/Izf;

    invoke-virtual {v6}, LX/Izf;->b()Ljava/lang/Long;

    move-result-object v6

    .line 2635159
    if-nez v6, :cond_2

    .line 2635160
    :cond_0
    :goto_0
    move v0, v5

    .line 2635161
    if-eqz v0, :cond_1

    .line 2635162
    invoke-virtual {p0}, LX/Izj;->b()V

    .line 2635163
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/IzQ;->a:LX/0U1;

    .line 2635164
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635165
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " = ? "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2635166
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 2635167
    const-string v3, "payment_card_ids"

    invoke-virtual {v1, v3, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2635168
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635169
    const v0, -0x38b33b9

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635170
    const v0, 0x217913df

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635171
    return-void

    .line 2635172
    :catchall_0
    move-exception v0

    const v2, -0x767cb517

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635173
    :catchall_1
    move-exception v0

    const v1, -0x75511a31

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_2
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v6, v7, p1

    if-nez v6, :cond_0

    const/4 v5, 0x1

    goto :goto_0
.end method
