.class public final LX/Hgx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Hgy;


# direct methods
.method public constructor <init>(LX/Hgy;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2494751
    iput-object p1, p0, LX/Hgx;->b:LX/Hgy;

    iput-object p2, p0, LX/Hgx;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2494752
    check-cast p1, Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    .line 2494753
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2494754
    const-string v0, "carrier_manager?ref={%s}"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2494755
    iget-object v0, p0, LX/Hgx;->b:LX/Hgy;

    iget-object v0, v0, LX/Hgy;->b:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    iget-object v2, p0, LX/Hgx;->a:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;Z)V

    .line 2494756
    iget-object v0, p0, LX/Hgx;->b:LX/Hgy;

    iget-object v0, v0, LX/Hgy;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0df;->E:LX/0Tn;

    invoke-interface {v0, v1, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2494757
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
