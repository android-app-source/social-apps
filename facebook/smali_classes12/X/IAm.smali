.class public final LX/IAm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1OZ;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;)V
    .locals 0

    .prologue
    .line 2546105
    iput-object p1, p0, LX/IAm;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 2546106
    iget-object v0, p0, LX/IAm;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->l:LX/IAh;

    const/4 p0, 0x1

    const/4 p1, 0x0

    .line 2546107
    invoke-virtual {v0, p3}, LX/A8Z;->e(I)LX/A8P;

    move-result-object p2

    .line 2546108
    iget-object v1, p2, LX/A8P;->b:LX/90h;

    check-cast v1, LX/IAg;

    iget p2, p2, LX/A8P;->a:I

    .line 2546109
    invoke-virtual {v1, p2}, LX/IAg;->getItem(I)Ljava/lang/Object;

    move-result-object p4

    .line 2546110
    instance-of p3, p4, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$BasicEventGuestModel;

    if-nez p3, :cond_6

    .line 2546111
    :goto_0
    const/4 v1, 0x0

    .line 2546112
    iget-object p2, v0, LX/IAh;->a:LX/0Px;

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p5

    move p2, v1

    move p4, v1

    :goto_1
    if-ge p2, p5, :cond_0

    iget-object v1, v0, LX/IAh;->a:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IAg;

    .line 2546113
    iget-object p3, v1, LX/IAg;->d:Ljava/util/HashSet;

    invoke-virtual {p3}, Ljava/util/HashSet;->size()I

    move-result p3

    move v1, p3

    .line 2546114
    add-int/2addr p4, v1

    .line 2546115
    add-int/lit8 v1, p2, 0x1

    move p2, v1

    goto :goto_1

    .line 2546116
    :cond_0
    move p4, p4

    .line 2546117
    iget v1, v0, LX/IAh;->c:I

    if-nez v1, :cond_2

    move p2, p0

    :goto_2
    if-nez p4, :cond_3

    move v1, p0

    :goto_3
    if-eq p2, v1, :cond_4

    move v1, p0

    .line 2546118
    :goto_4
    iput p4, v0, LX/IAh;->c:I

    .line 2546119
    if-eqz v1, :cond_1

    .line 2546120
    iget-object v1, v0, LX/IAh;->b:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    if-eqz v1, :cond_1

    .line 2546121
    iget-object v1, v0, LX/IAh;->b:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    iget p2, v0, LX/IAh;->c:I

    if-eqz p2, :cond_5

    .line 2546122
    :goto_5
    iput-boolean p0, v1, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->r:Z

    .line 2546123
    invoke-static {v1, p0}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->b(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;Z)V

    .line 2546124
    :cond_1
    return-void

    :cond_2
    move p2, p1

    .line 2546125
    goto :goto_2

    :cond_3
    move v1, p1

    goto :goto_3

    :cond_4
    move v1, p1

    goto :goto_4

    :cond_5
    move p0, p1

    .line 2546126
    goto :goto_5

    .line 2546127
    :cond_6
    check-cast p4, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$BasicEventGuestModel;

    invoke-virtual {p4}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$BasicEventGuestModel;->b()Ljava/lang/String;

    move-result-object p4

    .line 2546128
    iget-object p3, v1, LX/IAg;->d:Ljava/util/HashSet;

    invoke-virtual {p3, p4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_7

    .line 2546129
    iget-object p3, v1, LX/IAg;->d:Ljava/util/HashSet;

    invoke-virtual {p3, p4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2546130
    :goto_6
    invoke-virtual {v1, p2}, LX/1OM;->i_(I)V

    goto :goto_0

    .line 2546131
    :cond_7
    iget-object p3, v1, LX/IAg;->d:Ljava/util/HashSet;

    invoke-virtual {p3, p4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_6
.end method
