.class public LX/JVT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/17S;

.field private final b:LX/3iX;


# direct methods
.method public constructor <init>(LX/17S;LX/3iX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2700842
    iput-object p1, p0, LX/JVT;->a:LX/17S;

    .line 2700843
    iput-object p2, p0, LX/JVT;->b:LX/3iX;

    .line 2700844
    return-void
.end method

.method public static a(LX/0QB;)LX/JVT;
    .locals 5

    .prologue
    .line 2700845
    const-class v1, LX/JVT;

    monitor-enter v1

    .line 2700846
    :try_start_0
    sget-object v0, LX/JVT;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700847
    sput-object v2, LX/JVT;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700848
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700849
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700850
    new-instance p0, LX/JVT;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v3

    check-cast v3, LX/17S;

    invoke-static {v0}, LX/3iX;->a(LX/0QB;)LX/3iX;

    move-result-object v4

    check-cast v4, LX/3iX;

    invoke-direct {p0, v3, v4}, LX/JVT;-><init>(LX/17S;LX/3iX;)V

    .line 2700851
    move-object v0, p0

    .line 2700852
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700853
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700854
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700855
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/JVT;Lcom/facebook/graphql/model/GraphQLStory;)LX/99r;
    .locals 2

    .prologue
    .line 2700856
    iget-object v0, p0, LX/JVT;->b:LX/3iX;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iX;->b(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    move-result-object v0

    invoke-static {v0}, LX/99r;->getEntryPoint(Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)LX/99r;

    move-result-object v0

    return-object v0
.end method
