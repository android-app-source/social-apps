.class public final LX/ILa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;)V
    .locals 0

    .prologue
    .line 2568186
    iput-object p1, p0, LX/ILa;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2568187
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2568188
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2568189
    iget-object v0, p0, LX/ILa;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    iget-object v1, v0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->d:Lcom/facebook/fig/button/FigButton;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2568190
    return-void

    .line 2568191
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
