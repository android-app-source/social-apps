.class public LX/I2e;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/I2j;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I2f;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/I2e",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/I2f;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530599
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2530600
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/I2e;->b:LX/0Zi;

    .line 2530601
    iput-object p1, p0, LX/I2e;->a:LX/0Ot;

    .line 2530602
    return-void
.end method

.method public static a(LX/0QB;)LX/I2e;
    .locals 4

    .prologue
    .line 2530603
    const-class v1, LX/I2e;

    monitor-enter v1

    .line 2530604
    :try_start_0
    sget-object v0, LX/I2e;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2530605
    sput-object v2, LX/I2e;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2530606
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2530607
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2530608
    new-instance v3, LX/I2e;

    const/16 p0, 0x1b20

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/I2e;-><init>(LX/0Ot;)V

    .line 2530609
    move-object v0, v3

    .line 2530610
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2530611
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/I2e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2530612
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2530613
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2530614
    check-cast p2, LX/I2d;

    .line 2530615
    iget-object v0, p0, LX/I2e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2f;

    const/4 p2, 0x2

    .line 2530616
    iget-object v1, v0, LX/I2f;->b:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f020df7

    .line 2530617
    :goto_0
    iget-object v2, v0, LX/I2f;->a:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    const p0, 0x7f0a00e6

    invoke-virtual {v2, p0}, LX/2xv;->j(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 2530618
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const p0, 0x7f02067a

    invoke-interface {v2, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v2, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    .line 2530619
    const p0, 0x2f525820

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2530620
    invoke-interface {v2, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p2, 0x7f0821df

    invoke-virtual {p0, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0050

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a010e

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 p0, 0x6

    const p2, 0x7f0b15c1

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b1583

    invoke-interface {v1, v2}, LX/1Dh;->M(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2530621
    return-object v0

    .line 2530622
    :cond_0
    const v1, 0x7f0207eb

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2530623
    invoke-static {}, LX/1dS;->b()V

    .line 2530624
    iget v0, p1, LX/1dQ;->b:I

    .line 2530625
    packed-switch v0, :pswitch_data_0

    .line 2530626
    :goto_0
    return-object v2

    .line 2530627
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2530628
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2530629
    check-cast v1, LX/I2d;

    .line 2530630
    iget-object v3, p0, LX/I2e;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/I2f;

    iget-object p1, v1, LX/I2d;->a:LX/I2j;

    .line 2530631
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 2530632
    iget-object p0, v3, LX/I2f;->c:LX/1nQ;

    invoke-interface {p1}, LX/I2j;->s()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2530633
    iget-object v0, v1, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v1, v0

    .line 2530634
    invoke-virtual {v1}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v1

    invoke-virtual {p0, v1}, LX/1nQ;->a(I)V

    .line 2530635
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object p0, v3, LX/I2f;->d:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/ComponentName;

    invoke-virtual {v1, p0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object p0

    .line 2530636
    const-string v1, "target_fragment"

    sget-object v0, LX/0cQ;->EVENTS_DISCOVERY_UPCOMING_EVENTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2530637
    const-string v1, "extra_ref_module"

    invoke-interface {p1}, LX/I2j;->s()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2530638
    iget-object v1, v3, LX/I2f;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, p0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2530639
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2f525820
        :pswitch_0
    .end packed-switch
.end method
