.class public LX/J0S;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J0S;


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637103
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2637104
    return-void
.end method

.method public static a(LX/0QB;)LX/J0S;
    .locals 4

    .prologue
    .line 2637105
    sget-object v0, LX/J0S;->b:LX/J0S;

    if-nez v0, :cond_1

    .line 2637106
    const-class v1, LX/J0S;

    monitor-enter v1

    .line 2637107
    :try_start_0
    sget-object v0, LX/J0S;->b:LX/J0S;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637108
    if-eqz v2, :cond_0

    .line 2637109
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2637110
    new-instance p0, LX/J0S;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-direct {p0, v3}, LX/J0S;-><init>(LX/0sO;)V

    .line 2637111
    move-object v0, p0

    .line 2637112
    sput-object v0, LX/J0S;->b:LX/J0S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637113
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637114
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637115
    :cond_1
    sget-object v0, LX/J0S;->b:LX/J0S;

    return-object v0

    .line 2637116
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637117
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2637118
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2637119
    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2637120
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2637121
    check-cast p1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;

    .line 2637122
    new-instance v0, LX/Dtf;

    invoke-direct {v0}, LX/Dtf;-><init>()V

    move-object v0, v0

    .line 2637123
    const-string v1, "request_id"

    .line 2637124
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2637125
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
