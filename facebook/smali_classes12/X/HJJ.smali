.class public final LX/HJJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/25K;


# instance fields
.field public final synthetic a:LX/2km;

.field public final synthetic b:LX/HLF;

.field public final synthetic c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic d:LX/HJL;


# direct methods
.method public constructor <init>(LX/HJL;LX/2km;LX/HLF;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2452435
    iput-object p1, p0, LX/HJJ;->d:LX/HJL;

    iput-object p2, p0, LX/HJJ;->a:LX/2km;

    iput-object p3, p0, LX/HJJ;->b:LX/HLF;

    iput-object p4, p0, LX/HJJ;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILX/0Px;)V
    .locals 3

    .prologue
    .line 2452436
    iget-object v0, p0, LX/HJJ;->a:LX/2km;

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/HJJ;->b:LX/HLF;

    iget-object v2, p0, LX/HJJ;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    .line 2452437
    invoke-virtual {v0}, LX/HLG;->a()I

    move-result v1

    if-eq v1, p1, :cond_0

    .line 2452438
    const/4 v2, 0x1

    .line 2452439
    iget-object v1, v0, LX/HLG;->g:Ljava/util/Map;

    iget p2, v0, LX/HLG;->f:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2452440
    iput-boolean v2, v0, LX/HLG;->b:Z

    .line 2452441
    iget-object v1, v0, LX/HLG;->g:Ljava/util/Map;

    iget v2, v0, LX/HLG;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2452442
    :goto_0
    iget-object v1, p0, LX/HJJ;->a:LX/2km;

    check-cast v1, LX/1Pr;

    iget-object v2, p0, LX/HJJ;->b:LX/HLF;

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2452443
    :cond_0
    return-void

    .line 2452444
    :cond_1
    iget-object v1, v0, LX/HLG;->g:Ljava/util/Map;

    iget p2, v0, LX/HLG;->f:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, v0, LX/HLG;->b:Z

    .line 2452445
    iget-object v1, v0, LX/HLG;->g:Ljava/util/Map;

    iget v2, v0, LX/HLG;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2452446
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
