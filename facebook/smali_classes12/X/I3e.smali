.class public LX/I3e;
.super LX/GmZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GmZ",
        "<",
        "LX/0zO",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;",
        ">;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final ab:Ljava/lang/String;


# instance fields
.field public X:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/I3i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Z:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final ac:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLInterfaces$EventCollectionSectionEdge;",
            ">;>;"
        }
    .end annotation
.end field

.field public final ad:Lcom/facebook/events/common/EventAnalyticsParams;

.field public ae:LX/Clo;

.field private af:LX/I4X;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2531680
    const-class v0, LX/I3e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/I3e;->ab:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 1

    .prologue
    .line 2531681
    invoke-direct {p0}, LX/GmZ;-><init>()V

    .line 2531682
    new-instance v0, LX/I3d;

    invoke-direct {v0, p0}, LX/I3d;-><init>(LX/I3e;)V

    iput-object v0, p0, LX/I3e;->ac:LX/0TF;

    .line 2531683
    iput-object p1, p0, LX/I3e;->ad:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531684
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/I3e;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v1

    check-cast v1, LX/Chv;

    invoke-static {p0}, LX/I3i;->a(LX/0QB;)LX/I3i;

    move-result-object v2

    check-cast v2, LX/I3i;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object p0

    check-cast p0, LX/1My;

    iput-object v1, p1, LX/I3e;->X:LX/Chv;

    iput-object v2, p1, LX/I3e;->Y:LX/I3i;

    iput-object v3, p1, LX/I3e;->Z:LX/03V;

    iput-object p0, p1, LX/I3e;->aa:LX/1My;

    return-void
.end method

.method private static d(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2531685
    if-eqz p0, :cond_0

    const-string v0, "extra_event_collection_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2531686
    :cond_0
    const/4 v0, 0x0

    .line 2531687
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "extra_event_collection_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final A()LX/3x6;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531688
    iget-object v0, p0, LX/I3e;->af:LX/I4X;

    return-object v0
.end method

.method public final D()LX/CH4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/richdocument/fetcher/RichDocumentFetcher",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2531689
    iget-object v0, p0, LX/I3e;->Y:LX/I3i;

    return-object v0
.end method

.method public final E()LX/CGs;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/CGs",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2531585
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2531586
    invoke-static {v0}, LX/I3e;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 2531587
    new-instance v1, LX/I3h;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/I3h;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v1
.end method

.method public final O()LX/0Pq;
    .locals 1

    .prologue
    .line 2531679
    sget-object v0, LX/I3g;->a:LX/I3f;

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531677
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2531678
    invoke-static {v0}, LX/I3e;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531676
    const-string v0, "event_collection"

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2531672
    const v0, 0x7f0d167d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/ShareBar;

    .line 2531673
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/instantarticles/view/ShareBar;->setShowShareButton(Z)V

    .line 2531674
    invoke-super {p0, p1, p2}, LX/GmZ;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2531675
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/Clo;
    .locals 13

    .prologue
    .line 2531603
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2531604
    if-eqz p1, :cond_0

    .line 2531605
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531606
    if-eqz v0, :cond_0

    .line 2531607
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531608
    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2531609
    :cond_0
    iget-object v1, p0, LX/I3e;->Z:LX/03V;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/I3e;->ab:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".onParseModel"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Null GraphQLResult"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const-string v0, " "

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "for event collection id("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2531610
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 2531611
    invoke-static {v3}, LX/I3e;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 2531612
    const/4 v0, 0x0

    .line 2531613
    :goto_1
    return-object v0

    .line 2531614
    :cond_1
    const-string v0, ".getResult "

    goto :goto_0

    .line 2531615
    :cond_2
    new-instance v1, LX/I3q;

    .line 2531616
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531617
    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-direct {v1, v0}, LX/I3q;-><init>(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;)V

    .line 2531618
    new-instance v0, LX/I4E;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/I3e;->ad:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {v0, v2, v3}, LX/I4E;-><init>(Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2531619
    invoke-virtual {v1}, LX/I3q;->e()LX/B5Y;

    move-result-object v2

    check-cast v2, LX/I3p;

    .line 2531620
    new-instance v3, LX/I4C;

    iget-object v4, v0, LX/I4D;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/I4C;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, LX/I3q;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/I4C;->f(Ljava/lang/String;)LX/I4C;

    move-result-object v3

    iget-object v4, v0, LX/I4E;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531621
    iput-object v4, v3, LX/I4C;->K:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531622
    move-object v3, v3

    .line 2531623
    invoke-virtual {v2}, LX/I3p;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/I4C;->g(Ljava/lang/String;)LX/I4C;

    move-result-object v3

    .line 2531624
    new-instance v4, LX/I4L;

    invoke-virtual {v2}, LX/I3p;->q()LX/8Z4;

    move-result-object v0

    invoke-direct {v4, v0}, LX/I4L;-><init>(LX/8Z4;)V

    .line 2531625
    iget-object v0, v2, LX/I3p;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->a()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2531626
    iput-object v0, v4, LX/I4L;->b:Ljava/lang/String;

    .line 2531627
    move-object v4, v4

    .line 2531628
    iget-object v0, v2, LX/I3p;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->c()LX/8Yr;

    move-result-object v0

    move-object v0, v0

    .line 2531629
    iput-object v0, v4, LX/I4L;->c:LX/8Yr;

    .line 2531630
    move-object v4, v4

    .line 2531631
    invoke-virtual {v4}, LX/I4L;->c()LX/I4M;

    move-result-object v4

    move-object v4, v4

    .line 2531632
    iput-object v4, v3, LX/I4C;->H:LX/Clr;

    .line 2531633
    move-object v3, v3

    .line 2531634
    iget-object v4, v2, LX/I3p;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-virtual {v4}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->d()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_7

    .line 2531635
    iget-object v4, v2, LX/I3p;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-virtual {v4}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->d()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v0, 0x0

    const-class v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;

    invoke-virtual {v5, v4, v0, v1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    .line 2531636
    :goto_2
    move-object v4, v4

    .line 2531637
    iget-object v5, v3, LX/I4C;->K:Lcom/facebook/events/common/EventAnalyticsParams;

    const/4 v1, 0x0

    .line 2531638
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;

    invoke-virtual {v6}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;

    invoke-virtual {v6}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 2531639
    new-instance v0, LX/I4P;

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;

    invoke-direct {v0, v5, v6}, LX/I4P;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;)V

    .line 2531640
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x2

    if-lt v6, v1, :cond_3

    .line 2531641
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;

    .line 2531642
    iput-object v6, v0, LX/I4P;->c:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;

    .line 2531643
    :cond_3
    invoke-virtual {v0}, LX/I4P;->c()LX/I4Q;

    move-result-object v6

    .line 2531644
    :goto_3
    move-object v5, v6

    .line 2531645
    iput-object v5, v3, LX/I4C;->I:LX/Clr;

    .line 2531646
    move-object v3, v3

    .line 2531647
    invoke-virtual {v2}, LX/I3p;->c()I

    move-result v4

    invoke-virtual {v3, v4}, LX/I4C;->b(I)LX/I4C;

    move-result-object v3

    .line 2531648
    invoke-virtual {v2}, LX/I3p;->l()LX/B5X;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/I4B;->a(LX/B5X;)LX/I4B;

    .line 2531649
    invoke-virtual {v3}, LX/I4B;->b()LX/Clo;

    move-result-object v2

    move-object v0, v2

    .line 2531650
    iput-object v0, p0, LX/I3e;->ae:LX/Clo;

    .line 2531651
    iget-object v0, p0, LX/I3e;->X:LX/Chv;

    new-instance v1, LX/CiP;

    iget-object v2, p0, LX/I3e;->ae:LX/Clo;

    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    invoke-direct {v1, v2, v3}, LX/CiP;-><init>(LX/Clo;LX/0ta;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2531652
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531653
    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;

    move-result-object v0

    .line 2531654
    if-eqz v0, :cond_5

    .line 2531655
    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;

    .line 2531656
    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;->a()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->c()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->FACEBOOK_EVENT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v4, v5, :cond_4

    .line 2531657
    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;->a()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;->a()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->d()LX/7oa;

    move-result-object v6

    if-nez v6, :cond_9

    .line 2531658
    :cond_4
    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2531659
    :cond_5
    iget-object v0, p0, LX/I3e;->ae:LX/Clo;

    goto/16 :goto_1

    .line 2531660
    :cond_6
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2531661
    goto/16 :goto_2

    .line 2531662
    :cond_7
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2531663
    goto/16 :goto_2

    :cond_8
    const/4 v6, 0x0

    goto :goto_3

    .line 2531664
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel;->a()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->d()LX/7oa;

    move-result-object v6

    invoke-interface {v6}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v6

    .line 2531665
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 2531666
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    .line 2531667
    invoke-interface {v12, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2531668
    new-instance v7, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2531669
    iget-object v8, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v9, v8

    .line 2531670
    iget-wide v10, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-object v8, v0

    invoke-direct/range {v7 .. v12}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2531671
    iget-object v8, p0, LX/I3e;->aa:LX/1My;

    iget-object v9, p0, LX/I3e;->ac:LX/0TF;

    invoke-virtual {v8, v9, v6, v7}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_5
.end method

.method public final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2531600
    const-class v0, LX/I3e;

    invoke-static {v0, p0}, LX/I3e;->a(Ljava/lang/Class;LX/02k;)V

    .line 2531601
    new-instance v0, LX/I4X;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I4X;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/I3e;->af:LX/I4X;

    .line 2531602
    invoke-super {p0, p1, p2, p3}, LX/GmZ;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2531595
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2531596
    const-string v1, "event_collection_id"

    .line 2531597
    iget-object v2, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v2, v2

    .line 2531598
    invoke-static {v2}, LX/I3e;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2531599
    return-object v0
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2531588
    iget-object v0, p0, LX/I3e;->Z:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/I3e;->ab:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".onFetchError"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error attempting to fetch blocks. event collection id("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2531589
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 2531590
    invoke-static {v3}, LX/I3e;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2531591
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2531592
    move-object v1, v1

    .line 2531593
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2531594
    return-void
.end method
