.class public LX/Hsu;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/FrameLayout;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2515162
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2515163
    const p1, 0x7f0303f3

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2515164
    const p1, 0x7f0d0553

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    iput-object p1, p0, LX/Hsu;->a:Landroid/widget/FrameLayout;

    .line 2515165
    const p1, 0x7f0d0a7e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/Hsu;->b:Landroid/view/View;

    .line 2515166
    const p1, 0x7f0d05b0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/Hsu;->c:Landroid/view/View;

    .line 2515167
    return-void
.end method


# virtual methods
.method public setLoadingIndicatorVisibility(Z)V
    .locals 2

    .prologue
    .line 2515171
    iget-object v1, p0, LX/Hsu;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2515172
    return-void

    .line 2515173
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2515174
    iget-object v0, p0, LX/Hsu;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2515175
    return-void
.end method

.method public setShowRemoveButton(Z)V
    .locals 2

    .prologue
    .line 2515168
    iget-object v1, p0, LX/Hsu;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2515169
    return-void

    .line 2515170
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
