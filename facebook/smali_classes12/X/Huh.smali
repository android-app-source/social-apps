.class public final LX/Huh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/resources/ui/FbTextView;

.field public final synthetic b:LX/Huj;


# direct methods
.method public constructor <init>(LX/Huj;Lcom/facebook/resources/ui/FbTextView;)V
    .locals 0

    .prologue
    .line 2517841
    iput-object p1, p0, LX/Huh;->b:LX/Huj;

    iput-object p2, p0, LX/Huh;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x31018ef8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2517842
    iget-object v1, p0, LX/Huh;->b:LX/Huj;

    iget-object v1, v1, LX/Huj;->i:LX/Hqb;

    const/4 v4, 0x0

    .line 2517843
    iget-object v3, v1, LX/Hqb;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v3}, LX/2zG;->E()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/Hqb;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, LX/Hqb;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short p1, LX/1EB;->ar:S

    invoke-interface {v3, p1, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_0
    move v3, v4

    .line 2517844
    :goto_0
    move v1, v3

    .line 2517845
    if-eqz v1, :cond_1

    .line 2517846
    const v1, -0x4ea38526

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2517847
    :goto_1
    return-void

    .line 2517848
    :cond_1
    iget-object v1, p0, LX/Huh;->b:LX/Huj;

    iget-object v1, v1, LX/Huj;->g:LX/0hs;

    if-nez v1, :cond_2

    .line 2517849
    iget-object v1, p0, LX/Huh;->b:LX/Huj;

    new-instance v2, LX/0hs;

    iget-object v3, p0, LX/Huh;->b:LX/Huj;

    iget-object v3, v3, LX/Huj;->f:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0hs;-><init>(Landroid/content/Context;)V

    .line 2517850
    iput-object v2, v1, LX/Huj;->g:LX/0hs;

    .line 2517851
    iget-object v1, p0, LX/Huh;->b:LX/Huj;

    iget-object v1, v1, LX/Huj;->g:LX/0hs;

    const/4 v2, -0x1

    .line 2517852
    iput v2, v1, LX/0hs;->t:I

    .line 2517853
    iget-object v1, p0, LX/Huh;->b:LX/Huj;

    iget-object v1, v1, LX/Huj;->g:LX/0hs;

    iget-object v2, p0, LX/Huh;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, LX/0ht;->c(Landroid/view/View;)V

    .line 2517854
    :cond_2
    iget-object v1, p0, LX/Huh;->b:LX/Huj;

    iget-object v1, v1, LX/Huj;->g:LX/0hs;

    iget-object v2, p0, LX/Huh;->b:LX/Huj;

    iget-object v2, v2, LX/Huj;->h:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v2, v2, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2517855
    iget-object v1, p0, LX/Huh;->b:LX/Huj;

    iget-object v1, v1, LX/Huj;->g:LX/0hs;

    invoke-virtual {v1}, LX/0ht;->d()V

    .line 2517856
    const v1, 0x2a47bddf

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 2517857
    :cond_3
    iget-object v3, v1, LX/Hqb;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v3}, Lcom/facebook/composer/activity/ComposerFragment;->M$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2517858
    const/4 v3, 0x1

    goto :goto_0
.end method
