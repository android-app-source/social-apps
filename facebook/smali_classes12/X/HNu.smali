.class public final LX/HNu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459074
    iput-object p1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x1635575c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459075
    iget-object v1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v2, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/CYT;->a(Ljava/lang/String;)V

    .line 2459076
    iget-object v1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2459077
    iget-object v1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2459078
    iget-object v1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    const-string v2, "admin_edit_services"

    invoke-static {v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/String;)V

    .line 2459079
    :goto_0
    iget-object v1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v2, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/CYT;->e(Ljava/lang/String;)V

    .line 2459080
    iget-object v1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v2, LX/0ig;->bd:LX/0ih;

    const-string v3, "tap_service_section"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2459081
    const v1, -0x29311816

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2459082
    :cond_0
    iget-object v1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    const-string v2, "admin_publish_services_edit_flow"

    invoke-static {v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 2459083
    :cond_1
    iget-object v1, p0, LX/HNu;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-static {v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->k$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    goto :goto_0
.end method
