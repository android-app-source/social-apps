.class public LX/J9m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J9l;


# instance fields
.field private final a:LX/JDA;

.field private final b:LX/J94;

.field private final c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field private final d:Landroid/view/LayoutInflater;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLInterfaces$AppCollectionItem$;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;


# direct methods
.method public constructor <init>(LX/JDA;LX/J94;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;)V
    .locals 1
    .param p3    # Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/view/LayoutInflater;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2653521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2653522
    iput-object p1, p0, LX/J9m;->a:LX/JDA;

    .line 2653523
    iput-object p2, p0, LX/J9m;->b:LX/J94;

    .line 2653524
    iput-object p3, p0, LX/J9m;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 2653525
    iput-object p4, p0, LX/J9m;->d:Landroid/view/LayoutInflater;

    .line 2653526
    const/4 v0, 0x0

    iput-object v0, p0, LX/J9m;->f:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653527
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2653520
    const/16 v0, 0x24

    return v0
.end method

.method public final a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/0us;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateCollection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2653512
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653513
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653514
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653515
    iput-object p2, p0, LX/J9m;->f:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653516
    iget-object v0, p0, LX/J9m;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2653517
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/J9m;->e:Ljava/util/List;

    .line 2653518
    :cond_0
    iget-object v0, p0, LX/J9m;->e:Ljava/util/List;

    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2653519
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->b()LX/0us;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;LX/J9V;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2653502
    sget-object v0, LX/J9j;->a:[I

    invoke-virtual {p2}, LX/J9V;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2653503
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown type in GenericCollectionSubAdapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2653504
    :pswitch_0
    iget-object v0, p0, LX/J9m;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    iget-object v1, p0, LX/J9m;->d:Landroid/view/LayoutInflater;

    invoke-static {v0, v1, p3}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2653505
    iget-object v2, p0, LX/J9m;->d:Landroid/view/LayoutInflater;

    invoke-static {v0, v2}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 2653506
    :goto_0
    return-object v0

    .line 2653507
    :pswitch_1
    iget-object v0, p0, LX/J9m;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    iget-object v1, p0, LX/J9m;->d:Landroid/view/LayoutInflater;

    invoke-static {v0, v1, p3}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2653508
    const v1, 0x7f0d0963

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2653509
    if-eqz v1, :cond_0

    .line 2653510
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2653511
    :cond_0
    iget-object v2, p0, LX/J9m;->d:Landroid/view/LayoutInflater;

    invoke-static {v0, v2}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2653499
    iget-object v0, p0, LX/J9m;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2653500
    iget-object v0, p0, LX/J9m;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2653501
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;LX/9lP;)V
    .locals 2

    .prologue
    .line 2653496
    invoke-static {p2}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/JCi;

    .line 2653497
    check-cast p1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    iget-object v1, p0, LX/J9m;->f:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-nez v1, :cond_0

    :cond_0
    invoke-interface {v0, p1}, LX/JCi;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    .line 2653498
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2653495
    iget-object v0, p0, LX/J9m;->a:LX/JDA;

    iget-object v1, p0, LX/J9m;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0, v1}, LX/JDA;->b(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)I

    move-result v0

    return v0
.end method

.method public final b(I)LX/J9V;
    .locals 1

    .prologue
    .line 2653492
    invoke-virtual {p0}, LX/J9m;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2653493
    sget-object v0, LX/J9V;->SUB_ADAPTER_ITEM_BOTTOM:LX/J9V;

    .line 2653494
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/J9V;->SUB_ADAPTER_ITEM_MIDDLE:LX/J9V;

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2653486
    const/4 v0, 0x0

    iput-object v0, p0, LX/J9m;->e:Ljava/util/List;

    .line 2653487
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2653489
    iget-object v0, p0, LX/J9m;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2653490
    iget-object v0, p0, LX/J9m;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2653491
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
    .locals 1

    .prologue
    .line 2653488
    iget-object v0, p0, LX/J9m;->c:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    return-object v0
.end method
