.class public LX/HVs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/HVa;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/17Y;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/HVa;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HVa;",
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475438
    iput-object p1, p0, LX/HVs;->a:LX/HVa;

    .line 2475439
    iput-object p3, p0, LX/HVs;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2475440
    iput-object p4, p0, LX/HVs;->c:LX/17Y;

    .line 2475441
    iput-object p2, p0, LX/HVs;->d:LX/0Or;

    .line 2475442
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2475443
    iget-boolean v0, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->b:Z

    if-eqz v0, :cond_1

    .line 2475444
    sget-object v0, LX/0ax;->eE:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2475445
    iget-object v1, p0, LX/HVs;->c:LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2475446
    if-eqz v0, :cond_0

    .line 2475447
    iget-object v1, p0, LX/HVs;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2475448
    :cond_0
    :goto_0
    return-void

    .line 2475449
    :cond_1
    iget-boolean v0, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HVs;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/FQe;->b:I

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2475450
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2475451
    iget-object v1, p0, LX/HVs;->c:LX/17Y;

    sget-object v2, LX/0ax;->eF:Ljava/lang/String;

    iget-wide v4, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2475452
    const-string v2, "profile_name"

    iget-object v3, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2475453
    iget-object v2, p0, LX/HVs;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2475454
    :cond_2
    iget-object v0, p0, LX/HVs;->a:LX/HVa;

    invoke-virtual {v0, p1, p2, p3}, LX/HVa;->onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V

    goto :goto_0
.end method
