.class public LX/IuR;
.super LX/IuO;
.source ""


# instance fields
.field private final m:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Ec;LX/FDp;LX/2Ox;LX/2Oi;LX/2P4;LX/IuT;LX/DoY;LX/2NB;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/7V0;LX/2Ow;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2PJ;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;LX/0Or;LX/IuN;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            "LX/FDp;",
            "LX/2Ox;",
            "LX/2Oi;",
            "LX/2P4;",
            "LX/IuT;",
            "LX/DoY;",
            "LX/2NB;",
            "LX/2P0;",
            "Lcom/facebook/messaging/tincan/messenger/interfaces/PreKeyManager;",
            "LX/7V0;",
            "LX/2Ow;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/outbound/Sender;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2PJ;",
            "Lcom/facebook/messaging/tincan/messenger/interfaces/MessengerErrorGenerator;",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/IuN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2626774
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p2

    move-object/from16 v8, p6

    move-object/from16 v9, p11

    move-object/from16 v10, p12

    move-object/from16 v11, p19

    move-object/from16 v12, p8

    move-object/from16 v13, p7

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    move-object/from16 v16, p13

    move-object/from16 v17, p14

    move-object/from16 v18, p15

    move-object/from16 v19, p16

    invoke-direct/range {v2 .. v19}, LX/IuO;-><init>(LX/3Ec;LX/2Ox;LX/2Oi;LX/2P4;LX/FDp;LX/IuT;LX/7V0;LX/2Ow;LX/IuN;LX/2NB;LX/DoY;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2PJ;)V

    .line 2626775
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuR;->m:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    .line 2626776
    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, LX/IuR;->n:LX/0Or;

    .line 2626777
    return-void
.end method

.method private d(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2626778
    iget-object v0, p0, LX/IuO;->d:LX/2P4;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {v0, v2, v3}, LX/2P4;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 2626779
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626780
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
    .locals 4

    .prologue
    .line 2626781
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 2626782
    iget-object v2, p0, LX/IuO;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2626783
    iget-object v2, p0, LX/IuO;->d:LX/2P4;

    invoke-virtual {v2, v0, v1}, LX/2P4;->b(J)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/IuO;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2626784
    :cond_0
    iget-object v0, p0, LX/IuO;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626785
    :cond_1
    :goto_0
    return-object p1

    .line 2626786
    :cond_2
    iget-object v1, p0, LX/IuR;->m:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    iget-object v0, p0, LX/IuR;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0806c5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 1

    .prologue
    .line 2626787
    iget-object v0, p0, LX/IuO;->a:LX/3Ec;

    invoke-virtual {v0, p1, p2}, LX/3Ec;->c(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)V
    .locals 11

    .prologue
    .line 2626788
    :try_start_0
    iget-object v0, p0, LX/IuO;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v1, p0, LX/IuO;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    iget-object v3, p0, LX/IuO;->l:LX/2PJ;

    invoke-virtual {v3}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-direct {p0, p1}, LX/IuR;->d(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p2, LX/IuS;->a:[B

    iget-object v8, p2, LX/IuS;->b:[B

    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    const-string v10, "UTF-8"

    invoke-virtual {v9, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual/range {v0 .. v10}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(JLjava/lang/String;JLjava/lang/String;[B[B[BZ)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2626789
    :goto_0
    return-void

    .line 2626790
    :catch_0
    move-exception v0

    .line 2626791
    const-class v1, LX/IuR;

    const-string v2, "Could not encode message ID into packet ID"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2626792
    iget-object v0, p0, LX/IuO;->g:LX/IuN;

    invoke-virtual {v0, p1}, LX/IuN;->b(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2

    .prologue
    .line 2626793
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/IuO;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2626794
    invoke-super {p0, p1, p2}, LX/IuO;->b(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)Lcom/facebook/messaging/model/messages/Message;

    .line 2626795
    :goto_0
    return-object p1

    .line 2626796
    :cond_0
    invoke-virtual {p0, p1}, LX/IuO;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object p1

    goto :goto_0
.end method
