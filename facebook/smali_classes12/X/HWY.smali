.class public final LX/HWY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2476532
    iput-object p1, p0, LX/HWY;->c:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    iput-object p2, p0, LX/HWY;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HWY;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v0, 0x1

    const v1, 0x6fd1d3e8

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2476533
    iget-object v0, p0, LX/HWY;->c:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->b:LX/Bgf;

    iget-object v0, p0, LX/HWY;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/HWY;->b:Ljava/lang/String;

    const-string v5, ""

    const/4 v6, 0x0

    const-string v7, "android_page_more_information_suggest_edits"

    invoke-virtual/range {v1 .. v7}, LX/Bgf;->a(JLjava/lang/String;Ljava/lang/String;LX/CdT;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2476534
    if-nez v1, :cond_0

    .line 2476535
    iget-object v0, p0, LX/HWY;->c:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->c:LX/03V;

    const-string v1, "page_identity_suggest_edit_fail"

    const-string v2, "Failed to resolve suggest edits intent!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2476536
    const v0, 0x79f1e443

    invoke-static {v9, v9, v0, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2476537
    :goto_0
    return-void

    .line 2476538
    :cond_0
    iget-object v0, p0, LX/HWY;->c:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->e:LX/9XE;

    sget-object v2, LX/9XI;->EVENT_TAPPED_SUGGEST_EDIT:LX/9XI;

    iget-object v3, p0, LX/HWY;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, LX/9XE;->a(LX/9X2;J)V

    .line 2476539
    iget-object v0, p0, LX/HWY;->c:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->d:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x2776

    iget-object v0, p0, LX/HWY;->c:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2476540
    const v0, -0x291527d2

    invoke-static {v0, v8}, LX/02F;->a(II)V

    goto :goto_0
.end method
