.class public LX/JIh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BRt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BRt",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLInterfaces$DiscoveryCardActionFields;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/JIh;


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/timeline/widget/actionbar/IsCopyProfileLinkEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation runtime Lcom/facebook/timeline/widget/actionbar/IsAddToGroupsMenuItemEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAWorkUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2678295
    iput-object p1, p0, LX/JIh;->e:LX/0ad;

    .line 2678296
    return-void
.end method

.method public static a(LX/0QB;)LX/JIh;
    .locals 7

    .prologue
    .line 2678297
    sget-object v0, LX/JIh;->f:LX/JIh;

    if-nez v0, :cond_1

    .line 2678298
    const-class v1, LX/JIh;

    monitor-enter v1

    .line 2678299
    :try_start_0
    sget-object v0, LX/JIh;->f:LX/JIh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2678300
    if-eqz v2, :cond_0

    .line 2678301
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2678302
    new-instance v4, LX/JIh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v3}, LX/JIh;-><init>(LX/0ad;)V

    .line 2678303
    const/16 v3, 0x1588

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v5, 0x1032

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x36b

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x2fc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2678304
    iput-object v3, v4, LX/JIh;->a:LX/0Or;

    iput-object v5, v4, LX/JIh;->b:LX/0Or;

    iput-object v6, v4, LX/JIh;->c:LX/0Or;

    iput-object p0, v4, LX/JIh;->d:LX/0Or;

    .line 2678305
    move-object v0, v4

    .line 2678306
    sput-object v0, LX/JIh;->f:LX/JIh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2678307
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2678308
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2678309
    :cond_1
    sget-object v0, LX/JIh;->f:LX/JIh;

    return-object v0

    .line 2678310
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2678311
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/BS1;Z)V
    .locals 11

    .prologue
    .line 2678312
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2678313
    iget-object v0, p0, LX/JIh;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 2678314
    iget-object v1, p0, LX/JIh;->e:LX/0ad;

    invoke-static {p1, v0, v1, p2, p3}, LX/BRx;->a(LX/5wN;LX/03R;LX/0ad;LX/BS1;Z)V

    .line 2678315
    const/4 v6, 0x2

    const/4 v7, 0x1

    .line 2678316
    invoke-interface {p1}, LX/FSi;->p()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v3, :cond_1

    .line 2678317
    const/16 v3, 0x11

    const v4, 0x7f083a68

    const v5, 0x7f020781

    move-object v2, p2

    move v8, v7

    invoke-interface/range {v2 .. v8}, LX/BS1;->a(IIIIZZ)V

    .line 2678318
    :cond_0
    :goto_0
    iget-object v0, p0, LX/JIh;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 2678319
    iget-object v1, p0, LX/JIh;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    .line 2678320
    invoke-static {v1, p1, v0, p2}, LX/BRy;->a(LX/0ad;LX/5wO;LX/03R;LX/BS1;)V

    .line 2678321
    iget-object v0, p0, LX/JIh;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 2678322
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0, p2}, LX/BRv;->a(ZLX/BS1;)V

    .line 2678323
    return-void

    .line 2678324
    :cond_1
    invoke-interface {p1}, LX/FSi;->p()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v3, :cond_0

    .line 2678325
    const/16 v3, 0x12

    const v4, 0x7f083a69

    const v5, 0x7f020781

    move-object v2, p2

    move v8, v7

    move v9, v7

    move v10, v7

    invoke-interface/range {v2 .. v10}, LX/BS1;->a(IIIIZZZZ)V

    goto :goto_0
.end method
