.class public final LX/J1g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/J1i;


# direct methods
.method public constructor <init>(LX/J1i;I)V
    .locals 0

    .prologue
    .line 2639086
    iput-object p1, p0, LX/J1g;->b:LX/J1i;

    iput p2, p0, LX/J1g;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    .line 2639087
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2639088
    iget-object v1, p0, LX/J1g;->b:LX/J1i;

    iget-object v1, v1, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 2639089
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v2

    .line 2639090
    iget v3, p0, LX/J1g;->a:I

    if-ge v2, v3, :cond_0

    .line 2639091
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2639092
    :goto_0
    return-void

    .line 2639093
    :cond_0
    new-instance v2, LX/J1l;

    invoke-direct {v2, v0}, LX/J1l;-><init>(F)V

    iget v0, p0, LX/J1g;->a:I

    add-int/lit8 v0, v0, -0x1

    iget v3, p0, LX/J1g;->a:I

    const/16 v4, 0x12

    invoke-interface {v1, v2, v0, v3, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method
