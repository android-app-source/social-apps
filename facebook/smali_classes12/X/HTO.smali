.class public LX/HTO;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Landroid/widget/TextView;

.field public c:Lcom/facebook/widget/CustomLinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2469477
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/HTO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2469478
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2469479
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2469480
    const v0, 0x7f030e52

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2469481
    const v0, 0x7f0d22ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/HTO;->c:Lcom/facebook/widget/CustomLinearLayout;

    .line 2469482
    const v0, 0x7f0d22f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/HTO;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2469483
    const v0, 0x7f0d22f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HTO;->b:Landroid/widget/TextView;

    .line 2469484
    return-void
.end method
