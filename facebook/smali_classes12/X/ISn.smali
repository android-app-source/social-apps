.class public LX/ISn;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Lcom/facebook/content/SecureContextHelper;

.field public static b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public static c:LX/9at;

.field public static d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

.field private static f:LX/0Zb;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/9at;LX/0Or;Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;LX/0Zb;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/9at;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2578345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2578346
    sput-object p1, LX/ISn;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2578347
    sput-object p2, LX/ISn;->b:LX/0Or;

    .line 2578348
    sput-object p3, LX/ISn;->c:LX/9at;

    .line 2578349
    sput-object p4, LX/ISn;->d:LX/0Or;

    .line 2578350
    sput-object p5, LX/ISn;->e:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    .line 2578351
    sput-object p6, LX/ISn;->f:LX/0Zb;

    .line 2578352
    return-void
.end method

.method public static a(LX/0QB;)LX/ISn;
    .locals 8

    .prologue
    .line 2578342
    new-instance v1, LX/ISn;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0xc

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/9at;->b(LX/0QB;)LX/9at;

    move-result-object v4

    check-cast v4, LX/9at;

    const/16 v5, 0x19e

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->b(LX/0QB;)Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    move-result-object v6

    check-cast v6, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-direct/range {v1 .. v7}, LX/ISn;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/9at;LX/0Or;Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;LX/0Zb;)V

    .line 2578343
    move-object v0, v1

    .line 2578344
    return-object v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2578337
    sget-object v0, LX/ISn;->f:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2578338
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2578339
    const-string v1, "group_inline_composer"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2578340
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2578341
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2578334
    sget-object v0, LX/ISn;->e:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    invoke-virtual {v0, p0, p1, p2}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2578335
    sget-object v1, LX/ISn;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2578336
    return-void
.end method
