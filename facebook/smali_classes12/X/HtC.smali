.class public LX/HtC;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/HtB;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2515613
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2515614
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/HtB;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesRemovedUrls;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j5;",
            ":",
            "LX/0j6;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "PluginData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAreAttachmentsReadOnlyGetter;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicSetters$SetsRemovedURLs",
            "<TMutation;>;:",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams$SetsShareParams",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0in",
            "<TPluginData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;)",
            "LX/HtB",
            "<TModelData;TDerivedData;TPluginData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2515615
    new-instance v0, LX/HtB;

    .line 2515616
    new-instance v3, LX/Hsi;

    invoke-direct {v3}, LX/Hsi;-><init>()V

    .line 2515617
    const/16 v1, 0x14d1

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v2

    check-cast v2, LX/0SI;

    .line 2515618
    iput-object v4, v3, LX/Hsi;->a:LX/0Or;

    iput-object v1, v3, LX/Hsi;->c:LX/0aG;

    iput-object v2, v3, LX/Hsi;->d:LX/0SI;

    .line 2515619
    move-object v1, v3

    .line 2515620
    check-cast v1, LX/Hsi;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->b(LX/0QB;)Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-static {p0}, LX/Hsl;->a(LX/0QB;)LX/Hsl;

    move-result-object v4

    check-cast v4, LX/Hsl;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v7

    check-cast v7, LX/0gd;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    move-object v9, p1

    check-cast v9, LX/0il;

    invoke-direct/range {v0 .. v9}, LX/HtB;-><init>(LX/Hsi;Landroid/content/res/Resources;Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;LX/Hsl;LX/03V;LX/0ad;LX/0gd;LX/1Ck;LX/0il;)V

    .line 2515621
    return-object v0
.end method
