.class public final LX/I0n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;)V
    .locals 0

    .prologue
    .line 2527819
    iput-object p1, p0, LX/I0n;->a:Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x359229ca    # -3896717.5f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2527820
    iget-object v1, p0, LX/I0n;->a:Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->m:LX/7oa;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/I0n;->a:Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->n:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v1, :cond_1

    .line 2527821
    :cond_0
    const v1, 0x4428cc8c

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2527822
    :goto_0
    return-void

    .line 2527823
    :cond_1
    iget-object v1, p0, LX/I0n;->a:Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->c:LX/Blh;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/I0n;->a:Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    iget-object v3, v3, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->m:LX/7oa;

    invoke-interface {v3}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/I0n;->a:Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    iget-object v4, v4, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->n:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_CALENDAR_TAB_INVITATION:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/Blh;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2527824
    const v1, 0xb4a35ac

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
