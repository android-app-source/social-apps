.class public LX/HyT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/Bky;

.field private c:LX/HyO;

.field private d:LX/0TD;

.field public e:I

.field private f:LX/Bl1;

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Bky;LX/Bl1;LX/HyO;LX/0TD;)V
    .locals 0
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2524225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2524226
    iput-object p1, p0, LX/HyT;->a:Landroid/content/Context;

    .line 2524227
    iput-object p2, p0, LX/HyT;->b:LX/Bky;

    .line 2524228
    iput-object p3, p0, LX/HyT;->f:LX/Bl1;

    .line 2524229
    iput-object p4, p0, LX/HyT;->c:LX/HyO;

    .line 2524230
    iput-object p5, p0, LX/HyT;->d:LX/0TD;

    .line 2524231
    return-void
.end method

.method public static a(LX/0QB;)LX/HyT;
    .locals 1

    .prologue
    .line 2524232
    invoke-static {p0}, LX/HyT;->b(LX/0QB;)LX/HyT;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/HyT;
    .locals 6

    .prologue
    .line 2524233
    new-instance v0, LX/HyT;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v2

    check-cast v2, LX/Bky;

    invoke-static {p0}, LX/Bl1;->a(LX/0QB;)LX/Bl1;

    move-result-object v3

    check-cast v3, LX/Bl1;

    invoke-static {p0}, LX/HyO;->a(LX/0QB;)LX/HyO;

    move-result-object v4

    check-cast v4, LX/HyO;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-direct/range {v0 .. v5}, LX/HyT;-><init>(Landroid/content/Context;LX/Bky;LX/Bl1;LX/HyO;LX/0TD;)V

    .line 2524234
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;I)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2524235
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2524236
    iget-object v1, p0, LX/HyT;->f:LX/Bl1;

    invoke-virtual {v1, p1}, LX/Bl1;->a(Landroid/database/Cursor;)V

    .line 2524237
    :goto_0
    iget v1, p0, LX/HyT;->e:I

    iget-object v2, p0, LX/HyT;->f:LX/Bl1;

    invoke-virtual {v2}, LX/Bl1;->b()I

    move-result v2

    if-ge v1, v2, :cond_0

    if-lez p2, :cond_0

    .line 2524238
    iget-object v1, p0, LX/HyT;->f:LX/Bl1;

    iget v2, p0, LX/HyT;->e:I

    invoke-virtual {v1, v2}, LX/Bl1;->a(I)Z

    .line 2524239
    iget-object v1, p0, LX/HyT;->f:LX/Bl1;

    invoke-virtual {v1}, LX/Bl1;->d()Lcom/facebook/events/model/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2524240
    iget v1, p0, LX/HyT;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/HyT;->e:I

    .line 2524241
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 2524242
    :cond_0
    const/4 v1, 0x0

    .line 2524243
    iput v1, p0, LX/HyT;->e:I

    .line 2524244
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2524245
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 2524246
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/HyT;->g:LX/0Px;

    .line 2524247
    iget-object v0, p0, LX/HyT;->g:LX/0Px;

    return-object v0
.end method

.method public final a(LX/Hx6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Hx6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2524248
    iget-object v0, p0, LX/HyT;->c:LX/HyO;

    sget-object v1, LX/HyN;->DB_FETCH:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->a(LX/HyN;)V

    .line 2524249
    iget-object v0, p0, LX/HyT;->d:LX/0TD;

    new-instance v1, LX/HyR;

    invoke-direct {v1, p0, p1}, LX/HyR;-><init>(LX/HyT;LX/Hx6;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
