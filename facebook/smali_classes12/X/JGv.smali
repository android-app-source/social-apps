.class public final LX/JGv;
.super LX/JGu;
.source ""


# instance fields
.field private final e:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;FFFFIZ)V
    .locals 7

    .prologue
    .line 2669474
    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, LX/JGu;-><init>(FFFFIZ)V

    .line 2669475
    iput-object p1, p0, LX/JGv;->e:Landroid/graphics/Rect;

    .line 2669476
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 2

    .prologue
    .line 2669472
    iget v0, p0, LX/JGu;->e:F

    move v0, v0

    .line 2669473
    iget-object v1, p0, LX/JGv;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public final a(FF)Z
    .locals 1

    .prologue
    .line 2669465
    invoke-virtual {p0}, LX/JGv;->a()F

    move-result v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    invoke-virtual {p0}, LX/JGv;->c()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    invoke-virtual {p0}, LX/JGv;->b()F

    move-result v0

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, LX/JGv;->d()F

    move-result v0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()F
    .locals 2

    .prologue
    .line 2669470
    iget v0, p0, LX/JGu;->f:F

    move v0, v0

    .line 2669471
    iget-object v1, p0, LX/JGv;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public final c()F
    .locals 2

    .prologue
    .line 2669468
    iget v0, p0, LX/JGu;->g:F

    move v0, v0

    .line 2669469
    iget-object v1, p0, LX/JGv;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method

.method public final d()F
    .locals 2

    .prologue
    .line 2669466
    iget v0, p0, LX/JGu;->h:F

    move v0, v0

    .line 2669467
    iget-object v1, p0, LX/JGv;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method
