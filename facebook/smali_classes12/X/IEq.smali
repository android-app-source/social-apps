.class public final enum LX/IEq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IEq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IEq;

.field public static final enum FAILED:LX/IEq;

.field public static final enum INIT:LX/IEq;

.field public static final enum LOADED:LX/IEq;

.field public static final enum LOADING:LX/IEq;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2553409
    new-instance v0, LX/IEq;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, LX/IEq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IEq;->INIT:LX/IEq;

    new-instance v0, LX/IEq;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/IEq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IEq;->LOADING:LX/IEq;

    new-instance v0, LX/IEq;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, LX/IEq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IEq;->LOADED:LX/IEq;

    new-instance v0, LX/IEq;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/IEq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IEq;->FAILED:LX/IEq;

    .line 2553410
    const/4 v0, 0x4

    new-array v0, v0, [LX/IEq;

    sget-object v1, LX/IEq;->INIT:LX/IEq;

    aput-object v1, v0, v2

    sget-object v1, LX/IEq;->LOADING:LX/IEq;

    aput-object v1, v0, v3

    sget-object v1, LX/IEq;->LOADED:LX/IEq;

    aput-object v1, v0, v4

    sget-object v1, LX/IEq;->FAILED:LX/IEq;

    aput-object v1, v0, v5

    sput-object v0, LX/IEq;->$VALUES:[LX/IEq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2553408
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IEq;
    .locals 1

    .prologue
    .line 2553407
    const-class v0, LX/IEq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IEq;

    return-object v0
.end method

.method public static values()[LX/IEq;
    .locals 1

    .prologue
    .line 2553406
    sget-object v0, LX/IEq;->$VALUES:[LX/IEq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IEq;

    return-object v0
.end method
