.class public LX/JC8;
.super LX/B0V;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/J90;

.field private final c:LX/J9L;

.field private final d:LX/JCx;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/J90;LX/J9L;LX/JCx;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2661592
    invoke-direct {p0}, LX/B0V;-><init>()V

    .line 2661593
    iput-object p1, p0, LX/JC8;->a:Ljava/lang/String;

    .line 2661594
    iput-object p2, p0, LX/JC8;->b:LX/J90;

    .line 2661595
    iput-object p3, p0, LX/JC8;->c:LX/J9L;

    .line 2661596
    iput-object p4, p0, LX/JC8;->d:LX/JCx;

    .line 2661597
    return-void
.end method


# virtual methods
.method public final a(LX/B0d;)LX/0zO;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B0d;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2661598
    iget-object v0, p0, LX/JC8;->b:LX/J90;

    iget-object v1, p0, LX/JC8;->a:Ljava/lang/String;

    iget-object v2, p0, LX/JC8;->c:LX/J9L;

    invoke-virtual {v2}, LX/J9L;->h()I

    move-result v2

    .line 2661599
    const/4 v3, 0x4

    move v3, v3

    .line 2661600
    iget-object v4, p1, LX/B0d;->b:Ljava/lang/String;

    .line 2661601
    new-instance v5, LX/JBo;

    invoke-direct {v5}, LX/JBo;-><init>()V

    move-object v5, v5

    .line 2661602
    const-string v6, "app_section_id"

    invoke-virtual {v5, v6, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "collection_list_item_image_size"

    iget-object p1, v0, LX/J90;->c:LX/JAU;

    invoke-virtual {p1}, LX/JAU;->b()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "collection_table_item_image_size"

    iget-object p1, v0, LX/J90;->c:LX/JAU;

    invoke-virtual {p1}, LX/JAU;->a()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    const-string p0, "facepile_image_size"

    iget-object p1, v0, LX/J90;->c:LX/JAU;

    invoke-virtual {p1}, LX/JAU;->c()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "items_per_collection"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "suggestions_after"

    const/4 p1, 0x0

    invoke-virtual {v6, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "items_after"

    invoke-virtual {v6, p0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "suggestions_per_collection"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p0, "automatic_photo_captioning_enabled"

    iget-object p1, v0, LX/J90;->f:LX/0sX;

    invoke-virtual {p1}, LX/0sX;->a()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2661603
    move-object v0, v5

    .line 2661604
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    .locals 1

    .prologue
    .line 2661605
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2661606
    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    invoke-virtual {p0, p1, v0}, LX/JC8;->a(LX/B0d;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;)LX/B0N;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/B0d;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;)LX/B0N;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2661607
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2661608
    :cond_0
    new-instance v0, LX/B0e;

    invoke-direct {v0, p1}, LX/B0e;-><init>(LX/B0d;)V

    .line 2661609
    :goto_0
    return-object v0

    .line 2661610
    :cond_1
    new-instance v2, LX/9JH;

    invoke-direct {v2}, LX/9JH;-><init>()V

    .line 2661611
    iget-object v0, p1, LX/B0d;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2661612
    invoke-virtual {p2}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    invoke-static {p2}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/9JH;->a(ILX/0w5;)V

    .line 2661613
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2661614
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->d()LX/0Px;

    move-result-object v7

    invoke-static {v7}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v6

    .line 2661615
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    if-eq v6, v7, :cond_3

    .line 2661616
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v6

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v7

    invoke-static {v0}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v2, v6, v7, v0, v1}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2661617
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2661618
    :cond_4
    const/4 v4, 0x0

    .line 2661619
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    .line 2661620
    if-nez v0, :cond_5

    .line 2661621
    const-string v0, "CollectionInitialConnectionConfiguration"

    const-string v3, "Missing page info"

    invoke-static {v0, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v1

    .line 2661622
    :goto_2
    new-instance v0, LX/B0i;

    move-object v1, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2661623
    :cond_5
    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v4

    .line 2661624
    invoke-interface {v0}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_3
    move v5, v0

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_3
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2661625
    const-string v0, "CollectionsSections"

    return-object v0
.end method
