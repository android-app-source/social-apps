.class public final LX/Ho6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;)V
    .locals 0

    .prologue
    .line 2505180
    iput-object p1, p0, LX/Ho6;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2505181
    if-eqz p1, :cond_0

    .line 2505182
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505183
    if-nez v0, :cond_1

    .line 2505184
    :cond_0
    :goto_0
    return-void

    .line 2505185
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505186
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2505187
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505188
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2505189
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 2505190
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505191
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v4, p0, LX/Ho6;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    iget-object v4, v4, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->D:Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderView;

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderView;->a(Ljava/lang/String;)V

    .line 2505192
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505193
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2505194
    iget-object v0, p0, LX/Ho6;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    iget-object v3, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->H:Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;

    .line 2505195
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505196
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2505197
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505198
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2505199
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505200
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2505201
    iget-object v0, p0, LX/Ho6;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2505202
    iput-object v3, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->z:Ljava/util/ArrayList;

    .line 2505203
    move v3, v2

    .line 2505204
    :goto_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505205
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 2505206
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505207
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2505208
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505209
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2505210
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505211
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v7, v0, LX/1vs;->b:I

    .line 2505212
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505213
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v8, v0, LX/1vs;->a:LX/15i;

    iget v9, v0, LX/1vs;->b:I

    .line 2505214
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505215
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v10, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2505216
    iget-object v11, p0, LX/Ho6;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    iget-object v11, v11, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->z:Ljava/util/ArrayList;

    new-instance v12, LX/HoQ;

    const/4 v13, 0x3

    invoke-virtual {v4, v5, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v9, v1}, LX/15i;->h(II)Z

    move-result v6

    const/4 v7, 0x2

    invoke-virtual {v10, v0, v7}, LX/15i;->j(II)I

    move-result v0

    invoke-direct {v12, v4, v5, v6, v0}, LX/HoQ;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2505217
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_2

    :cond_5
    move v0, v2

    .line 2505218
    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_1

    .line 2505219
    :cond_7
    iget-object v0, p0, LX/Ho6;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    invoke-static {v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->n(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2505220
    iget-object v0, p0, LX/Ho6;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchElectionHubHeaderAndTabs"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2505221
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2505222
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Ho6;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
