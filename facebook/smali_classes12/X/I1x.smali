.class public final LX/I1x;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V
    .locals 0

    .prologue
    .line 2529560
    iput-object p1, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2529549
    iget-object v0, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->l:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2529550
    iget-object v0, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->l:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2529551
    :cond_0
    iget-object v0, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->a$redex0(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529552
    iget-object v0, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->j:LX/I1o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I1o;->b(Z)V

    .line 2529553
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2529554
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529555
    iget-object v0, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->l:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2529556
    iget-object v0, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->l:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2529557
    :cond_0
    iget-object v0, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    invoke-static {v0, p1}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->a$redex0(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529558
    iget-object v0, p0, LX/I1x;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->j:LX/I1o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I1o;->b(Z)V

    .line 2529559
    return-void
.end method
