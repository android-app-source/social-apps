.class public final LX/JMy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLEvent;

.field public final synthetic b:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;Lcom/facebook/graphql/model/GraphQLEvent;)V
    .locals 0

    .prologue
    .line 2684903
    iput-object p1, p0, LX/JMy;->b:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iput-object p2, p0, LX/JMy;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    const v0, 0x57c9fa24

    invoke-static {v6, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2684904
    iget-object v0, p0, LX/JMy;->b:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iget-object v0, v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->v:LX/1nQ;

    const-string v2, "native_newsfeed"

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->EVENT_CHAINING:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v2, v3, v8}, LX/1nQ;->b(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)V

    .line 2684905
    iget-object v0, p0, LX/JMy;->b:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iget-object v0, v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    sget-object v2, LX/21D;->NEWSFEED:LX/21D;

    const-string v3, "shareEvent"

    iget-object v4, p0, LX/JMy;->a:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x403827a

    invoke-static {v4, v5}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    invoke-static {v4}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v4

    invoke-virtual {v4}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v3, p0, LX/JMy;->b:Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    iget-object v3, v3, LX/AnF;->a:Landroid/content/Context;

    invoke-interface {v0, v8, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2684906
    const v0, 0x4d2e47e1    # 1.8274664E8f

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
