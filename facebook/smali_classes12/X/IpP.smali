.class public LX/IpP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Inb;


# instance fields
.field public final a:LX/0Uh;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/03V;

.field public final d:LX/6Oo;

.field public final e:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field public f:LX/Io3;

.field public g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

.field public h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;Ljava/util/concurrent/Executor;LX/03V;LX/6Oo;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612875
    iput-object p1, p0, LX/IpP;->a:LX/0Uh;

    .line 2612876
    iput-object p2, p0, LX/IpP;->b:Ljava/util/concurrent/Executor;

    .line 2612877
    iput-object p3, p0, LX/IpP;->c:LX/03V;

    .line 2612878
    iput-object p4, p0, LX/IpP;->d:LX/6Oo;

    .line 2612879
    iput-object p5, p0, LX/IpP;->e:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2612880
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2612881
    iget-object v0, p0, LX/IpP;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2612882
    iget-object v0, p0, LX/IpP;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612883
    iput-object v2, p0, LX/IpP;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612884
    :cond_0
    iget-object v0, p0, LX/IpP;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2612885
    iget-object v0, p0, LX/IpP;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612886
    iput-object v2, p0, LX/IpP;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612887
    :cond_1
    iget-object v0, p0, LX/IpP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2612888
    iget-object v0, p0, LX/IpP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612889
    iput-object v2, p0, LX/IpP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612890
    :cond_2
    return-void
.end method

.method public final a(LX/Io3;)V
    .locals 0

    .prologue
    .line 2612872
    iput-object p1, p0, LX/IpP;->f:LX/Io3;

    .line 2612873
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 2

    .prologue
    .line 2612856
    iput-object p2, p0, LX/IpP;->g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612857
    iget-object v0, p0, LX/IpP;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2612858
    :goto_0
    iget-object v0, p0, LX/IpP;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2612859
    :goto_1
    iget-object v0, p0, LX/IpP;->a:LX/0Uh;

    const/16 v1, 0x5a5

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2612860
    :cond_0
    :goto_2
    return-void

    .line 2612861
    :cond_1
    iget-object v0, p0, LX/IpP;->e:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    iget-object p1, p0, LX/IpP;->g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612862
    iget-object p2, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object p1, p2

    .line 2612863
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0rS;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/IpP;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612864
    iget-object v0, p0, LX/IpP;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/IpM;

    invoke-direct {v1, p0}, LX/IpM;-><init>(LX/IpP;)V

    iget-object p1, p0, LX/IpP;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2612865
    :cond_2
    iget-object v0, p0, LX/IpP;->d:LX/6Oo;

    iget-object v1, p0, LX/IpP;->g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612866
    iget-object p1, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v1, p1

    .line 2612867
    sget-object p1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v0, v1, p1}, LX/6Oo;->b(Lcom/facebook/user/model/UserKey;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/IpP;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612868
    iget-object v0, p0, LX/IpP;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/IpN;

    invoke-direct {v1, p0}, LX/IpN;-><init>(LX/IpP;)V

    iget-object p1, p0, LX/IpP;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2612869
    :cond_3
    iget-object v0, p0, LX/IpP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2612870
    iget-object v0, p0, LX/IpP;->e:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/IpP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612871
    iget-object v0, p0, LX/IpP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/IpO;

    invoke-direct {v1, p0}, LX/IpO;-><init>(LX/IpP;)V

    iget-object p1, p0, LX/IpP;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2
.end method
