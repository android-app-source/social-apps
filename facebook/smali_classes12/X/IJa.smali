.class public LX/IJa;
.super LX/IJZ;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2563936
    invoke-direct {p0}, LX/IJZ;-><init>()V

    .line 2563937
    iput-object p1, p0, LX/IJa;->a:Landroid/content/Context;

    .line 2563938
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/location/ImmutableLocation;Ljava/lang/String;)LX/0am;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/location/ImmutableLocation;",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2563939
    iget-object v1, p0, LX/IJa;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, LX/6Zi;->a(Landroid/content/Context;DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2563940
    iget-object v0, p0, LX/IJa;->a:Landroid/content/Context;

    const v1, 0x7f083866

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2563941
    const-string v0, "maps"

    return-object v0
.end method
