.class public LX/HJl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HJj;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HJm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2453288
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/HJl;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HJm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453289
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2453290
    iput-object p1, p0, LX/HJl;->b:LX/0Ot;

    .line 2453291
    return-void
.end method

.method public static a(LX/0QB;)LX/HJl;
    .locals 4

    .prologue
    .line 2453292
    const-class v1, LX/HJl;

    monitor-enter v1

    .line 2453293
    :try_start_0
    sget-object v0, LX/HJl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453294
    sput-object v2, LX/HJl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453295
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453296
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453297
    new-instance v3, LX/HJl;

    const/16 p0, 0x2bb8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJl;-><init>(LX/0Ot;)V

    .line 2453298
    move-object v0, v3

    .line 2453299
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453300
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453301
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Landroid/view/View$OnClickListener;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2453303
    const v0, -0x5c8c5a19

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2453304
    check-cast p2, LX/HJk;

    .line 2453305
    iget-object v0, p0, LX/HJl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/HJk;->a:Ljava/lang/String;

    iget-object v1, p2, LX/HJk;->b:Ljava/lang/String;

    iget-object v2, p2, LX/HJk;->c:Landroid/view/View$OnClickListener;

    const/4 p0, 0x2

    const/4 p2, 0x1

    .line 2453306
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a010d

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    .line 2453307
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0213e6

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b02aa

    invoke-virtual {v5, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0a0a08

    invoke-virtual {v5, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x4

    const p0, 0x7f0b02ac

    invoke-interface {v4, v5, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x5

    const p0, 0x7f0b02ad

    invoke-interface {v4, v5, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0060

    invoke-interface {v4, p2, v5}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b02ab

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    .line 2453308
    const v5, -0x5c8c5a19

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v2, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2453309
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    .line 2453310
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const p0, 0x7f0a0114

    invoke-interface {v5, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x0

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/16 p0, 0x8

    const p2, 0x7f0b0060

    invoke-interface {v5, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2453311
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2453312
    invoke-static {}, LX/1dS;->b()V

    .line 2453313
    iget v0, p1, LX/1dQ;->b:I

    .line 2453314
    packed-switch v0, :pswitch_data_0

    .line 2453315
    :goto_0
    return-object v3

    .line 2453316
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2453317
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Landroid/view/View$OnClickListener;

    .line 2453318
    iget-object p1, p0, LX/HJl;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2453319
    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2453320
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x5c8c5a19
        :pswitch_0
    .end packed-switch
.end method
