.class public LX/Hdb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HdZ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hdc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2488809
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hdb;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hdc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2488810
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2488811
    iput-object p1, p0, LX/Hdb;->b:LX/0Ot;

    .line 2488812
    return-void
.end method

.method public static a(LX/0QB;)LX/Hdb;
    .locals 4

    .prologue
    .line 2488813
    const-class v1, LX/Hdb;

    monitor-enter v1

    .line 2488814
    :try_start_0
    sget-object v0, LX/Hdb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2488815
    sput-object v2, LX/Hdb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2488816
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2488817
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2488818
    new-instance v3, LX/Hdb;

    const/16 p0, 0x371c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hdb;-><init>(LX/0Ot;)V

    .line 2488819
    move-object v0, v3

    .line 2488820
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2488821
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hdb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2488822
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2488823
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2488824
    check-cast p2, LX/Hda;

    .line 2488825
    iget-object v0, p0, LX/Hdb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hdc;

    iget-object v1, p2, LX/Hda;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    iget-object v2, p2, LX/Hda;->b:LX/1Pm;

    .line 2488826
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0a0097

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    iget-object p0, v0, LX/Hdc;->c:LX/Hdh;

    invoke-virtual {v1}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;->a()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/Hdh;->a(Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const p0, 0x7f0b0050

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const p0, 0x7f0a00a4

    invoke-virtual {v4, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 p0, 0x6

    const p2, 0x7f0b22f6

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x3

    const p2, 0x7f0b22f8

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/Hdc;->a:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-static {v0, p1, v2, v1}, LX/Hdc;->a(LX/Hdc;LX/1De;LX/1Pm;Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;)LX/3mX;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const p0, 0x7f0b22f9

    invoke-interface {v3, v4, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2488827
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2488828
    invoke-static {}, LX/1dS;->b()V

    .line 2488829
    const/4 v0, 0x0

    return-object v0
.end method
