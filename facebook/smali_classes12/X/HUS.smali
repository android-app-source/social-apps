.class public final LX/HUS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2472898
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 2472899
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2472900
    :goto_0
    return v1

    .line 2472901
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2472902
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 2472903
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2472904
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2472905
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 2472906
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2472907
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2472908
    :cond_2
    const-string v7, "opinion_content"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2472909
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2472910
    :cond_3
    const-string v7, "opinion_topic"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2472911
    const/4 v6, 0x0

    .line 2472912
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_c

    .line 2472913
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2472914
    :goto_2
    move v3, v6

    .line 2472915
    goto :goto_1

    .line 2472916
    :cond_4
    const-string v7, "opinion_video_obj"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2472917
    invoke-static {p0, p1}, LX/5i4;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2472918
    :cond_5
    const-string v7, "url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2472919
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2472920
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2472921
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2472922
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2472923
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2472924
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2472925
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2472926
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 2472927
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2472928
    :cond_9
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_b

    .line 2472929
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2472930
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2472931
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 2472932
    const-string v8, "edges"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2472933
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2472934
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_a

    .line 2472935
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_a

    .line 2472936
    const/4 v8, 0x0

    .line 2472937
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_10

    .line 2472938
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2472939
    :goto_5
    move v7, v8

    .line 2472940
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2472941
    :cond_a
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2472942
    goto :goto_3

    .line 2472943
    :cond_b
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2472944
    invoke-virtual {p1, v6, v3}, LX/186;->b(II)V

    .line 2472945
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_c
    move v3, v6

    goto :goto_3

    .line 2472946
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2472947
    :cond_e
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 2472948
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2472949
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2472950
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_e

    if-eqz v9, :cond_e

    .line 2472951
    const-string v10, "node"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 2472952
    invoke-static {p0, p1}, LX/HUR;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_6

    .line 2472953
    :cond_f
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2472954
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 2472955
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_5

    :cond_10
    move v7, v8

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2472956
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2472957
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2472958
    if-eqz v0, :cond_0

    .line 2472959
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472960
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2472961
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2472962
    if-eqz v0, :cond_1

    .line 2472963
    const-string v1, "opinion_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472964
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2472965
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2472966
    if-eqz v0, :cond_5

    .line 2472967
    const-string v1, "opinion_topic"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472968
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2472969
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2472970
    if-eqz v1, :cond_4

    .line 2472971
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472972
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2472973
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 2472974
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 2472975
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2472976
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2472977
    if-eqz v4, :cond_2

    .line 2472978
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472979
    invoke-static {p0, v4, p2}, LX/HUR;->a(LX/15i;ILX/0nX;)V

    .line 2472980
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2472981
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2472982
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2472983
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2472984
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2472985
    if-eqz v0, :cond_6

    .line 2472986
    const-string v1, "opinion_video_obj"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472987
    invoke-static {p0, v0, p2, p3}, LX/5i4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2472988
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2472989
    if-eqz v0, :cond_7

    .line 2472990
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472991
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2472992
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2472993
    return-void
.end method
