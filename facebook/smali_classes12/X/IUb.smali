.class public LX/IUb;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3my;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/graphics/Paint;

.field public d:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

.field public e:Landroid/widget/LinearLayout;

.field public f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2580727
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2580728
    const/4 p1, -0x1

    .line 2580729
    const-class v0, LX/IUb;

    invoke-static {v0, p0}, LX/IUb;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2580730
    const v0, 0x7f03086c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2580731
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/IUb;->setWillNotDraw(Z)V

    .line 2580732
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/IUb;->c:Landroid/graphics/Paint;

    .line 2580733
    iget-object v0, p0, LX/IUb;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/IUb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2580734
    iget-object v0, p0, LX/IUb;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2580735
    const v0, 0x7f0d15ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    iput-object v0, p0, LX/IUb;->d:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    .line 2580736
    const v0, 0x7f0d15ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/IUb;->e:Landroid/widget/LinearLayout;

    .line 2580737
    const v0, 0x7f0d15eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/IUb;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2580738
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2580739
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 2580740
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/IUb;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2580741
    :goto_0
    return-void

    .line 2580742
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/IUb;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IUb;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object p0

    check-cast p0, LX/3my;

    iput-object v1, p1, LX/IUb;->a:LX/0ad;

    iput-object p0, p1, LX/IUb;->b:LX/3my;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZ)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 2580743
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->C()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    .line 2580744
    :cond_0
    :goto_0
    return-void

    .line 2580745
    :cond_1
    if-eqz p3, :cond_7

    .line 2580746
    iget-object v0, p0, LX/IUb;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2580747
    iget-object v0, p0, LX/IUb;->d:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-virtual {v0, v3}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->setVisibility(I)V

    .line 2580748
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->q()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->q()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    if-le v0, v7, :cond_2

    .line 2580749
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->q()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2580750
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2580751
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->q()LX/2uF;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/2uF;->a(I)LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2580752
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2580753
    iget-object v6, p0, LX/IUb;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2580754
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;

    invoke-virtual {v5, v4, v3, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2580755
    iget-object v0, p0, LX/IUb;->d:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-virtual {v5, v4, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Ljava/lang/String;ZZ)V

    .line 2580756
    :cond_2
    :goto_2
    if-eqz p2, :cond_d

    .line 2580757
    iget-object v0, p0, LX/IUb;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2580758
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->C()LX/0Px;

    move-result-object v5

    .line 2580759
    :goto_3
    iget-object v0, p0, LX/IUb;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_9

    .line 2580760
    iget-object v0, p0, LX/IUb;->e:Landroid/widget/LinearLayout;

    new-instance v1, LX/DRU;

    invoke-virtual {p0}, LX/IUb;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, LX/DRU;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_3

    .line 2580761
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2580762
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 2580763
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2580764
    goto :goto_1

    .line 2580765
    :cond_4
    const-class v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;

    invoke-virtual {v5, v4, v3, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2580766
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;

    .line 2580767
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_6

    move-object v1, v2

    .line 2580768
    :goto_5
    iget-object v6, p0, LX/IUb;->d:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-virtual {v5, v4, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;->k()I

    move-result v5

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;->j()I

    move-result v0

    invoke-virtual {v6, v4, v1, v5, v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_2

    .line 2580769
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2580770
    goto :goto_4

    .line 2580771
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerContextModel$ViewerJoinCommunityContextModel$RangesModel;->a()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v6, v1, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    .line 2580772
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2580773
    iget-object v0, p0, LX/IUb;->d:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-virtual {v0, v3}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->setVisibility(I)V

    .line 2580774
    iget-object v0, p0, LX/IUb;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2580775
    iget-object v0, p0, LX/IUb;->d:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7, v7}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_2

    .line 2580776
    :cond_8
    iget-object v0, p0, LX/IUb;->d:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-virtual {v0, v8}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->setVisibility(I)V

    .line 2580777
    iget-object v0, p0, LX/IUb;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2580778
    :cond_9
    :goto_6
    iget-object v0, p0, LX/IUb;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-le v0, v1, :cond_b

    .line 2580779
    iget-object v0, p0, LX/IUb;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_6

    :cond_a
    move-object v0, v2

    .line 2580780
    :goto_7
    invoke-virtual {v1, v4, v0, p2}, LX/DRU;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2580781
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    :cond_b
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 2580782
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupTopicTagsModel;

    .line 2580783
    iget-object v1, p0, LX/IUb;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/DRU;

    .line 2580784
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupTopicTagsModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 2580785
    :goto_8
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupTopicTagsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_c
    move-object v4, v2

    .line 2580786
    goto :goto_8

    .line 2580787
    :cond_d
    iget-object v0, p0, LX/IUb;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2580788
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2580789
    invoke-virtual {p0}, LX/IUb;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2580790
    const/4 v1, 0x0

    int-to-float v2, v0

    invoke-virtual {p0}, LX/IUb;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/IUb;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2580791
    return-void
.end method
