.class public final LX/J80;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J7a;


# instance fields
.field public final synthetic a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V
    .locals 0

    .prologue
    .line 2651353
    iput-object p1, p0, LX/J80;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2651354
    iget-object v0, p0, LX/J80;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    const/4 p0, 0x0

    .line 2651355
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2651356
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2651357
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2651358
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/J80;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2651359
    const v1, 0x7f08391f

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f083920

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    const v2, 0x7f083921

    new-instance v3, LX/J7z;

    invoke-direct {v3, p0}, LX/J7z;-><init>(LX/J80;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2651360
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2651361
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2651362
    iget-object v0, p0, LX/J80;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    .line 2651363
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->j:Landroid/widget/TextView;

    .line 2651364
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    invoke-virtual {v3}, LX/J7b;->e()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    invoke-virtual {v3}, LX/J7b;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2651365
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2651366
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    if-nez v1, :cond_1

    .line 2651367
    new-instance v1, LX/J82;

    iget-object v2, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    invoke-virtual {v2}, LX/J7b;->c()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, LX/J82;-><init>(LX/0Px;)V

    iput-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    .line 2651368
    :goto_0
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->r:LX/1Qq;

    if-nez v1, :cond_0

    .line 2651369
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->g:LX/J8C;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2651370
    sget-object v3, LX/J8D;->a:LX/J8D;

    move-object v3, v3

    .line 2651371
    new-instance p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment$9;

    invoke-direct {p0, v0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment$9;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-virtual {v1, v2, v3, p0}, LX/J8C;->a(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;)LX/J8B;

    move-result-object v1

    move-object v1, v1

    .line 2651372
    iget-object v2, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->d:LX/1DS;

    iget-object v3, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->e:LX/0Ot;

    iget-object p0, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    invoke-virtual {v2, v3, p0}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v2

    .line 2651373
    iput-object v1, v2, LX/1Ql;->f:LX/1PW;

    .line 2651374
    move-object v2, v2

    .line 2651375
    invoke-virtual {v2}, LX/1Ql;->e()LX/1Qq;

    move-result-object v2

    move-object v1, v2

    .line 2651376
    iput-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->r:LX/1Qq;

    .line 2651377
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->o:LX/0g8;

    iget-object v2, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->r:LX/1Qq;

    invoke-interface {v1, v2}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2651378
    :goto_1
    invoke-static {v0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->p(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    .line 2651379
    return-void

    .line 2651380
    :cond_0
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->r:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    goto :goto_1

    .line 2651381
    :cond_1
    iget-object v1, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    iget-object v2, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    invoke-virtual {v2}, LX/J7b;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/J82;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 2651382
    iget-object v0, p0, LX/J80;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    .line 2651383
    invoke-static {v0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->p(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    .line 2651384
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;

    .line 2651385
    iget-object v2, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->f:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    sget-object v3, LX/ICQ;->FINISH:LX/ICQ;

    .line 2651386
    iget-object p0, v1, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->p:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    move-object v1, p0

    .line 2651387
    invoke-virtual {v2, v3, v1}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a(LX/ICQ;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V

    .line 2651388
    new-instance v1, LX/J84;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/J84;-><init>(Landroid/content/Context;)V

    .line 2651389
    new-instance v2, LX/J7q;

    invoke-direct {v2, v0}, LX/J7q;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    .line 2651390
    iput-object v2, v1, LX/J84;->a:LX/J7n;

    .line 2651391
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2651392
    invoke-virtual {v1, v2}, LX/0ht;->a(Landroid/view/View;)V

    .line 2651393
    return-void
.end method
