.class public final LX/Hhs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/pm/ResolveInfo;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Hht;


# direct methods
.method public constructor <init>(LX/Hht;Landroid/content/pm/ResolveInfo;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2496341
    iput-object p1, p0, LX/Hhs;->d:LX/Hht;

    iput-object p2, p0, LX/Hhs;->a:Landroid/content/pm/ResolveInfo;

    iput-object p3, p0, LX/Hhs;->b:Landroid/content/Context;

    iput-object p4, p0, LX/Hhs;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 2496342
    iget-object v0, p0, LX/Hhs;->d:LX/Hht;

    invoke-virtual {v0}, LX/Hht;->a()Ljava/lang/String;

    move-result-object v0

    .line 2496343
    iget-object v1, p0, LX/Hhs;->a:Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 2496344
    new-instance v2, Landroid/content/Intent;

    sget-object v3, LX/Hht;->a:Landroid/content/Intent;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 2496345
    const-string v3, "android.intent.extra.TEXT"

    iget-object v4, p0, LX/Hhs;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f083761

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, LX/Hhs;->c:Ljava/lang/String;

    aput-object v8, v6, v7

    aput-object v0, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2496346
    iget-object v3, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v4, v1, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2496347
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2496348
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    .line 2496349
    const-string v4, "link"

    invoke-virtual {v3, v4, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2496350
    iget-object v0, p0, LX/Hhs;->d:LX/Hht;

    iget-object v0, v0, LX/Hht;->g:LX/0if;

    sget-object v4, LX/0ig;->Y:LX/0ih;

    const-string v5, "share_action_click"

    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5, v1, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2496351
    iget-object v0, p0, LX/Hhs;->d:LX/Hht;

    iget-object v0, v0, LX/Hht;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Hhs;->b:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2496352
    return v9
.end method
