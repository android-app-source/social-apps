.class public final LX/J6S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 0

    .prologue
    .line 2649654
    iput-object p1, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 2649655
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_0

    .line 2649656
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v1, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649657
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649658
    const-string v2, "checkup_cancel"

    iget-object v3, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v5, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v5, v5, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v6, v6, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649659
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->finish()V

    .line 2649660
    :goto_0
    return-void

    .line 2649661
    :cond_0
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v1, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2649662
    iget-object v3, v0, LX/J3t;->l:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/J4L;

    .line 2649663
    iget-object v5, v0, LX/J3t;->k:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 2649664
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2649665
    iget-object v8, v3, LX/J4L;->b:Ljava/util/HashMap;

    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/J4J;

    .line 2649666
    iget-object v6, v6, LX/J4J;->f:LX/8SR;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1oT;

    invoke-virtual {v6, v5}, LX/8SR;->b(LX/1oT;)V

    goto :goto_1

    .line 2649667
    :cond_1
    invoke-virtual {v0}, LX/J3t;->a()V

    .line 2649668
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewPager;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2649669
    const v1, 0x7f0d26aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 2649670
    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    .line 2649671
    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 2649672
    const v1, -0x2ee83321

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2649673
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649674
    iput v2, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    .line 2649675
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649676
    iput v2, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    .line 2649677
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v5, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649678
    iget-object v1, v0, LX/J3t;->i:Ljava/lang/String;

    move-object v6, v1

    .line 2649679
    const-string v7, "step_navigation"

    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object v12, v4

    invoke-virtual/range {v5 .. v12}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649680
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2649681
    iget-object v0, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    iget-object v1, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v2, p0, LX/J6S;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
