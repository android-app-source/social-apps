.class public final LX/J12;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2638166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2638167
    new-instance v0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;

    invoke-direct {v0, p1}, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2638168
    new-array v0, p1, [Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;

    return-object v0
.end method
