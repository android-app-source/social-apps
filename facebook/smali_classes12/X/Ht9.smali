.class public final LX/Ht9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HtB;


# direct methods
.method public constructor <init>(LX/HtB;)V
    .locals 0

    .prologue
    .line 2515423
    iput-object p1, p0, LX/Ht9;->a:LX/HtB;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2515420
    iget-object v0, p0, LX/Ht9;->a:LX/HtB;

    iget-object v1, p0, LX/Ht9;->a:LX/HtB;

    invoke-static {v1, p1}, LX/HtB;->a$redex0(LX/HtB;Ljava/lang/Throwable;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2515421
    invoke-static {v0, v1}, LX/HtB;->b$redex0(LX/HtB;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2515422
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2515406
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2515407
    if-nez p1, :cond_0

    .line 2515408
    iget-object v0, p0, LX/Ht9;->a:LX/HtB;

    iget-object v1, p0, LX/Ht9;->a:LX/HtB;

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "fetch result is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, LX/HtB;->a$redex0(LX/HtB;Ljava/lang/Throwable;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2515409
    invoke-static {v0, v1}, LX/HtB;->b$redex0(LX/HtB;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2515410
    :goto_0
    return-void

    .line 2515411
    :cond_0
    iget-object v0, p0, LX/Ht9;->a:LX/HtB;

    .line 2515412
    iget-object v1, v0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    move-object v2, v1

    .line 2515413
    check-cast v2, LX/0im;

    invoke-interface {v2}, LX/0im;->c()LX/0jJ;

    move-result-object v2

    sget-object v3, LX/HtB;->a:LX/0jK;

    invoke-virtual {v2, v3}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v2

    check-cast v2, LX/0jL;

    new-instance v3, LX/89G;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-direct {v3, v1}, LX/89G;-><init>(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)V

    .line 2515414
    iput-object p1, v3, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2515415
    move-object v1, v3

    .line 2515416
    invoke-virtual {v1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2515417
    iget-object v0, p0, LX/Ht9;->a:LX/HtB;

    .line 2515418
    invoke-static {v0, p1}, LX/HtB;->b$redex0(LX/HtB;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2515419
    goto :goto_0
.end method
