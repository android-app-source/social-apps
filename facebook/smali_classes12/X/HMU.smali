.class public final LX/HMU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:LX/HMV;


# direct methods
.method public constructor <init>(LX/HMV;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2457180
    iput-object p1, p0, LX/HMU;->b:LX/HMV;

    iput-object p2, p0, LX/HMU;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 2457181
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2457182
    iget-object v0, p0, LX/HMU;->a:Landroid/content/Intent;

    const-string v1, "extra_actor_viewer_context"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2457183
    iget-object v0, p0, LX/HMU;->b:LX/HMV;

    iget-object v0, v0, LX/HMV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, p0, LX/HMU;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
