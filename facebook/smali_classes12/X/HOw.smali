.class public LX/HOw;
.super LX/E93;
.source ""


# instance fields
.field private g:Landroid/view/View;

.field private h:I

.field private i:I

.field private j:J

.field private k:LX/16I;

.field private l:Ljava/lang/Boolean;

.field private m:LX/8YB;

.field private n:LX/CfF;

.field private o:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Boolean;LX/0o8;LX/8YB;JLX/0SG;LX/E2N;LX/3Tp;LX/E1j;LX/0bH;LX/1Db;LX/AjP;LX/1My;LX/1Db;LX/E1l;LX/CfF;LX/CvY;LX/Cfw;LX/3mH;LX/967;LX/16I;LX/0ad;)V
    .locals 25
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1SX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0o8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/8YB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2461145
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move-object/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v21, p21

    move-object/from16 v22, p22

    move-object/from16 v23, p23

    invoke-direct/range {v4 .. v23}, LX/E93;-><init>(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;LX/0SG;LX/E2N;LX/3Tp;LX/E1j;LX/0bH;LX/1Db;LX/AjP;LX/1My;LX/1Db;LX/E1l;Lcom/facebook/reaction/ReactionUtil;LX/CvY;LX/Cfw;LX/3mH;LX/967;)V

    .line 2461146
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, LX/HOw;->h:I

    .line 2461147
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, LX/HOw;->i:I

    .line 2461148
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, LX/HOw;->j:J

    .line 2461149
    move-wide/from16 v0, p7

    move-object/from16 v2, p0

    iput-wide v0, v2, LX/HOw;->j:J

    .line 2461150
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HOw;->m:LX/8YB;

    .line 2461151
    move-object/from16 v0, p24

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HOw;->k:LX/16I;

    .line 2461152
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HOw;->l:Ljava/lang/Boolean;

    .line 2461153
    move-object/from16 v0, p19

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HOw;->n:LX/CfF;

    .line 2461154
    move-object/from16 v0, p25

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HOw;->o:LX/0ad;

    .line 2461155
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 6

    .prologue
    .line 2461133
    sget v0, LX/HPY;->a:I

    if-ne p2, v0, :cond_0

    .line 2461134
    new-instance v0, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/HOw;->g:Landroid/view/View;

    .line 2461135
    iget v0, p0, LX/HOw;->h:I

    invoke-virtual {p0, v0}, LX/HOw;->f(I)V

    .line 2461136
    new-instance v0, LX/E97;

    iget-object v1, p0, LX/HOw;->g:Landroid/view/View;

    invoke-direct {v0, v1}, LX/E97;-><init>(Landroid/view/View;)V

    .line 2461137
    :goto_0
    return-object v0

    .line 2461138
    :cond_0
    sget v0, LX/HPY;->b:I

    if-ne p2, v0, :cond_2

    .line 2461139
    new-instance v1, LX/Gls;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Gls;-><init>(Landroid/content/Context;)V

    .line 2461140
    iget-wide v2, p0, LX/HOw;->j:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 2461141
    iget-wide v2, p0, LX/HOw;->j:J

    .line 2461142
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/Gls;->d:Ljava/lang/String;

    .line 2461143
    :cond_1
    new-instance v0, LX/E97;

    invoke-direct {v0, v1}, LX/E97;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2461144
    :cond_2
    invoke-super {p0, p1, p2}, LX/E93;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)LX/E8s;
    .locals 2

    .prologue
    .line 2461199
    invoke-super {p0, p1}, LX/E93;->b(Landroid/content/Context;)LX/E8s;

    move-result-object v0

    .line 2461200
    const/4 v1, 0x1

    .line 2461201
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2461202
    return-object v0
.end method

.method public final f(I)V
    .locals 4

    .prologue
    .line 2461195
    iput p1, p0, LX/HOw;->h:I

    .line 2461196
    iget-object v0, p0, LX/HOw;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2461197
    iget-object v0, p0, LX/HOw;->g:Landroid/view/View;

    new-instance v1, LX/1a3;

    const/4 v2, -0x1

    iget v3, p0, LX/HOw;->h:I

    invoke-direct {v1, v2, v3}, LX/1a3;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2461198
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2461203
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2461204
    sget v0, LX/HPY;->a:I

    .line 2461205
    :goto_0
    return v0

    .line 2461206
    :cond_0
    iget-object v0, p0, LX/HOw;->m:LX/8YB;

    sget-object v1, LX/8YB;->GROUPS:LX/8YB;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 2461207
    sget v0, LX/HPY;->b:I

    goto :goto_0

    .line 2461208
    :cond_1
    invoke-super {p0, p1}, LX/E93;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public final h()V
    .locals 7

    .prologue
    const-wide/16 v4, 0x5

    .line 2461157
    iget-object v0, p0, LX/HOw;->o:LX/0ad;

    sget-short v1, LX/8Dn;->j:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2461158
    invoke-super {p0}, LX/E93;->h()V

    .line 2461159
    :cond_0
    :goto_0
    return-void

    .line 2461160
    :cond_1
    iget-object v0, p0, LX/E8m;->f:LX/2jY;

    if-eqz v0, :cond_0

    .line 2461161
    iget v0, p0, LX/HOw;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/HOw;->i:I

    .line 2461162
    iget-object v0, p0, LX/E8m;->f:LX/2jY;

    .line 2461163
    iget-boolean v1, v0, LX/2jY;->o:Z

    move v1, v1

    .line 2461164
    iget-object v0, p0, LX/HOw;->k:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/HOw;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2461165
    sget-object v0, LX/0zS;->b:LX/0zS;

    .line 2461166
    iget-object v2, p0, LX/E8m;->f:LX/2jY;

    .line 2461167
    iget-object v3, v2, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v2, v3

    .line 2461168
    if-eqz v2, :cond_2

    .line 2461169
    iget-object v2, p0, LX/E8m;->f:LX/2jY;

    .line 2461170
    iget-object v3, v2, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v2, v3

    .line 2461171
    const-wide/16 v4, 0xf

    .line 2461172
    iput-wide v4, v2, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2461173
    :cond_2
    :goto_1
    iget-object v2, p0, LX/HOw;->n:LX/CfF;

    iget-object v3, p0, LX/E8m;->f:LX/2jY;

    invoke-virtual {v2, v3, v0}, LX/CfF;->a(LX/2jY;LX/0zS;)Z

    move-result v0

    .line 2461174
    iget-object v2, p0, LX/HOw;->k:LX/16I;

    invoke-virtual {v2}, LX/16I;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez v0, :cond_0

    iget-object v0, p0, LX/E8m;->f:LX/2jY;

    .line 2461175
    iget-boolean v2, v0, LX/2jY;->o:Z

    move v0, v2

    .line 2461176
    if-eq v1, v0, :cond_0

    .line 2461177
    :cond_3
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0

    .line 2461178
    :cond_4
    iget v0, p0, LX/HOw;->i:I

    const/16 v2, 0xa

    if-ge v0, v2, :cond_5

    iget-object v0, p0, LX/HOw;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2461179
    sget-object v0, LX/0zS;->d:LX/0zS;

    .line 2461180
    iget-object v2, p0, LX/E8m;->f:LX/2jY;

    .line 2461181
    iget-object v3, v2, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v2, v3

    .line 2461182
    if-eqz v2, :cond_2

    .line 2461183
    iget-object v2, p0, LX/E8m;->f:LX/2jY;

    .line 2461184
    iget-object v3, v2, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v2, v3

    .line 2461185
    iput-wide v4, v2, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2461186
    goto :goto_1

    .line 2461187
    :cond_5
    sget-object v0, LX/0zS;->c:LX/0zS;

    .line 2461188
    iget-object v2, p0, LX/E8m;->f:LX/2jY;

    .line 2461189
    iget-object v3, v2, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v2, v3

    .line 2461190
    if-eqz v2, :cond_2

    .line 2461191
    iget-object v2, p0, LX/E8m;->f:LX/2jY;

    .line 2461192
    iget-object v3, v2, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v2, v3

    .line 2461193
    iput-wide v4, v2, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2461194
    goto :goto_1
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2461156
    invoke-super {p0}, LX/E93;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
