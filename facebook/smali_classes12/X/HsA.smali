.class public final LX/HsA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/1bf;",
        "LX/HsC;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0P1;

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:LX/0il;

.field public final synthetic d:Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;LX/0P1;Landroid/net/Uri;LX/0il;)V
    .locals 0

    .prologue
    .line 2514054
    iput-object p1, p0, LX/HsA;->d:Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;

    iput-object p2, p0, LX/HsA;->a:LX/0P1;

    iput-object p3, p0, LX/HsA;->b:Landroid/net/Uri;

    iput-object p4, p0, LX/HsA;->c:LX/0il;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2514055
    check-cast p1, LX/1bf;

    .line 2514056
    new-instance v2, LX/HsC;

    iget-object v0, p0, LX/HsA;->d:Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;

    iget-object v0, v0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->e:LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    .line 2514057
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v4, v0

    .line 2514058
    iget-object v0, p0, LX/HsA;->a:LX/0P1;

    iget-object v1, p0, LX/HsA;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HsA;->c:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    iget-object v0, p0, LX/HsA;->a:LX/0P1;

    iget-object v5, p0, LX/HsA;->b:Landroid/net/Uri;

    invoke-virtual {v0, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, LX/HsA;->a:LX/0P1;

    iget-object v5, p0, LX/HsA;->b:Landroid/net/Uri;

    invoke-virtual {v0, v5}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HsA;->a:LX/0P1;

    iget-object v5, p0, LX/HsA;->b:Landroid/net/Uri;

    invoke-virtual {v0, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    invoke-direct {v2, v3, v4, v1, v0}, LX/HsC;-><init>(LX/1aZ;Landroid/net/Uri;Lcom/facebook/composer/attachments/ComposerAttachment;I)V

    return-object v2

    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
