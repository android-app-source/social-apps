.class public LX/IkE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:LX/IkB;

.field private final d:Landroid/content/Context;

.field private final e:LX/InS;

.field private final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2606510
    const-class v0, LX/IkE;

    sput-object v0, LX/IkE;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/IkB;Landroid/content/Context;LX/InS;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606512
    iput-object p1, p0, LX/IkE;->b:Landroid/content/res/Resources;

    .line 2606513
    iput-object p2, p0, LX/IkE;->c:LX/IkB;

    .line 2606514
    iput-object p3, p0, LX/IkE;->d:Landroid/content/Context;

    .line 2606515
    iput-object p4, p0, LX/IkE;->e:LX/InS;

    .line 2606516
    iput-object p5, p0, LX/IkE;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2606517
    return-void
.end method

.method public static a(LX/0QB;)LX/IkE;
    .locals 9

    .prologue
    .line 2606518
    const-class v1, LX/IkE;

    monitor-enter v1

    .line 2606519
    :try_start_0
    sget-object v0, LX/IkE;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606520
    sput-object v2, LX/IkE;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606521
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606522
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606523
    new-instance v3, LX/IkE;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/IkB;->a(LX/0QB;)LX/IkB;

    move-result-object v5

    check-cast v5, LX/IkB;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/InS;->b(LX/0QB;)LX/InS;

    move-result-object v7

    check-cast v7, LX/InS;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v3 .. v8}, LX/IkE;-><init>(Landroid/content/res/Resources;LX/IkB;Landroid/content/Context;LX/InS;Lcom/facebook/content/SecureContextHelper;)V

    .line 2606524
    move-object v0, v3

    .line 2606525
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606526
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606527
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606528
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
