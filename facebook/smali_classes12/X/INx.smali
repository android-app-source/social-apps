.class public LX/INx;
.super LX/1a1;
.source ""


# instance fields
.field public final l:Lcom/facebook/content/SecureContextHelper;

.field public final m:Landroid/content/Context;

.field public final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

.field public p:Ljava/lang/String;

.field public q:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0Or;Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2571308
    invoke-direct {p0, p4}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2571309
    iput-object p1, p0, LX/INx;->l:Lcom/facebook/content/SecureContextHelper;

    .line 2571310
    iput-object p2, p0, LX/INx;->m:Landroid/content/Context;

    .line 2571311
    iput-object p3, p0, LX/INx;->n:LX/0Or;

    .line 2571312
    iput-object p4, p0, LX/INx;->o:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    .line 2571313
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V
    .locals 9

    .prologue
    .line 2571314
    iput-object p1, p0, LX/INx;->p:Ljava/lang/String;

    .line 2571315
    iget-object v1, p0, LX/INx;->o:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    int-to-double v6, p5

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v8, p6

    invoke-virtual/range {v1 .. v8}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;DZ)V

    .line 2571316
    iget-object v0, p0, LX/INx;->q:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2571317
    new-instance v0, LX/INw;

    invoke-direct {v0, p0}, LX/INw;-><init>(LX/INx;)V

    iput-object v0, p0, LX/INx;->q:Landroid/view/View$OnClickListener;

    .line 2571318
    iget-object v0, p0, LX/INx;->o:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    iget-object v1, p0, LX/INx;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2571319
    :cond_0
    return-void
.end method
