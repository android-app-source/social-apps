.class public LX/JHb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5KN;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Kd;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Kj;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hpj;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BsQ;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Iv;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GeK;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GeU;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GeZ;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ged;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/5KS;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/5Kd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5Kj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Hpj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BsQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9Iv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/GeK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/GeU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/GeZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ged;",
            ">;",
            "LX/5KS;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2673186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2673187
    iput-object p1, p0, LX/JHb;->a:LX/0Ot;

    .line 2673188
    iput-object p2, p0, LX/JHb;->b:LX/0Ot;

    .line 2673189
    iput-object p3, p0, LX/JHb;->c:LX/0Ot;

    .line 2673190
    iput-object p4, p0, LX/JHb;->d:LX/0Ot;

    .line 2673191
    iput-object p5, p0, LX/JHb;->e:LX/0Ot;

    .line 2673192
    iput-object p6, p0, LX/JHb;->f:LX/0Ot;

    .line 2673193
    iput-object p7, p0, LX/JHb;->g:LX/0Ot;

    .line 2673194
    iput-object p8, p0, LX/JHb;->h:LX/0Ot;

    .line 2673195
    iput-object p9, p0, LX/JHb;->i:LX/0Ot;

    .line 2673196
    return-void
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/String;Lcom/facebook/java2js/JSValue;LX/5KI;)LX/1X5;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2673197
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2673198
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 2673199
    :sswitch_0
    const-string v1, "CSButton"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "CSFBNetworkImage"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "CSFlexbox"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "CSImage"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "CSInset"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "CSRatioLayout"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "CSText"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v1, "CSTouchableWithoutFeedback"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v1, "CSCollection"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v1, "CSHeaderActor"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v1, "CSFBFeedNetworkImage"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v1, "CSFBFeedActorProfilePicture"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v1, "CSFBFeedContentText"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "CSFBFeedExplanationContent"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v1, "CSFBFeedExplanation"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v1, "CSFBFeedHeader"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v1, "CSFBFeedLikePageActionButton"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v1, "CSFBFeedSecondaryAction"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v1, "CSFIGCardContentLandscapeAttachment"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    .line 2673200
    :pswitch_0
    const/4 v0, 0x0

    .line 2673201
    new-instance v1, LX/5KU;

    invoke-direct {v1}, LX/5KU;-><init>()V

    .line 2673202
    sget-object p0, LX/5KV;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/5KT;

    .line 2673203
    if-nez p0, :cond_1

    .line 2673204
    new-instance p0, LX/5KT;

    invoke-direct {p0}, LX/5KT;-><init>()V

    .line 2673205
    :cond_1
    invoke-static {p0, p1, v0, v0, v1}, LX/5KT;->a$redex0(LX/5KT;LX/1De;IILX/5KU;)V

    .line 2673206
    move-object v1, p0

    .line 2673207
    move-object v0, v1

    .line 2673208
    move-object v0, v0

    .line 2673209
    iget-object v1, v0, LX/5KT;->a:LX/5KU;

    iput-object p3, v1, LX/5KU;->a:Lcom/facebook/java2js/JSValue;

    .line 2673210
    iget-object v1, v0, LX/5KT;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673211
    move-object v0, v0

    .line 2673212
    goto/16 :goto_1

    .line 2673213
    :pswitch_1
    iget-object v0, p0, LX/JHb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Kd;

    const/4 v1, 0x0

    .line 2673214
    new-instance p0, LX/5Kc;

    invoke-direct {p0, v0}, LX/5Kc;-><init>(LX/5Kd;)V

    .line 2673215
    sget-object p2, LX/5Kd;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/5Kb;

    .line 2673216
    if-nez p2, :cond_2

    .line 2673217
    new-instance p2, LX/5Kb;

    invoke-direct {p2}, LX/5Kb;-><init>()V

    .line 2673218
    :cond_2
    invoke-static {p2, p1, v1, v1, p0}, LX/5Kb;->a$redex0(LX/5Kb;LX/1De;IILX/5Kc;)V

    .line 2673219
    move-object p0, p2

    .line 2673220
    move-object v1, p0

    .line 2673221
    move-object v0, v1

    .line 2673222
    iget-object v1, v0, LX/5Kb;->a:LX/5Kc;

    iput-object p3, v1, LX/5Kc;->a:Lcom/facebook/java2js/JSValue;

    .line 2673223
    iget-object v1, v0, LX/5Kb;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673224
    move-object v0, v0

    .line 2673225
    goto/16 :goto_1

    .line 2673226
    :pswitch_2
    const/4 v0, 0x0

    .line 2673227
    new-instance v1, LX/5Kf;

    invoke-direct {v1}, LX/5Kf;-><init>()V

    .line 2673228
    sget-object p0, LX/5Kg;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/5Ke;

    .line 2673229
    if-nez p0, :cond_3

    .line 2673230
    new-instance p0, LX/5Ke;

    invoke-direct {p0}, LX/5Ke;-><init>()V

    .line 2673231
    :cond_3
    invoke-static {p0, p1, v0, v0, v1}, LX/5Ke;->a$redex0(LX/5Ke;LX/1De;IILX/5Kf;)V

    .line 2673232
    move-object v1, p0

    .line 2673233
    move-object v0, v1

    .line 2673234
    move-object v0, v0

    .line 2673235
    iget-object v1, v0, LX/5Ke;->a:LX/5Kf;

    iput-object p3, v1, LX/5Kf;->a:Lcom/facebook/java2js/JSValue;

    .line 2673236
    iget-object v1, v0, LX/5Ke;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673237
    move-object v0, v0

    .line 2673238
    iget-object v1, v0, LX/5Ke;->a:LX/5Kf;

    iput-object p4, v1, LX/5Kf;->b:LX/5KI;

    .line 2673239
    iget-object v1, v0, LX/5Ke;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673240
    move-object v0, v0

    .line 2673241
    goto/16 :goto_1

    .line 2673242
    :pswitch_3
    iget-object v0, p0, LX/JHb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Kj;

    const/4 v1, 0x0

    .line 2673243
    new-instance p0, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;

    invoke-direct {p0, v0}, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;-><init>(LX/5Kj;)V

    .line 2673244
    sget-object p2, LX/5Kj;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/5Ki;

    .line 2673245
    if-nez p2, :cond_4

    .line 2673246
    new-instance p2, LX/5Ki;

    invoke-direct {p2}, LX/5Ki;-><init>()V

    .line 2673247
    :cond_4
    invoke-static {p2, p1, v1, v1, p0}, LX/5Ki;->a$redex0(LX/5Ki;LX/1De;IILcom/facebook/componentscript/components/CSImage$CSImageImpl;)V

    .line 2673248
    move-object p0, p2

    .line 2673249
    move-object v1, p0

    .line 2673250
    move-object v0, v1

    .line 2673251
    iget-object v1, v0, LX/5Ki;->a:Lcom/facebook/componentscript/components/CSImage$CSImageImpl;

    iput-object p3, v1, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->a:Lcom/facebook/java2js/JSValue;

    .line 2673252
    iget-object v1, v0, LX/5Ki;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673253
    move-object v0, v0

    .line 2673254
    goto/16 :goto_1

    .line 2673255
    :pswitch_4
    const/4 v0, 0x0

    .line 2673256
    new-instance v1, LX/5Kl;

    invoke-direct {v1}, LX/5Kl;-><init>()V

    .line 2673257
    sget-object p0, LX/5Km;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/5Kk;

    .line 2673258
    if-nez p0, :cond_5

    .line 2673259
    new-instance p0, LX/5Kk;

    invoke-direct {p0}, LX/5Kk;-><init>()V

    .line 2673260
    :cond_5
    invoke-static {p0, p1, v0, v0, v1}, LX/5Kk;->a$redex0(LX/5Kk;LX/1De;IILX/5Kl;)V

    .line 2673261
    move-object v1, p0

    .line 2673262
    move-object v0, v1

    .line 2673263
    move-object v0, v0

    .line 2673264
    iget-object v1, v0, LX/5Kk;->a:LX/5Kl;

    iput-object p3, v1, LX/5Kl;->a:Lcom/facebook/java2js/JSValue;

    .line 2673265
    iget-object v1, v0, LX/5Kk;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673266
    move-object v0, v0

    .line 2673267
    iget-object v1, v0, LX/5Kk;->a:LX/5Kl;

    iput-object p4, v1, LX/5Kl;->b:LX/5KI;

    .line 2673268
    iget-object v1, v0, LX/5Kk;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673269
    move-object v0, v0

    .line 2673270
    goto/16 :goto_1

    .line 2673271
    :pswitch_5
    const/4 v0, 0x0

    .line 2673272
    new-instance v1, LX/5Kp;

    invoke-direct {v1}, LX/5Kp;-><init>()V

    .line 2673273
    sget-object p0, LX/5Kq;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/5Ko;

    .line 2673274
    if-nez p0, :cond_6

    .line 2673275
    new-instance p0, LX/5Ko;

    invoke-direct {p0}, LX/5Ko;-><init>()V

    .line 2673276
    :cond_6
    invoke-static {p0, p1, v0, v0, v1}, LX/5Ko;->a$redex0(LX/5Ko;LX/1De;IILX/5Kp;)V

    .line 2673277
    move-object v1, p0

    .line 2673278
    move-object v0, v1

    .line 2673279
    move-object v0, v0

    .line 2673280
    iget-object v1, v0, LX/5Ko;->a:LX/5Kp;

    iput-object p3, v1, LX/5Kp;->a:Lcom/facebook/java2js/JSValue;

    .line 2673281
    iget-object v1, v0, LX/5Ko;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673282
    move-object v0, v0

    .line 2673283
    iget-object v1, v0, LX/5Ko;->a:LX/5Kp;

    iput-object p4, v1, LX/5Kp;->b:LX/5KI;

    .line 2673284
    iget-object v1, v0, LX/5Ko;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673285
    move-object v0, v0

    .line 2673286
    goto/16 :goto_1

    .line 2673287
    :pswitch_6
    const/4 v0, 0x0

    .line 2673288
    new-instance v1, LX/5Kt;

    invoke-direct {v1}, LX/5Kt;-><init>()V

    .line 2673289
    sget-object p0, LX/5Ku;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/5Ks;

    .line 2673290
    if-nez p0, :cond_7

    .line 2673291
    new-instance p0, LX/5Ks;

    invoke-direct {p0}, LX/5Ks;-><init>()V

    .line 2673292
    :cond_7
    invoke-static {p0, p1, v0, v0, v1}, LX/5Ks;->a$redex0(LX/5Ks;LX/1De;IILX/5Kt;)V

    .line 2673293
    move-object v1, p0

    .line 2673294
    move-object v0, v1

    .line 2673295
    move-object v0, v0

    .line 2673296
    iget-object v1, v0, LX/5Ks;->a:LX/5Kt;

    iput-object p3, v1, LX/5Kt;->a:Lcom/facebook/java2js/JSValue;

    .line 2673297
    iget-object v1, v0, LX/5Ks;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673298
    move-object v0, v0

    .line 2673299
    goto/16 :goto_1

    .line 2673300
    :pswitch_7
    const/4 v0, 0x0

    .line 2673301
    new-instance v1, LX/5Kx;

    invoke-direct {v1}, LX/5Kx;-><init>()V

    .line 2673302
    sget-object p0, LX/5Ky;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/5Kw;

    .line 2673303
    if-nez p0, :cond_8

    .line 2673304
    new-instance p0, LX/5Kw;

    invoke-direct {p0}, LX/5Kw;-><init>()V

    .line 2673305
    :cond_8
    invoke-static {p0, p1, v0, v0, v1}, LX/5Kw;->a$redex0(LX/5Kw;LX/1De;IILX/5Kx;)V

    .line 2673306
    move-object v1, p0

    .line 2673307
    move-object v0, v1

    .line 2673308
    move-object v0, v0

    .line 2673309
    iget-object v1, v0, LX/5Kw;->a:LX/5Kx;

    iput-object p3, v1, LX/5Kx;->a:Lcom/facebook/java2js/JSValue;

    .line 2673310
    iget-object v1, v0, LX/5Kw;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673311
    move-object v0, v0

    .line 2673312
    iget-object v1, v0, LX/5Kw;->a:LX/5Kx;

    iput-object p4, v1, LX/5Kx;->b:LX/5KI;

    .line 2673313
    iget-object v1, v0, LX/5Kw;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673314
    move-object v0, v0

    .line 2673315
    goto/16 :goto_1

    .line 2673316
    :pswitch_8
    iget-object v0, p0, LX/JHb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hpj;

    const/4 v1, 0x0

    .line 2673317
    new-instance p0, LX/Hpi;

    invoke-direct {p0, v0}, LX/Hpi;-><init>(LX/Hpj;)V

    .line 2673318
    sget-object p2, LX/Hpj;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Hph;

    .line 2673319
    if-nez p2, :cond_9

    .line 2673320
    new-instance p2, LX/Hph;

    invoke-direct {p2}, LX/Hph;-><init>()V

    .line 2673321
    :cond_9
    invoke-static {p2, p1, v1, v1, p0}, LX/Hph;->a$redex0(LX/Hph;LX/1De;IILX/Hpi;)V

    .line 2673322
    move-object p0, p2

    .line 2673323
    move-object v1, p0

    .line 2673324
    move-object v0, v1

    .line 2673325
    iget-object v1, v0, LX/Hph;->a:LX/Hpi;

    iput-object p3, v1, LX/Hpi;->a:Lcom/facebook/java2js/JSValue;

    .line 2673326
    iget-object v1, v0, LX/Hph;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673327
    move-object v0, v0

    .line 2673328
    iget-object v1, v0, LX/Hph;->a:LX/Hpi;

    iput-object p4, v1, LX/Hpi;->b:LX/5KI;

    .line 2673329
    iget-object v1, v0, LX/Hph;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673330
    move-object v0, v0

    .line 2673331
    goto/16 :goto_1

    .line 2673332
    :pswitch_9
    iget-object v0, p0, LX/JHb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BsQ;

    const/4 v1, 0x0

    .line 2673333
    new-instance p0, LX/BsP;

    invoke-direct {p0, v0}, LX/BsP;-><init>(LX/BsQ;)V

    .line 2673334
    iget-object p2, v0, LX/BsQ;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/BsO;

    .line 2673335
    if-nez p2, :cond_a

    .line 2673336
    new-instance p2, LX/BsO;

    invoke-direct {p2, v0}, LX/BsO;-><init>(LX/BsQ;)V

    .line 2673337
    :cond_a
    invoke-static {p2, p1, v1, v1, p0}, LX/BsO;->a$redex0(LX/BsO;LX/1De;IILX/BsP;)V

    .line 2673338
    move-object p0, p2

    .line 2673339
    move-object v1, p0

    .line 2673340
    move-object v0, v1

    .line 2673341
    iget-object v1, v0, LX/BsO;->a:LX/BsP;

    iput-object p3, v1, LX/BsP;->a:Lcom/facebook/java2js/JSValue;

    .line 2673342
    iget-object v1, v0, LX/BsO;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673343
    move-object v0, v0

    .line 2673344
    goto/16 :goto_1

    .line 2673345
    :pswitch_a
    iget-object v0, p0, LX/JHb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Iv;

    const/4 v1, 0x0

    .line 2673346
    new-instance p0, LX/9Iu;

    invoke-direct {p0, v0}, LX/9Iu;-><init>(LX/9Iv;)V

    .line 2673347
    iget-object p2, v0, LX/9Iv;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/9It;

    .line 2673348
    if-nez p2, :cond_b

    .line 2673349
    new-instance p2, LX/9It;

    invoke-direct {p2, v0}, LX/9It;-><init>(LX/9Iv;)V

    .line 2673350
    :cond_b
    invoke-static {p2, p1, v1, v1, p0}, LX/9It;->a$redex0(LX/9It;LX/1De;IILX/9Iu;)V

    .line 2673351
    move-object p0, p2

    .line 2673352
    move-object v1, p0

    .line 2673353
    move-object v0, v1

    .line 2673354
    iget-object v1, v0, LX/9It;->a:LX/9Iu;

    iput-object p3, v1, LX/9Iu;->a:Lcom/facebook/java2js/JSValue;

    .line 2673355
    iget-object v1, v0, LX/9It;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673356
    move-object v0, v0

    .line 2673357
    goto/16 :goto_1

    .line 2673358
    :pswitch_b
    iget-object v0, p0, LX/JHb;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GeK;

    const/4 v1, 0x0

    .line 2673359
    new-instance p0, LX/GeJ;

    invoke-direct {p0, v0}, LX/GeJ;-><init>(LX/GeK;)V

    .line 2673360
    sget-object p2, LX/GeK;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/GeI;

    .line 2673361
    if-nez p2, :cond_c

    .line 2673362
    new-instance p2, LX/GeI;

    invoke-direct {p2}, LX/GeI;-><init>()V

    .line 2673363
    :cond_c
    invoke-static {p2, p1, v1, v1, p0}, LX/GeI;->a$redex0(LX/GeI;LX/1De;IILX/GeJ;)V

    .line 2673364
    move-object p0, p2

    .line 2673365
    move-object v1, p0

    .line 2673366
    move-object v0, v1

    .line 2673367
    iget-object v1, v0, LX/GeI;->a:LX/GeJ;

    iput-object p3, v1, LX/GeJ;->a:Lcom/facebook/java2js/JSValue;

    .line 2673368
    iget-object v1, v0, LX/GeI;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673369
    move-object v0, v0

    .line 2673370
    goto/16 :goto_1

    .line 2673371
    :pswitch_c
    const/4 v0, 0x0

    .line 2673372
    new-instance v1, LX/GeM;

    invoke-direct {v1}, LX/GeM;-><init>()V

    .line 2673373
    sget-object p0, LX/GeN;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/GeL;

    .line 2673374
    if-nez p0, :cond_d

    .line 2673375
    new-instance p0, LX/GeL;

    invoke-direct {p0}, LX/GeL;-><init>()V

    .line 2673376
    :cond_d
    invoke-static {p0, p1, v0, v0, v1}, LX/GeL;->a$redex0(LX/GeL;LX/1De;IILX/GeM;)V

    .line 2673377
    move-object v1, p0

    .line 2673378
    move-object v0, v1

    .line 2673379
    move-object v0, v0

    .line 2673380
    iget-object v1, v0, LX/GeL;->a:LX/GeM;

    iput-object p3, v1, LX/GeM;->a:Lcom/facebook/java2js/JSValue;

    .line 2673381
    iget-object v1, v0, LX/GeL;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673382
    move-object v0, v0

    .line 2673383
    goto/16 :goto_1

    .line 2673384
    :pswitch_d
    iget-object v0, p0, LX/JHb;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GeU;

    const/4 v1, 0x0

    .line 2673385
    new-instance p0, LX/GeT;

    invoke-direct {p0, v0}, LX/GeT;-><init>(LX/GeU;)V

    .line 2673386
    sget-object p2, LX/GeU;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/GeS;

    .line 2673387
    if-nez p2, :cond_e

    .line 2673388
    new-instance p2, LX/GeS;

    invoke-direct {p2}, LX/GeS;-><init>()V

    .line 2673389
    :cond_e
    invoke-static {p2, p1, v1, v1, p0}, LX/GeS;->a$redex0(LX/GeS;LX/1De;IILX/GeT;)V

    .line 2673390
    move-object p0, p2

    .line 2673391
    move-object v1, p0

    .line 2673392
    move-object v0, v1

    .line 2673393
    iget-object v1, v0, LX/GeS;->a:LX/GeT;

    iput-object p3, v1, LX/GeT;->a:Lcom/facebook/java2js/JSValue;

    .line 2673394
    iget-object v1, v0, LX/GeS;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673395
    move-object v0, v0

    .line 2673396
    iget-object v1, v0, LX/GeS;->a:LX/GeT;

    iput-object p4, v1, LX/GeT;->b:LX/5KI;

    .line 2673397
    iget-object v1, v0, LX/GeS;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673398
    move-object v0, v0

    .line 2673399
    goto/16 :goto_1

    .line 2673400
    :pswitch_e
    const/4 v0, 0x0

    .line 2673401
    new-instance v1, LX/GeQ;

    invoke-direct {v1}, LX/GeQ;-><init>()V

    .line 2673402
    sget-object p0, LX/GeR;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/GeP;

    .line 2673403
    if-nez p0, :cond_f

    .line 2673404
    new-instance p0, LX/GeP;

    invoke-direct {p0}, LX/GeP;-><init>()V

    .line 2673405
    :cond_f
    invoke-static {p0, p1, v0, v0, v1}, LX/GeP;->a$redex0(LX/GeP;LX/1De;IILX/GeQ;)V

    .line 2673406
    move-object v1, p0

    .line 2673407
    move-object v0, v1

    .line 2673408
    move-object v0, v0

    .line 2673409
    iget-object v1, v0, LX/GeP;->a:LX/GeQ;

    iput-object p3, v1, LX/GeQ;->a:Lcom/facebook/java2js/JSValue;

    .line 2673410
    iget-object v1, v0, LX/GeP;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673411
    move-object v0, v0

    .line 2673412
    iget-object v1, v0, LX/GeP;->a:LX/GeQ;

    iput-object p4, v1, LX/GeQ;->b:LX/5KI;

    .line 2673413
    iget-object v1, v0, LX/GeP;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673414
    move-object v0, v0

    .line 2673415
    goto/16 :goto_1

    .line 2673416
    :pswitch_f
    iget-object v0, p0, LX/JHb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GeZ;

    const/4 v1, 0x0

    .line 2673417
    new-instance p0, LX/GeY;

    invoke-direct {p0, v0}, LX/GeY;-><init>(LX/GeZ;)V

    .line 2673418
    sget-object p2, LX/GeZ;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/GeX;

    .line 2673419
    if-nez p2, :cond_10

    .line 2673420
    new-instance p2, LX/GeX;

    invoke-direct {p2}, LX/GeX;-><init>()V

    .line 2673421
    :cond_10
    invoke-static {p2, p1, v1, v1, p0}, LX/GeX;->a$redex0(LX/GeX;LX/1De;IILX/GeY;)V

    .line 2673422
    move-object p0, p2

    .line 2673423
    move-object v1, p0

    .line 2673424
    move-object v0, v1

    .line 2673425
    iget-object v1, v0, LX/GeX;->a:LX/GeY;

    iput-object p3, v1, LX/GeY;->a:Lcom/facebook/java2js/JSValue;

    .line 2673426
    iget-object v1, v0, LX/GeX;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673427
    move-object v0, v0

    .line 2673428
    iget-object v1, v0, LX/GeX;->a:LX/GeY;

    iput-object p4, v1, LX/GeY;->b:LX/5KI;

    .line 2673429
    iget-object v1, v0, LX/GeX;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673430
    move-object v0, v0

    .line 2673431
    goto/16 :goto_1

    .line 2673432
    :pswitch_10
    iget-object v0, p0, LX/JHb;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ged;

    const/4 v1, 0x0

    .line 2673433
    new-instance p0, LX/Gec;

    invoke-direct {p0, v0}, LX/Gec;-><init>(LX/Ged;)V

    .line 2673434
    sget-object p2, LX/Ged;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Geb;

    .line 2673435
    if-nez p2, :cond_11

    .line 2673436
    new-instance p2, LX/Geb;

    invoke-direct {p2}, LX/Geb;-><init>()V

    .line 2673437
    :cond_11
    invoke-static {p2, p1, v1, v1, p0}, LX/Geb;->a$redex0(LX/Geb;LX/1De;IILX/Gec;)V

    .line 2673438
    move-object p0, p2

    .line 2673439
    move-object v1, p0

    .line 2673440
    move-object v0, v1

    .line 2673441
    iget-object v1, v0, LX/Geb;->a:LX/Gec;

    iput-object p3, v1, LX/Gec;->a:Lcom/facebook/java2js/JSValue;

    .line 2673442
    iget-object v1, v0, LX/Geb;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673443
    move-object v0, v0

    .line 2673444
    goto/16 :goto_1

    .line 2673445
    :pswitch_11
    const/4 v0, 0x0

    .line 2673446
    new-instance v1, LX/Geg;

    invoke-direct {v1}, LX/Geg;-><init>()V

    .line 2673447
    sget-object p0, LX/Geh;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Gef;

    .line 2673448
    if-nez p0, :cond_12

    .line 2673449
    new-instance p0, LX/Gef;

    invoke-direct {p0}, LX/Gef;-><init>()V

    .line 2673450
    :cond_12
    invoke-static {p0, p1, v0, v0, v1}, LX/Gef;->a$redex0(LX/Gef;LX/1De;IILX/Geg;)V

    .line 2673451
    move-object v1, p0

    .line 2673452
    move-object v0, v1

    .line 2673453
    move-object v0, v0

    .line 2673454
    iget-object v1, v0, LX/Gef;->a:LX/Geg;

    iput-object p3, v1, LX/Geg;->a:Lcom/facebook/java2js/JSValue;

    .line 2673455
    iget-object v1, v0, LX/Gef;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673456
    move-object v0, v0

    .line 2673457
    goto/16 :goto_1

    .line 2673458
    :pswitch_12
    const/4 v0, 0x0

    .line 2673459
    new-instance v1, LX/Gek;

    invoke-direct {v1}, LX/Gek;-><init>()V

    .line 2673460
    sget-object p0, LX/Gel;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Gej;

    .line 2673461
    if-nez p0, :cond_13

    .line 2673462
    new-instance p0, LX/Gej;

    invoke-direct {p0}, LX/Gej;-><init>()V

    .line 2673463
    :cond_13
    invoke-static {p0, p1, v0, v0, v1}, LX/Gej;->a$redex0(LX/Gej;LX/1De;IILX/Gek;)V

    .line 2673464
    move-object v1, p0

    .line 2673465
    move-object v0, v1

    .line 2673466
    move-object v0, v0

    .line 2673467
    iget-object v1, v0, LX/Gej;->a:LX/Gek;

    iput-object p3, v1, LX/Gek;->a:Lcom/facebook/java2js/JSValue;

    .line 2673468
    iget-object v1, v0, LX/Gej;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673469
    move-object v0, v0

    .line 2673470
    iget-object v1, v0, LX/Gej;->a:LX/Gek;

    iput-object p4, v1, LX/Gek;->b:LX/5KI;

    .line 2673471
    iget-object v1, v0, LX/Gej;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2673472
    move-object v0, v0

    .line 2673473
    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x6ccd8ce0 -> :sswitch_b
        -0x6ccc9f28 -> :sswitch_9
        -0x6790155c -> :sswitch_7
        -0x4ff8f931 -> :sswitch_e
        -0x4f0e7f5e -> :sswitch_2
        -0x3224f83e -> :sswitch_0
        -0x2540be04 -> :sswitch_c
        -0x1a208137 -> :sswitch_12
        0x9ece280 -> :sswitch_11
        0x43ad02f8 -> :sswitch_10
        0x49d369d7 -> :sswitch_a
        0x58abc72e -> :sswitch_8
        0x60849017 -> :sswitch_f
        0x6a1b76cb -> :sswitch_3
        0x6a1c2e8d -> :sswitch_4
        0x6c294eaa -> :sswitch_d
        0x6d69bc85 -> :sswitch_5
        0x73263319 -> :sswitch_1
        0x770e09bd -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method
