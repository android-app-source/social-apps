.class public LX/IyJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/facebook/payments/cart/model/PaymentsCartParams;

.field private c:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2633164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633165
    iput-object p1, p0, LX/IyJ;->a:Landroid/content/Context;

    .line 2633166
    return-void
.end method

.method private a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2633119
    iget-object v0, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v0, v0

    .line 2633120
    iget-object v0, v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2633121
    invoke-static {}, Lcom/facebook/payments/form/model/ItemFormData;->newBuilder()LX/6xR;

    move-result-object v0

    .line 2633122
    iget-object v1, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v1, v1

    .line 2633123
    iget v1, v1, Lcom/facebook/payments/cart/model/CustomItemsConfig;->b:I

    .line 2633124
    iput v1, v0, LX/6xR;->a:I

    .line 2633125
    move-object v0, v0

    .line 2633126
    iput-object p1, v0, LX/6xR;->e:Landroid/os/Parcelable;

    .line 2633127
    move-object v1, v0

    .line 2633128
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    .line 2633129
    iget-object v0, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v0, v0

    .line 2633130
    iget-object v0, v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    sget-object v3, LX/6xN;->TITLE:LX/6xN;

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2633131
    sget-object v3, LX/6xN;->TITLE:LX/6xN;

    .line 2633132
    iget-object v0, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v0, v0

    .line 2633133
    iget-object v0, v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    sget-object v4, LX/6xN;->TITLE:LX/6xN;

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 2633134
    iget-object v4, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2633135
    invoke-virtual {v0, v4}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(Ljava/lang/String;)Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2633136
    :cond_0
    iget-object v0, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v0, v0

    .line 2633137
    iget-object v0, v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    sget-object v3, LX/6xN;->SUBTITLE:LX/6xN;

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2633138
    sget-object v3, LX/6xN;->SUBTITLE:LX/6xN;

    .line 2633139
    iget-object v0, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v0, v0

    .line 2633140
    iget-object v0, v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    sget-object v4, LX/6xN;->SUBTITLE:LX/6xN;

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 2633141
    iget-object v4, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2633142
    invoke-virtual {v0, v4}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(Ljava/lang/String;)Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2633143
    :cond_1
    iget-object v0, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v0, v0

    .line 2633144
    iget-object v0, v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    sget-object v3, LX/6xN;->PRICE:LX/6xN;

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2633145
    sget-object v3, LX/6xN;->PRICE:LX/6xN;

    .line 2633146
    iget-object v0, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v0, v0

    .line 2633147
    iget-object v0, v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    sget-object v4, LX/6xN;->PRICE:LX/6xN;

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-virtual {v0, p5}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(Ljava/lang/String;)Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2633148
    :cond_2
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2633149
    iput-object v0, v1, LX/6xR;->f:LX/0P1;

    .line 2633150
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    iget-object v2, p0, LX/IyJ;->b:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v2, v2, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0, v2}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v0

    sget-object v2, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    .line 2633151
    iput-object v2, v0, LX/6wu;->a:LX/6ws;

    .line 2633152
    move-object v0, v0

    .line 2633153
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    .line 2633154
    iget-object v2, p0, LX/IyJ;->a:Landroid/content/Context;

    sget-object v3, LX/6xK;->ITEM_FORM_CONTROLLER:LX/6xK;

    .line 2633155
    iget-object v4, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v4, v4

    .line 2633156
    iget-object v4, v4, Lcom/facebook/payments/cart/model/CustomItemsConfig;->a:Ljava/lang/String;

    invoke-static {v3, v4, v0}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v0

    invoke-virtual {v1}, LX/6xR;->a()Lcom/facebook/payments/form/model/ItemFormData;

    move-result-object v1

    .line 2633157
    iput-object v1, v0, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 2633158
    move-object v0, v0

    .line 2633159
    iput-object p4, v0, LX/6xW;->f:Ljava/lang/String;

    .line 2633160
    move-object v0, v0

    .line 2633161
    invoke-virtual {v0}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2633162
    iget-object v1, p0, LX/IyJ;->c:LX/6qh;

    invoke-virtual {v1, v0, p3}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 2633163
    return-void
.end method

.method private a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 2633167
    invoke-static {}, Lcom/facebook/payments/form/model/ItemFormData;->newBuilder()LX/6xR;

    move-result-object v0

    .line 2633168
    iput-object p1, v0, LX/6xR;->e:Landroid/os/Parcelable;

    .line 2633169
    move-object v0, v0

    .line 2633170
    iget v1, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->h:I

    move v1, v1

    .line 2633171
    iput v1, v0, LX/6xR;->a:I

    .line 2633172
    move-object v0, v0

    .line 2633173
    iget-object v1, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->e:Lcom/facebook/payments/cart/model/CatalogItemsConfig;

    move-object v1, v1

    .line 2633174
    if-eqz v1, :cond_0

    .line 2633175
    iget-object v1, p2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->e:Lcom/facebook/payments/cart/model/CatalogItemsConfig;

    move-object v1, v1

    .line 2633176
    iget-boolean v1, v1, Lcom/facebook/payments/cart/model/CatalogItemsConfig;->a:Z

    if-eqz v1, :cond_0

    .line 2633177
    sget-object v1, LX/6xN;->PRICE:LX/6xN;

    sget-object v2, LX/6xN;->PRICE:LX/6xN;

    iget-object v3, p0, LX/IyJ;->a:Landroid/content/Context;

    const v4, 0x7f081e41

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/6xe;->REQUIRED:LX/6xe;

    sget-object v5, LX/6xO;->PRICE:LX/6xO;

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;

    move-result-object v2

    .line 2633178
    iget-object v3, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v3, v3

    .line 2633179
    iget-object v4, v3, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v3, v4

    .line 2633180
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2633181
    iput-object v3, v2, LX/6xM;->e:Ljava/lang/String;

    .line 2633182
    move-object v2, v2

    .line 2633183
    invoke-virtual {v2}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    .line 2633184
    iput-object v1, v0, LX/6xR;->f:LX/0P1;

    .line 2633185
    :cond_0
    iget-object v1, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2633186
    invoke-static {v1}, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->a(Ljava/lang/String;)LX/73R;

    move-result-object v1

    .line 2633187
    iget-object v2, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2633188
    iput-object v2, v1, LX/73R;->d:Ljava/lang/String;

    .line 2633189
    move-object v1, v1

    .line 2633190
    iget-object v2, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->e:Ljava/lang/String;

    move-object v2, v2

    .line 2633191
    iput-object v2, v1, LX/73R;->e:Ljava/lang/String;

    .line 2633192
    move-object v1, v1

    .line 2633193
    invoke-virtual {p1}, Lcom/facebook/payments/cart/model/SimpleCartItem;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/73R;->b(Ljava/lang/String;)LX/73R;

    move-result-object v1

    invoke-virtual {v1}, LX/73R;->a()Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    move-result-object v1

    .line 2633194
    iput-object v1, v0, LX/6xR;->d:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    .line 2633195
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v1

    iget-object v2, p0, LX/IyJ;->b:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v2, v2, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v1, v2}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v1

    sget-object v2, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    .line 2633196
    iput-object v2, v1, LX/6wu;->a:LX/6ws;

    .line 2633197
    move-object v1, v1

    .line 2633198
    invoke-virtual {v1}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    .line 2633199
    iget-object v2, p0, LX/IyJ;->a:Landroid/content/Context;

    sget-object v3, LX/6xK;->ITEM_FORM_CONTROLLER:LX/6xK;

    iget-object v4, p0, LX/IyJ;->a:Landroid/content/Context;

    const v5, 0x7f0838d3

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v1

    invoke-virtual {v0}, LX/6xR;->a()Lcom/facebook/payments/form/model/ItemFormData;

    move-result-object v0

    .line 2633200
    iput-object v0, v1, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 2633201
    move-object v0, v1

    .line 2633202
    iput-object p3, v0, LX/6xW;->f:Ljava/lang/String;

    .line 2633203
    move-object v0, v0

    .line 2633204
    invoke-virtual {v0}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2633205
    iget-object v1, p0, LX/IyJ;->c:LX/6qh;

    invoke-virtual {v1, v0, p4}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 2633206
    return-void
.end method

.method public static b(LX/0QB;)LX/IyJ;
    .locals 2

    .prologue
    .line 2633117
    new-instance v1, LX/IyJ;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/IyJ;-><init>(Landroid/content/Context;)V

    .line 2633118
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2633116
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(LX/6qh;Lcom/facebook/payments/cart/model/PaymentsCartParams;)V
    .locals 0

    .prologue
    .line 2633113
    iput-object p1, p0, LX/IyJ;->c:LX/6qh;

    .line 2633114
    iput-object p2, p0, LX/IyJ;->b:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    .line 2633115
    return-void
.end method

.method public final a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;)V
    .locals 6

    .prologue
    .line 2633101
    sget-object v0, LX/IyI;->a:[I

    .line 2633102
    iget-object v1, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->b:LX/IyK;

    move-object v1, v1

    .line 2633103
    invoke-virtual {v1}, LX/IyK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2633104
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2633105
    :pswitch_0
    const/4 v3, 0x2

    iget-object v0, p0, LX/IyJ;->a:Landroid/content/Context;

    const v1, 0x7f081e43

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/IyJ;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;ILjava/lang/String;Ljava/lang/String;)V

    .line 2633106
    :goto_0
    return-void

    .line 2633107
    :pswitch_1
    iget-object v0, p0, LX/IyJ;->a:Landroid/content/Context;

    const v1, 0x7f081e43

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-direct {p0, p1, p2, v0, v1}, LX/IyJ;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;Ljava/lang/String;I)V

    goto :goto_0

    .line 2633108
    :pswitch_2
    const/4 v3, 0x4

    iget-object v0, p0, LX/IyJ;->a:Landroid/content/Context;

    const v1, 0x7f081e44

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2633109
    iget-object v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 2633110
    iget-object v1, v0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v0, v1

    .line 2633111
    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/IyJ;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2633112
    :pswitch_3
    iget-object v0, p0, LX/IyJ;->a:Landroid/content/Context;

    const v1, 0x7f081e44

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-direct {p0, p1, p2, v0, v1}, LX/IyJ;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;Ljava/lang/String;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
