.class public final LX/Hn8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V
    .locals 0

    .prologue
    .line 2501371
    iput-object p1, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501372
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2501373
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2501374
    iget-object v0, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    invoke-virtual {v0}, LX/HnH;->d()V

    .line 2501375
    :cond_0
    iget-object v0, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->q:LX/Hmo;

    const/16 v1, 0x1388

    .line 2501376
    iget-object p0, v0, LX/Hmo;->a:LX/0TD;

    new-instance p1, LX/Hmm;

    invoke-direct {p1, v0, v1}, LX/Hmm;-><init>(LX/Hmo;I)V

    invoke-interface {p0, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2501377
    return-void

    .line 2501378
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2501379
    iget-object v0, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-virtual {v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2501380
    :goto_0
    return-void

    .line 2501381
    :cond_0
    const-string v0, "BEAM"

    const-string v1, "Failure in futures"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2501382
    iget-object v0, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->u:LX/03V;

    const-string v1, "BEAM"

    const-string v2, "Failure in futures"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2501383
    instance-of v0, p1, LX/Hmx;

    if-eqz v0, :cond_1

    .line 2501384
    iget-object v0, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    iget-object v1, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const v2, 0x7f083970

    invoke-virtual {v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast p1, LX/Hmx;

    .line 2501385
    iget-object v2, p1, LX/Hmx;->mSenderErrorMessage:Ljava/lang/String;

    move-object v2, v2

    .line 2501386
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/HnH;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2501387
    :cond_1
    instance-of v0, p1, LX/Hmy;

    if-nez v0, :cond_3

    .line 2501388
    instance-of v0, p1, LX/HnI;

    if-eqz v0, :cond_2

    .line 2501389
    iget-object v0, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    invoke-virtual {v0}, LX/HnH;->f()V

    goto :goto_0

    .line 2501390
    :cond_2
    instance-of v0, p1, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_3

    .line 2501391
    iget-object v0, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    invoke-virtual {v0}, LX/HnH;->g()V

    goto :goto_0

    .line 2501392
    :cond_3
    iget-object v0, p0, LX/Hn8;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    invoke-virtual {v0}, LX/HnH;->e()V

    goto :goto_0
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501393
    check-cast p1, Ljava/lang/Boolean;

    invoke-direct {p0, p1}, LX/Hn8;->a(Ljava/lang/Boolean;)V

    return-void
.end method
