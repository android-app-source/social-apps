.class public final LX/Iun;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/support/v4/app/FragmentActivity;

.field public final synthetic b:Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    .prologue
    .line 2627353
    iput-object p1, p0, LX/Iun;->b:Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;

    iput-object p2, p0, LX/Iun;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x561ff427

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2627334
    iget-object v1, p0, LX/Iun;->b:Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;

    const-string v2, "getgames_ad_choices_tap"

    invoke-static {v1, v2}, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->b(Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;Ljava/lang/String;)V

    .line 2627335
    iget-object v1, p0, LX/Iun;->a:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0838af

    const p0, 0x3f666666    # 0.9f

    .line 2627336
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 2627337
    const v5, 0x7f0309df

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    .line 2627338
    const-string v5, "window"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 2627339
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    .line 2627340
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 2627341
    invoke-virtual {v5, v7}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2627342
    const v5, 0x7f0d1910

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/neko/util/HackWebView;

    .line 2627343
    invoke-virtual {v5}, Lcom/facebook/neko/util/HackWebView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v8, v7, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    mul-float/2addr v8, p0

    float-to-int v8, v8

    iput v8, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2627344
    const v6, 0x7f0d04de

    invoke-virtual {v4, v6}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ProgressBar;

    .line 2627345
    invoke-virtual {v6}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    mul-float/2addr v7, p0

    float-to-int v7, v7

    iput v7, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2627346
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2627347
    invoke-virtual {v5, v8}, Lcom/facebook/neko/util/HackWebView;->loadUrl(Ljava/lang/String;)V

    .line 2627348
    const v7, 0x7f0d1911

    invoke-virtual {v4, v7}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/resources/ui/FbTextView;

    .line 2627349
    new-instance p0, Landroid/app/AlertDialog$Builder;

    invoke-direct {p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const p0, 0x7f0838b4

    new-instance p1, LX/IvA;

    invoke-direct {p1}, LX/IvA;-><init>()V

    invoke-virtual {v4, p0, p1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2627350
    new-instance v4, LX/Iv9;

    invoke-direct {v4, v8, v6, v1, v7}, LX/Iv9;-><init>(Ljava/lang/String;Landroid/widget/ProgressBar;Landroid/content/Context;Lcom/facebook/resources/ui/FbTextView;)V

    .line 2627351
    invoke-virtual {v5, v4}, Lcom/facebook/neko/util/HackWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2627352
    const v1, -0x45bcba21

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
