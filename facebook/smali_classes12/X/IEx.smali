.class public LX/IEx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2553505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2553506
    iput-object p1, p0, LX/IEx;->a:LX/0Zb;

    .line 2553507
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IEx;->b:Ljava/lang/String;

    .line 2553508
    return-void
.end method

.method public static c(LX/IEx;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2553509
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "birthday_post"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "event"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    iget-object v1, p0, LX/IEx;->b:Ljava/lang/String;

    .line 2553510
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2553511
    move-object v0, v0

    .line 2553512
    return-object v0
.end method
