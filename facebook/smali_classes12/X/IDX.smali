.class public final LX/IDX;
.super LX/84a;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendlist/fragment/FriendListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V
    .locals 0

    .prologue
    .line 2550929
    iput-object p1, p0, LX/IDX;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-direct {p0}, LX/84a;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2550930
    check-cast p1, LX/84Z;

    .line 2550931
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/IDX;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    if-nez v0, :cond_1

    .line 2550932
    :cond_0
    :goto_0
    return-void

    .line 2550933
    :cond_1
    iget-object v0, p0, LX/IDX;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    invoke-virtual {v0}, LX/IE2;->c()Ljava/util/Map;

    move-result-object v0

    iget-wide v2, p1, LX/84Z;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IDH;

    .line 2550934
    if-eqz v0, :cond_0

    .line 2550935
    iget-object v1, v0, LX/IDH;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v1, v1

    .line 2550936
    iget-object v2, p1, LX/84Z;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq v1, v2, :cond_0

    .line 2550937
    iget-object v1, p0, LX/IDX;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-wide v2, p1, LX/84Z;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/friendlist/fragment/FriendListFragment;->a$redex0(Lcom/facebook/friendlist/fragment/FriendListFragment;Ljava/lang/String;)V

    .line 2550938
    iget-object v1, p1, LX/84Z;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2550939
    iput-object v1, v0, LX/IDH;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2550940
    iget-object v0, p0, LX/IDX;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    const v1, -0x239d5ab9

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
