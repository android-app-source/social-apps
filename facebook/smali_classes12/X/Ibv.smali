.class public LX/Ibv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Ibv;


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2594913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2594914
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/690;->a(Landroid/content/Context;)V

    .line 2594915
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Ibv;->a:I

    .line 2594916
    return-void
.end method

.method public static a(LX/0QB;)LX/Ibv;
    .locals 4

    .prologue
    .line 2594917
    sget-object v0, LX/Ibv;->b:LX/Ibv;

    if-nez v0, :cond_1

    .line 2594918
    const-class v1, LX/Ibv;

    monitor-enter v1

    .line 2594919
    :try_start_0
    sget-object v0, LX/Ibv;->b:LX/Ibv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2594920
    if-eqz v2, :cond_0

    .line 2594921
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2594922
    new-instance p0, LX/Ibv;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/Ibv;-><init>(Landroid/content/Context;)V

    .line 2594923
    move-object v0, p0

    .line 2594924
    sput-object v0, LX/Ibv;->b:LX/Ibv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2594925
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2594926
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2594927
    :cond_1
    sget-object v0, LX/Ibv;->b:LX/Ibv;

    return-object v0

    .line 2594928
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2594929
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
