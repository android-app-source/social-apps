.class public LX/Hsz;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0jK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2515243
    const-class v0, LX/Hsz;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Hsz;->a:LX/0jK;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2515244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2515245
    return-void
.end method

.method public static a(LX/0il;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesRemovedUrls;",
            ":",
            "LX/0j5;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicSetters$SetsRemovedURLs",
            "<TMutation;>;:",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams$SetsShareParams",
            "<TMutation;>;Services::",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;)V"
        }
    .end annotation

    .prologue
    .line 2515246
    move-object v0, p0

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/Hsz;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    .line 2515247
    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v3, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 2515248
    if-eqz v3, :cond_1

    move-object v0, v1

    .line 2515249
    check-cast v0, LX/0jL;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRemovedUrls()LX/0Px;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2515250
    iget-object v3, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V

    .line 2515251
    iget-object v3, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRemovedUrls()LX/0Px;

    move-result-object v3

    invoke-static {v3, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2515252
    iget-object v3, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v3, :cond_0

    .line 2515253
    iget-object v3, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v3

    iput-object v3, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2515254
    :cond_0
    iget-object v3, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v3, v2}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setRemovedUrls(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2515255
    iget-object v3, v0, LX/0jL;->a:LX/0cA;

    sget-object v4, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v3, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2515256
    :cond_1
    check-cast v1, LX/0jL;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2515257
    return-void
.end method
