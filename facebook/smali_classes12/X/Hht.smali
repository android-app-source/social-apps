.class public LX/Hht;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/content/Intent;


# instance fields
.field private final b:LX/0tX;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Lx;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4nT;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0if;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2496353
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, LX/Hht;->a:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0tX;LX/0Ot;LX/0if;LX/0Ot;LX/0Or;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "LX/3Lx;",
            ">;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/0Ot",
            "<",
            "LX/4nT;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2496354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2496355
    iput-object p1, p0, LX/Hht;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2496356
    iput-object p6, p0, LX/Hht;->d:LX/0Or;

    .line 2496357
    iput-object p3, p0, LX/Hht;->e:LX/0Ot;

    .line 2496358
    iput-object p2, p0, LX/Hht;->b:LX/0tX;

    .line 2496359
    iput-object p5, p0, LX/Hht;->f:LX/0Ot;

    .line 2496360
    iput-object p4, p0, LX/Hht;->g:LX/0if;

    .line 2496361
    return-void
.end method

.method public static b(LX/0QB;)LX/Hht;
    .locals 7

    .prologue
    .line 2496362
    new-instance v0, LX/Hht;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    const/16 v3, 0x123d

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    const/16 v5, 0x3770

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x15e7

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Hht;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0tX;LX/0Ot;LX/0if;LX/0Ot;LX/0Or;)V

    .line 2496363
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2496364
    iget-object v0, p0, LX/Hht;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, LX/0yq;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 2496365
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fb.com/reg?i=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2496366
    iget-object v0, p0, LX/Hht;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lx;

    invoke-virtual {v0, p1}, LX/3Lx;->a(Ljava/lang/String;)LX/4hT;

    move-result-object v0

    .line 2496367
    if-nez v0, :cond_0

    .line 2496368
    const/4 v0, 0x0

    .line 2496369
    :goto_0
    return v0

    .line 2496370
    :cond_0
    new-instance v1, LX/4Cz;

    invoke-direct {v1}, LX/4Cz;-><init>()V

    const-string v2, "top_up"

    invoke-virtual {v1, v2}, LX/4Cz;->a(Ljava/lang/String;)LX/4Cz;

    move-result-object v1

    const-string v2, "top_up"

    invoke-virtual {v1, v2}, LX/4Cz;->b(Ljava/lang/String;)LX/4Cz;

    move-result-object v1

    .line 2496371
    iget-object v2, v0, LX/4hT;->rawInput_:Ljava/lang/String;

    move-object v0, v2

    .line 2496372
    invoke-virtual {v1, v0}, LX/4Cz;->c(Ljava/lang/String;)LX/4Cz;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Cz;->a(Ljava/lang/Integer;)LX/4Cz;

    move-result-object v0

    .line 2496373
    invoke-static {}, LX/Hi0;->a()LX/Hhz;

    move-result-object v1

    .line 2496374
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2496375
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2496376
    iget-object v1, p0, LX/Hht;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2496377
    const/4 v0, 0x1

    goto :goto_0
.end method
