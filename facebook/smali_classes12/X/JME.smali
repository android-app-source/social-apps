.class public LX/JME;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/JME;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 2683147
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 2683148
    iput-boolean p2, p0, LX/JME;->a:Z

    .line 2683149
    return-void
.end method

.method private k()LX/5pH;
    .locals 3

    .prologue
    .line 2683139
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2683140
    const-string v1, "target"

    .line 2683141
    iget v2, p0, LX/5r0;->c:I

    move v2, v2

    .line 2683142
    invoke-interface {v0, v1, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2683143
    const-string v1, "value"

    .line 2683144
    iget-boolean v2, p0, LX/JME;->a:Z

    move v2, v2

    .line 2683145
    invoke-interface {v0, v1, v2}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2683146
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2683134
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2683135
    invoke-virtual {p0}, LX/JME;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/JME;->k()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2683136
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683138
    const-string v0, "topChange"

    return-object v0
.end method

.method public final f()S
    .locals 1

    .prologue
    .line 2683137
    const/4 v0, 0x0

    return v0
.end method
