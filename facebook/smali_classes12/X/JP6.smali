.class public LX/JP6;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JP6",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2688811
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2688812
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JP6;->b:LX/0Zi;

    .line 2688813
    iput-object p1, p0, LX/JP6;->a:LX/0Ot;

    .line 2688814
    return-void
.end method

.method public static a(LX/0QB;)LX/JP6;
    .locals 4

    .prologue
    .line 2688815
    const-class v1, LX/JP6;

    monitor-enter v1

    .line 2688816
    :try_start_0
    sget-object v0, LX/JP6;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688817
    sput-object v2, LX/JP6;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688818
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688819
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688820
    new-instance v3, LX/JP6;

    const/16 p0, 0x1fd6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JP6;-><init>(LX/0Ot;)V

    .line 2688821
    move-object v0, v3

    .line 2688822
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688823
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JP6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688824
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2688791
    check-cast p2, LX/JP5;

    .line 2688792
    iget-object v0, p0, LX/JP6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;

    iget-object v1, p2, LX/JP5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JP5;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p2, LX/JP5;->c:LX/1Pn;

    const/4 v8, 0x3

    .line 2688793
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v4, ""

    .line 2688794
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b257f

    invoke-interface {v5, v6}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b2580

    invoke-interface {v5, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020a3d

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v6, 0x7f0a010c

    invoke-virtual {v4, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b2582

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/16 v6, 0x8

    const v7, 0x7f0b2571

    invoke-interface {v4, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->u()LX/0Px;

    move-result-object v5

    const/4 v7, 0x0

    .line 2688795
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2688796
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2688797
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    move v6, v7

    .line 2688798
    :goto_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p2

    if-ge v6, p2, :cond_2

    .line 2688799
    const/16 p2, 0x9

    if-le v6, p2, :cond_0

    .line 2688800
    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2688801
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2688802
    :cond_0
    const/4 p2, 0x4

    if-le v6, p2, :cond_1

    .line 2688803
    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {v10, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2688804
    :cond_1
    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {v9, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2688805
    :cond_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    invoke-static {v0, p1, v9}, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->a(Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;LX/1De;Ljava/util/List;)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {v0, p1, v10}, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->a(Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;LX/1De;Ljava/util/List;)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {v0, p1, p0}, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->a(Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;LX/1De;Ljava/util/List;)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    move-object v5, v6

    .line 2688806
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->c:LX/JOt;

    invoke-virtual {v5, p1}, LX/JOt;->c(LX/1De;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/JOs;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/JOs;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/JOs;->a(LX/1Pn;)LX/JOs;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b2581

    invoke-interface {v4, v8, v5}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    .line 2688807
    const v5, 0x2e8c418

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2688808
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2688809
    return-object v0

    .line 2688810
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2688769
    invoke-static {}, LX/1dS;->b()V

    .line 2688770
    iget v0, p1, LX/1dQ;->b:I

    .line 2688771
    packed-switch v0, :pswitch_data_0

    .line 2688772
    :goto_0
    return-object v2

    .line 2688773
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2688774
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2688775
    check-cast v1, LX/JP5;

    .line 2688776
    iget-object v3, p0, LX/JP6;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;

    iget-object v4, v1, LX/JP5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/JP5;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688777
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p1

    .line 2688778
    if-nez p1, :cond_2

    if-eqz v4, :cond_2

    .line 2688779
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 2688780
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p1

    move-object p2, p1

    .line 2688781
    :goto_1
    if-nez p2, :cond_0

    .line 2688782
    iget-object p1, v3, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JPN;

    const-string p2, "Cannot open profile without GraphQLPage"

    invoke-virtual {p1, v4, v5, p2}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2688783
    :goto_2
    goto :goto_0

    .line 2688784
    :cond_0
    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p1

    if-nez p1, :cond_1

    .line 2688785
    iget-object p1, v3, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JPN;

    const-string p2, "Cannot open profile LinkifyTargetBuilder.getLinkifyTarget(page) is null"

    invoke-virtual {p1, v4, v5, p2}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_2

    .line 2688786
    :cond_1
    iget-object p1, v3, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JPN;

    invoke-virtual {p1, v4, v5}, LX/JPN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    .line 2688787
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2688788
    const-string p0, "extra_is_admin"

    const/4 v1, 0x1

    invoke-virtual {p1, p0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2688789
    const-string p0, "extra_page_tab"

    sget-object v1, LX/BEQ;->INSIGHTS:LX/BEQ;

    invoke-virtual {p1, p0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2688790
    iget-object p0, v3, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->a:LX/1nA;

    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p2

    invoke-virtual {p0, v0, p2, p1}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    goto :goto_2

    :cond_2
    move-object p2, p1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2e8c418
        :pswitch_0
    .end packed-switch
.end method
