.class public final LX/JHJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68J;


# instance fields
.field public final synthetic a:LX/5pC;

.field public final synthetic b:Lcom/facebook/android/maps/MapView;

.field public final synthetic c:Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;


# direct methods
.method public constructor <init>(Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;LX/5pC;Lcom/facebook/android/maps/MapView;)V
    .locals 0

    .prologue
    .line 2672600
    iput-object p1, p0, LX/JHJ;->c:Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;

    iput-object p2, p0, LX/JHJ;->a:LX/5pC;

    iput-object p3, p0, LX/JHJ;->b:Lcom/facebook/android/maps/MapView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/680;)V
    .locals 13

    .prologue
    .line 2672601
    invoke-virtual {p1}, LX/680;->b()V

    .line 2672602
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/JHJ;->a:LX/5pC;

    invoke-interface {v1}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 2672603
    iget-object v1, p0, LX/JHJ;->a:LX/5pC;

    invoke-interface {v1, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v1

    .line 2672604
    new-instance v2, LX/JH8;

    iget-object v3, p0, LX/JHJ;->b:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v3}, Lcom/facebook/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/JH8;-><init>(Landroid/content/Context;)V

    .line 2672605
    new-instance v4, Lcom/facebook/android/maps/model/LatLng;

    const-string v5, "latitude"

    invoke-interface {v1, v5}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    const-string v5, "longitude"

    invoke-interface {v1, v5}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    iput-object v4, v2, LX/JH8;->f:Lcom/facebook/android/maps/model/LatLng;

    .line 2672606
    const-string v4, "subtitle"

    invoke-interface {v1, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2672607
    const-string v4, "subtitle"

    invoke-interface {v1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, LX/JH8;->h:Ljava/lang/String;

    .line 2672608
    :cond_0
    const-string v4, "anchorX"

    invoke-interface {v1, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "anchorY"

    invoke-interface {v1, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2672609
    const-string v4, "anchorX"

    invoke-interface {v1, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v2, LX/JH8;->j:F

    .line 2672610
    const-string v4, "anchorY"

    invoke-interface {v1, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v2, LX/JH8;->k:F

    .line 2672611
    iget v4, v2, LX/JH8;->j:F

    float-to-double v4, v4

    iget v6, v2, LX/JH8;->k:F

    float-to-double v6, v6

    .line 2672612
    const/4 v10, 0x1

    iput-boolean v10, v2, LX/JH8;->i:Z

    .line 2672613
    double-to-float v10, v4

    iput v10, v2, LX/JH8;->j:F

    .line 2672614
    double-to-float v10, v6

    iput v10, v2, LX/JH8;->k:F

    .line 2672615
    iget-object v10, v2, LX/JH8;->e:LX/698;

    if-eqz v10, :cond_1

    .line 2672616
    iget-object v10, v2, LX/JH8;->e:LX/698;

    iget v11, v2, LX/JH8;->j:F

    iget v12, v2, LX/JH8;->k:F

    invoke-virtual {v10, v11, v12}, LX/698;->f(FF)V

    .line 2672617
    :cond_1
    invoke-virtual {v2}, LX/JH8;->a()V

    .line 2672618
    :cond_2
    const-string v4, "title"

    invoke-interface {v1, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2672619
    const-string v4, "title"

    invoke-interface {v1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, LX/JH8;->g:Ljava/lang/String;

    .line 2672620
    :cond_3
    const-string v4, "image"

    invoke-interface {v1, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2672621
    const-string v4, "image"

    invoke-interface {v1, v4}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v4

    .line 2672622
    const-string v5, "uri"

    invoke-interface {v4, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2672623
    if-nez v4, :cond_b

    .line 2672624
    const/4 v5, 0x0

    iput-object v5, v2, LX/JH8;->l:LX/68w;

    .line 2672625
    invoke-virtual {v2}, LX/JH8;->a()V

    .line 2672626
    :cond_4
    :goto_1
    iget-object v1, v2, LX/JH8;->d:LX/699;

    if-nez v1, :cond_9

    .line 2672627
    new-instance v1, LX/699;

    invoke-direct {v1}, LX/699;-><init>()V

    .line 2672628
    iget-object v3, v2, LX/JH8;->f:Lcom/facebook/android/maps/model/LatLng;

    if-eqz v3, :cond_5

    .line 2672629
    iget-object v3, v2, LX/JH8;->f:Lcom/facebook/android/maps/model/LatLng;

    .line 2672630
    iput-object v3, v1, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2672631
    :cond_5
    iget-boolean v3, v2, LX/JH8;->i:Z

    if-eqz v3, :cond_6

    .line 2672632
    iget v3, v2, LX/JH8;->j:F

    iget v4, v2, LX/JH8;->k:F

    invoke-virtual {v1, v3, v4}, LX/699;->a(FF)LX/699;

    .line 2672633
    :cond_6
    iget-object v3, v2, LX/JH8;->g:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 2672634
    iget-object v3, v2, LX/JH8;->g:Ljava/lang/String;

    .line 2672635
    iput-object v3, v1, LX/699;->i:Ljava/lang/String;

    .line 2672636
    :cond_7
    iget-object v3, v2, LX/JH8;->h:Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 2672637
    iget-object v3, v2, LX/JH8;->h:Ljava/lang/String;

    .line 2672638
    iput-object v3, v1, LX/699;->h:Ljava/lang/String;

    .line 2672639
    :cond_8
    invoke-static {v2}, LX/JH8;->d(LX/JH8;)LX/68w;

    move-result-object v3

    .line 2672640
    iput-object v3, v1, LX/699;->c:LX/68w;

    .line 2672641
    move-object v1, v1

    .line 2672642
    iput-object v1, v2, LX/JH8;->d:LX/699;

    .line 2672643
    :cond_9
    iget-object v1, v2, LX/JH8;->d:LX/699;

    move-object v1, v1

    .line 2672644
    invoke-virtual {p1, v1}, LX/680;->a(LX/699;)LX/698;

    move-result-object v1

    iput-object v1, v2, LX/JH8;->e:LX/698;

    .line 2672645
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 2672646
    :cond_a
    return-void

    .line 2672647
    :cond_b
    const-string v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_c

    const-string v5, "https://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 2672648
    :cond_c
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v5

    invoke-virtual {v5}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 2672649
    invoke-static {}, LX/4AN;->b()LX/1HI;

    move-result-object v6

    .line 2672650
    invoke-virtual {v6, v5, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v6

    iput-object v6, v2, LX/JH8;->c:LX/1ca;

    .line 2672651
    invoke-static {}, LX/4AN;->a()LX/1bk;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v5

    check-cast v5, LX/1bk;

    iget-object v6, v2, LX/JH8;->m:LX/1Ai;

    invoke-virtual {v5, v6}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v5

    check-cast v5, LX/1bk;

    iget-object v6, v2, LX/JH8;->a:LX/1aX;

    .line 2672652
    iget-object v4, v6, LX/1aX;->f:LX/1aZ;

    move-object v6, v4

    .line 2672653
    invoke-virtual {v5, v6}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v5

    check-cast v5, LX/1bk;

    invoke-virtual {v5}, LX/1Ae;->h()LX/1bp;

    move-result-object v5

    .line 2672654
    iget-object v6, v2, LX/JH8;->a:LX/1aX;

    invoke-virtual {v6, v5}, LX/1aX;->a(LX/1aZ;)V

    goto/16 :goto_1

    .line 2672655
    :cond_d
    iget-object v5, v2, LX/JH8;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "drawable"

    iget-object v7, v2, LX/JH8;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v4, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    move v5, v5

    .line 2672656
    invoke-static {v5}, LX/690;->a(I)LX/68w;

    move-result-object v5

    move-object v5, v5

    .line 2672657
    iput-object v5, v2, LX/JH8;->l:LX/68w;

    .line 2672658
    invoke-virtual {v2}, LX/JH8;->a()V

    goto/16 :goto_1
.end method
